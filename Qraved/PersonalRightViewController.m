//
//  PersonalRightViewController.m
//  PersonalHomePageDemo
//
//  Created by Kegem Huang on 2017/3/15.
//  Copyright © 2017年 huangkejin. All rights reserved.
//

#import "PersonalRightViewController.h"
#import "ProfileHandler.h"
#import "IMGPhotoModel.h"
#import "AlbumViewController.h"
#import "IMGDish.h"
#import "LoadingView.h"
@interface PersonalRightViewController ()
{
    int offset;
}
@property (nonatomic, strong) NSMutableArray *photoArr;
@property (nonatomic, assign) BOOL isTouch;
@end

@implementation PersonalRightViewController
- (void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
     [self requestData];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    offset=0;
    self.photoArr = [NSMutableArray array];
   
    [IMGAmplitudeUtil trackProfileWithName:@"RC - View User Profile Photo Tab" andUserId:[IMGUser currentUser].userId andLocation:nil];
    
    [self createUI];
     _photoCollection.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logOutClcik) name:@"logoutNotification" object:nil];
    
   _photoCollection.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(downRefreshData)];
}
- (void)downRefreshData{
    offset =0;
  [self.photoArr removeAllObjects];
   [self getPhotoList];
    

}
- (void)logOutClcik{
    offset = 0;
    [self.photoArr removeAllObjects];
    [_photoCollection reloadData];
}
-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logoutNotification" object:nil];
}
- (void)createUI{

    self.view.backgroundColor = [UIColor whiteColor];
    
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    
    layout.minimumLineSpacing = 1;
    layout.minimumInteritemSpacing = 1;
    
    _photoCollection = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    _photoCollection.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _photoCollection.backgroundColor = [UIColor whiteColor];
    _photoCollection.delegate = self;
    _photoCollection.dataSource = self;
    _photoCollection.showsHorizontalScrollIndicator = NO;
    _photoCollection.showsVerticalScrollIndicator = NO;
    if (@available(iOS 11.0, *)) {
        _photoCollection.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    [self.view addSubview:_photoCollection];
    
    [_photoCollection registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"instagramCell"];

}

- (void)loadMoreData{
    [self getPhotoList];
}
- (void)requestData{
    
    [self getPhotoList];
}

- (void)getPhotoList{
    [[LoadingView sharedLoadingView] startLoading];
    IMGUser * user = [IMGUser currentUser];
    NSDictionary *param = @{@"targetUserId":self.isSelf?_otherUser.userId:user.userId,@"limit":@"20",@"offset":[NSString stringWithFormat:@"%d",offset]};
    [ProfileHandler getPhotos:param andSuccessBlock:^(NSMutableArray *array) {
         [[LoadingView sharedLoadingView] stopLoading];
         [_photoCollection.mj_footer endRefreshing];
        [_photoCollection.mj_header endRefreshing];

        
            if ([array count] >0) {
                offset += 20;
                
                [self.photoArr addObjectsFromArray:array];
            }
        
        [_photoCollection reloadData];
    } anderrorBlock:^{
         [[LoadingView sharedLoadingView] stopLoading];
        [_photoCollection.mj_footer endRefreshing];
        [_photoCollection.mj_header endRefreshing];
    }];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{

    if (self.photoArr.count > 0) {
        CGFloat width = (DeviceWidth-2)/3;
        return CGSizeMake(width, width);
    }else{
    
        return CGSizeMake(0, 0);
    }
   
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.photoArr.count > 0) {
        
        return self.photoArr.count;
    }else{
    
        return 0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.photoArr.count > 0) {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"instagramCell" forIndexPath:indexPath];
        for (UIView *subView in cell.contentView.subviews) {
            [subView removeFromSuperview];
        }
        IMGPhotoModel *model = [self.photoArr objectAtIndex:indexPath.row];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.contentView.bounds];
        [imageView sd_setImageWithURL:[NSURL URLWithString:[model.imageUrl returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            //image animation
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            if ([manager diskImageExistsForURL:[NSURL URLWithString:[model.imageUrl returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]]]) {
                NSLog(@"dont loading animation");
            }else {
                imageView.alpha = 0.0;
                [UIView transitionWithView:imageView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                    
                    imageView.image = image;
                    imageView.alpha = 1.0;
                } completion:NULL];
                
            }

        }];
        [cell.contentView addSubview:imageView];
        
        
        return cell;

    }else{
    
        return nil;
    }
   
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableArray *photoArr = [NSMutableArray array];
    for (IMGPhotoModel *model in self.photoArr) {
        IMGDish *dish = [[IMGDish alloc] init];
        dish.dishId = model.myID;
        dish.userId = model.userId;
        dish.restaurantId = model.restaurantId;
        dish.commentCount = model.commentCount;
        dish.likeCount = model.likeCount;
        dish.descriptionStr = model.des;
        //dish.createTime = model.createTime;
        //dish.userName = model.creator;
        dish.type = model.type;
        dish.restaurantTitle = model.restaurantTitle;
        dish.imageUrl = model.imageUrl;
        dish.isRestaurantPhoto = YES;
        [photoArr addObject:dish];
    }
   
    AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:photoArr andPhotoTag:indexPath.row andRestaurant:nil andFromDetail:NO];
    
//    albumViewController.title = restaurant.title;
    albumViewController.showPage=NO;
    albumViewController.amplitudeType = @"User Profile Page - Journey";
    //albumViewController.isUseWebP = self.isUseWebP;
    [self.navigationController pushViewController:albumViewController animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
