//
//  ShareViewController.h
//  ShareExtension
//
//  Created by lucky on 15/12/25.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Social/Social.h>
#import "SearchRestaurantListViewController.h"
#import "SelectCityViewController.h"

@interface ShareViewController : SLComposeServiceViewController<SearchRestaurantListDelegate,SelectCityViewControllerDelegate>

@end
