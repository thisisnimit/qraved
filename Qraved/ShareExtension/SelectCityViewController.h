//
//  SelectCityViewController.h
//  Qraved
//
//  Created by lucky on 15/12/28.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SelectCityViewControllerDelegate <NSObject>

- (void)selectCity:(NSDictionary *)dict;

@end


@interface SelectCityViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,weak)id<SelectCityViewControllerDelegate>delegate;


@end
