//
//  SearchRestaurantListViewController.h
//  Qraved
//
//  Created by lucky on 15/12/25.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchRestaurantListDelegate <NSObject>

- (void)selectRestaurant:(NSDictionary *)dict;

@end



@interface SearchRestaurantListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UISearchBarDelegate>

@property(nonatomic,strong) UISearchBar *searchBar;
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic)        NSNumber *currentCityId;
@property(nonatomic,weak)id<SearchRestaurantListDelegate>delegate;


@end
