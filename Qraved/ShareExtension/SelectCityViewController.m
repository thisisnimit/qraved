//
//  SelectCityViewController.m
//  Qraved
//
//  Created by lucky on 15/12/28.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import "SelectCityViewController.h"
#import "IMGNetWork.h"
#import "Consts.h"
@interface SelectCityViewController ()
{
    NSMutableArray *_dataArrM;
    UITableView *_tableView;
}
@end

@implementation SelectCityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _dataArrM = [[NSMutableArray alloc] init];
    _tableView = [[UITableView alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.frame = CGRectMake(0, 0, self.view.bounds.size.width, 225);
    [self.view addSubview:_tableView];
    [self getData];
}

- (void)getData
{
    [[AFHTTPSessionManager manager] GET:[NSString stringWithFormat:@"%@banner/cities%@",QRAVED_WEB_SERVICE_SERVER,QRAVED_WEB_SERVICE_CLIENT_VERSION] parameters:[[NSDictionary alloc]initWithObjectsAndKeys:@99,@"countryID", nil] success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        _dataArrM = [NSMutableArray arrayWithArray:responseObject];
        [_tableView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
    //    [[AFHTTPRequestOperationManager manager] GET:@"banner/cities" parameters:[[NSDictionary alloc]initWithObjectsAndKeys:@99,@"countryID", nil] success:^(AFHTTPRequestOperation *operation, id responseObject){
    //
    //        _dataArrM = [NSMutableArray arrayWithArray:responseObject];
    ////        for (int i=0; i<array.count; i++) {
    ////            NSDictionary *dict = [array objectAtIndex:i];
    ////            IMGCity *city = [[IMGCity alloc]init];
    ////            city.countryId=[dict objectForKey:@"countryId"];
    ////            city.cityId=[dict objectForKey:@"id"];
    ////            city.name=[dict objectForKey:@"name"];
    ////            city.status=[NSNumber numberWithInt:1];
    ////            [_dataArrM addObject:city];
    ////        }
    //        [_tableView reloadData];
    //
    //    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    //        NSLog(@"getDishesFromServer erors when get datas:%@",error.localizedDescription);
    //    }];
}


-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexpath{
    return 50;
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArrM.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"citycell"];
    if(cell==nil){
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"citycell"];
    }
    cell.textLabel.text = [[_dataArrM objectAtIndex:indexPath.row] objectForKey:@"name"];;
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [_dataArrM objectAtIndex:indexPath.row];
    if([self.delegate respondsToSelector:@selector(selectCity:)])
    {
        [self.delegate selectCity:dict];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
