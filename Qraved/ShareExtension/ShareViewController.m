//
//  ShareViewController.m
//  MyShareE
//
//  Created by lucky on 15/12/22.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import "ShareViewController.h"
#import "IMGNetWork.h"
//#import "AFHTTPRequestOperation.h"
#import "SearchRestaurantListViewController.h"
#import "AFHTTPSessionManager.h"
#import "Consts.h"
@interface ShareViewController ()
{
    SLComposeSheetConfigurationItem * restaurantItem;
    SLComposeSheetConfigurationItem * cityItem;
    NSString *currentRestaurantId;
    NSString *currentCityId;
    BOOL isUser;
}
@end

@implementation ShareViewController

- (BOOL)isContentValid {
    // Do validation of contentText and/or NSExtensionContext attachments here
    
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
    
    
    
    if (![shared valueForKey:@"userId"])
    {
        isUser = NO;
        return NO;
    }
    else
    {
        isUser = YES;
    }
    
    
    if (!currentRestaurantId)
    {
        return NO;
    }
    else
    {
        return YES;
    }
    
}

- (void)didSelectPost {
    // This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.
    
    // Inform the host that we're done, so it un-blocks its UI. Note: Alternatively you could call super's -didSelectPost, which will similarly complete the extension context.
    
    // Verify that we have a valid NSExtensionItem
    NSExtensionItem *imageItem = [self.extensionContext.inputItems firstObject];
    if(!imageItem){
        return;
    }
    
    // Verify that we have a valid NSItemProvider
    NSItemProvider *imageItemProvider = [[imageItem attachments] firstObject];
    if(!imageItemProvider){
        return;
    }
    
    // Look for an image inside the NSItemProvider
    if([imageItemProvider hasItemConformingToTypeIdentifier:@"public.jpeg"]) {
        [imageItemProvider loadItemForTypeIdentifier:@"public.jpeg" options:nil completionHandler:^(UIImage *image, NSError *error) {
            
            if(image) {
                
                NSString *title = self.title;
                
                NSLog(@"title %@", title);
                NSLog(@"description %@", self.textView.text);
                NSLog(@"image %@", image);
                [self postImage:image andTitle:title andDescription:self.textView.text andSuccessBlock:^{
                    
                } andFailedBlock:^{
                    
                }];
                
            }
        }];
        
        
    }
    else if ([imageItemProvider hasItemConformingToTypeIdentifier:@"public.png"]){
        
        [imageItemProvider loadItemForTypeIdentifier:@"public.png" options:nil completionHandler:^(UIImage *image, NSError *error) {
            
            if(image) {
                
                NSString *title = self.title;
                
                NSLog(@"title %@", title);
                NSLog(@"description %@", self.textView.text);
                NSLog(@"image %@", image);
                [self postImage:image andTitle:title andDescription:self.textView.text andSuccessBlock:^{
                    
                } andFailedBlock:^{
                    
                }];
                
                
            }
        }];
        
        
    }
    
    
    
}

-(void)postImage:(UIImage *)image andTitle:(NSString *)title andDescription:(NSString *)description andSuccessBlock:(void(^)())successBlock andFailedBlock:(void(^)())failedBlock
{
    if(image){
        NSData *fileData = UIImageJPEGRepresentation(image,0.3);
        
        NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
        
        NSNumber *userID = [NSNumber numberWithInt:0];
        if ([shared valueForKey:@"userId"])
        {
            userID = [shared valueForKey:@"userId"];
        }
        
        
        NSString *t = @"0";
        if ([shared valueForKey:@"userToken"])
        {
            t = [shared valueForKey:@"userToken"];
        }
        
        NSString *postDescription =description;
        NSDictionary* dataDic = [[NSDictionary alloc] initWithObjectsAndKeys:userID, @"userId",
                                 postDescription, @"description",currentRestaurantId,@"restaurantId",t,@"t",nil];
        
        //        NSDictionary *dataDic = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",dishList,@"dishList",_userReview.reviewId,@"reviewId", nil];
        
        AFHTTPSessionManager *webmanager = [AFHTTPSessionManager manager];
        webmanager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        [webmanager POST:[NSString stringWithFormat:@"%@common/header/uploadImgByMobileForShare",QRAVED_WEB_UPLOAD_PHOTO_SERVER] parameters:dataDic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            [formData appendPartWithFileData:fileData name:@"up-image" fileName:[NSString stringWithFormat:@"%f.jpg",[[NSDate date]timeIntervalSince1970]] mimeType:@"multipart/form-data"];
            NSLog(@"fileName = %@",[NSString stringWithFormat:@"%f.jpg",[[NSDate date]timeIntervalSince1970]]);
            
        } progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            NSString *message ;
            
            [self.extensionContext completeRequestReturningItems:@[] completionHandler:nil];
            
            if (task.error == nil ) {
                //                message = operation.responseString;
                NSDictionary *JSON =(NSDictionary*)responseObject;
                successBlock(JSON);
            }
            else {
                message = task.error.localizedDescription;
            }
            
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
        }];
        //        AFHTTPRequestOperationManager *webmanager = [AFHTTPRequestOperationManager webmanager];
        //        webmanager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        //        [webmanager POST:[NSString stringWithFormat:@"%@common/header/uploadImgByMobileForShare",QRAVED_WEB_SERVER] parameters:options constructingBodyWithBlock:^(id <AFMultipartFormData> formData) {
        //            [formData appendPartWithFileData:fileData name:@"up-image" fileName:[NSString stringWithFormat:@"%f.jpg",[[NSDate date]timeIntervalSince1970]] mimeType:@"multipart/form-data"];
        //            NSLog(@"fileName = %@",[NSString stringWithFormat:@"%f.jpg",[[NSDate date]timeIntervalSince1970]]);
        //        } success:^(AFHTTPRequestOperation  *operation, id responseObject) {
        //            NSString *message ;
        //
        //            [self.extensionContext completeRequestReturningItems:@[] completionHandler:nil];
        //
        //            if (operation.error == nil ) {
        //                message = operation.responseString;
        //                NSDictionary *JSON =
        //                [NSJSONSerialization JSONObjectWithData: [message dataUsingEncoding:NSUTF8StringEncoding]
        //                                                options: NSJSONReadingMutableContainers
        //                                                  error:nil];
        //                successBlock(JSON);
        //            }
        //            else {
        //                message = operation.error.localizedDescription;
        //            }
        
        //        } failure:^(AFHTTPRequestOperation  *operation, NSError *error) {
        //            [self.extensionContext completeRequestReturningItems:@[] completionHandler:nil];
        //            failedBlock(error);
        //        }];
    }
}


- (void)selectRestaurant:(NSDictionary *)dict
{
    restaurantItem.title = [dict objectForKey:@"title"];
    currentRestaurantId = [dict objectForKey:@"restaurantId"];
    [self validateContent];
}

- (void)selectCity:(NSDictionary *)dict
{
    cityItem.title = [dict objectForKey:@"name"];
    currentCityId = [dict objectForKey:@"id"];
}


- (NSArray *)configurationItems {
    // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
    
    
    if (isUser)
    {
        cityItem = [[SLComposeSheetConfigurationItem alloc]init];
        cityItem.title = @"Jakarta";
        cityItem.valuePending = NO;
        cityItem.tapHandler = ^(void)
        {
            
            SelectCityViewController * scvc = [[SelectCityViewController alloc] init];
            scvc.delegate = self;
            [self pushConfigurationViewController:scvc];
            
        };
        
        
        restaurantItem = [[SLComposeSheetConfigurationItem alloc]init];
        restaurantItem.title = @"Restaurant";
        restaurantItem.valuePending = NO;
        restaurantItem.tapHandler = ^(void)
        {
            
            SearchRestaurantListViewController * srlvc = [[SearchRestaurantListViewController alloc] init];
            srlvc.delegate = self;
            
            NSString *currentCityIdTemp;
            if (currentCityId)
            {
                currentCityIdTemp = currentCityId;
            }
            else
                currentCityIdTemp = @"2";
            
            srlvc.currentCityId = [NSNumber numberWithInt:[currentCityIdTemp intValue]];
            
            [self pushConfigurationViewController:srlvc];
            
        };
        
        
        return @[cityItem,restaurantItem];
    }
    else
    {
        SLComposeSheetConfigurationItem *noneItem = [[SLComposeSheetConfigurationItem alloc]init];
        //        noneItem.title = @"Not signed in";
        //        noneItem.valuePending = YES;
        noneItem.value = @"Not signed in";
        
        return @[noneItem];
        
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
}

@end
