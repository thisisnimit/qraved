//
//  SearchRestaurantListViewController.m
//  Qraved
//
//  Created by lucky on 15/12/25.
//  Copyright © 2015年 Imaginato. All rights reserved.
//
#define DEFAULT_SEARCH_TEXT @"Search more restaurants by name"


#import "SearchRestaurantListViewController.h"
#import "IMGNetWork.h"
#import "Consts.h"
@interface SearchRestaurantListViewController ()
{
    NSMutableArray *_dataArrM;
    NSMutableArray *_defaultArrM;
}
@end

@implementation SearchRestaurantListViewController

@synthesize searchBar;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _dataArrM = [[NSMutableArray alloc] init];
    _defaultArrM = [[NSMutableArray alloc] init];
    
    
    UIView *backgroundView = [[UIView alloc] init];
    backgroundView.frame = CGRectMake(0, 0, self.view.bounds.size.width - 30, 45);
    backgroundView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:backgroundView];
    
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width - 30, 45)];
    searchBar.delegate = self;
    searchBar.enablesReturnKeyAutomatically=NO;
    searchBar.placeholder = DEFAULT_SEARCH_TEXT;
    
    
    
    //    for(UIView *subview in searchBar.subviews){
    //        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
    //            [subview removeFromSuperview];
    //        }
    //    }
    //
    //    [searchBar.layer setMasksToBounds:YES];
    //    [searchBar.layer setCornerRadius:10.0]; //设置矩圆角半径
    //    [searchBar.layer setBorderWidth:1.0];   //边框宽度
    //    //    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    //    //    CGColorRef colorref = CGColorCreate(colorSpace,(CGFloat[]){ 1, 0, 0, 1 });
    //    //    [searchBar.layer setBorderColor:colorref];//边框颜色
    //
    //
    //    searchBar.layer.borderColor = [UIColor colorWithRed:108/255.0 green:108/255.0 blue:108/255.0 alpha:1].CGColor;
    //    searchBar.layer.borderWidth = 1.0;
    //    searchBar.backgroundColor = [UIColor whiteColor];
    //    searchBar.backgroundImage = [self imageWithColor:[UIColor whiteColor] size:searchBar.bounds.size];
    //    [searchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"search_input-box" ]  forState:UIControlStateNormal];
    [searchBar becomeFirstResponder];
    
    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0 ,searchBar.frame.size.height,searchBar.frame.size.width,185)];
    //    _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    _tableView.delegate=self;
    _tableView.dataSource=self;
    _tableView.backgroundColor = [UIColor whiteColor];
    
    
    
    [self.view addSubview:searchBar];
    [self.view addSubview:_tableView];
    
    [self recentlyViewBookedRestaurants];
    
}
- (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}


-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexpath{
    return 50;
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataArrM.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    return 1;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexpath{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"viewedcell"];
    if(cell==nil){
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"viewedcell"];
    }
    cell.textLabel.text = [[_dataArrM objectAtIndex:indexpath.row] objectForKey:@"title"];;
    cell.detailTextLabel.text = [[_dataArrM objectAtIndex:indexpath.row] objectForKey:@"cuisine"];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [_dataArrM objectAtIndex:indexPath.row];
    if([self.delegate respondsToSelector:@selector(selectRestaurant:)])
    {
        [self.delegate selectRestaurant:dict];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField*)field{
    [field resignFirstResponder];
    return NO;
}


-(void)searchBar:(UISearchBar*)searchbar textDidChange:(NSString*)text{
    
    if(!text.length){
        [_dataArrM removeAllObjects];
        if (_defaultArrM.count)
        {
            _dataArrM = [NSMutableArray arrayWithArray:_defaultArrM];
        }
        [self.tableView reloadData];
        searchbar.placeholder=DEFAULT_SEARCH_TEXT;
        return;
    }
    
    NSMutableDictionary *parameters=[[NSMutableDictionary alloc] init];
    [parameters setObject:text forKey:@"filterWord"];
    [parameters setObject:@"10" forKey:@"max"];
    [parameters setObject:self.currentCityId forKey:@"cityId"];
    [parameters setObject:@"0" forKey:@"offset"];
    [parameters setValue:QRAVED_WEB_SERVICE_VERSION forKey:@"v"];
    [parameters setValue:QRAVED_APIKEY forKey:@"appApiKey"];
    [parameters setValue:QRAVED_WEB_SERVICE_CLIENT forKey:@"client"];

    [[AFHTTPSessionManager manager] GET:[NSString stringWithFormat:@"%@app/booking/keywordFilter",QRAVED_WEB_SERVICE_SERVER] parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSArray *restaurantList=[responseObject objectForKey:@"restaurantList"];
        
        [_dataArrM removeAllObjects];
        
        for(NSDictionary *restaurant in restaurantList){
            NSMutableDictionary *tmp=[[NSMutableDictionary alloc] init];
            [tmp setObject:[[restaurant objectForKey:@"restaurantTitle"] stringByReplacingOccurrencesOfString:@"&amp" withString:@"&"] forKey:@"title"];
            [tmp setObject:[restaurant objectForKey:@"restaurantId"] forKey:@"restaurantId"];
            [tmp setObject:[restaurant objectForKey:@"seoKeyword"] forKey:@"seoKeyword"];
            // [tmp setObject:[restaurant objectForKey:@"inList"] forKey:@"inList"];
            NSEnumerator *enumerator=[[restaurant objectForKey:@"cuisineList"] reverseObjectEnumerator];
            NSMutableArray *cuisines=[[NSMutableArray alloc] init];
            for(NSDictionary *cuisine in [enumerator allObjects]){
                [cuisines addObject:[cuisine objectForKey:@"name"]];
            }
            // [cuisines addObject:[restaurant objectForKey:@"districtName"]];
            [tmp setObject:[cuisines componentsJoinedByString:@" / "] forKey:@"cuisine"];
            [_dataArrM addObject:tmp];
        }
        
        [self.tableView reloadData];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
    //    [[AFHTTPRequestOperationManager manager] GET:@"app/booking/keywordFilter" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject){
    //        //        NSNumber *total=[responseObject objectForKey:@"restaurantCount"];
    //        NSArray *restaurantList=[responseObject objectForKey:@"restaurantList"];
    //
    //        [_dataArrM removeAllObjects];
    //
    //        for(NSDictionary *restaurant in restaurantList){
    //            NSMutableDictionary *tmp=[[NSMutableDictionary alloc] init];
    //            [tmp setObject:[[restaurant objectForKey:@"restaurantTitle"] stringByReplacingOccurrencesOfString:@"&amp" withString:@"&"] forKey:@"title"];
    //            [tmp setObject:[restaurant objectForKey:@"restaurantId"] forKey:@"restaurantId"];
    //            [tmp setObject:[restaurant objectForKey:@"seoKeyword"] forKey:@"seoKeyword"];
    //            // [tmp setObject:[restaurant objectForKey:@"inList"] forKey:@"inList"];
    //            NSEnumerator *enumerator=[[restaurant objectForKey:@"cuisineList"] reverseObjectEnumerator];
    //            NSMutableArray *cuisines=[[NSMutableArray alloc] init];
    //            for(NSDictionary *cuisine in [enumerator allObjects]){
    //                [cuisines addObject:[cuisine objectForKey:@"name"]];
    //            }
    //            // [cuisines addObject:[restaurant objectForKey:@"districtName"]];
    //            [tmp setObject:[cuisines componentsJoinedByString:@" / "] forKey:@"cuisine"];
    //            [_dataArrM addObject:tmp];
    //        }
    //
    //        [self.tableView reloadData];
    //
    //
    //
    //    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    //        NSLog(@"getDishesFromServer erors when get datas:%@",error.localizedDescription);
    //    }];
    
    
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)_searchBar {
    _searchBar.showsCancelButton = NO;
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)_searchBar {
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)_searchBar {
    _searchBar.showsCancelButton = NO;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)_searchBar {
    [_searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)_searchBar {
    [_searchBar resignFirstResponder];
}

-(void)recentlyViewBookedRestaurants{
    
    NSMutableDictionary *parameters=[[NSMutableDictionary alloc] init];
    [parameters setObject:@"10" forKey:@"max"];
    [parameters setObject:self.currentCityId forKey:@"cityId"];
    [parameters setObject:@"0" forKey:@"offset"];
    
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
    NSNumber *userID = [NSNumber numberWithInt:0];
    if ([shared valueForKey:@"userId"])
    {
        userID = [shared valueForKey:@"userId"];
    }
    
    [parameters setObject:userID forKey:@"userId"];
    [parameters setValue:QRAVED_WEB_SERVICE_VERSION forKey:@"v"];
    [parameters setValue:QRAVED_APIKEY forKey:@"appApiKey"];
    [parameters setValue:QRAVED_WEB_SERVICE_CLIENT forKey:@"client"];

    [[AFHTTPSessionManager manager] POST:[NSString stringWithFormat:@"%@restaurant/viewed/list/v2",QRAVED_WEB_SERVICE_SERVER]parameters:parameters success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject = %@",responseObject);
        NSArray *restaurantList = [responseObject objectForKey:@"restaurantList"];
        NSString *exceptionmsg = [responseObject objectForKey:@"exceptionmsg"];
        if(exceptionmsg!=nil){
            
            return;
        }
        
        
        for(NSDictionary *restaurant in restaurantList){
            NSMutableDictionary *tmp=[[NSMutableDictionary alloc] init];
            [tmp setObject:[[restaurant objectForKey:@"title"] stringByReplacingOccurrencesOfString:@"&amp" withString:@"&"] forKey:@"title"];
            [tmp setObject:[restaurant objectForKey:@"restaurantId"] forKey:@"restaurantId"];
            NSEnumerator *enumerator=[[restaurant objectForKey:@"cuisineList"] reverseObjectEnumerator];
            NSMutableArray *cuisines=[[NSMutableArray alloc] init];
            for(NSDictionary *cuisine in [enumerator allObjects]){
                [cuisines addObject:[cuisine objectForKey:@"name"]];
            }
            [cuisines addObject:[restaurant objectForKey:@"districtName"]];
            [tmp setObject:[cuisines componentsJoinedByString:@" / "] forKey:@"cuisine"];
            [_defaultArrM addObject:tmp];
        }
        _dataArrM = [NSMutableArray arrayWithArray:_defaultArrM];
        [_tableView reloadData];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
    //     [[AFHTTPRequestOperationManager manager]POST:@"/restaurant/viewed/list/v2" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject)
    //            {
    //                NSLog(@"responseObject = %@",responseObject);
    //                NSArray *restaurantList = [responseObject objectForKey:@"restaurantList"];
    //                NSString *exceptionmsg = [responseObject objectForKey:@"exceptionmsg"];
    //                if(exceptionmsg!=nil){
    //
    //                    return;
    //                }
    //
    //
    //                for(NSDictionary *restaurant in restaurantList){
    //                    NSMutableDictionary *tmp=[[NSMutableDictionary alloc] init];
    //                    [tmp setObject:[[restaurant objectForKey:@"title"] stringByReplacingOccurrencesOfString:@"&amp" withString:@"&"] forKey:@"title"];
    //                    [tmp setObject:[restaurant objectForKey:@"restaurantId"] forKey:@"restaurantId"];
    //                    NSEnumerator *enumerator=[[restaurant objectForKey:@"cuisineList"] reverseObjectEnumerator];
    //                    NSMutableArray *cuisines=[[NSMutableArray alloc] init];
    //                    for(NSDictionary *cuisine in [enumerator allObjects]){
    //                        [cuisines addObject:[cuisine objectForKey:@"name"]];
    //                    }
    //                    [cuisines addObject:[restaurant objectForKey:@"districtName"]];
    //                    [tmp setObject:[cuisines componentsJoinedByString:@" / "] forKey:@"cuisine"];
    //                    [_defaultArrM addObject:tmp];
    //                }
    //                _dataArrM = [NSMutableArray arrayWithArray:_defaultArrM];
    //                [_tableView reloadData];
    //
    //            }failure:^(AFHTTPRequestOperation *operation, NSError *error)
    //            {
    //
    //            }];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
