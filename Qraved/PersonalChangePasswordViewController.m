//
//  PersonalChangePasswordViewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/10.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "PersonalChangePasswordViewController.h"
#import "ProfileHandler.h"
#import "LoadingView.h"

@interface PersonalChangePasswordViewController ()<UITextFieldDelegate>
{
    UITextField *oldT;
    UITextField *newT;
    UITextField *VerifyT;
}
@end

@implementation PersonalChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Edit Profile";
    [self addCancel];
    [self setSaveBarButton];
    [self initUI];
    self.view.backgroundColor = [UIColor defaultColor];
    
   
}

- (void)initUI{

    UIView *backView = [UIView new];
    backView.backgroundColor =[UIColor  whiteColor];
    
    UIView *lineViewtop = [UIView new];
    lineViewtop.backgroundColor = [UIColor colorWithRed:223/255.0 green:223/255.0 blue:223/255.0 alpha:1];
    
    UILabel *oldP = [UILabel new];
    oldP.textColor = [UIColor color333333];
    oldP.text = @"Old Password";
    oldP.font = [UIFont systemFontOfSize:17];
    
    oldT = [UITextField new];
    oldT.font = [UIFont systemFontOfSize:17];
    oldT.textColor = [UIColor lightGrayColor];
    oldT.placeholder = @"Required";
    oldT.secureTextEntry = YES;
    oldT.tag = 100;
    oldT.delegate = self;
    
    UIView *lineViewo = [UIView new];
    lineViewo.backgroundColor = [UIColor colorWithRed:223/255.0 green:223/255.0 blue:223/255.0 alpha:1];
    
    UILabel *newP = [UILabel new];
    newP.textColor = [UIColor color333333];
    newP.text = @"New Password";
    newP.font = [UIFont systemFontOfSize:17];
    
    newT = [UITextField new];
    newT.font = [UIFont systemFontOfSize:17];
    newT.textColor = [UIColor lightGrayColor];
    newT.placeholder = @"Required";
    newT.secureTextEntry = YES;
    newT.tag = 101;
    newT.delegate = self;
    
    UIView *lineViewt = [UIView new];
    lineViewt.backgroundColor = [UIColor colorWithRed:223/255.0 green:223/255.0 blue:223/255.0 alpha:1];
    
    UILabel *Verify = [UILabel new];
    Verify.textColor = [UIColor color333333];
    Verify.text = @"Verify";
    Verify.font = [UIFont systemFontOfSize:17];
    
    VerifyT = [UITextField new];
    VerifyT.font = [UIFont systemFontOfSize:17];
    VerifyT.textColor = [UIColor lightGrayColor];
    VerifyT.placeholder = @"Required";
    VerifyT.secureTextEntry = YES;
    VerifyT.tag = 102;
    VerifyT.delegate = self;
    
    UIView *lineViewoth = [UIView new];
    lineViewoth.backgroundColor = [UIColor colorWithRed:223/255.0 green:223/255.0 blue:223/255.0 alpha:1];
    
    [backView sd_addSubviews:@[lineViewtop ,oldP, oldT, lineViewo, newP, newT, lineViewt, Verify, VerifyT, lineViewoth]];
    [self.view addSubview:backView];
    
    backView.sd_layout
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .topSpaceToView(self.view, 20)
    .heightIs(143);
    
    lineViewtop.sd_layout
    .topSpaceToView(backView, 0)
    .leftSpaceToView(backView, 0)
    .widthIs(DeviceWidth)
    .heightIs(1);
    
    oldP.sd_layout
    .topSpaceToView(lineViewtop, 0)
    .leftSpaceToView(backView, 15)
    .widthIs(150)
    .heightIs(40);
    
    oldT.sd_layout
    .topEqualToView(oldP)
    .leftSpaceToView(oldP, 0)
    .rightSpaceToView(backView, 15)
    .heightIs(40);
    
    lineViewo.sd_layout
    .topSpaceToView(oldP, 0)
    .leftSpaceToView(backView, 0)
    .widthIs(DeviceWidth)
    .heightIs(1);
    
    newP.sd_layout
    .topSpaceToView(lineViewo, 0)
    .leftEqualToView(oldP)
    .widthIs(150)
    .heightIs(40);
    
    newT.sd_layout
    .topEqualToView(newP)
    .leftSpaceToView(newP, 0)
    .rightSpaceToView(backView, 15)
    .heightIs(40);
    
    lineViewt.sd_layout
    .topSpaceToView(newP, 0)
    .leftSpaceToView(backView, 0)
    .widthIs(DeviceWidth)
    .heightIs(1);
    
    Verify.sd_layout
    .topSpaceToView(lineViewt, 0)
    .leftEqualToView(oldP)
    .heightIs(40)
    .widthIs(150);
    
    VerifyT.sd_layout
    .topEqualToView(Verify)
    .leftSpaceToView(Verify, 0)
    .rightSpaceToView(backView, 15)
    .heightIs(40);
    
    lineViewoth.sd_layout
    .topSpaceToView(Verify, 0)
    .leftSpaceToView(backView, 0)
    .widthIs(DeviceWidth)
    .heightIs(1);


}
-(void)addCancel{

    UIBarButtonItem *cancelBtn=[[UIBarButtonItem alloc] initWithTitle:L(@"Cancel") style:UIBarButtonItemStylePlain target:self action:@selector(cancelBtnTapped)];
    cancelBtn.tintColor=[UIColor color333333];
    self.navigationItem.leftBarButtonItem=cancelBtn;
}
- (void)cancelBtnTapped{

    [self.navigationController popViewControllerAnimated:YES];

}


-(void)setSaveBarButton{
    UIBarButtonItem *saveBtn=[[UIBarButtonItem alloc] initWithTitle:L(@"Done") style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonTapped)];
    saveBtn.tintColor=[UIColor colorWithRed:9/255.0f green:191/255.0f blue:211/255.0f alpha:1.0f];
    self.navigationItem.rightBarButtonItem=saveBtn;
}
- (void)saveButtonTapped{

    
    IMGUser *User = [IMGUser currentUser];
    if (User.userId == nil || User.token == nil){
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    if (oldT.text.length == 0  || newT.text.length == 0 ||VerifyT.text.length == 0) {
        [SVProgressHUD showImage:[UIImage imageNamed:@""] status:@"The information can not be empty"];
        [SVProgressHUD dismissWithDelay:1.5];
//         [self errorViewShowWithTextField:newT andAlertText:@"This field cannot be empty"];
        return;
    }
    
    
    NSDictionary *dict = @{@"userId":User.userId, @"t":User.token, @"oldPassword":oldT.text, @"newPassword": newT.text};
    
    if (![newT.text isEqualToString:VerifyT.text]) {
        [SVProgressHUD showImage:[UIImage imageNamed:@""] status:@"Verify is different with new password"];
        [SVProgressHUD dismissWithDelay:1.5];
        
    }else{
    
        [[LoadingView sharedLoadingView] startLoading];
        [ProfileHandler changePassword:dict andSuccessBlock:^(NSMutableDictionary *twoDict) {
            NSLog(@"%@",twoDict);
            [[LoadingView sharedLoadingView] stopLoading];
            if (twoDict[@"exceptionmsg"]) {
                
                [SVProgressHUD showImage:[UIImage imageNamed:@""] status:twoDict[@"exceptionmsg"]];
                [SVProgressHUD dismissWithDelay:1.5];
                
            }else{
                
                [self.navigationController popViewControllerAnimated:YES];
            }
            
            
        } anderrorBlock:^{
            [[LoadingView sharedLoadingView] stopLoading];
            
        }];
    }
    
    
    

}
//-(void)errorViewShowWithTextField:(UITextField*)textField andAlertText:(NSString*)alertText
//{
//    textField.background = [UIImage imageNamed:@"input-box-selected-1"];
//   [textField drawPlaceholderInRect:CGRectMake(LEFTLEFTSET, 0, DeviceWidth, 49)];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
