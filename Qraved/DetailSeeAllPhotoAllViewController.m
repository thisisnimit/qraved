//
//  DetailSeeAllPhotoAllViewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/10.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "DetailSeeAllPhotoAllViewController.h"
#import "AddPhotoViewController.h"
#import "IMGDish.h"
#import "IMGRestaurantImage.h"
#import "AlbumViewController.h"
#import "LoadingView.h"
@interface DetailSeeAllPhotoAllViewController ()<UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    NSInteger currentCount;
    UIScrollView *scrollView;
    CGFloat currentHeight;
    float leftCurrentHeight;
    float rightCurrentHeight;
    
    int currentQrave;
    
    int fetchQravesMax;
    
    float startPointY;
    
    NSMutableArray *restaurantPhotoArray;
    NSMutableArray *userPhotoArray;
    NSMutableArray *eventPhotoArray;
    IMGRestaurant *restaurant;
    NSInteger oldUserPhotoArrayCount;
    int offset;
    NSNumber *dishcount;
    int maxNum;
    
    UICollectionView *allCollectionView;
    BOOL noMorePhotos;
    IMGShimmerView *shimmerView;
}

@property (nonatomic, retain) NSMutableArray *qravesList;
@end

@implementation DetailSeeAllPhotoAllViewController

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=NO;
    [self.navigationItem setTitle:restaurant.title];
    [super viewWillAppear:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    
    
    NSLog(@"%lu-----",(unsigned long)[SDWebImageDownloader sharedDownloader].currentDownloadCount);
    
}

-(instancetype)initWithRestaurant:(IMGRestaurant*)paramRestaurant restaurantPhotoArray:(NSArray*)photoArr{
    self = [super init];
    if (self != nil) {
        restaurant = paramRestaurant;
        //restaurantPhotoArray=[photoArr mutableCopy];
        
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"Restaurant Photo list page";
    [self setBackBarButtonOffset30];
    //    [self setPhotoBarButton];
    self.qravesList = [NSMutableArray arrayWithCapacity:0];
    restaurantPhotoArray = [NSMutableArray array];
    leftCurrentHeight = 5;
    rightCurrentHeight = 5;
    
    currentCount=0;
    oldUserPhotoArrayCount = 0;
    offset = 0;
    noMorePhotos = NO;
    userPhotoArray = [[NSMutableArray alloc] initWithCapacity:0];
    
    [self requestData];
    [self initUI];
     allCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    
}
- (void)loadMoreData{

    [self getDishesFromServer];

}
- (void)requestData{
 [self getDishesFromServer];
}
- (void)initUI{

    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    CGFloat width = (DeviceWidth-2)/3;
    layout.itemSize = CGSizeMake(width, width);
    layout.minimumLineSpacing = 1;
    layout.minimumInteritemSpacing = 1;
//    layout.scrollDirection = UICollectionViewScrollDirectionVertical;

    
    allCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,40, DeviceWidth, DeviceHeight - 84) collectionViewLayout:layout];
    allCollectionView.backgroundColor = [UIColor whiteColor];
    allCollectionView.delegate = self;
    allCollectionView.dataSource = self;
    allCollectionView.showsHorizontalScrollIndicator = NO;
    allCollectionView.showsVerticalScrollIndicator = NO;
    allCollectionView.bounces = NO;
    [self.view addSubview:allCollectionView];
    
    
    [allCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"photoCell"];
    
    shimmerView = [[IMGShimmerView alloc] initWithFrame:CGRectMake(0, 40, DeviceWidth, self.view.bounds.size.height - 40)];
    [shimmerView createPhotoShimmerView];
    [self.view addSubview:shimmerView];
}

- (void)getDishesFromServer
{
    [[DBManager manager] deleteWithSql:[NSString stringWithFormat:@"delete from IMGDish where restaurantId = '%@';",restaurant.restaurantId]];
    
    NSDictionary *parameters=@{@"restaurantId":[NSString stringWithFormat:@"%@",restaurant.restaurantId],@"max":@"15",@"offset":[NSString stringWithFormat:@"%d",offset]};
    [[IMGNetWork sharedManager] GET:@"app/detail/photos/v2" parameters:parameters success:^(NSURLSessionDataTask *operation, id responseObject) {
        [allCollectionView.mj_footer endRefreshing];
        
        if (offset == 0) {
            [shimmerView removeFromSuperview];
        }
        restaurant.goFoodLink = [responseObject objectForKey:@"goFoodLink"];
        dishcount = [responseObject objectForKey:@"dishCount"];
        
        if ([[responseObject objectForKey:@"restaurantImages"] count]>0 && offset==0) {
           
            NSArray *restaurantPhotoArray_ = [responseObject objectForKey:@"restaurantImages"];
           
            
            for(int i=0; i<restaurantPhotoArray_.count; i++){
                IMGDish *dish = [[IMGDish alloc] init];
                dish.imageUrl =  restaurantPhotoArray_[i][1];
                dish.dishId = restaurantPhotoArray_[i][0];
                dish.restaurantId = [restaurantPhotoArray_[i][2] objectForKey:@"restaurantId"];
                dish.isRestaurantPhoto = YES;
                
                [dish updatePhotoCredit:restaurantPhotoArray_[i][2]];
                
                [restaurantPhotoArray addObject:dish];
            }
        }
        NSArray *dishArray = [responseObject objectForKey:@"dishList"];
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        NSDate *createTime=[[NSDate alloc] init];
        [formatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
        for(int i=0; i<dishArray.count; i++){
            NSDictionary *tempDic = [dishArray objectAtIndex:i];
            IMGDish *dish = [[IMGDish alloc] init];
            if ([[tempDic objectForKey:@"type"] isEqualToString:@"2"]) {
                NSDictionary *dishDictionary = [tempDic objectForKey:@"instagramPhoto"];
                if (![dishDictionary isKindOfClass:[NSNull class]]) {
                    [dish setValuesForKeysWithDictionary:dishDictionary];
                    dish.userName=[dishDictionary objectForKey:@"instagramUserName"];
                    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[[dishDictionary objectForKey:@"approvedDate"] longLongValue]/1000];
                    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
                    
                    createTime=[formatter dateFromString:confromTimespStr];
                    dish.createTime=[NSNumber numberWithDouble:createTime.timeIntervalSince1970];
                   
                    if ([dishDictionary objectForKey:@"instagramUserId"] != nil) {
                        dish.photoCreditTypeDic = @"User ID";
                    }
                    
                    dish.userAvatarDic = [dishDictionary objectForKey:@"instagramUserProfilePicture"];
                    dish.userIdDic = [dishDictionary objectForKey:@"instagramUserId"];
                    
                    if ([[dishDictionary objectForKey:@"standardResolutionImage"] length]>0) {
                        dish.imageUrl = [dishDictionary objectForKey:@"standardResolutionImage"];
                    }else{
                        if ([[dishDictionary objectForKey:@"thumbnailImage"] length]>0) {
                            dish.imageUrl = [dishDictionary objectForKey:@"thumbnailImage"];
                        }else{
                            dish.imageUrl = [dishDictionary objectForKey:@"lowResolutionImage"];
                        }
                    }
                    
                }
            }else{
                NSDictionary *dishDictionary = [tempDic objectForKey:@"dish"];
                [dish setValuesForKeysWithDictionary:dishDictionary];
                dish.imageUrl = [dishDictionary objectForKey:@"imageUrl"];
                dish.descriptionStr = [dishDictionary objectForKey:@"description"];
                dish.userName=[dishDictionary objectForKey:@"creator"];
                createTime=[formatter dateFromString:[dishDictionary objectForKey:@"createTime"]];
                dish.createTime=[NSNumber numberWithDouble:createTime.timeIntervalSince1970];
                [dish updatePhotoCredit:[dishDictionary objectForKey:@"photoCredit"]];
            }
            
            
            [restaurantPhotoArray addObject:dish];
        }
        [self loadImageUrl:restaurantPhotoArray];
        if (dishArray.count > 0) {
            offset += 15;
        }else{
            noMorePhotos = YES;
        }
        
        
        [allCollectionView reloadData];
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        [allCollectionView.mj_footer endRefreshing];
//         [[LoadingView sharedLoadingView] stopLoading];
        NSLog(@"getDishesFromServer erors when get datas:%@",error.localizedDescription);
        
    }];
    //    }
    
    
}

-(void)setPhotoBarButton
{
    UIBarButtonItem *photoBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:NavigationPhotoImage] style:UIBarButtonItemStylePlain target:self action:@selector(photoButtonClick:)];
    photoBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=photoBtn;
}
-(void)photoButtonClick
{
    UIImagePickerController *imagepicker = [[UIImagePickerController alloc] init];
    imagepicker.delegate = (id)self;
    imagepicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagepicker.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    imagepicker.allowsEditing = YES;
    [self presentViewController:imagepicker animated:YES completion:nil];
}
#pragma mark -ImagePickerController delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage=[info objectForKey:UIImagePickerControllerEditedImage];
    CGFloat imageHeight=0;
    CGFloat imageWidth=0;
    if(chosenImage.size.width>UPLOAD_PICTURE_SIZE_WIDTH)
    {
        imageHeight=UPLOAD_PICTURE_SIZE_WIDTH*chosenImage.size.height/chosenImage.size.width;
        imageWidth=UPLOAD_PICTURE_SIZE_WIDTH;
    }
    else
    {
        imageHeight=chosenImage.size.height;
        imageWidth=chosenImage.size.width;
    }
    
    CGSize tagetSize=CGSizeMake(imageWidth, imageHeight);
    UIImage*tagetImage=[chosenImage imageWithScaledToSize:tagetSize];
    
    [picker dismissViewControllerAnimated:NO completion:^{
        
        AddPhotoViewController *addPhotoVC = [[AddPhotoViewController alloc]initWithRestauarnt:restaurant];
        addPhotoVC.isWriteReview = NO;
        addPhotoVC.selectedImage = tagetImage;
        [self presentViewController:addPhotoVC animated:YES completion:nil];
    }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark -- collection  delegate
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    
    for (UIView *subView in cell.contentView.subviews) {
        [subView removeFromSuperview];
    }
    IMGDish *dish = [restaurantPhotoArray objectAtIndex:indexPath.row];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.contentView.bounds];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    NSString *imageStr = dish.imageUrl;
    if ([dish.type isEqual:@2]) {
       
        if (dish.lowResolutionImage.length>0) {
            imageStr = dish.lowResolutionImage;
        }else if (dish.thumbnailImage.length>0){
            imageStr = dish.thumbnailImage;
            
        }else if (dish.standardResolutionImage.length>0) {
            imageStr = dish.standardResolutionImage;
        }
        [imageView sd_setImageWithURL:[NSURL URLWithString:imageStr] placeholderImage:[UIImage imageNamed:@"placeholder"] options:SDWebImageRetryFailed|SDWebImageAllowInvalidSSLCertificates completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            //image animation
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            if ([manager diskImageExistsForURL:[NSURL URLWithString:imageStr]]) {
//                NSLog(@"dont loading animation");
            }else {
                if (image) {
                    imageView.alpha = 0.0;
                    [UIView transitionWithView:imageView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                        imageView.image = image;
                        imageView.alpha = 1.0;
                    } completion:NULL];
                }
               
            }
        }];
    }
    else{
        [imageView sd_setImageWithURL:[NSURL URLWithString:[dish.imageUrl returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            //image animation
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            if ([manager diskImageExistsForURL:[NSURL URLWithString:[dish.imageUrl returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]]]) {
//                NSLog(@"dont loading animation");
            }else {
                imageView.alpha = 0.0;
                [UIView transitionWithView:imageView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                    imageView.image = image;
                    imageView.alpha = 1.0;
                } completion:NULL];
                
            }
            
            
        }];
    }
   
    [cell.contentView addSubview:imageView];

    if( indexPath.row==restaurantPhotoArray.count-15) {
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            if (!noMorePhotos) {
                [self getDishesFromServer];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
            });
            
        });
    }
    
   return cell;
    
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    return restaurantPhotoArray.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
// NSInteger tag = indexPath.row;
   // NSMutableArray *images=[[NSMutableArray alloc] init];
//    for(IMGRestaurantImage *image in restaurantPhotoArray){
//        IMGDish *dish = [[IMGDish alloc] init];
//        dish.imageUrl=image.imageUrl;
//        dish.restaurantId=image.restaurantId;
//        
//        dish.userPhotoCountDic = image.userPhotoCountDic;
//        dish.photoCreditTypeDic  = image.photoCreditTypeDic;
//        dish.userIdDic  = image.userIdDic;
//        dish.photoCreditDic  = image.photoCreditDic;
//        dish.userReviewCountDic  = image.userReviewCountDic;
//        dish.userAvatarDic  = image.userAvatarDic;
//        dish.photoCreditUrlDic  = image.photoCreditUrlDic;
//        dish.restaurantIdDic  = image.restaurantIdDic;
//        
//        dish.isRestaurantPhoto = YES;
//        [images addObject:dish];
//    }
    //[images addObjectsFromArray:userPhotoArray];
//    [images addObjectsFromArray:restaurantPhotoArray];
    
    AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:restaurantPhotoArray andPhotoTag:indexPath.row andRestaurant:restaurant andFromDetail:NO];
    
    albumViewController.title = restaurant.title;
    albumViewController.showPage=NO;
    albumViewController.amplitudeType = @"Restaurant detail page - ";
    albumViewController.isUseWebP = self.isUseWebP;
    [self.navigationController pushViewController:albumViewController animated:YES];
}

- (void)loadImageUrl:(NSMutableArray *)imageArr{
    imageArr = (NSMutableArray *)[[imageArr reverseObjectEnumerator] allObjects];
    for (IMGDish *dish in imageArr) {
        NSString *imageStr = dish.imageUrl;
        if ([dish.type intValue]==2) {
            if (dish.lowResolutionImage.length>0) {
                imageStr = dish.lowResolutionImage;
            }else if (dish.thumbnailImage.length>0){
                imageStr = dish.thumbnailImage;
                
            }else if (dish.standardResolutionImage.length>0) {
                imageStr = dish.standardResolutionImage;
            }

            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:imageStr] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                
            }];
        }else{
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[dish.imageUrl returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                
            }];

        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"%f",allCollectionView.contentOffset.y);
    if (allCollectionView.contentOffset.y <= 0) {
        allCollectionView.bounces = NO;
        
        NSLog(@"jinzhixiala");
    }
    else
        if (allCollectionView.contentOffset.y >= 0){
            allCollectionView.bounces = YES;
            NSLog(@"allow shangla");
            
        }
}


@end
