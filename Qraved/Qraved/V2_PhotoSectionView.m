//
//  V2_PhotoSectionView.m
//  Qraved
//
//  Created by harry on 2017/8/30.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_PhotoSectionView.h"
#import "IMGDish.h"
#import "V2_InstagramModel.h"
#import "IMGMenu.h"

@interface V2_PhotoSectionView ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    UICollectionView *articleCollectionView;
    UILabel *lblTitle;
    NSString *currentTitle;
}
@end

@implementation V2_PhotoSectionView

- (instancetype)initWithFrame:(CGRect)frame WithTitle:(NSString *)title{
    self = [super initWithFrame:frame];
    if (self) {
        currentTitle = title;
        [self createUI:title];
    }
    return self;
}

- (void)createUI:(NSString *)title{
    
    self.backgroundColor = [UIColor whiteColor];
    lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 17, DeviceWidth-90, 16)];
    lblTitle.textColor = [UIColor color333333];
    lblTitle.text = title;
    lblTitle.font = [UIFont boldSystemFontOfSize:14];
    [self addSubview:lblTitle];
    
    UIButton *btnChange = [UIButton buttonWithType:UIButtonTypeCustom];
    btnChange.frame = CGRectMake(15, 0, DeviceWidth-30, 50);
    [btnChange addTarget:self action:@selector(seeAllClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnChange];
    [btnChange setImage:[UIImage imageNamed:@"ic_home_arrow"] forState:UIControlStateNormal];
    btnChange.imageEdgeInsets = UIEdgeInsetsMake(0, DeviceWidth-30-10, 0, 0);
    
    UILabel *lblSeeAll = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth-95, 6, 50, 38)];
    lblSeeAll.text = @"See All";
    lblSeeAll.textAlignment = NSTextAlignmentRight;
    lblSeeAll.textColor = [UIColor color999999];
    lblSeeAll.font =[UIFont systemFontOfSize:12];
    [btnChange addSubview:lblSeeAll];
    
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(125, 125);
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 10;
    layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 0);
    
    articleCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,50, DeviceWidth, 125) collectionViewLayout:layout];
    articleCollectionView.backgroundColor = [UIColor whiteColor];
    articleCollectionView.delegate = self;
    articleCollectionView.dataSource = self;
    articleCollectionView.showsHorizontalScrollIndicator = NO;
    [self addSubview:articleCollectionView];
    
    [articleCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"photoCell"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.photoArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *idenStr = @"photoCell";
    UICollectionViewCell *cell  =[collectionView dequeueReusableCellWithReuseIdentifier:idenStr forIndexPath:indexPath];
    if (cell==nil) {
        cell = [[UICollectionViewCell alloc] init];
    }
    cell.backgroundColor = [UIColor whiteColor];
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.contentView.bounds];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    NSDictionary *dic = [self.photoArr objectAtIndex:indexPath.row];
    if ([currentTitle isEqualToString:@"Instagram Photos"]) {
        [imageView sd_setImageWithURL:[NSURL URLWithString:[dic objectForKey:@"low_resolution_image"]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
    }else{
        [imageView sd_setImageWithURL:[NSURL URLWithString:[[dic objectForKey:@"image_url"] returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
    }
    
    [cell.contentView addSubview:imageView];
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    NSDictionary *dic = [self.photoArr objectAtIndex:indexPath.row];
    NSMutableArray *arr = [NSMutableArray array];
    if ([currentTitle isEqualToString:@"User Photos"]) {
        for (NSDictionary *dic in self.photoArr) {
            IMGDish *dish = [[IMGDish alloc] init];
            dish.dishId = [dic objectForKey:@"id"];
            dish.imageUrl = [dic objectForKey:@"image_url"];
            dish.restaurantId = [dic objectForKey:@"restaurant_id"];
            dish.isRestaurantPhoto = YES;
            dish.photoCreditTypeDic = [dic objectForKey:@"photo_credit_type"];
            [arr addObject:dish];
        }
       
    }else if ([currentTitle isEqualToString:@"Restaurant Photos"]){
        for (NSDictionary *dic in self.photoArr) {
            IMGDish *dish = [[IMGDish alloc] init];
            dish.dishId = [dic objectForKey:@"id"];
            dish.imageUrl = [dic objectForKey:@"image_url"];
            dish.restaurantId = [dic objectForKey:@"restaurant_id"];
            dish.isRestaurantPhoto = YES;
            dish.photoCreditTypeDic = [dic objectForKey:@"photo_credit_type"];
            //dish.dishType = [dic objectForKey:@"photo_credit_type"];
            [arr addObject:dish];
        }
        
    }else if ([currentTitle isEqualToString:@"Instagram Photos"]){
        for (NSDictionary *dic in self.photoArr) {
            V2_InstagramModel *model = [[V2_InstagramModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [arr addObject:model];
        }
    }else if ([currentTitle isEqualToString:@"Menu Photos"]){
        for (NSDictionary *dic in self.photoArr) {
            IMGMenu *menu = [[IMGMenu alloc] init];

            menu.imageUrl = [dic objectForKey:@"image_url"];
            menu.menuId = [dic objectForKey:@"id"];
            //menu.createTime = [dic objectForKey:@"create_time"];
            menu.restaurantId = [dic objectForKey:@"restaurant_id"];
            menu.userId = [dic objectForKey:@"user_id"];
            [arr addObject:menu];
        }
    }
    
    if (self.photoClick) {
        self.photoClick(arr,indexPath.row);
    }
    
}

- (void)seeAllClick{
    if (self.seeAll) {
        self.seeAll();
    }
}

@end
