//
//  InstagramDetailController.h
//  Qraved
//
//  Created by Tek Yin on 5/11/17.
//  Copyright © 2017 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@class V2_InstagramPhotoView;
@class V2_InstagramModel;

@interface InstagramDetailController : UIViewController
@property(weak, nonatomic) IBOutlet UILabel              *uSender;
@property(weak, nonatomic) IBOutlet UILabel              *uDate;
@property(weak, nonatomic) IBOutlet V2_InstagramPhotoView *uInstagramPhoto;
@property(weak, nonatomic) IBOutlet UILabel      *uDetail;
@property(weak, nonatomic) IBOutlet UIButton             *uDetailDismissButton;

@property(nonatomic, strong) V2_InstagramModel *instagramModel;

+ (UIViewController *)createWithData:(V2_InstagramModel *)model;
@end
