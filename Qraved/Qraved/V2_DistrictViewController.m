//
//  V2_DistrictViewController.m
//  Qraved
//
//  Created by harry on 2017/7/27.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DistrictViewController.h"
#import "HomeService.h"
#import "V2_DistrictModel.h"
#import "DistrictCell.h"
#import "V2_LovePreferenceLayout.h"
#import "V2_DistrictTableViewCell.h"
#import "UserDataHandler.h"
#import "LoadingView.h"
#import "V2_LocationModel.h"
#import "CommonMethod.h"
@interface V2_DistrictViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *selectArray;
    NSMutableArray *districtArray;
    UITableView *districtTableView;
    NSMutableArray *sectionArray;
    UIImageView *imageV;
}
@end

@implementation V2_DistrictViewController
//static NSInteger mySection = 1000;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadNavButton];
    [self loadData];
    [self requestData];
    [self loadMainUI];
    
     sectionArray = [[NSMutableArray alloc] initWithObjects:@"1",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",nil];
}

- (void)loadNavButton{
    self.navigationItem.title = @"Locations";
    
    UIButton *cst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
    [cst addTarget:self action:@selector(doApply:) forControlEvents:UIControlEventTouchUpInside];
    [cst setTitle:@"Done" forState:UIControlStateNormal];
    [cst setTitleColor:[UIColor colorWithHexString:@"09BFD3"] forState:UIControlStateNormal];
    cst.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    cst.titleLabel.font = [UIFont systemFontOfSize:17];
    
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithCustomView:cst];
    [self.navigationItem setRightBarButtonItems:@[btn] animated:NO];
    
    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCancel.frame = CGRectMake(0, 0, 60, 30);
    [btnCancel addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnCancel setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    btnCancel.titleLabel.font = [UIFont systemFontOfSize:17];
    btnCancel.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnCancel];
}

- (void)loadData{
    selectArray = [NSMutableArray array];
    districtArray = [NSMutableArray array];
    
    for (V2_LocationModel *model in self.locationArray) {
        V2_AreaModel *areaModel = [[V2_AreaModel alloc] init];
        areaModel.districtId = model.location_id;
        areaModel.name = model.name;
        areaModel.selected = YES;
        areaModel.type = model.location_type;
        [selectArray addObject:areaModel];
    }
}

- (void)loadMainUI{
    self.view.backgroundColor = [UIColor whiteColor];
    
    districtTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-44) style:UITableViewStyleGrouped];
    districtTableView.backgroundColor = [UIColor whiteColor];
    districtTableView.delegate = self;
    districtTableView.dataSource = self;
    
    [self.view addSubview:districtTableView];
    districtTableView.separatorStyle = UITableViewCellSeparatorStyleNone;

}

- (void)requestData{
    
    [[LoadingView sharedLoadingView] startLoading];
    [HomeService getLocation:nil andIsPreferred:YES andBlock:^(id locationArr) {
        
        [districtArray addObjectsFromArray:locationArr];
        [self handleSelectDistrict];
        [districtTableView reloadData];
        [[LoadingView sharedLoadingView] stopLoading];
        
    } andError:^(NSError *error) {
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

- (void)handleSelectDistrict{
    for (int i = 0; i < districtArray.count; i ++) {
        V2_DistrictModel *model = [districtArray objectAtIndex:i];
        
        for (V2_AreaModel *areaModel in model.districts) {
            for (V2_AreaModel *selectModel in selectArray) {
                if ([areaModel.districtId isEqual:selectModel.districtId] && [areaModel.type isEqualToString:selectModel.type]) {
                    areaModel.selected = YES;
                    selectModel.landMarkTypeId = areaModel.landMarkTypeId;
                    break;
                }
            }
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (districtArray.count>0) {
        return districtArray.count+1;
    }else{
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([[sectionArray objectAtIndex:section] integerValue] == 1) {
        return 1;
    }else{
    
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
     NSString *idenStr = [NSString stringWithFormat:@"%ld%ld",(long)indexPath.section, indexPath.row];
    V2_DistrictTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
    if (cell == nil) {
        cell = [[V2_DistrictTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.section==0) {
        
        cell.selectArr = selectArray;
        
        cell.removeDidtrict = ^(V2_AreaModel *model){
            
            for (V2_DistrictModel *districtModel in districtArray) {
                for (V2_AreaModel *areaModel in districtModel.districts) {
                    if ([areaModel.districtId isEqual:model.districtId] && [areaModel.type isEqualToString:model.type]) {
                        areaModel.selected = NO;
                    }
                }
            }
            
            for (V2_AreaModel *areaModel in selectArray) {
                if ([areaModel.districtId isEqual:model.districtId] && [areaModel.type isEqualToString:model.type]) {
                    model.selected = NO;
                    areaModel.selected = NO;
                    [selectArray removeObject:areaModel];
                    [districtTableView reloadData];
                    return ;
                }
            }
        };
        
    }else{
        cell.addDistrict = ^(V2_AreaModel *model){
            model.selected = YES;
            [selectArray addObject:model];
            [districtTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
        };
        cell.removeDidtrict = ^(V2_AreaModel *model){
            
            for (V2_AreaModel *areaModel in selectArray) {
                if ([areaModel.districtId isEqual:model.districtId] && [areaModel.type isEqualToString:model.type]) {
                    model.selected = NO;
                    areaModel.selected = NO;
                    [selectArray removeObject:areaModel];
                    [districtTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
                    return ;
                }
            }
        };
        V2_DistrictModel *model = [districtArray objectAtIndex:indexPath.section-1];
        if (model.districts.count>0) {
            cell.model = model;
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
     V2_DistrictTableViewCell *cell = [[V2_DistrictTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"heightCell"];
    if (indexPath.section==0) {

        cell.selectArr = selectArray;
        return cell.pointY;
    }else{
        V2_DistrictModel *model = [districtArray objectAtIndex:indexPath.section-1];
        cell.model = model;
        return cell.pointY;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIButton *view = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 38)];
    [view addTarget:self action:@selector(foldBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    view.tag = 100 + section;
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 20, DeviceWidth-45, 18)];
    lblTitle.font = [UIFont boldSystemFontOfSize:16];
    lblTitle.textColor = [UIColor color333333];
    [view addSubview:lblTitle];
    if (section==0) {
        lblTitle.text = @"Selected Location";
    }else{
        V2_DistrictModel *model = [districtArray objectAtIndex:section-1];
        lblTitle.text = [model.name capitalizedString];
    }
    
    imageV = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth - 30, 0, 12, 8)];
    imageV.image = [UIImage imageNamed:@"ic_city_down"];
    imageV.centerY = lblTitle.centerY;
    
    [view addSubview:imageV];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 38;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

- (void)doApply:(UIButton *)button{
    NSLog(@"%@",selectArray);
    button.enabled = NO;
    [[LoadingView sharedLoadingView] startLoading];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSMutableArray *districtArr = [NSMutableArray array];
    NSMutableArray *landMarkArr = [NSMutableArray array];
    NSMutableArray *locationArr = [NSMutableArray array];

    for (V2_AreaModel *model in selectArray) {
        if ([model.type isEqualToString:@"landmark"]) {
            [landMarkArr addObject:model.districtId];
        }else{
            [districtArr addObject:model.districtId];
        }
        [locationArr addObject:model.name];
    }
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setValue:[locationArr componentsJoinedByString:@","] forKey:@"Value"];
    [[Amplitude instance] logEvent:@"CL - Apply Location preference" withEventProperties:param];
    
    [params setObject:[districtArr componentsJoinedByString:@","] forKey:@"districtIds"];
    [params setObject:[landMarkArr componentsJoinedByString:@","] forKey:@"landmarkIds"];
    [params setObject:[IMGUser currentUser].userId forKey:@"userId"];
    [params setObject:[IMGUser currentUser].token forKey:@"t"];
    [params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
    [UserDataHandler preferredLocationAdd:params andSuccessBlock:^(id responseObjectLocal) {
        [[LoadingView sharedLoadingView] stopLoading];
        button.enabled = YES;
        
        if (self.refreshLocation) {
            self.refreshLocation();
        }
        [self.navigationController popViewControllerAnimated:YES];
        
    } andFailedBlock:^(NSString *errorMsg) {
        button.enabled = YES;
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

- (void)back:(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)foldBtnClick:(UIButton *)sender{

//    NSLog(@"fold%ld",(long)btn.tag);
//    btn.selected = !btn.selected;
//    NSInteger newTag = btn.tag - 1000;
//    mySection = newTag;
    
    if ([[sectionArray objectAtIndex:sender.tag - 100] intValue] == 0) {
        [sectionArray replaceObjectAtIndex:sender.tag - 100 withObject:@"1"];
        NSLog(@"%ld打开",(long)sender.tag);

        [CommonMethod unfoldAnimationWith:imageV];
    }
    else
    {
        [sectionArray replaceObjectAtIndex:sender.tag - 100 withObject:@"0"];
        NSLog(@"%ld关闭",(long)sender.tag);
        
        [CommonMethod foldAnimationWith:imageV];
    }
    
    
    
    [districtTableView reloadData];
}

@end
