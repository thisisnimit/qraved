//
// Created by Tek Yin on 5/24/17.
// Copyright (c) 2017 Qraved. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface QRStarView : UIImageView

- (void)setValue:(int)value;

- (void)pulse;

- (id)initWithPosition:(int)index stars:(NSArray *)starArr;
@end

