//
// Created by Tek Yin on 5/24/17.
// Copyright (c) 2017 Qraved. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@protocol QRStarRatingDelegate;

IB_DESIGNABLE

@interface QRStarComponent : UIControl
@property(nonatomic, strong) id <QRStarRatingDelegate> delegate;

- (void)setValue:(int)i;
@end

@protocol QRStarRatingDelegate

//- (void)starComponent:(QRStarComponent *)control onRatingChangeUpdate:(float)rating;

- (void)starComponent:(QRStarComponent *)control onRatingChangeEnd:(int)rating;

@end