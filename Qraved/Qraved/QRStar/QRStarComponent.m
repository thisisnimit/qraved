//
// Created by Tek Yin on 5/24/17.
// Copyright (c) 2017 Qraved. All rights reserved.
//

#import "QRStarComponent.h"
#import "QRStarView.h"
#import "UIView+Subviews.h"


@implementation QRStarComponent {

    int                currentIdx;
    NSArray<UIImage *> *stars;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupView];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    self.clipsToBounds = NO;
    stars = @[
              [UIImage imageNamed:@"ic_star_line_big"],
              [UIImage imageNamed:@"ic_star_peach_big"],
              [UIImage imageNamed:@"ic_star_yellow_big"],
              [UIImage imageNamed:@"ic_star_gold_big"],
              [UIImage imageNamed:@"ic_star_rose_big"],
              [UIImage imageNamed:@"ic_star_red_big"],
              ];

    for (int i = 0; i < 5; i++) {
        QRStarView *v = [[QRStarView alloc] initWithPosition:i stars:stars];
        [self addSubview:v];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    for (int i = 0; i < 5; i++) {
        QRStarView *v      = (QRStarView *) [self subViewWithTag:i];
        CGFloat    size    = MIN(self.height, stars[0].size.width);
        CGFloat    spacing = size / stars[0].size.width * 10;
        CGFloat    xx      = (self.width - size * 5 - spacing * 4) / 2;
        CGFloat    yy      = (self.height - size) / 2;
        v.frame = CGRectMake(xx + (size + spacing) * i, yy, size, size);
    }
}

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    CGPoint    point          = [touch locationInView:self];
    QRStarView *pressedButton = [self starForPoint:point];
    if (pressedButton) {
        int idx = (int) pressedButton.tag;
//        if (pressedButton.highlighted) {
        [self disableStarsDownToExclusive:idx];
//        } else {
        [self enableStarsUpTo:idx];
//        }
        currentIdx = idx;
        [pressedButton pulse];

        if (self.delegate != nil) {
            [self.delegate starComponent:self onRatingChangeEnd:currentIdx];
        }
    }
    return YES;
}

//- (BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
//    CGPoint point = [touch locationInView:self];
//    if (point.x > [self subViewWithTag:0].frame.origin.x) {
//        QRStarView *pressedButton = [self starForPoint:point];
//        if (pressedButton) {
//            int        idx            = (int) pressedButton.tag;
//            QRStarView *currentButton = (QRStarView *) [self subViewWithTag:currentIdx];
//
//            if (idx < currentIdx) {
//                [currentButton setValue:-1];
//                currentIdx = idx;
//                [self disableStarsDownToExclusive:idx];
//            } else if (idx > currentIdx) {
//                [currentButton setValue:-1];
//                [pressedButton setValue:idx];
//                currentIdx = idx;
//                [self enableStarsUpTo:idx];
//            }
//        } else if (point.x < [self subViewWithTag:0].frame.origin.x) {
//            ((QRStarView *) [self subViewWithTag:0]).highlighted = NO;
//            currentIdx = -1;
//            [self disableStarsDownToExclusive:0];
//        }
//        if (self.delegate != nil) {
//            [self.delegate starComponent:self onRatingChangeUpdate:currentIdx];
//        }
//    }
//    return YES;
//}
//
//- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
//    [super endTrackingWithTouch:touch withEvent:event];
//    if (self.delegate != nil) {
//        [self.delegate starComponent:self onRatingChangeEnd:currentIdx];
//    }
//}

- (QRStarView *)starForPoint:(CGPoint)point {
    for (int i = 0; i < 5; i++) {
        if (CGRectContainsPoint([self subViewWithTag:i].frame, point)) {
            return (QRStarView *) [self subViewWithTag:i];
        }
    }
    return nil;
}

- (void)disableStarsDownToExclusive:(int)idx {
//    for (int i = 0; i <= idx; i++) {
//        QRStarView *b = (QRStarView *) [self subViewWithTag:i];
//        [b setValue:idx];
//    }
    for (int i = 0; i < 5; i++) {
        QRStarView *b = (QRStarView *) [self subViewWithTag:i];
        if (i > idx)
            [b setValue:-1];
        else {
            [b setValue:idx];
            [b pulse];
        }

    }
}

- (void)disableStarsDownTo:(int)idx {
    for (int i = 5; i >= idx; --i) {
        QRStarView *b = (QRStarView *) [self subViewWithTag:i];
//        b.highlighted = NO;
        [b setValue:-1];
    }
}


- (void)enableStarsUpTo:(int)idx {
    for (int i = 0; i <= idx; i++) {
        QRStarView *b = (QRStarView *) [self subViewWithTag:i];
//        b.highlighted = YES;
        [b setValue:idx];
        [b pulse];
    }
}

- (void)setValue:(int)i {
    currentIdx = i;
    [self disableStarsDownToExclusive:i];
}
@end
