//
// Created by Tek Yin on 5/24/17.
// Copyright (c) 2017 Qraved. All rights reserved.
//

#import "QRStarView.h"


@implementation QRStarView {

}
static NSArray<UIImage *> *stars;

- (id)initWithPosition:(int)index stars:(NSArray *)starArr {
    if (stars == nil)
        stars = starArr;

    UIImage *idleImage = stars[0];
    self = [super initWithFrame:CGRectMake(0, 0, idleImage.size.width, idleImage.size.height)];

    if (self) {
        [self setTag:index];
        self.contentMode   = UIViewContentModeScaleAspectFit;
        self.image         = idleImage;
        self.clipsToBounds = NO;
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (void)setValue:(int)value {
    self.image = stars[(NSUInteger) value + 1];
}

- (void)pulse {
    CABasicAnimation *theAnimation;

    theAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    theAnimation.duration     = 0.1f;
    theAnimation.repeatCount  = 0;
    theAnimation.autoreverses = YES;
    theAnimation.fromValue    = @1.0F;
    theAnimation.toValue      = @1.5F;
    [self.layer addAnimation:theAnimation forKey:@"animateScale"];
}


@end