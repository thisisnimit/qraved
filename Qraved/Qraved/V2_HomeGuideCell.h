//
//  V2_HomeGuideCell.h
//  Qraved
//
//  Created by harry on 2017/10/12.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_HomeModel.h"

@protocol V2_HomeGuideCellDelegate <NSObject>
- (void)seeAllGuide:(NSString *)name;
- (void)gotoDiningGuideDetail:(IMGDiningGuide *)diningGuide;

@end

@interface V2_HomeGuideCell : UITableViewCell

@property (nonatomic, strong) V2_HomeModel *model;
@property (nonatomic, weak) id <V2_HomeGuideCellDelegate>delegate;

@end
