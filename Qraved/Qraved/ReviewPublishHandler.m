//
//  ReviewPublishHandler.m
//  Qraved
//
//  Created by apple on 17/4/5.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "ReviewPublishHandler.h"
#import "LoadingView.h"
#import "IMGDish.h"

@implementation ReviewPublishHandler
+(void)uploadDishWithPrama:(NSMutableDictionary*)params andSuccessBlock:(void(^)(NSDictionary* dic,NSString *returnStatusString,NSString *exceptionMsg,NSString* photoId))successBlock anderrorBlock:(void(^)( NSString *str))errorBlock{
    [[IMGNetWork sharedManager]POST:@"uploadDish" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSString *returnStatusString = [responseObject objectForKey:@"status"];
        NSString * exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
        NSString *photoID = [responseObject objectForKey:@"id"];
         if([returnStatusString isEqualToString:@"succeed"]){
            NSDictionary *dishDictionary = [responseObject objectForKey:@"dish"];
             successBlock(dishDictionary,returnStatusString,exceptionMsg,photoID);
         }else{
             successBlock(nil,returnStatusString,exceptionMsg,photoID);

         }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        [[LoadingView sharedLoadingView] stopLoading];
        errorBlock(error.localizedFailureReason);
        NSLog(@"requestToAddDish error: %@",error.localizedDescription);
    }];

}
+(void)deleteDishWithParams:(NSDictionary*)parameters andBlock:(void(^)())block{
    
    [[IMGNetWork sharedManager]POST:@"dish/delete" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        
        block();
    }failure:^(NSURLSessionDataTask *operation, NSError *error){
    }];

}
+(void)dashboardWithParams:(NSDictionary*)parameters{
    [[IMGNetWork sharedManager] GET:@"app/dashboard" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSString * exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
        [[LoadingView sharedLoadingView] stopLoading];
        if(exceptionMsg){
            
        }else{
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"ReservationHandler histories error:%@",error.description);
    }];
}
+(void)getReviewWithParams:(NSDictionary*)parameters andBlock:(void(^)(NSDictionary *dict))block{
    [[IMGNetWork sharedManager] POST:@"review/getReviewById" parameters:parameters progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSDictionary *dict = [responseObject objectForKey:@"review"];
        
        [[LoadingView sharedLoadingView] stopLoading];
        
        block(dict);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        [[LoadingView sharedLoadingView] stopLoading];
        NSLog(@"user/review/last error: %@",error.localizedDescription);
        
    }];

}
+(void)getLastReviewWithParams:(NSDictionary*)parameters andBlock:(void(^)(NSDictionary *dict))block{
    [[IMGNetWork sharedManager]POST:@"user/review/last" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        
        NSString *returnStatus = [responseObject objectForKey:@"hasReview"];
        
        if (![returnStatus intValue])
        {
            [[LoadingView sharedLoadingView] stopLoading];
            return ;
        }
        if (returnStatus)
        {
            NSDictionary *dict = [responseObject objectForKey:@"review"];
            block(dict);
            
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        [[LoadingView sharedLoadingView] stopLoading];
        NSLog(@"user/review/last error: %@",error.localizedDescription);
    }];


}
+(void)editdescriptionWithParams:(NSMutableDictionary *)dataDic{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",QRAVED_WEB_SERVICE_SERVER,@"dish/editdescription"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSMutableDictionary *json = dataDic;
    [json setValue:@"2" forKey:@"client"];
    [json setValue:QRAVED_WEB_SERVICE_VERSION forKey:@"v"];
    [json setValue:QRAVED_APIKEY forKey:@"appApiKey"];
    NSData *data = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:nil];
    request.HTTPBody = data;
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
    }];
}
+(void)updateReviewWithPatams:(NSDictionary*)parameters andBlock:(void(^)(NSDictionary *dic, NSString* exceptionMsg,NSString *ratingMap))block anderrorBlock:(void(^)( NSString *str))errorBlock{
    [[IMGNetWork sharedManager]POST: @"review/update" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        
        NSString * exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
        
        NSString *ratingMap = [responseObject objectForKey:@"ratingMap"];
        NSDictionary *reviewDictionary=[responseObject objectForKey:@"review"];
        block(reviewDictionary,exceptionMsg,ratingMap);
        
        
    }failure:^(NSURLSessionDataTask *operation, NSError *error){

        errorBlock(error.localizedDescription);
    }];
}
+(void)writereviewWithParams:(NSDictionary*)parameters andBlock:(void(^)(NSString *returnStatusString,NSString *exceptionMsg,NSString *userInteractCount,NSNumber *returnReviewId,NSDictionary *reviewDictionary,NSString* ratingMap))block anderrorBlock:(void(^)( NSString *str))errorBlock{
    

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",QRAVED_WEB_SERVICE_SERVER,@"user/writereviewV2"]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSDictionary *json = parameters;
    NSData *data = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:nil];
    request.HTTPBody = data;
    [[IMGNetWork sharedManager] requestWithPath:@"user/writereviewV2" withBody:data withSuccessBlock:^(id responseObject) {
        NSString *returnStatusString = [responseObject objectForKey:@"status"];
        NSString *exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
        if ([exceptionMsg
             hasPrefix:@"Vendor can not write review for his restaurant"]) {
            [[LoadingView sharedLoadingView] stopLoading];
            
            [SVProgressHUD showImage:[UIImage imageNamed:@""] status:L(@"ERROR\nYou cannot  give rating or write review on your own restaurant")];
            [SVProgressHUD dismissWithDelay:1.5];
            
            return ;
            
        }
        NSString *userInteractCount = [responseObject objectForKey:@"userInteractCount"];
        NSNumber *returnReviewId =[responseObject objectForKey:@"reviewId"];
        NSDictionary *reviewDictionary=[responseObject objectForKey:@"review"];
        NSString* ratingMap = [responseObject objectForKey:@"ratingMap"];
        block(returnStatusString,exceptionMsg,userInteractCount,returnReviewId,reviewDictionary,ratingMap);
    } withFailurBlock:^(NSError *error) {
         errorBlock(error.localizedDescription);
    }];
}
+(void)updateReviewWithParams:(NSDictionary*)parameters andBlock:(void(^)(NSDictionary *dic,NSString *exceptionMsg,NSString *ratingMap))block anderrorBlock:(void(^)( NSString *str))errorBlock{

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",QRAVED_WEB_SERVICE_SERVER,@"review/update/v2"]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSDictionary *json = parameters;
    NSData *data = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:nil];
    request.HTTPBody = data;
    [[IMGNetWork sharedManager] requestWithPath:@"review/update/v2" withBody:data withSuccessBlock:^(id responseObject) {
        NSString * exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
        
        NSDictionary *reviewDictionary=[responseObject objectForKey:@"review"];
        
        NSString *ratingMap =  [responseObject objectForKey:@"ratingMap"];
        block(reviewDictionary,exceptionMsg,ratingMap);

    } withFailurBlock:^(NSError *error) {
         errorBlock(error.localizedDescription);
    }];
}
@end
