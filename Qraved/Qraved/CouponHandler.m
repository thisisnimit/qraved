//
//  CouponHandler.m
//  Qraved
//
//  Created by harry on 2018/1/22.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "CouponHandler.h"
#import "CouponModel.h"
#import "CampaignModel.h"
#import "BrandVideoModel.h"
#import "V2_InstagramModel.h"
#import "BrandBannerModel.h"
#import "BrandEventModel.h"
#import "IMGMediaComment.h"

@implementation CouponHandler


+(void)getCouponDetail:(NSDictionary*)dic andSuccessBlock:(void(^)(CouponModel *model))successBlock anderrorBlock:(void(^)())errorBlock{
    [[IMGNetWork sharedManager] GET:@"id/home/api/v2/mobile/coupon" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        CouponModel *model = [[CouponModel alloc] init];
        [model setValuesForKeysWithDictionary:responseObject];
        
        
        NSDictionary *styleDic = [[responseObject objectForKey:@"style"] objectForKey:@"voucher"];
        if ([Tools isBlankString:[styleDic objectForKey:@"button_background_color"]]) {
            model.button_background_color = @"D20000";
        }else{
            model.button_background_color = [styleDic objectForKey:@"button_background_color"];
        }
        
        if ([Tools isBlankString:[styleDic objectForKey:@"button_font_color"]]) {
            model.button_font_color = @"FFFFFF";
        }else{
            model.button_font_color = [styleDic objectForKey:@"button_font_color"];
        }
        
        if ([Tools isBlankString:[styleDic objectForKey:@"button_font_color_disabled"]]) {
            model.button_font_color_disabled = @"FFFFFF";
        }else{
            model.button_font_color_disabled = [styleDic objectForKey:@"button_font_color_disabled"];
        }
        
        if ([Tools isBlankString:[styleDic objectForKey:@"button_background_color_disabled"]]) {
            model.button_background_color_disabled = @"#EAEAEA";
        }else{
            model.button_background_color_disabled = [styleDic objectForKey:@"button_background_color_disabled"];
        }
        
        if ([Tools isBlankString:[styleDic objectForKey:@"button_outline_color_disabled"]]) {
            model.button_outline_color_disabled = @"#FFFFFF";
        }else{
            model.button_outline_color_disabled = [styleDic objectForKey:@"button_outline_color_disabled"];
        }
        
        if ([Tools isBlankString:[styleDic objectForKey:@"main_background_color"]]) {
            model.main_background_color = @"D20000";
        }else{
            model.main_background_color = [styleDic objectForKey:@"main_background_color"];
        }
        
        if ([Tools isBlankString:[styleDic objectForKey:@"main_font_color"]]) {
            model.main_font_color = @"FFFFFF";
        }else{
            model.main_font_color = [styleDic objectForKey:@"main_font_color"];
        }
        
        if ([Tools isBlankString:[styleDic objectForKey:@"sub_background_color"]]) {
            model.sub_background_color = @"FFFFFF";
        }else{
            model.sub_background_color = [styleDic objectForKey:@"sub_background_color"];
        }
        
        if ([Tools isBlankString:[styleDic objectForKey:@"sub_font_color"]]) {
            model.sub_font_color = @"FFFFFF";
        }else{
            model.sub_font_color = [styleDic objectForKey:@"sub_font_color"];
        }
        
        if ([Tools isBlankString:[styleDic objectForKey:@"button_outline_color"]]) {
            model.button_outline_color = @"FFFFFF";
        }else{
            model.button_outline_color = [styleDic objectForKey:@"button_outline_color"];
        }
        
        successBlock(model);
        NSLog(@"%@",responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock();
    }];
}

+(void)getBrand:(NSDictionary*)dic andSuccessBlock:(void(^)(BrandModel *model))successBlock anderrorBlock:(void(^)())errorBlock{
    [[IMGNetWork sharedManager] GET:@"id/home/api/v2/mobile/brands" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        BrandModel *model = [[BrandModel alloc] init];
        [model setValuesForKeysWithDictionary:[responseObject objectForKey:@"detail"]];
        
        NSMutableArray *journalArr = [NSMutableArray array];
        for (NSDictionary *journalDic in [responseObject objectForKey:@"articles"]) {
            IMGMediaComment *journal = [[IMGMediaComment alloc] init];
            journal.mediaCommentId = [journalDic objectForKey:@"article_id"];
            journal.journalTitle = [journalDic objectForKey:@"title"];
            journal.contents = [journalDic objectForKey:@"description"];
            journal.status = [journalDic objectForKey:@"status"];
            journal.journalImageUrl = [journalDic objectForKey:@"main_photo"];
            [journalArr addObject:journal];
        }
        
        NSMutableArray *bannerArr = [NSMutableArray array];
        for (NSDictionary *bannerDic in [responseObject objectForKey:@"banners"]) {
            BrandBannerModel *model = [[BrandBannerModel alloc] init];
            [model setValuesForKeysWithDictionary:bannerDic];
            [bannerArr addObject:model];
        }
        
        NSMutableArray *eventArr = [NSMutableArray array];
        for (NSDictionary *eventDic in [responseObject objectForKey:@"event"]) {
            CampaignModel *model = [[CampaignModel alloc] init];
            [model setValuesForKeysWithDictionary:eventDic];
            [eventArr addObject:model];
        }
        
        NSMutableArray *instagramArr = [NSMutableArray array];
        for (NSDictionary *instagramDic in [responseObject objectForKey:@"instagram_photos"]) {
            V2_InstagramModel *model = [[V2_InstagramModel alloc] init];
            [model setValuesForKeysWithDictionary:instagramDic];
            [instagramArr addObject:model];
        }
        
        NSMutableArray *couponArr = [NSMutableArray array];
        for (NSDictionary *couponDic in [responseObject objectForKey:@"coupons"]) {
            CouponModel *model = [[CouponModel alloc] init];
            [model setValuesForKeysWithDictionary:couponDic];
            [couponArr addObject:model];
        }
        
        NSMutableArray *videoArr = [NSMutableArray array];
        for (NSDictionary *videoDic in [responseObject objectForKey:@"videos"]) {
            BrandVideoModel *model = [[BrandVideoModel alloc] init];
            [model setValuesForKeysWithDictionary:videoDic];
            [videoArr addObject:model];
        }
        
        model.bannerArr = bannerArr;
        model.eventArr = eventArr;
        model.instagramArr = instagramArr;
        model.couponArr = couponArr;
        model.journalArr = journalArr;
        model.videoArr = videoArr;
        successBlock(model);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

+(void)getMall:(NSDictionary*)dic andSuccessBlock:(void(^)(BrandModel *model))successBlock anderrorBlock:(void(^)())errorBlock{
    [[IMGNetWork sharedManager] GET:@"id/home/api/v2/mobile/mall" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        BrandModel *model = [[BrandModel alloc] init];
        [model setValuesForKeysWithDictionary:[responseObject objectForKey:@"detail"]];
        
        NSMutableArray *journalArr = [NSMutableArray array];
        for (NSDictionary *journalDic in [responseObject objectForKey:@"articles"]) {
            IMGMediaComment *journal = [[IMGMediaComment alloc] init];
            journal.mediaCommentId = [journalDic objectForKey:@"article_id"];
            journal.journalTitle = [journalDic objectForKey:@"title"];
            journal.contents = [journalDic objectForKey:@"description"];
            journal.status = [journalDic objectForKey:@"status"];
            journal.journalImageUrl = [journalDic objectForKey:@"main_photo"];
            [journalArr addObject:journal];
        }
        
        NSMutableArray *bannerArr = [NSMutableArray array];
        for (NSDictionary *bannerDic in [responseObject objectForKey:@"banners"]) {
            BrandBannerModel *model = [[BrandBannerModel alloc] init];
            [model setValuesForKeysWithDictionary:bannerDic];
            [bannerArr addObject:model];
        }
        
        NSMutableArray *eventArr = [NSMutableArray array];
        for (NSDictionary *eventDic in [responseObject objectForKey:@"event"]) {
            CampaignModel *model = [[CampaignModel alloc] init];
            [model setValuesForKeysWithDictionary:eventDic];
            
            [eventArr addObject:model];
        }
        
        
        NSMutableArray *instagramArr = [NSMutableArray array];
        for (NSDictionary *instagramDic in [responseObject objectForKey:@"instagram"]) {
            V2_InstagramModel *model = [[V2_InstagramModel alloc] init];
            [model setValuesForKeysWithDictionary:instagramDic];
            [instagramArr addObject:model];
        }
        
        NSMutableArray *couponArr = [NSMutableArray array];
        for (NSDictionary *couponDic in [responseObject objectForKey:@"coupon"]) {
            CouponModel *model = [[CouponModel alloc] init];
            [model setValuesForKeysWithDictionary:couponDic];
            [couponArr addObject:model];
        }
        
        NSMutableArray *videoArr = [NSMutableArray array];
        for (NSDictionary *videoDic in [responseObject objectForKey:@"videos"]) {
            BrandVideoModel *model = [[BrandVideoModel alloc] init];
            [model setValuesForKeysWithDictionary:videoDic];
            [videoArr addObject:model];
        }
        
        model.bannerArr = bannerArr;
        model.eventArr = eventArr;
        model.instagramArr = instagramArr;
        model.couponArr = couponArr;
        model.journalArr = journalArr;
        model.videoArr = videoArr;
        successBlock(model);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

+(void)couponSave:(NSDictionary*)dic andSuccessBlock:(void(^)())successBlock anderrorBlock:(void(^)())errorBlock{
    [[IMGNetWork sharedManager] POST:@"id/home/api/v2/mobile/user_coupon/save" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        successBlock();
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock();
    }];
}

+(void)couponUnsave:(NSDictionary*)dic andSuccessBlock:(void(^)())successBlock anderrorBlock:(void(^)())errorBlock{
    [[IMGNetWork sharedManager] POST:@"id/home/api/v2/mobile/user_coupon/unsave" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        successBlock();
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock();
    }];
}

+(void)couponRedeem:(NSDictionary*)dic andSuccessBlock:(void(^)())successBlock anderrorBlock:(void(^)())errorBlock{
    [[IMGNetWork sharedManager] POST:@"id/home/api/v2/mobile/user_coupon/redeem" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        successBlock();
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock();
    }];
}

+(void)couponList:(NSDictionary*)dic andSuccessBlock:(void(^)(NSArray *couponList))successBlock anderrorBlock:(void(^)())errorBlock{
    [[IMGNetWork sharedManager] GET:@"id/home/api/v2/coupons/v2" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *couponArr = [NSMutableArray array];
        for (NSDictionary *couponDic in [responseObject objectForKey:@"coupon_list"]) {
            CouponModel *model = [[CouponModel alloc] init];
            [model setValuesForKeysWithDictionary:couponDic];
            [couponArr addObject:model];
        }
        successBlock(couponArr);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock();
    }];
}

@end
