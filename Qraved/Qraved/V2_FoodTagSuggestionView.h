//
//  V2_FoodTagSuggestionView.h
//  Qraved
//
//  Created by harry on 2017/10/3.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiscoverCategories.h"
@interface V2_FoodTagSuggestionView : UIView

@property (nonatomic, strong) DiscoverCategoriesModel *selectModel;

@property (nonatomic, strong) NSArray *foodTagArr;
@property (nonatomic, copy) void (^selectFoodTag)(DiscoverCategoriesModel *model,NSInteger index);

@end
