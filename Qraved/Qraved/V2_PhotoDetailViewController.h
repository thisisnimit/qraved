//
//  V2_PhotoDetailViewController.h
//  Qraved
//
//  Created by harry on 2017/7/17.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "V2_InstagramModel.h"
@interface V2_PhotoDetailViewController : BaseViewController

@property (nonatomic, assign) BOOL isFromMiniRDP;
@property (nonatomic, assign) BOOL isInstagram;
@property (nonatomic, strong) IMGRestaurant *restaurant;
@property (nonatomic, strong) V2_InstagramModel *currentInstagram;
@property (nonatomic, strong) NSArray *photoArray;
@property (nonatomic, assign) NSInteger currentIndex;

@end
