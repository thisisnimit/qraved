//
//  V2_FoodTagSuggestionView.m
//  Qraved
//
//  Created by harry on 2017/10/3.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_FoodTagSuggestionView.h"

@interface V2_FoodTagSuggestionView ()<TTGTextTagCollectionViewDelegate>{
    TTGTextTagCollectionView *tagCollectionView;

}

@end

@implementation V2_FoodTagSuggestionView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
   
    tagCollectionView = [[TTGTextTagCollectionView alloc] initWithFrame:CGRectMake(0, 4, DeviceWidth, 44)];//
    tagCollectionView.defaultConfig.tagExtraSpace = CGSizeMake(30, 16);
    //
    tagCollectionView.delegate = self;
    tagCollectionView.defaultConfig.tagTextFont = [UIFont systemFontOfSize:14];
    tagCollectionView.scrollDirection = TTGTagCollectionScrollDirectionHorizontal;
    tagCollectionView.alignment = TTGTagCollectionAlignmentLeft;
    tagCollectionView.numberOfLines = 1;
    tagCollectionView.defaultConfig.tagBackgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    tagCollectionView.defaultConfig.tagSelectedBackgroundColor = [UIColor colorWithHexString:@"DE2029"];
    tagCollectionView.defaultConfig.tagTextColor = [UIColor color333333];
    tagCollectionView.defaultConfig.tagSelectedTextColor = [UIColor whiteColor];
    tagCollectionView.defaultConfig.tagBorderWidth = 0;
    tagCollectionView.defaultConfig.tagBorderColor = [UIColor clearColor];
    tagCollectionView.contentInset = UIEdgeInsetsMake(5, 15, 5, 5);
    tagCollectionView.defaultConfig.tagShadowOffset = CGSizeMake(0, 0);
    tagCollectionView.defaultConfig.tagShadowRadius = 0;
    tagCollectionView.defaultConfig.tagShadowOpacity = 0;
    //tagCollectionView.manualCalculateHeight = YES;
    tagCollectionView.defaultConfig.tagCornerRadius = 16;
    tagCollectionView.defaultConfig.tagSelectedCornerRadius = 16;
    [self addSubview:tagCollectionView];
}

- (void)textTagCollectionView:(TTGTextTagCollectionView *)textTagCollectionView didTapTag:(NSString *)tagText atIndex:(NSUInteger)index selected:(BOOL)selected{
    
    for (int i = 0; i < self.foodTagArr.count; i ++) {
        DiscoverCategoriesModel *model = [self.foodTagArr objectAtIndex:i];
        if ([model.myID isEqual:self.selectModel.myID] && [model.name isEqualToString:self.selectModel.name]) {
            [tagCollectionView setTagAtIndex:i selected:NO];
        }
    }
    DiscoverCategoriesModel *model = [self.foodTagArr objectAtIndex:index];
    if (self.selectFoodTag) {
        self.selectFoodTag(model, index);
    }
}

- (void)setFoodTagArr:(NSArray *)foodTagArr{
    _foodTagArr = foodTagArr;
    
    NSMutableArray *titleArr = [NSMutableArray array];
    [tagCollectionView removeAllTags];
    
    for (DiscoverCategoriesModel *model in foodTagArr) {
        
        [titleArr addObject:model.name];
    }
    [tagCollectionView addTags:titleArr];
    
//    for (int i = 0; i < model.districts.count; i ++) {
//        V2_AreaModel *areaModel = [model.districts objectAtIndex:i];
//        if (areaModel.selected) {
//            [tagCollectionView setTagAtIndex:i selected:YES];
//        }
//    }
}

- (void)setSelectModel:(DiscoverCategoriesModel *)selectModel{
    _selectModel = selectModel;
    
    for (int i = 0; i < self.foodTagArr.count; i ++) {
        DiscoverCategoriesModel *model = [self.foodTagArr objectAtIndex:i];
        if ([model.myID isEqual:selectModel.myID] && [model.name isEqualToString:selectModel.name]) {
            [tagCollectionView setTagAtIndex:i selected:YES];
        }
    }

}

@end
