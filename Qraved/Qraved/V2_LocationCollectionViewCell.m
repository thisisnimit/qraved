//
//  V2_LocationCollectionViewCell.m
//  Qraved
//
//  Created by harry on 2017/6/16.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_LocationCollectionViewCell.h"

@implementation V2_LocationCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    self.locationImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 95, 95)];
    self.locationImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.locationImageView.clipsToBounds = YES;

    [self.contentView addSubview:self.locationImageView];
    
    self.lblLocationName = [[UILabel alloc] initWithFrame:CGRectMake(0, self.locationImageView.endPointY + 7, 95, 14)];
    self.lblLocationName.font = [UIFont systemFontOfSize:12];
    self.lblLocationName.textColor = [UIColor color333333];
    self.lblLocationName.textAlignment = NSTextAlignmentLeft;
    self.lblLocationName.lineBreakMode = NSLineBreakByTruncatingTail;
    [self.contentView addSubview:self.lblLocationName];
    
    self.lblLandmarkTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 95, 95)];
    self.lblLandmarkTitle.textAlignment = NSTextAlignmentCenter;
    self.lblLandmarkTitle.textColor = [UIColor color999999];
    self.lblLandmarkTitle.font = [UIFont boldSystemFontOfSize:30];
    [self.contentView addSubview:self.lblLandmarkTitle];
}

- (void)setLocationModel:(V2_LocationModel *)locationModel{
    _locationModel = locationModel;
    
    if ([locationModel.image_url isBlankString]) {
        self.lblLandmarkTitle.text = [locationModel.name substringToIndex:1];
        [self.locationImageView sd_setImageWithURL:[NSURL URLWithString:[locationModel.default_image_url returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
    }else{
        self.lblLandmarkTitle.text = @"";
        [self.locationImageView sd_setImageWithURL:[NSURL URLWithString:[locationModel.image_url returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
    }
    
    self.lblLocationName.text = locationModel.name;
    
}

@end
