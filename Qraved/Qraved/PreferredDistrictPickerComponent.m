//
//  PreferredDistrictPickerComponent.m
//  Qraved
//
//  Created by Tek Yin on 5/4/17.
//  Copyright © 2017 Imaginato. All rights reserved.
//

#import "PreferredDistrictPickerComponent.h"
#import "NExpandableCollectionView.h"
#import "DistrictCell.h"
#import "CollectionViewHeader.h"
#import "DXAlertView.h"
#import "UIColor+Hex.h"
#import "V2_HomeHandler.h"
#import "V2_DistrictModel.h"
#import "HomeService.h"
#define image_padding 15

@implementation PreferredDistrictPickerComponent {
    UIEdgeInsets                     componentMargin;
    CGFloat                          itemHeight;
    CGFloat                          itemWidth;
    //NSMutableArray <DistrictModel *> *selectedDistricts;
    NSMutableArray *selectedArr;
    NSMutableArray<NSIndexPath *>    *ipath;
    NSMutableArray *districtArr;
}


+ (UINavigationController *)create {
    PreferredDistrictPickerComponent *controller = [[NSBundle mainBundle] loadNibNamed:@"PreferredDistrictPickerComponent" owner:self options:nil][0];
    UINavigationController           *navCon     = [[UINavigationController alloc] initWithRootViewController:controller];
    return navCon;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    
    [self requestData];

    [self.uCollection registerNib:[UINib nibWithNibName:@"DistrictCell" bundle:nil] forCellWithReuseIdentifier:@"DistrictCell"];
    [self.uCollection registerNib:[UINib nibWithNibName:@"CollectionViewHeader" bundle:nil] forCellWithReuseIdentifier:@"CollectionViewHeader"];
    [self.uCollection registerNib:[UINib nibWithNibName:NSStringFromClass([CollectionViewHeader class]) bundle:[NSBundle mainBundle]] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([CollectionViewHeader class])];
    self.uCollection.allowsMultipleSelection        = YES;
    self.uCollection.allowsMultipleExpandedSections = YES;
    [self.uCollection setDelegate:self];
    [self.uCollection setDataSource:self];
    ipath = [NSMutableArray new];
    [self loadNavbutton];
    
    districtArr = [NSMutableArray array];
    selectedArr = [NSMutableArray array];
}

- (void)loadNavbutton {
    UIButton *cst = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
    [cst addTarget:self action:@selector(doApply:) forControlEvents:UIControlEventTouchUpInside];
    [cst setTitle:@"Done" forState:UIControlStateNormal];
    [cst setTitleColor:[UIColor colorWithHexString:@"09BFD3"] forState:UIControlStateNormal];
    cst.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    cst.titleLabel.font = [UIFont systemFontOfSize:17];

    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithCustomView:cst];
    [self.navigationItem setRightBarButtonItems:@[btn] animated:NO];


    UIButton *search = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    [search setImage:[UIImage imageNamed:@"ic_close_location"] forState:UIControlStateNormal];
    [search addTarget:self action:@selector(dismiss:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *btn2 = [[UIBarButtonItem alloc] initWithCustomView:search];
    [self.navigationItem setLeftBarButtonItems:@[btn2] animated:NO];

}

- (void)doApply:(id)doApply {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dismiss:(id)dismiss {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)requestData{
    NSDictionary *param = @{@"cityId":@"2"};
    [HomeService getLocation:param andIsPreferred:NO andBlock:^(id locationArr) {
        
        [districtArr addObjectsFromArray:locationArr];
        
        [self.uCollection reloadData];
        
    } andError:^(NSError *error) {
        
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self calculateSize:self.view.bounds];
    self.navigationItem.title = @"Preferred Locations";
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [ipath addObject:indexPath];
    V2_DistrictModel *model = [districtArr objectAtIndex:indexPath.section];
    V2_AreaModel *areaModel = [model.districts objectAtIndex:indexPath.row];
    [selectedArr addObject:areaModel];
    [self.uCollection reloadData];
    for (NSIndexPath *path in ipath) {
        [self.uCollection selectItemAtIndexPath:path animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    }
//    NSLog(@"selected district: %@", selectedDistricts);
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    [ipath removeObject:indexPath];
    V2_DistrictModel *model = [districtArr objectAtIndex:indexPath.section];
    V2_AreaModel *areaModel = [model.districts objectAtIndex:indexPath.row];
    [selectedArr removeObject:areaModel];
    [self.uCollection reloadData];
    for (NSIndexPath *path in ipath) {
        [self.uCollection selectItemAtIndexPath:path animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    }
}

- (void)collectionView:(NExpandableCollectionView *)collectionView didExpandSection:(NSUInteger)section {
//    if (section == 1) {
//        [self.uCollection selectItemAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]
//                                       animated:NO
//                                 scrollPosition:UICollectionViewScrollPositionNone];
//    }
    for (NSIndexPath *path in ipath) {
        if (path.section == section) {
            NSLog(@"select indexPath: %@", path);
            [self.uCollection selectItemAtIndexPath:path animated:NO scrollPosition:UICollectionViewScrollPositionNone];
        }
    }
    NSLog(@"Done selecting");
}

- (void)collectionView:(NExpandableCollectionView *)collectionView didExpandIndexPaths:(NSArray *)indexPathsExpanded collapssIndexPaths:(NSArray *)indexPathsCollapsed {
}


#pragma mark -
#pragma mark UICollectionViewDelegateFlowLayout

- (void)collectionViewHeaderDidTap:(UICollectionReusableView *)collectionViewHeader {
    [self.uCollection toggleSection:collectionViewHeader.tag];
}

- (void)calculateSize:(CGRect)frame {
    componentMargin = UIEdgeInsetsMake(10, 20, 0, 20);
    itemHeight      = 30;
    CGSize  size      = frame.size;
    CGFloat widthLeft = size.width - image_padding - componentMargin.left - componentMargin.right; // 2 item per row, that mean there is 1 padding   0 | 0  and + margin (1 pixel each)
    itemWidth = widthLeft / 2;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(itemWidth, itemHeight);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return componentMargin;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return image_padding;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return image_padding;
}


#pragma mark -
#pragma mark UICollectionViewDataSource

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(nonnull NSString *)kind atIndexPath:(nonnull NSIndexPath *)indexPath {
    if (kind == UICollectionElementKindSectionHeader) {
        

        CollectionViewHeader *headerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:NSStringFromClass([CollectionViewHeader class]) forIndexPath:indexPath];
        headerview.tag      = indexPath.section;
        headerview.delegate = self;
//        if (indexPath.section==0) {
//            headerview.text     = [NSString stringWithFormat:@"%@ (%ld)", @"selected", selectedArr.count];
//        }else{
//            
//        }
        V2_DistrictModel  *model = [districtArr objectAtIndex:indexPath.section];
        headerview.text     = [NSString stringWithFormat:@"%@ (%ld)", model.name, model.districts.count];
        

        return headerview;
    }
    return nil;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return districtArr.count;
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
//    if (section == 0) {
//        return selectedArr.count;
//    } else{
//        
//
//    }
    V2_DistrictModel *model = [districtArr objectAtIndex:section];
    return model.districts.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if (section==0) {
        return CGSizeMake(self.view.width, 54);
    }
    return CGSizeMake(self.view.width, 44);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"DistrictCell";

    DistrictCell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:identifier owner:self options:nil];
        if ([nib count] > 0) {
            cell = (DistrictCell *) nib[0];
        }
    }
    
    
//    if (indexPath.section == 0) {
//        areaModel = [selectedArr objectAtIndex:indexPath.row];
//    } else{
//        V2_DistrictModel *model = [districtArr objectAtIndex:indexPath.section-1] ;
//        areaModel = [model.districts objectAtIndex:indexPath.row];
//    }
    V2_DistrictModel *model = [districtArr objectAtIndex:indexPath.section] ;
    V2_AreaModel *areaModel = [model.districts objectAtIndex:indexPath.row];
    

    cell.uTitle.text = areaModel.name;
    return cell;
}


@end
