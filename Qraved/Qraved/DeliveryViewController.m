//
//  DeliveryViewController.m
//  Qraved
//
//  Created by harry on 2018/3/1.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "DeliveryViewController.h"
#import "JournalDetailViewController.h"
#import "WebViewController.h"
#import "JournalHandler.h"
#import "SearchUtil.h"
#import "V2_JournalCell.h"
#import "UIScrollView+Helper.h"
#import "HomeService.h"
#import "V2_FoodTagSuggestionView.h"
#import "DeliveryBannerModel.h"
#import "V2_DiscoverListViewController.h"
#import "DetailViewController.h"
#import "JournalDetailViewController.h"

@interface DeliveryViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,SDCycleScrollViewDelegate>
{
    int offset;
    int journalMax;
    NSMutableArray *bannerArray;
    NSMutableArray *imageArr;
    NSMutableArray *journalArray;
    NSMutableArray *foodTagArray;
    
    UICollectionView *journalCollectionView;
    IMGMediaComment *journal;
    
    V2_FoodTagSuggestionView *foodTagView;
    
    DiscoverCategoriesModel *foodModel;
    BOOL isUpdateFoodTag;
    NSString *journalHeaderTitle;
    IMGShimmerView *shimmerView;
}
@end

@implementation DeliveryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self loadNavigation];
    [self loadData];
    [self loadMainUI];
    [self requestData];
}

- (void)loadNavigation{
    self.navigationItem.title = @"Delivery";
    UIBarButtonItem *leftBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_back"] style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
    leftBtn.tintColor=[UIColor blackColor];
    self.navigationItem.leftBarButtonItem=leftBtn;
    
    UIButton *btnSearch = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSearch.frame = CGRectMake(DeviceWidth-65, 20, 50, 44);
    btnSearch.imageEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0);
    [btnSearch setImage:[UIImage imageNamed:@"ic_home_search_black"] forState:UIControlStateNormal];
    [btnSearch addTarget:self action:@selector(searchClick) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSearch];
}

- (void)backClick{
    [[Amplitude instance] logEvent:@"CL - Close Delivery Page" withEventProperties:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchClick{
    
    [IMGAmplitudeUtil trackDeliveryWithName:@"SC - Open Search Idle Page" andJournalId:nil andFoodTagId:nil andLocation:nil andOrigin:@"Delivery Page"];
    
    V2_DiscoverListViewController *DiscoverListV = [[V2_DiscoverListViewController alloc] init];
    DiscoverListV.cityName = [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT];
    [self.navigationController pushViewController:DiscoverListV animated:YES];
}

- (void)refreshCity{
    [self getJournalList];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)loadData{
    isUpdateFoodTag = YES;
    offset = 0;
    journalMax = 9;
    imageArr = [NSMutableArray array];
    bannerArray = [NSMutableArray array];
    id bannerJson = [CommonMethod getJsonFromFile:DELIVERY_BANNER_CACHE];
    if (bannerJson) {
        [bannerArray addObjectsFromArray:[CommonMethod getDeliveryBanner:bannerJson]];
    }
    journalArray = [NSMutableArray array];
    id json = [CommonMethod getJsonFromFile:DELIVERY_CACHE];
    if (json) {
        NSArray *arr = [CommonMethod getDeliveryJournalList:json];
        journal = [arr firstObject];
        for (int i = 1; i < arr.count - 1; i ++) {
            [journalArray addObject:[arr objectAtIndex:i]];
        }
    }
    foodTagArray = [NSMutableArray array];
    
    foodModel = [[DiscoverCategoriesModel alloc] init];
    foodModel.myID = @0;
    foodModel.name = @"All";
}

- (void)requestData{
    [self getJournalList];
    [self getBanner];
    [self getFoodTag];
}

- (void)getBanner{
    [HomeService getDeliveryBannerWithBlock:^(NSArray *bannerArr) {
        [bannerArray removeAllObjects];
        [bannerArray addObjectsFromArray:bannerArr];
        [journalCollectionView reloadData];
    } andError:^{
        
    }];
}

- (void)getFoodTag{

    [HomeService getDeliveryFoodTagWithBlock:^(NSArray *tagArr) {
        [foodTagArray removeAllObjects];
        [foodTagArray addObjectsFromArray:tagArr];
        [journalCollectionView reloadData];
    } andError:^{
        
    }];
}

- (void)getJournalList{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
    [params setValue:[NSNumber numberWithInt:journalMax] forKey:@"limit"];
    
    if (foodModel != nil) {
        [params setValue:foodModel.myID forKey:@"foodTagId"];
    }
   
    if (offset == 0) {
        [[LoadingView sharedLoadingView] startLoading];
    }
    
    [HomeService getDeliveryJournal:params andBlock:^(NSArray *journalArr, NSString *journalTitle) {
        [[LoadingView sharedLoadingView] stopLoading];
        [journalCollectionView.mj_header endRefreshing];
        [journalCollectionView.mj_footer endRefreshing];
        if (offset==0) {
            [journalArray removeAllObjects];
            [shimmerView removeFromSuperview];
        }
        if (journalArr.count > 0) {
            journalCollectionView.mj_footer.hidden = NO;
        }else{
            journalCollectionView.mj_footer.hidden = YES;
        }
        
        journalHeaderTitle = journalTitle;
        [journalArray addObjectsFromArray:journalArr];
        if (journalMax == 9 && journalArr.count > 0) {
            journal = [journalArray firstObject];
            [journalArray removeObjectAtIndex:0];
        }else if (journalMax == 9 && journalArr.count == 0){
            journal = nil;
        }
        [journalCollectionView reloadData];
    } andError:^{
        [[LoadingView sharedLoadingView] stopLoading];
        [journalCollectionView.mj_header endRefreshing];
        [journalCollectionView.mj_footer endRefreshing];
    }];
}

- (void)loadMainUI{
    self.view.backgroundColor = [UIColor whiteColor];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 0);
    layout.itemSize = CGSizeMake((DeviceWidth-15)/2, 190);
    
    journalCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+20-IMG_StatusBarAndNavigationBarHeight) collectionViewLayout:layout];
    journalCollectionView.backgroundColor = [UIColor whiteColor];
    journalCollectionView.delegate = self;
    journalCollectionView.dataSource = self;
    [self.view addSubview:journalCollectionView];
    
    journalCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
    
    MJRefreshAutoNormalFooter *footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
    footer.refreshingTitleHidden = YES;
    footer.hidden = YES;
    journalCollectionView.mj_footer = footer;
    
    [journalCollectionView registerClass:[V2_JournalCell class] forCellWithReuseIdentifier:@"journalCell"];
    [journalCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"journalHeader"];
    
    id bannerJson = [CommonMethod getJsonFromFile:DELIVERY_BANNER_CACHE];
    id json = [CommonMethod getJsonFromFile:DELIVERY_CACHE];
    if (!bannerJson && !json) {
        shimmerView = [[IMGShimmerView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, self.view.bounds.size.height)];
        [shimmerView createJournalListShimmerView];
        [self.view addSubview:shimmerView];
    }
}

- (void)refreshData{
    offset = 0;
    [self requestData];
}

- (void)loadMore{
    offset+=journalMax;
    journalMax = 10;
    [self getJournalList];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return journalArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    V2_JournalCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"journalCell" forIndexPath:indexPath];
    IMGMediaComment *mediaComment = [journalArray objectAtIndex:indexPath.row];
    cell.mediaComment = mediaComment;
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    UICollectionReusableView *reusableView = nil;
    if (kind == UICollectionElementKindSectionHeader){
        UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"journalHeader" forIndexPath:indexPath];
        for (UIView *view in header.subviews) {
            [view removeFromSuperview];
        }
        
        SDCycleScrollView *bannerScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceWidth*0.632) delegate:self placeholderImage:[UIImage imageNamed:@"placeholder"]];
        bannerScrollView.showPageControl = YES;
        bannerScrollView.pageDotColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.5];
        bannerScrollView.currentPageDotColor = [UIColor whiteColor];
        bannerScrollView.autoScrollTimeInterval = 4;
        bannerScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleClassic;
        [header addSubview:bannerScrollView];
        
        [imageArr removeAllObjects];
        if (bannerArray.count > 0) {
            bannerScrollView.hidden = NO;
            for (DeliveryBannerModel *modle in bannerArray) {
                [imageArr addObject:modle.image_url];
            }
            bannerScrollView.imageURLStringsGroup = imageArr;
        }else{
            bannerScrollView.hidden = YES;
            bannerScrollView.frame = CGRectMake(0, 0, DeviceWidth, 0);
        }
        
        IMGMediaComment *headerJournal = journal;
  
        if (isUpdateFoodTag) {
            foodTagView = [[V2_FoodTagSuggestionView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 0)];
            if (foodTagArray.count>0) {
                foodTagView.frame = CGRectMake(0, bannerScrollView.endPointY + 5, DeviceWidth, 48);
            }else{
                foodTagView.frame = CGRectMake(0, bannerScrollView.endPointY, DeviceWidth, 0);
            }
            foodTagView.foodTagArr = foodTagArray;
            foodTagView.selectModel = foodModel;
            
            
            __weak typeof(self) weakSelf = self;
            foodTagView.selectFoodTag = ^(DiscoverCategoriesModel *model, NSInteger index) {
                [weakSelf refreshJournalWithFoodTag:model];
                
            };
            [header addSubview:foodTagView];
        }else{
            [header addSubview:foodTagView];
        }
        
        if (headerJournal ==nil) {
            return header;
        }
        
        UILabel *lblJournalTitle = [[UILabel alloc] init];
        lblJournalTitle.font = [UIFont boldSystemFontOfSize:15];
        lblJournalTitle.textColor = [UIColor color333333];
        [header addSubview:lblJournalTitle];
        
        if ([Tools isBlankString:journalHeaderTitle]) {
            lblJournalTitle.frame = CGRectMake(LEFTLEFTSET, foodTagView.endPointY, DeviceWidth - 30, 0);
        }else{
            lblJournalTitle.text = journalHeaderTitle;
            lblJournalTitle.frame = CGRectMake(LEFTLEFTSET, foodTagView.endPointY+15, DeviceWidth - 30, 18);
        }
        
        UIImageView *headImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, lblJournalTitle.endPointY + 10, DeviceWidth-30, DeviceWidth*.35)];
        headImageView.contentMode = UIViewContentModeScaleAspectFill;
        headImageView.clipsToBounds = YES;
        headImageView.userInteractionEnabled = YES;
        [header addSubview:headImageView];
        if ([headerJournal.journalImageUrl hasPrefix:@"http://"]||[headerJournal.journalImageUrl hasPrefix:@"https://"])
        {
            UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-30, (DeviceWidth-30)*.35)];
            [headImageView sd_setImageWithURL:[NSURL URLWithString:headerJournal.journalImageUrl] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            }];
        }
        else
        {
            NSString *urlStr = [headerJournal.journalImageUrl returnFullImageUrlWithWidth:DeviceWidth-30];
            UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-30, (DeviceWidth-30)*.35)];
            
            [headImageView sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            }];
        }
        
        UIImageView *videoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(headImageView.bounds.size.width-42, 15, 27, 19)];
        videoImageView.image = [UIImage imageNamed:@"ic_journal_video.png"];
        [headImageView addSubview:videoImageView];
        if ([headerJournal.hasVideo isEqual:@1]) {
            videoImageView.hidden = NO;
        }else{
            videoImageView.hidden = YES;
        }
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, headImageView.endPointY+5, DeviceWidth-30, 50)];
        lblTitle.font = [UIFont systemFontOfSize:14];
        lblTitle.textColor = [UIColor color333333];
        lblTitle.text = headerJournal.journalTitle;
        lblTitle.numberOfLines = 2;
        [header addSubview:lblTitle];
        
        CGSize size = [headerJournal.journalTitle sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(DeviceWidth-30, 50) lineBreakMode:NSLineBreakByWordWrapping];
        lblTitle.frame = CGRectMake(15, headImageView.endPointY+5, DeviceWidth-30, size.height);
        
        header.backgroundColor = [UIColor whiteColor];
        
        UITapGestureRecognizer *headerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(journalHeaderTapped:)];
        [headImageView addGestureRecognizer:headerTap];
        
        reusableView = header;
    }
    return reusableView;
}

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    DeliveryBannerModel *model = [bannerArray objectAtIndex:index];
    
    if ([model.object_type isEqualToString:@"url"]) {
        if (![Tools isBlankString:model.object_value]) {
            [IMGAmplitudeUtil trackDeliveryBannerWithName:@"CL - Delivery Page Banner" andOrder:[NSString stringWithFormat:@"%ld",index+1] andType:@"url" andTypeValue:model.object_value];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:model.object_value]];
        }
    }else if ([model.object_type isEqualToString:@"jdp"]){
        if (model.object_id != nil) {
            [IMGAmplitudeUtil trackDeliveryBannerWithName:@"CL - Delivery Page Banner" andOrder:[NSString stringWithFormat:@"%ld",index+1] andType:@"jdp" andTypeValue:[NSString stringWithFormat:@"%@",model.object_id]];
            IMGMediaComment *journal = [[IMGMediaComment alloc] init];
            journal.mediaCommentId = model.object_id;
            JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
            jdvc.journal = journal;
            [self.navigationController pushViewController:jdvc animated:YES];
        }
        
    }else if ([model.object_type isEqualToString:@"rdp"]){
        if (model.object_id != nil) {
            [IMGAmplitudeUtil trackDeliveryBannerWithName:@"CL - Delivery Page Banner" andOrder:[NSString stringWithFormat:@"%ld",index+1] andType:@"rdp" andTypeValue:[NSString stringWithFormat:@"%@",model.object_id]];
            DetailViewController *detailViewController = [[DetailViewController alloc] initWithRestaurantId:model.object_id];
            [self.navigationController pushViewController:detailViewController animated:YES];
        }
    }
}

- (void)refreshJournalWithFoodTag:(DiscoverCategoriesModel *)model{
    if ([foodModel.myID isEqual:model.myID] && [foodModel.name isEqual:model.name]) {
        foodModel = nil;
        
    }else{
        foodModel = model;
        foodTagView.selectModel = model;
        [IMGAmplitudeUtil trackDeliveryWithName:@"CL - Food Tag" andJournalId:nil andFoodTagId:model.myID andLocation:@"Delivery Page" andOrigin:nil];
    }
    isUpdateFoodTag = NO;
    offset = 0;
    journalMax = 9;
    
    [self getJournalList];
}

- (void)journalHeaderTapped:(UITapGestureRecognizer *)tap{

    [IMGAmplitudeUtil trackGoFoodWithName:@"RC - View Journal detail" andJournalId:journal.mediaCommentId andRestaurantId:nil andPhotoId:nil andLocation:nil andOrigin:@"Delivery Page" andOrder:@"1"];
    
    JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
    jdvc.amplitudeType = @"Article card on Article list";
    jdvc.journal = journal;
    [self.navigationController pushViewController:jdvc animated:YES];
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{

    CGSize size = [journal.journalTitle sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(DeviceWidth-30, 50) lineBreakMode:NSLineBreakByWordWrapping];
    
    CGFloat bannerHeight = 0.0f;
    if (bannerArray.count > 0) {
        bannerHeight = DeviceWidth*0.632;
    }
    CGFloat journalTitleHeight = 0.0f;
    if (![Tools isBlankString:journalHeaderTitle]) {
        journalTitleHeight = 33;
    }
    CGFloat foodTagHeight = 0.0f;
    if (foodTagArray.count>0) {
        foodTagHeight = 53;
    }
    CGFloat headerJouranlHeight = 0.0f;
    if (journal != nil) {
        headerJouranlHeight = 30 + DeviceWidth*.35+size.height;
    }
    return CGSizeMake(DeviceWidth, headerJouranlHeight + bannerHeight + journalTitleHeight + foodTagHeight);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    IMGMediaComment *journal = [journalArray objectAtIndex:indexPath.row];
    
    [IMGAmplitudeUtil trackGoFoodWithName:@"RC - View Journal detail" andJournalId:journal.mediaCommentId andRestaurantId:nil andPhotoId:nil andLocation:nil andOrigin:@"Delivery Page" andOrder:[NSString stringWithFormat:@"%d",(int)indexPath.row + 2]];
    
    JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
    jdvc.amplitudeType = @"Article card on Article list";
    jdvc.journal = journal;
    [self.navigationController pushViewController:jdvc animated:YES];
}

@end
