//
//  V2_PhotoScrollView.h
//  Qraved
//
//  Created by harry on 2017/7/17.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_InstagramModel.h"
@interface V2_PhotoScrollView : UIView

- (instancetype)initWithFrame:(CGRect)frame withPhotoArray:(NSArray *)photoArray withCurrentIndex:(NSInteger)index;


@property (nonatomic, copy) void(^scrollPhoto)(NSInteger index);
@property (nonatomic, copy) void(^imageTapped)();


@property (nonatomic, copy) void(^panTapped)(UIPanGestureRecognizer *panGesture);
@end
