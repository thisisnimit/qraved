//
//  V2_InstagramViewController.h
//  Qraved
//
//  Created by harry on 2017/6/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "InstagramFilterModel.h"
#import "BrandModel.h"
@interface V2_InstagramViewController : BaseViewController

@property (nonatomic, strong) InstagramFilterModel *selectModel;

@property (nonatomic, strong) BrandModel *model;
@property (nonatomic, assign) BOOL isMall;

@end
