//
//  V2_ArticleModel.m
//  Qraved
//
//  Created by harry on 2017/6/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_ArticleModel.h"

@implementation V2_ArticleModel

-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        self.articleId = value;
    }
}
@end
