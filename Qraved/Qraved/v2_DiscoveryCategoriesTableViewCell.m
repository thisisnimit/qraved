//
//  v2_DiscoveryCategoriesTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/9/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "v2_DiscoveryCategoriesTableViewCell.h"
#import "DiscoverCategories.h"

@interface v2_DiscoveryCategoriesTableViewCell ()<TTGTextTagCollectionViewDelegate>{
    TTGTextTagCollectionView *tagCollectionView;
}

@end

@implementation v2_DiscoveryCategoriesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self createUI];
    }
    return self;
}
- (void)createUI{
    
    tagCollectionView = [[TTGTextTagCollectionView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 44)];//
    tagCollectionView.defaultConfig.tagExtraSpace = CGSizeMake(25, 18);
    tagCollectionView.delegate = self;
    tagCollectionView.defaultConfig.tagTextFont = [UIFont systemFontOfSize:14];
    tagCollectionView.alignment = TTGTagCollectionAlignmentLeft;
    tagCollectionView.defaultConfig.tagBackgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    tagCollectionView.defaultConfig.tagSelectedBackgroundColor = [UIColor colorWithHexString:@"DE2029"];
    tagCollectionView.defaultConfig.tagTextColor = [UIColor color333333];
    tagCollectionView.defaultConfig.tagSelectedTextColor = [UIColor whiteColor];
    tagCollectionView.defaultConfig.tagBorderWidth = 0;
    tagCollectionView.defaultConfig.tagBorderColor = [UIColor clearColor];
    tagCollectionView.contentInset = UIEdgeInsetsMake(5, 15, 5, 5);
    tagCollectionView.defaultConfig.tagShadowOffset = CGSizeMake(0, 0);
    tagCollectionView.defaultConfig.tagShadowRadius = 0;
    tagCollectionView.defaultConfig.tagShadowOpacity = 0;
    tagCollectionView.manualCalculateHeight = YES;
    tagCollectionView.defaultConfig.tagCornerRadius = 16;
    tagCollectionView.defaultConfig.tagSelectedCornerRadius = 16;
    [self.contentView addSubview:tagCollectionView];
    
}
- (void)textTagCollectionView:(TTGTextTagCollectionView *)textTagCollectionView didTapTag:(NSString *)tagText atIndex:(NSUInteger)index selected:(BOOL)selected{
    
    NSLog(@"%@===%ld===%d", tagText, index, selected ? YES : NO);
    DiscoverCategoriesModel *model;
    if (self.selectArr.count>0) {
        model = [self.selectArr objectAtIndex:index];
    }else{
        model = [self.model.childrens objectAtIndex:index];
    }
    
    if (self.gotoDiscoverResultPage) {
        self.gotoDiscoverResultPage(model);
    }
 
}

- (void)setModel:(DiscoverCategories *)model{
    
    _model = model;
     NSMutableArray *titleArr = [NSMutableArray array];
     [tagCollectionView removeAllTags];
    
    for (DiscoverCategoriesModel *areaModel in model.childrens) {
        
        [titleArr addObject:areaModel.name];
    }
    [tagCollectionView addTags:titleArr];
    
    tagCollectionView.frame = CGRectMake(0, 0, DeviceWidth, tagCollectionView.contentSize.height);
    self.pointY = tagCollectionView.contentSize.height;
}

@end
