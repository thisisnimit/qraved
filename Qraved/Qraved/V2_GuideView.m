//
//  V2_GuideView.m
//  Qraved
//
//  Created by harry on 2017/6/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_GuideView.h"
#import "UIColor+Hex.h"
#import "V2_GuideCell.h"
#import "DiningGuideRestaurantsViewController.h"

@interface V2_GuideView ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    UICollectionView *guideCollectionView;
    UILabel *lblTitle;
}

@end
@implementation V2_GuideView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    self.backgroundColor = [UIColor whiteColor];
    
    lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 17, DeviceWidth-90, 16)];
    lblTitle.textColor = [UIColor color333333];
    lblTitle.font = [UIFont boldSystemFontOfSize:14];
    [self addSubview:lblTitle];
    
    UIButton *btnChange = [UIButton buttonWithType:UIButtonTypeCustom];
    btnChange.frame = CGRectMake(15, 0, DeviceWidth-30, 50);
    [btnChange addTarget:self action:@selector(seeAllClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnChange];
    [btnChange setImage:[UIImage imageNamed:@"ic_home_arrow"] forState:UIControlStateNormal];
    btnChange.imageEdgeInsets = UIEdgeInsetsMake(0, DeviceWidth-30-10, 0, 0);
    
    UILabel *lblSeeAll = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth-95, 6, 50, 38)];
    lblSeeAll.text = @"See All";
    lblSeeAll.textAlignment = NSTextAlignmentRight;
    lblSeeAll.textColor = [UIColor color999999];
    lblSeeAll.font =[UIFont systemFontOfSize:12];
    [btnChange addSubview:lblSeeAll];
    
    
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(135, 135);
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 10;
    layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 0);
    
    guideCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,50, DeviceWidth, 135) collectionViewLayout:layout];
    guideCollectionView.backgroundColor = [UIColor whiteColor];
    guideCollectionView.delegate = self;
    guideCollectionView.dataSource = self;
    guideCollectionView.showsHorizontalScrollIndicator = NO;
    [self addSubview:guideCollectionView];
    
    
    
//    [guideCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"guideCell"];
    [guideCollectionView registerNib:[UINib nibWithNibName:@"V2_GuideCell" bundle:nil] forCellWithReuseIdentifier:@"guideCell"];
    
}
- (void)seeAllClick{
    if (self.seeAllGuide != nil) {
        self.seeAllGuide();
    }
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.guideArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    V2_GuideCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"guideCell" forIndexPath:indexPath];
    IMGDiningGuide *model = [self.guideArray objectAtIndex:indexPath.row];
    cell.model = model;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    IMGDiningGuide *model = [self.guideArray objectAtIndex:indexPath.row];
//    IMGDiningGuide *diningGuide = [[IMGDiningGuide alloc] init];
//    diningGuide.diningGuideId = model.guideId;
//    diningGuide.pageContent = model.page_content;
//    diningGuide.pageName = model.header_image_alt_text;
//    diningGuide.pageTitle = model.page_title;
//    diningGuide.pageUrl = model.page_url;
//    diningGuide.headerImage = model.header_image;
    if (self.gotoDiningGuideDetail != nil) {
         self.gotoDiningGuideDetail(model);
    }
   
//    DiningGuideRestaurantsViewController *diningResList = [[DiningGuideRestaurantsViewController alloc] initWithDiningGuide:diningGuide];
//    [self.navigationController pushViewController:diningResList animated:YES];
}

- (void)setGuideArray:(NSArray *)guideArray{
    _guideArray = guideArray;
    [self setupAutoHeightWithBottomView:guideCollectionView bottomMargin:20];
    [guideCollectionView reloadData];
}
- (void)setName:(NSString *)name{
    _name = name;
    lblTitle.text = name;
}
@end
