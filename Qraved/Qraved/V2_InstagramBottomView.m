//
//  V2_InstagramBottomView.m
//  Qraved
//
//  Created by harry on 2017/7/31.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_InstagramBottomView.h"
#import "DLStarRatingControl.h"
#import <MapKit/MapKit.h>
//#import <UberRides/UberRides-Swift.h>
#import "MapViewController.h"
#import "InstagramPlacesViewController.h"
#import "V2_PhotoSectionView.h"
#import "DetailSeeMenuViewController.h"
#import "DetailSeeAllPhotoViewController.h"
#import "AlbumViewController.h"
#import "V2_PhotoDetailViewController.h"
#import "IMGMenu.h"
#import "MenuPhotoViewColltroller.h"
#import "MenuPhotoGalleryController.h"

@interface V2_InstagramBottomView ()<MKMapViewDelegate>{
    UIImageView *topArrow;
    UIImageView *imgRestaurant;
    UILabel *lblTitle;
    UILabel *lblCuisine;
    UIButton *btnSaved;
    
    UIView *restaurantView;
    UILabel *lblRestaurantTitle;
    DLStarRatingControl *ratingControl;
    UILabel *lblDistance;
    UILabel *lblInformation;
    UILabel *lblTime;
    UILabel *lblState;
    UIView *mapView;
    MKMapView *map;
    UIButton *btnPlaces;
    UIView *currentView;
    V2_PhotoSectionView *instagramView;
    V2_PhotoSectionView *menuView;
    V2_PhotoSectionView *resView;
    V2_PhotoSectionView *userView;
    MKPointAnnotation *pointAnnatation;
    FunctionButton *goFoodButton;
}

@end

@implementation V2_InstagramBottomView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)extracted {
    self.gotoMiniRDP();
}

- (void)createUI{
    
    topArrow = [[UIImageView alloc] initWithFrame:CGRectMake((DeviceWidth-13)/2, 0, 13, 8)];
    topArrow.image = [UIImage imageNamed:@"ic_arrow_up_white.png"];
    [self addSubview:topArrow];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 15, DeviceWidth, 64)];
    bgView.backgroundColor = [UIColor colorWithHexString:@"000000" alpha:0.1];

    [self addSubview:bgView];
    bgView.userInteractionEnabled = YES;
    [bgView bk_whenTapped:^{
        if (self.gotoMiniRDP) {
            [self extracted];
        }
    }];
    
    imgRestaurant = [[UIImageView alloc] initWithFrame:CGRectMake(15, 8, 48, 48)];
    imgRestaurant.contentMode = UIViewContentModeScaleAspectFill;
    imgRestaurant.clipsToBounds = YES;
    [bgView addSubview:imgRestaurant];
    
    lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(imgRestaurant.endPointX+10, 8, DeviceWidth - 125, 22)];
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.font = [UIFont systemFontOfSize:14];
    [bgView addSubview:lblTitle];
    
    lblCuisine = [[UILabel alloc] initWithFrame:CGRectMake(imgRestaurant.endPointX+10, lblTitle.endPointY, DeviceWidth - 125, 20)];
    lblCuisine.textColor = [UIColor whiteColor];
    lblCuisine.font = [UIFont systemFontOfSize:14];
    [bgView addSubview:lblCuisine];
    
    UIView *navView = [[UIView alloc] initWithFrame:CGRectMake(0, bgView.endPointY, DeviceWidth, 64)];
    navView.backgroundColor = [UIColor colorWithHexString:@"000000"];
    [self addSubview:navView];
    
    btnSaved = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSaved.frame = CGRectMake(DeviceWidth-40, 25, 40, 34);
    [btnSaved addTarget:self action:@selector(savedClick:) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:btnSaved];
    
    UIButton *btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
    btnClose.frame = CGRectMake(15, 31, 30, 30);
    btnClose.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 8, 8);
    [btnClose setImage:[UIImage imageNamed:@"ic_close_photo"] forState:UIControlStateNormal];
    [btnClose addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:btnClose];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 79+64, DeviceWidth, DeviceHeight+20-64)];
    view.backgroundColor = [UIColor whiteColor];
    [self addSubview:view];
    
    restaurantView = [[UIView alloc] initWithFrame:CGRectMake(0, navView.endPointY, DeviceWidth, 120)];
    restaurantView.backgroundColor = [UIColor whiteColor];
    [self addSubview:restaurantView];
    restaurantView.userInteractionEnabled = YES;
    
    [restaurantView bk_whenTapped:^{
        if (self.gotoRDP) {
            self.gotoRDP();
        }
    }];
    
    UIImageView *restaurantArrow = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-25, 20, 10, 17)];
    restaurantArrow.image = [UIImage imageNamed:@"ic_MiniRDP_arrow"];
    [restaurantView addSubview:restaurantArrow];
    
    lblRestaurantTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, DeviceWidth-50, 29)];
    lblRestaurantTitle.font = [UIFont systemFontOfSize:24];
    lblRestaurantTitle.textColor = [UIColor color333333];
    [restaurantView addSubview:lblRestaurantTitle];
    
    ratingControl = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(15, lblRestaurantTitle.endPointY+3, 73, 14) andStars:5 andStarWidth:13 isFractional:YES spaceWidth:2];
    ratingControl.userInteractionEnabled = NO;
    [restaurantView addSubview:ratingControl];
    
    lblDistance = [[UILabel alloc] initWithFrame:CGRectMake(ratingControl.endPointX+5, lblRestaurantTitle.endPointY+3, 100, 14)];
    lblDistance.font = [UIFont systemFontOfSize:12];
    lblDistance.textColor = [UIColor color999999];
    [restaurantView addSubview:lblDistance];
    
    lblInformation = [[UILabel alloc] initWithFrame:CGRectMake(15, lblDistance.endPointY+7, DeviceWidth-30, 14)];
    lblInformation.font = [UIFont systemFontOfSize:12];
    lblInformation.textColor = [UIColor color999999];
    [restaurantView addSubview:lblInformation];
    
    lblTime = [[UILabel alloc] initWithFrame:CGRectMake(15, lblInformation.endPointY+5, DeviceWidth-30-60, 14)];
    lblTime.font = [UIFont systemFontOfSize:12];
    lblTime.textColor = [UIColor color999999];
    [restaurantView addSubview:lblTime];
    
    lblState = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth-15-60, lblInformation.endPointY+3, 60, 14)];
    lblState.font = [UIFont systemFontOfSize:12];
    lblState.textColor = [UIColor colorWithHexString:@"19A574"];
    [restaurantView addSubview:lblState];
    
    goFoodButton = [FunctionButton buttonWithType:UIButtonTypeCustom];
    goFoodButton.frame = CGRectMake(LEFTLEFTSET, restaurantView.endPointY, DeviceWidth - LEFTLEFTSET * 2, FUNCTION_BUTTON_HEIGHT);
    goFoodButton.hidden = YES;
    [self addSubview:goFoodButton];
    
    mapView=[[UIView alloc]init];
    mapView.backgroundColor = [UIColor whiteColor];
    [self addSubview:mapView];
    map=[[MKMapView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 115)];
    [map setMapType:MKMapTypeStandard];
    [map setDelegate:self];
    [mapView addSubview:map];
    
    [mapView setFrame:CGRectMake(0,goFoodButton.endPointY + 15, DeviceWidth, 115)];
    
    UIButton *mapBackground=[UIButton buttonWithType:UIButtonTypeCustom];
    [mapBackground setFrame:CGRectMake(0, 0, DeviceWidth, mapView.frame.size.height)];
    [mapBackground addTarget:self action:@selector(gotoMap) forControlEvents:UIControlEventTouchUpInside];
    [mapView addSubview:mapBackground];
    
    btnPlaces = [UIButton buttonWithType:UIButtonTypeCustom];
    btnPlaces.frame = CGRectMake(0, mapView.endPointY, DeviceWidth, 58);
    btnPlaces.backgroundColor = [UIColor whiteColor];
    [btnPlaces addTarget:self action:@selector(instagramClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnPlaces];
    
    
    
    UIImageView *instagramImage = [[UIImageView alloc]initWithFrame:CGRectMake(15, 16, 24, 24)];
    instagramImage.image = [UIImage imageNamed:@"instagram"];
    [btnPlaces addSubview:instagramImage];
    
    UILabel *lblInstagram = [[UILabel alloc] initWithFrame:CGRectMake(instagramImage.endPointX+20, 21, 150, 16)];
    lblInstagram.font = [UIFont systemFontOfSize:14];
    lblInstagram.textColor = [UIColor color333333];
    lblInstagram.text = @"Instagram Places";
    [btnPlaces addSubview:lblInstagram];
    
    UIImageView *imgArrow = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-15-8, 22, 8, 14)];
    imgArrow.image = [UIImage imageNamed:@"ic_instagram_arrow"];
    [btnPlaces addSubview:imgArrow];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 57.5, DeviceWidth, 0.5)];
    lineView.backgroundColor = [UIColor colorCCCCCC];
    [btnPlaces addSubview:lineView];
}

- (void)setRestaurant:(IMGRestaurant *)restaurant{
    _restaurant = restaurant;
    
    UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(48,48)];
    NSString *urlString = [_restaurant.imageUrl returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution];
    __weak typeof(imgRestaurant) weakRestaurantImageView = imgRestaurant;
    [imgRestaurant sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (!image && error) {
            weakRestaurantImageView.image = placeHoderImage;
        }else{
            image = [image imageByScalingAndCroppingForSize:CGSizeMake(48, 48)];
            weakRestaurantImageView.image = image;
        }
        
    }];

    
    lblTitle.text = restaurant.title;
    
    NSString *locationStr = [Tools isBlankString:restaurant.restaurant_landmark] ? restaurant.districtName : restaurant.restaurant_landmark;
    lblCuisine.text = [NSString stringWithFormat:@"%@ • %@",restaurant.cuisineName,locationStr];
    
    if ([restaurant.saved isEqual:@1]) {
        [btnSaved setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
    }else{
        [btnSaved setImage:[UIImage imageNamed:@"ic_home_heart"] forState:UIControlStateNormal];
    }
    if (![restaurant.ratingScore isKindOfClass:[NSNull class]]) {
        float score = [restaurant.ratingScore floatValue];
        int scoreInt =(int)roundf(score);
        if (scoreInt/2.0-scoreInt/2>0) {
            [ratingControl updateRating:[NSNumber numberWithFloat:scoreInt]];
            
        }else{
            [ratingControl updateRating:[NSNumber numberWithFloat:scoreInt]];
        }

    }
    
    lblRestaurantTitle.text = restaurant.title;
    NSString *distance = [restaurant distance];
    NSString *ratingStr = @"";
    if (restaurant.reviewCount != nil && ![restaurant.reviewCount isEqual:@0]) {
        ratingStr = [NSString stringWithFormat:@"(%@) •",restaurant.reviewCount];
    }
    
    if (![distance isEqualToString:@""]) {
        lblDistance.text = [NSString stringWithFormat:@"%@ %@",ratingStr, distance];
    }else{
        lblDistance.text = ratingStr;
    }
    
    if ([restaurant.cuisineName isEqualToString:@""]) {
        lblInformation.text = [NSString stringWithFormat:@"%@ • %@",locationStr,restaurant.priceName];
    }else{
        lblInformation.text = [NSString stringWithFormat:@"%@ • %@ • %@",restaurant.cuisineName,locationStr,restaurant.priceName];
    }
    
    if (restaurant.displayTime != nil) {
        lblTime.text = [NSString stringWithFormat:@"Hours %@",restaurant.displayTime];
    }
    
    if (([restaurant.state isEqual:@1] || [restaurant.state isEqual:@6]) && [restaurant.openNow isEqual:@1]) {
        lblState.text = @"Open Now";
    }else{
        lblState.text = @"Closed";
        lblState.textColor = [UIColor color999999];
    }
    
    if (![Tools isBlankString:restaurant.goFoodLink]) {
        goFoodButton.hidden = NO;
        [goFoodButton addTarget:self action:@selector(goFoodTapped) forControlEvents:UIControlEventTouchUpInside];
        [mapView setFrame:CGRectMake(0,goFoodButton.endPointY + 15, DeviceWidth, 115)];
        
    }else{
        goFoodButton.hidden = YES;
        mapView.frame = CGRectMake(0,restaurantView.endPointY, DeviceWidth, 115);
    }
//    btnPlaces.frame = CGRectMake(0, mapView.endPointY, DeviceWidth, 58);
    
    CLLocationCoordinate2D coor;
    coor.latitude = [restaurant.latitude doubleValue];
    coor.longitude = [restaurant.longitude doubleValue];
    CLLocationCoordinate2D theCoordinate =CLLocationCoordinate2DMake(coor.latitude, coor.longitude);
    MKCoordinateSpan  theSpan;
    theSpan.latitudeDelta=0.01;
    theSpan.longitudeDelta=0.01;
    MKCoordinateRegion theRegion;
    theRegion.center=theCoordinate;
    theRegion.span=theSpan;
    MKPointAnnotation *pointAnnatation= [[MKPointAnnotation alloc] init];
    if(CLLocationCoordinate2DIsValid(theCoordinate))
    {
        [map setRegion:theRegion animated:NO];
        
        [map regionThatFits:theRegion];
        
        pointAnnatation.coordinate=theCoordinate;
        
        [map addAnnotation:pointAnnatation];
    }
}

- (void)goFoodTapped{
    [IMGAmplitudeUtil trackGoFoodWithName:@"CL - Go-Food CTA" andJournalId:nil andRestaurantId:self.restaurant.restaurantId andPhotoId:nil andLocation:@"Mini RDP" andOrigin:nil andOrder:nil];

    [CommonMethod goFoodButtonTapped:self.restaurant andViewController:self.controller andRestaurantState:[self.restaurant isOpenRestaurant] andCallBackBlock:^(BOOL isSaved) {
        [self freshSavedButton:isSaved];
    }];
}

- (void)setLocationId:(NSNumber *)locationId{
    _locationId = locationId;
    
    if ([locationId isEqual:[NSNull null]] || locationId==nil) {
        btnPlaces.frame = CGRectMake(0, mapView.endPointY, DeviceWidth, 0);
        self.currentPointY = mapView.endPointY;
    }else{
        btnPlaces.frame = CGRectMake(0, mapView.endPointY, DeviceWidth, 58);
        self.currentPointY = btnPlaces.endPointY;
    }
}

- (void)setPhotoDictionary:(NSDictionary *)photoDictionary{
    _photoDictionary = photoDictionary;
    currentView = btnPlaces;
    if (instagramView) {
        [instagramView removeFromSuperview];
    }
    if (userView) {
        [userView removeFromSuperview];
    }
    if (resView) {
        [resView removeFromSuperview];
    }
    if (menuView) {
        [menuView removeFromSuperview];
    }
    if ([[photoDictionary objectForKey:@"photos_instagram"] count]>0) {
        instagramView = [[V2_PhotoSectionView alloc] initWithFrame:CGRectMake(0, currentView.endPointY, DeviceWidth, 184) WithTitle:@"Instagram Photos"];
        instagramView.photoArr = [photoDictionary objectForKey:@"photos_instagram"];
        __weak typeof(self) weakSelf = self;
        instagramView.seeAll = ^(){
            DetailSeeAllPhotoViewController *DetailSeeAllPhotoV = [[DetailSeeAllPhotoViewController alloc] initWithRestaurant:weakSelf.restaurant restaurantPhotoArray:nil];
            DetailSeeAllPhotoV.isUseWebP = YES;
            DetailSeeAllPhotoV.currentIndex = 2;
            [weakSelf.controller pushViewController:DetailSeeAllPhotoV animated:YES];
        };
        
        instagramView.photoClick = ^(NSArray *photoArr,NSInteger index){
            V2_PhotoDetailViewController *photoDetail = [[V2_PhotoDetailViewController alloc] init];
            photoDetail.restaurant = weakSelf.restaurant;
            photoDetail.currentInstagram = [photoArr objectAtIndex:index];
            photoDetail.isFromMiniRDP = YES;
            photoDetail.photoArray = photoArr;
            photoDetail.currentIndex = index;
            [weakSelf.controller pushViewController:photoDetail animated:YES];
        };
        [self addSubview:instagramView];
        currentView = instagramView;
    }
    if ([[photoDictionary objectForKey:@"photos_user"] count]>0){
        userView = [[V2_PhotoSectionView alloc] initWithFrame:CGRectMake(0, currentView.endPointY, DeviceWidth, 184) WithTitle:@"User Photos"];
        userView.photoArr = [photoDictionary objectForKey:@"photos_user"];
        __weak typeof(self) weakSelf = self;
        userView.seeAll = ^(){
            DetailSeeAllPhotoViewController *DetailSeeAllPhotoV = [[DetailSeeAllPhotoViewController alloc] initWithRestaurant:weakSelf.restaurant restaurantPhotoArray:nil];
            DetailSeeAllPhotoV.isUseWebP = YES;
            DetailSeeAllPhotoV.currentIndex = 1;
            [weakSelf.controller pushViewController:DetailSeeAllPhotoV animated:YES];
        };
        
        userView.photoClick = ^(NSArray *photoArr,NSInteger index){
            
            AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:photoArr andPhotoTag:index andRestaurant:weakSelf.restaurant andFromDetail:NO];
            //    [albumViewController updateRestautantDetailData:moreDishDataArr];
            albumViewController.title = weakSelf.restaurant.title;
            albumViewController.showPage=NO;
            albumViewController.restaurantId = weakSelf.restaurant.restaurantId;
            albumViewController.amplitudeType = @"Restaurant detail page - ";
            albumViewController.isUseWebP = YES;
            [weakSelf.controller pushViewController:albumViewController animated:YES];
            
        };
        [self addSubview:userView];
        currentView = userView;
    }
    if ([[photoDictionary objectForKey:@"photos_restaurant"] count]>0){
        resView = [[V2_PhotoSectionView alloc] initWithFrame:CGRectMake(0, currentView.endPointY, DeviceWidth, 184) WithTitle:@"Restaurant Photos"];
        resView.photoArr = [photoDictionary objectForKey:@"photos_restaurant"];
        __weak typeof(self) weakSelf = self;
        resView.seeAll = ^(){
            DetailSeeAllPhotoViewController *DetailSeeAllPhotoV = [[DetailSeeAllPhotoViewController alloc] initWithRestaurant:weakSelf.restaurant restaurantPhotoArray:nil];
            DetailSeeAllPhotoV.isUseWebP = YES;
            DetailSeeAllPhotoV.currentIndex = 0;
            [weakSelf.controller pushViewController:DetailSeeAllPhotoV animated:YES];
        };
        
        resView.photoClick = ^(NSArray *photoArr,NSInteger index){
            
            AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:photoArr andPhotoTag:index andRestaurant:weakSelf.restaurant andFromDetail:NO];
            //    [albumViewController updateRestautantDetailData:moreDishDataArr];
            albumViewController.title = weakSelf.restaurant.title;
            albumViewController.showPage=NO;
            albumViewController.restaurantId = weakSelf.restaurant.restaurantId;
            albumViewController.amplitudeType = @"Restaurant detail page - ";
            albumViewController.isUseWebP = YES;
            [weakSelf.controller pushViewController:albumViewController animated:YES];
            
        };
        [self addSubview:resView];
        currentView = resView;
    }
    if ([[photoDictionary objectForKey:@"photos_menu"] count]>0){
        menuView = [[V2_PhotoSectionView alloc] initWithFrame:CGRectMake(0, currentView.endPointY, DeviceWidth, 184) WithTitle:@"Menu Photos"];
        menuView.photoArr = [photoDictionary objectForKey:@"photos_menu"];
        __weak typeof(self) weakSelf = self;
        menuView.seeAll = ^(){
//            IMGMenu *menu_ = [menuPhotoArray firstObject];
//            BOOL isSelfUpload = [menu_.type intValue]==0;
            MenuPhotoGalleryController *menuVC = [[MenuPhotoGalleryController alloc] initWithRestaurant:weakSelf.restaurant andIsSelfUpload:NO];
            menuVC.isUseWebP = YES;
            [weakSelf.controller pushViewController:menuVC animated:YES];
        };
        
        menuView.photoClick = ^(NSArray *photoArr, NSInteger index){
        
            IMGMenu *menu=[photoArr objectAtIndex:index];
            MenuPhotoViewColltroller *menuPhotoViewColltroller = [[MenuPhotoViewColltroller alloc]initWithPhotosArray:photoArr andPhotoTag:index andRestaurant:weakSelf.restaurant andFromDetail:NO];
            menuPhotoViewColltroller.menuPhotoCount = [NSNumber numberWithInteger:photoArr.count];
            menuPhotoViewColltroller.amplitudeType = @"Restaurant detail page - menu list";
            menuPhotoViewColltroller.isUseWebP = YES;
            if ([menu.type intValue] == 0){
                menuPhotoViewColltroller.ifUnauditedData = YES;
            }else{
                [menuPhotoViewColltroller updateRestautantDetailData];
            }
            [weakSelf.controller pushViewController:menuPhotoViewColltroller animated:YES];
            
        };
        [self addSubview:menuView];
        currentView = menuView;
    }
    
    self.currentPointY = currentView.endPointY;
    
}

#pragma mark - MapViewDelegate
- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation{
    MKAnnotationView *pinView = nil;
    static NSString *defaultPinID = @"imaginato_map";
    pinView = (MKAnnotationView *)[theMapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    if ( pinView == nil ) pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
    pinView.canShowCallout = YES;
    pinView.image = [[UIImage imageNamed:@"ic_red_pin.png"] imageByScalingAndCroppingForSize:CGSizeMake(27, 33)];
    return pinView;
}

-(void)gotoMap{
    
    [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Map" andRestaurantId:self.restaurant.restaurantId andRestaurantTitle:nil andLocation:@"Restaurant detail page" andOrigin:nil andType:nil];
    
    IMGUser *user = [IMGUser currentUser];
    NSNumber *userId = [[NSNumber alloc] init];
    if (user.userId != nil)
    {
        userId = user.userId;
    }
    else
        userId = [NSNumber numberWithInt:0];
//    [TrackHandler trackWithUserId:userId andRestaurantId:restaurant.restaurantId andContent:[NSDictionary dictionaryWithObject:@"1" forKey:@"type"]];
    
    MapViewController *mapViewController=[[MapViewController alloc]initWithRestaurant:self.restaurant];
    mapViewController.amplitudeType=@"Restaurant detail page";
    mapViewController.restaurantId=self.restaurant.restaurantId;
    mapViewController.fromDetail = YES;
    [self.controller pushViewController:mapViewController animated:YES];
}

- (void)instagramClick:(UIButton *)button{
    InstagramPlacesViewController *InstagramPlacesVC = [[InstagramPlacesViewController alloc] init];
    InstagramPlacesVC.instagramLocationId = [NSString stringWithFormat:@"%@",self.locationId];
    [self.controller pushViewController:InstagramPlacesVC animated:YES];
}

- (void)savedClick:(UIButton *)button{
    if (self.saveRestaurant) {
        self.saveRestaurant(self.restaurant);
    }
}


- (void)freshSavedButton:(BOOL)liked{
    
    if (liked) {
        self.restaurant.saved = @1;
        [btnSaved setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
    }else{
        self.restaurant.saved = @0;
        [btnSaved setImage:[UIImage imageNamed:@"ic_home_heart"] forState:UIControlStateNormal];
    }
}

- (void)close{
    if (self.closeMiniRDP) {
        self.closeMiniRDP();
    }
}

@end
