//
//  ReviewPublishHandler.h
//  Qraved
//
//  Created by apple on 17/4/5.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGUserReview.h"
@interface ReviewPublishHandler : NSObject
//+(void)getSearchAllCategryDataFromeService:(NSDictionary*)dic andSuccessBlock:(void(^)(NSArray* cusinearr,NSArray* landmark,NSArray* district,NSArray* dininguide))successBlock anderrorBlock:(void(^)( NSString *str))errorBlock;

+(void)uploadDishWithPrama:(NSMutableDictionary*)param andSuccessBlock:(void(^)(NSDictionary* dic,NSString *returnStatusString,NSString *exceptionMsg,NSString *photoId))successBlock anderrorBlock:(void(^)( NSString *str))errorBlock;
+(void)deleteDishWithParams:(NSDictionary*)parameters andBlock:(void(^)())block;
+(void)dashboardWithParams:(NSDictionary*)parameters;
+(void)getReviewWithParams:(NSDictionary*)parameters andBlock:(void(^)(NSDictionary *dict))block;
+(void)getLastReviewWithParams:(NSDictionary*)parameters andBlock:(void(^)(NSDictionary *dict))block;
+(void)editdescriptionWithParams:(NSMutableDictionary*)dict;
+(void)updateReviewWithParams:(NSDictionary*)parameters andBlock:(void(^)(NSDictionary *dic,NSString *exceptionMsg,NSString *ratingMap))block anderrorBlock:(void(^)( NSString *str))errorBlock;
+(void)writereviewWithParams:(NSDictionary*)parameters andBlock:(void(^)(NSString *returnStatusString,NSString *exceptionMsg,NSString *userInteractCount,NSNumber *returnReviewId,NSDictionary *reviewDictionary,NSString* ratingMap))block anderrorBlock:(void(^)( NSString *str))errorBlock;
@end
