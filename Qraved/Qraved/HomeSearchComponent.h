//
//  HomeSearchComponent.h
//  Qraved
//
//  Created by Tek Yin on 5/9/17.
//  Copyright © 2017 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

IB_DESIGNABLE
@interface HomeSearchComponent : UIView

@property(weak, nonatomic) IBOutlet UILabel *uSearchText;
@property(nonatomic) BOOL                   isExpanded;
@property(nonatomic) CGRect                 originalFrame;

+ (HomeSearchComponent *)create;

- (void)shrink;

- (void)expand;

- (void)initialFrame:(CGRect)rect;
@end
