//
//  V2_DiscoverResultViewController.m
//  Qraved
//
//  Created by harry on 2017/10/2.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverResultViewController.h"
#import "LoadingView.h"
#import "SearchUtil.h"
#import "HomeService.h"
#import "V2_DiscoverListResultRestaurantTableViewCell.h"
#import "DetailViewController.h"
#import "SearchEmptyResultView.h"
#import "V2_DiscoverListViewController.h"
#import "V2_DistrictView.h"
#import "V2_DistrictModel.h"
#import "V2_DiscoverListResultFiltersViewController.h"
#import "MapViewController.h"
#import "RestaurantSuggestViewController.h"
#import "V2_FoodTagSuggestionView.h"
#import "DiscoverCategories.h"
#import "MapUtil.h"
#import "V2_DiscoverListHandler.h"
#import "HeaderForJournalView.h"
#import "V2_DiscoverListJournalSeeAllViewController.h"
#import "IMGMediaComment.h"
#import "JournalDetailViewController.h"

@interface V2_DiscoverResultViewController ()<UITableViewDelegate, UITableViewDataSource,V2_DiscoverListResultRestaurantTableViewCellDelegate,SearchEmptyResultViewDelegate,FiltersViewControllerDelegate,SearchEmptyResultViewDelegate,UIAlertViewDelegate>
{
    int offset;
    UIButton *btnSearch;
    UIButton *cityButton;
    NSMutableArray *restaurantArr;
    NSMutableArray *districtArr;
    V2_DistrictView *districtView;
    NSString *currentDistrict;
    NSString *currentLandMark;
    NSString *currentArea;
    NSMutableArray *foodTagArr;
    NSMutableArray *journalArr;
    
    V2_DiscoverListResultRestaurantTableViewCell *currentCell;
    
    //notification send
    NSString *notificationcuisines;
    NSString *notificationfeature;
    NSString *notificationprice;
    NSString *notificationrating;
    NSString *notificationsortby;
    
    UIButton *mapBtn;
    UIView *btnView;
    MKMapView *mapView;
    MapViewController *mapViewController;
    UIButton *navigateButton;
    V2_FoodTagSuggestionView *foodTagView;
    DiscoverCategoriesModel *selectFoodTag;
    SearchEmptyResultView *headerView;
    BOOL isUpdateFoodTag;
    BOOL isCheckLocation;
    BOOL isSearchThisArea;
    
    NSString *offsStr;
    NSString *offersStr;
    FilterType filterType;
    IMGShimmerView *shimmerView;
}

@property (nonatomic, strong)  UITableView  *tableView;
@end

@implementation V2_DiscoverResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadData];
    [self loadMainUI];
    [self loadEmptyTableVIewHeadView];
    [self reuquestFoodTagSuggestion];
    [self requestJournal];
    [self bottomBtn];
    [self requestSelecteCityViewData];
    [self getOffers];
    
    [self addBackBtn];
    [self loadNavbutton];
    [self initMap];
}

- (void)loadData{
    offset = 0;
    isUpdateFoodTag = YES;
    restaurantArr = [NSMutableArray array];
    districtArr = [NSMutableArray array];
    foodTagArr = [NSMutableArray array];
    journalArr = [NSMutableArray array];
    
    if (self.foodModel != nil && ![self.foodModel.name isEqualToString:@"All"]) {
        selectFoodTag = self.foodModel;
    }
}

- (void)loadMainUI{
    self.view.backgroundColor = [UIColor whiteColor];
   
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth,DeviceHeight-44) style:UITableViewStyleGrouped];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    self.tableView.estimatedRowHeight = 0;
    self.tableView.estimatedSectionFooterHeight = 0;
    self.tableView.estimatedSectionHeaderHeight = 0;
   
    if (@available(iOS 11.0, *)) {
        //self.tableView.contentInset = UIEdgeInsetsMake(-35, 0, 0, 0);
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    
    if (!self.isLocation) {
        shimmerView = [[IMGShimmerView alloc] initWithFrame:self.view.bounds];
        [shimmerView createRestaurantShimmerView];
        [self.view addSubview:shimmerView];
    }
}

#pragma marm - map
- (void)initMap{
    
    mapViewController=[[MapViewController alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-44) andSearchThisArea:YES];
    mapViewController.fromSearchPage=YES;
    mapViewController.fromSaved = YES;
    mapViewController.restaurantsViewController=self;
    
    
    __weak typeof(self) weakSelf = self;
    mapViewController.searchThisArea = ^{
        offset=0;
        isSearchThisArea = YES;
        [weakSelf requestGetRestaurant:nil cuisines:nil feature:nil price:nil rating:nil sortby:nil loadMore:NO];
    };
    
    mapView = mapViewController.mapView;
    mapView.frame = CGRectMake(0,0, DeviceWidth, self.view.frame.size.height);
    mapView.hidden = YES;
    [self.view addSubview:mapViewController.mapView];
    
    //     [self bottomBtn];
    
    navigateButton = [[UIButton alloc] initWithFrame:CGRectMake(DeviceWidth-50, 10, 35, 35)];
    
    navigateButton.backgroundColor = [UIColor clearColor];
    [navigateButton addTarget:self action:@selector(nearYouBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navigateButton setImage:[UIImage imageNamed:@"ic_located"] forState:UIControlStateNormal];
    
    [mapView addSubview:navigateButton];
    [self.view bringSubviewToFront:mapBtn];
}
- (void)nearYouBtnClick
{
    CLLocationCoordinate2D center;
    
    center.latitude=[AppDelegate ShareApp].locationManager.location.coordinate.latitude;
    center.longitude = [AppDelegate ShareApp].locationManager.location.coordinate.longitude;
    CLLocationCoordinate2D theCoordinate =CLLocationCoordinate2DMake(center.latitude, center.longitude);
    MKCoordinateSpan span;
    span.latitudeDelta=0.03;
    span.longitudeDelta=0.005;
    MKCoordinateRegion theRegion;
    theRegion.center=theCoordinate;
    theRegion.span=span;
    [mapView setRegion:theRegion animated:NO];
    
    [mapView regionThatFits:theRegion];
    mapView.showsUserLocation = YES;
    
}

- (void)bottomBtn{
    
    btnView =[[UIView alloc] init];
    btnView.backgroundColor = [UIColor whiteColor];
    //    [self.view addSubview:btnView];
    //    [self.view bringSubviewToFront:btnView];
    [[AppDelegate ShareApp].window addSubview:btnView];
    
    btnView.sd_layout
    .bottomSpaceToView([AppDelegate ShareApp].window , 20)
    .centerXEqualToView([AppDelegate ShareApp].window)
    .heightIs(36)
    .widthIs(184);
    btnView.layer.borderColor = [UIColor colorCCCCCC].CGColor;
    btnView.layer.borderWidth = 1;
    btnView.layer.cornerRadius = 18;
    btnView.layer.masksToBounds = YES;
    
    mapBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [mapBtn setTitle:@"MAP" forState:UIControlStateNormal];
    [mapBtn setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    mapBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [mapBtn setImage:[UIImage imageNamed:@"ic_map_small"] forState:UIControlStateNormal];
    [btnView addSubview:mapBtn];
    mapBtn.backgroundColor = [UIColor whiteColor];
    [mapBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 0)];
    [mapBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -40, 0, 0)];
    [mapBtn addTarget:self action:@selector(btnMapClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnFilters = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnFilters setTitle:@"FILTERS" forState:UIControlStateNormal];
    [btnFilters setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    [btnFilters setImage:[UIImage imageNamed:@"ic_tune_small"] forState:UIControlStateNormal];
    btnFilters.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [btnFilters addTarget:self action:@selector(btnFiltersClick:) forControlEvents:UIControlEventTouchUpInside];
    btnFilters.backgroundColor = [UIColor whiteColor];
    [btnView addSubview:btnFilters];
    btnFilters.backgroundColor = [UIColor whiteColor];
    [btnFilters setImageEdgeInsets:UIEdgeInsetsMake(0, 70, 0, 0)];
    [btnFilters setTitleEdgeInsets:UIEdgeInsetsMake(0, -40, 0, 0)];
    [btnFilters addTarget:self action:@selector(btnFiltersClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *lineview= [[UIView alloc] init];
    lineview.backgroundColor = [UIColor colorCCCCCC];
    [btnView addSubview:lineview];
    
    mapBtn.sd_layout
    .topSpaceToView(btnView, 0)
    .leftSpaceToView(btnView, 0)
    .heightIs(36)
    .widthIs(85);
    
    btnFilters.sd_layout
    .topEqualToView(mapBtn)
    .leftSpaceToView(mapBtn, 0)
    .heightIs(36)
    .widthIs(99);
    
    lineview.sd_layout
    .topSpaceToView(btnView, 5)
    .bottomSpaceToView(btnView, 5)
    .widthIs(1)
    .leftSpaceToView(mapBtn, 0);
    //    .centerXEqualToView(btnView);
    
}

- (void)btnMapClick:(UIButton *)btnMap{
    NSLog(@"map");
    mapBtn.selected = !mapBtn.selected;
    if (mapBtn.selected) {
        //        wantToGoTab.hidden = YES;
        btnView.sd_resetLayout
        .bottomSpaceToView([AppDelegate ShareApp].window , 165)
        .centerXEqualToView([AppDelegate ShareApp].window)
        .heightIs(36)
        .widthIs(184);
        self.tableView.hidden = YES;
        mapView.hidden = NO;
        [mapBtn setTitle:@"LIST" forState:UIControlStateNormal];
        [mapBtn setImage:[UIImage imageNamed:@"ic_list_view"] forState:UIControlStateNormal];
        [mapBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 0)];
        [mapBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -40, 0, 0)];
    }else{
        
        //        wantToGoTab.hidden = NO;
        btnView.sd_resetLayout
        .bottomSpaceToView([AppDelegate ShareApp].window , 20)
        .centerXEqualToView([AppDelegate ShareApp].window)
        .heightIs(36)
        .widthIs(184);
        self.tableView.hidden = NO;
        mapView.hidden = YES;
        [mapBtn setTitle:@"MAP" forState:UIControlStateNormal];
        [mapBtn setImage:[UIImage imageNamed:@"ic_map_small"] forState:UIControlStateNormal];
        [mapBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 0)];
        [mapBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];
        
    }
}
- (void)btnFiltersClick:(UIButton *)btnFilters{
    
    NSLog(@"btnFilters");
    V2_DiscoverListResultFiltersViewController *V2_DiscoverListResultFiltersVC = [[V2_DiscoverListResultFiltersViewController alloc] initWithDiscover:nil];
    V2_DiscoverListResultFiltersVC.fvcDelegate = self;
    [self.navigationController pushViewController:V2_DiscoverListResultFiltersVC animated:YES];
    
}


- (void)loadMoreData{
    
    NSLog(@"%@==%@==%@==%@==%@",notificationcuisines,notificationfeature,notificationprice,notificationrating,notificationsortby);
    
    [self requestGetRestaurant:nil cuisines:notificationcuisines ? notificationcuisines: @"" feature:notificationfeature ? notificationfeature: @"" price:notificationprice ? notificationprice: @"" rating:notificationrating ? notificationrating: @"" sortby:notificationsortby ? notificationsortby: @"" loadMore:YES];
}

#pragma mark - btn city
- (void)loadNavbutton{
    
    cityButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT] == nil || [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] == nil) {
    }else{
        [cityButton setTitle:[[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT] capitalizedString] forState:UIControlStateNormal];
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"currentCityId"] != nil && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied){
        [cityButton setTitle:NEARBYTITLE forState:UIControlStateNormal];
    }
    
    if (self.homeLocationName != nil) {
        [cityButton setTitle:self.homeLocationName forState:UIControlStateNormal];
    }
    
    if (self.isHomeNearBy) {
        [cityButton setTitle:NEARBYTITLE forState:UIControlStateNormal];
    }
    
//    cityButton.enabled  = NO;
    [cityButton setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    [cityButton setImage:[UIImage imageNamed:@"ic_city_down"] forState:UIControlStateNormal];
    cityButton.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    [cityButton addTarget:self action:@selector(doSelectCity:) forControlEvents:UIControlEventTouchUpInside];
    cityButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 15);
    [cityButton.layer setAffineTransform:CGAffineTransformMakeScale(-1, 1)];
    [cityButton.titleLabel.layer setAffineTransform:CGAffineTransformMakeScale(-1, 1)];
    
    CGSize titleSize = [cityButton.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17]}];
    
    cityButton.height = 35;
    cityButton.width= titleSize.width + 50;
    
    cityButton.layer.masksToBounds = YES;
    cityButton.layer.cornerRadius = 18;
    cityButton.backgroundColor = [UIColor colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1];;
    
    cityButton.x = (self.view.width - cityButton.width)/2;
    cityButton.y = 4;
    
    btnSearch = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSearch.frame = CGRectMake(DeviceWidth-65, 20, 50, 44);
    btnSearch.imageEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0);
    [btnSearch setImage:[UIImage imageNamed:@"ic_home_search_black"] forState:UIControlStateNormal];
    [btnSearch addTarget:self action:@selector(searchClick) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSearch];
}

- (void)doSelectCity:(UIButton *)btn{
    if (districtView) {
        [districtView removeFromSuperview];
    }
    districtView = [[V2_DistrictView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+20)];
    
    if (districtArr.count==0) {
        districtView.districtArr = self.locationArr;
    }else{
        districtView.districtArr = districtArr;
    }
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
        [districtView changeLocationSkip];
    }
    
    __weak typeof(self) weakSelf = self;
    districtView.selectedDistrict = ^(id model){
        
        if ([model isKindOfClass:[V2_AreaModel class]]) {
            [weakSelf filterByArea:model];
        }else if ([model isKindOfClass:[V2_DistrictModel class]]){
            [weakSelf filterByDistrict:model];
        }
    };
    
    districtView.nearbyCheck = ^{
        if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied) {
            isCheckLocation = YES;
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"To continue, let your device turn on location" delegate:weakSelf cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
            [alert show];
            
        }else{
            [weakSelf refreshInstagram:NEARBYTITLE];
        }
    };
    
    
    districtView.alpha = 0;
    [[AppDelegate ShareApp].window addSubview:districtView];
    [UIView animateWithDuration:0.3 animations:^{
        districtView.alpha = 1;
    }];
    
}

- (void)filterByArea:(V2_AreaModel *)model{
    [self resetSelectArea];
    if ([model.type isEqualToString:@"landmark"]) {
        currentLandMark = [NSString stringWithFormat:@"%@",model.districtId];
    }else{
        currentDistrict = [NSString stringWithFormat:@"%@",model.districtId];
    }
    
    [IMGAmplitudeUtil trackDiscoverWithName:@"SC - Filter Location Succeed" andFoodName:nil andLocationName:model.name andLocation:nil];
    
    [self refreshInstagram:model.name];
    isUpdateFoodTag = YES;
    [self reuquestFoodTagSuggestion];
}

- (void)resetSelectArea{
    isSearchThisArea = NO;
    currentLandMark = @"";
    currentDistrict = @"";
    currentArea = @"";
}

- (void)filterByDistrict:(V2_DistrictModel *)model{
    [self resetSelectArea];
    
    [IMGAmplitudeUtil trackDiscoverWithName:@"SC - Filter Location Succeed" andFoodName:nil andLocationName:model.name andLocation:nil];
    
    currentArea = [NSString stringWithFormat:@"%@",model.districtId];
    [self refreshInstagram:model.name];
}

- (void)refreshInstagram:(NSString *)name{
    
    [cityButton setTitle:name forState:UIControlStateNormal];
    CGSize titleSize = [cityButton.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17]}];
    cityButton.height = 35;
    cityButton.width=titleSize.width+50;
    cityButton.y = 4;
    cityButton.x = (self.view.width - cityButton.width) / 2;
    
    offset=0;
    [self requestGetRestaurant:nil cuisines:nil feature:nil price:nil rating:nil sortby:nil loadMore:NO];

}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
        }
    }
}

- (void)searchClick{
    V2_DiscoverListViewController *DiscoverListV = [[V2_DiscoverListViewController alloc] init];
    DiscoverListV.cityName = cityButton.titleLabel.text;
    [self.navigationController pushViewController:DiscoverListV animated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar addSubview:cityButton];
    [self.navigationController.navigationBar layoutIfNeeded];
    [[AppDelegate ShareApp].window addSubview:btnView];
    
    if (isCheckLocation && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
        isCheckLocation = NO;
        [self refreshInstagram:NEARBYTITLE];
    }
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [cityButton removeFromSuperview];
    [btnView removeFromSuperview];
}

- (void)requestSelecteCityViewData{
    
    if (self.isLocation) {
        [self doSelectCity:cityButton];
    }
    
    [HomeService getLocation:nil andIsPreferred:NO andBlock:^(id locationArr) {
        
//        cityButton.enabled  = YES;
//        districtView.districtArr = locationArr;
        [districtArr addObjectsFromArray:locationArr];
        
    } andError:^(NSError *error) {
        
    }];
}

- (void)reuquestFoodTagSuggestion{
  
    NSMutableDictionary *pamars = [[NSMutableDictionary alloc] init];
    
    if (self.foodModel != nil && [self.foodModel.name isEqualToString:@"All"]) {
        [pamars setValue:self.foodModel.myID forKey:@"footTagId"];
    }
    
    if (selectFoodTag != nil) {
        [pamars setValue:selectFoodTag.myID forKey:@"footTagId"];
    }
    
    if (self.homeLandmarkId != nil) {
        [pamars setValue:[NSString stringWithFormat:@"%@",self.homeLandmarkId] forKey:@"landmarkIds"];
    }
    
    if (self.homeDistrictId != nil) {
        [pamars setValue:[NSString stringWithFormat:@"%@",self.homeDistrictId] forKey:@"districtIds"];
    }
    
    if (![Tools isBlankString:currentDistrict]) {
        [pamars setValue:currentDistrict  forKey:@"districtIds"];
    }
    
    if (![Tools isBlankString:currentLandMark]) {
        [pamars setValue:currentLandMark  forKey:@"landmarkIds"];
    }
    
    [HomeService getFoodTag:pamars andBlock:^(id locationArr) {
        [foodTagArr removeAllObjects];
        if (selectFoodTag !=nil && [locationArr count]>0) {
            [foodTagArr insertObject:selectFoodTag atIndex:0];
            for (DiscoverCategoriesModel *model in locationArr) {
                if ([model.myID isEqual:selectFoodTag.myID] && [model.name isEqualToString:selectFoodTag.name]) {
                }else{
                    [foodTagArr addObject:model];
                }
            }
        }else if ([locationArr count]>0){
            [foodTagArr addObjectsFromArray:locationArr];
        }
       
    } andError:^(NSError *error) {
        
    }];
    
}

- (void)requestJournal{

    NSMutableDictionary *pamars = [[NSMutableDictionary alloc] init];
    
    if (self.foodModel != nil && [self.foodModel.name isEqualToString:@"All"]) {
        [pamars setValue:self.foodModel.myID forKey:@"foodtagId"];
    }
    
    if (self.quicklySearchFoodTagId != nil) {
        [pamars setValue:self.quicklySearchFoodTagId forKey:@"foodtagId"];
    }
    
    if (selectFoodTag != nil) {
        [pamars setValue:selectFoodTag.myID forKey:@"foodtagId"];
    }
    [V2_DiscoverListHandler getgetTrendingJournal:pamars andSuccessBlock:^(V2_DiscoverListResultRestaurantTableViewCellModel * model) {
        [journalArr removeAllObjects];
        if (model) {
           // _V2_DiscoverListResultRestaurantModel = model;
            
            [journalArr addObjectsFromArray:model.data];
        }
        
        [self requestGetRestaurant:nil cuisines:nil feature:nil price:nil rating:nil sortby:nil loadMore:NO];
    } anderrorBlock:^{
        [self requestGetRestaurant:nil cuisines:nil feature:nil price:nil rating:nil sortby:nil loadMore:NO];
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

#pragma amrk - requestGetRestaurant
- (void)requestGetRestaurant:(NSString *)districtId cuisines:(NSString *)cuisines feature:(NSString *)feature price:(NSString *)price rating:(NSString *)rating sortby:(NSString *)sortby loadMore:(BOOL)isLoadMore{
    
//    IMGUser * user = [IMGUser currentUser];
    NSNumber *cityID = [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    NSString *latitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"];
    NSString *longitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"];
    
    CLLocationCoordinate2D center;
    center.latitude=[AppDelegate ShareApp].locationManager.location.coordinate.latitude;
    center.longitude = [AppDelegate ShareApp].locationManager.location.coordinate.longitude;
    
    
    NSMutableDictionary *pamars = [[NSMutableDictionary alloc] init];
    
    if (self.quicklySearchFoodTagId != nil) {
        [pamars setValue:self.quicklySearchFoodTagId forKey:@"foodTagIds"];
    }
    
    if ([Tools isBlankString:sortby]) {
        [pamars setValue:@"popularity" forKey:@"sortby"];
    }else{
        [pamars setValue:sortby forKey:@"sortby"];
        if ([sortby isEqualToString:@"distance"]) {
            [pamars setValue:@"asc" forKey:@"order"];
        }
    }
    
    [pamars setValue:cityID ? cityID : @1 forKey:@"cityID"];
    [pamars setValue:latitude forKey:@"latitude"];
    [pamars setValue:longitude forKey:@"longitude"];
    [pamars setValue:[NSString stringWithFormat:@"%d",offset] forKey:@"offset"];
    [pamars setValue:@"10" forKey:@"max"];
    
    if (self.homeLandmarkId != nil) {
        [pamars setValue:[NSString stringWithFormat:@"%@",self.homeLandmarkId] forKey:@"landmarkIDs"];
    }
    
    if (self.homeDistrictId != nil) {
        [pamars setValue:[NSString stringWithFormat:@"%@",self.homeDistrictId] forKey:@"districtIDs"];
    }
    
    if (![Tools isBlankString:currentDistrict]) {
        [pamars setValue:currentDistrict  forKey:@"districtIDs"];
    }
    
    if (![Tools isBlankString:currentLandMark]) {
        [pamars setValue:currentLandMark  forKey:@"landmarkIDs"];
    }
    
    if (![Tools isBlankString:currentArea]) {
        [pamars setValue:currentArea  forKey:@"areaIDs"];
    }
    
    if (![Tools isBlankString:districtId]) {
        [pamars setValue:districtId forKey:@"districtIDs"];
    }
    
    if (![Tools isBlankString:cuisines]) {
        
        [pamars setValue:cuisines forKey:@"cuisineIDs"];
    }
    if (![Tools isBlankString:feature]) {
        
        [pamars setValue:feature forKey:@"tagIDs"];
        
    }
    if (![Tools isBlankString:price]) {
        
        [pamars setValue:price forKey:@"price"];
    }
    if (![Tools isBlankString:rating]) {
        
        [pamars setValue:rating forKey:@"rating"];
    }
    
    if (self.foodModel != nil && [self.foodModel.name isEqualToString:@"All"]) {
        [pamars setValue:self.foodModel.myID forKey:@"foodTagIds"];
    }
    
    if (selectFoodTag != nil) {
        [pamars setValue:selectFoodTag.myID forKey:@"foodTagIds"];
    }

    NSString *nearByStr = [cityButton titleForState:UIControlStateNormal];
    if ([nearByStr isEqualToString:NEARBYTITLE]) {
        [pamars setValue:@"nearby" forKey:@"specialLandmark"];
    }
    
    if (filterType == FilterTypePromo) {
        [pamars setValue:offsStr forKey:@"offs"];
        [pamars setValue:offersStr forKey:@"offerTypes"];
    }else if (filterType == FilterTypeOpenNow){
        [pamars setValue:@"1" forKey:@"openNowToggle"];
    }else if (filterType == FilterTypeSaved){
        [pamars setValue:@"1" forKey:@"myListToggle"];
    }
    
    if (isSearchThisArea) {
        double deltaLatitude = mapViewController.mapView.region.span.latitudeDelta;
        double deltaLongitude = mapViewController.mapView.region.span.longitudeDelta;
        
        double centerLatitude = mapViewController.mapView.region.center.latitude;
        double centerLongitude = mapViewController.mapView.region.center.longitude;
        
        NSNumber *minLatitudeTemp = [NSNumber numberWithDouble:[MapUtil minLatitude:centerLatitude deltaLatitude:deltaLatitude]];
        NSNumber *maxLatitudeTemp = [NSNumber numberWithDouble:[MapUtil maxLatitude:centerLatitude deltaLatitude:deltaLatitude]];
        NSNumber *minLongitudeTemp = [NSNumber numberWithDouble:[MapUtil minLongitude:centerLongitude deltaLongitude:deltaLongitude]];
        NSNumber *maxLongitudeTemp = [NSNumber numberWithDouble:[MapUtil maxLongitude:centerLongitude deltaLongitude:deltaLongitude]];
        
        [pamars setValue:minLatitudeTemp forKey:@"minLatitude"];
        [pamars setValue:maxLatitudeTemp forKey:@"maxLatitude"];
        [pamars setValue:minLongitudeTemp forKey:@"minLongitude"];
        [pamars setValue:maxLongitudeTemp forKey:@"maxLongitude"];
    }
    
    
    [[LoadingView sharedLoadingView]startLoading];
    [SearchUtil getSearchResault:pamars andSuccessBlock:^(NSMutableArray *array, NSNumber *restaurantCount) {
        [[LoadingView sharedLoadingView]stopLoading];
        [self.tableView.mj_footer endRefreshing];
        if (isLoadMore != YES) {
            [restaurantArr removeAllObjects];
            [shimmerView removeFromSuperview];
        }
        
        if ([array isKindOfClass:[NSMutableArray class]]) {
            if (array.count>0) {
                offset += 10;
            }
            [restaurantArr addObjectsFromArray:array];
            
            [mapViewController setMapDatas:restaurantArr offset:0];
        }
        
        if (restaurantArr.count==0 && journalArr.count == 0) {
            headerView.hidden = NO;
        }else{
            headerView.hidden = YES;
        }

        [self.tableView reloadData];
    } anderrorBlock:^{
        [self.tableView.mj_footer endRefreshing];
        [shimmerView removeFromSuperview];
        [[LoadingView sharedLoadingView]stopLoading];
    }];
}

-(void)loadEmptyTableVIewHeadView{
//    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
//    if ( (discover.keyword != nil) && (![@"" isEqualToString:discover.keyword] ) ) {
//        [eventProperties setValue:discover.keyword forKey:@"keyword"];
//    }
//    [[Amplitude instance] logEvent:@"RC - View Empty Result on Search" withEventProperties:eventProperties];
    headerView = [[SearchEmptyResultView alloc]initWithFrame:CGRectMake(0, 70, DeviceWidth, DeviceHeight-44)];
    headerView.delegate = self;
    headerView.hidden = YES;
    //    [searchResultsTableView setTableHeaderView:headerView];
    [self.view addSubview:headerView];
    //    [self initDiscovers];
    
}

#pragma mark - tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return restaurantArr.count;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//
//    return 0.001;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//
//    return  0.0001;
//}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    IMGRestaurant *model =  restaurantArr[indexPath.row];
    return [self.tableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[V2_DiscoverListResultRestaurantTableViewCell class] contentViewWidth:DeviceWidth];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
    V2_DiscoverListResultRestaurantTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[V2_DiscoverListResultRestaurantTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellStyleDefault;
        cell.Delegate = self;
    }
    cell.model = restaurantArr[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    IMGRestaurant *model = [restaurantArr objectAtIndex:indexPath.row];
    NSLog(@"%@",model.restaurantId);
    
    DetailViewController *DetailVC = [[DetailViewController alloc] initWithRestaurant:model];
    DetailVC.amplitudeType = @"New SRP";
    [self.navigationController pushViewController:DetailVC animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (journalArr.count>0) {
        return 278+5;
    }else{
        return 48;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    if (isUpdateFoodTag) {
        foodTagView = [[V2_FoodTagSuggestionView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 48)];
        foodTagView.backgroundColor = [UIColor whiteColor];
       
        foodTagView.foodTagArr = foodTagArr;
        foodTagView.selectModel = selectFoodTag;
        __weak typeof(self) weakSelf = self;
        __weak typeof(foodTagView) weakFoodTagView = foodTagView;
        foodTagView.selectFoodTag = ^(DiscoverCategoriesModel *model, NSInteger index) {
            if ([selectFoodTag.myID isEqual:model.myID] && [selectFoodTag.name isEqualToString:model.name]) {
                selectFoodTag = nil;
                offset=0;
                isUpdateFoodTag = NO;
            }else{
                
                [IMGAmplitudeUtil trackDiscoverWithName:@"SC - Filter Food Succeed" andFoodName:model.name andLocationName:nil andLocation:@"New SRP"];
                
                selectFoodTag = model;
                offset=0;
                isUpdateFoodTag = NO;
                weakFoodTagView.selectModel = model;
            }
            [weakSelf requestJournal];
        };
        [view addSubview:foodTagView];
    }else{
        [view addSubview:foodTagView];
    }

    if (journalArr.count>0) {
        HeaderForJournalView *journalView = [[HeaderForJournalView alloc] initWithFrame:CGRectMake(0, foodTagView.endPointY, DeviceWidth, 230)];
        journalView.journalArray = journalArr;
        view.frame = CGRectMake(0, 0, DeviceWidth, 278);
        __weak typeof(self) weakSelf = self;
        journalView.seeAllJournal = ^{
            [weakSelf gotoJournalList];
        };
        journalView.gotoJDP = ^(V2_DiscoverListResultRestaurantTableViewCellSubModel *model) {
            [weakSelf gotoJournalDetail:model];
        };
        
        [view addSubview:journalView];
    }else{
        view.frame = CGRectMake(0, 0, DeviceWidth, 48);
    }
    
    return view;
}

- (void)gotoJournalDetail:(V2_DiscoverListResultRestaurantTableViewCellSubModel *)model{
    IMGMediaComment *journal = [[IMGMediaComment alloc] init];
    journal.mediaCommentId = [NSNumber numberWithInt:[model.myID intValue]];
    
    JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
    jdvc.journal = journal;

    [self.navigationController pushViewController:jdvc animated:YES];
}

- (void)gotoJournalList{
    V2_DiscoverListJournalSeeAllViewController *journalList = [[V2_DiscoverListJournalSeeAllViewController alloc] init];
//    if (self.keywordString != nil) {
//        V2_DiscoverListJournalSeeAllVC.keyword = self.keywordString;
//    }else if (self.placeholderString != nil){
//        V2_DiscoverListJournalSeeAllVC.keyword = self.placeholderString;
//    }
    journalList.foodTagArr = foodTagArr;
    journalList.foodModel = selectFoodTag;
    journalList.quicklySearchFoodTagId = self.quicklySearchFoodTagId;
    journalList.keyword = @"";
    [self.navigationController pushViewController:journalList animated:YES];
}

- (void)delegateLikeBtn:(UITableViewCell *)cell andRestaurant:(IMGRestaurant *)restaurant{
    currentCell = (V2_DiscoverListResultRestaurantTableViewCell *)cell;
    [CommonMethod doSaveWithRestaurant:restaurant andViewController:self andCallBackBlock:^(BOOL isSaved) {
        [currentCell refreshSavedButton:isSaved];
    }];
}

-(void)search:(NSNotification*)notification
{
    NSLog(@"=====%@",notification.userInfo);
    notificationcuisines = notification.userInfo[@"cuisines"];
    notificationfeature  = notification.userInfo[@"feature"];
    notificationprice = notification.userInfo[@"price"];
    notificationrating = notification.userInfo[@"rating"];
    notificationsortby = notification.userInfo[@"sortby"];
    filterType = [notification.userInfo[@"availablity"] integerValue];
    offset=0;
    isSearchThisArea = NO;
    NSLog(@"%@==%@==%@==%@==%@",notificationcuisines,notificationfeature,notificationprice,notificationrating,notificationsortby);
    [self requestGetRestaurant:nil cuisines:notification.userInfo[@"cuisines"] feature:notification.userInfo[@"feature"] price:notification.userInfo[@"price"] rating:notification.userInfo[@"rating"] sortby:notification.userInfo[@"sortby"] loadMore:NO];
    
}

- (void)gotoSuggest
{
    NSLog(@"gotoSuggest");
    RestaurantSuggestViewController *rsvc = [[RestaurantSuggestViewController alloc] init];
    rsvc.amplitudeType = @"Restaurant search result page";
    [self.navigationController pushViewController:rsvc animated:YES];
}

- (void)getOffers{
    [SearchUtil getOffers:^(NSString *offs, NSString *offers) {
        offsStr = offs;
        offersStr = offers;
    } anderrorBlock:^{
        offsStr = @"";
        offersStr = @"";
    }];
}


@end
