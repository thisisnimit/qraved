//
//  HeaderForJournalView.h
//  Qraved
//
//  Created by harry on 2017/12/18.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_DiscoverListResultRestaurantTableViewCellModel.h"
@interface HeaderForJournalView : UIView

@property (nonatomic, strong) NSArray *journalArray;
@property (nonatomic, copy) void(^seeAllJournal)();
@property (nonatomic, copy) void(^gotoJDP)(V2_DiscoverListResultRestaurantTableViewCellSubModel *model);

@end
