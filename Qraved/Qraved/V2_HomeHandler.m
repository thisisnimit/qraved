//
//  V2_HomeHandler.m
//  Qraved
//
//  Created by harry on 2017/6/14.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_HomeHandler.h"
#import "V2_CityModel.h"
#import "V2_InstagramModel.h"
#import "V2_LocationModel.h"
#import "V2_QuickSearchModel.h"
#import "V2_PersonalizedModel.h"
#import "V2_GuideModel.h"
#import "V2_ArticleModel.h"
#import "V2_RestaurantModel.h"
#import "V2_DistrictModel.h"
@implementation V2_HomeHandler
+(void)getCities:(void (^)(id object))successBlock andError:(void (^)(NSError *))errorBlock{
    //Location/city
    [[IMGNetWork sharedManager] homeGetRequest:@"Location/city" withBody:nil withSuccessBlock:^(id responseObject) {
        NSMutableArray *cityArray = [NSMutableArray array];
        NSArray *cityArr = [responseObject objectForKey:@"data"];
        for (NSDictionary *dic in cityArr) {
            V2_CityModel *model = [[V2_CityModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [cityArray addObject:model];
        }
        successBlock(cityArray);
        
    } withFailurBlock:^(NSError *error) {
        
    }];
}

+ (void)getHomeDataWithParam:(NSDictionary *)params andBlock:(void(^)(id instagram,id location,id quickSearch,id personalized))successBlock andError:(void(^)(NSError *error))errorBlock{
    //Homepage/all
    [[IMGNetWork sharedManager] GET:@"Homepage/all" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSDictionary *dic = [responseObject objectForKey:@"data"];
        NSMutableArray *instagramArr = [NSMutableArray array];
        NSMutableArray *locationArr = [NSMutableArray array];
        NSMutableArray *quickSearchArr = [NSMutableArray array];
        NSMutableArray *personalizedArr = [NSMutableArray array];
        for (NSDictionary *instagramDic in [dic objectForKey:@"instagram"]) {
            V2_InstagramModel *model = [[V2_InstagramModel alloc] init];
            [model setValuesForKeysWithDictionary:instagramDic];
            [instagramArr addObject:model];
        }
        
        for (NSDictionary *locationDic in [dic objectForKey:@"location"]) {
            V2_LocationModel *model = [[V2_LocationModel alloc] init];
            [model setValuesForKeysWithDictionary:locationDic];
            [locationArr addObject:model];
        }
        for (NSDictionary *quickSearchDic in [dic objectForKey:@"quickSearch"]) {
            V2_QuickSearchModel *model = [[V2_QuickSearchModel alloc] init];
            [model setValuesForKeysWithDictionary:quickSearchDic];
            [quickSearchArr addObject:model];
        }
        for (NSDictionary *personalizedDic in [dic objectForKey:@"personalized"]) {
            NSMutableArray *tempArr = [NSMutableArray array];
            V2_PersonalizedModel *model = [[V2_PersonalizedModel alloc] init];
            //[model setValuesForKeysWithDictionary:personalizedDic];
            
            model.type = [personalizedDic objectForKey:@"type"];
            model.name = [personalizedDic objectForKey:@"name"];
            if ([[personalizedDic objectForKey:@"type"] isEqual:@1]) {
                for (NSDictionary *guideDic in [personalizedDic objectForKey:@"data"]) {
                    V2_GuideModel *model = [[V2_GuideModel alloc] init];
                    [model setValuesForKeysWithDictionary:guideDic];
                    [tempArr addObject:model];
                }
                
                model.data  = tempArr;
                
            }else if ([[personalizedDic objectForKey:@"type"] isEqual:@2])
            {
                for (NSDictionary *guideDic in [personalizedDic objectForKey:@"data"]) {
                    V2_ArticleModel *model = [[V2_ArticleModel alloc] init];
                    [model setValuesForKeysWithDictionary:guideDic];
                    [tempArr addObject:model];
                }
                
                model.data  = tempArr;
            }else if ([[personalizedDic objectForKey:@"type"] isEqual:@3]){
                for (NSDictionary *guideDic in [personalizedDic objectForKey:@"data"]) {
                    V2_RestaurantModel *model = [[V2_RestaurantModel alloc] init];
                    [model setValuesForKeysWithDictionary:guideDic];
                    [tempArr addObject:model];
                }
                
                model.data  = tempArr;
            }
            [personalizedArr addObject:model];
           
        }
        
        successBlock(instagramArr,locationArr,quickSearchArr,personalizedArr);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock(error);
    }];
}

+(void)getInstagramPhotos:(NSDictionary *)params andBlock:(void(^)(id instagramPhotos))successBlock andError:(void(^)(NSError *error))errorBlock{
    [[IMGNetWork sharedManager] GET:@"Instagram/photos" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
    
        NSMutableArray *instagramArr = [NSMutableArray array];
        for (NSDictionary *instagramDic in [responseObject objectForKey:@"data"]) {
            V2_InstagramModel *model = [[V2_InstagramModel alloc] init];
            [model setValuesForKeysWithDictionary:instagramDic];
            [instagramArr addObject:model];
        }
        successBlock(instagramArr);
        
        NSLog(@"%@",responseObject)
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

+(void)getLocation:(NSDictionary *)params andBlock:(void(^)(id locationArr))successBlock andError:(void(^)(NSError *error))errorBlock{
    [[IMGNetWork sharedManager] GET:@"Location/area/district" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSMutableArray *districtArr = [NSMutableArray array];
        for (NSDictionary *dic in [responseObject objectForKey:@"data"]) {
            V2_DistrictModel *model = [[V2_DistrictModel alloc] init];
            //[model setValuesForKeysWithDictionary:dic];
            model.name = [dic objectForKey:@"name"];
            model.districtId = [dic objectForKey:@"id"];
            NSMutableArray *locationArr = [NSMutableArray array];
            for (NSDictionary *locationDic in [dic objectForKey:@"districts"]) {
                V2_AreaModel *model = [[V2_AreaModel alloc] init];
                [model setValuesForKeysWithDictionary:locationDic];
                [locationArr addObject:model];
                
            }
            
            model.districts = locationArr;
            [districtArr addObject:model];
        }
        
        successBlock(districtArr);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

@end
