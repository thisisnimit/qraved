//
//  V2_ArticleCell.h
//  Qraved
//
//  Created by harry on 2017/6/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGMediaComment.h"
#import "V2_DiscoverListResultRestaurantTableViewCellModel.h"
@interface V2_ArticleCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *articleImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *videoImageView;


@property (copy, nonatomic) IMGMediaComment *model;
@property (assign, nonatomic) BOOL isBrand;
@property (copy, nonatomic) V2_DiscoverListResultRestaurantTableViewCellSubModel *journalModel;

@end
