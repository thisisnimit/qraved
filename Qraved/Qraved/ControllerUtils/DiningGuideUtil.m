//
//  DiningGuideUtil.m
//  Qraved
//
//  Created by Laura on 14/12/23.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "DiningGuideUtil.h"
#import "DBManager.h"
#import "IMGRestaurantDiningGuide.h"
#import "IMGDiningGuide.h"
#import "IMGRestaurant.h"
#import "RestaurantHandler.h"
#import "AppDelegate.h"

@implementation DiningGuideUtil

+(void)getDiningGuideListWithCityId:(NSNumber *)cityId andMax:(NSNumber *)max andOffset:(NSNumber *)offset andBlock:(void (^)(NSArray *dataArray,BOOL hasData))block andFailure:(void(^)(NSString *description))failureBlock
{
    NSDictionary * parameters = [[NSDictionary alloc] init];

    parameters = @{@"cityId":cityId,@"max":max,@"offset":offset};
    __block int skip= [max intValue] + [offset intValue];

    [[IMGNetWork sharedManager]POST:@"diningguide/list4app" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         if (responseObject && [offset intValue] == 0) {
             [CommonMethod setJsonToFile:GUIDELIST_CACHE andJson:responseObject];
         }
         
         NSNumber *total =[responseObject objectForKey:@"count"];
         BOOL hasData=YES;
         if(skip >= [total intValue]){
             
             hasData = NO;
         }
         
         block([CommonMethod getGuideList:responseObject],hasData);
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         failureBlock(error.localizedDescription);
         NSLog(@"getDiningGuideList error: %@",error.localizedDescription);
     }];
}

+(void)getDiningGuideRestaurants:(NSDictionary*)parameters completion:(void(^)(NSNumber *total,NSArray *restaurants,IMGDiningGuide *guide))completionBlock failure:(void(^)(NSString *description))failureBlock{

    [[IMGNetWork sharedManager] GET:@"diningGuide/restaurants/v2" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSLog(@"%@",responseObject);
        
        IMGDiningGuide *guide = [[IMGDiningGuide alloc] init];
        guide.diningGuideId = [parameters objectForKey:@"diningGuideId"];
        guide.pageName = [responseObject objectForKey:@"pageName"];
        guide.pageContent = [responseObject objectForKey:@"pageContent"];
        guide.headerImage = [responseObject objectForKey:@"headerImage"];
        NSMutableArray *restaurants=[NSMutableArray array];
        NSDictionary *list=[responseObject objectForKey:@"restaurantList"];
        NSNumber *total=[responseObject objectForKey:@"count"];
        for(NSDictionary *dic in list){
            IMGRestaurant *restaurant=[[IMGRestaurant alloc] init];
            
            [restaurant setValuesForKeysWithDictionary:dic];
            restaurant.restaurantId=[dic objectForKey:@"id"];
            restaurant.imageUrl=[dic objectForKey:@"image"];
            restaurant.ratingScore=[dic objectForKey:@"rating"];
            restaurant.saved = [dic objectForKey:@"isSaved"];
            restaurant.goFoodLink = [dic objectForKey:@"goFoodLink"];

            if([[dic objectForKey:@"cuisine"] count]){
                restaurant.cuisineName=[[dic objectForKey:@"cuisine"] objectForKey:@"name"];    
            }else{
                restaurant.cuisineName=@"";
            }
            restaurant.priceName=[[dic objectForKey:@"priceLevel"] objectForKey:@"priceName"];
            
            restaurant.canBook=[(NSString*)[dic objectForKey:@"booknow"] boolValue];
            
            
            NSDictionary* yesterOpenDic=[dic objectForKey:@"yesterDayOpenTime"];
            restaurant.yesteropenDay=[yesterOpenDic objectForKey:@"openDay"];
            restaurant.yesteropenTime=[yesterOpenDic objectForKey:@"openTime"];
            restaurant.yesterclosedTime=[yesterOpenDic objectForKey:@"closedTime"];
            
            
            NSDictionary* nextOpenDay=[dic objectForKey:@"nextOpenDay"];
            restaurant.nextopenTime=[nextOpenDay objectForKey:@"openTime"];
            restaurant.nextclosedTime=[nextOpenDay objectForKey:@"closedTime"];
            restaurant.nextopenDay=[nextOpenDay objectForKey:@"openDay"];
            
            NSDictionary* secondOpenDic=[dic objectForKey:@"secondOpenDay"];
            restaurant.secondclosedTime=[secondOpenDic objectForKey:@"closedTime"];
            restaurant.secondopenTime=[secondOpenDic objectForKey:@"openTime"];
            restaurant.secondopenDay=[secondOpenDic objectForKey:@"openDay"];
            [restaurants addObject:restaurant];
        }
        completionBlock(total,restaurants,guide);
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        failureBlock(nil);
        NSLog(@"AFHTTPRequestOperation erors when get diningGuide/restaurants?diningGuideId:%@",error.localizedDescription);
    }];
}

+ (void)viewedGuide:(IMGDiningGuide *)guide{
    NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
    IMGUser *user=[IMGUser currentUser];
    if(user!=nil && user.userId!=nil){
        [parameters setObject:user.userId forKey:@"userId"];
    }
    [parameters setObject:guide.diningGuideId forKey:@"diningGuideId"];
    [parameters setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [parameters setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    
    NSString *sessionId = [[NSUserDefaults standardUserDefaults] stringForKey:@"qraved_session_id"];
    if (sessionId != nil) {
        [parameters setObject:sessionId forKey:@"sessionId"];
    }
    
    [[IMGNetWork sharedManager] POST:@"diningGuide/count" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject = %@",responseObject);
        NSString *sessionId = [responseObject objectForKey:@"sessionId"];
        if (sessionId != nil) {
            [[NSUserDefaults standardUserDefaults] setObject:sessionId forKey:@"qraved_session_id"];
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"trackTapped error: %@",error.localizedDescription);
    }];

}

@end
