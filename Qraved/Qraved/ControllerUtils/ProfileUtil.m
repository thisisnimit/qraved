//
//  ProfileUtil.m
//  Qraved
//
//  Created by Jeff on 12/25/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "ProfileUtil.h"
#import "IMGUser.h"
#import "IMGExceptionAlertView.h"
#import "IMGReservation.h"
#import "IMGRewardTrigger.h"
#import "IMGUserFavoriteRestaurant.h"
#import "IMGReview.h"
#import "IMGRestaurant.h"
#import "RestaurantHandler.h"


@implementation ProfileUtil

+(void)getUpcomingBookingsWithUser:(IMGUser *)user andBlock:(void(^)(NSArray*))block{

    NSDictionary * parameters;
    if (user.token)
    {
        parameters = @{@"userID":user.userId,@"t":user.token};
    }
    else
    {
        parameters = @{@"userID":user.userId};
    }
    [[IMGNetWork sharedManager] POST:@"app/user/reservation/upcoming" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation,id responseObject){
        NSLog(@"%@",responseObject);
        NSMutableArray *bookings=[[NSMutableArray alloc] init];
        NSArray * List = [responseObject objectForKey:@"userReservationList"];
        for (NSDictionary * subDict in List) {
            IMGReservation * reservation  = [[IMGReservation alloc]init];
            [reservation setValuesForKeysWithDictionary:subDict];
            NSDictionary *restaurant=[subDict objectForKey:@"simpleRestaurant"];
            reservation.restaurantPhone=[restaurant objectForKey:@"phoneNumber"];
            reservation.restaurantName=[subDict objectForKey:@"restaurantTitle"];
            reservation.party=[subDict objectForKey:@"bookPax"];
            [bookings addObject:reservation];
        }
        block(bookings);
    } failure:^(NSURLSessionDataTask *operation, NSError *error){

    }];
}

+(void)getUpcomingAndHistoryBookings:(void(^)(NSArray *upcomings,NSArray *histories,NSNumber *historyTotal))block{
    IMGUser *user=[IMGUser currentUser];
    NSDictionary * parameters = @{@"userID":user.userId,@"t":user.token};
    [[IMGNetWork sharedManager] POST:@"app/user/reservation" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation,id responseObject){
        NSLog(@"%@",responseObject);
        NSMutableArray *upcomings=[[NSMutableArray alloc] init];
        NSArray * List1 = [[responseObject objectForKey:@"upcoming"] objectForKey:@"userReservationList"];
        for (NSDictionary * subDict in List1) {
            IMGReservation * reservation  = [[IMGReservation alloc]init];
            [reservation setValuesForKeysWithDictionary:subDict];
            reservation.restaurantName=[subDict objectForKey:@"restaurantTitle"];
            reservation.party=[subDict objectForKey:@"bookPax"];
            [upcomings addObject:reservation];
        }
        NSMutableArray *histories=[[NSMutableArray alloc] init];
        NSNumber *total = [[responseObject objectForKey:@"history"] objectForKey:@"count"];
        NSArray * List2 = [[responseObject objectForKey:@"history"] objectForKey:@"userReservationList"];
        for (NSDictionary * subDict in List2) {
            IMGReservation * reservation  = [[IMGReservation alloc]init];
            [reservation setValuesForKeysWithDictionary:subDict];
            reservation.restaurantName=[subDict objectForKey:@"restaurantTitle"];
            reservation.party=[subDict objectForKey:@"bookPax"];
            [histories addObject:reservation];
        }
        block(upcomings,histories,total);
    } failure:^(NSURLSessionDataTask *operation, NSError *error){

    }];
}

+(void)getHisotryBookings:(NSDictionary*)parameters block:(void(^)(NSArray *histories,NSNumber *total))block{
    IMGUser *user=[IMGUser currentUser];
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithDictionary:parameters];
    [params setObject:user.userId forKey:@"userID"];
    [params setObject:user.token forKey:@"t"];
    [[IMGNetWork sharedManager] POST:@"app/user/reservation/history" parameters:params progress:nil success:^(NSURLSessionDataTask *operation,id responseObject){
        NSLog(@"%@",responseObject);
        NSMutableArray *histories=[[NSMutableArray alloc] init];
        NSNumber *total = [responseObject objectForKey:@"count"];
        NSArray * List = [responseObject objectForKey:@"userReservationList"];
        for (NSDictionary * subDict in List) {
            IMGReservation * reservation  = [[IMGReservation alloc]init];
            [reservation setValuesForKeysWithDictionary:subDict];
            reservation.restaurantName=[subDict objectForKey:@"restaurantTitle"];
            reservation.party=[subDict objectForKey:@"bookPax"];
            [histories addObject:reservation];
        }
        block(histories,total);
    } failure:^(NSURLSessionDataTask *operation, NSError *error){

    }];
}

@end
