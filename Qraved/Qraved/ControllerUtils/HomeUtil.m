//
//  HomeUtil.m
//  Qraved
//
//  Created by Jeff on 12/23/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "HomeUtil.h"
#import "DBManager.h"
#import "IMGEvent.h"
#import "IMGDiningGuide.h"
#import "BookUtil.h"
#import "IMGMediaComment.h"
#import "SearchUtil.h"
#import "NSString+Helper.h"
#import "IMGDish.h"
#import "IMGReview.h"
#import "IMGUser.h"
#import "IMGDishComment.h"
#import "IMGReviewComment.h"
#import "IMGRestaurantEvent.h"
#import "IMGRestaurantEventComment.h"
#import "IMGMenu.h"
#import "IMGLikeList.h"
#import "UIConstants.h"
#import "IMGUploadPhotoCard.h"
#import "IMGReviewCard.h"
#import "IMGUpdateCard.h"
#import "IMGMenuCard.h"
#import "AppDelegate.h"
#import "SDWebImagePrefetcher.h"
#import "IMGEngageCard.h"
#import "IMGOfferCard.h"

@implementation HomeUtil

+(void)refreshAndHandle:(void (^)(NSArray *eventArray,NSArray *diningGuideArray))block{
    NSNumber *cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    NSDictionary *parameters = [[NSDictionary alloc]initWithObjectsAndKeys:cityId,@"cityId", nil];
    
    [[IMGNetWork sharedManager] GET:@"home/datas" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueue];
        [queue inDatabase:^(FMDatabase *db) {
            //event
            [db executeUpdate:@"delete from IMGEvent"];
            NSArray *tmpEvevtArray = [responseObject objectForKey:@"events"];
            for (int i=0; i<tmpEvevtArray.count; i++) {
                NSDictionary *dict = [tmpEvevtArray objectAtIndex:i];
                IMGEvent *event = [[IMGEvent alloc]init];
                [event setValuesForKeysWithDictionary:dict];
                if([event.status intValue]==1){
                    [[DBManager manager]insertModel:event selectField:@"eventId" andSelectID:event.eventId andFMDatabase:db];
                }
            }
            
            //diningGuide
            [db executeUpdate:@"delete from IMGDiningGuide"];
            NSArray * array =[responseObject objectForKey:@"diningguides"];
            for (int i=0; i<array.count; i++) {
                NSArray *diningGuideArray = [array objectAtIndex:i];
                NSDictionary * diningGuideDictionary = [diningGuideArray objectAtIndex:0];
                IMGDiningGuide * guide =[[IMGDiningGuide alloc]init];
                [guide setValuesForKeysWithDictionary:diningGuideDictionary];
                guide.restaurantCount = [diningGuideArray objectAtIndex:1];
                guide.diningGuideId = [diningGuideDictionary objectForKey:@"id"];
                if([guide.pageShow intValue]==1){
                    [[DBManager manager] insertModel:guide selectField:@"diningGuideId" andSelectID:guide.diningGuideId andFMDatabase:db];
                }
            }
            NSArray *eventArray = [HomeUtil addEventToArray];
            NSArray *diningGuideArray = [HomeUtil addDiningGuideToArray];
            
            block(eventArray,diningGuideArray);
        }];
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSArray *eventArray = [HomeUtil addEventToArray];
        NSArray *diningGuideArray = [HomeUtil addDiningGuideToArray];
        block(eventArray,diningGuideArray);
        
        NSLog(@"AFHTTPRequestOperation erors when get home/datas:%@",error.localizedDescription);
    }];
}

+(NSArray *)addDiningGuideToArray{
    NSMutableArray *diningGuideArray=[[NSMutableArray alloc]init];
    NSString *diningGuideSql = @"select * from IMGDiningGuide dg where dg.pageShow=1 order by CAST(sortOrder AS INTeger)";
    FMResultSet *resultSet1 =[[DBManager manager] executeQuery:diningGuideSql];
    while ([resultSet1 next]) {
        IMGDiningGuide *diningGuide =[[IMGDiningGuide alloc]init];
        [diningGuide setValueWithResultSet:resultSet1];
        diningGuide.diningGuideId = [NSNumber numberWithInt:[resultSet1 intForColumn:@"diningGuideId"]];
        [diningGuideArray addObject:diningGuide];
    }
    [resultSet1 close];
    return diningGuideArray;
}

+(NSArray *)addEventToArray{
    NSMutableArray *eventArray=[[NSMutableArray alloc]init];
    NSString *todayString = [BookUtil todayStringWithFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSString *eventSql = [NSString stringWithFormat:@"SELECT * FROM IMGEvent WHERE startDate<='%@' and endDate>='%@' and status=1  order by CAST(sortOrder AS INTeger);",todayString,todayString];
    FMResultSet *resultSet2=[[DBManager manager]executeQuery:eventSql];
    
    while ([resultSet2 next]) {
        IMGEvent *event =[[IMGEvent alloc]init];
        [event setValueWithResultSet:resultSet2];
        [eventArray addObject:event];
    }
    [resultSet2 close];
    return eventArray;
}

+ (void)getEventsFromService:(void (^)(NSArray *eventArray))block
{
    NSNumber *cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    NSDictionary *parameters = [[NSDictionary alloc]initWithObjectsAndKeys:cityId,@"cityId", nil];
//    http://dev.qraved.com:7055/app/appBanner?cityID=2
    [[IMGNetWork sharedManager] GET:@"app/appBanner" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        
        NSLog(@"%f,getHomeEventData 2.0",[[NSDate date]timeIntervalSince1970]);

        FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueue];
        [queue inDatabase:^(FMDatabase *db) {
            //event
            [db executeUpdate:@"delete from IMGEvent"];
            
            NSArray *tmpEvevtArray = responseObject;
            NSMutableArray *eventArray = [[NSMutableArray alloc] init];
            
            for (int i=0; i<tmpEvevtArray.count; i++) {
                NSDictionary *dict = [tmpEvevtArray objectAtIndex:i];
                IMGEvent *event = [[IMGEvent alloc]init];
//                [event setValuesForKeysWithDictionary:dict];
                event.eventId = [dict objectForKey:@"bannerId"];
                event.appBanner = [dict objectForKey:@"appImage"];
                event.name = [dict objectForKey:@"title"];
                event.targetUrlApp = [dict objectForKey:@"app_link"];
                event.bannerImageId = [dict objectForKey:@"bannerImageId"];
                [eventArray addObject:event];
                [[DBManager manager]insertModel:event selectField:@"bannerImageId" andSelectID:event.bannerImageId andFMDatabase:db];
            }

            block(eventArray);
            
        }];


        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        
        NSLog(@"AFHTTPRequestOperation erors when get home/datas:%@",error.localizedDescription);
        block(nil);
    }];

}
+ (void)getJournalFromService:(void (^)(NSArray *journalArray))block
{

    [[IMGNetWork sharedManager] GET:@"home/journalsearch" parameters:nil progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        
        NSMutableArray *dataArrM = [[NSMutableArray alloc] init];

        NSArray *journalArticleList = [responseObject objectForKey:@"journalArticleList"];
        for (NSDictionary *journalDic in journalArticleList)
        {
            IMGMediaComment *journal = [[IMGMediaComment alloc] init];
            
            journal.title = [journalDic objectForKey:@"articleTitle"];
            NSRange range=[journal.title rangeOfString:@"amp;"];
            if (range.location != NSNotFound)
            {
                journal.title = [journal.title stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
            }
            
            journal.mediaCommentId = [journalDic objectForKey:@"articleId"];
            journal.imageUrl = [journalDic objectForKey:@"articlePhoto"];
            NSArray *categories=[journalDic objectForKey:@"category"];
            NSMutableArray *category_list=[[NSMutableArray alloc] init];
            if(categories.count){
                for (NSDictionary *category in categories) {
                    [category_list addObject:[category objectForKey:@"name"]];
                }
                
                journal.categoryName=[category_list componentsJoinedByString:@", "];
            }
            
//            journal.shareCount = [journalDic objectForKey:@"shareCount"];
            journal.imported = [journalDic objectForKey:@"imported"];
            [dataArrM addObject:journal];
        }
        block(dataArrM);
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {

        NSLog(@"AFHTTPRequestOperation erors when getJournalFromService:%@",error.localizedDescription);
    }];
    
}
+ (void)getNearYouFromService:(void (^)(NSArray *nearYouArray))block
{

}
+ (void)getDiningGuideFromService:(void (^)(NSArray *diningGuideArray))block
{
    NSNumber *cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    NSDictionary *parameters = [[NSDictionary alloc]initWithObjectsAndKeys:cityId,@"cityId", nil];
    
    [[IMGNetWork sharedManager] GET:@"home/diningguides" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
       
        NSMutableArray *diningGuideArrM = [[NSMutableArray alloc] init];
        for (NSDictionary *dic in [responseObject objectForKey:@"diningguides"])
        {
            IMGDiningGuide *diningGuide = [[IMGDiningGuide alloc] init];
            [diningGuide setValuesForKeysWithDictionary:dic];
            diningGuide.restaurantCount = [dic objectForKey:@"count"];
            diningGuide.pageName = [diningGuide.pageName filterHtml];
            [diningGuideArrM addObject:diningGuide];
        }
        block(diningGuideArrM);
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        
        NSLog(@"AFHTTPRequestOperation erors when getDiningGuideFromService:%@",error.localizedDescription);
    }];
    

}
+ (void)getRestaurantWithOfferFromService:(void (^)(NSArray *restaurantArray))block
{
    
}

+ (void)getSingleDiningGuideFromServiceWithDiningGuideId:(NSNumber *)diningGuideId andBlock:(void (^)(IMGDiningGuide *guide))block
{
    NSDictionary *parameters = [[NSDictionary alloc]initWithObjectsAndKeys:diningGuideId,@"diningGuideId", nil];
    
    [[IMGNetWork sharedManager] GET:@"app/diningGuide" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        
        NSArray * array = [responseObject objectAtIndex:0];
        NSDictionary *diningGuideDictionary = [array objectAtIndex:0];
        IMGDiningGuide * guide =[[IMGDiningGuide alloc]init];
        [guide setValuesForKeysWithDictionary:diningGuideDictionary];
        guide.restaurantCount = [array objectAtIndex:1];
        guide.diningGuideId = [diningGuideDictionary objectForKey:@"id"];
//        if([guide.pageShow intValue]==1)
//        {
//            [[DBManager manager] insertModel:guide selectField:@"diningGuideId" andSelectID:guide.diningGuideId];
//        }
        block(guide);
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        
        NSLog(@"AFHTTPRequestOperation erors when getSingleDiningGuideFromServiceWithDiningGuideId:%@",error.localizedDescription);
    }];

}
+(void)getLikeListFromServiceWithReviewId:(NSNumber*)reviewId andMax:(NSNumber*)max andoffset:(NSNumber*)offset andBlock:(void(^)(NSArray *likeListArray,NSNumber* likeviewcount))block
{

    NSDictionary *parameters=@{@"reviewId":reviewId,@"max":max,@"minId":offset,@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]};
    [[IMGNetWork sharedManager]GET:@"review/likeList" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
         NSNumber* likecount=[responseObject objectForKey:@"count"];
        NSArray* resultList=[responseObject objectForKey:@"resultList"];
        NSMutableArray* likeLisrArray=[[NSMutableArray alloc]init];
        for (NSDictionary* dic in resultList) {
             IMGLikeList* likeList=[[IMGLikeList alloc]init];
            likeList.fullName=[dic objectForKey:@"fullName"];
            likeList.imageUrl=[dic objectForKey:@"imageUrl"];
            likeList.photoCount=[dic objectForKey:@"photoCount"];
            likeList.reviewCount=[dic objectForKey:@"reviewCount"];
            likeList.userId=[dic objectForKey:@"userId"];
            likeList.reviewId=[dic objectForKey:@"reviewId"];
            likeList.likeId=[dic objectForKey:@"likeId"];
            [likeLisrArray addObject:likeList];
        }
       
        block(likeLisrArray,likecount);
        
        
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
      NSLog(@"AFHTTPRequestOperation erors when getLikeListFromServiceWithReviewId:%@",error.localizedDescription);
    }];





}
+(void)getLikeListFromServiceWithDishId:(NSNumber*)dishId andMax:(NSNumber*)max andoffset:(NSNumber*)offset andBlock:(void(^)(NSArray *likeListArray,NSNumber* likeviewcount))block
{

NSDictionary *parameters=@{@"dishId":dishId,@"max":max,@"minId":offset};

    [[IMGNetWork sharedManager]GET:@"dish/qravedList" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSNumber* likecount=[responseObject objectForKey:@"count"];
        NSArray* resultList=[responseObject objectForKey:@"resultList"];
        NSLog(@"%@",resultList);
        NSMutableArray* likeLisrArray=[[NSMutableArray alloc]init];
        for (NSDictionary* dic in resultList) {
            IMGLikeList* likeList=[[IMGLikeList alloc]init];
            likeList.fullName=[dic objectForKey:@"fullName"];
            likeList.imageUrl=[dic objectForKey:@"imageUrl"];
            likeList.photoCount=[dic objectForKey:@"photoCount"];
            likeList.reviewCount=[dic objectForKey:@"reviewCount"];
            likeList.userId=[dic objectForKey:@"userId"];
            likeList.dishId=[dic objectForKey:@"dishId"];
            likeList.likeId=[dic objectForKey:@"likeId"];
            [likeLisrArray addObject:likeList];
        
        }
        block(likeLisrArray,likecount);

        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        
        
        NSLog(@"AFHTTPRequestOperation erors when getLikeListFromServiceWithDishId:%@",error.localizedDescription); 
        
        
        
        
    }];
}

+(void)getLikeListMuchPhotoFromServiceWithmoderateReviewId:(NSNumber*)moderateReviewId andMax:(NSNumber*)max andoffset:(NSNumber*)offset andBlock:(void(^)(NSArray *likeListArray,NSNumber* likeviewcount))block
    {
       
        NSDictionary *parameters=@{@"moderateReviewId":moderateReviewId,@"max":max,@"minId":offset,@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]};
        
        [[IMGNetWork sharedManager]GET:@"moderateReview/likeList" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
          //  NSNumber* count=[responseObject objectForKey:@"count"];
            NSArray* resultList=[responseObject objectForKey:@"resultList"];
            NSNumber* likecount=[responseObject objectForKey:@"count"];

            NSLog(@"%@",resultList);
            NSMutableArray* likeLisrArray=[[NSMutableArray alloc]init];
            for (NSDictionary* dic in resultList) {
                IMGLikeList* likeList=[[IMGLikeList alloc]init];
                likeList.fullName=[dic objectForKey:@"fullName"];
                likeList.imageUrl=[dic objectForKey:@"imageUrl"];
                likeList.photoCount=[dic objectForKey:@"photoCount"];
                likeList.reviewCount=[dic objectForKey:@"reviewCount"];
                likeList.userId=[dic objectForKey:@"userId"];
                likeList.dishId=[dic objectForKey:@"dishId"];
                likeList.likeId=[dic objectForKey:@"likeId"];

                [likeLisrArray addObject:likeList];
                
            }
            block(likeLisrArray,likecount);
            
            
        } failure:^(NSURLSessionDataTask *operation, NSError *error) {
            
            
            NSLog(@"AFHTTPRequestOperation erors when getLikeListMuchPhotoFromServiceWithDishId:%@",error.localizedDescription);
            
            
            
            
        }];
        
        




}
+(void)getLikeListMuchPhotoFromServiceWithHomeTimelineRestaurantUpdateId:(NSNumber*)homeTimelineRestaurantUpdateId andMax:(NSNumber*)max andoffset:(NSNumber*)offset andBlock:(void(^)(NSArray *likeListArray,NSNumber* likeviewcount))block
{
    
    NSDictionary *parameters=@{@"homeTimelineRestaurantUpdateId":homeTimelineRestaurantUpdateId,@"max":max,@"minId":offset};
    
    [[IMGNetWork sharedManager]GET:@"restaurantEvent/homeTimeLineLikeList" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        //  NSNumber* count=[responseObject objectForKey:@"count"];
        NSArray* resultList=[responseObject objectForKey:@"resultList"];
          NSNumber* likecount=[responseObject objectForKey:@"count"];
        NSLog(@"%@",resultList);
        NSMutableArray* likeLisrArray=[[NSMutableArray alloc]init];
        for (NSDictionary* dic in resultList) {
            IMGLikeList* likeList=[[IMGLikeList alloc]init];
            likeList.fullName=[dic objectForKey:@"fullName"];
            likeList.imageUrl=[dic objectForKey:@"imageUrl"];
            likeList.photoCount=[dic objectForKey:@"photoCount"];
            likeList.reviewCount=[dic objectForKey:@"reviewCount"];
            likeList.userId=[dic objectForKey:@"userId"];
            likeList.dishId=[dic objectForKey:@"dishId"];
            likeList.likeId=[dic objectForKey:@"likeId"];

            [likeLisrArray addObject:likeList];
            
        }
        block(likeLisrArray,likecount);
        
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        
        
        NSLog(@"AFHTTPRequestOperation erors when getLikeListFromServiceWithResurantEventId:%@",error.localizedDescription);
        
        
        
        
    }];
    
    
    
    
    
    
}
+(void)getLikeListFromServiceWithOfferCardId:(NSNumber*)OfferCardId andMax:(NSNumber*)max andoffset:(NSNumber*)offset andBlock:(void(^)(NSArray *likeListArray,NSNumber* likeviewcount))block
{
    
    NSDictionary *parameters=@{@"restaurantOfferId":OfferCardId,@"max":max,@"minId":offset,@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]};
    
//    NSMutableDictionary *parameters=[[NSMutableDictionary alloc]init];
//    [parameters set];
    
    
    
    [[IMGNetWork sharedManager]GET:@"restaurantoffer/likelist" parameters:parameters success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSNumber* likecount=[responseObject objectForKey:@"count"];
        NSArray* resultList=[responseObject objectForKey:@"resultList"];
        NSLog(@"%@",resultList);
        NSMutableArray* likeLisrArray=[[NSMutableArray alloc]init];
        for (NSDictionary* dic in resultList) {
            IMGLikeList* likeList=[[IMGLikeList alloc]init];
            likeList.fullName=[dic objectForKey:@"fullName"];
            likeList.imageUrl=[dic objectForKey:@"imageUrl"];
            likeList.photoCount=[dic objectForKey:@"photoCount"];
            likeList.reviewCount=[dic objectForKey:@"reviewCount"];
            likeList.userId=[dic objectForKey:@"userId"];
            likeList.restaurantEventId=[dic objectForKey:@"restaurantEventId"];
            likeList.likeId=[dic objectForKey:@"likeId"];
            
            [likeLisrArray addObject:likeList];
            
        }
        block(likeLisrArray,likecount);
        
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        
        
        NSLog(@"AFHTTPRequestOperation erors when getLikeListFromServiceWithResturantId:%@",error.localizedDescription);
        
        
        
        
    }];
    
    
    
    
    
}

+(void)getLikeListFromServiceWithResturantId:(NSNumber*)restaurantID andMax:(NSNumber*)max andoffset:(NSNumber*)offset andBlock:(void(^)(NSArray *likeListArray,NSNumber* likeviewcount))block
{
    
    NSDictionary *parameters=@{@"restaurantEventId":restaurantID,@"max":max,@"minId":offset,@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]};
    
    [[IMGNetWork sharedManager]GET:@"restaurantEvent/likeList" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
         NSNumber* likecount=[responseObject objectForKey:@"count"];
        NSArray* resultList=[responseObject objectForKey:@"resultList"];
        NSLog(@"%@",resultList);
        NSMutableArray* likeLisrArray=[[NSMutableArray alloc]init];
        for (NSDictionary* dic in resultList) {
            IMGLikeList* likeList=[[IMGLikeList alloc]init];
            likeList.fullName=[dic objectForKey:@"fullName"];
            likeList.imageUrl=[dic objectForKey:@"imageUrl"];
            likeList.photoCount=[dic objectForKey:@"photoCount"];
            likeList.reviewCount=[dic objectForKey:@"reviewCount"];
            likeList.userId=[dic objectForKey:@"userId"];
            likeList.restaurantEventId=[dic objectForKey:@"restaurantEventId"];
            likeList.likeId=[dic objectForKey:@"likeId"];

            [likeLisrArray addObject:likeList];
            
        }
        block(likeLisrArray,likecount);
        
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        
        
        NSLog(@"AFHTTPRequestOperation erors when getLikeListFromServiceWithResturantId:%@",error.localizedDescription);
        
        
        
        
    }];
    
    
    
    
    
}
+(void)getLikeListFromServiceWithJournalArticleId:(IMGMediaComment*)MediaComment andMax:(NSNumber*)max andoffset:(NSNumber*)offset andBlock:(void(^)(NSArray *likeListArray,NSNumber* likeviewcount))block
{
    
    NSDictionary *parameters=@{@"journalArticleId":MediaComment.mediaCommentId,@"max":max,@"minId":offset,@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]};
    
    [[IMGNetWork sharedManager]GET:@"journalArticle/likeList" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
            NSNumber* likecount=[responseObject objectForKey:@"count"];
        NSArray* resultList=[responseObject objectForKey:@"resultList"];
        NSMutableArray* likeLisrArray=[[NSMutableArray alloc]init];
        for (NSDictionary* dic in resultList) {
            IMGLikeList* likeList=[[IMGLikeList alloc]init];
            likeList.fullName=[dic objectForKey:@"fullName"];
            likeList.imageUrl=[dic objectForKey:@"imageUrl"];
            likeList.photoCount=[dic objectForKey:@"photoCount"];
            likeList.reviewCount=[dic objectForKey:@"reviewCount"];
            likeList.userId=[dic objectForKey:@"userId"];
            likeList.restaurantEventId=[dic objectForKey:@"restaurantEventId"];
            likeList.likeId=[dic objectForKey:@"likeId"];

            [likeLisrArray addObject:likeList];
            
        }
        block(likeLisrArray,likecount);
        
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        
        
        NSLog(@"AFHTTPRequestOperation erors when getLikeListFromServiceWithResturantId:%@",error.localizedDescription);
        
        
        
        
    }];
    
    
    
    
    
}
+ (void)getSingleEventFromServiceWithEventId:(NSNumber *)eventId andBlock:(void (^)())block failure:(void(^)())failure
{
    NSDictionary *parameters = [[NSDictionary alloc]initWithObjectsAndKeys:eventId,@"eventId", nil];
    
    [[IMGNetWork sharedManager] GET:@"app/event" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        if ( (responseObject!=nil) && ([responseObject count]!=0) ) {
            NSDictionary *dict = [responseObject objectAtIndex:0];
            IMGEvent *event = [[IMGEvent alloc]init];
            [event setValuesForKeysWithDictionary:dict];
            if([event.status intValue]==1){
                [[DBManager manager]insertModel:event selectField:@"eventId" andSelectID:event.eventId];
            }
            block();
        }else{
            failure();
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        failure();
        NSLog(@"AFHTTPRequestOperation erors when getSingleEventFromServiceWithEventId:%@",error.localizedDescription);
    }];
}

+ (void)getSplashWithBlock:(void (^)(IMGSplash *splash))block
{
    [[IMGNetWork sharedManager] GET:@"splash" parameters:nil progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        if([[responseObject objectForKey:@"id"] intValue]>0){
            IMGSplash *splash = [[IMGSplash alloc] init];
            [splash setValuesForKeysWithDictionary:responseObject];
            splash.splashId = [responseObject objectForKey:@"id"];
            splash.splashDescription = [responseObject objectForKey:@"description"];
            block(splash);
        }else{
            [[AppDelegate ShareApp] addPopLoginNSTimer];
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        [[AppDelegate ShareApp] addPopLoginNSTimer];
        NSLog(@"AFHTTPRequestOperation erors when getSplash:%@",error.localizedDescription);
    }];
}
+(void)getBookRatingSplashWithBlock:(void(^)(NSMutableArray *splashArray))block{
    IMGUser *user = [IMGUser currentUser];
    if ([user.userId intValue]==0) {
        block(nil);
        return;
    }
    [[IMGNetWork sharedManager] POST:@"reservation/getReservationByUserId" parameters:@{@"userId":user.userId} progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        NSNumber *reservationCount = [responseObject objectForKey:@"reservationCount"];
        if ([reservationCount intValue]==0) {
            block(nil);
        }
        NSMutableArray *reservationList = [responseObject objectForKey:@"reservationList"];
        if (reservationList.count>0) {
            block(reservationList);
        }else{
            block(nil);
        }
        

    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(nil);

    }];

}
+(void)postBookRatingScore:(NSDictionary*)parameters andBlock:(void(^)(BOOL isSucceed))block{
    [[IMGNetWork sharedManager] POST:@"review/postBookRating" parameters:parameters progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            block(YES);
           
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        block(NO);
    }];
}
+(void)getDownloadInfoFromService:(NSDictionary*)parameters andBlock:(void(^)(id responese))block andErroBlock:(void(^)(NSError* error))errorBlock{

    [[IMGNetWork sharedManager] GET:@"iosdata/download" parameters:nil progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {

        block(responseObject);
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {

        errorBlock(error);
        
        NSLog(@"AFHTTPRequestOperation erors when getSplash:%@",error.localizedDescription);
    }];




}

+(void)getversionInfoFromService:(NSDictionary*)parameters andBlock:(void(^)(id responese))block andErroBlock:(void(^)(NSError* error))errorBlock{
    
    [[IMGNetWork sharedManager] GET:@"iosdata/version" parameters:nil progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        block(responseObject);
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        
        errorBlock(error);
        
        NSLog(@"AFHTTPRequestOperation erors when getSplash:%@",error.localizedDescription);
    }];
    
    
    
    
}

+ (void)getNewHomeDataFromService:(NSDictionary*)parameters andBlock:(void (^)(NSArray *dataArr,int minId,NSArray *imageUrlsArr))block
{
    [[IMGNetWork sharedManager] POST:@"app/home/timeline/v2" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSLog(@"%f,getHomeData 2.0",[[NSDate date]timeIntervalSince1970]);
        id timeLineList = [responseObject objectForKey:@"timeLineList"];
        NSMutableArray *dataArrM = [[NSMutableArray alloc] init];
        if([timeLineList isKindOfClass:[NSDictionary class]] && [responseObject objectForKey:@"exceptionmsg"]){
            block(dataArrM,0,nil);
            return;
        
        }
        
                
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isHadHomeFirstData"])
        {
            
            NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *path = [pathArray objectAtIndex:0];
            //获取文件的完整路径
            NSString *filePatch = [path stringByAppendingPathComponent:@"HomeLastDatastickCards.plist"];
            //写入plist里面
            [responseObject writeToFile:filePatch atomically:YES];
            
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isHadHomeFirstData"];
        }

        int minId = [[[timeLineList lastObject] objectForKey:@"timelineId"] intValue];
        
        NSDictionary *dic = [HomeUtil processingData:responseObject];
        block([dic objectForKey:@"data"],minId,[dic objectForKey:@"imageUrls"]);
        

    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        
        NSLog(@"getNewHomeDataFromService erors when getSplash:%@",error.localizedDescription);
        block(nil,0,nil);
    }];
}
+ (NSDictionary *)processingData:(id)responseObject
{
    NSArray *timeLineList = nil;
    NSArray *stickCards = nil;
    
    if ([responseObject isKindOfClass:[NSArray class]]) {
        timeLineList = responseObject;
    }else if([responseObject isKindOfClass:[NSDictionary class]]) {
        timeLineList = [responseObject objectForKey:@"timeLineList"];
        stickCards = [responseObject objectForKey:@"stickCards"];
    }
    
    NSMutableArray *dataArrM = [[NSMutableArray alloc] init];
    NSMutableArray *imageUrlsArrM = [[NSMutableArray alloc] init];
    for (NSDictionary *dic in stickCards){
        NSNumber *type = [dic objectForKey:@"type"];
        if([type isEqualToNumber:@(1)]){
            //Dish
            IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
            restaurant.title = [dic objectForKey:@"restaurantTitle"];
            restaurant.restaurantId = [dic objectForKey:@"restaurantId"];
            restaurant.timeline = [dic objectForKey:@"timeline"];
            if (restaurant.timeline == nil) {
                restaurant.timeline = [dic objectForKey:@"createTime"];
            }
            restaurant.imageUrl = [dic objectForKey:@"restaurantImage"];
            restaurant.timelineId = [dic objectForKey:@"timelineId"];
            restaurant.likeCardCount = [dic objectForKey:@"likeCount"];
            restaurant.commentCardCount = [dic objectForKey:@"commentCount"];
            restaurant.isLikeCard = [[dic objectForKey:@"isLike"] boolValue];
            //                    restaurant.commentCardList = [dic objectForKey:@"commentList"];
            restaurant.commentCardList = [IMGRestaurant commentListFromArray:[dic objectForKey:@"commentList"] andIsVendor:[[dic objectForKey:@"isVendor"] boolValue] andRestaurant:restaurant ];
            
            
            
            NSMutableArray *dishArrM = [[NSMutableArray alloc] init];
            NSArray *dishArr = [dic objectForKey:@"dishList"];
            for (NSDictionary *subDic in dishArr)
            {
                IMGDish *dish = [[IMGDish alloc] init];
                [dish setValuesForKeysWithDictionary:subDic];
                dish.dishId = [subDic objectForKey:@"id"];
                dish.restaurantSeo = [dic objectForKey:@"restaurantSeo"];
                dish.restaurantTitle = [dic objectForKey:@"restaurantTitle"];
                dish.restaurantId = [dic objectForKey:@"restaurantId"];
                dish.descriptionStr = [subDic objectForKey:@"description"];
                dish.userName = [dic objectForKey:@"userName"];
                dish.showWriteReview = [[dic objectForKey:@"showWriteReview"] boolValue];
                dish.userId = [dic objectForKey:@"userId"];
                [dish updatePhotoCredit:[subDic objectForKey:@"photoCredit"]];
                dish.moderateReviewId=[dic objectForKey:@"moderateReviewId"];
                NSDictionary *pictureInfo = [subDic objectForKey:@"pictureInfo"];
                double width = [[pictureInfo objectForKey:@"width"] doubleValue];
                double height = [[pictureInfo objectForKey:@"height"] doubleValue];
                dish.imageHeight = [NSNumber numberWithInt:((DeviceWidth - LEFTLEFTSET*2)*height)/width];
                if ([dish.imageHeight floatValue] < 0)
                {
                    dish.imageHeight = 0;
                }
                if ([dishArr count] == 1 && [dish.commentCount intValue])
                {
                    NSArray *commentArray=[subDic objectForKey:@"commentList"];
                    dish.commentList = [IMGDishComment commentListFromArray:commentArray andIsVendor:[[dic objectForKey:@"isVendor"] boolValue] andRestaurant:restaurant];
                }
                
                
                [dishArrM addObject:dish];
            }
            
            [imageUrlsArrM addObjectsFromArray:[self returnImageUrlsWithDishArr:dishArrM]];
            
            IMGUser *user = [[IMGUser alloc] init];
            [user setValuesForKeysWithDictionary:dic];
            user.reviewCount = [dic objectForKey:@"userReviewCount"];
            user.photoCount = [dic objectForKey:@"userPhotoCount"];
            user.moderateReviewId = [dic objectForKey:@"moderateReviewId"];
            IMGUploadPhotoCard *uploadPhotoCard = [[IMGUploadPhotoCard alloc] init];
            uploadPhotoCard.uploadCardUser = user;
            uploadPhotoCard.uploadCardRestaurant = restaurant;
            uploadPhotoCard.dishArrM = dishArrM;
            NSDictionary *dataDic = [[NSDictionary alloc] initWithObjectsAndKeys:uploadPhotoCard,@(1001), nil];
            [dataArrM addObject:dataDic];
        }else if([type isEqualToNumber:@(2)]){
            //Journal
            IMGMediaComment *journal=[[IMGMediaComment alloc] init];
            journal.mediaCommentId=[dic objectForKey:@"journalId"];
            journal.journalTitle=[dic objectForKey:@"journalTitle"];
            NSArray *cats=[dic objectForKey:@"categoryList"];
            journal.categoryName=[cats componentsJoinedByString:@", "];
            journal.likeCount=[dic objectForKey:@"likeCount"];
            journal.commentCount=[dic objectForKey:@"commentCount"];
            journal.journalImageUrl=[dic objectForKey:@"mainPhoto"];
            
            NSString *urlString = [journal.journalImageUrl returnFullImageUrlWithWidth:DeviceWidth-LEFTLEFTSET*2];
            [imageUrlsArrM addObject:urlString];
            
            journal.isLike = [[dic objectForKey:@"isLike"] boolValue];
            journal.typeName = [dic objectForKey:@"typename"];
            journal.link = [dic objectForKey:@"link"];
            journal.createdDate=[dic objectForKey:@"timeline"];
//            if (journal.createdDate == nil) {
//                journal.createdDate=[dic objectForKey:@"postTime"];
//            }
            journal.postTime = [dic objectForKey:@"postTime"];
            journal.viewCount = [dic objectForKey:@"viewCount"];
            NSDictionary *journalDic = [[NSDictionary alloc] initWithObjectsAndKeys:journal,@(1002), nil];
            NSArray *commentArray=[dic objectForKey:@"commentList"];
            journal.commentList=[IMGMediaComment commentListFromArray:commentArray];
            [dataArrM addObject:journalDic];
        }else if([type isEqualToNumber:@(3)]){
            //Offer
        }else if([type isEqualToNumber:@(4)]){
            //Restaurant
            IMGRestaurant *restaurant=[[IMGRestaurant alloc] init];
            restaurant.title=[dic objectForKey:@"restaurantTitle"];
            restaurant.priceName=[dic objectForKey:@"priceName"];
            restaurant.ratingScore=[NSString stringWithFormat:@"%.0f",[[dic objectForKey:@"rating"] floatValue]/2];
            restaurant.imageUrl=[dic objectForKey:@"restaurantImage"];
            NSString *urlString = [restaurant.imageUrl returnFullImageUrlWithWidth:DeviceWidth-2*LEFTLEFTSET];
            [imageUrlsArrM addObject:urlString];
            restaurant.phoneNumber=[dic objectForKey:@"phone"];
            restaurant.cuisineName=[dic objectForKey:@"cuisineName"];
            restaurant.districtName=[dic objectForKey:@"districtName"];
            restaurant.timeline=[dic objectForKey:@"timeline"];
            if (restaurant.timeline == nil) {
                restaurant.timeline = [dic objectForKey:@"lastTimeline"];
            }
            restaurant.reviewCount = [dic objectForKey:@"reviewCount"];
            restaurant.latitude=[dic objectForKey:@"latitude"];
            restaurant.longitude=[dic objectForKey:@"longitude"];
            restaurant.savedCount=[dic objectForKey:@"savedCount"];
            restaurant.saved=[dic objectForKey:@"isSaved"];
            restaurant.typeName = [dic objectForKey:@"typename"];
            restaurant.seoKeyword = [dic objectForKey:@"restaurantSeo"];
            restaurant.restaurantId = [dic objectForKey:@"restaurantId"];
            restaurant.cityId = [dic objectForKey:@"cityId"];
            NSDictionary *restaurantDic = [[NSDictionary alloc] initWithObjectsAndKeys:restaurant,@(1004), nil];
            [dataArrM addObject:restaurantDic];
        }else if ([type isEqualToNumber:@(5)]) {
            //write review
            IMGEngageCard *engageCard = [[IMGEngageCard alloc] init];
            engageCard.reviewId = [dic objectForKey:@"reviewId"];
            engageCard.restaurantId = [dic objectForKey:@"restaurantId"];
            engageCard.restaurantTitle = [dic objectForKey:@"restaurantTitle"];
            engageCard.restaurantSeo = [dic objectForKey:@"restaurantSeo"];
            engageCard.restaurantImage = [dic objectForKey:@"restaurantImage"];
            engageCard.restaurantImageAltText = [dic objectForKey:@"restaurantImageAltText"];
            engageCard.cityId = [dic objectForKey:@"cityId"];
            NSDictionary *engageDic = [[NSDictionary alloc] initWithObjectsAndKeys:engageCard,@(1005), nil];
            [dataArrM addObject:engageDic];
        }else if ([type isEqualToNumber:@(6)]) {
            //Upload Photo
            IMGEngageCard *engageCard = [[IMGEngageCard alloc] init];
            engageCard.reviewId = [dic objectForKey:@"reviewId"];
            engageCard.restaurantId = [dic objectForKey:@"restaurantId"];
            engageCard.restaurantTitle = [dic objectForKey:@"restaurantTitle"];
            engageCard.restaurantSeo = [dic objectForKey:@"restaurantSeo"];
            engageCard.restaurantImage = [dic objectForKey:@"restaurantImage"];
            engageCard.restaurantImageAltText = [dic objectForKey:@"restaurantImageAltText"];
            engageCard.cityId = [dic objectForKey:@"cityId"];
            NSDictionary *engageDic = [[NSDictionary alloc] initWithObjectsAndKeys:engageCard,@(1006), nil];
            [dataArrM addObject:engageDic];
        }
    }
    
    for (NSDictionary *dic in timeLineList)
    {
        switch ([[dic objectForKey:@"type"] intValue])
        {
            case 2:{
                IMGMediaComment *journal=[[IMGMediaComment alloc] init];
                journal.mediaCommentId=[dic objectForKey:@"journalId"];
                journal.journalTitle=[dic objectForKey:@"journalTitle"];
                NSArray *cats=[dic objectForKey:@"categoryList"];
                journal.categoryName=[cats componentsJoinedByString:@", "];
                journal.likeCount=[dic objectForKey:@"likeCount"];
                journal.commentCount=[dic objectForKey:@"commentCount"];
                journal.journalImageUrl=[dic objectForKey:@"mainPhoto"];
                journal.postTime = [dic objectForKey:@"postTime"];
                journal.viewCount = [dic objectForKey:@"viewCount"];
                NSString *urlString = [journal.journalImageUrl returnFullImageUrlWithWidth:DeviceWidth-LEFTLEFTSET*2];
                [imageUrlsArrM addObject:urlString];

                journal.isLike = [[dic objectForKey:@"isLike"] boolValue];
                journal.typeName = [dic objectForKey:@"typename"];
                journal.link = [dic objectForKey:@"link"];
                journal.createdDate=[dic objectForKey:@"timeline"];
                NSDictionary *journalDic = [[NSDictionary alloc] initWithObjectsAndKeys:journal,[dic objectForKey:@"type"], nil];
                NSArray *commentArray=[dic objectForKey:@"commentList"];
                journal.commentList=[IMGMediaComment commentListFromArray:commentArray];
                [dataArrM addObject:journalDic];
                break;
            }
            case 4:{
                
                IMGDiningGuide *diningGuide=[[IMGDiningGuide alloc] init];
                [diningGuide setValuesForKeysWithDictionary:[dic objectForKey:@"diningGuide"]];
                diningGuide.restaurantCount=[dic objectForKey:@"restaurantCount"];
                diningGuide.typeName = [dic objectForKey:@"typename"];
                diningGuide.timeline = [dic objectForKey:@"timeline"];
                diningGuide.imageUrl = [[dic objectForKey:@"diningGuide"] objectForKey:@"headerImage"];
                
                NSString *urlString = [diningGuide.imageUrl returnFullImageUrlWithWidth:DeviceWidth-2*LEFTLEFTSET];
                [imageUrlsArrM addObject:urlString];
                
                NSDictionary *diningGuideDic = [[NSDictionary alloc] initWithObjectsAndKeys:diningGuide,[dic objectForKey:@"type"], nil];
                [dataArrM addObject:diningGuideDic];
                break;
            }
            case 8:{
                IMGOfferCard *offerModel=[[IMGOfferCard alloc]init];
                [offerModel setValuesForKeysWithDictionary:dic];
                offerModel.isLike=[[dic objectForKey:@"isLike"] boolValue];
                
                NSDictionary *offerInfo = [dic objectForKey:@"offer"];
                NSDictionary *offerImage=[offerInfo objectForKey:@"offerImage"];
                NSDictionary *offerPictureInfo=[offerImage objectForKey:@"pictureInfo"];
                offerModel.offerTitle=[offerInfo objectForKey:@"offerTitle"];
                offerModel.offerType=[offerInfo objectForKey:@"offerType"];
                double width = [[offerPictureInfo objectForKey:@"width"] doubleValue];
                double height = [[offerPictureInfo objectForKey:@"height"] doubleValue];
                if (width!=0||height!=0) {
                    offerModel.offerImageHeight = [NSNumber numberWithInt:((DeviceWidth - LEFTLEFTSET*2)*height)/width];
                }
                if ([offerModel.offerImageHeight floatValue] < 0)
                {
                    offerModel.offerImageHeight = 0;
                }
                offerModel.offerId=[offerInfo objectForKey:@"offerId"];
                offerModel.offerImageUrl=[offerImage objectForKey:@"imageUrl"];
                offerModel.commentCardList = [IMGOfferCard commentListFromArray:[dic objectForKey:@"commentList"] andIsVendor:[[dic objectForKey:@"isVendor"] boolValue] andRestaurant:offerModel ];
                NSDictionary *restaurantDic = [[NSDictionary alloc] initWithObjectsAndKeys:offerModel,[dic objectForKey:@"type"], nil];
                [dataArrM addObject:restaurantDic];
                break;
                
            }

            case 10:
            {
                IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
                restaurant.ratingScore = [dic objectForKey:@"rating"];
                [restaurant setValuesForKeysWithDictionary:dic];
                restaurant.title = [dic objectForKey:@"restaurantTitle"];
                restaurant.imageUrl = [dic objectForKey:@"restaurantImage"];
                NSString *urlString = [restaurant.imageUrl returnFullImageUrlWithWidth:DeviceWidth-2*LEFTLEFTSET];
                [imageUrlsArrM addObject:urlString];
                restaurant.saved=[dic objectForKey:@"isSaved"];
                restaurant.timelineId = [dic objectForKey:@"timelineId"];
                restaurant.homeTimeLineId = [dic objectForKey:@"restaurantUpdateId"];
                restaurant.likeCardCount = [dic objectForKey:@"likeCount"];
                restaurant.commentCardCount = [dic objectForKey:@"commentCount"];
                restaurant.isLikeCard = [[dic objectForKey:@"isLike"] boolValue];
                //                    restaurant.commentCardList = [dic objectForKey:@"commentList"];
                restaurant.commentCardList = [IMGRestaurant commentListFromArray:[dic objectForKey:@"commentList"] andIsVendor:[[dic objectForKey:@"isVendor"] boolValue] andRestaurant:restaurant ];
                
                NSMutableArray *eventArrM = [[NSMutableArray alloc] init];
                NSArray *eventArr = [dic objectForKey:@"restaurantEventList"];
                for (NSDictionary *subDic in eventArr)
                {
                    IMGRestaurantEvent *restaurantEvent = [[IMGRestaurantEvent alloc] init];
                    [restaurantEvent setValuesForKeysWithDictionary:subDic];
                    restaurantEvent.eventId = [subDic objectForKey:@"id"];
                    restaurantEvent.descriptionStr = [subDic objectForKey:@"description"];
                    [restaurantEvent updatePhotoCredit:[subDic objectForKey:@"photoCredit"]];
                    restaurantEvent.restaurantSeo = [dic objectForKey:@"restaurantSeo"];
                    restaurantEvent.restaurantTitle = [dic objectForKey:@"restaurantTitle"];
                    restaurantEvent.restaurantId = restaurant.restaurantId;
                    restaurantEvent.homeTimeLineId=[dic objectForKey:@"restaurantUpdateId"];
                    NSDictionary *pictureInfo = [subDic objectForKey:@"pictureInfo"];
                    double width = [[pictureInfo objectForKey:@"width"] doubleValue];
                    double height = [[pictureInfo objectForKey:@"height"] doubleValue];
                    restaurantEvent.imageHeight = [NSNumber numberWithInt:((DeviceWidth - LEFTLEFTSET*2)*height)/width];
                    
                    if ([restaurantEvent.imageHeight floatValue] < 0)
                    {
                        restaurantEvent.imageHeight = 0;
                    }
                    
                    if ([eventArr count] == 1 && [restaurantEvent.commentCount intValue])
                    {
                        NSArray *commentArray = [subDic objectForKey:@"commentList"];
                        restaurantEvent.commentList = [IMGRestaurantEventComment commentListFromArray:commentArray andIsVendor:[[dic objectForKey:@"isVendor"] boolValue] andRestaurant:restaurant];
                    }
                    [eventArrM addObject:restaurantEvent];
                }
                IMGUpdateCard* updateCardModel=[[IMGUpdateCard alloc]init];
                updateCardModel.restaurant=restaurant;
                updateCardModel.eventArr=eventArrM;
                NSDictionary *restaurantDic = [[NSDictionary alloc] initWithObjectsAndKeys:updateCardModel,[dic objectForKey:@"type"], nil];
                [dataArrM addObject:restaurantDic];
                break;
                
            }
            case 11:{
                
            }
            case 12:{
                
                IMGRestaurant *restaurant=[[IMGRestaurant alloc] init];
                restaurant.title=[dic objectForKey:@"restaurantTitle"];
                restaurant.priceName=[dic objectForKey:@"priceName"];
                restaurant.ratingScore=[NSString stringWithFormat:@"%.0f",[[dic objectForKey:@"rating"] floatValue]/2];
                restaurant.imageUrl=[dic objectForKey:@"restaurantImage"];
                NSString *urlString = [restaurant.imageUrl returnFullImageUrlWithWidth:DeviceWidth-2*LEFTLEFTSET];
                [imageUrlsArrM addObject:urlString];
                restaurant.phoneNumber=[dic objectForKey:@"phone"];
                restaurant.cuisineName=[dic objectForKey:@"cuisineName"];
                restaurant.districtName=[dic objectForKey:@"districtName"];
                restaurant.timeline=[dic objectForKey:@"timeline"];
                restaurant.latitude=[dic objectForKey:@"latitude"];
                restaurant.longitude=[dic objectForKey:@"longitude"];
                restaurant.savedCount=[dic objectForKey:@"savedCount"];
                restaurant.saved=[dic objectForKey:@"isSaved"];
                restaurant.typeName = [dic objectForKey:@"typename"];
                restaurant.seoKeyword = [dic objectForKey:@"restaurantSeo"];
                restaurant.restaurantId = [dic objectForKey:@"restaurantId"];
                restaurant.reviewCount = [dic objectForKey:@"reviewCount"];
                NSDictionary *restaurantDic = [[NSDictionary alloc] initWithObjectsAndKeys:restaurant,[dic objectForKey:@"type"], nil];
                [dataArrM addObject:restaurantDic];
                break;
                
            }
            case 14:
            {
                IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
                restaurant.title = [dic objectForKey:@"restaurantTitle"];
                [restaurant setValuesForKeysWithDictionary:dic];
                restaurant.imageUrl = [dic objectForKey:@"restaurantImage"];
                IMGReview *review = [[IMGReview alloc] init];
                [review setValuesForKeysWithDictionary:[dic objectForKey:@"reviewMap"]];
                review.fullName = [[dic objectForKey:@"reviewMap"] objectForKey:@"userName"];
                //                    review.restaurantTitle = [dic objectForKey:@"restaurantTitle"];
                review.restaurantTitle = restaurant.title;
                review.userDishCount = [dic objectForKey:@"userPhotoCount"];
                review.userReviewCount = [dic objectForKey:@"userReviewCount"];
                review.timeline=[[dic objectForKey:@"reviewMap"] objectForKey:@"timeCreated"];
                review.summarize = [review.summarize stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                
                
                NSArray *commentArr = [[dic objectForKey:@"reviewMap"] objectForKey:@"commentList"];
                review.commentList = [IMGReviewComment commentListFromArray:commentArr andIsVendor:[[dic objectForKey:@"isVendor"] boolValue] andRestaurant:restaurant];
                
                NSMutableArray *dishArrM = [[NSMutableArray alloc] init];
                NSArray *dishArr = [[dic objectForKey:@"reviewMap"] objectForKey:@"dishList"];
                for (NSDictionary *subDic in dishArr)
                {
                    IMGDish *dish = [[IMGDish alloc] init];
                    [dish setValuesForKeysWithDictionary:subDic];
                    dish.descriptionStr = [subDic objectForKey:@"description"];
                    [dish updatePhotoCredit:[subDic objectForKey:@"photoCredit"]];
                    NSDictionary *pictureInfo = [subDic objectForKey:@"pictureInfo"];
                    double width = [[pictureInfo objectForKey:@"width"] doubleValue];
                    double height = [[pictureInfo objectForKey:@"height"] doubleValue];
                    dish.imageHeight = [NSNumber numberWithInt:((DeviceWidth - LEFTLEFTSET*2)*height)/width];
                    if ([dish.imageHeight floatValue] < 0)
                    {
                        dish.imageHeight = 0;
                    }
                    if (dishArr.count == 1)
                    {
                        review.imageHeight = dish.imageHeight;
                        NSString *urlString = [dish.imageUrl returnFullImageUrlWithWidth:DeviceWidth-2*LEFTLEFTSET];
                        [imageUrlsArrM addObject:urlString];
                    }
                    
                    
                    [dishArrM addObject:dish];
                }
                [imageUrlsArrM addObjectsFromArray:[self returnImageUrlsWithDishArr:dishArrM]];

                IMGReviewCard *reviewCard = [[IMGReviewCard alloc] init];
                reviewCard.reviewCardReview = review;
                reviewCard.reviewCardRestaurant = restaurant;
                reviewCard.dishArrM = dishArrM;
                reviewCard.isReadMore = NO;
                NSDictionary *dataDic = [[NSDictionary alloc] initWithObjectsAndKeys:reviewCard,[dic objectForKey:@"type"], nil];
                [dataArrM addObject:dataDic];
                break;
            }
            case 15:
            {
                IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
                restaurant.title = [dic objectForKey:@"restaurantTitle"];
                restaurant.restaurantId = [dic objectForKey:@"restaurantId"];
                restaurant.timeline = [dic objectForKey:@"timeline"];
                restaurant.imageUrl = [dic objectForKey:@"restaurantImage"];
                restaurant.timelineId = [dic objectForKey:@"timelineId"];
                restaurant.likeCardCount = [dic objectForKey:@"likeCount"];
                restaurant.commentCardCount = [dic objectForKey:@"commentCount"];
                restaurant.isLikeCard = [[dic objectForKey:@"isLike"] boolValue];
                //                    restaurant.commentCardList = [dic objectForKey:@"commentList"];
                restaurant.commentCardList = [IMGRestaurant commentListFromArray:[dic objectForKey:@"commentList"] andIsVendor:[[dic objectForKey:@"isVendor"] boolValue] andRestaurant:restaurant ];
                
                
                
                NSMutableArray *dishArrM = [[NSMutableArray alloc] init];
                NSArray *dishArr = [dic objectForKey:@"dishList"];
                for (NSDictionary *subDic in dishArr)
                {
                    IMGDish *dish = [[IMGDish alloc] init];
                    [dish setValuesForKeysWithDictionary:subDic];
                    dish.dishId = [subDic objectForKey:@"id"];
                    dish.restaurantSeo = [dic objectForKey:@"restaurantSeo"];
                    dish.restaurantTitle = [dic objectForKey:@"restaurantTitle"];
                    dish.restaurantId = [dic objectForKey:@"restaurantId"];
                    dish.descriptionStr = [subDic objectForKey:@"description"];
                    dish.userName = [dic objectForKey:@"userName"];
                    dish.showWriteReview = [[dic objectForKey:@"showWriteReview"] boolValue];
                    dish.userId = [dic objectForKey:@"userId"];
                    [dish updatePhotoCredit:[subDic objectForKey:@"photoCredit"]];
                    dish.moderateReviewId=[dic objectForKey:@"moderateReviewId"];
                    NSDictionary *pictureInfo = [subDic objectForKey:@"pictureInfo"];
                    double width = [[pictureInfo objectForKey:@"width"] doubleValue];
                    double height = [[pictureInfo objectForKey:@"height"] doubleValue];
                    dish.imageHeight = [NSNumber numberWithInt:((DeviceWidth - LEFTLEFTSET*2)*height)/width];
                    if ([dish.imageHeight floatValue] < 0)
                    {
                        dish.imageHeight = 0;
                    }
                    if ([dishArr count] == 1 && [dish.commentCount intValue])
                    {
                        NSArray *commentArray=[subDic objectForKey:@"commentList"];
                        dish.commentList = [IMGDishComment commentListFromArray:commentArray andIsVendor:[[dic objectForKey:@"isVendor"] boolValue] andRestaurant:restaurant];
                    }
                    
                    
                    [dishArrM addObject:dish];
                }
                
               [imageUrlsArrM addObjectsFromArray:[self returnImageUrlsWithDishArr:dishArrM]];
                
                IMGUser *user = [[IMGUser alloc] init];
                [user setValuesForKeysWithDictionary:dic];
                user.reviewCount = [dic objectForKey:@"userReviewCount"];
                user.photoCount = [dic objectForKey:@"userPhotoCount"];
                user.moderateReviewId = [dic objectForKey:@"moderateReviewId"];
                IMGUploadPhotoCard *uploadPhotoCard = [[IMGUploadPhotoCard alloc] init];
                uploadPhotoCard.uploadCardUser = user;
                uploadPhotoCard.uploadCardRestaurant = restaurant;
                uploadPhotoCard.dishArrM = dishArrM;
                 NSDictionary *dataDic = [[NSDictionary alloc] initWithObjectsAndKeys:uploadPhotoCard,[dic objectForKey:@"type"], nil];
                [dataArrM addObject:dataDic];
                break;
                
            }
            case 16:
            {
                IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
                restaurant.ratingScore = [dic objectForKey:@"rating"];
                [restaurant setValuesForKeysWithDictionary:dic];
                restaurant.title = [dic objectForKey:@"restaurantTitle"];
                restaurant.imageUrl = [dic objectForKey:@"restaurantImage"];
                
                restaurant.saved=[dic objectForKey:@"isSaved"];
                NSMutableArray *menuArrM = [[NSMutableArray alloc] init];
                NSArray *menuArr = [dic objectForKey:@"menuPhotoList"];
                for (NSDictionary *subDic in menuArr)
                {
                    IMGMenu *menu = [[IMGMenu alloc] init];
                    [menu setValuesForKeysWithDictionary:subDic];
                    menu.menuId = [subDic objectForKey:@"id"];
                    menu.restaurantSeo = [dic objectForKey:@"restaurantSeo"];
                    menu.restaurantTitle = [dic objectForKey:@"restaurantTitle"];
                    menu.restaurantId = restaurant.restaurantId;
                    menu.type = [subDic objectForKey:@"type"];
                    [menuArrM addObject:menu];
                }
                [self returnImageUrlsWithDishArr:menuArrM];
                IMGMenuCard *menuCard = [[IMGMenuCard alloc] init];
                menuCard.restaurant =restaurant;
                menuCard.menuArr= menuArrM;
                NSDictionary *restaurantDic = [[NSDictionary alloc] initWithObjectsAndKeys:menuCard,[dic objectForKey:@"type"], nil];
                [dataArrM addObject:restaurantDic];
                
            }
                break;
            default:
            {
                
            }
                break;
        }
    }
    
//    [[SDWebImagePrefetcher sharedImagePrefetcher] prefetchURLs:imageUrlsArrM];
    NSDictionary *dic = @{@"data":dataArrM,@"imageUrls":imageUrlsArrM};
    return dic;
}
+ (NSMutableArray *)returnImageUrlsWithDishArr:(NSMutableArray *)dishArrM
{
    float twoPhotoWidth = ((DeviceWidth - LEFTLEFTSET*2-1)/2.0);
    float threePhotoWidth = ((DeviceWidth - LEFTLEFTSET*2-2)/3.0);
    
    NSMutableArray *imageUrlsArrM = [[NSMutableArray alloc] init];
    
    for (int i = 0; i<dishArrM.count; i++)
    {
        IMGEntity *dish = [dishArrM objectAtIndex:i];
        if (dishArrM.count == 1)
        {
            NSString *urlString = [dish.imageUrl returnFullImageUrlWithWidth:DeviceWidth-2*LEFTLEFTSET];
            [imageUrlsArrM addObject:urlString];
        }
        else if (dishArrM.count == 2)
        {
            NSString *urlString = [dish.imageUrl returnFullImageUrlWithWidth:twoPhotoWidth];
            [imageUrlsArrM addObject:urlString];
        }
        else if (dishArrM.count == 3)
        {
            NSString *urlString = [dish.imageUrl returnFullImageUrlWithWidth:threePhotoWidth];
            [imageUrlsArrM addObject:urlString];
        }
        else if (dishArrM.count == 4)
        {
            NSString *urlString = [dish.imageUrl returnFullImageUrlWithWidth:twoPhotoWidth];
            [imageUrlsArrM addObject:urlString];
        }
        else if (dishArrM.count == 5)
        {
            if (i<3)
            {
                NSString *urlString = [dish.imageUrl returnFullImageUrlWithWidth:threePhotoWidth];
                [imageUrlsArrM addObject:urlString];
            }
            else
            {
                NSString *urlString = [dish.imageUrl returnFullImageUrlWithWidth:twoPhotoWidth];
                [imageUrlsArrM addObject:urlString];
            }
        }
        else if (dishArrM.count >= 6)
        {
            NSString *urlString = [dish.imageUrl returnFullImageUrlWithWidth:threePhotoWidth];
            [imageUrlsArrM addObject:urlString];
        }
    }
    return imageUrlsArrM;
}
@end


