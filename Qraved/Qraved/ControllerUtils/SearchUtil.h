//
//  SearchUtil.h
//  Qraved
//
//  Created by Jeff on 12/17/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGDiscover.h"
#import "IMGRestaurant.h"
#import "IMGDistrict.h"
#import <MapKit/MapKit.h>
#import "BrandModel.h"

#define SEARCH_RESULT_PAGE_SIZE 25

@interface SearchUtil : NSObject

+(void)isfromEventsearch:(IMGDiscover *)discover offset:(long)offset minLatitude:(NSNumber *)minLatitude maxLatitude:(NSNumber *)maxLatitude minLongitude:(NSNumber *)minLongitude maxLongitude:(NSNumber *)maxLongitude andBlock:(void (^)(NSArray * restaurantArray,NSDictionary *suggestDic))block;

+(void)isfromEventsearch:(IMGDiscover *)discover offset:(long)offset max:(int)max  minLatitude:(NSNumber *)minLatitude maxLatitude:(NSNumber *)maxLatitude minLongitude:(NSNumber *)minLongitude maxLongitude:(NSNumber *)maxLongitude andBlock:(void (^)(NSArray * restaurantArray,NSDictionary *suggestDic))block ;

+(void)journalSearch:(NSDictionary *)parameters andBlock:(void(^) (NSArray *journalDataArray))block;

+(NSMutableArray *) searchedRestaurantArray;
+(int)searchedRestaurantCount;
+ (NSMutableDictionary *) searchedRestaurantOfferDic;
+(void)filterFromServerWithSearchText:(NSString *)searchText andBlock:(void(^) (NSArray *districtArray,NSArray *restaurantArray,NSArray *cuisineArray,NSArray *foodTypeArray))block;
+(void)journalFilterFromServerWithSearchText:(NSString *)searchText andBlock:(void(^) (NSArray *journalDataArray))block;

+(MKCoordinateRegion)regionOfCity;
+(MKCoordinateRegion)regionOfDistrict:(NSNumber *)districtId;

+(NSMutableArray *)filteredRestaurantArray:(NSString *)title;
+(BOOL)existViewed;
+(void)listViewed:(void (^)(NSArray * restaurantArray,BOOL recently))block;
+(void)viewed:(IMGRestaurant *)restaurant cityId:(NSNumber *)cityId;
+ (void)getOffersFromResponseObject:(NSArray *)responseObject andOffset:(long)offset;


+(void)getSearchResault:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array, NSNumber *restaurantCount))successBlock anderrorBlock:(void(^)())errorBlock;

+ (void)getOffers:(void(^)(NSString *offs,NSString *offers))successBlock anderrorBlock:(void(^)())errorBlock;



@end
