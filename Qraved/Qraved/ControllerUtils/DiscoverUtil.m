//
//  DiscoverUtil.m
//  Qraved
//
//  Created by Laura on 14/12/23.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "DiscoverUtil.h"
#import "DBManager.h"
#import "IMGDiscoverEntity.h"
#import "IMGDiscover.h"

@implementation DiscoverUtil
+(void)refreshDiscoverAndHandle:(void (^)(NSArray *discoverArray))block
{
    NSNumber *cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    BOOL needRefresh=FALSE;
    NSDate *lastRefreshTime=[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"REFRESH_DISCOVER_INTERVAL%@",cityId ]];
    if (lastRefreshTime == nil){
        needRefresh=TRUE;
    }
    if (((0-[lastRefreshTime timeIntervalSinceNow])/60) >=60*2) {
       needRefresh=TRUE;
    }
    
    if(needRefresh){

        NSDictionary *parameters = [[NSDictionary alloc]initWithObjectsAndKeys:cityId,@"cityID", nil];
        [[IMGNetWork sharedManager] GET:@"user/discover/list/detail" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
            NSString *exceptionMessage = [responseObject objectForKey:@"exceptionmsg"];
            if(exceptionMessage!=nil){
                block ([DiscoverUtil addDiscoverToArray]);
                NSLog(@"initDiscoverCategory erors when get datas, exceptionmsg is:%@",exceptionMessage);
                return;
            }
            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:[NSString stringWithFormat:@"REFRESH_DISCOVER_INTERVAL"]];
            
            FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueue];
            [queue inDatabase:^(FMDatabase *db) {
                [db executeUpdate:@"delete from IMGDiscoverEntity"];
                NSArray *returnDiscoverArray=[responseObject objectForKey:@"discoverDetailList"];
                for(NSDictionary *discoverDictionary in returnDiscoverArray){
                    IMGDiscoverEntity *discoverEntity=[[IMGDiscoverEntity alloc]init];
                    [discoverEntity initFromDictionary:discoverDictionary];
                    if ([@"Mostbooked" isEqualToString:discoverEntity.sortby]) {
                        discoverEntity.sortby = SORTBY_BOOKINGS;
                    }else if ([@"Bestoffer" isEqualToString:discoverEntity.sortby]){
                        discoverEntity.sortby = SORTBY_OFFERS;
                    }else if ((![@"Popularity" isEqualToString:discoverEntity.sortby])
                              &&(![@"Offers" isEqualToString:discoverEntity.sortby])
                              &&(![@"Bookings" isEqualToString:discoverEntity.sortby])
                              &&(![@"Rating" isEqualToString:discoverEntity.sortby])
                              &&(![@"Distance" isEqualToString:discoverEntity.sortby])
                              &&(![@"Relevance" isEqualToString:discoverEntity.sortby])){
                        discoverEntity.sortby = SORTBY_POPULARITY;
                    }
                    discoverEntity.discoverId = [discoverDictionary objectForKey:@"id"];
                    [[DBManager manager] insertModel:discoverEntity selectField:@"discoverId" andSelectID:discoverEntity.discoverId  andFMDatabase:db];
                }
                block ([DiscoverUtil addDiscoverToArray]);
            }];
        }failure:^(NSURLSessionDataTask *operation, NSError *error) {
            block ([DiscoverUtil addDiscoverToArray]);
            NSLog(@"initDiscoverCategory erors when get datas:%@",error.localizedDescription);
        }];
    }else{
        block ([DiscoverUtil addDiscoverToArray]);
    }
}
+(NSArray *)addDiscoverToArray{
    
    NSMutableArray* discoverArray = [[NSMutableArray alloc]init];
    NSString *discoverSql = @"select * from IMGDiscoverEntity;";
    FMResultSet *resultSet=[[DBManager manager]executeQuery:discoverSql];
    
    while ([resultSet next]) {
        IMGDiscoverEntity *discoverEntity=[[IMGDiscoverEntity alloc]init];
        [discoverEntity setValueWithResultSet:resultSet];
        IMGDiscover *tempDiscover = [[IMGDiscover alloc]init];
        [tempDiscover initFromDiscoverEntity:discoverEntity];
        [discoverArray addObject:tempDiscover];
    }
    [resultSet close];
    return discoverArray;
    
}
@end
