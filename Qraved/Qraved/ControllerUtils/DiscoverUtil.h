//
//  DiscoverUtil.h
//  Qraved
//
//  Created by Laura on 14/12/23.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DiscoverUtil : NSObject
+(void)refreshDiscoverAndHandle:(void (^)(NSArray *discoverArray))block;

@end
