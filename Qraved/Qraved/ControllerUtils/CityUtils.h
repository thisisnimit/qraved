//
//  CityUtils.h
//  Qraved
//
//  Created by Laura on 15/2/9.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityUtils : NSObject
+(void)getCitiesWithCountryId:(NSString *)countyId andBlock:(void (^)(NSArray *cityArray))block;
@end
