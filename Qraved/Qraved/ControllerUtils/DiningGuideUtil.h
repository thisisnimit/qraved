//
//  DiningGuideUtil.h
//  Qraved
//
//  Created by Laura on 14/12/23.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DiningGuideUtil : NSObject

+(void)getDiningGuideListWithCityId:(NSNumber *)cityId andMax:(NSNumber *)max andOffset:(NSNumber *)offset andBlock:(void (^)(NSArray *dataArray,BOOL hasData))block andFailure:(void(^)(NSString *description))failureBlock;
+(void)getDiningGuideRestaurants:(NSDictionary*)parameters completion:(void(^)(NSNumber *total,NSArray *restaurants,IMGDiningGuide *guide))completionBlock failure:(void(^)(NSString *description))failureBlock;

+ (void)viewedGuide:(IMGDiningGuide *)guide;

@end
