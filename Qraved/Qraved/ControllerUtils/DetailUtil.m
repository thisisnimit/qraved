//
//  DetailViewControllerUtil.m
//  Qraved
//
//  Created by Jeff on 12/11/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "DetailUtil.h"
#import "RestaurantHandler.h"
#import "DBManager.h"
#import "IMGUser.h"
#import "IMGRestaurantViewed.h"

@implementation DetailUtil


+(void)checkAndHandle:(NSNumber*)restaurantId andPassBlock:(void (^)(IMGRestaurant * restaurant))passBlock andUnPassBlock:(void (^)(IMGRestaurant * restaurant))unPassBlock {
    NSDate *lastRefreshTime=[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"REFRESH_RESTAURANT_INTERVAL_%@",restaurantId]];
    if(lastRefreshTime!=nil){
        NSLog(@"the time now is:%f minutes ago",(0-[lastRefreshTime timeIntervalSinceNow])/60);
    }
    IMGRestaurant *tmpRestaurant=[IMGRestaurant findById:restaurantId];
    if(tmpRestaurant==nil){
//    if(lastRefreshTime==nil || ((0-[lastRefreshTime timeIntervalSinceNow])/60) >=60*24*14){
        unPassBlock(nil);
    }else{
        passBlock(nil);
    }
}

+(void)refreshAndHandle:(NSNumber*)restaurantId andBlock:(void (^)(IMGRestaurant * _restaurant))block {
    NSDate *lastRefreshTime=[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"REFRESH_RESTAURANT_INTERVAL_%@",restaurantId]];
    if(lastRefreshTime!=nil){
        NSLog(@"the time now is:%f minutes ago",(0-[lastRefreshTime timeIntervalSinceNow])/60);
    }
    // IMGRestaurant *tmpRestaurant=[IMGRestaurant findById:restaurantId];
    // if(tmpRestaurant==nil){
        [RestaurantHandler getDetailFromServer:restaurantId andBlock:^(IMGRestaurant *tmpRestaurant) {
            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:[NSString stringWithFormat:@"REFRESH_RESTAURANT_INTERVAL_%ld",(long)restaurantId]];
            IMGRestaurant *restaurant=[IMGRestaurant findById:restaurantId];
            if (restaurant!=nil) {
                block(restaurant);
            }
        }];
    // }else{
        // [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:[NSString stringWithFormat:@"REFRESH_RESTAURANT_INTERVAL_%@",restaurantId]];
        // block(tmpRestaurant);
    // }
}

+(void)refreshDetailViewController:(NSNumber*)restaurantId andBlock:(void (^)(IMGRestaurant * restaurant))block {
    NSDate *lastRefreshTime=[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"REFRESH_RESTAURANT_INTERVAL_%@",restaurantId]];
    if(lastRefreshTime!=nil){
        NSLog(@"the time now is:%f minutes ago",(0-[lastRefreshTime timeIntervalSinceNow])/60);
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:[NSString stringWithFormat:@"REFRESH_RESTAURANT_INTERVAL_%@",restaurantId]];
    
    [RestaurantHandler getDetailFromServer:restaurantId andBlock:^(IMGRestaurant *tmpRestaurant) {
        IMGRestaurant *restaurant=[[IMGRestaurant alloc]init];
        FMResultSet *resultSet = [[DBManager manager] executeQuery:[NSString stringWithFormat:@"SELECT * FROM IMGRestaurant WHERE restaurantId =  %@;",restaurantId]];
        if ([resultSet next]) {
            [restaurant setValueWithResultSet:resultSet];
            [resultSet close];
            
            block(restaurant);
        }
    }];

}


@end
