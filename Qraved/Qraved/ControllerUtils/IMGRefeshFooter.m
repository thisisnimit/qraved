//
//  IMGRefeshFooter.m
//  
//
//  Created by apple on 17/3/6.
//
//

#import "IMGRefeshFooter.h"

#define TEXT_COLOR	 [UIColor colorWithRed:87.0/255.0 green:108.0/255.0 blue:137.0/255.0 alpha:1.0]

@interface IMGRefeshFooter()
{
}
@property (weak, nonatomic) UIActivityIndicatorView *loadingView;
@end

@implementation IMGRefeshFooter
#pragma mark - 懒加载子控件



- (void)setActivityIndicatorViewStyle:(UIActivityIndicatorViewStyle)activityIndicatorViewStyle
{
    
    self.loadingView = nil;
    [self setNeedsLayout];
}
#pragma mark - 重写父类的方法
- (void)prepare
{
    [super prepare];
    
    self.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    self.stateLabel.textColor = TEXT_COLOR;
}

- (void)placeSubviews
{
    [super placeSubviews];
    
}

- (void)setState:(MJRefreshState)state
{
    MJRefreshCheckState
    
    // 根据状态做事情
    self.stateLabel.text = @"";
}

@end

