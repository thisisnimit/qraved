//
//  DetailViewControllerUtil.h
//  Qraved
//
//  Created by Jeff on 12/11/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGRestaurant.h"

@interface DetailUtil : NSObject

+(void)checkAndHandle:(NSNumber*)restaurantId andPassBlock:(void (^)(IMGRestaurant * restaurant))passBlock andUnPassBlock:(void (^)(IMGRestaurant * restaurant))unPassBlock ;
+(void)refreshAndHandle:(NSNumber*)restaurantId andBlock:(void (^)(IMGRestaurant * restaurant))block;

+(void)refreshDetailViewController:(NSNumber*)restaurantId andBlock:(void (^)(IMGRestaurant * restaurant))block;

@end
