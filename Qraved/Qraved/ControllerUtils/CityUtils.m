//
//  CityUtils.m
//  Qraved
//
//  Created by Laura on 15/2/9.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "CityUtils.h"
#import "IMGCity.h"
#import "CityHandler.h"
#import "DBManager.h"

@implementation CityUtils
+(void)getCitiesWithCountryId:(NSString *)countyId andBlock:(void (^)(NSArray *cityArray))block;
{
    NSMutableArray *cities = [[NSMutableArray alloc]init];
    FMResultSet *resultSet = [[DBManager manager]executeQuery:@"select * from IMGCity order by name asc"];
    while ([resultSet next]) {
        IMGCity *city=[[IMGCity alloc]init];
        [city setValueWithResultSet:resultSet];
        [cities addObject:city];
    }
    [resultSet close];
    
    block(cities);
    
    [[CityHandler alloc]getDatasFromServer];
}
@end
