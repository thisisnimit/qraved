//
//  HomeUtil.h
//  Qraved
//
//  Created by Jeff on 12/23/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGSplash.h"
#import "IMGReview.h"
#import "IMGDish.h"
#import "IMGRestaurant.h"
#import "IMGMediaComment.h"
#import "IMGRestaurantEvent.h"
@interface HomeUtil : NSObject

+(void)refreshAndHandle:(void (^)(NSArray *eventArray,NSArray *diningGuideArray))block;
+(NSArray *)addEventToArray;

+ (void)getEventsFromService:(void (^)(NSArray *eventArray))block;
+ (void)getJournalFromService:(void (^)(NSArray *journalArray))block;
+ (void)getNearYouFromService:(void (^)(NSArray *nearYouArray))block;
+ (void)getDiningGuideFromService:(void (^)(NSArray *diningGuideArray))block;
+ (void)getRestaurantWithOfferFromService:(void (^)(NSArray *restaurantArray))block;

+ (void)getSingleDiningGuideFromServiceWithDiningGuideId:(NSNumber *)diningGuideId andBlock:(void (^)(IMGDiningGuide *guide))block;
+ (void)getSingleEventFromServiceWithEventId:(NSNumber *)eventId andBlock:(void (^)())block failure:(void(^)())failure;

+ (void)getSplashWithBlock:(void (^)(IMGSplash *splash))block;
+(void)getLikeListFromServiceWithReviewId:(NSNumber*)reviewId andMax:(NSNumber*)max andoffset:(NSNumber*)offset andBlock:(void(^)(NSArray *likeListArray,NSNumber* likeviewcount))block;
+(void)getLikeListFromServiceWithDishId:(NSNumber*)dishId andMax:(NSNumber*)max andoffset:(NSNumber*)offset andBlock:(void(^)(NSArray *likeListArray,NSNumber* likeviewcount))block;
+(void)getLikeListFromServiceWithResturantId:(NSNumber*)restaurantID andMax:(NSNumber*)max andoffset:(NSNumber*)offset andBlock:(void(^)(NSArray *likeListArray,NSNumber* likeviewcount))block;
+(void)getLikeListFromServiceWithJournalArticleId:(IMGMediaComment*)MediaComment andMax:(NSNumber*)max andoffset:(NSNumber*)offset andBlock:(void(^)(NSArray *likeListArray,NSNumber* likeviewcount))block;
+(void)getLikeListMuchPhotoFromServiceWithmoderateReviewId:(NSNumber*)moderateReviewId andMax:(NSNumber*)max andoffset:(NSNumber*)offset andBlock:(void(^)(NSArray *likeListArray,NSNumber* likeviewcount))block;
+(void)getLikeListFromServiceWithOfferCardId:(NSNumber*)OfferCardId andMax:(NSNumber*)max andoffset:(NSNumber*)offset andBlock:(void(^)(NSArray *likeListArray,NSNumber* likeviewcount))block;
+(void)getLikeListMuchPhotoFromServiceWithHomeTimelineRestaurantUpdateId:(NSNumber*)homeTimelineRestaurantUpdateId andMax:(NSNumber*)max andoffset:(NSNumber*)offset andBlock:(void(^)(NSArray *likeListArray,NSNumber* likeviewcount))block;
+(void)getBookRatingSplashWithBlock:(void(^)(NSMutableArray *splashArray))block;
+(void)postBookRatingScore:(NSDictionary*)parameters andBlock:(void(^)(BOOL isSucceed))block;
// 2.5.3
+ (void)getNewHomeDataFromService:(NSDictionary*)parameters andBlock:(void (^)(NSArray *dataArr,int minId,NSArray *imageUrlsArr))block;

+ (NSDictionary *)processingData:(id)responseObject;
+(void)getversionInfoFromService:(NSDictionary*)parameters andBlock:(void(^)(id responese))block andErroBlock:(void(^)(NSError* error))errorBlock;
+(void)getDownloadInfoFromService:(NSDictionary*)parameters andBlock:(void(^)(id responese))block andErroBlock:(void(^)(NSError* error))errorBlock;
@end
