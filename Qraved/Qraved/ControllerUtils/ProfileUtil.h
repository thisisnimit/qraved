//
//  ProfileUtil.h
//  Qraved
//
//  Created by Jeff on 12/25/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGUser.h"

@interface ProfileUtil : NSObject

+(void)getUpcomingBookingsWithUser:(IMGUser *)user andBlock:(void(^)(NSArray*))block;
+(void)getUpcomingAndHistoryBookings:(void(^)(NSArray *upcomings,NSArray *histories,NSNumber *historyTotal))block;
+(void)getHisotryBookings:(NSDictionary*)parameters block:(void(^)(NSArray *histories,NSNumber *total))block;
@end
