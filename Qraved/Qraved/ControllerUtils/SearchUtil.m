//
//  SearchUtil.m
//  Qraved
//
//  Created by Jeff on 12/17/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "SearchUtil.h"
#import "RestaurantHandler.h"
#import "AppDelegate.h"
#import "BookUtil.h"
#import "IMGDistrictAndLandmark.h"
#import "IMGCuisineAndFoodType.h"
#import "IMGTag.h"
#import "IMGTagUpdate.h"
#import "IMGRestaurant.h"
#import "IMGUser.h"
#import "IMGRestaurantViewed.h"
#import "IMGRestaurantEvent.h"
#import "IMGMediaComment.h"
#import "NSString+Helper.h"
#import "V2_DiscoverListResultRestaurantModel.h"

static NSMutableArray *restaurantArray;
static NSMutableArray *existRestauantIdArray;
static NSMutableDictionary *restaurantOfferDic;
static int fetchedRestaurantCount;
BOOL isRefresh;

@implementation SearchUtil

+(NSMutableArray *) searchedRestaurantArray{
    return restaurantArray;
}

+ (NSMutableDictionary *) searchedRestaurantOfferDic{
    return restaurantOfferDic;
}

+(int)searchedRestaurantCount{
    return fetchedRestaurantCount;
}

+(void)isfromEventsearch:(IMGDiscover *)discover offset:(long)offset minLatitude:(NSNumber *)minLatitude maxLatitude:(NSNumber *)maxLatitude minLongitude:(NSNumber *)minLongitude maxLongitude:(NSNumber *)maxLongitude andBlock:(void (^)(NSArray * restaurantArray,NSDictionary *suggestDic))block {
    [self isfromEventsearch:discover offset:offset max:SEARCH_RESULT_PAGE_SIZE minLatitude:minLatitude maxLatitude:maxLatitude minLongitude:minLongitude maxLongitude:maxLongitude andBlock:^(NSArray *_restaurantArray,NSDictionary *suggestDic) {
        block(_restaurantArray,suggestDic);
    }];
    
}
+(void)isfromEventsearch:(IMGDiscover *)discover offset:(long)offset max:(int)max minLatitude:(NSNumber *)minLatitude maxLatitude:(NSNumber *)maxLatitude minLongitude:(NSNumber *)minLongitude maxLongitude:(NSNumber *)maxLongitude andBlock:(void (^)(NSArray * restaurantArray,NSDictionary *suggestDic))block {
   
    NSMutableDictionary *parameters= [[NSMutableDictionary alloc]init];
    if(minLatitude!=nil){
        [parameters setObject:minLatitude forKey:@"minLatitude"];
    }
    if(maxLatitude!=nil){
        [parameters setObject:maxLatitude forKey:@"maxLatitude"];
    }
    if(minLongitude!=nil){
        [parameters setObject:minLongitude forKey:@"minLongitude"];
    }
    if(maxLongitude!=nil){
        [parameters setObject:maxLongitude forKey:@"maxLongitude"];
    }
    [parameters setObject:[NSNumber numberWithLong:offset] forKey:@"offset"];
    [parameters setObject:[NSNumber numberWithInt:max] forKey:@"max"];
    if([discover.sortby isEqualToString:@"Distance"]||[discover.sortby isEqualToString:@"distance"]){
        [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
        [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]  forKey:@"longitude"];        //        [parameters setObject:[NSNumber numberWithDouble:-6.2027181] forKey:@"latitude"];
        //        [parameters setObject:[NSNumber numberWithDouble:106.823298] forKey:@"longitude"];
        [parameters setObject:@"asc" forKey:@"order"];
    }
    //    else if ([discover.sortby isEqualToString:@"Offers"])
    else
    {
        [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
        [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]  forKey:@"longitude"];
    }
    NSLog(@"latitude = %f",[AppDelegate ShareApp].locationManager.location.coordinate.latitude);
    if(discover.sortby!=nil){
        if([discover.sortby isEqualToString:SORTBY_POPULARITY]){
            
            [parameters setObject:@"popularity" forKey:@"sortby"];
        }else if([discover.sortby isEqualToString:@"Offers"]){
            
            [parameters setObject:@"bestOffer" forKey:@"sortby"];
        }else if([discover.sortby isEqualToString:@"Bookings"]){
            
            [parameters setObject:@"mostBooked" forKey:@"sortby"];
        }else if([discover.sortby isEqualToString:@"Rating"]){
            
            [parameters setObject:@"rating" forKey:@"sortby"];
        }else if([discover.sortby isEqualToString:@"Distance"]){
            
            [parameters setObject:@"distance" forKey:@"sortby"];
        }else if([discover.sortby isEqualToString:@"Relevance"]){
            
            [parameters setObject:@"relevance" forKey:@"sortby"];
        }else{
            [parameters setObject:discover.sortby forKey:@"sortby"];
        }
    }else{
        [parameters setObject:@"popularity" forKey:@"sortby"];
    }
    if ([discover.openNowToggle intValue]>0) {
        [parameters setObject:@"1" forKey:@"openNowToggle"];
    }
    if ([discover.myListToggle intValue]>0) {
        [parameters setObject:@"1" forKey:@"myListToggle"];
    }
    if(discover.eventArray!=nil && [discover.eventArray count]>0){
        IMGOfferType *offerType = [discover.eventArray objectAtIndex:0];
        NSString *valueStr = [NSString stringWithFormat:@"%@",offerType.offerId];
        for(int i=1;i<discover.districtArray.count;i++){
            offerType = [discover.districtArray objectAtIndex:i];
            valueStr = [valueStr stringByAppendingString:[NSString stringWithFormat:@",%@",offerType.offerId]];
        }
        [parameters setObject:valueStr forKey:@"eventIDs"];
    }
    
    if(discover.searchDistrictArray!=nil && [discover.searchDistrictArray count]>0&&[discover.searchDistrictArray.firstObject isKindOfClass:NSClassFromString(@"IMGDistrict")]){
        IMGDistrict *district = [discover.searchDistrictArray objectAtIndex:0];
       NSNumber *districtId = district.districtId;
//        NSNumber *districtId =[discover.districtArray objectAtIndex:0];
        NSString *valueStr = [NSString stringWithFormat:@"%@",districtId];
        for(int i=1;i<discover.searchDistrictArray.count;i++){
            IMGDistrict *tmpDistrict = [discover.searchDistrictArray objectAtIndex:i];
            districtId = tmpDistrict.districtId;
            valueStr = [valueStr stringByAppendingString:[NSString stringWithFormat:@",%@",districtId]];
        }
        [parameters setObject:valueStr forKey:@"districtIDs"];
    }

    
    if(discover.bookTime!=nil&&![discover.bookTime isEqualToString:@""]){
        [parameters setObject:discover.bookTime forKey:@"bookTime"];
    }
    
    if(discover.pax!=nil){
        [parameters setObject:discover.pax forKey:@"pax"];
    }
    
    if(discover.bookDate!=nil){
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *bookDateString = [dateFormatter stringFromDate:discover.bookDate];
        [parameters setObject:bookDateString forKey:@"bookDate"];
    }
    
    if(discover.offerTypeArray!=nil && [discover.offerTypeArray count]>0){
        IMGOfferType *offerType = [discover.offerTypeArray objectAtIndex:0];
        NSString *valueStr = [NSString stringWithFormat:@"%@",offerType.typeId];
        for(int i=1;i<discover.offerTypeArray.count;i++){
            offerType = [discover.offerTypeArray objectAtIndex:i];
            valueStr = [valueStr stringByAppendingString:[NSString stringWithFormat:@",%@",offerType.typeId]];
        }
        [parameters setObject:valueStr forKey:@"offerTypes"];
    }
    
    if(discover.cuisineArray!=nil && [discover.cuisineArray count]>0){
        IMGCuisine *cuisine = [discover.cuisineArray objectAtIndex:0];
        NSString *valueStr = [NSString stringWithFormat:@"%@",cuisine.cuisineId];
        for(int i=1;i<discover.cuisineArray.count;i++){
            cuisine = [discover.cuisineArray objectAtIndex:i];
            valueStr = [valueStr stringByAppendingString:[NSString stringWithFormat:@",%@",cuisine.cuisineId]];
        }
        [parameters setObject:valueStr forKey:@"cuisineIDs"];

    }
    
    if(discover.offArray!=nil && [discover.offArray count]>0){
        IMGOfferType *offerType = [discover.offArray objectAtIndex:0];
        NSString *valueStr = [NSString stringWithFormat:@"%@",offerType.off];
        for(int i=1;i<discover.offArray.count;i++){
            offerType = [discover.offArray objectAtIndex:i];
            valueStr = [valueStr stringByAppendingString:[NSString stringWithFormat:@",%@",offerType.off]];
        }
        [parameters setObject:valueStr forKey:@"offs"];
    }
    
    if(discover.areaArray!=nil && [discover.areaArray count]>0){
        if ([[discover.areaArray objectAtIndex:0] isKindOfClass:[IMGArea class]]) {
            IMGArea *area = [discover.areaArray objectAtIndex:0];
            NSString *valueStr = [NSString stringWithFormat:@"%@",area.tagId];
            for(int i=1;i<discover.areaArray.count;i++){
                area = [discover.areaArray objectAtIndex:i];
                valueStr = [valueStr stringByAppendingString:[NSString stringWithFormat:@",%@",area.tagId]];
            }
            [parameters setObject:valueStr forKey:@"areaIDs"];
        }
    }
    
    NSMutableArray *tagArray = [[NSMutableArray alloc]initWithCapacity:0];
    NSMutableArray *tagUpdateArray = [[NSMutableArray alloc]initWithCapacity:0];

    
    if(discover.tagsArr!=nil && [discover.tagsArr count]>0){
        for(int i=0;i<discover.tagsArr.count;i++){
            IMGTag *tag = [discover.tagsArr objectAtIndex:i];
            [tagArray addObject:tag];
        }
    }
    if(discover.tagUpdateArr!=nil && [discover.tagUpdateArr count]>0){
        for(int i=0;i<discover.tagUpdateArr.count;i++){
            IMGTagUpdate *tag = [discover.tagUpdateArr objectAtIndex:i];
            [tagUpdateArray addObject:tag];
        }
    }

    if(discover.occasionArray!=nil && [discover.occasionArray count]>0){
        for(int i=0;i<discover.occasionArray.count;i++){
            IMGTag *tag = [discover.occasionArray objectAtIndex:i];
            [tagArray addObject:tag];
        }
    }
    
    if(discover.landmarkArray!=nil && [discover.landmarkArray count]>0){
        for(int i=0;i<discover.landmarkArray.count;i++){
            IMGTag *tag = [discover.landmarkArray objectAtIndex:i];
            [tagArray addObject:tag];
        }
    }
    
    if(discover.foodTypeArray!=nil && [discover.foodTypeArray count]>0){
        for(int i=0;i<discover.foodTypeArray.count;i++){
            IMGTag *tag = [discover.foodTypeArray objectAtIndex:i];
            [tagArray addObject:tag];
        }
    }
   
    if (discover.ratingNumber!=nil) {
    [parameters setObject:discover.ratingNumber forKey:@"rating"];
    
    }
    if (tagUpdateArray!=nil&&tagUpdateArray.count>0) {
        if ([[tagUpdateArray objectAtIndex:0] isKindOfClass:[IMGTagUpdate class]]){
            
            IMGTagUpdate *tagUpdate = [tagUpdateArray objectAtIndex:0];
            [parameters setObject:tagUpdate.tagTypeId forKey:@"tagType"];
            
        }
    }
    
    if(tagArray!=nil&&tagArray.count>0){
        if ([[tagArray objectAtIndex:0] isKindOfClass:[NSNumber class]])
        {
            NSNumber *tagId = [tagArray objectAtIndex:0];
            NSString *valueStr = [NSString stringWithFormat:@"%@",tagId];
            for(int i=1;i<tagArray.count;i++){
                tagId = [tagArray objectAtIndex:i];
                valueStr = [valueStr stringByAppendingString:[NSString stringWithFormat:@",%@",tagId]];
            }
            [parameters setObject:valueStr forKey:@"tagIDs"];
        }else
        {
            IMGTag *tag = [tagArray objectAtIndex:0];
            NSString *valueStr = [NSString stringWithFormat:@"%@",tag.tagId];
            for(int i=1;i<tagArray.count;i++){
                tag = [tagArray objectAtIndex:i];
                valueStr = [valueStr stringByAppendingString:[NSString stringWithFormat:@",%@",tag.tagId]];
            }
            [parameters setObject:valueStr forKey:@"tagIDs"];
        }
        
    }
    
    if(discover.priceLevelArray!=nil && discover.priceLevelArray.count>0){
        IMGPriceLevel *priceLevel = [discover.priceLevelArray objectAtIndex:0];
        NSNumber *startPrice = priceLevel.priceLevelId;
        NSNumber *endPrice = priceLevel.priceLevelId;
        
        for(int i=1;i<discover.priceLevelArray.count;i++){
            priceLevel = [discover.priceLevelArray objectAtIndex:i];
            if([priceLevel.priceLevelId intValue]<[startPrice intValue]){
                startPrice=priceLevel.priceLevelId;
            }
            if([priceLevel.priceLevelId intValue]>[endPrice intValue]){
                endPrice=priceLevel.priceLevelId;
            }
        }
        [parameters setObject:startPrice forKey:@"startPrice"];
        [parameters setObject:endPrice forKey:@"endPrice"];
    }
  
    if(discover.keyword!=nil){
        [parameters setObject:discover.keyword forKey:@"keyword"];
    }
    
    if(discover.cityId!=nil){
        [parameters setObject:discover.cityId forKey:@"cityID"];
    }else{
        [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityID"];
    }
    
    if ([discover.fromWhere intValue] == 8)
    {
        [parameters setObject:@"home" forKey:@"type"];
    }
    
    if (discover.needLog)
    {
        [parameters setObject:[NSNumber numberWithInt:1] forKey:@"needlog"];
        if (discover.discoverId)
        {
            [parameters setObject:discover.discoverId forKey:@"discoverId"];
        }
    }
    
    IMGUser *user = [IMGUser currentUser];
    if (user.userId)
    {
        [parameters setObject:user.userId forKey:@"userId"];
    }
    
    if (discover.journalId!=nil) {
        [parameters setObject:discover.journalId forKey:@"journalId"];
    }
    if (discover.restaurantListId!=nil) {
        [parameters setObject:discover.restaurantListId forKey:@"restaurantListId"];
    }
    if (discover.diningGuideId!=nil) {
        [parameters setObject:discover.diningGuideId forKey:@"diningGuideId"];
    }
    //    NSLog(@"parameters = %@",parameters);
    NSLog(@"%f,getSearchData 1.0",[[NSDate date]timeIntervalSince1970]);
    
        [[IMGNetWork sharedManager]GET:@"app/search/v8" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
            if  ([responseObject isKindOfClass:[NSDictionary class]]&&[responseObject objectForKey:@"exceptionmsg"]) {
                return ;
            }
            if(offset==0){
                if(restaurantArray!=nil && restaurantArray.count>0){
                    [restaurantArray removeAllObjects];
                }else{
                    restaurantArray=[[NSMutableArray alloc]initWithCapacity:0];
                }
                
                if (restaurantOfferDic!=nil && [restaurantOfferDic allKeys].count>0)
                {
                    [restaurantOfferDic removeAllObjects];
                }else{
                    restaurantOfferDic = [[NSMutableDictionary alloc] initWithCapacity:0];
                }
                
                if(existRestauantIdArray!=nil && existRestauantIdArray.count>0){
                    [existRestauantIdArray removeAllObjects];
                }else{
                    existRestauantIdArray=[[NSMutableArray alloc]initWithCapacity:0];
                }
            }
            
            
            fetchedRestaurantCount=0;
            
            NSMutableDictionary *suggestDic = [[NSMutableDictionary alloc] init];
            
            NSNumber *numFound = [responseObject objectForKey:@"numFound"];
            if ( (numFound!=nil)
                && (![numFound isKindOfClass:[NSNull class]])
                &&([numFound isKindOfClass:[NSNumber class]])) {
                [suggestDic setObject:numFound forKey:@"numFound"];
            }
            
            NSString *searchInstead = [responseObject objectForKey:@"searchInstead"];
            if ( (searchInstead!=nil) && (![searchInstead isKindOfClass:[NSNull class]]) ) {
                [suggestDic setObject:[responseObject objectForKey:@"searchInstead"] forKey:@"searchInstead"];
            }
            NSDictionary *spellcheck = [responseObject objectForKey:@"spellcheck"];
            if ( (spellcheck!=nil)
                && (![spellcheck isKindOfClass:[NSNull class]])
                && ([spellcheck isKindOfClass:[NSDictionary class]])
                ) {
                NSDictionary *collations = [spellcheck objectForKey:@"collations"];
                if ( (collations!=nil)
                    && (![collations isKindOfClass:[NSNull class]])
                    && ([collations isKindOfClass:[NSDictionary class]])
                    ) {
                    NSString *collationQuery = [collations objectForKey:@"collationQuery"];
                    if ( (collationQuery!=nil)
                        && (![collationQuery isKindOfClass:[NSNull class]])
                        ) {
                        [suggestDic setObject:collationQuery forKey:@"collationQuery"];
                    }
                }
            }
            
            
            
            NSArray *adRestaurantList = [responseObject objectForKey:@"advertisementRestaurantList"];
            
            
            for (int i=0; i<adRestaurantList.count; i++) {
                NSDictionary *dic=[adRestaurantList objectAtIndex:i];
                IMGRestaurant* restaurant=[[IMGRestaurant alloc]init];
                restaurant.isAd=YES;
                restaurant.adNum=i;
                restaurant.adAllNums=adRestaurantList.count;
                restaurant.ratingScore=[dic objectForKey:@"ratingScore"];
                restaurant.priceLevel=[dic objectForKey:@"priceLevel"];
                restaurant.imageUrl=[dic objectForKey:@"imageUrl"];
                restaurant.state=[dic objectForKey:@"state"];
                restaurant.address1=[dic objectForKey:@"address1"];
                restaurant.bestOfferName=[dic objectForKey:@"bestOfferName"];
                restaurant.bookStatus=[dic objectForKey:@"bookStatus"];
                restaurant.title=[dic objectForKey:@"title"];
                restaurant.districtName=[dic objectForKey:@"districtName"];
                restaurant.menuCount=[dic objectForKey:@"menuPhotoCount"];
                restaurant.districtId=[dic objectForKey:@"districtId"];
                restaurant.phoneNumber=[dic objectForKey:@"phoneNumber"];
                restaurant.reviewCount=[dic objectForKey:@"reviewCount"];
                restaurant.longitude=[dic objectForKey:@"longitude"];
                restaurant.latitude=[dic objectForKey:@"latitude"];
                restaurant.restaurantId=[dic objectForKey:@"restaurantId"];
                restaurant.cuisineName=[dic objectForKey:@"cuisineName"];
                restaurant.stateName=[dic objectForKey:@"stateName"];
                restaurant.logoImage=[dic objectForKey:@"eventLogo"];
                
                NSDictionary* yesterOpenDic=[dic objectForKey:@"yesterDayOpenTime"];
                restaurant.yesteropenDay=[yesterOpenDic objectForKey:@"openDay"];
                restaurant.yesteropenTime=[yesterOpenDic objectForKey:@"openTime"];
                restaurant.yesterclosedTime=[yesterOpenDic objectForKey:@"closedTime"];
                
                
                NSDictionary* nextOpenDay=[dic objectForKey:@"nextOpenDay"];
                restaurant.nextopenTime=[nextOpenDay objectForKey:@"openTime"];
                restaurant.nextclosedTime=[nextOpenDay objectForKey:@"closedTime"];
                restaurant.nextopenDay=[nextOpenDay objectForKey:@"openDay"];
                
                NSDictionary* secondOpenDic=[dic objectForKey:@"secondOpenDay"];
                restaurant.secondclosedTime=[secondOpenDic objectForKey:@"closedTime"];
                restaurant.secondopenTime=[secondOpenDic objectForKey:@"openTime"];
                restaurant.secondopenDay=[secondOpenDic objectForKey:@"openDay"];
                [restaurantArray addObject:restaurant];
                

            }
            
            
            NSArray *restaurantList = [responseObject objectForKey:@"restaurantList"];
            for (NSDictionary* dic in restaurantList) {
                
                IMGRestaurant* restaurant=[[IMGRestaurant alloc]init];
                restaurant.adAllNums=adRestaurantList.count;

                restaurant.ratingScore=[dic objectForKey:@"ratingScore"];
                restaurant.priceLevel=[dic objectForKey:@"priceLevel"];
                restaurant.imageUrl=[dic objectForKey:@"imageUrl"];
                restaurant.state=[dic objectForKey:@"state"];
                restaurant.address1=[dic objectForKey:@"address1"];
                restaurant.bestOfferName=[dic objectForKey:@"bestOfferName"];
                restaurant.bookStatus=[dic objectForKey:@"bookStatus"];
                restaurant.title=[dic objectForKey:@"title"];
                restaurant.districtName=[dic objectForKey:@"districtName"];
                restaurant.menuCount=[dic objectForKey:@"menuPhotoCount"];
                restaurant.districtId=[dic objectForKey:@"districtId"];
                restaurant.phoneNumber=[dic objectForKey:@"phoneNumber"];
                restaurant.reviewCount=[dic objectForKey:@"reviewCount"];
                restaurant.longitude=[dic objectForKey:@"longitude"];
                restaurant.latitude=[dic objectForKey:@"latitude"];
                restaurant.restaurantId=[dic objectForKey:@"restaurantId"];
                restaurant.cuisineName=[dic objectForKey:@"cuisineName"];
                restaurant.stateName=[dic objectForKey:@"stateName"];
                restaurant.logoImage=[dic objectForKey:@"eventLogo"];
                
                NSDictionary* yesterOpenDic=[dic objectForKey:@"yesterDayOpenTime"];
                restaurant.yesteropenDay=[yesterOpenDic objectForKey:@"openDay"];
                restaurant.yesteropenTime=[yesterOpenDic objectForKey:@"openTime"];
                restaurant.yesterclosedTime=[yesterOpenDic objectForKey:@"closedTime"];
                
                
                NSDictionary* nextOpenDay=[dic objectForKey:@"nextOpenDay"];
                restaurant.nextopenTime=[nextOpenDay objectForKey:@"openTime"];
                restaurant.nextclosedTime=[nextOpenDay objectForKey:@"closedTime"];
                restaurant.nextopenDay=[nextOpenDay objectForKey:@"openDay"];
                
                NSDictionary* secondOpenDic=[dic objectForKey:@"secondOpenDay"];
                restaurant.secondclosedTime=[secondOpenDic objectForKey:@"closedTime"];
                restaurant.secondopenTime=[secondOpenDic objectForKey:@"openTime"];
                restaurant.secondopenDay=[secondOpenDic objectForKey:@"openDay"];
                [restaurantArray addObject:restaurant];
 
                
                
                
            }
            block(restaurantArray,suggestDic);
            
        } failure:^(NSURLSessionDataTask *operation, NSError *error) {
            
            NSLog(@"error.app/search/V7:::::::%@",error.description);
            
            
        }];
        

}
+ (void)getOffersFromResponseObject:(NSArray *)responseObject andOffset:(long)offset
{
    for(NSArray *restaurantInfoArray in responseObject)
    {
        if (![[restaurantInfoArray objectAtIndex:2] isKindOfClass:[NSNull class]])
        {
            NSDictionary *dic = [[NSDictionary alloc] initWithDictionary:[restaurantInfoArray objectAtIndex:2]];
            IMGRestaurantOffer *restaurantOffer = [[IMGRestaurantOffer alloc] init];
            [restaurantOffer setValuesForKeysWithDictionary:dic];
            if (restaurantOffer)
            {
                NSInteger index = [responseObject indexOfObject:restaurantInfoArray];
                //NSLog(@"index = %ld",index);
                [restaurantOfferDic setObject:restaurantOffer forKey:[NSString stringWithFormat:@"%ld",(long)index+offset]];
            }
        }
    }
}

+ (void)searchFromLocal:(IMGDiscover *)discover  offset:(long)offset minLatitude:(NSNumber *)minLatitude maxLatitude:(NSNumber *)maxLatitude minLongitude:(NSNumber *)minLongitude maxLongitude:(NSNumber *)maxLongitude{
    NSString *sortbyString;
    NSString *limitString=[NSString stringWithFormat:@" limit %ld,%d;",(long)offset,SEARCH_RESULT_PAGE_SIZE];
    if(discover.sortby!=nil){
        if([discover.sortby isEqualToString:SORTBY_POPULARITY]){
            sortbyString = @" order by r.qravedCount desc";
        }else if([discover.sortby isEqualToString:@"Offers"]){
            sortbyString = @" order by r.discount desc,r.offerCount desc,r.boost desc,r.ratingScore desc";
        }else if([discover.sortby isEqualToString:@"Bookings"]){
            sortbyString = @" order by r.bookingCount desc";
        }else if([discover.sortby isEqualToString:@"Rating"]){
            sortbyString = @" order by r.ratingScore desc";
        }else if([discover.sortby isEqualToString:@"Distance"]){
            sortbyString = [NSString stringWithFormat:@" order by distance(r.latitude,r.longitude,%f,%f) asc",[[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] doubleValue],[[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] doubleValue]];
            
            //            center.longitude = [AppDelegate ShareApp].locationManager.location.coordinate.longitude;
        }else{
            sortbyString = @" order by r.title";
        }
    }else{
        sortbyString = @" order by r.qravedCount desc";
    }
    
    NSString *sqlString = [NSString stringWithFormat:@"SELECT distinct r.*, distance(r.latitude,r.longitude,%f,%f) as calculatedDistance FROM IMGRestaurant r ",[[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] doubleValue],[[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] doubleValue]];
    
    if((discover.offerTypeArray!=nil && [discover.offerTypeArray count]>0)||(discover.offArray!=nil && [discover.offArray count]>0)){
        sqlString = [NSString stringWithFormat:@"SELECT distinct r.*,ro.offerId,ro.offerTitle FROM IMGRestaurant r "];
        sortbyString = [sortbyString stringByAppendingString:@",ro.offerType asc"];
    }
    
    NSString *whereString = @" where 1=1";
    if(minLatitude!=nil){
        whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" and  r.latitude >= %@ ",minLatitude]];
    }
    if(maxLatitude!=nil){
        whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" and  r.latitude <= %@ ",maxLatitude]];
    }
    if(minLongitude!=nil){
        whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" and  r.longitude >= %@ ",minLongitude]];
    }
    if(maxLongitude!=nil){
        whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" and  r.longitude <= %@ ",maxLongitude]];
    }
    if(discover.eventArray!=nil && [discover.eventArray count]>0){
        sqlString = [sqlString stringByAppendingString:[NSString stringWithFormat:@" left join IMGRestaurantEvent re on r.restaurantId=re.restaurantId"]];
        IMGOfferType *offerType = [discover.eventArray objectAtIndex:0];
        whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" and  (re.eventId = %@ ",offerType.offerId]];
        for(int i=1;i<discover.eventArray.count;i++){
            offerType = [discover.eventArray objectAtIndex:i];
            whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" or re.eventId = %@ ",offerType.offerId]];
        }
        whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@")"]];
    }
    
    //IMGDiscover
    if(discover.districtArray!=nil && [discover.districtArray count]>0){
        NSNumber *districtId = [discover.districtArray objectAtIndex:0];
        if(districtId!=nil&&![districtId isKindOfClass:[NSNull class]]&&![districtId isEqual:@""]){
            whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" and r.districtId=%@",districtId]];
        }
    }
    
    if(discover.offerTypeArray!=nil && [discover.offerTypeArray count]>0){
        
        sqlString = [sqlString stringByAppendingString:[NSString stringWithFormat:@" left join IMGRestaurantOffer ro on ro.restaurantId=r.restaurantId "]];
        
        IMGOfferType *offerType = [discover.offerTypeArray objectAtIndex:0];
        NSString *todayString=[BookUtil todayStringWithFormat:@"yyyy-MM-dd"];
        whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" and ro.offerStartDate<='%@' and ro.offerEndDate>='%@' and (ro.offerType=%@",todayString,todayString,offerType.typeId]];
        for(int i=1;i<discover.offerTypeArray.count;i++){
            offerType = [discover.offerTypeArray objectAtIndex:i];
            whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" or ro.offerType=%@",offerType.typeId]];
        }
        
        if(discover.offArray!=nil && [discover.offArray count]>0){
            for(int i=1;i<discover.offArray.count;i++){
                offerType = [discover.offArray objectAtIndex:i];
                whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" or (ro.offerType=0 and ro.offerSlotMaxDisc=%@)",offerType.off]];
            }
        }
        
        whereString = [whereString stringByAppendingString:@")"];
        
    }else if(discover.offArray!=nil && [discover.offArray count]>0){
        sqlString = [sqlString stringByAppendingString:[NSString stringWithFormat:@" left join IMGRestaurantOffer ro on ro.restaurantId=r.restaurantId"]];
        
        
        IMGOfferType *offerType = [discover.offArray objectAtIndex:0];
        NSString *todayString=[BookUtil todayStringWithFormat:@"yyyy-MM-dd"];
        whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" and ro.offerStartDate<='%@' and ro.offerEndDate>='%@' and ((ro.offerType=0 and ro.offerSlotMaxDisc=%@)",todayString,todayString,offerType.off]];
        for(int i=1;i<discover.offArray.count;i++){
            offerType = [discover.offArray objectAtIndex:i];
            whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" or (ro.offerType=0 and ro.offerSlotMaxDisc=%@)",offerType.off]];
        }
        whereString = [whereString stringByAppendingString:@")"];
    }
    
    if((discover.offerTypeArray!=nil && [discover.offerTypeArray count]>0)||(discover.offArray!=nil && [discover.offArray count]>0)){
        if(discover.bookTime!=nil&&![discover.bookTime isEqualToString:@""]){
            NSCalendar *calendar=[NSCalendar currentCalendar];
            NSDateComponents*comps1=[calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth |NSCalendarUnitDay | NSCalendarUnitWeekday) fromDate:discover.bookDate];
            long currentWeekDay = [comps1 weekday];
            currentWeekDay=currentWeekDay-1;
            
            whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" and os.weekday='%@'",[NSString stringWithFormat:@"%ld",currentWeekDay]]];
            NSArray *bookTimeArray=[BookUtil around30minutes:discover.bookTime];
            whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" and os.bookTime>='%@'",bookTimeArray[0]]];
            whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" and os.bookTime<='%@'",bookTimeArray[1]]];
            whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" and os.avail>='%@'",discover.pax]];
            NSString *todayString=[BookUtil todayStringWithFormat:@"yyyy-MM-dd"];
            if([[BookUtil dateStringWithFormat:@"yyyy-MM-dd" forDate:discover.bookDate] isEqualToString:todayString]){
                whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" and os.bookTime>=strftime('%@:%@',datetime(strftime('%@','now','+7 hour')+r.minimumAvailableBooking*60,'unixepoch'))",@"%H",@"%M",@"%s"]];
            }
        }
    }
    
    if(discover.tagsArr!=nil && [discover.tagsArr count]>0){
        sqlString = [sqlString stringByAppendingString:[NSString stringWithFormat:@" left join IMGRestaurantTag rtf on rtf.restaurantId=r.restaurantId"]];
        IMGTag *tag = [discover.tagsArr objectAtIndex:0];
        whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" and (rtf.tagId=%@",tag.tagId]];
        for(int i=1;i<discover.tagsArr.count;i++){
            tag = [discover.tagsArr objectAtIndex:i];
            whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" or rtf.tagId=%@",tag.tagId]];
        }
        whereString = [whereString stringByAppendingString:@")"];
    }
    
    if(discover.occasionArray!=nil && [discover.occasionArray count]>0){
        sqlString = [sqlString stringByAppendingString:[NSString stringWithFormat:@" left join IMGRestaurantTag rto on rto.restaurantId=r.restaurantId"]];
        IMGTag *occasion = [discover.occasionArray objectAtIndex:0];
        whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" and (rto.tagId=%@",occasion.tagId]];
        for(int i=1;i<discover.occasionArray.count;i++){
            occasion = [discover.occasionArray objectAtIndex:i];
            whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" or rto.tagId=%@",occasion.tagId]];
        }
        whereString = [whereString stringByAppendingString:@")"];
    }
    
    if(discover.cuisineArray!=nil && [discover.cuisineArray count]>0){
        sqlString = [sqlString stringByAppendingString:[NSString stringWithFormat:@" left join IMGRestaurantCuisine rc on rc.restaurantId=r.restaurantId"]];
        IMGCuisine *cuisine = [discover.cuisineArray objectAtIndex:0];
        whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" and (rc.cuisineId=%@",cuisine.cuisineId]];
        for(int i=1;i<discover.cuisineArray.count;i++){
            IMGCuisine *cuisine = [discover.cuisineArray objectAtIndex:i];
            whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" or rc.cuisineId=%@",cuisine.cuisineId]];
        }
        whereString = [whereString stringByAppendingString:@")"];
    }
    
    if(discover.priceLevelArray!=nil && discover.priceLevelArray.count>0){
        //        whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" and r.score=%@",discover.priceLevel.priceLevelId]];
        IMGPriceLevel *priceLevel = [discover.priceLevelArray objectAtIndex:0];
        whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" and (r.priceLevel=%@",priceLevel.priceLevelId]];
        for(int i=1;i<discover.priceLevelArray.count;i++){
            IMGPriceLevel *priceLevel = [discover.priceLevelArray objectAtIndex:i];
            whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" or r.priceLevel=%@",priceLevel.priceLevelId]];
        }
        whereString = [whereString stringByAppendingString:@")"];
    }
    
    if(discover.keyword!=nil){
        whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@" and r.title like '%@%@%@'",@"%",[discover.keyword stringByReplacingOccurrencesOfString:@"'" withString:@"''"],@"%"]];
    }
    if([discover.sortby isEqualToString:@"Distance"]){
        whereString = [whereString stringByAppendingString:@" and r.latitude!=0 and r.longitude!=0 "];
    }
    
    if(discover.cityId!=nil){
        whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@"%@%@",@" and cityId=",discover.cityId]];
    }else{
        whereString = [whereString stringByAppendingString:[NSString stringWithFormat:@"%@%@",@" and cityId=",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]] ];
    }
    
    sqlString = [[[sqlString stringByAppendingString:whereString] stringByAppendingString:sortbyString ] stringByAppendingString:limitString];
    
    FMResultSet * resultSet = [[DBManager manager] executeQuery:sqlString];
    fetchedRestaurantCount=0;
    while([resultSet next]){
        IMGRestaurant *restaurant=[[IMGRestaurant alloc]init];
        [restaurant setValueWithResultSet:resultSet];
        
        if(![resultSet columnIsNull:@"offerId"]){
            NSNumber *currentRestaurantrId=[resultSet objectForColumnName:@"restaurantId"];
            if([existRestauantIdArray indexOfObject:currentRestaurantrId]==NSNotFound){
                restaurant.bestOfferName=[resultSet objectForColumnName:@"offerTitle"];
                restaurant.calculatedDistance=[resultSet objectForColumnName:@"calculatedDistance"];
                [restaurantArray addObject:restaurant];
                
                [existRestauantIdArray addObject:currentRestaurantrId];
            }
        }else{
            
            [restaurantArray addObject:restaurant];
        }
        fetchedRestaurantCount++;
    }
    [resultSet close];
    
    //    if(searchResultsTableView!=nil){
    //        if (restaurantArray==nil || restaurantArray.count == 0) {
    //            [self loadEmptyTableVIewHeadView];
    //        }else{
    //            [searchResultsTableView setTableHeaderView:nil];
    //        }
    //        [searchResultsTableView reloadData];
    //    }
    //
    //    if(mapViewController!=nil){
    //        [mapViewController setDatas:restaurantArray];
    //    }
    //    [[LoadingView sharedLoadingView]stopLoading];
    //    [self stopLoading];
}
+(void)filterFromServerWithSearchText:(NSString *)searchText andBlock:(void(^) (NSArray *districtArray,NSArray *restaurantArray,NSArray *cuisineArray,NSArray *foodTypeArray))block{
    
    NSDictionary *parameters = @{@"filterWord":searchText,@"cityID":@"1"};
    NSLog(@"%f,type keyword1.0",[[NSDate date] timeIntervalSince1970]);
    [[IMGNetWork sharedManager]GET:@"search/filtergroupedname/v2" parameters:parameters success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSLog(@"%f,type keyword2.0",[[NSDate date] timeIntervalSince1970]);

        if ([responseObject objectForKey:@"exceptionmsg"] != nil) {
            return;
        }
        NSArray *restaurantList = [responseObject objectForKey:@"restaurantList"];//restaurant
        NSArray *districtList = [responseObject objectForKey:@"districtList"];//location
        NSArray *tagList = [responseObject objectForKey:@"tagList"];//location
        NSArray *locationList = [responseObject objectForKey:@"locationList"];//location
        NSArray *cuisineList = [responseObject objectForKey:@"cuisineList"];//cuisine
        NSArray *foodTypeList = [responseObject objectForKey:@"foodTypeList"];//foodType
        block ([SearchUtil addDistrictFromServerDistrictArray:districtList andTagArray:tagList andLocationArray:locationList orSearchText:searchText],[SearchUtil addRestaurantFromServerArray:restaurantList orSearchText:searchText],[SearchUtil addFoodTypeFromServerCuisineArray:cuisineList orSearchText:searchText],[SearchUtil addFoodTypeFromServerFoodArray:foodTypeList]);
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        block ([SearchUtil addDistrictFromServerDistrictArray:nil andTagArray:nil andLocationArray:nil orSearchText:searchText],[SearchUtil addRestaurantFromServerArray:nil orSearchText:searchText],[SearchUtil addFoodTypeFromServerCuisineArray:nil orSearchText:searchText],[NSArray array]);
    }];
    
}
+(void)journalFilterFromServerWithSearchText:(NSString *)searchText andBlock:(void(^) (NSArray *journalDataArray))block
{
    
    NSDictionary *parameters = @{@"filterWord":searchText};
    [[IMGNetWork sharedManager]GET:@"journalarticle/search/filtertitle" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        
        if ([responseObject isKindOfClass:[NSDictionary class]]  && ([responseObject objectForKey:@"exceptionmsg"] != nil)) {
            return;
        }
        NSMutableArray *dataArrM = [[NSMutableArray alloc] init];
        for (NSDictionary *dic in responseObject)
        {
            IMGMediaComment *journal = [[IMGMediaComment alloc] init];
            
            journal.title = [[dic objectForKey:@"articleTitle"] enterKey];
            journal.mediaCommentId = [dic objectForKey:@"articleId"];
            journal.imported = [dic objectForKey:@"imported"];
            [dataArrM addObject:journal];
        }
        block(dataArrM);
        
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"journalFilterFromServerWithSearchText  --- error");
        
    }];
    
}

+(void)journalSearch:(NSDictionary *)parameters andBlock:(void(^) (NSArray *journalDataArray))block
{
    NSMutableArray *dataArrM = [[NSMutableArray alloc] init];
    NSNumber *max = [parameters objectForKey:@"max"];
    NSNumber *offset = [parameters objectForKey:@"offset"];
    if(!max){
        max=[NSNumber numberWithInt:10];
    }
    if(!offset){
        offset=[NSNumber numberWithInt:0];
    }
    
    NSLog(@"journalarticle/search parameters = %@",parameters);
    
    [[IMGNetWork sharedManager]GET:@"journalarticle/search" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSLog(@"--responseObject = %@",responseObject);
        NSArray *journalArticleListArr = [responseObject objectForKey:@"journalArticleList"];
        for (NSDictionary *journalDic in journalArticleListArr)
        {
            IMGMediaComment *journal = [[IMGMediaComment alloc] init];
            
            journal.title = [journalDic objectForKey:@"articleTitle"];
            NSRange range=[journal.title rangeOfString:@"amp;"];
            if (range.location != NSNotFound)
            {
                journal.title = [journal.title stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
            }
            
            journal.mediaCommentId = [journalDic objectForKey:@"articleId"];
            journal.imageUrl = [journalDic objectForKey:@"articlePhoto"];
            NSArray *categories=[journalDic objectForKey:@"category"];
            NSMutableArray *category_list=[[NSMutableArray alloc] init];
            if(categories.count){
                for (NSDictionary *category in categories) {
                    [category_list addObject:[category objectForKey:@"name"]];
                }
                
                journal.categoryName=[category_list componentsJoinedByString:@", "];
            }
            
            journal.shareCount = [journalDic objectForKey:@"shareCount"];
            journal.imported = [journalDic objectForKey:@"imported"];
            //            journal.saved = [[journalDic objectForKey:@"saved"] boolValue];
            
            [dataArrM addObject:journal];
        }
        block(dataArrM);
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"journalFilterFromServerWithSearchText  --- error");
        
    }];
    
}

+(NSArray *)addFoodTypeFromServerCuisineArray:(NSArray *)serverCuisineArray orSearchText:(NSString *)searchText{
    NSMutableArray *cuisineArray = [[NSMutableArray alloc]init];
    if (serverCuisineArray != nil) {
        for (int i=0; i<serverCuisineArray.count; i++) {
            NSNumber *entityId = [[serverCuisineArray objectAtIndex:i] objectAtIndex:0];
            IMGCuisineAndFoodType *cuisineAndFoodType = [[IMGCuisineAndFoodType alloc]init];
            cuisineAndFoodType.cuisineId = entityId;
            cuisineAndFoodType.name = [[[serverCuisineArray objectAtIndex:i] objectAtIndex:1] filterHtml];
            cuisineAndFoodType.entityType=[NSNumber numberWithInt:0];
            [cuisineArray addObject:cuisineAndFoodType];
        }
    }
    else{
        NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM IMGCuisine WHERE name like '%@%@' ORDER BY name;",@"%",[[searchText stringByAppendingString:@"%"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"] ];
        FMResultSet *resultSet = [[DBManager manager]executeQuery:sqlString];
        while ([resultSet next]) {
            IMGCuisineAndFoodType *cuisineAndFoodType = [[IMGCuisineAndFoodType alloc]init];
            [cuisineAndFoodType setValueWithResultSet:resultSet];
            cuisineAndFoodType.entityType = [NSNumber numberWithInt:0];
            [cuisineArray addObject:cuisineAndFoodType];
        }
        [resultSet close];
    }
    
    return cuisineArray;
}
+(NSArray *)addFoodTypeFromServerFoodArray:(NSArray *)serverFoodTypeArray{
    NSMutableArray *foodArray = [[NSMutableArray alloc]init];
    if (serverFoodTypeArray != nil) {
        for (int i=0; i<serverFoodTypeArray.count; i++) {
            NSNumber *entityId = [[serverFoodTypeArray objectAtIndex:i] objectAtIndex:0];
            IMGCuisineAndFoodType *cuisineAndFoodType = [[IMGCuisineAndFoodType alloc]init];
            cuisineAndFoodType.cuisineId = entityId;
            cuisineAndFoodType.name = [[serverFoodTypeArray objectAtIndex:i] objectAtIndex:1];
            cuisineAndFoodType.entityType=[NSNumber numberWithInt:1];
            [foodArray addObject:cuisineAndFoodType];
        }
    }
    return foodArray;
}
+(NSArray *)addDistrictFromServerDistrictArray:(NSArray *)serverDistrictArray andTagArray:(NSArray *)serverTagArray andLocationArray:(NSArray *)locationArray orSearchText:(NSString *)searchText{
    NSMutableArray *districtArray = [[NSMutableArray alloc]init];
    if (locationArray != nil) {
        for (NSDictionary *item in locationArray) {
            NSString *typeIdStr = [NSString stringWithFormat:@"%@",[item objectForKey:@"typeId"]];
            if ([@"3" isEqualToString:typeIdStr]) {
                IMGTag *tag = [[IMGTag alloc] initWithDictionary:item];
                tag.type = @"Landmark/ Mall";
                [districtArray addObject:tag];
            }else if([@"6" isEqualToString:typeIdStr]) {
                NSNumber *districtId = [item objectForKey:@"id"];
                IMGDistrictAndLandmark *districtAndLandmark = [[IMGDistrictAndLandmark alloc]init];
                districtAndLandmark.districtId = districtId;
                districtAndLandmark.name = [[item objectForKey:@"name"] filterHtml];
                districtAndLandmark.locationType=[NSNumber numberWithInt:0];
                [districtArray addObject:districtAndLandmark];
            }
        }
    }else if (serverDistrictArray != nil) {
        for (int i=0; i<serverDistrictArray.count; i++) {
            NSNumber *districtId = [[serverDistrictArray objectAtIndex:i] objectAtIndex:0];
            //            NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM IMGDistrict WHERE districtId=%@;",districtId];
            //            FMResultSet *resultSet = [[DBManager manager]executeQuery:sqlString];
            //            if ([resultSet next]) {
            //                IMGDistrict *district = [[IMGDistrict alloc]init];
            //                [district setValueWithResultSet:resultSet];
            //                [districtArray addObject:district];
            //                [resultSet close];
            //            }else{
            IMGDistrictAndLandmark *districtAndLandmark = [[IMGDistrictAndLandmark alloc]init];
            districtAndLandmark.districtId = districtId;
            districtAndLandmark.name = [[[serverDistrictArray objectAtIndex:i] objectAtIndex:1] filterHtml];
            districtAndLandmark.locationType=[NSNumber numberWithInt:0];
            [districtArray addObject:districtAndLandmark];
            //            }
        }
        for (int i=0; i<serverTagArray.count; i++) {
            NSDictionary *tagDictionary = [serverTagArray objectAtIndex:i];
            IMGTag *tag = [[IMGTag alloc] initWithDictionary:tagDictionary];
            tag.type = @"Landmark/ Mall";
            [districtArray addObject:tag];
        }
    }
    else{
        NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM IMGDistrict WHERE name like '%@%@' ORDER BY name;",@"%",[[searchText stringByAppendingString:@"%"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"] ];
        FMResultSet *resultSet = [[DBManager manager]executeQuery:sqlString];
        while ([resultSet next]) {
            IMGDistrictAndLandmark *district = [[IMGDistrictAndLandmark alloc]init];
            [district setValueWithResultSet:resultSet];
            district.locationType = [NSNumber numberWithInt:0];
            [districtArray addObject:district];
        }
        [resultSet close];
    }
    
    return districtArray;
}
+(NSArray *)addRestaurantFromServerArray:(NSArray *)serverArray orSearchText:(NSString *)searchText{
    NSMutableArray *restaurantArray = [[NSMutableArray alloc]init];
    if (serverArray != nil) {
        for (int i=0; i<serverArray.count; i++) {
            NSNumber *restaurantId = [[serverArray objectAtIndex:i]objectAtIndex:0];
            IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
            restaurant.restaurantId = restaurantId;
            NSString *title = [[serverArray objectAtIndex:i] objectAtIndex:1];
            restaurant.title = title;
            [restaurantArray addObject:restaurant];
        }
    }
    //    else{
    //        NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM IMGRestaurant WHERE title like '%@%@' ORDER BY title;",@"%",[[searchText stringByAppendingString:@"%"] stringByReplacingOccurrencesOfString:@"'" withString:@"''"] ];
    //        FMResultSet *resultSet = [[DBManager manager]executeQuery:sqlString];
    //        while ([resultSet next]) {
    //            IMGRestaurant *restaurant = [[IMGRestaurant alloc]init];
    //            [restaurant setValueWithResultSet:resultSet];
    //            [restaurantArray addObject:restaurant];
    //        }
    //        [resultSet close];
    //    }
    return restaurantArray;
}

+(MKCoordinateRegion)regionOfCity{
    
    NSNumber *minLatitude=@0;
    NSNumber *maxLatitude=@0;
    NSNumber *minLongitude=@0;
    NSNumber *maxLongitude=@0;
    NSInteger cityId=[[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] intValue];
    if(cityId==2){
        minLatitude=@-7.242557;
        maxLatitude=@-6.012078;
        minLongitude=@106.042743;
        maxLongitude=@112.742119;
    }else if(cityId==1){
        minLatitude=@-8.847331;
        maxLatitude=@-8.101911;
        minLongitude=@114.5692078;
        maxLongitude=@116.035856;
    }else if(cityId == 3){
        minLatitude=@-7.206803;
        maxLatitude=@-6.184962;
        minLongitude=@106.900295;
        maxLongitude=@107.9482437;
        
    }
    
    CLLocationCoordinate2D center;
    
    center.latitude=([maxLatitude doubleValue]+[minLatitude doubleValue])/2;
    center.longitude = ([maxLongitude doubleValue]+[minLongitude doubleValue])/2;
    
    MKCoordinateSpan span;
    span.latitudeDelta=([maxLatitude doubleValue]-[minLatitude doubleValue]);
    span.longitudeDelta=([maxLongitude doubleValue]-[minLongitude doubleValue]);
    
    MKCoordinateRegion region={center,span};
    return region;
    
    NSString *sqlString = [NSString stringWithFormat:@"select min(latitude) as minLatitude,max(latitude) as maxLatitude,min(longitude) as minLongitude,max(longitude) as maxLongitude from IMGRestaurant where cityId=%@ and (latitude>-9.9 and latitude<=-5.0 and longitude<=118 and longitude>=104) ",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]];
    FMResultSet *resultSet = [[DBManager manager]executeQuery:sqlString];
    if([resultSet next]){
        NSNumber *minLatitude=[resultSet objectForColumnName:@"minLatitude"];
        NSNumber *maxLatitude=[resultSet objectForColumnName:@"maxLatitude"];
        NSNumber *minLongitude=[resultSet objectForColumnName:@"minLongitude"];
        NSNumber *maxLongitude=[resultSet objectForColumnName:@"maxLongitude"];
        
        [resultSet close];
        
        CLLocationCoordinate2D center;
        if ([maxLatitude isKindOfClass:[NSNull class]]) {
            maxLatitude = @0;
        }
        if ([minLatitude isKindOfClass:[NSNull class]]) {
            minLatitude = @0;
        }
        if ([maxLongitude isKindOfClass:[NSNull class]]) {
            maxLongitude = @0;
        }
        if ([minLongitude isKindOfClass:[NSNull class]]) {
            minLongitude = @0;
        }
        center.latitude=([maxLatitude doubleValue]+[minLatitude doubleValue])/2;
        center.longitude = ([maxLongitude doubleValue]+[minLongitude doubleValue])/2;
        
        MKCoordinateSpan span;
        span.latitudeDelta=([maxLatitude doubleValue]-[minLatitude doubleValue]);
        span.longitudeDelta=([maxLongitude doubleValue]-[minLongitude doubleValue]);
        
        MKCoordinateRegion region={center,span};
        return region;
    }else{
        CLLocationCoordinate2D center;
        center.latitude=-6.237977;
        center.longitude = 106.807509;
        
        MKCoordinateSpan span;
        span.latitudeDelta=0.01;
        span.longitudeDelta=0.01;
        MKCoordinateRegion region={center,span};
        return region;
    }
    
}

+(MKCoordinateRegion)regionOfDistrict:(NSNumber *)districtId{
    
    
    NSNumber *minLatitude=@-7.242557;
    NSNumber *maxLatitude=@-6.012078;
    NSNumber *minLongitude=@106.042743;
    NSNumber *maxLongitude=@112.742119;
    
    CLLocationCoordinate2D center;
    if ([maxLatitude isKindOfClass:[NSNull class]]) {
        maxLatitude = @0;
    }
    if ([minLatitude isKindOfClass:[NSNull class]]) {
        minLatitude = @0;
    }
    if ([maxLongitude isKindOfClass:[NSNull class]]) {
        maxLongitude = @0;
    }
    if ([minLongitude isKindOfClass:[NSNull class]]) {
        minLongitude = @0;
    }
    center.latitude=([maxLatitude doubleValue]+[minLatitude doubleValue])/2;
    center.longitude = ([maxLongitude doubleValue]+[minLongitude doubleValue])/2;
    
    MKCoordinateSpan span;
    span.latitudeDelta=([maxLatitude doubleValue]-[minLatitude doubleValue]);
    span.longitudeDelta=([maxLongitude doubleValue]-[minLongitude doubleValue]);
    
    MKCoordinateRegion region={center,span};
    return region;
    
    
    NSString *sqlString = [NSString stringWithFormat:@"select min(latitude),max(latitude),min(longitude),max(longitude) from IMGRestaurant where districtId=%@ and (latitude>-9.9 and latitude<=-5.0 and longitude<=118 and longitude>=104) ",districtId];
    FMResultSet *resultSet = [[DBManager manager]executeQuery:sqlString];
    if([resultSet next]){
        NSNumber *minLatitude=[resultSet objectForColumnIndex:0];
        NSNumber *maxLatitude=[resultSet objectForColumnIndex:1];
        NSNumber *minLongitude=[resultSet objectForColumnIndex:2];
        NSNumber *maxLongitude=[resultSet objectForColumnIndex:3];
        
        [resultSet close];
        
        CLLocationCoordinate2D center;
        if ([maxLatitude isKindOfClass:[NSNull class]]) {
            maxLatitude = @0;
        }
        if ([minLatitude isKindOfClass:[NSNull class]]) {
            minLatitude = @0;
        }
        if ([maxLongitude isKindOfClass:[NSNull class]]) {
            maxLongitude = @0;
        }
        if ([minLongitude isKindOfClass:[NSNull class]]) {
            minLongitude = @0;
        }
        center.latitude=([maxLatitude doubleValue]+[minLatitude doubleValue])/2;
        center.longitude = ([maxLongitude doubleValue]+[minLongitude doubleValue])/2;
        
        MKCoordinateSpan span;
        span.latitudeDelta=([maxLatitude doubleValue]-[minLatitude doubleValue]);
        span.longitudeDelta=([maxLongitude doubleValue]-[minLongitude doubleValue]);
        
        MKCoordinateRegion region={center,span};
        return region;
    }else{
        CLLocationCoordinate2D center;
        center.latitude=-6.237977;
        center.longitude = 106.807509;
        
        MKCoordinateSpan span;
        span.latitudeDelta=0.03;
        span.longitudeDelta=0.03;
        MKCoordinateRegion region={center,span};
        return region;
    }
    
}

+(BOOL)existViewed{
    NSString *sqlString1 = [NSString stringWithFormat:@"select restaurantId from IMGRestaurantViewed limit 1;"];
    FMResultSet *resultSet1=[[DBManager manager] executeQuery:sqlString1];
    if([resultSet1 next]){
        IMGRestaurant *restaurant=[IMGRestaurant findById:[resultSet1 objectForColumnName:@"restaurantId"]];
        if(restaurant!=nil){
            [resultSet1 close];
            return TRUE;
        }
    }
    
    return FALSE;
}

+(void)listViewed:(void (^)(NSArray * restaurantArray,BOOL recently))block{
    NSMutableArray *restaurantArray=[[NSMutableArray alloc]initWithCapacity:0];
    
    NSString *sqlString1 = [NSString stringWithFormat:@"select * from IMGRestaurantViewed order by Rowid desc;"];
    FMResultSet *resultSet1=[[DBManager manager] executeQuery:sqlString1];
    while([resultSet1 next]){
        IMGRestaurantViewed *restaurantViewed = [[IMGRestaurantViewed alloc] init];
        [restaurantViewed setValueWithResultSet:resultSet1];
        IMGRestaurant *restaurant=[[IMGRestaurant alloc] init];
        restaurant.restaurantId = restaurantViewed.restaurantId;
        restaurant.title = restaurantViewed.title;
        restaurant.districtName = restaurantViewed.districtName;
        
        [restaurantArray addObject:restaurant];
    }
    [resultSet1 close];
    
    if([restaurantArray count]==0){
        IMGUser *user=[IMGUser currentUser];
        if(user!=nil && user.userId!=nil){
            NSMutableDictionary *parameters=[[NSMutableDictionary alloc]initWithCapacity:0];
            [parameters setObject:user.userId forKey:@"userId"];
            
            [[IMGNetWork sharedManager] GET:@"restaurant/viewed/list/v1" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
                NSArray *returnRestaurantIdArray = responseObject;
                if([returnRestaurantIdArray count]>0){
                    for(NSDictionary *dic in returnRestaurantIdArray){
                        NSNumber *restaurantId = [dic objectForKey:@"restaurantId"];
                        NSString *districtName = [dic objectForKey:@"districtName"];
                        NSString *title = [dic objectForKey:@"title"];
                        IMGRestaurantViewed *restaurantViewed=[IMGRestaurantViewed findByRestaurantId:restaurantId];
                        if(restaurantViewed==nil){
                            restaurantViewed = [[IMGRestaurantViewed alloc]init];
                            restaurantViewed.restaurantId=restaurantId;
                            restaurantViewed.title = title;
                            restaurantViewed.districtName = districtName;
                            [[DBManager manager] insertModel:restaurantViewed];
                        }
                        IMGRestaurant *restaurant=[[IMGRestaurant alloc] init];
                        restaurant.restaurantId = restaurantViewed.restaurantId;
                        restaurant.title = restaurantViewed.title;
                        restaurant.districtName = restaurantViewed.districtName;
                        
                        [restaurantArray addObject:restaurant];
                        
                    }
                    block(restaurantArray,YES);
                }else{
                    [restaurantArray addObjectsFromArray:[SearchUtil recommendRestaurantArray]];
                    block(restaurantArray,NO);
                }
                
                
            } failure:^(NSURLSessionDataTask *operation, NSError *error) {
                [restaurantArray addObjectsFromArray:[SearchUtil recommendRestaurantArray]];
                block(restaurantArray,NO);
            }];
        }else{
            [restaurantArray addObjectsFromArray:[SearchUtil recommendRestaurantArray]];
            block(restaurantArray,NO);
        }
    }else{
        block(restaurantArray,YES);
    }
    
}

+(NSMutableArray *)filteredRestaurantArray:(NSString *)title{
    NSMutableArray *restaurantArray=[[NSMutableArray alloc]initWithCapacity:0];
    NSString *sqlString2 = [NSString stringWithFormat:@"SELECT r.* FROM IMGRestaurant r where r.cityId=%@ and r.title like '%@%@' limit %d,%d;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID],title,@"%",0,20];
    FMResultSet *resultSet2=[[DBManager manager] executeQuery:sqlString2];
    while([resultSet2 next]){
        IMGRestaurant *restaurant=[[IMGRestaurant alloc]init];
        [restaurant setValueWithResultSet:resultSet2];
        [restaurantArray addObject:restaurant];
    }
    [resultSet2 close];
    return restaurantArray;
}

+(NSMutableArray *)recommendRestaurantArray{
    NSMutableArray *restaurantArray=[[NSMutableArray alloc]initWithCapacity:0];
    NSString *sqlString2 = [NSString stringWithFormat:@"SELECT r.* FROM IMGRestaurant r and r.cityId=%@ order by r.discount desc,r.offerCount desc,r.boost desc,r.ratingScore desc limit %d,%d;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID],0,20];
    FMResultSet *resultSet2=[[DBManager manager] executeQuery:sqlString2];
    while([resultSet2 next]){
        IMGRestaurant *restaurant=[[IMGRestaurant alloc]init];
        [restaurant setValueWithResultSet:resultSet2];
        [restaurantArray addObject:restaurant];
    }
    [resultSet2 close];
    return restaurantArray;
}

+(void)viewed:(IMGRestaurant *)restaurant cityId:(NSNumber *)cityId{
    NSMutableDictionary *parameters=[[NSMutableDictionary alloc]initWithCapacity:0];
    IMGUser *user=[IMGUser currentUser];
    if(user!=nil && user.userId!=nil){
        [parameters setObject:user.userId forKey:@"userId"];
    }
    
    [parameters setObject:restaurant.restaurantId forKey:@"restaurantId"];
    [parameters setObject:cityId forKey:@"cityId"];
    [(NSMutableDictionary *)parameters setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [(NSMutableDictionary *)parameters setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    
    NSString *sessionId = [[NSUserDefaults standardUserDefaults] stringForKey:@"qraved_session_id"];
    if (sessionId != nil) {
        [parameters setObject:sessionId forKey:@"sessionId"];
    }
    
    [[IMGNetWork sharedManager] POST:@"restaurant/viewed/add" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        
        NSString *sessionId = [responseObject objectForKey:@"sessionId"];
        if (sessionId != nil) {
            [[NSUserDefaults standardUserDefaults] setObject:sessionId forKey:@"qraved_session_id"];
        }
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"viewed restaurant erors when post datas:%@",error.description);
    }];
    
    if(user!=nil && user.userId!=nil){
        IMGRestaurantViewed *restaurantViewed=[IMGRestaurantViewed findByRestaurantId:restaurant.restaurantId];
        if(restaurantViewed==nil){
            restaurantViewed = [[IMGRestaurantViewed alloc]init];
            restaurantViewed.restaurantId=restaurant.restaurantId;
            restaurantViewed.districtName=restaurant.districtName;
            restaurantViewed.title=restaurant.title;
            [[DBManager manager] insertModel:restaurantViewed];
        }
    }
}

+(void)getSearchResault:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array, NSNumber *restaurantCount))successBlock anderrorBlock:(void(^)())errorBlock{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithDictionary:dic];
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        [paramDic setObject:user.userId forKey:@"userId"];
    }
    [[IMGNetWork sharedManager]GET:@"app/search/v8" parameters:paramDic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        restaurantArray = [NSMutableArray array];
//        array = [V2_DiscoverListResultRestaurantModel mj_objectArrayWithKeyValuesArray:responseObject[@"restaurantList"]];

        NSArray *adRestaurantList = [responseObject objectForKey:@"advertisementRestaurantList"];
        
        
        for (int i=0; i<adRestaurantList.count; i++) {
            NSDictionary *dic=[adRestaurantList objectAtIndex:i];
            IMGRestaurant* restaurant=[[IMGRestaurant alloc]init];
            restaurant.isAd=YES;
            restaurant.adNum=i;
            restaurant.adAllNums=adRestaurantList.count;
            restaurant.ratingScore=[dic objectForKey:@"ratingScore"];
            //restaurant.priceLevel=[dic objectForKey:@"priceLevel"];
            restaurant.imageUrl=[dic objectForKey:@"imageUrl"];
            restaurant.state=[dic objectForKey:@"state"];
            restaurant.address1=[dic objectForKey:@"address1"];
            restaurant.bestOfferName=[dic objectForKey:@"bestOfferName"];
            restaurant.bookStatus=[dic objectForKey:@"bookStatus"];
            restaurant.title=[dic objectForKey:@"title"];
            restaurant.districtName=[dic objectForKey:@"districtName"];
            restaurant.menuCount=[dic objectForKey:@"menuPhotoCount"];
            restaurant.districtId=[dic objectForKey:@"districtId"];
            restaurant.phoneNumber=[dic objectForKey:@"phoneNumber"];
            restaurant.reviewCount=[dic objectForKey:@"reviewCount"];
            restaurant.longitude=[dic objectForKey:@"longitude"];
            restaurant.latitude=[dic objectForKey:@"latitude"];
            restaurant.restaurantId=[dic objectForKey:@"restaurantId"];
            restaurant.cuisineName=[dic objectForKey:@"cuisineName"];
            restaurant.stateName=[dic objectForKey:@"stateName"];
            restaurant.logoImage=[dic objectForKey:@"eventLogo"];
            restaurant.priceName =[dic objectForKey:@"priceLevel"];
            restaurant.landMarkList = [dic objectForKey:@"landMarkList"];
            restaurant.goFoodLink = [dic objectForKey:@"goFoodLink"];
            restaurant.ratingCount = [dic objectForKey:@"ratingCount"];
            restaurant.saved = [dic objectForKey:@"isSaved"];
            
            NSDictionary* yesterOpenDic=[dic objectForKey:@"yesterDayOpenTime"];
            restaurant.yesteropenDay=[yesterOpenDic objectForKey:@"openDay"];
            restaurant.yesteropenTime=[yesterOpenDic objectForKey:@"openTime"];
            restaurant.yesterclosedTime=[yesterOpenDic objectForKey:@"closedTime"];
            
            
            NSDictionary* nextOpenDay=[dic objectForKey:@"nextOpenDay"];
            restaurant.nextopenTime=[nextOpenDay objectForKey:@"openTime"];
            restaurant.nextclosedTime=[nextOpenDay objectForKey:@"closedTime"];
            restaurant.nextopenDay=[nextOpenDay objectForKey:@"openDay"];
            
            NSDictionary* secondOpenDic=[dic objectForKey:@"secondOpenDay"];
            restaurant.secondclosedTime=[secondOpenDic objectForKey:@"closedTime"];
            restaurant.secondopenTime=[secondOpenDic objectForKey:@"openTime"];
            restaurant.secondopenDay=[secondOpenDic objectForKey:@"openDay"];
            [restaurantArray addObject:restaurant];
            
        }

        
        
        NSArray *restaurantList = [responseObject objectForKey:@"restaurantList"];
        for (NSDictionary* dic in restaurantList) {
            
            IMGRestaurant* restaurant=[[IMGRestaurant alloc]init];
            restaurant.adAllNums=adRestaurantList.count;
            
            restaurant.ratingScore=[dic objectForKey:@"ratingScore"];
            //restaurant.priceLevel=[dic objectForKey:@"priceLevel"];
            restaurant.imageUrl=[dic objectForKey:@"imageUrl"];
            restaurant.state=[dic objectForKey:@"state"];
            restaurant.address1=[dic objectForKey:@"address1"];
            restaurant.bestOfferName=[dic objectForKey:@"bestOfferName"];
            restaurant.saved = [dic objectForKey:@"isSaved"];
            restaurant.bookStatus=[dic objectForKey:@"bookStatus"];
            restaurant.title=[dic objectForKey:@"title"];
            restaurant.districtName=[dic objectForKey:@"districtName"];
            restaurant.menuCount=[dic objectForKey:@"menuPhotoCount"];
            restaurant.districtId=[dic objectForKey:@"districtId"];
            restaurant.phoneNumber=[dic objectForKey:@"phoneNumber"];
            restaurant.reviewCount=[dic objectForKey:@"reviewCount"];
            restaurant.longitude=[dic objectForKey:@"longitude"];
            restaurant.latitude=[dic objectForKey:@"latitude"];
            restaurant.restaurantId=[dic objectForKey:@"restaurantId"];
            restaurant.cuisineName=[dic objectForKey:@"cuisineName"];
            restaurant.stateName=[dic objectForKey:@"stateName"];
            restaurant.logoImage=[dic objectForKey:@"eventLogo"];
            restaurant.wellKnownFor = [dic objectForKey:@"wellKnownFor"];
            restaurant.trendingInstagram = [dic objectForKey:@"trendingInstagram"];
            restaurant.inJournal = [dic objectForKey:@"inJournal"];
            restaurant.journaArticlelTitle = [dic objectForKey:@"journaArticlelTitle"];
            restaurant.journalArticleId = [dic objectForKey:@"journalArticleId"];
            restaurant.priceName =[dic objectForKey:@"priceLevel"];;
            restaurant.photoCount = [dic objectForKey:@"photoCount"];
            restaurant.landMarkList = [dic objectForKey:@"landMarkList"];
            restaurant.goFoodLink = [dic objectForKey:@"goFoodLink"];
            restaurant.ratingCount = [dic objectForKey:@"ratingCount"];
            
            NSDictionary* yesterOpenDic=[dic objectForKey:@"yesterDayOpenTime"];
            restaurant.yesteropenDay=[yesterOpenDic objectForKey:@"openDay"];
            restaurant.yesteropenTime=[yesterOpenDic objectForKey:@"openTime"];
            restaurant.yesterclosedTime=[yesterOpenDic objectForKey:@"closedTime"];
            
            
            NSDictionary* nextOpenDay=[dic objectForKey:@"nextOpenDay"];
            restaurant.nextopenTime=[nextOpenDay objectForKey:@"openTime"];
            restaurant.nextclosedTime=[nextOpenDay objectForKey:@"closedTime"];
            restaurant.nextopenDay=[nextOpenDay objectForKey:@"openDay"];
            
            NSDictionary* secondOpenDic=[dic objectForKey:@"secondOpenDay"];
            restaurant.secondclosedTime=[secondOpenDic objectForKey:@"closedTime"];
            restaurant.secondopenTime=[secondOpenDic objectForKey:@"openTime"];
            restaurant.secondopenDay=[secondOpenDic objectForKey:@"openDay"];
            [restaurantArray addObject:restaurant];

        }

        
        
        successBlock(restaurantArray, [responseObject objectForKey:@"numFound"]);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock();
        
    }];
}

+ (void)getOffers:(void(^)(NSString *offs,NSString *offers))successBlock anderrorBlock:(void(^)())errorBlock{
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"select * from IMGOfferType order by typeId asc,off desc ;"] successBlock:^(FMResultSet *resultSet) {
        while([resultSet next]) {
            IMGOfferType *tag = [[IMGOfferType alloc] init];
            [tag setValueWithResultSet:resultSet];
            [arr addObject:tag];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        errorBlock();
        NSLog(@"FiltersViewController loadButtonGridView error when get datas:%@",error.description);
    }];
    NSMutableArray *offArr=[[NSMutableArray alloc]init];
    NSMutableArray *offerTpyeArr=[[NSMutableArray alloc]init];
    
    if (arr!=nil&&arr.count>0) {
        for (IMGOfferType *tag in arr) {
            
            if ([tag.typeId intValue]==0) {
                [offArr addObject:tag.off];
            }else{
                
                [offerTpyeArr addObject:tag.typeId];
            }
        }
        
    }
    NSString *offsStr = [offArr componentsJoinedByString:@","];
    NSString *offersStr = [offerTpyeArr componentsJoinedByString:@","];
    successBlock(offsStr,offersStr);
}


@end
