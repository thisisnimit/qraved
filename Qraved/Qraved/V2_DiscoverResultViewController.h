//
//  V2_DiscoverResultViewController.h
//  Qraved
//
//  Created by harry on 2017/10/2.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "DiscoverCategories.h"
@interface V2_DiscoverResultViewController : BaseViewController

@property (nonatomic, assign) BOOL isHomeNearBy;
@property (nonatomic, assign) BOOL isLocation;
@property (nonatomic, assign) DiscoverCategoriesModel *foodModel;
@property (nonatomic, assign) NSNumber *quicklySearchFoodTagId;
@property (nonatomic, strong) NSNumber *homeDistrictId;
@property (nonatomic, strong) NSNumber *homeLandmarkId;
@property (nonatomic, strong) NSString *homeLocationName;

@property (nonatomic, strong) NSArray *locationArr;

@end
