//
//  JourneyHandler.h
//  Qraved
//
//  Created by lucky on 16/9/9.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "DataHandler.h"

@interface JourneyHandler : DataHandler

+(void)getJourneyList:(NSDictionary *)params andBlock:(void (^)(NSArray *journeyDataArr))block;
+(void)getOtherJourneyList:(NSDictionary *)params andBlock:(void (^)(NSArray *journeyDataArr))block;
+(void)deletUploadPhoto:(NSDictionary*)params andBlock:(void (^)(NSDictionary *returnInfo))block;
@end
