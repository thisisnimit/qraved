//
//  v2_DiscoveryCategoriesViewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/9/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "v2_DiscoveryCategoriesViewController.h"
#import "CommonMethod.h"
#import "HomeService.h"
#import "LoadingView.h"
#import "v2_DiscoveryCategoriesTableViewCell.h"
#import "DiscoverCategories.h"
#import "V2_DiscoverResultViewController.h"
#import "FoodTagHeaderView.h"

@interface v2_DiscoveryCategoriesViewController ()<UITableViewDelegate, UITableViewDataSource>
{
   UITableView *DiscoveryCategoriesView;
    UIImageView *imageV;
    NSMutableArray *sectionArray;
    NSMutableArray *discoveryCategoriesArray;
    NSMutableArray *quiklySearchArr;
}
@end

@implementation v2_DiscoveryCategoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self loadNavButton];
    [self requestData];
    [self loadMainUI];
    sectionArray = [[NSMutableArray alloc] initWithObjects:@"1",@"1",@"1",@"1",@"1",@"1",@"1",@"1",@"1",@"1",@"1",@"1",@"1",@"1",nil];
    discoveryCategoriesArray = [NSMutableArray array];
    quiklySearchArr = [NSMutableArray array];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (discoveryCategoriesArray.count>0) {
        [DiscoveryCategoriesView reloadData];
    }
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
 
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
}

- (void)loadNavButton{
    self.navigationItem.title = @"Categories";
    
    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCancel.frame = CGRectMake(0, 0, 60, 30);
    [btnCancel addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    [btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
    [btnCancel setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    btnCancel.titleLabel.font = [UIFont systemFontOfSize:17];
    btnCancel.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnCancel];
}
- (void)back:(UIButton *)button{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)requestData{
    [self getQuicklySearchFoodTag];
    [self getFoodTagList];
}

- (void)getQuicklySearchFoodTag{
    [HomeService getQuicklySearchFoodTag:nil andBlock:^(id foodtagArr) {
        [quiklySearchArr addObjectsFromArray:foodtagArr];
        [DiscoveryCategoriesView reloadData];
    } andError:^(NSError *error) {
        
    }];
}

- (void)getFoodTagList{
    [[LoadingView sharedLoadingView] startLoading];
    [HomeService getCategories:nil andBlock:^(id locationArr) {
        
        NSLog(@"%@",locationArr);
        [discoveryCategoriesArray addObjectsFromArray:locationArr];
        [DiscoveryCategoriesView reloadData];
        [[LoadingView sharedLoadingView] stopLoading];
    } andError:^(NSError *error) {
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

- (void)loadMainUI{
    self.view.backgroundColor = [UIColor whiteColor];
    
    DiscoveryCategoriesView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-44) style:UITableViewStyleGrouped];
    DiscoveryCategoriesView.backgroundColor = [UIColor whiteColor];
    DiscoveryCategoriesView.delegate = self;
    DiscoveryCategoriesView.dataSource = self;
    
    [self.view addSubview:DiscoveryCategoriesView];
    DiscoveryCategoriesView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (discoveryCategoriesArray.count >0) {
        return discoveryCategoriesArray.count+1;
    }else{
        return 0;
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([[sectionArray objectAtIndex:section] integerValue] == 1) {
        return 1;
    }else{
        
        return 0;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0) {
        NSString *idenStr = @"foodTag";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
        }
      
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
        FoodTagHeaderView *header = [[FoodTagHeaderView alloc] initWithFrame:CGRectMake(0, 10, DeviceWidth, 90)];
        header.foodTagArr = quiklySearchArr;
        __weak typeof(self) weakSelf = self;
        header.quicklySearchFoodTag = ^(DiscoverCategoriesModel *model) {
            [weakSelf quicklySearchTapped:model.myID];
        };
        [cell.contentView addSubview:header];
        
        return cell;
    }else{
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        v2_DiscoveryCategoriesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[v2_DiscoveryCategoriesTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        DiscoverCategories *model = [discoveryCategoriesArray objectAtIndex:indexPath.section-1];
        if (model.childrens.count>0) {
            cell.model = model;
            __weak typeof(self) weakSelf = self;
            cell.gotoDiscoverResultPage = ^(DiscoverCategoriesModel *model) {
                
                [IMGAmplitudeUtil trackDiscoverWithName:@"SC - Filter Food Succeed" andFoodName:model.name andLocationName:nil andLocation:@"New SRP"];
                
                V2_DiscoverResultViewController *discoverResultPage = [[V2_DiscoverResultViewController alloc] init];
                discoverResultPage.foodModel = model;
                discoverResultPage.locationArr = self.locationArr;
                [weakSelf.navigationController pushViewController:discoverResultPage animated:YES];
            };
        }
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section ==0) {
        int lines = (int)quiklySearchArr.count/4;
        if (quiklySearchArr.count%4 >0) {
            lines = (int)quiklySearchArr.count/4 +1;
        }
        return 100*lines+36;

    }else{
        v2_DiscoveryCategoriesTableViewCell *cell = [[v2_DiscoveryCategoriesTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"heightCell"];
        DiscoverCategories *model = [discoveryCategoriesArray objectAtIndex:indexPath.section-1];
        cell.model = model;
        NSLog(@"%f",cell.pointY);
        return cell.pointY;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return [UIView new];
    }else{
        UIButton *view = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 36)];
        [view addTarget:self action:@selector(foldBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        view.tag = 100 + section;
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, DeviceWidth-45, 16)];
        lblTitle.font = [UIFont boldSystemFontOfSize:14];
        lblTitle.textColor = [UIColor color333333];
        [view addSubview:lblTitle];
        
        DiscoverCategories *model = [discoveryCategoriesArray objectAtIndex:section-1];
        lblTitle.text = model.name;
        
        
        imageV = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth - 30, 0, 12, 8)];
        imageV.image = [UIImage imageNamed:@"ic_city_down"];
        imageV.centerY = lblTitle.centerY;
        
        [view addSubview:imageV];
        
        return view;
    }

    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section==0) {
        return 0.001;
    }else{
        return 36;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}
- (void)foldBtnClick:(UIButton *)sender{
    
    //    NSLog(@"fold%ld",(long)btn.tag);
    //    btn.selected = !btn.selected;
    //    NSInteger newTag = btn.tag - 1000;
    //    mySection = newTag;
    
    if ([[sectionArray objectAtIndex:sender.tag - 100] intValue] == 0) {
        [sectionArray replaceObjectAtIndex:sender.tag - 100 withObject:@"1"];
        NSLog(@"%ld打开",(long)sender.tag);
        
        [CommonMethod unfoldAnimationWith:imageV];
    }
    else
    {
        [sectionArray replaceObjectAtIndex:sender.tag - 100 withObject:@"0"];
        NSLog(@"%ld关闭",(long)sender.tag);
        
        [CommonMethod foldAnimationWith:imageV];
    }
    
    
    
    [DiscoveryCategoriesView reloadData];
}

- (void)quicklySearchTapped:(NSNumber *)foodTagId{
    V2_DiscoverResultViewController *discoverResultPage = [[V2_DiscoverResultViewController alloc] init];
    if (![foodTagId isEqual:@0]) {
        discoverResultPage.quicklySearchFoodTagId = foodTagId;
    }
    discoverResultPage.locationArr = self.locationArr;
    [self.navigationController pushViewController:discoverResultPage animated:YES];
}


@end
