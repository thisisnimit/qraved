
// Created by Tek Yin on 5/16/17.
// Copyright (c) 2017 Imaginato. All rights reserved.
//


#import "WebServiceV2.h"
#import "V2_HomeModel.h"
#import "IMGEvent.h"
#import "IMGDiningGuide.h"
#import "IMGMediaComment.h"
#import "V2_BannerModel.h"
@implementation WebServiceV2 {

}
static AFHTTPSessionManager *_manager;

+ (AFHTTPSessionManager *)manager {
    if (!_manager) {
        _manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[[NSURL alloc] initWithString:QRAVED_WEB_SERVICE_SERVER]];
        _manager.responseSerializer = [AFJSONResponseSerializer serializer];
      
        _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        
        _manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        [_manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", @"text/plain", @"text/javascript", @"text/json",  nil];
        
        _manager.securityPolicy.allowInvalidCertificates = NO;
    }
    return _manager;
}

+ (NSString *)getFullLinkForImage:(NSString *)imgUrl {
    return [NSString stringWithFormat:@"https://img.qraved.co/v2/image/%@", imgUrl];
}

+ (void)getBanners:(NSDictionary *)params andBlock:(void(^)(id bannerArr))successBlock andError:(void(^)(NSError *error))errorBlock{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithDictionary:params];
    NSString *deviceId =[[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [paramDic setObject:@"ios" forKey:@"os"];
    [paramDic setObject:@"mobileapp" forKey:@"app"];
    [paramDic setObject:@"BhaxC05eXMSwQ4HK" forKey:@"apiKey"];
    [paramDic setObject:QRAVED_WEB_SERVICE_VERSION forKey:@"versionName"];
    [paramDic setObject:QRAVED_VERSION forKey:@"versionCode"];
    [paramDic setObject:deviceId forKey:@"deviceId"];
    [paramDic setObject:@"1000" forKey:@"screenId"];
    [paramDic setObject:@"1" forKey:@"screenAreaId"];
    [paramDic setObject:@"0" forKey:@"districtId"];
    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    [paramDic setObject:[self currentTimeString] forKey:@"timestamp"];
    
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        [paramDic setObject:user.userId forKey:@"userId"];
    }
    NSString *string = [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    if (string) {
        [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
    }else{
        [paramDic setObject:@"2" forKey:@"cityId"];
    }
    
    [[WebServiceV2 manager] GET:@"id/reco/v1/reco/banners" parameters:paramDic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (responseObject != nil) {
            [CommonMethod setJsonToFile:HOME_CACHE_BANNER andJson:responseObject];
        }
        successBlock([CommonMethod getHomeBanner:responseObject]);
        NSLog(@"%@",responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock(error);
    }];
}

+ (void)getStripBanner:(NSDictionary *)params andBlock:(void(^)(id bannerArr))successBlock andError:(void(^)(NSError *error))errorBlock{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithDictionary:params];
    NSString *deviceId =[[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [paramDic setObject:@"ios" forKey:@"os"];
    [paramDic setObject:@"mobileapp" forKey:@"app"];
    [paramDic setObject:@"BhaxC05eXMSwQ4HK" forKey:@"apiKey"];
    [paramDic setObject:QRAVED_WEB_SERVICE_VERSION forKey:@"versionName"];
    [paramDic setObject:QRAVED_VERSION forKey:@"versionCode"];
    [paramDic setObject:deviceId forKey:@"deviceId"];
    [paramDic setObject:@"1000" forKey:@"screenId"];
    [paramDic setObject:@"2" forKey:@"screenAreaId"];
    [paramDic setObject:@"0" forKey:@"districtId"];
    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    [paramDic setObject:[self currentTimeString] forKey:@"timestamp"];
    
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        [paramDic setObject:user.userId forKey:@"userId"];
    }
//    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
    NSString *string = [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    if (string) {
        [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
    }else{
        [paramDic setObject:@"2" forKey:@"cityId"];
    }
    [[WebServiceV2 manager] GET:@"id/reco/v1/reco/banners" parameters:paramDic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (responseObject != nil) {
            [CommonMethod setJsonToFile:HOME_CACHE_STRIPBANNER andJson:responseObject];
        }
        
        successBlock([CommonMethod getHomeBanner:responseObject]);
        NSLog(@"%@",responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock(error);
    }];
}

+ (void)getGoFoodBanner:(NSDictionary *)params andBlock:(void(^)(NSArray *bannerArr))successBlock andError:(void(^)(NSError *error))errorBlock{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithDictionary:params];
    NSString *deviceId =[[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [paramDic setObject:@"ios" forKey:@"os"];
    [paramDic setObject:@"mobileapp" forKey:@"app"];
    [paramDic setObject:@"BhaxC05eXMSwQ4HK" forKey:@"apiKey"];
    [paramDic setObject:QRAVED_WEB_SERVICE_VERSION forKey:@"versionName"];
    [paramDic setObject:QRAVED_VERSION forKey:@"versionCode"];
    [paramDic setObject:deviceId forKey:@"deviceId"];
    [paramDic setObject:@"1001" forKey:@"screenId"];
    [paramDic setObject:@"1" forKey:@"screenAreaId"];
    [paramDic setObject:@"0" forKey:@"districtId"];
    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    [paramDic setObject:[self currentTimeString] forKey:@"timestamp"];
    
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        [paramDic setObject:user.userId forKey:@"userId"];
    }
    //    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
    NSString *string = [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    if (string) {
        [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
    }else{
        [paramDic setObject:@"2" forKey:@"cityId"];
    }
    
    
    [[WebServiceV2 manager] GET:@"id/reco/v1/reco/banners" parameters:paramDic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSMutableArray *bannerArr = [NSMutableArray array];
        for (NSDictionary *dic in responseObject) {
            [bannerArr addObject:dic];
        }
        successBlock(bannerArr);
        NSLog(@"%@",responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock(error);
    }];
}

+ (void)getHomeData:(NSString *)page andBlock:(void(^)(id homedata))successBlock andError:(void(^)(NSError *error))errorBlock{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];
    NSString *deviceId =[[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [paramDic setObject:page forKey:@"page"];
    [paramDic setObject:@"ios" forKey:@"os"];
    [paramDic setObject:@"mobileapp" forKey:@"app"];
    [paramDic setObject:@"BhaxC05eXMSwQ4HK" forKey:@"apiKey"];
    [paramDic setObject:QRAVED_WEB_SERVICE_VERSION forKey:@"versionName"];
    [paramDic setObject:QRAVED_VERSION forKey:@"versionCode"];
    [paramDic setObject:deviceId forKey:@"deviceId"];
    [paramDic setObject:@"1000" forKey:@"screenId"];
    [paramDic setObject:@"3" forKey:@"screenAreaId"];
    [paramDic setObject:@"0" forKey:@"districtId"];
    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    [paramDic setObject:[self currentTimeString] forKey:@"timestamp"];
    
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        [paramDic setObject:user.userId forKey:@"userId"];
    }
    NSString *string = [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    if (string) {
        [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
    }else{
        [paramDic setObject:@"2" forKey:@"cityId"];
    }
    
    [[WebServiceV2 manager] GET:@"id/reco/v1/reco/components" parameters:paramDic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (responseObject != nil && [page intValue] == 1) {
            [CommonMethod setJsonToFile:HOME_CACHE_COMPONENTS andJson:responseObject];
        }
        successBlock([CommonMethod getHomeComponents:responseObject]);
      
        NSLog(@"%@",responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock(error);
    }];
}

+ (void)instagramRestaurant:(NSDictionary *)params andBlock:(void(^)(IMGRestaurant *restaurant,NSNumber *instagramLocationId,NSDictionary *photoDic))successBlock andError:(void(^)(NSError *error))errorBlock{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithDictionary:params];
    NSString *deviceId =[[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [paramDic setObject:@"ios" forKey:@"os"];
    [paramDic setObject:@"mobileapp" forKey:@"app"];
    [paramDic setObject:QRAVED_APIKEY forKey:@"apiKey"];
    [paramDic setObject:QRAVED_WEB_SERVICE_VERSION forKey:@"versionName"];
    [paramDic setObject:QRAVED_VERSION forKey:@"versionCode"];
    [paramDic setObject:deviceId forKey:@"deviceId"];
    [paramDic setObject:@"1000" forKey:@"screenId"];
    [paramDic setObject:@"3" forKey:@"screenAreaId"];
    [paramDic setObject:@"0" forKey:@"districtId"];
    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    [paramDic setObject:[self currentTimeString] forKey:@"timestamp"];
    
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        [paramDic setObject:user.userId forKey:@"userId"];
    }
  
    [[WebServiceV2 manager] GET:@"id/home/api/restaurant/snippet" parameters:paramDic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *restaurantDic = [[responseObject objectForKey:@"data"] objectForKey:@"restaurant_detail"];
        if (restaurantDic.count >0) {
            IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
            restaurant.restaurantId = [restaurantDic objectForKey:@"restaurant_id"];
            restaurant.title = [restaurantDic objectForKey:@"restaurant_name"];
            if ([[restaurantDic objectForKey:@"rating"] isEqual:[NSNull null]]) {
                restaurant.ratingScore = @"0";
            }else{
                restaurant.ratingScore = [restaurantDic objectForKey:@"rating"];
            }
            
            restaurant.reviewCount = [restaurantDic objectForKey:@"total_review"];
            restaurant.saved = [restaurantDic objectForKey:@"user_saved"];
            restaurant.imageUrl = [restaurantDic objectForKey:@"path_original"];
            restaurant.cuisineName = [restaurantDic objectForKey:@"cuisine"];
            restaurant.districtName = [restaurantDic objectForKey:@"district"];
            restaurant.longitude = [restaurantDic objectForKey:@"longitude"];
            restaurant.latitude = [restaurantDic objectForKey:@"latitude"];
            restaurant.priceName = [[restaurantDic objectForKey:@"price_level"] objectForKey:@"priceName"];
            restaurant.displayTime = [restaurantDic objectForKey:@"open_hour"];
            restaurant.openNow = [restaurantDic objectForKey:@"open_now"];
            restaurant.state = [restaurantDic objectForKey:@"state"];

            restaurant.restaurant_landmark = [restaurantDic objectForKey:@"restaurant_landmark"];
            restaurant.goFoodLink = [restaurantDic objectForKey:@"gofood_link"];
            
            NSMutableDictionary *dic = [NSMutableDictionary dictionary];
            [dic setObject:[[responseObject objectForKey:@"data"] objectForKey:@"photos_instagram"] forKey:@"photos_instagram"];
            [dic setObject:[[responseObject objectForKey:@"data"] objectForKey:@"photos_menu"] forKey:@"photos_menu"];
            [dic setObject:[[responseObject objectForKey:@"data"] objectForKey:@"photos_restaurant"] forKey:@"photos_restaurant"];
            [dic setObject:[[responseObject objectForKey:@"data"] objectForKey:@"photos_user"] forKey:@"photos_user"];
            
            successBlock(restaurant,[[responseObject objectForKey:@"data"] objectForKey:@"instagram_location"],dic);

        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock(error);
    }];

}



+ (void)relatedArticleArray:(NSDictionary *)params andBlock:(void(^)(NSArray *relatedArr))successBlock andError:(void(^)(NSError *error))errorBlock{
    
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithDictionary:params];
    NSString *deviceId =[[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [paramDic setObject:@"ios" forKey:@"os"];
    [paramDic setObject:@"mobileapp" forKey:@"app"];
    [paramDic setObject:QRAVED_APIKEY forKey:@"apiKey"];
    [paramDic setObject:QRAVED_WEB_SERVICE_VERSION forKey:@"versionName"];
    [paramDic setObject:QRAVED_VERSION forKey:@"versionCode"];
    [paramDic setObject:deviceId forKey:@"deviceId"];
    [paramDic setObject:@"8c5f19906862e01d6b0c425a9f2ea82f7906de46dc9344802574b4b0cee2df1a" forKey:@"key"];
    
    [[WebServiceV2 manager] GET:@"id/articles/api/articles/getRelatedArticle" parameters:paramDic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSMutableArray *relatedPostArrM = [[NSMutableArray alloc] init];
        NSArray *postedArticlesArrFromService = [[responseObject objectForKey:@"result"] objectForKey:@"data"];
        if (![postedArticlesArrFromService isEqual:[NSNull null]]) {
            for (NSDictionary *dic in postedArticlesArrFromService)
            {
                IMGMediaComment *journal = [[IMGMediaComment alloc] init];
                journal.mediaCommentId = [dic objectForKey:@"articleId"];
                
                journal.title = [dic objectForKey:@"articleTitle"];
                journal.imageUrl = [dic objectForKey:@"articlePhoto"];
                
                [relatedPostArrM addObject:journal];
            }
        }
        
        successBlock(relatedPostArrM);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

+ (NSString *)currentTimeString{
    NSDate *date = [NSDate date];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];

    [formatter setDateStyle:NSDateFormatterMediumStyle];
    
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [formatter setDateFormat:@"YYYYMMddHHmmss"];
    NSString *dateTime = [formatter stringFromDate:date];
    return dateTime;
}


@end
