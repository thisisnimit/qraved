//
//  InstagramDetailController.m
//  Qraved
//
//  Created by Tek Yin on 5/11/17.
//  Copyright © 2017 Imaginato. All rights reserved.
//

#import "InstagramDetailController.h"
#import "V2_InstagramPhotoView.h"
#import "V2_InstagramModel.h"

@interface InstagramDetailController ()

@end

@implementation InstagramDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.uDetail.layer.shadowColor   = [[UIColor color333333] CGColor];
    self.uDetail.layer.shadowRadius  = 1.5f;
    self.uDetail.layer.shadowOpacity = .9;
    self.uDetail.layer.shadowOffset  = CGSizeZero;
    self.uDetail.layer.masksToBounds = NO;
    
    
   
    
}

- (void)setInstagramModel:(V2_InstagramModel *)instagramModel{
    _instagramModel = instagramModel;
    
    [self createTruncatedDetail:self.instagramModel.caption];
    
}
- (void)createTruncatedDetail:(NSString *)str {
//    NSString                  *expansionToken = @"... Read More";
//    PatternTapResponder       tap             = ^(NSString *string) {
//        self.uDetail.numberOfLines = 0;
//        [self.uDetail setText:str withTruncation:NO];
//        self.uDetailDismissButton.hidden = NO;
//        self.uDetailDismissButton.alpha  = 0;
//
//        [UIView animateWithDuration:0.2
//                         animations:^{
//                             self.uDetailDismissButton.alpha = 1;
//                             [self.view layoutIfNeeded];
//                         }];
//    };
//    NSMutableAttributedString *attribString;
//    attribString = [[NSMutableAttributedString alloc] initWithString:expansionToken
//                                                          attributes:@{
//                                                                  NSForegroundColorAttributeName: [UIColor color2D8DED],
//                                                                  NSFontAttributeName: self.uDetail.font,
//                                                                  RLTapResponderAttributeName: tap}];
//    [self.uDetail setAttributedTruncationToken:attribString];
//    [self.uDetail setText:str withTruncation:YES];
//    self.uDetail.numberOfLines = 2;
//    if (!self.uDetailDismissButton.hidden) {
//        [UIView animateWithDuration:0.2
//                         animations:^{
//                             self.uDetailDismissButton.alpha = 1;
//                             [self.view layoutIfNeeded];
//                         }
//                         completion:^(BOOL finished) {
//                             self.uDetailDismissButton.hidden = YES;
//                         }];
//        self.uDetailDismissButton.hidden = YES;
//    }
//    [self.view layoutIfNeeded];
}

- (IBAction)doDismissDetail:(id)sender {
    //[self createTruncatedDetail:self.data.caption];
}

+ (UIViewController *)createWithData:(V2_InstagramModel *)model {
    InstagramDetailController *controller = [[InstagramDetailController alloc] initWithNibName:@"InstagramViewController" bundle:nil];
    controller.instagramModel = model;
    return controller;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];

    [self.uInstagramPhoto loadImageUrl:self.instagramModel.standard_resolution_image];
    self.uSender.text = [NSString stringWithFormat:@"@%@", self.instagramModel.instagram_user_name];//photo's
    self.uDate.text   = self.instagramModel.instagram_create_date;
    self.uDetail.text = self.instagramModel.caption;
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doClose:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
