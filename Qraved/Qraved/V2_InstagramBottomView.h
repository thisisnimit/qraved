//
//  V2_InstagramBottomView.h
//  Qraved
//
//  Created by harry on 2017/7/31.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface V2_InstagramBottomView : UIView

@property (nonatomic, strong) IMGRestaurant *restaurant;
@property (nonatomic, strong) NSNumber *locationId;
@property (nonatomic, strong) NSDictionary *photoDictionary;

@property (nonatomic, assign) CGFloat currentPointY;
@property (nonatomic, copy) void (^saveRestaurant)(IMGRestaurant *restaurant);
@property (nonatomic, copy) void (^closeMiniRDP)();
@property (nonatomic, copy) void (^gotoRDP)();
@property (nonatomic, copy) void (^gotoMiniRDP)();
@property (nonatomic, strong) id controller;

//@property (nonatomic, strong)  void  (^jumpToMiniRDP)(NSNumber *restaurantID);

- (void)freshSavedButton:(BOOL)liked;

@end
