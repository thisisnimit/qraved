//
//  V2_BannerModel.m
//  Qraved
//
//  Created by harry on 2017/7/28.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_BannerModel.h"

@implementation V2_BannerModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"id"]) {
        self.bannerId = value;
    }else if ([key isEqualToString:@"image"]){
        self.bannerImage = value;
    }else if ([key isEqualToString:@"description"]){
        self.bannerDescription = value;
    }
}

@end
