//
//  FilterCollectionViewCell.h
//  Qraved
//
//  Created by harry on 2017/12/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstagramFilterModel.h"
@interface FilterCollectionViewCell : UICollectionViewCell


@property (nonatomic, strong) InstagramFilterModel *model;

@end
