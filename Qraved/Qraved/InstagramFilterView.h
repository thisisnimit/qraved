//
//  InstagramFilterView.h
//  Qraved
//
//  Created by harry on 2017/12/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstagramFilterModel.h"
@interface InstagramFilterView : UIView

@property (nonatomic, assign) BOOL isFromHome;
@property (nonatomic, copy) NSArray *filterArr;

@property (nonatomic, copy) void(^chooseFilter)(InstagramFilterModel *filterModel);
@end
