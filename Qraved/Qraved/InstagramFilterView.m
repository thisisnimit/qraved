//
//  InstagramFilterView.m
//  Qraved
//
//  Created by harry on 2017/12/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "InstagramFilterView.h"
#import "FilterCollectionViewCell.h"

@interface InstagramFilterView ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    UICollectionView *filterCollectionView;
}

@end

@implementation InstagramFilterView

- (instancetype)init{
    self = [super init];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    self.backgroundColor = [UIColor whiteColor];
    
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(66, 105);
    layout.minimumLineSpacing = 13;
    layout.minimumInteritemSpacing = 13;
    layout.sectionInset = UIEdgeInsetsMake(0, 12, 0, 12);
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    filterCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,0, DeviceWidth, 105) collectionViewLayout:layout];
    filterCollectionView.showsHorizontalScrollIndicator = NO;
    filterCollectionView.backgroundColor = [UIColor whiteColor];
    filterCollectionView.delegate = self;
    filterCollectionView.dataSource = self;
    [self addSubview:filterCollectionView];
    
    
    [filterCollectionView registerClass:[FilterCollectionViewCell class] forCellWithReuseIdentifier:@"filterCell"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.filterArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    FilterCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"filterCell" forIndexPath:indexPath];
    InstagramFilterModel *model = [self.filterArr objectAtIndex:indexPath.item];
    cell.model = model;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    InstagramFilterModel *model = [self.filterArr objectAtIndex:indexPath.item];
    if (!self.isFromHome) {
        model.isSelect = !model.isSelect;
        [self resetFilterArr:indexPath.item];
        
        [filterCollectionView reloadData];
    }
    if (self.chooseFilter) {
        self.chooseFilter(model);
    }
}

- (void)resetFilterArr:(NSInteger)index{
    for (int i = 0; i < self.filterArr.count; i ++) {
        if (i != index) {
            InstagramFilterModel *model
            = [self.filterArr objectAtIndex:i];
            model.isSelect = NO;
        }
    }
}

- (void)setFilterArr:(NSArray *)filterArr{
    _filterArr = filterArr;
    [filterCollectionView reloadData];
}

@end
