//
//  V2_RestaurantModel.m
//  Qraved
//
//  Created by harry on 2017/6/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_RestaurantModel.h"

@implementation V2_RestaurantModel


-(NSString *)distance{

    double lon2=[[[NSUserDefaults standardUserDefaults]objectForKey:@"longitude"] doubleValue];
    double lat2=[[[NSUserDefaults standardUserDefaults]objectForKey:@"latitude"] doubleValue];
    NSLog(@"%f,%f",lon2,lat2);
    double dd = M_PI/180;
    double x1=[self.latitude doubleValue]*dd,x2=lat2 *dd;
    double y1=[self.longitude doubleValue]*dd,y2=lon2 *dd;
    double R = 6378138;
    double distance = (2*R*asin(sqrt(2-2*cos(x1)*cos(x2)*cos(y1-y2) - 2*sin(x1)*sin(x2))/2));
    
    if(distance < 1000){
        return [[NSString alloc] initWithFormat:@"%dm",[[NSNumber numberWithDouble:distance] intValue]];
    }else if(distance > 8000*1000){
        return @"";
    }else{
        long kmDistance = (long)distance/1000;
        return [[NSString alloc] initWithFormat:@"%ldkm",kmDistance];
    }
}


@end
