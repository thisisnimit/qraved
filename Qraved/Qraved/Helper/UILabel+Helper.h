//
//  UILabel+Helper.h
//  PocketPlan
//
//  Created by Libo Liu on 10/17/13.
//  Copyright (c) 2013 com.imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Helper)

-(void)configButtonTitle;
-(void)configToTitle;
-(void)configDetailSectionTitle;
-(void)configToMoreLink;
-(void)configToTitleCaption;
-(void)configToSectionTitle;
-(void)configToBody;
-(void)configToSubsectionBody;
-(void)configToDefaultFont:(CGFloat)size;
-(void)configToDefaultBoldFont:(CGFloat)size;
-(void)configToDefault;

-(void)configDetailContent;
-(void)configDetailBodyContent;

-(void)configSearchRestaurantTitle;

-(void)configToLoginCaption;

-(void)configPickerTitle;

-(void)configPickerSubTitle;


-(UIFont *)defaultFont:(CGFloat)fontSize;

-(CGSize)getLabelSize:(NSString *)string withFont:(UIFont *)font;
-(CGSize)sizeWithWidth:(float)width;

-(CGSize)sizeWithHeight:(float)height;

-(float)resizeToFit;
-(float)expectedHeight;
-(float)expectedHeightWithNumberOfLines:(float)numberOfLines;

-(void)resizeToStretch:(CGFloat) space;
-(void)resizeToStretch;
-(float)expectedWidth;

- (float)pointX;
- (float)pointY;

-(CGFloat)calculatedCommentTextHeight:(NSString*)comment;
@end
