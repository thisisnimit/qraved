//
//  NSDate+Helper.m
//  Qraved
//
//  Created by Libo Liu on 10/9/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

/*
Now you want all the string formats that can be used with NSDateFormatter. Here is that

a: AM/PM

A: 0~86399999 (Millisecond of Day)

c/cc: 1~7 (Day of Week)

ccc: Sun/Mon/Tue/Wed/Thu/Fri/Sat

cccc: Sunday/Monday/Tuesday/Wednesday/Thursday/Friday/Saturday

d: 1~31 (0 padded Day of Month)

D: 1~366 (0 padded Day of Year)

e: 1~7 (0 padded Day of Week)

E~EEE: Sun/Mon/Tue/Wed/Thu/Fri/Sat

EEEE: Sunday/Monday/Tuesday/Wednesday/Thursday/Friday/Saturday

F: 1~5 (0 padded Week of Month, first day of week = Monday)

g: Julian Day Number (number of days since 4713 BC January 1)

G~GGG: BC/AD (Era Designator Abbreviated)

GGGG: Before Christ/Anno Domini

h: 1~12 (0 padded Hour (12hr))

H: 0~23 (0 padded Hour (24hr))

k: 1~24 (0 padded Hour (24hr)
         
         K: 0~11 (0 padded Hour (12hr))
         
         L/LL: 1~12 (0 padded Month)
         
         LLL: Jan/Feb/Mar/Apr/May/Jun/Jul/Aug/Sep/Oct/Nov/Dec
         
         LLLL: January/February/March/April/May/June/July/August/September/October/November/December
         
         m: 0~59 (0 padded Minute)
         
         M/MM: 1~12 (0 padded Month)
         
         MMM: Jan/Feb/Mar/Apr/May/Jun/Jul/Aug/Sep/Oct/Nov/Dec
         
         MMMM: January/February/March/April/May/June/July/August/September/October/November/December
         
         q/qq: 1~4 (0 padded Quarter)
         
         qqq: Q1/Q2/Q3/Q4
         
         qqqq: 1st quarter/2nd quarter/3rd quarter/4th quarter
         
         Q/QQ: 1~4 (0 padded Quarter)
         
         QQQ: Q1/Q2/Q3/Q4
         
         QQQQ: 1st quarter/2nd quarter/3rd quarter/4th quarter
         
         s: 0~59 (0 padded Second)
         
         S: (rounded Sub-Second)
         
         u: (0 padded Year)
         
         v~vvv: (General GMT Timezone Abbreviation)
         
         vvvv: (General GMT Timezone Name)
         
         w: 1~53 (0 padded Week of Year, 1st day of week = Sunday, NB: 1st week of year starts from the last Sunday of last year)
         
         W: 1~5 (0 padded Week of Month, 1st day of week = Sunday)
         
         y/yyyy: (Full Year)
         
         yy/yyy: (2 Digits Year)
         
         Y/YYYY: (Full Year, starting from the Sunday of the 1st week of year)
         
         YY/YYY: (2 Digits Year, starting from the Sunday of the 1st week of year)
         
         z~zzz: (Specific GMT Timezone Abbreviation)
         
         zzzz: (Specific GMT Timezone Name)
         
         Z: +0000 (RFC 822 Timezone)
         
         ==========================================
         */


#import "NSDate+Helper.h"

@implementation NSDate (Helper)
-(NSString*)getBookDateFormatString{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd"];
    return [dateFormatter stringFromDate:self];
}
-(NSString*)getYYYY_MM_DDCenterLineFormatString{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [dateFormatter stringFromDate:self];
}
-(NSString*)get_MM_DD_YYYYFormatString{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    return [dateFormatter stringFromDate:self];
}
-(int)getTimeInt_HH_MM{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"HHmm"];
    return [[dateFormatter stringFromDate:self] intValue];
}
-(NSString*)getDateFullString{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"EEEE, LLLL d, yyyy"];
    return [dateFormatter stringFromDate:self];
}
-(NSString*)getHDateFullString{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"LLLL d, yyyy"];
    return [dateFormatter stringFromDate:self];
}
-(NSString*)getDateTimeFullString{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"EEEE, LLLL d, yyyy, h:mm a"];
    return [dateFormatter stringFromDate:self];
}
-(NSString*)getAM_PM_String{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"h:mm a"];
    return [dateFormatter stringFromDate:self];
}

-(NSString *)getShortTime
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"h:mm"];
    return [dateFormatter stringFromDate:self];
}

-(NSString *)getBookingTimeString
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"EEEE, MMM d"];
    return [dateFormatter stringFromDate:self];
}

-(NSString *)getWeekString
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"EEEE"];
    return [[dateFormatter stringFromDate:self] uppercaseString];
}

-(NSString *)getLowerWeekString
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"EEEE"];
    return [dateFormatter stringFromDate:self];
}

-(NSString *)getMonthDayString
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"LLL dd"];
    return [dateFormatter stringFromDate:self];

}
-(NSString *)getDayMonthString
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"LLL d"];
    return [[dateFormatter stringFromDate:self] uppercaseString];
    
}
-(NSString *)getDayOfWeekString
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"e"];
    return [dateFormatter stringFromDate:self];
}

-(NSString *)getDayMonthYearString
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"ddLLLyyyy"];
    return [dateFormatter stringFromDate:self];
}

-(NSString*)getYYYY_MM_DD_hh_mmssFormatString
{
    
    NSTimeZone *currentTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"WIT"];
    NSTimeZone *utcTimeZone = [NSTimeZone localTimeZone];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:self];
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:self];
    NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
    
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:self];
    
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [dateFormatter stringFromDate:destinationDate];
}
-(NSString*)getYYYY_MM_DDFormatString
{
    
    NSTimeZone *currentTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"WIT"];
    NSTimeZone *utcTimeZone = [NSTimeZone localTimeZone];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:self];
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:self];
    NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
    
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:self];
    
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    return [dateFormatter stringFromDate:destinationDate];
}

-(NSString *)getUserPointsTimeFormat
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"d MMM yyyy"];
    return [dateFormatter stringFromDate:self];
}
//July 2014
-(NSString *)getMonthAndYearFormat
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"EEEE, LLLL d, yyyy"];
    return [dateFormatter stringFromDate:self];
}
-(NSString *)getMMDD_hhmmFormatString;{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"LLL dd, HH:mm"];
    return [dateFormatter stringFromDate:self];
}


@end
