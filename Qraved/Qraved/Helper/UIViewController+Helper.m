//
//  UIViewController+Helper.m
//  Qraved
//
//  Created by Libo Liu on 10/8/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#define LOADINGVIEWTAG 146852

#import "UIViewController+Helper.h"
#import "NavigationBarButtonItem.h"
#import "Helper.h"
@implementation UIViewController (Helper)

-(void)setIos7Layout{
    if( [UIDevice isLaterThanIos6]) {
        self.edgesForExtendedLayout= UIRectEdgeNone;
        self.navigationController.navigationBar.translucent = NO;
    }
}

-(void)setBackBarButtonOffset30{
    NavigationBarButtonItem *navigationBarLeftButton = [[NavigationBarButtonItem alloc ]initWithImage:[UIImage imageNamed:NavigationBackImage] andTitle:@"" target:self action:@selector(goToBackViewController) width:43.0f offset:-15-[UIDevice heightDifference]];
    self.navigationItem.leftBarButtonItem = navigationBarLeftButton;
}
-(void)setBackBarButton{
    NavigationBarButtonItem *navigationBarLeftButton = [[NavigationBarButtonItem alloc ]initWithImage:[UIImage imageNamed:NavigationBackImage] andTitle:@"" target:self action:@selector(goToBackViewController) width:43.0f offset:0-[UIDevice heightDifference]];
    self.navigationItem.leftBarButtonItem = navigationBarLeftButton;
}

-(void)setCloseBarButton{
    NavigationBarButtonItem *navigationBarLeftButton = [[NavigationBarButtonItem alloc ]initWithImage:[UIImage imageNamed:NavigationCloseImage] andTitle:@"" target:self action:@selector(goToBackViewController) width:43.0f offset:[UIDevice isLaterThanIos6]?30:0];
    self.navigationItem.rightBarButtonItem = navigationBarLeftButton;

}

- (void)goToBackViewController{
    [self removeNotifications];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)removeNotifications{
    
}

-(void)showLoadingView
{
//    self = [super initWithFrame:CGRectMake(0, 100, 100, 100)];
    UIView *loadingView=[[UIView alloc]initWithFrame:CGRectMake(0, 100, 100, 100)];
    [loadingView setTag:LOADINGVIEWTAG];
    UIImageView *loadingImgView;
    
//    CGRect rect = [UIScreen mainScreen].bounds;
    CGRect rect=self.view.frame;
    loadingView.center = CGPointMake(rect.size.width/2, rect.size.height/2);
        NSMutableArray *muArr =[NSMutableArray array];
        for (int i = 1; i <= 9; i++) {
            NSString *imgStr =[NSString stringWithFormat:@"GIF%d",i];
            UIImage *img = [UIImage imageNamed:imgStr];
            [muArr addObject:img];
        }
        for (int i = 10; i <= 30; i++) {
            NSString *imgStr =[NSString stringWithFormat:@"GIF%d",i];
            UIImage *img = [UIImage imageNamed:imgStr];
            [muArr addObject:img];
        }
        loadingImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
        UIImageView *loadingBGView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loading-background"]];
        loadingBGView.frame = CGRectMake(0, 0, 100, 100);
        
        [loadingView addSubview:loadingBGView];
        [loadingView addSubview:loadingImgView];
        
        loadingImgView.animationImages = muArr;
        loadingImgView.animationDuration=1.0;
    
    [loadingImgView startAnimating];
    [self.view addSubview:loadingView];
}

-(void)removeLoadingView
{
    UIView *loadView=[self.view viewWithTag:LOADINGVIEWTAG];
    if(loadView)
    {
        [loadView removeFromSuperview];
    }
}

-(int)returnIOSVersion
{
    return [[[[UIDevice currentDevice] systemVersion] substringToIndex:1] intValue];
}

@end
