//
//  NSDictionary+Helper.m
//  Qraved
//
//  Created by Shine Wang on 9/30/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "NSDictionary+Helper.h"
#import "NSString+Helper.h"
@implementation NSDictionary (Helper)
- (NSString*)filterStringForKey:(id)aKey
{
    NSString *value = [self objectForKey:aKey];
    return [value filterHtml];
}


@end
