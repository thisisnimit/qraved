//
//  UIView+Helper.m
//  Qraved
//
//  Created by Libo Liu on 10/8/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "UIView+Helper.h"
#import "UIConstants.h"
#import "UIColor+Helper.h"
@implementation UIView (Helper)


-(CGFloat)endPointX
{
    return self.frame.origin.x+self.frame.size.width;
}
-(CGFloat)endPointY
{
    return self.frame.origin.y+self.frame.size.height;
}
-(CGFloat)startPointX
{
    return self.frame.origin.x;
}
-(CGFloat)startPointY
{
    return self.frame.origin.y;
}
-(id)initDetailFeatureViewWithOrignY:(float)OrignY andText:(NSString*)text
{
    self= [self init];
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, OrignY, DeviceWidth, 26)];
    view.backgroundColor= [UIColor colorFBFBFB];
    UILabel *openingHoursLabel = [[UILabel alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 1, DeviceWidth-LEFTLEFTSET, 26)];
    openingHoursLabel.text = text;
    openingHoursLabel.backgroundColor= [UIColor colorFBFBFB];
    openingHoursLabel.font = [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:13];
    openingHoursLabel.textColor = [UIColor color222222];
    
    [view addSubview:openingHoursLabel];
    
    UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 1)];
    lineImage.image = [UIImage imageNamed:@"CutOffRule"];
    [view addSubview:lineImage];

    
    UIImageView *lineImage1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, view.frame.size.height, DeviceWidth, 1)];
    lineImage1.image = [UIImage imageNamed:@"CutOffRule"];
    [view addSubview:lineImage1];
    
    return view;
}
@end
