//
//  NSDate+Helper.h
//  Qraved
//
//  Created by Libo Liu on 10/9/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Helper)
-(NSString*)getBookDateFormatString;

-(NSString*)getYYYY_MM_DDCenterLineFormatString;
-(int)getTimeInt_HH_MM;

//Thursday, August 14, 2014
-(NSString *)getDateFullString;
//Thursday, August 14, 2014, 1:29 PM
-(NSString *)getDateTimeFullString;
//1:29 PM
-(NSString *)getAM_PM_String;
//1:29
-(NSString *)getShortTime;
//08/14/2014
-(NSString *)get_MM_DD_YYYYFormatString;
//Thursday, Aug 14
-(NSString *)getBookingTimeString;
//THURSDAY
-(NSString *)getWeekString;
//Thursday
-(NSString *)getLowerWeekString;
//AUG 14
-(NSString *)getMonthDayString;
//14 AUG
-(NSString *)getDayMonthString;
//5
-(NSString *)getDayOfWeekString;
//14-08-2014
-(NSString *)getDayMonthYearString;
//2014-08-14 12:29:55
-(NSString *)getYYYY_MM_DD_hh_mmssFormatString;
//2014-08-14
-(NSString*)getYYYY_MM_DDFormatString;
//14 Aug 2014
-(NSString *)getUserPointsTimeFormat;
//Mar 14, 12:22
-(NSString *)getMMDD_hhmmFormatString;
//Mar 14, 2014
-(NSString*)getHDateFullString;
//July 2014
-(NSString *)getMonthAndYearFormat;




@end
