//
//  NSArray+Boundary.m
//  Qraved
//
//  Created by apple on 16/8/24.
//  Copyright © 2016年 Imaginato. All rights reserved.
//防止数组越界

#import "NSArray+Boundary.h"
#import <objc/runtime.h>
@implementation NSArray (Boundary)
+(void)load{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        SEL safeSel=@selector(safeObjectAtIndex:);
        SEL unsafeSel=@selector(objectAtIndex:);
        
        Class myClass = NSClassFromString(@"__NSArrayI");
        Method safeMethod=class_getInstanceMethod (myClass, safeSel);
        Method unsafeMethod=class_getInstanceMethod (myClass, unsafeSel);
        
        method_exchangeImplementations(unsafeMethod, safeMethod);
        
    });
}

-(id)safeObjectAtIndex:(NSUInteger)index{
    
    if (index>(self.count-1)) {
        return nil;
    }
    else{
        return [self safeObjectAtIndex:index];
    }
}
@end
