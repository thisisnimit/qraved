//
//  UIImageView+Helper.m
//  Qraved
//
//  Created by Laura on 14-9-5.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "UIImageView+Helper.h"
#import "UIImage+Helper.h"
#import "UIColor+Helper.h"
#import "objc/runtime.h"

#define SHADOW_IMAGE [UIImage imageWithColor:[UIColor colorE4E4E4] size:CGSizeMake(DeviceWidth, 8)]
#define SHADOWIMAGE [UIImage imageNamed:@"shadow"]
#define SHADOWIMAGE_v2 [UIImage imageNamed:@"shadow_v2"]
static char operationKey;

@implementation UIImageView (Helper)
-(id)initShadowImageViewWithShadowOriginY:(float)OriginY andHeight:(float)height
{
    UIImageView *shadowView=[[UIImageView alloc]initWithFrame:CGRectMake(0, OriginY, DeviceWidth, height+6)];
    [shadowView setImage:SHADOWIMAGE];
    return shadowView;
}
-(id)initShadowImageViewWithShadowOriginY_v2:(float)OriginY andHeight:(float)height
{
    UIImageView *shadowView=[[UIImageView alloc]initWithFrame:CGRectMake(0, OriginY, DeviceWidth, height)];
    [shadowView setImage:SHADOWIMAGE_v2];
    return shadowView;
}
-(id)initShadowColorViewWithShadowOriginY:(float)OriginY andHeight:(float)height
{
    UIImageView *shadowView=[[UIImageView alloc]initWithFrame:CGRectMake(0, OriginY, DeviceWidth, height)];
    [shadowView setImage:SHADOW_IMAGE];
    return shadowView;
}
-(id)initArrowRightImageWithFrame:(CGRect)frame{
    UIImageView *rightImage = [[UIImageView alloc]initWithFrame:frame];
    rightImage.image = [UIImage imageNamed:@"ArrowRight"];
    return rightImage;
}
//Transparent Gradient Layer
- (void) insertTransparentGradient {
    UIColor *colorOne1 = [UIColor colorWithRed:0/255.0  green:0/255.0  blue:0/255.0  alpha:0.6];
    UIColor *colorTwo2 = [UIColor colorWithRed:0/255.0  green:0/255.0  blue:0/255.0  alpha:0.0];
    
    NSArray *colors1 = [NSArray arrayWithObjects:(id)colorOne1.CGColor, colorTwo2.CGColor, nil];
    
    NSNumber *stopOne1 = [NSNumber numberWithFloat:0.2];
    NSNumber *stopTwo2 = [NSNumber numberWithFloat:1.0];
    
    NSArray *locations1 = [NSArray arrayWithObjects:stopOne1, stopTwo2, nil];
    //crate gradient layer
    CAGradientLayer *headerLayer1 = [CAGradientLayer layer];
    headerLayer1.colors = colors1;
    headerLayer1.locations = locations1;
    headerLayer1.frame = self.bounds;
    headerLayer1.startPoint = CGPointMake(0.0, 0.0);
    headerLayer1.endPoint = CGPointMake(0.0, 0.1);
    
    [self.layer insertSublayer:headerLayer1 atIndex:0];
    
    
    UIColor *colorOne11 = [UIColor colorWithRed:0/255.0  green:0/255.0  blue:0/255.0  alpha:0.0];
    UIColor *colorTwo22 = [UIColor colorWithRed:0/255.0  green:0/255.0  blue:0/255.0  alpha:0.7];
    
    NSArray *colors11 = [NSArray arrayWithObjects:(id)colorOne11.CGColor, colorTwo22.CGColor, nil];
    
    NSNumber *stopOne11 = [NSNumber numberWithFloat:0.2];
    NSNumber *stopTwo22 = [NSNumber numberWithFloat:1.0];
    
    NSArray *locations11 = [NSArray arrayWithObjects:stopOne11, stopTwo22, nil];
    //crate gradient layer
    CAGradientLayer *headerLayer11 = [CAGradientLayer layer];
    headerLayer11.colors = colors11;
    headerLayer11.locations = locations11;
    headerLayer11.frame = self.bounds;
    headerLayer11.startPoint = CGPointMake(0.0, 0.5);
    headerLayer11.endPoint = CGPointMake(0.0, 1.0);
    
    [self.layer insertSublayer:headerLayer11 atIndex:1];
    
}
//color gradient layer
- (void) insertColorGradient {
    
    UIColor *colorOne = [UIColor colorWithRed:(1) green:(1) blue:(1) alpha:1.0];
    UIColor *colorTwo = [UIColor colorWithRed:(0)  green:(0)  blue:(0)  alpha:1.0];
    
    NSArray *colors = [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, nil];
    
    NSNumber *stopOne = [NSNumber numberWithFloat:0.0];
    NSNumber *stopTwo = [NSNumber numberWithFloat:1.0];
    
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    CAGradientLayer *headerLayer = [CAGradientLayer layer];
    headerLayer.colors = colors;
    headerLayer.locations = locations;
    headerLayer.frame = self.bounds;
    
    [self.layer insertSublayer:headerLayer above:0];
    
}
- (void)getIamgeHeight:(NSURL *)url placeholderImage:(UIImage *)placeholder options:(SDWebImageOptions)options progress:(SDWebImageDownloaderProgressBlock)progressBlock completed:(SDWebImageCompletionBlock)completedBlock
{
    [self sd_cancelCurrentImageLoad];
    
    CGFloat cropWidth=self.frame.size.width;
    CGFloat cropHeight=self.frame.size.height;
    
    //    UIActivityIndicatorView *_activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    //    _activityView.frame = CGRectMake( 0, 0, 20.0f, 20.0f);
    //    _activityView.center = self.center;
    //    _activityView.color = [UIColor blackColor];
    //    [self addSubview:_activityView];
    //    [_activityView startAnimating];
    
    self.image = [placeholder imageByScalingAndCroppingForSize:CGSizeMake(cropWidth, cropHeight)];
    
    if (url)
    {
        __weak UIImageView *wself = self;
//        id<SDWebImageOperation> operation = [SDWebImageManager.sharedManager downloadWithURL:url options:options progress:progressBlock completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished)
//                                             {
//                                                 //            [_activityView stopAnimating];
//                                                 
//                                                 if (!wself) return;
//                                                 dispatch_main_sync_safe(^
//                                                                         {
//                                                                             if (!wself) return;
//                                                                             UIImage *cropImage=[UIImage new];
//                                                                             if (image)
//                                                                             {
//                                                                                 cropImage=[image imageByScalingAndCroppingForSize:CGSizeMake(cropWidth, image.size.height/2)];
//                                                                                 wself.image = cropImage;
//                                                                                 //                    [wself setNeedsLayout];
//                                                                             }
//                                                                             if (completedBlock && finished)
//                                                                             {
//                                                                                 completedBlock(cropImage, error, cacheType);
//                                                                             }
//                                                                         });
//                                             }];
//        objc_setAssociatedObject(self, &operationKey, operation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        id<SDWebImageOperation> operation =  [SDWebImageManager.sharedManager downloadImageWithURL:url options:options progress:progressBlock completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            if (!wself) return ;
            dispatch_main_sync_safe(^{
            
                if (!wself) return ;
                UIImage *cropImage=[UIImage new];
                if (image) {
                    cropImage = [image imageByScalingAndCroppingForSize:CGSizeMake(cropWidth, image.size.height/2)];
                    wself.image = cropImage;
                }
                if (completedBlock && finished) {
                    completedBlock(cropImage, error, cacheType,imageURL);
                }
            
            });
            
        }];
        objc_setAssociatedObject(self, &operationKey, operation, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }else{
        //        [_activityView stopAnimating];
    }
}
@end
