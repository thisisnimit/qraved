//
//  UITextField+Helper.h
//  Qraved
//
//  Created by Shine Wang on 11/27/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Helper)

- (void)limitTextLength:(int)length;
@end
