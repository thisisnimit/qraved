//
//  NSNumber+Helper.h
//  Qraved
//
//  Created by Libo Liu on 14-3-5.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Helper)
-(float)getRatingNumber;
@end
