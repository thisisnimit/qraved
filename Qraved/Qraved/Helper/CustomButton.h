//
//  CustomButton.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/17.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomButton : UIButton

@property (nonatomic, assign)  BOOL isSelected;

@end
