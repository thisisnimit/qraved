//
//  UIViewController+Helper.h
//  Qraved
//
//  Created by Libo Liu on 10/8/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Helper)
-(void)setBackBarButton;
-(void)setBackBarButtonOffset30;
-(void)setCloseBarButton;
-(void)setIos7Layout;
-(void)goToBackViewController;
-(void)removeNotifications;
-(void)showLoadingView;
-(void)removeLoadingView;
-(int)returnIOSVersion;
@end
