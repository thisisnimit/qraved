//
//  PushHelper.m
//  Qraved
//
//  Created by Admin on 9/23/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "PushHelper.h"
#import "PostDataHandler.h"
#import "IMGUser.h"
#import "KeychainItemWrapper.h"

@implementation PushHelper

+(void)postUserDeviceToken: (NSString *)deviceToken {
    
    [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:kRemoteNotificationDeviceToken];
    
    KeychainItemWrapper *keychain=[[KeychainItemWrapper alloc] initWithIdentifier:@"deviceToken" accessGroup:nil];
    NSString *previousDeviceToken = [keychain objectForKey:(id)kSecValueData];
    
    [keychain setObject:deviceToken forKey:(id)kSecValueData];
    
    
    
    IMGUser *user=[IMGUser currentUser];
    
    
    NSMutableDictionary *parameters=  [NSMutableDictionary dictionaryWithCapacity:0];
    [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:kRemoteNotificationDeviceToken] forKey:@"deviceToken"];
    if (user.userId!=nil) {
        [parameters setObject:user.userId forKey:@"userID"];
    }
    if (previousDeviceToken!=nil) {
        [parameters setObject:previousDeviceToken forKey:@"previousDeviceToken"];
    }
    
    [[IMGNetWork sharedManager] POST:@"push/set" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSLog(@"post device token to server get status: %@",[responseObject objectForKey:@"status"]);
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"post device token to server get error: %@",error.localizedDescription);
    }];
}

+(void)postUserDeviceToken {
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:kRemoteNotificationDeviceToken];
    if(deviceToken){
        [PushHelper postUserDeviceToken:deviceToken];
    }
}

@end
