//
//  UIActivityItemProvider+CopyUrl.m
//  Qraved
//
//  Created by harry on 2017/11/27.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "UIActivityItemProvider+CopyUrl.h"

@implementation UIActivityItemProvider (CopyUrl)

- (NSString *)returnCopyUrl:(NSURL *)url{
    return [NSString stringWithFormat:@"Check out: %@",url];
}
@end
