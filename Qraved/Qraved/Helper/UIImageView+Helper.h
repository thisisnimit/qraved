//
//  UIImageView+Helper.h
//  Qraved
//
//  Created by Laura on 14-9-5.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+Resize.h"
#import "UIImageView+WebCache.h"

@interface UIImageView (Helper)
-(id)initShadowImageViewWithShadowOriginY:(float)OriginY andHeight:(float)height;
-(id)initShadowImageViewWithShadowOriginY_v2:(float)OriginY andHeight:(float)height;
-(id)initArrowRightImageWithFrame:(CGRect)frame;
- (void) insertTransparentGradient;
- (void) insertColorGradient;
- (void)getIamgeHeight:(NSURL *)url placeholderImage:(UIImage *)placeholder options:(SDWebImageOptions)options progress:(SDWebImageDownloaderProgressBlock)progressBlock completed:(SDWebImageCompletionBlock)completedBlock;

@end
