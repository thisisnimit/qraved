//
//  CommonMethod.h
//  Qraved
//
//  Created by Adam.zhang on 2017/9/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^CommonCallBackBlock)();
typedef void (^SaveCallBackBlock)(BOOL isSaved);
typedef void (^SavedToListBlock)(BOOL isSaved, NSString *listName);
@interface CommonMethod : NSObject

+ (void)unfoldAnimationWith:(UIView *)view;
+ (void)foldAnimationWith:(UIView *)view;
+ (void)doSaveWithRestaurant:(IMGRestaurant *)restaurant andViewController:(id)viewController andCallBackBlock:(SaveCallBackBlock)block;
+ (void)saveRestaurantToList:(IMGRestaurant *)restaurant andViewController:(id)viewController andCallBackBlock:(SaveCallBackBlock)block;

+ (void)saveRestaurantToList:(IMGRestaurant *)restaurant andViewController:(id)viewController andSavedToListBlock:(SavedToListBlock)block;
+ (void)goFoodButtonTapped:(IMGRestaurant *)restaurant andViewController:(id)viewController andCallBackBlock:(SaveCallBackBlock)block;
+ (void)goFoodButtonTapped:(IMGRestaurant *)restaurant andViewController:(id)viewController andRestaurantState:(BOOL)isOpen andCallBackBlock:(SaveCallBackBlock)block;

+ (void)checkCameraStateWithViewController:(id)viewController andCallBackBlock:(CommonCallBackBlock)block;

+ (void)setJsonToFile:(NSString *)fileName andJson:(id)jsonDic;
+ (id)getJsonFromFile:(NSString *)fileName;

+ (NSArray *)getHomeBanner:(id)json;
+ (NSArray *)getHomeBannerImage:(id)json;
+ (NSArray *)getHomeInstagram:(id)json;
+ (NSArray *)getHomeComponents:(id)json;
+ (NSArray *)getHomeLocation:(id)json;
+ (NSArray *)getHomeInstagramFilter:(id)json;
+ (NSArray *)getSavedRestaurant:(id)json;
+ (NSArray *)getDeliveryJournalList:(id)json;
+ (NSArray *)getDeliveryBanner:(id)json;
+ (NSArray *)getDeliveryBannerImage:(id)json;
+ (NSArray *)getGuideList:(id)json;

@end
