//
//  UIApplication+UIApplicationHelper.h
//  Qraved
//
//  Created by apple on 16/9/5.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIApplication (UIApplicationHelper)
-(BOOL)ZH_openURLMethod:(NSURL *)url;
@end
