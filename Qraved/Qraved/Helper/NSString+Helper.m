//
//  NSString+Helper.m
//  Qraved
//
//  Created by Shine Wang on 9/30/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "NSString+Helper.h"
#import "Tools.h"
#import "UIConstants.h"
#import <CommonCrypto/CommonDigest.h>
#import "NSURL+Base.h"

//     care222.liu@gmail&ampcom
@implementation NSString (Helper)
-(NSString *)filterHtml
{
    NSString *returnString= [[[[[[self stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"]  stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"] stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"] stringByReplacingOccurrencesOfString:@"<span>" withString:@""] stringByReplacingOccurrencesOfString:@"</span>" withString:@""]
        stringByReplacingOccurrencesOfString:@"&Amp;" withString:@"&"];
    
//    DLog(@"return String is %@",returnString);
    return returnString ;
}

-(NSString *)inFilterHtmlWithHref:(NSString *)herf
{
    return [NSString stringWithFormat:@"<a href='%@' title='%@'>%@</a>",herf,self,self];
}

-(NSString *)promotionsHTML
{
    //    NSLog(@"%@",modifiedHTML);
    //    return modifiedHTML;
    
    NSString *returnString= [[[self stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"]  stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"] stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    
    NSString *prexHtml=@"<html><style type='text/css'>img { max-width: 100%; width: auto; height: auto; }</style><head><meta name=\"viewport\" content=\"width=300, initial-scale=1.0,maximum-scale=3.0, minimum-scale=0.5, user-scalable=yes,target-densitydpi=device-dpi\" /></head><body>";
    NSString *suffixHtml=@"</body></html>";
    returnString=[prexHtml stringByAppendingString:returnString];
    returnString=[returnString stringByAppendingString:suffixHtml];
    
//    DLog(@"after modfied url is %@",returnString);
    return  returnString;
}

-(NSString *)changeStyle
{
    NSString *originHTML=self;
    UIFont *font=[UIFont fontWithName:DEFAULT_FONT_NAME size:PAGE_TITLE_FONT_SIZE];
    return [NSString stringWithFormat:@"<span style=\"font-family: %@;color:333333; font-size: %f\">%@</span>",
            font.fontName,
            DETAIL_BODY_FONT_SIZE,
            originHTML];
}

-(NSString *)replaceUrlString
{
    NSString *originalHTML = [self filterHtml];
    NSString *regexPattern = @"<img[^>]*width:['\"\\s]*([0-9]+)[^>]*height:['\"\\s]*([0-9]+)[^>]*>";
    
    NSRegularExpression *regex =
    [NSRegularExpression regularExpressionWithPattern:regexPattern
                                              options:NSRegularExpressionDotMatchesLineSeparators
                                                error:nil];
    NSMutableString *modifiedHTML = [NSMutableString stringWithString:originalHTML];
    
    NSArray *matchesArray=[regex matchesInString:modifiedHTML options:NSMatchingReportProgress range:NSMakeRange(0, [modifiedHTML length])];
    
    NSTextCheckingResult *match;
    
    NSInteger offset = 0, newoffset = 0;
    
    int maxWidth = 300;
    
    for (match in matchesArray) {
        
        NSRange widthRange = [match rangeAtIndex:1];
        NSRange heightRange = [match rangeAtIndex:2];
        
        widthRange.location += offset;
        heightRange.location += offset;
        
        NSString *widthStr = [modifiedHTML substringWithRange:widthRange];
        NSString *heightStr = [modifiedHTML substringWithRange:heightRange];
        
        int width = [widthStr intValue];
        int height = [heightStr intValue];
        
        if (width > maxWidth) {
            height = (height * maxWidth) / width;
            width = maxWidth;
            
            NSString *newWidthStr = [NSString stringWithFormat:@"%d", width];
            NSString *newHeightStr = [NSString stringWithFormat:@"%d", height];
            
            [modifiedHTML replaceCharactersInRange:widthRange withString:newWidthStr];
            
            newoffset = ([newWidthStr length] - [widthStr length]);
            heightRange.location += newoffset;
            
            [modifiedHTML replaceCharactersInRange:heightRange withString:newHeightStr];
            
            newoffset += ([newHeightStr length] - [heightStr length]);
            offset += newoffset;
        }
    }
    
    return modifiedHTML;

}

-(NSString *)filterNullString
{
    return [[self stringByReplacingOccurrencesOfString:@"<null>" withString:@""]  stringByReplacingOccurrencesOfString:@"null" withString:@""];
}

-(NSString *)returnCurrentImageString{
    if ([self hasPrefix:@"http:"] || [self hasPrefix:@"https:"]) {
        return self;
    }else{
        return [self returnFullImageUrl];
    }
}

-(BOOL)isValidateUsername{
    
    NSString *trimmedString = [self stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    if (self.length<3 || trimmedString.length==0) {
        return NO;
    }
    return YES;
}
-(BOOL)isValidateEmail{

    if (self.length<1) {
        return NO;
    }
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

-(BOOL)isValidatePhone{
    
    if (self.length<8||self.length>17) {
        return NO;
    }
    return YES;

}
-(BOOL)isClaimYourRestaurantValidatePhone{
    NSString* number=@"^(\\+?)(\\d+)$";
    NSPredicate *numberPre = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",number];
    if ( [numberPre evaluateWithObject:self] && (self.length >= 8) && (self.length <= 17) ) {
        return YES;
    }
    return NO;
}

-(BOOL)isValidateInternationPhone
{
    if (self.length<1) {
        return NO;
    }
    NSString *numberRegex = @"^(\\+?)(\\d{10,15})$";
    NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", numberRegex];
    return [numberTest evaluateWithObject:[self stringByReplacingOccurrencesOfString:@"-" withString:@""]];
}


-(BOOL)isBlankString{
    if (self == nil || self == NULL) {
        return YES;
    }
    if ([self isKindOfClass:[NSNull class]]) {
        return YES;
    }
    if ([[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0) {
        return YES;
    }
    return NO;
}

- (NSDate *)dateFromString{
    NSString *dateString=self;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    NSDate *destDate= [dateFormatter dateFromString:dateString];
    
    return destDate;
    
}

-(NSString *)returnShortPhoneNum
{
    if([self hasPrefix:@"+"])
    {
        return [self substringFromIndex:3];
    }
    
    return self;
}
-(NSString *)returnFullImageUrl
{
    NSString* encodedUrl = [self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [QRAVED_WEB_IMAGE_SERVER stringByAppendingString:encodedUrl];

}
-(NSString *)returnnewFullImageUrl
{
    NSString* encodedUrl = [self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return [QRAVED_WEB_NEW_IMAGE_SERVER stringByAppendingString:encodedUrl];
    
}

-(NSString*)ImgURLStringWide:(int)wide High:(int)high {
    NSString * encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(    NULL,    (CFStringRef)self,    NULL,    (CFStringRef)@"!*'();:@&=+$,/?%#[]",    kCFStringEncodingUTF8 ));
    encodedString= [NSString stringWithFormat:@"%@%@&width=%d&height=%d", QRAVED_WEB_IMAGE_SERVER, encodedString, wide, high];
    return encodedString;
}

-(NSString*)ImgURLStringWide:(ImageSizeType)imageSizeType{
    
    int width = 0;
    int height = 0;
    switch (imageSizeType) {
        case ImageSizeTypeLowResolution:
            width = 200;
            height = width;
            break;
        case ImageSizeTypeThumbnail:
            width = 500;
            height = width;
        default:
            width = DeviceWidth *2;
            height = width;
            break;
    }
    
    NSString * encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(    NULL,    (CFStringRef)self,    NULL,    (CFStringRef)@"!*'();:@&=+$,/?%#[]",    kCFStringEncodingUTF8 ));
    encodedString= [NSString stringWithFormat:@"%@%@&width=%d&height=%d", QRAVED_WEB_IMAGE_SERVER, encodedString, width, height];
    return encodedString;
}
-(NSString *)returnFullImageUrlWithImageSizeType:(ImageSizeType)imageSizeType{
    CGFloat width = 0.0;
    CGFloat height = 0.0;
    switch (imageSizeType) {
        case ImageSizeTypeLowResolution:
            width = 200;
            height = width;
            break;
        case ImageSizeTypeThumbnail:
            width = 500;
            height = width;
        default:
            width = DeviceWidth *2;
            height = width;
            break;
    }
    
    NSRange range=[self rangeOfString:@"%20"];
    if (range.location != NSNotFound)
    {
        NSLog(@"%@", [[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:self] stringByAppendingFormat:@"&width=%f&height=%f",width,height]);
        return [[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:self] stringByAppendingFormat:@"&width=%f&height=%f",width,height];
    }
    
    NSString* encodedUrl = [self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if(encodedUrl!=NULL)
    {
        NSLog(@"%@", [[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:encodedUrl] stringByAppendingFormat:@"&width=%f&height=%f",width,height]);
        return [[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:encodedUrl] stringByAppendingFormat:@"&width=%f&height=%f",width,height];
    }
    return @"";
}

-(NSString*)ImgURLString{
    NSString * encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(    NULL,    (CFStringRef)self,    NULL,    (CFStringRef)@"!*'();:@&=+$,/?%#[]",    kCFStringEncodingUTF8 ));
    encodedString= [NSString stringWithFormat:@"%@%@",QRAVED_WEB_IMAGE_SERVER, encodedString];
    return encodedString;
}
-(NSString *)returnFullImageUrlWithFixedWidth:(CGFloat )width
{
    CGFloat realWidth=width;
    if (DeviceWidth <= 320)
    {
        realWidth=640;
    }
    else if (DeviceWidth <= 375)
    {
        realWidth=750;
    }
    else if (DeviceWidth <= 414)
    {
        realWidth = 828;
    }
    
    NSRange range=[self rangeOfString:@"%20"];
    if (range.location != NSNotFound)
    {
        return [[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:self] stringByAppendingFormat:@"&width=%f",realWidth];
    }
    
    NSString* encodedUrl = [self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if(encodedUrl!=NULL)
    {
        return [[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:encodedUrl] stringByAppendingFormat:@"&width=%f",realWidth];
    }
    return @"";

}
-(NSString *)returnFullImageUrlWithWidth:(CGFloat )width
{
    CGFloat realWidth=width;
    if([Tools isRetinaScreen])
    {
        realWidth=width*2;
    }
    
    NSRange range=[self rangeOfString:@"%20"];
    if (range.location != NSNotFound)
    {
        return [[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:self] stringByAppendingFormat:@"&width=%f",realWidth];
    }

    NSString* encodedUrl = [self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if(encodedUrl!=NULL)
    {
        return [[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:encodedUrl] stringByAppendingFormat:@"&width=%d",(int)realWidth];
    }
    return @"";
}
-(NSString *)returnFullWebPImageUrlWithWidth:(CGFloat )width
{
#ifdef QRAVED_WEB_WEBP_IMAGE_SERVER
    CGFloat realWidth=width;
    if([Tools isRetinaScreen])
    {
        realWidth=width*2;
    }
    
    NSRange range=[self rangeOfString:@"%20"];
    if (range.location != NSNotFound)
    {
        return [[QRAVED_WEB_WEBP_IMAGE_SERVER stringByAppendingString:self] stringByAppendingFormat:@"&width=%f",realWidth];
    }
    
    NSString* encodedUrl = [self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if(encodedUrl!=NULL)
    {
        return [[QRAVED_WEB_WEBP_IMAGE_SERVER stringByAppendingString:encodedUrl] stringByAppendingFormat:@"&width=%d",(int)realWidth];
    }
    return @"";
#else 
    return [self returnFullImageUrlWithWidth:width];
#endif
}
-(NSString *)returnFullImageUrlWithWidth:(CGFloat )width andHeight:(CGFloat)height
{
    CGFloat realWidth=width;
    CGFloat realHeight=height;
    if([Tools isRetinaScreen])
    {
        realWidth=width*2;
        realHeight=height*2;
    }
    
    NSRange range=[self rangeOfString:@"%20"];
    if (range.location != NSNotFound)
    {
        NSLog(@"%@", [[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:self] stringByAppendingFormat:@"&width=%f&height=%f",realWidth,realHeight]);
        return [[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:self] stringByAppendingFormat:@"&width=%f&height=%f",realWidth,realHeight];
    }
    
    NSString* encodedUrl = [self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    if(encodedUrl!=NULL)
    {
        NSLog(@"%@", [[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:encodedUrl] stringByAppendingFormat:@"&width=%f&height=%f",realWidth,realHeight]);
        return [[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:encodedUrl] stringByAppendingFormat:@"&width=%f&height=%f",realWidth,realHeight];
    }
    return @"";
}
-(NSString *)MD5String
{
    /*
    const char *cStr = [self UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, strlen(cStr), result); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
     
     */
    
    const char *cStr = [self UTF8String];
    unsigned char digest[16];
    CC_MD5( cStr, (unsigned int)strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
}
-(NSString*)returnFullCDNImageUrlWithWidth:(CGFloat)width andHeight:(CGFloat)height{
    if (self.length>4) {
        
    }else{
        return nil;
    }
    NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *pathStr = [pathArray objectAtIndex:0];
    //获取文件的完整路径
    NSString *filePatch = [pathStr stringByAppendingPathComponent:@"DownloadData.plist"];
    //读取plist文件的内容
    NSMutableDictionary *dataAllDic = [[NSMutableDictionary alloc] initWithContentsOfFile:filePatch];
    NSLog (@"%@",dataAllDic);
    NSDictionary *dataDic=[dataAllDic objectForKey:@"data"];
    NSString *cdnUrl=[dataDic objectForKey:@"cdnUrl"];
    
    CGFloat realWidth=width;
    CGFloat realHeight=height;

    if([Tools isRetinaScreen])
    {
        realWidth=width*2;
        realHeight=height*2;
    }
    NSString *lastStr=[self substringFromIndex:self.length-4];
    NSLog(@"lastStr====%@",lastStr);
    NSString *fullStr=[self substringToIndex:self.length-4];
    NSArray *arr=[self componentsSeparatedByString:@"-"];
    if (arr.count>0) {
        NSString *WH=[arr lastObject];
        if ([WH rangeOfString:@"x"].location!=NSNotFound) {
            fullStr=[fullStr stringByAppendingString:[NSString stringWithFormat:@"-%fx%f%@",realWidth,realHeight,lastStr]];
            NSLog(@"firstStr=====%@",[cdnUrl stringByAppendingString:fullStr]);
            return [cdnUrl stringByAppendingString:fullStr];
        }
    }
    return nil;

}
-(NSString*)returnFullCDNImageUrlWithWidth:(CGFloat)width{
    if (self.length>4) {
        
    }else{
        return nil;
    }
    NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *pathStr = [pathArray objectAtIndex:0];
    //获取文件的完整路径
    NSString *filePatch = [pathStr stringByAppendingPathComponent:@"DownloadData.plist"];
    //读取plist文件的内容
    NSMutableDictionary *dataAllDic = [[NSMutableDictionary alloc] initWithContentsOfFile:filePatch];
    NSLog (@"%@",dataAllDic);
    NSDictionary *dataDic=[dataAllDic objectForKey:@"data"];
    NSString *cdnUrl=[dataDic objectForKey:@"cdnUrl"];
    
    NSString *lastStr=[self substringFromIndex:self.length-4];
    NSLog(@"lastStr====%@",lastStr);
    NSString *fullStr=[self substringToIndex:self.length-4];
    NSArray *arr=[self componentsSeparatedByString:@"-"];
    if (arr.count>0) {
        NSString *WH=[arr lastObject];
        if ([WH rangeOfString:@"x"].location!=NSNotFound) {
            NSString *WHlastStr=[WH substringToIndex:WH.length-4];
            NSArray *whs=[WHlastStr componentsSeparatedByString:@"x"];
            int w=[[whs firstObject] intValue];
            int h=[[whs lastObject] intValue];
            int  needHeight=h*((int)(width*2))/w;
            fullStr=[fullStr stringByAppendingString:[NSString stringWithFormat:@"-%dx%d%@",(int)width*2,needHeight,lastStr]];
            NSLog(@"firstStr=====%@",[cdnUrl stringByAppendingString:fullStr]);
            return [cdnUrl stringByAppendingString:fullStr];
        }
    }
    return nil;
}
- (NSString *)toFormatNumberString{
    @try
    {
        if (self.length < 3)
        {
            return self;
        }
        NSString *numStr = self;
        NSArray *array = [numStr componentsSeparatedByString:@"."];
        NSString *numInt = [array objectAtIndex:0];
        if (numInt.length <= 3)
        {
            return self;
        }
        NSString *suffixStr = @"";
        if ([array count] > 1)
        {
            suffixStr = [NSString stringWithFormat:@".%@",[array objectAtIndex:1]];
        }
        
        NSMutableArray *numArr = [[NSMutableArray alloc] init];
        while (numInt.length > 3)
        {
            NSString *temp = [numInt substringFromIndex:numInt.length - 3];
            numInt = [numInt substringToIndex:numInt.length - 3];
            [numArr addObject:[NSString stringWithFormat:@",%@",temp]];//得到的倒序的数据
        }
        NSUInteger count = [numArr count];
        for (int i = 0; i < count; i++)
        {
            numInt = [numInt stringByAppendingFormat:@"%@",[numArr objectAtIndex:(count -1 -i)]];
        }
    
        numStr = [NSString stringWithFormat:@"%@%@",numInt,suffixStr];
        return numStr;
    }
    @catch (NSException *exception)
    {
        return self;
    }
    @finally
    {}
    
}
- (NSString *)removeHTML{
    NSString *html = [NSString stringWithString:self];
    NSScanner *theScanner;
    
    NSString *text = nil;
    /*
      str = str.replace("&lt;", "<");
      str = str.replace("&gt;", ">");
      str = str.replace("<ol>", "<p>");
      str = str.replace("</ol>", "</p>");
      str = str.replace("<li>", " ");
      str = str.replace("</li>", "<br>");
      return str;
     */
    
    html = [html stringByReplacingOccurrencesOfString:@"&amp;amp;" withString:@"&"];
    html = [html stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    html = [html stringByReplacingOccurrencesOfString:@"&nbsp;" withString:@" "];
    html = [html stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
    html = [html stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    
    theScanner = [NSScanner scannerWithString:html];
    
    while ([theScanner isAtEnd] == NO) {
        
        // find start of tag
        
        [theScanner scanUpToString:@"<" intoString:NULL] ;
        
        
        
        // find end of tag
        
        [theScanner scanUpToString:@">" intoString:&text] ;
        
        
        
        // replace the found tag with a space
        
        //(you can filter multi-spaces out later if you wish)
        
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
        
        
        
    }
    
    return html;
    
}
- (NSString *)removeHTML2{
    NSString *html = [NSString stringWithString:self];
    NSArray *components = [html componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    
    
    
    NSMutableArray *componentsToKeep = [NSMutableArray array];
    
    for (int i = 0; i < [components count]; i = i + 2) {
        
        [componentsToKeep addObject:[components objectAtIndex:i]];
        
    }
    
    
    
    NSString *plainText = [componentsToKeep componentsJoinedByString:@""];
    
    return plainText;
    
}

-(BOOL)isLogout{
    if ([self isEqualToString:@"Please sign in first."] || [self isEqualToString:@"userID and token not match"]) {
        return YES;
    }
    return NO;
}

-(NSString *)groupConcat:(NSArray *)stringArray
{
    if(stringArray!=nil){
        NSString *tmpStr = @"";
        for(NSString *str in stringArray){
            if([tmpStr length]>0){
                tmpStr = [[NSString alloc] initWithFormat:@"%@, %@",tmpStr,str];
            }else{
                tmpStr = [[NSString alloc] initWithFormat:@"%@%@",tmpStr,str];
            }
        }
        return tmpStr;
    }
    return @"";
}

-(NSString *)groupConcat:(NSArray *)stringArray length:(int)length
{
    if(stringArray!=nil){
        NSString *tmpStr = @"";
        for(int i=0;i<length && i<stringArray.count;i++){
            NSString *str = stringArray[i];
            if([tmpStr length]>0){
                tmpStr = [[NSString alloc] initWithFormat:@"%@, %@",tmpStr,str];
            }else{
                tmpStr = [[NSString alloc] initWithFormat:@"%@%@",tmpStr,str];
            }
        }
        return tmpStr;
    }
    return @"";
}

-(NSString *)html
{
    //    NSLog(@"%@",modifiedHTML);
    //    return modifiedHTML;
    
    NSString *returnString= [[[self stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"]  stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"] stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
    
    NSString *prexHtml=@"<html><style type='text/css'>img { max-width: 100%; width: auto; height: auto; }</style><head><meta name=\"viewport\" content=\"width=300, initial-scale=1.0,maximum-scale=3.0, minimum-scale=0.5, user-scalable=yes,target-densitydpi=device-dpi\" /></head><body>";
    NSString *suffixHtml=@"</body></html>";
    returnString=[prexHtml stringByAppendingString:returnString];
    returnString=[returnString stringByAppendingString:suffixHtml];
    
    //    DLog(@"after modfied url is %@",returnString);
    return  returnString;
}
-(NSString *)enterKey
{
    return [self stringByReplacingOccurrencesOfString:@"\n" withString:@""];
}

-(NSString *)getHtmlTitle{
    NSRange range1 = [self rangeOfString:@"<meta property=\"og:title\" content=\""];
    if (range1.location != NSNotFound) {
        NSString *string = [self substringFromIndex:range1.location+range1.length];
        NSRange range2= [string rangeOfString:@"\" />"];
        if (range2.location != NSNotFound) {
            NSString *title = [string substringToIndex:range2.location];
            return title;
        }
        //        NSString *title = [self substringWithRange:NSMakeRange(range1.location+range1.length, (range2.location-1)-(range1.location+range1.length))];
    }
    return nil;
}
-(NSString *)getHtmlContent{
    NSRange range1 = [self rangeOfString:@"<meta property=\"og:title\" content=\""];
    if (range1.location != NSNotFound) {
        NSString *string = [self substringFromIndex:range1.location+range1.length];
        NSRange range2= [string rangeOfString:@"\"/>"];
        if (range2.location != NSNotFound) {
            NSString *title = [string substringToIndex:range2.location];
            return title;
        }
        //        NSString *title = [self substringWithRange:NSMakeRange(range1.location+range1.length, (range2.location-1)-(range1.location+range1.length))];
    }
    return nil;
}

-(NSString *)getHtmlImageUrl{
    NSRange range1 = [self rangeOfString:@"<meta property=\"og:image\" content=\""];
    if (range1.location != NSNotFound) {
        NSString *string = [self substringFromIndex:range1.location+range1.length];
        NSRange range2= [string rangeOfString:@"\""];
        if (range2.location != NSNotFound) {
            NSString *imageUrl = [string substringToIndex:range2.location];
            return imageUrl;
        }
        //        NSString *title = [self substringWithRange:NSMakeRange(range1.location+range1.length, (range2.location-1)-(range1.location+range1.length))];
    }
    return nil;
}

- (NSString *)getHtmlId
{
    NSRange range1 = [self rangeOfString:@"single single-post postid-"];
    if (range1.location != NSNotFound) {
        NSString *string = [self substringFromIndex:range1.location+range1.length];
        NSRange range2= [string rangeOfString:@" single-format-standard"];
        if (range2.location != NSNotFound) {

            NSString *idText = [string substringToIndex:range2.location];
            return idText;
        }
    }
    return nil;
}

-(NSString *)getHtmlJournalImageUrl{
    NSRange range1 = [self rangeOfString:@"src="];
    if (range1.location != NSNotFound) {
        NSString *string = [self substringFromIndex:range1.location+range1.length+1];
        NSRange range2 = [string rangeOfString:@".jpg"];
        NSRange range3 = [string rangeOfString:@".jpeg"];
        NSRange range4 = [string rangeOfString:@".png"];
        if (range2.location != NSNotFound) {
            NSString *title = [string substringToIndex:range2.location+4];
            return title;
        }else if (range3.location != NSNotFound)
        {
            NSString *title = [string substringToIndex:range3.location+5];
            return title;
        }else if (range4.location != NSNotFound)
        {
            NSString *title = [string substringToIndex:range4.location+4];
            return title;
        }
        
        //        NSString *title = [self substringWithRange:NSMakeRange(range1.location+range1.length, (range2.location-1)-(range1.location+range1.length))];
    }
    return nil;
}
- (NSString*)dictionaryToJson:(NSDictionary *)dic

{
    
    NSError *parseError = nil;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
    
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
}
- (CGSize)sizeWithFont:(UIFont *)font{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic[NSFontAttributeName] = font;
    return [self sizeWithAttributes:dic];
}

- (CGSize)sizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic[NSFontAttributeName] = font;
    return [self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
}
- (CGSize)sizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size lineBreakMode:(NSLineBreakMode)lineBreakMode
{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic[NSFontAttributeName] = font;
    return [self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
}

- (void)drawAtPoint:(CGPoint)point withFont:(UIFont *)font{
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    dic[NSFontAttributeName] = font;
    [self drawAtPoint:point withAttributes:dic];
}
-(BOOL) isEmpty {
    
    if (!self) {
        return true;
    } else {
        //A character set containing only the whitespace characters space (U+0020) and tab (U+0009) and the newline and nextline characters (U+000A–U+000D, U+0085).
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        
        //Returns a new string made by removing from both ends of the receiver characters contained in a given character set.
        NSString *trimedString = [self stringByTrimmingCharactersInSet:set];
        
        if ([trimedString length] == 0) {
            return true;
        } else {
            return false;
        }
    }
}
-(NSString*)getAMOrPMString:(NSString*)string{
    NSString *hourString = [string substringWithRange:NSMakeRange(0, 2)];
    NSString *aMOrPMString;
    if ([hourString intValue]>12) {
        hourString = [NSString stringWithFormat:@"%d",[hourString intValue]-12];
        aMOrPMString = @"PM";
    }else{
        aMOrPMString = @"AM";
    }
    NSString * minStr = [string substringWithRange:NSMakeRange(3, 2)];
    string = [NSString stringWithFormat:@"%@:%@%@",hourString,minStr,aMOrPMString];
    return string;
}

+(NSString *)returnDistanceWithLatitude:(NSString *)latitude andLongitude:(NSString *)longitude{
    //    if([self.latitude intValue]==0 || [self.longitude intValue]==0){
    //        return @"";
    //    }
    
    //    double lon2=[AppDelegate ShareApp].locationManager.location.coordinate.longitude;
    //    double lat2=[AppDelegate ShareApp].locationManager.location.coordinate.latitude;
    double lon2=[[[NSUserDefaults standardUserDefaults]objectForKey:@"longitude"] doubleValue];
    double lat2=[[[NSUserDefaults standardUserDefaults]objectForKey:@"latitude"] doubleValue];
    NSLog(@"%f,%f",lon2,lat2);
    double dd = M_PI/180;
    double x1=[latitude doubleValue]*dd,x2=lat2 *dd;
    double y1=[longitude doubleValue]*dd,y2=lon2 *dd;
    double R = 6378138;
    double distance = (2*R*asin(sqrt(2-2*cos(x1)*cos(x2)*cos(y1-y2) - 2*sin(x1)*sin(x2))/2));
    //    NSLog(@"distance is less than 0,%f",distance);
    
    if(distance < 1000){
        return [[NSString alloc] initWithFormat:@"%dm",[[NSNumber numberWithDouble:distance] intValue]];
    }else if(distance > 8000*1000){
        return @"";
    }else{
        long kmDistance = (long)distance/1000;
        return [[NSString alloc] initWithFormat:@"%ldkm",kmDistance];
    }
}



-(NSString *)dateTimeT_and_Z:(NSString *)timestring{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSDate *localDate = [dateFormatter dateFromString:timestring];
    
    NSDateFormatter *endDateFormat = [ [NSDateFormatter alloc] init];
    [endDateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *string =  [endDateFormat stringFromDate:localDate];
    
    return string;
    
}

-(NSString *)getLocalDateFormateUTCDate:(NSString *)utcDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //输入格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSTimeZone *localTimeZone = [NSTimeZone localTimeZone];
    [dateFormatter setTimeZone:localTimeZone];
    
    NSDate *dateFormatted = [dateFormatter dateFromString:utcDate];
    //输出格式
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:dateFormatted];

    return dateString;
}

+ (NSString *)dateFormart:(NSString *)dateString{
    NSDateFormatter *dateFormat1 = [ [NSDateFormatter alloc] init];
    [dateFormat1 setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormat1 dateFromString:dateString];
    NSDateFormatter *dateFormat2 = [ [NSDateFormatter alloc] init];
    [dateFormat2 setDateFormat:@"MMM dd, yyyy"];
    NSString *string = [dateFormat2 stringFromDate: date];
    return string;
}

+ (NSString *)dateFormart1:(NSString *)dateString{
    NSDateFormatter *dateFormat1 = [ [NSDateFormatter alloc] init];
    [dateFormat1 setDateFormat:@"MM/dd/yyyy"];
    NSDate *date = [dateFormat1 dateFromString:dateString];
    NSDateFormatter *dateFormat2 = [ [NSDateFormatter alloc] init];
    [dateFormat2 setDateFormat:@"MMM dd, yyyy"];
    NSString *string = [dateFormat2 stringFromDate: date];
    return string;
}


+ (NSString *)dateFormart2:(NSString *)dateString{
    NSDateFormatter *dateFormat1 = [ [NSDateFormatter alloc] init];
    [dateFormat1 setDateFormat:@"MMM dd, yyyy"];
    NSDate *date = [dateFormat1 dateFromString:dateString];
    NSDateFormatter *dateFormat2 = [ [NSDateFormatter alloc] init];
    [dateFormat2 setDateFormat:@"MM/dd/yyyy"];
    NSString *string = [dateFormat2 stringFromDate: date];
    return string;
}

+ (NSString *)returnStringWithTag:(NSString *)str{
    if (str==nil) {
        return @"";
    }
    NSArray *strArray = [str componentsSeparatedByString:@"#"];
    if (strArray.count>1) {
        NSMutableString *strReturn = [NSMutableString string];
        for (int i = 0; i <strArray.count; i ++) {
            if (i==0) {
                [strReturn appendString: [strArray objectAtIndex:0]];
            }else{
                NSString *string = [NSString stringWithFormat:@"#%@",[strArray objectAtIndex:i]];
                NSError *error = NULL;
                NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"([#][a-zA-Z0-9_]*\\b)" options:NSRegularExpressionCaseInsensitive error:&error];
                NSTextCheckingResult *result = [regex firstMatchInString:string options:0 range:NSMakeRange(0, [string length])];
                if (result) {
                    NSString *aStr = [NSString stringWithFormat:@"<a style=\"text-decoration:none;color:#DE2029;font-family:'OpenSans'\" href=\"http://www.qraved.com/\">%@</a>",[string substringWithRange:result.range]];
                    NSLog(@"%@",aStr);
                    [strReturn appendString:aStr];
                    [strReturn appendString:[string substringFromIndex:result.range.length]];
                    
                }else{
                    [strReturn appendString:string];
                }
            }
        }
        return strReturn;
    }else{
        return str;
    }
}

+ (NSString *)dateFormartForBrand:(NSString *)dateString{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.sssZ"];
    NSDate *localDate = [dateFormatter dateFromString:dateString];
    
    NSDateFormatter *endDateFormat = [ [NSDateFormatter alloc] init];
    [endDateFormat setDateFormat:@"yyyy/MM/dd HH:mm"];
    endDateFormat.timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
    NSString *string =  [endDateFormat stringFromDate:localDate];
    
    NSLog(@"%@",string);
    return string;
}

+ (NSString *)dateFormartNOHour:(NSString *)dateString{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.sssZ"];
    NSDate *localDate = [dateFormatter dateFromString:dateString];
    
    NSDateFormatter *endDateFormat = [ [NSDateFormatter alloc] init];
    [endDateFormat setDateFormat:@"MMM dd, yyyy"];
    endDateFormat.timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
    NSString *string =  [endDateFormat stringFromDate:localDate];
    
    NSLog(@"%@",string);
    return string;
}

@end
