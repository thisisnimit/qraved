//
//  NSDictionary+Helper.h
//  Qraved
//
//  Created by Shine Wang on 9/30/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Helper)

- (NSString*)filterStringForKey:(id)aKey;

@end
