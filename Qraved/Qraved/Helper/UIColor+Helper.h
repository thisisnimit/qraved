//
//  UIColor+Helper.h
//  PocketPlan
//
//  Created by Libo Liu on 10/21/13.
//  Copyright (c) 2013 com.imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Helper)
+(UIColor*)colorLightGray;
+(UIColor*)colorMidGray;
+(UIColor*)colorDarkGray;
+(UIColor*)colorRed;
+(UIColor*)colorGreen;
+ (UIColor *)colorCyan;
+(UIColor *)submitBtnRedColor;

+(UIColor *)redButtonColor;
+(UIColor *)grayButtonColor;
+(UIColor *)greenButtonColor;

+(UIColor *)waterFallBgColor;
+(UIColor *)buttonRedColor;
+(UIColor *)formBorderColor;
+(UIColor *)formBGColor;
+(UIColor *)defaultColor;
+(UIColor *)detailPageContentColor;
+(UIColor *)detailBGColor;
+(UIColor *)buttonEnableColor;
+(UIColor *)buttonDisableColor;
+(UIColor *)barButtonTitleColor;

+(UIColor*)colorF4F4F4;
+(UIColor*)colorD9D9D9;
+(UIColor*)colorE4E4E4;
+(UIColor*)color333333;
+(UIColor*)color58D3CC;
+(UIColor*)color6B6B6B;
+(UIColor*)color61D2C0;
+(UIColor*)color434343;
+(UIColor*)colorFFC000;
+(UIColor*)colorEDEDED;
+(UIColor*)color111111;
+(UIColor*)colorC2060A;
+(UIColor*)colorCCCCCC;
+(UIColor*)color999999;
+(UIColor*)color222222;
+(UIColor*)colorDDDDDD;
+(UIColor*)colorEEEEEE;
+(UIColor*)color3BAF24;
+(UIColor*)colorFBFBFB;
+(UIColor*)color666666;
+(UIColor*)colorE7E7E7;
+(UIColor*)color6F6F6F;
+(UIColor*)colorC2C2C2;
+(UIColor*)colorEBEBEB;

+(UIColor *)colortextBlue;
+(UIColor *)colorLineGray;
+(UIColor*)colorAAAAAA;

+(UIColor *)color234Gray;
+(UIColor *)color222Red;

+ (UIColor *)colorWithHexString:(NSString *)hexString;

@end
