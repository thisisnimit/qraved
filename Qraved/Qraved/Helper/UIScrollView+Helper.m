//
//  UIScrollView+Helper.m
//  Qraved
//
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "UIScrollView+Helper.h"
#import <objc/runtime.h>

@implementation UIScrollView (Helper)

- (NSNumber *)qraved_navigationTitleScrollLastOffsetY {
    return objc_getAssociatedObject(self, @selector(qraved_navigationTitleScrollLastOffsetY));
}

- (void)qraved_setNavigationTitleScrollLastOffsetY:(NSNumber *)qraved_navigationTitleScrollLastOffsetY {
    objc_setAssociatedObject(self, @selector(qraved_navigationTitleScrollLastOffsetY), qraved_navigationTitleScrollLastOffsetY, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(void)scrollNavigationController:(MLNavigationController *)navigationController{
    
    CGFloat navigationControllerInitY = [navigationController getNavigationControllerInitY];
    CGFloat navigationControllerInitHeight = [navigationController getNavigationControllerInitHeight];
    CGFloat navigationBarHeight = [navigationController getNavigationBarHeight];
    
    CGFloat curOffsetY = self.contentOffset.y;
    CGFloat lastOffsetY = [self.qraved_navigationTitleScrollLastOffsetY floatValue];
    
    CGRect navigationControllerFrame = navigationController.view.frame;
    if (curOffsetY < 0) {
        if (lastOffsetY > curOffsetY) {
            if (navigationControllerFrame.origin.y != navigationControllerInitY) {
                CGFloat newNavFrameY = navigationControllerFrame.origin.y + (lastOffsetY - curOffsetY);
                if (newNavFrameY < navigationControllerInitY) {
                    navigationControllerFrame.origin.y = navigationControllerFrame.origin.y  + (lastOffsetY - curOffsetY);
                    navigationControllerFrame.size.height = navigationControllerFrame.size.height - (lastOffsetY - curOffsetY);
                }else{
                    navigationControllerFrame.origin.y = navigationControllerInitY;
                    navigationControllerFrame.size.height = navigationControllerInitHeight;
                }
                navigationController.view.frame = navigationControllerFrame;
                navigationController.currFrameY = navigationController.view.frame.origin.y;
                navigationController.currFrameHeight = navigationController.view.frame.size.height;
            }
        }
    }else {
        if ( curOffsetY > lastOffsetY) {
            if (navigationControllerFrame.origin.y != (navigationControllerInitY-navigationBarHeight)) {
                CGFloat newNavFrameY = navigationControllerFrame.origin.y - (curOffsetY - lastOffsetY);
                if (newNavFrameY > (navigationControllerInitY-navigationBarHeight)) {
                    navigationControllerFrame.origin.y = navigationControllerFrame.origin.y  - (curOffsetY - lastOffsetY);
                    navigationControllerFrame.size.height = navigationControllerFrame.size.height + (curOffsetY - lastOffsetY);
                }else{
                    navigationControllerFrame.origin.y = (navigationControllerInitY-navigationBarHeight);
                    navigationControllerFrame.size.height = navigationControllerInitHeight + navigationBarHeight;
                }
                navigationController.view.frame = navigationControllerFrame;
                navigationController.currFrameY = navigationController.view.frame.origin.y;
                navigationController.currFrameHeight = navigationController.view.frame.size.height;
            }
        }else{
            if (navigationControllerFrame.origin.y != navigationControllerInitY) {
                CGFloat newNavFrameY = navigationControllerFrame.origin.y + (lastOffsetY - curOffsetY);
                if (newNavFrameY < navigationControllerInitY) {
                    navigationControllerFrame.origin.y = navigationControllerFrame.origin.y  + (lastOffsetY - curOffsetY);
                    navigationControllerFrame.size.height = navigationControllerFrame.size.height - (lastOffsetY - curOffsetY);
                }
                else{
                    navigationControllerFrame.origin.y = navigationControllerInitY;
                    navigationControllerFrame.size.height = navigationControllerInitHeight;
                }
                navigationController.view.frame = navigationControllerFrame;
                navigationController.currFrameY = navigationController.view.frame.origin.y;
                navigationController.currFrameHeight = navigationController.view.frame.size.height;
            }
        }
    }
    self.qraved_navigationTitleScrollLastOffsetY = [NSNumber numberWithFloat:curOffsetY];
}
@end
