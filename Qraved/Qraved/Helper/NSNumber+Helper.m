//
//  NSNumber+Helper.m
//  Qraved
//
//  Created by Libo Liu on 14-3-5.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "NSNumber+Helper.h"

@implementation NSNumber (Helper)
-(float)getRatingNumber{
    float rat = [self floatValue]/2;
    float fff = floorf(rat);
    float f2= rat-fff;
    if (f2<0.3) {
        f2 =0;
    }else if (f2<0.8){
        f2 = 0.5;
    }else {
        f2 = 1;
    }
    rat = fff + f2;
    
    return rat;
}
@end
