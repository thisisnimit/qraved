//
//  UILabel+Helper.m
//  PocketPlan
//
//  Created by Libo Liu on 10/17/13.
//  Copyright (c) 2013 com.imaginato. All rights reserved.
//

#import "UILabel+Helper.h"
#import "UIConstants.h"
#import "UIColor+Helper.h"

#define LEFTMARGIN 15
#define RIGHTMARGIN 30
#define PROFILEIMAGEVIEWSIZE 20

@implementation UILabel (Helper)

-(void)configButtonTitle
{
    [self setTextColor:[UIColor whiteColor]];
    [self setFont:[UIFont fontWithName:DEFAULT_FONT_NAME size:DEFAULT_BUTTON_TITLE_SIZE]];
    [self setNumberOfLines:1];
    self.backgroundColor=[UIColor clearColor];
}

-(void)configDetailContent{
    [self setTextColor:[UIColor colorDarkGray]];
    [self setFont:[self defaultFont:CUISINE_TITLE_FONT_SIZE]];
    [self setTextAlignment:NSTextAlignmentLeft];
    [self setLineBreakMode:NSLineBreakByWordWrapping];
    [self setNumberOfLines:0];
    self.backgroundColor = [UIColor clearColor];
}

-(void)configDetailBodyContent {
    [self setTextColor:[UIColor colorDarkGray]];
    [self setFont:[self defaultFont:DETAIL_BODY_FONT_SIZE]];
    [self setTextAlignment:NSTextAlignmentLeft];
    [self setLineBreakMode:NSLineBreakByWordWrapping];
    [self setNumberOfLines:0];
    self.backgroundColor = [UIColor clearColor];
}

-(void)configToTitle{
    [self setTextColor:[UIColor colorDarkGray]];
    [self setFont:[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:POST_TITLE_FONT_SIZE]];
    [self setNumberOfLines:1];
    self.backgroundColor = [UIColor clearColor];
}

-(void)configSearchRestaurantTitle
{
    [self setTextColor:[UIColor colorDarkGray]];
    [self setFont:[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:PAGE_TITLE_FONT_SIZE]];
    [self setNumberOfLines:1];
    self.backgroundColor = [UIColor clearColor];
}

-(void)configToMoreLink {
    [self setTextColor:[UIColor colorRed]];
    [self setFont:[UIFont fontWithName:DEFAULT_FONT_NAME size:POST_TITLE_FONT_SIZE]];
    [self setNumberOfLines:1];
    self.backgroundColor = [UIColor clearColor];
}

-(void)configDetailSectionTitle
{
    [self setTextColor:[UIColor colorMidGray]];
    [self setFont:[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:13]];
    [self setNumberOfLines:1];
    self.backgroundColor = [UIColor clearColor];

}

-(void)configToDefault{

}
-(void)configToEventTitle{
    self.backgroundColor = [UIColor clearColor];
    self.textColor = [UIColor colorWithHue:(DEFAULT_HIGHLIGHT_HUE/360.0)
                                 saturation:DEFAULT_HIGHLIGHT_SAT
                                 brightness:DEFAULT_HIGHLIGHT_BRI
                                      alpha:1.0];
    self.textAlignment = NSTextAlignmentLeft;
    self.font = [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:POST_TITLE_FONT_SIZE];
}
-(void)configToTitleCaption{
    [self setTextColor:[UIColor colorLightGray]];
    [self setFont:[self defaultFont:POST_TITLE_CAPTION_FONT_SIZE]];
    [self setNumberOfLines:0];
    self.backgroundColor = [UIColor clearColor];
}
-(void)configToSectionTitle{
    [self setTextColor:[UIColor colorMidGray]];
    [self setFont:[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:15]];
    [self setNumberOfLines:0];
}

-(void)configToBody{
    [self setTextColor:[UIColor colorDarkGray]];
    [self setFont:[self defaultFont:13]];
    [self setNumberOfLines:0];
}

-(void)configToSubsectionBody{
    [self setTextColor:[UIColor colorLightGray]];
    [self setFont:[UIFont fontWithName:DEFAULT_FONT_NAME size:11]];
    [self setNumberOfLines:0];
}


-(void)configToBookPanelSubsectionBody1{
    [self setTextColor:[UIColor whiteColor]];
    [self setFont:[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:11]];
    [self setNumberOfLines:0];
}

-(void)configToBookPanelSubsectionBody3{
    [self setTextColor:[UIColor whiteColor]];
    [self setFont:[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:12]];
    [self setNumberOfLines:0];
}

-(void)configToDefaultFont:(CGFloat)size{
    [self setFont:[UIFont fontWithName:DEFAULT_FONT_NAME size:size]];
}
-(void)configToDefaultBoldFont:(CGFloat)size{
    [self setFont:[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:size]];
}

-(void)configToLoginCaption{
    [self setFont:[self defaultFont:POST_TITLE_CAPTION_FONT_SIZE]];
    [self setNumberOfLines:0];
}

-(void)configPickerSubTitle
{
    [self setTextColor:[UIColor blackColor]];
    [self setFont:[UIFont fontWithName:DEFAULT_FONT_NAME size:13]];
    [self setTextAlignment:NSTextAlignmentLeft];
    [self setBackgroundColor:[UIColor clearColor]];
    [self setNumberOfLines:1];
}

-(void)configPickerTitle
{
    [self setTextColor:[UIColor blackColor]];
    [self setFont:[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:17]];
    [self setTextAlignment:NSTextAlignmentLeft];
    [self setBackgroundColor:[UIColor clearColor]];
    [self setNumberOfLines:1];
}


-(UIFont *)defaultFont:(CGFloat)fontSize
{
    return [UIFont fontWithName:DEFAULT_FONT_NAME size:fontSize];//[UIFont systemFontOfSize:fontSize];
}

-(CGSize)getLabelSize:(NSString *)string withFont:(UIFont *)font {
    return [string sizeWithFont:font constrainedToSize:CGSizeMake(DeviceWidth, 1000) lineBreakMode:NSLineBreakByWordWrapping];
}
-(CGSize)sizeWithWidth:(float)width{
    return [self.text sizeWithFont:self.font constrainedToSize:CGSizeMake(width, MAXFLOAT) lineBreakMode:self.lineBreakMode];
}
-(CGSize)sizeWithHeight:(float)height{
    return [self.text sizeWithFont:self.font constrainedToSize:CGSizeMake(MAXFLOAT, height) lineBreakMode:self.lineBreakMode];
}

-(void)resizeToStretch{
    float width = [self expectedWidth];
    CGRect newFrame = [self frame];
    newFrame.size.width = width;
    [self setFrame:newFrame];
}

-(void)resizeToStretch:(CGFloat) space{
    float width = [self expectedWidth];
    CGRect newFrame = [self frame];
    newFrame.size.width = width+space;
    [self setFrame:newFrame];
}

-(float)expectedWidth
{
    [self setNumberOfLines:1];
    
    CGSize maximumLabelSize = CGSizeMake(9999,self.frame.size.height);
    
    CGSize expectedLabelSize = [[self text] sizeWithFont:[self font]
                                       constrainedToSize:maximumLabelSize
                                           lineBreakMode:[self lineBreakMode]];
    return expectedLabelSize.width;
}

-(float)resizeToFit{
    float height = [self expectedHeight];
    CGRect newFrame = [self frame];
    newFrame.size.height = height;
    [self setFrame:newFrame];
    return newFrame.origin.y + newFrame.size.height;
}

-(float)expectedHeight{
    [self setNumberOfLines:0];
    [self setLineBreakMode:NSLineBreakByWordWrapping];
    CGSize maximumLabelSize = CGSizeMake(self.frame.size.width,9999);
    
    CGSize expectedLabelSize = [[self text] sizeWithFont:[self font]
                                       constrainedToSize:maximumLabelSize
                                           lineBreakMode:[self lineBreakMode]];
    return expectedLabelSize.height;
}

-(float)expectedHeightWithNumberOfLines:(float)numberOfLines
{
    [self setNumberOfLines:numberOfLines];
    [self setLineBreakMode:NSLineBreakByWordWrapping];
    CGSize maximumLabelSize = CGSizeMake(self.frame.size.width,9999);
    
    CGSize expectedLabelSize = [[self text] sizeWithFont:[self font]
                                       constrainedToSize:maximumLabelSize
                                           lineBreakMode:[self lineBreakMode]];
    return expectedLabelSize.height;
}

- (float)pointX
{
    return self.frame.origin.x;
}
- (float)pointY
{
    return self.frame.origin.y;
}

-(CGFloat)calculatedCommentTextHeight:(NSString*)comment{
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:17]};
    CGSize singleSize = [@"test" boundingRectWithSize:CGSizeMake(DeviceWidth-LEFTMARGIN-PROFILEIMAGEVIEWSIZE-RIGHTMARGIN, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    CGSize textSize = [comment boundingRectWithSize:CGSizeMake(DeviceWidth-LEFTMARGIN-PROFILEIMAGEVIEWSIZE-RIGHTMARGIN, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    NSInteger lines = textSize.height/singleSize.height;
    CGFloat height=0.0f;
    if(lines>3){
        height=3*singleSize.height;
    }else{
        height=textSize.height;
    }
    return height;
}


@end
