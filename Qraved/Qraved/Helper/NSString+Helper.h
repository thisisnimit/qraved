//
//  NSString+Helper.h
//  Qraved
//
//  Created by Shine Wang on 9/30/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ImageSizeENUM.h"

@interface NSString (Helper)
-(NSString *)filterHtml;
-(NSString *)inFilterHtmlWithHref:(NSString *)herf;
-(NSString *)promotionsHTML;
-(NSString *)changeStyle;
-(NSString *)filterNullString;
-(NSString *)replaceUrlString;
-(NSString *)returnCurrentImageString;

-(BOOL)isValidateUsername;
-(BOOL)isValidateEmail;
-(BOOL)isValidatePhone;
-(BOOL)isValidateInternationPhone;
-(BOOL)isClaimYourRestaurantValidatePhone;

-(BOOL)isBlankString;
-(NSDate *)dateFromString;
-(NSString*)ImgURLStringWide:(int)wide High:(int)high;
-(NSString*)ImgURLString;
-(NSString *)returnShortPhoneNum;   //delete country code +62;
-(NSString *)returnFullImageUrl;
-(NSString *)returnFullImageUrlWithWidth:(CGFloat )width;
-(NSString *)returnFullWebPImageUrlWithWidth:(CGFloat )width;
-(NSString *)returnFullImageUrlWithFixedWidth:(CGFloat )width;
-(NSString *)returnFullImageUrlWithWidth:(CGFloat )width andHeight:(CGFloat)height;
-(NSString*)ImgURLStringWide:(ImageSizeType)imageSizeType;
-(NSString *)returnFullImageUrlWithImageSizeType:(ImageSizeType)imageSizeType;
-(NSString *)MD5String;
- (NSString *)toFormatNumberString;
- (NSString *)removeHTML;
- (NSString *)removeHTML2;
-(NSString *)html;
-(BOOL)isLogout;

-(NSString *)groupConcat:(NSArray *)stringArray;
-(NSString *)groupConcat:(NSArray *)stringArray length:(int)length;

-(NSString *)getHtmlTitle;
-(NSString *)getHtmlImageUrl;
-(NSString *)getHtmlJournalImageUrl;
- (NSString *)getHtmlId;
- (NSString*)dictionaryToJson:(NSDictionary *)dic;
-(NSString *)enterKey;

- (CGSize)sizeWithFont:(UIFont *)font;
- (CGSize)sizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size;
- (CGSize)sizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size lineBreakMode:(NSLineBreakMode)lineBreakMode;
- (void)drawAtPoint:(CGPoint)point withFont:(UIFont *)font;
-(NSString*)returnFullCDNImageUrlWithWidth:(CGFloat)width;
-(NSString*)returnFullCDNImageUrlWithWidth:(CGFloat)width andHeight:(CGFloat)height;

-(BOOL)isEmpty;
-(NSString*)getAMOrPMString:(NSString*)string;

+(NSString *)returnDistanceWithLatitude:(NSString *)latitude andLongitude:(NSString *)longitude;

-(NSString *)returnnewFullImageUrl;

-(NSString *)dateTimeT_and_Z:(NSString *)timestring;
-(NSString *)getLocalDateFormateUTCDate:(NSString *)utcDate;


//Mar 14, 2014
+ (NSString *)dateFormart:(NSString *)dateString;//yyyy-mm-dd
+ (NSString *)dateFormart1:(NSString *)dateString;//yyyy/mm/dd
+ (NSString *)dateFormart2:(NSString *)dateString;
+ (NSString *)dateFormartForBrand:(NSString *)dateString;
+ (NSString *)dateFormartNOHour:(NSString *)dateString;

+ (NSString *)returnStringWithTag:(NSString *)str;

@end
