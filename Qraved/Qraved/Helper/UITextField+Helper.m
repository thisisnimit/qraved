//
//  UITextField+Helper.m
//  Qraved
//
//  Created by Shine Wang on 11/27/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "UITextField+Helper.h"
#import <objc/objc.h>
#import <objc/runtime.h>

static NSString *kLimitTextLengthKey = @"kLimitTextLengthKey";

@implementation UITextField (Helper)

- (void)limitTextLength:(int)length
{
    objc_setAssociatedObject(self, &kLimitTextLengthKey, [NSNumber numberWithInt:length], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    [self addTarget:self action:@selector(textFieldTextLengthLimit:) forControlEvents:UIControlEventEditingChanged];
}

- (void)textFieldTextLengthLimit:(id)sender
{
    NSNumber *lengthNumber = objc_getAssociatedObject(self, &kLimitTextLengthKey);
    int length = [lengthNumber intValue];
    if(self.text.length > length){
        self.text = [self.text substringToIndex:length];
    }
}

@end
