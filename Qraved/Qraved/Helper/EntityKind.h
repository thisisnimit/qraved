//
//  EntityKind.h
//  Qraved
//
//  Created by DeliveLee on 16/3/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGDish.h"
#import "IMGRestaurantEvent.h"
#import "IMGMenu.h"

typedef NS_ENUM(NSInteger, IMGType) {
    TypeForUnknown            = 0,
    TypeForIMGDish            = 1,   // From IMGDish
    TypeForIMGRestaurantEvent = 2,   // From RestaurantEvent
    TypeForIMGReview          = 3,    // From IMGReview
    TypeForIMGMenu            = 4    // From IMGMenu
};


@interface EntityKind : NSObject

+ (IMGType)typeKindFromEntity:(IMGEntity *)entity;
+ (Class)classKindFromType:(IMGType)imgType;
+ (NSDictionary *) entityToDictionary:(id)entity;
+(void)unfoldAnimationWith:(UIView *)view;
+(void)foldAnimationWith:(UIView *)view;
@end
