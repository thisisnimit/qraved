//
//  UIActivityItemProvider+CopyUrl.h
//  Qraved
//
//  Created by harry on 2017/11/27.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIActivityItemProvider (CopyUrl)

- (NSString *)returnCopyUrl:(NSURL *)url;

@end
