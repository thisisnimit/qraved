//
//  CommonMethod.m
//  Qraved
//
//  Created by Adam.zhang on 2017/9/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "CommonMethod.h"
#import "V2_BannerModel.h"
#import "V2_InstagramModel.h"
#import "V2_HomeModel.h"
#import "IMGDiningGuide.h"
#import "IMGMediaComment.h"
#import "V2_LocationModel.h"
#import "InstagramFilterModel.h"
#import "DeliveryBannerModel.h"
@implementation CommonMethod
+(void)unfoldAnimationWith:(UIView *)view
{
    
    [UIView animateWithDuration:0.3 animations:^{
        CGAffineTransform rotate = view.transform;
        rotate = CGAffineTransformRotate(rotate,-M_PI);
        [view setTransform:rotate];
    } completion:^(BOOL finished) {
    }];
    
}

+(void)foldAnimationWith:(UIView *)view
{
    [UIView animateWithDuration:0.3 animations:^{
        CGAffineTransform rotate = view.transform;
        rotate = CGAffineTransformRotate(rotate,M_PI+0.001);
        [view setTransform:rotate];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.01 animations:^{
            CGAffineTransform rotate = view.transform;
            rotate = CGAffineTransformRotate(rotate,-0.001);
            [view setTransform:rotate];
        }];
    }];
    
}

+ (void)doSaveWithRestaurant:(IMGRestaurant *)restaurant andViewController:(id)viewController andCallBackBlock:(SaveCallBackBlock)block{
    if ([restaurant.saved isEqual:@1]) {
        [[LoadingView sharedLoadingView] startLoading];
        [ListHandler removeRestaurantFromListWithRestaurantId:restaurant.restaurantId andListId:nil andBlock:^{
            [[LoadingView sharedLoadingView] stopLoading];
            block(NO);
        } failure:^(NSString *exceptionMsg) {
            
        }];
    }else{
        [self saveRestaurantToList:restaurant andViewController:viewController andCallBackBlock:^(BOOL isSaved) {
            block(YES);
        }];
    }
}
+ (void)saveRestaurantToList:(IMGRestaurant *)restaurant andViewController:(id)viewController andCallBackBlock:(SaveCallBackBlock)block{
    V2_SavedToListView *savedAddListView = [[V2_SavedToListView alloc] init];
    savedAddListView.controller = viewController;
    savedAddListView.restaurant = restaurant;
    savedAddListView.changeButtonTapped = ^{
        [self saveRestaurantToList:restaurant andViewController:self andCallBackBlock:^(BOOL isSaved) {
            block(YES);
        }];
    };
    savedAddListView.refreshRestaurant = ^(BOOL liked,NSString *listName) {
        block(YES);
    };
    savedAddListView.frame = [AppDelegate ShareApp].window.bounds;
    [[AppDelegate ShareApp].window addSubview:savedAddListView];
}

+ (void)saveRestaurantToList:(IMGRestaurant *)restaurant andViewController:(id)viewController andSavedToListBlock:(SavedToListBlock)block{
    V2_SavedToListView *savedAddListView = [[V2_SavedToListView alloc] init];
    savedAddListView.controller = viewController;
    savedAddListView.restaurant = restaurant;
    savedAddListView.changeButtonTapped = ^{
        [self saveRestaurantToList:restaurant andViewController:self andSavedToListBlock:^(BOOL isSaved, NSString *listName) {
            block(YES, listName);
        }];
    };
    savedAddListView.refreshRestaurant = ^(BOOL liked,NSString *listName) {
        block(YES, listName);
    };
    savedAddListView.frame = [AppDelegate ShareApp].window.bounds;
    [[AppDelegate ShareApp].window addSubview:savedAddListView];
}

+ (void)goFoodButtonTapped:(IMGRestaurant *)restaurant andViewController:(id)viewController andCallBackBlock:(SaveCallBackBlock)block;{
    if ([restaurant isOpenRestaurant]) {
        if (![Tools isBlankString:restaurant.goFoodLink]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:restaurant.goFoodLink]];
        }
    }else{
        [UIAlertController showAlertCntrollerWithViewController:viewController alertControllerStyle:UIAlertControllerStyleAlert title:@"Restaurant Closed" message:@"This restaurant is currently closed but don’t worry! You can save it for later." cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Save for later" CallBackBlock:^(NSInteger btnIndex) {
            if (btnIndex == 1) {
                [self saveRestaurantToList:restaurant andViewController:self andCallBackBlock:^(BOOL isSaved) {
                    block(YES);
                }];
            }
        }];
    }
}
+ (void)goFoodButtonTapped:(IMGRestaurant *)restaurant andViewController:(id)viewController andRestaurantState:(BOOL)isOpen andCallBackBlock:(SaveCallBackBlock)block{
    if (isOpen) {
        if (![Tools isBlankString:restaurant.goFoodLink]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:restaurant.goFoodLink]];
        }
    }else{
        [UIAlertController showAlertCntrollerWithViewController:viewController alertControllerStyle:UIAlertControllerStyleAlert title:@"Restaurant Closed" message:@"This restaurant is currently closed but don’t worry! You can save it for later." cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Save for later" CallBackBlock:^(NSInteger btnIndex) {
            if (btnIndex == 1) {
                [self saveRestaurantToList:restaurant andViewController:self andCallBackBlock:^(BOOL isSaved) {
                    block(YES);
                }];
            }
        }];
    }
}

+ (void)checkCameraStateWithViewController:(id)viewController andCallBackBlock:(CommonCallBackBlock)block{
    NSString* mediaType =AVMediaTypeVideo;
    AVAuthorizationStatus authorizationStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
    if (authorizationStatus == AVAuthorizationStatusRestricted || authorizationStatus == AVAuthorizationStatusDenied) {
        [UIAlertController showFunctionAlertCntrollerWithViewController:viewController alertControllerStyle:UIAlertControllerStyleAlert title:@"Please allow access to your Camera" message:@"To enable access, tap Settings and turn on Camera" cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Settings" CallBackBlock:^(NSInteger btnIndex) {
            NSURL*url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            if( [[UIApplication sharedApplication]canOpenURL:url] ) {
                [[UIApplication sharedApplication]openURL:url];
            }
        }];
    }else{
        block();
    }
}

+ (void)setJsonToFile:(NSString *)fileName andJson:(id)jsonDic{
     NSString *filePath = [NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@.json",fileName]];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDic options:NSJSONWritingPrettyPrinted error:nil];
    [jsonData writeToFile:filePath atomically:YES];
}

+ (id)getJsonFromFile:(NSString *)fileName{
    NSString *filePath = [NSHomeDirectory() stringByAppendingString:[NSString stringWithFormat:@"/Documents/%@.json",fileName]];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    if (data != nil) {
        NSError *error;
        id json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        return json;
    }else{
        return nil;
    }
}

+ (NSArray *)getHomeBanner:(id)json{
    NSMutableArray *bannerArr = [NSMutableArray array];
    for (NSDictionary *dic in json) {
        V2_BannerModel *bannerModel = [[V2_BannerModel alloc]init];
        [bannerModel setValuesForKeysWithDictionary:dic];
        NSDictionary *actionDic = [[dic objectForKey:@"rule"] objectForKey:@"action"];
        if (![[actionDic objectForKey:@"attributes"] isEqual:[NSNull null]]) {
            bannerModel.attributes = [actionDic objectForKey:@"attributes"];
        }
        bannerModel.type = [actionDic objectForKey:@"type"];
        [bannerArr addObject:bannerModel];
    }
    return bannerArr;
}

+ (NSArray *)getHomeBannerImage:(id)json{
    NSMutableArray *arr = [NSMutableArray array];
    for (NSDictionary *dic in json) {
        [arr addObject:[dic objectForKey:@"image"]];
    }
    return arr;
}

+ (NSArray *)getHomeInstagram:(id)json{
    NSMutableArray *instagramArr = [NSMutableArray array];
    for (NSDictionary *instagramDic in [json objectForKey:@"data"]) {
        V2_InstagramModel *model = [[V2_InstagramModel alloc] init];
        [model setValuesForKeysWithDictionary:instagramDic];
        [instagramArr addObject:model];
    }
    return instagramArr;
}

+ (NSArray *)getHomeComponents:(id)json{
    NSMutableArray *homeArray = [NSMutableArray array];
    NSArray *homeArr = [json objectForKey:@"components"];
    for (NSDictionary *homeDic in homeArr) {
        NSMutableArray *tempArr = [NSMutableArray array];
        V2_HomeModel *model = [[V2_HomeModel alloc] init];
        model.asset = [homeDic objectForKey:@"asset"];
        model.title = [homeDic objectForKey:@"title"];
        model.componentId = [homeDic objectForKey:@"id"];
        
        NSDictionary *customDic = [homeDic objectForKey:@"customAttributes"];
        if (![customDic isKindOfClass:[NSNull class]]) {
            NSArray *categoryArr = [customDic objectForKey:@"categoryId"];
            if (categoryArr != nil && categoryArr.count != 0) {
                model.categoryId = [categoryArr objectAtIndex:0];
            }
        }
        
        if ([[homeDic objectForKey:@"asset"] isEqualToString:@"restaurant"]) {
            for (NSDictionary *restaurantDic in [homeDic objectForKey:@"docs"]) {
                IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
                restaurant.restaurantId = [restaurantDic objectForKey:@"id"];
                restaurant.logoImage = [restaurantDic objectForKey:@"image"];
                restaurant.title = [restaurantDic objectForKey:@"title"];
                restaurant.state = [restaurantDic objectForKey:@"state"];
                if ([[restaurantDic objectForKey:@"rating"] isEqual:[NSNull null]]) {
                    restaurant.ratingScore = @"0";
                }else{
                    restaurant.ratingScore = [restaurantDic objectForKey:@"rating"];
                }
                restaurant.saved = [restaurantDic objectForKey:@"userSaved"];
                restaurant.priceName = [restaurantDic objectForKey:@"priceLevelDisplayName"];
                restaurant.longitude = [restaurantDic objectForKey:@"longitude"];
                restaurant.latitude = [restaurantDic objectForKey:@"latitude"];
                restaurant.districtName = [restaurantDic objectForKey:@"districtName"];
                if ([[restaurantDic objectForKey:@"ratingCount"] isEqual:[NSNull null]]) {
                    restaurant.ratingCount = @0;
                }else{
                    restaurant.ratingCount = [restaurantDic objectForKey:@"ratingCount"];
                }
                restaurant.priceLevel = [restaurantDic objectForKey:@"priceLevel"];
                restaurant.cuisineName = [restaurantDic objectForKey:@"primaryCuisineName"];
                restaurant.brandLogo = [restaurantDic objectForKey:@"brandImage"];
                restaurant.goFoodLink = [restaurantDic objectForKey:@"gofoodLink"];
                
                [tempArr addObject:restaurant];
            }
            model.docs = tempArr;
        }else if ([[homeDic objectForKey:@"asset"] isEqualToString:@"guide"]){
            for (NSDictionary *guideDic in [homeDic objectForKey:@"docs"]) {
                IMGDiningGuide *guide = [[IMGDiningGuide alloc] init];
                guide.diningGuideId = [guideDic objectForKey:@"id"];
                if (![[guideDic objectForKey:@"headerImage"] isEqual:[NSNull null]]) {
                    guide.headerImage = [guideDic objectForKey:@"headerImage"];
                }
                
                guide.cityId = [guideDic objectForKey:@"cityId"];
                guide.pageType = [guideDic objectForKey:@"pageType"];
                if (![[guideDic objectForKey:@"pageName"] isEqual:[NSNull null]]) {
                    guide.pageName = [guideDic objectForKey:@"pageName"];
                }
                
                guide.pageTitle = [guideDic objectForKey:@"pageTitle"];
                
                if (![[guideDic objectForKey:@"pageContent"] isEqual:[NSNull null]]) {
                    guide.pageContent = [guideDic objectForKey:@"pageContent"];
                }
                guide.headerImageThumbnail = [guideDic objectForKey:@"thumbnailImageUrl"];
                
                guide.pageUrl = [guideDic objectForKey:@"pageUrl"];
                [tempArr addObject:guide];
            }
            
            model.docs = tempArr;
        }else if ([[homeDic objectForKey:@"asset"] isEqualToString:@"article"]){
            
            for (NSDictionary *journalDic in [homeDic objectForKey:@"docs"]) {
                IMGMediaComment *journal = [[IMGMediaComment alloc] init];
                journal.mediaCommentId = [journalDic objectForKey:@"id"];
                journal.journalImageUrl = [journalDic objectForKey:@"mainPhoto"];
                journal.journalTitle = [journalDic objectForKey:@"title"];
                journal.title = [journalDic objectForKey:@"title"];
                journal.link = [journalDic objectForKey:@"link"];
                journal.imageUrl = [journalDic objectForKey:@"mainPhoto"];
                journal.hasVideo = [journalDic objectForKey:@"hasVideo"];
                [tempArr addObject:journal];
            }
            if (tempArr.count==0) {
                break;
            }
            model.docs = tempArr;
            
        }
        if (model.docs.count>2) {
            [homeArray addObject:model];
        }
    }
    return homeArray;
}

+ (NSArray *)getHomeLocation:(id)json{
    NSMutableArray *locationArr = [NSMutableArray array];
    for (NSDictionary *locationDic in json) {
        V2_LocationModel *model = [[V2_LocationModel alloc] init];
        [model setValuesForKeysWithDictionary:locationDic];
        [locationArr addObject:model];
    }
    return locationArr;
}

+ (NSArray *)getHomeInstagramFilter:(id)json{
    NSMutableArray *filterArr = [NSMutableArray array];
    for (NSDictionary *dic in json) {
        InstagramFilterModel *model = [[InstagramFilterModel alloc] init];
        [model setValuesForKeysWithDictionary:dic];
        model.isSelect = NO;
        [filterArr addObject:model];
    }
    return filterArr;
}

+ (NSArray *)getSavedRestaurant:(id)json{
    NSArray *tempArry = [json objectForKey:@"restaurantList"];
    NSMutableArray *restaurantArr = [NSMutableArray array];
    for (NSDictionary *dic in tempArry) {
        IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
        [restaurant setValuesForKeysWithDictionary:dic];
        restaurant.ratingScore = [dic objectForKey:@"rating"];
        restaurant.address1 = [dic objectForKey:@"address"];
        restaurant.bookStatus = [dic objectForKey:@"status"];
        NSDictionary* yesterOpenDic=[dic objectForKey:@"yesterDayOpenTime"];
        restaurant.yesteropenDay=[yesterOpenDic objectForKey:@"openDay"];
        restaurant.yesteropenTime=[yesterOpenDic objectForKey:@"openTime"];
        restaurant.yesterclosedTime=[yesterOpenDic objectForKey:@"closedTime"];
        restaurant.saved = @1;
        restaurant.goFoodLink = [dic objectForKey:@"goFoodLink"];
        
        NSDictionary* nextOpenDay=[dic objectForKey:@"nextOpenDay"];
        restaurant.nextopenTime=[nextOpenDay objectForKey:@"openTime"];
        restaurant.nextclosedTime=[nextOpenDay objectForKey:@"closedTime"];
        restaurant.nextopenDay=[nextOpenDay objectForKey:@"openDay"];
        
        NSDictionary* secondOpenDic=[dic objectForKey:@"secondOpenDay"];
        restaurant.secondclosedTime=[secondOpenDic objectForKey:@"closedTime"];
        restaurant.secondopenTime=[secondOpenDic objectForKey:@"openTime"];
        restaurant.secondopenDay=[secondOpenDic objectForKey:@"openDay"];
        
        [restaurantArr addObject:restaurant];
    }
    return restaurantArr;
}

+ (NSArray *)getDeliveryJournalList:(id)json{
    NSMutableArray *journalArray = [NSMutableArray array];
    for (NSDictionary *dic in [json objectForKey:@"article_list"]) {
        IMGMediaComment *journal = [[IMGMediaComment alloc] init];
        journal.mediaCommentId = [dic objectForKey:@"id"];
        journal.journalTitle = [dic objectForKey:@"title"];
        if (![Tools isBlankString:[dic objectForKey:@"custom_image_url"]]) {
            journal.journalImageUrl = [dic objectForKey:@"custom_image_url"];
        }else{
            journal.journalImageUrl = [dic objectForKey:@"main_photo"];
        }
        journal.hasVideo = [dic objectForKey:@"has_video"];
        [journalArray addObject:journal];
    }
    return journalArray;
}

+ (NSArray *)getDeliveryBanner:(id)json{
    NSMutableArray *bannerArray = [NSMutableArray array];
    for (NSDictionary *dic in json) {
        DeliveryBannerModel *model = [[DeliveryBannerModel alloc] init];
        [model setValuesForKeysWithDictionary:dic];
        [bannerArray addObject:model];
    }
    return bannerArray;
}
//+ (NSArray *)getDeliveryBannerImage:(id)json{
//    
//}

+ (NSArray *)getGuideList:(id)json{
    NSMutableArray *diningGuideArrM = [[NSMutableArray alloc] init];
    NSArray * array =[json objectForKey:@"diningguideList"];
    for (int i=0; i<array.count; i++)
    {
        NSArray *diningGuideArray = [array objectAtIndex:i];
        NSDictionary * diningGuideDictionary = [diningGuideArray objectAtIndex:0];
        IMGDiningGuide * guide =[[IMGDiningGuide alloc]init];
        [guide setValuesForKeysWithDictionary:diningGuideDictionary];
        guide.restaurantCount = [diningGuideArray objectAtIndex:1];
        guide.pageName = [guide.pageName filterHtml];
        guide.diningGuideId = [diningGuideDictionary objectForKey:@"id"];
        if([guide.pageShow intValue]==1)
        {
            [diningGuideArrM addObject:guide];
        }
    }
    return diningGuideArrM;
}

@end
