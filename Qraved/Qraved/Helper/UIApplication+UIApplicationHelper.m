//
//  UIApplication+UIApplicationHelper.m
//  Qraved
//
//  Created by apple on 16/9/5.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "UIApplication+UIApplicationHelper.h"
#import "DetailViewController.h"
#import <objc/message.h>
#import "AppDelegate.h"
#import "JournalDetailViewController.h"
#import "IMGMediaComment.h"
@implementation UIApplication (UIApplicationHelper)
+ (void)load
{
    
//     交换方法实现,方法都是定义在类里面
//     class_getMethodImplementation:获取方法实现
//     class_getInstanceMethod:获取对象
//     class_getClassMethod:获取类方法
//     IMP:方法实现
//    
//     imageNamed
//     Class:获取哪个类方法
//     SEL:获取方法编号,根据SEL就能去对应的类找方法
//    Method openURLMethod = class_getInstanceMethod([UIApplication class], @selector(openURL:));
//    
//    // xmg_imageNamed
//    Method ZH_openURLMethod = class_getInstanceMethod([UIApplication class], @selector(ZH_openURLMethod:));
//    
//    // 交换方法实现
//    method_exchangeImplementations(openURLMethod, ZH_openURLMethod);
}

// 运行时

// 先写一个其他方法,实现这个功能


- (BOOL)ZH_openURLMethod:(NSURL *)url
{
    IMGNetWork *manager = [IMGNetWork sharedManager];
    NSString *urlString = [NSString stringWithFormat:@"%@",url];
    if ([urlString rangeOfString:@"/Jakarta/"].location == NSNotFound && [urlString rangeOfString:@"/Bandung/"].location == NSNotFound && [urlString rangeOfString:@"/Bali/"].location == NSNotFound) {
        if ([urlString rangeOfString:@"/journal/"].location!=NSNotFound) {
            NSArray *tmpArray = [urlString componentsSeparatedByString:@"/"];
            NSString *journalSeoString = tmpArray.lastObject;
            if ([journalSeoString isEqualToString:@""]) {
                journalSeoString = tmpArray[tmpArray.count-2];
            }else{
                return [[UIApplication sharedApplication] ZH_openURLMethod:url];

            }
            NSDictionary *parameters = @{@"seoKeyword":journalSeoString};
           [manager requestWithMethod:GET withPath:@"app/journal/check/exist" withParams:parameters withSuccessBlock:^(id responseObject) {
               NSArray *journalArticleIdArray=[responseObject objectForKey:@"journalArticleIdList"];
               if (journalArticleIdArray.count==1) {
                   JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
                   NSNumber *tmpInteger = (NSNumber*)journalArticleIdArray.firstObject;
                   IMGMediaComment *journal = [[IMGMediaComment alloc] init];
                   journal.mediaCommentId = tmpInteger;
                   jdvc.journal=journal;
                   ;
                   jdvc.isFromURL = YES;
                   UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:jdvc];
                   UIViewController *topRootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
                   while (topRootViewController.presentedViewController)
                   {
                       topRootViewController = topRootViewController.presentedViewController;
                   }
                   //  AppDelegate* app=(AppDelegate*)[UIApplication sharedApplication].delegate ;
                   [topRootViewController presentViewController:nav animated:YES completion:nil];
               }else{
                   [[UIApplication sharedApplication] ZH_openURLMethod:url];
                   
               }


               NSLog(@"%@",responseObject);
            } withFailurBlock:^(NSError *error) {
                [[UIApplication sharedApplication] ZH_openURLMethod:url];

            }];
            
        }
        else{
            return [[UIApplication sharedApplication] ZH_openURLMethod:url];
            
        }
    }
    else{
        NSString *keyWord;
        if (!([urlString rangeOfString:@"/Jakarta/"].location == NSNotFound)) {
            keyWord = [urlString substringFromIndex:([urlString rangeOfString:@"/Jakarta/"].location+[urlString rangeOfString:@"/Jakarta/"].length)];
        }else if (!([urlString rangeOfString:@"/Bali/"].location == NSNotFound)){
            keyWord = [urlString substringFromIndex:([urlString rangeOfString:@"/Bali/"].location+[urlString rangeOfString:@"/Bali/"].length)];
        }else if (!([urlString rangeOfString:@"/Bandung/"].location == NSNotFound)){
            keyWord = [urlString substringFromIndex:([urlString rangeOfString:@"/Bandung/"].location+[urlString rangeOfString:@"/Bandung/"].length)];
            
        }else{
            return [[UIApplication sharedApplication] ZH_openURLMethod:url];

        }
        NSArray *tmpArray = [keyWord componentsSeparatedByString:@"/"];
        if (tmpArray.count>0) {
            keyWord = tmpArray.firstObject;
        }

        NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:keyWord,@"seoKeyword", nil];
        [manager POST:@"app/restaurant/check/exist" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        
            NSArray *returnRestaurantIDArray=[responseObject objectForKey:@"restaurantIdList"];
            if (returnRestaurantIDArray.count==1) {
                DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurantId:[returnRestaurantIDArray objectAtIndex:0]];
                detailViewController.isFromURL = YES;
                UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:detailViewController];
                UIViewController *topRootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;   
                while (topRootViewController.presentedViewController)
                {
                    topRootViewController = topRootViewController.presentedViewController;
                }
              //  AppDelegate* app=(AppDelegate*)[UIApplication sharedApplication].delegate ;
                [topRootViewController presentViewController:nav animated:YES completion:nil];
                
            }else{
                [[UIApplication sharedApplication] ZH_openURLMethod:url];

            }
            
        }failure:^(NSURLSessionDataTask *operation, NSError *error) {
            [[UIApplication sharedApplication] ZH_openURLMethod:url];

            NSLog(@"submit erors when get datas:%@",error.localizedDescription);
        
        }];
    }
    return [[UIApplication sharedApplication] ZH_openURLMethod:url];

}

@end
