//
//  UIImage+Size.h
//  Qraved
//
//  Created by System Administrator on 3/9/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage(Size)


+(CGSize)getImageSizeWithURL:(id)imageURL;
+(CGSize)getPNGImageSizeWithRequest:(NSMutableURLRequest*)request;
+(CGSize)getGIFImageSizeWithRequest:(NSMutableURLRequest*)request;
+(CGSize)getJPGImageSizeWithRequest:(NSMutableURLRequest*)request;

@end
