//
//  UIScrollView+Helper.h
//  Qraved
//
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLNavigationController.h"
@interface UIScrollView (Helper)
@property (nonatomic, strong, setter = qraved_setNavigationTitleScrollLastOffsetY:) NSNumber *qraved_navigationTitleScrollLastOffsetY;
-(void)scrollNavigationController:(MLNavigationController *)navigationController;
@end
