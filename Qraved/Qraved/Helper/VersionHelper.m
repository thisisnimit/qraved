//
//  VersionHelper.m
//  Qraved
//
//  Created by Admin on 9/23/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "VersionHelper.h"
#import "PostDataHandler.h"
#import "AppDelegate.h"
#import "UIAlertView+BlocksKit.h"

#define kAppId  @"709021052"
#define kAppUrl @"https://itunes.apple.com/us/app/qraved-test/id709021052?ls=1&mt=8"


@implementation VersionHelper

-(void)checkNewVersion{
    NSString* const kAppdateUrl = @"http://itunes.apple.com/lookup";

    NSDictionary *parameters=  [NSDictionary dictionaryWithObjectsAndKeys:kAppId,@"id",nil];
    [[IMGNetWork sharedManager] GET:kAppdateUrl parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        [self getAppVersionFromAppStoreOK:responseObject];
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"checkNewVersion error:%@",error.description);
    }];
}
-(void)getAppVersionFromAppStoreOK:(id)postDic{
    NSArray* results = [postDic objectForKey: @"results"];
    if (results.count > 0)
    {
        NSDictionary* jsonData = [results objectAtIndex: 0];
        
        NSString* thisVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString*) kCFBundleVersionKey];
        NSString* thatVersion = [jsonData objectForKey: @"version"];
        
        BOOL needUpdate = [self compareVersions: thisVersion version2: thatVersion] == NSOrderedAscending;
        if (needUpdate) {
            [self showYouShouldUpdateYourApp];
        }
    }
}
-(void)gotoAppStore{
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", kAppUrl]]];
}
-(void)showYouShouldUpdateYourApp{
    UIAlertView *alertView=[UIAlertView bk_alertViewWithTitle:ALERT_TITLE message:@"This app has update, please update in App Store !"];
    [alertView addButtonWithTitle:@"Cancel"];
    [alertView bk_addButtonWithTitle:@"OK" handler:^(){
        [self gotoAppStore];
    }];
    [alertView show];
}

- (NSComparisonResult) compareVersions: (NSString*) version1 version2: (NSString*) version2
{
    NSComparisonResult result = NSOrderedSame;
    
    NSMutableArray* a = (NSMutableArray*) [version1 componentsSeparatedByString: @"."];
    NSMutableArray* b = (NSMutableArray*) [version2 componentsSeparatedByString: @"."];
    
    while (a.count < b.count) { [a addObject: @"0"]; }
    while (b.count < a.count) { [b addObject: @"0"]; }
    
    for (int i = 0; i < a.count; ++i)
    {
        if ([[a objectAtIndex: i] integerValue] < [[b objectAtIndex: i] integerValue])
        {
            result = NSOrderedAscending;
            break;
        }
        else if ([[b objectAtIndex: i] integerValue] < [[a objectAtIndex: i] integerValue])
        {
            result = NSOrderedDescending;
            break;
        }
    }
    
    return result;
}

@end
