//
//  PushHelper.h
//  Qraved
//
//  Created by Admin on 9/23/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#define kRemoteNotificationDeviceToken @"Qraved2RemoteNotificationDeviceToken"

@interface PushHelper : NSObject

+(void)postUserDeviceToken;
+(void)postUserDeviceToken: (NSString*)deviceToken;


@end
