//
//  EntityKind.m
//  Qraved
//
//  Created by DeliveLee on 16/3/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "EntityKind.h"
#import <objc/runtime.h> 

@implementation EntityKind

+ (IMGType)typeKindFromEntity:(IMGEntity *)entity {
    if ([entity isKindOfClass:[IMGDish class]]) {
        return TypeForIMGDish;
    }else if([entity isKindOfClass:[IMGRestaurantEvent class]]) {
        return TypeForIMGRestaurantEvent;
    }else if([entity isKindOfClass:[IMGMenu class]]) {
        return TypeForIMGMenu;
    }else{
        return TypeForUnknown;
    }
}
+ (Class)classKindFromType:(IMGType)imgType{
    if (imgType == TypeForIMGDish) {
        return [IMGDish class];
    }if (imgType == TypeForIMGRestaurantEvent) {
        return [IMGRestaurantEvent class];
    }else if(imgType == TypeForIMGMenu) {
        return [IMGMenu class];
    }else{
        return [NSNull class];
    }
}

+ (NSDictionary *) entityToDictionary:(id)entity
{
    
    Class clazz = [entity class];
    u_int count;
    
    objc_property_t* properties = class_copyPropertyList(clazz, &count);
    NSMutableArray* propertyArray = [NSMutableArray arrayWithCapacity:count];
    NSMutableArray* valueArray = [NSMutableArray arrayWithCapacity:count];
    
    for (int i = 0; i < count ; i++)
    {
        objc_property_t prop=properties[i];
        const char* propertyName = property_getName(prop);
        
        [propertyArray addObject:[NSString stringWithCString:propertyName encoding:NSUTF8StringEncoding]];
        
        //        const char* attributeName = property_getAttributes(prop);
        //        NSLog(@"%@",[NSString stringWithUTF8String:propertyName]);
        //        NSLog(@"%@",[NSString stringWithUTF8String:attributeName]);
        SuppressPerformSelectorLeakWarning(
                                           id value =  [entity performSelector:NSSelectorFromString([NSString stringWithUTF8String:propertyName])];
                                           if(value ==nil)
                                           [valueArray addObject:[NSNull null]];
                                           else {
                                               [valueArray addObject:value];
                                           }

        
        );
                //        NSLog(@"%@",value);
    }
    
    free(properties);
    
    NSDictionary* returnDic = [NSDictionary dictionaryWithObjects:valueArray forKeys:propertyArray];
    NSLog(@"%@", returnDic);
    
    return returnDic;
}
+(void)unfoldAnimationWith:(UIView *)view
{
    
    [UIView animateWithDuration:0.3 animations:^{
        CGAffineTransform rotate = view.transform;
        rotate = CGAffineTransformRotate(rotate,-M_PI);
        [view setTransform:rotate];
    } completion:^(BOOL finished) {
    }];
    
}

+(void)foldAnimationWith:(UIView *)view
{
    [UIView animateWithDuration:0.3 animations:^{
        CGAffineTransform rotate = view.transform;
        rotate = CGAffineTransformRotate(rotate,M_PI+0.001);
        [view setTransform:rotate];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.01 animations:^{
            CGAffineTransform rotate = view.transform;
            rotate = CGAffineTransformRotate(rotate,-0.001);
            [view setTransform:rotate];
        }];
    }];
    
}



@end
