//
//  VersionHelper.h
//  Qraved
//
//  Created by Admin on 9/23/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VersionHelper : NSObject
-(void)checkNewVersion;
@end
