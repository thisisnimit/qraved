//
//  IMGArea.h
//  Qraved
//
//  Created by lucky on 16/9/21.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGArea : IMGEntity

@property(nonatomic,retain) NSNumber *tagId;
@property(nonatomic,retain) NSNumber *cityId;
@property(nonatomic,copy)   NSString *name;
@property(nonatomic,retain) NSNumber *ranking;
@property(nonatomic,retain) NSNumber *status;
@property(nonatomic,copy)   NSString *type;
@property(nonatomic,copy)   NSString *typeShows;
@property(nonatomic,retain) NSNumber *typeVisible;
@property(nonatomic,retain) NSNumber *tagTypeId;


@end
