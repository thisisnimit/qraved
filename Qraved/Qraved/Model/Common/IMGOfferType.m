//
//  IMGOfferType.m
//  Qraved
//
//  Created by Jeff on 8/21/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGOfferType.h"

@implementation IMGOfferType

@dynamic name;

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.offerId forKey:@"offerId"];
    [encoder encodeObject:self.typeId forKey:@"typeId"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.off forKey:@"off"];
}
- (id)initWithCoder:(NSCoder *)decoder
{
    if(self = [super init])
    {
        self.offerId = [decoder decodeObjectForKey:@"offerId"];
        self.typeId = [decoder decodeObjectForKey:@"typeId"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.off = [decoder decodeObjectForKey:@"off"];
    }
    return  self;
}
@end
