//
//  IMGFeature.h
//  Qraved
//
//  Created by Jeff on 8/21/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGEntity.h"

@interface IMGFeature : IMGEntity



@property(nonatomic,copy) NSString * title;
@property(nonatomic,strong) NSString *featureType;
@end
