//
//  IMGTagUpdate.h
//  Qraved
//
//  Created by lucky on 16/9/2.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGTagUpdate : IMGEntity

@property(nonatomic,strong) NSNumber *tagTypeId;
@property(nonatomic,strong) NSNumber *typeId;
@property(nonatomic,copy)   NSString *type;
@property(nonatomic,copy) NSString *updateDate;
@property(nonatomic,strong) NSNumber *rank;
@property(nonatomic,strong) NSNumber *visible;
@property(nonatomic,copy)   NSString *shows;
@end
