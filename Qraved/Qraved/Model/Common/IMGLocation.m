//
//  IMGLocation.m
//  Qraved
//
//  Created by Jeff on 8/6/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGLocation.h"
#import "IMGDistrict.h"
@implementation IMGLocation
-(void)setValue:(id)value forUndefinedKey:(NSString *)key{

}

-(void)setValuesForKeysWithDictionary:(NSDictionary *)keyedValues{
    for (NSString * key in [keyedValues allKeys]) {
        if([key isEqualToString:@"district"]){
            IMGDistrict * district =[[IMGDistrict alloc]init];
            [district setValuesForKeysWithDictionary:[keyedValues objectForKey:key]];
        }
    }
}
@end
