//
//  IMGExceptionAlertView.m
//  Qraved
//
//  Created by Olaf on 14/9/28.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGExceptionAlertView.h"

@implementation IMGExceptionAlertView
+(void)showExceptionAlertViewWithDetail:(NSString *)detail{
    IMGExceptionAlertView * alertView = [[IMGExceptionAlertView alloc]initWithTitle:@"Error" message:detail delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}
@end
