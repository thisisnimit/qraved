//
//  IMGEvent.h
//

//
//  Created by Jeff on 8/25/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGEntity.h"

@interface IMGEvent : IMGEntity
@property(nonatomic,retain) NSNumber *eventId;
@property(nonatomic,copy)   NSString *banner;
@property(nonatomic,copy)   NSString *contents;
@property(nonatomic,copy)   NSString *startDate;
@property(nonatomic,copy)   NSString *endDate;
@property(nonatomic,copy)   NSString *logo;
@property(nonatomic,copy)   NSString *name;
@property(nonatomic,copy)   NSString *seoUrl;
@property(nonatomic,copy)   NSString *smallBanner;
@property(nonatomic,copy)   NSString *appBanner;
@property(nonatomic,retain) NSNumber *bannerImageId;
@property(nonatomic,copy)   NSString *smallBanner4Mobile;
@property(nonatomic,retain) NSNumber *sortOrder;
@property(nonatomic,copy)   NSString *splash;
@property(nonatomic,retain) NSNumber *status;
@property(nonatomic,copy)   NSString *subTitle;
@property(nonatomic,copy)   NSString *targetUrl;
@property(nonatomic,copy)   NSString *targetUrlApp;

+(id)findById:(NSNumber*)eventId;

@end
