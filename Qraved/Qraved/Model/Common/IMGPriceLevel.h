//
//  IMGPriceLevel.h
//  Qraved
//
//  Created by Jeff on 8/21/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGEntity.h"

@interface IMGPriceLevel : IMGEntity

@property(nonatomic,retain) NSNumber * priceLevelId;
@property(nonatomic,copy)   NSString * dollors;
@property(nonatomic,copy)   NSString * descriptionStr;
@end
