//
//  FetchPrameter.m
//  Qraved
//
//  Created by Shine Wang on 7/26/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "FetchPrameter.h"

@implementation FetchPrameter
@synthesize paramters;

+(FetchPrameter *)sharedParameter
{
    static FetchPrameter *fetchPrameter=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        fetchPrameter=[[FetchPrameter alloc]init];
        
    });
    
    return fetchPrameter;
}


@end
