//
//  IMGSetting.h
//  Qraved
//
//  Created by Jeff on 10/15/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGSetting : IMGEntity

@property(nonatomic,retain) NSNumber *settingId;
@property(nonatomic,retain) NSNumber *storeId;
@property(nonatomic,retain) NSNumber *serialized;
@property (nonatomic,copy)  NSString *key;
@property (nonatomic,copy)  NSString *value;
@end
