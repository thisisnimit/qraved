//
//  City.h
//  Qraved
//
//  Created by Shine Wang on 7/25/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGCity : IMGEntity
@property (nonatomic,retain)      NSNumber *countryId;
@property (nonatomic,retain)      NSNumber *cityId;
@property (nonatomic,retain)      NSNumber *latitude;
@property (nonatomic,retain)      NSNumber *longitude;
@property (nonatomic,copy)      NSString *name;
@property (nonatomic,retain)    NSNumber *status;
@property (nonatomic,retain)      NSNumber *maxLatitude;
@property (nonatomic,retain)      NSNumber *maxLongitude;
@property (nonatomic,retain)      NSNumber *minLatitude;
@property (nonatomic,retain)      NSNumber *minLongitude;
+(id)findById:(NSNumber*)cityId;
@end
