//
//  IMGOfferType.h
//  Qraved
//
//  Created by Jeff on 8/21/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGEntity.h"

@interface IMGOfferType : IMGEntity


@property(nonatomic,retain) NSNumber *offerId;
@property(nonatomic,retain) NSNumber *typeId;
@property(nonatomic,retain) NSNumber *off;
@property(nonatomic,retain) NSNumber *eventType;
@property(nonatomic,copy)   NSString *name;

- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)decoder;
@end
