//
//  IMGDistrict.h
//  Qraved
//
//  Created by Laura on 14-9-19.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGDistrict : IMGEntity
@property(nonatomic,retain) NSNumber *districtId;
@property(nonatomic,retain) NSNumber *cityId;
@property(nonatomic,retain) NSNumber *popular;
@property (nonatomic,copy)  NSString *name;
@property(nonatomic,retain) NSNumber *latitude;
@property(nonatomic,retain) NSNumber *longitude;
@property(nonatomic,retain) NSNumber *restaurantCount;


@end
