//
//  IMGNotificationSetting.m
//  Qraved
//
//  Created by Olaf on 14/9/26.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGNotificationSetting.h"

@implementation IMGNotificationSetting
-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        self.notificationSettingId = value;
    }
}
@end
