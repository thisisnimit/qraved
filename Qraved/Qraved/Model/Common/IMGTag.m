//
//  IMGTag.m
//  Qraved
//
//  Created by Olaf on 14/9/26.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGTag.h"
#import "DBManager.h"

@implementation IMGTag
-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        self.tagId = value;
    }
}

+(id)findById:(NSNumber*)tagId{
    IMGTag *tag = [[IMGTag alloc] init];
    FMResultSet *resultSet = [[DBManager manager] executeQuery:[NSString stringWithFormat:@"select * from IMGTag where tagId=%@",tagId]];
    if([resultSet next]){
        [tag setValueWithResultSet:resultSet];
        [resultSet close];
        return tag;
    }else{
        [resultSet close];
        return nil;
    }
    
}
@end
