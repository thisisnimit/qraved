//
//  IMGLocation.h
//  Qraved
//
//  Created by Jeff on 8/6/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGEntity.h"
@class IMGDistrict;

@interface IMGLocation :IMGEntity

@property (nonatomic,copy)      NSString * address1;
@property (nonatomic,copy)      NSString * address2;
@property (nonatomic,copy)      NSString * city;
@property (nonatomic,copy)      NSString * country;
@property (nonatomic,retain)    NSNumber *locationId;
@property (nonatomic,copy)      NSString * latitude;
@property (nonatomic,copy)      NSString * longitude;
@property (nonatomic,copy)      NSString * province;
@property (nonatomic,copy)      NSString * region;
@property(nonatomic,retain)     NSNumber *districtId;
@property(nonatomic,copy)       NSString *districtTitle;


@end
