//
//  IMGSetting.m
//  Qraved
//
//  Created by Jeff on 10/15/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGSetting.h"

@implementation IMGSetting
-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        self.settingId = value;
    }
}
@end
