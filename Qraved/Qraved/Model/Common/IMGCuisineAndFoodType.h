//
//  IMGCuisineAndFoodType.h
//  Qraved
//
//  Created by Jeff on 1/21/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "IMGCuisine.h"

@interface IMGCuisineAndFoodType : IMGCuisine
@property(nonatomic,retain) NSNumber *entityType;//0 for cuisine, 1 for food type

@end
