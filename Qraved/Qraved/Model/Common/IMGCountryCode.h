//
//  IMGCountryCode.h
//  Qraved
//
//  Created by mac on 14-11-27.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGCountryCode : IMGEntity
@property(nonatomic,retain) NSNumber *code;
@end
