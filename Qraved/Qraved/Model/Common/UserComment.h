//
//  comment.h
//  Qraved
//
//  Created by Shine Wang on 8/13/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserComment : NSObject

@property(nonatomic,retain)NSString *objectID;
@property(nonatomic,retain)NSString *comment;
@property(nonatomic,retain)NSString *fullName;
@property(nonatomic,retain)NSString *reviewid;
@property(nonatomic,retain)NSString *createdTime;
@property(nonatomic,retain)NSString *userID;
@property(nonatomic,retain)NSString *avatarImageUrl;


//return commentArray
-(void)configWithJson:(id )json;
@end
