//
//  IMGExceptionAlertView.h
//  Qraved
//
//  Created by Olaf on 14/9/28.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IMGExceptionAlertView : UIAlertView
+(void)showExceptionAlertViewWithDetail:(NSString *)detail;
@end
