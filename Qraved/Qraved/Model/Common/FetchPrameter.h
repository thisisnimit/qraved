//
//  FetchPrameter.h
//  Qraved
//
//  Created by Shine Wang on 7/26/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FetchPrameter : NSObject

+(FetchPrameter *)sharedParameter;

@property(nonatomic,retain)NSDictionary *paramters;
@end
