//
//  IMGEntity.m
//  Qraved
//
//  Created by Jeff on 8/25/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@implementation IMGEntity

-(void)setValueWithResultSet:(FMResultSet *)resultSet{
    int columnNumber = resultSet.columnCount;
    for (int i=0; i<columnNumber; i++) {
        NSString *columnName = [resultSet columnNameForIndex:i];
        [self setValue:[resultSet objectForColumnIndex:i] forKey:columnName];
    }
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if([key isEqualToString:@"id"]){
        [self setValue:value forKey:@"ID"];
    }
    if([key isEqualToString:@"class"]){
        [self setValue:value forKey:@"class_name"];
    }
}
+(NSData *)toJSONData:(id)theData{
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:theData
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
    if ([jsonData length] > 0 && error == nil){
        return jsonData;
    }else{
        return nil;
    }
}
@end
