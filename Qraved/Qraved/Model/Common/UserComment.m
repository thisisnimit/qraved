//
//  comment.m
//  Qraved
//
//  Created by Shine Wang on 8/13/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "UserComment.h"

@implementation UserComment
@synthesize avatarImageUrl,comment,fullName,objectID,reviewid,createdTime,userID;

-(void)configWithJson:(id )jsonObject
{
    self.avatarImageUrl=[jsonObject objectForKey:@"avatar"];
    self.userID=[jsonObject objectForKey:@"userId"];
    self.fullName=[jsonObject objectForKey:@"fullName"];
    self.comment=[jsonObject objectForKey:@"comment"];
    self.createdTime=[jsonObject objectForKey:@"timeCreatedStr"];
}
@end
