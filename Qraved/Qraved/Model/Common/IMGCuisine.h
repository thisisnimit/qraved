//
//  IMGCuisine.h
//  Qraved
//
//  Created by Jeff on 8/21/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGCuisine:IMGEntity
@property (nonatomic,retain) NSNumber * cuisineId;
@property (nonatomic,copy)   NSString * name;
@property (nonatomic,copy)   NSString * imageUrl;
@property (nonatomic,retain) NSNumber * code;
@property (nonatomic,retain) NSNumber * restaurantCount;
@end
