//
//  CustomAnnotation.h
//  Qraved
//
//  Created by Shine Wang on 10/17/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "IMGRestaurant.h"


@interface CustomAnnotation : MKPointAnnotation
@property(nonatomic,retain)IMGRestaurant *restaurant;
@end
