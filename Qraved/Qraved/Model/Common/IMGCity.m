//
//  City.m
//  Qraved
//
//  Created by Shine Wang on 7/25/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "IMGCity.h"
#import "DBManager.h"

@implementation IMGCity

+(id)findById:(NSNumber*)cityId{
    IMGCity *city = [[IMGCity alloc] init];
    FMResultSet *resultSet = [[DBManager manager] executeQuery:[NSString stringWithFormat:@"select * from IMGCity where cityId=%@",cityId]];
    if([resultSet next]){
        [city setValueWithResultSet:resultSet];
        [resultSet close];
        return city;
    }else{
        [resultSet close];
        return nil;
    }
    
}
@end
