//
//  IMGCuisine.m
//  Qraved
//
//  Created by Jeff on 8/21/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGCuisine.h"

@implementation IMGCuisine
-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"id"]) {
        self.cuisineId=value;
    }
}
@end
