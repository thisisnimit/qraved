//
//  FetchPrameter.m
//  Qraved
//
//  Created by Jeff
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "PathPrameter.h"

@implementation PathPrameter
@synthesize paramters;

NSString *const PATH_SEARCH = @"search";
NSString *const PATH_NOTIFICATION_LIST = @"notification/list";
NSString *const PATH_NOTIFICATION_CLEAR = @"notification/status/clear";

+(PathPrameter *)sharedParameter
{
    static PathPrameter *pathPrameter=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        pathPrameter=[[PathPrameter alloc]init];
        
    });
    
    return pathPrameter;
}


@end
