//
//  IMGEvent.m
//  Qraved
//
//  Created by Jeff on 8/25/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGEvent.h"
#import "DBManager.h"

@implementation IMGEvent
-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        self.eventId = value;
    }
}

+(id)findById:(NSNumber*)eventId{
    IMGEvent *event = [[IMGEvent alloc] init];
    FMResultSet *resultSet = [[DBManager manager] executeQuery:[NSString stringWithFormat:@"select * from IMGEvent where eventId=%@",eventId]];
    if([resultSet next]){
        [event setValueWithResultSet:resultSet];
        [resultSet close];
        return event;
    }else{
        [resultSet close];
        return nil;
    }
    
}
@end
