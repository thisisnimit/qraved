//
//  IMGNotificationSetting.h
//  Qraved
//
//  Created by Olaf on 14/9/26.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGNotificationSetting : IMGEntity

@property (nonatomic,copy)  NSString *emailCancelReservationFlag;
@property (nonatomic,copy)  NSString *emailCommentPhotoFlag;
@property (nonatomic,copy)  NSString *emailFollowFlag;
@property (nonatomic,copy)  NSString *emailMakeReservationFlag;
@property (nonatomic,copy)  NSString *emailQravePhotoFlag;
@property (nonatomic,retain)NSNumber *notificationSettingId;
@property (nonatomic,copy)  NSString *lastUpdated;
@property (nonatomic,copy)  NSString *receiveNotifyFlag;
@property (nonatomic,copy)  NSString *receiveWeeklyFlag;
@property (nonatomic,copy)  NSString *shareQuaveFlag;
@property (nonatomic,copy)  NSString *shareReserveFlag;
@end
