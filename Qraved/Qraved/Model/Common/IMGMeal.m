//
//  IMGMeal.m
//  Qraved
//
//  Created by Jeff on 10/23/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGMeal.h"

@implementation IMGMeal
-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        self.mealId = value;
    }
}
@end
