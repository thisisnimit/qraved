//
//  IMGDistrict.m
//  Qraved
//
//  Created by Laura on 14-9-19.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGDistrict.h"

@implementation IMGDistrict

@dynamic name;

-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        self.districtId = value;
    }
}
@end
