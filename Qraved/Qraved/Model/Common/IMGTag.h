//
//  IMGTag.h
//  Qraved
//
//  Created by Olaf on 14/9/26.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

#define TAG_TYPE @"tags"

#define TAG_TYPE_FEATURES @"Features"
#define TAG_TYPE_OCCASION @"Occasion"
#define TAG_TYPE_LANDMARK @"Landmark/ Mall"

typedef NSString * IMGTagType;

@interface IMGTag : IMGEntity
@property(nonatomic,retain) NSNumber *tagId;
@property(nonatomic,retain) NSNumber *cityId;
@property(nonatomic,copy)   NSString *name;
@property(nonatomic,retain) NSNumber *ranking;
@property(nonatomic,retain) NSNumber *status;
@property(nonatomic,copy)   NSString *type;
@property(nonatomic,copy)   NSString *typeShows;
@property(nonatomic,retain) NSNumber *typeVisible;
@property(nonatomic,retain) NSNumber *tagTypeId;
@property(nonatomic,retain) NSNumber *restaurantCount;


+(id)findById:(NSNumber*)tagId;

@end
