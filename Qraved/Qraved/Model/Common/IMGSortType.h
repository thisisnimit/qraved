//
//  SortType.h
//  Qraved
//
//  Created by Jeff on 8/18/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGEntity.h"

@interface IMGSortType : IMGEntity
@property(nonatomic,retain) NSNumber *sortTypeId;
@property(nonatomic,copy)   NSString * name;

@end
