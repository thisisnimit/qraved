//
//  FetchPrameter.h
//  Qraved
//
//  Created by Jeff.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PathPrameter : NSObject

extern NSString *const PATH_SEARCH;
extern NSString *const PATH_NOTIFICATION_LIST;
extern NSString *const PATH_NOTIFICATION_CLEAR;

+(PathPrameter *)sharedParameter;

@property(nonatomic,retain)NSDictionary *paramters;
@end
