//
//  IMGMeal.h
//  Qraved
//
//  Created by Jeff on 10/23/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGMeal : IMGEntity
@property(nonatomic,retain) NSNumber *mealId;
@property(nonatomic,copy)   NSString *fromTime;
@property(nonatomic,copy)   NSString *toTime;
@property(nonatomic,copy)   NSString *mealName;
@property(nonatomic,copy)   NSString *sortOrder;
@end
