//
//  personalTypeFourModel.h
//  Qraved
//
//  Created by Adam.zhang on 2017/6/28.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IMGJourneySave : NSObject

@property (nonatomic, strong)  NSNumber  *savedTime;
@property (nonatomic, strong)  NSNumber  *restaurantId;
@property (nonatomic, strong)  NSString  *restaurantTitle;
@property (nonatomic, strong)  NSString  *restaurantCuisine;
@property (nonatomic, strong)  NSString  *restaurantDistrict;
@property (nonatomic, strong)  NSString  *restaurantCityName;
@property (nonatomic, strong)  NSNumber  *restaurantPriceLevel;
@property (nonatomic, strong)  NSString  *restaurantSeoKeywords;
@property (nonatomic, strong)  NSString  *listName;
@property (nonatomic, strong)  NSNumber  *userId;
@property (nonatomic, strong)  NSString  *userFullName;
@property (nonatomic, strong)  NSString  *userAvatar;
@property (nonatomic, strong)  NSString  *restaurantDefaultImageUrl;





@end
