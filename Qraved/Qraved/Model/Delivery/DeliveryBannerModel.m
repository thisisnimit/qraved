//
//  DeliveryBannerModel.m
//  Qraved
//
//  Created by harry on 2018/3/1.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "DeliveryBannerModel.h"

@implementation DeliveryBannerModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"id"]) {
        self.bannerId = value;
    }
}

@end
