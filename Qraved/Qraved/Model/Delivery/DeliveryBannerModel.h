//
//  DeliveryBannerModel.h
//  Qraved
//
//  Created by harry on 2018/3/1.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeliveryBannerModel : NSObject

@property (nonatomic, copy) NSNumber *bannerId;
@property (nonatomic, copy) NSNumber *object_id;
@property (nonatomic, copy) NSString *image_url;
@property (nonatomic, copy) NSString *object_type;
@property (nonatomic, copy) NSString *object_value;
@property (nonatomic, copy) NSString *sub_title;
@property (nonatomic, copy) NSString *title;

@end
