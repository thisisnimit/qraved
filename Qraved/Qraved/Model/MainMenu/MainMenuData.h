//
//  MainMenuData.h
//  Qraved
//
//  Created by Admin on 9/22/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MainMenuData : NSObject
@property(nonatomic, retain) NSString *title;
@property(nonatomic, retain) NSString *iconName;
@property(nonatomic, retain) NSString *selectedIconName;
@property(nonatomic, retain) NSArray *dataArray;

-(id)initWithTitle:(NSString*)title withData:(NSArray*)dataArr;

-(instancetype)initWithTitle:(NSString*)title withIcon:(NSString*)icon selectedIcon:(NSString*)selectedIcon;
@end
