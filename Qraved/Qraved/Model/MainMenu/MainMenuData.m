//
//  MainMenuData.m
//  Qraved
//
//  Created by Admin on 9/22/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "MainMenuData.h"

@implementation MainMenuData
-(id)initWithTitle:(NSString*)title withData:(NSArray*)dataArr{
    self = [super init];
    if (self) {
        self.title = title;
        self.dataArray = dataArr;
    }
    return self;
}

-(instancetype)initWithTitle:(NSString*)title withIcon:(NSString*)icon selectedIcon:(NSString*)selectedIcon{

	self=[super init];
	if(self){
		self.title=title;
		self.iconName=icon;
		self.selectedIconName=selectedIcon;		
	}
	return self;
}

@end
