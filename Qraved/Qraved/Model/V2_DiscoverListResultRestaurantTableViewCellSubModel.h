//
//  V2_DiscoverListResultRestaurantTableViewCellSubModel.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/24.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface V2_DiscoverListResultRestaurantTableViewCellSubModel : NSObject

@property (nonatomic, strong)  NSString  *distance;
@property (nonatomic, strong)  NSString  *districtName;
@property (nonatomic, strong)  NSNumber  *myID;
@property (nonatomic, strong)  NSString  *img;
@property (nonatomic, strong)  NSNumber  *isTrending;
@property (nonatomic, strong)  NSString  *title;
@property (nonatomic, strong)  NSString  *type;
@property (nonatomic, strong)  NSNumber  *hasVideo;


@end
