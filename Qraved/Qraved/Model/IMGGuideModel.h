//
//  IMGGuideModel.h
//  Qraved
//
//  Created by Gary.yao on 2017/6/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IMGGuideModel : NSObject

@property (nonatomic,strong) NSString * name;
@property (nonatomic,strong) NSString * coverImage;
@property (nonatomic,strong) NSString * seoKeyword;
@property (nonatomic,strong) NSNumber * myID;
@property (nonatomic,strong) NSNumber * listType;
@property (nonatomic,strong) NSNumber * status;
@end
