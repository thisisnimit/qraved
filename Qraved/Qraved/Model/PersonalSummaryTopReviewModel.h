//
//  PersonalSummaryTopReviewModel.h
//  Qraved
//
//  Created by Adam.zhang on 2017/6/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersonalSummaryTopReviewModel : NSObject

@property (nonatomic, strong)  NSNumber  *ambianceScore;
@property (nonatomic, strong)  NSString  *myClass;
@property (nonatomic, strong)  NSNumber  *commentCount;
@property (nonatomic, strong)  NSArray  *commentList;
@property (nonatomic, strong)  NSString  *createTimeStr;
@property (nonatomic, strong)  NSArray  *dishList;
@property (nonatomic, strong)  NSNumber  *followingUser;
@property (nonatomic, strong)  NSNumber  *foodScore;
@property (nonatomic, strong)  NSNumber  *helpful;
@property (nonatomic, strong)  NSNumber  *helpfulCount;
@property (nonatomic, strong)  NSNumber  *myId;
@property (nonatomic, strong)  NSString  *imageUrl;
@property (nonatomic, assign)  BOOL isLike;
@property (nonatomic, strong)  NSNumber  *likeCount;
@property (nonatomic, strong)  NSDictionary  *mini1Restaurant;
@property (nonatomic, strong)  NSString  *location;
@property (nonatomic, strong)  NSNumber  *qravesCount;
@property (nonatomic, strong)  NSNumber  *reviewCount;
@property (nonatomic, strong)  NSNumber  *score;
@property (nonatomic, strong)  NSNumber  *serviceScore;
@property (nonatomic, strong)  NSNumber  *siblingCountByUser;
@property (nonatomic, strong)  NSString  *sourceSite;
@property (nonatomic, strong)  NSString  *summarize;
@property (nonatomic, strong)  NSNumber  *targetId;
@property (nonatomic, strong)  NSString  *targetTitle;
@property (nonatomic, strong)  NSNumber  *timeCreated;
@property (nonatomic, strong)  NSString  *title;
@property (nonatomic, strong)  NSDictionary   *user;
@property (nonatomic, strong)  NSNumber  *userDishCount;
@property (nonatomic, strong)  NSNumber  *userDishInstagramCount;
@property (nonatomic, strong)  NSNumber  *userFollowerCount;
@property (nonatomic, strong)  NSNumber  *userPhotoCount;
@property (nonatomic, strong)  NSNumber  *userReviewCount;
@property (nonatomic, strong)  NSString  *userSeoKeyword;

@property (nonatomic,assign) BOOL isReadMore;

@end


@interface mini1RestaurantModel : NSObject

@property (nonatomic, strong)  NSString  *myClass;
@property (nonatomic, strong)  NSNumber  *code;
@property (nonatomic, strong)  NSString  *myDescription;
@property (nonatomic, strong)  NSNumber  *myId;
@property (nonatomic, strong)  NSString  *imageUrl;
@property (nonatomic, strong)  NSString  *name;
@property (nonatomic, strong)  NSString  *primary;
@property (nonatomic, strong)  NSString  *restaurantCount;
@property (nonatomic, strong)  NSString  *seoKeyword;



@end



@interface locationModel : NSObject

@property (nonatomic, strong)  NSNumber  *areaId;
@property (nonatomic, strong)  NSString  *areaName;
@property (nonatomic, strong)  NSString  *city;
@property (nonatomic, strong)  NSNumber  *cityId;
@property (nonatomic, strong)  NSString  *cityName;
@property (nonatomic, strong)  NSString  *myClass;
@property (nonatomic, strong)  NSNumber  *code;
@property (nonatomic, strong)  NSNumber  *myId;
@property (nonatomic, strong)  NSNumber  *latitude;
@property (nonatomic, strong)  NSNumber  *longitude;
@property (nonatomic, strong)  NSString  *name;
@property (nonatomic, strong)  NSNumber  *popular;
@property (nonatomic, strong)  NSString  *postalCode;
@property (nonatomic, strong)  NSNumber  *restaurantCount;
@property (nonatomic, strong)  NSString  *seoKeyword;
@property (nonatomic, strong)  NSNumber  *sortOrder;


@end











