//
//  IMGLikeList.h
//  Qraved
//
//  Created by imaginato on 16/4/20.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGLikeList : IMGEntity
@property(nonatomic,copy)NSString* fullName;
@property(nonatomic,copy)NSString* imageUrl1;
@property(nonatomic,retain)NSNumber* photoCount;
@property(nonatomic,retain)NSNumber* reviewCount;
@property(nonatomic,retain)NSNumber* userId;
@property(nonatomic,retain)NSNumber* dishId;
@property(nonatomic,retain)NSNumber* reviewId;
@property(nonatomic,retain)NSNumber* restaurantEventId;
@property(nonatomic,retain)NSNumber* likeId;
@end
