//
//  ThirdPartUser.h
//  App
//
//  Created by jefftang on 1/11/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ThirdPartyUser : NSObject

@property (weak, nonatomic) NSString *userID ;
@property (weak, nonatomic) NSString *email ;
@property (weak, nonatomic) NSString *password  ;
@property (weak, nonatomic) NSString *name  ;
@property (weak, nonatomic) NSString *gender ;
@property (weak, nonatomic) NSString *userName ;
@property (weak, nonatomic) NSString *firstName ;
@property (weak, nonatomic) NSString *lastName ;
@property (weak, nonatomic) NSString *picture ;
@property (weak, nonatomic) NSString *birthday ;
@property (weak, nonatomic) NSString *mobileNumber ;
@property (weak, nonatomic) NSString *updatedTime ;

-(id)init;

@end
