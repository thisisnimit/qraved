//
//  TWitterUser.h
//  App
//
//  Created by jefftang on 1/11/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ThirdPartyUser.h"

@interface TWitterUser : ThirdPartyUser

@property (weak, nonatomic) NSString * screenName;

-(id)init;


@end
