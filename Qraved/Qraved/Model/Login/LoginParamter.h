//
//  LoginParamter.h
//  Qraved
//
//  Created by Shine Wang on 11/18/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginParamter : NSObject
+(LoginParamter *)sharedParameter;

@property(nonatomic,retain)NSMutableDictionary *paramters;
@property(nonatomic,retain)NSString *loginwith;
@end
