//
//  LoginParamter.m
//  Qraved
//
//  Created by Shine Wang on 11/18/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "LoginParamter.h"

@implementation LoginParamter
@synthesize paramters;
+(LoginParamter *)sharedParameter
{
    static LoginParamter *loginPrameter=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        loginPrameter=[[LoginParamter alloc]init];
        
    });
    
    return loginPrameter;
}
@end
