//
//  ThirdPartUser.m
//  App
//
//  Created by jefftang on 1/11/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "ThirdPartyUser.h"

@implementation ThirdPartyUser

-(id)init {
    self.userID = @"id";
    self.email = @"email";
    self.password = @"password";
    self.name = @"name";
    self.gender = @"gender";
    self.userName = @"username";
    self.firstName = @"first_name";
    self.lastName = @"last_name";
    self.picture = @"picture";
    self.birthday = @"birthday";
    self.mobileNumber = @"mobile_number";
    self.updatedTime = @"updated_time";
    return self;
}

@end
