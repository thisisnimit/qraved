//
//  FBUser.m
//  App
//
//  Created by jefftang on 12/18/12.
//  Copyright (c) 2012 Imaginato. All rights reserved.
//

#import "FBUser.h"

@implementation FBUser

-(id)init {
    self = [super init];
    self.userID = @"id";
    self.email = @"email";
    self.password = @"password";
    self.name = @"name";
    self.gender = @"gender";
    self.userName = @"username";
    self.firstName = @"first_name";
    self.lastName = @"last_name";
    self.picture = @"picture";
    self.birthday = @"birthday";
    self.mobileNumber = @"mobile_number";
    self.updatedTime = @"updated_time";
    return self;

}

@end
