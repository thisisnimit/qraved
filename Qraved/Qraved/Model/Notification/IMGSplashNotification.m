//
//  IMGSplashNotification.m
//  Qraved
//
//  Created by Lucky on 15/11/9.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import "IMGSplashNotification.h"

@implementation IMGSplashNotification

@dynamic  splashId;
@dynamic contentTitle;
@dynamic splashDescription;
@dynamic image;
@dynamic button;
@dynamic buttonValue;
@dynamic buttonType;
@dynamic buttonUrl;



- (void)setValue:(id)value forKey:(NSString *)key
{
    if([key isEqualToString:@"id"]){
        [self setValue:value forKey:@"splashId"];
    }

    if([key isEqualToString:@"description"]){
        [self setValue:value forKey:@"splashDescription"];
    }
}
@end
