//
//  IMGNotifications.h
//  Qraved
//
//  Created by Lucky on 15/11/9.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGNotifications : IMGEntity

@property(nonatomic,retain) NSString*icon;
@property(nonatomic,retain) NSString*message;
@property(nonatomic,retain) NSNumber *subtype;
@property(nonatomic,retain) NSNumber *type;

@property(nonatomic,retain) NSNumber *notificationId;
@property(nonatomic,retain) NSNumber *createTime;
@property(nonatomic,assign) BOOL isRead;

//splash    2
@property (nonatomic,retain)   NSNumber *splashId;
@property (nonatomic,retain)   NSString *contentTitle;
@property (nonatomic,retain)   NSString *splashDescription;
@property (nonatomic,retain)   NSString *image;
@property (nonatomic,retain)   NSString *button;
@property (nonatomic,retain)   NSNumber *buttonValue;
@property (nonatomic,retain)   NSString *buttonType;
@property (nonatomic,retain)   NSString *buttonUrl;

//landingPage   1
@property (nonatomic,retain)   NSString *landingPageUrl;
@property (nonatomic,retain)   NSString *content;

//restaurant    4
@property (nonatomic,retain)   NSNumber *restaurantId;
@property (nonatomic,retain)   NSString *restaurantName;
@property (nonatomic,retain)   NSNumber *userId;
@property (nonatomic,retain)   NSString *restaurantImageUrl;
@property (nonatomic,retain)   NSString *userName;

//book      3
@property (nonatomic,retain)   NSNumber *reservationId;
@property (nonatomic,retain)   NSString *bookDate;
@property (nonatomic,retain)   NSString *bookTime;

//review    5
@property (nonatomic,retain)   NSString *commentContext;
@property (nonatomic,retain)   NSString *userAvatar;
@property (nonatomic,retain)   NSNumber *landingObjectId;
@property (nonatomic,retain)   NSNumber *guestUserId;
@property (nonatomic,retain)   NSString *reviewUserName;
@property (nonatomic,retain)   NSString *guestUserName;
@property (nonatomic,retain)   NSNumber *reviewId;
@property (nonatomic,retain)   NSNumber *reviewUserId;
@property (nonatomic,retain)   NSString *reviewContext;
@property (nonatomic,retain)   NSNumber *userType;
@property (nonatomic,assign)   BOOL isVendor;

//dish      6
@property (nonatomic,copy)     NSString *dishUserName;
@property (nonatomic,retain)   NSNumber *dishUserId;
@property (nonatomic,retain)   NSMutableArray *dishList;
//mutibul dish 7
@property (nonatomic,copy)   NSString *comment;
@property (nonatomic,retain) NSNumber *hostUserId;
@property (nonatomic,copy)   NSString *hostUserName;
@property (nonatomic,retain) NSNumber *moderateReviewId;
@property (nonatomic,copy)   NSString *guestUserAvatar;
@property (nonatomic,copy)   NSString *guestIsVendor;
@property (nonatomic,copy)   NSString *hostIsVendor;

@property (nonatomic, copy) NSDictionary *couponSaveInfo;

@end
