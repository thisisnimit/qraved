//
//  IMGNotification.h
//  Qraved
//
//  Created by Laura on 14/12/5.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGNotification : IMGEntity
//@property (nonatomic, copy) NSString * imageUrl;
@property(nonatomic,retain) NSString*createTime;
@property(nonatomic,retain) NSString*deviceToken;
@property(nonatomic,retain) NSNumber *notificationId;
@property(nonatomic,retain) NSString*lastUpdatedStr;
@property(nonatomic,retain) NSString*message;
@property(nonatomic,retain) NSNumber *objectId1;
@property(nonatomic,retain) NSNumber *objectId2;
@property(nonatomic,retain) NSNumber *objectId3;
@property(nonatomic,retain) NSNumber *objectId4;
@property(nonatomic,retain) NSNumber *objectId5;
@property(nonatomic,retain) NSString*objectName1;
@property(nonatomic,retain) NSString*objectName2;
@property(nonatomic,retain) NSString*objectName3;
@property(nonatomic,retain) NSString*objectName4;
@property(nonatomic,retain) NSString*objectName5;
@property(nonatomic,retain) NSString*objectSeo1;
@property(nonatomic,retain) NSString*objectSeo2;
@property(nonatomic,retain) NSString*objectSeo3;
@property(nonatomic,retain) NSString*objectSeo4;
@property(nonatomic,retain) NSString*objectSeo5;
@property(nonatomic,retain) NSNumber *pushedStatus;
@property(nonatomic,retain) NSNumber *readStatus;
@property(nonatomic,retain) NSNumber *type;
@property(nonatomic,retain) NSNumber *userId;
@property(nonatomic,retain) NSString*who;
@end
