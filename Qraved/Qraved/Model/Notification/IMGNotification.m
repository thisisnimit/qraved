//
//  IMGNotification.m
//  Qraved
//
//  Created by Laura on 14/12/5.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGNotification.h"

@implementation IMGNotification
-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        self.notificationId = value;
    }
}
@end
