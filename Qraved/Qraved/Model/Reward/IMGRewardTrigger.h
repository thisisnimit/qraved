//
//  Qraved
//
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGRewardTrigger : IMGEntity

@property(nonatomic, retain) NSNumber *rewardTriggerId;
@property(nonatomic, retain) NSNumber *points;
@property(nonatomic, retain) NSNumber *rewardValue;

@end
