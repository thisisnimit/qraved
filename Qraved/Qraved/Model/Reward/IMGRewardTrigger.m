//
//  Qraved
//
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGRewardTrigger.h"
#import "Tools.h"
#import "NSDate+Helper.h"

@implementation IMGRewardTrigger

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"id"]) {
        self.rewardTriggerId = value;
    }
}

@end
