//
//  UserPoints.h
//  Qraved
//
//  Created by Shine Wang on 1/26/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGPointReward.h"

@interface IMGUserPoints : NSObject
@property(nonatomic,retain) NSNumber *redeemablePoints;
@property(nonatomic,retain) NSNumber *pendingPoints;
@property(nonatomic,retain) NSNumber *usedPoints;
@property(nonatomic,retain) NSNumber *accumulativePoints;
@property(nonatomic,retain) NSNumber *rewardSetting;
@property(nonatomic,retain) NSArray *rewardHistory;
@end
