//
//  PointReward.m
//  Qraved
//
//  Created by Shine Wang on 1/26/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGPointReward.h"
#import "Tools.h"
#import "NSDate+Helper.h"

@implementation IMGPointReward

-(void)configWithJson:(id)json
{
    if(json)
    {
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        if(![Tools isBlankString:[json objectForKey:@"dateCreated"]])
        {
            self.dateCreated=[dateFormatter dateFromString:[[[json objectForKey:@"dateCreated"] stringByReplacingOccurrencesOfString:@"T" withString:@" "]stringByReplacingOccurrencesOfString:@"Z" withString:@""]];
        }
        if(![Tools isBlankString:[json objectForKey:@"voucherCode"]])
        {
            self.voucherCode=[json objectForKey:@"voucherCode"];
        }
        
        if([json objectForKey:@"value"] !=[NSNull null])
        {
            self.rewardValue=[json objectForKey:@"value"];
        }
        
        if(![Tools isBlankString:[json objectForKey:@"expiryDate"]])
        {
            self.expiryDate=[dateFormatter dateFromString:[[[json objectForKey:@"expiryDate"] stringByReplacingOccurrencesOfString:@"T" withString:@" "]stringByReplacingOccurrencesOfString:@"Z" withString:@""]];
        }
        if(![Tools isBlankString:[json objectForKey:@"usedDate"]])
        {
            self.usedDate=[dateFormatter dateFromString: [[[json objectForKey:@"usedDate"] stringByReplacingOccurrencesOfString:@"T" withString:@" "]stringByReplacingOccurrencesOfString:@"Z" withString:@""]];
        }
        
        if(![Tools isBlankString:[json objectForKey:@"reservationDate"]])
        {
            self.reservationDate=[dateFormatter dateFromString: [[[json objectForKey:@"reservationDate"] stringByReplacingOccurrencesOfString:@"T" withString:@" "]stringByReplacingOccurrencesOfString:@"Z" withString:@""]];
        }
        
        if([json objectForKey:@"pointUsed"] !=[NSNull null])
        {
            self.pointUsed=[json objectForKey:@"pointUsed"];
        }
    }
}

-(NSString *)getDateCreatedString
{
    return [self dateToString:self.dateCreated];

}
-(NSString *)getReservationDateString
{
    return [self dateToString:self.reservationDate];
}
-(NSString *)getUsedDateString
{
    return [self dateToString:self.usedDate];

}
-(NSString *)getexpiryDateString
{
    return [self dateToString:self.expiryDate];

}

-(NSString *)dateToString:(NSDate *)date
{
    if(date && [date isKindOfClass:[NSDate class]])
    {
        return [NSString stringWithFormat:@"%@",[date getUserPointsTimeFormat]];
    }
    return @"";
}

@end
