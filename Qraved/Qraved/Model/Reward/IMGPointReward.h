//
//  PointReward.h
//  Qraved
//
//  Created by Shine Wang on 1/26/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGPointReward : IMGEntity
@property(nonatomic, retain) NSDate *dateCreated;
@property(nonatomic, retain) NSDate *expiryDate;
@property(nonatomic, retain) NSDate *usedDate;
@property(nonatomic, retain) NSDate *reservationDate;
@property(nonatomic, retain) NSString *voucherCode;
@property(nonatomic, retain) NSNumber *rewardValue;
@property(nonatomic, retain) NSNumber *pointUsed;

-(void)configWithJson:(id)json;

-(NSString *)getDateCreatedString;
-(NSString *)getReservationDateString;
-(NSString *)getUsedDateString;
-(NSString *)getexpiryDateString;
@end
