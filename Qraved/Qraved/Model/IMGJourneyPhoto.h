//
//  personalTypeThreeModel.h
//  Qraved
//
//  Created by Adam.zhang on 2017/6/28.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IMGJourneyPhoto : NSObject

@property (nonatomic, strong)  NSNumber  *uploadedPhotoId;
@property (nonatomic, strong)  NSNumber  *uploadedPhotoTimeCreated;
@property (nonatomic, strong)  NSNumber  *restaurantId;
@property (nonatomic, strong)  NSString  *restaurantTitle;
@property (nonatomic, strong)  NSString  *restaurantCuisine;
@property (nonatomic, strong)  NSString  *restaurantDistrict;
@property (nonatomic, strong)  NSString  *restaurantCityName;
@property (nonatomic, strong)  NSNumber  *restaurantPriceLevel;
@property (nonatomic, strong)  NSString  *restaurantSeoKeywords;
@property (nonatomic, strong)  NSArray  *dishList;
@property (nonatomic, strong)  NSNumber  *userId;
@property (nonatomic, strong)  NSString  *userFullName;
@property (nonatomic, strong)  NSString  *userAvatar;

@property (nonatomic, strong)  NSString  *restaurantImageUrl;





@end
