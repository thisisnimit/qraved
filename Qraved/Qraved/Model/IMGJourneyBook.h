//
//  personalTypeTwoModel.h
//  Qraved
//
//  Created by Adam.zhang on 2017/6/28.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IMGJourneyBook : NSObject

@property (nonatomic, strong)  NSNumber  *myID;
@property (nonatomic, strong)  NSString  *bookDate;
@property (nonatomic, strong)  NSString  *bookTime;
@property (nonatomic, strong)  NSNumber  *bookPax;
@property (nonatomic, strong)  NSString  *status;
@property (nonatomic, strong)  NSString  *voucherCode;
@property (nonatomic, strong)  NSString  *code;
@property (nonatomic, strong)  NSString  *restaurantPhone;
@property (nonatomic, strong)  NSNumber  *offerId;
@property (nonatomic, strong)  NSNumber  *offerType;
@property (nonatomic, strong)  NSString  *offerName;
@property (nonatomic, strong)  NSNumber  *off;
@property (nonatomic, strong)  NSNumber  *restaurantId;
@property (nonatomic, strong)  NSString  *restaurantTitle;
@property (nonatomic, strong)  NSString  *restaurantImage;
@property (nonatomic, strong)  NSString  *address;
@property (nonatomic, strong)  NSNumber  *restaurantLatitude;
@property (nonatomic, strong)  NSNumber  *restaurantLongitude;
@property (nonatomic, strong)  NSString  *restaurantSeoKeywords;
@property (nonatomic, strong)  NSString  *dateCreated;
@property (nonatomic, strong)  NSString  *restaurantCuisine;
@property (nonatomic, strong)  NSString  *restaurantDistrict;
@property (nonatomic, strong)  NSString  *restaurantCityName;
@property (nonatomic, strong)  NSNumber  *restaurantPriceLevel;
@property (nonatomic, strong)  NSNumber  *isOutOfDate;


@property (nonatomic, strong)  NSNumber  *fettle;





@end
