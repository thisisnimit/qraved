//
//  IMGSummary.h
//  Qraved
//
//  Created by Gary.yao on 2017/6/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGPhotoModel.h"
#import "CouponModel.h"
#import "PersonalSummaryTopReviewModel.h"
@interface IMGSummary : NSObject

@property (nonatomic,strong) NSNumber * ratingCount;
@property (nonatomic,strong) NSNumber * ratingSum;
@property (nonatomic,strong) NSArray * ratings;
@property (nonatomic,strong) NSArray * cuisineReviewList;
@property (nonatomic,strong) NSArray * districtReviewList;
@property (nonatomic,strong) NSArray<IMGPhotoModel*> * topPhotoList;
@property (nonatomic,strong) NSArray<CouponModel*> *couponList;
@property (nonatomic,strong) NSDictionary *topReview;
@property (nonatomic,strong) NSArray<PersonalSummaryTopReviewModel *> *topReviews;

@end
