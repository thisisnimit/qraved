//
//  IMGJourney.h
//  Qraved
//
//  Created by lucky on 16/9/9.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGJourney : IMGEntity

@property (nonatomic,strong)    NSNumber *type;

@property(nonatomic,strong) NSNumber *restaurantId;
@property(nonatomic,strong) NSNumber *restaurantPriceLevel;
@property(nonatomic,copy)   NSString *restaurantCuisine;
@property(nonatomic,copy)   NSString *restaurantDistrict;
@property(nonatomic,copy)   NSString *restaurantCityName;
@property(nonatomic,copy)   NSString *restaurantTitle;
@property(nonatomic,copy)   NSString *restaurantSeoKeywords;
@property(nonatomic,copy)   NSString *restaurantImage;

@end
