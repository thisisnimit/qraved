//
//  IMGJNReservation.h
//  Qraved
//
//  Created by lucky on 16/9/9.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "IMGJourney.h"

@interface IMGJNReservation : IMGJourney

@property(nonatomic,strong) NSNumber *reservationId;
@property(nonatomic,strong) NSNumber *status;
@property(nonatomic,strong) NSNumber *offerId;
@property(nonatomic,strong) NSNumber *offerType;
@property(nonatomic,strong) NSNumber *off;
@property(nonatomic,strong) NSNumber *restaurantLatitude;
@property(nonatomic,strong) NSNumber *restaurantLongitude;
@property(nonatomic,strong) NSNumber* restaurantPhone;

@property(nonatomic,copy)   NSString *bookDate;
@property(nonatomic,copy)   NSString *bookTime;
@property(nonatomic,strong)   NSNumber *bookPax;
@property(nonatomic,copy)   NSString *voucherCode;
@property(nonatomic,copy)   NSString *code;
@property(nonatomic,copy)   NSString *offerName;
@property(nonatomic,copy)   NSString *address;
@property(nonatomic,copy)   NSString *dateCreated;

@property(nonatomic,assign) BOOL isOutOfDate;
@property(nonatomic,assign) BOOL isother;


@end
