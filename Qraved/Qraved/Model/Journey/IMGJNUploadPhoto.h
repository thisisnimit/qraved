//
//  IMGJNUploadPhoto.h
//  Qraved
//
//  Created by lucky on 16/9/9.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "IMGJourney.h"

@interface IMGJNUploadPhoto : IMGJourney

@property(nonatomic,strong) NSNumber *uploadedPhotoId;
@property(nonatomic,copy)   NSString *uploadedPhotoTimeCreated;
@property(nonatomic,assign)BOOL isother;
@property(nonatomic,strong) NSArray *dishArr;

@end
