//
//  IMGJNReview.h
//  Qraved
//
//  Created by lucky on 16/9/9.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "IMGJourney.h"

@interface IMGJNReview : IMGJourney

@property(nonatomic,strong) NSNumber *reviewId;
@property(nonatomic,strong) NSNumber *reviewScore;
@property(nonatomic,strong) NSNumber *userId;

@property(nonatomic,copy)   NSString *reviewTimeCreated;
@property(nonatomic,copy)   NSString *reviewTitle;
@property(nonatomic,copy)   NSString *reviewSummarize;
@property(nonatomic,copy)   NSString *userFullName;
@property(nonatomic,copy)   NSString *userAvatar;

@property(nonatomic,strong) NSArray *dishArr;
@property(nonatomic,assign)BOOL isother;

@end
