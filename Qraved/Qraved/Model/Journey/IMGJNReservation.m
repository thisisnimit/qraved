//
//  IMGJNReservation.m
//  Qraved
//
//  Created by lucky on 16/9/9.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "IMGJNReservation.h"

@implementation IMGJNReservation

-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        self.reservationId = value;
    }
    
    if ([key isEqualToString:@"isOutOfDate"]) {
        self.isOutOfDate = [value boolValue];
    }
}


@end
