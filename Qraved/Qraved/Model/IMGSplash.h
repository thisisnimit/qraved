//
//  IMGSplash.h
//  Qraved
//
//  Created by Lucky on 15/11/4.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGSplash : IMGEntity


@property (nonatomic,retain)   NSNumber *splashId;
@property (nonatomic,retain)   NSString *contentTitle;
@property (nonatomic,retain)   NSString *splashDescription;
@property (nonatomic,retain)   NSString *image;
@property (nonatomic,retain)   NSString *button;
@property (nonatomic,retain)   NSNumber *buttonValue;
@property (nonatomic,retain)   NSString *buttonType;
@property (nonatomic,retain)   NSString *buttonUrl;


@end
