//
//  V2_DiscoverListResultRestaurantTableViewCellModel.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "V2_DiscoverListResultRestaurantTableViewCellSubModel.h"

@interface V2_DiscoverListResultRestaurantTableViewCellModel : NSObject

@property (nonatomic, strong)  NSArray<V2_DiscoverListResultRestaurantTableViewCellSubModel *>  *data;

@end
