//
//  IMGListModel.h
//  Qraved
//
//  Created by Gary.yao on 2017/6/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IMGListModel : NSObject

@property (nonatomic,strong) NSString * occupation;
@property (nonatomic,strong) NSNumber * isCelebrity;
@property (nonatomic,strong) NSNumber * guideCount;
@property (nonatomic,strong) NSArray * guideList;

@end
