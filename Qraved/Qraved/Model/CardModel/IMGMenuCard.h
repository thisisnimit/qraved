//
//  IMGMenuCard.h
//  Qraved
//
//  Created by imaginato on 16/6/1.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGRestaurant.h"

@interface IMGMenuCard : NSObject

@property (nonatomic,strong) IMGRestaurant *restaurant;
@property (nonatomic,strong) NSMutableArray *menuArr;

@end
