//
//  IMGReviewCard.h
//  Qraved
//
//  Created by imaginato on 16/6/1.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGRestaurant.h"
#import "IMGReview.h"

@interface IMGReviewCard : NSObject

@property (nonatomic,strong) IMGRestaurant *reviewCardRestaurant;
@property (nonatomic,strong) IMGReview *reviewCardReview;
@property (nonatomic,strong) NSMutableArray *dishArrM;
@property (nonatomic,strong) NSMutableDictionary *reveiwCardIsReadMore;

@property (nonatomic,assign) BOOL isReadMore;


@end
