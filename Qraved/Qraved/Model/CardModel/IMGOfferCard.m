//
//  IMGOfferCard.m
//  Qraved
//
//  Created by josn.liu on 2016/12/6.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "IMGOfferCard.h"
#import "IMGDishComment.h"
@implementation IMGOfferCard
+(NSMutableArray*)commentListFromArray:(NSArray*)commentArray andIsVendor:(BOOL)isVendor andRestaurant:(IMGOfferCard*)restaurant
{
    
    
    NSMutableArray* commentList = [[NSMutableArray alloc]initWithCapacity:0];
    for(int i=0;commentArray!=nil && [commentArray isKindOfClass:[NSArray class]] && i<commentArray.count;i++){
        if([[commentArray objectAtIndex:i] isKindOfClass:[IMGDishComment class]]){
            [commentList addObject:[commentArray objectAtIndex:i]];
        }else{
            NSDictionary *commentDictionary = [commentArray objectAtIndex:i];
            IMGDishComment *dishComment = [[IMGDishComment alloc]init];
            NSString *commentIdStr = [commentDictionary objectForKey:@"id"];
            if(commentIdStr){
                dishComment.commentId=[NSNumber numberWithInteger:[commentIdStr integerValue]];
                dishComment.content=[commentDictionary objectForKey:@"comment"];
                NSString *createTime = [commentDictionary objectForKey:@"timeCreated"];
                if(createTime){
                    dishComment.createTime=[NSNumber numberWithLongLong:[createTime longLongValue]];
                }
                dishComment.timeCreatedStr = [commentDictionary objectForKey:@"timeCreatedStr"];
                dishComment.restaurantId = restaurant.restaurantId;
                dishComment.restaurantName = restaurant.title;
                dishComment.restaurantImageUrl = restaurant.imageUrl;
                dishComment.userType = [commentDictionary objectForKey:@"userType"];
                dishComment.isVendor = isVendor;
                NSString *commentUserId =[commentDictionary objectForKey:@"userId"];
                if(commentUserId){
                    dishComment.userId=[NSNumber numberWithInteger:[commentUserId integerValue]];
                }
                dishComment.userImageUrl=[commentDictionary objectForKey:@"avatar"];
                dishComment.userName=[commentDictionary objectForKey:@"fullName"];
                [commentList addObject:dishComment];
            }
        }
        
    }
    return commentList;

    
    
}

@end
