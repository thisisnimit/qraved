//
//  IMGOfferCard.h
//  Qraved
//
//  Created by josn.liu on 2016/12/6.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "IMGEvent.h"

@interface IMGOfferCard : IMGEvent
@property(nonatomic,retain)NSString *restaurantTitle;
@property(nonatomic,retain)NSNumber *rating;
@property (nonatomic,strong) NSNumber *timelineId;
@property (nonatomic,strong) NSNumber *timeline;
@property(nonatomic,assign) BOOL isSaved;
@property (nonatomic,retain) NSNumber *likeCount;
@property (nonatomic,retain) NSNumber *commentCount;
@property (nonatomic,assign) BOOL isLike;
@property (nonatomic,assign) BOOL isVendor;
@property (nonatomic,strong) NSMutableArray *commentCardList;
@property(nonatomic,copy)NSString *landMark;
@property (nonatomic,strong) NSNumber *restaurantId;
@property(nonatomic,retain)NSNumber *offerId;
@property(nonatomic,copy)NSString *offerType;
@property(nonatomic,copy)NSString *offerTitle;
@property(nonatomic,copy)NSString *offerImageUrl;
@property(nonatomic,retain)NSNumber *offerImageHeight;
@property(nonatomic,retain)NSNumber *offerImageWidth;
@property(nonatomic,copy)NSString *restaurantSeo;
@property(nonatomic,retain)NSNumber *restaurantUpdateId;
@property(nonatomic,copy)NSString *restaurantImage;
@property(nonatomic,copy)NSString *cuisineName;



+(NSMutableArray*)commentListFromArray:(NSArray*)commentArray andIsVendor:(BOOL)isVendor andRestaurant:(IMGOfferCard*)restaurant;
@end
