//
//  IMGEngageCard.h
//  Qraved
//
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IMGEngageCard : NSObject
@property (nonatomic,retain) NSNumber *reviewId;
@property (nonatomic,retain) NSNumber *restaurantId;
@property (nonatomic,copy) NSString *restaurantTitle;
@property (nonatomic,copy) NSString *restaurantSeo;
@property (nonatomic,copy) NSString *restaurantImage;
@property (nonatomic,copy) NSString *restaurantImageAltText;
@property (nonatomic,retain) NSNumber *cityId;
@end
