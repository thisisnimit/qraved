//
//  IMGUpdateCard.h
//  Qraved
//
//  Created by imaginato on 16/6/1.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGRestaurant.h"
@interface IMGUpdateCard : NSObject
@property(nonatomic,retain)IMGRestaurant* restaurant;
@property(nonatomic,retain)NSMutableArray* eventArr;
@end
