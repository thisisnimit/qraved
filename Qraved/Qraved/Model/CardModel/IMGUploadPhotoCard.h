//
//  IMGUploadPhotoCard.h
//  Qraved
//
//  Created by imaginato on 16/5/31.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGRestaurant.h"
#import "IMGUser.h"

@interface IMGUploadPhotoCard : NSObject

@property (nonatomic,strong) IMGRestaurant *uploadCardRestaurant;
@property (nonatomic,strong) IMGUser *uploadCardUser;
@property (nonatomic,strong) NSMutableArray *dishArrM;




@end
