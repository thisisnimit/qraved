//
//  V2_DiscoverListResultRestaurantTableViewCellModel.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverListResultRestaurantTableViewCellModel.h"

@implementation V2_DiscoverListResultRestaurantTableViewCellModel

/* 数组中存储模型数据，需要说明数组中存储的模型数据类型 */
+(NSDictionary *)mj_objectClassInArray
{
    return @{@"data" : [V2_DiscoverListResultRestaurantTableViewCellSubModel class]};
}
@end
