//
//  IMGSavedRestaurant.m
//  Qraved
//
//  Created by harry on 2017/6/2.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "IMGSavedRestaurant.h"

@implementation IMGSavedRestaurant


-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if([key isEqualToString:@"id"]){
        self.restaurantId=value;
    }
    if([key isEqualToString:@"discount"]){
        self.disCount=value;
    }
    if ([key isEqualToString:@"description"]) {
        self.descriptionStr = value;
    }
    
}
-(void)setValuesForKeysWithDictionary:(NSDictionary *)keyedValues{
    [super setValuesForKeysWithDictionary:keyedValues];
}

-(NSString *)distance{
    //    if([self.latitude intValue]==0 || [self.longitude intValue]==0){
    //        return @"";
    //    }
    
    //    double lon2=[AppDelegate ShareApp].locationManager.location.coordinate.longitude;
    //    double lat2=[AppDelegate ShareApp].locationManager.location.coordinate.latitude;
    double lon2=[[[NSUserDefaults standardUserDefaults]objectForKey:@"longitude"] doubleValue];
    double lat2=[[[NSUserDefaults standardUserDefaults]objectForKey:@"latitude"] doubleValue];
    NSLog(@"%f,%f",lon2,lat2);
    double dd = M_PI/180;
    double x1=[self.latitude doubleValue]*dd,x2=lat2 *dd;
    double y1=[self.longitude doubleValue]*dd,y2=lon2 *dd;
    double R = 6378138;
    double distance = (2*R*asin(sqrt(2-2*cos(x1)*cos(x2)*cos(y1-y2) - 2*sin(x1)*sin(x2))/2));
    //    NSLog(@"distance is less than 0,%f",distance);
    
    if(distance < 1000){
        return [[NSString alloc] initWithFormat:@"%dm",[[NSNumber numberWithDouble:distance] intValue]];
    }else if(distance > 8000*1000){
        return @"";
    }else{
        long kmDistance = (long)distance/1000;
        return [[NSString alloc] initWithFormat:@"%ldkm",kmDistance];
    }
}
-(NSArray *)openingHours{
    NSMutableArray *openingHours = [[NSMutableArray alloc]init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"hh:mm"];
    NSArray *array = @[@2,@3,@4,@5,@6,@7,@1];
    for (int i=0; i<array.count; i++) {
        NSInteger index = [[array objectAtIndex:i] integerValue];
        
        switch (index) {
                
            case 2:
                //            {
                //                NSDate *openDate = [dateFormatter dateFromString:self.monTimeOpen];
                //                NSDate *closeDate = [dateFormatter dateFromString:self.monTimeClosed];
                //                if (([self.monIntermission isKindOfClass:[NSString class]] && self.monIntermission.length > 0) || ![openDate isEqualToDate:closeDate]) {
                [openingHours addObject:[self dispalyTimeAtWeekday:index weekdayName:@"Monday"]];
                //                }
                //            }
                break;
            case 3:
                //            {
                //                NSDate *openDate = [dateFormatter dateFromString:self.tueTimeOpen];
                //                NSDate *closeDate = [dateFormatter dateFromString:self.tueTimeClosed];
                //                if (([self.tueIntermission isKindOfClass:[NSString class]] && self.tueIntermission.length > 0) || ![openDate isEqualToDate:closeDate]) {
                [openingHours addObject:[self dispalyTimeAtWeekday:index weekdayName:@"Tuesday"]];
                //                }
                //            }
                break;
            case 4:
                //            {
                //                NSDate *openDate = [dateFormatter dateFromString:self.wedTimeOpen];
                //                NSDate *closeDate = [dateFormatter dateFromString:self.wedTimeClosed];
                //                if (([self.wedIntermission isKindOfClass:[NSString class]] && self.wedIntermission.length > 0) || ![openDate isEqualToDate:closeDate]) {
                [openingHours addObject:[self dispalyTimeAtWeekday:index weekdayName:@"Wednesday"]];
                //                }
                //            }
                break;
            case 5:
                //            {
                //                NSDate *openDate = [dateFormatter dateFromString:self.thuTimeOpen];
                //                NSDate *closeDate = [dateFormatter dateFromString:self.thuTimeClosed];
                //                if (([self.thuIntermission isKindOfClass:[NSString class]] && self.thuIntermission.length > 0) || ![openDate isEqualToDate:closeDate]) {
                [openingHours addObject:[self dispalyTimeAtWeekday:index weekdayName:@"Thursday"]];
                //                }
                //            }
                break;
            case 6:
                //            {
                //                NSDate *openDate = [dateFormatter dateFromString:self.friTimeOpen];
                //                NSDate *closeDate = [dateFormatter dateFromString:self.friTimeClosed];
                //                if (([self.friIntermission isKindOfClass:[NSString class]] && self.friIntermission.length > 0) || ![openDate isEqualToDate:closeDate]) {
                [openingHours addObject:[self dispalyTimeAtWeekday:index weekdayName:@"Friday"]];
                //                }
                //            }
                break;
            case 7:
                //            {
                //                NSDate *openDate = [dateFormatter dateFromString:self.satTimeOpen];
                //                NSDate *closeDate = [dateFormatter dateFromString:self.satTimeClosed];
                //                if (([self.satIntermission isKindOfClass:[NSString class]] && self.satIntermission.length > 0) || ![openDate isEqualToDate:closeDate]) {
                [openingHours addObject:[self dispalyTimeAtWeekday:index weekdayName:@"Saturday"]];
                //                }
                //            }
                break;
            case 1:
                //            {
                //                NSDate *openDate = [dateFormatter dateFromString:self.sunTimeOpen];
                //                NSDate *closeDate = [dateFormatter dateFromString:self.sunTimeClosed];
                //                if (([self.sunIntermission isKindOfClass:[NSString class]] && self.sunIntermission.length > 0) || ![openDate isEqualToDate:closeDate]) {
                [openingHours addObject:[self dispalyTimeAtWeekday:index weekdayName:@"Sunday"]];
                //                }
                //                
                //            }
                break;
            default:
                break;
        }
        
        
    }
    return openingHours;
}
-(NSString *)dispalyTimeAtWeekday:(NSInteger)currentWeekDay weekdayName:(NSString *)weekdayName{
    switch (currentWeekDay) {
        case 2:
        {
            if ([self.monIntermission isKindOfClass:[NSString class]] && self.monIntermission.length > 0) {
                NSArray *intermissionArray = [self.monIntermission componentsSeparatedByString:@" - "];
                return [NSString stringWithFormat:@"%@: %@ - %@, %@ - %@",weekdayName,self.monTimeOpen,[intermissionArray objectAtIndex:0],[intermissionArray objectAtIndex:1],self.monTimeClosed];
            }else{
                NSString *displayTime = [NSString stringWithFormat:@"%@: %@ - %@",weekdayName,self.monTimeOpen,self.monTimeClosed];
                if(self.d1!=nil && ![self.d1 isKindOfClass:[NSNull class]] && [self.d1 intValue]==1){
                    if(([displayTime isEqualToString:[NSString stringWithFormat:@"%@: 00:00 - 00:00",weekdayName]]||[displayTime isEqualToString:[NSString stringWithFormat:@"%@: 0 - 0",weekdayName]])){
                        displayTime=[NSString stringWithFormat:@"%@: 24 Hours Open",weekdayName];
                    }
                }else{
                    displayTime=[NSString stringWithFormat:@"%@: Closed",weekdayName];
                }
                return displayTime;
            }
        }
            break;
        case 3:
        {
            if ([self.tueIntermission isKindOfClass:[NSString class]] && self.tueIntermission.length > 0) {
                NSArray *intermissionArray = [self.tueIntermission componentsSeparatedByString:@" - "];
                return [NSString stringWithFormat:@"%@: %@ - %@, %@ - %@",weekdayName,self.tueTimeOpen,[intermissionArray objectAtIndex:0],[intermissionArray objectAtIndex:1],self.tueTimeClosed];
            }else{
                NSString *displayTime =[NSString stringWithFormat:@"%@: %@ - %@",weekdayName,self.tueTimeOpen,self.tueTimeClosed];
                if(self.d2!=nil && ![self.d2 isKindOfClass:[NSNull class]] && [self.d2 intValue]==1){
                    if(([displayTime isEqualToString:[NSString stringWithFormat:@"%@: 00:00 - 00:00",weekdayName]]||[displayTime isEqualToString:[NSString stringWithFormat:@"%@: 0 - 0",weekdayName]])){
                        displayTime=[NSString stringWithFormat:@"%@: 24 Hours Open",weekdayName];
                    }
                }else{
                    displayTime=[NSString stringWithFormat:@"%@: Closed",weekdayName];
                }
                return displayTime;
            }
        }
            break;
        case 4:
        {
            if ([self.wedIntermission isKindOfClass:[NSString class]] && self.wedIntermission.length > 0) {
                NSArray *intermissionArray = [self.wedIntermission componentsSeparatedByString:@" - "];
                return [NSString stringWithFormat:@"%@: %@ - %@, %@ - %@",weekdayName,self.wedTimeOpen,[intermissionArray objectAtIndex:0],[intermissionArray objectAtIndex:1],self.wedTimeClosed];
            }else{
                NSString *displayTime =[NSString stringWithFormat:@"%@: %@ - %@",weekdayName,self.wedTimeOpen,self.wedTimeClosed];
                if(self.d3!=nil && ![self.d3 isKindOfClass:[NSNull class]] && [self.d3 intValue]==1){
                    if(([displayTime isEqualToString:[NSString stringWithFormat:@"%@: 00:00 - 00:00",weekdayName]]||[displayTime isEqualToString:[NSString stringWithFormat:@"%@: 0 - 0",weekdayName]])){
                        displayTime=[NSString stringWithFormat:@"%@: 24 Hours Open",weekdayName];
                    }
                }else{
                    displayTime=[NSString stringWithFormat:@"%@: Closed",weekdayName];
                }
                return displayTime;
            }
        }
            break;
        case 5:
        {
            if ([self.thuIntermission isKindOfClass:[NSString class]] && self.thuIntermission.length > 0) {
                NSArray *intermissionArray = [self.thuIntermission componentsSeparatedByString:@" - "];
                return [NSString stringWithFormat:@"%@: %@ - %@, %@ - %@",weekdayName,self.thuTimeOpen,[intermissionArray objectAtIndex:0],[intermissionArray objectAtIndex:1],self.thuTimeClosed];
            }else{
                NSString *displayTime =[NSString stringWithFormat:@"%@: %@ - %@",weekdayName,self.thuTimeOpen,self.thuTimeClosed];
                if(self.d4!=nil && ![self.d4 isKindOfClass:[NSNull class]] && [self.d4 intValue]==1){
                    if(([displayTime isEqualToString:[NSString stringWithFormat:@"%@: 00:00 - 00:00",weekdayName]]||[displayTime isEqualToString:[NSString stringWithFormat:@"%@: 0 - 0",weekdayName]])){
                        displayTime=[NSString stringWithFormat:@"%@: 24 Hours Open",weekdayName];
                    }
                }else{
                    displayTime=[NSString stringWithFormat:@"%@: Closed",weekdayName];
                }
                return displayTime;
            }
        }
            break;
        case 6:
        {
            if ([self.friIntermission isKindOfClass:[NSString class]] && self.friIntermission.length > 0) {
                NSArray *intermissionArray = [self.friIntermission componentsSeparatedByString:@" - "];
                return [NSString stringWithFormat:@"%@: %@ - %@, %@ - %@",weekdayName,self.friTimeOpen,[intermissionArray objectAtIndex:0],[intermissionArray objectAtIndex:1],self.friTimeClosed];
            }else{
                NSString *displayTime =[NSString stringWithFormat:@"%@: %@ - %@",weekdayName,self.friTimeOpen,self.friTimeClosed];
                if(self.d5!=nil && ![self.d5 isKindOfClass:[NSNull class]] && [self.d5 intValue]==1){
                    if(([displayTime isEqualToString:[NSString stringWithFormat:@"%@: 00:00 - 00:00",weekdayName]]||[displayTime isEqualToString:[NSString stringWithFormat:@"%@: 0 - 0",weekdayName]])){
                        displayTime=[NSString stringWithFormat:@"%@: 24 Hours Open",weekdayName];
                    }
                }else{
                    displayTime=[NSString stringWithFormat:@"%@: Closed",weekdayName];
                }
                return displayTime;
            }
        }
            break;
        case 7:
        {
            if ([self.satIntermission isKindOfClass:[NSString class]] && self.satIntermission.length > 0) {
                NSArray *intermissionArray = [self.satIntermission componentsSeparatedByString:@" - "];
                return [NSString stringWithFormat:@"%@: %@ - %@, %@ - %@",weekdayName,self.satTimeOpen,[intermissionArray objectAtIndex:0],[intermissionArray objectAtIndex:1],self.satTimeClosed];
            }else{
                NSString *displayTime =[NSString stringWithFormat:@"%@: %@ - %@",weekdayName,self.satTimeOpen,self.satTimeClosed];
                if(self.d6!=nil && ![self.d6 isKindOfClass:[NSNull class]] && [self.d6 intValue]==1){
                    if(([displayTime isEqualToString:[NSString stringWithFormat:@"%@: 00:00 - 00:00",weekdayName]]||[displayTime isEqualToString:[NSString stringWithFormat:@"%@: 0 - 0",weekdayName]])){
                        displayTime=[NSString stringWithFormat:@"%@: 24 Hours Open",weekdayName];
                    }
                }else{
                    displayTime=[NSString stringWithFormat:@"%@: Closed",weekdayName];
                }
                return displayTime;
            }
        }
            break;
        case 1:
        {
            if ([self.sunIntermission isKindOfClass:[NSString class]] && self.sunIntermission.length > 0) {
                NSArray *intermissionArray = [self.sunIntermission componentsSeparatedByString:@" - "];
                NSLog(@"dispalyTime = %@",[NSString stringWithFormat:@"%@: %@ - %@, %@ - %@",weekdayName,self.sunTimeOpen,[intermissionArray objectAtIndex:0],[intermissionArray objectAtIndex:1],self.sunTimeClosed]);
                return [NSString stringWithFormat:@"%@: %@ - %@, %@ - %@",weekdayName,self.sunTimeOpen,[intermissionArray objectAtIndex:0],[intermissionArray objectAtIndex:1],self.sunTimeClosed];
            }else{
                NSString *displayTime =[NSString stringWithFormat:@"%@: %@ - %@",weekdayName,self.sunTimeOpen,self.sunTimeClosed];
                if(self.d7!=nil && ![self.d7 isKindOfClass:[NSNull class]] && [self.d7 intValue]==1){
                    if(([displayTime isEqualToString:[NSString stringWithFormat:@"%@: 00:00 - 00:00",weekdayName]]||[displayTime isEqualToString:[NSString stringWithFormat:@"%@: 0 - 0",weekdayName]])){
                        displayTime=[NSString stringWithFormat:@"%@: 24 Hours Open",weekdayName];
                    }
                }else{
                    displayTime=[NSString stringWithFormat:@"%@: Closed",weekdayName];
                }
                return displayTime;
            }
        }
            break;
        default:
            return nil;
            break;
    }
}

@end
