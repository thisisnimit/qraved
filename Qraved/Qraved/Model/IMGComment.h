//
//  IMGComment.h
//  Qraved
//
//  Created by lucky on 16/3/24.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"
#import "IMGRestaurant.h"
#import "IMGUser.h"

@interface IMGComment : IMGEntity

@property(nonatomic,assign) BOOL isVendor;
@property(nonatomic,retain) NSNumber *userType;
@property(nonatomic,retain) NSString *restaurantName;
@property(nonatomic,retain) NSNumber *restaurantId;
@property(nonatomic,retain) NSString *restaurantImageUrl;
@property(nonatomic,retain) NSString *userName;
@property(nonatomic,retain) NSNumber *userId;
@property(nonatomic,retain) NSString *userImageUrl;
@property(nonatomic,retain) NSNumber *commentId;

@end
