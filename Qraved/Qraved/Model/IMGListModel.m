//
//  IMGListModel.m
//  Qraved
//
//  Created by Gary.yao on 2017/6/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "IMGListModel.h"
#import "IMGGuideModel.h"
@implementation IMGListModel


- (NSDictionary *)objectClassInArray{
    
    return@{@"guideList":[IMGGuideModel class]};
    
}

@end
