//
//  IMGDishComment.h
//  Qraved
//
//  Created by Jeff on 3/9/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "IMGComment.h"
#import "IMGRestaurant.h"

@interface IMGDishComment : IMGComment

@property(nonatomic,retain) NSString *content;
@property(nonatomic,retain) NSString *createdDate;
@property(nonatomic,retain) NSNumber *createTime;
@property(nonatomic,retain) NSString *timeCreatedStr;
@property(nonatomic,assign) BOOL readMore;

+(NSMutableArray*)commentListFromArray:(NSArray*)commentArray andIsVendor:(BOOL)isVendor andRestaurant:(IMGRestaurant *)restaurant;
-(id)initFromDictionary:(NSDictionary*)commentDictionary;
@end
