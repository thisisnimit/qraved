//
//  IMGDishComment.m
//  Qraved
//
//  Created by Jeff on 3/9/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "IMGDishComment.h"

@implementation IMGDishComment
-(id)initFromDictionary:(NSDictionary*)commentDictionary
{
    self = [super init];
    if(self){
        NSString *commentIdStr = [commentDictionary objectForKey:@"id"];
        if(commentIdStr){
            NSString *commentIdStr = [commentDictionary objectForKey:@"id"];
            if(commentIdStr){
                self.commentId=[NSNumber numberWithInteger:[commentIdStr integerValue]];
                self.content=[commentDictionary objectForKey:@"comment"];
                NSString *createTime = [commentDictionary objectForKey:@"timeCreated"];
                if(createTime){
                    self.createTime=[NSNumber numberWithLongLong:[createTime longLongValue]];
                }
                self.restaurantId = [commentDictionary objectForKey:@"id"];
                self.restaurantName = [commentDictionary objectForKey:@"restaurantName"];
                self.timeCreatedStr = [commentDictionary objectForKey:@"timeCreatedStr"];
                NSString *commentUserId =[commentDictionary objectForKey:@"userId"];
                if(commentUserId){
                    self.userId=[NSNumber numberWithInteger:[commentUserId integerValue]];
                }
                self.userImageUrl=[commentDictionary objectForKey:@"avatar"];
                self.userName=[commentDictionary objectForKey:@"fullName"];
            }
        }
    }
    return self;
}


+(NSMutableArray*)commentListFromArray:(NSArray*)commentArray andIsVendor:(BOOL)isVendor andRestaurant:(IMGRestaurant *)restaurant
{
    NSMutableArray* commentList = [[NSMutableArray alloc]initWithCapacity:0];
    for(int i=0;commentArray!=nil && [commentArray isKindOfClass:[NSArray class]] && i<commentArray.count;i++){
        if([[commentArray objectAtIndex:i] isKindOfClass:[IMGDishComment class]]){
            [commentList addObject:[commentArray objectAtIndex:i]];
        }else{
            NSDictionary *commentDictionary = [commentArray objectAtIndex:i];
            IMGDishComment *dishComment = [[IMGDishComment alloc]init];
            NSString *commentIdStr = [commentDictionary objectForKey:@"id"];
            if(commentIdStr){
                dishComment.commentId=[NSNumber numberWithInteger:[commentIdStr integerValue]];
                dishComment.content=[commentDictionary objectForKey:@"comment"];
                NSString *createTime = [commentDictionary objectForKey:@"timeCreated"];
                if(createTime){
                    dishComment.createTime=[NSNumber numberWithLongLong:[createTime longLongValue]];
                }
                dishComment.timeCreatedStr = [commentDictionary objectForKey:@"timeCreatedStr"];
                dishComment.restaurantId = restaurant.restaurantId;
                dishComment.restaurantName = restaurant.title;
                dishComment.restaurantImageUrl = restaurant.imageUrl;
                dishComment.userType = [commentDictionary objectForKey:@"userType"];
                dishComment.isVendor = isVendor;
                NSString *commentUserId =[commentDictionary objectForKey:@"userId"];
                if(commentUserId){
                    dishComment.userId=[NSNumber numberWithInteger:[commentUserId integerValue]];
                }
                dishComment.userImageUrl=[commentDictionary objectForKey:@"avatar"];
                dishComment.userName=[commentDictionary objectForKey:@"fullName"];
                [commentList addObject:dishComment];
            }
        }
       
    }
    return commentList;
}

@end

