//
//  RecentSearchesTableViewCellModel.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/18.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecentSearchesTableViewCellModel : NSObject

@property (nonatomic, strong)  NSNumber  *myID;
@property (nonatomic, strong)  NSString  *img;
@property (nonatomic, strong)  NSString  *title;
@property (nonatomic, strong)  NSString  *type;
@property (nonatomic, strong)  NSString  *updated_date;

@end
