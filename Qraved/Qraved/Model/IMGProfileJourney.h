//
//  IMGProfileJourney.h
//  Qraved
//
//  Created by harry on 2018/1/31.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGJourneyReview.h"
#import "IMGJourneyBook.h"
#import "IMGJourneyPhoto.h"
#import "IMGJourneySave.h"
#import "CouponModel.h"

@interface IMGProfileJourney : NSObject

@property (nonatomic, copy) NSNumber *type;
@property (nonatomic, strong) IMGJourneyReview *journeyReview;
@property (nonatomic, strong) IMGJourneyBook *journeyBook;
@property (nonatomic, strong) IMGJourneyPhoto *journeyPhoto;
@property (nonatomic, strong) IMGJourneySave *journeySave;
@property (nonatomic, strong) CouponModel *journeyCoupon;

@end
