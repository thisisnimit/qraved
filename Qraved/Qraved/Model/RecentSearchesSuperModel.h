//
//  RecentSearchesSuperModel.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "RecentSearchesTableViewCellModel.h"

@interface RecentSearchesSuperModel : NSObject

@property (nonatomic, strong)  NSNumber  *userId;

@property (nonatomic, strong)  NSArray<RecentSearchesTableViewCellModel *>  *history;
@end
