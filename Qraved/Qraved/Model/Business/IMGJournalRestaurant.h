//
//  IMGJournalRestaurant.h
//  Qraved
//
//  Created by Lucky on 15/7/1.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGJournalRestaurant : IMGEntity

// address1 2 class content
@property(nonatomic,retain) NSNumber *restaurantId;
@property(nonatomic,copy)   NSString *address1;
@property(nonatomic,copy)   NSString *address2;
@property(nonatomic,copy)   NSString *content;
@property(nonatomic,copy)   NSString *createTime;
@property(nonatomic,retain) NSNumber *journalId;
@property(nonatomic,retain) NSNumber *journalArticleId;
@property(nonatomic,retain) NSNumber *latitude;
@property(nonatomic,retain) NSNumber *longitude;
@property(nonatomic,retain) NSNumber *orders;
@property(nonatomic,copy)   NSString *photoCredit;
@property(nonatomic,copy)   NSString *restaurantPhoto;
@property(nonatomic,copy)   NSString *restaurantTitle;
@property(nonatomic,retain) NSNumber *score;
@property(nonatomic,retain) NSNumber *status;
@property(nonatomic,copy)   NSString *updateTime;

@end
