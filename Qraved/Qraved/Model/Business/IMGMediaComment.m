//
//  IMGMediaComment.m
//  Qraved
//
//  Created by Jeff on 10/14/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGMediaComment.h"
#import "DBManager.h"
#import "NSString+Helper.h"
#import "IMGJournalComment.h"

@implementation IMGMediaComment{
    NSString *_title;
    NSString *_restaurantTitle;
}

@synthesize title=_title;

-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        self.mediaCommentId = value;
    }else if([key isEqualToString:@"photoCredit4AppImpl"]) {
        [self updatePhotoCredit:value];
    }
}

+(id)findById:(NSNumber*)mediaCommentId{
    IMGMediaComment *media = [[IMGMediaComment alloc] init];
    FMResultSet *resultSet = [[DBManager manager] executeQuery:[NSString stringWithFormat:@"select * from IMGMediaComment where mediaCommentId=%@",mediaCommentId]];
    
    if([resultSet next]){
        [media setValueWithResultSet:resultSet];
        [resultSet close];
        return media;
    }else{
        [resultSet close];
        return nil;
    }
    
}

-(void)setTitle:(NSString*)title{
    NSString *tmp=[title copy];
    _title=[tmp filterHtml];
}

-(void)setRestaurantTitle:(NSString*)title{
    _restaurantTitle=[title filterHtml];
}

-(void)setJournalTitle:(NSString*)title{
    _journalTitle=[title filterHtml];
}

+(NSMutableArray*)commentListFromArray:(NSArray*)commentArray{
    NSMutableArray* commentList = [[NSMutableArray alloc]initWithCapacity:0];
    for(int i=0;commentArray!=nil && [commentArray isKindOfClass:[NSArray class]] && i<commentArray.count;i++){
        NSDictionary *commentDictionary = [commentArray objectAtIndex:i];
        IMGJournalComment *journalComment = [[IMGJournalComment alloc]init];
        NSString *commentIdStr = [commentDictionary objectForKey:@"commentId"];
        if(commentIdStr){
            journalComment.commentId=[NSNumber numberWithInteger:[commentIdStr integerValue]];
            journalComment.content=[commentDictionary objectForKey:@"content"];
            NSString *createTime = [commentDictionary objectForKey:@"createTime"];
            if(createTime){
                journalComment.createTime=[NSNumber numberWithLongLong:[createTime longLongValue]];
            }
            NSString *commentUserId =[commentDictionary objectForKey:@"userId"];
            if(commentUserId){
                journalComment.userId=[NSNumber numberWithInteger:[commentUserId integerValue]];
            }
            journalComment.userImageUrl=[commentDictionary objectForKey:@"userImageUrl"];
            journalComment.userName=[commentDictionary objectForKey:@"fullName"];
            [commentList addObject:journalComment];
        }
    }
    return commentList;
}

-(void)updatePhotoCredit:(NSDictionary*)dic{
    if (dic != nil) {
        self.userPhotoCountDic = [dic objectForKey:@"userPhotoCount"];
        self.photoCreditTypeDic  = [dic objectForKey:@"photoCreditType"];
        self.userIdDic  = [dic objectForKey:@"userId"];
        self.photoCreditDic  = [dic objectForKey:@"photoCredit"];
        self.userReviewCountDic  = [dic objectForKey:@"userReviewCount"];
        self.userAvatarDic  = [dic objectForKey:@"userAvatar"];
        self.photoCreditUrlDic  = [dic objectForKey:@"photoCreditUrl"];
        self.restaurantIdDic  = [dic objectForKey:@"restaurantId"];
    }
}
@end
