//
//  IMGRestaurantOffer.m
//  Qraved
//
//  Created by Olaf on 14/9/26.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGRestaurantViewed.h"
#import "DBManager.h"

@implementation IMGRestaurantViewed

+(id)findByRestaurantId:(NSNumber*)restaurantId{
    IMGRestaurantViewed *restaurantViewed = [[IMGRestaurantViewed alloc] init];
    FMResultSet *resultSet = [[DBManager manager] executeQuery:[NSString stringWithFormat:@"select * from IMGRestaurantViewed where restaurantId=%@",restaurantId]];
    if([resultSet next]){
        [restaurantViewed setValueWithResultSet:resultSet];
        [resultSet close];
        return restaurantViewed;
    }else{
        [resultSet close];
        return nil;
    }
    
}

@end
