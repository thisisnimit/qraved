//
//  IMGRestaurantOffer.m
//  Qraved
//
//  Created by Olaf on 14/9/26.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGRestaurantImage.h"

@implementation IMGRestaurantImage

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if([key isEqualToString:@"photoCredit"]) {
        [self updatePhotoCredit:value];
    }
}
-(void)updatePhotoCredit:(NSDictionary*)dic{
    if (dic != nil) {
        self.userPhotoCountDic = [dic objectForKey:@"userPhotoCount"];
        self.photoCreditTypeDic  = [dic objectForKey:@"photoCreditType"];
        self.userIdDic  = [dic objectForKey:@"userId"];
        self.photoCreditDic  = [dic objectForKey:@"photoCredit"];
        self.userReviewCountDic  = [dic objectForKey:@"userReviewCount"];
        self.userAvatarDic  = [dic objectForKey:@"userAvatar"];
        self.photoCreditUrlDic  = [dic objectForKey:@"photoCreditUrl"];
        self.restaurantIdDic  = [dic objectForKey:@"restaurantId"];
    }
}

@end
