//
//  IMGDiningGuide.h
//
;
//
//  Created by Laura on 14-9-19.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGDiningGuide : IMGEntity

@property (nonatomic,retain)    NSNumber *diningGuideId;
@property (nonatomic,copy)      NSString *pageName;
@property (nonatomic,copy)      NSString *pageType;
@property (nonatomic,retain)    NSNumber *pageShow;
@property (nonatomic,copy)      NSString *headerImage;
@property (nonatomic,copy)      NSString *pageTitle;
@property (nonatomic,copy)      NSString *typeName;
@property (nonatomic,copy)      NSString * metaKeyword;
@property (nonatomic,copy)      NSString * metaDescription;
@property (nonatomic,copy)      NSString *tagline;
@property (nonatomic,copy)      NSString * pageContent;
@property (nonatomic,copy)      NSString *pageUrl;
@property (nonatomic,copy)      NSString *searchPageUrl;
@property (nonatomic,copy)      NSString *buttonText;
@property (nonatomic,retain)    NSNumber *sortOrder;
@property (nonatomic,retain)    NSNumber *restaurantCount;
@property (nonatomic,retain)    NSNumber *cityId;
@property (nonatomic,copy)      NSString *timeline;
@property (nonatomic,copy)      NSString *headerImageThumbnail;

+(id)findDiningGuideById:(NSNumber*)diningGuideId;

@end
