//
//  IMGRestaurantDistrict.h
//  Qraved
//
//  Created by Laura on 14-9-29.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGEntity.h"

@interface IMGRestaurantDistrict : IMGEntity
@property(nonatomic,retain) NSNumber *districtId;
@property(nonatomic,retain) NSNumber *restaurantId;
@end
