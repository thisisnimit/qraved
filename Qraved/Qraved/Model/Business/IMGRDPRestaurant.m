//
//  IMGRDPRestaurant.m
//  Qraved
//
//  Created by lucky on 16/6/7.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "IMGRDPRestaurant.h"

@implementation IMGRDPRestaurant
-(id)initWithRestId:(NSNumber *)restaurantId andRDPFirstDataArr:(NSArray *)RDPFirstDataArr andMenuList:(NSArray *)menuList andDishList:(NSArray *)dishList andMoreDishList:(NSArray *)moreDishList andReviewList:(NSArray *)reviewList andEventList:(NSArray *)eventList andDiningGuideList:(NSArray *)diningGuideList andJournalList:(NSArray *)journalList andMenuPhotoCount:(NSNumber *)menuPhotoCount andDishCount:(NSNumber *)dishCount andEventCount:(NSNumber *)eventCount andDiningGuideCount:(NSNumber *)diningGuideCount andJournalCount:(NSNumber *)journalCount andIsHadRDPSecondData:(NSNumber *)isHadRDPSecondData andRestRatingCounts:(NSString *)ratingCounts andRestRatingScore:(NSString *)ratingScore andReviewCount:(NSNumber *)reviewCount;
{
    self = [super init];
    if (self) {
        self.restaurantId = restaurantId;
        self.RDPFirstDataArr = RDPFirstDataArr;
        self.menuList = menuList;
        self.dishList = dishList;
        self.moreDishList = moreDishList;
        self.reviewList = reviewList;
        self.eventList = eventList;
        self.diningGuideList = diningGuideList;
        self.journalList = journalList;
        
        self.menuPhotoCount = menuPhotoCount;
        self.dishCount = dishCount;
        self.eventCount = eventCount;
        self.diningGuideCount = diningGuideCount;
        self.journalCount = journalCount;
        
        self.isHadRDPSecondData = isHadRDPSecondData;
        
        self.ratingCounts = ratingCounts;
        self.ratingScore = ratingScore;
        self.reviewCount = reviewCount;

    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder //将属性进行编码
{
    [aCoder encodeObject:self.restaurantId forKey:@"restaurantId"];
    [aCoder encodeObject:self.RDPFirstDataArr forKey:@"RDPFirstDataArr"];
    
    [aCoder encodeObject:self.menuList forKey:@"menuList"];
    [aCoder encodeObject:self.dishList forKey:@"dishList"];
    [aCoder encodeObject:self.moreDishList forKey:@"moreDishList"];
    [aCoder encodeObject:self.reviewList forKey:@"reviewList"];
    [aCoder encodeObject:self.eventList forKey:@"eventList"];
    [aCoder encodeObject:self.diningGuideList forKey:@"diningGuideList"];
    [aCoder encodeObject:self.journalList forKey:@"journalList"];
    
    [aCoder encodeObject:self.menuPhotoCount forKey:@"menuPhotoCount"];
    [aCoder encodeObject:self.dishCount forKey:@"dishCount"];
    [aCoder encodeObject:self.eventCount forKey:@"eventCount"];
    [aCoder encodeObject:self.diningGuideCount forKey:@"diningGuideCount"];
    [aCoder encodeObject:self.journalCount forKey:@"journalCount"];

    [aCoder encodeObject:self.isHadRDPSecondData forKey:@"isHadRDPSecondData"];
    
    [aCoder encodeObject:self.ratingCounts forKey:@"ratingCounts"];
    [aCoder encodeObject:self.ratingScore forKey:@"ratingScore"];
    [aCoder encodeObject:self.reviewCount forKey:@"reviewCount"];


}
- (id)initWithCoder:(NSCoder *)aDecoder //将属性进行解码
{
    NSNumber *restaurantId = [aDecoder decodeObjectForKey:@"restaurantId"];
    NSMutableArray *RDPFirstDataArr = [aDecoder decodeObjectForKey:@"RDPFirstDataArr"];
    NSArray *menuList = [aDecoder decodeObjectForKey:@"menuList"];
    NSArray *dishList = [aDecoder decodeObjectForKey:@"dishList"];
    NSArray *moreDishList = [aDecoder decodeObjectForKey:@"moreDishList"];
    NSArray *reviewList = [aDecoder decodeObjectForKey:@"reviewList"];
    NSArray *eventList = [aDecoder decodeObjectForKey:@"eventList"];
    NSArray *diningGuideList = [aDecoder decodeObjectForKey:@"diningGuideList"];
    NSArray *journalList = [aDecoder decodeObjectForKey:@"journalList"];
    
    NSNumber *menuPhotoCount = [aDecoder decodeObjectForKey:@"menuPhotoCount"];
    NSNumber *dishCount = [aDecoder decodeObjectForKey:@"dishCount"];
    NSNumber *eventCount = [aDecoder decodeObjectForKey:@"eventCount"];
    NSNumber *diningGuideCount = [aDecoder decodeObjectForKey:@"diningGuideCount"];
    NSNumber *journalCount = [aDecoder decodeObjectForKey:@"journalCount"];
    
    NSNumber *isHadRDPSecondData = [aDecoder decodeObjectForKey:@"isHadRDPSecondData"];
 
    NSString *ratingCounts = [aDecoder decodeObjectForKey:@"ratingCounts"];
    NSString *ratingScore = [aDecoder decodeObjectForKey:@"ratingScore"];
    NSNumber *reviewCount = [aDecoder decodeObjectForKey:@"reviewCount"];

    
    self = [self initWithRestId:restaurantId andRDPFirstDataArr:RDPFirstDataArr andMenuList:menuList andDishList:dishList andMoreDishList:moreDishList andReviewList:reviewList andEventList:eventList andDiningGuideList:diningGuideList andJournalList:journalList andMenuPhotoCount:menuPhotoCount andDishCount:dishCount andEventCount:eventCount andDiningGuideCount:diningGuideCount andJournalCount:journalCount andIsHadRDPSecondData:isHadRDPSecondData andRestRatingCounts:ratingCounts andRestRatingScore:ratingScore andReviewCount:reviewCount];
    return self;
}

@end
