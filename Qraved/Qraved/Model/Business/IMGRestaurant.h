//
//  IMGRestaurant.h
//  Qraved
//
//  Created by Jeff on 8/12/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//
#import "IMGEntity.h"
#import "DBManager.h"

#define IMGENTITY_EXTEND
@interface IMGRestaurant : IMGEntity

@property(nonatomic,retain) NSNumber *restaurantId;
@property(nonatomic,retain) NSNumber *bookingCount;
@property(nonatomic,copy)   NSString *priceName;
@property(nonatomic,retain) NSNumber *priceLevel;
@property(nonatomic,retain) NSNumber *dailyBookingLimit;
@property(nonatomic,copy)   NSString *descriptionStr;
@property(nonatomic,retain) NSNumber *dinersCount;
@property(nonatomic,retain) NSNumber *disCount;
@property(nonatomic,copy)   NSString *discountContent;
@property(nonatomic,copy)   NSString *needUpdate;
@property(nonatomic,copy)   NSString *wellKnownFor;
@property(nonatomic,copy)   NSString *journaArticlelTitle;
@property(nonatomic,copy)   NSNumber *inJournal;
@property(nonatomic,copy)   NSNumber *journalArticleId;
@property(nonatomic,copy)   NSNumber *trendingInstagram;
@property(nonatomic,copy)   NSString *brandLogo;
@property(nonatomic,copy)   NSString *restaurant_landmark;
@property(nonatomic,copy)   NSString *goFoodLink;
@property(nonatomic,copy)   NSArray *couponBannerArr;

@property(nonatomic,retain) NSNumber *dishCount;
@property(nonatomic,copy)   NSString *displayTime;
@property(nonatomic,copy)   NSNumber *sortOrder;

@property(nonatomic,retain) NSNumber *imageCount;
@property(nonatomic,copy)   NSString *imageUrl;
@property(nonatomic,copy)   NSString *note;

@property(nonatomic,copy)   NSString *logoImage;

@property(nonatomic,retain) NSNumber *maximumPax;
@property(nonatomic,retain) NSNumber *maximumAvailableBooking;
@property(nonatomic,retain) NSNumber *mediaCommentCount;
@property(nonatomic,retain) NSNumber *menuCount;
@property(nonatomic,retain) NSNumber *photoCount;

@property(nonatomic,retain) NSNumber *minimumAvailableBooking;
@property(nonatomic,retain) NSNumber *minimumPax;
@property(nonatomic,copy)   NSString *phoneNumber;
@property(nonatomic,retain) NSNumber *qravedCount;
@property(nonatomic,retain) NSNumber *reviewCount;
@property(nonatomic,retain) NSNumber *ratingCount;

@property (nonatomic,strong) NSNumber *likeCardCount;
@property (nonatomic,strong) NSNumber *commentCardCount;
@property (nonatomic,assign) BOOL isLikeCard;
@property (nonatomic,strong) NSMutableArray *commentCardList;
@property(nonatomic,retain)NSString* nextopenTime;
@property(nonatomic,retain)NSString* nextclosedTime;
@property(nonatomic,retain)NSString* nextopenDay;

@property(nonatomic,retain)NSString* secondopenTime;
@property(nonatomic,retain)NSString* secondclosedTime;
@property(nonatomic,retain)NSString* secondopenDay;

@property(nonatomic,retain)NSString* yesteropenTime;
@property(nonatomic,retain)NSString* yesterclosedTime;
@property(nonatomic,retain)NSString* yesteropenDay;

@property(nonatomic,copy)   NSString *seoKeyword;

@property(nonatomic,copy)   NSString *title;
@property(nonatomic,copy)   NSString *website;
@property(nonatomic,retain) NSNumber *liked;
@property(nonatomic,retain) NSNumber *saved;
@property(nonatomic,retain) NSNumber *openNow;

@property(nonatomic,retain) NSNumber *savedCount;

@property(nonatomic,retain) NSNumber *offerCount;
@property(nonatomic,retain) NSNumber *boost;


@property(nonatomic,copy)   NSString *ratingScore;
@property(nonatomic,copy)   NSString *ambianceScore;
@property(nonatomic,retain) NSString *serviceScore;
@property(nonatomic,copy)   NSString *foodScore;

/*open time*/
@property(nonatomic,copy)   NSString *monIntermission;
@property(nonatomic,copy)   NSString *monTimeOpen;
@property(nonatomic,copy)   NSString *monTimeClosed;
@property(nonatomic,copy)   NSString *tueIntermission;
@property(nonatomic,copy)   NSString *tueTimeOpen;
@property(nonatomic,copy)   NSString *tueTimeClosed;
@property(nonatomic,copy)   NSString *wedIntermission;
@property(nonatomic,copy)   NSString *wedTimeOpen;
@property(nonatomic,copy)   NSString *wedTimeClosed;
@property(nonatomic,copy)   NSString *thuIntermission;
@property(nonatomic,copy)   NSString *thuTimeOpen;
@property(nonatomic,copy)   NSString *thuTimeClosed;
@property(nonatomic,copy)   NSString *friIntermission;
@property(nonatomic,copy)   NSString *friTimeOpen;
@property(nonatomic,copy)   NSString *friTimeClosed;
@property(nonatomic,copy)   NSString *satIntermission;
@property(nonatomic,copy)   NSString *satTimeOpen;
@property(nonatomic,copy)   NSString *satTimeClosed;
@property(nonatomic,copy)   NSString *sunIntermission;
@property(nonatomic,copy)   NSString *sunTimeOpen;
@property(nonatomic,copy)   NSString *sunTimeClosed;
@property(nonatomic,retain) NSNumber *d1;
@property(nonatomic,retain) NSNumber *d2;
@property(nonatomic,retain) NSNumber *d3;
@property(nonatomic,retain) NSNumber *d4;
@property(nonatomic,retain) NSNumber *d5;
@property(nonatomic,retain) NSNumber *d6;
@property(nonatomic,retain) NSNumber *d7;
/*location*/
@property(nonatomic,copy)   NSString *address1;
@property(nonatomic,copy)   NSString *address2;
@property(nonatomic,retain) NSNumber *bookStatus;
@property(nonatomic,retain) NSNumber *latitude;
@property(nonatomic,retain) NSNumber *longitude;
@property(nonatomic,retain) NSNumber *calculatedDistance;

@property(nonatomic,retain) NSNumber *cityId;
@property(nonatomic,copy)   NSString *cityName;
@property(nonatomic,retain) NSNumber *districtId;
@property(nonatomic,copy)   NSString *districtName;
@property(nonatomic,copy)   NSString *bestOfferName;
@property(nonatomic,copy)   NSString *timeUpdated;
@property(nonatomic,retain) NSNumber *state;
@property(nonatomic,retain) NSString *stateName;
@property(nonatomic,copy)   NSString *cuisineName;
@property(nonatomic,copy)   NSString *dateCreated;
@property(nonatomic,copy)   NSString *offerIds;
@property(nonatomic,copy)   NSString *ratingCounts;
@property(nonatomic,retain) NSNumber *updateOfferTime;

@property(nonatomic,copy)   NSNumber *userRating;

@property(nonatomic,assign) BOOL canBook;
@property(nonatomic,copy)   NSString *hasReview;


@property(nonatomic,copy)   NSString *typeName;

@property(nonatomic,copy)   NSString *todayTimeOpen;
@property(nonatomic,copy)   NSString *todayTimeClosed;
@property(nonatomic,retain) NSNumber *td; // today closed 1 open 0 close
@property(nonatomic,retain) NSNumber *diningGuideCount;
@property(nonatomic,retain) NSNumber *eventCount;
@property(nonatomic,retain) NSNumber *journalCount;
@property(nonatomic,retain) NSNumber *listCount;

@property(nonatomic,copy)   NSString *timeline;
@property (nonatomic,strong) NSNumber *timelineId;
@property (nonatomic,strong) NSNumber *homeTimeLineId;
@property(nonatomic,retain) NSNumber *restaurantEventCount;

@property (nonatomic,copy) NSArray *landMarkList;
@property (nonatomic,copy) NSString *area;
@property(nonatomic,assign)BOOL isAd;
@property(nonatomic,assign)NSInteger adNum;
@property(nonatomic,assign)NSInteger adAllNums;
@property (nonatomic, copy) NSString *type;

@property (nonatomic, strong)  NSNumber  *instagramPhotoCount;
@property (nonatomic, strong)  NSNumber  *instagramLocationId;
/*
    new---------------------
*/

- (NSString *)address;
-(NSString *)distance;
-(NSString *)dispalyTimeAtWeekday:(NSInteger)currentWeekDay weekdayName:(NSString *)weekdayName;
-(NSArray *)openingHours;
- (BOOL)isOpenRestaurant;
/*
 banerImageList
 cuisineList
 dishList
 eventList
 imageUrlList
 mediaCommentList
 menuList
 restaurantAdList
 restaurantNearbyList
 
 restaurantOfferDetailList
 restaurantTabList
 reviewList
 sameCuisineRestaurantList
 sameDistrictRestaurantList
 signatureMenuList
 tagList
 timeSlotList
 
 
 location
 */

+(id)findById:(NSNumber*)restaurantId;

+ (id)findFirstInfo:(NSNumber *)restaurantID andRestaurant:(IMGRestaurant *)restaurant andNumForModules:(NSNumber *)modulesNum;
+(NSMutableArray*)commentListFromArray:(NSArray*)commentArray andIsVendor:(BOOL)isVendor andRestaurant:(IMGRestaurant *)restaurant;

@end
