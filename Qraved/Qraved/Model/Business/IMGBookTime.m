//
//  IMGBookTime.m
//  Qraved
//
//  Created by Libo Liu on 10/12/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "IMGBookTime.h"

@implementation IMGBookTime

-(int)getIntTime{
    NSString *tempStr = [self.timeStr stringByReplacingOccurrencesOfString:@":" withString:@""];
    return [tempStr intValue];
}

-(void)configType{
    int timeInt = self.getIntTime;
    self.timeType = [IMGBookTime getBookTimeType:timeInt];
}
+(IMGBookTimeType)getBookTimeType:(int)timeInt{
    IMGBookTimeType timeType;
    if (timeInt<1100&&timeInt>=600) {
        timeType = IMGBookTimeTypeBreakfast;
    }else if (timeInt<=1400&&timeInt>1000) {
        timeType = IMGBookTimeTypeLunch;
    }else if (timeInt<=2100&&timeInt>1700) {
        timeType = IMGBookTimeTypeDinner;
    }else if (timeInt>=2100&&timeInt>=0) {
        timeType = IMGBookTimeTypeLate;
    }else{
        timeType = IMGBookTimeTypeLate;
    }
    return timeType;
}
@end
