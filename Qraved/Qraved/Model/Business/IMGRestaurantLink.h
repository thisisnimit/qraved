//
//  IMGSetting.h
//  Qraved
//
//  Created by Jeff on 10/15/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGRestaurantLink : IMGEntity

@property(nonatomic,retain) NSNumber *restaurantId;
@property (nonatomic,copy)  NSString *url;
@end
