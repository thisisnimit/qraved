//
//  IMGRestaurantEvent.h
//  Qraved
//
//  Created by Olaf on 14/9/26.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGEntity.h"

@interface IMGRestaurantDiningGuide : IMGEntity
@property(nonatomic,retain) NSNumber *restaurantId;
@property(nonatomic,retain) NSNumber *diningGuideId;

@end
