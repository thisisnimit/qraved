//
//  IMGRestaurantOffer.h
//  Qraved
//
//  Created by Olaf on 14/9/26.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGRestaurantViewed : IMGEntity

+(id)findByRestaurantId:(NSNumber*)restaurantId;

@property(nonatomic,retain) NSNumber *restaurantId;
@property(nonatomic,copy)   NSString *title;
@property(nonatomic,copy)   NSString *districtName;

@end
