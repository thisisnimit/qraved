//
//  IMGDiningGuide.m
//  Qraved
//
//  Created by Laura on 14-9-19.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGDiningGuide.h"
#import "DBManager.h"
#import "NSString+Helper.h"

@implementation IMGDiningGuide

-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"id"]) {
        self.diningGuideId = value;
    }
}
+(id)findDiningGuideById:(NSNumber*)diningGuideId{
    IMGDiningGuide *diningGuide = [[IMGDiningGuide alloc] init];
    FMResultSet *resultSet = [[DBManager manager] executeQuery:[NSString stringWithFormat:@"select * from IMGDiningGuide where diningGuideId=%@",diningGuideId]];
    
    if([resultSet next]){
        [diningGuide setValueWithResultSet:resultSet];
        [resultSet close];
        return diningGuide;
    }else{
        [resultSet close];
        return nil;
    }
    
}
@end
