//
//  Dish.h
//  Qraved
//
//  Created by Shine Wang on 8/14/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGDish : IMGEntity

@property(nonatomic,retain) NSNumber *dishId;
@property(nonatomic,retain) NSNumber *restaurantId;
@property(nonatomic,retain) NSNumber *sectionId;
@property(nonatomic,retain) NSNumber *currencyId;
@property(nonatomic,retain) NSNumber *menuId;
@property(nonatomic,retain) NSNumber *userId;
@property(nonatomic,copy)   NSString *userName;
@property(nonatomic,retain) NSNumber *reviewId;
@property(nonatomic,retain) NSNumber *dish;
@property(nonatomic,retain) NSNumber *createTime;
@property(nonatomic,retain) NSNumber *price;
@property(nonatomic,retain) NSNumber *status;
@property(nonatomic,copy)   NSString *restaurantTitle;
@property(nonatomic,copy)   NSString *restaurantSeo;
@property(nonatomic,copy)   NSString *descriptionStr;


@property(nonatomic,retain) NSNumber *likeCount;
@property(nonatomic,retain) NSNumber *commentCount;

@property(nonatomic,retain) NSMutableArray *commentList;
@property(nonatomic,assign) BOOL isLike;
@property(nonatomic,assign) BOOL showWriteReview;
//notification with dish
@property(nonatomic,copy)   NSString *dishType;
@property(nonatomic,retain) NSNumber *landingObjectId;
@property (nonatomic,assign) BOOL isRestaurantPhoto;

@property (nonatomic,strong) NSNumber *imageHeight;
@property(nonatomic,retain)NSNumber* moderateReviewId;

@property(nonatomic,retain) NSNumber *userPhotoCountDic;
@property(nonatomic,copy)   NSString *photoCreditTypeDic;
@property(nonatomic,retain) NSNumber *userIdDic;
@property(nonatomic,copy)   NSString *photoCreditDic;
@property(nonatomic,retain) NSNumber *userReviewCountDic;
@property(nonatomic,copy)   NSString *userAvatarDic;
@property(nonatomic,copy)   NSString *photoCreditUrlDic;
@property(nonatomic,retain) NSNumber *restaurantIdDic;
@property(nonatomic,copy)   NSString *restaurantTitleDic;
@property(nonatomic,strong) NSNumber *type;
@property(nonatomic,strong) NSString *instagramLink;
@property(nonatomic,assign)float JournalGifH;
@property(nonatomic,assign)float JournalGifW;
@property(nonatomic,copy)NSString *lowResolutionImage;
@property(nonatomic,copy)NSString *thumbnailImage;
@property(nonatomic,copy)NSString *standardResolutionImage;

-(CGSize)getDishImageSize;
-(void)updatePhotoCredit:(NSDictionary*)dic;
@end
