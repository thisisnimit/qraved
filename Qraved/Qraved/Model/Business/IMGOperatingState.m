//
//  IMGSetting.m
//  Qraved
//
//  Created by Jeff on 10/15/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGOperatingState.h"
#import "DBManager.h"

@implementation IMGOperatingState

+(id)findByStateId:(NSNumber*)stateId{
    IMGOperatingState *operatingState = [[IMGOperatingState alloc] init];
    FMResultSet *resultSet = [[DBManager manager] executeQuery:[NSString stringWithFormat:@"select * from IMGOperatingState where stateId=%@",stateId]];
    if([resultSet next]){
        [operatingState setValueWithResultSet:resultSet];
        [resultSet close];
        return operatingState;
    }else{
        [resultSet close];
        return nil;
    }
    
}

@end
