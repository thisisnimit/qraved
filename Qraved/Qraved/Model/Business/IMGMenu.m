//
//  IMGMenu.m
//  Qraved
//
//  Created by Olaf on 14/9/26.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGMenu.h"
#import "NSString+Helper.h"

@implementation IMGMenu{
	NSString *_title;
}

@synthesize title=_title;

-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"description"]) {
        self.descriptionStr = value;
    }
    if ([key isEqualToString:@"id"]) {
        self.menuId = value;
    }
}

-(void)setValuesForKeysWithDictionary:(NSDictionary *)keyedValues{
    self.createTimeLong = [[keyedValues objectForKey:@"createTime"] longLongValue];
    [super setValuesForKeysWithDictionary:keyedValues];
}

-(void)setTitle:(NSString*)title{
	NSString *tmp=[title copy];
    _title=[tmp filterHtml];
}

-(void)setDescriptionStr:(NSString*)description{
    NSString *tmp=[description copy];
    if([tmp isBlankString]){
        _descriptionStr=@"";
    }else{
        _descriptionStr=[tmp filterHtml];
    }
}
@end
