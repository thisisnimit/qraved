//
//  IMGRestaurantEvent.h
//  Qraved
//
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGEntity.h"

@interface IMGRestaurantEvent : IMGEntity
@property(nonatomic,retain) NSNumber *restaurantId;
@property(nonatomic,retain) NSNumber *eventId;
@property(nonatomic,retain) NSNumber *sectionId;
@property(nonatomic,retain) NSNumber *currencyId;
@property(nonatomic,retain) NSNumber *menuId;
@property(nonatomic,retain) NSNumber *reviewId;
@property(nonatomic,retain) NSNumber *dish;
@property(nonatomic,retain) NSNumber *createTime;
@property(nonatomic,retain) NSNumber *price;
@property(nonatomic,retain) NSNumber *status;
@property(nonatomic,copy)   NSString *restaurantTitle;
@property(nonatomic,copy)   NSString *restaurantSeo;
@property(nonatomic,copy)   NSString *descriptionStr;

@property(nonatomic,retain) NSNumber *likeCount;
@property(nonatomic,retain) NSNumber *commentCount;

@property(nonatomic,retain) NSMutableArray *commentList;
@property(nonatomic,retain)NSNumber* homeTimelineRestaurantUpdateId;
@property(nonatomic,assign) BOOL isLike;

@property (nonatomic,strong) NSNumber *imageHeight;
@property(nonatomic,retain)NSNumber* homeTimeLineId;

@property(nonatomic,retain) NSNumber *userPhotoCountDic;
@property(nonatomic,copy)   NSString *photoCreditTypeDic;
@property(nonatomic,retain) NSNumber *userIdDic;
@property(nonatomic,copy)   NSString *photoCreditDic;
@property(nonatomic,retain) NSNumber *userReviewCountDic;
@property(nonatomic,copy)   NSString *userAvatarDic;
@property(nonatomic,copy)   NSString *photoCreditUrlDic;
@property(nonatomic,retain) NSNumber *restaurantIdDic;

+(BOOL)checkByEventId:(NSNumber *)eventId andRestaurantId:(NSNumber *)restaurnatId;
-(CGSize)getDishImageSize;
-(void)updatePhotoCredit:(NSDictionary*)dic;
@end
