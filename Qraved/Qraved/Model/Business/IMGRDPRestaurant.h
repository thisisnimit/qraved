//
//  IMGRDPRestaurant.h
//  Qraved
//
//  Created by lucky on 16/6/7.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"
#import "IMGRestaurant.h"
#import "IMGRestaurantOffer.h"

@interface IMGRDPRestaurant : IMGEntity

@property(nonatomic,retain) NSNumber *restaurantId;
@property(nonatomic,retain) NSDictionary *RDPFirstDataArr;

@property(nonatomic,retain) NSArray *menuList;
@property(nonatomic,retain) NSArray *dishList;
@property(nonatomic,retain) NSArray *moreDishList;
@property(nonatomic,retain) NSMutableArray *reviewList;
@property(nonatomic,retain) NSArray *eventList;
@property(nonatomic,retain) NSArray *diningGuideList;
@property(nonatomic,retain) NSArray *journalList;

@property(nonatomic,retain) NSNumber *menuPhotoCount;
@property(nonatomic,retain) NSNumber *dishCount;
@property(nonatomic,retain) NSNumber *eventCount;
@property(nonatomic,retain) NSNumber *diningGuideCount;
@property(nonatomic,retain) NSNumber *journalCount;

@property(nonatomic,assign) NSNumber *isHadRDPSecondData;

@property(nonatomic,copy)   NSString *ratingCounts;
@property(nonatomic,copy)   NSString *ratingScore;
@property(nonatomic,retain) NSNumber *reviewCount;
@property (nonatomic, strong)  NSNumber  *instagramLocationId;

//@property(nonatomic,retain) IMGRestaurant *restaurant;
//@property(nonatomic,strong) IMGRestaurantOffer *restaurantOffer;
//@property(nonatomic,retain) NSMutableArray* eventList;
//@property(nonatomic,retain) NSMutableArray* banerImageArr;

-(id)initWithRestId:(NSNumber *)restaurantId andRDPFirstDataArr:(NSArray *)RDPFirstDataArr andMenuList:(NSArray *)menuList andDishList:(NSArray *)dishList andMoreDishList:(NSArray *)moreDishList andReviewList:(NSArray *)reviewList andEventList:(NSArray *)eventList andDiningGuideList:(NSArray *)diningGuideList andJournalList:(NSArray *)journalList andMenuPhotoCount:(NSNumber *)menuPhotoCount andDishCount:(NSNumber *)dishCount andEventCount:(NSNumber *)eventCount andDiningGuideCount:(NSNumber *)diningGuideCount andJournalCount:(NSNumber *)journalCount andIsHadRDPSecondData:(NSNumber *)isHadRDPSecondData andRestRatingCounts:(NSString *)ratingCounts andRestRatingScore:(NSString *)ratingScore andReviewCount:(NSNumber *)reviewCount;
@end
