//
//  IMGBookTime.h
//  Qraved
//
//  Created by Libo Liu on 10/12/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

typedef enum {
    IMGBookTimeTypeBreakfast = 1,
    IMGBookTimeTypeLunch = 2,
    IMGBookTimeTypeDinner = 3,
    IMGBookTimeTypeLate = 4,
} IMGBookTimeType;

@interface IMGBookTime : IMGEntity
@property(nonatomic,assign) BOOL avaliable;
/*old*/
@property(nonatomic,copy)NSString *timeStr;
@property(nonatomic,copy)NSString *saveStr;
@property(nonatomic,retain)NSNumber *maxPeopleCount;
@property(nonatomic)IMGBookTimeType timeType;

-(int)getIntTime;
-(void)configType;
+(IMGBookTimeType)getBookTimeType:(int)timeInt;

@end
