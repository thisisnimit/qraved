//
//  IMGRestaurantOffer.h
//  Qraved
//
//  Created by Olaf on 14/9/26.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGRestaurantOffer : IMGEntity
@property(nonatomic,retain) NSNumber *offerId;
@property(nonatomic,retain) NSNumber *restaurantId;
@property(nonatomic,copy)   NSString *offerDescription;
@property(nonatomic,copy)   NSString *offerEndDate;
@property(nonatomic,copy)   NSString *offerEndTime;
@property(nonatomic,copy)   NSString *offerImage;
@property(nonatomic,copy)   NSString *offerMenu;
@property(nonatomic,copy)   NSNumber *offerSlotId;
@property(nonatomic,copy)   NSString *offerSlotList;
@property(nonatomic,copy)   NSNumber *offerSlotMaxDisc;
@property(nonatomic,copy)   NSNumber *offerSlotMinDisc;
@property(nonatomic,copy)   NSString *offerStartDate;
@property(nonatomic,copy)   NSString *offerStartTime;
@property(nonatomic,copy)   NSString *offerTerms;
@property(nonatomic,copy)   NSString *offerTitle;
@property(nonatomic,copy)   NSNumber *offerType;
@property(nonatomic,copy)   NSString *offerUrl;
@property(nonatomic,copy)   NSString *tagLine;
@property(nonatomic,copy)   NSString *typeIcon;
@property (nonatomic, strong)  NSString  *imageUrl;
@property(nonatomic,retain) NSNumber *offerImageH;
@property(nonatomic,assign)   BOOL hasNewOfferSign;
@property(nonatomic,assign)   BOOL requireBooking;

+(id)findById:(NSNumber*)offerId;

@end
