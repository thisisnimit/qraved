//
//  Dish.m
//  Qraved
//
//  Created by Shine Wang on 8/14/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "IMGDish.h"
#import "Tools.h"
#import "UserComment.h"
#import "NSString+Helper.h"
#import "IMGDishComment.h"

@implementation IMGDish

- (instancetype)init
{
    self = [super init];
    if (self) {
        if (!self.moderateReviewId)
        {
            self.moderateReviewId = [NSNumber numberWithInt:0];
        }
    }
    return self;
}
-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if([key isEqualToString:@"id"]){
        self.dishId=value;
    }else if([key isEqualToString:@"photoCreditImpl"] || [key isEqualToString:@"photoCredit"]) {
        [self updatePhotoCredit:value];
    }else if ([key isEqualToString:@"caption"]){
        self.descriptionStr = value;
    }

}

-(void)setDescriptionStr:(NSString*)descriptionStr{
    if((![descriptionStr isEqual:[NSNull null]]) && descriptionStr && descriptionStr.length){
        _descriptionStr=[descriptionStr filterHtml];
    }else{
        _descriptionStr=@"";
    }
}

-(CGSize)getDishImageSize{
    NSString *urlStr = self.imageUrl;
    NSString *frameStrWithHou = [[urlStr componentsSeparatedByString:@"-"]lastObject];
    float qraveWidth=305.0/2;
    float width = qraveWidth;
    float height;
    float scale;
    
    long widthOriginal;
    long heightOriginal;
    
    NSString *frameStr = [[frameStrWithHou componentsSeparatedByString:@"."]objectAtIndex:0];
    NSRange r = [urlStr rangeOfString:@"-"];
    
    
    if (r.length != 0) {
        widthOriginal = [[[frameStr componentsSeparatedByString:@"x"]objectAtIndex:0] integerValue];
        heightOriginal = [[[frameStr componentsSeparatedByString:@"x"]lastObject] integerValue];
        
        if(heightOriginal==0)
        {
            height=qraveWidth;
        }
        else
        {
            scale = (float)widthOriginal / qraveWidth;
            height = (float)heightOriginal / scale;
        }
    } else {
        
        height = qraveWidth;
    }
    
    if(height==NAN)
    {
        height=qraveWidth;
    }
    if (r.length == 0) {
        height = width;
    }
    return CGSizeMake(width, height);
}

-(void)updatePhotoCredit:(NSDictionary*)dic{
    if (dic != nil) {
        self.userPhotoCountDic = [dic objectForKey:@"userPhotoCount"];
        self.photoCreditTypeDic  = [dic objectForKey:@"photoCreditType"];
        self.userIdDic  = [dic objectForKey:@"userId"];
        self.photoCreditDic  = [dic objectForKey:@"photoCredit"];
        self.userReviewCountDic  = [dic objectForKey:@"userReviewCount"];
        self.userAvatarDic  = [dic objectForKey:@"userAvatar"];
        self.photoCreditUrlDic  = [dic objectForKey:@"photoCreditUrl"];
        self.restaurantIdDic  = [dic objectForKey:@"restaurantId"];
    }
}

@end
