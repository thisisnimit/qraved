//
//  IMGMediaComment.h
//  Qraved
//
//  Created by Jeff on 10/14/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGEntity.h"
#import "IMGUser.h"

@interface IMGMediaComment : IMGEntity

@property(nonatomic,retain) NSNumber *mediaCommentId;
@property(nonatomic,retain) NSNumber *restaurantId;
@property(nonatomic,retain) NSString *restaurantTitle;
@property(nonatomic,retain) NSString *source;
@property(nonatomic,retain) NSString *link;
@property(nonatomic,retain) NSString *contents;
@property(nonatomic,retain) NSString *createdDate;
@property(nonatomic,retain) NSString *createTimeUnixTimestamp;
@property(nonatomic,retain) NSString *mediaCommentDate;
@property(nonatomic,retain) NSNumber *status;
@property(nonatomic,retain) NSNumber *sortOrder;
@property(nonatomic,retain) NSNumber *viewCount;
@property(nonatomic,retain) NSString *postTime;

@property(nonatomic,retain) NSString *categoryName;
@property(nonatomic,retain) NSNumber *shareCount;
@property(nonatomic,retain) NSNumber *imported;
@property(nonatomic,retain) NSString *journalTitle;
@property(nonatomic,retain) NSString *journalImageUrl;
@property(nonatomic,retain) NSNumber *hasVideo;

@property(nonatomic,retain) NSString *authorName;
@property(nonatomic,retain) NSString *authorDescritpion;
@property(nonatomic,retain) NSString *authorPhoto;
@property(nonatomic,retain) NSNumber *likeCount;
@property(nonatomic,retain) NSNumber *commentCount;
@property(nonatomic,retain) NSNumber *listCount;
@property(nonatomic,retain) NSString *typeName;
@property(nonatomic,assign)float JournalGifH;
@property(nonatomic,assign)float JournalGifW;


@property(nonatomic,assign) BOOL isLike;

@property(nonatomic)        IMGUser *photoCreditUser;
@property(nonatomic,retain) NSMutableArray *commentList;

@property(nonatomic,retain) NSNumber *userPhotoCountDic;
@property(nonatomic,copy)   NSString *photoCreditTypeDic;
@property(nonatomic,retain) NSNumber *userIdDic;
@property(nonatomic,copy)   NSString *photoCreditDic;
@property(nonatomic,retain) NSNumber *userReviewCountDic;
@property(nonatomic,copy)   NSString *userAvatarDic;
@property(nonatomic,copy)   NSString *photoCreditUrlDic;
@property(nonatomic,retain) NSNumber *restaurantIdDic;

+(id)findById:(NSNumber*)mediaCommentId;
+(NSMutableArray*)commentListFromArray:(NSArray*)commentArray;
-(void)updatePhotoCredit:(NSMutableDictionary*)dic;
@end
