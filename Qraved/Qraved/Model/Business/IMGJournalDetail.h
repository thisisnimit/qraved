//
//  IMGJournalDetail.h
//  Qraved
//
//  Created by Lucky on 15/7/26.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGJournalDetail : IMGEntity

@property (nonatomic) NSNumber *journalDetailTag;

@property (nonatomic) NSString *photoImage;
@property (nonatomic) NSString *url;

@property (nonatomic) NSString *titleLabel;

@property (nonatomic) NSString *contentLabel;

@property (nonatomic) NSString *buttonContent;

@property (nonatomic) NSString *credit;

@property (nonatomic) NSString *creditUrl;

@property (nonatomic) NSNumber *restaurantId;

@end
