//
//  IMGJournalSearch.h
//  Qraved
//
//  Created by Lucky on 15/7/30.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGJournalCategory : IMGEntity

@property (nonatomic) NSString *categoryName;
@property (nonatomic) NSNumber *categoryType;
@property (nonatomic) NSNumber *categoryOrder;


@end
