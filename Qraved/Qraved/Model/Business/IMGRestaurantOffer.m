//
//  IMGRestaurantOffer.m
//  Qraved
//
//  Created by Olaf on 14/9/26.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGRestaurantOffer.h"
#import "DBManager.h"

@implementation IMGRestaurantOffer

+(id)findById:(NSNumber*)offerId{
    IMGRestaurantOffer *restaurantOffer = [[IMGRestaurantOffer alloc] init];
    FMResultSet *resultSet = [[DBManager manager] executeQuery:[NSString stringWithFormat:@"select * from IMGRestaurantOffer where offerId=%@",offerId]];
    if([resultSet next]){
        [restaurantOffer setValueWithResultSet:resultSet];
        [resultSet close];
        return restaurantOffer;
    }else{
        [resultSet close];
        return nil;
    }
    
}

@end
