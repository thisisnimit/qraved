//
//  IMGRestaurant.m
//  Qraved
//
//  Created by Jeff on 8/12/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGRestaurant.h"
#import "AppDelegate.h"
#import "NSString+Helper.h"
#import <CoreLocation/CoreLocation.h>
#import "IMGMenu.h"

@implementation IMGRestaurant{
    NSString *_title;
}

@synthesize title=_title;

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if([key isEqualToString:@"id"]){
        self.restaurantId=value;
    }
    if([key isEqualToString:@"discount"]){
        self.disCount=value;
    }
    if ([key isEqualToString:@"description"]) {
        self.descriptionStr = value;
    }
    
}
-(void)setValuesForKeysWithDictionary:(NSDictionary *)keyedValues{
    [super setValuesForKeysWithDictionary:keyedValues];
}

- (NSString *)address
{
    if(self.address1!=nil && ![self.address1 isEqual:[NSNull null]] && ![self.address1 isEqualToString:@"type address"] && self.address2!=nil &&  ![self.address2 isEqual:[NSNull null]] && ![self.address2 isEqualToString:@"type address"]){
        return [[self.address1 stringByAppendingString:@", "] stringByAppendingString:self.address2];
    }else if(self.address1!=nil && ![self.address1 isEqual:[NSNull null]] && ![self.address1 isEqualToString:@"type address"]){
        return self.address1;
    }else if(self.address2!=nil && ![self.address2 isEqual:[NSNull null]] && ![self.address2 isEqualToString:@"type address"]){
        return self.address2;
    }else{
        return nil;
    }
}

-(void)setTitle:(NSString*)title{
    NSString *tmp=[title copy];
    _title=[tmp filterHtml];
}

-(void)setCuisineName:(NSString*)cuisine{
    NSString *tmp=[cuisine copy];
    _cuisineName=[tmp filterHtml];
}

-(void)setDistrictName:(NSString*)district{

        NSString *tmp=[district copy];
        
        _districtName=[tmp filterHtml];
    
   // _districtName=[tmp filterHtml];
}

-(NSString *)distance1{
    double lon2=[AppDelegate ShareApp].locationManager.location.coordinate.longitude;
    double lat2=[AppDelegate ShareApp].locationManager.location.coordinate.latitude;
    NSLog(@"-(NSString *)longitude1{:%f",lon2);
    NSLog(@"-(NSString *)longitude2{:%f",[self.longitude doubleValue]);
    
    NSLog(@"-(NSString *)latitude1{:%f",lat2);
    NSLog(@"-(NSString *)latitude2{:%f",[self.latitude doubleValue]);
    double er = 6378137; // 6378700.0f;
    //ave. radius = 6371.315 (someone said more accurate is 6366.707)
    //equatorial radius = 6378.388
    //nautical mile = 1.15078
    double radlat1 = M_PI*[self.latitude doubleValue]/180.0f;
    double radlat2 = M_PI*lat2/180.0f;
    //now long.
    double radlong1 = M_PI*[self.longitude doubleValue]/180.0f;
    double radlong2 = M_PI*lon2/180.0f;
    if( radlat1 < 0 ) radlat1 = M_PI/2 + fabs(radlat1);// south
    if( radlat1 > 0 ) radlat1 = M_PI/2 - fabs(radlat1);// north
    if( radlong1 < 0 ) radlong1 = M_PI*2 - fabs(radlong1);//west
    if( radlat2 < 0 ) radlat2 = M_PI/2 + fabs(radlat2);// south
    if( radlat2 > 0 ) radlat2 = M_PI/2 - fabs(radlat2);// north
    if( radlong2 < 0 ) radlong2 = M_PI*2 - fabs(radlong2);// west
    //spherical coordinates x=r*cos(ag)sin(at), y=r*sin(ag)*sin(at), z=r*cos(at)
    //zero ag is up so reverse lat
    double x1 = er * cos(radlong1) * sin(radlat1);
    double y1 = er * sin(radlong1) * sin(radlat1);
    double z1 = er * cos(radlat1);
    double x2 = er * cos(radlong2) * sin(radlat2);
    double y2 = er * sin(radlong2) * sin(radlat2);
    double z2 = er * cos(radlat2);
    double d = sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2));
    //side, side, side, law of cosines and arccos
    double theta = acos((er*er+er*er-d*d)/(2*er*er));
    double distance  = theta*er;
    if(distance < 1000){
        return [[NSString alloc] initWithFormat:@"%dm",[[NSNumber numberWithDouble:distance] intValue]];
    }else{
        double kmDistance = distance/1000.0;
        if(kmDistance>1000){
            return [[NSString alloc] initWithFormat:@"%.0fkm",kmDistance];
        }else{
            return [[NSString alloc] initWithFormat:@"%.2fkm",kmDistance];
        }
    }
}

-(NSString *)distance{
//    if([self.latitude intValue]==0 || [self.longitude intValue]==0){
//        return @"";
//    }
   
//    double lon2=[AppDelegate ShareApp].locationManager.location.coordinate.longitude;
//    double lat2=[AppDelegate ShareApp].locationManager.location.coordinate.latitude;
    if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied){
        return @"";
    }
    double lon2=[[[NSUserDefaults standardUserDefaults]objectForKey:@"longitude"] doubleValue];
    double lat2=[[[NSUserDefaults standardUserDefaults]objectForKey:@"latitude"] doubleValue];
    NSLog(@"%f,%f",lon2,lat2);
    double dd = M_PI/180;
    double x1=[self.latitude doubleValue]*dd,x2=lat2 *dd;
    double y1=[self.longitude doubleValue]*dd,y2=lon2 *dd;
    double R = 6378138;
    double distance = (2*R*asin(sqrt(2-2*cos(x1)*cos(x2)*cos(y1-y2) - 2*sin(x1)*sin(x2))/2));
//    NSLog(@"distance is less than 0,%f",distance);

    if(distance < 1000){
        return [[NSString alloc] initWithFormat:@"%dm",[[NSNumber numberWithDouble:distance] intValue]];
    }else if(distance > 8000*1000){
        return @"";
    }else{
        long kmDistance = (long)distance/1000;
        return [[NSString alloc] initWithFormat:@"%ldkm",kmDistance];
    }
}
-(NSArray *)openingHours{
    NSMutableArray *openingHours = [[NSMutableArray alloc]init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"hh:mm"];
    NSArray *array = @[@2,@3,@4,@5,@6,@7,@1];
    for (int i=0; i<array.count; i++) {
        NSInteger index = [[array objectAtIndex:i] integerValue];
        
        switch (index) {
            
            case 2:
//            {
//                NSDate *openDate = [dateFormatter dateFromString:self.monTimeOpen];
//                NSDate *closeDate = [dateFormatter dateFromString:self.monTimeClosed];
//                if (([self.monIntermission isKindOfClass:[NSString class]] && self.monIntermission.length > 0) || ![openDate isEqualToDate:closeDate]) {
                    [openingHours addObject:[self dispalyTimeAtWeekday:index weekdayName:@"Monday"]];
//                }
//            }
                break;
            case 3:
//            {
//                NSDate *openDate = [dateFormatter dateFromString:self.tueTimeOpen];
//                NSDate *closeDate = [dateFormatter dateFromString:self.tueTimeClosed];
//                if (([self.tueIntermission isKindOfClass:[NSString class]] && self.tueIntermission.length > 0) || ![openDate isEqualToDate:closeDate]) {
                    [openingHours addObject:[self dispalyTimeAtWeekday:index weekdayName:@"Tuesday"]];
//                }
//            }
                break;
            case 4:
//            {
//                NSDate *openDate = [dateFormatter dateFromString:self.wedTimeOpen];
//                NSDate *closeDate = [dateFormatter dateFromString:self.wedTimeClosed];
//                if (([self.wedIntermission isKindOfClass:[NSString class]] && self.wedIntermission.length > 0) || ![openDate isEqualToDate:closeDate]) {
                    [openingHours addObject:[self dispalyTimeAtWeekday:index weekdayName:@"Wednesday"]];
//                }
//            }
                break;
            case 5:
//            {
//                NSDate *openDate = [dateFormatter dateFromString:self.thuTimeOpen];
//                NSDate *closeDate = [dateFormatter dateFromString:self.thuTimeClosed];
//                if (([self.thuIntermission isKindOfClass:[NSString class]] && self.thuIntermission.length > 0) || ![openDate isEqualToDate:closeDate]) {
                    [openingHours addObject:[self dispalyTimeAtWeekday:index weekdayName:@"Thursday"]];
//                }
//            }
                break;
            case 6:
//            {
//                NSDate *openDate = [dateFormatter dateFromString:self.friTimeOpen];
//                NSDate *closeDate = [dateFormatter dateFromString:self.friTimeClosed];
//                if (([self.friIntermission isKindOfClass:[NSString class]] && self.friIntermission.length > 0) || ![openDate isEqualToDate:closeDate]) {
                    [openingHours addObject:[self dispalyTimeAtWeekday:index weekdayName:@"Friday"]];
//                }
//            }
                break;
            case 7:
//            {
//                NSDate *openDate = [dateFormatter dateFromString:self.satTimeOpen];
//                NSDate *closeDate = [dateFormatter dateFromString:self.satTimeClosed];
//                if (([self.satIntermission isKindOfClass:[NSString class]] && self.satIntermission.length > 0) || ![openDate isEqualToDate:closeDate]) {
                    [openingHours addObject:[self dispalyTimeAtWeekday:index weekdayName:@"Saturday"]];
//                }
//            }
                break;
            case 1:
//            {
//                NSDate *openDate = [dateFormatter dateFromString:self.sunTimeOpen];
//                NSDate *closeDate = [dateFormatter dateFromString:self.sunTimeClosed];
//                if (([self.sunIntermission isKindOfClass:[NSString class]] && self.sunIntermission.length > 0) || ![openDate isEqualToDate:closeDate]) {
                    [openingHours addObject:[self dispalyTimeAtWeekday:index weekdayName:@"Sunday"]];
//                }
//                
//            }
                break;
            default:
                break;
        }
        
        
    }
    return openingHours;
}

-(NSString *)dispalyTimeAtWeekday:(NSInteger)currentWeekDay weekdayName:(NSString *)weekdayName{
    switch (currentWeekDay) {
        case 2:
        {
            if ([self.monIntermission isKindOfClass:[NSString class]] && self.monIntermission.length > 0) {
                NSArray *intermissionArray = [self.monIntermission componentsSeparatedByString:@" - "];
                return [NSString stringWithFormat:@"%@: %@ - %@, %@ - %@",weekdayName,self.monTimeOpen,[intermissionArray objectAtIndex:0],[intermissionArray objectAtIndex:1],self.monTimeClosed];
            }else{
                NSString *displayTime = [NSString stringWithFormat:@"%@: %@ - %@",weekdayName,self.monTimeOpen,self.monTimeClosed];
                if(self.d1!=nil && ![self.d1 isKindOfClass:[NSNull class]] && [self.d1 intValue]==1){
                    if(([displayTime isEqualToString:[NSString stringWithFormat:@"%@: 00:00 - 00:00",weekdayName]]||[displayTime isEqualToString:[NSString stringWithFormat:@"%@: 0 - 0",weekdayName]])){
                        displayTime=[NSString stringWithFormat:@"%@: 24 Hours Open",weekdayName];
                    }
                }else{
                    displayTime=[NSString stringWithFormat:@"%@: Closed",weekdayName];
                }
                return displayTime;
            }
        }
            break;
        case 3:
        {
            if ([self.tueIntermission isKindOfClass:[NSString class]] && self.tueIntermission.length > 0) {
                NSArray *intermissionArray = [self.tueIntermission componentsSeparatedByString:@" - "];
                return [NSString stringWithFormat:@"%@: %@ - %@, %@ - %@",weekdayName,self.tueTimeOpen,[intermissionArray objectAtIndex:0],[intermissionArray objectAtIndex:1],self.tueTimeClosed];
            }else{
                NSString *displayTime =[NSString stringWithFormat:@"%@: %@ - %@",weekdayName,self.tueTimeOpen,self.tueTimeClosed];
                if(self.d2!=nil && ![self.d2 isKindOfClass:[NSNull class]] && [self.d2 intValue]==1){
                    if(([displayTime isEqualToString:[NSString stringWithFormat:@"%@: 00:00 - 00:00",weekdayName]]||[displayTime isEqualToString:[NSString stringWithFormat:@"%@: 0 - 0",weekdayName]])){
                        displayTime=[NSString stringWithFormat:@"%@: 24 Hours Open",weekdayName];
                    }
                }else{
                    displayTime=[NSString stringWithFormat:@"%@: Closed",weekdayName];
                }
                return displayTime;
            }
        }
            break;
        case 4:
        {
            if ([self.wedIntermission isKindOfClass:[NSString class]] && self.wedIntermission.length > 0) {
                NSArray *intermissionArray = [self.wedIntermission componentsSeparatedByString:@" - "];
                return [NSString stringWithFormat:@"%@: %@ - %@, %@ - %@",weekdayName,self.wedTimeOpen,[intermissionArray objectAtIndex:0],[intermissionArray objectAtIndex:1],self.wedTimeClosed];
            }else{
                NSString *displayTime =[NSString stringWithFormat:@"%@: %@ - %@",weekdayName,self.wedTimeOpen,self.wedTimeClosed];
                if(self.d3!=nil && ![self.d3 isKindOfClass:[NSNull class]] && [self.d3 intValue]==1){
                    if(([displayTime isEqualToString:[NSString stringWithFormat:@"%@: 00:00 - 00:00",weekdayName]]||[displayTime isEqualToString:[NSString stringWithFormat:@"%@: 0 - 0",weekdayName]])){
                        displayTime=[NSString stringWithFormat:@"%@: 24 Hours Open",weekdayName];
                    }
                }else{
                    displayTime=[NSString stringWithFormat:@"%@: Closed",weekdayName];
                }
                return displayTime;
            }
        }
            break;
        case 5:
        {
            if ([self.thuIntermission isKindOfClass:[NSString class]] && self.thuIntermission.length > 0) {
                NSArray *intermissionArray = [self.thuIntermission componentsSeparatedByString:@" - "];
                return [NSString stringWithFormat:@"%@: %@ - %@, %@ - %@",weekdayName,self.thuTimeOpen,[intermissionArray objectAtIndex:0],[intermissionArray objectAtIndex:1],self.thuTimeClosed];
            }else{
                NSString *displayTime =[NSString stringWithFormat:@"%@: %@ - %@",weekdayName,self.thuTimeOpen,self.thuTimeClosed];
                if(self.d4!=nil && ![self.d4 isKindOfClass:[NSNull class]] && [self.d4 intValue]==1){
                    if(([displayTime isEqualToString:[NSString stringWithFormat:@"%@: 00:00 - 00:00",weekdayName]]||[displayTime isEqualToString:[NSString stringWithFormat:@"%@: 0 - 0",weekdayName]])){
                        displayTime=[NSString stringWithFormat:@"%@: 24 Hours Open",weekdayName];
                    }
                }else{
                    displayTime=[NSString stringWithFormat:@"%@: Closed",weekdayName];
                }
                return displayTime;
            }
        }
            break;
        case 6:
        {
            if ([self.friIntermission isKindOfClass:[NSString class]] && self.friIntermission.length > 0) {
                NSArray *intermissionArray = [self.friIntermission componentsSeparatedByString:@" - "];
                return [NSString stringWithFormat:@"%@: %@ - %@, %@ - %@",weekdayName,self.friTimeOpen,[intermissionArray objectAtIndex:0],[intermissionArray objectAtIndex:1],self.friTimeClosed];
            }else{
                NSString *displayTime =[NSString stringWithFormat:@"%@: %@ - %@",weekdayName,self.friTimeOpen,self.friTimeClosed];
                if(self.d5!=nil && ![self.d5 isKindOfClass:[NSNull class]] && [self.d5 intValue]==1){
                    if(([displayTime isEqualToString:[NSString stringWithFormat:@"%@: 00:00 - 00:00",weekdayName]]||[displayTime isEqualToString:[NSString stringWithFormat:@"%@: 0 - 0",weekdayName]])){
                        displayTime=[NSString stringWithFormat:@"%@: 24 Hours Open",weekdayName];
                    }
                }else{
                    displayTime=[NSString stringWithFormat:@"%@: Closed",weekdayName];
                }
                return displayTime;
            }
        }
            break;
        case 7:
        {
            if ([self.satIntermission isKindOfClass:[NSString class]] && self.satIntermission.length > 0) {
                NSArray *intermissionArray = [self.satIntermission componentsSeparatedByString:@" - "];
                return [NSString stringWithFormat:@"%@: %@ - %@, %@ - %@",weekdayName,self.satTimeOpen,[intermissionArray objectAtIndex:0],[intermissionArray objectAtIndex:1],self.satTimeClosed];
            }else{
                NSString *displayTime =[NSString stringWithFormat:@"%@: %@ - %@",weekdayName,self.satTimeOpen,self.satTimeClosed];
                if(self.d6!=nil && ![self.d6 isKindOfClass:[NSNull class]] && [self.d6 intValue]==1){
                    if(([displayTime isEqualToString:[NSString stringWithFormat:@"%@: 00:00 - 00:00",weekdayName]]||[displayTime isEqualToString:[NSString stringWithFormat:@"%@: 0 - 0",weekdayName]])){
                        displayTime=[NSString stringWithFormat:@"%@: 24 Hours Open",weekdayName];
                    }
                }else{
                    displayTime=[NSString stringWithFormat:@"%@: Closed",weekdayName];
                }
                return displayTime;
            }
        }
            break;
        case 1:
        {
            if ([self.sunIntermission isKindOfClass:[NSString class]] && self.sunIntermission.length > 0) {
                NSArray *intermissionArray = [self.sunIntermission componentsSeparatedByString:@" - "];
                NSLog(@"dispalyTime = %@",[NSString stringWithFormat:@"%@: %@ - %@, %@ - %@",weekdayName,self.sunTimeOpen,[intermissionArray objectAtIndex:0],[intermissionArray objectAtIndex:1],self.sunTimeClosed]);
                return [NSString stringWithFormat:@"%@: %@ - %@, %@ - %@",weekdayName,self.sunTimeOpen,[intermissionArray objectAtIndex:0],[intermissionArray objectAtIndex:1],self.sunTimeClosed];
            }else{
                NSString *displayTime =[NSString stringWithFormat:@"%@: %@ - %@",weekdayName,self.sunTimeOpen,self.sunTimeClosed];
                if(self.d7!=nil && ![self.d7 isKindOfClass:[NSNull class]] && [self.d7 intValue]==1){
                    if(([displayTime isEqualToString:[NSString stringWithFormat:@"%@: 00:00 - 00:00",weekdayName]]||[displayTime isEqualToString:[NSString stringWithFormat:@"%@: 0 - 0",weekdayName]])){
                        displayTime=[NSString stringWithFormat:@"%@: 24 Hours Open",weekdayName];
                    }
                }else{
                    displayTime=[NSString stringWithFormat:@"%@: Closed",weekdayName];
                }
                return displayTime;
            }
        }
            break;
        default:
            return nil;
            break;
    }
}

+(id)findById:(NSNumber*)restaurantId{
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];

    
    FMResultSet *resultSet = [[DBManager manager] executeQuery:[NSString stringWithFormat:@"select * from IMGRestaurant where restaurantId=%@",restaurantId]];

    if([resultSet next]){
        [restaurant setValueWithResultSet:resultSet];
        [resultSet close];
        return restaurant;
    }else{
        [resultSet close];
        restaurant.restaurantId = restaurantId;
        return restaurant;
    }
    
}



+ (id)findFirstInfo:(NSNumber *)restaurantID andRestaurant:(IMGRestaurant *)restaurant andNumForModules:(NSNumber *)modulesNum
{
    if (!restaurant)
    {
        IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
        restaurant.restaurantId = restaurantID;
    }
    
    
    NSString *sqlStr;
    switch ([modulesNum intValue])
    {
        case 1:
        {
            sqlStr = [NSString stringWithFormat:@"select title,address1 from IMGRestaurant where restaurantId = %@",restaurantID];
        }
            break;
        case 2:
        {
            sqlStr = [NSString stringWithFormat:@"select title,address1 from IMGRestaurant where restaurantId = %@",restaurantID];
        }
            break;
        case 3:
        {
            sqlStr = [NSString stringWithFormat:@"select title,address1 from IMGRestaurant where restaurantId = %@",restaurantID];
        }
            break;
        case 4:
        {
            sqlStr = [NSString stringWithFormat:@"select title,address1 from IMGRestaurant where restaurantId = %@",restaurantID];
        }
            break;
        case 5:
        {
            sqlStr = [NSString stringWithFormat:@"select title,address1 from IMGRestaurant where restaurantId = %@",restaurantID];
        }
            break;
        case 6:
        {
            sqlStr = [NSString stringWithFormat:@"select title,address1 from IMGRestaurant where restaurantId = %@",restaurantID];
        }
            break;
        default:
            break;
    }
    
    
    FMResultSet *resultSet = [[DBManager manager] executeQuery:sqlStr];
    if ([resultSet next])
    {
        [restaurant setValueWithResultSet:resultSet];
        [resultSet close];
        return restaurant;
    }
    else
    {
        [resultSet close];
        return nil;
    }
    
}
+(NSMutableArray*)commentListFromArray:(NSArray*)commentArray andIsVendor:(BOOL)isVendor andRestaurant:(IMGRestaurant *)restaurant
{
    NSMutableArray* commentList = [[NSMutableArray alloc]initWithCapacity:0];
    for(int i=0;commentArray!=nil && [commentArray isKindOfClass:[NSArray class]] && i<commentArray.count;i++){
        if([[commentArray objectAtIndex:i] isKindOfClass:[IMGDishComment class]]){
            [commentList addObject:[commentArray objectAtIndex:i]];
        }else{
            NSDictionary *commentDictionary = [commentArray objectAtIndex:i];
            IMGDishComment *dishComment = [[IMGDishComment alloc]init];
            NSString *commentIdStr = [commentDictionary objectForKey:@"id"];
            if(commentIdStr){
                dishComment.commentId=[NSNumber numberWithInteger:[commentIdStr integerValue]];
                dishComment.content=[commentDictionary objectForKey:@"comment"];
                NSString *createTime = [commentDictionary objectForKey:@"timeCreated"];
                if(createTime){
                    dishComment.createTime=[NSNumber numberWithLongLong:[createTime longLongValue]];
                }
                dishComment.timeCreatedStr = [commentDictionary objectForKey:@"timeCreatedStr"];
                dishComment.restaurantId = restaurant.restaurantId;
                dishComment.restaurantName = restaurant.title;
                dishComment.restaurantImageUrl = restaurant.imageUrl;
                dishComment.userType = [commentDictionary objectForKey:@"userType"];
                dishComment.isVendor = isVendor;
                NSString *commentUserId =[commentDictionary objectForKey:@"userId"];
                if(commentUserId){
                    dishComment.userId=[NSNumber numberWithInteger:[commentUserId integerValue]];
                }
                dishComment.userImageUrl=[commentDictionary objectForKey:@"avatar"];
                dishComment.userName=[commentDictionary objectForKey:@"fullName"];
                [commentList addObject:dishComment];
            }
        }
        
    }
    return commentList;
}

- (BOOL)isOpenRestaurant{
    if (self.openNow != nil) {
        if ([self.state intValue]==6||[self.state intValue]==1) {
            return [self.openNow boolValue];
        }else{
            return NO;
        }
    }else{
        NSString *status;
        NSString *endStatus;
        NSInteger weekNum=[self returnTodayIsWeekDay];
        NSString* weekDay=[self returnWeekDayWithNumber:weekNum];
        
        NSDate *  senddate=[NSDate date];
        NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
        [dateformatter setDateFormat:@"HH:mm"];
        NSString *  locationString=[dateformatter stringFromDate:senddate];
        NSString* hours=[locationString substringToIndex:2];
        NSRange rang={3,2};
        NSString* min=[locationString substringWithRange:rang];
        int curenttime=[hours intValue]*3600+[min intValue]*60;
        
        if ([self.state intValue]==6||[self.state intValue]==1) {
            NSString *closedTime = self.nextclosedTime;
            if ([weekDay isEqualToString:self.nextopenDay]) {
                if ([self.nextopenTime isEqualToString:@"00:00"]&&[self.nextclosedTime isEqualToString:@"00:00"]) {
                    closedTime = @"24:00";
                }
                NSString *openHour = [self.nextopenTime substringToIndex:2];
                NSString *openMin = [self.nextopenTime substringWithRange:rang];
                NSString *closeHour = [closedTime substringToIndex:2];
                NSString *closeMin = [closedTime substringWithRange:rang];
                
                int openTime = [openHour intValue]*3600+[openMin intValue]*60;
                int closeTime = [closeHour intValue]*3600+[closeMin intValue]*60;
                if (openTime < closeTime) {
                    if (openTime <= curenttime && curenttime <= closeTime) {
                        status = @"open";
                        endStatus = @"now";
                    }else if (curenttime < openTime){
                        status = @"closed";
                        endStatus = [NSString stringWithFormat:@"• Open at %@",self.nextopenTime];
                    }else if (curenttime > closeTime){
                        status = @"closed";
                        endStatus = [NSString stringWithFormat:@"• Open on %@",[self.secondopenDay uppercaseString]];
                    }
                }else if (openTime >closeTime){
                    if (openTime<=curenttime) {
                        status = @"open";
                        endStatus = @"now";
                    }else{
                        status = @"closed";
                        endStatus = [NSString stringWithFormat:@"• Open at %@",self.nextopenTime];
                        
                    }
                }else{
                    status = @"open";
                    endStatus = @"now";
                }
                
            }else{
                status = @"closed";
                endStatus = [NSString stringWithFormat:@"• Open on %@",[self.nextopenDay uppercaseString]];
            }
            
            NSString* lastWeekDay=[self returnWeekDayWithNumber:weekNum-1];
            
            long lastCloseTime;
            if (self.yesteropenDay != nil && [self.yesteropenDay isEqualToString:lastWeekDay]) {
                lastCloseTime = [self.yesterclosedTime longLongValue];
                
                if (curenttime<=lastCloseTime) {
                    status = @"open";
                    endStatus = @"now";
                }
            }
            
            if ([self.nextopenTime isEqualToString:@""] || [self.nextclosedTime isEqualToString:@""]) {
                status = @"Closed";
                endStatus = @"";
            }
        }else{
            status = @"Closed";
            endStatus = @"";
        }
        
        if ([status isEqualToString:@"open"]) {
            return YES;
        }else{
            return NO;
        }
    }
}

-(NSString*)returnWeekDayWithNumber:(NSInteger)number
{
    
    switch (number) {
        case 1:
            return [NSString stringWithFormat:@"Sun"];
            break;
        case 2:
            return [NSString stringWithFormat:@"Mon"];
            break;
        case 3:
            return [NSString stringWithFormat:@"Tue"];
            break;
        case 4:
            return [NSString stringWithFormat:@"Wed"];
            break;
        case 5:
            return [NSString stringWithFormat:@"Thu"];
            break;
        case 6:
            return [NSString stringWithFormat:@"Fri"];
            break;
        case 7:
            return [NSString stringWithFormat:@"Sat"];
            break;
        default:
            break;
    }
    
    return nil;
}

-(NSInteger)returnTodayIsWeekDay{
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *now = [NSDate date];;
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    comps = [calendar components:unitFlags fromDate:now];
    NSInteger weekDay=[comps weekday];
    NSLog(@"today is %ld day ",(long)weekDay);
    return weekDay;
}

@end
