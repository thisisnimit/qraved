//
//  IMGSlot
//  Qraved
//
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGSlot : IMGEntity

@property(nonatomic,retain) NSNumber *restaurantId;
@property(nonatomic,copy)   NSString *bookTime;
@property(nonatomic,retain) NSString *weekDay;
@property(nonatomic,retain) NSNumber *avail;



@end
