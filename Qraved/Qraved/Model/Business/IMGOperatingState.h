//
//  IMGSetting.h
//  Qraved
//
//  Created by Jeff on 10/15/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGOperatingState : IMGEntity

@property(nonatomic,retain) NSNumber *stateId;
@property(nonatomic,retain) NSNumber *sortOrder;
@property (nonatomic,copy)  NSString *color;
@property (nonatomic,copy)  NSString *stateName;

+(id)findByStateId:(NSNumber*)stateId;

@end
