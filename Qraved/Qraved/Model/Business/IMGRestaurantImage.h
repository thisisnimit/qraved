//
//  IMGRestaurantOffer.h
//  Qraved
//
//  Created by Olaf on 14/9/26.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGRestaurantImage : IMGEntity

@property(nonatomic,retain) NSNumber *imageId;
@property(nonatomic,retain) NSNumber *restaurantId;
@property(nonatomic,copy)   NSString *imageUrl;

@property(nonatomic,retain) NSNumber *userPhotoCountDic;
@property(nonatomic,copy)   NSString *photoCreditTypeDic;
@property(nonatomic,retain) NSNumber *userIdDic;
@property(nonatomic,copy)   NSString *photoCreditDic;
@property(nonatomic,retain) NSNumber *userReviewCountDic;
@property(nonatomic,copy)   NSString *userAvatarDic;
@property(nonatomic,copy)   NSString *photoCreditUrlDic;
@property(nonatomic,retain) NSNumber *restaurantIdDic;

-(void)updatePhotoCredit:(NSDictionary*)dic;
@end
