//
//  IMGMenu.h
//  Qraved
//
//  Created by Olaf on 14/9/26.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"
#import "IMGLocation.h"

@interface IMGMenu : IMGEntity

@property(nonatomic,retain) NSNumber *menuId;
@property(nonatomic,retain) NSNumber *restaurantId;
@property(nonatomic,copy)   NSString *restaurantSeo;
@property(nonatomic,copy)   NSString *restaurantTitle;
@property(nonatomic,copy)   NSString *title;
@property(nonatomic,retain) NSNumber *currencyId;
@property(nonatomic,copy)   NSString *currencyName;
@property(nonatomic,copy)   NSString *descriptionStr;
@property(nonatomic,retain) NSNumber *dish;
@property(nonatomic,copy)   NSString *price;
@property(nonatomic,retain) NSNumber *sectionId;
@property(nonatomic,copy)   NSString *sectionName;
@property(nonatomic,retain) NSNumber *sectionSortOrder;
@property(nonatomic,copy)   NSString *sectionTypeName;
@property(nonatomic,retain) NSNumber *sectionTypeSortOrder;
@property(nonatomic,retain) NSNumber *sortOrder;
@property(nonatomic,assign) BOOL hasNewMenuSign;

@property (nonatomic ,strong) NSNumber *userId;
@property (nonatomic ,strong) NSNumber *type;
@property (nonatomic ,strong) NSString *sourceValue;
@property (nonatomic ,strong) NSNumber *chain;
@property (nonatomic, assign) NSNumber *createTime;
@property (nonatomic, assign) long long createTimeLong;
@property (nonatomic,strong) NSNumber *id;
@property (nonatomic,copy)NSString *sourceRestaurantSearchTitle;
@property (nonatomic,strong) NSString *sourceType;
@property (nonatomic,copy) NSString *sourceUrl;
@property (nonatomic,copy) NSString *sourceUserName;
@property (nonatomic,strong) NSNumber *menuPhotoCount;
@property (nonatomic,strong) NSNumber *menuPhotoCountTypeRestant;
@property (nonatomic,strong) NSNumber *menuPhotoCountTypeBar;
@property (nonatomic,strong) NSNumber *menuPhotoCountTypeDelivery;
@property (nonatomic,copy)  NSString *photoCreditType;

@property (nonatomic,strong) NSNumber *imageHeight;
@property (nonatomic,strong) NSString *resSourceTitle;


-(void)setValuesForKeysWithDictionary:(NSDictionary *)keyedValues;
@end
