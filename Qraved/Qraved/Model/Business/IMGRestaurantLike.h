//
//  IMGRestaurantOffer.h
//  Qraved
//
//  Created by Olaf on 14/9/26.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGRestaurantLike : IMGEntity

@property(nonatomic,retain) NSNumber *userId;
@property(nonatomic,retain) NSNumber *restaurantId;

@end
