//
//  IMGRestaurantCuisine.h
//  Qraved
//
//  Created by Laura on 14-9-29.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGEntity.h"

@interface IMGRestaurantCuisine : IMGEntity
@property (nonatomic,retain) NSNumber *restaurantId;
@property (nonatomic,retain) NSNumber *cuisineId;
@end
