//
//  IMGRestaurantEvent.m
//  Qraved
//
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGRestaurantEvent.h"
#import "DBManager.h"

@implementation IMGRestaurantEvent

+(BOOL)checkByEventId:(NSNumber *)eventId andRestaurantId:(NSNumber *)restaurnatId{
    FMResultSet *resultSet = [[DBManager manager]executeQuery:[NSString stringWithFormat:@"select eventId from IMGRestaurantEvent where eventId=%@ and restaurantId=%@;",eventId,restaurnatId]];
    if([resultSet next]){
        [resultSet close];
        return TRUE;
    }
    return FALSE;
}

-(CGSize)getDishImageSize{
    NSString *urlStr = self.imageUrl;
    NSString *frameStrWithHou = [[urlStr componentsSeparatedByString:@"-"]lastObject];
    float qraveWidth=305.0/2;
    float width = qraveWidth;
    float height;
    float scale;
    
    long widthOriginal;
    long heightOriginal;
    
    NSString *frameStr = [[frameStrWithHou componentsSeparatedByString:@"."]objectAtIndex:0];
    NSRange r = [urlStr rangeOfString:@"-"];
    
    
    if (r.length != 0) {
        widthOriginal = [[[frameStr componentsSeparatedByString:@"x"]objectAtIndex:0] integerValue];
        heightOriginal = [[[frameStr componentsSeparatedByString:@"x"]lastObject] integerValue];
        
        if(heightOriginal==0)
        {
            height=qraveWidth;
        }
        else
        {
            scale = (float)widthOriginal / qraveWidth;
            height = (float)heightOriginal / scale;
        }
    } else {
        
        height = qraveWidth;
    }
    
    if(height==NAN)
    {
        height=qraveWidth;
    }
    if (r.length == 0) {
        height = width;
    }
    return CGSizeMake(width, height);
}
-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if([key isEqualToString:@"photoCreditImpl"] || [key isEqualToString:@"photoCredit"]) {
        [self updatePhotoCredit:value];
    }
}
-(void)updatePhotoCredit:(NSDictionary*)dic{
    if (dic != nil) {
        self.userPhotoCountDic = [dic objectForKey:@"userPhotoCount"];
        self.photoCreditTypeDic  = [dic objectForKey:@"photoCreditType"];
        self.userIdDic  = [dic objectForKey:@"userId"];
        self.photoCreditDic  = [dic objectForKey:@"photoCredit"];
        self.userReviewCountDic  = [dic objectForKey:@"userReviewCount"];
        self.userAvatarDic  = [dic objectForKey:@"userAvatar"];
        self.photoCreditUrlDic  = [dic objectForKey:@"photoCreditUrl"];
        self.restaurantIdDic  = [dic objectForKey:@"restaurantId"];
    }
}
@end
