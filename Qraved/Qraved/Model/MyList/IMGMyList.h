//
//  IMGMyList.h
//  Qraved
//
//  Created by Lucky on 15/4/26.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGEntity.h"

@interface IMGMyList : IMGEntity

@property (nonatomic,retain) NSNumber *listCount;
@property (nonatomic,retain) NSNumber *listId;
@property (nonatomic,copy)   NSString *listImageUrls;
@property (nonatomic,retain) NSNumber *listType;
@property (nonatomic,copy)   NSString *listName;
@property (nonatomic,copy)   NSNumber *listUserId;
@property (nonatomic,copy)   NSNumber *inList;
@property (nonatomic,copy)  NSNumber *cityId;
@property (nonatomic,copy) NSNumber *restaurantListItemCount;
@end
