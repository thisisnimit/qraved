//
//  IMGReservationCompletedId
//  Qraved
//
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGEntity.h"
@interface IMGReservationCompletedId : IMGEntity

@property(nonatomic,retain) NSNumber *userId;
@property(nonatomic,retain) NSNumber *restaurantId;

@end
