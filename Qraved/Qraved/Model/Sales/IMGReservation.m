//
//  IMGReservation.m
//  Qraved
//
//  Created by Olaf on 14/9/26.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGReservation.h"

@implementation IMGReservation

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{

    if([key isEqualToString:@"id"]){
        self.reservationId =  value;
    }else if ([key isEqualToString:@"location"]) {

    }else if([key isEqualToString:@"simpleRestaurant"]){
        NSDictionary * dic =(NSDictionary *) value;
        self.restaurantId =[dic objectForKey:@"id"];
        self.restaurantName=[dic objectForKey:@"title"];
        self.title = [dic objectForKey:@"title"];
    }
//    NSLog(@"%@ is not found",key);
}

-(void)setValue:(id)value forKey:(NSString *)key{
    [super setValue:value forKey:key];
    if([key isEqualToString:@"status"]){
        self.status = [NSString stringWithFormat:@"%@",value];
    }
}
@end
