//
//  IMGReservation.h
//  Qraved
//
//  Created by Olaf on 14/9/26.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGEntity.h"
@interface IMGReservation : IMGEntity

@property(nonatomic,copy)   NSString * bookDate;
@property(nonatomic,copy)   NSString * bookTime;
@property(nonatomic,copy)   NSString * voucherCode;
@property(nonatomic,copy)   NSString * code;
@property(nonatomic,copy)   NSString * status;
@property(nonatomic,copy)   NSString * title;
@property(nonatomic,copy)   NSString * address;
@property(nonatomic,copy)   NSString * phone;
@property(nonatomic,copy)   NSString * timeUpdated;
@property(nonatomic,copy)   NSString * dateCreated;
@property(nonatomic,copy)   NSString * restaurantName;
@property(nonatomic,copy)   NSString * restaurantPhone;
@property(nonatomic,copy)   NSString * restaurantImage;
@property(nonatomic,copy)   NSString * restaurantSeoKeywords;
@property(nonatomic,copy)   NSString * email;
@property(nonatomic,copy)   NSString * fullName;
@property(nonatomic,copy)   NSString * firstName;
@property(nonatomic,copy)   NSString * lastName;
@property(nonatomic,copy)   NSString * specialRequest;
@property(nonatomic,copy)   NSString * offerName;
@property(nonatomic,retain) NSNumber * party;
@property(nonatomic,retain) NSNumber * gender;
@property(nonatomic,retain) NSNumber * off;
@property(nonatomic,retain) NSNumber * offerId;
@property(nonatomic,retain) NSNumber * longitude;
@property(nonatomic,retain) NSNumber * latitude;
@property(nonatomic,retain) NSNumber * reservationId;
@property(nonatomic,retain) NSNumber * userId;
@property(nonatomic,retain) NSNumber * restaurantId;
@property(nonatomic,copy)   NSString * bookingStatus;
@property(nonatomic,copy)   NSString * cuisineName;
@property(nonatomic,copy)   NSNumber *priceName;
@property(nonatomic,copy)   NSString *districtName;
@property(nonatomic,copy)   NSString *ratingScore;
@property(nonatomic,retain) NSNumber *restaurantLatitude;
@property(nonatomic,retain) NSNumber *restaurantLongitude;
@property(nonatomic,copy)   NSString * restaurantCuisine;
@property(nonatomic,copy)   NSString * restaurantDistrict;
@property(nonatomic,copy)   NSString * restaurantTitle;
@property(nonatomic,copy)   NSNumber *bookPax;
@property (nonatomic, strong)  NSArray  *landMarkList;


@end
