//
//  IMGPhotoModel.h
//  Qraved
//
//  Created by Gary.yao on 2017/6/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGJourney.h"
@interface IMGPhotoModel : NSObject

@property (nonatomic,strong) NSNumber * commentCount;
@property (nonatomic,strong) NSNumber * likeCount;
@property (nonatomic,strong) NSNumber * myID;
@property (nonatomic,strong) NSNumber * userId;
@property (nonatomic,strong) NSString * des;
@property (nonatomic,strong) NSNumber * restaurantId;
@property (nonatomic,strong) NSString * createTime;
@property (nonatomic,strong) NSString * creator;
@property (nonatomic,strong) NSDictionary * photoCredit;
@property (nonatomic,strong) NSNumber * qravedCount;
@property (nonatomic,strong) NSString * altText;
@property (nonatomic,strong) NSString * createTimeStr;
@property (nonatomic,strong) NSNumber *type;

@property(nonatomic,strong) NSNumber *restaurantPriceLevel;
@property(nonatomic,copy)   NSString *restaurantCuisine;
@property(nonatomic,copy)   NSString *restaurantDistrict;
@property(nonatomic,copy)   NSString *restaurantCityName;
@property(nonatomic,copy)   NSString *restaurantTitle;
@property(nonatomic,copy)   NSString *restaurantSeoKeywords;
@property(nonatomic,copy)   NSString *restaurantImage;
@property (nonatomic, copy)    NSString * imageUrl;
@end
