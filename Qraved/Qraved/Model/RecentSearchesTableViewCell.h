//
//  RecentSearchesTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/18.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RecentSearchesTableViewCellModel.h"
#import "RecentSearchesSuperModel.h"

@protocol RecentSearchesTableViewCellDelegate <NSObject>

- (void)recentSearchTapped:(RecentSearchesTableViewCellModel *)model;

@end

@interface RecentSearchesTableViewCell : UITableViewCell<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong)  RecentSearchesSuperModel  *Supermodel;
@property (nonatomic, strong)  UICollectionView  *collectionView;
@property (nonatomic, strong)  NSMutableArray  *RecentSearchedsArray;

@property (nonatomic, weak)  id<RecentSearchesTableViewCellDelegate> delegate;


- (void)createUIWithModel:(RecentSearchesSuperModel *)model;
@end
