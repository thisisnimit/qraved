//
//  IMGProfileModel.h
//  Qraved
//
//  Created by Gary.yao on 2017/6/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IMGProfileModel : NSObject


@property (nonatomic,strong) NSString * email;
@property (nonatomic,strong) NSString * firstName;
@property (nonatomic,strong) NSString * lastName;
@property (nonatomic,strong) NSString * userName;
@property (nonatomic,strong) NSString * birthDate;
@property (nonatomic,strong) NSString * phone;
@property (nonatomic,strong) NSString * address1;
@property (nonatomic,strong) NSString * isVerified;
@property (nonatomic,strong) NSString * instagramUserFullName;
@property (nonatomic,strong) NSString * instagramUserName;
@property (nonatomic,strong) NSString * avatar;
@property (nonatomic,strong) NSString * des;
@property (nonatomic,strong) NSString * coverImage;
@property (nonatomic,strong) NSString * occupation;
@property (nonatomic,strong) NSString * additionalInfo;
@property (nonatomic,strong) NSString * blogUrl;
@property (nonatomic,strong) NSNumber * myID;
@property (nonatomic,strong) NSNumber * gender;
@property (nonatomic,strong) NSNumber * countryId;
@property (nonatomic,strong) NSNumber * cityId;
@property (nonatomic,strong) NSNumber * districtId;
@property (nonatomic,strong) NSNumber * userLevel;
@property (nonatomic,strong) NSNumber * isCelebrity;
@property (nonatomic,strong) NSNumber * instagramfollowers;

@end
