//
//  V2_DiscoverListResultRestaurantTableViewCellSubModel.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/24.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverListResultRestaurantTableViewCellSubModel.h"

@implementation V2_DiscoverListResultRestaurantTableViewCellSubModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    
    return @{@"myID":@"id"};
    
}

@end
