//
//  IMGRestaurantEventComment.m
//  Qraved
//
//  Created by Jeff on 3/9/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "IMGRestaurantEventComment.h"

@implementation IMGRestaurantEventComment

-(id)initFromDictionary:(NSDictionary*)commentDictionary{
    self = [super init];
    if(self){
        NSString *commentIdStr = [commentDictionary objectForKey:@"id"];
        if(commentIdStr){
            self.commentId=[NSNumber numberWithInteger:[commentIdStr integerValue]];
            self.content=[commentDictionary objectForKey:@"comment"];
            NSString *createTime = [commentDictionary objectForKey:@"timeCreated"];
            if(createTime){
                self.createTime=[NSNumber numberWithLongLong:[createTime longLongValue]];
            }
            NSString *commentUserId =[commentDictionary objectForKey:@"userId"];
            if(commentUserId){
                self.userId=[NSNumber numberWithInteger:[commentUserId integerValue]];
            }
            self.userImageUrl=[commentDictionary objectForKey:@"avatar"];
            self.userName=[commentDictionary objectForKey:@"fullName"];
        }
    }
    return self;
}



+(NSMutableArray*)commentListFromArray:(NSArray*)commentArray andIsVendor:(BOOL)isVendor andRestaurant:(IMGRestaurant *)restaurant
{
    NSMutableArray* commentList = [[NSMutableArray alloc]initWithCapacity:0];
    for(int i=0;commentArray!=nil && [commentArray isKindOfClass:[NSArray class]] && i<commentArray.count;i++){
        NSDictionary *commentDictionary = [commentArray objectAtIndex:i];
        IMGRestaurantEventComment *restaurantEventComment = [[IMGRestaurantEventComment alloc]init];
        NSString *commentIdStr = [commentDictionary objectForKey:@"id"];
        if(commentIdStr){
            restaurantEventComment.commentId=[NSNumber numberWithInteger:[commentIdStr integerValue]];
            restaurantEventComment.content=[commentDictionary objectForKey:@"comment"];
            NSString *createTime = [commentDictionary objectForKey:@"timeCreated"];
            if(createTime){
                restaurantEventComment.createTime=[NSNumber numberWithLongLong:[createTime longLongValue]];
            }
            restaurantEventComment.restaurantId = restaurant.restaurantId;
            restaurantEventComment.restaurantName = restaurant.title;
            restaurantEventComment.restaurantImageUrl = restaurant.imageUrl;
            restaurantEventComment.userType = [commentDictionary objectForKey:@"userType"];
            restaurantEventComment.isVendor = isVendor;
            NSString *commentUserId =[commentDictionary objectForKey:@"userId"];
            if(commentUserId){
                restaurantEventComment.userId=[NSNumber numberWithInteger:[commentUserId integerValue]];
            }
            restaurantEventComment.userImageUrl=[commentDictionary objectForKey:@"avatar"];
            restaurantEventComment.userName=[commentDictionary objectForKey:@"fullName"];
            [commentList addObject:restaurantEventComment];
        }
    }
    return commentList;
}


@end
