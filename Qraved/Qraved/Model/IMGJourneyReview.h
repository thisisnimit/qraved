//
//  personalTypeOneModel.h
//  Qraved
//
//  Created by Adam.zhang on 2017/6/28.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IMGJourneyReview : NSObject

@property (nonatomic, strong)  NSNumber  *reviewId;
@property (nonatomic, strong)  NSNumber  *reviewScore;
@property (nonatomic, strong)  NSNumber  *reviewTimeCreated;
@property (nonatomic, strong)  NSString  *reviewTitle;
@property (nonatomic, strong)  NSString  *reviewSummarize;
@property (nonatomic, strong)  NSNumber  *restaurantId;
@property (nonatomic, strong)  NSString  *restaurantTitle;
@property (nonatomic, strong)  NSString  *restaurantCuisine;
@property (nonatomic, strong)  NSString  *restaurantDistrict;
@property (nonatomic, strong)  NSString  *restaurantCityName;
@property (nonatomic, strong)  NSNumber  *restaurantPriceLevel;
@property (nonatomic, strong)  NSString  *restaurantPriceLevelStr;
@property (nonatomic, strong)  NSString  *restaurantSeoKeywords;
@property (nonatomic, strong)  NSString  *restaurantDefaultImageUrl;
@property (nonatomic, strong)  NSArray  *dishList;
@property (nonatomic, strong)  NSNumber  *userId;
@property (nonatomic, strong)  NSString  *userFullName;
@property (nonatomic, strong)  NSString  *userAvatar;
@property (nonatomic, strong)  NSNumber  *userReviewCount;
@property (nonatomic, strong)  NSNumber  *isCelebrity;
@property (nonatomic, strong)  NSNumber  *commentCount;
@property (nonatomic, strong)  NSNumber  *likeCount;

@property (nonatomic, strong)  NSArray  *reviewCommentList;//原来的设计的评论列表

@property(nonatomic,assign) BOOL isLike;
@property (nonatomic,assign) BOOL isReadMore;


@end
