//
//  IMGDiscover.m
//  Qraved
//
//  Created by Jeff on 10/16/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGDiscover.h"
#import "DBManager.h"
#import "IMGPriceLevel.h"
#import "IMGEvent.h"
#import "IMGOfferType.h"
#import "IMGTagUpdate.h"
#import "IMGArea.h"

@implementation IMGDiscover
-(void)initFromDiscoverEntity:(IMGDiscoverEntity*)discoverEntity{
    self.cityId = discoverEntity.cityId;
    self.discoverId = discoverEntity.discoverId;
    self.status=discoverEntity.status;
    if(discoverEntity.eventId!=nil && [discoverEntity.eventId intValue]>0){
        IMGEvent *event = [IMGEvent findById:discoverEntity.eventId];
        if(event!=nil){
            self.eventArray = [[NSMutableArray alloc]initWithCapacity:0];
            IMGOfferType *offerType = [[IMGOfferType alloc]init];
            offerType.offerId = event.eventId;
            offerType.eventType=[NSNumber numberWithInt:1];
            offerType.typeId = [NSNumber numberWithInt:4];
            offerType.off = [NSNumber numberWithInt:0];
            offerType.name = event.name;
            [self.eventArray addObject:offerType];
        }
    }
    self.sortby = discoverEntity.sortby;
    self.keyword=discoverEntity.keyword;
    
    self.pax = discoverEntity.pax;
    
    if(discoverEntity.bookDateString!=nil || ![discoverEntity.bookDateString isEqualToString:@""]){
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        self.bookDate=[dateFormatter dateFromString:discoverEntity.bookDateString];
    }
    self.bookDateString = discoverEntity.bookDateString;
    self.bookTime = discoverEntity.bookTime;
    
    self.name=discoverEntity.name;
    self.imageUrl=discoverEntity.imageUrl;
    
    self.priceLevelArray = [[NSMutableArray alloc]init];
    if (discoverEntity.priceLevelString != nil && discoverEntity.priceLevelString.length>0) {
        NSArray *priceLevelAray = [discoverEntity.priceLevelString componentsSeparatedByString:@"&&&&"];
        for (int i=0; i<priceLevelAray.count; i++) {
            IMGPriceLevel *priceLevel = [[IMGPriceLevel alloc] init];
            priceLevel.priceLevelId = [priceLevelAray objectAtIndex:i];
            if([[priceLevelAray objectAtIndex:i] intValue]==1){
                priceLevel.title=@"100k-199k";
                priceLevel.dollors=@"$";
            }else if([[priceLevelAray objectAtIndex:i] intValue]==2){
                priceLevel.title=@"200k-299k";
                priceLevel.dollors=@"$$";
            }else if([[priceLevelAray objectAtIndex:i] intValue]==3){
                priceLevel.title=@"300k-399k";
                priceLevel.dollors=@"$$$";
            }else if([[priceLevelAray objectAtIndex:i] intValue]==4){
                priceLevel.title=@">400k";
                priceLevel.dollors=@"$$$$";
            }
            [self.priceLevelArray addObject:priceLevel];
        }
    }
    
    self.districtArray=[[NSMutableArray alloc]initWithCapacity:0];
    NSArray *districtArray = [discoverEntity.districtString componentsSeparatedByString:@"&&&&"];
    for(int i=0;i<districtArray.count;i++){
        if ([[districtArray objectAtIndex:i] integerValue]>0) {
            NSNumber *districtId = [districtArray objectAtIndex:i];
            NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM IMGDistrict WHERE districtId = %@;",districtId];
            //        NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM IMGDistrict"];
            
            NSLog(@"id======%@",districtId);
            FMResultSet *resultSet = [[DBManager manager]executeQuery:sqlString];
            if ([resultSet next]) {
                IMGDistrict *district = [[IMGDistrict alloc]init];
                [district setValueWithResultSet:resultSet];
                [self.districtArray addObject:district];
                [resultSet close];
            }
            //        [self.districtArray addObject:[districtArray objectAtIndex:i]];
        }
        
    }
    
    
    self.offArray=[[NSMutableArray alloc]initWithCapacity:0];
    if (discoverEntity.offString !=nil && discoverEntity.offString.length>0) {
        NSArray *offArray = [discoverEntity.offString componentsSeparatedByString:@"&&&&"];
        for (int i=0; i<offArray.count; i++) {
            NSArray *array = [[offArray objectAtIndex:i]componentsSeparatedByString:@"+"];
            if (array!=nil&&array.count == 2) {
                IMGOfferType *offerType1 = [[IMGOfferType alloc] init];
                offerType1.typeId = [array objectAtIndex:1];
                offerType1.offerId = [array objectAtIndex:0];
                if([[array objectAtIndex:0] intValue]==5){
                    offerType1.off = [NSNumber numberWithInt:50];
                    offerType1.name = @"50% Qraved Discount";
                }else if([[array objectAtIndex:0] intValue]==4){
                    offerType1.off = [NSNumber numberWithInt:40];
                    offerType1.name = @"40% Qraved Discount";
                }else if([[array objectAtIndex:0] intValue]==3){
                    offerType1.off = [NSNumber numberWithInt:30];
                    offerType1.name = @"30% Qraved Discount";
                }else if([[array objectAtIndex:0] intValue]==2){
                    offerType1.off = [NSNumber numberWithInt:20];
                    offerType1.name = @"20% Qraved Discount";
                }else {
                    offerType1.off = [NSNumber numberWithInt:10];
                    offerType1.name = @"10% Qraved Discount";
                }
                
                [self.offArray addObject:offerType1];
            }
            
        }
    }
    
    self.offerTypeArray=[[NSMutableArray alloc]initWithCapacity:0];

    if (discoverEntity.offersString!=nil && discoverEntity.offersString.length>0) {
        NSArray *offerTypeArray = [discoverEntity.offersString componentsSeparatedByString:@"&&&&"];
        for (int i=0; i<offerTypeArray.count; i++) {
            NSArray *array = [[offerTypeArray objectAtIndex:i]componentsSeparatedByString:@"+"];
            if (array != nil && array.count == 2) {
                IMGOfferType *offerType1=[[IMGOfferType alloc] init];
                offerType1.typeId = [array objectAtIndex:1];
                offerType1.offerId = [array objectAtIndex:0];
                offerType1.off = [NSNumber numberWithInt:0];
                if([[array objectAtIndex:0] intValue]==6){
                    offerType1.name = @"Special Set Menu";
                    [self.offerTypeArray addObject:offerType1];
                }else if([[array objectAtIndex:0] intValue]==7){
                    offerType1.name = @"In-restaurant Promotion";
                    [self.offerTypeArray addObject:offerType1];
                }else if([[array objectAtIndex:0] intValue]==8){
                    offerType1.name = @"Featured Offers";
                    [self.offerTypeArray addObject:offerType1];
                }
                
            }
            
        }

    }
    
    NSArray *tagIdArray = [discoverEntity.tagString componentsSeparatedByString:@"&&&&"];
    self.tagsArr=[[NSMutableArray alloc]initWithCapacity:0];
    if(tagIdArray!=nil && [tagIdArray count]>0 && ![[tagIdArray objectAtIndex:0] isEqualToString:@""]){
        FMResultSet *resultSet = [[DBManager manager] executeQuery:[NSString stringWithFormat:@"SELECT tagId FROM IMGTag;"]];
        while([resultSet next]){
            NSNumber *tagId=[resultSet objectForColumnName:@"tagId"];
            for(int i=0;i<[tagIdArray count];i++){
                NSString *tmpTagId=[tagIdArray objectAtIndex:i];
                if([tmpTagId intValue]==[tagId intValue]){
                    IMGTag *tag = [IMGTag findById:tagId];
                    [self.tagsArr addObject:tag];
                    break;
                }
            }
        }
        [resultSet close];
    }
    
    self.occasionArray=[[NSMutableArray alloc]initWithCapacity:0];
    if(tagIdArray!=nil && [tagIdArray count]>0 && ![[tagIdArray objectAtIndex:0] isEqualToString:@""]){
        [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT tagId FROM IMGTag where type='Occasion';"] successBlock:^(FMResultSet *resultSet) {
            while([resultSet next]){
                NSNumber *tagId=[resultSet objectForColumnName:@"tagId"];
                for(int i=0;i<[tagIdArray count];i++){
                    NSString *tmpTagId=[tagIdArray objectAtIndex:i];
                    if([tmpTagId intValue]==[tagId intValue]){
                        IMGTag *tag = [IMGTag findById:tagId];
                        [self.occasionArray addObject:tag];
                        break;
                    }
                }
            }
            [resultSet close];
        }failureBlock:^(NSError *error) {
            NSLog(@"initFromDictionary:(NSDictionary *)dictionary error when get datas:%@",error.description);
        }];

    }
    self.landmarkArray=[[NSMutableArray alloc]initWithCapacity:0];
    if(tagIdArray!=nil && [tagIdArray count]>0 && ![[tagIdArray objectAtIndex:0] isEqualToString:@""]){
        [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT tagId FROM IMGTag where type='Landmark/ Mall';"] successBlock:^(FMResultSet *resultSet) {
            while([resultSet next]){
                NSNumber *tagId=[resultSet objectForColumnName:@"tagId"];
                for(int i=0;i<[tagIdArray count];i++){
                    NSString *tmpTagId=[tagIdArray objectAtIndex:i];
                    if([tmpTagId intValue]==[tagId intValue]){
                        IMGTag *tag = [IMGTag findById:tagId];
                        [self.landmarkArray addObject:tag];
                        break;
                    }
                }
            }
            [resultSet close];
        }failureBlock:^(NSError *error) {
            NSLog(@"initFromDictionary:(NSDictionary *)dictionary error when get datas:%@",error.description);
        }];

    }
    self.foodTypeArray=[[NSMutableArray alloc]initWithCapacity:0];
    if(tagIdArray!=nil && [tagIdArray count]>0 && ![[tagIdArray objectAtIndex:0] isEqualToString:@""]){
        [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT tagId FROM IMGTag where type='Food Type';"] successBlock:^(FMResultSet *resultSet) {
            while([resultSet next]){
                NSNumber *tagId=[resultSet objectForColumnName:@"tagId"];
                for(int i=0;i<[tagIdArray count];i++){
                    NSString *tmpTagId=[tagIdArray objectAtIndex:i];
                    if([tmpTagId intValue]==[tagId intValue]){
                        IMGTag *tag = [IMGTag findById:tagId];
                        [self.foodTypeArray addObject:tag];
                        break;
                    }
                }
            }
            [resultSet close];
        }failureBlock:^(NSError *error) {
            NSLog(@"initFromDictionary:(NSDictionary *)dictionary error when get datas:%@",error.description);
        }];

    }
    
    self.cuisineArray=[[NSMutableArray alloc]initWithCapacity:0];
    NSArray *cuisineArray=[discoverEntity.cuisineString componentsSeparatedByString:@"&&&&"];
    for(int i=0;i<cuisineArray.count;i++){
        if ([[cuisineArray objectAtIndex:i] integerValue]>0) {
            NSNumber *cuisineId=[cuisineArray objectAtIndex:i];
            FMResultSet *resultSet = [[DBManager manager]executeQuery:[NSString stringWithFormat:@"select * from IMGCuisine where cuisineId=%@",cuisineId]];
            if(resultSet.next){
                IMGCuisine *cuisine=[[IMGCuisine alloc]init];
                [cuisine setValueWithResultSet:resultSet];
                [self.cuisineArray addObject:cuisine];
            }
        }
        
    }
    
    self.areaArray=[[NSMutableArray alloc]initWithCapacity:0];
    NSArray *areaArray=[discoverEntity.areaString componentsSeparatedByString:@"&&&&"];
    for(int i=0;i<areaArray.count;i++){
        if ([[areaArray objectAtIndex:i] integerValue]>0) {
            NSNumber *areaId=[areaArray objectAtIndex:i];
            FMResultSet *resultSet = [[DBManager manager]executeQuery:[NSString stringWithFormat:@"select * from IMGArea where tagId=%@",areaId]];
            if(resultSet.next){
                IMGArea *area=[[IMGArea alloc]init];
                [area setValueWithResultSet:resultSet];
                [self.areaArray addObject:area];
            }
        }
        
    }

//    self.areaArray = [[NSMutableArray alloc] init];
//    for (NSString *idStr in [discoverEntity.areaString componentsSeparatedByString:@"&&&&"])
//    {
//        IMGTag *tag = [[IMGTag alloc] init];
//        tag.tagId = [NSNumber numberWithInt:[idStr intValue]];
//        [self.areaArray addObject:tag];
//    }
//    self.areaArray=[[NSMutableArray alloc]initWithArray:[discoverEntity.areaString componentsSeparatedByString:@"&&&&"]];

}
-(void)initFromDictionary:(NSDictionary *)dictionary{
    self.discoverId=[dictionary objectForKey:@"id"];
    self.status=[dictionary objectForKey:@"status"];
    self.name=[dictionary objectForKey:@"name"];
    self.keyword=[dictionary objectForKey:@"searchContent"];
    self.imageUrl=[dictionary objectForKey:@"imageUrl"];
    NSString *bookDateString=[dictionary objectForKey:@"bookDate"];
    if([bookDateString isKindOfClass:[NSString class]] && bookDateString!=nil&&![bookDateString isEqualToString:@""]){
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        self.bookDate=[dateFormatter dateFromString:bookDateString];
    }
    NSString *bookTimeString=[dictionary objectForKey:@"bookTime"];
    if(bookTimeString!=nil){
        self.bookTime=bookTimeString;
    }
    NSNumber *pax=[dictionary objectForKey:@"pax"];
    self.pax=pax;
    
    self.offArray = [[NSMutableArray alloc]initWithCapacity:0];
    self.offerTypeArray=[[NSMutableArray alloc]initWithCapacity:0];
    NSArray *offerArray = [dictionary objectForKey:@"offersList"];
    for(NSDictionary *offerDictionary in offerArray){
        NSNumber *offerType=[offerDictionary objectForKey:@"offerType"];
        NSNumber *off=[offerDictionary objectForKey:@"off"];
        if(offerType!=nil && [offerType intValue]==0 && [off intValue]!=0){
            IMGOfferType *offerType1=[[IMGOfferType alloc] init];
            offerType1.typeId = [NSNumber numberWithInt:0];
            if([off intValue]==50){
                offerType1.offerId = [NSNumber numberWithInt:1];
                offerType1.off = [NSNumber numberWithInt:50];
                offerType1.name = L(@"50% Qraved Discount");
                [self.offArray addObject:offerType1];
            }else if([off intValue]==40){
                offerType1.offerId = [NSNumber numberWithInt:2];
                offerType1.off = [NSNumber numberWithInt:40];
                offerType1.name = L(@"40% Qraved Discount");
                [self.offArray addObject:offerType1];
            }else if([off intValue]==30){
                offerType1.offerId = [NSNumber numberWithInt:3];
                offerType1.off = [NSNumber numberWithInt:30];
                offerType1.name = L(@"30% Qraved Discount");
                [self.offArray addObject:offerType1];
            }else if([off intValue]==20){
                offerType1.offerId = [NSNumber numberWithInt:4];
                offerType1.off = [NSNumber numberWithInt:20];
                offerType1.name = L(@"20% Qraved Discount");
                [self.offArray addObject:offerType1];
            }else if([off intValue]==10){
                offerType1.offerId = [NSNumber numberWithInt:5];
                offerType1.off = [NSNumber numberWithInt:10];
                offerType1.name = L(@"10% Qraved Discount");
                [self.offArray addObject:offerType1];
            }
        }else{
            IMGOfferType *offerType1=[[IMGOfferType alloc] init];
            offerType1.typeId = offerType;
            offerType1.off = [NSNumber numberWithInt:0];
            if([offerType intValue]==1){
                offerType1.offerId = [NSNumber numberWithInt:6];
                offerType1.name = L(@"Special Set Menu");
            }else if([offerType intValue]==2){
                offerType1.offerId = [NSNumber numberWithInt:7];
                offerType1.name = L(@"In-restaurant Promotion");
            }else if([offerType intValue]==3){
                offerType1.offerId = [NSNumber numberWithInt:8];
                offerType1.name = L(@"Featured Offers");
            }
            [self.offerTypeArray addObject:offerType1];
        }
    }
    
    self.districtArray=[[NSMutableArray alloc]initWithCapacity:0];
    NSArray *districtArray=[dictionary objectForKey:@"districtList"];
    for(NSDictionary *districtDictionary in districtArray){
        IMGDistrict *district = [[IMGDistrict alloc] init];
        [district setValuesForKeysWithDictionary:districtDictionary];
        district.districtId=[districtDictionary objectForKey:@"districtId"];
        district.name = [districtDictionary objectForKey:@"name"];
        [self.districtArray addObject:district];
    }
    
    self.cuisineArray=[[NSMutableArray alloc]initWithCapacity:0];
    NSArray *cuisineArray=[dictionary objectForKey:@"cuisineList"];
    for(NSDictionary *cuisineDictionary in cuisineArray){
        NSNumber *cuisineId=[cuisineDictionary objectForKey:@"cuisineId"];
        FMResultSet *resultSet = [[DBManager manager]executeQuery:[NSString stringWithFormat:@"select * from IMGCuisine where cuisineId=%@",cuisineId]];
        if(resultSet.next){
            IMGCuisine *cuisine=[[IMGCuisine alloc]init];
            [cuisine setValueWithResultSet:resultSet];
            [self.cuisineArray addObject:cuisine];
        }
    }
    
    self.priceLevelArray = [[NSMutableArray alloc]initWithCapacity:0];
    NSNumber *endPrice = [dictionary objectForKey:@"endPrice"];
    NSNumber *startPrice = [dictionary objectForKey:@"startPrice"];
    for (int i=[startPrice intValue];i<=[endPrice intValue];i++){
        if(i==1){
            IMGPriceLevel *priceLevel1 = [[IMGPriceLevel alloc] init];
            priceLevel1.priceLevelId = [NSNumber numberWithInt:1];
            priceLevel1.title=@"100k-199k";
            priceLevel1.dollors=@"$";
            [self.priceLevelArray addObject:priceLevel1];
        }else if(i==2){
            IMGPriceLevel *priceLevel2 = [[IMGPriceLevel alloc] init];
            priceLevel2.priceLevelId = [NSNumber numberWithInt:2];
            priceLevel2.title=@"200k-299k";
            priceLevel2.dollors=@"$$";
            [self.priceLevelArray addObject:priceLevel2];
        }else if(i==3){
            IMGPriceLevel *priceLevel3 = [[IMGPriceLevel alloc] init];
            priceLevel3.priceLevelId = [NSNumber numberWithInt:3];
            priceLevel3.title=@"300k-399k";
            priceLevel3.dollors=@"$$$";
            [self.priceLevelArray addObject:priceLevel3];
        }else if(i==4){
            IMGPriceLevel *priceLevel4 = [[IMGPriceLevel alloc] init];
            priceLevel4.priceLevelId = [NSNumber numberWithInt:4];
            priceLevel4.title=@">400k";
            priceLevel4.dollors=@"$$$$";
            [self.priceLevelArray addObject:priceLevel4];
        }
    }
    
    NSArray *sortbyArray=[dictionary objectForKey:@"sortby"];
    if(sortbyArray!=nil&&[sortbyArray count]>0){
        self.sortby=[sortbyArray objectAtIndex:0];
    }
    
    NSArray *tagArray=[dictionary objectForKey:@"tagList"];
    NSMutableArray *tagIdArray=[[NSMutableArray alloc]init];
    for(NSDictionary *tagDictionary in tagArray){
        NSNumber *tagId=[tagDictionary objectForKey:@"tagId"];
        [tagIdArray addObject:tagId];
    }
    
    self.tagsArr=[[NSMutableArray alloc]initWithCapacity:0];
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT tagId FROM IMGTag where status=1;"] successBlock:^(FMResultSet *resultSet) {
        while([resultSet next]){
            NSNumber *tagId=[resultSet objectForColumnName:@"tagId"];
            if([tagIdArray indexOfObject:tagId]!=NSNotFound){
                [self.tagsArr addObject:tagId];
            }
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"initFromDictionary:(NSDictionary *)dictionary error when get datas:%@",error.description);
    }];
    
    self.occasionArray=[[NSMutableArray alloc]initWithCapacity:0];
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT tagId FROM IMGTag where status=1 and type='Occasion';"] successBlock:^(FMResultSet *resultSet) {
        while([resultSet next]){
            NSNumber *tagId=[resultSet objectForColumnName:@"tagId"];
            if([tagIdArray indexOfObject:tagId]!=NSNotFound){
                [self.occasionArray addObject:tagId];
            }
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"initFromDictionary:(NSDictionary *)dictionary error when get datas:%@",error.description);
    }];
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
    if([key isEqualToString:@"id"]){
        self.discoverId =  value;
    }
}

-(void)addOfferType:(IMGOfferType *)offerType{
    if([self hasOfferType:offerType]){
        return;
    }
    if(self.offArray==nil){
        self.offArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    if(self.offerTypeArray==nil){
        self.offerTypeArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    if (self.eventArray == nil) {
        self.eventArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    if([offerType.typeId intValue]==0){
        [self.offArray addObject:offerType];
    }else if([offerType.eventType intValue] == 1){
        [self.eventArray addObject:offerType];
    }else{
        [self.offerTypeArray addObject:offerType];
    }

}

-(void)removeOfferType:(IMGOfferType *)offerType{
    if(![self hasOfferType:offerType]){
        return;
    }
    if(self.offArray==nil){
        self.offArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    if(self.offerTypeArray==nil){
        self.offerTypeArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    if (self.eventArray == nil) {
        self.eventArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    if([offerType.typeId intValue]==0){
        for(int i=0;i<self.offArray.count;i++){
            IMGOfferType *tmpOfferType = [self.offArray objectAtIndex:i];
            if([tmpOfferType.off intValue]==[offerType.off intValue]){
                [self.offArray removeObjectAtIndex:i];
                break;
            }
        }
    }else if([offerType.eventType intValue] == 1){
        for(int i=0;i<self.eventArray.count;i++){
            IMGOfferType *tmpOfferType = [self.eventArray objectAtIndex:i];
            if([tmpOfferType.offerId intValue]==[offerType.offerId intValue]){
                [self.eventArray removeObjectAtIndex:i];
                break;
            }
        }
    }else{
        for(int i=0;i<self.offerTypeArray.count;i++){
            IMGOfferType *tmpOfferType = [self.offerTypeArray objectAtIndex:i];
            if([tmpOfferType.typeId intValue]==[offerType.typeId intValue]){
                [self.offerTypeArray removeObjectAtIndex:i];
                break;
            }
        }
    }
    
}

-(BOOL)hasOfferType:(IMGOfferType *)offerType{
    if((self.offArray==nil||self.offArray.count==0)&&(self.offerTypeArray==nil||self.offerTypeArray.count==0)&&(self.eventArray==nil||self.eventArray.count==0)){
        return NO;
    }
    if([offerType.typeId intValue]==0){
        for(IMGOfferType *tmpOfferType in self.offArray){
            if([tmpOfferType.off intValue]==[offerType.off intValue]){
                return YES;
            }
        }
    }else if([offerType.eventType intValue]==1){
        for(IMGOfferType *tmpOfferType in self.eventArray){
            if([tmpOfferType.offerId intValue]==[offerType.offerId intValue]){
                return YES;
            }
        }
    }else{
        for(IMGOfferType *tmpOfferType in self.offerTypeArray){
            if([tmpOfferType.typeId intValue]==[offerType.typeId intValue]){
                return YES;
            }
        }
    }
    return NO;
}

-(void)removeOccasion:(IMGTag *)tag{
    if(![self hasOccasion:tag]){
        return;
    }
    if(self.occasionArray==nil){
        self.occasionArray = [[NSMutableArray alloc]initWithCapacity:0];
        return;
    }
    for(int i=0;i<self.occasionArray.count;i++){
        IMGTag *tmpTag = [self.occasionArray objectAtIndex:i];
        if([tag.tagId intValue]==[tmpTag.tagId intValue]){
            [self.occasionArray removeObjectAtIndex:i];
            break;
        }
    }
}

-(void)addOccasion:(IMGTag *)tag{
    if([self hasOccasion:tag]){
        return;
    }
    if(self.occasionArray==nil){
        self.occasionArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    [self.occasionArray addObject:tag];
}

-(BOOL)hasOccasion:(IMGTag *)tag{
    if(self.occasionArray==nil||self.occasionArray.count==0){
        return NO;
    }
    
    for(IMGTag *tmpTag  in self.occasionArray){
        if([tag.tagId intValue]==[tmpTag.tagId intValue]){
            return YES;
        }
    }
    
    return NO;
}

-(BOOL)hasOccasionWithTagId:(NSInteger)tagId{
    if(self.occasionArray==nil||self.occasionArray.count==0){
        return NO;
    }
    
    for(IMGTag *tmpTag  in self.occasionArray){
        if(tagId==[tmpTag.tagId intValue]){
            return YES;
        }
    }
    
    return NO;
}
-(void)removeArea:(IMGArea *)area{
    if(![self hasArea:area]){
        return;
    }
    if(self.areaArray==nil){
        self.areaArray = [[NSMutableArray alloc]initWithCapacity:0];
        return;
    }
    for(int i=0;i<self.areaArray.count;i++){
        IMGArea *tmpArea = [self.areaArray objectAtIndex:i];
        if([area.tagId intValue]==[tmpArea.tagId intValue]){
            [self.areaArray removeObjectAtIndex:i];
            break;
        }
    }
}

-(void)addArea:(IMGArea *)area{
    if([self hasArea:area]){
        return;
    }
    if(self.areaArray==nil){
        self.areaArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    [self.areaArray addObject:area];
}

-(BOOL)hasArea:(IMGArea *)area{
    if(self.areaArray==nil||self.areaArray.count==0){
        return NO;
    }
    
    for(IMGArea *tmpArea  in self.areaArray){
        if ([tmpArea isKindOfClass:[IMGArea class]]) {
            if([area.tagId intValue]==[tmpArea.tagId intValue]){
                return YES;
            }
        }
    }
    return NO;
}


-(void)removeTag:(IMGTag *)tag{
    if(![self hasTag:tag]){
        return;
    }
    if(self.tagsArr==nil){
        self.tagsArr = [[NSMutableArray alloc]initWithCapacity:0];
        return;
    }
    for(int i=0;i<self.tagsArr.count;i++){
        IMGTag *tmpTag = [self.tagsArr objectAtIndex:i];
        if([tag.tagId intValue]==[tmpTag.tagId intValue]){
            [self.tagsArr removeObjectAtIndex:i];
            break;
        }
    }
}

-(void)addTag:(IMGTag *)tag{
    if([self hasTag:tag]){
        return;
    }
    if(self.tagsArr==nil){
        self.tagsArr = [[NSMutableArray alloc]initWithCapacity:0];
    }
    [self.tagsArr addObject:tag];
}

-(BOOL)hasTag:(IMGTag *)tag{
    if(self.tagsArr==nil||self.tagsArr.count==0){
        return NO;
    }
    
    for(IMGTag *tmpTag  in self.tagsArr){
        if([tag.tagId intValue]==[tmpTag.tagId intValue]){
            return YES;
        }
    }
    
    return NO;
}


-(void)removeCuisine:(IMGCuisine *)cuisine{
    if(![self hasCuisine:cuisine]){
        return;
    }
    if(self.cuisineArray==nil){
        self.cuisineArray = [[NSMutableArray alloc]initWithCapacity:0];
        return;
    }
    for(int i=0;i<self.cuisineArray.count;i++){
        IMGCuisine *tmpCuisine = [self.cuisineArray objectAtIndex:i];
        if([tmpCuisine.cuisineId intValue]==[cuisine.cuisineId intValue]){
            [self.cuisineArray removeObjectAtIndex:i];
            break;
        }
    }
}

-(void)addCuisine:(IMGCuisine *)cuisine{
    if([self hasCuisine:cuisine]){
        return;
    }
    if(self.cuisineArray==nil){
        self.cuisineArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    [self.cuisineArray addObject:cuisine];
}

-(BOOL)hasCuisine:(IMGCuisine *)cuisine{
    if(self.cuisineArray==nil||self.cuisineArray.count==0){
        return NO;
    }

    for(IMGCuisine *tmpCuisine in self.cuisineArray){
        if([tmpCuisine.cuisineId intValue]==[cuisine.cuisineId intValue]){
            return YES;
        }
    }

    return NO;
}
-(void)removeDistrict:(IMGDistrict *)district{
    if(![self hasDistrict:district]){
        return;
    }
    if(self.searchDistrictArray==nil){
        self.searchDistrictArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    for(int i=0;i<self.searchDistrictArray.count;i++){
        IMGDistrict *tmpDistrict = [self.searchDistrictArray objectAtIndex:i];
        if([tmpDistrict.districtId intValue]==[district.districtId intValue]){
            [self.searchDistrictArray removeObjectAtIndex:i];
            break;
        }
    }
}

-(void)addDistrict:(IMGDistrict *)district{
    if([self hasDistrict:district]){
        return;
    }
    if(self.searchDistrictArray==nil){
        self.searchDistrictArray = [[NSMutableArray alloc]initWithCapacity:0];
    }
    [self.searchDistrictArray addObject:district];
}

-(BOOL)hasDistrict:(IMGDistrict *)district
{
    if(self.searchDistrictArray==nil||self.searchDistrictArray.count==0){
        return NO;
    }
    
    for(IMGDistrict *tmpDistrict in self.searchDistrictArray){
        if([tmpDistrict.districtId intValue]==[district.districtId intValue]){
            return YES;
        }
    }
    
    return NO;

}

-(BOOL)hasFilters{
    if(self.offArray!=nil && [self.offArray count]>0){
        return YES;
    }
    if(self.offerTypeArray!=nil && [self.offerTypeArray count]>0){
        return YES;
    }
    if(self.eventArray!=nil && [self.eventArray count]>0){
        return YES;
    }
    if(self.cuisineArray!=nil && [self.cuisineArray count]>0){
        return YES;
    }
    if(self.tagsArr!=nil && [self.tagsArr count]>0){
        return YES;
    }
    if(self.occasionArray!=nil && [self.occasionArray count]>0){
        return YES;
    }
    if (self.areaArray!=nil&&[self.areaArray count]>0) {
        return YES;
    }
    if(self.priceLevelArray!=nil && [self.priceLevelArray count]>0&&[self.priceLevelArray count]!=4){
        return YES;
    }
    if (self.searchDistrictArray!=nil&&[self.searchDistrictArray count]>0) {
        return YES;
    }

    return NO;
}

-(NSString*)ReturnFiltersName{
    NSMutableArray* FiltersArr=[[NSMutableArray alloc]init];
    if(self.cuisineArray!=nil && [self.cuisineArray count]>0){
        [FiltersArr addObject:self.cuisineArray];
    }
    if(self.tagsArr!=nil && [self.tagsArr count]>0){
        [FiltersArr addObject:self.tagsArr];
    }
    if (self.areaArray!=nil&&[self.areaArray count]>0) {
        [FiltersArr addObject:self.areaArray];
        

    }
    if (self.offArray!=nil&&[self.offArray count]>0) {
        [FiltersArr addObject:self.offArray];
        
        
    }
    if (self.offerTypeArray!=nil&&[self.offerTypeArray count]>0&&self.offArray.count==0) {
            [FiltersArr addObject:self.offerTypeArray];

    }


    if (self.searchDistrictArray!=nil&&[self.searchDistrictArray count]>0) {
       [FiltersArr addObject:self.searchDistrictArray];
    }
    if(self.priceLevelArray!=nil && [self.priceLevelArray count]>0&&[self.priceLevelArray count]!=4){
        [FiltersArr addObject:self.priceLevelArray];
    }
    if (FiltersArr.count>1) {
        return @"Filters";
    }
    if (self.areaArray!=nil&&[self.areaArray count]>0) {
          return @"Filter";
        
    }

    if(self.cuisineArray!=nil && [self.cuisineArray count]>0){
        return @"Cuisines";
    }
    if(self.tagsArr!=nil && [self.tagsArr count]>0){
        
        NSString* str=@"";
        if (self.tagsArr.count==1) {
          str =@"Filter";
        }else{
          str=@"Filters";
        }
        return str;
    }
    if (self.searchDistrictArray!=nil&&[self.searchDistrictArray count]>0) {
        return @"Location";
    }
    if(self.priceLevelArray!=nil && [self.priceLevelArray count]>0&&[self.priceLevelArray count]!=4){
        return @"Price";
    }
    if (self.offArray!=nil&&[self.offArray count]>0) {
        
        return @"Promo";
    }
    if (self.offerTypeArray!=nil&&[self.offerTypeArray count]>0) {
        
        return @"Promo";
    }


    return @"";
}

-(void)resetMapParam{
//    self.journalId = nil;
//    self.restaurantListId = nil;
//    self.diningGuideId = nil;
}

@end
