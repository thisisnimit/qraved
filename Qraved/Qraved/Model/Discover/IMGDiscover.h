//
//  IMGDiscover.h
//  Qraved
//
//  Created by Jeff on 10/16/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGDiscoverEntity.h"
#import "IMGDistrict.h"
#import "IMGArea.h"

#define SORTBY_POPULARITY @"popularity"
#define SORTBY_OFFERS @"Offers"
#define SORTBY_BOOKINGS @"Bookings"
#define SORTBY_DISTANCE @"Distance"
#define SORTBY_RATING @"Rating"
#define SORTBY_RELEVANCE @"Relevance"
typedef  NS_ENUM(NSInteger ,WillGoto){
        Isnull,
        IsNearby,
        IsLunch,
        IsDinner,
        IsBooking,
        IsDelivery,
        Ispromo,
        IsPopular
    
};
@interface IMGDiscover : IMGDiscoverEntity

@property(nonatomic,retain) NSDate  *bookDate;

@property(nonatomic,retain) NSMutableArray* priceLevelArray;
@property(nonatomic,retain) NSMutableArray* districtArray;
@property(nonatomic,retain) NSMutableArray* searchDistrictArray;
@property(nonatomic,retain) NSMutableArray* landmarkArray;
@property(nonatomic,retain) NSMutableArray* foodTypeArray;
@property(nonatomic,retain) NSMutableArray* offArray;
@property(nonatomic,retain) NSMutableArray* offerTypeArray;
@property(nonatomic,retain) NSMutableArray* eventArray;

@property(nonatomic,retain) NSMutableArray* tagsArr;
@property(nonatomic,retain) NSMutableArray* tagUpdateArr;

@property(nonatomic,retain) NSMutableArray* occasionArray;
@property(nonatomic,retain) NSMutableArray* cuisineArray;
@property(nonatomic,retain) NSMutableArray* areaArray;
@property(nonatomic,retain) NSMutableArray* foodArray;

@property (nonatomic,assign)BOOL fromCity; //0
@property (nonatomic,assign)BOOL fromNearBy; //1
@property (nonatomic,assign)BOOL fromLocation; //2
@property (nonatomic,assign)BOOL fromDiscover; //3
@property (nonatomic,assign)BOOL fromEvent; //4
@property (nonatomic,assign)BOOL fromFood; //5
@property (nonatomic,assign)BOOL fromPopular; //6
@property (nonatomic,assign)BOOL fromBooked; //7
@property (nonatomic,assign)BOOL fromHome; //8
@property (nonatomic,assign)BOOL fromRDPEvent; //9
@property (nonatomic,assign)WillGoto gotoWhere;
@property(nonatomic,retain) NSNumber  *fromWhere;

@property(nonatomic,retain) NSNumber *journalId;
@property(nonatomic,retain) NSNumber *restaurantListId;
@property(nonatomic,retain) NSNumber *diningGuideId;
@property(nonatomic,retain) NSNumber *openNowToggle;
@property(nonatomic,retain) NSNumber *myListToggle;
@property(nonatomic,retain) NSNumber *ratingNumber;


-(BOOL)hasFilters;

-(void)removeOfferType:(IMGOfferType *)offerType;
-(void)addOfferType:(IMGOfferType *)offerType;
-(BOOL)hasOfferType:(IMGOfferType *)offerType;

-(void)removeTag:(IMGTag *)tag;
-(void)addTag:(IMGTag *)tag;
-(BOOL)hasTag:(IMGTag *)tag;

-(void)removeArea:(IMGArea *)tag;
-(void)addArea:(IMGArea *)tag;
-(BOOL)hasArea:(IMGArea *)tag;


-(void)removeOccasion:(IMGTag *)tag;
-(void)addOccasion:(IMGTag *)tag;
-(BOOL)hasOccasion:(IMGTag *)tag;
-(BOOL)hasOccasionWithTagId:(NSInteger)tagId;

-(void)removeCuisine:(IMGCuisine *)cuisine;
-(void)addCuisine:(IMGCuisine *)cuisine;
-(BOOL)hasCuisine:(IMGCuisine *)cuisine;

-(void)removeDistrict:(IMGDistrict *)district;
-(void)addDistrict:(IMGDistrict *)district;
-(BOOL)hasDistrict:(IMGDistrict *)district;

-(void)initFromDiscoverEntity:(IMGDiscoverEntity*)discoverEntity;
-(NSString*)ReturnFiltersName;
-(void)resetMapParam;
@end
