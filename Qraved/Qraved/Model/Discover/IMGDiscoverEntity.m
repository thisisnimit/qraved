//
//  IMGDiscoverEntity.m
//  Qraved
//
//  Created by Laura on 14/12/10.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGDiscoverEntity.h"


@implementation IMGDiscoverEntity

-(void)initFromOnBoarding:(NSMutableDictionary *)paramDic{
    
    NSString *cityId = [paramDic objectForKey:@"cityId"];
    if (cityId!=nil) {
        self.cityId=[NSNumber numberWithLongLong:[cityId longLongValue]];
    }
    
    self.keyword = [paramDic objectForKey:@"keyword"];
    
    if ([paramDic objectForKey:@"sortby"]) {
        NSString *sortbyNum =[paramDic objectForKey:@"sortby"];
        if ([sortbyNum isEqualToString:@"0"]) {
            self.sortby = @"Popularity";
        }else if ([sortbyNum isEqualToString:@"1"]) {
            self.sortby = @"Offers";
        }else if ([sortbyNum isEqualToString:@"2"]) {
            self.sortby = @"Bookings";
        }else if ([sortbyNum isEqualToString:@"3"]) {
            self.sortby = @"Rating";
        }else if ([sortbyNum isEqualToString:@"4"]) {
            self.sortby = @"Distance";
        }
    }
    
    self.bookDateString = [paramDic objectForKey:@"bookDate"];
    
    NSString *bookTimeString=[paramDic objectForKey:@"bookTime"];
    if(bookTimeString!=nil){
        self.bookTime=bookTimeString;
    }else{
        self.bookTime = @"";
    }
    
    NSString *pax = [paramDic objectForKey:@"pax"];
    if (pax!=nil) {
        self.pax = [NSNumber numberWithInt:[pax intValue]];
    }
    
    NSString *endPriceStr = [paramDic objectForKey:@"endPrice"];
    NSString *startPriceStr = [paramDic objectForKey:@"startPrice"];
    self.priceLevelString = @"";
    if ( (endPriceStr !=nil) && (startPriceStr != nil) ) {
        NSNumber *endPrice = [NSNumber numberWithInt:[endPriceStr intValue]];
        NSNumber *startPrice = [NSNumber numberWithInt:[startPriceStr intValue]];
        for (int i=[startPrice intValue];i<=[endPrice intValue];i++){
            IMGPriceLevel *priceLevel = [[IMGPriceLevel alloc] init];
            
            if(i==1){
                priceLevel.priceLevelId = [NSNumber numberWithInt:1];
            }else if(i==2){
                priceLevel.priceLevelId = [NSNumber numberWithInt:2];
                
            }else if(i==3){
                priceLevel.priceLevelId = [NSNumber numberWithInt:3];
            }else{
                priceLevel.priceLevelId = [NSNumber numberWithInt:4];
            }
            if (self.priceLevelString.length == 0) {
                self.priceLevelString = [self.priceLevelString stringByAppendingFormat:@"%@",priceLevel.priceLevelId];
            }else{
                self.priceLevelString = [self.priceLevelString stringByAppendingFormat:@"&&&&%@",priceLevel.priceLevelId];
            }
        }
    }
    
    
    self.districtString = @"";
    NSString *districtIdsStr=[paramDic objectForKey:@"districtIDs"];
    if (districtIdsStr!=nil) {
        NSArray *_districtArray = [districtIdsStr componentsSeparatedByString:@","];
        for(NSString *districtId in _districtArray){
            if (self.districtString.length == 0) {
                self.districtString = [self.districtString stringByAppendingFormat:@"%@",districtId];
            }else{
                self.districtString = [self.districtString stringByAppendingFormat:@"&&&&%@",districtId];
            }
        }
    }
    
    self.offString = @"";
    self.offersString = @"";
    NSString *offerTypeStr = [paramDic objectForKey:@"offerTypes"];
    NSString *offStr = [paramDic objectForKey:@"offs"];
    if ((offerTypeStr!=nil) && (offStr!=nil)) {
        NSArray *offerTypeStrArr = [offerTypeStr componentsSeparatedByString:@","];
        NSArray *offStrArr = [offStr componentsSeparatedByString:@","];
        for(int i = 0 ; i < offerTypeStrArr.count ;i++){
            NSNumber *offerType=[NSNumber numberWithInt:[((NSString*)offerTypeStrArr[i]) intValue]];
            NSNumber *off;
            if (offStrArr.count>i) {
                off=[NSNumber numberWithInt:[((NSString*)offStrArr[i]) intValue]];
            }
            if(offerType!=nil && [offerType intValue]==0 && [off intValue]!=0){
                IMGOfferType *offerType1=[[IMGOfferType alloc] init];
                offerType1.typeId = [NSNumber numberWithInt:0];
                if([off intValue]==50){
                    offerType1.offerId = [NSNumber numberWithInt:5];
                }else if([off intValue]==40){
                    offerType1.offerId = [NSNumber numberWithInt:4];
                }else if([off intValue]==30){
                    offerType1.offerId = [NSNumber numberWithInt:3];
                }else if([off intValue]==20){
                    offerType1.offerId = [NSNumber numberWithInt:2];
                }else if([off intValue]==10){
                    offerType1.offerId = [NSNumber numberWithInt:1];
                }
                if (self.offString.length == 0) {
                    self.offString = [self.offString stringByAppendingFormat:@"%@+%@",offerType1.offerId,offerType1.typeId];
                }else{
                    self.offString = [self.offString stringByAppendingFormat:@"&&&&%@+%@",offerType1.offerId,offerType1.typeId];
                }
            }else{
                IMGOfferType *offerType1=[[IMGOfferType alloc] init];
                offerType1.typeId = offerType;
                offerType1.off = [NSNumber numberWithInt:0];
                if([offerType intValue]==1){
                    offerType1.offerId = [NSNumber numberWithInt:6];
                }else if([offerType intValue]==2){
                    offerType1.offerId = [NSNumber numberWithInt:7];
                }else if([offerType intValue]==3){
                    offerType1.offerId = [NSNumber numberWithInt:8];
                }
                if (self.offersString.length == 0) {
                    self.offersString = [self.offersString stringByAppendingFormat:@"%@+%@",offerType1.offerId,offerType1.typeId];
                }else{
                    self.offersString = [self.offersString stringByAppendingFormat:@"&&&&%@+%@",offerType1.offerId,offerType1.typeId];
                }
            }
        }
        
        
    }
    
    self.tagString = @"";
    NSString *tagsIdsStr=[paramDic objectForKey:@"tagIDs"];
    if (tagsIdsStr!=nil) {
        NSArray *tagsIdsStrArr = [tagsIdsStr componentsSeparatedByString:@","];
        for(NSString *tagId in tagsIdsStrArr){
            if (self.tagString.length == 0) {
                self.tagString = [self.tagString stringByAppendingFormat:@"%@",tagId];
            }else{
                self.tagString = [self.tagString stringByAppendingFormat:@"&&&&%@",tagId];
            }
        }
    }
    
    self.cuisineString = @"";
    NSString *cuisineIdsStr=[paramDic objectForKey:@"cuisineIDs"];
    if (cuisineIdsStr!=nil) {
        NSArray *cuisineIdsStrArr = [cuisineIdsStr componentsSeparatedByString:@","];
        for(NSString *cuisineId in cuisineIdsStrArr){
            if (self.cuisineString.length == 0) {
                self.cuisineString = [self.cuisineString stringByAppendingFormat:@"%@",cuisineId];
            }else{
                self.cuisineString = [self.cuisineString stringByAppendingFormat:@"&&&&%@",cuisineId];
            }
        }
    }
    
    self.areaString = @"";
    NSString *areaIdsStr=[paramDic objectForKey:@"areaIDs"];
    if (areaIdsStr!=nil) {
        NSArray *areaIdsStrArr = [areaIdsStr componentsSeparatedByString:@","];
        for(NSString *areaId in areaIdsStrArr){
            if (self.areaString.length == 0) {
                self.areaString = [self.areaString stringByAppendingFormat:@"%@",areaId];
            }else{
                self.areaString = [self.areaString stringByAppendingFormat:@"&&&&%@",areaId];
            }
        }
    }
}

-(void)initFromUrl:(NSString *)url{
    
    if ([url rangeOfString:@"app/searchResultPage"].location != NSNotFound){
        NSRange range = [url rangeOfString:@"?"];
        if (range.location != NSNotFound){
            NSString *paramStr = [url substringFromIndex:range.location+1];
            NSArray *paramArr = [paramStr componentsSeparatedByString:@"&"];
            if ( (paramArr!=nil) && (paramArr.count!=0) ) {
                NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
                for (NSString *item in paramArr) {
                    NSRange range = [item rangeOfString:@"="];
                    if (range.location != NSNotFound){
                        NSString *key = [item substringToIndex:range.location];
                        NSString *value = [item substringFromIndex:range.location+1];
                        [paramDic setValue:value forKey:key];
                    }
                }
                
                NSString *cityId = [paramDic objectForKey:@"cityId"];
                if (cityId!=nil) {
                    self.cityId=[NSNumber numberWithLongLong:[cityId longLongValue]];
                }
                
                self.keyword = [paramDic objectForKey:@"keyword"];
                
                if ([paramDic objectForKey:@"sortby"]) {
                    NSString *sortbyNum =[paramDic objectForKey:@"sortby"];
                    if ([sortbyNum isEqualToString:@"0"]) {
                        self.sortby = @"Popularity";
                    }else if ([sortbyNum isEqualToString:@"1"]) {
                        self.sortby = @"Offers";
                    }else if ([sortbyNum isEqualToString:@"2"]) {
                        self.sortby = @"Bookings";
                    }else if ([sortbyNum isEqualToString:@"3"]) {
                        self.sortby = @"Rating";
                    }else if ([sortbyNum isEqualToString:@"4"]) {
                        self.sortby = @"Distance";
                    }
                }
                
                self.bookDateString = [paramDic objectForKey:@"bookDate"];
                
                NSString *bookTimeString=[paramDic objectForKey:@"bookTime"];
                if(bookTimeString!=nil){
                    self.bookTime=bookTimeString;
                }else{
                    self.bookTime = @"";
                }
                
                NSString *pax = [paramDic objectForKey:@"pax"];
                if (pax!=nil) {
                    self.pax = [NSNumber numberWithInt:[pax intValue]];
                }
                
                NSString *endPriceStr = [paramDic objectForKey:@"endPrice"];
                NSString *startPriceStr = [paramDic objectForKey:@"startPrice"];
                self.priceLevelString = @"";
                if ( (endPriceStr !=nil) && (startPriceStr != nil) ) {
                    NSNumber *endPrice = [NSNumber numberWithInt:[endPriceStr intValue]];
                    NSNumber *startPrice = [NSNumber numberWithInt:[startPriceStr intValue]];
                    for (int i=[startPrice intValue];i<=[endPrice intValue];i++){
                        IMGPriceLevel *priceLevel = [[IMGPriceLevel alloc] init];
                        
                        if(i==1){
                            priceLevel.priceLevelId = [NSNumber numberWithInt:1];
                        }else if(i==2){
                            priceLevel.priceLevelId = [NSNumber numberWithInt:2];
                            
                        }else if(i==3){
                            priceLevel.priceLevelId = [NSNumber numberWithInt:3];
                        }else{
                            priceLevel.priceLevelId = [NSNumber numberWithInt:4];
                        }
                        if (self.priceLevelString.length == 0) {
                            self.priceLevelString = [self.priceLevelString stringByAppendingFormat:@"%@",priceLevel.priceLevelId];
                        }else{
                            self.priceLevelString = [self.priceLevelString stringByAppendingFormat:@"&&&&%@",priceLevel.priceLevelId];
                        }
                    }
                }
                
                
                self.districtString = @"";
                NSString *districtIdsStr=[paramDic objectForKey:@"districtIDs"];
                if (districtIdsStr!=nil) {
                    NSArray *_districtArray = [districtIdsStr componentsSeparatedByString:@","];
                    for(NSString *districtId in _districtArray){
                        if (self.districtString.length == 0) {
                            self.districtString = [self.districtString stringByAppendingFormat:@"%@",districtId];
                        }else{
                            self.districtString = [self.districtString stringByAppendingFormat:@"&&&&%@",districtId];
                        }
                    }
                }
                
                self.offString = @"";
                self.offersString = @"";
                NSString *offerTypeStr = [paramDic objectForKey:@"offerTypes"];
                NSString *offStr = [paramDic objectForKey:@"offs"];
                if ((offerTypeStr!=nil) && (offStr!=nil)) {
                    NSArray *offerTypeStrArr = [offerTypeStr componentsSeparatedByString:@","];
                    NSArray *offStrArr = [offStr componentsSeparatedByString:@","];
                    for(int i = 0 ; i < offerTypeStrArr.count ;i++){
                        NSNumber *offerType=[NSNumber numberWithInt:[((NSString*)offerTypeStrArr[i]) intValue]];
                        NSNumber *off;
                        if (offStrArr.count>i) {
                            off=[NSNumber numberWithInt:[((NSString*)offStrArr[i]) intValue]];
                        }
                        if(offerType!=nil && [offerType intValue]==0 && [off intValue]!=0){
                            IMGOfferType *offerType1=[[IMGOfferType alloc] init];
                            offerType1.typeId = [NSNumber numberWithInt:0];
                            if([off intValue]==50){
                                offerType1.offerId = [NSNumber numberWithInt:5];
                            }else if([off intValue]==40){
                                offerType1.offerId = [NSNumber numberWithInt:4];
                            }else if([off intValue]==30){
                                offerType1.offerId = [NSNumber numberWithInt:3];
                            }else if([off intValue]==20){
                                offerType1.offerId = [NSNumber numberWithInt:2];
                            }else if([off intValue]==10){
                                offerType1.offerId = [NSNumber numberWithInt:1];
                            }
                            if (self.offString.length == 0) {
                                self.offString = [self.offString stringByAppendingFormat:@"%@+%@",offerType1.offerId,offerType1.typeId];
                            }else{
                                self.offString = [self.offString stringByAppendingFormat:@"&&&&%@+%@",offerType1.offerId,offerType1.typeId];
                            }
                        }else{
                            IMGOfferType *offerType1=[[IMGOfferType alloc] init];
                            offerType1.typeId = offerType;
                            offerType1.off = [NSNumber numberWithInt:0];
                            if([offerType intValue]==1){
                                offerType1.offerId = [NSNumber numberWithInt:6];
                            }else if([offerType intValue]==2){
                                offerType1.offerId = [NSNumber numberWithInt:7];
                            }else if([offerType intValue]==3){
                                offerType1.offerId = [NSNumber numberWithInt:8];
                            }
                            if (self.offersString.length == 0) {
                                self.offersString = [self.offersString stringByAppendingFormat:@"%@+%@",offerType1.offerId,offerType1.typeId];
                            }else{
                                self.offersString = [self.offersString stringByAppendingFormat:@"&&&&%@+%@",offerType1.offerId,offerType1.typeId];
                            }
                        }
                    }

                    
                }
            
                self.tagString = @"";
                NSString *tagsIdsStr=[paramDic objectForKey:@"tagIDs"];
                if (tagsIdsStr!=nil) {
                    NSArray *tagsIdsStrArr = [tagsIdsStr componentsSeparatedByString:@","];
                    for(NSString *tagId in tagsIdsStrArr){
                        if (self.tagString.length == 0) {
                            self.tagString = [self.tagString stringByAppendingFormat:@"%@",tagId];
                        }else{
                            self.tagString = [self.tagString stringByAppendingFormat:@"&&&&%@",tagId];
                        }
                    }
                }
                
                self.cuisineString = @"";
                NSString *cuisineIdsStr=[paramDic objectForKey:@"cuisineIDs"];
                if (cuisineIdsStr!=nil) {
                    NSArray *cuisineIdsStrArr = [cuisineIdsStr componentsSeparatedByString:@","];
                    for(NSString *cuisineId in cuisineIdsStrArr){
                        if (self.cuisineString.length == 0) {
                            self.cuisineString = [self.cuisineString stringByAppendingFormat:@"%@",cuisineId];
                        }else{
                            self.cuisineString = [self.cuisineString stringByAppendingFormat:@"&&&&%@",cuisineId];
                        }
                    }
                }
                
                self.areaString = @"";
                NSString *areaIdsStr=[paramDic objectForKey:@"areaIDs"];
                if (areaIdsStr!=nil) {
                    NSArray *areaIdsStrArr = [areaIdsStr componentsSeparatedByString:@","];
                    for(NSString *areaId in areaIdsStrArr){
                        if (self.areaString.length == 0) {
                            self.areaString = [self.areaString stringByAppendingFormat:@"%@",areaId];
                        }else{
                            self.areaString = [self.areaString stringByAppendingFormat:@"&&&&%@",areaId];
                        }
                    }
                }
                
            }
        }
    }
}
-(void)initFromDictionary:(NSDictionary *)dictionary{
    self.discoverId=[dictionary objectForKey:@"id"];
    self.status=[dictionary objectForKey:@"status"];
    self.name=[dictionary objectForKey:@"name"];
    self.keyword=[dictionary objectForKey:@"searchContent"];
    self.imageUrl=[dictionary objectForKey:@"imageUrl"];
    self.cityId=[dictionary objectForKey:@"cityId"];
    NSArray *sortbyArray=[dictionary objectForKey:@"sortby"];
    if(sortbyArray!=nil&&[sortbyArray count]>0){
        self.sortby=[sortbyArray objectAtIndex:0];
    }
    self.bookDateString=[dictionary objectForKey:@"bookDate"];
    
    NSString *bookTimeString=[dictionary objectForKey:@"bookTime"];
    if(bookTimeString!=nil){
        self.bookTime=bookTimeString;
    }else{
        self.bookTime = @"";
    }
    NSNumber *pax=[dictionary objectForKey:@"pax"];
    self.pax=pax;
    
    NSNumber *endPrice = [dictionary objectForKey:@"endPrice"];
    NSNumber *startPrice = [dictionary objectForKey:@"startPrice"];
    self.priceLevelString = @"";
    for (int i=[startPrice intValue];i<=[endPrice intValue];i++){
        IMGPriceLevel *priceLevel = [[IMGPriceLevel alloc] init];

        if(i==1){
            priceLevel.priceLevelId = [NSNumber numberWithInt:1];
        }else if(i==2){
            priceLevel.priceLevelId = [NSNumber numberWithInt:2];
            
        }else if(i==3){
            priceLevel.priceLevelId = [NSNumber numberWithInt:3];
        }else{
            priceLevel.priceLevelId = [NSNumber numberWithInt:4];
        }
        if (self.priceLevelString.length == 0) {
            self.priceLevelString = [self.priceLevelString stringByAppendingFormat:@"%@",priceLevel.priceLevelId];
        }else{
            self.priceLevelString = [self.priceLevelString stringByAppendingFormat:@"&&&&%@",priceLevel.priceLevelId];
        }
    }
    
    self.districtString = @"";
    NSArray *_districtArray=[dictionary objectForKey:@"districtList"];
    for(NSDictionary *districtDictionary in _districtArray){
        NSNumber *districtId=[districtDictionary objectForKey:@"districtId"];
        if (self.districtString.length == 0) {
            self.districtString = [self.districtString stringByAppendingFormat:@"%@",districtId];
        }else{
            self.districtString = [self.districtString stringByAppendingFormat:@"&&&&%@",districtId];
        }
        
    }
    self.offString = @"";
    self.offersString = @"";
    NSArray *offerArray = [dictionary objectForKey:@"offersList"];
    for(NSDictionary *offerDictionary in offerArray){
        NSNumber *offerType=[offerDictionary objectForKey:@"offerType"];
        NSNumber *off=[offerDictionary objectForKey:@"off"];
        if(offerType!=nil && [offerType intValue]==0 && [off intValue]!=0){
            IMGOfferType *offerType1=[[IMGOfferType alloc] init];
            offerType1.typeId = [NSNumber numberWithInt:0];
            if([off intValue]==50){
                offerType1.offerId = [NSNumber numberWithInt:5];
            }else if([off intValue]==40){
                offerType1.offerId = [NSNumber numberWithInt:4];
            }else if([off intValue]==30){
                offerType1.offerId = [NSNumber numberWithInt:3];
            }else if([off intValue]==20){
                offerType1.offerId = [NSNumber numberWithInt:2];
            }else if([off intValue]==10){
                offerType1.offerId = [NSNumber numberWithInt:1];
            }
            if (self.offString.length == 0) {
                self.offString = [self.offString stringByAppendingFormat:@"%@+%@",offerType1.offerId,offerType1.typeId];
            }else{
                self.offString = [self.offString stringByAppendingFormat:@"&&&&%@+%@",offerType1.offerId,offerType1.typeId];
            }
        }else{
            IMGOfferType *offerType1=[[IMGOfferType alloc] init];
            offerType1.typeId = offerType;
            offerType1.off = [NSNumber numberWithInt:0];
            if([offerType intValue]==1){
                offerType1.offerId = [NSNumber numberWithInt:6];
            }else if([offerType intValue]==2){
                offerType1.offerId = [NSNumber numberWithInt:7];
            }else if([offerType intValue]==3){
                offerType1.offerId = [NSNumber numberWithInt:8];
            }
            if (self.offersString.length == 0) {
                self.offersString = [self.offersString stringByAppendingFormat:@"%@+%@",offerType1.offerId,offerType1.typeId];
            }else{
                self.offersString = [self.offersString stringByAppendingFormat:@"&&&&%@+%@",offerType1.offerId,offerType1.typeId];
            }
        }
    }

    self.tagString = @"";
    NSArray *tagArray=[dictionary objectForKey:@"tagsList"];
    for(NSDictionary *tagDictionary in tagArray){
        NSNumber *tagId=[tagDictionary objectForKey:@"tagId"];
        if (self.tagString.length == 0) {
            self.tagString = [self.tagString stringByAppendingFormat:@"%@",tagId];
        }else{
            self.tagString = [self.tagString stringByAppendingFormat:@"&&&&%@",tagId];
        }
    }
    self.cuisineString = @"";
    NSArray *_cuisineArray=[dictionary objectForKey:@"cuisineList"];
    for(NSDictionary *cuisineDictionary in _cuisineArray){
        NSNumber *cuisineId=[cuisineDictionary objectForKey:@"cuisineId"];
        if (self.cuisineString.length == 0) {
            self.cuisineString = [self.cuisineString stringByAppendingFormat:@"%@",cuisineId];
        }else{
            self.cuisineString = [self.cuisineString stringByAppendingFormat:@"&&&&%@",cuisineId];
        }
    }
    
    self.areaString = @"";
    NSArray *_areaArray=[dictionary objectForKey:@"areaList"];
    for(NSDictionary *areaDictionary in _areaArray){
        NSNumber *areaId=[areaDictionary objectForKey:@"tagId"];
        if (self.areaString.length == 0) {
            self.areaString = [self.areaString stringByAppendingFormat:@"%@",areaId];
        }else{
            self.areaString = [self.areaString stringByAppendingFormat:@"&&&&%@",areaId];
        }
    }
    
    
//    NSArray *_areaArray=[dictionary objectForKey:@"areaList"];
//    NSMutableArray *areaList=[[NSMutableArray alloc] init];
//    for(NSDictionary *areaDic in _areaArray){
//        [areaList addObject:[areaDic objectForKey:@"areaId"]];
//    }
//    self.areaString=[areaList componentsJoinedByString:@"&&&&"];
}

//-(void)setSortby:(NSString*)sort{
//    NSString *first=[sort substringToIndex:1];
//    NSString *left=[sort substringFromIndex:1];
//    _sortby=[NSString stringWithFormat:@"%@%@",[first uppercaseString],[left lowercaseString]];
//    if (sort.length==0) {
//     _sortby =@"";
//    }
//}

@end
