//
//  IMGDiscoverEntity.h
//  Qraved
//
//  Created by Laura on 14/12/10.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"
#import "IMGOfferType.h"
#import "IMGCuisine.h"
#import "IMGTag.h"
#import "DBManager.h"
#import "IMGPriceLevel.h"

@interface IMGDiscoverEntity : IMGEntity
@property(nonatomic,retain) NSNumber *discoverId;
@property(nonatomic,retain) NSNumber *status;
@property(nonatomic,copy)   NSString *name;
@property (nonatomic,copy)  NSString *imageUrl;
@property(nonatomic,retain) NSString *sortby;
@property(nonatomic,retain) NSString *keyword;
@property(nonatomic,retain) NSNumber *eventId;
@property(nonatomic,retain) NSNumber *cityId;
@property(nonatomic,retain) NSNumber *pax;
@property(nonatomic,retain) NSString  *bookDateString;
@property(nonatomic,retain) NSString *bookTime;

@property(nonatomic,retain) NSString* cuisineString;
@property(nonatomic,retain) NSString* districtString;
@property(nonatomic,retain) NSString* offersString;
@property(nonatomic,retain) NSString* tagString;
@property(nonatomic,retain) NSString* priceLevelString;
@property(nonatomic,retain) NSString* offString;
@property(nonatomic,retain) NSString *areaString;

@property(nonatomic,assign) BOOL needLog;

-(void)initFromDictionary:(NSDictionary *)dictionary;
-(void)initFromUrl:(NSString *)url;
-(void)initFromOnBoarding:(NSMutableDictionary *)paramDic;
@end
