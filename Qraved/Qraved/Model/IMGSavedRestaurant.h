//
//  IMGSavedRestaurant.h
//  Qraved
//
//  Created by harry on 2017/6/2.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGSavedRestaurant : IMGEntity

@property(nonatomic,retain) NSNumber *restaurantId;
@property(nonatomic,retain) NSNumber *bookingCount;
@property(nonatomic,copy)   NSString *priceName;
@property(nonatomic,retain) NSNumber *priceLevel;
@property(nonatomic,retain) NSNumber *dailyBookingLimit;
@property(nonatomic,copy)   NSString *descriptionStr;
@property(nonatomic,retain) NSNumber *dinersCount;
@property(nonatomic,retain) NSNumber *disCount;
@property(nonatomic,copy)   NSString *discountContent;
@property(nonatomic,copy)   NSString *needUpdate;
@property(nonatomic,copy)   NSString *wellKnownFor;
@property(nonatomic,copy)   NSString *journaArticlelTitle;
@property(nonatomic,copy)   NSNumber *inJournal;
@property(nonatomic,copy)   NSNumber *journalArticleId;
@property(nonatomic,copy)   NSNumber *trendingInstagram;

@property(nonatomic,retain) NSNumber *dishCount;
@property(nonatomic,copy)   NSString *displayTime;
@property(nonatomic,copy)   NSNumber *sortOrder;

@property(nonatomic,retain) NSNumber *imageCount;
@property(nonatomic,copy)   NSString *imageUrl;
@property(nonatomic,copy)   NSString *note;

@property(nonatomic,copy)   NSString *logoImage;
@property(nonatomic,retain) NSNumber *reviewCount;
@property(nonatomic,retain) NSNumber *ratingCount;
@property(nonatomic,retain)NSString* nextopenTime;
@property(nonatomic,retain)NSString* nextclosedTime;
@property(nonatomic,retain)NSString* nextopenDay;

@property(nonatomic,retain)NSString* secondopenTime;
@property(nonatomic,retain)NSString* secondclosedTime;
@property(nonatomic,retain)NSString* secondopenDay;

@property(nonatomic,retain)NSString* yesteropenTime;
@property(nonatomic,retain)NSString* yesterclosedTime;
@property(nonatomic,retain)NSString* yesteropenDay;

@property(nonatomic,copy)   NSString *seoKeyword;

@property(nonatomic,copy)   NSString *title;
@property(nonatomic,copy)   NSString *website;
@property(nonatomic,retain) NSNumber *liked;
@property(nonatomic,retain) NSNumber *saved;

@property(nonatomic,retain) NSNumber *savedCount;

@property(nonatomic,retain) NSNumber *offerCount;
@property(nonatomic,retain) NSNumber *boost;


@property(nonatomic,copy)   NSString *ratingScore;
@property(nonatomic,retain) NSNumber *latitude;
@property(nonatomic,retain) NSNumber *longitude;
@property(nonatomic,copy)   NSString *monIntermission;
@property(nonatomic,copy)   NSString *monTimeOpen;
@property(nonatomic,copy)   NSString *monTimeClosed;
@property(nonatomic,copy)   NSString *tueIntermission;
@property(nonatomic,copy)   NSString *tueTimeOpen;
@property(nonatomic,copy)   NSString *tueTimeClosed;
@property(nonatomic,copy)   NSString *wedIntermission;
@property(nonatomic,copy)   NSString *wedTimeOpen;
@property(nonatomic,copy)   NSString *wedTimeClosed;
@property(nonatomic,copy)   NSString *thuIntermission;
@property(nonatomic,copy)   NSString *thuTimeOpen;
@property(nonatomic,copy)   NSString *thuTimeClosed;
@property(nonatomic,copy)   NSString *friIntermission;
@property(nonatomic,copy)   NSString *friTimeOpen;
@property(nonatomic,copy)   NSString *friTimeClosed;
@property(nonatomic,copy)   NSString *satIntermission;
@property(nonatomic,copy)   NSString *satTimeOpen;
@property(nonatomic,copy)   NSString *satTimeClosed;
@property(nonatomic,copy)   NSString *sunIntermission;
@property(nonatomic,copy)   NSString *sunTimeOpen;
@property(nonatomic,copy)   NSString *sunTimeClosed;
@property(nonatomic,retain) NSNumber *d1;
@property(nonatomic,retain) NSNumber *d2;
@property(nonatomic,retain) NSNumber *d3;
@property(nonatomic,retain) NSNumber *d4;
@property(nonatomic,retain) NSNumber *d5;
@property(nonatomic,retain) NSNumber *d6;
@property(nonatomic,retain) NSNumber *d7;
@property(nonatomic,retain) NSNumber *cityId;
@property(nonatomic,copy)   NSString *cityName;
@property(nonatomic,retain) NSNumber *districtId;
@property(nonatomic,copy)   NSString *districtName;
@property(nonatomic,copy)   NSString *bestOfferName;
@property(nonatomic,copy)   NSString *timeUpdated;
@property(nonatomic,retain) NSNumber *state;
@property(nonatomic,retain) NSString *stateName;
@property(nonatomic,copy)   NSString *cuisineName;
-(NSString *)distance;
@end
