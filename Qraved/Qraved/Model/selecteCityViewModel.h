//
//  selecteCityViewModel.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "selecteCityCenterViewModel.h"
#import "selecteCityViewModel.h"
#import "selecteCityCenterViewModel.h"

@interface selecteCityViewModel : NSObject

@property (nonatomic, strong)  NSNumber  *myID;
@property (nonatomic, strong)  NSString  *name;
@property (nonatomic, strong)  NSArray<selecteCityCenterViewModel *>  *districts;
@end
