//
//  IMGJournalComment.m
//  Qraved
//
//  Created by Jeff on 3/7/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "IMGJournalComment.h"

@implementation IMGJournalComment

-(id)initFromDictionary:(NSDictionary*)commentDictionary{
    self = [super init];
    if(self){
        NSString *commentIdStr = [commentDictionary objectForKey:@"commentId"];
        if(commentIdStr){
            self.commentId=[NSNumber numberWithInteger:[commentIdStr integerValue]];
            self.content=[commentDictionary objectForKey:@"content"];
            NSString *createTime = [commentDictionary objectForKey:@"createTime"];
            if(createTime){
                self.createTime=[NSNumber numberWithLongLong:[createTime longLongValue]];
            }
            NSString *commentUserId =[commentDictionary objectForKey:@"userId"];
            if(commentUserId){
                self.userId=[NSNumber numberWithInteger:[commentUserId integerValue]];
            }
            self.userImageUrl=[commentDictionary objectForKey:@"userImageUrl"];
            self.userName=[commentDictionary objectForKey:@"userName"];
            if(!self.userName){
                self.userName=[commentDictionary objectForKey:@"fullName"];                
            }
        }
    }
    return self;
}

@end
