//
//  IMGJournalComment.h
//  Qraved
//
//  Created by Jeff on 3/7/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "IMGComment.h"

@interface IMGJournalComment : IMGComment

@property(nonatomic,retain) NSString *content;
@property(nonatomic,retain) NSString *createdDate;
@property(nonatomic,retain) NSNumber *createTime;
@property(nonatomic,assign) BOOL readMore;


-(id)initFromDictionary:(NSDictionary*)commentDictionary;
@end
