//
//  IMGLandMark.h
//  Qraved
//
//  Created by josn.liu on 2017/1/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "IMGEvent.h"

@interface IMGLandMark : IMGEvent
@property (nonatomic,retain) NSNumber * landMarkId;
@property (nonatomic,retain) NSNumber * restaurantCount;

@end
