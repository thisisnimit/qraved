//
//  IMGThirdPartyUser
//  Qraved
//
//  Created by Jeff on 8/25/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGThirdPartyUser : IMGEntity

@property(nonatomic,retain) NSNumber *thirdPartyUserId;
@property(nonatomic,retain) NSNumber *userId;
@property(nonatomic,retain) NSNumber *type;
@property(nonatomic,copy)   NSString *thirdId;
@property(nonatomic,copy)   NSString *firstName;
@property(nonatomic,copy)   NSString *lastName;

@end
