//
//  IMGThirdPartyUser
//  Qraved
//
//  Created by Jeff on 8/25/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGThirdPartyUser.h"

@implementation IMGThirdPartyUser

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"id"]) {
        self.thirdPartyUserId = value;
    }
}
@end
