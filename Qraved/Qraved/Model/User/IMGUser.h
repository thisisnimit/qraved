//
//  IMGUser.h
//  Qraved
//
//  Created by Jeff on 8/25/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGUser : IMGEntity<NSCoding>
+(instancetype)currentUser;

@property(nonatomic,strong) NSNumber *userId;
@property(nonatomic,copy)   NSString *email;
@property(nonatomic,copy)   NSString *firstName;
@property(nonatomic,copy)   NSString *lastName;
@property(nonatomic,copy)   NSString *fullName;
@property(nonatomic,copy)   NSString *userName;

@property(nonatomic,copy)   NSString *avatar;
@property (nonatomic, strong)  NSString  *coverImage;

@property(nonatomic,copy)   NSString *token;
@property(nonatomic,copy)   NSString *birthday;
@property(nonatomic,copy)   NSString *phone;
@property(nonatomic,copy)   NSString *website;
@property(nonatomic,copy)   NSString *gender;
@property(nonatomic,copy)   NSString *cityName;
@property(nonatomic,copy)   NSString *districtName;
@property(nonatomic,copy)   NSString *facebookAcount;
@property(nonatomic,copy)   NSString *twitterAcount;
@property(nonatomic,copy)   NSString *lastLoginDate;

@property(nonatomic,strong) NSNumber *bookingCount;
@property(nonatomic,strong) NSNumber *redeemablePoints;
@property(nonatomic,strong) NSNumber *reviewCount;

@property(nonatomic,strong) NSNumber *islogin;

@property(nonatomic,copy)   NSString *countryName;
@property(nonatomic,strong) NSNumber *isNewUser;
@property(nonatomic,strong) NSNumber *qravedCount;
@property(nonatomic,strong) NSNumber *favoriteCount;
@property(nonatomic,strong) NSNumber *pointsPerBooking;

@property(nonatomic,strong) NSNumber *photoCount;
@property (nonatomic,strong) NSNumber *moderateReviewId;
@property (nonatomic,strong) NSString *dateCreated;





-(void)setValueWithUser:(IMGUser *)user;
+(BOOL)checkIsSelf:(NSString *)userID;

-(void)logOut;
//+(void)registerMixpanelSuperProperties;
@end
