//
//  IMGUserFavoriteRestaurant
//  Qraved
//
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGEntity.h"

@interface IMGUserFavoriteRestaurant : IMGEntity
@property(nonatomic,retain) NSNumber *restaurantId;
@property(nonatomic,retain) NSNumber *userId;

@end
