//
//  IMGUser.m
//  Qraved
//
//  Created by Jeff on 8/25/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "IMGUser.h"
#import "AppDelegate.h"
//#import "Mixpanel.h"
#import "UserDataHandler.h"


@implementation IMGUser
-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"id"]) {
        self.userId = value;
    }
}

static IMGUser * user;
+(instancetype)currentUser{
    if(user==nil){
        user=[[IMGUser alloc]init];
    }
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
    if ([shared objectForKey:@"person"]) {
        user=(IMGUser*)[NSKeyedUnarchiver unarchiveObjectWithData:[shared objectForKey:@"person"]];
    }
    if ([shared objectForKey:@"imgUrl"]) {
        user.avatar = [shared objectForKey:@"imgUrl"];

    }
    if ([shared objectForKey:@"dateCreated"]) {
        user.dateCreated = [shared objectForKey:@"dateCreated"];
    }

    return  user;
}
-(NSString *)fullName{
    
    
    return [[[self.firstName capitalizedString]stringByAppendingFormat:@" %@",[self.lastName capitalizedString]] filterHtml];
}
-(NSString *)userName{
    return [_userName filterHtml];
}
-(NSString*)firstName{
    return [_firstName filterHtml];
}
-(NSString*)lastName{
    return [_lastName filterHtml];
}
-(void)setValueWithUser:(IMGUser *)user{

    self.userId=user.userId;
    self.email=user.email;
    self.firstName=user.firstName;
    self.lastName=user.lastName;
    self.fullName=user.fullName;
    self.userName=user.userName;
    self.avatar=user.avatar;
    self.redeemablePoints=user.redeemablePoints;
    self.bookingCount=user.bookingCount;
    self.reviewCount=user.reviewCount;
    self.token=user.token;
    self.birthday=user.birthday;
    self.phone=user.phone;
    self.website=user.website;
    self.gender=user.gender;
    self.cityName=user.cityName;
    self.districtName=user.districtName;
    self.redeemablePoints=user.redeemablePoints;
    self.facebookAcount=user.facebookAcount;
    self.twitterAcount=user.twitterAcount;
}


//+(void)registerMixpanelSuperProperties{
//    if (![AppDelegate ShareApp].userLogined) {
//        return;
//    }
//    NSLog(@"registerMixpanelSuperProperties");
//    IMGUser *user = [IMGUser currentUser];
//    NSMutableDictionary *mixDic = [[NSMutableDictionary alloc]initWithCapacity:0];
//    if (user.userId) {
//        [mixDic setObject:user.userId forKey:@"Customer ID"];
//    }
//    if (user.firstName) {
//        [mixDic setObject:user.firstName forKey:@"First Name"];
//    }
//    if (user.lastName) {
//        [mixDic setObject:user.lastName forKey:@"Last Name"];
//    }
//    if (user.email) {
//        [mixDic setObject:user.email forKey:@"Email"];
//    }
//    if (user.phone) {
//        [mixDic setObject:user.phone forKey:@"Phone number"];
//    }
//    if (user.cityName) {
//        [mixDic setObject:user.cityName forKey:@"City"];
//    }
//    if (user.countryName) {
//        [mixDic setObject:user.countryName forKey:@"Country"];
//    }
//    if (user.gender) {
//        [mixDic setObject:user.gender forKey:@"Gender"];
//    }
//    if (user.birthday){
//        [mixDic setObject:user.birthday forKey:@"Date of Birth"];
//    }
//    if (user.redeemablePoints) {
//        [mixDic setObject:user.redeemablePoints forKey:@"Loyalty Points"];
//    }
//    if (user.qravedCount) {
//        [mixDic setObject:user.qravedCount forKey:@"Number of Qraves"];
//    }
//    if (user.reviewCount) {
//        [mixDic setObject:user.reviewCount forKey:@"Numner of Reviews"];
//    }
//    //Number of Upcoming Bookings
//    //Numner of History Bookings
//
//    if (user.userId) {
//        Mixpanel *mixpanel = [Mixpanel sharedInstance];
//        [mixpanel clearSuperProperties];
//        [mixpanel registerSuperProperties:mixDic];
//        [mixpanel identify:[NSString stringWithFormat:@"%@",user.userId]];
//        [mixpanel.people set:mixDic];
//    }
//}
-(void)logOut{
    
    [SHK logoutOfAll];
    [UserDataHandler logout];
    [AppDelegate ShareApp].userLogined=NO;
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"userLogined"];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"loginUserId"];
    
    [[Amplitude instance] setUserId:nil];
    [[Amplitude instance] clearUserProperties];
    [AppDelegate ShareApp].isAlreadySetAmplitudeUserProperties = NO;
    
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
    [shared removeObjectForKey:@"userId"];
    [shared removeObjectForKey:@"userToken"];
    [shared removeObjectForKey:@"person"];
    [shared synchronize];
    
    //Facebook logout
//    if ([FBSession.activeSession isOpen]) {
//        // Session is open
//        [FBSession.activeSession closeAndClearTokenInformation];
//        
//        [FBSession.activeSession close];
//        FBSession.activeSession=nil;
//    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"logoutNotification" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_HOME object:nil];
    [[AppDelegate ShareApp] requestToShowNotificationCount];
    
    [IMGUser currentUser].userId = nil;
    [IMGUser currentUser].email = nil;
    [IMGUser currentUser].firstName = nil;
    [IMGUser currentUser].lastName = nil;
    [IMGUser currentUser].fullName = nil;
    [IMGUser currentUser].userName = nil;
    [IMGUser currentUser].avatar = nil;
    [IMGUser currentUser].token = nil;
    [IMGUser currentUser].birthday = nil;
    [IMGUser currentUser].phone = nil;
    [IMGUser currentUser].website = nil;
    [IMGUser currentUser].gender = nil;
    [IMGUser currentUser].cityName = nil;
    [IMGUser currentUser].districtName = nil;
    [IMGUser currentUser].facebookAcount = nil;
    [IMGUser currentUser].twitterAcount = nil;
    [IMGUser currentUser].lastLoginDate = nil;
    [IMGUser currentUser].bookingCount = nil;
    [IMGUser currentUser].redeemablePoints = nil;
    [IMGUser currentUser].reviewCount = nil;
    [IMGUser currentUser].islogin = nil;
    [IMGUser currentUser].countryName = nil;
    [IMGUser currentUser].isNewUser = nil;
    [IMGUser currentUser].qravedCount = nil;
    
    
}
-(void)configByQraveJsonObject:(id)jsonObject
{
    self.avatar=[jsonObject objectForKey:@"avatar"];
    self.fullName=[jsonObject objectForKey:@"fullName"];
    self.userId=[jsonObject objectForKey:@"id"];
}

+(BOOL )checkIsSelf:(NSNumber *)userID
{
    IMGUser *user = [self currentUser];
    if([[NSString stringWithFormat:@"%@",userID] isEqualToString:[NSString stringWithFormat:@"%@",user.userId]])
    {
        return YES;
    }
    
    return NO;
}
-(void)encodeWithCoder:(NSCoder *)aCoder

{
    [aCoder encodeObject:self.userId forKey:@"userId"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.email forKey:@"email"];
    [aCoder encodeObject:self.firstName forKey:@"firstName"];
    [aCoder encodeObject:self.lastName forKey:@"lastName"];
    [aCoder encodeObject:self.fullName forKey:@"fullName"];
    [aCoder encodeObject:self.userName forKey:@"userName"];
    [aCoder encodeObject:self.avatar forKey:@"avatar"];
    [aCoder encodeObject:self.redeemablePoints forKey:@"redeemablePoints"];
    [aCoder encodeObject:self.bookingCount forKey:@"bookingCount"];
    [aCoder encodeObject:self.reviewCount forKey:@"reviewCount"];
    [aCoder encodeObject:self.token forKey:@"token"];
    [aCoder encodeObject:self.birthday forKey:@"birthday"];
    [aCoder encodeObject:self.phone forKey:@"phone"];
    [aCoder encodeObject:self.website forKey:@"website"];
    [aCoder encodeObject:self.gender forKey:@"gender"];
    [aCoder encodeObject:self.cityName forKey:@"cityName"];
    [aCoder encodeObject:self.districtName forKey:@"districtName"];
    [aCoder encodeObject:self.redeemablePoints forKey:@"redeemablePoints"];
    [aCoder encodeObject:self.facebookAcount forKey:@"facebookAcount"];
    [aCoder encodeObject:self.twitterAcount forKey:@"twitterAcount"];
    
    
    
}



-(id)initWithCoder:(NSCoder *)aDecoder

{
    
    self = [super init];
    
    if(self)
        
    {
        self.userId=[aDecoder decodeObjectForKey:@"userId"];
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.email = [aDecoder decodeObjectForKey:@"email"];
        self.firstName = [aDecoder decodeObjectForKey:@"firstName"];
        self.lastName = [aDecoder decodeObjectForKey:@"lastName"];
        self.fullName = [aDecoder decodeObjectForKey:@"fullName"];
        self.userName = [aDecoder decodeObjectForKey:@"userName"];
        self.avatar = [aDecoder decodeObjectForKey:@"avatar"];
        self.redeemablePoints = [aDecoder decodeObjectForKey:@"redeemablePoints"];
        self.bookingCount = [aDecoder decodeObjectForKey:@"bookingCount"];
        self.reviewCount = [aDecoder decodeObjectForKey:@"reviewCount"];
        self.token = [aDecoder decodeObjectForKey:@"token"];
        self.birthday = [aDecoder decodeObjectForKey:@"birthday"];
        self.phone = [aDecoder decodeObjectForKey:@"phone"];
        self.website = [aDecoder decodeObjectForKey:@"website"];
        self.gender = [aDecoder decodeObjectForKey:@"gender"];
        self.cityName = [aDecoder decodeObjectForKey:@"cityName"];
        self.districtName = [aDecoder decodeObjectForKey:@"districtName"];
        self.redeemablePoints = [aDecoder decodeObjectForKey:@"redeemablePoints"];
        self.facebookAcount = [aDecoder decodeObjectForKey:@"facebookAcount"];
        self.twitterAcount = [aDecoder decodeObjectForKey:@"twitterAcount"];

    }
    
    return self;
  
}


@end
