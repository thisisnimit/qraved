//
//  IMGReviewComment.h
//  Qraved
//
//  Created by Jeff on 3/9/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "IMGComment.h"

@interface IMGReviewComment : IMGComment

@property(nonatomic,retain) NSString *content;
@property(nonatomic,retain) NSString *createdDate;
@property(nonatomic,retain) NSNumber *createTime;
@property(nonatomic,retain) NSString *timeCreatedStr;
@property(nonatomic,retain) NSNumber *reviewId;
@property(nonatomic,assign) BOOL readMore;
@property(nonatomic,assign) BOOL isLiked;


-(id)initFromDictionary:(NSDictionary*)commentDictionary;
+(NSMutableArray*)commentListFromArray:(NSArray*)commentArray andIsVendor:(BOOL)isVendor andRestaurant:(IMGRestaurant *)restaurant;
@end
