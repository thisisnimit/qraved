//
//  IMGUploadPhoto.h
//  Qraved
//
//  Created by Laura on 15/2/9.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGUploadPhoto : IMGEntity

@property (nonatomic,retain)    UIImage*image;
@property (nonatomic,copy)      NSString *title;
@property (nonatomic,copy)      NSString *descriptionStr;
@property (nonatomic)           NSInteger tag;
@property(nonatomic,copy) NSString* photoUrl;
@property (nonatomic)           BOOL isOldPhoto;
@property (nonatomic)           NSNumber *dishId;
@property (nonatomic,copy)           NSString *imageUrl;
@property (nonatomic)           BOOL isFromCamera;
@property (nonatomic,copy) NSString *latitude;
@property (nonatomic,copy) NSString *longitude;
@end
