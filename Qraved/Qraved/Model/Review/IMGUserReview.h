//
//  IMGUserReview.h
//  Qraved
//
//  Created by Lucky on 15/10/22.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"

@interface IMGUserReview : IMGEntity

@property(nonatomic,retain) NSNumber *reviewId;
@property(nonatomic,retain) NSNumber *restaurantId;
@property(nonatomic,retain) NSNumber *reviewScore;
@property(nonatomic,retain) NSNumber *userId;
@property(nonatomic,retain) NSNumber *restaurantPriceLevel;
@property(nonatomic,copy)   NSNumber *reviewTimeCreated;
@property(nonatomic,copy)   NSString *reviewTitle;
@property(nonatomic,copy)   NSString *reviewSummarize;
@property(nonatomic,copy)   NSString *restaurantTitle;
@property(nonatomic,copy)   NSString *restaurantCuisine;
@property(nonatomic,copy)   NSString *restaurantDistrict;
@property(nonatomic,copy)   NSString *restaurantSeoKeywords;
@property(nonatomic,copy)   NSString *restaurantCityName;
@property(nonatomic,copy)   NSString *userFullName;
@property(nonatomic,copy)   NSString *userAvatar;

@property(nonatomic)        NSMutableArray *dishListArrM;


@end
