//
//  IMGReviewComment.m
//  Qraved
//
//  Created by Jeff on 3/9/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "IMGReviewComment.h"

@implementation IMGReviewComment

-(id)initFromDictionary:(NSDictionary*)commentDictionary
{
    self = [super init];
    if(self){
        NSString *commentIdStr = [commentDictionary objectForKey:@"id"];
        if(commentIdStr){
            self.commentId=[NSNumber numberWithInteger:[commentIdStr integerValue]];
            self.content=[commentDictionary objectForKey:@"comment"];
            NSString *createTime = [commentDictionary objectForKey:@"timeCreated"];
            if(createTime){
                self.createTime=[NSNumber numberWithLongLong:[createTime longLongValue]];
            }
            self.timeCreatedStr = [commentDictionary objectForKey:@"timeCreatedStr"];
            NSString *commentUserId =[commentDictionary objectForKey:@"userId"];
            if(commentUserId){
                self.userId=[NSNumber numberWithInteger:[commentUserId integerValue]];
            }
            self.userImageUrl=[commentDictionary objectForKey:@"avatar"];
            self.userName=[commentDictionary objectForKey:@"fullName"];
            self.reviewId = [commentDictionary objectForKey:@"reviewId"];
        }
    }
    return self;

}


+(NSMutableArray*)commentListFromArray:(NSArray*)commentArray andIsVendor:(BOOL)isVendor andRestaurant:(IMGRestaurant *)restaurant
{
    NSMutableArray* commentList = [[NSMutableArray alloc]initWithCapacity:0];
    for(int i=0;commentArray!=nil && [commentArray isKindOfClass:[NSArray class]] && i<commentArray.count;i++){
        if([[commentArray objectAtIndex:i] isKindOfClass:[IMGReviewComment class]]){
            [commentList addObject:[commentArray objectAtIndex:i]];
        }else{
            NSDictionary *commentDictionary = [commentArray objectAtIndex:i];
            IMGReviewComment *reviewComment = [[IMGReviewComment alloc]init];
            NSString *commentIdStr = [commentDictionary objectForKey:@"id"];
            if(commentIdStr){
                reviewComment.commentId=[NSNumber numberWithInteger:[commentIdStr integerValue]];
                reviewComment.content=[commentDictionary objectForKey:@"comment"];
                NSString *createTime = [commentDictionary objectForKey:@"timeCreated"];
                if(createTime){
                    reviewComment.createTime=[NSNumber numberWithLongLong:[createTime longLongValue]];
                }
                reviewComment.timeCreatedStr = [commentDictionary objectForKey:@"timeCreatedStr"];
                reviewComment.restaurantId = restaurant.restaurantId;
                reviewComment.restaurantName = restaurant.title;
                reviewComment.restaurantImageUrl = restaurant.imageUrl;
                reviewComment.userType = [commentDictionary objectForKey:@"userType"];
                reviewComment.isVendor = isVendor;

                NSString *commentUserId =[commentDictionary objectForKey:@"userId"];
                if(commentUserId){
                    reviewComment.userId=[NSNumber numberWithInteger:[commentUserId integerValue]];
                }
                reviewComment.userImageUrl=[commentDictionary objectForKey:@"avatar"];
                reviewComment.userName=[commentDictionary objectForKey:@"fullName"];
                reviewComment.reviewId = [commentDictionary objectForKey:@"reviewId"];
                [commentList addObject:reviewComment];
            }
        }
      
    }
    return commentList;
}
@end
