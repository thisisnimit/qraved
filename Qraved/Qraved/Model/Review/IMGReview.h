//
//  IMGReview.h
//

//
//  Created by Olaf on 14/9/26.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGEntity.h"
@interface IMGReview : IMGEntity


@property(nonatomic,retain) NSNumber *reviewId;
@property(nonatomic,retain) NSNumber *restaurantId;
@property(nonatomic,retain) NSNumber *journalId;
@property(nonatomic,copy)   NSString *reviewDescriptionStr;
@property(nonatomic,copy)   NSString *restaurantTitle;

@property(nonatomic,retain) NSNumber *score;
@property(nonatomic,retain) NSNumber *ambianceScore;
@property(nonatomic,retain) NSNumber *foodScore;
@property(nonatomic,retain) NSNumber *serviceScore;

@property(nonatomic,retain) NSNumber *commentCount;
@property(nonatomic,copy)   NSString *createTimeStr;
@property(nonatomic,retain) NSNumber *followingUser;
@property(nonatomic,retain) NSNumber *helpful;
@property(nonatomic,retain) NSNumber *helpfulCount;

//@property(nonatomic,copy)   NSString *mini1Restaurant;
@property(nonatomic,retain) NSNumber *qravesCount;
@property(nonatomic,retain) NSNumber *reviewCount;
@property(nonatomic,copy)   NSString *sourceSite;
@property(nonatomic,copy)   NSString *summarize;
@property(nonatomic,retain) NSNumber *targetId;
@property(nonatomic,copy)   NSString *targetTitle;
@property(nonatomic,retain) NSNumber *timeCreated;
@property(nonatomic,retain) NSNumber *userDishCount;
@property(nonatomic,retain) NSNumber *userFollowerCount;
@property(nonatomic,retain) NSNumber *userReviewCount;
@property(nonatomic,copy)   NSString *userSeoKeyword;

@property(nonatomic,copy)   NSString *fullName;
@property(nonatomic,copy)   NSString *avatar;
@property(nonatomic,retain) NSString *userId;

@property(nonatomic,retain) NSNumber *likeCount;
@property(nonatomic,assign) BOOL isLike;

@property(nonatomic,retain) NSMutableArray *commentList;
@property(nonatomic,copy)   NSString *timeline;
@property(nonatomic,strong) NSNumber *imageHeight;
@property(nonatomic,strong) NSNumber *siblingCountByUser;
@property(nonatomic,copy)   NSString *caption;
@property(nonatomic,strong) NSNumber *reviewType;
@property(nonatomic,copy)   NSString *userName;
@property(nonatomic,copy)   NSString *userPicture;
@property (nonatomic, strong) NSArray *imageArray;
@property (nonatomic, assign) BOOL isReadMore;
@property(nonatomic,copy)   NSString *link;
//dishList
//commentList
@end
