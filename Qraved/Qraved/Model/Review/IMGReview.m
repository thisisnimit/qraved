//
//  IMGReview.m
//  Qraved
//
//  Created by Olaf on 14/9/26.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "IMGReview.h"

@implementation IMGReview
-(void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    if ([key isEqualToString:@"user"]) {
        NSDictionary *dict = value;
        self.fullName = [dict objectForKey:@"fullName"];
        self.avatar = [dict objectForKey:@"avatar"];
        self.userId = [dict objectForKey:@"id"];
    }
    
    if ([key isEqualToString:@"id"]) {
        
        self.reviewId = value;
    }
}
-(NSString*)fullName{
    return [_fullName filterHtml];
}
@end
