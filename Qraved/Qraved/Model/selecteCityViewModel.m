//
//  selecteCityViewModel.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "selecteCityViewModel.h"

@implementation selecteCityViewModel

/* 数组中存储模型数据，需要说明数组中存储的模型数据类型 */
+(NSDictionary *)mj_objectClassInArray
{
    return @{@"districts" : [selecteCityCenterViewModel class]};
}
+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    
    return @{@"myID":@"id"};
    
}
@end
