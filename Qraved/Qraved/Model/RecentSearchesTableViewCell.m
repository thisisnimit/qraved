//
//  RecentSearchesTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/18.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "RecentSearchesTableViewCell.h"
#import "IMGRestaurant.h"
@implementation RecentSearchesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUIWithModel:(RecentSearchesSuperModel *)model{

    _Supermodel = model;
    
    for (UIView *view in self.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(58, 105);
    layout.minimumLineSpacing = 15;
    layout.minimumInteritemSpacing = 1;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    UICollectionView* photoCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(15,0, DeviceWidth-15, 105) collectionViewLayout:layout];
    photoCollectionView.backgroundColor = [UIColor whiteColor];
    photoCollectionView.delegate = self;
    photoCollectionView.dataSource = self;
    photoCollectionView.showsHorizontalScrollIndicator = NO;
    [self.contentView addSubview:photoCollectionView];
    
    UIView *spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, photoCollectionView.endPointY, DeviceWidth, 5)];
    spaceView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    [self.contentView addSubview:spaceView];
    
    [photoCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"photoCell"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return _Supermodel.history.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    RecentSearchesTableViewCellModel *model = [_Supermodel.history objectAtIndex:indexPath.row];
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 58, 58)];
    imageView.backgroundColor = [UIColor whiteColor];
    imageView.layer.cornerRadius = imageView.height / 2;
    imageView.layer.masksToBounds = YES;
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    
    
    if ([model.type isEqualToString:@"foodtype"]) {
        imageView.image = [UIImage imageNamed:@"ic_foodtype"];
    }else{
        if ([model.img hasPrefix:@"http"]) {
            [imageView sd_setImageWithURL:[NSURL URLWithString:model.img] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
        }else{
            [imageView sd_setImageWithURL:[NSURL URLWithString:[model.img returnFullImageUrl]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
        }
    }
    
    [cell.contentView addSubview:imageView];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, imageView.bottom +5, 58, 20)];
    lblTitle.backgroundColor = [UIColor whiteColor];
    lblTitle.text = model.title ? model.title : @"";
    lblTitle.textColor = [UIColor color333333];
    lblTitle.font = [UIFont systemFontOfSize:12];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    
    [cell.contentView addSubview:lblTitle];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    NSLog(@"%ld", (long)indexPath.row);
    RecentSearchesTableViewCellModel *model = [_Supermodel.history objectAtIndex:indexPath.row];
    
    NSLog(@"%@",model.myID);
    if (self.delegate && [self.delegate respondsToSelector:@selector(recentSearchTapped:)]) {
        [self.delegate recentSearchTapped:model];
    }
}

@end
