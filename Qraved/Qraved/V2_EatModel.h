//
//  V2_EatModel.h
//  Qraved
//
//  Created by harry on 2017/6/16.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface V2_EatModel : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *objectId;
@property (nonatomic, copy) NSNumber *objectType;
@property (nonatomic, copy) NSString *state;
@end
