//
//  ProfileHandler.m
//  Qraved
//
//  Created by Gary.yao on 2017/6/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "ProfileHandler.h"
#import <MJExtension/MJExtension.h>
#import "IMGGuideModel.h"
#import "PersonalSummaryTopReviewModel.h"
#import "IMGProfileJourney.h"

@implementation ProfileHandler


+(void)getsummary:(NSDictionary*)dic andSuccessBlock:(void(^)(IMGSummary * summary))successBlock anderrorBlock:(void(^)())errorBlock{

    [[IMGNetWork sharedManager] GET:@"user/summary" parameters:dic progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {

        IMGSummary * summary = [IMGSummary mj_objectWithKeyValues:responseObject];
        summary.topPhotoList = [IMGPhotoModel mj_objectArrayWithKeyValuesArray:summary.topPhotoList];
        NSMutableArray *couponArr = [NSMutableArray array];
        for (NSDictionary *dic in [responseObject objectForKey:@"couponList"]) {
            CouponModel *model = [[CouponModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [couponArr addObject:model];
        }
        summary.couponList = couponArr;
        NSMutableArray *reviewArr = [NSMutableArray array];
        for (NSDictionary *dic in [responseObject objectForKey:@"topReviews"]) {
           PersonalSummaryTopReviewModel *model =  [PersonalSummaryTopReviewModel mj_objectWithKeyValues:dic];
            [reviewArr addObject:model];
        }
        summary.topReviews = reviewArr;
        
        successBlock(summary);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        errorBlock();
    }];
}

+(void)getUpCommingBooking:(NSDictionary*)dic andSuccessBlock:(void (^)(NSMutableArray *))successBlock anderrorBlock:(void (^)())errorBlock{

    [[IMGNetWork sharedManager] GET:@"app/user/reservation/upcoming" parameters:dic progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSMutableArray * reservationArr = [NSMutableArray arrayWithCapacity:0];
        for (NSDictionary * dict in responseObject[@"userReservationList"]) {
            IMGReservation * reservation = [IMGReservation mj_objectWithKeyValues:dict];
            reservation.reservationId = [dict objectForKey:@"id"];
            reservation.phone = [dict objectForKey:@"restaurantPhone"];
            reservation.restaurantName = [dict objectForKey:@"restaurantTitle"];
            [reservationArr addObject:reservation];
            
        }
        successBlock(reservationArr);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        errorBlock();
    }];
}

+(void)getPhotos:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array))successBlock anderrorBlock:(void(^)())errorBlock{

    [[IMGNetWork sharedManager] GET:@"user/photos" parameters:dic progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSMutableArray * reservationArr = [NSMutableArray arrayWithCapacity:0];
        for (NSDictionary * dict in responseObject[@"photoList"]) {
            IMGPhotoModel * photo = [IMGPhotoModel mj_objectWithKeyValues:dict];
            [reservationArr addObject:photo];
            
        }
        successBlock(reservationArr);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        errorBlock();
    }];
}

+(void)getSelfProfile:(NSDictionary*)dic andSuccessBlock:(void(^)(IMGProfileModel * model))successBlock anderrorBlock:(void(^)())errorBlock{
    [[IMGNetWork sharedManager] GET:@"user/profile/v2/self" parameters:dic progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        IMGProfileModel * model = [IMGProfileModel mj_objectWithKeyValues:responseObject];
        successBlock(model);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        errorBlock();
    }];
    
}

+(void)getEditProfile:(NSDictionary*)dic andSuccessBlock:(void(^)(IMGProfileModel * model))successBlock anderrorBlock:(void(^)())errorBlock{

    [[IMGNetWork sharedManager] GET:@"user/profile/v2/other" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        IMGProfileModel * model = [IMGProfileModel mj_objectWithKeyValues:responseObject];
        successBlock(model);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        errorBlock();
    }];


}
+(void)changePassword:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableDictionary *twoDict))successBlock anderrorBlock:(void(^)())errorBlock{

    [[IMGNetWork sharedManager] GET:@"user/changePassword" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        successBlock(responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
         errorBlock();
    }];

}

+(void)getOtherProfile:(NSDictionary*)dic andSuccessBlock:(void(^)(IMGProfileModel * model))successBlock anderrorBlock:(void(^)())errorBlock{

    [[IMGNetWork sharedManager] GET:@"user/profile/v2/other" parameters:dic progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        IMGProfileModel * model = [IMGProfileModel mj_objectWithKeyValues:responseObject];
        successBlock(model);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        errorBlock();
    }];
    
}

+(void)getOtherList:(NSDictionary*)dic andSuccessBlock:(void(^)(IMGListModel * model))successBlock anderrorBlock:(void(^)())errorBlock{

    [[IMGNetWork sharedManager] GET:@"lists/v2/other" parameters:dic progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        IMGListModel * model = [IMGListModel mj_objectWithKeyValues:responseObject];
        model.guideList = [IMGGuideModel mj_objectArrayWithKeyValuesArray:model.guideList];
        successBlock(model);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        errorBlock();
    }];
}

+(void)getJourney:(NSDictionary *)dic andSuccessBlock:(void(^)(NSMutableArray * journeyArray))successBlock anderrorBlock:(void(^)())errorBlock{
    [[IMGNetWork sharedManager] GET:@"userhistory/list/v3" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSMutableArray *journeyArr = [NSMutableArray array];
        for (NSDictionary *dic in [responseObject objectForKey:@"userHistoryList"]) {
            IMGProfileJourney *profileJourney = [[IMGProfileJourney alloc] init];
            profileJourney.type = [dic objectForKey:@"type"];
            
            NSString *type = [dic objectForKey:@"type"];
            if ([type isEqualToString:@"1"]) {
                IMGJourneyReview *review = [IMGJourneyReview mj_objectWithKeyValues:[dic objectForKey:@"Review"]];
                profileJourney.journeyReview = review;
            }else if ([type isEqualToString:@"2"]){
                IMGJourneyBook *book = [IMGJourneyBook mj_objectWithKeyValues:[dic objectForKey:@"Reservation"]];
                profileJourney.journeyBook = book;
            }else if ([type isEqualToString:@"3"]){
                IMGJourneyPhoto *uploadPhoto = [IMGJourneyPhoto mj_objectWithKeyValues:[dic objectForKey:@"UploadedPhoto"]];
                profileJourney.journeyPhoto = uploadPhoto;
            }else if ([type isEqualToString:@"4"]){
                IMGJourneySave *save = [IMGJourneySave mj_objectWithKeyValues:[dic objectForKey:@"SaveRestaurant"]];
                profileJourney.journeySave = save;
            }else if ([type isEqualToString:@"5"]){
                CouponModel *couponModel = [[CouponModel alloc] init];
                [couponModel setValuesForKeysWithDictionary:[dic objectForKey:@"Coupon"]];
                profileJourney.journeyCoupon = couponModel;
            }
            
            [journeyArr addObject:profileJourney];
        }
        
        successBlock(journeyArr);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock();
        
    }];

}

+(void)getOtherJourney:(NSDictionary *)dic andSuccessBlock:(void(^)(NSMutableArray * journeyArray))successBlock anderrorBlock:(void(^)())errorBlock{

    [[IMGNetWork sharedManager] GET:@"otherhistory/list/v2" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSMutableArray *journeyArr = [NSMutableArray array];
        for (NSDictionary *dic in [responseObject objectForKey:@"userHistoryList"]) {
            IMGProfileJourney *profileJourney = [[IMGProfileJourney alloc] init];
            profileJourney.type = [dic objectForKey:@"type"];
            
            NSString *type = [dic objectForKey:@"type"];
            if ([type isEqualToString:@"1"]) {
                IMGJourneyReview *review = [IMGJourneyReview mj_objectWithKeyValues:[dic objectForKey:@"Review"]];
                profileJourney.journeyReview = review;
            }else if ([type isEqualToString:@"2"]){
                IMGJourneyBook *book = [IMGJourneyBook mj_objectWithKeyValues:[dic objectForKey:@"Reservation"]];
                profileJourney.journeyBook = book;
            }else if ([type isEqualToString:@"3"]){
                IMGJourneyPhoto *uploadPhoto = [IMGJourneyPhoto mj_objectWithKeyValues:[dic objectForKey:@"UploadedPhoto"]];
                profileJourney.journeyPhoto = uploadPhoto;
            }else if ([type isEqualToString:@"4"]){
                IMGJourneySave *save = [IMGJourneySave mj_objectWithKeyValues:[dic objectForKey:@"SaveRestaurant"]];
                profileJourney.journeySave = save;
            }
            else if ([type isEqualToString:@"5"]){
                CouponModel *couponModel = [[CouponModel alloc] init];
                [couponModel setValuesForKeysWithDictionary:[dic objectForKey:@"Coupon"]];
                profileJourney.journeyCoupon = couponModel;
            }
            
            [journeyArr addObject:profileJourney];
        }
        
        successBlock(journeyArr);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock();
        
    }];

}


@end
