//
//  V2_InstagramModel.h
//  Qraved
//
//  Created by harry on 2017/6/15.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface V2_InstagramModel : NSObject

@property (nonatomic, copy) NSString *caption;
@property (nonatomic, copy) NSNumber *city_id;
@property (nonatomic, copy) NSString *city_name;
@property (nonatomic, copy) NSNumber *comment_count;
@property (nonatomic, copy) NSNumber *district_id;
@property (nonatomic, copy) NSString *instagram_create_date;
@property (nonatomic, copy) NSString *instagram_link;
@property (nonatomic, copy) NSNumber *instagram_media_id;
@property (nonatomic, copy) NSString *instagram_user_name;
@property (nonatomic, copy) NSString *instagram_user_profile_picture;
@property (nonatomic, copy) NSNumber *like_count;
@property (nonatomic, copy) NSString *low_resolution_image;
@property (nonatomic, copy) NSNumber *restaurant_id;
@property (nonatomic, copy) NSString *restaurant_name;
@property (nonatomic, copy) NSString *seo_keyword;
@property (nonatomic, copy) NSString *standard_resolution_image;
@property (nonatomic, copy) NSString *thumbnail_image;


@end
