//
//  IMGBasicViewController.m
//  Qraved
//
//  Created by Libo.Liu on 13-9-25.
//  Copyright (c) 2013年 Imaginato. All rights reserved.
//

#import "IMGBasicViewController.h"

@interface IMGBasicViewController ()
//@property(nonatomic,retain)UIView *mView;
@end

@implementation IMGBasicViewController
-(id)init{
    self = [super init];
    if (self) {
        
    }
    return self;
}
//-(UIView*)mainView{
//    if (!self.mView) {
////        if([[[UIDevice currentDevice] systemVersion] floatValue]>=7.0){
////            self.mView = [[UIView alloc]initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height-20)];
////        }else{
////            self.mView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
////        }
//        self.mView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
//    }
//    return self.mView;
//}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if( floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        self.edgesForExtendedLayout= UIRectEdgeNone;
        self.navigationController.navigationBar.translucent = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
