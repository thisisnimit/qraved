//
//  DistrictCell.m
//  Qraved
//
//  Created by Tek Yin on 5/4/17.
//  Copyright © 2017 Imaginato. All rights reserved.
//

#import "DistrictCell.h"

@implementation DistrictCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 10;
}

@end
