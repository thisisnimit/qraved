//
//  AppDelegate.h
//  Qraved
//
//  Created by Shine Wang on 5/30/13.
//  Copyright (c) 2013 ShineWang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "LoginViewController.h"
#import "NewFeatureViewController.h"

#import <EventKit/EventKit.h>
#import "UIPopoverListView.h"
#import <MessageUI/MessageUI.h>
#import "SavedViewController.h"
#import "HomeViewController.h"
//#import "QrarvedIncrementalStore.h"
#import "ProfileViewController.h"
#import "MLNavigationController.h"
#import "PointRewardsViewController.h"
#import "SettingViewController.h"
#import "DiscoverListViewController.h"
#import "Helper.h"
#import "RESideMenu.h"
#import "RDVTabBarController.h"
#import "RDVTabBarItem.h"
#import "HomeSlideViewController.h"
#import "UserNotifications/UserNotifications.h"
#import <Google/SignIn.h>
#import "V2_HomeViewController.h"
#import "ContributionController.h"
#import "ReviewPublishViewController.h"
#import "FeedViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,RESideMenuDelegate,RDVTabBarControllerDelegate,UNUserNotificationCenterDelegate>

@property(nonatomic, strong) id<GAITracker> tracker;
@property(nonatomic, retain) CLLocationManager *locationManager;
@property(nonatomic, retain) CLLocationManager *lastLocationManager;

@property (strong, nonatomic) UIWindow *window;

@property(nonatomic,retain)RDVTabBarController *tabMenuViewController;
@property(nonatomic,retain)CLLocation *currentLocation;

@property(nonatomic,assign)BOOL userLogined;
@property(nonatomic,assign)BOOL qraveRefresh;
@property (nonatomic, assign) BOOL isSettingNotification;
//@property(nonatomic,assign)BOOL isShare;
//@property(nonatomic,assign)BOOL isUpload;
@property(nonatomic,retain)NSMutableDictionary *uploadPictureDic;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
//@property (readonly, strong, nonatomic) AFIncrementalStore *incrementalStore ;

@property (assign) int touchedIndex;

@property(nonatomic,assign) NSInteger selectedPrice;

@property (nonatomic, strong) V2_HomeViewController *homeViewController;
@property (nonatomic, strong) HomeSlideViewController *homeSlideViewController;
@property (nonatomic, strong) SavedViewController *savedViewController;
@property (nonatomic, strong) DiscoverListViewController *discoverListViewController;
@property (nonatomic, strong) UIViewController *hotKeyViewController;
@property (nonatomic, strong) NotificationsViewController *notificationsViewController;
@property (nonatomic, strong) ContributionController *contributionController;
@property (nonatomic, strong) MLNavigationController *contributionNavigationController;
@property (nonatomic, strong) ProfileViewController *myProfileViewController;
@property (nonatomic, strong) FeedViewController *feedViewcontroller;
@property (nonatomic, strong) ReviewPublishViewController  *ReviewPublishViewController;
@property (nonatomic, strong) MLNavigationController *ReviewPublishNavigationController;

@property (nonatomic, strong) PointRewardsViewController *pointRewardViewController;

@property (nonatomic, strong) SettingViewController *settingViewController;

@property (nonatomic, strong) MLNavigationController *homeNavigationController;
@property (nonatomic, strong) MLNavigationController *homeSlideNavigationController;
@property (nonatomic, strong) MLNavigationController *savedNavigationController;
@property (nonatomic, strong) MLNavigationController *feedNavifationController;

@property (nonatomic, strong) MLNavigationController *discoverListNavigationController;
@property (nonatomic, strong) MLNavigationController *hotKeyNavigationController;
@property (nonatomic, strong) MLNavigationController *notificationsNavigationController;
@property (nonatomic, strong) MLNavigationController *ProfileNavigationController;

@property (nonatomic, strong) MLNavigationController *qraveNavigationController;
@property (nonatomic, strong) MLNavigationController *searchNavigationController;
@property (nonatomic, strong) UINavigationController *filterNavigationController;
@property (nonatomic, strong) MLNavigationController *offersNavigationController;
@property (nonatomic, strong) MLNavigationController *diningGuideNavigationController;
@property (nonatomic, strong) MLNavigationController *nearByNavigationController;
@property (nonatomic, strong) MLNavigationController *qravedJournalNavigationController;
@property (nonatomic, strong) UINavigationController *loginNavagationController;
@property (nonatomic, strong) MLNavigationController *pointRewardsNavigationController;
@property (nonatomic, strong) MLNavigationController *settingNavigationController;
@property(nonatomic,retain)NSTimer* timer;
@property(nonatomic,retain)MLNavigationController *selectedNavigationController;
//判断是否登录

@property(nonatomic,retain) NSMutableArray * eatjktData;
@property(nonatomic,retain)NSMutableArray *splashsArray;
//@property(nonatomic,retain)NSString *kTrackingId;

@property(nonatomic) float autoSizeScaleX;
@property(nonatomic) float autoSizeScaleY;

@property(nonatomic,assign)BOOL isAlreadyPopLogin;
@property(nonatomic,retain)NSTimer* popLoginNSTimer;

@property(nonatomic,assign)BOOL isAlreadySetAmplitudeUserProperties;

@property(nonatomic,assign)BOOL isSlideHomeHidden;
@property(nonatomic,assign)BOOL isSlideJournalHidden;
@property(nonatomic,assign)BOOL isSlideDiningGuideHidden;
@property(nonatomic,assign)BOOL isShowPersonalization;

@property (nonatomic, strong)  UIImage  *backGroundImage;
@property (nonatomic, strong)  UIImage  *iconImage;

- (NSURL *)applicationDocumentsDirectory;

-(void)goToPage:(NSInteger)tabIndex;
-(void)goToPage:(NSInteger)tabIndex  withSearchTag:(NSDictionary*)searchTag withEventLogo:(NSString*)eventLogo;
-(void)goToLoginController:(NSNumber *)fromWhere;
-(void)goToLoginController;
-(void)goToLoginControllerWithindex:(int)index;
-(void)goToLoginControllerByDelegate:(id)autoPostDelegate dictionary:(NSDictionary *)autoPostDictionary;
-(void)requestToShowNotificationCount;
-(void)clearNotificationCount;

- (void)gotoHomePage;
- (void)deepLinkhandle;
- (void)referrerDeeplink;

-(BOOL)isShowEvent;
- (UIViewController *)getCurrentVC;
// Layer
-(void)loadLayerButton;
//-(void)HideLayerWindow;


+(AppDelegate *)ShareApp;
-(void)addPopLoginNSTimer;
-(void)removeEcodingFile:(NSString*)path;
@end
