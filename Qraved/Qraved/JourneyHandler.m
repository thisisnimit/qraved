//
//  JourneyHandler.m
//  Qraved
//
//  Created by lucky on 16/9/9.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "JourneyHandler.h"
#import "IMGJNReview.h"
#import "IMGJNUploadPhoto.h"
#import "IMGJNReservation.h"
#import "IMGDish.h"
#import "IMGUser.h"
@implementation JourneyHandler

+(void)getJourneyList:(NSDictionary *)params andBlock:(void (^)(NSArray *journeyDataArr))block{

    [[IMGNetWork sharedManager]GET:@"userhistory/list" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         
         NSArray *userHistoryList = [responseObject objectForKey:@"userHistoryList"];
         NSMutableArray *dataArrM = [[NSMutableArray alloc] init];
         for (NSDictionary *dic in userHistoryList)
         {
             switch ([[dic objectForKey:@"type"] intValue])
             {
                 case 1: case 4:
                 {
                     IMGJNReview *jnReview = [[IMGJNReview alloc] init];
                     [jnReview setValuesForKeysWithDictionary:[dic objectForKey:@"Review"]];
                     int userId= [[params objectForKey:@"userId"] intValue];
                     if ([[IMGUser currentUser].userId intValue]!=userId) {
                         jnReview.isother=YES;
                     }
                     jnReview.type = [dic objectForKey:@"type"];
                     NSArray *dishList = [[dic objectForKey:@"Review"] objectForKey:@"dishList"];
                     NSMutableArray *dishArrM = [[NSMutableArray alloc] init];
                     for (NSDictionary *dic in dishList)
                     {
                         IMGDish *dish = [[IMGDish alloc] init];
                         dish.dishId = [dic objectForKey:@"id"];
                         dish.imageUrl = [dic objectForKey:@"imageUrl"];
                         dish.title = [dic objectForKey:@"title"];
                         dish.descriptionStr = [dic objectForKey:@"description"];
                         dish.createTime = [dic objectForKey:@"createTimeStr"];
                         NSDictionary *photoCreditDic = [dic objectForKey:@"photoCredit"];
                         dish.userName = [photoCreditDic objectForKey:@"photoCredit"];
                         dish.photoCreditTypeDic = [photoCreditDic objectForKey:@"photoCreditType"];
                         dish.restaurantId = [photoCreditDic objectForKey:@"restaurantId"];
                         dish.userAvatarDic = [photoCreditDic objectForKey:@"userAvatar"];
                         dish.userId = [photoCreditDic objectForKey:@"userId"];
                         dish.userPhotoCountDic = [photoCreditDic objectForKey:@"userPhotoCount"];
                         dish.userReviewCountDic = [photoCreditDic objectForKey:@"userReviewCount"];
                         [dishArrM addObject:dish];
                     }
                     jnReview.dishArr = [NSArray arrayWithArray:dishArrM];
                     [dataArrM addObject:jnReview];
                 }
                     break;
                 case 2:
                 {
                     IMGJNReservation *reservation = [[IMGJNReservation alloc] init];
                     int userId= [[params objectForKey:@"userId"] intValue];
                     if ([[IMGUser currentUser].userId intValue]!=userId) {
                         reservation.isother=YES;
                     }
                     [reservation setValuesForKeysWithDictionary:[dic objectForKey:@"Reservation"]];
                     reservation.type = [dic objectForKey:@"type"];

                     NSString* timeStr = reservation.dateCreated;
                     NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                     [formatter setDateStyle:NSDateFormatterMediumStyle];
                     [formatter setTimeStyle:NSDateFormatterShortStyle];
                     [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"]; // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
                     NSTimeZone *zone = [NSTimeZone defaultTimeZone];//获得当前应用程序默认的时区
                     [formatter setTimeZone:zone];
                     NSDate* date = [formatter dateFromString:timeStr]; //------------将字符串按formatter转成nsdate
                     NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[date timeIntervalSince1970]];
                     reservation.dateCreated = [NSString stringWithFormat:@"%lld",[timeSp longLongValue]*1000];
                     [dataArrM addObject:reservation];
                 }
                     break;
                 case 3:
                 {
                     IMGJNUploadPhoto *jnUploadPhoto = [[IMGJNUploadPhoto alloc] init];
                     [jnUploadPhoto setValuesForKeysWithDictionary:[dic objectForKey:@"UploadedPhoto"]];
//                     IMGJNReservation *reservation = [[IMGJNReservation alloc] init];
                     int userId= [[params objectForKey:@"userId"] intValue];
                     if ([[IMGUser currentUser].userId intValue]!=userId) {
                         jnUploadPhoto.isother=YES;
                     }

                     jnUploadPhoto.type = [dic objectForKey:@"type"];
                     NSArray *dishList = [[dic objectForKey:@"UploadedPhoto"] objectForKey:@"dishList"];
                     NSMutableArray *dishArrM = [[NSMutableArray alloc] init];
                     for (NSDictionary *dic in dishList)
                     {
                         IMGDish *dish = [[IMGDish alloc] init];
                         dish.dishId = [dic objectForKey:@"id"];
                         dish.imageUrl = [dic objectForKey:@"imageUrl"];
                         dish.title = [dic objectForKey:@"title"];
                         dish.descriptionStr = [dic objectForKey:@"description"];
                         dish.createTime = [dic objectForKey:@"createTimeStr"];
                         NSDictionary *photoCreditDic = [dic objectForKey:@"photoCredit"];
                         dish.userName = [photoCreditDic objectForKey:@"photoCredit"];
                         dish.photoCreditTypeDic = [photoCreditDic objectForKey:@"photoCreditType"];
                         dish.restaurantId = [photoCreditDic objectForKey:@"restaurantId"];
                         dish.userAvatarDic = [photoCreditDic objectForKey:@"userAvatar"];
                         dish.userId = [photoCreditDic objectForKey:@"userId"];
                         dish.userPhotoCountDic = [photoCreditDic objectForKey:@"userPhotoCount"];
                         dish.userReviewCountDic = [photoCreditDic objectForKey:@"userReviewCount"];
                         [dishArrM addObject:dish];
                     }
                     jnUploadPhoto.dishArr = [NSArray arrayWithArray:dishArrM];
                     [dataArrM addObject:jnUploadPhoto];
                 }
                     break;

                 default:
                     break;
             }
         }
         block(dataArrM);

     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         NSLog(@"getJourneyList error: %@",error.localizedDescription);
     }];
    
}
+(void)getOtherJourneyList:(NSDictionary *)params andBlock:(void (^)(NSArray *journeyDataArr))block{
    
    [[IMGNetWork sharedManager]GET:@"otherhistory/list" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         
         NSArray *userHistoryList = [responseObject objectForKey:@"userHistoryList"];
         NSMutableArray *dataArrM = [[NSMutableArray alloc] init];
         for (NSDictionary *dic in userHistoryList)
         {
             switch ([[dic objectForKey:@"type"] intValue])
             {
                 case 1: case 4:
                 {
                     IMGJNReview *jnReview = [[IMGJNReview alloc] init];
                     if ([[dic objectForKey:@"Review"] isKindOfClass:[NSNull class]]) {
                         break;
                     }
                     [jnReview setValuesForKeysWithDictionary:[dic objectForKey:@"Review"]];
                     int userId= [[params objectForKey:@"userId"] intValue];
                     if ([[IMGUser currentUser].userId intValue]!=userId) {
                         jnReview.isother=YES;
                     }
                     jnReview.type = [dic objectForKey:@"type"];
                     NSArray *dishList = [[dic objectForKey:@"Review"] objectForKey:@"dishList"];
                     NSMutableArray *dishArrM = [[NSMutableArray alloc] init];
                     for (NSDictionary *dic in dishList)
                     {
                         IMGDish *dish = [[IMGDish alloc] init];
                         dish.dishId = [dic objectForKey:@"id"];
                         dish.imageUrl = [dic objectForKey:@"imageUrl"];
                         dish.title = [dic objectForKey:@"title"];
                         dish.descriptionStr = [dic objectForKey:@"description"];
                         dish.createTime = [dic objectForKey:@"createTimeStr"];
                         NSDictionary *photoCreditDic = [dic objectForKey:@"photoCredit"];
                         dish.userName = [photoCreditDic objectForKey:@"photoCredit"];
                         dish.photoCreditTypeDic = [photoCreditDic objectForKey:@"photoCreditType"];
                         dish.restaurantId = [photoCreditDic objectForKey:@"restaurantId"];
                         dish.userAvatarDic = [photoCreditDic objectForKey:@"userAvatar"];
                         dish.userId = [photoCreditDic objectForKey:@"userId"];
                         dish.userPhotoCountDic = [photoCreditDic objectForKey:@"userPhotoCount"];
                         dish.userReviewCountDic = [photoCreditDic objectForKey:@"userReviewCount"];
                         [dishArrM addObject:dish];
                     }
                     jnReview.dishArr = [NSArray arrayWithArray:dishArrM];
                     [dataArrM addObject:jnReview];
                 }
                     break;
                 case 2:
                 {
                     IMGJNReservation *reservation = [[IMGJNReservation alloc] init];
                     int userId= [[params objectForKey:@"userId"] intValue];
                     if ([[IMGUser currentUser].userId intValue]!=userId) {
                         reservation.isother=YES;
                     }
                     [reservation setValuesForKeysWithDictionary:[dic objectForKey:@"Reservation"]];
                     reservation.type = [dic objectForKey:@"type"];
                     
                     NSString* timeStr = reservation.dateCreated;
                     NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                     [formatter setDateStyle:NSDateFormatterMediumStyle];
                     [formatter setTimeStyle:NSDateFormatterShortStyle];
                     [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"]; // ----------设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
                     NSTimeZone *zone = [NSTimeZone defaultTimeZone];//获得当前应用程序默认的时区
                     [formatter setTimeZone:zone];
                     NSDate* date = [formatter dateFromString:timeStr]; //------------将字符串按formatter转成nsdate
                     NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[date timeIntervalSince1970]];
                     reservation.dateCreated = [NSString stringWithFormat:@"%lld",[timeSp longLongValue]*1000];
                     [dataArrM addObject:reservation];
                 }
                     break;
                 case 3:
                 {
                     IMGJNUploadPhoto *jnUploadPhoto = [[IMGJNUploadPhoto alloc] init];
                     if (![[dic objectForKey:@"UploadedPhoto"] isKindOfClass:[NSNull class]]) {
                         [jnUploadPhoto setValuesForKeysWithDictionary:[dic objectForKey:@"UploadedPhoto"]];

                     }
                     else{
                     
                         break ;
                     }
//                     IMGJNReservation *reservation = [[IMGJNReservation alloc] init];
                     int userId= [[params objectForKey:@"userId"] intValue];
                     if ([[IMGUser currentUser].userId intValue]!=userId) {
                         jnUploadPhoto.isother=YES;
                     }
                     
                     jnUploadPhoto.type = [dic objectForKey:@"type"];
                     NSArray *dishList = [[dic objectForKey:@"UploadedPhoto"] objectForKey:@"dishList"];
                     NSMutableArray *dishArrM = [[NSMutableArray alloc] init];
                     for (NSDictionary *dic in dishList)
                     {
                         IMGDish *dish = [[IMGDish alloc] init];
                         dish.dishId = [dic objectForKey:@"id"];
                         dish.imageUrl = [dic objectForKey:@"imageUrl"];
                         dish.title = [dic objectForKey:@"title"];
                         dish.descriptionStr = [dic objectForKey:@"description"];
                         dish.createTime = [dic objectForKey:@"createTimeStr"];
                         NSDictionary *photoCreditDic = [dic objectForKey:@"photoCredit"];
                         dish.userName = [photoCreditDic objectForKey:@"photoCredit"];
                         dish.photoCreditTypeDic = [photoCreditDic objectForKey:@"photoCreditType"];
                         dish.restaurantId = [photoCreditDic objectForKey:@"restaurantId"];
                         dish.userAvatarDic = [photoCreditDic objectForKey:@"userAvatar"];
                         dish.userId = [photoCreditDic objectForKey:@"userId"];
                         dish.userPhotoCountDic = [photoCreditDic objectForKey:@"userPhotoCount"];
                         dish.userReviewCountDic = [photoCreditDic objectForKey:@"userReviewCount"];
                         [dishArrM addObject:dish];
                     }
                     jnUploadPhoto.dishArr = [NSArray arrayWithArray:dishArrM];
                     [dataArrM addObject:jnUploadPhoto];
                 }
                     break;
                     
                 default:
                     break;
             }
         }
         block(dataArrM);
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         NSLog(@"getJourneyList error: %@",error.localizedDescription);
     }];
    
}

+(void)deletUploadPhoto:(NSDictionary*)params andBlock:(void (^)(NSDictionary*returnInfo))block{

    [[IMGNetWork sharedManager]GET:@"app/moderateReview/remove" parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
        block(responseObject);
        
        
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"app/moderateReview/remove error:%@ ",error);
    }];




}

@end
