//
//  V2_HomeJournalCell.m
//  Qraved
//
//  Created by harry on 2017/10/12.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_HomeJournalCell.h"
#import "UIColor+Hex.h"
#import "V2_ArticleCell.h"

@interface V2_HomeJournalCell()<UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource>{
    UICollectionView *articleCollectionView;
    UILabel *lblTitle;
}

@end

@implementation V2_HomeJournalCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    self.backgroundColor = [UIColor whiteColor];
    
    lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 17, DeviceWidth-90, 16)];
    lblTitle.textColor = [UIColor color333333];
    lblTitle.font = [UIFont boldSystemFontOfSize:14];
    [self addSubview:lblTitle];
    
    UIButton *btnChange = [UIButton buttonWithType:UIButtonTypeCustom];
    btnChange.frame = CGRectMake(15, 0, DeviceWidth-30, 50);
    [btnChange addTarget:self action:@selector(seeAllClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnChange];
    [btnChange setImage:[UIImage imageNamed:@"ic_home_arrow"] forState:UIControlStateNormal];
    btnChange.imageEdgeInsets = UIEdgeInsetsMake(0, DeviceWidth-30-10, 0, 0);
    
    UILabel *lblSeeAll = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth-95, 6, 50, 38)];
    lblSeeAll.text = @"See All";
    lblSeeAll.textAlignment = NSTextAlignmentRight;
    lblSeeAll.textColor = [UIColor color999999];
    lblSeeAll.font =[UIFont systemFontOfSize:12];
    [btnChange addSubview:lblSeeAll];
    
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(125, 158);
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 10;
    layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 0);
    
    articleCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,50, DeviceWidth, 158) collectionViewLayout:layout];
    articleCollectionView.backgroundColor = [UIColor whiteColor];
    articleCollectionView.delegate = self;
    articleCollectionView.dataSource = self;
    articleCollectionView.showsHorizontalScrollIndicator = NO;
    [self addSubview:articleCollectionView];
    
    [articleCollectionView registerNib:[UINib nibWithNibName:@"V2_ArticleCell" bundle:nil] forCellWithReuseIdentifier:@"articleCell"];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.model.docs.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    V2_ArticleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"articleCell" forIndexPath:indexPath];
    
    IMGMediaComment *model = [self.model.docs objectAtIndex:indexPath.row];
    cell.model = model;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    IMGMediaComment *journal = [self.model.docs objectAtIndex:indexPath.row];
//    if (self.gotoJournalDetail) {
//        self.gotoJournalDetail(journal);
//    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(gotoJournalDetail:andName:)]) {
        [self.delegate gotoJournalDetail:journal andName:self.model.asset];
    }
}

- (void)seeAllClick{
    if (self.delegate && [self.delegate respondsToSelector:@selector(seeAllJournal:)]) {
        [self.delegate seeAllJournal:self.model];
    }
}

- (void)setModel:(V2_HomeModel *)model{
    _model = model;
    lblTitle.text = model.title;

    [articleCollectionView reloadData];
}


@end
