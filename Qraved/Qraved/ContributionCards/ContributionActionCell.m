//
//  ContributionActionCell.m
//  Qraved
//
//  Created by Tek Yin on 5/23/17.
//  Copyright © 2017 Imaginato. All rights reserved.
//

#import "ContributionActionCell.h"
#import <Photos/Photos.h>
@implementation ContributionActionCell {
    __weak IBOutlet UILabel *uRestaurantName;

}




- (void)getUserNewImage{
    
//    if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusAuthorized)
//    {
//        PHFetchOptions *options = [[PHFetchOptions alloc] init];
//        PHFetchResult *assetsFetchResults = [PHAsset fetchAssetsWithOptions:options];
//        PHAsset *phasset = [assetsFetchResults lastObject];
//        PHCachingImageManager *imageManager = [[PHCachingImageManager alloc] init];
//        [imageManager requestImageForAsset:phasset targetSize:CGSizeMake(DeviceWidth-90, (DeviceWidth-90)*.55) contentMode:PHImageContentModeAspectFill options:nil resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
//            self.imgUser.image = result;
//
//        }];
//    }
}

- (IBAction)doCamera:(id)sender {

    if (self.takePhoto) {
        self.takePhoto();
    }
}

- (IBAction)doGallery:(id)sender {

    if (self.uploadPhoto) {
        self.uploadPhoto();
    }
}

- (IBAction)doSearch:(id)sender {

    if (self.searchRestaurant) {
        self.searchRestaurant();
    }
}

- (void)setUserImage:(UIImage *)userImage{
    _userImage = userImage;
    
    if (userImage==nil) {
       // [self getUserNewImage];
    }else{
        self.imgUser.image = userImage;
    }
}

@end
