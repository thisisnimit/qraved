//
//  ContributionActionCell.h
//  Qraved
//
//  Created by Tek Yin on 5/23/17.
//  Copyright © 2017 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ContributionActionCellDelegate <NSObject>

- (void)searchRestaurant;
- (void)uploadPhoto;
- (void)takePhoto;

@end

@interface ContributionActionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgUser;

@property(nonatomic, strong) UIImage *userImage;
@property(nonatomic, copy) void (^searchRestaurant)();
@property(nonatomic, copy) void (^uploadPhoto)();
@property(nonatomic, copy) void (^takePhoto)();
@end
