//
//  ContributionRestaurantCell.h
//  Qraved
//
//  Created by Tek Yin on 5/23/17.
//  Copyright © 2017 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QRStarComponent.h"

@class QRStarComponent;
@class ContributionListModel;
@class QRCheckmark;

@interface ContributionRestaurantCell : UICollectionViewCell <QRStarRatingDelegate>
@property (weak, nonatomic) IBOutlet UIView *uMainContainer;
@property(weak, nonatomic) IBOutlet UIImageView          *uImage;
@property(weak, nonatomic) IBOutlet QRStarComponent      *uStar;
@property(weak, nonatomic) IBOutlet NSLayoutConstraint   *cTopSpace;
@property(weak, nonatomic) IBOutlet UIView               *uExtendedContainer;
@property(weak, nonatomic) IBOutlet UIView               *uExtendedContent;
@property(weak, nonatomic) IBOutlet UILabel              *uRestaurantName1;
@property(strong, nonatomic) IBOutlet NSLayoutConstraint *cLeftConstraint;
@property(strong, nonatomic) IBOutlet NSLayoutConstraint *cCenterConstraint;
@property(weak, nonatomic) IBOutlet UILabel              *uRatingLabel;
@property(weak, nonatomic) IBOutlet UILabel              *uDetail;
@property(weak, nonatomic) IBOutlet UIButton             *uBtnNegative;
@property(weak, nonatomic) IBOutlet UIButton             *uBtnPositive;
@property(weak, nonatomic) IBOutlet QRCheckmark          *uCheckmark;

@property(nonatomic, copy) void (^onFadeOut)(ContributionRestaurantCell *cell,int ratingCount);

@property(nonatomic, copy) void (^onWriteReview)(IMGRestaurant *restaurant,int ratingCount);

@property(nonatomic, copy) ContributionListModel *contributionModel;

- (void)resetState;

- (void)fadeOut;

//- (void)updateData:(ContributionListModel *)model;
@end
