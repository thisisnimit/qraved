//
//  V2_SearchBar.m
//  Qraved
//
//  Created by harry on 2017/8/24.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_SearchBar.h"

@implementation V2_SearchBar


- (void)layoutSubviews {
    [super layoutSubviews];
    
    for (UIView *view in self.subviews)
    {
        if ([view isKindOfClass:NSClassFromString(@"UIView")] && view.subviews.count > 0) {
            for (UIView *subview in view.subviews) {
                if ([subview isKindOfClass:NSClassFromString(@"UITextField")]) {
                    UITextField *textField = (UITextField *)subview;
                    CGRect frame = textField.frame;
                    frame.size.height = 40;
                     frame.origin.y = 10;
                    textField.frame = frame;
                    textField.font = [UIFont systemFontOfSize:14];
                    
                    if (textField) {
                        
                        UIImage *image = [UIImage imageNamed: @"ic_search_medium"];
                        
                        UIImageView *iView = [[UIImageView alloc] initWithImage:image];
                        
                        iView.frame = CGRectMake(0, 0, image.size.width , image.size.height);
                        
                        
                        
                        
                        textField.leftView = iView;
                        
                        
                    }
                    

                    
                    
                    break;
                }
               

            }
            break;
        }
    }
}

@end
