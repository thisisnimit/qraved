//
//  ContributionRestaurantCell.m
//  Qraved
//
//  Created by Tek Yin on 5/23/17.
//  Copyright © 2017 Imaginato. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import "ContributionRestaurantCell.h"
#import "ContributionListModel.h"
#import "WebServiceV2.h"
#import "QRCheckmark.h"

@implementation ContributionRestaurantCell {
    BOOL isExpanded;
    int currentRating;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    self.uStar.delegate = self;
    self.uRestaurantName1.textColor = [UIColor color333333];
    self.uDetail.textColor = [UIColor color999999];
}

- (void)resetState {
    isExpanded = NO;
    [self.uCheckmark reset];
    [self.uStar setValue:-1];
    self.uMainContainer.alpha    = 1;
    self.cTopSpace.constant      = 0;
    self.uExtendedContainer.y    = self.uDetail.bottom;
    self.uExtendedContent.alpha  = 0;
    self.uExtendedContent.hidden = YES;
    [NSLayoutConstraint activateConstraints:@[self.cLeftConstraint]];
    [NSLayoutConstraint deactivateConstraints:@[self.cCenterConstraint]];
    [self.uBtnPositive setTitle:@"I love it" forState:UIControlStateNormal];
    [self.uBtnNegative setTitle:@"Not interested" forState:UIControlStateNormal];
    [self layoutIfNeeded];

}

- (void)updateRatingText:(int)rating {
//    NSArray *ratingText = @[
//            @"I won't come back",
//            @"I didn't like it",
//            @"It was okay",
//            @"I like it",
//            @"I loved it"
//    ];
    NSArray *ratingText = @[
                            @"Terrible",
                            @"Bad",
                            @"Average",
                            @"Good",
                            @"Excellent"
                            ];
    self.uRatingLabel.text = ratingText[(NSUInteger) rating];
    currentRating = rating;
}

- (void)starComponent:(QRStarComponent *)control onRatingChangeEnd:(int)rating {
    [self setStarInfo:rating];
}

- (void)setStarInfo:(int)rating{
    [self updateRatingText:rating];
    if (!isExpanded) {
        isExpanded = YES;
        self.cTopSpace.constant      = -self.uImage.height; // slide until the image is disappeared
        self.uExtendedContent.alpha  = 0;
        self.uExtendedContent.hidden = NO;
        [UIView animateWithDuration:0.5
                         animations:^{
                             [NSLayoutConstraint activateConstraints:@[self.cCenterConstraint]];
                             [NSLayoutConstraint deactivateConstraints:@[self.cLeftConstraint]];
                             self.uExtendedContainer.y   = self.uRestaurantName1.bottom;
                             self.uExtendedContent.alpha = 1;
                             [self layoutIfNeeded];
                         }];
        [self.uBtnPositive setTitle:@"Submit" forState:UIControlStateNormal];
        [self.uBtnNegative setTitle:@"Cancel" forState:UIControlStateNormal];
    }

}

- (void)loveOut {
    [self.uCheckmark startAnimation];
    [UIView animateWithDuration:0.5
                          delay:0.3
                        options:nil
                     animations:^{
                         self.uMainContainer.alpha = 0;
                     }
                     completion:^(BOOL finished) {
                         if (self.onFadeOut) {
                             self.onFadeOut(self,currentRating);
                         }
                     }];
}

- (void)fadeOut {
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.uMainContainer.alpha = 0;
                     }
                     completion:^(BOOL finished) {
                         if (self.onFadeOut) {
                             self.onFadeOut(self,-5);
                         }
                     }];
}

- (IBAction)doWriteReview:(id)sender {
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    restaurant.restaurantId = self.contributionModel.restaurant_id ;
    restaurant.title = self.contributionModel.restaurant_name;
    restaurant.cuisineName = self.contributionModel.cuisine;
    if (self.onWriteReview) {
        self.onWriteReview(restaurant,currentRating);
    }
}

- (IBAction)doNegative:(id)sender {
    if (isExpanded) {
        isExpanded = 0;
        [self.uStar setValue:-1];
        [UIView animateWithDuration:0.5
                         animations:^{
                             [NSLayoutConstraint activateConstraints:@[self.cLeftConstraint]];
                             [NSLayoutConstraint deactivateConstraints:@[self.cCenterConstraint]];
                             self.cTopSpace.constant     = 0;
                             self.uExtendedContainer.y   = self.uDetail.bottom;
                             self.uExtendedContent.alpha = 0;
                             [self layoutIfNeeded];
                         }
                         completion:^(BOOL finished) {
                             self.uExtendedContent.hidden = YES;
                         }];
        [self.uBtnPositive setTitle:@"I love it" forState:UIControlStateNormal];
        [self.uBtnNegative setTitle:@"Not interested" forState:UIControlStateNormal];
    } else {
        [self fadeOut];
    }
}

- (IBAction)doPositive:(id)sender {
    
    if ([self.uBtnPositive.titleLabel.text isEqualToString:@"I love it"]) {
        [self.uStar setValue:4];
        [self setStarInfo:4];
    }else{
         [self loveOut];
    }
    
   
}

- (void)setContributionModel:(ContributionListModel *)contributionModel{
    _contributionModel = contributionModel;
    
    self.uRestaurantName1.text = contributionModel.restaurant_name;
    self.uDetail.text = [NSString stringWithFormat:@"%@ • %@", contributionModel.cuisine,contributionModel.district];
    [self.uImage sd_setImageWithURL:[NSURL URLWithString:[contributionModel.photo returnFullImageUrl]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
}

@end
