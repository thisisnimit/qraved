//
//  V2_InstagramDistrictCell.h
//  Qraved
//
//  Created by harry on 2017/8/1.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_DistrictModel.h"

@interface V2_InstagramDistrictCell : UICollectionViewCell


@property (nonatomic, strong) V2_AreaModel *model;
@property (nonatomic, copy) void (^chooseDistrict)(V2_AreaModel *model);
@property (nonatomic, copy) void (^removeDistrict)(V2_AreaModel *model);

@end
