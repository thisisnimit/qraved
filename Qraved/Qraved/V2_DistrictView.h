//
//  V2_DistrictView.h
//  Qraved
//
//  Created by harry on 2017/6/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_DistrictModel.h"
@interface V2_DistrictView : UIView

@property (nonatomic, strong) NSArray *districtArr;

@property (nonatomic, copy) void (^selectedDistrict)(id model);
@property (nonatomic, copy) void (^nearbyCheck)();

- (void)changeLocationSkip;

@end
