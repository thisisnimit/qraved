//
//  QRInstagramPhotoView.h
//  Qraved-2
//
//  Created by Tek Yin on 5/15/17.
//  Copyright © 2017 Qraved. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface V2_InstagramPhotoView : UIScrollView <UIScrollViewDelegate>
@property(nonatomic, strong) UIImageView *uImage;
@property(nonatomic) void (^imageTapBlock)();

- (void)loadImage:(UIImage *)image;

- (void)loadImageUrl:(NSString *)imageUrl;
@end
