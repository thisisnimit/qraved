//
// Created by Tek Yin on 5/15/17.
// Copyright (c) 2017 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIImage (Colorfy)
- (UIColor *)averageColor;
@end