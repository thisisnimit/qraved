//
//  V2_DiscoverListHandler.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/18.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JAKARTANetWork.h"
#import "RecentSearchesTableViewCellModel.h"
#import "RecentSearchesSuperModel.h"
#import "V2_DiscoverListResultRestaurantTableViewCellModel.h"

@interface V2_DiscoverListHandler : NSObject

+(void)getTrendingNow:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array))successBlock anderrorBlock:(void(^)())errorBlock;

+(void)getRecentSearcheds:(NSDictionary*)dic andSuccessBlock:(void(^)(RecentSearchesSuperModel * model ,NSArray * restaurantD ))successBlock anderrorBlock:(void(^)())errorBlock;

+(void)getSearchResult:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * cuisineArray,NSMutableArray * foodtypeArray,NSMutableArray * landmarkAnddistrictArray,NSMutableArray * restaurantArray,NSMutableArray * journalArray))successBlock anderrorBlock:(void(^)())errorBlock;

+(void)getSearchSuggestion:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * cuisineArray,NSMutableArray * foodtypeArray,NSMutableArray * landmarkAnddistrictArray,NSMutableArray * restaurantArray,NSMutableArray * journalArray))successBlock anderrorBlock:(void(^)())errorBlock;

+(void)getgetTrendingJournal:(NSDictionary*)dic andSuccessBlock:(void(^)(V2_DiscoverListResultRestaurantTableViewCellModel * model))successBlock anderrorBlock:(void(^)())errorBlock;

+(void)getgetTrendingJournalSeeAll:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array))successBlock anderrorBlock:(void(^)())errorBlock;


+(void)getResaultCityViewData:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array))successBlock anderrorBlock:(void(^)())errorBlock;


+(void)setRecentKeywords:(NSDictionary*)dic userID:(NSString *)userid andSuccessBlock:(void(^)(NSMutableArray * array))successBlock anderrorBlock:(void(^)())errorBlock;
@end
