//
//  V2_DiscoverView.h
//  Qraved
//
//  Created by harry on 2017/9/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface V2_DiscoverView : UIView

@property (nonatomic, copy)void (^foodTagTapped)(NSNumber *tagId);
@property (nonatomic, copy)void (^deliveryTapped)();
@property (nonatomic, copy)void (^discoverBy)(BOOL isLocation);


@end
