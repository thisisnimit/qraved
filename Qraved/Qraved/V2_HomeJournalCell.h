//
//  V2_HomeJournalCell.h
//  Qraved
//
//  Created by harry on 2017/10/12.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_HomeModel.h"
#import "IMGMediaComment.h"
@protocol V2_HomeJournalCellDelegate <NSObject>
- (void)seeAllJournal:(V2_HomeModel *)model;
- (void)gotoJournalDetail:(IMGMediaComment *)journal andName:(NSString *)name;

@end

@interface V2_HomeJournalCell : UITableViewCell

@property (nonatomic, strong) V2_HomeModel *model;
@property (nonatomic, weak) id <V2_HomeJournalCellDelegate>delegate;

@end
