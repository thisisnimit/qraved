//
//  V2_HomeRestaurantCell.h
//  Qraved
//
//  Created by harry on 2017/10/12.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_HomeModel.h"

@protocol V2_HomeRestaurantCellDelegate <NSObject>
- (void)seeAllRestaurant:(NSNumber *)componentId andName:(NSString *)name;
- (void)gotoRestaurantDetail:(IMGRestaurant *)restaurant andName:(NSString *)name;

@end

@interface V2_HomeRestaurantCell : UITableViewCell

@property (nonatomic, strong) V2_HomeModel *model;
@property (nonatomic, weak) id <V2_HomeRestaurantCellDelegate>delegate;
@property (nonatomic, copy) void(^savedTapped)(IMGRestaurant *restaurant);
- (void)freshSavedButton:(BOOL)liked;
@end
