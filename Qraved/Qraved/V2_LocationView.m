//
//  V2_LocationView.m
//  Qraved
//
//  Created by harry on 2017/6/16.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_LocationView.h"

#import "UIColor+Hex.h"
#import "V2_LocationCollectionViewCell.h"
#import <BlocksKit/BlocksKit+UIKit.h>
@interface V2_LocationView ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    UICollectionView *locationCollectionView;
}

@end

@implementation V2_LocationView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    
    self.backgroundColor = [UIColor whiteColor];
    
//    UIView *topSpace = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 7)];
//    topSpace.backgroundColor = [UIColor whiteColor];
//    [self addSubview:topSpace];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15,24, DeviceWidth-90, 16)];
    lblTitle.text = @"Nearby Locations";
    lblTitle.textColor = [UIColor color333333];
    lblTitle.font = [UIFont boldSystemFontOfSize:14];
    [self addSubview:lblTitle];
    
    UIButton *btnChange = [UIButton buttonWithType:UIButtonTypeCustom];
    btnChange.frame = CGRectMake(15, 7, DeviceWidth-30, 50);
    [self addSubview:btnChange];
    [btnChange setImage:[UIImage imageNamed:@"ic_home_arrow"] forState:UIControlStateNormal];
    btnChange.imageEdgeInsets = UIEdgeInsetsMake(0, DeviceWidth-30-10, 0, 0);
    [btnChange bk_whenTapped:^{
        if (self.gotoLocationList) {
            self.gotoLocationList();
        }
    }];
    
    UILabel *lblSeeAll = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth-95, 10, 50, 30)];
    lblSeeAll.text = @"See All";
    lblSeeAll.textAlignment = NSTextAlignmentRight;
    lblSeeAll.textColor = [UIColor color999999];
    lblSeeAll.font =[UIFont systemFontOfSize:12];
    [btnChange addSubview:lblSeeAll];
    
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(95, 120);
//    layout.minimumLineSpacing = 15;
//    layout.minimumInteritemSpacing = 15;
    layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 0);
    
    locationCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 40, DeviceWidth, 145) collectionViewLayout:layout];
    locationCollectionView.backgroundColor = [UIColor whiteColor];
    locationCollectionView.delegate = self;
    locationCollectionView.dataSource = self;
    locationCollectionView.showsHorizontalScrollIndicator = NO;
    [self addSubview:locationCollectionView];
    
    
    UIView *hSpace = [[UIView alloc] initWithFrame:CGRectMake(0, locationCollectionView.endPointY+5, DeviceWidth, 5)];
    hSpace.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    [self addSubview:hSpace];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.locationArr.count+1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
    [locationCollectionView registerClass:[V2_LocationCollectionViewCell class] forCellWithReuseIdentifier:CellIdentifier];

    V2_LocationCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (indexPath.row==0) {
        cell.locationImageView.image = [UIImage imageNamed:@"ic_home_nearby"];
        cell.lblLocationName.text = @"Nearby";
        cell.lblLandmarkTitle.hidden = YES;
    }else{
        cell.lblLandmarkTitle.hidden = NO;
        V2_LocationModel *model = [self.locationArr objectAtIndex:indexPath.row-1];
        cell.locationModel = model;
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        if (self.nearBy) {
            self.nearBy();
        }
    }else{
        V2_LocationModel *model = [self.locationArr objectAtIndex:indexPath.row-1];
        if (self.gotoSRPbyLocation) {
            self.gotoSRPbyLocation(model);
        }
    }
}

- (void)setLocationArr:(NSArray *)locationArr{
    _locationArr = locationArr;
    [locationCollectionView reloadData];
}

@end
