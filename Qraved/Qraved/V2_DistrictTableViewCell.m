//
//  V2_DistrictTableViewCell.m
//  Qraved
//
//  Created by harry on 2017/7/28.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DistrictTableViewCell.h"
#import "V2_DistrictModel.h"
#import "V2_LocationModel.h"
@interface V2_DistrictTableViewCell ()<TTGTagCollectionViewDelegate,TTGTagCollectionViewDataSource>{
    TTGTagCollectionView *tagCollectionView;
    NSMutableArray <UIView *> *tagViews;
}

@end

@implementation V2_DistrictTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    
        [self createUI];
    }
    return self;
}

- (void)createUI{
    tagViews = [NSMutableArray array];
    tagCollectionView = [[TTGTagCollectionView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 10)];
    tagCollectionView.delegate = self;
    tagCollectionView.dataSource = self;
    tagCollectionView.horizontalSpacing = 5;
    tagCollectionView.verticalSpacing = 5;
    tagCollectionView.contentInset = UIEdgeInsetsMake(20, 15, 0, 15);
    [self.contentView addSubview:tagCollectionView];
}

#pragma mark - TTGTagCollectionViewDelegate

- (CGSize)tagCollectionView:(TTGTagCollectionView *)tagCollectionView sizeForTagAtIndex:(NSUInteger)index {
    return tagViews[index].frame.size;
}

- (void)tagCollectionView:(TTGTagCollectionView *)tagCollectionView didSelectTag:(UIView *)tagView atIndex:(NSUInteger)index {
    
    UIButton *button = (UIButton *)[tagViews objectAtIndex:index];
    
    V2_AreaModel *model;
    if (self.selectArr.count>0) {
        model = [self.selectArr objectAtIndex:index];
    }else{
        model = [self.model.districts objectAtIndex:index];
    }
    
    if (model.selected) {
        [self setButtonNormalStyle:button andAreaModel:model];
        if (self.removeDidtrict) {
            self.removeDidtrict(model);
        }
    }else{
        [self setButtonSelectStyle:button andAreaModel:model];
        if (self.addDistrict) {
            self.addDistrict(model);
        }
    }
}

#pragma mark - TTGTagCollectionViewDataSource

- (NSUInteger)numberOfTagsInTagCollectionView:(TTGTagCollectionView *)tagCollectionView {
    return tagViews.count;
}

- (UIView *)tagCollectionView:(TTGTagCollectionView *)tagCollectionView tagViewForIndex:(NSUInteger)index {
    return tagViews[index];
}

- (void)setModel:(V2_DistrictModel *)model{
    _model = model;
    
    [tagViews removeAllObjects];
    for (V2_AreaModel *areaModel in model.districts) {
        [tagViews addObject:[self createButton:areaModel]];
    }
    [tagCollectionView reload];
    CGFloat tagHeight = tagCollectionView.contentSize.height;
    tagCollectionView.frame = CGRectMake(0, 0, DeviceWidth, tagHeight);
    self.pointY = tagHeight;
}

- (UIButton *)createButton:(V2_AreaModel *)model{
    UIButton *btnDistrict = [UIButton buttonWithType:UIButtonTypeCustom];
    CGFloat tagLabelW = [model.name boundingRectWithSize:CGSizeMake(DeviceWidth, MAXFLOAT)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,NSFontAttributeName:[UIFont systemFontOfSize:12]}
                                            context:nil].size.width;
    if ([model.landMarkTypeId isEqual:@1] || [model.landMarkTypeId isEqual:@2]) {
        btnDistrict.frame = CGRectMake(0, 0, tagLabelW+45, 32);
    }else{
        btnDistrict.frame = CGRectMake(0, 0, tagLabelW+30, 32);
    }
    
    btnDistrict.layer.masksToBounds = YES;
    [btnDistrict setTitle:model.name forState:UIControlStateNormal];
    btnDistrict.layer.cornerRadius = 16;
    btnDistrict.titleLabel.font = [UIFont systemFontOfSize:12];
    btnDistrict.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
//    [btnDistrict addTarget:self action:@selector(chooseDistrict:) forControlEvents:UIControlEventTouchUpInside];
    btnDistrict.userInteractionEnabled = NO;
    if (model.selected) {
        [self setButtonSelectStyle:btnDistrict andAreaModel:model];
    }else{
        [self setButtonNormalStyle:btnDistrict andAreaModel:model];
    }
    
    return btnDistrict;
}

- (void)setButtonSelectStyle:(UIButton *)button andAreaModel:(V2_AreaModel *)model{
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithHexString:@"DE2029"];
    
    if ([model.landMarkTypeId isEqual:@1]) {
        [button setImage:[UIImage imageNamed:@"ic_badge_mall_select.png"] forState:UIControlStateNormal];
    }else if ([model.landMarkTypeId isEqual:@2]){
        [button setImage:[UIImage imageNamed:@"ic_badge_hotel_idle_select.png"] forState:UIControlStateNormal];
    }
}

- (void)setButtonNormalStyle:(UIButton *)button andAreaModel:(V2_AreaModel *)model{
    [button setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    
    if ([model.landMarkTypeId isEqual:@1]) {
        [button setImage:[UIImage imageNamed:@"ic_badge_mall_idle.png"] forState:UIControlStateNormal];
    }else if ([model.landMarkTypeId isEqual:@2]){
        [button setImage:[UIImage imageNamed:@"ic_badge_hotel_idle.png"] forState:UIControlStateNormal];
    }
}

- (void)setSelectArr:(NSArray *)selectArr{
    _selectArr = selectArr;
    [tagViews removeAllObjects];
    for (V2_AreaModel *areaModel in selectArr) {
        [tagViews addObject:[self createButton:areaModel]];
    }
    [tagCollectionView reload];
    CGFloat tagHeight = tagCollectionView.contentSize.height;
    tagCollectionView.frame = CGRectMake(0, 0, DeviceWidth, tagHeight);
    self.pointY = tagHeight;
}

@end
