//
//  V2_QuickSearchModel.m
//  Qraved
//
//  Created by harry on 2017/6/15.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_QuickSearchModel.h"

@implementation V2_QuickSearchModel


- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"id"]) {
        self.searchId = value;
    }
}

@end
