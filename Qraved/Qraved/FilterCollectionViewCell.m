//
//  FilterCollectionViewCell.m
//  Qraved
//
//  Created by harry on 2017/12/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "FilterCollectionViewCell.h"
#import "VerticalAlignmentLabel.h"
@implementation FilterCollectionViewCell
{
    UIImageView *bgImageView;
    UIImageView *imageView;
    UIImageView *logoImageView;
    VerticalAlignmentLabel *lblTitle;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    
    bgImageView = [UIImageView new];
    bgImageView.image = [UIImage imageNamed:@"ic_bg_circle"];
    bgImageView.hidden = YES;
    
    imageView = [UIImageView new];
    
    logoImageView = [UIImageView new];
    logoImageView.backgroundColor = [UIColor whiteColor];
    logoImageView.hidden = YES;
    
    lblTitle = [VerticalAlignmentLabel new];
    lblTitle.font = [UIFont systemFontOfSize:12];
    lblTitle.textColor = [UIColor color333333];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    
    [self.contentView sd_addSubviews:@[bgImageView,imageView,logoImageView,lblTitle]];
    
    bgImageView.sd_layout
    .topSpaceToView(self.contentView, 0)
    .leftSpaceToView(self.contentView, 0)
    .widthIs(66)
    .heightEqualToWidth();
    
    imageView.sd_layout
    .topSpaceToView(self.contentView, 4)
    .leftSpaceToView(self.contentView, 4)
    .widthIs(58)
    .heightEqualToWidth();
    
    imageView.layer.masksToBounds = YES;
    imageView.layer.cornerRadius = 29;
    
    logoImageView.sd_layout
    .leftSpaceToView(self.contentView, 40)
    .topSpaceToView(self.contentView, 47)
    .widthIs(21)
    .heightEqualToWidth();
    logoImageView.layer.masksToBounds = YES;
    logoImageView.layer.cornerRadius = 10.5;
    
    lblTitle.sd_layout
    .topSpaceToView(bgImageView, 10)
    .leftEqualToView(bgImageView)
    .widthIs(66)
    .heightIs(30);
}

- (void)setModel:(InstagramFilterModel *)model{
    _model = model;
    if ([model.image hasPrefix:@"http"]) {
        [imageView sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
    }else{
        [imageView sd_setImageWithURL:[NSURL URLWithString:[model.image returnFullImageUrl]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
    }
    
    if ([model.type isEqualToString:@"location"]) {
        lblTitle.text = model.locationName;
    }else if ([model.type isEqualToString:@"hashtag"]){
        lblTitle.text = model.tag;
    }else if ([model.type isEqualToString:@"user"]){
        lblTitle.text = model.username;
    }else if ([model.type isEqualToString:@"foodtag"]){
        lblTitle.text = model.tagName;
    }
    
    if ([model.type isEqualToString:@"user"]) {
        [lblTitle setNumberOfLines:1];
    }else{
        [lblTitle setNumberOfLines:2];
    }
   
    if ([model.type isEqualToString:@"location"]) {
        logoImageView.hidden = NO;
        logoImageView.image = [UIImage imageNamed:@"ic_loc"];
    }else if ([model.type isEqualToString:@"hashtag"]){
        logoImageView.hidden = NO;
        logoImageView.image = [UIImage imageNamed:@"ic_hashtg"];
    }else if ([model.type isEqualToString:@"user"]){
        logoImageView.hidden = NO;
        logoImageView.image = [UIImage imageNamed:@"ic_blogger"];
    }else if ([model.type isEqualToString:@"foodtag"]){
        logoImageView.hidden = NO;
        logoImageView.image = [UIImage imageNamed:@"ic_hashtg"];
    }else{
        logoImageView.hidden = YES;
    }
    
    if (model.isSelect) {
        bgImageView.hidden = NO;
    }else{
        bgImageView.hidden = YES;
    }
}

@end
