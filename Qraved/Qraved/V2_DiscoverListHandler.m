//
//  V2_DiscoverListHandler.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/18.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverListHandler.h"
#import "RecentSearchesTableViewCellModel.h"
#import "RecentSearchesSuperModel.h"
#import "V2_DiscoverListResultRestaurantTableViewCellModel.h"
#import "V2_DiscoverListResultRestaurantTableViewCellSubModel.h"
#import "selecteCityViewModel.h"
#import "selecteCityCenterViewModel.h"

@implementation V2_DiscoverListHandler
+(void)getTrendingNow:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array))successBlock anderrorBlock:(void(^)())errorBlock{
    
    [[JAKARTANetWork sharedManager] GET:@"id/search/api/searchService/getTrendingKeywords" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        successBlock(responseObject[@"data"]);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if(error.userInfo[@"com.alamofire.serialization.response.error.data"]){
            NSLog(@"%@", error.userInfo[@"com.alamofire.serialization.response.error.data"]);
            NSString *result = [[NSString alloc] initWithData:error.userInfo[@"com.alamofire.serialization.response.error.data"]  encoding:NSUTF8StringEncoding];
            NSLog(@"result = %@",result);
            NSData *JSONData = [result dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *responseJSON = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableLeaves error:nil];
            NSLog(@"%@",responseJSON);
        }
        errorBlock();
    }];


}

+(void)getRecentSearcheds:(NSDictionary*)dic andSuccessBlock:(void(^)(RecentSearchesSuperModel * model, NSArray *restaurantD ))successBlock anderrorBlock:(void(^)())errorBlock{

    [[JAKARTANetWork sharedManager] GET:@"id/search/api/searchService/getRecentKeywords" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        RecentSearchesSuperModel *model = [RecentSearchesSuperModel mj_objectWithKeyValues:responseObject[@"data"]];
        model.history = [RecentSearchesTableViewCellModel mj_objectArrayWithKeyValuesArray:model.history];
        
        successBlock(model, responseObject[@"histroy"]);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        if(error.userInfo[@"com.alamofire.serialization.response.error.data"]){
            NSLog(@"%@", error.userInfo[@"com.alamofire.serialization.response.error.data"]);
            NSString *result = [[NSString alloc] initWithData:error.userInfo[@"com.alamofire.serialization.response.error.data"]  encoding:NSUTF8StringEncoding];
            NSLog(@"result = %@",result);
            NSData *JSONData = [result dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *responseJSON = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableLeaves error:nil];
            NSLog(@"%@",responseJSON);
        }
        errorBlock();
    }];

}
+(void)getSearchSuggestion:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * cuisineArray,NSMutableArray * foodtypeArray,NSMutableArray * landmarkAnddistrictArray,NSMutableArray * restaurantArray,NSMutableArray * journalArray))successBlock anderrorBlock:(void(^)())errorBlock{
    [[JAKARTANetWork sharedManager] GET:@"id/search/api/suggest" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //        NSLog(@"%@",responseObject);
        NSMutableArray *cuisinesArray = [NSMutableArray array];//cuisines
        NSMutableArray *foodtypeArray = [NSMutableArray array];//Foods
        NSMutableArray *landmarkAnddistrictArray = [NSMutableArray array];//Locations
        NSMutableArray *restaurantArray = [NSMutableArray array];//Restaurants
        NSMutableArray *journalArray = [NSMutableArray array];//Journals
        if ([responseObject isKindOfClass:[NSArray class]] && [responseObject count]>0) {
            for (NSDictionary *dict in responseObject) {
                if ([dict[@"type"] isEqualToString:@"cuisine"]) {
                    [cuisinesArray addObject:dict];
                }
                else if ([dict[@"type"] isEqualToString:@"foodtype"]){
                    [foodtypeArray addObject:dict];
                }
                else if ([dict[@"type"] isEqualToString:@"landmark"]){
                    
                    [landmarkAnddistrictArray addObject:dict];
                }
                else if ([dict[@"type"] isEqualToString:@"district"]){
                    
                    [landmarkAnddistrictArray addObject:dict];
                }
                else if ([dict[@"type"] isEqualToString:@"restaurant"]){
                    
                    [restaurantArray addObject:dict];
                }
                else if ([dict[@"type"] isEqualToString:@"journal"]){
                    
                    [journalArray addObject:dict];
                }
            }
            
            //        NSLog(@"%@",landmarkAnddistrictArray);
            //        NSLog(@"%@",cuisinesArray);
            //        NSLog(@"%@",foodtypeArray);
            //        NSLog(@"%@",restaurantArray);
            //        NSLog(@"%@",journalArray);
            
            successBlock(cuisinesArray,foodtypeArray,landmarkAnddistrictArray,restaurantArray, journalArray);
        }
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        
    }];

}

+(void)getSearchResult:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * cuisineArray,NSMutableArray * foodtypeArray,NSMutableArray * landmarkAnddistrictArray,NSMutableArray * restaurantArray,NSMutableArray * journalArray))successBlock anderrorBlock:(void(^)())errorBlock{


    [[JAKARTANetWork sharedManager] GET:@"id/search/api/searchService/getSuggestion" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        NSLog(@"%@",responseObject);
        NSMutableArray *cuisinesArray = [NSMutableArray array];//cuisines
        NSMutableArray *foodtypeArray = [NSMutableArray array];//Foods
        NSMutableArray *landmarkAnddistrictArray = [NSMutableArray array];//Locations
        NSMutableArray *restaurantArray = [NSMutableArray array];//Restaurants
        NSMutableArray *journalArray = [NSMutableArray array];//Journals
        for (NSDictionary *dict in responseObject[@"data"]) {
            if ([dict[@"type"] isEqualToString:@"cuisine"]) {
                [cuisinesArray addObject:dict];
            }
            else if ([dict[@"type"] isEqualToString:@"foodtype"]){
                [foodtypeArray addObject:dict];
            }
            else if ([dict[@"type"] isEqualToString:@"landmark"]){
            
                [landmarkAnddistrictArray addObject:dict];
            }
            else if ([dict[@"type"] isEqualToString:@"district"]){
                
                [landmarkAnddistrictArray addObject:dict];
            }
            else if ([dict[@"type"] isEqualToString:@"restaurant"]){
            
                [restaurantArray addObject:dict];
            }
            else if ([dict[@"type"] isEqualToString:@"journal"]){
                
                [journalArray addObject:dict];
            }
        }
        
//        NSLog(@"%@",landmarkAnddistrictArray);
//        NSLog(@"%@",cuisinesArray);
//        NSLog(@"%@",foodtypeArray);
//        NSLog(@"%@",restaurantArray);
//        NSLog(@"%@",journalArray);
        
        successBlock(cuisinesArray,foodtypeArray,landmarkAnddistrictArray,restaurantArray, journalArray);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
    }];
}


+(void)getgetTrendingJournal:(NSDictionary*)dic andSuccessBlock:(void(^)(V2_DiscoverListResultRestaurantTableViewCellModel * model))successBlock anderrorBlock:(void(^)())errorBlock{

    [[JAKARTANetWork sharedManager] GET:@"id/search/api/searchService/getTrendingJournal" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
//        NSLog(@"%@",responseObject);
        V2_DiscoverListResultRestaurantTableViewCellModel *model = [V2_DiscoverListResultRestaurantTableViewCellModel mj_objectWithKeyValues:responseObject];
        model.data = [V2_DiscoverListResultRestaurantTableViewCellSubModel mj_objectArrayWithKeyValuesArray:model.data];

        successBlock(model);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"%@",error);
        errorBlock();
    }];

}


+(void)getgetTrendingJournalSeeAll:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array))successBlock anderrorBlock:(void(^)())errorBlock{
    
    [[JAKARTANetWork sharedManager] GET:@"id/search/api/searchService/getTrendingJournal" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        //        NSLog(@"%@",responseObject);
        NSMutableArray *array = [NSMutableArray array];
        array = [V2_DiscoverListResultRestaurantTableViewCellSubModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
        
        successBlock(array);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        NSLog(@"%@",error);
    }];
    
}

+(void)getResaultCityViewData:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array))successBlock anderrorBlock:(void(^)())errorBlock{

    [[JAKARTANetWork sharedManager] GET:@"id/home/api/Location/area/district" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSMutableArray *aray = [NSMutableArray array];
        
        aray = [selecteCityViewModel mj_objectArrayWithKeyValuesArray:responseObject[@"data"]];
        
        
        successBlock(aray);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
               NSLog(@"%@",error);
    }];

}

+(void)setRecentKeywords:(NSDictionary*)dic userID:(NSString *)userid andSuccessBlock:(void(^)(NSMutableArray * array))successBlock anderrorBlock:(void(^)())errorBlock{
    NSString *typeS = [dic objectForKey:@"type"];
    NSString *titleS = [dic objectForKey:@"title"];
    NSString *idS = [dic objectForKey:@"id"];
    NSString *imgS = [[dic objectForKey:@"img"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    IMGUser *user = [IMGUser currentUser];
    NSNumber *userIdS = user.userId;
    
    NSString *encodedTitle = CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)titleS, nil, CFSTR(":/?#[]@!$&’()*+,;="), kCFStringEncodingUTF8));
    NSString *urlString = [NSString stringWithFormat:@"id/search/api/searchService/setRecentActivity?type=%@&title=%@&id=%@&img=%@&userId=%@",typeS,encodedTitle, idS, imgS, userIdS];

    [[JAKARTANetWork sharedManager] POST:urlString parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"%@",responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if(error.userInfo[@"com.alamofire.serialization.response.error.data"]){
            NSLog(@"%@", error.userInfo[@"com.alamofire.serialization.response.error.data"]);
            NSString *result = [[NSString alloc] initWithData:error.userInfo[@"com.alamofire.serialization.response.error.data"]  encoding:NSUTF8StringEncoding];
            NSLog(@"result = %@",result);
            NSData *JSONData = [result dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *responseJSON = [NSJSONSerialization JSONObjectWithData:JSONData options:NSJSONReadingMutableLeaves error:nil];
            NSLog(@"%@",responseJSON);
        }
    }];
}

@end
