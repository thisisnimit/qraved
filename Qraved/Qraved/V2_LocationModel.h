//
//  V2_LocationModel.h
//  Qraved
//
//  Created by harry on 2017/6/15.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface V2_LocationModel : NSObject

@property (nonatomic,copy) NSNumber *area_id;
@property (nonatomic,copy) NSNumber *code;
@property (nonatomic,copy) NSNumber *location_id;
@property (nonatomic,copy) NSString *image_url;
@property (nonatomic,copy) NSString *latitude;
@property (nonatomic,copy) NSString *longitude;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSNumber *popular;
@property (nonatomic,copy) NSString *seo_keyword;
@property (nonatomic,copy) NSNumber *sort_order;
@property (nonatomic,copy) NSString *location_type;
@property (nonatomic,copy) NSString *default_image_url;


@end
