//
//  V2_HomeViewController.m
//  Qraved
//
//  Created by harry on 2017/6/14.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_HomeViewController.h"
#import "V2_HomeHandler.h"
#import "V2_CityView.h"
#import "HomeSearchComponent.h"
#import "V2_QuickSearchView.h"
#import "LoadingView.h"
#import "V2_LocationView.h"
#import "V2_Instagram.h"
#import "V2_HomeModel.h"
#import "V2_GuideView.h"
#import "V2_RestaurantView.h"
#import "V2_ArticleView.h"
#import "V2_InstagramViewController.h"
#import "InstagramDetailController.h"
#import "V2_DistrictViewController.h"
#import "WebServiceV2.h"
#import "V2_DiningGuideListViewController.h"
#import "DiningGuideRestaurantsViewController.h"
#import "CityUtils.h"
#import "V2_JournalListViewController.h"
#import "CityHandler.h"
#import "V2_BannerModel.h"
#import "HomeService.h"
//#import "DiscoverListViewController.h"
#import "V2_DiscoverListViewController.h"
#import "JournalDetailViewController.h"
#import "DetailViewController.h"
#import "V2_PhotoDetailViewController.h"
#import "V2_DiscoverListResultViewController.h"
#import "HomeUtil.h"
#import "V2_CItyViewController.h"
#import "DetailViewController.h"
#import "MXParallaxHeader.h"
#import "UIScrollView+Helper.h"
#import "homeNewFeatuerView.h"
#import "UIScrollView+Helper.h"
#import "V2_PreferenceViewController.h"
#import "V2_DiscoverView.h"
#import "v2_DiscoveryCategoriesViewController.h"
#import "V2_DiscoverResultViewController.h"
#import "V2_HomeRestaurantCell.h"
#import "V2_HomeGuideCell.h"
#import "V2_HomeJournalCell.h"
#import "BrandViewController.h"
#import "CouponDetailViewController.h"
#import "CampaignWebViewController.h"
#import "DeliveryViewController.h"

@interface V2_HomeViewController ()<CLLocationManagerDelegate,UIScrollViewDelegate,MXParallaxHeaderDelegate,AndroidRefreshDelegate,SDCycleScrollViewDelegate,UITableViewDelegate,UITableViewDataSource,V2_HomeJournalCellDelegate,V2_HomeGuideCellDelegate,V2_HomeRestaurantCellDelegate>
{
    // UIScrollView *homeScroll;
    V2_LocationView *locationView;
    V2_Instagram *instagramView;
    V2_GuideView *guideView;
    UIView *currentView;
    locationHandler _locationHandler;
    BOOL isFirstAlertView;
    //UIScrollView *bannerScrollView;
    V2_DiscoverView *discoverView;
    UIView *navView;
    UIButton *btnSearch;

    IMGRestaurant *currentRestaurant;
    SDCycleScrollView *stripImageView;
    SDCycleScrollView *bannerScrollView;
  
    UIView *lineView;
    
    NSMutableArray *hashTagArr;
    NSMutableArray *filterArray;
    NSMutableArray *districtArr;
    NSMutableArray *imgBannerArr;
    
    UIView *currentView1;
    UIImageView *currentImageView;
    homeNewFeatuerView *homeNewFeatuerV;
    NSMutableArray *imgArray;
    NSMutableArray *imgModelArray;
    BOOL isPullFresh;
    
    UITableView *homeTableView;
    int page;
    BOOL isLoadMore;
}
@property(nonatomic, strong) HomeSearchComponent *uSearchButton;
@property (nonatomic, strong) NSMutableArray *cityArray;
@property (nonatomic, strong) NSMutableArray *bannerArr;
@property (nonatomic, strong) NSMutableArray *homeDataArr;
@property (nonatomic, strong) NSMutableArray *locationArr;
@property (nonatomic, strong) NSMutableArray *instagramArr;
@end

@implementation V2_HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [HomeUtil getversionInfoFromService:nil andBlock:^(id responese) {
        NSNumber *lastVersion=[[NSUserDefaults standardUserDefaults] valueForKey:@"version"];
        if (![lastVersion isEqualToNumber:[responese objectForKey:@"version"]]) {
            
            [HomeUtil getDownloadInfoFromService:nil andBlock:^(id responese) {
                NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *loadPath = [pathArray objectAtIndex:0];
                //获取文件的完整路径
                NSString *filePatch = [loadPath stringByAppendingPathComponent:@"DownloadData.plist"];
                //写入plist里面
                [responese writeToFile:filePatch atomically:YES];
                
                
            } andErroBlock:^(NSError *error) {
                
                
                NSLog(@"getDownloadInfoFromServiceError:%@",error.localizedDescription);
                
                
            }];
        }
        [[NSUserDefaults standardUserDefaults] setValue:[responese objectForKey:@"version"] forKey:@"version"];
        
        
    } andErroBlock:^(NSError *error) {
        
        
        NSLog(@"getversionInfoFromServiceError:%@",error.localizedDescription);
        
        
    }];
    
    [self loadData];
    [self loadMainUI];
    [self requestData];
    [self getDistrictData];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(freshData) name:NOTIFICATION_REFRESH_HOME object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCity) name:CITY_SELECT_NOTIFICATION object:nil];
    //[self CreateNewFeature];
    
   
}

- (void)getDistrictData{
    [HomeService getLocation:nil andIsPreferred:NO andBlock:^(id locationArr) {
        [districtArr removeAllObjects];
        [districtArr addObjectsFromArray:locationArr];
        
    } andError:^(NSError *error) {
    }];
}

#pragma mark - new feature
-(void)CreateNewFeature{
    
    BOOL isFirstNew=[[NSUserDefaults standardUserDefaults] boolForKey:@"isFirstNew"];
    if (isFirstNew) {
        IMGUser *user = [IMGUser currentUser];
        if (user.userId || user.token){
            
            homeNewFeatuerV = [[homeNewFeatuerView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHEIGHT)];
            [[AppDelegate ShareApp].window addSubview:homeNewFeatuerV];
            [[NSUserDefaults standardUserDefaults] setObject:@"no" forKey:@"isFirstNew"];
            [homeNewFeatuerV bk_whenTapped:^{
                [homeNewFeatuerV removeFromSuperview];
            }];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [homeNewFeatuerV removeFromSuperview];
            });
        }
    }
}
#pragma mark - change city fresh data
- (void)refreshCity{
    isPullFresh = NO;
    [self getDistrictData];
    [self freshData];
}

- (void)freshData{
    page = 1;
    [self getBannerList];
    [self requestInstagramFilter];
    [self requestInstagramHashTag];
    [self requestHomeLocation];
    [self requestHomeInstagram];
    [self getHomeList];
}


#pragma mark - AndroidRefreshDelegate
- (void)androidRefreshViewDidCancel:(ARAndroidRefreshView *)refreshView {
    NSLog(@"%s %d Cancel", __FUNCTION__, __LINE__);
}

- (void)androidRefreshViewDidComplete:(ARAndroidRefreshView *)refreshView {
    NSLog(@"%s %d Complete", __FUNCTION__, __LINE__);
}
- (BOOL)androidRefreshView:(ARAndroidRefreshView *)refreshView shouldRefreshView:(UIView *)targetView{
    
    if (homeTableView.contentOffset.y>0) {
        return NO;
    }else{
        return YES;
    }
}

- (void)androidRefreshView:(ARAndroidRefreshView *)refreshView beginRefreshingView:(UIView *)targetView {
    NSLog(@"%s %d Refresh View", __FUNCTION__, __LINE__);
    isPullFresh = YES;
    [self downRefreshData];
    
}

- (void)loadMainUI{
    self.view.backgroundColor = [UIColor whiteColor];
    //self.automaticallyAdjustsScrollViewInsets = NO;
    
    homeTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+20-IMG_StatusBarAndNavigationBarHeight-IMG_TabbarHeight) style:UITableViewStyleGrouped];
    homeTableView.backgroundColor = [UIColor whiteColor];
    homeTableView.delegate = self;
    homeTableView.dataSource = self;
    [self.view addSubview:homeTableView];
    homeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [homeTableView ar_addAndroidRefreshWithDelegate:self];
    homeTableView.ar_headerView.refreshOffset = 44;
    
    if (@available(iOS 11.0, *)) {
        homeTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        homeTableView.estimatedRowHeight = 0;
        homeTableView.estimatedSectionHeaderHeight = 0;
        homeTableView.estimatedSectionFooterHeight = 0;
    }
    
    homeTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
    homeTableView.mj_footer.hidden = YES;
}
- (void)loadMore{
    page+=1;
    [self getHomeList];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.homeDataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    V2_HomeModel *model = [self.homeDataArr objectAtIndex:indexPath.row];
    
    if ([model.asset isEqualToString:@"restaurant"]) {
        NSString *CellIdentifier = [NSString stringWithFormat:@"RestaurantCell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        V2_HomeRestaurantCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[V2_HomeRestaurantCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.delegate = self;
        }
        cell.model = model;
        __weak typeof(self) weakSelf = self;
        __weak typeof(cell) weakCell = cell;
        cell.savedTapped = ^(IMGRestaurant *restaurant) {
            [weakSelf savedRestaurant:restaurant andCell:weakCell];
        };
        
        return cell;
        
    }else if ([model.asset isEqualToString:@"article"]){
//        static NSString *idenStr = @"homeJournal";
        NSString *CellIdentifier = [NSString stringWithFormat:@"JournalCell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        V2_HomeJournalCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[V2_HomeJournalCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.delegate = self;
        }
        cell.model = model;
        return cell;
        
    }else if ([model.asset isEqualToString:@"guide"]){
//        static NSString *idenStr = @"homeGuide";
        NSString *CellIdentifier = [NSString stringWithFormat:@"GuideCell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        V2_HomeGuideCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[V2_HomeGuideCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.delegate = self;
        }
        cell.model = model;
        return cell;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    V2_HomeModel *model = [self.homeDataArr objectAtIndex:indexPath.row];
    if ([model.asset isEqualToString:@"restaurant"]){
        return 265;
    }else if ([model.asset isEqualToString:@"article"]){
        return 228;
    }else if ([model.asset isEqualToString:@"guide"]){
        return 205;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerView = [UIView new];
    headerView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    
    bannerScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceWidth*.8) delegate:self placeholderImage:[UIImage imageNamed:@"placeholder"]];
    bannerScrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    if (imgBannerArr.count>0) {
        bannerScrollView.imageURLStringsGroup = imgBannerArr;
        bannerScrollView.hidden = NO;
    }else{
        bannerScrollView.hidden = YES;
    }
    bannerScrollView.autoScrollTimeInterval = 4;
    bannerScrollView.showPageControl = YES;
    bannerScrollView.pageDotColor = [UIColor colorWithHexString:@"FFFFFF" alpha:0.5];
    bannerScrollView.currentPageDotColor = [UIColor whiteColor];
    bannerScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleClassic;
    
    [headerView addSubview:bannerScrollView];
    
    discoverView = [V2_DiscoverView new];
    [headerView addSubview:discoverView];

    __weak typeof(self) weakSelf = self;
//    discoverView.foodTagTapped = ^(NSNumber *tagId) {
//        [weakSelf foodTagClick:tagId];
//    };
    discoverView.discoverBy = ^(BOOL isLocation) {
        [weakSelf discoverClick:isLocation];
    };

    discoverView.deliveryTapped = ^{

        [IMGAmplitudeUtil trackGoFoodWithName:@"CL - Delivery CTA" andJournalId:nil andRestaurantId:nil andPhotoId:nil andLocation:@"Homepage" andOrigin:nil andOrder:nil];

        [IMGAmplitudeUtil trackDeliveryWithName:@"RC - View Delivery Page" andJournalId:nil andFoodTagId:nil andLocation:nil andOrigin:@"Delivery Page CTA"];

        DeliveryViewController *deliveryViewController = [[DeliveryViewController alloc] init];
        [weakSelf.navigationController pushViewController:deliveryViewController animated:YES];
    };

    //location view
    locationView = [V2_LocationView new];

    locationView.locationArr = self.locationArr;
    __weak typeof(districtArr) weakDistrictArr = districtArr;
    locationView.gotoLocationList = ^(){
        V2_DiscoverResultViewController *discoverListResultVC = [[V2_DiscoverResultViewController alloc] init];
        discoverListResultVC.isLocation = YES;
        discoverListResultVC.locationArr = weakDistrictArr;
        [weakSelf.navigationController pushViewController:discoverListResultVC animated:YES];
    };
    locationView.nearBy = ^{

        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        [param setValue:@"Nearby" forKey:@"Type"];
        [[Amplitude instance] logEvent:@"CL – Preferred Location" withEventProperties:param];

        V2_DiscoverResultViewController *discoverListResultVC = [[V2_DiscoverResultViewController alloc] init];
        discoverListResultVC.isHomeNearBy = YES;
        discoverListResultVC.locationArr = weakDistrictArr;
        [weakSelf.navigationController pushViewController:discoverListResultVC animated:YES];
    };
    locationView.gotoSRPbyLocation = ^(V2_LocationModel *model){

        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        [param setValue:@"Dynamic" forKey:@"Type"];
        [param setValue:model.location_type forKey:@"Location Type"];
        [param setValue:model.name forKey:@"Value"];
        [[Amplitude instance] logEvent:@"CL – Preferred Location" withEventProperties:param];

        V2_DiscoverResultViewController *discoverResultVC = [[V2_DiscoverResultViewController alloc] init];
        if ([model.location_type isEqualToString:@"landmark"]) {
            discoverResultVC.homeLandmarkId = model.location_id;
        }else{
            discoverResultVC.homeDistrictId = model.location_id;
        }
        discoverResultVC.homeLocationName = model.name;
        discoverResultVC.locationArr = weakDistrictArr;
        [weakSelf.navigationController pushViewController:discoverResultVC animated:YES];
    };


    instagramView = [V2_Instagram new];
    
    instagramView.filterArr = filterArray;
    instagramView.hashTagArr = hashTagArr;
    instagramView.instagramArr = self.instagramArr;

    instagramView.seeAllInstagram = ^{
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        [param setValue:@"Homepage" forKey:@"Location"];
        [[Amplitude instance] logEvent:@"CL - Trending Instagram" withEventProperties:param];
        V2_InstagramViewController *instagramViewController = [[V2_InstagramViewController alloc] init];
        //        instagramViewController.cityName = [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT];
        [weakSelf.navigationController pushViewController:instagramViewController animated:YES];
    };

    instagramView.tappedFilterTag = ^(InstagramFilterModel *model) {
        V2_InstagramViewController *instagramViewController = [[V2_InstagramViewController alloc] init];
        //        instagramViewController.cityName = [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT];
        instagramViewController.selectModel = model;
        [weakSelf.navigationController pushViewController:instagramViewController animated:YES];
    };

    [headerView sd_addSubviews:@[locationView,instagramView]];


    stripImageView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceWidth*.27) delegate:self placeholderImage:[UIImage imageNamed:@"placeholder"]];
    stripImageView.pageControlAliment = SDCycleScrollViewPageContolAlimentCenter;
    if (imgArray.count>0) {
        stripImageView.imageURLStringsGroup = imgArray;
        stripImageView.hidden = NO;
    }else{
        stripImageView.hidden = YES;
    }
    
    stripImageView.autoScroll = NO;
    stripImageView.pageControlStyle = SDCycleScrollViewPageContolStyleNone;

    [headerView addSubview:stripImageView];

    if (self.bannerArr.count>0) {
        bannerScrollView.sd_layout
        .topSpaceToView(headerView,0)
        .leftSpaceToView(headerView,0)
        .rightSpaceToView(headerView,0)
        .heightIs(DeviceWidth * .8);
    }else{
        bannerScrollView.sd_layout
        .topSpaceToView(headerView,0)
        .leftSpaceToView(headerView,0)
        .rightSpaceToView(headerView,0)
        .heightIs(0);
    }
    
    
    //    pageControl.sd_layout
    //    .topSpaceToView(headerView,DeviceWidth*.8 - 17)
    //    .leftSpaceToView(headerView,0)
    //    .rightSpaceToView(headerView,0)
    //    .heightIs(10);
    
    discoverView.sd_layout
    .topSpaceToView(bannerScrollView, 0)
    .leftEqualToView(bannerScrollView)
    .rightEqualToView(bannerScrollView)
    .heightIs(100);

    locationView.sd_layout
    .leftEqualToView(discoverView)
    .rightEqualToView(discoverView)
    .topSpaceToView(discoverView,0)
    .heightIs(195);

    instagramView.sd_layout
    .leftEqualToView(locationView)
    .rightEqualToView(locationView)
    .topSpaceToView(locationView,0)
    .autoHeightRatio(0);

    if (imgArray.count>0) {
        stripImageView.sd_layout
        .topSpaceToView(instagramView,0)
        .leftSpaceToView(headerView,0)
        .widthIs(DeviceWidth)
        .heightIs(DeviceWidth*.27);
    }else{
        stripImageView.sd_layout
        .topSpaceToView(instagramView,0)
        .leftSpaceToView(headerView,0)
        .widthIs(DeviceWidth)
        .heightIs(0);
    }


    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    CGFloat bannerHeight = 0.0f;
    if (self.bannerArr.count>0) {
        bannerHeight = DeviceWidth*.8;
    }
    
    CGFloat instagramHeight = 0.0f;
    if (self.instagramArr.count>0) {
        int lines = (int)self.instagramArr.count/3;
        if (self.instagramArr.count%3 >0) {
            lines = (int)self.instagramArr.count/3 +1;
        }
        instagramHeight = ((DeviceWidth-2)/3+1)*lines+105;
        
    }else{
        instagramHeight = 105;
    }
    
    CGFloat hashTagHeight = 0.0f;
    if (hashTagArr.count>0) {
        hashTagHeight = 46;
    }
    CGFloat filterHeight = 0.0f;
    if (filterArray.count>0) {
        filterHeight = 120;
    }
    
    CGFloat stripBannerHeight = 0.0f;
    if (imgArray.count>0) {
        stripBannerHeight = DeviceWidth*.27+5;
    }
    
    return bannerHeight+100+195+instagramHeight+stripBannerHeight+hashTagHeight+filterHeight;
}

- (void)savedRestaurant:(IMGRestaurant *)restaurant andCell:(V2_HomeRestaurantCell *)cell{
    currentRestaurant = restaurant;
    [CommonMethod doSaveWithRestaurant:restaurant andViewController:self andCallBackBlock:^(BOOL isSaved) {
        [cell freshSavedButton:isSaved];
    }];
}

- (void)discoverClick:(BOOL)isLocation{
    if (isLocation) {
        [[Amplitude instance] logEvent:@"CL - Discover by Location" withEventProperties:nil];
        V2_DiscoverResultViewController *discoverResultVC = [[V2_DiscoverResultViewController alloc] init];
        discoverResultVC.isLocation = YES;
        discoverResultVC.locationArr = districtArr;
        [self.navigationController pushViewController:discoverResultVC animated:YES];
    }else{
        
        [[Amplitude instance] logEvent:@"CL - Discover by Food" withEventProperties:nil];
        v2_DiscoveryCategoriesViewController *discoverVC = [[v2_DiscoveryCategoriesViewController alloc] init];
        discoverVC.locationArr = districtArr;
        [self.navigationController pushViewController:discoverVC animated:YES];
    }
}

#pragma mark - cell delegate
- (void)seeAllRestaurant:(NSNumber *)componentId andName:(NSString *)name{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setValue:name forKey:@"Origin"];
    
    [[Amplitude instance] logEvent:@"SC – View Search Result Restaurant" withEventProperties:param];
    
    V2_DiscoverListResultViewController *discoverListResultVC = [[V2_DiscoverListResultViewController alloc] init];
    discoverListResultVC.keywordString = @"";
    //discoverListResultVC.cityName = cityButton.titleLabel.text;
    discoverListResultVC.isFromHome = YES;
    discoverListResultVC.homeSectionComponentId = componentId;
    discoverListResultVC.locationArr = districtArr;
    [self.navigationController pushViewController:discoverListResultVC animated:YES];
    
}

- (void)gotoRestaurantDetail:(IMGRestaurant *)restaurant andName:(NSString *)name{
    
    [IMGAmplitudeUtil trackRestaurantWithName:@"RC – View Restaurant Page" andRestaurantId:restaurant.restaurantId andRestaurantTitle:restaurant.title andLocation:nil andOrigin:name andType:nil];
    
    DetailViewController *detailViewController = [[DetailViewController alloc] initWithRestaurant:restaurant];
    [self.navigationController pushViewController:detailViewController animated:YES];
    
}

- (void)seeAllGuide:(NSString *)name{
    
    [IMGAmplitudeUtil trackGuideWithName:@"RC - View Dining Guide list" andDiningGuideId:nil andDiningGuideName:nil andOrigin:@"Homepage"];
    
    V2_DiningGuideListViewController *guideViewController = [[V2_DiningGuideListViewController alloc] init];
    [self.navigationController pushViewController:guideViewController animated:YES];
    
}
- (void)gotoDiningGuideDetail:(IMGDiningGuide *)diningGuide{
    
    [IMGAmplitudeUtil trackGuideWithName:@"RC - View Dining Guide page" andDiningGuideId:diningGuide.diningGuideId andDiningGuideName:diningGuide.pageName andOrigin:@"Homepage"];
    
    DiningGuideRestaurantsViewController *diningResList = [[DiningGuideRestaurantsViewController alloc] initWithDiningGuide:diningGuide];
    [self.navigationController pushViewController:diningResList animated:YES];
}

- (void)seeAllJournal:(V2_HomeModel *)model{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setValue:model.asset forKey:@"Origin"];
    [[Amplitude instance] logEvent:@"SC – View Search Result Journal" withEventProperties:param];
    
    V2_JournalListViewController *journalVC = [[V2_JournalListViewController alloc] init];
    if (model.categoryId != nil) {
        journalVC.categoryId = model.categoryId;
    }
    [self.navigationController pushViewController:journalVC animated:YES];
    
}
- (void)gotoJournalDetail:(IMGMediaComment *)journal andName:(NSString *)name{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setValue:name forKey:@"Origin"];
    [[Amplitude instance] logEvent:@"RC – View Journal Detai" withEventProperties:param];
    
    [IMGAmplitudeUtil trackJDPWithName:@"RC - View Journal detail" andJournalId:journal.mediaCommentId andOrigin:@"Homepage" andChannel:nil andLocation:nil];
    
    JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
    jdvc.amplitudeType = @"Article card on Article list";
    jdvc.journal = journal;
    [self.navigationController pushViewController:jdvc animated:YES];
} 

- (void)loadData{
    isPullFresh = NO;
    isLoadMore = NO;
    page = 1;
    self.cityArray = [NSMutableArray array];
    self.bannerArr = [NSMutableArray array];
    imgBannerArr = [NSMutableArray array];
    id json = [CommonMethod getJsonFromFile:HOME_CACHE_BANNER];
    if (json) {
        [imgBannerArr addObjectsFromArray:[CommonMethod getHomeBannerImage:json]];
        [self.bannerArr addObjectsFromArray:[CommonMethod getHomeBanner:json]];
    }
    self.homeDataArr = [NSMutableArray array];
    id componentsJson = [CommonMethod getJsonFromFile:HOME_CACHE_COMPONENTS];
    if (componentsJson) {
        [self.homeDataArr addObjectsFromArray:[CommonMethod getHomeComponents:componentsJson]];
    }
    
    self.instagramArr = [NSMutableArray array];
    id instagramJson = [CommonMethod getJsonFromFile:HOME_CACHE_INSTAGRAM];
    if (instagramJson) {
        [self.instagramArr addObjectsFromArray:[CommonMethod getHomeInstagram:instagramJson]];
    }
    self.locationArr = [NSMutableArray array];
    id locationJson = [CommonMethod getJsonFromFile:HOME_CACHE_LOCATION];
    if (locationJson) {
        [self.locationArr addObjectsFromArray:[CommonMethod getHomeLocation:locationJson]];
    }
    hashTagArr = [NSMutableArray array];
    id hashTagJson = [CommonMethod getJsonFromFile:HOME_CACHE_HASHTAG];
    if (hashTagJson) {
        [hashTagArr addObjectsFromArray:[CommonMethod getHomeInstagramFilter:hashTagJson]];
    }
    filterArray = [NSMutableArray array];
    id filterJson = [CommonMethod getJsonFromFile:HOME_CACHE_FILTER];
    if (filterJson) {
        [filterArray addObjectsFromArray:[CommonMethod getHomeInstagramFilter:filterJson]];
    }
    districtArr = [NSMutableArray array];
    imgArray = [NSMutableArray array];
    imgModelArray = [NSMutableArray array];
    id stripBannerJson = [CommonMethod getJsonFromFile:HOME_CACHE_STRIPBANNER];
    if (stripBannerJson) {
        [imgArray addObjectsFromArray:[CommonMethod getHomeBannerImage:stripBannerJson]];
        [imgModelArray addObjectsFromArray:[CommonMethod getHomeBanner:stripBannerJson]];
    }
}
- (void)downRefreshData{
    [self freshData];
}
#pragma mark - request method
- (void)requestData{
    [self getBannerList];
    [self requestInstagramFilter];
    [self requestInstagramHashTag];
    [self requestHomeLocation];
    [self requestHomeInstagram];
    [self getStripBanner];
    [self getHomeList];
}

- (void)requestCity{
    
    [CityUtils getCitiesWithCountryId:0 andBlock:^(NSArray *cityArray){
        
        [self.cityArray addObjectsFromArray:cityArray];
        
    }];
}

- (void)getStripBanner{
    [WebServiceV2 getStripBanner:nil andBlock:^(id bannerArr) {
        
        if ([bannerArr count]>0) {
            [imgModelArray removeAllObjects];
            [imgArray removeAllObjects];
            [imgModelArray addObjectsFromArray:bannerArr];
            
            for (V2_BannerModel *bannerModel in bannerArr) {
                [imgArray addObject:bannerModel.bannerImage];
                
            }
            
        }
        stripImageView.hidden = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [homeTableView reloadData];
        });
        
        
    } andError:^(NSError *error) {
        [[LoadingView sharedLoadingView] stopLoading];
        
        
    }];
}

- (void)getBannerList{
    [WebServiceV2 getBanners:nil andBlock:^(id bannerArr) {
        [homeTableView.ar_headerView finishRefreshing];
        [self.bannerArr removeAllObjects];
        [imgBannerArr removeAllObjects];
        [self.bannerArr addObjectsFromArray:bannerArr];
        
        for (V2_BannerModel *bannerModel in bannerArr) {
            [imgBannerArr addObject:bannerModel.bannerImage];
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [homeTableView reloadData];
        });
    } andError:^(NSError *error) {
        
        [homeTableView.ar_headerView finishRefreshing];
    }];
}


- (void)getHomeList{
    [WebServiceV2 getHomeData:[NSString stringWithFormat:@"%d",page] andBlock:^(id homedata) {
        
        if ([homedata count]>0) {
            isLoadMore = YES;
            homeTableView.mj_footer.hidden = NO;
        }else{
            isLoadMore = NO;
            homeTableView.mj_footer.hidden = YES;
        }
        if (page == 1) {
            [self.homeDataArr removeAllObjects];
        }
        [self.homeDataArr addObjectsFromArray:homedata];
        [homeTableView reloadData];
        [homeTableView.mj_footer endRefreshing];
        
        [[LoadingView sharedLoadingView] stopLoading];
        
    } andError:^(NSError *error) {
        [homeTableView.mj_footer endRefreshing];
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

- (void)requestInstagramFilter{
    [HomeService getInstagramFilter:nil andBlock:^(NSArray *filterArr) {
        [filterArray removeAllObjects];
        [filterArray addObjectsFromArray:filterArr];
    } andError:^(NSError *error) {
        
    }];
}

- (void)requestInstagramHashTag{
    [HomeService getInstagramHashtags:nil andBlock:^(NSArray *hashtagsArr) {
        [hashTagArr removeAllObjects];
        [hashTagArr addObjectsFromArray:hashtagsArr];
        
    } andError:^(NSError *error) {
        
    }];
}

- (void)requestHomeInstagram{
    NSMutableDictionary *param = [NSMutableDictionary dictionaryWithDictionary:@{@"cityId":[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID],@"limit":@"6",@"offset":@"0",@"shuffle":@"false"}];
    [HomeService getInstagramPhotos:param andBlock:^(id instagramPhotos) {
        [self.instagramArr removeAllObjects];
        [self.instagramArr addObjectsFromArray:instagramPhotos];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [homeTableView reloadData];
        });
    } andError:^(NSError *error) {
        
    }];
}

- (void)requestHomeLocation{
    if (!isPullFresh) {
        [[LoadingView sharedLoadingView] startLoading];
    }
    [HomeService getHomeLocation:nil andBlock:^(NSArray *locationArr) {
        [self.locationArr removeAllObjects];
        [self.locationArr addObjectsFromArray:locationArr];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [homeTableView reloadData];
        });
    } andError:^(NSError *error) {
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    homeTableView.bounces = YES;
    
    if (homeTableView.contentOffset.y<=0) {
        homeTableView.qraved_navigationTitleScrollLastOffsetY = [NSNumber numberWithFloat:44];
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if (![userDefaults boolForKey:[NSString stringWithFormat:@"%@%@",@"isFirstTimeOpenApp",QRAVED_VERSION]]) {
        [userDefaults setBool:true forKey:[NSString stringWithFormat:@"%@%@",@"isFirstTimeOpenApp",QRAVED_VERSION]];
    }
    
    
    [AppDelegate ShareApp].isSlideHomeHidden = NO;
    [[self rdv_tabBarController] setTabBarHidden:NO animated:NO];
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];

    [AppDelegate ShareApp].isSlideHomeHidden = YES;
    if ([AppDelegate ShareApp].isSlideHomeHidden
        &&[AppDelegate ShareApp].isSlideJournalHidden
        &&[AppDelegate ShareApp].isSlideDiningGuideHidden) {
        [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    }
    [[SDWebImageManager sharedManager].imageDownloader setExecutionOrder:1];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSLog(@"%f",scrollView.contentOffset.y);
    if (scrollView == homeTableView && ![homeTableView.mj_footer isRefreshing]) {
        
        if (scrollView.contentOffset.y>0) {
            [scrollView scrollNavigationController:[AppDelegate ShareApp].homeNavigationController];
            if (isLoadMore) {
                if (scrollView.contentOffset.y>200) {
                    homeTableView.bounces = YES;
                }else{
                    homeTableView.bounces = NO;
                }
                
            }else{
                
                homeTableView.bounces = NO;
            }
            
        }else{
            [UIView animateWithDuration:0.3 animations:^{
                [scrollView scrollNavigationController:[AppDelegate ShareApp].homeNavigationController];
            }];
            
            if (scrollView.contentOffset.y>-44) {
                homeTableView.bounces = NO;
            }
            
        }
    }
}

- (void)bannerImageClick:(UITapGestureRecognizer *)tap{
    UIView *view = tap.view;
    NSInteger index = view.tag;
    
}

#pragma mark - SDCycleScrollViewDelegate

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index
{
    if (cycleScrollView == bannerScrollView) {
        V2_BannerModel *bannerModel = [self.bannerArr objectAtIndex:index];
        
        if ([bannerModel.type isEqualToString:@"deliveryPage"]) {
            [IMGAmplitudeUtil trackGoFoodBannerWithName:@"CL - Home Banner" andBannerId:bannerModel.bannerId];
        }else{
            NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
            [param setValue:@"Carousel Top Banner" forKey:@"Type"];
            [param setValue:bannerModel.type forKey:@"Landing Page"];
            [param setValue:[NSString stringWithFormat:@"%@",bannerModel.bannerId] forKey:@"Banner_ID"];
            [[Amplitude instance] logEvent:@"CL - Home Banner" withEventProperties:param];
        }
        
        [self bannerTapped:bannerModel];
        
    }else{
        V2_BannerModel *bannerModel = [imgModelArray objectAtIndex:index];
        
        if ([bannerModel.type isEqualToString:@"deliveryPage"]) {
            [IMGAmplitudeUtil trackGoFoodBannerWithName:@"CL - Home Banner" andBannerId:bannerModel.bannerId];
        }else{
            NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
            [param setValue:@"Strip Banner" forKey:@"Type"];
            [param setValue:bannerModel.type forKey:@"Landing Page"];
            [param setValue:[NSString stringWithFormat:@"%@",bannerModel.bannerId] forKey:@"Banner_ID"];
            [[Amplitude instance] logEvent:@"CL - Home Banner" withEventProperties:param];
        }
        [self bannerTapped:bannerModel];
    }
}
- (void)bannerTapped:(V2_BannerModel *)bannerModel{
    if ([bannerModel.type isKindOfClass:[NSNull class]]) {
        return ;
    }
    
    
    if ([bannerModel.type isEqualToString:@"web"]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[bannerModel.attributes objectForKey:@"webLink"]]];
        
    }else if ([bannerModel.type isEqualToString:@"article"]){
        
        JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
        jdvc.amplitudeType = @"Article card on Article list";
        IMGMediaComment *journal = [[IMGMediaComment alloc] init];
        journal.mediaCommentId = [bannerModel.attributes objectForKey:@"id"];
        jdvc.journal = journal;
        //        jdvc.categoryDataArrM = [NSMutableArray arrayWithArray:categoryArray];
        [self.navigationController pushViewController:jdvc animated:YES];
        
        
    }else if ([bannerModel.type isEqualToString:@"guide"]){
        
        IMGDiningGuide *guide = [[IMGDiningGuide alloc] init];
        guide.diningGuideId = [bannerModel.attributes objectForKey:@"id"];
        DiningGuideRestaurantsViewController *diningResList = [[DiningGuideRestaurantsViewController alloc] initWithDiningGuide:guide];
        
        [self.navigationController pushViewController:diningResList animated:YES];
    }else if ([bannerModel.type isEqualToString:@"appstore"]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/cn/app/qraved/id731842943?l=en&mt=8"]];
        
    }else if ([bannerModel.type isEqualToString:@"restaurant"]){
        
        IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
        restaurant.restaurantId = [bannerModel.attributes objectForKey:@"id"];
        DetailViewController *detailVc = [[DetailViewController alloc] initWithRestaurant:restaurant];
        [self.navigationController pushViewController:detailVc animated:YES];
        
        
    }else if ([bannerModel.type isEqualToString:@"openInstagramTrendingPhotos"]){
        
        //        instagramView = [V2_Instagram new];
        //        instagramView.instagramArr = InstagramArr;
        //        instagramView.seeAllInstagram = ^{
        //            NSLog(@"-----");
        V2_InstagramViewController *instagramViewController = [[V2_InstagramViewController alloc] init];
        //            instagramViewController.cityName = [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT];;
        [self.navigationController pushViewController:instagramViewController animated:YES];
        //        };
        
    }else if ([bannerModel.type isEqualToString:@"scrollToHomeSuggestionArea"]){
        NSLog(@"%f",currentView1.frame.origin.y);
        //[homeTableView setContentOffset:CGPointMake(0, currentView1.frame.origin.y - 64) animated:YES];
        
    }else if ([bannerModel.type isEqualToString:@"openApp"]){
        
        if ([[bannerModel.attributes objectForKey:@"iosDeepLink"] length] > 0) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[bannerModel.attributes objectForKey:@"iosDeepLink"]]];
        }else{
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[bannerModel.attributes objectForKey:@"url"]]];
        }
    }else if ([bannerModel.type isEqualToString:@"brand"]){
        
        [IMGAmplitudeUtil trackCampaignWithName:@"RC - View Brand Page" andCampaignId:nil andBrandId:[bannerModel.attributes objectForKey:@"brandId"]  andCouponId:nil andLocation:nil andOrigin:@"Homepage Banner" andIsMall:NO];
        
        BrandViewController *brandViewController = [[BrandViewController alloc] init];
        brandViewController.brandId = [bannerModel.attributes objectForKey:@"brandId"];
        brandViewController.isMall = NO;
        [self.navigationController pushViewController:brandViewController animated:YES];
    }else if ([bannerModel.type isEqualToString:@"mall"]){
        
        [IMGAmplitudeUtil trackCampaignWithName:@"RC - View Mall Page" andCampaignId:nil andBrandId:[bannerModel.attributes objectForKey:@"landmarkId"] andCouponId:nil andLocation:nil andOrigin:@"Homepage Banner" andIsMall:YES];
        
        BrandViewController *brandViewController = [[BrandViewController alloc] init];
        brandViewController.mallId = [bannerModel.attributes objectForKey:@"landmarkId"];
        brandViewController.isMall = YES;
        [self.navigationController pushViewController:brandViewController animated:YES];
    }else if ([bannerModel.type isEqualToString:@"coupon"]){
        
        [IMGAmplitudeUtil trackCampaignWithName:@"CL - Coupon Detail Page" andCampaignId:nil andBrandId:nil andCouponId:[bannerModel.attributes objectForKey:@"id"] andLocation:@"Homepage" andOrigin:nil andIsMall:NO];
        
        CouponDetailViewController *couponDetailViewController = [[CouponDetailViewController alloc] init];
        couponDetailViewController.couponId = [bannerModel.attributes objectForKey:@"id"];
        MLNavigationController *nav = [[MLNavigationController alloc] initWithRootViewController:couponDetailViewController];
        [self.navigationController presentViewController:nav animated:YES completion:nil];
    }else if ([bannerModel.type isEqualToString:@"in-app-browser"]){
        CampaignWebViewController *campaignWebViewController = [[CampaignWebViewController alloc] init];
        campaignWebViewController.campaignUrl = [bannerModel.attributes objectForKey:@"webLink"];
        [self.navigationController pushViewController:campaignWebViewController animated:YES];
    }else if ([bannerModel.type isEqualToString:@"deliveryPage"]){
        
        [IMGAmplitudeUtil trackDeliveryWithName:@"RC - View Delivery Page" andJournalId:nil andFoodTagId:nil andLocation:nil andOrigin:@"Home Banner"];
        DeliveryViewController *deliveryViewController = [[DeliveryViewController alloc] init];
        [self.navigationController pushViewController:deliveryViewController animated:YES];
    }
}


- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

- (void)loadImageUrl:(NSArray *)imageArr imageType:(NSString *)type{
    if ([type isEqualToString:@"restaurant"]) {
        for (IMGRestaurant *restaurant in imageArr) {
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[restaurant.logoImage returnFullImageUrlWithWidth:216 andHeight:135]] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                
            }];
        }
    }else if ([type isEqualToString:@"guide"]){
        for (IMGDiningGuide *guide in imageArr) {
            if (![guide.headerImage isEqual:[NSNull null]]) {
                [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[guide.headerImage returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]]options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                    
                }];
            }
        }
    }else if ([type isEqualToString:@"article"]){
        for (IMGMediaComment *journal in imageArr) {
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[journal.journalImageUrl returnFullImageUrlWithWidth:125 andHeight:85]]options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                
            }];
        }
    }else if ([type isEqualToString:@"instagram"]){
        for (NSString *imgStr in imageArr) {
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:imgStr]options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                
            }];
        }
    }else{
        for (NSString *imgStr in imageArr) {
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[imgStr returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]]options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                
            }];
        }
    }
}

- (void)foodTagClick:(NSNumber *)tagId{
    V2_DiscoverResultViewController *discoverResultPage = [[V2_DiscoverResultViewController alloc] init];
    if (![tagId isEqual:@0]) {
        discoverResultPage.quicklySearchFoodTagId = tagId;
    }
    discoverResultPage.locationArr = self.locationArr;
    [self.navigationController pushViewController:discoverResultPage animated:YES];
}

- (void)gotoLocationList:(NSArray *)locationArr{
    V2_DistrictViewController *districtVC = [[V2_DistrictViewController alloc] init];
    districtVC.locationArray = locationArr;
    districtVC.refreshLocation = ^{
        [self.locationArr removeAllObjects];
        locationView.locationArr = self.locationArr;
        [self freshData];
        
    };
    
    [self.navigationController pushViewController:districtVC animated:YES];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_REFRESH_HOME object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CITY_SELECT_NOTIFICATION object:nil];
}


@end

