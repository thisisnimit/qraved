    //
//  main.m
//  Qraved
//
//  Created by Shine Wang on 5/30/13.
//  Copyright (c) 2013 ShineWang. All rights reserved.
//
    
#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
