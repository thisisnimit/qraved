//
//  V2_Instagram.m
//  Qraved
//
//  Created by harry on 2017/6/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_Instagram.h"
#import "V2_InstagramModel.h"
#import "UIColor+Hex.h"
#import <BlocksKit/BlocksKit+UIKit.h>
#import "InstagramFilterView.h"
@interface V2_Instagram ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,TTGTextTagCollectionViewDelegate>{
    UICollectionView *instagramCollectionView;
    TTGTextTagCollectionView *tagCollectionView;
    NSMutableArray *titleArr;
}

@end
#define ITEM_SIZE_WIDTH (DeviceWidth-2)/3
@implementation V2_Instagram

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    self.backgroundColor = [UIColor whiteColor];
    
    titleArr = [NSMutableArray array];
    
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(ITEM_SIZE_WIDTH, ITEM_SIZE_WIDTH);
    layout.minimumLineSpacing = 1;
    layout.minimumInteritemSpacing = 1;
    //layout.sectionInset = UIEdgeInsetsMake(0, 1, 0, 0);
    
    instagramCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,0, DeviceWidth, self.bounds.size.height) collectionViewLayout:layout];
    instagramCollectionView.backgroundColor = [UIColor whiteColor];
    instagramCollectionView.delegate = self;
    instagramCollectionView.dataSource = self;
    instagramCollectionView.showsHorizontalScrollIndicator = NO;
    instagramCollectionView.scrollEnabled = NO;
    [self addSubview:instagramCollectionView];
    
    
    [instagramCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"instagramCell"];
    [instagramCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"instagramHeader"];
    [instagramCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"instagramFooter"];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.instagramArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"instagramCell" forIndexPath:indexPath];
    V2_InstagramModel *model = [self.instagramArr objectAtIndex:indexPath.row];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.contentView.bounds];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    [imageView sd_setImageWithURL:[NSURL URLWithString:model.low_resolution_image] placeholderImage:[UIImage imageNamed:@"placeholder"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    [cell.contentView addSubview:imageView];

    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    UICollectionReusableView *reusableView = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"instagramHeader" forIndexPath:indexPath];
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 16, DeviceWidth-30, 16)];
        lblTitle.text = @"Trending Photos on Instagram";
        lblTitle.font = [UIFont boldSystemFontOfSize:14];
        lblTitle.textColor = [UIColor color333333];
        [header addSubview:lblTitle];
        
        InstagramFilterView *filterView = [[InstagramFilterView alloc] init];
        filterView.isFromHome = YES;
        filterView.filterArr = self.filterArr;
        if (self.filterArr.count) {
            filterView.frame = CGRectMake(0, lblTitle.endPointY+16, DeviceWidth, 120);
        }else{
            filterView.hidden = YES;
        }
        __weak typeof(self) weakSelf = self;
        filterView.chooseFilter = ^(InstagramFilterModel *filterModel) {
            [weakSelf filterTagTapped:filterModel];
        };
        [header addSubview:filterView];
        
        reusableView = header;
    }else if (kind == UICollectionElementKindSectionFooter){
        UICollectionReusableView *footerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"instagramFooter" forIndexPath:indexPath];
        
        tagCollectionView = [[TTGTextTagCollectionView alloc] init];
        tagCollectionView.delegate = self;
        tagCollectionView.defaultConfig.tagTextFont = [UIFont systemFontOfSize:12];
        tagCollectionView.scrollDirection = TTGTagCollectionScrollDirectionHorizontal;
        tagCollectionView.showsHorizontalScrollIndicator = NO;
        tagCollectionView.alignment = TTGTagCollectionAlignmentLeft;
        tagCollectionView.defaultConfig.tagBackgroundColor = [UIColor whiteColor];
        tagCollectionView.defaultConfig.tagSelectedBackgroundColor = [UIColor whiteColor];
        tagCollectionView.defaultConfig.tagTextColor = [UIColor colorWithHexString:@"D20000"];
        tagCollectionView.defaultConfig.tagSelectedTextColor = [UIColor colorWithHexString:@"D20000"];
        tagCollectionView.contentInset = UIEdgeInsetsMake(5, 15, 5, 5);
        tagCollectionView.defaultConfig.tagShadowOffset = CGSizeMake(0, 0);
        tagCollectionView.defaultConfig.tagShadowRadius = 0;
        tagCollectionView.defaultConfig.tagShadowOpacity = 0;
        
        [tagCollectionView addTags:titleArr];
        [footerview addSubview:tagCollectionView];
        
        if (titleArr.count>0) {
            tagCollectionView.frame = CGRectMake(0, 2, DeviceWidth, 44);
            
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 43, DeviceWidth, 1)];
            lineView.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
            [tagCollectionView addSubview:lineView];
        }else{
            tagCollectionView.frame = CGRectMake(0, 0, DeviceWidth, 0);
        }
        
        
        UIButton *btnSeeAll = [UIButton buttonWithType:UIButtonTypeCustom];
        btnSeeAll.frame = CGRectMake(0, tagCollectionView.endPointY, DeviceWidth, 50);
        [btnSeeAll setTitle:@"See All" forState:UIControlStateNormal];
        [btnSeeAll setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
        btnSeeAll.titleLabel.font = [UIFont systemFontOfSize:12];
        [btnSeeAll bk_whenTapped:^{
            if (self.seeAllInstagram) {
                self.seeAllInstagram();
            }
        }];
        [footerview addSubview:btnSeeAll];
        
        CGSize size = [@"See All" sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(DeviceWidth, 50) lineBreakMode:NSLineBreakByWordWrapping];
        
        [btnSeeAll setImage:[UIImage imageNamed:@"ic_home_arrow"] forState:UIControlStateNormal];
        btnSeeAll.imageEdgeInsets = UIEdgeInsetsMake(0, size.width+60, 0, 0);
        
        UIView *spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, btnSeeAll.endPointY, DeviceWidth, 5)];
        spaceView.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
        [footerview addSubview:spaceView];

        reusableView = footerview;
    }
    return reusableView;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section{
    if (titleArr.count>0) {
        return CGSizeMake(DeviceWidth, 55+46);
    }else{
        return CGSizeMake(DeviceWidth, 55);
    }
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (self.filterArr.count>0) {
        return CGSizeMake(DeviceWidth, 50+120);
    }else{
        return CGSizeMake(DeviceWidth, 50);
    }
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.seeAllInstagram) {
        self.seeAllInstagram();
    }
}

- (void)textTagCollectionView:(TTGTextTagCollectionView *)textTagCollectionView didTapTag:(NSString *)tagText atIndex:(NSUInteger)index selected:(BOOL)selected{
    InstagramFilterModel *filterModel = [self.hashTagArr objectAtIndex:index];
    [self filterTagTapped:filterModel];
}

- (void)setInstagramArr:(NSArray *)instagramArr{
    _instagramArr = instagramArr;
    int lines = (int)instagramArr.count/3;
    if (instagramArr.count%3 >0) {
        lines = (int)instagramArr.count/3 +1;
    }
    CGFloat filterHeight = 0.0f;
    if (self.filterArr.count>0) {
        filterHeight = 120;
    }
    CGFloat hashTagHeight = 0.0f;
    if (titleArr.count>0) {
        hashTagHeight = 46;
    }

    instagramCollectionView.frame = CGRectMake(0, 0, DeviceWidth, (ITEM_SIZE_WIDTH+1)*lines+104+hashTagHeight+filterHeight);
   
    [self setupAutoHeightWithBottomView:instagramCollectionView bottomMargin:0];
    [instagramCollectionView reloadData];
}

- (void)setHashTagArr:(NSArray *)hashTagArr{
    _hashTagArr = hashTagArr;
    
    [titleArr removeAllObjects];
    for (InstagramFilterModel *model in hashTagArr) {
        [titleArr addObject:[NSString stringWithFormat:@"#%@",model.tag]];
    }
    
    [instagramCollectionView reloadData];
}

- (void)filterTagTapped:(InstagramFilterModel *)model{
    if (self.tappedFilterTag) {
        self.tappedFilterTag(model);
    }
}

@end
