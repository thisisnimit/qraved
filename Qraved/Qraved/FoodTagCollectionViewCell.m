//
//  FoodTagCollectionViewCell.m
//  Qraved
//
//  Created by harry on 2017/11/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "FoodTagCollectionViewCell.h"

@implementation FoodTagCollectionViewCell
{
    UIView *bgView;
    UIImageView *imgFoodTag;
    UILabel *lblTitle;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    
    
    UIImageView *imageView = [UIImageView new];

    imageView.backgroundColor = [UIColor colorWithHexString:@"#F5F5F5"];
    [self.contentView addSubview:imageView];
    
    imageView.sd_layout
    .topSpaceToView(self.contentView, 0)
    .centerXEqualToView(self.contentView)
    .widthIs(51)
    .heightEqualToWidth();
    
    imageView.layer.masksToBounds = YES;
    imageView.layer.cornerRadius = imageView.bounds.size.width/2;
    
    imgFoodTag = [UIImageView new];
    [imageView addSubview:imgFoodTag];
  
    imgFoodTag.contentMode = UIViewContentModeScaleAspectFit;
    
    imgFoodTag.sd_layout
    .centerYEqualToView(imageView)
    .centerXEqualToView(imageView)
    .widthIs(20)
    .heightIs(26);
    
    lblTitle = [UILabel new];
    lblTitle.font = [UIFont systemFontOfSize:12];
    lblTitle.textColor = [UIColor color333333];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:lblTitle];
    lblTitle.sd_layout
    .topSpaceToView(imageView, 7)
    .leftSpaceToView(self.contentView, 0)
    .widthIs(68)
    .heightIs(32);
    
    [lblTitle setNumberOfLines:2];
}

- (void)setModel:(DiscoverCategoriesModel *)model{
    _model = model;
    
    lblTitle.text = model.name;
    [imgFoodTag sd_setImageWithURL:[NSURL URLWithString:model.icon_image_url] completed:nil];
}

@end
