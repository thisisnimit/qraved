//
//  V2_LocationCollectionViewCell.h
//  Qraved
//
//  Created by harry on 2017/6/16.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_LocationModel.h"
@interface V2_LocationCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *locationImageView;
@property (nonatomic, strong) UILabel *lblLocationName;
@property (nonatomic, strong) UILabel *lblLandmarkTitle;
@property (nonatomic, copy) V2_LocationModel *locationModel;

@end
