//
//  ContributionHandler.h
//  Qraved
//
//  Created by harry on 2017/7/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContributionHandler : NSObject

+(void)getContribution:(NSDictionary *)params andBlock:(void(^)(id contributionArr))successBlock andError:(void(^)(NSError *error))errorBlock;

@end
