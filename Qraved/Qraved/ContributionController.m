//
//  ContributionController.m
//  Qraved
//
//  Created by Tek Yin on 5/23/17.
//  Copyright © 2017 Imaginato. All rights reserved.
//

#import "ContributionController.h"
#import "ContributionRestaurantCell.h"
#import "ContributionActionCell.h"
#import "ContributionHandler.h"
#import "ContributionListModel.h"
#import "HomeService.h"
#import "ReviewPublishViewController.h"
#import "ReviewPublishHandler.h"
#import <Photos/Photos.h>
#import "ContributionFlowLayout.h"
#import "LoadingView.h"

@interface ContributionController ()<UICollectionViewDelegateFlowLayout, UICollectionViewDataSource,UICollectionViewDelegate,ZYQAssetPickerControllerDelegate,UINavigationControllerDelegate>
{
    UILabel *lblCardName;
    UICollectionView *contributionCollectionView;
    UIPageControl *pageControl;
    NSMutableArray *contributionArray;
    UIImage *userImage;
    BOOL isEndRefresh;
}

@end

@implementation ContributionController {
    CGFloat itemWidth;
    CGFloat itemHeight;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    
    [self loadData];
    [self loadMainUI];
    [self getUserNewImage];
    //[self requestData];
}

- (void)getUserNewImage{
    
    if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusNotDetermined) {
        
        [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
            
            if (status == PHAuthorizationStatusAuthorized) {
                PHFetchOptions *options = [[PHFetchOptions alloc] init];
                PHFetchResult *assetsFetchResults = [PHAsset fetchAssetsWithOptions:options];
                PHAsset *phasset = [assetsFetchResults lastObject];
                PHCachingImageManager *imageManager = [[PHCachingImageManager alloc] init];
                [imageManager requestImageForAsset:phasset targetSize:CGSizeMake(DeviceWidth-90, (DeviceWidth-90)*.55) contentMode:PHImageContentModeAspectFill options:nil resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
                    
                    userImage = result;
                    [contributionCollectionView reloadData];
                }];
            }
        }];
    }
    
//
//    if ([PHPhotoLibrary authorizationStatus] == PHAuthorizationStatusAuthorized) {
//
//        PHCachingImageManager *imageManager = [[PHCachingImageManager alloc] init];
//        [imageManager requestImageForAsset:phasset targetSize:CGSizeMake(DeviceWidth-90, (DeviceWidth-90)*.55) contentMode:PHImageContentModeAspectFill options:nil resultHandler:^(UIImage * _Nullable result, NSDictionary * _Nullable info) {
//
//            userImage = result;
//            // 这里的 result 就是用户最新的image
//        }];
//    }


}


- (void)loadData{
    isEndRefresh = NO;
    contributionArray = [NSMutableArray array];
}

- (void)requestData{
    [[LoadingView sharedLoadingView] startLoading];;
    [HomeService getContribution:nil andBlock:^(id contributionArr) {
        isEndRefresh = YES;
        [[LoadingView sharedLoadingView] stopLoading];
        lblCardName.hidden = NO;
        [contributionArray addObjectsFromArray:contributionArr];
        [contributionCollectionView reloadData];
        
    } andError:^(NSError *error) {
        isEndRefresh = YES;
        lblCardName.hidden = NO;
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

- (void)loadMainUI{
    
    self.view.backgroundColor = [UIColor colorEBEBEB];
    
    lblCardName = [[UILabel alloc] initWithFrame:CGRectMake(60, 60, DeviceWidth-120, 40)];
    lblCardName.text = @"How do you want to share your dining experience ?";
    lblCardName.font = [UIFont boldSystemFontOfSize:14];
    lblCardName.textAlignment = NSTextAlignmentCenter;
    lblCardName.textColor = [UIColor color333333];
    lblCardName.numberOfLines = 2;
    //lblCardName.hidden = YES;
    [self.view addSubview:lblCardName];
    
    
    ContributionFlowLayout *layout = [[ContributionFlowLayout alloc] init];
    contributionCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 109, DeviceWidth, 430) collectionViewLayout:layout];

    contributionCollectionView.delegate = self;
    contributionCollectionView.dataSource = self;
    contributionCollectionView.backgroundColor = [UIColor colorEBEBEB];
    contributionCollectionView.showsHorizontalScrollIndicator = NO;
    //contributionCollectionView.pagingEnabled = YES;
    [self.view addSubview:contributionCollectionView];
    
    [contributionCollectionView registerNib:[UINib nibWithNibName:@"ContributionActionCell" bundle:nil] forCellWithReuseIdentifier:@"ContributionActionCell"];
    
   // [contributionCollectionView registerNib:[UINib nibWithNibName:@"ContributionRestaurantCell" bundle:nil] forCellWithReuseIdentifier:@"ContributionRestaurantCell"];
    
//    pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, contributionCollectionView.endPointY+40, DeviceWidth, 7)];
//    pageControl.currentPageIndicatorTintColor = [UIColor color333333];
//    pageControl.pageIndicatorTintColor = [UIColor colorWithHexString:@"C1C1C1"];
//    [self.view addSubview:pageControl];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self updatePageIndicator];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [[self rdv_tabBarController] setTabBarHidden:NO animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
   
//    if (isEndRefresh) {
//        pageControl.numberOfPages = contributionArray.count+1;
//        return contributionArray.count+1;
//    }else{
//        return 0;
//    }
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    //if (indexPath.row == 0) {
        static NSString        *identifier = @"ContributionActionCell";
        ContributionActionCell *cell       = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        if (cell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:identifier owner:self options:nil];
            if ([nib count] > 0) {
                cell = (ContributionActionCell *) nib[0];
            }
        }
        cell.userImage = userImage;
        cell.takePhoto = ^{
            [self openCamera];
        };
        
        cell.uploadPhoto = ^{
            [self uploadPhoto];
        };
        
        cell.searchRestaurant= ^{
            [self writeReview];
        };
        
        cell.backgroundColor = [UIColor whiteColor];
        return cell;
   // }
//    else {
//        static NSString            *identifier = @"ContributionRestaurantCell";
//        ContributionRestaurantCell *cell       = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
//        if (cell == nil) {
//            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:identifier owner:self options:nil];
//            if ([nib count] > 0) {
//                cell = (ContributionRestaurantCell *) nib[0];
//            }
//        }
//        cell.backgroundColor = [UIColor whiteColor];
//        ContributionListModel      *info       = [contributionArray objectAtIndex:indexPath.row-1];
//        [cell resetState];
//        cell.contributionModel = info;
//        cell.onFadeOut     = ^(ContributionRestaurantCell *sender,int ratingCount) {
//            if (ratingCount>=0) {
//                [self postRating:info andRatingCount:ratingCount];
//            }
//            
//            [contributionArray removeObject:info];
//            [contributionCollectionView performBatchUpdates:^{
//                [contributionCollectionView deleteItemsAtIndexPaths:@[indexPath]];
//            }
//                                       completion:^(BOOL finished) {
//                                           [contributionCollectionView reloadData];
//                                           [self updatePageIndicator];
//                                       }];
//        };
//        cell.onWriteReview = ^(IMGRestaurant *restaurant, int ratingCount) {
//            ReviewPublishViewController *rpvc = [[ReviewPublishViewController alloc] initWithRestaurant:restaurant andOverallRating:ratingCount+1 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
//            //rpvc.isFromDetail = YES;
////            rpvc.isUpdateReview = YES;
////            rpvc.isEditReview = YES;
//            //rpvc.amplitudeType = @"RDP";
//            //rpvc.isEdit = YES;
//            [self.navigationController presentViewController:[[UINavigationController alloc] initWithRootViewController:rpvc] animated:YES completion:^{
//                
//            }];
//        };
//        return cell;
//    }
}

- (void)openCamera{
    [CommonMethod checkCameraStateWithViewController:self andCallBackBlock:^{
        CameraViewController *cameraVC = [[CameraViewController alloc]init];
        cameraVC.isFromUploadPhoto = YES;
        cameraVC.isCanSearch = YES;
        cameraVC.isFromContribution = YES;
        cameraVC.isFromHotkey = YES;
        [self.navigationController presentViewController:cameraVC animated:YES completion:nil];
    }];
}

- (void)uploadPhoto{
    
    ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] initWithRestaurantTitle:nil andIsFromUploadPhoto:YES andIsUploadMenuPhoto:NO andCurrentRestaurantId:nil];
    picker.publishViewController = self;
    picker.maximumNumberOfSelection = 9;
    picker.assetsFilter = [ALAssetsFilter allPhotos];
    picker.showEmptyGroups=NO;
    picker.delegate = self;
    picker.isFromUploadPhoto = YES;

    picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
            NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
            return duration >= 5;
        } else {
            return YES;
        }
    }];
    [self.navigationController presentViewController:picker animated:YES completion:nil];
}

- (void)writeReview{
    ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc] init];
    publishReviewVC.isFromHotkey = YES;
    publishReviewVC.amplitudeType = @"Hot Key";
    publishReviewVC.isCanSearch = YES;
    publishReviewVC.restaurantArr = contributionArray;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:publishReviewVC];
    [self.navigationController presentViewController:navigationController animated:YES completion:nil];
}

- (void)updatePageIndicator {
    CGFloat   pageWidth      = DeviceWidth-90; // you need to have a **iVar** with getter for scrollView
    float     fractionalPage = contributionCollectionView.contentOffset.x / pageWidth;
    NSInteger page           = lround(fractionalPage);
    pageControl.currentPage = page;
    
    if (page>0) {
        lblCardName.text = @"Do you love these restaurants ?";
    }else{
        lblCardName.text = @"How do you want to share your dining experience ?";
    }

}

-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    
    NSMutableArray *photosArrM = [[NSMutableArray alloc] init];
    
    for (int i=0; i<assets.count; i++) {
        ALAsset *asset=assets[i];
        UIImageView *imgview=[[UIImageView alloc] init];
        imgview.contentMode=UIViewContentModeScaleAspectFill;
        imgview.clipsToBounds=YES;
        UIImage *tempImg=[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage];
        // [imgview setImage:tempImg];
        IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
        photo.image = tempImg;
        photo.photoUrl = [NSString stringWithFormat:@"%@",[asset valueForProperty:ALAssetPropertyAssetURL]];
        //NSMutableDictionary *infoDic = [[NSMutableDictionary alloc] initWithDictionary:asset.defaultRepresentation.metadata];
        
        CLLocation *current = [asset valueForProperty:ALAssetPropertyLocation];
        NSNumber *currentLatitude=@(current.coordinate.latitude);
        NSNumber *currentLongtitue=@(current.coordinate.longitude);
        if (![currentLatitude isEqual:@0]) {
            photo.latitude = [NSString stringWithFormat:@"%@",currentLatitude];
        }
        if (![currentLongtitue isEqual:@0]) {
            photo.longitude = [NSString stringWithFormat:@"%@",currentLongtitue];
        }
        
        
     //   photo.latitude = [[infoDic objectForKey:@"{GPS}"] objectForKey:@"Latitude"];
      //  photo.longitude = [[infoDic objectForKey:@"{GPS}"] objectForKey:@"Longitude"];
        NSLog(@"%@-----%@",currentLatitude, currentLongtitue);
        
        [photosArrM addObject:photo];
    }
    
    [picker dismissViewControllerAnimated:NO completion:^{
        ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc]initWithRestaurant:nil andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
        publishReviewVC.isFromHotkey = YES;
        publishReviewVC.amplitudeType = @"Hot Key";
        publishReviewVC.isUploadPhoto = YES;
        publishReviewVC.photosArrM = [NSMutableArray arrayWithArray:photosArrM];
        publishReviewVC.isCanSearch = YES;
        publishReviewVC.isFromOnboard = NO;
        publishReviewVC.ifSuccessLoginClickPost = YES;
        [self.navigationController presentViewController:[[UINavigationController alloc] initWithRootViewController:publishReviewVC] animated:YES completion:nil];
        
        
    }];
}

- (void)postRating:(ContributionListModel *)model andRatingCount:(int)ratingCount{
    IMGUser *user = [IMGUser currentUser];

    int currentRating = (ratingCount + 1)*2;
    NSDictionary * parameters=@{@"dishes":@[],
                                @"t": user.token,
                                @"userId":user.userId,
                                @"restaurantId":model.restaurant_id,
                                @"rating":[NSNumber numberWithInt:currentRating],
                                @"foodScore":[NSNumber numberWithInt:currentRating],
                                @"ambianceScore":[NSNumber numberWithInt:currentRating],
                                @"serviceScore":[NSNumber numberWithInt:currentRating],
                                @"title":@"",
                                @"review":@"",
                                @"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] ,
                                @"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],
                                @"client":QRAVED_WEB_SERVICE_CLIENT,
                                @"v":QRAVED_WEB_SERVICE_VERSION,
                                @"appApiKey":QRAVED_APIKEY
                                };
    
    [ReviewPublishHandler writereviewWithParams:parameters andBlock:^(NSString *returnStatusString, NSString *exceptionMsg, NSString *userInteractCount, NSNumber *returnReviewId, NSDictionary *reviewDictionary, NSString *ratingMap) {
        
    } anderrorBlock:^(NSString *str) {
        
    }];
}

@end
