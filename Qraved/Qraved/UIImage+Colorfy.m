//
// Created by Tek Yin on 5/15/17.
// Copyright (c) 2017 Imaginato. All rights reserved.
//

#import "UIImage+Colorfy.h"


@implementation UIImage (Colorfy)

- (UIColor *)averageColor {
    // resize the image
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(100, 100), YES, 0.0);
    [self drawInRect:CGRectMake(0, 0, 100, 100)];
    UIImage *sample = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    // get the average color
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char   rgba[4];
    CGContextRef    context    = CGBitmapContextCreate(rgba, 1, 1, 8, 4, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGContextDrawImage(context, CGRectMake(0, 0, 1, 1), sample.CGImage);
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    if (rgba[3] > 0) {
        CGFloat alpha      = (CGFloat) (((CGFloat) rgba[3]) / 255.0);
        CGFloat multiplier = (CGFloat) (alpha / 255.0);
        return [UIColor colorWithRed:((CGFloat) rgba[0]) * multiplier
                               green:((CGFloat) rgba[1]) * multiplier
                                blue:((CGFloat) rgba[2]) * multiplier
                               alpha:alpha];
    } else {
        return [UIColor colorWithRed:((CGFloat) rgba[0]) / 255.0f
                               green:((CGFloat) rgba[1]) / 255.0f
                                blue:((CGFloat) rgba[2]) / 255.0f
                               alpha:((CGFloat) rgba[3]) / 255.0f];
    }
}


@end