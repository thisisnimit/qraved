//
//  DistrictCell.h
//  Qraved
//
//  Created by Tek Yin on 5/4/17.
//  Copyright © 2017 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DistrictCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *uTitle;

@end
