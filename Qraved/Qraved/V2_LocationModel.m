//
//  V2_LocationModel.m
//  Qraved
//
//  Created by harry on 2017/6/15.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_LocationModel.h"

@implementation V2_LocationModel


- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"id"]) {
        self.location_id = value;
    }
}

@end
