//
//  V2_DistrictTableViewCell.h
//  Qraved
//
//  Created by harry on 2017/7/28.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_DistrictModel.h"
@interface V2_DistrictTableViewCell : UITableViewCell

@property (nonatomic, strong) V2_DistrictModel *model;
@property (nonatomic, copy) void (^addDistrict)(V2_AreaModel *model);
@property (nonatomic, copy) void (^removeDidtrict)(V2_AreaModel *model);
@property (nonatomic, strong) NSArray *selectArr;// only
@property (nonatomic, assign) CGFloat pointY;
@property (nonatomic, assign) CGFloat selectPointY;

@end
