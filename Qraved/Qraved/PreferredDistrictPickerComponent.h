//
//  PreferredDistrictPickerComponent.h
//  Qraved
//
//  Created by Tek Yin on 5/4/17.
//  Copyright © 2017 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NExpandableCollectionViewDelegate.h"
#import "CollectionViewHeaderDelegate.h"

@class NExpandableCollectionView;
@class AreaModel;

@interface PreferredDistrictPickerComponent : UIViewController <NExpandableCollectionViewDelegate, UICollectionViewDataSource, CollectionViewHeaderDelegate>
@property(weak, nonatomic) IBOutlet NExpandableCollectionView *uCollection;

@property(nonatomic, strong) NSArray<AreaModel *> *list;

+ (UINavigationController *)create;

@end
