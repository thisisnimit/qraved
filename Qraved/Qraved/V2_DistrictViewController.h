//
//  V2_DistrictViewController.h
//  Qraved
//
//  Created by harry on 2017/7/27.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"

@interface V2_DistrictViewController : BaseViewController

@property (nonatomic, copy) void(^refreshLocation)();
@property (nonatomic, strong) NSArray *locationArray;

@end
