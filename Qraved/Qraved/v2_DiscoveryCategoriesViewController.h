//
//  v2_DiscoveryCategoriesViewController.h
//  Qraved
//
//  Created by Adam.zhang on 2017/9/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"

@interface v2_DiscoveryCategoriesViewController : BaseViewController

@property (nonatomic, strong) NSArray *locationArr;

@end
