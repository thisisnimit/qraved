//
// Created by Tek Yin on 5/29/17.
// Copyright (c) 2017 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ContributionListModel : NSObject

@property(nonatomic, copy) NSString *city;
@property(nonatomic, copy) NSString *cuisine;
@property(nonatomic, copy) NSString *district;
@property(nonatomic, copy) NSString *featured_article;
@property(nonatomic, copy) NSString *latitude;
@property(nonatomic, copy) NSString *longitude;
@property(nonatomic, copy) NSString *path_original;
@property(nonatomic, copy) NSString *photo;
@property(nonatomic, copy) NSString *price_level;
@property(nonatomic, copy) NSString *rating;
@property(nonatomic, copy) NSString *rating_count;
@property(nonatomic, copy) NSNumber *restaurant_id;
@property(nonatomic, copy) NSString *restaurant_landmark;
@property(nonatomic, copy) NSString *restaurant_name;
@property(nonatomic, copy) NSString *seo_keyword;
@property(nonatomic, copy) NSString *short_desc;

@end
