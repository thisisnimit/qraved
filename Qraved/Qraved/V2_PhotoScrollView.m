//
//  V2_PhotoScrollView.m
//  Qraved
//
//  Created by harry on 2017/7/17.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_PhotoScrollView.h"
#import "V2_InstagramModel.h"
#import "UIImage+Colorfy.h"
@interface V2_PhotoScrollView ()<UIScrollViewDelegate>
{
    UIScrollView *photoScrollView;
    NSArray *photoArr;
    NSInteger currentIndex;
    BOOL isZoomed;
}
@end

@implementation V2_PhotoScrollView

- (instancetype)initWithFrame:(CGRect)frame withPhotoArray:(NSArray *)photoArray withCurrentIndex:(NSInteger)index{
    self = [super initWithFrame:frame];
    if (self) {
        
        photoArr = photoArray;
        currentIndex = index;
        
        [self createUI];
    }
    
    return self;
}

- (void)createUI{
    photoScrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
    photoScrollView.pagingEnabled = YES;
    photoScrollView.showsVerticalScrollIndicator = NO;
    photoScrollView.showsHorizontalScrollIndicator = NO;
    photoScrollView.delegate = self;
    [self addSubview:photoScrollView];
    
    for (int i = 0; i < photoArr.count; i ++) {
        V2_InstagramModel *model = [photoArr objectAtIndex:i];
        UIScrollView *bgView = [[UIScrollView alloc] initWithFrame:CGRectMake(DeviceWidth*i, 0, DeviceWidth, DeviceHeight+20)];
        bgView.delegate = self;
        bgView.backgroundColor = [UIColor blackColor];
        [photoScrollView addSubview:bgView];
        
        UIImageView *photoImageView  = [[UIImageView alloc] initWithFrame:CGRectMake(0, (DeviceHeight+20-DeviceWidth)/2, DeviceWidth, DeviceWidth)];
        photoImageView.contentMode = UIViewContentModeScaleAspectFill;
        photoImageView.clipsToBounds = YES;
        photoImageView.userInteractionEnabled = YES;
        [photoImageView sd_setImageWithURL:[NSURL URLWithString:model.standard_resolution_image] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//            bgView.backgroundColor = [photoImageView.image averageColor];
        }];

        [bgView setContentSize:CGSizeMake(DeviceWidth, DeviceWidth)];
        bgView.minimumZoomScale = 1.0;
        bgView.maximumZoomScale = 3.0;
        [bgView addSubview:photoImageView];
        
//        UITapGestureRecognizer *twoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTappedTwice:)];
//        twoTap.cancelsTouchesInView    = NO;
//        twoTap.numberOfTapsRequired    = 2;
//        twoTap.numberOfTouchesRequired = 1;
        
        UITapGestureRecognizer *oneTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
        oneTap.cancelsTouchesInView    = NO;
        oneTap.numberOfTapsRequired    = 1;
        oneTap.numberOfTouchesRequired = 1;
//        [oneTap requireGestureRecognizerToFail:twoTap];
        
        [bgView addGestureRecognizer:oneTap];
//        [bgView addGestureRecognizer:twoTap];
        
        //bgView.backgroundColor = [photoImageView.image averageColor];
        
        
        UIPanGestureRecognizer *panTap = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
      
        [self addGestureRecognizer:panTap];
        
    }

    [photoScrollView setContentSize:CGSizeMake(DeviceWidth*photoArr.count, 0)];
    [photoScrollView setContentOffset:CGPointMake(DeviceWidth*currentIndex, 0)];
}
- (void)handlePan:(UIPanGestureRecognizer *)recognizer{
    if (self.panTapped) {
        self.panTapped(recognizer);
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)gestureRecognizer {
    CGPoint translation = [gestureRecognizer translationInView:gestureRecognizer.view];
    NSLog(@"translation == %f", translation.x);
    //    if (translation.x >= 0) {
    //        return NO;
    //    }
    return YES;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    if (scrollView != photoScrollView) {
        for (UIView *view in scrollView.subviews) {
            return view;
        }
    }

    return nil;
}
- (void)scrollViewDidZoom:(UIScrollView *)scrollView {

}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView==photoScrollView) {
        NSInteger index = scrollView.contentOffset.x / DeviceWidth;
        if (currentIndex != index) {
            if (self.scrollPhoto) {
                self.scrollPhoto(index);
            }
            
            currentIndex = index;
        }

    }
    
}

- (void)imageTapped:(UITapGestureRecognizer *)tap{
//    if (self.imageTapped) {
//        self.imageTapped();
//    }
    //[self scrollViewDidScroll:photoScrollView];
    NSInteger index = photoScrollView.contentOffset.x / DeviceWidth;
//    
//    [photoScrollView setContentSize:CGSizeMake(DeviceWidth*index, 0)];
    if ((index+1) < photoArr.count) {
        [UIView animateWithDuration:0.3 animations:^{
            [photoScrollView setContentOffset:CGPointMake(DeviceWidth*(index+1), 0)];
        }];
        
        [self scrollViewDidScroll:photoScrollView];
    }
    
//        if (currentIndex != index) {
//            if (self.scrollPhoto) {
//                self.scrollPhoto(index);
//            }
//
//            currentIndex = index;
//        }

}

- (void)imageTappedTwice:(UITapGestureRecognizer *)tap{
     UIScrollView *view = tap.view;
    if (isZoomed) {
        [view setZoomScale:1.0 animated:YES];
        isZoomed = NO;
    }else{
         [view setZoomScale:3.0 animated:YES];
        isZoomed = YES;
    }
   
   
    
}

@end
