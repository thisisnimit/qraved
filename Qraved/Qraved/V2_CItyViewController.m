//
//  V2_CItyViewController.m
//  Qraved
//
//  Created by harry on 2017/8/17.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_CItyViewController.h"

@interface V2_CItyViewController ()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>  
{
    UITableView *cityTableView;
}
@end

@implementation V2_CItyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self loadMainUI];
}


- (void)loadMainUI{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(DeviceWidth-65, IMG_StatusBarHeight, 50, 44);
    [button setImage:[UIImage imageNamed:@"ic_close_location"] forState:UIControlStateNormal];
    button.imageEdgeInsets = UIEdgeInsetsMake(0, 25, 0, 0);
    [button addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    self.view.backgroundColor = [UIColor whiteColor];
    cityTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, IMG_StatusBarAndNavigationBarHeight, DeviceWidth, DeviceHeight+20-IMG_StatusBarAndNavigationBarHeight) style:UITableViewStyleGrouped];
    cityTableView.backgroundColor = [UIColor whiteColor];
    cityTableView.delegate = self;
    cityTableView.dataSource = self;
    [self.view addSubview:cityTableView];
    cityTableView.separatorStyle = UITableViewCellSeparatorStyleNone;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.cityArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *idenT = @"cityCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenT];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenT];
    }
    IMGCity *city = [self.cityArray objectAtIndex:indexPath.row];
    
    UILabel *lblCityName = [[UILabel alloc] initWithFrame:CGRectMake(17, 30, DeviceWidth-34, 20)];
    lblCityName.textColor = [UIColor color333333];
    lblCityName.font = [UIFont boldSystemFontOfSize:17];
    [cell.contentView addSubview:lblCityName];
    
    lblCityName.text = [city.name capitalizedString];
    
//    cell.textLabel.text = [city.name capitalizedString];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 96)];
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 20, DeviceWidth-30, 22)];
    lblTitle.textColor = [UIColor color333333];
    lblTitle.font = [UIFont boldSystemFontOfSize:20];
    lblTitle.text = @"Discover Other Cities";
    [headerView  addSubview:lblTitle];
    
    UILabel *lblDes = [[UILabel alloc] initWithFrame:CGRectMake(15, lblTitle.endPointY+7, DeviceWidth-30, 16)];
    lblDes.textColor = [UIColor color999999];
    lblDes.font = [UIFont systemFontOfSize:14];
    lblDes.text = @"Explore the best food and places…";
    [headerView addSubview:lblDes];
    
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied||[self nearByCheck]){
        UIButton *btnNearby = [UIButton buttonWithType:UIButtonTypeCustom];
        btnNearby.frame = CGRectMake(15, 96, DeviceWidth-30, 20);
        btnNearby.titleLabel.font = [UIFont boldSystemFontOfSize:17];
        [btnNearby setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
        btnNearby.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [btnNearby setTitle:@"Nearby" forState:UIControlStateNormal];
        [btnNearby bk_whenTapped:^{
            if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied) {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"To continue, let your device turn on location" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
                [alert show];
                
            }else{
                if (self.nearbySelected) {
                    self.nearbySelected();
                }
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }];
        CGSize size = [@"Nearby" sizeWithFont:[UIFont boldSystemFontOfSize:17] constrainedToSize:CGSizeMake(DeviceWidth-30, 20) lineBreakMode:NSLineBreakByWordWrapping];
        UIImageView *imgNearBy = [[UIImageView alloc] initWithFrame:CGRectMake(size.width+30, 99, 11, 14)];
        imgNearBy.image = [UIImage imageNamed:@"icn_city_nearby"];
        [headerView addSubview:btnNearby];
        [headerView addSubview:imgNearBy];
    }
    
    
    return headerView;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
//    if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied){
//        
//    }else{
//        return 66;
//    }
    if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied||[self nearByCheck]) {
        return 116;
    }else{
        return 66;
    }
    

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    IMGCity *city = [self.cityArray objectAtIndex:indexPath.row];
    if (self.citySelected) {
        self.citySelected(city);
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)back{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)nearByCheck{
    for (IMGCity *city in self.cityArray) {
        if (city.maxLongitude==nil||[city.maxLongitude isEqual:[NSNull null]]) {
            continue;
        }
        CGFloat maxLatitue=[city.maxLatitude floatValue];
        CGFloat maxLongtitude=[city.maxLongitude floatValue];
        CGFloat minLatitue=[city.minLatitude floatValue];
        CGFloat minLongtitude=[city.minLongitude floatValue];
        //[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"];
        NSNumber *currentLatitude=[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"];
        //[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]
        NSNumber *currentLongtitue=[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"];
        
        if (([currentLatitude floatValue]>=minLatitue&&[currentLatitude floatValue]<=maxLatitue)&&([currentLongtitue floatValue]>=minLongtitude&&[currentLongtitue floatValue]<=maxLongtitude)) {
            return YES;
            
            
        }
    }
    
    return NO;

}


@end
