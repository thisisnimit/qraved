//
//  V2_Instagram.h
//  Qraved
//
//  Created by harry on 2017/6/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_InstagramModel.h"
#import "InstagramFilterModel.h"
@interface V2_Instagram : UIView

@property (nonatomic, copy) NSArray *instagramArr;
@property (nonatomic, copy) NSArray *hashTagArr;
@property (nonatomic, copy) NSArray *filterArr;
@property (nonatomic, copy) void(^selectedInstagram)(V2_InstagramModel *model,NSInteger index);
@property (nonatomic, copy) void(^seeAllInstagram)();
@property (nonatomic, copy) void(^tappedFilterTag)(InstagramFilterModel *model);

@end
