//
//  NewFeatureViewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/9/25.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "NewFeatureViewController.h"
#define imageViewCount 3
#import "UIImage+animatedGIF.h"
#import "LoginViewController.h"
#import <SDWebImage/UIImage+GIF.h>
@interface NewFeatureViewController ()<UIScrollViewDelegate>
{
    UIScrollView *featureScrollV;
    UIPageControl *pageV;
    UIImageView  *imgGifView;
    UILabel  *lblTitle;
    UILabel  *lblDetail;
    UIButton  *btnNext;
    UIButton  *btnSkip;
    int currentIndex;
}
@end

@implementation NewFeatureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self createScrollView];
    currentIndex = 0;
}
- (void)createScrollView{
    NSArray *titleArray = @[@"Restaurants Near You",@"Get Foodie Inspirations",@"Ultimate Resto Guide"];
    NSArray *detailArray = @[@"It's 7 pm and you'd like to dine-out after a busy day. Qraved knows just the right place.",@"We'll keep you updated on trending food and restaurant information from social media.",@"From getting directions to restaurant reservations, you can do it all on Qraved."];
    NSArray *btnArray = @[@"NEXT",@"NEXT",@"NEXT"];
    
    featureScrollV = [UIScrollView new];
    featureScrollV.delegate = self;
//    featureScrollV.backgroundColor = [UIColor yellowColor];
    [self.view addSubview:featureScrollV];
    featureScrollV.sd_layout
    .topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .widthIs(DeviceWidth)
    .heightIs(DeviceHEIGHT);

    
    pageV = [UIPageControl new];
    pageV.numberOfPages=imageViewCount;
    pageV.pageIndicatorTintColor=[UIColor grayColor];
    pageV.currentPageIndicatorTintColor=[UIColor color222Red];
    [self.view addSubview:pageV];
    pageV.userInteractionEnabled=NO;
    
    
    CGFloat imageViewW= featureScrollV.width;
//    CGFloat imageViewH= featureScrollV.height;
    for (int index = 0; index <imageViewCount; index ++) {
         CGFloat imageViewX = imageViewW*index;
        //NSString *iconimage = [NSString stringWithFormat:@"Onboarding_%d",index +1];
        imgGifView = [UIImageView new];
        imgGifView.backgroundColor = [UIColor whiteColor];
        //imgGifView.image = [UIImage sd_animatedGIFNamed:iconimage];
        [featureScrollV addSubview:imgGifView];
        
        lblTitle = [UILabel new];
        lblTitle.text = titleArray[index];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.font = [UIFont systemFontOfSize:28];
        lblTitle.textColor = [UIColor color333333];
        [featureScrollV addSubview:lblTitle];
        
        lblDetail = [UILabel new];
        lblDetail.text = detailArray[index];
        lblDetail.font = [UIFont systemFontOfSize:15];
        lblDetail.textColor = [UIColor color999999];
        lblDetail.textAlignment = NSTextAlignmentCenter;
        [featureScrollV addSubview:lblDetail];
        
        btnNext = [[UIButton alloc]init];
        btnNext.backgroundColor = [UIColor color222Red];
        [btnNext setTitle:[btnArray[index] uppercaseString] forState:UIControlStateNormal];
        [btnNext setTitleColor:[UIColor whiteColor] forState: UIControlStateNormal];
        btnNext.titleLabel.font = [UIFont boldSystemFontOfSize:12];
        [featureScrollV addSubview:btnNext];
        btnNext.tag = index +10;
        [btnNext addTarget:self action:@selector(startbuttonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        btnSkip = [[UIButton alloc]init];
        [btnSkip setTitle:[@"SKIP" uppercaseString] forState:UIControlStateNormal];
        [btnSkip setTitleColor:[UIColor color999999] forState: UIControlStateNormal];
        btnSkip.titleLabel.font = [UIFont boldSystemFontOfSize:12];
        [featureScrollV addSubview:btnSkip];
        btnSkip.tag = index +10;
        [btnSkip addTarget:self action:@selector(skipButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        lblTitle.sd_layout
        .centerYIs(DeviceHEIGHT/2)
        .leftSpaceToView(featureScrollV, imageViewX)
        .widthIs(DeviceWidth)
        .autoHeightRatio(0);
        if (index==0) {
            imgGifView.sd_layout
            .bottomSpaceToView(lblTitle, 20)
            .leftSpaceToView(featureScrollV, imageViewX + DeviceWidth*0.17)
            .widthIs((DeviceWidth - DeviceWidth*0.17*2))
            .heightIs((DeviceWidth - DeviceWidth*0.17*2) * 0.97);
        }else if (index==1){
            imgGifView.sd_layout
            .bottomSpaceToView(lblTitle, 20)
            .leftSpaceToView(featureScrollV, imageViewX + DeviceWidth*0.25)
            .widthIs((DeviceWidth - DeviceWidth*0.25*2))
            .heightIs((DeviceWidth - DeviceWidth*0.25*2) * 0.95);
        }
        else if (index==2) {
            imgGifView.sd_layout
            .bottomSpaceToView(lblTitle, 20)
            .leftSpaceToView(featureScrollV, imageViewX + DeviceWidth*0.13)
            .widthIs((DeviceWidth - DeviceWidth*0.13*2))
            .heightIs((DeviceWidth - DeviceWidth*0.13*2) * 0.75);
        }
        
        
        lblDetail.sd_layout
        .topSpaceToView(lblTitle, 20)
        .leftSpaceToView(featureScrollV, imageViewX +40)
        .widthIs(DeviceWidth - 80)
        .autoHeightRatio(0);
        
        pageV.sd_layout
        .topSpaceToView(lblDetail, 50)
        .centerXEqualToView(featureScrollV)
        .widthIs(100)
        .heightIs(30);
        
        btnNext.sd_layout
        .topSpaceToView(lblDetail, 100)
        .leftSpaceToView(featureScrollV, imageViewX +(DeviceWidth/2 - 190/2))
        .widthIs(190)
        .heightIs(42);
        btnNext.layer.cornerRadius = btnNext.height/2;
        btnNext.layer.masksToBounds = YES;
        
        btnSkip.sd_layout
        .topSpaceToView(btnNext, 15)
        .leftSpaceToView(featureScrollV, imageViewX+10)
        .widthIs(DeviceWidth-20)
        .heightIs(42);
        
    }
    
    [featureScrollV setContentSize:CGSizeMake(DeviceWidth *imageViewCount, 0)];
    featureScrollV.bounces =  NO;
    featureScrollV.showsHorizontalScrollIndicator = NO;
    featureScrollV.pagingEnabled = YES;
    
    
}

- (void)startbuttonClick:(UIButton *)start{
    
    NSLog(@"next%ld",(long)start.tag);
    if (currentIndex==0) {
        [UIView animateWithDuration:0.3 animations:^{
            featureScrollV.contentOffset = CGPointMake(DeviceWidth, 0);
        }];
        currentIndex=1;
    }else if (currentIndex==1){
         [UIView animateWithDuration:0.3 animations:^{
        featureScrollV.contentOffset = CGPointMake(DeviceWidth*2, 0);
         }];
        currentIndex=2;
        
    }else{
        LoginViewController *loginVC=[[LoginViewController alloc]init];
        loginVC.isFromonboard = YES;
        UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
//        [AppDelegate ShareApp].window.rootViewController = [AppDelegate ShareApp].tabMenuViewController;
        [[AppDelegate ShareApp].window.rootViewController presentViewController:loginNavagationController animated:NO completion:^{

        }];
       
    }
}

- (void)skipButtonClick:(UIButton *)skip{
    
    NSLog(@"skip");
    LoginViewController *loginVC=[[LoginViewController alloc]init];
    loginVC.isFromonboard = YES;
    UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
//    [AppDelegate ShareApp].window.rootViewController = [AppDelegate ShareApp].tabMenuViewController;
    [[AppDelegate ShareApp].window.rootViewController presentViewController:loginNavagationController animated:NO completion:^{
        
    }];
}
#pragma mark - 设置scrollvie的代理
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //    XZLog(@"%@",NSStringFromCGPoint(self.feature.contentOffset));
    int pagecount = scrollView.contentOffset.x / DeviceWidth;
    pageV.currentPage = pagecount;
    currentIndex = pagecount;
}
@end
