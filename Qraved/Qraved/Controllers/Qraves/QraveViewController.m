//
//  QraveViewController.m
//  Qraved
//
//  Created by Admin on 8/20/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "QraveViewController.h"
#import "PostData.h"
#import "AppDelegate.h"
#import "NodataView.h"
#import "UIView+Helper.h"
#import "Tools.h"
#import "UIViewController+Helper.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "UILabel+Helper.h"
#import "Mixpanel.h"
#import "UIImage+Helper.h"

#define MaxConst 3

//#define cuisineWidth (320-2.0)/3
//#define cuisineHeight (320-2.0)/3

#define qraveWidth 305.0/2
#define qraveHeight 306.0/2+80

#define exceptImageHeight 80.5

#define kDishViewTotal 10


@interface QraveViewController ()<DishViewDelegate> {
    int currentQrave;
    
    CGFloat contentOffsetY;
    CGFloat oldContentOffsetY;
    CGFloat newContentOffsetY;
    CGFloat maxContentOffsetY;
    int fetchQravesMax;
    
    UIImageView *logoImageview;
    
    float leftCurrentHeight;
    float rightCurrentHeight;
    
    BOOL _reloading;
    EGORefreshTableHeaderView *refreshHeaderView;
    
    BOOL hasMoreData;
    BOOL isUpdate;
    
    NodataView *nodataView;
    
    UIView *blurView;
    
    UIView *tempDishView;
    
    NSInteger newDishCount;
    
    BOOL uploadSuccess;
}
//from Shine
//@property(nonatomic,assign)NSInteger paramOffset;
//@property(nonatomic,retain)NSMutableArray *dishArrays;
@property CGFloat maxCellHeight;


@end

@implementation QraveViewController

@synthesize maxCellHeight;
@synthesize isTop,params;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

-(id)initWithIsTopQraves:(BOOL)paramIsTop andParameters:(NSDictionary *)paramDictionary
{
    self=[super init];
    if(self)
    {
        self.isTop=paramIsTop;
        self.params=paramDictionary;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(imageUploadFinished:) name:@"QravedImageUpload" object:nil];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
//    [[AppDelegate ShareApp] setTabbarAppear];
    self.screenName=@"QraveList";
    hasMoreData=YES;
    
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"View Qraves List" properties:@{}];
    
    if(!nodataView)
    {
        [self addNodataView];
    }
    
    if(self.isTop)
    {
        if(!logoImageview)
        {
            UIImage *logoImage=[UIImage imageNamed:@"logo"];
            logoImageview=[[UIImageView alloc]initWithImage:logoImage];
            [logoImageview setTag:200];
            [logoImageview setFrame:CGRectMake(115, 9, logoImage.size.width, logoImage.size.height)];
        }
        [self.navigationController.navigationBar addSubview:logoImageview];
    }
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    if([AppDelegate ShareApp].qraveRefresh)
    {
        [AppDelegate ShareApp].qraveRefresh=NO;
        
        [self reloadQraveDataFromNet];
    }
    
}

-(void)addNodataView
{
    NSString *noDataMessage = [NSString stringWithFormat:NSLocalizedString(@"Qraver doesn't have any Qraves yet.", nil)];
    
    nodataView=[[NodataView alloc]initWithFrame:CGRectMake(0,(self.view.frame.size.height-44)/2-30, DeviceWidth, 80) andMessage:noDataMessage];
    
    [self.view addSubview:nodataView];
    
    [nodataView setHidden:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    [logoImageview removeFromSuperview];
    
    CGRect rect = self.navigationController.navigationBar.frame;
    float fixY =[UIDevice isIos7]?20:0;
    self.navigationController.navigationBar.frame = CGRectMake(0, fixY, rect.size.width, rect.size.height);
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    uploadSuccess=NO;
    
    [AppDelegate ShareApp].qraveRefresh=YES;
    
      self.view.backgroundColor = [UIColor whiteColor];
    
    //paramOffset=0;
    maxCellHeight=0.0;
    
    [self addScrollView];
    
    [self createRefreshView];
    
    [self loadRevealController];
}

-(void)reloadQraveDataFromNet{
    
    currentQrave=0;
    isUpdate=YES;
    fetchQravesMax = 10;
    [self.qravesList removeAllObjects];
    [self.dishViewFrameList removeAllObjects];
    [self.dishObjectArray removeAllObjects];
    [self.dishViewArray removeAllObjects];
    
    if([self.parentViewController isKindOfClass:[ProfileViewController class]]) {
        leftCurrentHeight = 0;
        rightCurrentHeight = 0;
        
    } else {
        leftCurrentHeight = 40;
        rightCurrentHeight = 40;
    }
    
    maxCellHeight=0.0;
    self.qravesList = [NSMutableArray arrayWithCapacity:0];
    self.dishViewFrameList = [NSMutableArray arrayWithCapacity:0];
    self.dishObjectArray = [NSMutableArray arrayWithArray:0];
    self.dishViewArray = [NSMutableArray arrayWithCapacity:0];
    currentQrave = 0;
    
    if ([self.qravesList count] == 0) {
        contentOffsetY = 0;
        NSMutableDictionary *requestDic = [NSMutableDictionary dictionaryWithCapacity:0];
        
        [requestDic setObject:[NSString stringWithFormat:@"%f",contentOffsetY] forKey:@"offset"];
        [requestDic setObject:[NSString stringWithFormat:@"%d",fetchQravesMax] forKey:@"max"];
        if(self.isTop)
        {
            [requestDic setObject:@"2" forKey:@"cityID"];
            if([AppDelegate ShareApp].isLogin)
            {
                [requestDic setObject:[User sharedUser].userID forKey:@"userID"];
            }
        }
        else
        {
            NSString *targetUserID=[self.params objectForKey:@"targetUserID"];
            [requestDic setObject:targetUserID forKey:@"targetUserId"];
            if([AppDelegate ShareApp].isLogin)
            {
                [requestDic setObject:[User sharedUser].userID forKey:@"userID"];
            }
        }
        
        [self requestQravesDataWithDic:requestDic];
        contentOffsetY = fetchQravesMax;
    }
    //    [scrView setContentSize:CGSizeMake(320, (qraveHeight + 5)*([self.QravesList count]/2)+80)];
}


-(void)createRefreshView
{
    CGFloat refreshY=self.isTop?0-self.mScrollView.bounds.size.height+44:0-self.mScrollView.bounds.size.height;
    refreshHeaderView=[[EGORefreshTableHeaderView alloc]initWithFrame:CGRectMake(0.0,refreshY, DeviceWidth, self.mScrollView.bounds.size.height)];
    [refreshHeaderView setTag:100];
    refreshHeaderView.backgroundColor=[UIColor clearColor];
//    refreshHeaderView.backgroundColor=[UIColor blackColor];
    refreshHeaderView.delegate=self;
    [self.mScrollView addSubview:refreshHeaderView];
}

-(void)addScrollView
{
    CGRect scrViewFrame=self.isTop?CGRectMake(0, -36, DeviceWidth,DeviceHeight):CGRectMake(0, 0, DeviceWidth,SUBVIEWHEIGHT);
    if (self.mScrollView == nil) {
        self.mScrollView = [[UIScrollView alloc]initWithFrame:scrViewFrame];
        self.mScrollView.showsVerticalScrollIndicator = NO;
        self.mScrollView.delegate = self;
        self.mScrollView.frame = scrViewFrame;
//        self.mScrollView.backgroundColor=[UIColor redColor];
        self.mScrollView.contentSize = CGSizeZero;
        [self.view addSubview:self.mScrollView];
    }
}

-(void)requestQravesDataWithDic:(NSDictionary *)requestDic {
    NSString *path=self.isTop?@"city/dishes":@"user/qraves";
    
    [[LoadingView sharedLoadingView] startLoading];
    
    isUpdate=NO;
    
    PostData *postData=[[PostData alloc]init];
    NSLog(@"requestDic=%@==%@",requestDic,path);
    [postData httpRequest:requestDic andPath:path andSuccessBlock:^(id qravedsData){
        [self getQravedsDataSuccessful:qravedsData];
        
        isUpdate=YES;
        [[LoadingView sharedLoadingView] stopLoading];
        
    } andFailedBlock:^(NSError *error){
        isUpdate=YES;
        
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

-(void)requestNewData
{
    NSMutableDictionary *requestDic = [NSMutableDictionary dictionaryWithCapacity:0];
    [requestDic setObject:[NSString stringWithFormat:@"%d",0] forKey:@"offset"];
    [requestDic setObject:[NSString stringWithFormat:@"%d",0] forKey:@"max"];
    [requestDic setObject:[self.params objectForKey:@"targetUserID"] forKey:@"targetUserId"];
    if([AppDelegate ShareApp].isLogin)
    {
        [requestDic setObject:[User sharedUser].userID forKey:@"userID"];
    }
    
     NSString *path=self.isTop? @"city/dishes":@"user/qraves";
    
    PostData *postData=[[PostData alloc]init];

    [postData httpRequest:requestDic andPath:path andSuccessBlock:^(id qravedsData){
        
        [self getNewDataSuccess:qravedsData];

    } andFailedBlock:^{
        
    }];
}

-(void)getNewDataSuccess:(NSMutableDictionary *)dic
{
    if(dic)
    {
        NSArray *dishList=[dic objectForKey:@"dishList"];
        
        id dishObject=[dishList objectAtIndex:0];
        
        Dish *dish=[[Dish alloc] init];
        [dish configByQraveJsonObject:dishObject];
        
        UIImage *tempImage=(UIImage *)[[AppDelegate ShareApp].uploadPictureDic objectForKey:@"image"];
        
        DishView *dishView= [[DishView alloc] initWithFrame:CGRectZero];
        dishView.delegate = self;
        [dishView configWithDish:dish andImage:tempImage];
        
        dishView.delegate=self;
        
        CGRect dishViewRect=tempDishView.frame;
        dishView.frame=CGRectMake(dishViewRect.origin.x, dishViewRect.origin.y, dishViewRect.size.width, dishViewRect.size.height);

        [self.mScrollView addSubview:dishView];
        
//        [AppDelegate ShareApp].uploadPictureDic=nil;
        
        uploadSuccess=YES;
    }
}

-(void)imageUploadFinished:(NSNotification *)notification
{
    [self requestNewData];
}

- (void)addJustAddQravedDishView {
    
    if([[[AppDelegate ShareApp].uploadPictureDic objectForKey:@"isUpload"] boolValue])
    {
        NSLog(@"addJustAddQravedDishView");
        [nodataView setHidden:YES];
        self.mScrollView.backgroundColor=[UIColor waterFallBgColor];
        
        RestaurantInfo *restaurantInfo=(RestaurantInfo *)[[AppDelegate ShareApp].uploadPictureDic objectForKey:@"restaurant"];
        
        Dish *dish=(Dish *)[[AppDelegate ShareApp].uploadPictureDic objectForKey:@"dish"];
        
//        NSString *description=[[AppDelegate ShareApp].uploadPictureDic objectForKey:@"description"];
        UIImage *tempImage=(UIImage *)[[AppDelegate ShareApp].uploadPictureDic objectForKey:@"image"];
        
        CGFloat taregetHeight=tempImage.size.height;
        CGFloat tempDishViewHeight=tempImage.size.height;
        
        UIImageView *tempImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, qraveWidth, taregetHeight)];
        [tempImageView setImage:tempImage];
        
        UIView *qravedView=[[UIView alloc]initWithFrame:CGRectMake(0, tempImageView.endPointY-3, 152, 25)];
        [qravedView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"qrave-bar.png"]]];
        
        UIImage *qraveImage=[UIImage imageNamed:@"qraved-icon"];
        //Qraved it
        
        UIButton *qravedBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        CGFloat qraveBarHeight=25;
        [qravedBtn setBackgroundImage:qraveImage forState:UIControlStateNormal];
        [qravedView addSubview:qravedBtn];
        
        UIImage *hLineImage=[UIImage imageNamed:@"m_qraved_bj_x.png"];
        UIImageView *hLineImageeView=[[UIImageView alloc]init];
        [hLineImageeView setImage:hLineImage];
        [qravedView addSubview:hLineImageeView];
        
        //Qraved count
        UILabel *qravedCountLabel=[[Label alloc] initCountLabel];
        [qravedCountLabel setText:@"1"];
        [qravedView addSubview:qravedCountLabel];
        
        UIImage *smallQraveImage=[UIImage imageNamed:@"qrave_small.png"];
        UIImageView *qravedImageView=[[UIImageView alloc] init];
        [qravedImageView setImage:smallQraveImage];
        [qravedView addSubview:qravedImageView];
        
        //comments count
        Label *commentsCountLabel=[[Label alloc] initCountLabel];
        [commentsCountLabel setText:@"1"];
        [qravedView addSubview:commentsCountLabel];
        
        //Comments countImage
        UIImage *commentImage=[UIImage imageNamed:@"comment_small.png"];
        UIImageView *commentImageView=[[UIImageView alloc] init];
        [commentImageView setImage:commentImage];
        [qravedView addSubview:commentImageView];
        
        [qravedBtn setFrame:CGRectMake(5, (qraveBarHeight-qraveImage.size.height/2)/2, qraveImage.size.width/2, qraveImage.size.height/2)];
        hLineImageeView.frame =CGRectMake(qravedBtn.endPointX+5, 0, hLineImage.size.width, qraveBarHeight);
        CGFloat elimenXpoint=[Tools viewEndPointX:hLineImageeView]+CELL_CONTENT_OFFSET;
        qravedImageView.frame=CGRectMake(elimenXpoint,(qraveBarHeight-smallQraveImage.size.height/2)/2, smallQraveImage.size.width/2, smallQraveImage.size.height/2);
        [qravedCountLabel setFrame:CGRectMake([Tools viewEndPointX:qravedImageView]+CELL_CONTENT_OFFSET,(qraveBarHeight-12)/2, 15, 12)];
        commentImageView.frame =CGRectMake([Tools viewEndPointX:qravedCountLabel]+CELL_CONTENT_OFFSET, (qraveBarHeight-commentImage.size.height/2)/2, commentImage.size.width/2, commentImage.size.height/2);
        [commentsCountLabel setFrame:CGRectMake([Tools viewEndPointX:commentImageView]+CELL_CONTENT_OFFSET, (qraveBarHeight-12)/2, 15, 12)];
        
        //title label
        Label *titleLabel=[[Label alloc] initTitleCaptionLabel];
        [titleLabel setText:dish.title];
        [titleLabel setFrame:CGRectMake(5, [Tools viewEndPointY:qravedView] + 5, 145.0, titleLabel.expectedHeight)];
        [titleLabel sizeToFit];
        
        UILabel *subTitleLabel=[[Label alloc] initTitleCaptionBoldLabel];
        [subTitleLabel setTextColor:[UIColor darkGrayColor]];
        [subTitleLabel setTextAlignment:NSTextAlignmentLeft];
        [subTitleLabel setText:[NSString stringWithFormat:@"at %@",[Tools returnNameString:restaurantInfo.title]]];
        [subTitleLabel setFrame:CGRectMake(CELL_CONTENT_OFFSET, titleLabel.endPointY+3, qraveWidth-CELL_CONTENT_OFFSET*2, subTitleLabel.expectedHeight)];
        [subTitleLabel sizeToFit];
        
        tempDishViewHeight=[Tools viewEndPointY:subTitleLabel]+CELL_CONTENT_OFFSET+4;
        
        tempDishView=[[UIView alloc]init];
        //        [tempDishView setBackgroundColor:[UIColor grayColor]];
        tempDishView.frame = CGRectMake(5, leftCurrentHeight, qraveWidth, tempDishViewHeight);
        tempDishView.layer.borderColor = [UIColor colorWithHue:(DEFAULT_BORDER_SAT/360.0)
                                                    saturation:DEFAULT_BORDER_SAT
                                                    brightness:DEFAULT_BORDER_BRI
                                                         alpha:1.0].CGColor;
        tempDishView.layer.borderWidth = 0.8;
        tempDishView.layer.cornerRadius=3;
        tempDishView.layer.masksToBounds=YES;
        
        UIImageView *shadowView=[[UIImageView alloc]initWithFrame:CGRectMake(0, tempDishViewHeight-8, 305.0/2, 8)];
        [shadowView setImage:[UIImage imageWithColor:[UIColor colorE4E4E4] size:CGSizeMake(DeviceWidth, 8)]];
        [tempDishView addSubview:tempImageView];
        [tempDishView addSubview:qravedView];
        [tempDishView addSubview:titleLabel];
        [tempDishView addSubview:subTitleLabel];
        [tempDishView addSubview:shadowView];
        
        leftCurrentHeight +=tempDishViewHeight+5;
        //添加模糊效果
        blurView=[[UIView alloc] init];
        CGRect tempViewRect=tempImageView.frame;
        [blurView setFrame:CGRectMake(0, 0, tempViewRect.size.width, tempViewRect.size.height)];
        [blurView setBackgroundColor:[UIColor whiteColor]];
        [blurView setAlpha:0.5];
        [tempDishView addSubview:blurView];
        
        [UIView animateWithDuration:20 animations:^{
            [blurView setFrame:CGRectMake(0, 0, tempViewRect.size.width, tempViewRect.size.height/10)];
        }
                         completion:^(BOOL finished) {

        }];
        
        newDishCount+=1;
        
        [self.mScrollView addSubview:tempDishView];
    }
}


-(void)getQravedsDataSuccessful:(id)qravesData {
    if(currentQrave==0)
    {
        NSArray *viewsToRemove = [self.mScrollView subviews];
        for (UIView *v in viewsToRemove)
        {
            if(v.tag!=100)
            {
                [v removeFromSuperview];
            }
        }
    }
    
    [self addJustAddQravedDishView];
    if(qravesData) {
        NSArray *dishList=[qravesData objectForKey:@"dishList"];
        NSLog(@"dishListcount=%d",dishList.count);
        [self.qravesList addObjectsFromArray:dishList];
        //        if(dishList.count<fetchQravesMax)
        //        {
        //            hasMoreData=NO;
        //        }
        
        if(dishList.count>0)
        {
            [nodataView setHidden:YES];
            
            DLog(@"dishList count is %d",self.qravesList.count);
            
            if(uploadSuccess)
            {
                currentQrave+=newDishCount;
                newDishCount=0;
            }
            NSLog(@"ccc1==%d",currentQrave);
            for(int i=currentQrave;i<self.qravesList.count;i++)
            {
//                int j=currentQrave==0?i:i-1;
                id dishObject=[self.qravesList objectAtIndex:(i)];
                
                Dish *dish=[[Dish alloc] init];
                [dish configByQraveJsonObject:dishObject];
                NSLog(@"iii=%@==%d",dish.imageUrl,i);
                
                DishView *dishView= [self getObjFromReuseArray:dish];
                dishView.delegate=self;
                
                if (i == 0 && ![[[AppDelegate ShareApp].uploadPictureDic objectForKey:@"isUpload"] boolValue]) {
                    dishView.frame = CGRectMake(WATERFALL_SPACING_WIDTH, leftCurrentHeight, qraveWidth, dishView.viewHeight);
                    leftCurrentHeight += (int)dishView.frame.size.height+WATERFALL_SPACING_HEIGHT;
                    
                } else {
                    if (leftCurrentHeight >= rightCurrentHeight) {
                        dishView.frame = CGRectMake(WATERFALL_SPACING_WIDTH*2+qraveWidth, rightCurrentHeight, qraveWidth, dishView.viewHeight);
                        rightCurrentHeight += (int)dishView.frame.size.height+WATERFALL_SPACING_HEIGHT;
                    } else {
                        
                        dishView.frame = CGRectMake(WATERFALL_SPACING_WIDTH, leftCurrentHeight, qraveWidth, dishView.viewHeight);
                        leftCurrentHeight += (int)dishView.frame.size.height+WATERFALL_SPACING_HEIGHT;
                    }
                }
                
                [self.dishObjectArray addObject:dish];

                float higherHeight = (leftCurrentHeight > rightCurrentHeight) ? leftCurrentHeight:rightCurrentHeight;
                
                [self.mScrollView setContentSize:CGSizeMake(320, higherHeight)];
                currentQrave = [self.qravesList count];
                currentQrave+=newDishCount;
                
                [self.mScrollView addSubview:dishView];
                //                [self.dishViewList addObject:dishView];
            }
            NSLog(@"ccc2==%d",currentQrave);
        }
        
        if(self.qravesList.count==0)
        {
            [nodataView setHidden:NO];
        }
        else
        {
            self.mScrollView.backgroundColor=[UIColor waterFallBgColor];
        }
    }
}


- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    contentOffsetY = scrollView.contentOffset.y;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {

    if(isUpdate)
    {
        if ((int)scrollView.contentSize.height - (int)scrollView.frame.size.height < ((int)scrollView.contentOffset.y * 2)) {  //load more when reach within 3x screen height left
            NSString *off = [NSString stringWithFormat:@"%d",[self.qravesList count]];
            NSDictionary *requestDic;
            if(self.isTop)
            {
                requestDic= @{@"cityID":@"2",@"offset":off,@"max":[NSString stringWithFormat:@"%d", fetchQravesMax]};
            }
            else
            {
                NSString *targetUserID=[self.params objectForKey:@"targetUserID"];
                requestDic = [NSDictionary dictionaryWithObjectsAndKeys:targetUserID,@"targetUserId",off,@"offset",[NSString stringWithFormat:@"%d",fetchQravesMax],@"max",[User sharedUser].userID,@"userID",nil];
            }
            [self requestQravesDataWithDic:requestDic];
        }
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    
//    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    if(self.isTop)
    {
        if (scrollView.dragging) {
            if ((scrollView.contentOffset.y - contentOffsetY) > 30.0f) { // 向上拖拽
//                [app setTabbarHidden];
                
                if (self.mScrollView.frame.origin.y!=-44) {
                    self.mScrollView.frame = CGRectMake(0, -44, 320, [UIScreen mainScreen].bounds.size.height - 20);
                }
                
                __block CGRect rect = self.navigationController.navigationBar.frame;
                [UIView animateWithDuration:0.25 animations:^{
                    
                    rect.origin.y -= rect.size.height;
                    self.navigationController.navigationBar.frame = rect;
                }];
            } else if ((contentOffsetY - scrollView.contentOffset.y) > 10.0f) { // 向下拖拽
//                [app setTabbarAppear];
                
                if (self.mScrollView.frame.origin.y!=-36) {
                    self.mScrollView.frame = CGRectMake(0, -36, 320, [UIScreen mainScreen].bounds.size.height - 20);
                }
                
                
                __block CGRect rect = self.navigationController.navigationBar.frame;
                rect.origin.y = 0;
                if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
                    rect.origin.y = 20;
                }
                [UIView animateWithDuration:0.25 animations:^{
                    self.navigationController.navigationBar.frame = rect;
                }];
                
            }
        }
    }
    
    
    
}
-(void)addObjToReuseArray:(DishView *)viewObj{
    if (!self.dishViewReuseArray) {
        self.dishViewReuseArray = [NSMutableArray arrayWithCapacity:0];
    }
    //[viewObj cleanSelf];
    [self.dishViewReuseArray addObject:viewObj];
}
-(DishView*)getObjFromReuseArray:(Dish*)dish{
   
    
    DishView *dishView= [[DishView alloc] initWithFrame:CGRectZero];
    dishView.delegate = self;
    [dishView configWithDish:dish andImage:nil];
    return dishView;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
	
	[refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
	
	return _reloading; // should return if data source model is reloading
}


#pragma mark refreshViewDelegate
-(void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view
{
    hasMoreData = YES;
    [self reloadQraveDataFromNet];
    [self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:2.0];
    
}

- (void)reloadTableViewDataSource{
	//  should be calling your tableviews data source model to reload
	//  put here just for demo
	_reloading = YES;
	
}

- (void)doneLoadingTableViewData{
	_reloading = NO;
	[refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.mScrollView];
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view{
	
	return [NSDate date]; // should return date data source was last changed
	
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView {
  
    __block CGRect rect = self.navigationController.navigationBar.frame;
    rect.origin.y = 0;
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        rect.origin.y = 20;
    }
    [UIView animateWithDuration:0.25 animations:^{
        self.navigationController.navigationBar.frame = rect;
    }];
    
    return YES;
}



-(void)pushQraveDetail:(UIImage *)image originFrame:(CGRect)rect andDish:(Dish *)dish
{
    
    QraveDetailViewController *qraveDetail=[[QraveDetailViewController alloc] initWithDish:dish];
    
    [self.navigationController pushViewController:qraveDetail animated:YES];
}

@end
