//
//  CommentsViewController.m
//  Qraved
//
//  Created by Admin on 8/19/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "CommentsViewController.h"
#import "PostData.h"
#import "CommentCell.h"
#import "Consts.h"
#import "NavigationBarButtonItem.h"
#import "AppDelegate.h"
#import "UIButton+Style.h"

@interface CommentsViewController ()

@end

@implementation CommentsViewController
@synthesize restaurantId;
@synthesize myDishId;


-(id)initWithDishId:(NSString *)dishId {
    self = [super init];
    if (self) {
        self.myDishId = dishId;
        NSDictionary *requestDic = @{@"offset":@"0",@"max":@"10",@"dishID":dishId};
        [self requestCommentsDataWithDic:requestDic];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NavigationBarButtonItem *navigationBarLeftButton = [NavigationBarButtonItem barItemWithImage:[UIImage imageNamed:NavigationBackImage] andTitle:@"" target:self action:@selector(back) width:43.0f];
    self.navigationItem.leftBarButtonItem = navigationBarLeftButton;
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    CGRect rect = [UIScreen mainScreen].bounds;
    
    UILabel *headLab = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 300, 30)];
    headLab.text = @"Check All Updates";
    [self.view addSubview:headLab];
    headLab.backgroundColor =[UIColor clearColor];
    headLab.textColor = [UIColor grayColor];
    
    commentBac = [[UIView alloc]initWithFrame:CGRectMake(0, rect.size.height-112, 320, 50)];
    UIImageView *backImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    backImageView.image = [UIImage imageNamed:@"comment_bg.png"];
    [commentBac addSubview:backImageView];
    commentField = [[UITextField alloc]initWithFrame:CGRectMake(10, 10, 240, 30)];

    commentField.delegate = self;
    commentField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    commentField.returnKeyType = UIReturnKeyDone;
    [commentBac addSubview:commentField];
    [self.view addSubview:commentBac];
    
    
    UIButton *postBtn = [[UIButton alloc] initHighlightButton];
   
    
    postBtn.titleLabel.text = @"Send";
    postBtn.frame = CGRectMake(260, 13, 50, 24);
    [commentBac addSubview:postBtn];
    [postBtn addTarget:self action:@selector(postComment:) forControlEvents:UIControlEventTouchDown];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil];
    
}

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)postComment:(UIButton *)btn {
    NSString *userId =  [[[NSUserDefaults standardUserDefaults]objectForKey:@"userID"]stringValue];
    NSString *Facebktoken = [[NSUserDefaults standardUserDefaults]objectForKey:@"token"];
    NSString *commentStr = commentField.text;
    NSString *trimmedString = [commentStr stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    if (!trimmedString.length==0) {
        NSString *urlString = [NSString stringWithFormat:@"%@dish/comment/write?userID=%@&t=%@&dishID=%@&restaurantID=%@&comment=%@&status=1",QRAVED_WEB_SERVICE_SERVER,userId,Facebktoken,self.myDishId,self.restaurantId,commentStr];
        NSURL *url = [NSURL URLWithString:urlString];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];
        [request setHTTPMethod:@"POST"];
//        NSData *received = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
//        NSString *str1 = [[NSString alloc]initWithData:received encoding:NSUTF8StringEncoding];
    }
    

    
    
   
    
    [commentField resignFirstResponder];
}

- (void) keyboardWasShown:(NSNotification *) notif{
    CGRect rect = [UIScreen mainScreen].bounds;
    
    NSDictionary *info = [notif userInfo];
    
    NSValue *value = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGSize keyboardSize = [value CGRectValue].size;
    
   
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    commentBac.frame = CGRectMake(0, rect.size.height - keyboardSize.height - 112, 320, 50);
    [self.view bringSubviewToFront:commentBac];
    [UIView commitAnimations];
}

-(void)getCommentsDataSuccessful:(id)qravesData {
    if(qravesData)
    {
        NSArray *commentsDicArray=[qravesData objectForKey:@"commentList"];
        commentsArr = [[NSMutableArray alloc]initWithArray:commentsDicArray];
        NSLog(@"[commentsArr count] ========== %d",[commentsArr count]);
        NSLog(@"commentsArr ======== %@",commentsArr);
        
        CGRect rect = [UIScreen mainScreen].bounds;
        commentsTab = [[UITableView alloc]initWithFrame:CGRectMake(0, 30, 320, rect.size.height-140)];
        commentsTab.separatorStyle = UITableViewCellSeparatorStyleNone;
        commentsTab.dataSource = self;
        commentsTab.delegate = self;
        [self.view addSubview:commentsTab];
        //        [commentsTab reloadData];
    }
}


-(void)requestCommentsDataWithDic:(NSDictionary *)requestDic {
    PostData *postData=[[PostData alloc]init];
        
        [postData httpRequest:requestDic andPath:@"dish/comments" andSuccessBlock:^(id qravedsData){
            //
            NSLog(@"cuisinesData ====== %@",qravedsData);
            [self getCommentsDataSuccessful:qravedsData];
            
        } andFailedBlock:^(NSError *error){
            
            NSLog(@"failed..");
        }];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [commentsArr count];
}


- (CommentCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    CommentCell *cell = (CommentCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[CommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.infoDic = [commentsArr objectAtIndex:indexPath.row];
    
   
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CommentCell *cell = (CommentCell *)[self tableView:tableView cellForRowAtIndexPath:indexPath];
    return [cell heightOfCell];
    //    return 100;
}




- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
    CGRect rect = [UIScreen mainScreen].bounds;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    commentBac.frame = CGRectMake(0, rect.size.height-112, 320, 50);
    [UIView commitAnimations];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    CGRect rect = [UIScreen mainScreen].bounds;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    commentBac.frame = CGRectMake(0, rect.size.height-112, 320, 50);
    [UIView commitAnimations];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

@end
