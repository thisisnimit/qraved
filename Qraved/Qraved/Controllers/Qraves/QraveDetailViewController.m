//
//  Qraved
//
//  Created by Shine Wang on 9/10/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//
#import "QraveDetailViewController.h"
#import "LoadingView.h"
#import "Tools.h"
#import "Constants.h"
#import "PostData.h"
#import "Date.h"
#import "UIImageView+Cache.h"
#import "DetailViewController.h"
#import "UIAlertView+BlocksKit.h"
#import "UIButton+LinkButton.h"
#import "ProfileViewController.h"
#import "UIButton+Style.h"
#import <QuartzCore/QuartzCore.h>
#import "QraveitView.h"
#import "AppDelegate.h"
#import "UIViewController+Helper.h"
#import "UILabel+Helper.h"
#import "GAIDictionaryBuilder.h"
#import "NSString+Helper.h"
#import "UILabel+Helper.h"
#import "UIView+Helper.h"
#import "GAIFields.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"
#import "Mixpanel.h"
#import "NSNumber+Helper.h"

#define kDefaultToolbarHeight 50

const double CONTENT_WIDTH = 305.0f;

@interface QraveDetailViewController ()<QraveItViewDelegate>
{
    UIScrollView *backgroundScrollView;
    UIView *descriptionView;
//    UITextField *commentField;
    UIInputToolbar *inputToolbar;
    UIView *sendCommentView;
    UIView *commentArea;
    UIImageView *imageView;
    NSString *urlString;
    NSMutableArray *commentArray;
    
    UIImageView *animationView;
    
    UIImage *sharedImage;
    
    @private
    BOOL keyboardIsVisible;
}
@end

@implementation QraveDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
-(id)initWithDish:(Dish *)paramDish
{
    self=[super init];
    if(self)
    {
        self.dish=paramDish;
//        if([[AppDelegate ShareApp].qravedDish.objectID isEqual:self.dish.objectID])
//        {
//            self.dish=[AppDelegate ShareApp].qravedDish;
//        }
    }
    return self;
}


-(id)initWithDish:(Dish *)paramDish andQraveitView:(QraveitView *)paramQraveitView
{
    self=[super init];
    if(self)
    {
        self.dish=paramDish;
        
        self.qraveitView=paramQraveitView;
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    self.screenName=@"QraveDetail";
}

- (void)goToBackViewController{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
//    if([self.navigationController respondsToSelector:@selector(setCanDragBack:) ])
//    {
//        ((MLNavigationController *)self.navigationController).canDragBack=NO;
//    }
    
//    [[LoadingView sharedLoadingView] stopLoading];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
//    [[AppDelegate ShareApp] setTabbarHidden];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationItem setTitle:self.dish.title];
    
    
    NSMutableDictionary *dddd = [NSMutableDictionary dictionaryWithDictionary:[self.dish getMixpanelDic]];
    if (self.restaurantInfo.cuisines ) {
        [dddd setObject:self.restaurantInfo.cuisines forKey:@"Cuisine"];
    }
    if (self.restaurantInfo.cuisinesIDs) {
        [dddd setObject:self.restaurantInfo.cuisinesIDs forKey:@"Cuisine"];
    }
    if (self.restaurantInfo.district) {
        [dddd setObject:self.restaurantInfo.district forKey:@"District"];
    }
    if (self.restaurantInfo.districtId) {
        [dddd setObject:self.restaurantInfo.districtId forKey:@"District ID"];
    }
    
    NSMutableString *scoreString=[[NSMutableString alloc]initWithCapacity:0];
    for(int i=0;i<[self.restaurantInfo.score intValue];i++)
    {
        [scoreString appendFormat:@"$"];
    }
    [dddd setObject:scoreString forKey:@"Price Level"];
    
    float num=[[NSNumber numberWithDouble:self.restaurantInfo.ratingScore] getRatingNumber];
    float num2=num/(int)num;
    if (num2-1==0) {
        [dddd setObject:[NSString stringWithFormat:@"%.f",num] forKey:@"Rating"];
    }
    else
    {
        [dddd setObject:[NSString stringWithFormat:@"%.1f",num] forKey:@"Rating"];
    }
    //Top Discount Off
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"View Qraved Photo Detail" properties:dddd];
    
    
    commentArray=[NSMutableArray array];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    backgroundScrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-44-44)]; //44 Send frame;
    [backgroundScrollView setShowsVerticalScrollIndicator:NO];
    [self.view addSubview:backgroundScrollView];
    NSString *lastImgUrlStr =self.dish.imageUrl;
    
    NSString *imgUrlStr=[lastImgUrlStr returnFullImageUrl];
    
    NSString *frameStrWithHou = [[imgUrlStr componentsSeparatedByString:@"-"]lastObject];
    NSString *frameStr = [[frameStrWithHou componentsSeparatedByString:@"."]objectAtIndex:0];

    
    NSInteger widthOriginal=[[[frameStr componentsSeparatedByString:@"x"]objectAtIndex:0] integerValue];
    NSInteger heightOriginal=[[[frameStr componentsSeparatedByString:@"x"]lastObject] integerValue];

    CGFloat targetHeight;
    if (heightOriginal && widthOriginal) {
        
        targetHeight=heightOriginal*DeviceWidth/widthOriginal;
    }
    else
    {
        targetHeight=DeviceHeight/2;
    }
    imageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth,targetHeight)];
    imageView.userInteractionEnabled=YES;
    
    animationView=[[UIImageView alloc] initWithFrame:CGRectZero];
    [animationView setAlpha:0.0];
    
    UIImage *animationImage=[UIImage imageNamed:@"heart"];
    CGFloat animationWidth=250;
    CGFloat animationHeight=animationWidth*animationImage.size.height/animationImage.size.width;
    if(animationHeight>targetHeight)
    {
        animationHeight=targetHeight;
        animationWidth=animationHeight*animationImage.size.width/animationImage.size.height;
    }
    
    [animationView setFrame:CGRectMake((DeviceWidth-animationWidth)/2, (targetHeight-animationHeight)/2, animationWidth, animationHeight)];
    [animationView setImage:animationImage];
    
    [imageView addSubview:animationView];
    
    CGFloat shareButtonWidth=60;
    UIButton *shareButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [shareButton setBackgroundColor:[UIColor clearColor]];
    [shareButton setFrame:CGRectMake(imageView.frame.size.width-shareButtonWidth, imageView.frame.size.height-shareButtonWidth, shareButtonWidth, shareButtonWidth)];
    [shareButton addTarget:self action:@selector(Share:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:shareButton];
    
    UIImage *shareImage=[UIImage imageNamed:@"fenxiang"];
    UIImageView *shareImageView=[[UIImageView alloc]initWithImage:shareImage];
    
    CGFloat mapIconWidth=22;
    [shareImageView setFrame:CGRectMake((shareButtonWidth-mapIconWidth)/2, (shareButtonWidth-mapIconWidth)/2, mapIconWidth, mapIconWidth)];
    [shareButton addSubview:shareImageView];
    
    descriptionView=[[UIView alloc] init];
    
    
    UIImage *defaultImage=[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(320, targetHeight)];
    
    
     __strong typeof(self) weakSelf=self;
    NSString *fuzzyImageUrl=[self.dish.imageUrl returnFullImageUrlWithWidth:305.0/2];
    [imageView setImageWithURL:[NSURL URLWithString:fuzzyImageUrl] placeholderImage:defaultImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        
        [weakSelf->imageView setAlpha:0.5];
        
        NSString *bigImageUrl=[weakSelf.dish.imageUrl returnFullImageUrlWithWidth:DeviceWidth];
        __strong typeof(self) weak_Self=weakSelf;
        
        [weakSelf->imageView setImageWithURL:[NSURL URLWithString:bigImageUrl] placeholderImage:image completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
            
            [UIView animateWithDuration:0.5 animations:^{
                [weak_Self->imageView setAlpha:1];
                
            }];
        }];
        
    }];
    
//    NSString *shareImageUrlString=[[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:lastImgUrlStr] stringByAppendingFormat:@"&width=100"];
    NSString *shareImageUrlString=[lastImgUrlStr returnFullImageUrlWithWidth:100];
    
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        [imageView setImageUrl:lastImgUrlStr andWidth:320];
//    
//    });
    
//    NSString *fullURLString =[[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:lastImgUrlStr] stringByAppendingFormat:@"&width=%f",DeviceWidth];
//    NSString *fullURLString=[lastImgUrlStr rreturnFullImageUrlWithWidth:DeviceWidth];
//    [imageView setImageWithURL:[NSURL URLWithString:fullURLString] placeholderImage:defaultImage];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSURL *shareImageUrl=[NSURL URLWithString:shareImageUrlString];
        
        NSData *shareImageData=[NSData dataWithContentsOfURL:shareImageUrl];
        if(shareImageData)
        {
            sharedImage=[UIImage imageWithData:shareImageData];
        }
    });
    
    
    urlString=[QRAVED_WEB_SERVER stringByAppendingFormat:@"image/%@",lastImgUrlStr];
    
    
    
//    NSLog(@"fullUrlString is %@",urlString);
    
//    //[[LoadingView sharedLoadingView] startLoading];
    
    [backgroundScrollView addSubview:imageView];
    
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard:)];
    
    [backgroundScrollView addGestureRecognizer:tap];
    
//    UIButton *dishBtn=[UIButton buttonWithType:UIButtonTypeCustom];
//    [dishBtn setFrame:CGRectMake(CELL_CONTENT_OFFSET, 5, DeviceWidth, 30)];
//    [dishBtn setBackgroundColor:[UIColor colorWithRed:0.071 green:0.071 blue:0.071 alpha:1]];
//    [dishBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [dishBtn setTitle:self.dish.title forState:UIControlStateNormal];
//    [dishBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
//    [descriptionView addSubview:dishBtn];
    //QravedView.
    
    NSNumber *object=[[NSUserDefaults standardUserDefaults] objectForKey:self.dish.objectID];
    if(object!=nil)
    {
        self.dish.qraved=[object boolValue];
    }
    
    int qraveBarHeight = 40;
    QraveitView *qravedView=[[QraveitView alloc] initWithFrame:CGRectZero];
    qravedView.delegate=self;
    [qravedView  configWithDish:self.dish andWidth:DeviceWidth andHeight:qraveBarHeight isDetail:YES];
    [qravedView setFrame:CGRectMake(0, [Tools viewEndPointY:imageView]-1, DeviceWidth, qraveBarHeight)];
    [backgroundScrollView addSubview:qravedView];
    
    UILabel *dishLabel = [[Label alloc] initDetailTitleLabel];
    dishLabel.text = self.dish.title;
    dishLabel.frame = CGRectMake(CELL_CONTENT_OFFSET*2, 5,  DeviceWidth-CELL_CONTENT_OFFSET*2, dishLabel.expectedHeight);
    [dishLabel sizeToFit];
    [descriptionView addSubview:dishLabel];

    UILabel *restaurantLabel =[ [Label alloc] initDetailTitleCaptionLabel];
    [restaurantLabel setText:[NSString stringWithFormat:@"at %@",[Tools returnNameString:self.dish.restaurantName]]];
    [restaurantLabel setFrame:CGRectMake(CELL_CONTENT_OFFSET*2, [Tools viewEndPointY:dishLabel] + 4, DeviceWidth-CELL_CONTENT_OFFSET*2, restaurantLabel.expectedHeight)];
    [restaurantLabel sizeToFit];
    [descriptionView addSubview:restaurantLabel];
    
    UIButton *restaurantBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [restaurantBtn setFrame:CGRectMake(0, 0, CONTENT_WIDTH, restaurantLabel.endPointY)];
    [restaurantBtn addTarget:self action:@selector(detailLink:) forControlEvents:UIControlEventTouchUpInside];
    //[restaurantBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [restaurantBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [descriptionView addSubview:restaurantBtn];
    
    
    UILabel *userLabel = [[Label alloc] initDetailNicknameLabel];
    [userLabel setText:self.dish.userFullName];
    userLabel.frame = CGRectMake(DeviceWidth-30-userLabel.expectedWidth-CELL_CONTENT_OFFSET*3, [Tools viewEndPointY:restaurantBtn]+5, userLabel.expectedWidth, userLabel.font.pointSize);
    [userLabel setTextAlignment:NSTextAlignmentRight];
    [userLabel sizeToFit];
    [descriptionView addSubview:userLabel];
    
    
    UILabel *userTimeLabel = [[Label alloc] initDetailTimestampLabel];
    [userTimeLabel setText:[Date getTimeInteval:self.dish.createTime]];
    userTimeLabel.frame = CGRectMake(DeviceWidth-30-userTimeLabel.expectedWidth-CELL_CONTENT_OFFSET*3, [Tools viewEndPointY:userLabel] + 3, userTimeLabel.expectedWidth, userTimeLabel.font.pointSize);
    [userTimeLabel setTextAlignment:NSTextAlignmentRight];
    [userTimeLabel sizeToFit];
    [descriptionView addSubview:userTimeLabel];
    

    UIButton *userAvatarImageView=[UIButton buttonWithType:UIButtonTypeCustom];
    [userAvatarImageView setFrame:CGRectMake(DeviceWidth -40, [Tools viewEndPointY:restaurantBtn]+5, 30, 30)];
    [userAvatarImageView setBackgroundImage:[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(30, 30)] forState:UIControlStateNormal];
    
    
    [userAvatarImageView addTarget:self action:@selector(userAvatarLink:) forControlEvents:UIControlEventTouchUpInside];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [userAvatarImageView setBackgroundImageUrl:self.dish.userAvatar andWidth:30];
    });
    
    [descriptionView addSubview:userAvatarImageView];

    
    
    descriptionView.backgroundColor = [UIColor whiteColor];
    descriptionView.layer.masksToBounds = NO;
    descriptionView.layer.shadowColor = [UIColor grayColor].CGColor;
    descriptionView.layer.shadowRadius = 2.0;
    descriptionView.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    descriptionView.layer.shadowOpacity = 0.3f;

    descriptionView.frame = CGRectMake(0, [Tools viewEndPointY:qravedView]+CELL_CONTENT_OFFSET, DeviceWidth, 100);
    
    float height=restaurantLabel.frame.size.height+dishLabel.frame.size.height;
    if (height > 50) {
        CGRect frame=descriptionView.frame;
        frame.size.height=100+height-50;
        descriptionView.frame=frame;
    }
    
    
    [backgroundScrollView addSubview:descriptionView];
    

    inputToolbar=[[UIInputToolbar alloc]initWithFrame:CGRectMake(0, DeviceHeight-44-kDefaultToolbarHeight, DeviceWidth, kDefaultToolbarHeight)];
    [inputToolbar setBackgroundColor:[UIColor colorWithRed:0.93 green:0.93 blue:0.93 alpha:1]];
    [self.view addSubview:inputToolbar];
    inputToolbar.inputDelegate=self;
    inputToolbar.textView.placeholder=@"Write a comment";

    [self updateComments];
}

-(void)qravedAnimation
{
    UIImage *disQravedImage=[UIImage imageNamed:@"heart"];
    [animationView setImage:disQravedImage];
    
    [self setAnimation];
}

-(void)disQravedAnimation
{
    UIImage *disQravedImage=[UIImage imageNamed:@"heart_broken"];
    [animationView setImage:disQravedImage];
    
    [self setAnimation];
}


-(void)setAnimation
{
    CGRect originRect=animationView.frame;
    
    [animationView setFrame:CGRectMake(originRect.origin.x+30, originRect.origin.y+30, originRect.size.width-60, originRect.size.height-60)];
    
    [UIView animateWithDuration:0.3 animations:^{
        
        [animationView setAlpha:1];
        
        [animationView setFrame:CGRectMake(originRect.origin.x+10, originRect.origin.y+10, originRect.size.width-20, originRect.size.height-20)];
        
    } completion:^(BOOL finished) {
        
        
        [UIView animateWithDuration:0.3 delay:0.3 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            //
            [animationView setAlpha:0];
            
            [animationView setFrame:originRect];
        } completion:nil];
        
    }];
    
}


-(void)updateComments
{
    PostData *postData=[[PostData alloc]init];
    NSDictionary *parameters;
    if([AppDelegate ShareApp].isLogin)
    {
        parameters=@{@"userID":[User sharedUser].userID,@"offset":@"0",@"max":@"10",@"dishID":self.dish.objectID};
    }
    else
    {
        parameters=@{@"offset":@"0",@"max":@"10",@"dishID":self.dish.objectID};
    }
    [postData httpRequest:parameters andPath:@"dish/comments" andSuccessBlock:^(id postArray){
        
        NSArray *commentList=[postArray objectForKey:@"commentList"];
        NSMutableArray *currentCommentArray=[NSMutableArray array];
        for(int i=0;i<[commentList count];i++)
        {
            id commentObject=[commentList objectAtIndex:i];
            UserComment *comment=[[UserComment alloc]init];
            comment.userID=[commentObject objectForKey:@"userId"];
            comment.avatarImageUrl=[commentObject objectForKey:@"avatar"];
            comment.fullName=[Tools retrunShortUserName:[commentObject objectForKey:@"fullName"]];
            comment.comment=[commentObject objectForKey:@"comment"];
            comment.createdTime=[commentObject objectForKey:@"timeCreatedStr"];
            [currentCommentArray addObject:comment];
        }
        if(![Tools isBlankString:self.dish.selfComment])
        {
            UserComment *firstComment=[[UserComment alloc]init];
            firstComment.userID=self.dish.userid;
            firstComment.avatarImageUrl=self.dish.userAvatar;
            firstComment.fullName=self.dish.userFullName;
            firstComment.comment=self.dish.selfComment;
            firstComment.createdTime=self.dish.createTime;
            [currentCommentArray addObject:firstComment];
        }
        
        commentArray=currentCommentArray;
        [self updateCommentView];
        
        inputToolbar.inputButton.enabled=YES;
    } andFailedBlock:^(NSError *error){
        [[LoadingView sharedLoadingView] stopLoading];
        
        inputToolbar.inputButton.enabled=YES;
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:ALERT_TITLE message:error.localizedDescription];
        [alertView addButtonWithTitle:@"OK"];
        [alertView show];
    }];
}

-(void)updateCommentView
{
    float currentEndPointY = 20;
    float allCommentHeight=0;
    
    [commentArea removeFromSuperview];
    
    commentArea=[[UIView alloc]initWithFrame:CGRectZero];
    
 
    for(int i=0;i<[commentArray count];i++)
    {
        UserComment *comment=(UserComment *)[commentArray objectAtIndex:i];
        UIButton *avatarImageView=[UIButton buttonWithType:UIButtonTypeCustom];
        [avatarImageView setFrame:CGRectMake(5, 0, 50, 50)];
        [avatarImageView setTag:i];
        [avatarImageView addTarget:self action:@selector(commentAvatarLink:) forControlEvents:UIControlEventTouchUpInside];
        [avatarImageView setBackgroundImage:[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(50, 50)] forState:UIControlStateNormal];

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [avatarImageView setBackgroundImageUrl:comment.avatarImageUrl andWidth: 50];
        });
        

        
        UILabel *userNameLabel = [[Label alloc] initTitleLabel];
        [userNameLabel setText:comment.fullName];
        userNameLabel.frame = CGRectMake([Tools viewEndPointX:avatarImageView]+5, 0, userNameLabel.expectedWidth, userNameLabel.font.pointSize);
        [userNameLabel sizeToFit];
        
        UILabel *commentLabel=[[Label alloc]initInfoHeaderLabel];
        CGSize maximumLabelSize = CGSizeMake(120, FLT_MAX);
        
        commentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        commentLabel.numberOfLines = 0;
        CGSize expectedLabelSize = [comment.comment sizeWithFont:commentLabel.font constrainedToSize:maximumLabelSize lineBreakMode:commentLabel.lineBreakMode];
        [commentLabel setText:comment.comment];
        commentLabel.frame = CGRectMake([Tools viewEndPointX:avatarImageView]+5, [Tools viewEndPointY:userNameLabel] + 6, DeviceWidth-[Tools viewEndPointX:avatarImageView]-15, expectedLabelSize.height);
        [commentLabel sizeToFit];
        
        
        UILabel *commentTimeLabel=[[Label alloc] initCommentTimestampLabel];
        if(![comment.createdTime isEqualToString:@"just Now"])
        {
            [commentTimeLabel setText:[Date getTimeInteval:comment.createdTime]];
        }
        else
        {
            [commentTimeLabel setText:@"1 second ago"];
        }
         commentTimeLabel.frame = CGRectMake(DeviceWidth-commentTimeLabel.expectedWidth-15, 0, commentTimeLabel.expectedWidth, commentTimeLabel.font.lineHeight);
        
        float commentViewHeight;
        
        if([Tools viewEndPointY:avatarImageView] > [Tools viewEndPointY:commentLabel]) {
            commentViewHeight = [Tools viewEndPointY:avatarImageView] + 13;
        } else {
            commentViewHeight = [Tools viewEndPointY:commentLabel] + 13;
        }
        
        UIView *commentView=[[UIView alloc]initWithFrame:CGRectMake(5, currentEndPointY, DeviceWidth, commentViewHeight)];
        [commentView setTag:100+i];
        
        [commentView addSubview:commentTimeLabel];
        [commentView addSubview:commentLabel];
        [commentView addSubview:userNameLabel];
        [commentView addSubview:avatarImageView];
        
        [commentArea addSubview:commentView];
        
        currentEndPointY = [Tools viewEndPointY:commentView];
        allCommentHeight+=commentView.frame.size.height;
    }
    
    [commentArea setFrame:CGRectMake(0, [Tools viewEndPointY:descriptionView]-10, DeviceWidth, allCommentHeight)];
    
    [backgroundScrollView setContentSize:CGSizeMake(DeviceWidth, [Tools viewEndPointY:descriptionView]+commentArea.frame.size.height+50)];
    [backgroundScrollView addSubview:commentArea];
    
    [[LoadingView sharedLoadingView] stopLoading];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)detailLink:(UIButton *)button
{
    DetailViewController *detailViewCongtroller=[[DetailViewController alloc]initWithRestaurantID:self.dish.restaurantID andRestaurantName:self.dish.restaurantName andSegmentedIndex:0];
    [self.navigationController pushViewController:detailViewCongtroller animated:YES];
}

-(void)postComment:(NSString*)textStr
{
    if(textStr.length==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:ALERT_TITLE message:@"Please enter a description."];
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    }
    else
    {
        inputToolbar.inputButton.enabled=NO;
        PostData *postData=[[PostData alloc]init];
        NSDictionary *parameters=@{@"t": [User sharedUser].token,@"userID":[User sharedUser].userID,@"dishID":self.dish.objectID,@"restaurantID":self.dish.restaurantID,@"comment":textStr,@"status":@"1"};
        
        [[LoadingView sharedLoadingView] startLoading];
        [postData postHttpRequest:parameters andPath:@"dish/comment/write" andSuccessBlock:^(id postArray){
            self.qravePhoto=[QravePhoto configByQraveJsonObject:postArray];
            NSMutableDictionary *dddd = [NSMutableDictionary dictionaryWithDictionary:[self.dish getMixpanelDic]];
            NSMutableDictionary *ddd=[NSMutableDictionary dictionaryWithCapacity:0];
            if (self.qravePhoto.cuisineName) {
                [ddd setObject:self.qravePhoto.cuisineName forKey:@"Cuisine"];
            }
            if (self.qravePhoto.cuisineID) {
                [ddd setObject:self.qravePhoto.cuisineID forKey:@"Cuisine ID"];
            }
            if (self.qravePhoto.districtName) {
                [ddd setObject:self.qravePhoto.districtName forKey:@"District"];
            }
            if (self.qravePhoto.discount) {
                [ddd setObject:self.qravePhoto.discount forKey:@"Top Discount Off"];
            }
            
            NSMutableString *scoreString=[[NSMutableString alloc]initWithCapacity:0];
            for(int i=0;i<[self.qravePhoto.priceLevel intValue];i++)
            {
                [scoreString appendFormat:@"$"];
            }
            [ddd setObject:scoreString forKey:@"Price Level"];
            float num=[[NSNumber numberWithDouble:[self.qravePhoto.ratingScore doubleValue]] getRatingNumber];
            float num2=num/(int)num;
            if (num2-1==0) {
                [ddd setObject:[NSString stringWithFormat:@"%.f",num] forKey:@"Rating"];
            }
            else
            {
                [ddd setObject:[NSString stringWithFormat:@"%.1f",num] forKey:@"Rating"];
            }
            [dddd addEntriesFromDictionary:ddd];
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Comment For Qraved Photo" properties:dddd];
            
            NSString *exceptionmsg=[postArray objectForKey:@"exceptionmsg"];
            if(exceptionmsg)
            {
                if ([exceptionmsg isLogout]) {
                    [[User sharedUser]logOut];
                }
                UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:ALERT_TITLE message:exceptionmsg];
                [alertView addButtonWithTitle:@"OK"];
                [alertView show];
            }
            else
            {
                
                UserComment *comment=[[UserComment alloc]init];
                comment.userID=[User sharedUser].userID;
                comment.avatarImageUrl=[User sharedUser].avatarUrl;
                comment.fullName=[NSString stringWithFormat:@"%@ %@",[User sharedUser].firstName,[User sharedUser].lastName];
                comment.comment=textStr;
                comment.createdTime=@"just Now";
                
                
                [commentArray insertObject:comment atIndex:0];
                [inputToolbar.textView clearText];
                [self updateCommentView];
                
                NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:@"Comment" action:@"Comment on photo" label:self.dish.restaurantName value:nil ] build];
                id tracker = [[GAI sharedInstance] defaultTracker];
                
                [tracker set:kGAIScreenName value:@"QraveDetail"];
                [tracker send:event];
            }
            
           
        } andFailedBlock:^(NSError *error){
            inputToolbar.inputButton.enabled=YES;
            
            UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:ALERT_TITLE message:error.localizedDescription];
            [alertView addButtonWithTitle:@"OK"];
            [alertView show];
            
            [[LoadingView sharedLoadingView] stopLoading];
        }];
        [self.view endEditing:YES];
    }
    
}



-(void)userAvatarLink:(UIButton *)button
{

    
    [self pushToProfileViewController:self.dish.userid];
    

}

-(void)commentAvatarLink:(UIButton *)button
{
    NSInteger buttonTag=button.tag;
    
    UserComment *comment=(UserComment *)[commentArray objectAtIndex:buttonTag];
    
    
        [self pushToProfileViewController:comment.userID];
    

}

-(void)pushToProfileViewController:(NSString *)userID
{
    ProfileViewController *profileViewController=[[ProfileViewController alloc]initWithSegementTag:1];
    [self.navigationController pushViewController:profileViewController animated:YES];
}


- (void)keyboardWillShow:(NSNotification *)notification
{
    
    if([AppDelegate ShareApp].isLogin)
    {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.3];
        CGRect frame = inputToolbar.frame;
        
        frame.origin.y = self.view.frame.size.height - frame.size.height - 216;
        inputToolbar.frame = frame;
        [UIView commitAnimations];
        keyboardIsVisible = YES;
        
    }
    
    else
    {
        LoginViewController *loginVC=[[LoginViewController alloc]init];
        UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
        [[AppDelegate ShareApp].window.rootViewController presentViewController:loginNavagationController animated:YES completion:nil];
      
    }
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    /* Move the toolbar back to bottom of the screen */
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	CGRect frame = inputToolbar.frame;
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation)) {
        frame.origin.y = self.view.frame.size.height - frame.size.height;
    }
    else {
        frame.origin.y = self.view.frame.size.width - frame.size.height;
    }
	inputToolbar.frame = frame;
	[UIView commitAnimations];
    keyboardIsVisible = NO;
}

-(void)inputButtonPressed:(NSString *)inputText
{
    NSString *trimmedString = [inputText stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    if (inputText && !trimmedString.length == 0) {
        if (inputText.length>0) {
            inputToolbar.textView.text = @"";
            [self postComment:inputText];
        }
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:ALERT_TITLE message:@"Please enter a comment."];
        [alert addButtonWithTitle:@"OK"];
        [alert show];
    }
    
    
    
}

-(void)dismissKeyboard:(UITapGestureRecognizer *)gesture
{
    [inputToolbar.textView resignFirstResponder];
    keyboardIsVisible=NO;
}

-(void)Share:(UIButton *)button
{
    UIActionSheet *actionSheet=[[UIActionSheet alloc]initWithTitle:@"" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:Nil otherButtonTitles:@"Facebook",@"Twitter",nil];
    [actionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            [self shareWithFacebook];
            break;
        }
        case 1:
        {
            [self shareWithTwitter];
            break;
        }
            
        default:
            break;
    }
}

-(void)shareWithFacebook
{
    int currentver =[self returnIOSVersion];;
    if(currentver>=6)
    {
        SLComposeViewController *slComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [slComposerSheet setInitialText:self.dish.title];
        if(sharedImage)
        {
            [slComposerSheet addImage:sharedImage];
        }
        [slComposerSheet addURL:[NSURL URLWithString:@"http://www.qraved.com"]];
        if(slComposerSheet)
        {
            [self presentViewController:slComposerSheet animated:YES completion:nil];
       
            
            [slComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
               
                Mixpanel *mixpanel = [Mixpanel sharedInstance];
                [mixpanel track:@"Share Qraved Photo " properties:[self.dish getMixpanelDic]];
                
                NSString *output;
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        output = @"Action Cancelled";
                        break;
                    case SLComposeViewControllerResultDone:
                        output = @"Post Successfull";
                        break;
                    default:
                        break;
                }
                if (result != SLComposeViewControllerResultCancelled)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook Message" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [alert show];
                }
            }];
        }
        else
        {
            [self shareFBWithOld];
        }
    }
    else
    {
        [self shareFBWithOld];
    }
    
    NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:@"Share" action:@"Share Restaurant" label:@"fb" value:nil] build];
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker set:kGAIScreenName value:@"QraveDetail"];
    [tracker send:event];
}

-(void)shareFBWithOld
{
    NSMutableDictionary *params =
    [NSMutableDictionary dictionaryWithObjectsAndKeys:
     self.dish.title, @"name",
     @"http://www.qraved.com", @"link",
     urlString, @"picture",
     nil];
    

    [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                           parameters:params
                                              handler:
     ^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
         if (error) {
             // Error launching the dialog or publishing a story.
             NSLog(@"Error publishing story %@",error.localizedDescription);
         } else {
             if (result == FBWebDialogResultDialogNotCompleted) {
                 
             } else {
         
                 NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                 if (![urlParams valueForKey:@"post_id"]) {
                     
                 } else {
                     [[[UIAlertView alloc] initWithTitle:ALERT_TITLE
                                                 message:@"Shared success!"
                                                delegate:nil
                                       cancelButtonTitle:@"OK!"
                                       otherButtonTitles:nil]
                      show];
                 }
             }
         }
     }];

}

- (NSDictionary*)parseURLParams:(NSString *)query {
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [kv[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        params[kv[0]] = val;
    }
    return params;
}

-(void)shareWithTwitter
{
    int currentver =[self returnIOSVersion];
    if(currentver>=6)
    {
        SLComposeViewController *slComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [slComposerSheet setInitialText:self.dish.title];
        if(sharedImage)
        {
            [slComposerSheet addImage:sharedImage];
        }
        [slComposerSheet addURL:[NSURL URLWithString:@"http://www.qraved.com"]];
        if(slComposerSheet)
        {
            [self presentViewController:slComposerSheet animated:YES completion:nil];
            //        }
            
            [slComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                //        NSLog(@"start completion block");
                Mixpanel *mixpanel = [Mixpanel sharedInstance];
                [mixpanel track:@"Share Qraved Photo " properties:[self.dish getMixpanelDic]];
                
                NSString *output;
                switch (result) {
                    case SLComposeViewControllerResultCancelled:
                        output = @"Action Cancelled";
                        break;
                    case SLComposeViewControllerResultDone:
                        output = @"Post Successfull";
                        break;
                    default:
                        break;
                }
                if (result != SLComposeViewControllerResultCancelled)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter Message" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [alert show];
                }
            }];
        }
        else
        {
            [self shareTwWithOld];
        }
    }
    else
    {
        
        [self shareTwWithOld];
    }
    
    NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:@"Share" action:@"Share Restaurant" label:@"tw" value:nil ] build];
    id tracker = [[GAI sharedInstance] defaultTracker];
    
    [tracker set:kGAIScreenName value:@"QraveDetail"];
    [tracker send:event];
}

-(void)shareTwWithOld
{
    SHKItem *shkItem=[SHKItem image:imageView.image title:self.dish.title];
    [shkItem setCustomValue:@"Absolut 100 Proof" forKey:@"status"];
    [SHK setRootViewController:self];
    [SHKTwitter shareItem:shkItem];

}
@end
