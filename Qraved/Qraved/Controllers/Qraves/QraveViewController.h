//
//  QraveViewController.h
//  Qraved
//
//  Created by Admin on 8/20/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QraveView.h"
#import "DishView.h"
#import "BaseViewController.h"
#import "EGORefreshTableHeaderView.h"
#import "UIButton+WebCache.h"
#import "QraveDetailViewController.h"

@class DishView;
@interface QraveViewController : BaseViewController <UIScrollViewDelegate,EGORefreshTableHeaderDelegate>{
    
    UIView *qravesView;
}
@property (nonatomic, retain) UIScrollView *mScrollView;


@property (nonatomic, retain) NSMutableArray *qravesList;


@property (nonatomic, retain) NSMutableArray *dishObjectArray;
@property (nonatomic, retain) NSMutableArray *dishViewFrameList;
@property (nonatomic, retain) NSMutableArray *dishViewArray;
@property (nonatomic, retain) NSMutableArray *dishViewReuseArray;


@property BOOL isTop;
@property(nonatomic,retain)NSDictionary *params;

-(id)initWithIsTopQraves:(BOOL)paramIsTop andParameters:(NSDictionary *)paramDictionary;

@end
