//
//  QraveDetailViewController.h
//  Qraved
//
//  Created by Shine Wang on 9/10/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Dish.h"
#import "BaseChildViewController.h"
#import "UIInputToolbar.h"
#import "QraveitView.h"
#import "RestaurantInfo.h"
#import "QravePhoto.h"
@class QraveitView;

@protocol QraveDetailDelegate <NSObject>

-(void)changeQraveState;
@end

@interface QraveDetailViewController : BaseChildViewController<UIInputToolbarDelegate,UIActionSheetDelegate>

@property(nonatomic,retain)Dish *dish;

@property (nonatomic, retain) QravePhoto *qravePhoto;

@property(nonatomic,retain)QraveitView *qraveitView;

@property(nonatomic,retain)id<QraveDetailDelegate>delegate;

@property(nonatomic,retain)RestaurantInfo*restaurantInfo;

-(id)initWithDish:(Dish *)paramDish andQraveitView:(QraveitView *)paramQraveitView;

-(id)initWithDish:(Dish *)paramDish;
@end
