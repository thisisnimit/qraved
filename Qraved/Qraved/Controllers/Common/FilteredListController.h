//
//  FilteredListController.h
//  Qraved
//
//  Created by Jeff on 8/22/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PassValueDelegate.h"

@interface FilteredListController : UITableViewController

@property (nonatomic, copy)NSString		*searchText;
@property (nonatomic, copy)NSString		*selectedText;
@property (nonatomic, retain)NSMutableArray	*resultList;
@property (assign) id <PassValueDelegate> passValueDelegate;

- (void)updateData;


@end
