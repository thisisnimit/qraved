//
//  LoadingImageViewController.h
//  Qraved
//
//  Copyright (c) 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseChildViewController.h"

@interface LoadingImageViewController : BaseChildViewController

@property (nonatomic,assign)BOOL isShowLoadingView;
@property (nonatomic,strong)NSNumber *hiddenNavigationBarStatus;
@property (nonatomic,assign) BOOL isShowLoadingImage;


@end
