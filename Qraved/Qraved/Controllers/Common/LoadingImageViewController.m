//
//  LoadingImageViewController.m
//  Qraved
//
//  Copyright (c) 2016年 Imaginato. All rights reserved.
//

#import "LoadingImageViewController.h"
#import "SimpleLoadingImageView.h"
#import "LoadingView.h"
@interface LoadingImageViewController (){
    BOOL isPrePageHiddenNavigation;
}

@end

@implementation LoadingImageViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self addBackBtn];
    self.view.backgroundColor = [UIColor whiteColor];
    if (self.isShowLoadingView) {
        SimpleLoadingImageView *loadView= [[SimpleLoadingImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+20)];
        [self.view addSubview:loadView];
    }
    if (self.isShowLoadingImage) {
        [[LoadingView sharedLoadingView]startLoading];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    isPrePageHiddenNavigation = self.navigationController.navigationBarHidden;
    if (self.hiddenNavigationBarStatus!=nil) {
        if ([self.hiddenNavigationBarStatus intValue] == 0) {
            self.navigationController.navigationBarHidden = NO;
        }else{
            self.navigationController.navigationBarHidden = YES;
        }
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = isPrePageHiddenNavigation;
    [[LoadingView sharedLoadingView]stopLoading];

}

 
@end
