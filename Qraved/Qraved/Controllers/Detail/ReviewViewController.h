//
//  LatestReviewViewController.h
//  Qraved
//
//  Created by Shine Wang on 8/2/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "BaseViewController.h"

@interface ReviewViewController : BaseViewController
-(id)initWithRestaurant:(IMGRestaurant*)restaurant;
@end
