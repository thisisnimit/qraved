//
//  WebViewController.m
//  Qraved
//
//  Created by mac on 14-11-12.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "WebViewController.h"
#import "UIViewController+Helper.h"
#import "UIColor+Helper.h"
#import "LoadingView.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "UIPopoverListView.h"
#import "Label.h"
#import "ShareToPathView.h"
#import "NXOAuth2AccountStore.h"
//#import "MixpanelHelper.h"
#import "UIDevice+Util.h"
#import "AppDelegate.h"
#import "IMGCity.h"
#import "DetailViewController.h"
#import "RestaurantHandler.h"
#import "JournalHandler.h"

@interface WebViewController ()<UIWebViewDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,UIPopoverListViewDataSource,UIPopoverListViewDelegate>{
    UIPopoverListView *poplistview;
}

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //self.navigationController.title = self.title;
    
    self.navigationController.navigationBarHidden = NO;
    
    UILabel *navTitleLabel = [[UILabel alloc] init];
    navTitleLabel.textColor = [UIColor whiteColor];
//    navTitleLabel.text = @"Qraved Journal";
    navTitleLabel.font = [UIFont boldSystemFontOfSize:20];
    navTitleLabel.frame = CGRectMake(DeviceWidth/2-navTitleLabel.expectedWidth/2, 2, navTitleLabel.expectedWidth, 38);
    self.navigationItem.titleView = navTitleLabel;
    //[self addBackButton];
    [self setBackBarButtonOffset30];
    
    if (!self.fromDetail) {
        UIBarButtonItem *shareBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:NavigationShareImage] style:UIBarButtonItemStylePlain target:self action:@selector(shareButtonClick)];
        shareBtn.tintColor=[UIColor whiteColor];
        self.navigationItem.rightBarButtonItem=shareBtn;
    }
    
    
    UIWebView *webView = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-44)];
    webView.backgroundColor = [UIColor whiteColor];
    webView.scalesPageToFit = YES;

    for (UIView *_aView in [webView subviews])
    {
        if ([_aView isKindOfClass:[UIScrollView class]])
        {
            [(UIScrollView *)_aView setShowsVerticalScrollIndicator:NO];
            //右侧的滚动条
            
            [(UIScrollView *)_aView setShowsHorizontalScrollIndicator:NO];
            //下侧的滚动条
            
//            for (UIView *_inScrollview in _aView.subviews)
//            {contentStyle
//                if ([_inScrollview isKindOfClass:[UIImageView class]])
//                {
//                    _inScrollview.hidden = YES;  //上下滚动出边界时的黑色的图片
//                }
//            }
        }
    }
    webView.delegate = self;
    

    
//    NSString *meta = [NSString stringWithFormat:@"document.getElementsByName(\"viewport\")[0].content = \"width=%f, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no\"", webView.frame.size.width];
//    [webView stringByEvaluatingJavaScriptFromString:meta];
    
    
//    [JournalHandler getJournalDetailWithJournal:_journal andJournalBlock:^(IMGMediaComment *journal) {
//        
//        if (journal.contents)
//        {
//            [webView loadHTMLString:[[journal.contents html] changeStyle] baseURL:nil];
//            webView.scalesPageToFit = YES;
//            [self.view addSubview:webView];
//        }
//        
//    } andRestaurantsBlock:^(NSArray *dataArr) {
//        
//    } andRelatedPostBlock:^(NSArray *relatedArr) {
//        
//    }];

    
//    [JournalHandler getJournalDetailWithJournal:_journal andBlock:^(IMGMediaComment *journal) {
//        if (journal.contents)
//        {
//            [webView loadHTMLString:[[journal.contents html] changeStyle] baseURL:nil];
//            webView.scalesPageToFit = YES;
//            [self.view addSubview:webView];
//        }else
//        {
//            if (![self.webSite hasPrefix:@"http://"]) {
//                self.webSite = [@"http://" stringByAppendingString:self.webSite];
//            }
//            
//            NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:self.webSite]];
//            NSLog(@"webSite = %@",self.webSite);
//            [self.view addSubview: webView];
//            [webView loadRequest:request];
//        }
//        
//    }];
//    
    
#pragma old
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    if (self.content.length)
    {
        
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"contentStyle" ofType:@"plist"];
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
        NSString *contentFirstStr = [data objectForKey:@"content"];
        NSString *cssTitleStr = [[NSString alloc] init];
        if (self.title == nil)
        {
            cssTitleStr = @"";
        }
        else
        {
            cssTitleStr = [NSString stringWithFormat:@"<div class=\"title\">%@</div>",self.title];
        }
        NSString *newContentsStr = [[NSString alloc] initWithFormat:@"%@%@%@%@",contentFirstStr,cssTitleStr,self.content,[data objectForKey:@"contentEnd"]];
        
        NSString *contentHead = [NSString stringWithFormat:@"<html><head><meta name=\"viewport\"content=\"width=%f, initial-scale=1, maximum-scale=1, user-scalable=no\">",DeviceWidth-LEFTLEFTSET*2];
        //NSString *titleStr = [NSString stringWithFormat:@"<div class=\"title\">\"%@\"</div>",self.title];
        NSString *contentHtmlStr = [NSString stringWithFormat:@"%@%@",contentHead,newContentsStr];
        NSString *newContentHtmlStr = [[contentHtmlStr stringByReplacingOccurrencesOfString:@"<p>&nbsp;</p>" withString:@" "] stringByReplacingOccurrencesOfString:@"690" withString:[NSString stringWithFormat:@"%f",DeviceWidth-LEFTLEFTSET*2]];
        
        [webView loadHTMLString:[[newContentHtmlStr html] changeStyle] baseURL:nil];
        
        webView.scalesPageToFit = YES;
        [self.view addSubview:webView];
    }
    else
    {
        if (![self.webSite hasPrefix:@"http"]) {
            self.webSite = [@"http://" stringByAppendingString:self.webSite];
        }
        
        NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:self.webSite]];
        NSLog(@"webSite = %@",self.webSite);
        [self.view addSubview: webView];
        [webView loadRequest:request];
    }
}
//- (void)webViewDidFinishLoad:(UIWebView *)webView
//{
//    //修改服务器页面的meta的值
//    NSString *meta = [NSString stringWithFormat:@"document.getElementsByName(\"viewport\")[0].content = \"width=%f, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no\"", webView.frame.size.width];
//    [webView stringByEvaluatingJavaScriptFromString:meta];
//}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
    [super viewWillAppear:animated];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[LoadingView sharedLoadingView]stopLoading];
}
-(void)webViewDidStartLoad:(UIWebView *)webView{
//    if (self.fromDetail) {
//        [[LoadingView sharedLoadingView] startLoading];
//    }
}
//-(void)webViewDidFinishLoad:(UIWebView *)webView
//{
//    [[LoadingView sharedLoadingView]stopLoading];
//}
//- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
//{
//    if (navigationType == UIWebViewNavigationTypeLinkClicked)
//    {
//        NSURL *url = [request URL];
//        NSString *urlString = [url absoluteString];
//        NSLog(@"shouldStartLoadWithRequest----%@",urlString);
//
//        NSRange cityRange = [urlString rangeOfString:@"http://www.qraved.com/"];
//        if (cityRange.location != NSNotFound) {
//            NSString *city = [urlString substringFromIndex:cityRange.length];
//            NSRange range = [city rangeOfString:@"/"];
//            if (range.location != NSNotFound) {
//                NSNumber *cityId;
//                NSString *cityName = [city substringToIndex:range.location];
//                NSString *keyWord = [city substringFromIndex:range.location+1];
//                NSRange otherRange1 = [keyWord rangeOfString:@"/"];
//                NSRange otherRange2 = [keyWord rangeOfString:@"?"];
//                if (otherRange1.location != NSNotFound ||otherRange2.location != NSNotFound) {
//                    return YES;
//                }
//                NSString *sqlString = [NSString stringWithFormat:@"select * from IMGCity where name='%@' limit 1",cityName] ;
//                FMResultSet *resultSet1 = [[DBManager manager] executeQuery:sqlString];
//                if ([resultSet1 next]) {
//                    IMGCity *city = [[IMGCity alloc]init];
//                    [city setValueWithResultSet:resultSet1];
//                    cityId=city.cityId;
//                }
//                [resultSet1 close];
//                
//                if(cityId!=nil){
//                    IMGRestaurant *restaurant=[self getRestaurantBySeoKeyword:keyWord andCityId:cityId];
//                    if(restaurant){
//                        DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurant:restaurant];
//                        [self.navigationController pushViewController:detailViewController animated:YES];
//                        return NO;
//                    }else{
//                        
//                    }
//                }
//            }
//            
//        }
////        if([[UIApplication sharedApplication]canOpenURL:url])
////        {
////            [[UIApplication sharedApplication]openURL:url];
////        }
//    }
//    return YES;
//}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSURL *url = [request URL];
        NSString *urlString = [url absoluteString];
        NSLog(@"shouldStartLoadWithRequest----%@",urlString);
        urlString = [urlString stringByReplacingOccurrencesOfString:@"/restaurant/" withString:@"/"];
        
        NSRange cityRange = [urlString rangeOfString:QRAVED_WEB_SERVER];
        //NSRange cityStagingRange = [urlString rangeOfString:@"http://staging.qraved.com/"];
        
        if (cityRange.location != NSNotFound) {
            
            NSString *lastPart = [urlString substringFromIndex:cityRange.length];
            NSRange range = [lastPart rangeOfString:@"/"];
            
            IMGRestaurant *restaurant=nil;
            NSString *keyWord;
            
            if (range.location != NSNotFound) {
                NSString *cityName;
                NSNumber *cityId;

                cityName = [lastPart substringToIndex:range.location];
                keyWord = [lastPart substringFromIndex:range.location+1];

                NSRange otherRange1 = [keyWord rangeOfString:@"/"];
                NSRange otherRange2 = [keyWord rangeOfString:@"?"];
                if (otherRange1.location != NSNotFound ||otherRange2.location != NSNotFound) {
                    return YES;
                }
                NSString *sqlString = [NSString stringWithFormat:@"select * from IMGCity where name='%@' limit 1",cityName] ;
                FMResultSet *resultSet1 = [[DBManager manager] executeQuery:sqlString];
                if ([resultSet1 next]) {
                    IMGCity *city = [[IMGCity alloc]init];
                    [city setValueWithResultSet:resultSet1];
                    cityId=city.cityId;
                }
                [resultSet1 close];
                

                if(cityId!=nil){
                    restaurant=[self getRestaurantBySeoKeyword:keyWord andCityId:cityId];
                    NSLog(@"keyWord = %@",keyWord);
                }
            }else{
                keyWord=lastPart;
                if (!keyWord || !keyWord.length)
                {
                    return NO;
                }
            }
            if(restaurant){
                DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurant:restaurant];
                [self.navigationController pushViewController:detailViewController animated:YES];
                return NO;
            }else{
                
                IMGNetWork *manager = [IMGNetWork sharedManager];
                
                NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:keyWord,@"seoKeyword", nil];
                [manager GET:@"restaurant/bykeyword" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
                    
                    NSLog(@"status_str = %@",responseObject);
                    
                    
                    NSArray *returnRestaurantArray=(NSArray *)responseObject;
                    IMGRestaurant *tmpRestaurant;
                    if ([IMGRestaurant findById:[returnRestaurantArray objectAtIndex:0]])
                    {
                        tmpRestaurant = [IMGRestaurant findById:[returnRestaurantArray objectAtIndex:0]];
                    }
                    else
                        tmpRestaurant = [RestaurantHandler updateRestaurant:returnRestaurantArray];
                    
                    NSLog(@"keyWord = %@",keyWord);
                    
                    if(tmpRestaurant){
                        DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurant:tmpRestaurant];
                        [self.navigationController pushViewController:detailViewController animated:YES];
                    }
                    
                }failure:^(NSURLSessionDataTask *operation, NSError *error) {
                    
                    NSLog(@"submit erors when get datas:%@",error.localizedDescription);
                    
                }];
                return NO;
            }
            
        }
        
        //        if([[UIApplication sharedApplication]canOpenURL:url])
        //        {
        //            [[UIApplication sharedApplication]openURL:url];
        //        }
    }
    return YES;
}

-(IMGRestaurant *)getRestaurantBySeoKeyword:(NSString *)keyWord andCityId:(NSNumber *)cityId{
    IMGRestaurant *restaurant;
    NSString *sqlString = [NSString stringWithFormat:@"select * from IMGRestaurant where seoKeyword='%@' and cityId=%@ limit 1",keyWord,cityId];
    FMResultSet *resultSet1 = [[DBManager manager] executeQuery:sqlString];
    if ([resultSet1 next]) {
        restaurant = [[IMGRestaurant alloc]init];
        [restaurant setValueWithResultSet:resultSet1];
        [resultSet1 close];
    }
    return restaurant;
}

//-(IMGRestaurant *)getRestaurantBySeoKeyword:(NSString *)keyWord andCityId:(NSNumber *)cityId{
//    IMGRestaurant *restaurant;
//    NSString *sqlString = [NSString stringWithFormat:@"select * from IMGRestaurant where seoKeyword='%@' and cityId=%@ limit 1",keyWord,cityId];
//    FMResultSet *resultSet1 = [[DBManager manager] executeQuery:sqlString];
//    if ([resultSet1 next]) {
//        restaurant = [[IMGRestaurant alloc]init];
//        [restaurant setValueWithResultSet:resultSet1];
//        [resultSet1 close];
//    }else{
//        [resultSet1 close];
//        sqlString = [NSString stringWithFormat:@"select * from IMGRestaurant where seoKeyword like '%@%@' and cityId=%@ limit 1",keyWord,@"%",cityId];
//        FMResultSet *resultSet2 = [[DBManager manager] executeQuery:sqlString];
//        if ([resultSet2 next]) {
//            restaurant = [[IMGRestaurant alloc]init];
//            [restaurant setValueWithResultSet:resultSet2];
//            
//        }
//        [resultSet2 close];
//    }
//    return restaurant;
//}

- (void)shareButtonClick {
    CGFloat yHeight = 310.0f + 52.0f;
    poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(0, DeviceHeight-yHeight, DeviceWidth, yHeight)];
    poplistview.delegate = self;
    poplistview.datasource = self;
    poplistview.listView.scrollEnabled = FALSE;
    [poplistview setTitle:@"Share"];
    [poplistview show];
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier];
    
    UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake(15*2, 12, 30, 30)];
    [cell.contentView addSubview:iconImage];
    
    Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(70, 15, DeviceWidth-70, 23) andTextFont:[UIFont systemFontOfSize:15] andTextColor:[UIColor color333333] andTextLines:0];
    [cell.contentView addSubview:titleLabel];
    
    NSInteger row = indexPath.row;
    
    if(row == 0){
        titleLabel.text = @"Facebook";
        titleLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:15];
        titleLabel.textColor = [UIColor color222222];
        iconImage.image = [UIImage imageNamed:@"facebook.png"];
    }else if (row == 1){
        titleLabel.text = @"Twitter";
        titleLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:15];
        titleLabel.textColor = [UIColor color222222];
        iconImage.image = [UIImage imageNamed:@"twitter.png"];
    }else if (row == 2){
        titleLabel.text = @"Message";
        titleLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:15];
        titleLabel.textColor = [UIColor color222222];
        iconImage.image = [UIImage imageNamed:@"message.png"];
    }else if (row == 3){
        titleLabel.text = @"Email";
        titleLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:15];
        titleLabel.textColor = [UIColor color222222];
        iconImage.image = [UIImage imageNamed:@"email.png"];
    }else if (row == 4)
    {
        titleLabel.text = @"Copy link";
        titleLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:15];
        titleLabel.textColor = [UIColor color222222];
        iconImage.image = [UIImage imageNamed:@"copyImage"];
    }
    else{
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:18];
        cell.textLabel.textColor = [UIColor colorWithRed:23/255.0 green:127/255.0 blue:252/255.0 alpha:1];
        cell.textLabel.text = @"Cancel";
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

#pragma mark - UIPopoverListViewDelegate
- (void)shareWithCopyTo
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    NSString *shareContentString = self.webSite;
    pasteboard.string = shareContentString;
}
- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0){
        [self shareWithFacebook];
    }else if(indexPath.row==1){
        [self shareWithTwitter];
    }else if(indexPath.row==2){
        [self shareWithSMS];
    }else if(indexPath.row==3){
        [self shareWithEmail];
    }
    else if (indexPath.row == 4)
    {
        [self shareWithCopyTo];
    }
    [popoverListView dismiss];
}

-(void)shareWithTwitter{
    [[LoadingView sharedLoadingView]startLoading];
    NSString *shareContentString = @"";
    SLComposeViewController *slComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    [slComposerSheet setInitialText:shareContentString];
    [slComposerSheet addURL:[NSURL URLWithString:self.webSite]];
    if(slComposerSheet){
        [self presentViewController:slComposerSheet animated:YES completion:^{
            [[LoadingView sharedLoadingView]stopLoading];
        }];
        [slComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            NSString *output;
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    output = @"Action Cancelled";
                    break;
                case SLComposeViewControllerResultDone:{
                    output = @"Post Successful";
//                    [MixpanelHelper trackShareToTwitterWithshareObject:self.title andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
                }
                    break;
                default:
                    break;
            }
            if (result != SLComposeViewControllerResultCancelled){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter Message" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
        }];
    }
}

-(void)shareWithFacebook{
    [[LoadingView sharedLoadingView]startLoading];
    NSString *shareContentString = @"";
    SLComposeViewController *slComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [slComposerSheet setInitialText:shareContentString];
    [slComposerSheet addURL:[NSURL URLWithString:self.webSite]];
    if(slComposerSheet){
        [self presentViewController:slComposerSheet animated:YES completion:^{
            [[LoadingView sharedLoadingView]stopLoading];
        }];
        
        [slComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result){
            NSString *output;
            switch(result){
                case SLComposeViewControllerResultCancelled:
                    output = @"Action Cancelled";
                    break;
                case SLComposeViewControllerResultDone:{
                    output = @"Post Successful";
//                    [MixpanelHelper trackShareToFacebookWithshareObject:self.title andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
                }
                    break;
                default:
                    break;
            }
            if (result != SLComposeViewControllerResultCancelled){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook Message" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
        }];
    }
}

-(void)shareWithEmail{
    if ([MFMailComposeViewController canSendMail]){
        NSString *subjectString = @"";
        
        NSString *shareContentString = [NSString stringWithFormat:@"<a href='%@'>%@</a> on Qraved. ",self.webSite,self.title];
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:subjectString];
        [mailer setMessageBody:shareContentString isHTML:YES];
        [self presentViewController:mailer animated:YES completion:nil];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Oops! This device does not support email."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
    }
}

-(void)shareWithSMS{
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Oops! This device does not support sms."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    NSString *shareContentString = [NSString stringWithFormat:@"%@ on Qraved. Open %@ to check out.",self.title,self.webSite];
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setBody:shareContentString];
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
//            [MixpanelHelper trackShareToEmailWithshareObject:self.title andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
            break;
        case MFMailComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Failed to send E-Mail!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
        }
            break;
            
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
//            [MixpanelHelper trackShareToSMSWithshareObject:self.title andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 53.0f;
}

 
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
