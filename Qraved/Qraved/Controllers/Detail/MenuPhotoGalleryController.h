//
//  MenuPhotoGalleryController.h
//  Qraved
//
//  Created by imaginato on 16/4/6.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIAlertView+BlocksKit.h"
#import "UIConstants.h"
#import "BaseChildViewController.h"
#import "IMGRestaurant.h"
#import "IMGMenu.h"
@interface MenuPhotoGalleryController : BaseChildViewController<UIScrollViewDelegate>

@property (nonatomic,assign)BOOL isSelfUpload;
@property (nonatomic,assign) BOOL isUseWebP;
-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant;
-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant andIsSelfUpload:(BOOL)isSelfUpload;
@end
