//
//  PhotosDetailViewController.m
//  Qraved
//
//  Created by Laura on 14-8-6.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "PhotosDetailViewController.h"

#import "DetailViewController.h"
#import "EScrollerView.h"
#import "PhotosEScrollerView.h"

#import "UIDevice+Util.h"
#import "NSString+Helper.h"
#import "UIViewController+Helper.h"
#import "Label.h"
#import "SearchPhotosViewController.h"
#import "Date.h"

#import "IMGDish.h"

@interface PhotosDetailViewController ()
{
    NSArray *photosArray;
    long photoTag;
    Label *numLabel;
    Label *userLabel;
    Label *createLabel;
    Label *nameLabel;
    BOOL isFromDetail;
    IMGRestaurant *currentRestaurant;
}
@end

@implementation PhotosDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithPhotosArray:(NSArray *)photos andPhotoTag:(NSInteger)tag andRestaurant:(IMGRestaurant *)restaurant andFromDetail:(BOOL)isDetail
{
    self=[super init];
    if(self)
    {
        photosArray = photos;
        photoTag = tag;
        isFromDetail = isDetail;
        currentRestaurant = restaurant;
        self.showMenu = YES;
        self.showPage = YES;
    }
    
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    if(self.showMenu){
        self.navigationController.navigationBarHidden =  NO;
    }else{
        self.navigationController.navigationBarHidden =  YES;
        self.view.backgroundColor=[UIColor color333333];
    }
    [super viewWillAppear:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    UIImageView *closeImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ReviewClosed"]];
//    closeImageView.frame = CGRectMake(DeviceWidth - 44, 0, 44, 44);
//    [self.view addSubview:closeImageView];
    if(self.useNew){
        PhotosEScrollerView *scrollView = [[PhotosEScrollerView alloc] initWithFrameRect:CGRectMake(0, 0, DeviceWidth, DeviceHeight-44) objectArray:photosArray isStartScroll:NO withPhotoTag:photoTag pageControlCenter:YES isCirculation:YES isPageControl:self.showPage isEvent:NO isPlaceHolder:NO sizeFitWidth:NO];
        [self.view addSubview:scrollView];
        scrollView.delegate = self;
    }else{
        EScrollerView *scrollView = [[EScrollerView alloc] initWithFrameRect:CGRectMake(0, 0, DeviceWidth, DeviceHeight+20) objectArray:photosArray isStartScroll:NO withPhotoTag:photoTag pageControlCenter:YES isCirculation:YES isPageControl:self.showPage isEvent:NO isPlaceHolder:NO sizeFitWidth:NO];
        [self.view addSubview:scrollView];
        scrollView.delegate = self;
    }
    
    if(self.showMenu){
        [self addNavigationBar];    
    }
    else
    {
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [closeButton setImage:[UIImage imageNamed:@"ReviewClosed"] forState:UIControlStateNormal];
        closeButton.frame = CGRectMake(DeviceWidth - 54, 0, 80, 80);
        [closeButton setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
        [self.view addSubview:closeButton];
        [closeButton addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];

    }
    [self showPhotoNumAndName];
    
}
-(void)addNavigationBar
{
    self.view.backgroundColor=[UIColor blackColor];
    [self setBackBarButtonOffset30];

    self.navigationItem.title=self.title;

    UIBarButtonItem *menuBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"MenuTop"] style:UIBarButtonItemStylePlain target:self action:@selector(menuClick)];
    menuBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=menuBtn;
}
-(void)backClick{
    int viewCount = isFromDetail?2:3;
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-viewCount] animated:YES];
}
-(void)menuClick{
    if (isFromDetail){
        SearchPhotosViewController *searchPhotosViewController=[[SearchPhotosViewController alloc]initWithRestaurant:currentRestaurant];
        [self.navigationController pushViewController:searchPhotosViewController animated:YES];
    }
    else
        [self.navigationController popViewControllerAnimated:YES];
}

-(void)showPhotoNumAndName
{

    CGFloat margin_top=10;
    userLabel=[[Label alloc] initWithFrame:CGRectMake(LEFTLEFTSET,margin_top,DeviceWidth-LEFTLEFTSET-100,18)];
    userLabel.font=[UIFont systemFontOfSize:16];
    userLabel.textColor=[UIColor whiteColor];
    createLabel=[[Label alloc] initWithFrame:CGRectMake(LEFTLEFTSET,margin_top+20,DeviceWidth-LEFTLEFTSET-100,14)];
    createLabel.font=[UIFont systemFontOfSize:12];
    createLabel.textColor=[UIColor grayColor];
    [self.view addSubview:userLabel];
    [self.view addSubview:createLabel];

    UIImageView *photoImage = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-LEFTLEFTSET-100-20, margin_top, 10, 10)];
    photoImage.image = [UIImage imageNamed:NavigationPhotoImage];
    
    [self.view addSubview:photoImage];

    float numMargin=margin_top+8;

    numLabel = [[Label alloc]initWithFrame:CGRectMake(DeviceWidth-LEFTLEFTSET-100,numMargin, 100, 12) andTextFont:[UIFont fontWithName:FONT_GOTHAM_LIGHT size:12] andTextColor:[UIColor whiteColor] andTextLines:1];
    [self.view addSubview:numLabel];
    numLabel.backgroundColor = [UIColor clearColor];
    numLabel.textAlignment = NSTextAlignmentLeft;
    numLabel.text = [NSString stringWithFormat:@"%ld / %lu",photoTag+1,(unsigned long)photosArray.count];
    CGSize size = [numLabel.text sizeWithFont:numLabel.font constrainedToSize:CGSizeMake(numLabel.frame.size.width, 10000) lineBreakMode:NSLineBreakByWordWrapping];

    if(self.showMenu){
        numLabel.frame=CGRectMake(DeviceWidth-10-size.width,numMargin,size.width,12);
        photoImage.frame=CGRectMake(DeviceWidth-10-size.width-15,numMargin,10,10);
    }else{
        numLabel.frame=CGRectMake((DeviceWidth-size.width)/2+7,numMargin+44,size.width,12);
        photoImage.frame=CGRectMake((DeviceWidth-size.width)/2-8,numMargin+44,10,10);
    }

    UIView *dishNameView;

    if(self.useNew){
        dishNameView = [[UIView alloc]initWithFrame:CGRectMake(0, DeviceHeight-44, DeviceWidth, 44)];
    }else{
        dishNameView = [[UIView alloc]initWithFrame:CGRectMake(0, DeviceHeight-88, DeviceWidth, 44)];
    }

    dishNameView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:dishNameView];
    nameLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 0, DeviceWidth-LEFTLEFTSET, 44) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12] andTextColor:[UIColor whiteColor] andTextLines:1];
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.textAlignment = NSTextAlignmentLeft;
    IMGDish *dish=[photosArray objectAtIndex:photoTag];
    if(dish.title && dish.title.length){

        nameLabel.text = dish.title;
    }else{

        nameLabel.text = dish.descriptionStr;  
    }
    if(dish.userName!=nil && ![dish.userName isBlankString] && [dish.createTime longLongValue]){
        userLabel.text=[NSString stringWithFormat:@"by %@",dish.userName];

        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        createLabel.text = [Date getTimeInteval_v4:[dish.createTime longLongValue]/1000];
    }else{
        userLabel.text=@"";
        createLabel.text=@"";
    }
    
    [dishNameView addSubview:nameLabel];
}

-(void)EScrollerViewDidClicked:(NSUInteger)index ContentOffset:(CGPoint)contentoffset{
    numLabel.text = [NSString stringWithFormat:@"%lu/%lu",(unsigned long)index,(unsigned long)photosArray.count];
    IMGDish *dish=[photosArray objectAtIndex:index-1];
    if(dish.title && dish.title.length){

        nameLabel.text = dish.title;
    }else{

        nameLabel.text = dish.descriptionStr;  
    }
    if(dish.userName!=nil && ![dish.userName isBlankString] && [dish.createTime longLongValue]){
        userLabel.text=[NSString stringWithFormat:@"by %@",dish.userName];
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
        createLabel.text = [Date getTimeInteval_v4:[dish.createTime longLongValue]/1000];
    }else{
        userLabel.text=@"";
        createLabel.text=@"";
    }
}

-(void)EScrollerViewDidClicked:(NSUInteger)index{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)PhotosEScrollerViewDidClicked:(NSUInteger)index ContentOffset:(CGPoint)contentoffset{
    numLabel.text = [NSString stringWithFormat:@"%lu/%lu",(unsigned long)index,(unsigned long)photosArray.count];
    IMGDish *dish=[photosArray objectAtIndex:index-1];
    if(dish.title && dish.title.length){

        nameLabel.text = dish.title;
    }else{

        nameLabel.text = dish.descriptionStr;  
    }
}

-(void)PhotosEScrollerViewDidClicked:(NSUInteger)index{
    [self.navigationController popViewControllerAnimated:YES];
}

 

@end
