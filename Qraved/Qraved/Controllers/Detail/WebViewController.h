//
//  WebViewController.h
//  Qraved
//
//  Created by mac on 14-11-12.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "IMGMediaComment.h"

@interface WebViewController : BaseViewController
@property (nonatomic,retain) NSString *webSite;

@property (nonatomic,retain) NSString *content;
@property (nonatomic,assign) BOOL fromDetail;
@property (nonatomic)        IMGMediaComment *journal;
@property (nonatomic,assign) BOOL isHaveTitle;

@end
