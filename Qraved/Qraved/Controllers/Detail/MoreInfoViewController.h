//
//  AboutViewController.h
//  Qraved
//
//  Created by Shine Wang on 8/2/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "UIPopoverListView.h"
#import "IMGRestaurant.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
@interface MoreInfoViewController :UIViewController<UIWebViewDelegate,UIPopoverListViewDataSource,UIPopoverListViewDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate>

-(id)initWithRestaurant:(IMGRestaurant *)tmpRestaurant;

@end
