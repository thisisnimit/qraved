//
//  MenuDetailViewController.h
//  Qraved
//
//  Created by Laura on 14-8-1.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"

@interface MenuDetailViewController : BaseViewController

-(id)initMenusArray:(NSArray*)menus andRestaurantName:(NSString *)resName andSectionTypeName:(NSString *)sectionTypeName andSectionName:(NSString *)sectionName;
-(id)initWithSectionId:(NSNumber*)sectionId andRestaurantName:(NSString *)resName andSectionTypeName:(NSString *)sectionTypeName andSectionName:(NSString *)sectionName;
@end
