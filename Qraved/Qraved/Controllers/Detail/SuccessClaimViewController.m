//
//  SuccessClaimViewController.m
//  Qraved
//
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "SuccessClaimViewController.h"
#import "NavigationBarButtonItem.h"
#import "UIDevice+Util.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import "DetailViewController.h"
#import "UIColor+Hex.h"

@interface SuccessClaimViewController ()

@end

@implementation SuccessClaimViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavagationBack];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationItem setTitle:@"Claim Your Restaurant"];
    [self loadMainView];
}

-(void)loadMainView{
    UIImageView *laughImageView = [[UIImageView alloc] initWithFrame:CGRectMake((DeviceWidth-50)/2, 55, 50, 50)];
    laughImageView.image = [UIImage imageNamed:@"claimSuccessLaughImage@2x.png"];
    [self.view addSubview:laughImageView];
    //label1
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake((DeviceWidth - 250)/2, laughImageView.endPointY+25, 250, 20)];
    label1.text= L(@"Congratulation!");
    label1.font=[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:17];
    label1.textColor=[UIColor colorWithHexString:@"#141823"];
    label1.textAlignment = NSTextAlignmentCenter;
    label1.numberOfLines = 0;
    [label1 sizeToFit];
    
    CGRect label1Frame = label1.frame;
    label1Frame.origin.x = (DeviceWidth - label1Frame.size.width)/2;
    label1Frame.origin.y = laughImageView.endPointY+25;
    label1.frame = label1Frame;
    
    [self.view addSubview:label1];
    //label2
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake((DeviceWidth - 250)/2, label1.endPointY+40, 250, 60)];
    label2.text= L(@"You're one step closer to\n grow your business even bigger!\n Thank you for connecting with us!");
    label2.font=[UIFont fontWithName:DEFAULT_FONT_NAME size:14];
    label2.textColor=[UIColor colorWithHexString:@"#141823"];
    label2.textAlignment = NSTextAlignmentCenter;
    label2.numberOfLines = 0;
    [label2 sizeToFit];
    
    CGRect label2Frame = label2.frame;
    label2Frame.origin.x = (DeviceWidth - label2Frame.size.width)/2;
    label2Frame.origin.y = label1.endPointY+40;
    label2.frame = label2Frame;
    
    [self.view addSubview:label2];
    //backToBtn
    UILabel *backToBtnLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, label2.endPointY+17, DeviceWidth, 30)];
    backToBtnLabel.text = [NSString stringWithFormat:@"Back to %@",self.restaurantTitle];
    backToBtnLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:14];
    backToBtnLabel.textColor=[UIColor colorWithHexString:@"#4990e2"];
    backToBtnLabel.textAlignment = NSTextAlignmentCenter;
    backToBtnLabel.numberOfLines = 0;
    backToBtnLabel.userInteractionEnabled = YES;
    [backToBtnLabel sizeToFit];
    
    CGRect backToBtnLabelFrame = backToBtnLabel.frame;
    backToBtnLabelFrame.origin.x = (DeviceWidth - backToBtnLabelFrame.size.width)/2.0;
    backToBtnLabelFrame.origin.y = label2.endPointY+17;
    backToBtnLabel.frame = backToBtnLabelFrame;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backToBtnClick)];
    [tap setNumberOfTapsRequired:1];
    [tap setNumberOfTouchesRequired:1];
    [backToBtnLabel addGestureRecognizer:tap];

    [self.view addSubview:backToBtnLabel];
}

-(void)backToBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

-(void)setNavagationBack
{
    NavigationBarButtonItem *navigationBarLeftButton = [[NavigationBarButtonItem alloc ]initWithImage:[UIImage imageNamed:NavigationBackImage] andTitle:@"" target:self action:@selector(goToBackViewController) width:43.0f offset:-15-[UIDevice heightDifference]];
    self.navigationItem.leftBarButtonItem = navigationBarLeftButton;
}



 
@end
