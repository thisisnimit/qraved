//
//  MenuPhotoViewColltroller.h
//  Qraved
//
//  Created by Evan on 16/4/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "IMGRestaurant.h"

@interface MenuPhotoViewColltroller : BaseViewController

@property (nonatomic , assign) BOOL ifUnauditedData;
@property (nonatomic , assign) BOOL ifFromJournal;
@property (nonatomic , strong) NSNumber *menuPhotoCount;
@property(nonatomic,retain) NSString *amplitudeType;
@property (nonatomic,assign) BOOL isUseWebP;
-(id)initWithPhotosArray:(NSMutableArray *)photos andPhotoTag:(NSInteger)photoTag andRestaurant:(IMGRestaurant *)restaurant andFromDetail:(BOOL)isDetail;
-(void)updateRestautantDetailData ;
@end
