//
//  MenusViewController.m
//  Qraved
//
//  Created by Shine Wang on 8/2/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

CGFloat const MenuImageWidth=90;
CGFloat const MenuImageHeight=90;

#import "SectionViewController.h"
#import "AppDelegate.h"
#import "Tools.h"

#import "MenuDetailViewController.h"

#import "UIViewController+Helper.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "Label.h"
#import "LoadingView.h"
#import "DetailViewController.h"
#import "NodataView.h"
#import "IMGMenu.h"
#import "IMGUser.h"
#import "DBManager.h"
#import "RestaurantHandler.h"

#define CONTENT_WIDTH 300

@implementation SectionViewController{
    CGFloat contentOffsetY;
    NodataView *nodataView;
    
    NSMutableDictionary *sectionDict;
    NSDictionary *sectionIdDict;
    IMGRestaurant *restaurant;
    NSMutableArray *sectionTypeNameArray;
    NSMutableArray *menuArrary;
    IMGUser *user;
}

-(id)initWithRestaurant:(IMGRestaurant *)rest{
    if (self = [super init]) {
        restaurant = rest;
        user=[IMGUser currentUser];
        sectionDict=[[NSMutableDictionary alloc] init];
        sectionTypeNameArray=[[NSMutableArray alloc]initWithCapacity:0];
        [[LoadingView sharedLoadingView] startLoading];
        NSDictionary *params=nil;
        if(user && user.userId){
            params=@{@"restaurantId":rest.restaurantId,@"userId":user.userId,@"menuUnsign":[NSNumber numberWithInt:1]};    
        }else{
            params=@{@"restaurantId":rest.restaurantId};
        }
        __block __weak __typeof(self)weakSelf = self;
        [RestaurantHandler getSectionsFromServer:params andBlock:^(BOOL status,NSDictionary *sections,NSDictionary *ids){
            [[LoadingView sharedLoadingView] stopLoading];
            sectionDict=[sections mutableCopy];
            sectionIdDict=ids;
            [weakSelf.tableView reloadData];
        }];
        
    }
    return self;
}

-(void)cancelSectionNewFlag:(NSInteger)index{
    NSMutableDictionary *mutableSectionArr=[[NSMutableDictionary alloc] initWithDictionary:sectionDict];
    NSArray *typeNames=[sectionDict allKeys];
    NSString *typeName=[typeNames objectAtIndex:index];
    NSArray *sectionArr=[sectionDict objectForKey:typeName];
    NSString *sectionName=[[sectionArr objectAtIndex:index] objectForKey:@"name"];

    for(NSDictionary *section in sectionArr){
        if([[section objectForKey:@"name"] isEqualToString:sectionName]){
            [[[mutableSectionArr objectForKey:sectionName] objectAtIndex:[sectionArr indexOfObject:section]] setObject:[NSNumber numberWithBool:0] forKey:@"hasNewSign"];
            break;
        }
    }

    sectionDict=mutableSectionArr;
    [self.tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self setBackBarButtonOffset30];
    
    [self.navigationItem setTitle:restaurant.title];
    self.navigationController.navigationBarHidden = NO;
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    UIView *headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 44)];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 2, DeviceWidth - 2*LEFTLEFTSET, 40)];
    [headView addSubview:titleLabel];
    titleLabel.text = @"Menu";
    titleLabel.font = [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:20];
    self.tableView.tableHeaderView = headView;
    self.tableView.frame= CGRectMake(0, 0, DeviceWidth,DeviceHeight-44);
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //标题下阴影
    UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:0 andHeight:4];
    [self.view addSubview:shadowImageView];
    
}
 

#pragma mark tableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [sectionDict allKeys].count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    NSString *typeName=[[sectionDict allKeys] objectAtIndex:section];
    NSArray *sectionArr=[sectionDict objectForKey:typeName];
    if(sectionArr!=nil){
        return sectionArr.count;
    }else{
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 36;
}

-(UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"cellIdentifier";
    UITableViewCell *cell = [theTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSArray *typeNames=[sectionDict allKeys];
    NSString *typeName=[typeNames objectAtIndex:indexPath.section];
    NSArray *sectionArr=[sectionDict objectForKey:typeName];
    NSString *sectionName=[[sectionArr objectAtIndex:indexPath.row] objectForKey:@"name"];
    BOOL hasNewSign=[[[sectionArr objectAtIndex:indexPath.row] objectForKey:@"hasNewSign"] boolValue];
    
    if (cell == nil) {
        cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 1)];
        lineImage.backgroundColor = [UIColor colorEEEEEE];
        [cell addSubview:lineImage];
    }
    for (UIView *view in cell.contentView.subviews) {
        if ([view isKindOfClass:[UIImageView class]]) {
            [view removeFromSuperview];
        }
        
    }
    
    if (indexPath.row == sectionArr.count-1) {
        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 35, DeviceWidth, 1)];
        lineImage.backgroundColor = [UIColor colorEDEDED];
        [cell.contentView addSubview:lineImage];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = [sectionName filterHtml];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    UIImageView *imageView=[cell viewWithTag:199];
    if(hasNewSign){
        if(imageView==nil){
            imageView=[[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-60,2,32,32)];
            imageView.tag=199;
            imageView.image=[UIImage imageNamed:@"newflag"];
            [cell addSubview:imageView];
        }
        imageView.hidden=NO;
    }else{
        imageView.hidden=YES;
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *typeNames=[sectionDict allKeys];
    NSString *typeName=[typeNames objectAtIndex:indexPath.section];
    NSMutableArray *sectionArr=[[sectionDict objectForKey:typeName] mutableCopy];
    NSMutableDictionary *section=[[sectionArr objectAtIndex:indexPath.row] mutableCopy];
    NSString *sectionName=[section objectForKey:@"name"];
    [section setObject:[NSNumber numberWithBool:0] forKey:@"hasNewSign"];
    [sectionArr replaceObjectAtIndex:indexPath.row withObject:section];
    [sectionDict setObject:sectionArr forKey:typeName];

    menuArrary=[[NSMutableArray alloc] initWithCapacity:0];
    NSNumber *sectionId=[sectionIdDict objectForKey:[NSString stringWithFormat:@"%@%@",typeName,sectionName]];
    // [self cancelSectionNewFlag:indexPath.row];
    MenuDetailViewController *menuDetail = [[MenuDetailViewController alloc] initWithSectionId:sectionId andRestaurantName:restaurant.title andSectionTypeName:typeName andSectionName:sectionName];
    [self.navigationController pushViewController:menuDetail animated:YES];
    [self.tableView reloadData];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    Label *sectionHeader = [[Label alloc] initWithFrame:CGRectMake(LEFTLEFTSET, 10, DeviceWidth-2*LEFTLEFTSET, 30) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:10] andTextColor:[UIColor color999999] andTextLines:1];
    sectionHeader.backgroundColor = [UIColor clearColor];
    NSArray *typeNames=[sectionDict allKeys];
    sectionHeader.text = [[typeNames objectAtIndex:section] uppercaseString];
    

    [headerView addSubview:sectionHeader];
    return headerView;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"backToMenuListNotification" object:nil];
}
@end
