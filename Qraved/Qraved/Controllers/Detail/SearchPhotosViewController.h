//
//  SearchPhotosViewController.h
//  Qraved
//
//  Created by Admin on 8/2/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIAlertView+BlocksKit.h"
#import "UIConstants.h"
#import "BaseChildViewController.h"
#import "IMGRestaurant.h"
@interface SearchPhotosViewController : BaseChildViewController<UIScrollViewDelegate>

@property(nonatomic,assign) BOOL isFromSearch;
@property (nonatomic,assign) BOOL isUseWebP;
-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant;
-(instancetype)initWithRestaurant:(IMGRestaurant*)paramRestaurant restaurantPhotoArray:(NSArray*)photoArr;
- (void)getDishesFromServer:(int)restaurantId max:(int)max;

@end
