//
//  SuggestMapViewController.h
//  Qraved
//
//  Created by Lucky on 15/5/6.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"
#import <MapKit/MapKit.h>

@protocol SuggestMapViewControllerDelegate <NSObject>

-(void)changeMap:(NSDictionary*)dic;

@end

@interface SuggestMapViewController : UIViewController<MKMapViewDelegate>
-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant;
@property (nonatomic,weak)id<SuggestMapViewControllerDelegate>delegate;
@end
