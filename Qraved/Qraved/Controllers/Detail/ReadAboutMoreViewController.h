//
//  ReadAboutMoreViewController.h
//  Qraved
//
//  Created by Laura on 14-8-27.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "IMGRestaurant.h"

@interface ReadAboutMoreViewController : BaseViewController<UIWebViewDelegate>

-(id)initWithRestaurant:(IMGRestaurant *)restaurant;
@end
