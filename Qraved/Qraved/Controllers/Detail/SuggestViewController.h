//
//  SuggestViewController.h
//  Qraved
//
//  Created by Lucky on 15/4/20.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"
#import <MapKit/MapKit.h>
#import "BaseViewController.h"

@class CSLinearLayoutView;

@interface SuggestViewController : BaseViewController<MKMapViewDelegate>

@property (nonatomic, retain) CSLinearLayoutView *linearLayoutView;

-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant;
@end
