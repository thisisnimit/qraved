//
//  EventsAndDiningViewController.h
//  Qraved
//
//  Created by Lucky on 15/6/10.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIKit.h>
#import "BaseChildViewController.h"

@class IMGEvent,IMGDiningGuide;

@interface EventsAndDiningViewController : BaseChildViewController

- (instancetype)initWithDiningGuideArr:(NSArray *)diningGuideArray andRestaurantName:(NSString *)name;
@property (nonatomic,retain) NSNumber *restaurantId;


-(void)loadEventAndDinningGuide:(void(^)(BOOL))block;
-(void)goToEvent:(IMGEvent*)event;
-(void)goToDinningGuide:(IMGDiningGuide*)diningGuide;

@end
