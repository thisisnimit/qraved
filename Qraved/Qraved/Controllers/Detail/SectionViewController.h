//
//  MenusViewController.h
//  Qraved
//
//  Created by Shine Wang on 8/2/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

extern CGFloat const MenuImageWidth;
extern CGFloat const MenuImageHeight;

#import <UIKit/UIKit.h>
#import "PostDataHandler.h"
#import "BaseAnimationController.h"
#import "IMGRestaurant.h"

@interface SectionViewController : BaseAnimationController


-(id)initWithRestaurant:(IMGRestaurant *)restaurant;
@end
