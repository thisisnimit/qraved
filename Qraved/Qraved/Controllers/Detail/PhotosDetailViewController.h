//
//  PhotosDetailViewController.h
//  Qraved
//
//  Created by Laura on 14-8-6.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "IMGRestaurant.h"
#import "AppDelegate.h"
#import "EScrollerView.h"
#import "PhotosEScrollerView.h"
@protocol EScrollerViewDelegate;
@protocol PhotosEScrollerViewDelegate;

@interface PhotosDetailViewController : BaseChildViewController<UIScrollViewDelegate,EScrollerViewDelegate,PhotosEScrollerViewDelegate>

//@property (nonatomic,copy) NSString *title;

@property (nonatomic,assign) BOOL showMenu;
@property (nonatomic,assign) BOOL popDismiss;
@property (nonatomic,assign) BOOL useNew;
@property (nonatomic,assign) BOOL showPage;

-(id)initWithPhotosArray:(NSArray *)photos andPhotoTag:(NSInteger)tag andRestaurant:(IMGRestaurant *)restaurant andFromDetail:(BOOL)isDetail;
@end
