//
//  DetailViewController.m
//  Qraved
//
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

float const BANNER_LABEL_WIDTH=250.0;
float const BANNER_LABEL_HEIGHT=12.0;

#import "DetailViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreText/CoreText.h>
#import "AddPhotoViewController.h"
#import "SelectPaxViewController.h"
#import "DiningGuideRestaurantsViewController.h"
#import "IMGDiningGuide.h"
#import "RestaurantsViewController.h"
#import "SearchUtil.h"
#import "ReadAboutMoreViewController.h"
#import "SearchPhotosViewController.h"
#import "ReviewViewController.h"
#import "MapViewController.h"
#import "SpecialOffersViewController.h"
#import "WebViewController.h"
#import "EGORefreshTableHeaderView.h"
#import "ColorUtil.h"
#import "Label.h"
#import "Helper.h"
#import "DLStarRatingControl.h"
#import "SendCall.h"
#import "ReviewView.h"
#import "LoadingView.h"
#import "LoadingImageView.h"
#import "IMGOperatingState.h"
#import "BookUtil.h"
#import "IMGEvent.h"
#import "IMGMenu.h"
#import "IMGTag.h"
#import "IMGUser.h"
#import "IMGRestaurantImage.h"
#import "IMGRestaurantDiningGuide.h"
#import "IMGRestaurantOffer.h"
#import "IMGMediaComment.h"
#import "DetailUtil.h"
#import "IMGRestaurantLink.h"
#import "MoreArticleViewController.h"
#import "CameraViewController.h"
#import "UIButton+WebCache.h"
#import "IMGUploadPhoto.h"
#import "ReviewPublishViewController.h"
#import "DetailDataHandler.h"
#import "AddToListController.h"
#import "ReviewActivityItemProvider.h"
#import "OtherProfileViewController.h"
#import "MenuPhotoGalleryController.h"
#import <GoogleAnalytics/GAIFields.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import "UserAllReviewController.h"
#import "DetailButtonsView.h"
#import "DetaiPhotosView.h"
#import "DetailMenuView.h"
#import "DetailFeatureInView.h"
#import "DBManager.h"
#import "RestaurantHandler.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "IMGDish.h"
#import "AlbumViewController.h"
#import "SuggestViewController.h"
#import "MenuPhotoViewColltroller.h"
#import "UIViewController+LewPopupViewController.h"
#import "trackHandler.h"
#import "EventsAndDiningViewController.h"
#import "RestaurantActivityItemProvider.h"
#import "TwitterActivityItemProvider.h"
#import "JournalListViewController.h"
#import "JournalDetailViewController.h"
#import "JournalHandler.h"
#import "EntityKind.h"
#import "UILikeCommentShareView.h"
#import "ReviewHandler.h"
#import "IMGReviewComment.h"
#import "IMGRDPRestaurant.h"
#import "LoadingImageViewController.h"
#import "MenuPublishViewController.h"
#import "ClaimYourRestaurantViewController.h"
#import "SuccessClaimViewController.h"
#import "SVProgressHUD.h"
//#import <UberRides/UberRides-Swift.h>
#import "DetailViewInformationView.h"
#import "DetailCallNowView.h"
#import "DetailReviewView.h"
#import "V2_CommentListViewController.h"
#import "ReviewInstagramView.h"
#import "V2_InstagramModel.h"
#import "V2_PhotoDetailViewController.h"
#import "DetailInformationView.h"
#import "DetailCouponView.h"
#import "CouponDetailViewController.h"
#import "DetailBottomButtonView.h"
#import "PlaceholderView.h"
#import "CouponListViewController.h"

#define navigationHeight  (44+[UIDevice heightDifference])
#define REFRESH_HEADER_HEIGHT 52.0f
#define CONTENT_OFFSET 5
#define TOP_SCROLL_HEIGHT 180
#define NAV_HEIGHT 35
#define mapViewHeight 100
#define SHADOWHEIGHT 8
#define CELLOFFSET 6
#define SCORLLVIEW_Y 23
#define TITLE_MARGIN 15
#define BTN_HEIGHT 35
#define NAVIGATIONBARHEIGHT 88
#define EDIT_REVIEW L(@"Edit Review")
#define Write_REVIEW L(@"Write Review")

#import "DetailSeeAllPhotoViewController.h"
#import "DetailSeeMenuViewController.h"
#import "DetailSeeMenuUserIDViewController.h"
#import "InstagramPlacesViewController.h"
@interface DetailViewController ()<EGORefreshTableHeaderDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,DLStarRatingDelegate,LikeCommentShareDelegate,ClaimYourRestaurantDelegate,DetailButtonsViewDelegate,DetaiPhotosViewDelegate,DetailMenuViewDelegate,DetailFeatureInViewDelegate,DetailViewInformationViewDelegate,DetailCallNowViewDelegate,ReviewPublishViewControllerDelegate,MenuPublishViewControllerDelegate,DetailReviewViewDelegate,ReviewInstagramViewDelegate,DetailBottomButtonViewDelegate>

/*old*/

@property(nonatomic,retain)UIScrollView *scrollView;
@property(nonatomic,strong)ReviewViewController *reviewViewController;
@property (nonatomic ,strong) MenuPhotoGalleryController *menuPhotoGalleryController;
@property(nonatomic,retain)EScrollerView *banerImageView;
@property(nonatomic,retain) NSMutableArray * banerImageList;

@end

@implementation DetailViewController{
    IMGRestaurant *restaurant;
    IMGRDPRestaurant *rdpRest;
    
    CGFloat currentY;
    BOOL isLoading;
    EGORefreshTableHeaderView*_refreshTableHeaderView;
    LoadingImageView *loadingImageView;
    
    UIButton *backButton ,*shareButton;
    UIImageView *imgSaved;
    UILabel *navTitleLabel;
    
    UIView *mapView ,*reviewsView ,*bookButtonView;
    DetailMenuView *menuView;
    DetailFeatureInView *featuredInView;
    NSMutableArray *reviewsArray ,*restaurantOfferArray ,*regularTimeArray;
    DetailButtonsView *btnsView;
    NSMutableDictionary *dishDic;
    UIView *navigationView;
    Label *naviNameLabel;
    
    BOOL viewDidLoaded;
    BOOL isViewDidLoaded;

    NSMutableArray *mediaCommentArray;
    Label *photoNumLabel;
    NSData *imageData;
    NSMutableArray *userPhotoArray;
    NSMutableArray *eventPhotoArray;
    NSMutableArray *menuPhotoArray;
    
    DetailReviewView *reviewsView_v1;
    UIView *photosViewForDish;
    
    UILabel *reviewsStrLabelTop;
    UILabel *reviewsCountLableTop;
    UIButton *scoreBtnTop;
    NSMutableArray *diningGuideArray;
    
    IMGRestaurantOffer *restaurantOffer;
    NSArray *menuDataArr;
    NSArray *dishDataArr;
    NSMutableArray *moreDishDataArr;
    NSMutableArray *reviewDataArr;
    NSArray *eventDataArr;
    NSArray *eventImageDataArr;
    NSArray *diningGuideDataArr;
    NSArray *journalDataArr;
    UIView *loadingView;
    
    NSMutableArray *newReviewArrM;
    NSMutableArray *newDishArrM;
    NSArray *tutorialStrArr;
    
    IMGUserReview *userReviewTemp;
    
    DLStarRatingControl *startRatingM;
    UIImageView *newOfferBadgeImageView;
    
    BOOL isEditReview;
    NSInteger thisCommentIndex;

    NSMutableArray *reviewHeight;
    NSMutableDictionary *reveiwCardIsReadMore;
    NSMutableArray *commentReadMore;
    BOOL isLastDismissBtn;
    NSMutableArray *eventArray;
    BOOL isHadRamadanView;
    BOOL isHadStateView;
    BOOL isFromDBData;
    BOOL isLikeReview;
    BOOL isUnLikeReview;

    
    NSTimeInterval beginLoadingData;
    NSTimeInterval finishUI;
    //Label *restaurantTitleLabel;
    
    NSNumber *showClaim;
    BOOL bookbuttonIsClicked;
    float ratingCount;
    NSNumber *reviewID;
    NSNumber *_reviewedUserCount;
    UIView * shopView;
    UIView *starView;
    
    NSMutableArray *couponArr;
    PlaceholderView *vPlaceHolderView;
}
@synthesize scrollView;

@synthesize banerImageView;

#pragma mark - Init Method
-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant{
    if(self= [super init]){
        restaurant = paramRestaurant;
    }
    return self;
}
-(id)initWithRestaurantId:(NSNumber *)restaurantId{
    if(self= [super init]){
        restaurant = [[IMGRestaurant alloc]init];
        restaurant.restaurantId = restaurantId;
    }
    return self;
}

#pragma mark - life cycle
-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"viewWillAppear");
    [super viewWillAppear:YES];
    bookbuttonIsClicked=YES;
    self.navigationController.navigationBarHidden = YES;
    naviNameLabel.hidden = NO;
    if (self.amplitudeType!=nil) {
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:[self getPageIdByAmplitudeType] forKey:@"Origin"];
        
        if ([self.amplitudeTypePageId intValue]) {
            [eventProperties setValue:self.amplitudeTypePageId forKey:self.amplitudeTypePage];
        }
        [[Amplitude instance] logEvent:@"RC - View Restaurant Page" withEventProperties:eventProperties];
        [[AppsFlyerTracker sharedTracker] trackEvent:@"RC - View Restaurant Page" withValues:eventProperties];
    }
    [self countStar];
}
-(void)viewDidAppear:(BOOL)animated{
    NSLog(@"viewDidAppear");
    [super viewDidAppear:YES];
    
    if (viewDidLoaded==NO)
    {
        viewDidLoaded = YES;
//get data from cache
        if ([rdpRest.restaurantId intValue] && isFromDBData)
        {
            if ([rdpRest.isHadRDPSecondData intValue])
            {
                [self addOtherViewWithDB];
            }
            [self refreshViewWithDB];
        }
        
    }
    self.navigationController.navigationBarHidden = YES;
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.navigationController.navigationBarHidden=NO;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    self.navigationController.navigationBarHidden=NO;
     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    ratingCount = 0;
}
-(void)viewDidLoad{
    [super viewDidLoad];
    NSLog(@"viewDidLoad");
    beginLoadingData = [[NSDate date]timeIntervalSince1970];
    self.screenName = @"Restaurant detail page";
    reveiwCardIsReadMore = [[NSMutableDictionary alloc]init];
    newReviewArrM = [[NSMutableArray alloc] init];
    newDishArrM = [[NSMutableArray alloc] init];
    couponArr = [NSMutableArray array];
    dishDic = [[NSMutableDictionary alloc] init];
    isViewDidLoaded = YES;
    UIScrollView * mainScrollView = [[UIScrollView alloc] init];
    mainScrollView.contentSize = CGSizeMake(DeviceWidth, currentY);
    [mainScrollView setShowsVerticalScrollIndicator:NO];
    mainScrollView.scrollsToTop=YES;
    mainScrollView.delegate = self;
   if (@available(iOS 11.0, *)) {
       mainScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    imageData = [[NSData alloc] init];
    [mainScrollView setBackgroundColor:[UIColor whiteColor]];
    [mainScrollView setFrame:[[UIScreen mainScreen]bounds]];
    scrollView = mainScrollView;
    [self.view addSubview:scrollView];
    
    self.view.userInteractionEnabled = YES;
    self.view.backgroundColor=[UIColor whiteColor];
    [self addBackButton];
    [self addPullToRefreshHeader];
    [scrollView setFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+[UIDevice heightDifference] -IMG_StatusBarAndNavigationBarHeight)];
//    scrollView.contentInset = UIEdgeInsetsMake(-64, 0, 0, 0);
    [[LoadingView sharedLoadingView] startLoading];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ReloadReviewsView_v1WithUpdateUserReview:) name:@"ReloadReviewsView_v1WithUpdateUserReview" object:nil];
//get data from cache
    NSMutableData *rdpCacheData = [NSMutableData dataWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",qravedRDPCachePath,restaurant.restaurantId]];
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc]initForReadingWithData:rdpCacheData];
    rdpRest = [unarchiver decodeObjectForKey:[NSString stringWithFormat:@"%@",restaurant.restaurantId]];
    if ([rdpRest.restaurantId intValue])
    {
        restaurant = [RestaurantHandler updateForDetailViewControllerNow:rdpRest.RDPFirstDataArr forRestaurant:restaurant][0];
        restaurant.ratingCounts = rdpRest.ratingCounts;
        restaurant.ratingScore = rdpRest.ratingScore;
        restaurant.reviewCount = rdpRest.reviewCount;
        
        eventImageDataArr = [RestaurantHandler updateForDetailViewControllerNow:rdpRest.RDPFirstDataArr forRestaurant:restaurant][1];
        restaurantOffer = [DetailDataHandler getOffersFromResponseObjectNow:rdpRest.RDPFirstDataArr];
        [couponArr addObjectsFromArray:[DetailDataHandler getCouponsFromResponseObject:rdpRest.RDPFirstDataArr]];
        _banerImageList = [[NSMutableArray alloc] init];
        for (NSArray *arr in [rdpRest.RDPFirstDataArr objectForKey:@"imageList"])
        {
            IMGRestaurantImage  *restaurantImage=[[IMGRestaurantImage alloc]init];
            restaurantImage.imageId = [arr objectAtIndex:0];
            restaurantImage.imageUrl = [arr objectAtIndex:1];
            [restaurantImage updatePhotoCredit:[arr objectAtIndex:2]];
            [_banerImageList addObject:restaurantImage];
        }
        
        [SearchUtil viewed:restaurant cityId:restaurant.cityId];
        
        [self addBanerImageView];
        
        [self addRamadanView];
        [self addStateView];
        [self addShopDetail];
        [self addBtnsView];
        [self addCouponView];
        [self addFirstOfferView];
        [[LoadingView sharedLoadingView] stopLoading];
        
        [self addLoadingView];
        isFromDBData = YES;
    }
    else
    {
        
        //create placeholderView
        [self createPlaceholderView];
        
        NSLog(@"%f,getRestaurantFirstData 1.0",[[NSDate date]timeIntervalSince1970]);
        [DetailDataHandler getRestaurantFirstDataFromServiceNow:restaurant.restaurantId andBlock:^(IMGRestaurant *restaurantTemp,NSArray *eventList,IMGRestaurantOffer *restaurantOfferTemp,NSArray *banerImageArr,IMGRDPRestaurant *rdpRestTemp,NSNumber *showClaimLoc,NSArray *couponArray) {
            showClaim = showClaimLoc;
            rdpRest = rdpRestTemp;
            [self writeRDPRestToFile];
            restaurant = restaurantTemp;
            eventImageDataArr = eventList;
            _banerImageList = [[NSMutableArray alloc] init];
            for (NSArray *arr in banerImageArr)
            {
                IMGRestaurantImage  *restaurantImage=[[IMGRestaurantImage alloc]init];
                restaurantImage.imageId = [arr objectAtIndex:0];
                restaurantImage.imageUrl = [arr objectAtIndex:1];
                [restaurantImage updatePhotoCredit:[arr objectAtIndex:2]];
                [_banerImageList addObject:restaurantImage];
                
                [self loadBannerImageUrl:_banerImageList];
            }
            [couponArr addObjectsFromArray:couponArray];
            restaurantOffer = restaurantOfferTemp;
            
            [SearchUtil viewed:restaurant cityId:restaurant.cityId];
            
            vPlaceHolderView.hidden = YES;
            [self addBanerImageView];
            
            NSLog(@"%f,getRestaurantFirstData 4.0",[[NSDate date]timeIntervalSince1970]);
            [self addOtherView];
            //[self addTutorialView];
        }];
    }
}

- (void)createPlaceholderView{
    if (![Tools isBlankString:restaurant.logoImage]) {
        vPlaceHolderView = [PlaceholderView new];
        vPlaceHolderView.restaurant = restaurant;
        [self.view addSubview:vPlaceHolderView];
        
        vPlaceHolderView.sd_layout
        .topSpaceToView(self.view, 0)
        .leftSpaceToView(self.view, 0)
        .rightSpaceToView(self.view, 0)
        .autoHeightRatio(0);
    }
}

-(void)addLoadingView{
    loadingView = [[UIView alloc] init];
    loadingView.backgroundColor = [UIColor whiteColor];
    loadingView.frame = CGRectMake(0, currentY, DeviceWidth, 400);
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityView.frame = CGRectMake(0, 0, loadingView.frame.size.width, loadingView.frame.size.height);
    [loadingView addSubview:activityView];
    [activityView startAnimating];
    [scrollView addSubview:loadingView];
    scrollView.contentSize = CGSizeMake(DeviceWidth, loadingView.endPointY);
}
-(void)addRamadanView{
    
    NSDate *senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"YYYY-MM-dd"];
    NSString *  locationString=[dateformatter stringFromDate:senddate];
    NSArray *tempArr = [locationString componentsSeparatedByString:@"-"];
    NSMutableArray *currentTimeArr = [[NSMutableArray alloc] initWithArray:tempArr];
    if (![[currentTimeArr objectAtIndex:0] isEqualToString:@"2017"] )
    {
        return;
    }
    
    if ([[currentTimeArr objectAtIndex:1] isEqualToString:@"05"])
    {
        if ([[currentTimeArr objectAtIndex:2] intValue] < 23)
        {
            return;
        }
    }
    else if ([[currentTimeArr objectAtIndex:1] isEqualToString:@"06"])
    {
        if ([[currentTimeArr objectAtIndex:2] intValue] > 29)
        {
            return;
        }
    }
    else
        return;
    
    UIView *backgroundView = [[UIView alloc] init];
    backgroundView.backgroundColor = [UIColor colorWithHexString:@"#fff6d5"];
    backgroundView.frame = CGRectMake(0, currentY, DeviceWidth, 55);
    [scrollView addSubview:backgroundView];
    
    Label *label=[[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET,currentY, DeviceWidth - LEFTLEFTSET - LEFTLEFTSET, 55) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor color222222] andTextLines:0];
    label.text = @"Jam operasional dapat berubah sewaktu-waktu selama bulan Ramadhan.";
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    [scrollView addSubview:label];
    currentY = label.endPointY;
    isHadRamadanView = YES;
}

- (void)couponTapped{
    CouponListViewController *vcCouponList = [[CouponListViewController alloc] init];
    vcCouponList.couponArray = couponArr;
    [self.navigationController pushViewController:vcCouponList animated:YES];
}

- (void)addCouponView{
    DetailCouponView *detailCouponView = [[DetailCouponView alloc] init];
    if (restaurant.couponBannerArr != nil && restaurant.couponBannerArr.count > 0) {
        detailCouponView.frame = CGRectMake(0, currentY, DeviceWidth, DeviceWidth*.24);
        detailCouponView.couponArray = restaurant.couponBannerArr;
    }else{
        detailCouponView.frame = CGRectMake(0, currentY, DeviceWidth, 0);
    }
    
    __weak typeof(self) weakSelf = self;
    detailCouponView.couponImageTapped = ^() {
        [weakSelf couponTapped];
    };
    [scrollView addSubview:detailCouponView];
    
    currentY=detailCouponView.endPointY;
    scrollView.contentSize = CGSizeMake(DeviceWidth, currentY);
}

-(void)addOtherView{
    [self addRamadanView];
    [self addStateView];
    [self addShopDetail];
    [self addBtnsView];
    [self addCouponView];
    [self addFirstOfferView];
//    [self addMapView];
//    [self addInformationView];
   // [[LoadingView sharedLoadingView] stopLoading];
    
    [self addLoadingView];
    NSLog(@"%f,getRestaurantSecondData 1.0",[[NSDate date]timeIntervalSince1970]);
    
    [DetailDataHandler getRestaurantSecondDataFromService:restaurant.restaurantId and:rdpRest andBlock:^(NSArray *menuList,NSArray *dishList,NSArray *moreDishList,NSArray *reviewList,NSArray *eventList,NSArray *diningGuideList,NSArray *journalList,NSNumber *menuPhotoCount,NSNumber *dishCount,NSNumber *reviewedUserCount,IMGRDPRestaurant *rdpCach) {
        
        [self loadIMGUrl:journalList :@"journal"];
        [self loadIMGUrl:diningGuideList :@"guide"];
        [self loadIMGUrl:dishList :@"photos"];
        [self loadIMGUrl:menuList :@"menu"];
        
        _reviewedUserCount= reviewedUserCount;
        rdpRest=rdpCach;
        menuDataArr = [[NSArray alloc] initWithArray:menuList];
        restaurant.menuCount = menuPhotoCount;
        
        [self addMenuView];
        [newDishArrM removeAllObjects];
        dishDataArr = [[NSArray alloc] initWithArray:dishList];
        restaurant.disCount = dishCount;
        
        moreDishDataArr = [[NSMutableArray alloc] init];
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        NSDate *createTime=[[NSDate alloc] init];
        for (NSDictionary *dic in moreDishList)
        {
            IMGDish *dish=[[IMGDish alloc]init];
            dish.type = [dic objectForKey:@"type"];
            
            [formatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
            formatter.timeZone=[NSTimeZone timeZoneWithName:@"Asia/Jakarta"];
            if ([dish.type intValue]==1) {
                NSDictionary *tempDic = [dic objectForKey:@"dish"];
                [dish setValuesForKeysWithDictionary:tempDic];
                
                createTime=[formatter dateFromString:[tempDic objectForKey:@"createTime"]];
                dish.createTime=[NSNumber numberWithUnsignedLongLong:createTime.timeIntervalSince1970];
                dish.userName=[tempDic objectForKey:@"creator"];
                dish.descriptionStr=[tempDic objectForKey:@"description"];
                dish.restaurantTitle = [tempDic objectForKey:@"restaurantTitle"];
                dish.isLike = [[tempDic objectForKey:@"isLike"] boolValue];
                [dish updatePhotoCredit:[tempDic objectForKey:@"photoCredit"]];
                [moreDishDataArr addObject:dish];
                
            }else if ([dish.type intValue]==2){
                NSDictionary *dishDictionary = [dic objectForKey:@"instagramPhoto"];
                if (![dishDictionary isKindOfClass:[NSNull class]]) {
                    [dish setValuesForKeysWithDictionary:dishDictionary];
                    dish.userName=[dishDictionary objectForKey:@"instagramUserName"];
                    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[[dishDictionary objectForKey:@"approvedDate"] longLongValue]/1000];
                    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
                    
                    createTime=[formatter dateFromString:confromTimespStr];
                    dish.createTime=[NSNumber numberWithDouble:createTime.timeIntervalSince1970];
                    dish.imageUrl = [dishDictionary objectForKey:@"standardResolutionImage"];
                    
                    [moreDishDataArr addObject:dish];
                }
             
            }
        }
        
        [self addPhotosView:1];
        
        reviewDataArr = [[NSMutableArray alloc] initWithArray:reviewList];
        [self addReviewsView_v1];
        
        eventDataArr = [[NSArray alloc] initWithArray:eventList];
        diningGuideDataArr = [[NSArray alloc] initWithArray:diningGuideList];
        journalDataArr = [[NSArray alloc] initWithArray:journalList];
        [[LoadingView sharedLoadingView] stopLoading];
    } andBlockEventCountWithDiningGuideCount:^(NSNumber *eventCount, NSNumber *diningGuideCount,NSNumber *journalCount) {
        rdpRest.isHadRDPSecondData = [NSNumber numberWithInt:1];
        [self writeRDPRestToFile];
        
        restaurant.eventCount = eventCount;
        restaurant.diningGuideCount = diningGuideCount;
        restaurant.journalCount = journalCount;
        [self addFeaturedInView];
        NSLog(@"%f,getRestaurantSecondData 3.0",[[NSDate date]timeIntervalSince1970]);
        
        [loadingView removeFromSuperview];
        finishUI = [[NSDate date]timeIntervalSince1970];
        float duringTime = finishUI - beginLoadingData;
        duringTime = duringTime * 1000;
        int duringtime = duringTime;
        if (isViewDidLoaded) {
            [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createTimingWithCategory:@"RDP"  interval:[NSNumber numberWithInt:duringtime] name:@"" label:@"label"] build]];
            isViewDidLoaded = NO;
        }

        [[LoadingView sharedLoadingView] stopLoading];
        
    }];
    NSMutableString *phoneNumber;
    if (restaurant.phoneNumber.length >1)
    {
        phoneNumber = [NSMutableString stringWithString:restaurant.phoneNumber];
    }
    [self judgeScrollViewFrameWithPhoneNumber:phoneNumber];
}

//#warning (map down)
-(void)addInformationView{
    DetailViewInformationView *infoView = [[DetailViewInformationView alloc] initWithRestaurant:restaurant andShowClaim:showClaim];
    infoView.frame = CGRectMake(0, currentY+CELL_CONTENT_OFFSET, DeviceWidth, infoView.frame.size.height);
    infoView.delegate = self;
    [scrollView addSubview:infoView];
    currentY = infoView.endPointY;
    scrollView.contentSize = CGSizeMake(DeviceWidth, currentY);

}
-(void)addLineImage:(CGFloat)currentPointY withCurrentView:(UIView *)currentView{
    UIImageView *lineImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, currentPointY+5, DeviceWidth, 1)];
    lineImage3.image = [UIImage imageNamed:@"CutOffRule"];
    currentPointY = lineImage3.endPointY;
    [currentView addSubview:lineImage3];
}
-(void)addShortLineImage:(CGFloat)currentPointY withCurrentView:(UIView *)currentView{
    UIImageView *lineImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(15, currentPointY+5, DeviceWidth-30, 1)];
    lineImage3.image = [UIImage imageNamed:@"CutOffRule"];
    currentPointY = lineImage3.endPointY;
    [currentView addSubview:lineImage3];
}


//#pragma  view (banner label  remove here)
- (void)addShopDetail{
    CGSize titleSize = [restaurant.title sizeWithFont:[UIFont boldSystemFontOfSize:24] constrainedToSize:CGSizeMake(DeviceWidth-75, 58) lineBreakMode:NSLineBreakByWordWrapping];
    if ([Tools isBlankString:restaurant.wellKnownFor]){
        shopView = [[UIView alloc] initWithFrame:CGRectMake(0, currentY, DeviceWidth, 95 + titleSize.height)];
        
    }else{
        shopView = [[UIView alloc] initWithFrame:CGRectMake(0, currentY, DeviceWidth, 125 + titleSize.height)];
    }
    
    shopView.backgroundColor = [UIColor whiteColor];
    [scrollView addSubview:shopView];
    currentY = shopView.endPointY;
    
    DetailInformationView *informationView = [[DetailInformationView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, shopView.bounds.size.height)];
    informationView.restaurant = restaurant;
    [shopView addSubview:informationView];
}
//#warning (call favorite save rate more)
-(void)addBtnsView{
     btnsView = [[DetailButtonsView alloc] init];
    btnsView.backgroundColor = [UIColor whiteColor];
    btnsView.frame = CGRectMake(0, currentY, DeviceWidth, 80);
    [scrollView addSubview:btnsView];
    imgSaved = [btnsView viewWithTag:2000];
    if ([restaurant.saved isEqual:@1]) {
        imgSaved.image = [UIImage imageNamed:@"ic_Heart_on"];
    }else{
        imgSaved.image = [UIImage imageNamed:@"ic_Heart"];
    }
    
    btnsView.delegate = self;
    [self addLikeViewWithBtn:btnsView.likeBtn];
    UIImageView *lineImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 80, DeviceWidth, 1)];
    lineImage3.image = [UIImage imageNamed:@"CutOffRule"];
    [btnsView addSubview:lineImage3];
    
    currentY = btnsView.endPointY;
    scrollView.contentSize = CGSizeMake(DeviceWidth, currentY);
}
//#warning (   Tell people what you think )
-(void)addStarView{
    if (starView) {
        [starView removeFromSuperview];
    }
    
    starView = [[UIView alloc] initWithFrame:CGRectMake(0, currentY, DeviceWidth, 95)];
    starView.backgroundColor = [UIColor whiteColor];
    UIImageView *lineImageTop = [[UIImageView alloc]initWithFrame:CGRectMake(0, 3, DeviceWidth, 1)];
    lineImageTop.image = [UIImage imageNamed:@"CutOffRule"];
    [starView addSubview:lineImageTop];
    UIImageView *lineImageBottom = [[UIImageView alloc]initWithFrame:CGRectMake(0, 94 +15, DeviceWidth, 10)];
//    lineImageBottom.image = [UIImage imageNamed:@"CutOffRule"];
    lineImageBottom.backgroundColor = [UIColor colorWithRed:239/255.0f green:239/255.0f blue:239/255.0f alpha:1.0f];
    [starView addSubview:lineImageBottom];
    currentY = starView.endPointY +15;
    UILabel *starLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, DeviceWidth, 44)];
    starLabel.text = @"Tell people what you think";
    starLabel.textAlignment = NSTextAlignmentCenter;
    starLabel.font = [UIFont systemFontOfSize:17];
    [starView addSubview:starLabel];
    UIImageView *lineImageMid = [[UIImageView alloc]initWithFrame:CGRectMake(50, starLabel.endPointY, DeviceWidth-100, 1)];
    lineImageMid.image = [UIImage imageNamed:@"CutOffRule"];
    [starView addSubview:lineImageMid];
    startRatingM = [[DLStarRatingControl alloc]initWithFrame:CGRectMake(DeviceWidth/2-112.5,lineImageMid.endPointY , 225, 45) andStars:5 andStarWidth:30 isFractional:NO spaceWidth:15 isFromRDP:YES];
    startRatingM.delegate = self;
    [self countStar];
    [starView addSubview:startRatingM];
    [scrollView addSubview:starView];
}

//#warning (photos)
-(void)addPhotosView:(int)photoType{
    userPhotoArray = [[NSMutableArray alloc] initWithArray:newDishArrM];
    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
    NSDate *createTime=[[NSDate alloc] init];
    for (NSDictionary *dic in dishDataArr)
    {
        IMGDish *dish=[[IMGDish alloc]init];
        dish.type = [dic objectForKey:@"type"];
        
        [formatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
        formatter.timeZone=[NSTimeZone timeZoneWithName:@"Asia/Jakarta"];
        if ([dish.type intValue]==1) {
            
            NSDictionary *tempDic = [dic objectForKey:@"dish"];
            [dish setValuesForKeysWithDictionary:tempDic];
    
            
            createTime=[formatter dateFromString:[tempDic objectForKey:@"createTime"]];
            dish.createTime=[NSNumber numberWithUnsignedLongLong:createTime.timeIntervalSince1970];
            dish.userName=[tempDic objectForKey:@"creator"];
            dish.descriptionStr=[tempDic objectForKey:@"description"];
            dish.restaurantTitle = [tempDic objectForKey:@"restaurantTitle"];
            dish.isLike = [[tempDic objectForKey:@"isLike"] boolValue];
            [dish updatePhotoCredit:[tempDic objectForKey:@"photoCredit"]];
            
            [userPhotoArray addObject:dish];
        }
        else{
        
         
            NSDictionary *tempDic = [dic objectForKey:@"instagramPhoto"];
            [dish setValuesForKeysWithDictionary:tempDic];
            NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[[tempDic objectForKey:@"approvedDate"] longLongValue]/1000];
            NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
            createTime=[formatter dateFromString:confromTimespStr];
            dish.createTime=[NSNumber numberWithDouble:createTime.timeIntervalSince1970];
            dish.userName=[tempDic objectForKey:@"instagramUserName"];
            dish.descriptionStr=[tempDic objectForKey:@"description"];
            dish.restaurantTitle = [tempDic objectForKey:@"restaurantTitle"];
            dish.isLike = [[tempDic objectForKey:@"isLike"] boolValue];
            [dish updatePhotoCredit:[tempDic objectForKey:@"photoCredit"]];
            
            if ([tempDic objectForKey:@"instagramUserId"] != nil) {
                dish.photoCreditTypeDic = @"User ID";
            }
            
            dish.userAvatarDic = [tempDic objectForKey:@"instagramUserProfilePicture"];
            dish.userIdDic = [tempDic objectForKey:@"instagramUserId"];
            
            if ([[tempDic objectForKey:@"standardResolutionImage"] length]>0) {
                dish.imageUrl = [tempDic objectForKey:@"standardResolutionImage"];
            }else{
                if ([[tempDic objectForKey:@"thumbnailImage"] length]>0) {
                    dish.imageUrl = [tempDic objectForKey:@"thumbnailImage"];
                }else{
                    dish.imageUrl = [tempDic objectForKey:@"lowResolutionImage"];
                }
            }
            [userPhotoArray addObject:dish];
        }
    }
    DetaiPhotosView *photoView = [[DetaiPhotosView alloc] initWithPhtosArray:userPhotoArray andRestaurant:restaurant];
    photosViewForDish = photoView;
//    [userPhotoArray addObjectsFromArray:photoView.userPhotoArray];
    photoView.delegate =self;
    photoView.frame = CGRectMake(0, currentY, photoView.frame.size.width, photoView.frame.size.height);
    [scrollView addSubview:photoView];
    currentY = photoView.endPointY;
    [self addMapView];
    [self addInformationView];
    
}
-(void)addBackButton{
    
    navigationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, IMG_StatusBarAndNavigationBarHeight)];
    navigationView.backgroundColor=[UIColor clearColor];
    navigationView.userInteractionEnabled=YES;
    navigationView.layer.zPosition=999999;
    
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"icon_wrong"] forState:UIControlStateNormal];
    backButton.frame = CGRectMake(LEFTLEFTSET, IMG_StatusBarHeight, 60,44);
    [backButton addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    backButton.imageEdgeInsets = UIEdgeInsetsMake(12, 0, 12, 40);
    
    shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareButton setImage:[UIImage imageNamed:@"ic_share_guide"] forState:UIControlStateNormal];
    shareButton.frame = CGRectMake(DeviceWidth-LEFTLEFTSET-44, IMG_StatusBarHeight, 60, 44);
    [shareButton addTarget:self action:@selector(shareButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    navTitleLabel=[[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET+60, IMG_StatusBarHeight, DeviceWidth-(LEFTLEFTSET+60)*2, 44)];
    [navTitleLabel setFont:[UIFont systemFontOfSize:17]];
    navTitleLabel.text=restaurant.title;
    navTitleLabel.textColor=[UIColor color333333];
    navTitleLabel.textAlignment=NSTextAlignmentCenter;
    navTitleLabel.hidden=YES;
    [navigationView addSubview:backButton];
    [navigationView addSubview:shareButton];
    
    [navigationView addSubview:navTitleLabel];
    [self.view addSubview:navigationView];
}

//#warning (banner)
-(void)addBanerImageView{
    banerImageView =[[EScrollerView alloc] initWithFrameRect:CGRectMake(0, 0, DeviceWidth, TOP_SCROLL_HEIGHT*[AppDelegate ShareApp].autoSizeScaleY) objectArray:_banerImageList isStartScroll:YES withPhotoTag:0 pageControlCenter:YES isCirculation:NO isPageControl:YES isEvent:NO isPlaceHolder:YES];
    if (_banerImageList.count == 0) {
        UIImageView *placeImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, TOP_SCROLL_HEIGHT*[AppDelegate ShareApp].autoSizeScaleY)];
        [placeImageView sd_setImageWithURL:nil placeholderImage:[[UIImage imageNamed:DEFAULT_IMAGE_STRING2]imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth, TOP_SCROLL_HEIGHT*[AppDelegate ShareApp].autoSizeScaleY)]];
        [banerImageView addSubview:placeImageView];
        UIImageView *placeImageView2 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, TOP_SCROLL_HEIGHT*[AppDelegate ShareApp].autoSizeScaleY)];
        placeImageView2.image = [UIImage imageNamed:@"DetailTopBg@2x.png"];
        [placeImageView addSubview:placeImageView2];
    }
    [scrollView addSubview:banerImageView];
    banerImageView.delegate = self;
    currentY=banerImageView.endPointY;
    scrollView.contentSize = CGSizeMake(DeviceWidth, currentY);
    [self addSummaryInfoView];
}

//#warning (banner all label )
-(void)addSummaryInfoView{
    UIButton *btnInstagram = [UIButton new];
    [btnInstagram setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnInstagram.titleLabel.font = [UIFont systemFontOfSize:12];
    [btnInstagram setImage:[UIImage imageNamed:@"ic_photo_instagram"] forState:UIControlStateNormal];
    btnInstagram.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    NSInteger instagramCount = [restaurant.instagramPhotoCount intValue];
    NSString *instagramTitle = instagramCount>0?[NSString stringWithFormat:@" %ld",(long)instagramCount]:@"";
    [btnInstagram setTitle:instagramTitle forState:UIControlStateNormal];
    [btnInstagram bk_whenTapped:^{
        [self gotoPhotos:@"1"];
    }];
    [scrollView addSubview:btnInstagram];
    
    UIButton *btnPhoto = [UIButton new];
    [btnPhoto setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btnPhoto.titleLabel.font = [UIFont systemFontOfSize:12];
    [btnPhoto setImage:[UIImage imageNamed:@"ic_image"] forState:UIControlStateNormal];
    btnPhoto.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    NSInteger dishCount = [restaurant.dishCount intValue];
    NSString *dishTitle = dishCount>0?[NSString stringWithFormat:@" %ld",(long)dishCount]:@"";
    [btnPhoto setTitle:dishTitle forState:UIControlStateNormal];
    [btnPhoto bk_whenTapped:^{
       [self gotoPhotos:@"1"];
    }];
    [scrollView addSubview:btnPhoto];

    btnInstagram.sd_layout
    .rightSpaceToView(scrollView, 15)
    .topSpaceToView(scrollView, TOP_SCROLL_HEIGHT*[AppDelegate ShareApp].autoSizeScaleY-21)
    .widthIs([self sizeForButton:instagramTitle])
    .heightIs(instagramCount>0?14:0);

    btnPhoto.sd_layout
    .rightSpaceToView(btnInstagram, 5)
    .topEqualToView(btnInstagram)
    .widthIs([self sizeForButton:dishTitle])
    .heightIs(dishCount>0?14:0);
}

- (CGFloat)sizeForButton:(NSString *)title{
    CGSize size = [title sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(DeviceWidth-30, 14) lineBreakMode:NSLineBreakByWordWrapping];
    if (size.width>0) {
        return size.width + 20;
    }else{
        return size.width;
    }
}

//#warning (Ratings and Reviews)
-(void)addReviewsView_v1{

    reviewsView_v1 = [[DetailReviewView alloc] initWithRestaurant:restaurant andNewReviewArrM:newReviewArrM andReviewDataArr:reviewDataArr andDishdic:dishDic andCommentReadMore:commentReadMore andReveiwCardIsReadMore:reveiwCardIsReadMore andScoreBtnTop:scoreBtnTop andReviewedUserCount:_reviewedUserCount andDelegate:self andController:self.navigationController];
    reviewsView_v1.delegate = self;
    reviewsArray = [NSMutableArray arrayWithArray:reviewsView_v1.reviewsArray];
    reviewHeight = [NSMutableArray arrayWithArray:reviewsView_v1.reviewHeight];
    [scrollView addSubview:reviewsView_v1];
    
    reviewsView_v1.frame = CGRectMake(0, currentY, DeviceWidth,reviewsView_v1.currentPointY/*+85*/);
    [self addLineImage:reviewsView_v1.currentPointY + 84 withCurrentView:reviewsView_v1];

    currentY = reviewsView_v1.endPointY;
    scrollView.contentSize = CGSizeMake(DeviceWidth, currentY);
    
    [self addStarView];
}

//#warning (map  和and mapdetail)
-(void)addMapView{
    mapView=[[UIView alloc]init];
    mapView.backgroundColor = [UIColor whiteColor];
    [scrollView addSubview:mapView];
    MKMapView *map=[[MKMapView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, mapViewHeight)];
    [map setMapType:MKMapTypeStandard];
    [map setDelegate:self];
    [mapView addSubview:map];
    CLLocationCoordinate2D coor;
    coor.latitude = [restaurant.latitude doubleValue];
    coor.longitude = [restaurant.longitude doubleValue];
    CLLocationCoordinate2D theCoordinate =CLLocationCoordinate2DMake(coor.latitude, coor.longitude);
    MKCoordinateSpan  theSpan;
    theSpan.latitudeDelta=0.01;
    theSpan.longitudeDelta=0.01;
    MKCoordinateRegion theRegion;
    theRegion.center=theCoordinate;
    theRegion.span=theSpan;
    MKPointAnnotation *pointAnnatation= [[MKPointAnnotation alloc] init];
    if(CLLocationCoordinate2DIsValid(theCoordinate))
    {
        [map setRegion:theRegion animated:NO];
        
        [map regionThatFits:theRegion];
        
        pointAnnatation.coordinate=theCoordinate;
        
        [map addAnnotation:pointAnnatation];
    }
    
    CGSize expectedSize = [[[restaurant address] filterHtml] sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14] constrainedToSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET-50, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
    Label *addressLabel=[[Label alloc] initWithFrame:CGRectMake(50,map.endPointY + 20, DeviceWidth-LEFTLEFTSET*2-50, expectedSize.height+15) andTextFont:[UIFont systemFontOfSize:14] andTextColor:[UIColor color333333] andTextLines:0];
    
    Label *addressLabel2=[[Label alloc] initWithFrame:CGRectMake(50,addressLabel.bottom, DeviceWidth-LEFTLEFTSET*2-50, 20) andTextFont:[UIFont systemFontOfSize:12] andTextColor:[UIColor color999999] andTextLines:0];
    
    if ([restaurant address])
    {
        NSString *addressStr = [NSString stringWithString:[restaurant address]];
        addressStr = [addressStr filterHtml];
        [addressLabel setText:addressStr];
    }
    
    [mapView addSubview:addressLabel];
    
    UIImageView *mapImageView = [[UIImageView alloc] init];
    mapImageView.frame = CGRectMake(LEFTLEFTSET, 20+map.endPointY+(addressLabel.frame.size.height/2)-8, 20, 20);
    mapImageView.image = [UIImage imageNamed:@"ic_location_on"];
    [mapView addSubview:mapImageView];
    
    [mapView setFrame:CGRectMake(0,currentY+WATERFALL_SPACING_WIDTH, DeviceWidth, addressLabel.endPointY)];
    
    UIButton *mapBackground=[UIButton buttonWithType:UIButtonTypeCustom];
    [mapBackground setFrame:CGRectMake(0, 0, DeviceWidth, mapView.frame.size.height)];
    [mapBackground addTarget:self action:@selector(gotoMap) forControlEvents:UIControlEventTouchUpInside];
    [mapView addSubview:mapBackground];
    
    currentY=mapView.endPointY;
    [scrollView setContentSize:CGSizeMake(DeviceWidth, currentY+SCORLLVIEW_Y-[UIDevice heightDifference] - 64)];
}

//#warning (Menu)
-(void)addMenuView{
    menuPhotoArray = [[NSMutableArray alloc] init];
    menuView = [[DetailMenuView alloc] initWithPhtosArray:menuDataArr andRestaurant:restaurant];
    menuView.frame = CGRectMake(0, currentY, menuView.frame.size.width, menuView.frame.size.height);
    menuView.delegate =self;
    [menuPhotoArray addObjectsFromArray:menuView.menuPhotoArray];
    [scrollView addSubview:menuView];
    currentY = menuView.endPointY;
    if (self.isGoMenuSection&&[[NSUserDefaults standardUserDefaults]boolForKey:@"notfirst"]) {
        scrollView.contentOffset=CGPointMake(0, menuView.startPointY);
    }
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"notfirst"];
}
-(void)addStateView{
    if(restaurant.state!=nil && [restaurant.state intValue]!=6){
        IMGOperatingState *operatingState = [IMGOperatingState findByStateId:restaurant.state];
        UILabel *stateLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, currentY, DeviceWidth, 38*[AppDelegate ShareApp].autoSizeScaleY)];
        stateLabel.layer.backgroundColor = [ColorUtil getColor:operatingState.color].CGColor;
        [stateLabel setText:[[NSString alloc] initWithFormat:@"   %@",operatingState.stateName]];
        [stateLabel setTextColor:[UIColor color333333]];
        [stateLabel setTextAlignment:NSTextAlignmentCenter];
        stateLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:15*[AppDelegate ShareApp].autoSizeScaleY];
        [scrollView addSubview:stateLabel];
        currentY = stateLabel.endPointY;
        isHadStateView = YES;
    }
}
//#warning promos
-(void)addFirstOfferView{
    
    [self initRestaurantOfferFromLocal:nil];
    if([restaurant.state intValue]==3 || !restaurantOffer){
        return;
    }
    UIButton *offerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    offerButton.backgroundColor = [UIColor whiteColor];
    offerButton.frame = CGRectMake(0, currentY+CELL_CONTENT_OFFSET, DeviceWidth, 100);
    [offerButton addTarget:self action:@selector(gotoOfferViewController) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:offerButton];
    
   
    
    Label *offerDiscountLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 15, DeviceWidth-120, 46) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:16] andTextColor:[UIColor colorC2060A] andTextLines:2];
    offerDiscountLabel.text = [NSString stringWithFormat:@"%@",restaurantOffer.offerTitle];
    offerDiscountLabel.numberOfLines = 2;
    offerDiscountLabel.lineBreakMode =  NSLineBreakByTruncatingTail;
    offerDiscountLabel.textAlignment = NSTextAlignmentCenter;
    [offerButton addSubview:offerDiscountLabel];
    
    UITapGestureRecognizer *tapGeature = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gotoOfferViewController)];
    [tapGeature setNumberOfTapsRequired:1];
    [tapGeature setNumberOfTouchesRequired:1];
    offerDiscountLabel.userInteractionEnabled=YES;
    [offerDiscountLabel addGestureRecognizer:tapGeature];
    
   
    UIImageView *bookImage = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth - 95, 15, 80, 80)];
    NSURL *urlString;
    if ([restaurantOffer.imageUrl hasPrefix:@"http"])
    {
        urlString = [NSURL URLWithString:[restaurantOffer.imageUrl filterHtml]];
    }
    else
    {
        urlString = [NSURL URLWithString:[restaurantOffer.imageUrl returnFullImageUrl]];
    }
    
    __weak UIImageView *weakimageView = bookImage;
    
    if ([restaurantOffer.imageUrl isEqualToString:@""]) {
        
        bookImage.image = [UIImage imageNamed:@"BookFlowOfferTypeTag0"];
        
    }else{
        [bookImage sd_setImageWithURL:urlString placeholderImage:[UIImage imageNamed:@"BookFlowOfferTypeTag0"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image.size.height>0&&image.size.width>0) {
//                image = [image cutCircleImage];
                weakimageView.image=image;
            }else{
                weakimageView.image=[UIImage imageNamed:@"BookFlowOfferTypeTag0"];
            }
        }];
    }
    [offerButton addSubview:bookImage];
    
    UILabel *seeMoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, offerDiscountLabel.bottom +5, 120, 20)];
    seeMoreLabel.centerX = offerButton.centerX;
    seeMoreLabel.text = @"See All Promos";
    seeMoreLabel.font = [UIFont systemFontOfSize:14];
    seeMoreLabel.textColor = [UIColor colortextBlue];
    [offerButton addSubview:seeMoreLabel];
    UITapGestureRecognizer *tapGeature1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gotoOfferViewController)];
    [tapGeature1 setNumberOfTapsRequired:1];
    [tapGeature1 setNumberOfTouchesRequired:1];
    seeMoreLabel.userInteractionEnabled=YES;
    [seeMoreLabel addGestureRecognizer:tapGeature1];
    
    if (restaurantOffer.hasNewOfferSign)
    {
        newOfferBadgeImageView = [[UIImageView alloc] init];
        [newOfferBadgeImageView setImage:[UIImage imageNamed:@"newflag"]];
        newOfferBadgeImageView.backgroundColor = [UIColor whiteColor];
        newOfferBadgeImageView.frame = CGRectMake(DeviceWidth -LEFTLEFTSET - 35, 5, 32, 32);
        [offerButton addSubview:newOfferBadgeImageView];
    }
    
    [self addLineImage:offerButton.frame.size.height-3 withCurrentView:offerButton];
    
    currentY=offerButton.endPointY;
    scrollView.contentSize = CGSizeMake(DeviceWidth, currentY);
}

//#warning  L(@"Featured In")
-(void)addFeaturedInView{
    featuredInView = [[DetailFeatureInView alloc]initWithDiningGuideDataArr:diningGuideDataArr andEventArray:eventDataArr andJournalDataArr:journalDataArr andRestaurant:restaurant];
    diningGuideArray = [[NSMutableArray alloc]initWithCapacity:0];
    eventArray = [[NSMutableArray alloc]initWithCapacity:0];
    mediaCommentArray = [[NSMutableArray alloc]initWithCapacity:0];
    [diningGuideArray addObjectsFromArray:featuredInView.diningGuideArray];
    [eventArray addObjectsFromArray:featuredInView.eventArray];
    [mediaCommentArray addObjectsFromArray:featuredInView.mediaCommentArray];
    featuredInView.delegate = self;
    float height = 0.0;
    if (scrollView.endPointY > (DeviceHeight+20) ) {
        height = 64.0;
    }
    featuredInView.frame = CGRectMake(0, currentY+CELL_CONTENT_OFFSET, DeviceWidth, featuredInView.frame.size.height);
    [scrollView addSubview:featuredInView];
    currentY=featuredInView.endPointY;
    [scrollView setContentSize:CGSizeMake(DeviceWidth, currentY+SCORLLVIEW_Y+height)];
    
}

//#warning (bottom btn)
-(void)initBookButtonView{
    bookButtonView = [[UIView alloc]initWithFrame:CGRectMake(20, DeviceHeight-60+[UIDevice heightDifference], DeviceWidth - 40, 40)];
//    bookButtonView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"SitDownTimeBg"]];
    bookButtonView.backgroundColor = [UIColor colorWithRed:250/255.0f green:250/255.0f blue:250/255.0f alpha:1.0f];
    bookButtonView.layer.cornerRadius = bookButtonView.height/2;
    bookButtonView.layer.masksToBounds = YES;
    bookButtonView.layer.borderWidth = 0.5;
    bookButtonView.layer.borderColor = [UIColor color999999].CGColor;
    [self.view addSubview:bookButtonView];
    
    UIButton *btnCall = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCall.frame = CGRectMake(0, 0, bookButtonView.width/3 - 20, 40);
    [btnCall setTitle:[@"Call" uppercaseString] forState:UIControlStateNormal];
    [btnCall setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    btnCall.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    [btnCall setImage:[UIImage imageNamed:@"ic_phone@2x"] forState:UIControlStateNormal];
    if (IS_IPHONE_4S || IS_IPHONE_5){
        [btnCall setImageEdgeInsets:UIEdgeInsetsMake(0, 52, 0, 0)];
        [btnCall setTitleEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 20)];
    }else{
        [btnCall setImageEdgeInsets:UIEdgeInsetsMake(0, 60, 0, 0)];
        [btnCall setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 30)];
    }
    
    [btnCall addTarget:self action:@selector(callButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bookButtonView addSubview:btnCall];
    
    UIView *callView = [[UIView alloc] initWithFrame:CGRectMake(btnCall.right, 5, 1, 30)];
    callView.backgroundColor = [UIColor color999999];
    [bookButtonView addSubview:callView];
    
    UIButton *btnMap = [UIButton buttonWithType:UIButtonTypeCustom];
    btnMap.frame = CGRectMake(btnCall.right, 0, bookButtonView.width/3 - 20, 40);
    [btnMap setTitle:[@"Map" uppercaseString] forState:UIControlStateNormal];
    [btnMap setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    btnMap.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    [btnMap setImage:[UIImage imageNamed:@"ic_map@2x"] forState:UIControlStateNormal];
    if (IS_IPHONE_4S || IS_IPHONE_5){
        [btnMap setImageEdgeInsets:UIEdgeInsetsMake(0, 52, 0, 0)];
        [btnMap setTitleEdgeInsets:UIEdgeInsetsMake(0, -5, 0, 22)];
    }else{
        [btnMap setImageEdgeInsets:UIEdgeInsetsMake(0, 60, 0, 0)];
        [btnMap setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 30)];
    }
    
    [btnMap addTarget:self action:@selector(gotoMap) forControlEvents:UIControlEventTouchUpInside];
    [bookButtonView addSubview:btnMap];
    
    UIView *mapV = [[UIView alloc] initWithFrame:CGRectMake(btnMap.right, 5, 1, 30)];
    mapV.backgroundColor = [UIColor color999999];
    [bookButtonView addSubview:mapV];
    
    UIButton *btnBookNow = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBookNow.frame = CGRectMake(btnMap.right, 0, bookButtonView.width/3 +40, 40);
    [btnBookNow setTitle:[@"Book Now" uppercaseString] forState:UIControlStateNormal] ;
    [btnBookNow setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    btnBookNow.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    [btnBookNow setImage:[UIImage imageNamed:@"ic_booknow@2x"] forState:UIControlStateNormal];
    if (IS_IPHONE_4S || IS_IPHONE_5){
        [btnBookNow setImageEdgeInsets:UIEdgeInsetsMake(0, 100, 0, 0)];
        [btnBookNow setTitleEdgeInsets:UIEdgeInsetsMake(0, -40, 0, 0)];
    }else{
        [btnBookNow setImageEdgeInsets:UIEdgeInsetsMake(0, 120, 0, 0)];
        [btnBookNow setTitleEdgeInsets:UIEdgeInsetsMake(0, -40, 0, 0)];

    }
    [btnBookNow addTarget:self action:@selector(bookButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [bookButtonView addSubview:btnBookNow];
   
}
-(void)initCallNowView{
    DetailCallNowView *callButtonView = [[DetailCallNowView alloc] initWithRestaurant:restaurant];
    callButtonView.delegate = self;
    [self.view addSubview:callButtonView];
}

-(void)addLikeViewWithBtn:(UIButton *)likeButton{
    [likeButton addTarget:self action:@selector(likeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    IMGUser *user=[IMGUser currentUser];
    if(user!=nil && user.userId!=nil && user.token!=nil){
        NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:restaurant.restaurantId,@"restaurantID",user.userId,@"userID",user.token,@"t", nil];
        [DetailDataHandler getRestaurantIsFavouriteWithParameter:parameters andBlock:^(NSString *likeStr) {
            IMGRestaurantLike *restaurantLike = [[IMGRestaurantLike alloc]init];
            restaurantLike.restaurantId = restaurant.restaurantId;
            restaurantLike.userId = user.userId;
            if([likeStr intValue]==1){
                [RestaurantHandler saveRestaurantLike:restaurantLike liked:YES];
            }else{
                [RestaurantHandler saveRestaurantLike:restaurantLike liked:NO];
            }
            [self loadLikeButtonFromDB:btnsView.likeBtn user:user];

        } andErrorBlock:^(NSError *error) {
            [self loadLikeButtonFromDB:btnsView.likeBtn user:user];
        }];
    }else{
        likeButton.tag=0;
        for (UIView *view in likeButton.subviews)
        {
            [view removeFromSuperview];
        }
        [btnsView.likeBtn setBackgroundImage:[UIImage imageNamed:@"favoriteGrayImage"] forState:UIControlStateNormal];
    }
}
-(void)addOtherViewWithDB{
    
    menuDataArr = [[NSArray alloc] initWithArray:rdpRest.menuList];
    restaurant.menuCount = rdpRest.menuPhotoCount;
    [self addMenuView];
    
    [newDishArrM removeAllObjects];
    dishDataArr = [[NSArray alloc] initWithArray:rdpRest.dishList];
    restaurant.disCount = rdpRest.dishCount;
    
    moreDishDataArr = [[NSMutableArray alloc] init];
    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
    NSDate *createTime=[[NSDate alloc] init];
    for (NSDictionary *dic in rdpRest.moreDishList)
    {
        IMGDish *dish=[[IMGDish alloc]init];
        dish.type = [dic objectForKey:@"type"];
        
        [formatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
        formatter.timeZone=[NSTimeZone timeZoneWithName:@"Asia/Jakarta"];
        if ([dish.type intValue]==1) {
            NSDictionary *tempDic = [dic objectForKey:@"dish"];
            [dish setValuesForKeysWithDictionary:tempDic];
            
            createTime=[formatter dateFromString:[tempDic objectForKey:@"createTime"]];
            dish.createTime=[NSNumber numberWithUnsignedLongLong:createTime.timeIntervalSince1970];
            dish.userName=[tempDic objectForKey:@"creator"];
            dish.descriptionStr=[tempDic objectForKey:@"description"];
            dish.restaurantTitle = [tempDic objectForKey:@"restaurantTitle"];
            dish.isLike = [[tempDic objectForKey:@"isLike"] boolValue];
            [dish updatePhotoCredit:[tempDic objectForKey:@"photoCredit"]];
            [moreDishDataArr addObject:dish];
            
        }else if ([dish.type intValue]==2){
            NSDictionary *dishDictionary = [dic objectForKey:@"instagramPhoto"];
            [dish setValuesForKeysWithDictionary:dishDictionary];
            dish.userName=[dishDictionary objectForKey:@"instagramUserName"];
            NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[[dishDictionary objectForKey:@"approvedDate"] longLongValue]/1000];
            NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
            
            createTime=[formatter dateFromString:confromTimespStr];
            dish.createTime=[NSNumber numberWithDouble:createTime.timeIntervalSince1970];
            dish.imageUrl = [dishDictionary objectForKey:@"standardResolutionImage"];
            
            [moreDishDataArr addObject:dish];
        }
    }
    
    [self addPhotosView:1];
    
    reviewDataArr = [[NSMutableArray alloc] initWithArray:rdpRest.reviewList];
    [self addReviewsView_v1];
    
    eventDataArr = [[NSArray alloc] initWithArray:rdpRest.eventList];
    diningGuideDataArr = [[NSArray alloc] initWithArray:rdpRest.diningGuideList];
    journalDataArr = [[NSArray alloc] initWithArray:rdpRest.journalList];
    
    restaurant.eventCount = rdpRest.eventCount;
    restaurant.diningGuideCount = rdpRest.diningGuideCount;
    restaurant.journalCount = rdpRest.journalCount;
    [self addFeaturedInView];
    
    [loadingView removeFromSuperview];
    
    NSMutableString *phoneNumber;
    if (restaurant.phoneNumber.length >1)
    {
        phoneNumber = [NSMutableString stringWithString:restaurant.phoneNumber];
    }
    [self judgeScrollViewFrameWithPhoneNumber:phoneNumber];
}
-(void)addCallAlert{
    if(@available(iOS 11.0, *)){
        [SendCall sendCall:restaurant.phoneNumber];
    }else{
        [UIAlertView bk_showAlertViewWithTitle:nil message:restaurant.phoneNumber cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Call"] handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if(buttonIndex==0){
                [alertView dismissWithClickedButtonIndex:0 animated:NO];
            }else if (buttonIndex==1){
                NSMutableDictionary* eventDic=[[NSMutableDictionary alloc]init];
                [eventDic setValue:@"Restaurant detail page" forKey:@"Location"];
                [eventDic setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
                [[Amplitude instance] logEvent:@"CL - Phone Number" withEventProperties:eventDic];
                [[Amplitude instance] logEvent:@"CL - Call Restaurant CTA" withEventProperties:eventDic];
                [[AppsFlyerTracker sharedTracker] trackEvent:@"CL - Call Restaurant CTA" withValues:eventDic];
                [SendCall sendCall:restaurant.phoneNumber];
            }
        }];
    }
}


#pragma mark - View Clicked Event
-(void)imagePressed:(UIGestureRecognizer *)gesture{
    NSInteger tag = gesture.view.tag;
    
    AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:userPhotoArray andPhotoTag:tag andRestaurant:restaurant andFromDetail:YES];
//    [albumViewController updateRestautantDetailData:moreDishDataArr];
    albumViewController.title = restaurant.title;
    albumViewController.showPage=NO;
    albumViewController.restaurantId = restaurant.restaurantId;
    albumViewController.amplitudeType = @"Restaurant detail page - ";
    albumViewController.isUseWebP = YES;
    [self.navigationController pushViewController:albumViewController animated:YES];
}
-(void)menuPhotoimagePressed:(UIGestureRecognizer *)gesture{
    NSInteger tag = gesture.view.tag;
    IMGMenu *menu=[menuPhotoArray firstObject];
    MenuPhotoViewColltroller *menuPhotoViewColltroller = [[MenuPhotoViewColltroller alloc]initWithPhotosArray:menuPhotoArray andPhotoTag:tag andRestaurant:restaurant andFromDetail:YES];
    menuPhotoViewColltroller.menuPhotoCount = restaurant.menuCount;
    menuPhotoViewColltroller.amplitudeType = @"Restaurant detail page - menu list";
    menuPhotoViewColltroller.isUseWebP = YES;
    if ([menu.type intValue] == 0){
        menuPhotoViewColltroller.ifUnauditedData = YES;
    }else{
        [menuPhotoViewColltroller updateRestautantDetailData];
    }
    [self.navigationController pushViewController:menuPhotoViewColltroller animated:YES];

}
-(void)restMenuPhotosCountClick:(UIGestureRecognizer *)gesture {
    
    MenuPhotoViewColltroller *menuPhotoViewColltroller = [[MenuPhotoViewColltroller alloc]initWithPhotosArray:menuPhotoArray andPhotoTag:9 andRestaurant:restaurant andFromDetail:YES];
    [menuPhotoViewColltroller updateRestautantDetailData];
    menuPhotoViewColltroller.menuPhotoCount = restaurant.menuCount;
    menuPhotoViewColltroller.amplitudeType = @"Restaurant detail page - menu list";
    [self.navigationController pushViewController:menuPhotoViewColltroller animated:YES];
}

-(void)restDishPhotosCountClick:(UIGestureRecognizer *)gesture{
    [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Photo list" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:nil andOrigin:@"Restaurant detail page" andType:nil];
    
    DetailSeeAllPhotoViewController *DetailSeeAllPhotoV = [[DetailSeeAllPhotoViewController alloc] initWithRestaurant:restaurant restaurantPhotoArray:_banerImageList];
    DetailSeeAllPhotoV.isUseWebP = YES;
    [self.navigationController pushViewController:DetailSeeAllPhotoV animated:YES];
}
-(void)newRating:(DLStarRatingControl *)control :(float)rating{
    ratingCount = rating;
    [self judgeReview];
}
-(void)changeSaveBtn:(BOOL)saved{
    if (saved) {
        imgSaved.image = [UIImage imageNamed:@"ic_Heart_on"];
    }else{
        imgSaved.image = [UIImage imageNamed:@"ic_Heart"];
    }

}
-(void)btnClick:(UIButton *)btn{
    switch (btn.tag)
    {
        case 100:
        {
            IMGUser *user = [IMGUser currentUser];
            NSNumber *userId = [[NSNumber alloc] init];
            if (user.userId != nil)
            {
                userId = user.userId;
            }
            else
                userId = [NSNumber numberWithInt:0];
            
            [TrackHandler trackWithUserId:userId andRestaurantId:restaurant.restaurantId andContent:[NSDictionary dictionaryWithObject:@"0" forKey:@"type"]];
            [self addCallAlert];
        }
            break;
        case 101:
        {
            ReadAboutMoreViewController *readAboutMoreViewController = [[ReadAboutMoreViewController alloc]initWithRestaurant:restaurant];
            [self.navigationController pushViewController:readAboutMoreViewController animated:YES];
        }
            break;
        case 102:
        {
            WebViewController *webViewController = [[WebViewController alloc]init];
            webViewController.webSite = restaurant.website;
            webViewController.title = restaurant.title;
            webViewController.fromDetail = YES;
            [self.navigationController pushViewController:webViewController animated:YES];
        }
            break;
        case 103:
        {
            [IMGAmplitudeUtil trackRDPWithName:@"CL - Restaurant Information" andRestaurantId:restaurant.restaurantId andOrigin:nil andLocation:nil];
            ReadAboutMoreViewController *readAboutMoreViewController = [[ReadAboutMoreViewController alloc]initWithRestaurant:restaurant];
            [self.navigationController pushViewController:readAboutMoreViewController animated:YES];
        }
            break;
        case 104:
        {
            [self gotoSuggestView];
        }
            break;
        case 105:
        {
            [self gotoClaimYourRestaurant];
        }
            break;
        case 110:
        {
            [self gotoReview:nil];
        }
            break;
        case 111:
        {
            [self judgeReview];
        }
            break;
        case 124:
        {
            [self gotoPhotos:@"1"];
        }
            break;
        case 125:
        {
            [self uploadPhoto];
        }
            break;
        case 130:
        {
            [[AppDelegate ShareApp]goToLoginController:[NSNumber numberWithInt:1]];
        }
            break;
        case 131:
        {
            LoginViewController *lvc = [[LoginViewController alloc] initWithFromWhere:[NSNumber numberWithInt:1]];
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:lvc];
            [self.navigationController presentViewController:nav animated:NO completion:^{
            }];
            
        }
            break;
   
            //save
        case 1000:
        {
            [CommonMethod doSaveWithRestaurant:restaurant andViewController:self andCallBackBlock:^(BOOL isSaved) {
                restaurant.saved = [NSNumber numberWithBool:isSaved];
                [self changeSaveBtn:isSaved];
            }];
        }
            break;
            //rate
        case 1001:
        {
            [self judgeReview];
        }
            break;
            //more
        case 1002:
        {
            ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] initWithRestaurantTitle:restaurant.title andIsFromUploadPhoto:YES andIsUploadMenuPhoto:NO andCurrentRestaurantId:restaurant.restaurantId andRestaurant:restaurant];
            picker.maximumNumberOfSelection = 9;
            picker.assetsFilter = [ALAssetsFilter allPhotos];
            picker.showEmptyGroups=NO;
            picker.delegate = self;
            picker.isFromUploadPhoto = YES;
            picker.restaurant = restaurant;
            picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
                    NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
                    return duration >= 5;
                } else {
                    return YES;
                }
            }];
            
            [self presentViewController:picker animated:YES completion:NULL];
        }
            break;
            
        case 100000:{
        
            NSLog(@"sdadsada");
            InstagramPlacesViewController *InstagramPlacesVC = [[InstagramPlacesViewController alloc] init];
            InstagramPlacesVC.instagramLocationId = [NSString stringWithFormat:@"%@",rdpRest.instagramLocationId];
            [self.navigationController pushViewController:InstagramPlacesVC animated:YES];
        }
        default:
            break;
    }
}


-(void)backClick{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ReloadReviewsView_v1WithUpdateUserReview" object:nil];
    [[LoadingView sharedLoadingView] stopLoading];
    if (self.isFromURL) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)shareButtonClick:(UIButton*)btn {
    
    
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",QRAVED_WEB_SERVER_OLD,restaurant.cityName?restaurant.cityName:@"",restaurant.seoKeyword]];
    NSString *shareContentString = [NSString stringWithFormat:@"%@ on Qraved. %@",restaurant.title,url];
    RestaurantActivityItemProvider *provider=[[RestaurantActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
    provider.url=url;
    provider.title=shareContentString;
    provider.districtName = restaurant.districtName;
    provider.restaurantTitle=restaurant.title;
    NSString *dollar=nil;
    switch(restaurant.priceLevel.intValue){
        case 1:dollar=@"Below 100K";break;
        case 2:dollar=@"100K - 200K";break;
        case 3:dollar=@"200K - 300K";break;
        case 4:dollar=@"Start from 300K";break;
    }
    provider.cuisineAreaPrice=[NSString stringWithFormat:@"%@ / %@ / %@",restaurant.cuisineName,restaurant.districtName,dollar];
    TwitterActivityItemProvider *twitterProvider=[[TwitterActivityItemProvider alloc] initWithPlaceholderItem:@""];
    twitterProvider.title=[NSString stringWithFormat:L(@"Look! It ’s %@ on #Qraved!"),restaurant.title];
    UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterProvider] applicationActivities:nil];
    activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
    [activityCtl setValue:[NSString stringWithFormat:L(@"I Found %@ on Qraved!"),restaurant.title] forKey:@"subject"];
    activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){

        if (completed) {
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:provider.activityType forKey:@"Channel"];
            [eventProperties setValue:@"Share CTA in RDP" forKey:@"Location"];

            [[Amplitude instance] logEvent:@"SH - Share Restaurant" withEventProperties:eventProperties];
            
        }else{
        }
    };
    
    if ([activityCtl respondsToSelector:@selector(popoverPresentationController)]) {
        
        activityCtl.popoverPresentationController.sourceView = btn;
        
    }
    [self presentViewController:activityCtl animated:YES completion:NULL];
}
-(void)gotoPhotos:(id)btnOrTap andindex:(NSInteger)index{
    NSInteger photoType;
    if([btnOrTap isKindOfClass:[NSString class]]){
        photoType = [((NSString *)btnOrTap) integerValue];
    }else if([btnOrTap isKindOfClass:[UIButton class]]){
        photoType = ((UIButton *)btnOrTap).tag;
    }else {
        photoType = ((UITapGestureRecognizer *)btnOrTap).view.tag;
    }
    
    if(photoType == 1){
        
        NSMutableArray *allPhotos=[[NSMutableArray alloc]init];
        for(IMGRestaurantImage *image in _banerImageList){
            IMGDish *dish = [[IMGDish alloc] init];
            dish.imageUrl=image.imageUrl;
            dish.restaurantId=image.restaurantId;
            dish.dishId = image.imageId;
            dish.userPhotoCountDic = image.userPhotoCountDic;
            dish.photoCreditTypeDic  = image.photoCreditTypeDic;
            dish.userIdDic  = image.userIdDic;
            dish.photoCreditDic  = image.photoCreditDic;
            dish.userReviewCountDic  = image.userReviewCountDic;
            dish.userAvatarDic  = image.userAvatarDic;
            dish.photoCreditUrlDic  = image.photoCreditUrlDic;
            dish.restaurantIdDic  = image.restaurantIdDic;
            dish.isRestaurantPhoto=YES;
            [allPhotos addObject:dish];
        }
        [allPhotos addObjectsFromArray:userPhotoArray];
        AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:allPhotos andPhotoTag:index+1 andRestaurant:restaurant andFromDetail:YES];
        albumViewController.title = restaurant.title;
        albumViewController.showPage=NO;
        albumViewController.restaurantId = restaurant.restaurantId;
        albumViewController.amplitudeType = @"RDP Banner";
        albumViewController.isUseWebP = YES;
        [self.navigationController pushViewController:albumViewController animated:YES];
        
    }else if(photoType == 2){
        
        [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Event list" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:nil andOrigin:@"Restaurant detail page" andType:nil];
        
        SearchPhotosViewController *searchPhotosViewController=[[SearchPhotosViewController alloc] initWithRestaurant:restaurant restaurantPhotoArray:eventPhotoArray];
        searchPhotosViewController.isUseWebP = YES;
        [self.navigationController pushViewController:searchPhotosViewController animated:YES];
        
    }
    
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:@"Restaurant detail page" forKey:@"Location"];
    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
    if(photoType == 1){
        [eventProperties setValue:@"User photo" forKey:@"Type"];
    }else if(photoType == 2){
        [eventProperties setValue:@"Restaurant photo" forKey:@"Type"];
    }
    [[Amplitude instance] logEvent:@"CL - See All Photos" withEventProperties:eventProperties];
}
-(void)gotoPhotos:(id)btnOrTap{
    
    [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Photo list" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:nil andOrigin:@"Restaurant detail page" andType:nil];
    
    DetailSeeAllPhotoViewController *DetailSeeAllPhotoV = [[DetailSeeAllPhotoViewController alloc] initWithRestaurant:restaurant restaurantPhotoArray:_banerImageList];
    DetailSeeAllPhotoV.isUseWebP = YES;
    [self.navigationController pushViewController:DetailSeeAllPhotoV animated:YES];
    
    [IMGAmplitudeUtil trackRestaurantWithName:@"CL - See All Photos" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:nil andOrigin:@"Restaurant detail page" andType:nil];
}
-(void)likeButtonTapped:(UIButton *)likeButton{
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:@"Favorite" forKey:@"Type"];
    [param setObject:@"Restaurant detail page" forKey:@"Location"];
    [param setObject:restaurant.restaurantId forKey:@"Restaurant_ID"];
    [param setObject:@"userFavorite" forKey:@"List_ID"];
    
    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        LoginViewController *loginVC=[[LoginViewController alloc]initWithFromWhere:[NSNumber numberWithInt:1]];
        UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
        [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
        
        return;
    }
    NSString *postUrl = @"user/restaurant/favorite";
    if(likeButton.tag==1){
        postUrl = @"user/restaurant/unfavorite";
        [[Amplitude instance] logEvent:@"BM - Unsave Restaurant Initiate" withEventProperties:param];
    }else{
        [[Amplitude instance] logEvent:@"BM - Save Restaurant Initiate" withEventProperties:param];
    }
    NSDictionary * parameters = @{@"userID":user.userId,@"t":user.token,@"restaurantID":restaurant.restaurantId,@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]};
    [DetailDataHandler saveRestaurantWithParameter:parameters andUrl:postUrl andBlock:^(NSString *succeedStr, NSString *failedString) {
        NSString *returnStatusString = succeedStr;
        if([returnStatusString isEqualToString:@"succeed"]){
            IMGRestaurantLike *restaurantLike = [[IMGRestaurantLike alloc]init];
            restaurantLike.restaurantId = restaurant.restaurantId;
            restaurantLike.userId = user.userId;
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"MyListCollectionViewReloadData" object:nil];
            if(likeButton.tag==1){
                [[Amplitude instance] logEvent:@"BM - Unsave Restaurant Succeed" withEventProperties:param];
                [RestaurantHandler saveRestaurantLike:restaurantLike liked:NO];
            }else{
                [[Amplitude instance] logEvent:@"BM - Save Restaurant Succeed" withEventProperties:param];
                [[AppsFlyerTracker sharedTracker]trackEvent:@"BM - Save Restaurant Succeed" withValues:param];
                [RestaurantHandler saveRestaurantLike:restaurantLike liked:YES];
            }
            [self loadLikeButtonFromDB:btnsView.likeBtn user:user];
        }else{
            NSString *exceptionmsg =failedString;
            if (exceptionmsg!=nil) {
                [param setObject:exceptionmsg forKey:@"Reason"];
            }
            [[Amplitude instance] logEvent:@"BM - Save Restaurant Failed" withEventProperties:param];
        }
        [[LoadingView sharedLoadingView]stopLoading];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"favoriteUpdatedNotification" object:nil];
    } andErrorBlock:^(NSError *error) {
        if(likeButton.tag==1){
            [param setObject:error.localizedDescription forKey:@"Reason"];
            [[Amplitude instance] logEvent:@"BM - Unsave Restaurant Failed" withEventProperties:param];
        }else{
            [[Amplitude instance] logEvent:@"BM - Save Restaurant Failed" withEventProperties:param];
        }
        [[LoadingView sharedLoadingView]stopLoading];
    }];
}
-(void)uploadImageViewPressed{
    IMGUser *user= [IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        LoginViewController *loginVC=[[LoginViewController alloc]initWithFromWhere:[NSNumber numberWithInt:1]];
        UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
        
        [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
        return;
    }
    ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] initWithRestaurantTitle:restaurant.title andIsFromUploadPhoto:YES andIsUploadMenuPhoto:YES andCurrentRestaurantId:restaurant.restaurantId];
    picker.maximumNumberOfSelection = 9;
    picker.assetsFilter = [ALAssetsFilter allPhotos];
    picker.showEmptyGroups=NO;
    picker.delegate = self;
    picker.isFromUploadPhoto = YES;
    picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
            NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
            return duration >= 5;
        } else {
            return YES;
        }
    }];
    [self presentViewController:picker animated:YES completion:NULL];
}
-(void)gotoOfferViewController{
    [[Amplitude instance] logEvent:@"RC - View Best Offers list" withEventProperties:@{@"Location":@"Restaurant Detail Page",@"Restaurant_ID":restaurant.restaurantId}];
    
    [IMGAmplitudeUtil trackPromoWithName:@"RC - View Restaurant Promo Image detail" andRestaurantId:restaurant.restaurantId andPromoId:restaurantOffer.offerId];
    
    [self loadSpecialOffersViewControllerWithBookDate:self.bookDate andBookTime:self.bookTime andPax:self.pax];
}

-(void)loadSpecialOffersViewControllerWithBookDate:(NSDate *)bookDate andBookTime:(NSString *)bookTime andPax:(NSNumber *)pax{
    [newOfferBadgeImageView removeFromSuperview];
    if(bookDate!=nil&&bookTime!=nil&&pax!=nil&&![bookTime isEqualToString:@""]){
        
    }else{
        [self initRestaurantOfferFromLocal:nil];
    }
    SpecialOffersViewController *specialOffersViewController = [[SpecialOffersViewController alloc]initWithRestaurant:restaurant andTimeArray:regularTimeArray andOfferArray:restaurantOfferArray];
    specialOffersViewController.amplitudeType = @"Restaurant detail page - offer list";
    if(bookDate!=nil&&bookTime!=nil&&pax!=nil&&![bookTime isEqualToString:@""]){
        specialOffersViewController.pax = pax;
        specialOffersViewController.bookDate = bookDate;
        specialOffersViewController.bookTime = bookTime;
    }
    [self.navigationController pushViewController:specialOffersViewController animated:YES];
}
-(void)telephoneButtonTapped2{
    IMGUser *user = [IMGUser currentUser];
    NSNumber *userId = [[NSNumber alloc] init];
    if (user.userId != nil)
    {
        userId = user.userId;
    }
    else
        userId = [NSNumber numberWithInt:0];
    
    [TrackHandler trackWithUserId:userId andRestaurantId:restaurant.restaurantId andContent:[NSDictionary dictionaryWithObject:@"0" forKey:@"type"]];
    [self addCallAlert];
}
- (void)mapButtonTapped2{
    
    [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Map" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:@"Restaurant detail page" andOrigin:nil andType:nil];
    
    IMGUser *user = [IMGUser currentUser];
    NSNumber *userId = [[NSNumber alloc] init];
    if (user.userId != nil)
    {
        userId = user.userId;
    }
    else
        userId = [NSNumber numberWithInt:0];
    [TrackHandler trackWithUserId:userId andRestaurantId:restaurant.restaurantId andContent:[NSDictionary dictionaryWithObject:@"1" forKey:@"type"]];
    
    MapViewController *mapViewController=[[MapViewController alloc]initWithRestaurant:restaurant];
    mapViewController.amplitudeType=@"Restaurant detail page";
    mapViewController.restaurantId=restaurant.restaurantId;
    mapViewController.fromDetail = YES;
    [self.navigationController pushViewController:mapViewController animated:YES];

}
-(void)gotoClaimYourRestaurant{
    
    [IMGAmplitudeUtil trackRDPWithName:@"CL - Restaurant Claim" andRestaurantId:restaurant.restaurantId andOrigin:@"RDP" andLocation:nil];
    
    ClaimYourRestaurantViewController *cyr = [[ClaimYourRestaurantViewController alloc] init];
    cyr.restaurantId = restaurant.restaurantId;
    cyr.claimYourRestaurantDelegate = self;
    [self.navigationController pushViewController:cyr animated:YES];
}
-(void)claimYourRestaurantGoToSuccessPage{
    SuccessClaimViewController *success = [[SuccessClaimViewController alloc] init];
    success.restaurantTitle = restaurant.title;
    [self.navigationController pushViewController:success animated:YES];
}
-(void)gotoArticle{
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:@"Restaurant detail page" forKey:@"Location"];
    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
    [[Amplitude instance] logEvent:@"CL - See All Journals" withEventProperties:eventProperties];
    
    MoreArticleViewController *moreArticleVC = [[MoreArticleViewController alloc] initWithRestaurant:restaurant.restaurantId];
    moreArticleVC.amplitudeType = @"Restaurant detail page";
    moreArticleVC.title=restaurant.title;
    [self.navigationController pushViewController:moreArticleVC animated:YES];
}
-(void)gotoJournal:(IMGMediaComment*)journal{
//    tapGesture.view.userInteractionEnabled=NO;
//    IMGMediaComment *journal = [mediaCommentArray objectAtIndex:tapGesture.view.tag - 10000];
    
    [IMGAmplitudeUtil trackJDPWithName:@"RC - View Journal detail" andJournalId:journal.mediaCommentId andOrigin:@"RDP" andChannel:nil andLocation:nil];
    
    LoadingImageViewController *loadController = [[LoadingImageViewController alloc] init];
    loadController.hiddenNavigationBarStatus = @(0);
    loadController.isShowLoadingImage = YES;
    [self.navigationController pushViewController:loadController animated:YES];
    
    [JournalHandler getJournalDetailTypeArticleIdWithJournalId:journal.mediaCommentId andJournalBlock:^(IMGMediaComment *journal) {
        if(journal==nil){
            [self.navigationController popViewControllerAnimated:NO];
            JournalListViewController *JVC = [[JournalListViewController alloc] init];
            JVC.amplitudeType = @"Restaurant detail page";
            [self.navigationController pushViewController:JVC animated:NO];
        }else if ([journal.imported intValue]== 1){
            [JournalHandler getJournalDetailWithJournalArticleId:journal.mediaCommentId andBlock:^(NSString *content,NSString *webSite) {
                [self.navigationController popViewControllerAnimated:NO];
                journal.contents = content;
                WebViewController *wvc = [[WebViewController alloc] init];
                wvc.content = journal.contents;
                wvc.journal = journal;
                wvc.webSite = webSite;
                wvc.title = journal.title;
                wvc.fromDetail = NO;
                [self.navigationController pushViewController:wvc animated:NO];
            }];
        }else{
            [self.navigationController popViewControllerAnimated:NO];
            JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
            jdvc.amplitudeType = @"Journal card on Restaurant detail page";
            jdvc.journal = journal;
            [self.navigationController pushViewController:jdvc animated:NO];
        }
//        tapGesture.view.userInteractionEnabled=YES;
    }];
}
-(void)eventsImagePressed:(UITapGestureRecognizer*)gesture{
    IMGEvent  *event= [eventArray objectAtIndex:gesture.view.tag];
    IMGDiscover *discover=[[IMGDiscover alloc]init];
    discover.sortby = SORTBY_POPULARITY;
    if(event!=nil && [event.eventId intValue]>0){
        discover.eventArray = [[NSMutableArray alloc]initWithCapacity:0];
        IMGOfferType *offerType = [[IMGOfferType alloc]init];
        offerType.offerId = event.eventId;
        offerType.eventType=[NSNumber numberWithInt:1];
        offerType.typeId = [NSNumber numberWithInt:4];
        offerType.off = [NSNumber numberWithInt:0];
        offerType.name = event.name;
        [discover.eventArray addObject:offerType];
    }
    discover.fromWhere=@4;
    RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc]initWithEvent:event];
    restaurantsViewController.amplitudeType = @"Restaurant detail page";
    [restaurantsViewController initDiscover:discover];
    [self.navigationController pushViewController:restaurantsViewController animated:YES];
    
    
    
}
-(void)diningGuideImagePressed:(IMGDiningGuide *)diningGuide{
    
    [IMGAmplitudeUtil trackGuideWithName:@"RC - View Dining Guide page" andDiningGuideId:diningGuide.diningGuideId andDiningGuideName:diningGuide.pageName andOrigin:@"Restaurant detail page"];
    
    DiningGuideRestaurantsViewController *diningGuideRestaurantsViewController = [[DiningGuideRestaurantsViewController alloc]initWithDiningGuide:diningGuide];
    diningGuideRestaurantsViewController.hiddenNavigationBarStatus=@1;
    [self.navigationController pushViewController:diningGuideRestaurantsViewController animated:YES];
}

-(void)gotoMenu:(UIButton *)sender{
    [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Menu list" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:nil andOrigin:@"Restaurant detail page" andType:nil];
    [IMGAmplitudeUtil trackRestaurantWithName:@"CL - View More Restaurant Menu" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:nil andOrigin:@"Restaurant detail page" andType:nil];
    
    sender.userInteractionEnabled=NO;
    IMGMenu *menu_ = [menuPhotoArray firstObject];
    BOOL isSelfUpload = [menu_.type intValue]==0;
    self.menuPhotoGalleryController = [[MenuPhotoGalleryController alloc] initWithRestaurant:restaurant andIsSelfUpload:isSelfUpload];
    self.menuPhotoGalleryController.isUseWebP = YES;
    [self.navigationController pushViewController:self.menuPhotoGalleryController animated:YES];

    sender.userInteractionEnabled=YES;
}

#pragma mark - call
-(void)callButtonClicked:(UIButton*)button{
    NSLog(@"call");
    
    [IMGAmplitudeUtil trackRestaurantWithName:@"CL - Call Restaurant CTA" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:@"RDP Floating Bar" andOrigin:nil andType:nil];
    
    IMGUser *user = [IMGUser currentUser];
    NSNumber *userId = [[NSNumber alloc] init];
    if (user.userId != nil)
    {
        userId = user.userId;
    }
    else
        userId = [NSNumber numberWithInt:0];
    
    [TrackHandler trackWithUserId:userId andRestaurantId:restaurant.restaurantId andContent:[NSDictionary dictionaryWithObject:@"0" forKey:@"type"]];
    
    
    if(@available(iOS 11.0, *)){
        [SendCall sendCall:restaurant.phoneNumber];
    }else{
        [UIAlertView bk_showAlertViewWithTitle:nil message:restaurant.phoneNumber cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Call"] handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if(buttonIndex==0){
                [alertView dismissWithClickedButtonIndex:0 animated:NO];
            }else if (buttonIndex==1){
                NSMutableDictionary* eventDic=[[NSMutableDictionary alloc]init];
                [eventDic setValue:@"Restaurant detail page" forKey:@"Location"];
                [eventDic setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
                [[Amplitude instance] logEvent:@"CL - Phone Number" withEventProperties:eventDic];
                [[Amplitude instance] logEvent:@"CL - Call Restaurant CTA" withEventProperties:eventDic];
                [[AppsFlyerTracker sharedTracker] trackEvent:@"CL - Call Restaurant CTA" withValues:eventDic];
                [SendCall sendCall:restaurant.phoneNumber];
            }
            
        }];
    }
}

#pragma mark - booknow
-(void)bookButtonClicked:(UIButton*)button{
  
    [IMGAmplitudeUtil trackRDPWithName:@"CL - Book Now" andRestaurantId:restaurant.restaurantId andOrigin:nil andLocation:@"Restaurant detail page"];
    
    [IMGAmplitudeUtil trackRDPWithName:@"TR - Reservation Initiate" andRestaurantId:restaurant.restaurantId andOrigin:nil andLocation:@"RDP Floating Bar"];
    
    if (bookbuttonIsClicked) {
        bookbuttonIsClicked=NO;
        [RestaurantHandler getOfferFromServer:restaurant.restaurantId andBookDate:nil andBlock:^{
            if(restaurantOfferArray!=nil){
                [restaurantOfferArray removeAllObjects];
            }
            [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGRestaurantOffer WHERE restaurantId = '%@' ORDER BY offerType,offerSlotMaxDisc desc;",restaurant.restaurantId] successBlock:^(FMResultSet *resultSet) {
                while ([resultSet next]) {
                    IMGRestaurantOffer  *tmpRestaurantOffer=[[IMGRestaurantOffer alloc]init];
                    [tmpRestaurantOffer setValueWithResultSet:resultSet];
                    [restaurantOfferArray addObject:tmpRestaurantOffer];
                }
                [resultSet close];
            }failureBlock:^(NSError *error) {
                NSLog(@"initRestaurantOfferFromLocal error when get datas:%@",error.description);
            }];
            
            SelectPaxViewController *selectDinersViewController = [[SelectPaxViewController alloc]initWithRestaurant:restaurant andTimeArray:nil andOfferArray:restaurantOfferArray andOffer:nil];
            [self.navigationController pushViewController:selectDinersViewController animated:YES];
        }];
    }
}
-(void)gotoMap{
    
    [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Map" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:@"Restaurant detail page" andOrigin:nil andType:nil];
    
    IMGUser *user = [IMGUser currentUser];
    NSNumber *userId = [[NSNumber alloc] init];
    if (user.userId != nil)
    {
        userId = user.userId;
    }
    else
        userId = [NSNumber numberWithInt:0];
    [TrackHandler trackWithUserId:userId andRestaurantId:restaurant.restaurantId andContent:[NSDictionary dictionaryWithObject:@"1" forKey:@"type"]];
    
    MapViewController *mapViewController=[[MapViewController alloc]initWithRestaurant:restaurant];
    mapViewController.amplitudeType=@"Restaurant detail page";
    mapViewController.restaurantId=restaurant.restaurantId;
    mapViewController.fromDetail = YES;
    [self.navigationController pushViewController:mapViewController animated:YES];
}

#pragma mark - hander data
-(void)writeRDPRestToFile{
    NSMutableData *data = [NSMutableData data];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc]initForWritingWithMutableData:data];
    [archiver encodeObject:rdpRest forKey:[NSString stringWithFormat:@"%@",rdpRest.restaurantId]];
    [archiver finishEncoding];
    [data writeToFile:[NSString stringWithFormat:@"%@/%@",qravedRDPCachePath,rdpRest.restaurantId] atomically:YES];
}
-(void)refreshViewWithDB{
    [DetailDataHandler getRestaurantFirstDataFromServiceNow:restaurant.restaurantId andBlock:^(IMGRestaurant *restaurantTemp,NSArray *eventList,IMGRestaurantOffer *restaurantOfferTemp,NSArray *banerImageArr,IMGRDPRestaurant *rdpRestTmp,NSNumber *showClaimLoc,NSArray *couponArray) {
        showClaim = showClaimLoc;
        rdpRest = rdpRestTmp;
        [self writeRDPRestToFile];
        if (isLoading) {
            isLoading = NO;
            [_refreshTableHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:scrollView];
        }else{
            [loadingImageView stopLoading];
            loadingImageView = nil;
        }
        [couponArr removeAllObjects];
        [couponArr addObjectsFromArray:couponArray];

        [DetailDataHandler getRestaurantSecondDataFromService:restaurant.restaurantId and:rdpRest andBlock:^(NSArray *menuList,NSArray *dishList,NSArray *moreDishList,NSArray *reviewList,NSArray *eventList,NSArray *diningGuideList,NSArray *journalList,NSNumber *menuPhotoCount,NSNumber *dishCount,NSNumber *reviewedUserCount,IMGRDPRestaurant *rdpCach) {
            _reviewedUserCount = reviewedUserCount;

            CGPoint lastPoint = scrollView.contentOffset;
            currentY = 0;
            
            for (UIView *view in scrollView.subviews) {
                [view removeFromSuperview];
            }
            [self addPullToRefreshHeader];
            [scrollView setFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+[UIDevice heightDifference] - IMG_StatusBarAndNavigationBarHeight)];// -64
            
            restaurant = restaurantTemp;
            _banerImageList = [[NSMutableArray alloc] init];
            for (NSArray *arr in banerImageArr)
            {
                IMGRestaurantImage  *restaurantImage=[[IMGRestaurantImage alloc]init];
                restaurantImage.imageId = [arr objectAtIndex:0];
                restaurantImage.imageUrl = [arr objectAtIndex:1];
                [restaurantImage updatePhotoCredit:[arr objectAtIndex:2]];
                [_banerImageList addObject:restaurantImage];
            }
            restaurantOffer = restaurantOfferTemp;
            
            [self addBanerImageView];
            
            [self addRamadanView];
            [self addStateView];
            [self addShopDetail];
            [self addBtnsView];
            [self addCouponView];
            [self addFirstOfferView];
//            [self addMapView];
//            [self addInformationView];
            [[LoadingView sharedLoadingView] stopLoading];
            rdpRest = rdpCach;
            menuDataArr = [[NSArray alloc] initWithArray:menuList];
            restaurant.menuCount = menuPhotoCount;
            
            [self addMenuView];
            [newDishArrM removeAllObjects];
            dishDataArr = [[NSArray alloc] initWithArray:dishList];
            restaurant.disCount = dishCount;
            
            moreDishDataArr = [[NSMutableArray alloc] init];
            NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            NSDate *createTime=[[NSDate alloc] init];
            for (NSDictionary *dic in moreDishList)
            {
                IMGDish *dish=[[IMGDish alloc]init];
                dish.type = [dic objectForKey:@"type"];
                
                [formatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
                formatter.timeZone=[NSTimeZone timeZoneWithName:@"Asia/Jakarta"];
                if ([dish.type intValue]==1) {
                    NSDictionary *tempDic = [dic objectForKey:@"dish"];
                    [dish setValuesForKeysWithDictionary:tempDic];
                    
                    createTime=[formatter dateFromString:[tempDic objectForKey:@"createTime"]];
                    dish.createTime=[NSNumber numberWithUnsignedLongLong:createTime.timeIntervalSince1970];
                    dish.userName=[tempDic objectForKey:@"creator"];
                    dish.descriptionStr=[tempDic objectForKey:@"description"];
                    dish.restaurantTitle = [tempDic objectForKey:@"restaurantTitle"];
                    dish.isLike = [[tempDic objectForKey:@"isLike"] boolValue];
                    [dish updatePhotoCredit:[tempDic objectForKey:@"photoCredit"]];
                    [moreDishDataArr addObject:dish];
                    
                }else if ([dish.type intValue]==2){
                    NSDictionary *dishDictionary = [dic objectForKey:@"instagramPhoto"];
                    [dish setValuesForKeysWithDictionary:dishDictionary];
                    dish.userName=[dishDictionary objectForKey:@"instagramUserName"];
                    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[[dishDictionary objectForKey:@"approvedDate"] longLongValue]/1000];
                    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
                    
                    createTime=[formatter dateFromString:confromTimespStr];
                    dish.createTime=[NSNumber numberWithDouble:createTime.timeIntervalSince1970];
                    dish.imageUrl = [dishDictionary objectForKey:@"standardResolutionImage"];
                    
                    [moreDishDataArr addObject:dish];
                }
            }
            [self addPhotosView:1];
            
            reviewDataArr = [[NSMutableArray alloc] initWithArray:reviewList];
            [self addReviewsView_v1];
            if (eventList) {
                
                eventDataArr = [[NSArray alloc] initWithArray:eventList];
            }
            if (diningGuideList) {
                
                diningGuideDataArr = [[NSArray alloc] initWithArray:diningGuideList];
            }
            if (journalList) {
                
                journalDataArr = [[NSArray alloc] initWithArray:journalList];
            }
            
            [scrollView setContentOffset:lastPoint animated:NO];

        } andBlockEventCountWithDiningGuideCount:^(NSNumber *eventCount, NSNumber *diningGuideCount,NSNumber *journalCount) {
            rdpRest.isHadRDPSecondData = [NSNumber numberWithInt:1];
            [self writeRDPRestToFile];
            
            restaurant.eventCount = eventCount;
            restaurant.diningGuideCount = diningGuideCount;
            restaurant.journalCount = journalCount;
            [self addFeaturedInView];
            NSLog(@"%f,getRestaurantSecondData 3.0",[[NSDate date]timeIntervalSince1970]);
            
            [loadingView removeFromSuperview];
    
        }];
        
        NSMutableString *phoneNumber;
        if (restaurant.phoneNumber.length >1)
        {
            phoneNumber = [NSMutableString stringWithString:restaurant.phoneNumber];
        }
        [self judgeScrollViewFrameWithPhoneNumber:phoneNumber];
    }];
    
    
}
-(void)refreshView{

    [DetailDataHandler getRestaurantFirstDataFromServiceNow:restaurant.restaurantId andBlock:^(IMGRestaurant *restaurantTemp,NSArray *eventList,IMGRestaurantOffer *restaurantOfferTemp,NSArray *banerImageArr,IMGRDPRestaurant *rdpRestTmp,NSNumber *showClaimLoc,NSArray *couponArray) {
        showClaim = showClaimLoc;
        rdpRest = rdpRestTmp;
        [self writeRDPRestToFile];
        if (isLoading) {
            isLoading = NO;
            [_refreshTableHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:scrollView];
        }else{
            [loadingImageView stopLoading];
            loadingImageView = nil;
        }
        currentY = 0;
        for (UIView *view in scrollView.subviews) {
            [view removeFromSuperview];
        }
        
        [self addPullToRefreshHeader];
        [scrollView setFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+[UIDevice heightDifference] - IMG_StatusBarAndNavigationBarHeight)];//-64
        
        
        restaurant = restaurantTemp;
        _banerImageList = [[NSMutableArray alloc] init];
        for (NSArray *arr in banerImageArr)
        {
            IMGRestaurantImage  *restaurantImage=[[IMGRestaurantImage alloc]init];
            restaurantImage.imageId = [arr objectAtIndex:0];
            restaurantImage.imageUrl = [arr objectAtIndex:1];
            [restaurantImage updatePhotoCredit:[arr objectAtIndex:2]];
            [_banerImageList addObject:restaurantImage];
        }
        restaurantOffer = restaurantOfferTemp;
        [couponArr removeAllObjects];
        [couponArr addObjectsFromArray:couponArray];
        
        [self addBanerImageView];
        
        [self addOtherView];
    }];
}
-(void)addPullToRefreshHeader {
    if (_refreshTableHeaderView && [_refreshTableHeaderView superview]) {
        [_refreshTableHeaderView removeFromSuperview];
    }
    _refreshTableHeaderView =[[EGORefreshTableHeaderView alloc]initWithFrame:CGRectMake(0, - REFRESH_HEADER_HEIGHT, DeviceWidth, REFRESH_HEADER_HEIGHT)];
    _refreshTableHeaderView.delegate=self;
    [_refreshTableHeaderView setBackgroundColor:[UIColor whiteColor] textColor:nil arrowImage:[UIImage imageNamed:@"arrowDown.png"]];
    _refreshTableHeaderView.statusLabel.font=[UIFont boldSystemFontOfSize:12.0];
    [scrollView addSubview:_refreshTableHeaderView];
    [_refreshTableHeaderView refreshLastUpdatedDate];
}
-(void)loadLikeButtonFromDB:(UIButton *)likeButton user:(IMGUser *)user{
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGRestaurantLike WHERE restaurantId = '%@' and userId='%@';",restaurant.restaurantId,user.userId] successBlock:^(FMResultSet *resultSet) {
        if ([resultSet next]) {
            likeButton.tag=1;
            for (UIView *view in likeButton.subviews)
            {
                [view removeFromSuperview];
            }
            UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, likeButton.frame.size.width, likeButton.frame.size.height)];
            backgroundImage.image = [UIImage imageNamed:@"favoriteRedImage"];
            [likeButton addSubview:backgroundImage];
        }else{
            likeButton.tag=0;
            for (UIView *view in likeButton.subviews)
            {
                [view removeFromSuperview];
            }
            UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, likeButton.frame.size.width, likeButton.frame.size.height)];
            backgroundImage.image = [UIImage imageNamed:@"favoriteGrayImage"];
            [likeButton addSubview:backgroundImage];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"loadLikeButtonFromDB error when get datas:%@",error.description);
    }];
}
-(void)initRestaurantOfferFromLocal:(NSDate *)bookDate{
    NSString *currentDateStr;
    NSDateFormatter *formatter = [[NSDateFormatter  alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    if(bookDate==nil){
        NSDate *date = [NSDate date];
        currentDateStr = [formatter stringFromDate:date];
        if(restaurantOfferArray!=nil){
            [restaurantOfferArray removeAllObjects];
        }else{
            restaurantOfferArray=[[NSMutableArray alloc]initWithCapacity:0];
        }
        [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGRestaurantOffer WHERE restaurantId='%@' ;",restaurant.restaurantId] successBlock:^(FMResultSet *resultSet) {
            while ([resultSet next]) {
                IMGRestaurantOffer  *tmpRestaurantOffer=[[IMGRestaurantOffer alloc]init];
                [tmpRestaurantOffer setValueWithResultSet:resultSet];
                [restaurantOfferArray addObject:tmpRestaurantOffer];
            }
            [resultSet close];
        }failureBlock:^(NSError *error) {
            NSLog(@"initRestaurantOfferFromLocal error when get datas:%@",error.description);
        }];
        
        [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGRestaurantOffer WHERE restaurantId = '%@' and offerStartDate<='%@' and offerEndDate>='%@' and offerType!=4 ORDER BY offerType,offerSlotMaxDisc desc;",restaurant.restaurantId,currentDateStr,currentDateStr] successBlock:^(FMResultSet *resultSet) {
            while ([resultSet next]) {
                IMGRestaurantOffer  *tmpRestaurantOffer=[[IMGRestaurantOffer alloc]init];
                [tmpRestaurantOffer setValueWithResultSet:resultSet];
                [restaurantOfferArray addObject:tmpRestaurantOffer];
            }
            [resultSet close];
        }failureBlock:^(NSError *error) {
            NSLog(@"initRestaurantOfferFromLocal error when get datas:%@",error.description);
        }];
        
    }
}

#pragma mark - ego refresh table view
-(void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view{
    isLoading = YES;
    [self refreshView];
}

#pragma mark - UIPopoverListViewDataSource and delegate
-(UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath{
    if (popoverListView.tag == 99)
    {
        static NSString *identifier = @"cell";
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET*2, 12, 30, 30)];
        [cell.contentView addSubview:iconImage];
        
        Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(70, 15, DeviceWidth-70, 23)
                                            andTextFont:[UIFont systemFontOfSize:15]
                                           andTextColor:[UIColor color333333]
                                           andTextLines:0];
        
        [cell.contentView addSubview:titleLabel];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSInteger row = indexPath.row;
        if(row == 0){
            titleLabel.text=Write_REVIEW;
            titleLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:15];
            titleLabel.textColor = [UIColor color222222];
            iconImage.image = [UIImage imageNamed:@"reviewBrownImage"];
        }else if (row == 1){
            titleLabel.text = L(@"Suggest an edit");
            titleLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:15];
            titleLabel.textColor = [UIColor color222222];
            iconImage.image = [UIImage imageNamed:@"editImage"];
        }else if (row == 2){
            titleLabel.text = L(@"Upload Photo");
            titleLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:15];
            titleLabel.textColor = [UIColor color222222];
            iconImage.image = [UIImage imageNamed:@"uploadImage"];
        }else
        {
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.textLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:18];
            cell.textLabel.textColor = [UIColor colorWithRed:23/255.0 green:127/255.0 blue:252/255.0 alpha:1];
            cell.textLabel.text = L(@"Cancel");
        }
        return cell;
        
    }
    else if (popoverListView.tag == 100)
    {
        static NSString *identifier = @"cell";
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET*2, 12, 30, 30)];
        [cell.contentView addSubview:iconImage];
        
        Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(70, 15, DeviceWidth-70, 23)
                                            andTextFont:[UIFont systemFontOfSize:15]
                                           andTextColor:[UIColor color333333]
                                           andTextLines:0];
        
        [cell.contentView addSubview:titleLabel];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSInteger row = indexPath.row;
        if(row == 0){
            titleLabel.text = @"Edit Review";
            titleLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:15];
            titleLabel.textColor = [UIColor color222222];
            iconImage.image = [UIImage imageNamed:@"listEditGreen"];
        }
        else
        {
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.textLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:18];
            cell.textLabel.textColor = [UIColor colorWithRed:23/255.0 green:127/255.0 blue:252/255.0 alpha:1];
            cell.textLabel.text = L(@"Cancel");
        }
        return cell;
    }
    return nil;
}
-(NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section{
    if (popoverListView.tag == 99)
    {
        return 4;
    }
    else if (popoverListView.tag == 100)
        return 2;
    return 0;
}
-(void)popoverListView:(UIPopoverListView *)popoverListView didSelectIndexPath:(NSIndexPath *)indexPath{
    if (popoverListView.tag == 99)
    {
        if (indexPath.row == 0)
        {
            [self judgeReview];
        }else if (indexPath.row == 1)
        {
            [self gotoSuggestView];
        }else if (indexPath.row == 2)
        {
            [self uploadPhoto];
        }
        [popoverListView dismiss];
    }
    else if (popoverListView.tag == 100)
    {
        {
            if (indexPath.row == 0)
            {
                ReviewPublishViewController *rpvc = [[ReviewPublishViewController alloc] initWithRestaurant:restaurant andOverallRating:[userReviewTemp.reviewScore floatValue]/2 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
                rpvc.isFromDetail = YES;
                rpvc.isUpdateReview = YES;
                rpvc.isEditReview = YES;
                rpvc.amplitudeType = @"RDP";
                rpvc.isEdit = YES;
                rpvc.reviewId = userReviewTemp.reviewId;
                rpvc.reviewPublishViewControllerDelegate=self;
                [self presentViewController:[[UINavigationController alloc] initWithRootViewController:rpvc] animated:YES completion:^{
                    
                }];
            }
            [popoverListView dismiss];
        }
    }
}
-(CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 53.0f;
}

#pragma mark - add  write review & upload photo
-(void)gotoSuggestView{
    IMGUser * user = [IMGUser currentUser];
    if(user.userId==nil){
        [[AppDelegate ShareApp]goToLoginController:[NSNumber numberWithInt:1]];
        return;
    }
    if(user.token==nil){
        [[AppDelegate ShareApp]goToLoginController:[NSNumber numberWithInt:1]];
        return;
    }
    
    [IMGAmplitudeUtil trackRDPWithName:@"UC - Suggest Edit Initiate" andRestaurantId:restaurant.restaurantId andOrigin:nil andLocation:nil];
    
    SuggestViewController *svc = [[SuggestViewController alloc] initWithRestaurant:restaurant];
    [self.navigationController pushViewController:svc animated:YES];
    
}
-(void)writeReview{
    ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc]initWithRestaurant:restaurant andOverallRating:ratingCount andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
    publishReviewVC.amplitudeType = @"RDP";
    publishReviewVC.isEdit = YES;
    publishReviewVC.isEditReview = YES;
    publishReviewVC.ifSuccessLoginClickPost = YES;
    publishReviewVC.reviewPublishViewControllerDelegate =self;
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:publishReviewVC] animated:YES completion:nil];
}
-(void)writeNewReview{
    ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc]initWithRestaurant:restaurant andOverallRating:ratingCount andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
    publishReviewVC.amplitudeType = @"RDP";
    publishReviewVC.isEdit = YES;
    publishReviewVC.reviewPublishViewControllerDelegate=self;
    publishReviewVC.ifSuccessLoginClickPost = YES;
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:publishReviewVC] animated:YES completion:nil];
    
}
-(void)reloadPhotosImageView:(NSArray*)dishArr{
    for(id dish in dishArr){
        [newDishArrM insertObject:dish atIndex:0];
    }
    
    currentY = menuView.endPointY;
    [self addPhotosView:1];
    
    reviewsView_v1.frame = CGRectMake(0, currentY + CELL_CONTENT_OFFSET, reviewsView_v1.frame.size.width, reviewsView_v1.frame.size.height);
    currentY=reviewsView_v1.endPointY;
    
    featuredInView.frame = CGRectMake(0, currentY + CELL_CONTENT_OFFSET, featuredInView.frame.size.width, featuredInView.frame.size.height);
    currentY=featuredInView.endPointY;
    
    [scrollView setContentSize:CGSizeMake(DeviceWidth, currentY+SCORLLVIEW_Y)];
    
}

-(void)gotoEvents{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:@"Restaurant detail page" forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL - See All Dining Guides" withEventProperties:param];
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:@"Restaurant detail page" forKey:@"Location"];
    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
    [[Amplitude instance] logEvent:@"CL - See All Events" withEventProperties:eventProperties];
    
    EventsAndDiningViewController *eadvc = [[EventsAndDiningViewController alloc] initWithDiningGuideArr:diningGuideArray andRestaurantName:restaurant.title];
    eadvc.restaurantId = restaurant.restaurantId;
    [self.navigationController pushViewController:eadvc animated:YES];
}
-(void)ReloadReviewsView_v1WithUpdateUserReview:(NSNotification *)notification{
    
    [[LoadingView sharedLoadingView] startLoading];
    
    [DetailDataHandler getRestaurantSecondDataFromService:restaurant.restaurantId and:rdpRest andBlock:^(NSArray *menuList,NSArray *dishList,NSArray *moreDishList,NSArray *reviewList,NSArray *eventList,NSArray *diningGuideList,NSArray *journalList,NSNumber *menuPhotoCount,NSNumber *dishCount,NSNumber *reviewedUserCount,IMGRDPRestaurant *rdpCach) {
        [reviewDataArr removeAllObjects];
        [reviewDataArr addObjectsFromArray:reviewList];

        currentY = reviewsView_v1.frame.origin.y;
        [reviewsView_v1 removeFromSuperview];
        [self addReviewsView_v1];
        featuredInView.frame = CGRectMake(0, currentY + CELL_CONTENT_OFFSET, featuredInView.frame.size.width, featuredInView.frame.size.height);
        currentY=featuredInView.endPointY;
        [scrollView setContentSize:CGSizeMake(DeviceWidth, currentY+SCORLLVIEW_Y)];
        
        [[LoadingView sharedLoadingView] stopLoading];
    } andBlockEventCountWithDiningGuideCount:^(NSNumber *eventCount, NSNumber *diningGuideCount,NSNumber *journalCount) {
    }];
    
}
-(void)addNewReviewWithDB:(NSDictionary *)reviewDicTmp{
    NSDictionary *userDic = [reviewDicTmp objectForKey:@"user"];
    NSMutableDictionary *reviewDic = [[NSMutableDictionary alloc] initWithDictionary:reviewDicTmp copyItems:YES];
    [reviewDic setObject:[userDic objectForKey:@"avatar"] forKey:@"avatar"];
    [reviewDic setObject:[userDic objectForKey:@"id"] forKey:@"userId"];
    [reviewDic setObject:[userDic objectForKey:@"fullName"] forKey:@"fullName"];
    
    NSMutableArray *reviewArrTmp = [[NSMutableArray alloc] initWithArray:rdpRest.reviewList];
    for (NSDictionary *dic in reviewArrTmp)
    {
        if ([[dic objectForKey:@"id"] intValue] == [[reviewDicTmp objectForKey:@"id"] intValue])
        {
            [reviewArrTmp removeObject:dic];
            break;
        }
    }
    [reviewArrTmp insertObject:reviewDic atIndex:0];
    rdpRest.reviewList = [NSMutableArray arrayWithArray:reviewArrTmp];
    [self writeRDPRestToFile];
    
}
-(void)gotoReview:(id)sender{
    if ( (sender != nil) && ([sender isKindOfClass:[IMGReview class]]) ) {
        IMGReview *review = (IMGReview*)sender;
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:@"Restaurant detail page - review list" forKey:@"Origin"];
        [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
        [[Amplitude instance] logEvent:@"RC - View Restaurant Review detail" withEventProperties:eventProperties];
    }else{
        [IMGAmplitudeUtil trackRestaurantWithName:@"CL - See All Reviews" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:@"Restaurant detail page" andOrigin:nil andType:nil];
    }
    
    [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Review list" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:nil andOrigin:@"Restaurant detail page" andType:nil];
    self.reviewViewController=[[ReviewViewController alloc] initWithRestaurant:restaurant];

    [self.navigationController pushViewController:self.reviewViewController animated:YES];
    
}

#pragma mark - MapViewDelegate
- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation{
    MKAnnotationView *pinView = nil;
    static NSString *defaultPinID = @"imaginato_map";
    pinView = (MKAnnotationView *)[theMapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    if ( pinView == nil ) pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
    pinView.canShowCallout = YES;
    pinView.image = [[UIImage imageNamed:@"ic_red_pin.png"] imageByScalingAndCroppingForSize:CGSizeMake(27, 33)];
    return pinView;
}

#pragma mark - scroll view delegate
-(void)scrollViewDidEndDragging:(UIScrollView *)scroll willDecelerate:(BOOL)decelerate {
    [_refreshTableHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}
-(void)scrollViewDidScroll:(UIScrollView *)scroll {
    
    [_refreshTableHeaderView egoRefreshScrollViewDidScroll:scrollView];
    if(scrollView.contentOffset.y>64){
        [UIView animateWithDuration:0.3 animations:^{
            
            [self blackNav];
        }];
    }else{
        [UIView animateWithDuration:0.3 animations:^{
            [self whiteNav];
            
        }];
    }
}
- (void)whiteNav{
    navigationView.backgroundColor = [UIColor clearColor];
    [backButton setImage:[UIImage imageNamed:@"icon_wrong"] forState:UIControlStateNormal];
    [shareButton setImage:[UIImage imageNamed:@"ic_share_guide"] forState:UIControlStateNormal];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
   navTitleLabel.hidden=YES;
}
- (void)blackNav{
    navigationView.backgroundColor = [UIColor whiteColor];
    [backButton setImage:[UIImage imageNamed:@"icon_black_wrong"] forState:UIControlStateNormal];
    [shareButton setImage:[UIImage imageNamed:@"ic_share_guide_black"] forState:UIControlStateNormal];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    navTitleLabel.hidden=NO;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}
-(void)EScrollerViewDidClicked:(NSUInteger)index ContentOffset:(CGPoint)contentoffset{
    if (contentoffset.x>DeviceWidth*(_banerImageList.count-1)) {
        if (userPhotoArray.count>0&&userPhotoArray!=nil) {
            [self gotoPhotos:@"1" andindex:index];
        }
    }
}
-(void)EScrollerViewDidClicked:(NSUInteger)index{
    [self gotoPhotos:@"1" andindex:index];
}

#pragma mark -ImagePickerController delegate methods
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage=[info objectForKey:UIImagePickerControllerEditedImage];
    CGFloat imageHeight=0;
    CGFloat imageWidth=0;
    if(chosenImage.size.width>UPLOAD_PICTURE_SIZE_WIDTH){
        imageHeight=UPLOAD_PICTURE_SIZE_WIDTH*chosenImage.size.height/chosenImage.size.width;
        imageWidth=UPLOAD_PICTURE_SIZE_WIDTH;
    }else{
        imageHeight=chosenImage.size.height;
        imageWidth=chosenImage.size.width;
    }
    
    CGSize tagetSize=CGSizeMake(imageWidth, imageHeight);
    UIImage*tagetImage=[chosenImage imageWithScaledToSize:tagetSize];
    
    [picker dismissViewControllerAnimated:NO completion:^{
        AddPhotoViewController *addPhotoVC = [[AddPhotoViewController alloc]initWithRestauarnt:restaurant];
        addPhotoVC.isWriteReview = NO;
        addPhotoVC.selectedImage = tagetImage;
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:addPhotoVC] animated:YES completion:nil];
    }];
}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - ZYQAssetPickerController Delegate
-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    
    NSMutableArray *photosArrM = [[NSMutableArray alloc] init];
    for (int i=0; i<assets.count; i++) {
        ALAsset *asset=assets[i];
        UIImageView *imgview=[[UIImageView alloc] init];
        imgview.contentMode=UIViewContentModeScaleAspectFill;
        imgview.clipsToBounds=YES;
        UIImage *tempImg=[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage];
        IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
        photo.image = tempImg;
        photo.photoUrl =[NSString stringWithFormat:@"%@",[asset valueForProperty:ALAssetPropertyAssetURL]];
        [photosArrM addObject:photo];
    }
    [picker dismissViewControllerAnimated:NO completion:^{
        if (picker.isUploadMenuPhoto) {
            MenuPublishViewController *publishReviewVC = [[MenuPublishViewController alloc]initWithRestaurant:restaurant];
            publishReviewVC.amplitudeType = @"Restaurant detail page";
            publishReviewVC.photosArrM = [NSMutableArray arrayWithArray:photosArrM];
            publishReviewVC.menuPublishViewControllerDelegate = self;
            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:publishReviewVC] animated:YES completion:^{
                
            }];
        }else{
            ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc]initWithRestaurant:restaurant andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
            publishReviewVC.isEdit = YES;
            publishReviewVC.isUploadPhoto = picker.isFromUploadPhoto;
            publishReviewVC.ifSuccessLoginClickPost=YES;
            publishReviewVC.amplitudeType = @"RDP";
            publishReviewVC.reviewPublishViewControllerDelegate = self;
            publishReviewVC.photosArrM = [NSMutableArray arrayWithArray:photosArrM];
            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:publishReviewVC] animated:YES completion:NULL];
        }
    }];
}

#pragma mark - actionSheetDelegate
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self performSelector:@selector(shareButtonClick:) withObject:nil afterDelay:0.5f];
    }if (buttonIndex == 1) {
        [self uploadPhoto];
    }if (buttonIndex == 2) {
    }if (buttonIndex == 3) {
    }
}


#pragma mark - reviewDelegate
- (void)shareButtonTapped:(IMGReview *)reviewView shareButton:(UIButton *)button{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    [[LoadingView sharedLoadingView] startLoading];
//    IMGReview *review=[reviewsArray objectAtIndex:index];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/review/%@",QRAVED_WEB_SERVER_OLD,reviewView.reviewId]];
    NSString *shareContentString = [NSString stringWithFormat:@"CHECK IT OUT NOW! A Restaurant Review by %@ on Qraved",reviewView.fullName];
    ReviewActivityItemProvider *provider=[[ReviewActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
    provider.username = reviewView.fullName;
    provider.restaurantTitle = reviewView.restaurantTitle;
    provider.url=url;
    provider.title=shareContentString;
    
    TwitterActivityItemProvider *twitterProvider=[[TwitterActivityItemProvider alloc] initWithPlaceholderItem:@""];
    twitterProvider.title=[NSString stringWithFormat:L(@"Check out %@ review at %@ on #Qraved!"),reviewView.fullName,reviewView.restaurantTitle];
    UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterProvider] applicationActivities:nil];
    activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
    [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK IT OUT NOW!A Restaurant Review By %@ on Qraved!"),reviewView.fullName] forKey:@"subject"];
    activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
        if (completed) {
            
            [IMGAmplitudeUtil trackReviewViewWithName:@"SH - Share Review" andRestaurantId:restaurant.restaurantId andReviewId:reviewView.reviewId andReviewUserId:reviewView.userId andOrigin:nil andLocation:@"Restaurant detail page - review list" andChannel:activityType];
        }else{
        }
    };
    if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
        // iOS8
        activityCtl.popoverPresentationController.sourceView = button;
    }
    
    [self presentViewController:activityCtl animated:YES completion:NULL];
    [[LoadingView sharedLoadingView] stopLoading];
}


-(void)previousLableTap:(IMGReview *)review_{
    UserAllReviewController *userAllReviewController = [[UserAllReviewController alloc] initWithRestaurant:restaurant and:review_];
    [self.navigationController pushViewController:userAllReviewController animated:YES];
}
-(void)reviewViewMsgClick:(ReviewView *)reviewView readMoreTapped:(id)review isReadMore:(BOOL)isReadMore{
    IMGReview *thisReview = (IMGReview *)review;
    [[Amplitude instance] logEvent:@"CL - Read More on Review" withEventProperties:@{@"Location":@"Restaurant detail page"}];
    [reveiwCardIsReadMore setObject:isReadMore?@1:@0 forKey:thisReview.targetId];
    [self ReloadReviewsView_v1WithUpdateUserReview:nil];
}
-(void)reviewViewClick:(IMGReview *)review{
    [self gotoReview:review];
}
-(void)reviewUserNameOrImageTapped:(id)sender{
    if ([sender isKindOfClass:[IMGUser class]])
    {
        IMGUser *user = (IMGUser *)sender;
        OtherProfileViewController *opvc = [[OtherProfileViewController alloc] init];
        opvc.amplitude = self.amplitudeType;
        IMGUser *currentUser = [IMGUser currentUser];
        if ([currentUser.userId intValue] == [user.userId intValue])
        {
            opvc.isOtherUser = NO;
        }else
        {
            opvc.isOtherUser = YES;
        }
        opvc.otherUser = user;
        [self.navigationController pushViewController:opvc animated:YES];
    }
}
-(void)reviewImagePressed:(ReviewView*)review images:(NSArray*)images withTag:(int)tag{
    AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:images andPhotoTag:tag andRestaurant:restaurant andFromDetail:YES];
    albumViewController.title = [review.review.title filterHtml];
    albumViewController.review= review.review;
    albumViewController.showPage=NO;
    albumViewController.amplitudeType = @"Restaurant detail page - ";
    [self.navigationController pushViewController:albumViewController animated:YES];
}

- (void)gotoJournalDetail:(NSNumber *)journalId{
    IMGMediaComment *journal = [[IMGMediaComment alloc] init];
    journal.mediaCommentId = journalId;
    JournalDetailViewController *detailVC = [[JournalDetailViewController alloc] init];
    detailVC.journal = journal;
    [self.navigationController pushViewController:detailVC animated:YES];
}

-(void)instagramImagePressed:(IMGReview *)review images:(NSArray*)images withTag:(int)tag{
    NSMutableArray *imgArray = [NSMutableArray array];
    for (IMGDish *dish in images) {
        V2_InstagramModel *model = [[V2_InstagramModel alloc] init];
        model.restaurant_id = restaurant.restaurantId;
        model.restaurant_name = restaurant.title;
        model.instagram_user_name = review.userName;
        model.instagram_user_profile_picture = review.userPicture;
        model.instagram_create_date = review.createTimeStr;
        model.low_resolution_image = dish.lowResolutionImage;
        model.thumbnail_image  =dish.thumbnailImage;
        model.standard_resolution_image = dish.standardResolutionImage;
        model.instagram_link = dish.instagramLink;
        [imgArray addObject:model];
    }
    
    V2_PhotoDetailViewController *photoDetail = [[V2_PhotoDetailViewController alloc] init];
    photoDetail.currentInstagram = [imgArray objectAtIndex:tag];
    photoDetail.photoArray = imgArray;
    photoDetail.currentIndex = tag;
    [self.navigationController pushViewController:photoDetail animated:YES];
}
- (void)userReviewMoreBtnClickWithIMGUserReview:(IMGUserReview*)review
{
    
    userReviewTemp = review;
    CGFloat yHeight = 152.0f;
    UIPopoverListView *poplistviewWithUser = [[UIPopoverListView alloc] initWithFrame:CGRectMake(0, DeviceHeight-yHeight, DeviceWidth, yHeight)];
    poplistviewWithUser.delegate = self;
    poplistviewWithUser.tag = 100;
    poplistviewWithUser.datasource = self;
    poplistviewWithUser.listView.scrollEnabled = FALSE;
    [poplistviewWithUser setTitle:nil];
    [poplistviewWithUser show];
    
}
#pragma mark - private method
-(void)countStar{
    IMGUser *user = [IMGUser currentUser];
    if (![user.userId intValue]) {
        [startRatingM setRating:0];
        return;
    }
    NSMutableDictionary *params=[[NSMutableDictionary alloc] init];
    [params setObject:user.userId forKey:@"userId"];
    [params setObject:restaurant.restaurantId forKey:@"restaurantId"];
    [DetailDataHandler getLastReviewWithParameter:params andBlock:^(NSString *str, NSNumber *num,NSNumber *reviewId) {
        //[[LoadingView sharedLoadingView] stopLoading];
        NSString *returnStatus =str;
        if (![returnStatus intValue]){
            [startRatingM setRating:0];
            return ;
        }
        if (returnStatus){
            if ([num intValue]) {
                [startRatingM setRating:[num intValue]/2];
            }
        }
    }];
}
-(void)judgeReview{
    [[LoadingView sharedLoadingView] startLoading];
    IMGUser *user = [IMGUser currentUser];
    if (![user.userId intValue]) {
        [self writeNewReview];
        return;
    }
    NSMutableDictionary *params=[[NSMutableDictionary alloc] init];
    [params setObject:user.userId forKey:@"userId"];
    [params setObject:restaurant.restaurantId forKey:@"restaurantId"];
    [DetailDataHandler getLastReviewWithParameter:params andBlock:^(NSString *str, NSNumber *num,NSNumber *reviewId) {
        NSString *returnStatus =str;
        reviewID = reviewId;
        if (![returnStatus intValue]){
            [self writeNewReview];
            return ;
        }
        if (returnStatus){
            NSString *messageString = @"You have previously wrote a review about this restaurant.Do you want to edit or write a new review?";
            [UIAlertView bk_showAlertViewWithTitle:nil message:messageString cancelButtonTitle:nil otherButtonTitles:@[@"Edit Review",@"New Review"] handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                if (buttonIndex==0) {
                    [self writeReview];
                }else if(buttonIndex==1){
                    ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc]initWithRestaurant:restaurant andOverallRating:ratingCount andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
                    publishReviewVC.amplitudeType = @"RDP";
                    publishReviewVC.isEdit = YES;
                    publishReviewVC.ifSuccessLoginClickPost = YES;
                    publishReviewVC.reviewPublishViewControllerDelegate = self;
                    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:publishReviewVC] animated:YES completion:nil];
                }
                
            }];
        }

    }];
}
-(NSString*)getPageIdByAmplitudeType{
    NSString *pageId = @"";
//    if ([@"Event page" isEqualToString:self.amplitudeType]) {
//        pageId = @",Homepage";
//    }
//    if ([@"Journal article" isEqualToString:self.amplitudeType]) {
//        pageId = @",Journal detail page";
//    }
    return [NSString stringWithFormat:@"%@",self.amplitudeType];
}
-(void)judgeScrollViewFrameWithPhoneNumber:(NSMutableString*)phoneNumber{
    
    DetailBottomButtonView *bottomButtonView = [[DetailBottomButtonView alloc] initWithFrame:CGRectMake(0, DeviceHEIGHT - 72, DeviceWidth, 72)];
    [bottomButtonView createUIWithRestaurant:restaurant];
    bottomButtonView.delegate = self;
    [self.view addSubview:bottomButtonView];
}

- (void)callButtonTapped{
    [IMGAmplitudeUtil trackRestaurantWithName:@"CL - Call Restaurant CTA" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:@"RDP Floating Bar" andOrigin:nil andType:nil];
    
    IMGUser *user = [IMGUser currentUser];
    NSNumber *userId = [[NSNumber alloc] init];
    if (user.userId != nil)
    {
        userId = user.userId;
    }
    else
        userId = [NSNumber numberWithInt:0];
    
    [TrackHandler trackWithUserId:userId andRestaurantId:restaurant.restaurantId andContent:[NSDictionary dictionaryWithObject:@"0" forKey:@"type"]];
    
    
    if(@available(iOS 11.0, *)){
        [SendCall sendCall:restaurant.phoneNumber];
    }else{
        [UIAlertView bk_showAlertViewWithTitle:nil message:restaurant.phoneNumber cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Call"] handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if(buttonIndex==0){
                [alertView dismissWithClickedButtonIndex:0 animated:NO];
            }else if (buttonIndex==1){
                NSMutableDictionary* eventDic=[[NSMutableDictionary alloc]init];
                [eventDic setValue:@"Restaurant detail page" forKey:@"Location"];
                [eventDic setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
                [[Amplitude instance] logEvent:@"CL - Phone Number" withEventProperties:eventDic];
                [[Amplitude instance] logEvent:@"CL - Call Restaurant CTA" withEventProperties:eventDic];
                [[AppsFlyerTracker sharedTracker] trackEvent:@"CL - Call Restaurant CTA" withValues:eventDic];
                [SendCall sendCall:restaurant.phoneNumber];
            }
            
        }];
    }
}

- (void)bookButtonTapped{
    [IMGAmplitudeUtil trackRDPWithName:@"CL - Book Now" andRestaurantId:restaurant.restaurantId andOrigin:nil andLocation:@"Restaurant detail page"];
    
    [IMGAmplitudeUtil trackRDPWithName:@"TR - Reservation Initiate" andRestaurantId:restaurant.restaurantId andOrigin:nil andLocation:@"RDP Floating Bar"];
    
    if (bookbuttonIsClicked) {
        bookbuttonIsClicked=NO;
        [RestaurantHandler getOfferFromServer:restaurant.restaurantId andBookDate:nil andBlock:^{
            if(restaurantOfferArray!=nil){
                [restaurantOfferArray removeAllObjects];
            }
            [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGRestaurantOffer WHERE restaurantId = '%@' ORDER BY offerType,offerSlotMaxDisc desc;",restaurant.restaurantId] successBlock:^(FMResultSet *resultSet) {
                while ([resultSet next]) {
                    IMGRestaurantOffer  *tmpRestaurantOffer=[[IMGRestaurantOffer alloc]init];
                    [tmpRestaurantOffer setValueWithResultSet:resultSet];
                    [restaurantOfferArray addObject:tmpRestaurantOffer];
                }
                [resultSet close];
            }failureBlock:^(NSError *error) {
                NSLog(@"initRestaurantOfferFromLocal error when get datas:%@",error.description);
            }];
            
            SelectPaxViewController *selectDinersViewController = [[SelectPaxViewController alloc]initWithRestaurant:restaurant andTimeArray:nil andOfferArray:restaurantOfferArray andOffer:nil];
            [self.navigationController pushViewController:selectDinersViewController animated:YES];
        }];
    }
}

- (void)mapButtonTapped{
    [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Map" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:@"Restaurant detail page" andOrigin:nil andType:nil];
    
    IMGUser *user = [IMGUser currentUser];
    NSNumber *userId = [[NSNumber alloc] init];
    if (user.userId != nil)
    {
        userId = user.userId;
    }
    else
        userId = [NSNumber numberWithInt:0];
    [TrackHandler trackWithUserId:userId andRestaurantId:restaurant.restaurantId andContent:[NSDictionary dictionaryWithObject:@"1" forKey:@"type"]];
    
    MapViewController *mapViewController=[[MapViewController alloc]initWithRestaurant:restaurant];
    mapViewController.amplitudeType=@"Restaurant detail page";
    mapViewController.restaurantId=restaurant.restaurantId;
    mapViewController.fromDetail = YES;
    [self.navigationController pushViewController:mapViewController animated:YES];
}

- (void)goFoodButtonTapped{
    [IMGAmplitudeUtil trackGoFoodWithName:@"CL - Go-Food CTA" andJournalId:nil andRestaurantId:restaurant.restaurantId andPhotoId:nil andLocation:@"Restaurant Detail Page" andOrigin:nil andOrder:nil];
    [CommonMethod goFoodButtonTapped:restaurant andViewController:self andCallBackBlock:^(BOOL isSaved) {
        restaurant.saved = [NSNumber numberWithBool:isSaved];
        [self changeSaveBtn:isSaved];
    }];
}

- (void)writeReviewReloadUploadPhotoCell:(id)reviewPublishViewController{
}


- (void)loadBannerImageUrl:(NSMutableArray *)imageArr{
    imageArr = (NSMutableArray *)[[imageArr reverseObjectEnumerator] allObjects];
    for (IMGRestaurantImage *model in imageArr) {
        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[model.imageUrl returnFullImageUrlWithWidth:DeviceWidth]] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            
        }];
    }
}

- (void)loadIMGUrl:(NSArray *)imgArray :(NSString *)typeString{
    NSMutableArray *reverseArr = [NSMutableArray arrayWithArray:imgArray];
    reverseArr = (NSMutableArray *)[[reverseArr reverseObjectEnumerator] allObjects];
    if ([typeString isEqualToString:@"journal"]) {
        
        for (NSDictionary *dic in reverseArr) {
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[[dic objectForKey:@"journalImageUrl"] returnFullImageUrlWithWidth:124.5 andHeight:85]] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                
            }];
            
        }
        
    }else if ([typeString isEqualToString:@"guide"]){
        for (NSDictionary *dic in reverseArr) {
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[[dic objectForKey:@"headerImageThumbnail"] returnFullImageUrl]] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                
            }];
        }
        
    }else if ([typeString isEqualToString:@"photos"]){
        for (NSDictionary *dic in reverseArr) {
            if ([[dic objectForKey:@"type"] intValue] == 1) {
                NSDictionary *tempDic = [dic objectForKey:@"dish"];
                [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[[tempDic objectForKey:@"imageUrl"] returnFullWebPImageUrlWithWidth:(DeviceWidth-LEFTLEFTSET-40)/3]] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                    
                }];
            }else if ([[dic objectForKey:@"type"] intValue] == 2){
                NSDictionary *tempDic = [dic objectForKey:@"instagramPhoto"];
                if (tempDic) {
                    NSString *imgStr;
                    if ([[tempDic objectForKey:@"lowResolutionImage"] length]>0) {
                        imgStr = [tempDic objectForKey:@"lowResolutionImage"];
                    }else{
                        if ([[tempDic objectForKey:@"thumbnailImage"] length]>0) {
                            imgStr = [tempDic objectForKey:@"thumbnailImage"];
                        }else{
                            imgStr = [tempDic objectForKey:@"standardResolutionImage"];
                        }
                    }
                    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:imgStr] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                        
                    }];
                }
            }
        }
    }else{
        for (NSDictionary *dic in reverseArr) {
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[[dic objectForKey:@"imageUrl"] returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                
            }];
        }
    }
}

@end
