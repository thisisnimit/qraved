//
//  SuggestViewController.m
//  Qraved
//
//  Created by Lucky on 15/4/20.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#define LEFTSET 55
#define LEFTMINSET 15
#define LEFTMEDSET 15
#define mapViewHeight 100



#import "SuggestViewController.h"
#import "CSLinearLayoutView.h"
#import "IMGRestaurant.h"
#import "UIColor+Hex.h"
#import "UILabel+Helper.h"
#import "UIViewController+Helper.h"
#import "UIView+Helper.h"
#import "MapViewController.h"
#import "UIConstants.h"
#import "IMGUser.h"
#import "CalloutMapAnnotation.h"
#import "CallOutAnnotationView.h"
#import "CustomAnnotation.h"
#import "LoadingView.h"
#import "SuggestMapViewController.h"
#import "Amplitude.h"
#import "NavigationBarButtonItem.h"
#import "UIDevice+Util.h"
#import "AppDelegate.h"
#import "DetailDataHandler.h"
#import "YBPopupMenu.h"

#define TITLES @[@"Normal",@"Under Renovation",@"Temporarily Closed",@"Permanantly closed"]
@interface SuggestViewController ()<UITextFieldDelegate,CLLocationManagerDelegate,UIScrollViewDelegate,SuggestMapViewControllerDelegate, YBPopupMenuDelegate>
{
    IMGRestaurant *_restaurant;
    MKMapView *map;
    NSMutableDictionary *infoDic;
    int btnTag;
    NSArray *nameAry;
    MKAnnotationView *newAnnotation;
    MKPointAnnotation *pointAnnatation;
    UIView *mapGrayView;
    UIView *mapView;
    NSArray *stateNameAry;
    NSMutableArray *btnsArr;
    UIButton *mapBtn;
    UITextField *TextField;
    NSMutableArray *textFieldArr;
    NSMutableArray *btnArr;
    UIButton *Status;
    NSString *statusString;
}
@property (nonatomic, strong)  UIScrollView  *scrollview;
@property (nonatomic, strong)  UITextField  *tfRestaurantName;
@property (nonatomic, strong)  UITextField  *tfPhoto;
@property (nonatomic, strong)  UITextField  *tfAddress1;
@property (nonatomic, strong)  UITextField  *tfAddress2;
@property (nonatomic, strong)  UITextField  *tfCity;
@property (nonatomic, strong)  UITextField  *tfDistrict;
@property (nonatomic, strong)  UITextField  *tfDisplay;
@property (nonatomic, strong) YBPopupMenu *popupMenu;
@end

@implementation SuggestViewController

@synthesize linearLayoutView = _linearLayoutView;

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationItem setTitle:_restaurant.title];
    self.navigationController.navigationBarHidden = NO;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"Restaurant Edit Info form page";
    btnArr = [[NSMutableArray alloc] init];
    infoDic = [[NSMutableDictionary alloc] init];
    textFieldArr = [[NSMutableArray alloc] init];
    //@"latitude",@"longitude",
    btnTag = 0;
    nameAry = [[NSArray alloc] initWithObjects:@"status",@"name",@"phone",@"address1",@"address2",@"city",@"district",@"displayTime", nil];
//    [self setBackBarButtonOffset30];
    [self setNavagationBack];
//    self.linearLayoutView = [[CSLinearLayoutView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-32)];
//    
//    self.linearLayoutView.userInteractionEnabled=YES;
//    
//    UITapGestureRecognizer* tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
//        [self.linearLayoutView addGestureRecognizer:tap];
//    
//    
//    _linearLayoutView.orientation = CSLinearLayoutViewOrientationVertical;
//    _linearLayoutView.scrollEnabled = YES;
//    _linearLayoutView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//    _linearLayoutView.backgroundColor = [UIColor defaultColor];
//    _linearLayoutView.delegate = self;
//    [self.view addSubview:_linearLayoutView];
    
    [self initUI];
//    [self addTitleLabelWithStr:@"Restaurant Status"];
//    NSString* restaurantStateName=[[NSString alloc]init];
//    switch ([_restaurant.state intValue]) {
//        case 1:
//            restaurantStateName=@"Grand Opening";
//            break;
//        case 2:
//            restaurantStateName=@"Under Renovation";
//            break;
//        case 3:
//            restaurantStateName=@"Closed";
//            break;
//        case 4:
//            restaurantStateName=@"Temporarily Closed";
//            break;
//        case 5:
//            restaurantStateName=@"Coming Soon";
//            break;
//        case 6:
//            restaurantStateName=@"Normal";
//            break;
//        default:
//            break;
//    }
//
//    [self addType:nil andInfo:restaurantStateName];
//
//    [self addTitleLabelWithStr:@"General Info"];
//    [self addType:@"Restaurant Name" andInfo:_restaurant.title];
//    [self addType:@"phone" andInfo:_restaurant.phoneNumber];
//    //[self addType:@"Cuisine" andInfo:_restaurant.cuisineName];
//    [self addTitleLabelWithStr:@"Address & Map"];
//    [self addType:@"Address 1" andInfo:_restaurant.address1];
//    [self addType:@"Address 2" andInfo:_restaurant.address2];
//    [self addType:@"City" andInfo:_restaurant.cityName];
//    [self addType:@"District" andInfo:_restaurant.districtName];
//    [self addTitleLabelWithStr:@"Adjust location"];
    [self addMapView];
//    [self addTitleLabelWithStr:@"Operation Hours"];
//    [DetailDataHandler getMoreInfoFromService:_restaurant andBlock:^{
//        
//    [self addType:@"displayTime" andInfo:[self getDisplayTime]];
//        UILabel *endLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 30)];
//        CSLinearLayoutItem *labelItem = [CSLinearLayoutItem layoutItemForView:endLabel];
//        labelItem.padding = CSLinearLayoutMakePadding(10.0, 10.0, 0.0, 10.0);
//        labelItem.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentLeft;
//        [_linearLayoutView addItem:labelItem];
//
//    }];
//
    
//        [self addType:@"displayTime" andInfo:[self getDisplayTime]];

    //[self addOperationHoursView];
//    
//    UILabel *endLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 30)];
//    CSLinearLayoutItem *labelItem = [CSLinearLayoutItem layoutItemForView:endLabel];
//    labelItem.padding = CSLinearLayoutMakePadding(10.0, 10.0, 0.0, 10.0);
//    labelItem.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentLeft;
//    [_linearLayoutView addItem:labelItem];
    
    
    
    UIButton *subBut = [UIButton buttonWithType:UIButtonTypeCustom];
    subBut.frame = CGRectMake(LEFTMINSET, 2, 185, 38);
    
//    subBut.backgroundColor = [UIColor colorWithHexString:@"#3baf24"];
    [subBut addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
    [subBut.layer setMasksToBounds:YES];
    [subBut.layer setCornerRadius:5];
    [subBut setTitle:@"Submit" forState:UIControlStateNormal];
    subBut.titleLabel.font = [UIFont systemFontOfSize:14];
    [subBut setTitleColor:[UIColor colorWithRed:241/255.0f green:15/255.0f blue:51/255.0f alpha:1.0f] forState:UIControlStateNormal];
    subBut.layer.borderColor = [UIColor colorWithRed:241/255.0f green:15/255.0f blue:51/255.0f alpha:1.0f].CGColor;
    subBut.layer.cornerRadius = 19;
    subBut.layer.borderWidth = 1;
    
    UIView *btnView = [[UIView alloc] init];
    btnView.frame = CGRectMake(0, DeviceHeight-92, DeviceWidth, 42);
    [btnView addSubview:subBut];
    btnView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:btnView];
    subBut.centerX = btnView.centerX;
    self.view.backgroundColor = [UIColor whiteColor];
    
    
//    UILabel *label = [[UILabel alloc] init];
//    label.frame = CGRectMake(0, _linearLayoutView.endPointY-50, DeviceWidth, 500);
//    label.backgroundColor = [UIColor purpleColor];
//    [self.view addSubview:label];
    
    
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if (textFieldArr!=nil) {
        for (UITextField *item in textFieldArr) {
            [item resignFirstResponder];
        }
    }
}
-(void)setNavagationBack
{

    NavigationBarButtonItem *navigationBarLeftButton = [[NavigationBarButtonItem alloc ]initWithImage:[UIImage imageNamed:NavigationBackImage] andTitle:@"" target:self action:@selector(goToBackViewController) width:43.0f offset:-15-[UIDevice heightDifference]];
    self.navigationItem.leftBarButtonItem = navigationBarLeftButton;
}
-(void)goToBackViewController
{

    [self.navigationController popViewControllerAnimated:YES];
    [[Amplitude instance] logEvent:@"UC - Suggest Edit Cancel" withEventProperties:@{@"Restaurant_ID":_restaurant.restaurantId}];

}
-(void)tap:(UIPanGestureRecognizer*)tap
{
       
    [TextField resignFirstResponder];

}
-(void)changeMap:(NSDictionary*)dic{
    _restaurant.longitude = [dic objectForKey:@"longitude"];
    _restaurant.latitude = [dic objectForKey:@"latitude"];
    
    [newAnnotation removeFromSuperview];
    [infoDic setObject:[NSString stringWithFormat:@"%@",_restaurant.latitude] forKey:@"latitude"];
    [infoDic setObject:[NSString stringWithFormat:@"%@",_restaurant.longitude] forKey:@"longitude"];
    
    CLLocationCoordinate2D theCoordinate =CLLocationCoordinate2DMake([_restaurant.latitude floatValue],[_restaurant.longitude floatValue]);
    
    MKCoordinateSpan  theSpan;
    theSpan.latitudeDelta=0.01;
    theSpan.longitudeDelta=0.01;
    
    MKCoordinateRegion theRegion;
    theRegion.center=theCoordinate;
    theRegion.span=theSpan;
    
    
    
    pointAnnatation= [[MKPointAnnotation alloc] init];
    
    if(CLLocationCoordinate2DIsValid(theCoordinate))
    {
        [map setRegion:theRegion animated:YES];
        
        [map regionThatFits:theRegion];
        
        pointAnnatation.coordinate=theCoordinate;
        
        
        [map addAnnotation:pointAnnatation];
    }
    

}
- (NSString *)getDisplayTime
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"hh:mm"];
    NSArray *array = @[@2,@3,@4,@5,@6,@7,@1];
    NSMutableArray* openTimeArr=[[NSMutableArray alloc]init];
    for (int i=0; i<array.count; i++) {
        NSInteger index = [[array objectAtIndex:i] integerValue];
        switch (index) {
                
            case 2:
                
;
                [openTimeArr addObject:[_restaurant dispalyTimeAtWeekday:index weekdayName:@"Monday"]];
                break;
            case 3:
                [openTimeArr addObject:[_restaurant dispalyTimeAtWeekday:index weekdayName:@"Tuesday"]];
                
                break;
            case 4:
                
               [openTimeArr addObject:[_restaurant dispalyTimeAtWeekday:index weekdayName:@"Wednesday"]];
                break;
            case 5:
                
                [openTimeArr addObject:[_restaurant dispalyTimeAtWeekday:index weekdayName:@"Thursday"]];
                break;
            case 6:
                
                [openTimeArr addObject:[_restaurant dispalyTimeAtWeekday:index weekdayName:@"Friday"]];
                break;
            case 7:
                [openTimeArr addObject:[_restaurant dispalyTimeAtWeekday:index weekdayName:@"Saturday"]];
                break;
            case 1:
                
                [openTimeArr addObject:[_restaurant dispalyTimeAtWeekday:index weekdayName:@"Sunday"]];
                break;
            default:
                break;
        }
    }
    NSInteger weekday=[self returnTodayIsWeekDay];
    NSInteger Num=[array indexOfObject:[NSNumber numberWithInteger:weekday]];
    return [openTimeArr objectAtIndex:Num];
}
-(NSInteger)returnTodayIsWeekDay{
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *now = [NSDate date];;
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    comps = [calendar components:unitFlags fromDate:now];
    NSInteger weekDay=[comps weekday];
    NSLog(@"today is %ld day ",(long)weekDay);
    return weekDay;
    
    
    
}

- (void)controlClick
{
//    for (CSLinearLayoutItem *labelItem in _linearLayoutView.subviews)
//    {
//        if ([labelItem isKindOfClass:[UITextField class]])
//        {
//            UITextField *textField = (UITextField *)labelItem;
//            [textField resignFirstResponder];
//        }
//    }
}
- (void)addOperationHoursView
{
    NSMutableArray *openingHours = [[NSMutableArray alloc]init];
    [openingHours addObjectsFromArray:[_restaurant openingHours]];
    if(openingHours==nil || openingHours.count==0){
        return;
    }
    
    for (NSString *str in openingHours)
    {
        NSRange range=[str rangeOfString:@":"];
        NSString *name = [str substringToIndex:range.location];
        NSString *value = [str substringFromIndex:range.location+2];
        [self addType:name andInfo:value];
    }
}
- (void)btnClick
{
    NSLog(@"");
    IMGUser *user=[IMGUser currentUser];
    NSArray *fieldNamesAry = @[@"status", @"name", @"phone", @"address1", @"address2", @"city", @"district", @"latitude", @"longitude", @"displayTime"];
    NSArray *fieldValuesAry = @[statusString,_tfRestaurantName.text, _tfPhoto.text, _tfAddress1.text, _tfAddress2.text, _tfCity.text, _tfDistrict.text, _restaurant.latitude, _restaurant.longitude, _tfDisplay.text];
    
    
    NSDictionary *dict = @{@"userId":user.userId, @"restaurantId":_restaurant.restaurantId, @"productSource":[NSNumber numberWithInt:4],@"fieldNames":fieldNamesAry, @"fieldValues":fieldValuesAry};
//    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",_restaurant.restaurantId, @"restaurantId",[NSNumber numberWithInt:4],@"productSource", fieldNamesAry,@"fieldNames" , fieldValuesAry,@"fieldValues", nil];
    
    [[LoadingView sharedLoadingView]startLoading];
    [DetailDataHandler suggestAnEditSubmit:dict andSuccessBlock:^(NSDictionary *array) {
        NSLog(@"%@",array);
        if ([array[@"status"] isEqualToString:@"succeed"]) {
            
            [[LoadingView sharedLoadingView]stopLoading];
            
            [IMGAmplitudeUtil trackRDPWithName:@"UC - Suggest Edit Succeed" andRestaurantId:_restaurant.restaurantId andOrigin:nil andLocation:nil];
            
            [UIAlertView bk_showAlertViewWithTitle:@"Success!" message:nil cancelButtonTitle:@"OK" otherButtonTitles:nil handler:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
        
            NSLog(@"error");
        }
        
        
    } anderrorBlock:^{
        [[LoadingView sharedLoadingView]stopLoading];
        
    }];
//    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
//    [eventProperties setValue:@"Restaurant detail page" forKey:@"Origin"];
//    [eventProperties setValue:_restaurant.restaurantId forKey:@"Restaurant_ID"];
//    
//    [[Amplitude instance] logEvent:@"UC - Suggest Edit Initiate" withEventProperties:eventProperties];
//
//    IMGUser *user=[IMGUser currentUser];
//
//    NSMutableArray *fieldNamesAry = [[NSMutableArray alloc] init];
//    NSMutableArray *fieldValuesAry = [[NSMutableArray alloc] init];
//    for (NSString *key in [infoDic allKeys])
//    {
//        NSString *value = [infoDic objectForKey:key];
//        [fieldNamesAry addObject:key];
//        [fieldValuesAry addObject:value];
//    }
//    
//    if (fieldNamesAry.count == 0)
//    {
//        return;
//    }
//    
//    [[LoadingView sharedLoadingView]startLoading];
//
//    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",[NSNumber numberWithInt:4],@"productSource",[NSNumber numberWithInt:1],@"sourceType", _restaurant.restaurantId,@"restaurantId",fieldNamesAry,@"fieldNames",fieldValuesAry,@"fieldValues", nil];
//    [parameters setValue:QRAVED_WEB_SERVICE_VERSION forKey:@"v"];
//    [parameters setValue:QRAVED_APIKEY forKey:@"appApiKey"];
//    [parameters setValue:QRAVED_WEB_SERVICE_CLIENT forKey:@"client"];
//    NSLog(@"parameters = %@",parameters);
//    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",QRAVED_WEB_SERVICE_SERVER,@"suggestion/add"]];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    request.HTTPMethod = @"POST";
//    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    NSDictionary *json = parameters;
//    NSData *data = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:nil];
//    request.HTTPBody = data;
//    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
//        if (connectionError==nil) {
//            NSError *error = nil;
//            id jsonObject = [NSJSONSerialization JSONObjectWithData:data
//                                                            options:NSJSONReadingAllowFragments
//                                                              error:&error];
//            NSLog(@"%@",jsonObject);
//            if (error == nil) {
//                UIAlertView *alter = [[UIAlertView alloc] initWithTitle:nil message:@"Success!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [[LoadingView sharedLoadingView] stopLoading];
//                [alter show];
//                [self.navigationController popViewControllerAnimated:YES];
//                [[Amplitude instance] logEvent:@"UC - Suggest Edit Succeed" withEventProperties:@{@"Restaurant_ID":_restaurant.restaurantId}];
//            }else{
//                [[Amplitude instance] logEvent:@"UC - Suggest Edit Failed" withEventProperties:@{@"Reson":connectionError.localizedDescription}];
//            }
//        }else{
//                [[Amplitude instance] logEvent:@"UC - Suggest Edit Failed" withEventProperties:@{@"Reson":connectionError.localizedDescription}];
//        }
//    }];
}
- (void)addTitleLabelWithStr:(NSString *)str
{
    UILabel *label = [[UILabel alloc] init];
    label.backgroundColor = [UIColor greenColor];
    label.text = str;
    label.font = [UIFont systemFontOfSize:14];
    label.textColor = [UIColor colorWithHexString:@"#333333"];
    label.frame = CGRectMake(LEFTMINSET, 20, label.expectedWidth, label.font.lineHeight);
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, label.endPointY)];
    [view addSubview:label];
    
    UIControl *control = [[UIControl alloc] init];
    [control addTarget:self action:@selector(controlClick) forControlEvents:UIControlEventTouchUpInside];
    //control.frame = CGRectMake(10, 150, 300, 100);
    control.frame = view.frame;
    [view addSubview:control];
    [view sendSubviewToBack:control];
    
    CSLinearLayoutItem *labelItem = [CSLinearLayoutItem layoutItemForView:view];
    labelItem.padding = CSLinearLayoutMakePadding(10.0, 10.0, 0.0, 10.0);
    labelItem.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentLeft;
    [_linearLayoutView addItem:labelItem];
    
}
- (CSLinearLayoutItem *)newItemWithTag:(NSInteger)btn_Tag {
    if (btn_Tag == 0)
    {
        btnsArr = [[NSMutableArray alloc] init];

        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(LEFTSET, 0, DeviceWidth-90, 20)];
        
        stateNameAry = [[NSArray alloc] initWithObjects:@"Temporarily closed",@"Under renovation",@"Permanantly closed", nil];
        for (int i = 0; i<3; i++)
        {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake(0, 28*i, DeviceWidth-90, 24);
            [btn setTitle:[stateNameAry objectAtIndex:i] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont systemFontOfSize:13];
            btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            btn.tag = i;
            [btn addTarget:self action:@selector(stateBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            
            UILabel *label = [[UILabel alloc] init];
            label.text = @"√";
            label.tag = 1;
            label.textColor = [UIColor greenColor];
            label.hidden = YES;
            label.frame = CGRectMake(btn.endPointX - 24, 0, 24, 24);
            [btn addSubview:label];
            
            [btnsArr addObject:btn];
            
            [view addSubview:btn];
            view.frame = CGRectMake(LEFTSET, 0, DeviceWidth-90, btn.endPointY);
        }
        
        
        CSLinearLayoutItem *item = [CSLinearLayoutItem layoutItemForView:view];
        item.padding = CSLinearLayoutMakePadding(10.0, 65.0, 0.0, 10.0);
        item.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentLeft;
        
        
        return item;
    }
    
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth-LEFTSET-LEFTMINSET, 20)];
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleRemoveView:)];
//    [view addGestureRecognizer:tap];
    
     TextField = [[UITextField alloc] init];
    TextField.font = [UIFont systemFontOfSize:17];
    TextField.textColor = [UIColor colorWithHexString:@"#999999"];
    TextField.frame = CGRectMake(LEFTSET, 15, DeviceWidth-90, 20);
    TextField.placeholder = @"Please input ..";
    TextField.delegate = self;
    TextField.tag = btn_Tag;
    [textFieldArr addObject:TextField];
    [view addSubview:TextField];
    
    UILabel *lineLabel = [[UILabel alloc] init];
    lineLabel.backgroundColor = [UIColor colorWithHexString:@"#999999"];
    lineLabel.frame = CGRectMake(0, TextField.frame.size.height-1, TextField.frame.size.width, 1);
    [TextField addSubview:lineLabel];
    
    
    CSLinearLayoutItem *item = [CSLinearLayoutItem layoutItemForView:TextField];
    item.padding = CSLinearLayoutMakePadding(10.0, 65.0, 0.0, 10.0);
    item.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentLeft;

    
    return item;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.tag == 7)
    {
        // scroll to the bottom, if necessary
//        if ((_linearLayoutView.frame.size.height < _linearLayoutView.layoutOffset) && btn.tag == 7)
//        {
            CGPoint bottomOffset = CGPointMake(0, _linearLayoutView.layoutOffset - _linearLayoutView.frame.size.height + 170);
            NSLog(@"yyyy = %lf",_linearLayoutView.layoutOffset - _linearLayoutView.frame.size.height);
            _linearLayoutView.contentSize = CGSizeMake(0, _linearLayoutView.layoutOffset - _linearLayoutView.frame.size.height);
            [_linearLayoutView setContentOffset:bottomOffset animated:YES];
        //}
    }
    
    
    
}
- (void)stateBtnClick:(UIButton *)btn
{
    for (UIButton *btn in btnsArr)
    {
        for (UILabel *label in btn.subviews)
        {
            if (label.tag == 1)
            {
                label.hidden = YES;
            }
        }
    }
    UIButton *tempBtn = [btnsArr objectAtIndex:btn.tag];
    for (UILabel *label in tempBtn.subviews)
    {
        if (label.tag == 1)
        {
            label.hidden = NO;
        }
    }
    [infoDic setObject:[stateNameAry objectAtIndex:btn.tag] forKey:[nameAry objectAtIndex:0]];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];

    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{


    [infoDic setObject:textField.text forKey:[nameAry objectAtIndex:textField.tag]];


}
- (void)addType:(NSString *)typeStr andInfo:(NSString *)infoStr
{
    if ((infoStr == nil) || [infoStr isKindOfClass:[NSNull class]] || (infoStr.length == 0) )
    {
        infoStr = typeStr;
    }
    UIView *view = [[UIView alloc] init];
    
    UILabel *typeLabel = [[UILabel alloc] init];
    typeLabel.text = typeStr;
    typeLabel.font = [UIFont systemFontOfSize:13];
    typeLabel.textColor = [UIColor colorWithHexString:@"#999999"];
    if ([typeStr isEqualToString:@"displayTime"]) {
        typeLabel.frame = CGRectMake(LEFTSET, -30, typeLabel.expectedWidth, typeLabel.font.lineHeight);
    }else{
    
        typeLabel.frame = CGRectMake(LEFTSET, 0+15, typeLabel.expectedWidth, typeLabel.font.lineHeight);

    }
    [view addSubview:typeLabel];
    
    UILabel *infoLabel = [[UILabel alloc] init];
    infoLabel.text = infoStr;
    infoLabel.font = [UIFont systemFontOfSize:17];
    infoLabel.numberOfLines = 0;
    infoLabel.tag = 100+btnTag;
    infoLabel.textColor = [UIColor colorWithHexString:@"#3baf24"];
    CGSize infoSize = [infoLabel.text sizeWithFont:infoLabel.font constrainedToSize:CGSizeMake(DeviceWidth-LEFTMINSET-LEFTSET, CGFLOAT_MAX)];
    infoLabel.frame = CGRectMake(LEFTSET, typeLabel.endPointY+15, infoSize.width, infoSize.height);
    if (!typeStr)
    {
        infoLabel.frame = CGRectMake(LEFTSET, 15, infoLabel.frame.size.width, infoLabel.frame.size.height);
    }
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleAddViewLabel:)];
    infoLabel.userInteractionEnabled = YES;
    tap.numberOfTapsRequired=1;
    tap.numberOfTouchesRequired=1;
    [infoLabel addGestureRecognizer:tap];
    [view addSubview:infoLabel];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:@"selectImage"] forState:UIControlStateSelected];
    [btn setBackgroundImage:[UIImage imageNamed:@"unSelectImage"] forState:UIControlStateNormal];
    btn.frame = CGRectMake(15, infoLabel.frame.origin.y, 20, 20);
    if (!typeStr)
    {
        btn.frame = CGRectMake(15, 15, 20, 20);
    }
    btn.tag = btnTag;
    NSLog(@"btn.tag = %ld",(long)btn.tag);
    UILabel *btnTagLabel = [[UILabel alloc] init];
    btnTagLabel.frame = CGRectMake(btn.frame.origin.x, btn.endPointY+2, 20, 20);
    btnTagLabel.text = [NSString stringWithFormat:@"%d",btnTag];
    //[view addSubview:btnTagLabel];
    
    btnTag++;
    NSLog(@"items.count(old) = %lu",(unsigned long)[_linearLayoutView.items count]);
    [btn addTarget:self action:@selector(handleAddView:) forControlEvents:UIControlEventTouchUpInside];
    [btnArr addObject:btn];
    [view addSubview:btn];
    
    view.frame = CGRectMake(0, 0, DeviceWidth, infoLabel.endPointY);
    
    UIControl *control = [[UIControl alloc] init];
    [control addTarget:self action:@selector(controlClick) forControlEvents:UIControlEventTouchUpInside];
    //control.frame = CGRectMake(10, 150, 300, 100);
    control.frame = view.frame;
    [view addSubview:control];
    [view sendSubviewToBack:control];

    
    CSLinearLayoutItem *labelItem = [CSLinearLayoutItem layoutItemForView:view];
    labelItem.padding = CSLinearLayoutMakePadding(10.0, 10.0, 0.0, 10.0);
    labelItem.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentLeft;
    [_linearLayoutView addItem:labelItem];
    NSLog(@"items.count(new) = %lu",(unsigned long)[_linearLayoutView.items count]);

}
- (void) handleAddViewLabel:(UITapGestureRecognizer *)sender {
    UIView *view = sender.view;
    UIButton *selectBtn = [btnArr objectAtIndex:(view.tag-100)];
    [self handleAddView:selectBtn];
}

- (void)handleAddView:(UIButton *)btn {
    
    if (btn.selected)
    {
        btn.selected = NO;
        for (UILabel *label in btn.superview.subviews)
        {
            if (label.tag > 99)
            {
                label.textColor = [UIColor colorWithHexString:@"#3baf24"];
                [label setAttributedText:[[NSMutableAttributedString alloc] initWithString:label.text]];
                break;
            }
        }
        [self handleRemoveView:btn];
        return;
    }
    else
    {
        for (UILabel *label in btn.superview.subviews)
        {
            if (label.tag > 99)
            {
                label.textColor = [UIColor colorWithRed:178/255.0 green:0/255.0 blue:11/255.0 alpha:1];
                NSString *oldPrice = label.text;
                NSUInteger length = [oldPrice length];
                
                NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:oldPrice];
                [attri addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:NSMakeRange(0, length)];
                [attri addAttribute:NSStrikethroughColorAttributeName value:[UIColor colorWithRed:178/255.0 green:0/255.0 blue:11/255.0 alpha:1] range:NSMakeRange(0, length)];
                [label setAttributedText:attri];
                break;
            }
        }
        btn.selected = YES;
    }
    
    // create the item
    CSLinearLayoutItem *item = [self newItemWithTag:btn.tag];
    int i = 0;
    for (CSLinearLayoutItem *item in _linearLayoutView.items)
    {
        i++;
        if (item.view == btn.superview)
        {
            NSLog(@"suc i = %d",i);
            break;
        }
    }
    
    // insert above the add button
    //NSUInteger index = btn.tag;
    [_linearLayoutView insertItem:item atIndex:i];
   
   
}
- (void)handleRemoveView:(UIButton *)btn
{
    int i = 0;
    for(CSLinearLayoutItem *item in _linearLayoutView.items) {
        if (i!=0)
        {
            [_linearLayoutView removeItem:item];
            break;
        }
        if(item.view == btn.superview) {
            i++;
        }
    }
}


-(void)addMapView
{
   
    
    mapView = [[UIView alloc]init];
    mapView.backgroundColor = [UIColor whiteColor];
    
    
    
    map=[[MKMapView alloc]initWithFrame:CGRectMake(LEFTSET, 15, DeviceWidth-LEFTSET-LEFTMINSET, mapViewHeight)];
    
    [map setMapType:MKMapTypeStandard];
    [map setDelegate:self];
    [mapView addSubview:map];
    CLLocationCoordinate2D coor;
    coor.latitude = [_restaurant.latitude doubleValue];
    coor.longitude = [_restaurant.longitude doubleValue];
    CLLocationCoordinate2D theCoordinate =CLLocationCoordinate2DMake(coor.latitude, coor.longitude);
    
    MKCoordinateSpan  theSpan;
    theSpan.latitudeDelta=0.01;
    theSpan.longitudeDelta=0.01;
    
    MKCoordinateRegion theRegion;
    theRegion.center=theCoordinate;
    theRegion.span=theSpan;
    
    
    pointAnnatation= [[MKPointAnnotation alloc] init];
    
    if(CLLocationCoordinate2DIsValid(theCoordinate))
    {
        [map setRegion:theRegion animated:NO];
        
        [map regionThatFits:theRegion];
        
        pointAnnatation.coordinate=theCoordinate;
        
        [map addAnnotation:pointAnnatation];
    }
    
    
    [mapView setFrame:CGRectMake(0,0, DeviceWidth-LEFTMINSET-LEFTSET, map.endPointY)];
    
    UITapGestureRecognizer *mTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
    [map addGestureRecognizer:mTap];
    
    mapBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [mapBtn setBackgroundImage:[UIImage imageNamed:@"selectImage"] forState:UIControlStateSelected];
    [mapBtn setBackgroundImage:[UIImage imageNamed:@"unSelectImage"] forState:UIControlStateNormal];
    mapBtn.frame = CGRectMake(15, mapView.frame.origin.y, 20, 20);
    [mapBtn addTarget:self action:@selector(mapViewClick:) forControlEvents:UIControlEventTouchUpInside];
    [mapView addSubview:mapBtn];
    
    mapGrayView = [[UIView alloc] initWithFrame:map.frame];
    mapGrayView.backgroundColor = [UIColor clearColor];
    //[mapView addSubview:mapGrayView];
    
    UIView *tempMapView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mapGrayView.frame.size.width, mapGrayView.frame.size.height)];
    tempMapView.backgroundColor = [UIColor grayColor];
    tempMapView.alpha = 0.4f;
    [mapGrayView addSubview:tempMapView];
    
    UILabel *label = [[UILabel alloc] init];
    label.text = @"update location on map";
    label.textColor = [UIColor blackColor];
    label.numberOfLines = 0;
    label.font = [UIFont systemFontOfSize:17];
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.frame = CGRectMake(35, map.frame.origin.y/2+25, label.expectedWidth, label.font.lineHeight);
    
    [mapGrayView addSubview:label];
    
    
    
    UITapGestureRecognizer *mapGrayTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mapTapPress)];
    [mapGrayView addGestureRecognizer:mapGrayTap];
    
    
    CSLinearLayoutItem *labelItem = [CSLinearLayoutItem layoutItemForView:mapView];
    labelItem.padding = CSLinearLayoutMakePadding(10.0, 10.0, 0.0, 10.0);
    labelItem.horizontalAlignment = CSLinearLayoutItemHorizontalAlignmentLeft;
    [_linearLayoutView addItem:labelItem];
    
}
- (void)mapTapPress
{
    SuggestMapViewController *smvc = [[SuggestMapViewController alloc] initWithRestaurant:_restaurant];
    smvc.delegate = self;
    [self.navigationController pushViewController:smvc animated:YES];
}

- (void)mapViewClick:(UIButton *)btn
{
    if (!btn.selected)
    {
        [btn setSelected:YES];
        [mapView addSubview:mapGrayView];
    }
    else
    {
        [btn setSelected:NO];
        [mapGrayView removeFromSuperview];
    }
}

- (void)tapPress:(UIGestureRecognizer*)gestureRecognizer {
    if (!mapBtn.selected)
    {
        [mapBtn setSelected:YES];
    }
    [mapView addSubview:mapGrayView];
    return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:map];//这里touchPoint是点击的某点在地图控件中的位置
    CLLocationCoordinate2D touchMapCoordinate =
    [map convertPoint:touchPoint toCoordinateFromView:map];//这里touchMapCoordinate就是该点的经纬度了
    
    NSLog(@"touching %f,%f",touchMapCoordinate.latitude,touchMapCoordinate.longitude);
    [infoDic setObject:[NSString stringWithFormat:@"%f",touchMapCoordinate.latitude] forKey:@"latitude"];
    [infoDic setObject:[NSString stringWithFormat:@"%f",touchMapCoordinate.longitude] forKey:@"longitude"];
    
    [newAnnotation removeFromSuperview];
    
    //newAnnotation=[[MKAnnotationView alloc] initWithAnnotation:nil reuseIdentifier:@"annotation1"];

    CLLocationCoordinate2D coor;
    coor.latitude = [_restaurant.latitude doubleValue];
    coor.longitude = [_restaurant.longitude doubleValue];
    CLLocationCoordinate2D theCoordinate =CLLocationCoordinate2DMake(touchMapCoordinate.latitude, touchMapCoordinate.longitude);
    
    MKCoordinateSpan  theSpan;
    theSpan.latitudeDelta=0.01;
    theSpan.longitudeDelta=0.01;
    
    MKCoordinateRegion theRegion;
    theRegion.center=theCoordinate;
    theRegion.span=theSpan;
    
    
    
    pointAnnatation= [[MKPointAnnotation alloc] init];
    
    if(CLLocationCoordinate2DIsValid(theCoordinate))
    {
        [map setRegion:theRegion animated:YES];
        
        [map regionThatFits:theRegion];
        
        pointAnnatation.coordinate=theCoordinate;
        
        
        [map addAnnotation:pointAnnatation];
    }

    
}
- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation{

    newAnnotation=[[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"annotation1"];
    
    newAnnotation.image = [UIImage imageNamed:@"location.png"];
        
    
    newAnnotation.canShowCallout=YES;
    return newAnnotation;
    //return nil;
}




-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant
{
    if(self= [super init]){
        _restaurant = paramRestaurant;
    }
    return self;
}

 -(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


//============================================================================================

- (void)initUI{

    _scrollview = [UIScrollView new];
    _scrollview.backgroundColor = [UIColor defaultColor];
    _scrollview.delegate = self;
    [self.view addSubview:_scrollview];
    _scrollview.sd_layout
    .topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .widthIs(DeviceWidth)
    .heightIs(DeviceHeight - 42 - 64);
    
    UILabel *restaurantStaus = [UILabel new];
    restaurantStaus.text = @"RESTAURANT STATUS";
    restaurantStaus.textColor = [UIColor lightGrayColor];
    restaurantStaus.font = [UIFont systemFontOfSize:13];
    
    UILabel *lblStaus = [UILabel new];
    lblStaus.text = @"   Status";
    lblStaus.textColor = [UIColor blackColor];
    lblStaus.font = [UIFont systemFontOfSize:17];
    lblStaus.backgroundColor = [UIColor whiteColor];
    
    
    
    
    NSString* restaurantStateName=[[NSString alloc]init];
    switch ([_restaurant.state intValue]) {
        case 1:
            restaurantStateName=@"Grand Opening";
            break;
        case 2:
            restaurantStateName=@"Under Renovation";
            break;
        case 3:
            restaurantStateName=@"Closed";
            break;
        case 4:
            restaurantStateName=@"Temporarily Closed";
            break;
        case 5:
            restaurantStateName=@"Coming Soon";
            break;
        case 6:
            restaurantStateName=@"Normal";
            break;
        default:
            break;
    }
    
    Status = [UIButton new];
    Status.backgroundColor = [UIColor whiteColor];
    [Status setTitle:restaurantStateName forState:UIControlStateNormal];
    [Status setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    Status.titleLabel.font = [UIFont systemFontOfSize:17];
    statusString = restaurantStateName;
    [Status addTarget:self action:@selector(onPopupClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    
    UIImageView *imagev = [UIImageView new];
    imagev.image = [UIImage imageNamed:@"Chevron_ic"];
    imagev.contentMode = UIViewContentModeScaleAspectFit;
    
    UILabel *lblGeneralInfo = [UILabel new];
    lblGeneralInfo.text = @"GENERAL INFO";
    lblGeneralInfo.textColor = [UIColor lightGrayColor];
    lblGeneralInfo.font = [UIFont systemFontOfSize:13];
    
    UILabel *lblRestaurantName = [UILabel new];
    lblRestaurantName.text = @"   Restaurant Name";
    lblRestaurantName.textColor = [UIColor blackColor];
    lblRestaurantName.font = [UIFont systemFontOfSize:17];
    lblRestaurantName.backgroundColor = [UIColor whiteColor];

    _tfRestaurantName = [UITextField new];
    _tfRestaurantName.text = _restaurant.title;
    _tfRestaurantName.textColor = [UIColor lightGrayColor];
    _tfRestaurantName.font = [UIFont systemFontOfSize:17];
    _tfRestaurantName.backgroundColor = [UIColor whiteColor];
    _tfRestaurantName.clearButtonMode=UITextFieldViewModeWhileEditing;
    
    UILabel *lblPhoto = [UILabel new];
    lblPhoto.text = @"   Phone";
    lblPhoto.textColor = [UIColor blackColor];
    lblPhoto.font = [UIFont systemFontOfSize:17];
    lblPhoto.backgroundColor = [UIColor whiteColor];
    
    _tfPhoto = [UITextField new];
    _tfPhoto.text = _restaurant.phoneNumber;
    _tfPhoto.textColor = [UIColor lightGrayColor];
    _tfPhoto.font = [UIFont systemFontOfSize:17];
    _tfPhoto.backgroundColor = [UIColor whiteColor];
    _tfPhoto.clearButtonMode=UITextFieldViewModeWhileEditing;
    
    UILabel *lblAddress = [UILabel new];
    lblAddress.text = @"ADDRESS & MAP";
    lblAddress.textColor = [UIColor lightGrayColor];
    lblAddress.font = [UIFont systemFontOfSize:13];
    
    UILabel *lblAddress1 = [UILabel new];
    lblAddress1.text = @"   Address 1";
    lblAddress1.textColor = [UIColor blackColor];
    lblAddress1.font = [UIFont systemFontOfSize:17];
    lblAddress1.backgroundColor = [UIColor whiteColor];
    
    _tfAddress1 = [UITextField new];
    _tfAddress1.text = _restaurant.address1;
    _tfAddress1.textColor = [UIColor lightGrayColor];
    _tfAddress1.font = [UIFont systemFontOfSize:17];
    _tfAddress1.backgroundColor = [UIColor whiteColor];
    _tfAddress1.clearButtonMode=UITextFieldViewModeWhileEditing;
    
    UILabel *lblAddress2 = [UILabel new];
    lblAddress2.text = @"   Address 2";
    lblAddress2.textColor = [UIColor blackColor];
    lblAddress2.font = [UIFont systemFontOfSize:17];
    lblAddress2.backgroundColor = [UIColor whiteColor];
    
    _tfAddress2 = [UITextField new];
    _tfAddress2.text = _restaurant.address2;
    _tfAddress2.textColor = [UIColor lightGrayColor];
    _tfAddress2.font = [UIFont systemFontOfSize:17];
    _tfAddress2.backgroundColor = [UIColor whiteColor];
    _tfAddress2.clearButtonMode=UITextFieldViewModeWhileEditing;

    UILabel *lblCity = [UILabel new];
    lblCity.text = @"   City";
    lblCity.textColor = [UIColor blackColor];
    lblCity.font = [UIFont systemFontOfSize:17];
    lblCity.backgroundColor = [UIColor whiteColor];
    
    _tfCity = [UITextField new];
    _tfCity.text = _restaurant.cityName;
    _tfCity.textColor = [UIColor lightGrayColor];
    _tfCity.font = [UIFont systemFontOfSize:17];
    _tfCity.backgroundColor = [UIColor whiteColor];
    _tfCity.clearButtonMode=UITextFieldViewModeWhileEditing;
    
    UILabel *lblDistrict = [UILabel new];
    lblDistrict.text = @"   District";
    lblDistrict.textColor = [UIColor blackColor];
    lblDistrict.font = [UIFont systemFontOfSize:17];
    lblDistrict.backgroundColor = [UIColor whiteColor];
    
    _tfDistrict = [UITextField new];
    _tfDistrict.text = _restaurant.districtName;
    _tfDistrict.textColor = [UIColor lightGrayColor];
    _tfDistrict.font = [UIFont systemFontOfSize:17];
    _tfDistrict.backgroundColor = [UIColor whiteColor];
    _tfDistrict.clearButtonMode=UITextFieldViewModeWhileEditing;

    UILabel *lblAdjust = [UILabel new];
    lblAdjust.text = @"ADJUST LOCATION";
    lblAdjust.textColor = [UIColor lightGrayColor];
    lblAdjust.font = [UIFont systemFontOfSize:13];
    
    mapView = [[UIView alloc]init];
    mapView.backgroundColor = [UIColor whiteColor];
    
    map=[[MKMapView alloc]init];
    map.userInteractionEnabled = YES;
    [map setMapType:MKMapTypeStandard];
    [map setDelegate:self];
    CLLocationCoordinate2D coor;
    coor.latitude = [_restaurant.latitude doubleValue];
    coor.longitude = [_restaurant.longitude doubleValue];
    CLLocationCoordinate2D theCoordinate =CLLocationCoordinate2DMake(coor.latitude, coor.longitude);
    
    MKCoordinateSpan  theSpan;
    theSpan.latitudeDelta=0.01;
    theSpan.longitudeDelta=0.01;
    
    MKCoordinateRegion theRegion;
    theRegion.center=theCoordinate;
    theRegion.span=theSpan;
    
    
    pointAnnatation= [[MKPointAnnotation alloc] init];
    
    if(CLLocationCoordinate2DIsValid(theCoordinate))
    {
        [map setRegion:theRegion animated:NO];
        
        [map regionThatFits:theRegion];
        
        pointAnnatation.coordinate=theCoordinate;
        
        [map addAnnotation:pointAnnatation];
    }

    
    
    UITapGestureRecognizer *mapGrayTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mapTapPress)];
    [map addGestureRecognizer:mapGrayTap];


    UILabel *lblOperation = [UILabel new];
    lblOperation.text = @"OPERATION HOURS";
    lblOperation.textColor = [UIColor lightGrayColor];
    lblOperation.font = [UIFont systemFontOfSize:13];
    
    UILabel *lblDisplay = [UILabel new];
    lblDisplay.text = @"   Display Time";
    lblDisplay.textColor = [UIColor blackColor];
    lblDisplay.font = [UIFont systemFontOfSize:17];
    lblDisplay.backgroundColor = [UIColor whiteColor];
    
    _tfDisplay = [UITextField new];
    _tfDisplay.text = [self getDisplayTime];
    _tfDisplay.textColor = [UIColor lightGrayColor];
    _tfDisplay.font = [UIFont systemFontOfSize:17];
    _tfDisplay.backgroundColor = [UIColor whiteColor];
    _tfDisplay.clearButtonMode=UITextFieldViewModeWhileEditing;
    

    
    [mapView addSubview:map];
    [view addSubview:imagev];
    [_scrollview sd_addSubviews:@[restaurantStaus, lblStaus, Status, view, lblGeneralInfo, lblRestaurantName, _tfRestaurantName, lblPhoto, _tfPhoto, lblAddress, lblAddress1, _tfAddress1, lblAddress2, _tfAddress2, lblCity, _tfCity, lblDistrict, _tfDistrict, lblAdjust, mapView, lblOperation, lblDisplay, _tfDisplay]];
    
    restaurantStaus.sd_layout
    .topSpaceToView(_scrollview, 20)
    .leftSpaceToView(_scrollview, 15)
    .widthIs(DeviceWidth - 30)
    .heightIs(20);
    
    lblStaus.sd_layout
    .topSpaceToView(restaurantStaus, 0)
    .leftSpaceToView(_scrollview, 0)
    .heightIs(44)
    .widthIs(DeviceWidth/2);
    
    Status.sd_layout
    .topEqualToView(lblStaus)
    .leftSpaceToView(lblStaus, 0)
    .heightIs(44)
    .widthIs((DeviceWidth/2) - 44);

    view.sd_layout
    .topEqualToView(Status)
    .leftSpaceToView(Status, 0)
    .widthIs(44)
    .heightIs(44);
    
    imagev.sd_layout
    .centerXEqualToView(view)
    .centerYEqualToView(view)
    .heightIs(15)
    .widthIs(15);
    
    lblGeneralInfo.sd_layout
    .topSpaceToView(lblStaus, 20)
    .leftSpaceToView(_scrollview, 15)
    .heightIs(20)
    .widthIs(DeviceWidth);
    
    lblRestaurantName.sd_layout
    .topSpaceToView(lblGeneralInfo, 0)
    .leftSpaceToView(_scrollview, 0)
    .heightIs(44)
    .widthIs(DeviceWidth/2);
    
    _tfRestaurantName.sd_layout
    .topEqualToView(lblRestaurantName)
    .leftSpaceToView(lblRestaurantName, 0)
    .widthIs(DeviceWidth/2)
    .heightIs(44);
    
    lblPhoto.sd_layout
    .topSpaceToView(lblRestaurantName, 0)
    .leftSpaceToView(_scrollview, 0)
    .heightIs(44)
    .widthIs(DeviceWidth/2);
    
    _tfPhoto.sd_layout
    .topEqualToView(lblPhoto)
    .leftSpaceToView(lblPhoto, 0)
    .widthIs(DeviceWidth/2)
    .heightIs(44);
    
    lblAddress.sd_layout
    .topSpaceToView(lblPhoto, 20)
    .leftSpaceToView(_scrollview, 15)
    .widthIs(DeviceWidth - 30)
    .heightIs(20);
    
    lblAddress1.sd_layout
    .topSpaceToView(lblAddress, 0)
    .leftSpaceToView(_scrollview, 0)
    .heightIs(44)
    .widthIs(DeviceWidth/2);
    
    _tfAddress1.sd_layout
    .topEqualToView(lblAddress1)
    .leftSpaceToView(lblAddress1, 0)
    .widthIs(DeviceWidth/2)
    .heightIs(44);
    
    lblAddress2.sd_layout
    .topSpaceToView(lblAddress1, 0)
    .leftSpaceToView(_scrollview, 0)
    .heightIs(44)
    .widthIs(DeviceWidth/2);
    
    _tfAddress2.sd_layout
    .topEqualToView(lblAddress2)
    .leftSpaceToView(lblAddress2, 0)
    .widthIs(DeviceWidth/2)
    .heightIs(44);
    
    lblCity.sd_layout
    .topSpaceToView(lblAddress2, 0)
    .leftSpaceToView(_scrollview, 0)
    .heightIs(44)
    .widthIs(DeviceWidth/2);
    
    _tfCity.sd_layout
    .topEqualToView(lblCity)
    .leftSpaceToView(lblCity, 0)
    .widthIs(DeviceWidth/2)
    .heightIs(44);
    
    lblDistrict.sd_layout
    .topSpaceToView(lblCity, 0)
    .leftSpaceToView(_scrollview, 0)
    .heightIs(44)
    .widthIs(DeviceWidth/2);
    
    _tfDistrict.sd_layout
    .topEqualToView(lblDistrict)
    .leftSpaceToView(lblDistrict, 0)
    .widthIs(DeviceWidth/2)
    .heightIs(44);
    
    lblAdjust.sd_layout
    .topSpaceToView(lblDistrict, 20)
    .leftSpaceToView(_scrollview, 15)
    .widthIs(DeviceWidth - 30)
    .heightIs(20);
    
    mapView.sd_layout
    .topSpaceToView(lblAdjust, 0)
    .leftSpaceToView(_scrollview, 0)
    .widthIs(DeviceWidth )
    .heightIs(100);
    
    map.sd_layout
    .topSpaceToView(mapView, 0)
    .leftSpaceToView(mapView,0)
    .widthIs(DeviceWidth)
    .heightIs(100);
    
    lblOperation.sd_layout
    .topSpaceToView(mapView, 20)
    .leftSpaceToView(_scrollview, 15)
    .widthIs(DeviceWidth - 30)
    .heightIs(20);
    
    lblDisplay.sd_layout
    .topSpaceToView(lblOperation, 0)
    .leftSpaceToView(_scrollview, 0)
    .heightIs(44)
    .widthIs(DeviceWidth/2);
    
    _tfDisplay.sd_layout
    .topEqualToView(lblDisplay)
    .leftSpaceToView(lblDisplay, 0)
    .widthIs(DeviceWidth/2)
    .heightIs(44);
    
    
    [_scrollview setupAutoContentSizeWithBottomView:lblDisplay bottomMargin:10];
    
    

}
- (void)onPopupClick:(UIButton *)sender {
    [YBPopupMenu showRelyOnView:sender titles:TITLES icons:nil menuWidth:DeviceWidth/2 delegate:self];
}
#pragma mark - YBPopupMenuDelegate
- (void)ybPopupMenuDidSelectedAtIndex:(NSInteger)index ybPopupMenu:(YBPopupMenu *)ybPopupMenu
{
    NSLog(@"点击了 %@ 选项",TITLES[index]);
    
    statusString = TITLES[index];
    [Status setTitle:TITLES[index] forState:UIControlStateNormal];
}
@end
