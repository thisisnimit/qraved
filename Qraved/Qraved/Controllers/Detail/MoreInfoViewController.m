//
//  AboutViewController.m
//  Qraved
//
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

CGFloat CellContentOffset=10		;
CGFloat CellContentRowOffset=10;

#import "MoreInfoViewController.h"
#import "AppDelegate.h"
#import "UIConstants.h"
//#import "MixpanelHelper.h"

#import "NSString+Helper.h"
#import "UIViewController+Helper.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "Label.h"
#import "UIDevice+Util.h"
#import "UIImageView+Helper.h"
#import "LoadingView.h"
#import "ShareToPathView.h"
#import "NXOAuth2AccountStore.h"
#import "NavigationBarButtonItem.h"

#import "DBManager.h"
#import "IMGTag.h"

@implementation MoreInfoViewController{
    UIScrollView *scrollView;
    float currentY;
    IMGRestaurant *restaurant;
    NSMutableArray *moreInfoArrary;
    NSString *foodTagNameString;
    NSString *typeTagNameString;
    NSString *paymentTagNameString;
    NSMutableArray *featureArrary;
    NSMutableArray *allFeatureArrary;
}

-(id)initWithRestaurant:(IMGRestaurant *)tmpRestaurant{
    self=[super init];
    if(self){
        restaurant=tmpRestaurant;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationItem setTitle:restaurant.title];
    self.navigationController.navigationBarHidden = NO;
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setBackBarButtonOffset30];
    
    NavigationBarButtonItem *navigationBarLeftButton = [[NavigationBarButtonItem alloc ]initWithImage:[UIImage imageNamed:NavigationShareImage] andTitle:@"" target:self action:@selector(shareButtonClick) width:43.0f offset:[UIDevice isIos6]?10:25];
    self.navigationItem.rightBarButtonItem = navigationBarLeftButton;
    
    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    scrollView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:scrollView];
    [self addopeningHoursView];
    [self addMoreInfoView];
    [self addFeaturesView];
    
    
}
/////////////////////////////////
- (void)shareButtonClick {
    
    CGFloat yHeight = 310.0f+52.0f;
    
    UIPopoverListView* poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(0, DeviceHeight-yHeight, DeviceWidth, yHeight)];
    poplistview.delegate = self;
    poplistview.datasource = self;
    poplistview.listView.scrollEnabled = FALSE;
    [poplistview setTitle:@"Share"];
    [poplistview show];
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier];
    
    UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET*2, 12, 30, 30)];
    [cell.contentView addSubview:iconImage];
    
    Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(70, 15, DeviceWidth-70, 23) andTextFont:[UIFont systemFontOfSize:15] andTextColor:[UIColor color333333] andTextLines:0];
    [cell.contentView addSubview:titleLabel];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSInteger row = indexPath.row;
    
    if(row == 0){
        titleLabel.text = @"Facebook";
        titleLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:15];
        titleLabel.textColor = [UIColor color222222];
        iconImage.image = [UIImage imageNamed:@"facebook.png"];
    }else if (row == 1){
        titleLabel.text = @"Twitter";
        titleLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:15];
        titleLabel.textColor = [UIColor color222222];
        iconImage.image = [UIImage imageNamed:@"twitter.png"];
    }else if (row == 2){
        titleLabel.text = @"Message";
        titleLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:15];
        titleLabel.textColor = [UIColor color222222];
        iconImage.image = [UIImage imageNamed:@"message.png"];
    }else if (row == 3){
        titleLabel.text = @"Email";
        titleLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:15];
        titleLabel.textColor = [UIColor color222222];
        iconImage.image = [UIImage imageNamed:@"email.png"];
    }else if (row == 4){
        titleLabel.text = @"Path";
        titleLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:15];
        titleLabel.textColor = [UIColor color222222];
        iconImage.image = [UIImage imageNamed:@"path1.png"];
    }else
    {
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:18];
        cell.textLabel.textColor = [UIColor colorWithRed:23/255.0 green:127/255.0 blue:252/255.0 alpha:1];
        cell.textLabel.text = @"Cancel";
    }
    
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

#pragma mark - UIPopoverListViewDelegate
- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0){
        [self shareWithFacebook];
    }else if(indexPath.row==1){
        [self shareWithTwitter];
    }else if(indexPath.row==2){
        [self shareWithSMS];
    }else if(indexPath.row==3){
        [self shareWithEmail];
    }else if(indexPath.row==4){
        [self shareToPath];
    }
    [popoverListView dismiss];
}
-(void)shareToPath{
    NSString *pathToken = [[NSUserDefaults standardUserDefaults] objectForKey:QRAVED_PATH_TOKEN_KEY];
    if(pathToken==nil){
        [[NXOAuth2AccountStore sharedStore] requestAccessToAccountWithType:@"Path"];
    }else{
        NSLog(@"get path token directly from NSUserDefaults");
    }
    
    ShareToPathView *shareToPathView = [[ShareToPathView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+[UIDevice heightDifference]) title:[NSString stringWithFormat:@"%@ on Qraved.com",restaurant.title] url:nil latitude:restaurant.latitude longitude:restaurant.longitude restaurantTitle:restaurant.title  phone:restaurant.phoneNumber address:[restaurant address]];
    [self.view addSubview:shareToPathView];
//    [MixpanelHelper trackShareToPathWithshareObject:restaurant.title andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];

}
-(void)shareWithTwitter{
    [[LoadingView sharedLoadingView]startLoading];
    NSString *shareContentString = [NSString stringWithFormat:@"%@ on Qraved.",restaurant.title];
    SLComposeViewController *slComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    [slComposerSheet setInitialText:shareContentString];
    [slComposerSheet addURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],restaurant.seoKeyword]]];
    if(slComposerSheet){
        [self presentViewController:slComposerSheet animated:YES completion:^{
            [[LoadingView sharedLoadingView]stopLoading];
        }];
        [slComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            NSString *output;
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    output = @"Action Cancelled";
                    break;
                case SLComposeViewControllerResultDone:{
                    output = @"Post Successful";
//                    [MixpanelHelper trackShareToTwitterWithshareObject:restaurant.title andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
                }
                    break;
                default:
                    break;
            }
            if (result != SLComposeViewControllerResultCancelled){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter Message" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
        }];
    }
}

-(void)shareWithFacebook{
    [[LoadingView sharedLoadingView]startLoading];
    NSString *shareContentString = [NSString stringWithFormat:@"%@ on Qraved.",restaurant.title];
    SLComposeViewController *slComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [slComposerSheet setInitialText:shareContentString];
    [slComposerSheet addURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],restaurant.seoKeyword]]];
    if(slComposerSheet){
        [self presentViewController:slComposerSheet animated:YES completion:^{
            [[LoadingView sharedLoadingView]stopLoading];
        }];
        
        [slComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result){
            NSString *output;
            switch(result){
                case SLComposeViewControllerResultCancelled:
                    output = @"Action Cancelled";
                    break;
                case SLComposeViewControllerResultDone:{
                    output = @"Post Successful";
//                    [MixpanelHelper trackShareToFacebookWithshareObject:restaurant.title andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
                }
                    break;
                default:
                    break;
            }
            if (result != SLComposeViewControllerResultCancelled){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook Message" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
        }];
    }
}

-(void)shareWithEmail{
    if ([MFMailComposeViewController canSendMail]){
        NSString *subjectString = [NSString stringWithFormat:@"%@ on Qraved",restaurant.title];
        NSString *shareContentString = [NSString stringWithFormat:@"<a href='%@'>%@</a> on Qraved.",[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],restaurant.seoKeyword],restaurant.title];
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:subjectString];
        [mailer setMessageBody:shareContentString isHTML:YES];
        [self presentViewController:mailer animated:YES completion:nil];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Oops! This device does not support email."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
    }
}

-(void)shareWithSMS{
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Oops! This device does not support sms."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    NSString *shareContentString = [NSString stringWithFormat:@"%@ on Qraved. Open %@ to check out.",restaurant.title,[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],restaurant.seoKeyword]];
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setBody:shareContentString];
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
//            [MixpanelHelper trackShareToEmailWithshareObject:restaurant.title andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
            break;
        case MFMailComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Failed to send E-Mail!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
        }
            break;
            
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
//            [MixpanelHelper trackShareToSMSWithshareObject:restaurant.title andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 53.0f;
}

-(void)addopeningHoursView
{
    NSMutableArray *openingHours = [[NSMutableArray alloc]init];
    [openingHours addObjectsFromArray:[restaurant openingHours]];
    if(openingHours==nil || openingHours.count==0){
        return;
    }
    
    
    UIView * openingHoursView = [[UIView alloc]init];
    [scrollView addSubview:openingHoursView];
    
    UIView *openView  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 26)];
    [openingHoursView addSubview:openView];
    openView.backgroundColor = [UIColor colorFBFBFB];
    
    UIView *openingHoursLabel = [[UIView alloc]initDetailFeatureViewWithOrignY:0 andText:@"Opening Hours"];
    [openingHoursView addSubview:openingHoursLabel];
    
    
    float startY = 12+openingHoursLabel.endPointY;
    for (int i=0; i<openingHours.count; i++) {
        UILabel *hoursLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, startY, DeviceWidth, 20)];
        hoursLabel.text = [openingHours objectAtIndex:i];
        hoursLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
        hoursLabel.textColor = [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1];
        hoursLabel.alpha = 0.8;
        [openingHoursView addSubview:hoursLabel];
        startY = hoursLabel.endPointY+5;
    }
    
    openingHoursView.frame = CGRectMake(0, currentY, DeviceWidth, startY+12);
    currentY = openingHoursView.endPointY;
    [scrollView setContentSize:CGSizeMake(DeviceWidth, currentY+80)];
    
    //标题下阴影
    UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:0 andHeight:4];
    [self.view addSubview:shadowImageView];
}
-(void)addMoreInfoView
{
    moreInfoArrary = [[NSMutableArray alloc]initWithCapacity:0];
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT name FROM IMGTag t left join IMGRestaurantTag rt on t.tagId=rt.tagId WHERE rt.restaurantId = '%@' and t.type='Food' ORDER BY t.name;",restaurant.restaurantId] successBlock:^(FMResultSet *resultSet) {
        int i=0;
        while([resultSet next]){
            if(i++==0){
                foodTagNameString = [NSString stringWithFormat:@"Food: %@",[resultSet stringForColumn:@"name"]];
            }else{
                foodTagNameString = [foodTagNameString stringByAppendingString:[NSString stringWithFormat:@", %@",[resultSet stringForColumn:@"name"]]];
            }
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"addMoreInfoView error when get datas:%@",error.description);
    }];
    if(foodTagNameString!=nil){
        [moreInfoArrary addObject:foodTagNameString];
    }
    
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT name FROM IMGTag t left join IMGRestaurantTag rt on t.tagId=rt.tagId WHERE rt.restaurantId = '%@' and t.type='Restaurant Type' ORDER BY t.name;",restaurant.restaurantId] successBlock:^(FMResultSet *resultSet) {
        int i=0;
        while([resultSet next]){
            if(i++==0){
                typeTagNameString = [NSString stringWithFormat:@"Type: %@",[resultSet stringForColumn:@"name"]];
            }else{
                typeTagNameString = [typeTagNameString stringByAppendingString:[NSString stringWithFormat:@", %@",[resultSet stringForColumn:@"name"]]];
            }
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"addMoreInfoView error when get datas:%@",error.description);
    }];
    if(typeTagNameString!=nil){
        [moreInfoArrary addObject:typeTagNameString];
    }
    
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT name FROM IMGTag t left join IMGRestaurantTag rt on t.tagId=rt.tagId WHERE rt.restaurantId = '%@' and t.type='Payment' ORDER BY t.name;",restaurant.restaurantId] successBlock:^(FMResultSet *resultSet) {
        int i=0;
        while([resultSet next]){
            if(i++==0){
                paymentTagNameString = [NSString stringWithFormat:@"Payment: %@",[resultSet stringForColumn:@"name"]];
            }else{
                paymentTagNameString = [paymentTagNameString stringByAppendingString:[NSString stringWithFormat:@", %@",[resultSet stringForColumn:@"name"]]];
            }
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"addMoreInfoView error when get datas:%@",error.description);
    }];
    if(paymentTagNameString!=nil){
        [moreInfoArrary addObject:paymentTagNameString];
    }
    
    if(moreInfoArrary.count==0){
        return;
    }
    
    UIView * moreInfoView = [[UIView alloc]init];
    [scrollView addSubview:moreInfoView];
    
    UIView *openView  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 26)];
    [moreInfoView addSubview:openView];
    openView.backgroundColor = [UIColor colorFBFBFB];
    
    UIView *moreInfoLabel = [[UIView alloc]initDetailFeatureViewWithOrignY:0 andText:@"More Info"];
    [openView addSubview:moreInfoLabel];
    
    
    UIView *moreInfo = [[UIView alloc]init];
    //    greatsView.backgroundColor = [UIColor redColor];
    [moreInfoView addSubview:moreInfo];
    
    CGFloat startY=12;
    
    for (int i=0; i<moreInfoArrary.count; i++) {
        CGSize size = [[moreInfoArrary objectAtIndex:i] sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(DeviceWidth-30, 200)];
        Label *infoLabel = [[Label alloc]initWithFrame:CGRectMake(15, startY, DeviceWidth-30, size.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1] andTextLines:0];
        infoLabel.text = [moreInfoArrary objectAtIndex:i];
        [moreInfo addSubview:infoLabel];
        startY = infoLabel.endPointY+5;
    }
    
    moreInfo.frame = CGRectMake(0, moreInfoLabel.endPointY, DeviceWidth, startY+12);
    moreInfoView.frame = CGRectMake(0, currentY, DeviceWidth, moreInfo.endPointY);
    
    currentY = moreInfoView.endPointY;
    [scrollView setContentSize:CGSizeMake(DeviceWidth, currentY+80)];
}

-(void)addFeaturesView{
    //SELECT name FROM IMGTag t left join IMGRestaurantTag rt on t.tagId=rt.tagId WHERE rt.restaurantId = '%@' and t.type='Features' and t.status=1 ORDER BY t.name;
    featureArrary = [[NSMutableArray alloc]initWithCapacity:0];
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT name FROM IMGTag t left join IMGRestaurantTag rt on t.tagId=rt.tagId WHERE rt.restaurantId = '%@' and t.type='Features' ORDER BY t.name;",restaurant.restaurantId] successBlock:^(FMResultSet *resultSet) {
        while([resultSet next]){
            [featureArrary addObject:[resultSet stringForColumn:@"name"]];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"addFeaturesView error when get datas:%@",error.description);
    }];
    
    //SELECT * FROM IMGTag where status=1 and type='Features';
    allFeatureArrary = [[NSMutableArray alloc]initWithCapacity:0];
//    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT name FROM IMGTag t left join IMGRestaurantTag rt on t.tagId=rt.tagId WHERE rt.restaurantId = '%@' and t.type='Features' ORDER BY t.name;",restaurant.restaurantId] successBlock:^(FMResultSet *resultSet) {
//        while([resultSet next]){
//            [allFeatureArrary addObject:[resultSet stringForColumn:@"name"]];
//        }
//        [resultSet close];
//    }failureBlock:^(NSError *error) {
//        NSLog(@"addFeaturesView error when get datas:%@",error.description);
//    }];
    [allFeatureArrary addObjectsFromArray:featureArrary];
    
    UIView * featuresView = [[UIView alloc]init];
    [scrollView addSubview:featuresView];
    
    UIView *openView  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 26)];
    [featuresView addSubview:openView];
    openView.backgroundColor = [UIColor colorFBFBFB];
    
    UIView *featuresLabel = [[UIView alloc]initDetailFeatureViewWithOrignY:0 andText:@"Features"];
    [openView addSubview:featuresLabel];

    
    UIView *featureImage = [[UIView alloc]initWithFrame:CGRectMake(0, featuresLabel.endPointY, DeviceWidth, 20)];
    [featuresView addSubview:featureImage];
    
    
    UIView *featureView = [[UIView alloc]init];
    [featuresView addSubview:featureView];
    
    CGFloat startY=0;
    for (int i=0; i<allFeatureArrary.count; i++) {
        CGSize size = [[allFeatureArrary objectAtIndex:i] sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(DeviceWidth-30, 200) lineBreakMode:NSLineBreakByTruncatingTail];
        
        Label *featureLabel = [[Label alloc]initWithFrame:CGRectMake(15, startY, DeviceWidth-30, size.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor color222222] andTextLines:1];
        featureLabel.text = [allFeatureArrary objectAtIndex:i];
       
        Label *retLabel = [[Label alloc]initWithFrame:CGRectMake(DeviceWidth-60, startY, 40, size.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor color333333] andTextLines:1];
        retLabel.textAlignment = NSTextAlignmentCenter;
        [featureView addSubview:retLabel];
        retLabel.text = @"YES";
        retLabel.textColor = [UIColor color3BAF24];
//        if([featureArrary indexOfObject:[allFeatureArrary objectAtIndex:i]]==NSNotFound){
//            retLabel.text = @"NO";
//            featureLabel.textColor = [UIColor colorCCCCCC];
//            retLabel.textColor = [UIColor colorC2060A];
//        }else{
//            retLabel.text = @"YES";
//            retLabel.textColor = [UIColor color3BAF24];
//        }
        
        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, featureLabel.endPointY+5, DeviceWidth, 1)];
        lineImage.image = [UIImage imageNamed:@"CutOffRule"];
        [featureView addSubview:lineImage];
        
        [featureView addSubview:featureLabel];
        startY = featureLabel.endPointY+10;
    }
    featureView.frame = CGRectMake(0, featureImage.endPointY-10, DeviceWidth, startY);
    featuresView.frame = CGRectMake(0, currentY, DeviceWidth, featureView.endPointY);
    currentY = featuresView.endPointY;
    [scrollView setContentSize:CGSizeMake(DeviceWidth, currentY+80)];
}

@end
