//
//  SuggestMapViewController.m
//  Qraved
//
//  Created by Lucky on 15/5/6.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "SuggestMapViewController.h"
#import "UIViewController+Helper.h"
#import "CalloutMapAnnotation.h"
#import "CallOutAnnotationView.h"

#define BtnHeight 88

@interface SuggestMapViewController ()<CLLocationManagerDelegate>
{
    IMGRestaurant *_restaurant;
    MKMapView *map;
    UIView *mapView;
    MKPointAnnotation *pointAnnatation;
    UIView *mapGrayView;
    UIImageView *locationImageView;
    CLLocationCoordinate2D centerCoordinate;
    NSDictionary *infoDic;
}
@end

@implementation SuggestMapViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationItem setTitle:@"Map"];
    self.navigationController.navigationBarHidden = NO;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    infoDic = [[NSDictionary alloc] init];
    [self setBackBarButtonOffset30];
    [self addMapView];
    [self addBtns];
    [self addlocationImageView];
}
- (void)addlocationImageView
{
    locationImageView = [[UIImageView alloc] init];
    locationImageView.image = [UIImage imageNamed:@"location"];
    locationImageView.frame = CGRectMake(0, 0, 28, 39);
    locationImageView.center = mapView.center;
    [self.view addSubview:locationImageView];
}
- (void)addBtns
{
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn setTitle:@"Cancel" forState:UIControlStateNormal];
    cancelBtn.backgroundColor = [UIColor whiteColor];
    cancelBtn.frame = CGRectMake(0, DeviceHeight-BtnHeight, DeviceWidth/2, 44);
    [cancelBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:cancelBtn];
    [cancelBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    cancelBtn.tag = 100;
    
    UIButton *subBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [subBtn setTitle:@"Sub" forState:UIControlStateNormal];
    subBtn.backgroundColor = [UIColor whiteColor];
    subBtn.frame = CGRectMake(DeviceWidth/2, DeviceHeight-BtnHeight, DeviceWidth/2, 44);
    [subBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:subBtn];
    [subBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    subBtn.tag = 101;
}
- (void)btnClick:(UIButton *)btn
{
    if (btn.tag == 100)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        if ([infoDic allKeys].count!=0)
        {
            if (self.delegate&&[self.delegate respondsToSelector:@selector(changeMap:)]) {
                [self.delegate changeMap:infoDic];
            }
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)addMapView
{
    
    
    mapView = [[UIView alloc]init];
    mapView.backgroundColor = [UIColor whiteColor];
    
    
    
    map=[[MKMapView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-BtnHeight)];
    
    [map setMapType:MKMapTypeStandard];
    [map setDelegate:self];
    [mapView addSubview:map];
    CLLocationCoordinate2D coor;
    coor.latitude = [_restaurant.latitude doubleValue];
    coor.longitude = [_restaurant.longitude doubleValue];
    CLLocationCoordinate2D theCoordinate =CLLocationCoordinate2DMake(coor.latitude, coor.longitude);
    
    MKCoordinateSpan  theSpan;
    theSpan.latitudeDelta=0.01;
    theSpan.longitudeDelta=0.01;
    
    MKCoordinateRegion theRegion;
    theRegion.center=theCoordinate;
    theRegion.span=theSpan;
    
    
    pointAnnatation= [[MKPointAnnotation alloc] init];
    
    if(CLLocationCoordinate2DIsValid(theCoordinate))
    {
        [map setRegion:theRegion animated:NO];
        
        [map regionThatFits:theRegion];
        
        pointAnnatation.coordinate=theCoordinate;
        
        //[map addAnnotation:pointAnnatation];
    }
    
    
    [mapView setFrame:CGRectMake(0,0, DeviceWidth, DeviceHeight-BtnHeight)];
    
    //UITapGestureRecognizer *mTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
    //[map addGestureRecognizer:mTap];
    
    [self.view addSubview:mapView];
    
}

- (void)mapView:(MKMapView *)mapView_ regionDidChangeAnimated:(BOOL)animated {
    MKCoordinateRegion region;
    centerCoordinate = mapView_.region.center;
    region.center= centerCoordinate;
    
    NSLog(@" regionDidChangeAnimated %f,%f",centerCoordinate.latitude, centerCoordinate.longitude);
    infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithFloat:centerCoordinate.latitude],@"latitude",[NSNumber numberWithFloat:centerCoordinate.longitude],@"longitude", nil];
}

-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant
{
    if(self= [super init]){
        _restaurant = paramRestaurant;
    }
    return self;
}
 


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
