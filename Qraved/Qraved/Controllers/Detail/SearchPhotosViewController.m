//
//  SearchPhotosViewController.m
//  Qraved
//
//  Created by Admin on 8/2/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "SearchPhotosViewController.h"
#import "PhotosDetailViewController.h"

#import "SDWebImageDownloader.h"
#import "Consts.h"
#import "AppDelegate.h"
#import "AddPhotoViewController.h"

#import "Tools.h"
#import "UIColor+Helper.h"
#import "UIViewController+Helper.h"
#import "UIView+Helper.h"
#import "UIButton+LinkButton.h"
#import "UIImage+Resize.h"
 
#import "UIImageView+WebCache.h"
#import "UIImage+Helper.h"
#import "Label.h"
#import "UIDevice+Util.h"
#import "NSString+Helper.h"
#import "NodataView.h"
#import "DBManager.h"
#import "IMGUser.h"
#import "IMGRestaurant.h"
#import "IMGDish.h"
#import "PostDataHandler.h"
#import "LoadingImageView.h"
#import "DetailViewController.h"
//#import "AFHTTPRequestOperationManager.h"
#import "IMGRestaurantImage.h"
#import "IMGRestaurantEvent.h"
#import "RestaurantHandler.h"
#import "AlbumViewController.h"

#define qraveWidth 305.0/2
#define IMAGE_WIDTH 60

@interface SearchPhotosViewController ()
{
    NSInteger currentCount;
    UIScrollView *scrollView;
    CGFloat currentHeight;
    float leftCurrentHeight;
    float rightCurrentHeight;
    
    int currentQrave;
    
    int fetchQravesMax;
    
    float startPointY;
    
    NodataView *nodataView;
    
    float currentY;
    NSMutableArray *restaurantPhotoArray;
    NSMutableArray *userPhotoArray;
    NSMutableArray *eventPhotoArray;
    IMGRestaurant *restaurant;
    
    LoadingImageView *loadingImageView;
    
    int maxNum;
    NSInteger oldUserPhotoArrayCount;
    Label *numLabel;
    
//    int photoType;
    NSNumber *offset;
    NSNumber *dishcount;
}
@property (nonatomic, retain) NSMutableArray *qravesList;
@property (nonatomic, retain) UIScrollView *mScrollView;
@end

@implementation SearchPhotosViewController


-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant{
    self = [super init];
    if (self != nil) {
        restaurant = paramRestaurant;
    }
    return self;
}

-(instancetype)initWithRestaurant:(IMGRestaurant*)paramRestaurant restaurantPhotoArray:(NSArray*)photoArr{
    self = [super init];
    if (self != nil) {
        restaurant = paramRestaurant;
//        if([[photoArr firstObject] isKindOfClass:[IMGRestaurantEvent class]]){
//            photoType = 2;
//            eventPhotoArray = [photoArr mutableCopy];
//        }else{
//            photoType = 1;
            restaurantPhotoArray=[photoArr mutableCopy];

//        }
        
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=NO;
    [self.navigationItem setTitle:restaurant.title];
    [super viewWillAppear:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Restaurant Photo list page";
    [self setBackBarButtonOffset30];
    //    [self setPhotoBarButton];
    self.qravesList = [NSMutableArray arrayWithCapacity:0];
    
    leftCurrentHeight = 5;
    rightCurrentHeight = 5;
    
    currentCount=0;
    oldUserPhotoArrayCount = 0;
    offset = [NSNumber numberWithInt:0];
    userPhotoArray = [[NSMutableArray alloc] initWithCapacity:0];

    [self  addScrollView];

//    if(photoType == 2){
//        [self addEventPhotos];
//    }else{
        //    [self postPhotosData:currentCount];
        [self addRestaurantPhotos];
        [self addUserPhotos];
//    }
    [self getDishesFromServer:[restaurant.restaurantId intValue] max:maxNum];

}

-(void)setPhotoBarButton
{
    UIBarButtonItem *photoBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:NavigationPhotoImage] style:UIBarButtonItemStylePlain target:self action:@selector(photoButtonClick:)];
    photoBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=photoBtn;
}
-(void)photoButtonClick
{
    UIImagePickerController *imagepicker = [[UIImagePickerController alloc] init];
    imagepicker.delegate = (id)self;
    imagepicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagepicker.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    imagepicker.allowsEditing = YES;
    [self presentViewController:imagepicker animated:YES completion:nil];
}
#pragma mark -ImagePickerController delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage=[info objectForKey:UIImagePickerControllerEditedImage];
    CGFloat imageHeight=0;
    CGFloat imageWidth=0;
    if(chosenImage.size.width>UPLOAD_PICTURE_SIZE_WIDTH)
    {
        imageHeight=UPLOAD_PICTURE_SIZE_WIDTH*chosenImage.size.height/chosenImage.size.width;
        imageWidth=UPLOAD_PICTURE_SIZE_WIDTH;
    }
    else
    {
        imageHeight=chosenImage.size.height;
        imageWidth=chosenImage.size.width;
    }
    
    CGSize tagetSize=CGSizeMake(imageWidth, imageHeight);
    UIImage*tagetImage=[chosenImage imageWithScaledToSize:tagetSize];
    
    [picker dismissViewControllerAnimated:NO completion:^{
        
        AddPhotoViewController *addPhotoVC = [[AddPhotoViewController alloc]initWithRestauarnt:restaurant];
        addPhotoVC.isWriteReview = NO;
        addPhotoVC.selectedImage = tagetImage;
        [self presentViewController:addPhotoVC animated:YES completion:nil];
    }];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)addScrollView
{
    if (self.mScrollView == nil) {
        self.mScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-44)];
        self.mScrollView.backgroundColor = [UIColor blackColor];
        self.mScrollView.showsVerticalScrollIndicator = NO;
        //        self.mScrollView.delegate = self;
        self.mScrollView.contentSize = CGSizeZero;
        self.mScrollView.scrollsToTop = NO;
        currentY = 0;
        [self.view addSubview:self.mScrollView];
    }
}
-(void)addEventPhotos
{
    currentY=10;
    if(eventPhotoArray.count==0) return;
    
    Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, currentY, DeviceWidth/2, 30) andTextFont:[UIFont fontWithName:FONT_GOTHAM_LIGHT size:12] andTextColor:[UIColor whiteColor] andTextLines:1];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.text = @"Promo Photos";
    [self.mScrollView addSubview:titleLabel];
    
    Label *numlabel = [[Label alloc]initWithFrame:CGRectMake(DeviceWidth-20-60, currentY, 60, 35) andTextFont:[UIFont fontWithName:FONT_GOTHAM_LIGHT size:12] andTextColor:[UIColor whiteColor] andTextLines:1];
    numlabel.textAlignment = NSTextAlignmentLeft;
    numlabel.text = [NSString stringWithFormat:@"%lu photos",(unsigned long)eventPhotoArray.count];
    
    [self.mScrollView addSubview:numlabel];
    
    
    for (int i=0; i<eventPhotoArray.count; i++) {
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(6+(3.5+(DeviceWidth-19)/3)*(i%3), titleLabel.endPointY+((DeviceWidth-19)/3+3)*(i/3), (DeviceWidth-19)/3, (DeviceWidth-19)/3)];
        imageView.tag =i;
        UITapGestureRecognizer *Tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagePressed:)];
        [Tap setNumberOfTapsRequired:1];
        [Tap setNumberOfTouchesRequired:1];
        imageView.userInteractionEnabled=YES;
        [imageView addGestureRecognizer:Tap];
        
        // UIImageView *placeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, TOP_SCROLL_HEIGHT)];
        // [imageView setImageWithURL:nil placeholderImage:[UIImage imageNamed:((IMGRestaurantImage *)[restaurantPhotoArray objectAtIndex:i]).imageUrl]];
        
        NSString *url;
        if (self.isUseWebP) {
            url = [((IMGRestaurantEvent *)[eventPhotoArray objectAtIndex:i]).imageUrl returnFullWebPImageUrlWithWidth:(DeviceWidth-19)/3];
        }else{
            url = [((IMGRestaurantEvent *)[eventPhotoArray objectAtIndex:i]).imageUrl returnFullImageUrlWithWidth:(DeviceWidth-19)/3];
        }
        UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake( (DeviceWidth-19)/3, (DeviceWidth-19)/3)];
        __weak typeof(imageView) weakImageView = imageView;
        [imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            image = [image imageByScalingAndCroppingForSize:CGSizeMake( (DeviceWidth-19)/3,(DeviceWidth-19)/3)];
            
            [weakImageView setImage:image];
            
        }];

//        [imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2]];

//        [imageView setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] sizeFitWidth:YES];
        [self.mScrollView addSubview:imageView];
        currentY = imageView.endPointY;
        
    }
    
    startPointY=currentY;
    
    self.mScrollView.contentSize = CGSizeMake(DeviceWidth, currentY);
}

-(void)addRestaurantPhotos
{
    currentY=10;
    if(restaurantPhotoArray.count==0) return;
    
    Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, currentY, DeviceWidth/2, 30) andTextFont:[UIFont fontWithName:FONT_GOTHAM_LIGHT size:12] andTextColor:[UIColor whiteColor] andTextLines:1];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.text = @"Restaurant Photos";
    [self.mScrollView addSubview:titleLabel];
    
    Label *numlabel = [[Label alloc]initWithFrame:CGRectMake(DeviceWidth-20-60, currentY, 65, 35) andTextFont:[UIFont fontWithName:FONT_GOTHAM_LIGHT size:12] andTextColor:[UIColor whiteColor] andTextLines:1];
    numlabel.textAlignment = NSTextAlignmentLeft;
    numlabel.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)restaurantPhotoArray.count,restaurantPhotoArray.count>1?@"photos":@"photo"];
    
    [self.mScrollView addSubview:numlabel];
    
    
    for (int i=0; i<restaurantPhotoArray.count; i++) {
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(6+(3.5+(DeviceWidth-19)/3)*(i%3), titleLabel.endPointY+((DeviceWidth-19)/3+3)*(i/3), (DeviceWidth-19)/3, (DeviceWidth-19)/3)];
        imageView.tag =i;
        UITapGestureRecognizer *Tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagePressed:)];
        [Tap setNumberOfTapsRequired:1];
        [Tap setNumberOfTouchesRequired:1];
        imageView.userInteractionEnabled=YES;
        [imageView addGestureRecognizer:Tap];
        
        // UIImageView *placeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, TOP_SCROLL_HEIGHT)];
        // [imageView setImageWithURL:nil placeholderImage:[UIImage imageNamed:((IMGRestaurantImage *)[restaurantPhotoArray objectAtIndex:i]).imageUrl]];
        NSString *url;
        if (self.isUseWebP) {
            url = [((IMGRestaurantImage *)[restaurantPhotoArray objectAtIndex:i]).imageUrl returnFullWebPImageUrlWithWidth:(DeviceWidth-19)/3];
        }else{
            url = [((IMGRestaurantImage *)[restaurantPhotoArray objectAtIndex:i]).imageUrl returnFullImageUrlWithWidth:(DeviceWidth-19)/3];
        }
//        [imageView setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] sizeFitWidth:YES];
//        [imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2]];
        UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake( (DeviceWidth-19)/3, (DeviceWidth-19)/3)];
        __weak typeof(imageView) weakImageView = imageView;
        [imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            image = [image imageByScalingAndCroppingForSize:CGSizeMake( (DeviceWidth-19)/3,(DeviceWidth-19)/3)];
            
            [weakImageView setImage:image];
            
        }];


        [self.mScrollView addSubview:imageView];
        currentY = imageView.endPointY;
        
    }

    startPointY=currentY;
    
    self.mScrollView.contentSize = CGSizeMake(DeviceWidth, currentY);
}
-(void)addUserPhotos{
    userPhotoArray = [[NSMutableArray alloc] initWithCapacity:0];
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGDish WHERE restaurantId = '%@' order by dishId desc;",restaurant.restaurantId] successBlock:^(FMResultSet *resultSet){
        int tempNum = 1;
        while([resultSet next]){
            IMGDish *dish=[[IMGDish alloc]init];
            [dish setValueWithResultSet:resultSet];
            tempNum = (tempNum > [dish.dishId intValue])?tempNum:[dish.dishId intValue];
            [userPhotoArray addObject:dish];
        }
        maxNum = tempNum;
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"addUserPhotos error when get datas:%@",error.description);
    }];

    if(userPhotoArray.count==0) return;
    
    
    [self addImages];
    
}
-(void)addImages{

    Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, startPointY+10, DeviceWidth/2, 30) andTextFont:[UIFont fontWithName:FONT_GOTHAM_LIGHT size:12] andTextColor:[UIColor whiteColor] andTextLines:1];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.text = @"User Photos";
    titleLabel.tag=1800;
    [self.mScrollView addSubview:titleLabel];
    
    numLabel = [[Label alloc]initWithFrame:CGRectMake(DeviceWidth-20-60, startPointY+10, 65, 30) andTextFont:[UIFont fontWithName:FONT_GOTHAM_LIGHT size:12] andTextColor:[UIColor whiteColor] andTextLines:1];
    numLabel.textAlignment = NSTextAlignmentLeft;
    numLabel.text = [NSString stringWithFormat:@"%lu %@",[dishcount integerValue],([dishcount integerValue]>1)?@"photos":@"photo"];
    numLabel.tag=1811;
    [self.mScrollView addSubview:numLabel];
    
    
    currentY = numLabel.endPointY;
    loadingImageView = [[LoadingImageView alloc]initWithFrame:CGRectMake(0, currentY, DeviceWidth, self.mScrollView.frame.size.height-currentY) andViewNum:1 andShadowWidth:0];
    [self.mScrollView addSubview:loadingImageView];
    [loadingImageView startLoading];
    // self.mScrollView .contentSize = CGSizeMake(DeviceWidth, DeviceHeight-43);
    
    float userPhotoHeight=currentY;
//    for (NSInteger i = (oldUserPhotoArrayCount != 0)?oldUserPhotoArrayCount:0 ; i<userPhotoArray.count; i++) {
    for (NSInteger i = 0 ; i<userPhotoArray.count; i++) {
        UIImageView *dishImageView = [[UIImageView alloc]initWithFrame:CGRectMake(6+(3.5+(DeviceWidth-19)/3)*(i%3), userPhotoHeight+((DeviceWidth-19)/3+3)*(i/3), (DeviceWidth-19)/3, (DeviceWidth-19)/3)];
        dishImageView.tag = restaurantPhotoArray.count+i;
        UITapGestureRecognizer *Tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagePressed:)];
        [Tap setNumberOfTapsRequired:1];
        [Tap setNumberOfTouchesRequired:1];
        dishImageView.userInteractionEnabled=YES;
        [dishImageView addGestureRecognizer:Tap];
        IMGDish *dish=[userPhotoArray objectAtIndex:i];
        //        dishImageView.image = [UIImage imageNamed:dish.imageUrl];
        [self.mScrollView addSubview:dishImageView];
        
        NSString *urlString;
        if (self.isUseWebP) {
            if ([dish.type intValue]==2) {
                if (dish.lowResolutionImage.length>0) {
                    urlString = dish.lowResolutionImage;

                }else if (dish.imageUrl.length>0){
                    urlString = dish.imageUrl;

                }else if (dish.thumbnailImage.length>0){
                    urlString = dish.thumbnailImage;
                }
                UIImageView *instagramLogo = [[UIImageView alloc] initWithFrame:CGRectMake(dishImageView.frame.size.width/6*5-5, dishImageView.frame.size.width/6*5-5, dishImageView.frame.size.width/6, dishImageView.frame.size.width/6)];
                instagramLogo.image = [UIImage imageNamed:@"ins2.png"];
                [dishImageView addSubview:instagramLogo];
            }else{
                urlString = [dish.imageUrl returnFullWebPImageUrlWithWidth:(DeviceWidth-19)/3];
            }

        }else{
            if (dish.lowResolutionImage.length>0) {
                urlString = dish.lowResolutionImage;
                UIImageView *instagramLogo = [[UIImageView alloc] initWithFrame:CGRectMake(dishImageView.frame.size.width/6*5-5, dishImageView.frame.size.width/6*5-5, dishImageView.frame.size.width/6, dishImageView.frame.size.width/6)];
                instagramLogo.image = [UIImage imageNamed:@"ins2.png"];
                [dishImageView addSubview:instagramLogo];
            }else{
            urlString = [dish.imageUrl returnFullImageUrlWithWidth:(DeviceWidth-19)/3];
            }
        }
        
        __weak typeof(dishImageView) weakRestaurantImageView = dishImageView;
        UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake( (DeviceWidth-19)/3, (DeviceWidth-19)/3)];

        [dishImageView setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
            if (image) {
                image = [image imageByScalingAndCroppingForSize:CGSizeMake( (DeviceWidth-19)/3,(DeviceWidth-19)/3)];
                
                [weakRestaurantImageView setImage:image];

            }

        }];
        
        currentY = dishImageView.endPointY;
    }
    oldUserPhotoArrayCount = userPhotoArray.count;
    self.mScrollView.contentSize = CGSizeMake(DeviceWidth, currentY+80);
    [loadingImageView stopLoading];
    loadingImageView = nil;
}

-(void)imagePressed:(UIGestureRecognizer*)gesture{
    NSInteger tag = gesture.view.tag;
    NSMutableArray *images=[[NSMutableArray alloc] init];
    
//    if (photoType == 2) {
//        
//        AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:eventPhotoArray andPhotoTag:tag andRestaurant:restaurant andFromDetail:NO];
//        
//        albumViewController.title = restaurant.title;
//        albumViewController.showPage=NO;
//        albumViewController.amplitudeType = @"Restaurant detail page - ";
//        albumViewController.isUseWebP = self.isUseWebP;
//        [self.navigationController pushViewController:albumViewController animated:YES];
//
//
//    }else{
        for(IMGRestaurantImage *image in restaurantPhotoArray){
            IMGDish *dish = [[IMGDish alloc] init];
            dish.imageUrl=image.imageUrl;
            dish.restaurantId=image.restaurantId;
            
            dish.userPhotoCountDic = image.userPhotoCountDic;
            dish.photoCreditTypeDic  = image.photoCreditTypeDic;
            dish.userIdDic  = image.userIdDic;
            dish.photoCreditDic  = image.photoCreditDic;
            dish.userReviewCountDic  = image.userReviewCountDic;
            dish.userAvatarDic  = image.userAvatarDic;
            dish.photoCreditUrlDic  = image.photoCreditUrlDic;
            dish.restaurantIdDic  = image.restaurantIdDic;
            
            dish.isRestaurantPhoto = YES;
            [images addObject:dish];
        }
        [images addObjectsFromArray:userPhotoArray];
        
        AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:images andPhotoTag:tag andRestaurant:restaurant andFromDetail:NO];
        
        albumViewController.title = restaurant.title;
        albumViewController.showPage=NO;
        albumViewController.amplitudeType = @"Restaurant detail page - ";
        albumViewController.isUseWebP = self.isUseWebP;
        [self.navigationController pushViewController:albumViewController animated:YES];


//    }
    
}


-(void)addNodataView
{
    NSString *noDataMessage= [NSString stringWithFormat:NSLocalizedString(@"This restaurant does not have any photos yet.", nil)];
    
    nodataView=[[NodataView alloc]initWIthMessage:noDataMessage];
    [nodataView setFrame:CGRectMake((DeviceWidth-nodataView.width)/2, (SUBVIEWHEIGHT-nodataView.height)/2, nodataView.width, nodataView.height)];
    [self.view addSubview:nodataView];
}
- (void)getDishesFromServer:(int)restaurantId max:(int)max
{
//    if(photoType == 2){
//        //接口不用了
//        [RestaurantHandler getAllEventsFromServer:[NSNumber numberWithInt:restaurantId] andBlock:^(NSArray *photoEventList) {
//            eventPhotoArray = [[NSMutableArray alloc] initWithArray:photoEventList];
//            if (eventPhotoArray.count > 0)
//            {
//                for (UIView *view in self.mScrollView.subviews)
//                {
//                    [view removeFromSuperview];
//                }
//                [self addEventPhotos];
//
//            }
//
//        }];
//        
//    }else{
        [[DBManager manager] deleteWithSql:[NSString stringWithFormat:@"delete from IMGDish where restaurantId = '%@';",restaurant.restaurantId]];
        
        NSDictionary *parameters=@{@"restaurantId":[NSNumber numberWithInt:restaurantId],@"max":@(50),@"offset":offset};
        
        [[IMGNetWork sharedManager] GET:@"app/detail/photos/v2" parameters:parameters success:^(NSURLSessionDataTask *operation, id responseObject) {
            
            NSLog(@"**********responseObject%@",responseObject);
            dishcount = [responseObject objectForKey:@"dishCount"];
            if ([[responseObject objectForKey:@"restaurantImages"] count]>0) {
                restaurantPhotoArray = [NSMutableArray array];
                NSArray *restaurantPhotoArray_ = [responseObject objectForKey:@"restaurantImages"];
                [restaurantPhotoArray removeAllObjects];
                for(int i=0; i<restaurantPhotoArray_.count; i++){
                    IMGDish *dish = [[IMGDish alloc] init];
                    dish.imageUrl =  restaurantPhotoArray_[i][1];
                    dish.dishId = restaurantPhotoArray_[i][0];
                    dish.restaurantId = [restaurantPhotoArray_[i][2] objectForKey:@"restaurantId"];
                    
                    [dish updatePhotoCredit:restaurantPhotoArray_[i][2]];
                    
                    
                    [restaurantPhotoArray addObject:dish];
                }
                
                
                [self addRestaurantPhotos];
                
            }
            NSArray *dishArray = [responseObject objectForKey:@"dishList"];
            NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            NSDate *createTime=[[NSDate alloc] init];
            [formatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
            for(int i=0; i<dishArray.count; i++){
                NSDictionary *tempDic = [dishArray objectAtIndex:i];
                NSNumber *type=[ tempDic objectForKey:@"type"];
                
                IMGDish *dish = [[IMGDish alloc] init];
                if ([type intValue]==1) {
                    NSDictionary *dishDictionary = [tempDic objectForKey:@"dish"];
                    [dish setValuesForKeysWithDictionary:dishDictionary];
                    dish.userName=[dishDictionary objectForKey:@"creator"];
                    createTime=[formatter dateFromString:[dishDictionary objectForKey:@"createTime"]];
                    dish.createTime=[NSNumber numberWithDouble:createTime.timeIntervalSince1970];
                    dish.descriptionStr = [dishDictionary objectForKey:@"description"];
                    [dish updatePhotoCredit:[dishDictionary objectForKey:@"photoCredit"]];
                    [userPhotoArray addObject:dish];
                }else if([type intValue]==2){
                    NSDictionary *dishDictionary = [tempDic objectForKey:@"instagramPhoto"];
                    if (![dishDictionary isKindOfClass:[NSNull class]]) {
                        [dish setValuesForKeysWithDictionary:dishDictionary];
                        dish.userName=[dishDictionary objectForKey:@"instagramUserName"];
                        NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[[dishDictionary objectForKey:@"approvedDate"] longLongValue]/1000];
                        NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
                        
                        createTime=[formatter dateFromString:confromTimespStr];
                        dish.createTime=[NSNumber numberWithDouble:createTime.timeIntervalSince1970];
                        dish.imageUrl = [dishDictionary objectForKey:@"standardResolutionImage"];
                        
                        [userPhotoArray addObject:dish];

                    }
                    
                }
            }
                


            if (dishArray.count > 0)
            {
//                NSArray *descArr = [[userPhotoArray reverseObjectEnumerator] allObjects];
//                userPhotoArray = [[NSMutableArray alloc] initWithArray:descArr];
                [self changePhotoNum];
                for (UIView *view in self.mScrollView.subviews)
                {
                    if (view.tag==1800 || view.tag==1801 || ([view isKindOfClass:[UIImageView class]] && view.tag>=restaurantPhotoArray.count))
                    {
                        [view removeFromSuperview];
                    }
                }
                
                
            }

            if ([dishcount intValue]>userPhotoArray.count) {
                offset = [NSNumber numberWithInt:([offset intValue]+50)];
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    // Do the work in background
                    [self getNextPageDishesFromServer:restaurantId max:max];
                });
            }else{
                [self addImages];

            }

        } failure:^(NSURLSessionDataTask *operation, NSError *error) {
            NSLog(@"getDishesFromServer erors when get datas:%@",error.localizedDescription);
            
        }];
//    }
    
    
}
-(void)getNextPageDishesFromServer:(int)restaurantId max:(int)max{
    [[DBManager manager] deleteWithSql:[NSString stringWithFormat:@"delete from IMGDish where restaurantId = '%@';",restaurant.restaurantId]];
    
    NSDictionary *parameters=@{@"restaurantId":[NSNumber numberWithInt:restaurantId],@"max":@(50),@"offset":offset};
    
    [[IMGNetWork sharedManager] GET:@"app/detail/photos/v2" parameters:parameters success:^(NSURLSessionDataTask *operation, id responseObject) {
        
        dishcount = [responseObject objectForKey:@"dishCount"];
        NSArray *dishArray = [responseObject objectForKey:@"dishList"];
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        NSDate *createTime=[[NSDate alloc] init];
        [formatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
        for(int i=0; i<dishArray.count; i++){
            NSDictionary *tempDic = [dishArray objectAtIndex:i];
            NSNumber *type=[ tempDic objectForKey:@"type"];
            
            IMGDish *dish = [[IMGDish alloc] init];
            if ([type intValue]==1) {
                NSDictionary *dishDictionary = [tempDic objectForKey:@"dish"];
                [dish setValuesForKeysWithDictionary:dishDictionary];
                dish.userName=[dishDictionary objectForKey:@"creator"];
                createTime=[formatter dateFromString:[dishDictionary objectForKey:@"createTime"]];
                dish.createTime=[NSNumber numberWithDouble:createTime.timeIntervalSince1970];
                dish.descriptionStr = [dishDictionary objectForKey:@"description"];
                [dish updatePhotoCredit:[dishDictionary objectForKey:@"photoCredit"]];
                [userPhotoArray addObject:dish];
            }else if([type intValue]==2){
                NSDictionary *dishDictionary = [tempDic objectForKey:@"instagramPhoto"];
                if (![dishDictionary isKindOfClass:[NSNull class]]) {
                    [dish setValuesForKeysWithDictionary:dishDictionary];
                    dish.userName=[dishDictionary objectForKey:@"instagramUserName"];
                    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[[dishDictionary objectForKey:@"approvedDate"] longLongValue]/1000];
                    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
                    
                    createTime=[formatter dateFromString:confromTimespStr];
                    dish.createTime=[NSNumber numberWithDouble:createTime.timeIntervalSince1970];
                    dish.imageUrl = [dishDictionary objectForKey:@"standardResolutionImage"];
                    
                    [userPhotoArray addObject:dish];
                    
                }
                
            }
        
        }
        
        if ([dishcount intValue]>userPhotoArray.count) {
            offset = [NSNumber numberWithInt:([offset intValue]+50)];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                // Do the work in background
                [self getNextPageDishesFromServer:restaurantId max:max];
            });
        }else{
            [self addImages];

        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"getDishesFromServer erors when get datas:%@",error.localizedDescription);
        
    }];

}
- (void)changePhotoNum
{
//    if(photoType == 2){
//        NSInteger photoNum=eventPhotoArray.count;
//        numLabel.text = [NSString stringWithFormat:@"%ld %@",(long)photoNum,(photoNum>1)?@"photos":@"photo"];
//        
////        NSMutableDictionary *changeNumDic = [[NSMutableDictionary alloc] init];
////        [changeNumDic setValue:[NSNumber numberWithInt:photoNum] forKey:@"discount"];
//    }else{
        numLabel.text = [NSString stringWithFormat:@"%@ %@",dishcount,([dishcount intValue]>1)?@"photos":@"photo"];

//    }

}
-(NSNumber *)getDishCount{
    NSNumber *dishCount = [NSNumber numberWithInt:0];
    FMResultSet *resultSet = [[DBManager manager]executeQuery:[NSString stringWithFormat:@"select count(1) as dishCount from IMGDish where restaurantId=%@",restaurant.restaurantId]];
    if([resultSet next]){
        dishCount = [resultSet objectForColumnName:@"dishCount"];
    }
    [resultSet close];
    return dishCount;
}
 


@end
