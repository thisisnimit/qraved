//
//  LatestReviewViewController.h
//  Qraved
//
//  Created by Shine Wang on 8/2/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "BaseAnimationController.h"
#import "ReviewView.h"
#import "IMGRestaurant.h"
#import "UserReviewView.h"
#import "UIPopoverListView.h"

@interface UserAllReviewController : BaseAnimationController<reviewDelegate,UITableViewDelegate,UITableViewDataSource,userReviewDelegate,UIPopoverListViewDataSource,UIPopoverListViewDelegate>
@property(nonatomic,retain) NSString *rating;  //Over all
@property(nonatomic,retain) NSString *foodScore;
@property(nonatomic,retain) NSString *serviceScore;
@property(nonatomic,retain) NSString *ambianceScore;
@property(nonatomic,retain) NSNumber *restaurantId;
@property(nonatomic,retain) IMGRestaurant *restaurant;
@property(nonatomic,retain) NSString *restaurantTitle;
@property(nonatomic,assign) NSUInteger page;
@property(nonatomic,assign) NSUInteger limit;
//@property(nonatomic,assign) BOOL hasReview;
@property(nonatomic,retain) NSString *amplitudeType;
-(id)initWithRestaurant:(IMGRestaurant*)restaurant and:(IMGReview*)review;
@end
