//
//  MeunPhotoViewController.m
//  Qraved
//
//  Created by Evan on 16/4/5.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "MenuPhotoViewColltroller.h"

#import "EntityKind.h"
#import "UIConstants.h"
#import "EScrollerView.h"
#import "NSString+Helper.h"
#import "Date.h"
#import "UIView+Helper.h"
#import "IMGMenu.h"
#import "IMGEvent.h"
#import "MenuPhotoHandler.h"
#import "DetailViewController.h"
#import "MenuPhotoActivityItemProvider.h"
#import "TwitterActivityItemProvider.h"
#import "UIImageView+WebCache.h"
#import "MenuPhotosEScrollerView.h"
#import "UILabel+Helper.h"
#import "IMGUser.h"
#import "OtherProfileViewController.h"

@interface MenuPhotoViewColltroller ()<EScrollerViewDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate>
{
    UILabel *resTitleLbl;
    IMGMenu *menu;


}
//点击照片的tag值
@property (nonatomic , assign) long photoTag;
//用户当前进入的Restaurant
@property (nonatomic , strong) IMGRestaurant *currentRestaurant;
//照片集合数组
@property (nonatomic , strong) NSMutableArray *photoArray;

@property (nonatomic , assign) IMGType imgType;

@property (nonatomic , strong) UIView *headerView;

@property (nonatomic , strong) UIView *footerView;

@property (nonatomic , strong) UILabel *photoNumLbl;

@property (nonatomic , strong) UILabel *MenuTypeLbl;

@property (nonatomic , strong) UILabel *createLbl;

@property (nonatomic , assign) BOOL isTouch;

@property (nonatomic , assign) BOOL showPage;

@property (nonatomic , assign) BOOL isFromDetail;

@property (nonatomic , strong) MenuPhotosEScrollerView *escrollView;

@property (nonatomic , strong) NSNumber *offset;

@property (nonatomic, strong) IMGMenu *menuMax;

@property (nonatomic , assign) int serveCout;

@property (nonatomic , strong) NSNumber *menuID;

@property (nonatomic , strong) NSMutableDictionary *photoArrayStatus;

@property (nonatomic , assign) BOOL isHiddenInfo;

@property (nonatomic , assign) BOOL isHiddenFooter;

@property (nonatomic , assign) long validPhotosArrayCount;

@property (nonatomic , assign) BOOL ifFromRestaurantDetail;

@end

@implementation MenuPhotoViewColltroller

//初始化方法
-(id)initWithPhotosArray:(NSMutableArray *)photos andPhotoTag:(NSInteger)photoTag andRestaurant:(IMGRestaurant *)restaurant andFromDetail:(BOOL)isDetail {
    
    self = [super init];
    if (self) {
        
        //赋值
        self.photoArray = [photos mutableCopy];
        
        self.currentRestaurant = restaurant;
        
        self.photoTag = photoTag;
        
        self.showPage = NO;
        
        self.isFromDetail = isDetail;
        
        self.imgType = [EntityKind typeKindFromEntity:[self.photoArray firstObject]];
        
    }
    
    return self;
}

-(void)updateRestautantDetailData {
    self.ifFromRestaurantDetail = YES;
    if (self.ifFromRestaurantDetail) {
        [self addNullObject];
    }
}

-(void)addNullObject{
    int addNullCount = 0;
    self.validPhotosArrayCount = self.photoArray.count;
    self.menuPhotoCount = self.currentRestaurant.menuCount;
    addNullCount = [self.menuPhotoCount intValue] - (int)self.photoArray.count;
    if (addNullCount > 0) {
        IMGMenu *nullMenu= [[IMGMenu alloc] init];
        nullMenu.entityId = [NSNumber numberWithInt:-1];
        for (int i = 0; i < addNullCount; i++) {
            [self.photoArray addObject:nullMenu];
        }
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden =  YES;
    if (self.amplitudeType) {
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:[self getPageIdByAmplitudeType] forKey:@"Origin"];
        [eventProperties setValue:_currentRestaurant.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:self.menuID forKey:@"RestaurantMenu_ID"];
        [[Amplitude instance] logEvent:@"RC - View Menu Card detail" withEventProperties:eventProperties];
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.navigationController.navigationBarHidden=YES;

}
- (void)viewDidLoad {
    self.offset = [NSNumber numberWithInt:0];
    
    [super viewDidLoad];
    self.navigationController.navigationBarHidden =  YES;
    self.screenName = @"Restaurant Menu detail page";
    //背景色全黑
    self.view.backgroundColor = [UIColor blackColor];
    
    if (self.ifFromJournal) {
        self.photoArray = [[NSMutableArray alloc]init];
        [self getDataFromService];
    }else{
        [self addScrollView];
        [self addHeaderView];
        [self addFooterView];
        [self EScrollerViewDidClicked:self.photoTag+1 ContentOffset:CGPointMake(0, 0)];
    }
    
    self.photoArrayStatus = [[NSMutableDictionary alloc] init];
    
    
    
}

-(void) getDataFromService {
    
    if (self.ifUnauditedData)
    {
        IMGUser *user = [IMGUser currentUser];
        NSDictionary *parameters=@{@"restaurantId":self.currentRestaurant.restaurantId,@"offset":self.offset,@"max":@10,@"userId":user.userId};

        [MenuPhotoHandler getUserUploadMenuDataFromService:parameters andBlock:^(NSMutableArray *menuList, NSNumber *offset, NSMutableArray *menuPhotoArray, NSNumber *menuPhotoCount, NSNumber *menuPhotoCountTypeRestant, NSNumber *menuPhotoCountTypeBar, NSNumber *menuPhotoCountTypeDelivery, IMGMenu *menu_) {
            self.photoArray = menuPhotoArray;
            
            self.menuPhotoCount = menuPhotoCount;
            
            self.menuID = menu_.menuId;
            
            //添加图片轮播
            [self addScrollView];
            //添加顶部试图
            [self addHeaderView];
            //添加底部视图
            [self addFooterView];
            
            [self EScrollerViewDidClicked:self.photoTag+1 ContentOffset:CGPointMake(0, 0)];

        }];
    }
    else
    {
        [MenuPhotoHandler getMenuGalleryDataFromService:self.currentRestaurant.restaurantId offset:self.offset andBlock:^(NSMutableArray *menuList,NSNumber *offset,NSMutableArray *menuPhotoArray, NSNumber *menuPhotoCount, NSNumber *menuPhotoCountTypeRestant, NSNumber *menuPhotoCountTypeBar, NSNumber *menuPhotoCountTypeDelivery, IMGMenu *menu_,NSNumber *offsetBlock) {
            
            self.photoArray = menuPhotoArray;
            
            self.menuPhotoCount = menuPhotoCount;
            
            self.menuID = menu_.menuId;
            
            //添加图片轮播
            [self addScrollView];
            //添加顶部试图
            [self addHeaderView];
            //添加底部视图
            [self addFooterView];
            
            [self addNullObject];
            
            [self EScrollerViewDidClicked:self.photoTag+1 ContentOffset:CGPointMake(0, 0)];
            
            
        }];
        

    }
    
    
}


- (void)addHeaderView {
    
    CGFloat margin = 20;
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 150)];
    headerView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:headerView];
    
    CGFloat closeBtnW = 30;
    CGFloat closeBtnH = closeBtnW;
    UIButton *closeBtn = [[UIButton alloc]initWithFrame:CGRectMake(DeviceWidth-closeBtnW, IMG_StatusBarHeight-5, closeBtnW, closeBtnH)];
    [closeBtn setImage:[UIImage imageNamed:@"closeWhite"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    
    [headerView addSubview:closeBtn];
    
 
    resTitleLbl = [[UILabel alloc]init];
    resTitleLbl.font = [UIFont systemFontOfSize:15];
    [headerView addSubview:resTitleLbl];
    resTitleLbl.frame = CGRectMake(closeBtnW+margin, 20+TOPOFFSET,DeviceWidth-2*margin-2*closeBtnW, 20);
//    resTitleLbl.text = self.currentRestaurant.title;
    resTitleLbl.numberOfLines = 0;
    resTitleLbl.textAlignment = NSTextAlignmentCenter;
    resTitleLbl.textColor = [UIColor whiteColor];
//    [resTitleLbl sizeToFit];
//    resTitleLbl.frame = CGRectMake((DeviceWidth - (resTitleLbl.frame.size.width))/2, 20+TOPOFFSET,resTitleLbl.frame.size.width, resTitleLbl.frame.size.height);
    
    UITapGestureRecognizer *resTitleTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gotoResDetailController)];
    self.headerView.userInteractionEnabled = YES;
    resTitleLbl.userInteractionEnabled = YES;
    resTitleTap.numberOfTapsRequired = 1;
    [resTitleLbl addGestureRecognizer:resTitleTap];
    
    
    UILabel *photoNumLbl = [[UILabel alloc]init];
    [headerView addSubview:photoNumLbl];
    photoNumLbl.frame = CGRectMake(margin+closeBtnW,20+TOPOFFSET+23,DeviceWidth-2*margin-2*closeBtnW, 15);
    photoNumLbl.font = [UIFont systemFontOfSize:12];
    photoNumLbl.textAlignment = NSTextAlignmentCenter;
    
    photoNumLbl.text = [NSString stringWithFormat:@"%ld of %ld %@",self.photoTag+1,[self.menuPhotoCount longValue],(([self.menuPhotoCount longValue]>1)?@"photos":@"photo")];
    photoNumLbl.textColor = [UIColor whiteColor];
    
    self.photoNumLbl = photoNumLbl;
    
    
    
    //Menu type
    UILabel *MenuTypeLbl = [[UILabel alloc]init];
    [headerView addSubview:MenuTypeLbl];
    MenuTypeLbl.frame = CGRectMake(margin, photoNumLbl.endPointY, DeviceWidth-2*margin, 20);
    MenuTypeLbl.font = [UIFont systemFontOfSize:12];
    //    MenuTypeLbl.text = @"Menu Type";
    MenuTypeLbl.textColor = [UIColor whiteColor];
    self.MenuTypeLbl = MenuTypeLbl;
    
    UILabel *createLbl = [[UILabel alloc]init];
    [headerView addSubview:createLbl];
    createLbl.frame = CGRectMake(margin, MenuTypeLbl.endPointY,DeviceWidth-2*margin, 15);
    createLbl.font=[UIFont systemFontOfSize:10];
    //createLbl.text = @"mm dd yyyy";
    createLbl.textColor = [UIColor whiteColor];
    self.createLbl = createLbl;
    
    self.headerView = headerView;
    
    CGRect headerViewFrame = headerView.frame;
    headerViewFrame.size.height = createLbl.endPointY;
    headerView.frame = headerViewFrame;
}

- (void)goFoodButtonTapped{
    [IMGAmplitudeUtil trackGoFoodWithName:@"CL - Go-Food CTA" andJournalId:nil andRestaurantId:self.currentRestaurant.restaurantId andPhotoId:self.menuID andLocation:@"Menu Photo Viewer" andOrigin:nil andOrder:nil];
    [CommonMethod goFoodButtonTapped:self.currentRestaurant andViewController:self andCallBackBlock:^(BOOL isSaved) {
        
    }];
}

- (void)addFooterView {
    
    if (![Tools isBlankString:self.currentRestaurant.goFoodLink]) {
        FunctionButton *goFoodButton = [FunctionButton buttonWithType:UIButtonTypeCustom];
        goFoodButton.frame = CGRectMake(LEFTLEFTSET, DeviceHEIGHT - FUNCTION_BUTTON_HEIGHT - 44 - 15, DeviceWidth - LEFTLEFTSET*2, FUNCTION_BUTTON_HEIGHT);
        [goFoodButton addTarget:self action:@selector(goFoodButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:goFoodButton];
    }
    
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, DeviceHeight-IMG_TabbarSafeBottomMargin-24, DeviceWidth, 44)];
    [self.view addSubview:footerView];
    footerView.backgroundColor = [UIColor clearColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth/3*2+30, 15, 13.5, 13.5)];
    imageView.image = [UIImage imageNamed:@"ic_share_white"];
    imageView.userInteractionEnabled = YES;
    [footerView addSubview:imageView];
    
    UITapGestureRecognizer *tapImageView=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shareButtonClick:)];
    tapImageView.numberOfTapsRequired=1;
    tapImageView.numberOfTouchesRequired=1;
    [imageView addGestureRecognizer:tapImageView];
    
    UILabel *shareLabel = [[UILabel alloc] initWithFrame:CGRectMake(imageView.endPointX+7, imageView.frame.origin.y, 50, imageView.frame.size.height)];
    shareLabel.userInteractionEnabled = YES;
    [shareLabel setText:@"Share"];
    [shareLabel setTextColor:[UIColor whiteColor]];
    shareLabel.textAlignment = NSTextAlignmentLeft;
    shareLabel.font = [UIFont systemFontOfSize:12];
    [footerView addSubview:shareLabel];
    
    UITapGestureRecognizer *tapShareLabel=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shareButtonClick:)];
    tapShareLabel.numberOfTapsRequired=1;
    tapShareLabel.numberOfTouchesRequired=1;
    [shareLabel addGestureRecognizer:tapShareLabel];
    
    self.footerView = footerView;
}
//添加图片轮播
- (void)addScrollView {
    
    MenuPhotosEScrollerView *scrollView = [[MenuPhotosEScrollerView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+18) imagesArray:self.photoArray index:self.photoTag  isMenuPhotoView:YES];
    scrollView.isUseWebP = self.isUseWebP;
    scrollView.userInteractionEnabled=YES;
    scrollView.escrollerViewDelegate = self;
    scrollView.ifCanNotFromBeginToEnd = self.ifFromRestaurantDetail;
    scrollView.ifCanNotFromBeginToEnd = self.ifFromJournal;
    if (@available(iOS 11.0, *)) {
        scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    [self.view addSubview:scrollView];
    
    self.escrollView = scrollView;
    
    
}

//图片滑动方法
-(void)EScrollerViewDidClicked:(NSUInteger)index ContentOffset:(CGPoint)contentoffset{
    
    [IMGAmplitudeUtil trackPhotoWithName:@"RC - View Restaurant Menu detail" andRestaurantId:self.currentRestaurant.restaurantId andRestaurantTitle:nil andOrigin:self.amplitudeType andUploaderUser_ID:nil andPhoto_ID:nil andRestaurantMenu_ID:self.menuID andRestaurantEvent_ID:nil andPhotoSource:nil andLocation:nil];
    
    NSLog(@"index = %lu",(unsigned long)index);
    
    self.photoNumLbl.text = [NSString stringWithFormat:@"%lu of %ld %@",(unsigned long)index,[self.menuPhotoCount longValue],(([self.menuPhotoCount longValue]>1)?@"photos":@"photo")];
    
    menu = [self.photoArray objectAtIndex:index - 1];
    
    self.menuID = menu.menuId;
    NSString *titlle=@"";
    NSString *sourceType=menu.sourceType;
    if ([sourceType isEqualToString:@"Restaurant"]) {
        titlle=menu.resSourceTitle;
    }else if ([sourceType isEqualToString:@"URL"]){
        titlle=menu.sourceValue;
    
    }else if ([sourceType isEqualToString:@"User ID"]){
        
        titlle=menu.sourceUserName;
    }else if ([sourceType isEqualToString:@"Qraved Team"]){
        titlle= self.currentRestaurant.title;
    }
    resTitleLbl.text=[titlle removeHTML];
    if([NSString stringWithFormat:@"%lld",menu.createTimeLong].length<11){
        menu.createTimeLong = menu.createTimeLong*1000;
    
    }
    
    if (menu.createTimeLong/1000) {
        
        self.createLbl.text = [Date getTimeInteval_v5:menu.createTimeLong/1000];
    }
    
    self.footerView.hidden = NO;
    self.isHiddenFooter = NO;
    NSNumber *type = menu.type;
    if([type intValue] == 0 ){
        self.footerView.hidden = YES;
        self.isHiddenFooter = YES;
    }else if([type intValue] == 1 ) {
        self.MenuTypeLbl.text = @"Restaurant Menu";
    }else if([type intValue] == 2 ) {
        self.MenuTypeLbl.text = @"Bar Menu";
        
    }else if([type intValue] == 3 ) {
        self.MenuTypeLbl.text = @"Delivery Menu";
        
    }
    if(self.isHiddenInfo) {
        self.headerView.hidden = YES;
        if (!self.isHiddenFooter) {
            self.footerView.hidden = YES;
        }
    }else{
        self.headerView.hidden = NO;
        if (!self.isHiddenFooter) {
            self.footerView.hidden = NO;
        }
    }
    if ( (![self.photoArrayStatus objectForKey:@(index)]) && (self.ifFromRestaurantDetail || self.ifFromJournal)) {
        //每当滑动到5的倍数时请求10条数据
        if (index %5 == 0) {
            
            [self.photoArrayStatus setObject:@(YES) forKey:@(index)];
            
            if ( (self.menuPhotoCount ==nil) || (self.validPhotosArrayCount <[self.menuPhotoCount longValue]) ) {
                self.offset = [NSNumber numberWithInt:([self.offset intValue]+10)];
                [MenuPhotoHandler getMenuGalleryDataFromService:self.currentRestaurant.restaurantId offset:self.offset andBlock:^(NSMutableArray *menuList,NSNumber *offset,NSMutableArray *menuPhotoArray, NSNumber *menuPhotoCount, NSNumber *menuPhotoCountTypeRestant, NSNumber *menuPhotoCountTypeBar, NSNumber *menuPhotoCountTypeDelivery, IMGMenu *menu_,NSNumber *offsetBlock) {
                    
                    if (!menuList.count) {
                        return ;
                    }
                    self.menuPhotoCount = menuPhotoCount;
                    self.validPhotosArrayCount += menuPhotoArray.count;
                    for (long i = 0; i < menuPhotoArray.count; i++) {
                        [self.photoArray replaceObjectAtIndex:([offsetBlock intValue] + i) withObject:[menuPhotoArray objectAtIndex:i]];
                    }
                    [self.escrollView updateImagesArray:self.photoArray];
                    
                }];
            }
            
            
        }
    }
   
    
}

//单击图片
-(void)EScrollerViewDidClicked:(NSUInteger)index{
    if (self.isHiddenInfo) {
        [UIView animateWithDuration:0.3 animations:^{
            self.isHiddenInfo = NO;
//            self.headerView.hidden = NO;
            if (!self.isHiddenFooter) {
                self.footerView.hidden = NO;
            }
        }];
    }else{
        [UIView animateWithDuration:0.3 animations:^{
            self.isHiddenInfo = YES;
//            self.headerView.hidden = YES;
            if (!self.isHiddenFooter) {
                self.footerView.hidden = YES;
            }
        }];
    }
}

//双击图片
-(void)EScrollerViewDidDoubleClicked:(NSUInteger)index{
    
}


//分享按钮放
-(void) shareButtonClick:(UIGestureRecognizer*)gesture {
    UIView* view=gesture.view;
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/menu/%@",QRAVED_WEB_SERVER_OLD,self.menuID]];
    NSString *shareContentString = [NSString stringWithFormat:@"%@ on Qraved. %@",self.currentRestaurant.title,url];
    MenuPhotoActivityItemProvider *provider = [[MenuPhotoActivityItemProvider alloc]initWithPlaceholderItem:shareContentString];
    
    provider.url=url;
    provider.title=shareContentString;
    provider.restaurantTitle= self.currentRestaurant.title;
    provider.districtName = self.currentRestaurant.districtName;
    
    TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
    twitterActivityItemProvider.title = [NSString stringWithFormat:@"Check out this menu at %@ on Qraved",self.currentRestaurant.title];
    
    UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
    
    activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
    [activityCtl setValue:[NSString stringWithFormat:L(@"Let’s find out great menu on Qraved"),self.currentRestaurant.title] forKey:@"subject"];
    activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
        
    };
    
    if ([activityCtl respondsToSelector:@selector(popoverPresentationController)]) {
        
        activityCtl.popoverPresentationController.sourceView = view;
        
    }

    [self presentViewController:activityCtl animated:YES completion:^{
        
    }];
}


//点击饭店名跳转餐馆详细页
- (void) gotoResDetailController {
    if ([menu.sourceType isEqualToString:@"Restaurant"]) {
        NSNumber *resturantId=[NSNumber numberWithLong:[menu.sourceValue longLongValue]];
        DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurantId:resturantId];
        
        [self.navigationController pushViewController:detailViewController animated:YES];
        
        
        
    }else if ([menu.sourceType isEqualToString:@"URL"]){
        
        //        NSURLRequest *request=[NSURLRequest requestWithURL:[NSURL URLWithString:menu.sourceUrl]];

        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:menu.sourceUrl]];
    }else if ([menu.sourceType isEqualToString:@"User ID"]){
        
        IMGUser *user = [[IMGUser alloc] init];
        user.userId = [NSNumber numberWithLong:[menu.sourceValue longLongValue]];
        user.userName =menu.sourceUserName;
        user.token = @"";
        //        user.avatar = review.avatar;
        OtherProfileViewController *opvc = [[OtherProfileViewController alloc] init];
        opvc.isOtherUser=YES;
        opvc.otherUser=user;
        [self.navigationController pushViewController:opvc animated:YES];
        
    }else if ([menu.sourceType isEqualToString:@"Qraved Team"]){
        
        DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurantId:self.currentRestaurant.restaurantId];
        detailViewController.amplitudeType = @"Card detail page";
        [self.navigationController pushViewController:detailViewController animated:YES];
        
    }
    

    
}



//返回按钮
- (void) backClick {
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSString*)getPageIdByAmplitudeType{
    NSString *pageId = @"";
    if ([@"Menu photo card detail page" isEqualToString:self.amplitudeType]) {
        pageId = @",Restaurant detail page";
    }
    return [NSString stringWithFormat:@"%@%@",self.amplitudeType,pageId];
}

@end
