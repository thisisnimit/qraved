//
//  MoreArticleViewController.m
//  Qraved
//
//  Created by Laura on 15/2/16.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "MoreArticleViewController.h"
#import "UIViewController+Helper.h"
#import "IMGMediaComment.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import <CoreText/CoreText.h>
#import "WebViewController.h"
#import "RestaurantJournalView.h"
#import "DetailDataHandler.h"
#import "JournalListViewController.h"
#import "JournalDetailViewController.h"
#import "JournalHandler.h"

@implementation MoreArticleViewController{
    int offset;
    int max;
    BOOL hasData;
    RestaurantJournalView *journalView;
    NSMutableArray *restaurantJournals;
}

-(instancetype)initWithRestaurant:(NSNumber*)restaurantId{
    if(self=[super init]){
        self.restaurantId=restaurantId;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.title = self.title;
    [self setBackBarButtonOffset30];
    offset=0;
    max=10;
    hasData=YES;
    restaurantJournals=[[NSMutableArray alloc] init];
    journalView=[[RestaurantJournalView alloc] initWithFrame:CGRectMake(0,10,DeviceWidth,DeviceHeight-44-10) withRestaurantId:self.restaurantId];
    journalView.delegate=self;
    [self.view addSubview:journalView];
    [self loadRestaurantJournals:^(BOOL hasData_){
        journalView.hasData=hasData_;
    }];
    
    [IMGAmplitudeUtil trackJournalListWithName:@"RC - View Journal list" andSorting:nil andFilter:nil andOrigin:@"RDP"];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}
-(void)gotoJournal:(IMGMediaComment*)journal inView:(UIView*)view{
    
    [IMGAmplitudeUtil trackJDPWithName:@"RC - View Journal detail" andJournalId:journal.mediaCommentId andOrigin:@"JLP" andChannel:nil andLocation:nil];
    
    view.userInteractionEnabled=NO;
    [JournalHandler getJournalDetailTypeArticleIdWithJournalId:journal.mediaCommentId andJournalBlock:^(IMGMediaComment *journal) {
        if(journal==nil){
            JournalListViewController *JVC = [[JournalListViewController alloc] init];
            JVC.amplitudeType = self.amplitudeType;
            [self.navigationController pushViewController:JVC animated:YES];
            NSLog(@"jump to app");
        }else if ([journal.imported intValue]== 1){
            [JournalHandler getJournalDetailWithJournalArticleId:journal.mediaCommentId andBlock:^(NSString *content,NSString *webSite) {
                journal.contents = content;
                WebViewController *wvc = [[WebViewController alloc] init];
                wvc.content = journal.contents;
                wvc.journal = journal;
                wvc.webSite = webSite;
                wvc.title = journal.title;
                wvc.fromDetail = NO;
                [self.navigationController pushViewController:wvc animated:YES];
            }];
        }else{
            JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
            jdvc.journal = journal;
            [self.navigationController pushViewController:jdvc animated:YES];
        }
        view.userInteractionEnabled=YES;
    }];
}

-(void)loadRestaurantJournals:(void(^)(BOOL))block{
    // __block __weak DiningGuideRestaurantsViewController *weakSelf=self;
    if(hasData){
        journalView.isLoading=YES;
        [DetailDataHandler getRestaurantJournal:@{@"restaurantId":self.restaurantId,@"offset":[NSNumber numberWithInt:offset],@"max":[NSNumber numberWithInt:max]} block:^(NSNumber *total,NSArray *journals){
            journalView.isLoading=NO;
            [self callback:total journals:journals block:block];
        } failure:^(NSString *description){
            journalView.isLoading=NO;
        }];
    }
}

-(void)callback:(NSNumber*)total journals:(NSArray*)journals block:(void(^)(BOOL))block{
    offset+=max;

    [restaurantJournals addObjectsFromArray:journals];
    journalView.journals=restaurantJournals;
    hasData=offset>[total intValue]?NO:YES;
    if(hasData){
        [journalView setRefreshViewFrame];
        journalView.refreshView.hidden=NO;
    }
    if(!hasData && journalView.refreshView){
        journalView.refreshView.hidden=YES;
        // [journalView.refreshView removeFromSuperview];
    }
    block(hasData);
}

 
@end
