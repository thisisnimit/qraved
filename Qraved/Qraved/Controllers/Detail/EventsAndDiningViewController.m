//
//  EventsAndDiningViewController.m
//  Qraved
//
//  Created by Lucky on 15/6/10.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "EventsAndDiningViewController.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "IMGDiningGuide.h"
#import "IMGEvent.h"
#import "NSString+Helper.h"
#import "UIConstants.h"
#import "UIImage+Resize.h"
#import "UIImageView+WebCache.h"
#import "Label.h"
#import "DBManager.h"
#import "DiningGuideRestaurantsViewController.h"
#import "RestaurantsViewController.h"
#import "UIDevice+Util.h"
#import "DetailDataHandler.h"
#import "LoadingView.h"
#import "IMGDiscover.h"
#import "EventsAndDiningGuideView.h"

@interface EventsAndDiningViewController ()
{
    NSArray *_diningGuideArray;
    NSArray *_eventArray;
    NSString *_title;
    UIView *navigationView;

}
@end

@implementation EventsAndDiningViewController

#define navigationHeight (44+[UIDevice heightDifference])

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"Restaurant Event list page";
    self.view.backgroundColor = [UIColor whiteColor];
    [self addBackButton];
    self.view.backgroundColor = [UIColor whiteColor];
    [[LoadingView sharedLoadingView] startLoading];
    EventsAndDiningGuideView *eventDiningView=[[EventsAndDiningGuideView alloc] initWithFrame:CGRectMake(0,0,DeviceWidth,DeviceHeight)];
    eventDiningView.delegate=self;
    [self.view addSubview:eventDiningView];
    [DetailDataHandler getEventAndDinningGuideFromService:self.restaurantId andBlock:^(NSArray *eventList, NSArray *dinningGuideList) {
        eventDiningView.events=[NSMutableArray arrayWithArray:eventList];
        eventDiningView.diningGuides=[NSMutableArray arrayWithArray:dinningGuideList];
        [eventDiningView.tableView reloadData];
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}
- (instancetype)initWithDiningGuideArr:(NSArray *)diningGuideArray andRestaurantName:(NSString *)name
{
    self = [super init];
    if (self) {
        _diningGuideArray = [[NSArray alloc] initWithArray:diningGuideArray];
        _title = [[NSString alloc] initWithString:name];
    }
    return self;
}

-(void)addBackButton
{
    self.view.backgroundColor=[UIColor clearColor];
    [self addBackBtn];
    self.navigationItem.title=_title;
}


-(void)loadEventAndDinningGuide:(void(^)(BOOL))block{
    [[LoadingView sharedLoadingView] startLoading];
    // __weak MyListAddRestaurantController *weakSelf=self;
//    if(hasData){
//        
//    }
}


-(void)goToEvent:(IMGEvent*)event{
    IMGDiscover *discover=[[IMGDiscover alloc]init];
    discover.sortby = SORTBY_POPULARITY;
    if(event!=nil && [event.eventId intValue]>0){
        discover.eventArray = [[NSMutableArray alloc]initWithCapacity:0];
        IMGOfferType *offerType = [[IMGOfferType alloc]init];
        offerType.offerId = event.eventId;
        offerType.eventType=[NSNumber numberWithInt:1];
        offerType.typeId = [NSNumber numberWithInt:4];
        offerType.off = [NSNumber numberWithInt:0];
        offerType.name = event.name;
        [discover.eventArray addObject:offerType];
    }
    discover.fromWhere=@9;
    RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc]initWithEvent:event];
    restaurantsViewController.amplitudeType = @"Events and Dining Guides";
    [restaurantsViewController initDiscover:discover];
    [self.navigationController pushViewController:restaurantsViewController animated:YES];
}

-(void)goToDinningGuide:(IMGDiningGuide*)diningGuide{
    DiningGuideRestaurantsViewController *diningGuideRestaurantsViewController = [[DiningGuideRestaurantsViewController alloc] initWithDiningGuide:diningGuide];
    [self.navigationController pushViewController:diningGuideRestaurantsViewController animated:YES];
}

 
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;

}

@end
