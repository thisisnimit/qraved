//
//  SelectofferViewController.m
//  Qraved
//
//  Created by Laura on 14-8-4.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "SelectOfferViewController.h"
#import <CoreText/CoreText.h>

#import "UIConstants.h"

#import "NSString+Helper.h"
#import "UILabel+Helper.h"
#import "UIImageView+Helper.h"
#import "UIImageView+WebCache.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "UIImage+Helper.h"
#import "Label.h"
#import "UIViewController+Helper.h"

#import "DTAttributedLabel.h"
#import "SelectDinersViewController.h"
#import "CustomIOS7AlertView.h"

#import "DBManager.h"
#import "IMGRestaurantOffer.h"

@interface SelectOfferViewController ()
{
    UIScrollView *_scrollView;
    IMGRestaurant *restaurant;
}
@end

@implementation SelectOfferViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithRestaurant: (IMGRestaurant *)pRestaurant{
    self = [super init];
    if (self) {
        restaurant = pRestaurant;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = L(@"Select an offer");
    [super viewWillAppear:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setBackBarButtonOffset30];
    [self loadMianView];
    self.view.backgroundColor = [UIColor defaultColor];
}
-(void)loadMianView
{
    
    NSMutableArray *offerArray=[[NSMutableArray alloc]initWithCapacity:0];
    IMGRestaurantOffer  *restaurantOffer=[[IMGRestaurantOffer alloc]init];
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGRestaurantOffer WHERE restaurantId = '%@' ORDER BY offerType,offerSlotMaxDisc;",restaurant.restaurantId] successBlock:^(FMResultSet *resultSet) {
        if ([resultSet next]) {
            [restaurantOffer setValueWithResultSet:resultSet];
            [offerArray addObject:restaurantOffer];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"addMenuView error when get datas:%@",error.description);
    }];

    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    _scrollView.contentSize = CGSizeMake(DeviceWidth, DeviceHeight);
    [self.view addSubview:_scrollView];
    
   
    
//    NSMutableArray *restaurantOfferDetailList = [NSMutableArray arrayWithArray:self.restaurantInfo.restaurantOfferDetailList];
    NSMutableArray *restaurantOfferDetailList = [[NSMutableArray alloc]initWithObjects:@"1",@"2", nil];

    
    NSArray *offerTitleArray = @[@"Up to 50% off",@"Eat Jakarta 2014 Set Menu 200k"];
    NSArray *offerDescriptionArr = @[@"Get up to 50% by booking from Qraved Enjoy the easiest experience to dine in your favored restaurants.",@"We are participating in Eat Jakarta 2014, a 28 day eating celebration. Sepecial set menus priced at IDR100K, 200K and 300K to showcase their food."];
    
    
    CGFloat startYY = 0;
    
    CGFloat currectPointY=0;
    for (int i=0; i<restaurantOfferDetailList.count; i++) {
       

        
        NSString *offerTitle = [offerTitleArray objectAtIndex:i];
        
        NSString *offerDescription = [offerDescriptionArr objectAtIndex:i];
        
        UIView *offerView = [[UIView alloc]init ];
        offerView.backgroundColor = [UIColor whiteColor];
        [_scrollView addSubview:offerView];
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 7.5, 45, 45)];
        
        if(i==0){
            imageView.image = [UIImage imageNamed:@"BookFlowOfferTypeTag"];
        }else{
            imageView.image = [UIImage imageNamed:@"BookFlowOfferTypeTag"];
        }
        
        [offerView addSubview:imageView];
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(imageView.endPointX+LEFTLEFTSET, 7.5, DeviceWidth-imageView.endPointX-LEFTLEFTSET*2, 45)];
        titleLabel.numberOfLines = 0;
        titleLabel.font=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:16];
        titleLabel.textColor=i==0?[UIColor colorC2060A]:[UIColor blackColor];
        titleLabel.text = offerTitle;
        [offerView addSubview:titleLabel];
        
        CGSize desSize = [offerDescription sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] constrainedToSize:CGSizeMake(DeviceWidth-30, 200)];
        UILabel *desLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, 15+titleLabel.endPointY, DeviceWidth-2*15, desSize.height)];
        desLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
        desLabel.text = offerDescription;
        desLabel.numberOfLines = 0;
        [offerView addSubview:desLabel];
        
        currectPointY=desLabel.endPointY+15;
        
        if(i>0){
            
            NSMutableAttributedString *mabstring = [[NSMutableAttributedString alloc]initWithString:L(@"See Menu")];
            [mabstring addAttribute:(id)kCTUnderlineStyleAttributeName value:(id)[NSNumber numberWithInt:kCTUnderlineStyleDouble] range:NSMakeRange(0, 8)];
            [mabstring addAttribute:(id)kCTForegroundColorAttributeName value:(id)[UIColor colorC2060A].CGColor range:NSMakeRange(0, 8)];
            
            DTAttributedLabel *menuLabel = [[DTAttributedLabel alloc]initWithFrame:CGRectMake(0, 0, 60, 15) ];
            [menuLabel setLineBreakMode:NSLineBreakByWordWrapping];
            menuLabel.numberOfLines = 0;
            [menuLabel setAttributedString:mabstring];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(seeMenuButtonClick)];
            [menuLabel addGestureRecognizer:tap];
            
            
            UIButton *MenuButton = [[UIButton alloc]initWithFrame:CGRectMake(15, currectPointY, 60, 15)];
            [MenuButton addTarget:self action:@selector(seeMenuButtonClick) forControlEvents:UIControlEventTouchUpInside];
            [MenuButton addSubview:menuLabel];
            [offerView addSubview:MenuButton];
            
            currectPointY=MenuButton.endPointY+10;
        }
        
        UILabel *termsLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, currectPointY, DeviceWidth-2*15, 30)];
        termsLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
        termsLabel.text = L(@"TERMS");
        [offerView addSubview:termsLabel];
        
        NSArray *termsArray = @[@"Discount is valid only for food and non-alcoholic beverages",@"Please let us know if you are a Landmark Building Employee"];
        CGFloat startY = termsLabel.endPointY;
        for (int i = 0; i<termsArray.count; i++) {
            
            CGFloat height=startY;
            
            UIImageView *termImage = [[UIImageView alloc]initWithFrame:CGRectMake(15, height+10, 3, 3)];
            [offerView addSubview:termImage];
            termImage.backgroundColor = [UIColor blackColor];
            termImage.layer.masksToBounds = YES;
            termImage.layer.cornerRadius = 1.5;
            
            CGSize size = [[termsArray objectAtIndex:i] sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] constrainedToSize:CGSizeMake(DeviceWidth-30, 200)];
            
            UILabel *termLabel = [[UILabel alloc]initWithFrame:CGRectMake(25, height, DeviceWidth-imageView.endPointX, size.height)];
            termLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
            termLabel.alpha = 0.7;
            termLabel.numberOfLines = 0;
            termLabel.text = [termsArray objectAtIndex:i];
            [offerView addSubview:termLabel];
            startY = termLabel.endPointY;
        }
        
        UIButton *offerButton = [UIButton buttonWithType:UIButtonTypeCustom];
        offerButton.frame = CGRectMake(15, 15+startY, DeviceWidth-2*15, 45);
        [offerButton addTarget:self action:@selector(bookButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        offerButton.tag = 100+i;
        [offerButton setTitle:L(@"Book Now") forState:UIControlStateNormal];
        [offerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [offerButton setBackgroundImage:[UIImage imageNamed:@"BookNowBtn"] forState:UIControlStateNormal];
        offerButton.titleLabel.font=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:15];
        [offerView addSubview:offerButton];
        
        
        offerView.frame = CGRectMake(0, startYY, DeviceWidth, offerButton.endPointY+10);
        
        UIImageView *shadowView=[[UIImageView alloc]initShadowImageViewWithShadowOriginY:offerView.frame.size.height andHeight:8];
        [offerView addSubview:shadowView];
        
        startYY = offerView.endPointY+8;
        _scrollView.contentSize = CGSizeMake(DeviceWidth, offerView.endPointY+100);
    }
    
    UIView *regularBookingView = [[UIView  alloc]initWithFrame:CGRectMake(0, startYY, DeviceWidth, 80)];
    regularBookingView.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:regularBookingView];
    
    Label *regularBookingLabel = [[Label alloc]initWithFrame:CGRectMake(15, 15, DeviceWidth-30, 20)andTextFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:16] andTextColor:[UIColor color222222] andTextLines:0];
    regularBookingLabel.text = L(@"Regular Booking");
    [regularBookingView addSubview:regularBookingLabel];
    
    UIButton *bookButton = [UIButton buttonWithType:UIButtonTypeCustom];
    bookButton.frame = CGRectMake(15,15 + regularBookingLabel.endPointY, DeviceWidth-30, 45);
    [bookButton setTitle:L(@"Book Now") forState:UIControlStateNormal];
    [bookButton addTarget:self action:@selector(bookButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [bookButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [bookButton setBackgroundImage:[UIImage imageNamed:@"BookNowGray-btn"] forState:UIControlStateNormal];
    bookButton.titleLabel.font=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:15];
    bookButton.tag = 100+restaurantOfferDetailList.count;
    [regularBookingView addSubview:bookButton];
    
    _scrollView.contentSize = CGSizeMake(DeviceWidth, regularBookingView.endPointY+100);
    
    //标题下阴影
    UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:0 andHeight:4];
    [self.view addSubview:shadowImageView];
    
}
-(void)bookButtonClick:(UIButton*)button
{
//    SpecialOffersViewController *specialOffers = [[SpecialOffersViewController alloc]init];
//    [self.navigationController pushViewController:specialOffers animated:YES];
    
    SelectDinersViewController *selectDinersVC = [[SelectDinersViewController alloc]init];
    selectDinersVC.OfferSelected = YES;
    [self.navigationController pushViewController:selectDinersVC animated:YES];
}
-(void)seeMenuButtonClick
{
    UIView *popUpView = [[UIView alloc]init];
    
    NSString *offerTitleStr = L(@"Eat Jakarta 2014 Set Menu 200k");
    
    CGSize maxSize = [offerTitleStr sizeWithFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
    Label *seeMenuTitle = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 54/2-(maxSize.height+4)/2, maxSize.width, maxSize.height+4) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] andTextColor:[UIColor color333333] andTextLines:0];
    seeMenuTitle.text = offerTitleStr;
    seeMenuTitle.textAlignment = NSTextAlignmentCenter;
    [popUpView addSubview:seeMenuTitle];
    
    NSArray *menusArr = @[@"Rp.500.000++/person",@"Appetizer\nBlue Swimmer Crab Cake",@"Main Course\nBlack Angus Strip Loin 200 Gr",@"Dessert\nTres Leches"];
    float startY = seeMenuTitle.endPointY ;
    for (int i=0; i<menusArr.count; i++) {
        CGSize maxSize1 = [[menusArr objectAtIndex:i] sizeWithFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:15] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
        
        Label *seeMenuLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, startY, DeviceWidth-4*LEFTLEFTSET, maxSize1.height+20) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15] andTextColor:[UIColor color333333] andTextLines:0];
        seeMenuLabel.textAlignment = NSTextAlignmentCenter;
        seeMenuLabel.text = [menusArr objectAtIndex:i];
        seeMenuLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [popUpView addSubview:seeMenuLabel];
        startY = seeMenuLabel.endPointY;
    }
    popUpView.frame = CGRectMake(0, 0, DeviceWidth-2*LEFTLEFTSET, startY);
    
    
    
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc]init];
    
    // Add some custom content to the alert view
    [alertView setContainerView:popUpView];
    
    //    [alertView setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, int buttonIndex) {
        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
