//
//  LatestReviewViewController.m
//  Qraved
//
//  Created by Shine Wang on 8/2/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

//tags
//rateView :200
//rateButton:201

//CellContentSize   尺寸
#define StartViewRowOffset 17

#define RateButtonHeight 34

#define  RateViewHeigth 200   //黑色透明rateView

#define StartImageWidth 16   //星星图片宽度
#define StartImageHeight 16   //星星图片高度

#define StarOffset 10 //星星之间的间距

#define ScrollViewImageHeight 61
#define ScrollViewImageWidth 61
#define CONTENT_WIDTH 240
#define REFRESH_HEADER_HEIGHT 52.0f

#define REFRESH_LIST_COUNT 10
//Tags
#define NameLabelTag 1
#define ReviewsCountLabelTag 2
#define QravesCountLabelTag 3
#define FollowButtonTag 4
#define AvatarImageTag 5
#define DisplayTimeLabel Tag 6
#define scoreLabelTag 7
#define SummarizeLabelTag 6554511
#define ScrollViewTag 9
#define CreateTimeLabelTag 4465454

#import "UserAllReviewController.h"
#import "PhotosDetailViewController.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "Tools.h"

#import "Label.h"
#import "UIView+Helper.h"
#import "NSNumber+Helper.h"
#import "UILabel+Helper.h"
#import "NSString+Helper.h"
#import "UIColor+Helper.h"
#import "UIButton+LinkButton.h"
#import "UIButton+WebCache.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Helper.h"
#import "UIImage+Resize.h"
#import "UIViewController+Helper.h"
#import "LoadingView.h"

#import "NodataView.h"
#import "DLStarRatingControl.h"
//#import "DTAttributedLabel.h"
#import "DetailViewController.h"
#import "RestaurantHandler.h"
#import "EGORefreshTableFooterView.h"
#import "ReviewPublishViewController.h"
#import "Date.h"
#import "IMGUserReview.h"
#import "IMGUser.h"

#import "UICommentInputView.h"
#import "EntityKind.h"
#import "UILikeCommentShareView.h"
#import "ReviewHandler.h"
#import "IMGReviewComment.h"

#import "RestaurantActivityItemProvider.h"
#import "TwitterActivityItemProvider.h"
#import "ReviewActivityItemProvider.h"

#import "AlbumViewController.h"
#import "OtherProfileViewController.h"
#import <GoogleAnalytics/GAIFields.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>

@interface UserAllReviewController ()<LikeCommentShareDelegate,EGORefreshTableFooterDelegate>
{
    NSArray *reviewArray;
    NSDictionary *dishDic;
    UIView *reviewsView;
    UIScrollView *scrollView;
    TYAttributedLabel *totalLabel;
    EGORefreshTableFooterView *refreshTableFooterView;
    BOOL hasReview;
    float totalLabelPointY;
    float currentPointY;
    IMGUser *user;
    
    NSInteger thisCommentIndex;
    BOOL noKeyboardChangeFired;
    BOOL keyboardShown;
    float keyboardHeight;
    NSMutableArray *reviewHeight;
    NSMutableDictionary *reveiwCardIsReadMore;
    IMGReview *_review;
    BOOL isFirstLoading;

}
@property(nonatomic,retain)UIButton *rateButton;
@end

@implementation UserAllReviewController{
    //    IMGRestaurant *restaurant;
    IMGUserReview *userReviewTemp;
    NSInteger _total;
}
@synthesize rateButton;

 

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardDidHideNotification object:nil];
    [[LoadingView sharedLoadingView] stopLoading];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.navigationItem setTitle:self.restaurant.title];
    self.navigationController.navigationBarHidden = NO;
    
}
-(id)initWithRestaurant:(IMGRestaurant*)restaurantT and:(IMGReview*)review_
{
    if (self = [super init]) {
        self.restaurant=restaurantT;
        self.restaurantId = restaurantT.restaurantId;
        _total=[self.restaurant.ratingCount intValue];
        hasReview = YES;
        self.page=1;
        self.limit=10;
        _review=review_;
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self sendManualScreenName:@"Restaurant Review list page"];
    reveiwCardIsReadMore = [[NSMutableDictionary alloc]init];
    
    reviewHeight = [[NSMutableArray alloc]init];
    
    user=[IMGUser currentUser];
    currentPointY=0.0f;
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadViewWithNSNotificationCenter) name:@"ReloadUserReviewsView" object:nil];
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    scrollView.delegate=self;
    scrollView.scrollsToTop=NO;
    [self.view addSubview:scrollView];
    [self setBackBarButtonOffset30];
    [[LoadingView sharedLoadingView] startLoading];
    
    [self addPullToRefreshFooter];
    // [self displayReviewView:reviewArray withDish:dishDic];
    [self loadReviews];
    isFirstLoading =YES;
    
    
}
- (void)goToBackViewController
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ReloadUserReviewsView" object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)reloadViewWithNSNotificationCenter
{
    self.page = 1;
    currentPointY = 0;
    isFirstLoading = YES;
    [self loadReviews];
}

-(void)displayReviewCount:(NSInteger)total{
    
    NSString *totalString = [NSString stringWithFormat:@"%lu %@",(long)total,total>1?@"Ratings":@"Rating"];
    
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc]initWithString:totalString];
    UIFont *font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12];
    NSRange range = [totalString rangeOfString:[NSString stringWithFormat:@"%lu",(long)total]];
    [attStr addAttribute:(id)kCTFontAttributeName value:font range:[totalString rangeOfString:totalString]];
    [attStr addAttribute:(id)kCTForegroundColorAttributeName value:(id)[UIColor colorFFC000].CGColor range:range];
    
    CGSize expectSize = [totalString sizeWithFont:font];
    totalLabel = [[TYAttributedLabel alloc] initWithFrame:CGRectMake(DeviceWidth/2-expectSize.width/2, totalLabelPointY, expectSize.width, expectSize.height) ];
    totalLabel.attributedText = attStr;

    [reviewsView addSubview:totalLabel];
}

-(void)displayReviewView:(NSArray *)reviewArray_ withDish:(NSDictionary *)dishDic_{
    
    for (int i=0; i<reviewArray_.count; i++) {
        
        UIView *detailReviewView = [[UIView alloc]init];
        detailReviewView.backgroundColor = [UIColor whiteColor];
        [scrollView addSubview:detailReviewView];
        
        IMGReview *review=[reviewArray_ objectAtIndex:i];
        NSArray *dishArray=[dishDic_ objectForKey:review.reviewId];
        review.restaurantTitle = _restaurant
        .title;
        
        if ([review.userId intValue] == [user.userId intValue])
        {
            
            IMGUserReview *userReview = [[IMGUserReview alloc] init];
            userReview.reviewId = review.reviewId;
            userReview.restaurantId = review.restaurantId;
            userReview.reviewScore = review.score;
            userReview.reviewTimeCreated =  review.timeCreated;
            userReview.userId = [NSNumber numberWithInt:[review.userId intValue]];
            userReview.restaurantPriceLevel = _restaurant.priceLevel;
            userReview.reviewTitle = [review.title filterHtml];;
            userReview.reviewSummarize = [review.summarize stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            userReview.restaurantTitle = _restaurant.title;
            userReview.restaurantCuisine = _restaurant.cuisineName;
            userReview.restaurantDistrict = _restaurant.districtName;
            userReview.restaurantSeoKeywords = _restaurant.seoKeyword;
            userReview.restaurantCityName = _restaurant.cityName;
            userReview.userFullName = [review.fullName filterHtml];
            userReview.userAvatar = review.avatar;
            userReview.dishListArrM = [NSMutableArray arrayWithArray:dishArray];
            if (i==0&&isFirstLoading) {
                ReviewView *userReviewView = [[ReviewView alloc] initWithReview:review andDish:dishArray withDelegate:self fromDetail:YES andIsOtherUser:NO andIsreadMore:(bool)[reveiwCardIsReadMore objectForKey:review.targetId]andIsFromUserallReviewPage:NO andIsNeedReadMore:NO];
                userReviewView.frame = CGRectMake(0, 0,DeviceWidth,userReviewView.reviewHeight);
                [detailReviewView addSubview:userReviewView];
                userReviewView.delegate = self;
                userReviewView.ATReviewsVC=YES;
                review.commentList = [IMGReviewComment commentListFromArray:review.commentList andIsVendor:NO andRestaurant:_restaurant];
                detailReviewView.frame = CGRectMake(0, currentPointY, DeviceWidth, userReviewView.endPointY);
                currentPointY = detailReviewView.endPointY+8;
                reviewHeight[i] = [NSString stringWithFormat:@"%f",detailReviewView.endPointY];
                [self addLineViewWithPointY:currentPointY andSuperView:userReviewView] ;

            }else{
                ReviewView *userReviewView = [[ReviewView alloc] initWithReview:review andDish:dishArray withDelegate:self fromDetail:YES andIsOtherUser:NO andIsreadMore:(bool)[reveiwCardIsReadMore objectForKey:review.targetId]andIsFromUserallReviewPage:YES andIsNeedReadMore:NO];
                userReviewView.frame = CGRectMake(0, 0,DeviceWidth,userReviewView.reviewHeight);
                [detailReviewView addSubview:userReviewView];
                userReviewView.delegate = self;
                userReviewView.ATReviewsVC=YES;
                review.commentList = [IMGReviewComment commentListFromArray:review.commentList andIsVendor:NO andRestaurant:_restaurant];
                detailReviewView.frame = CGRectMake(0, currentPointY, DeviceWidth, userReviewView.endPointY);
                currentPointY = detailReviewView.endPointY+8;
                reviewHeight[i] = [NSString stringWithFormat:@"%f",detailReviewView.endPointY];
                [self addLineViewWithPointY:currentPointY andSuperView:userReviewView] ;

            }

        }
        else
        {
            if (i==0&&isFirstLoading) {
                ReviewView *reviewView = [[ReviewView alloc] initWithReview:review andDish:dishArray withDelegate:self fromDetail:NO andIsOtherUser:YES andIsreadMore:(bool)[reveiwCardIsReadMore objectForKey:review.targetId] andIsFromUserallReviewPage:NO andIsNeedReadMore:NO];
                reviewView.frame = CGRectMake(0, 0,DeviceWidth,reviewView.reviewHeight);
                [detailReviewView addSubview:reviewView];
                //            detailReviewView.frame = CGRectMake(0, currentPointY, DeviceWidth, reviewView.endPointY);
                reviewView.ATReviewsVC=YES;
                review.commentList = [IMGReviewComment commentListFromArray:review.commentList andIsVendor:NO andRestaurant:_restaurant];
                detailReviewView.frame = CGRectMake(0, currentPointY, DeviceWidth, reviewView.endPointY);
                
                currentPointY = detailReviewView.endPointY+8;
                reviewHeight[i] = [NSString stringWithFormat:@"%f",detailReviewView.endPointY];
                [self addLineViewWithPointY:currentPointY andSuperView:reviewView];

            }else{
                ReviewView *reviewView = [[ReviewView alloc] initWithReview:review andDish:dishArray withDelegate:self fromDetail:NO andIsOtherUser:YES andIsreadMore:(bool)[reveiwCardIsReadMore objectForKey:review.targetId] andIsFromUserallReviewPage:YES andIsNeedReadMore:NO];
                reviewView.frame = CGRectMake(0, 0,DeviceWidth,reviewView.reviewHeight);
                [detailReviewView addSubview:reviewView];
                //            detailReviewView.frame = CGRectMake(0, currentPointY, DeviceWidth, reviewView.endPointY);
                reviewView.ATReviewsVC=YES;
                review.commentList = [IMGReviewComment commentListFromArray:review.commentList andIsVendor:NO andRestaurant:_restaurant];
                detailReviewView.frame = CGRectMake(0, currentPointY, DeviceWidth, reviewView.endPointY);
                
                currentPointY = detailReviewView.endPointY+8;
                reviewHeight[i] = [NSString stringWithFormat:@"%f",detailReviewView.endPointY];
                [self addLineViewWithPointY:currentPointY andSuperView:reviewView];

            }

        }
        
        
        
        
        currentPointY = detailReviewView.endPointY+8;
        
        
    }
    
    scrollView.contentSize = CGSizeMake(DeviceWidth, currentPointY+45);
}

-(void)loadReviews{
    NSMutableDictionary *params=[[NSMutableDictionary alloc]initWithDictionary: @{@"max":[NSNumber numberWithUnsignedInteger:self.limit],@"offset":[NSNumber numberWithUnsignedInteger:self.limit*(self.page-1)],@"restaurantId":self.restaurantId,@"targetUserId":_review.userId}];
    __weak UserAllReviewController *weakSelf=self;
    [RestaurantHandler getUserAllReviewsFromServer:params andBlock:^(BOOL status,NSUInteger total,NSArray *reviewArrayT,NSDictionary *reviewDishDicT){
        if (status) {
            if (weakSelf.page*weakSelf.limit>=total){
                hasReview=NO;
            }
            if(weakSelf.page==1){
                for (UIView *view in scrollView.subviews) {
                    [view removeFromSuperview];
                }
                [weakSelf addPullToRefreshFooter];
                [weakSelf displayReviewCount:total];
            }
            weakSelf.page+=1;
            reviewArray = [NSArray arrayWithArray:reviewArrayT];
            dishDic = [NSDictionary dictionaryWithDictionary:reviewDishDicT];
            [weakSelf displayReviewView:reviewArrayT withDish:reviewDishDicT];
            isFirstLoading = NO;
            [[LoadingView sharedLoadingView] stopLoading];
        }
    }];
}

- (void)addPullToRefreshFooter {
    __autoreleasing __typeof(self)weakSelf = self;
    refreshTableFooterView = [[EGORefreshTableFooterView alloc] initWithFrame:CGRectMake(0, scrollView.frame.size.height+15, DeviceWidth, REFRESH_HEADER_HEIGHT)];
    refreshTableFooterView.backgroundColor = [UIColor clearColor];
    refreshTableFooterView.delegate=weakSelf;
    [scrollView addSubview:refreshTableFooterView];
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView_{
    if (scrollView_.contentOffset.y<-66) {
        scrollView_.contentOffset = CGPointMake(0, -66);
    }
    if (scrollView_.contentSize.height>scrollView_.frame.size.height) {
        refreshTableFooterView.frame = CGRectMake(0, scrollView_.contentSize.height-20, DeviceWidth, REFRESH_HEADER_HEIGHT);
    }else{
        refreshTableFooterView.frame = CGRectMake(0, scrollView_.frame.size.height, DeviceWidth, REFRESH_HEADER_HEIGHT);
    }
    refreshTableFooterView.hidden = NO;
    if(!hasReview){
        refreshTableFooterView.hidden = YES;
        [scrollView_ setContentInset:UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f)];
        return;
    }
    [refreshTableFooterView egoRefreshScrollViewDidScroll:scrollView_];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView_ willDecelerate:(BOOL)decelerate{
    [refreshTableFooterView egoRefreshScrollViewDidEndDragging:scrollView_];
}

- (void)egoRefreshTableFooterDidTriggerRefresh:(EGORefreshTableFooterView*)view{
    if (!hasReview){
        return;
    }
    [self loadReviews];
}
-(void)reviewImagePressed:(ReviewView*)review images:(NSArray*)images withTag:(int)tag{
    
    AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:images andPhotoTag:tag andRestaurant:_restaurant andFromDetail:YES];
    
    albumViewController.title = [review.review.title filterHtml];
    albumViewController.showPage=NO;
    albumViewController.amplitudeType = self.amplitudeType;
    [self.navigationController pushViewController:albumViewController animated:YES];
    
}


-(void)userReviewImagePressed:(UserReviewView*)review images:(NSArray*)images withTag:(int)tag andRestaurant:(IMGRestaurant *)restaurantT
{
    AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:images andPhotoTag:tag andRestaurant:_restaurant andFromDetail:YES];
    
    albumViewController.title = [review.review.title filterHtml];
    albumViewController.showPage=NO;
    albumViewController.amplitudeType = self.amplitudeType;
    [self.navigationController pushViewController:albumViewController animated:YES];
}


- (void)userReviewMoreBtnClickWithIMGUserReview:(IMGUserReview*)review
{
    
    userReviewTemp = review;
    CGFloat yHeight = 152.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(0, DeviceHeight-yHeight, DeviceWidth, yHeight)];
    poplistview.delegate = self;
    poplistview.tag = 99;
    poplistview.datasource = self;
    poplistview.listView.scrollEnabled = FALSE;
    [poplistview setTitle:nil];
    [poplistview show];
    
}
- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView cellForIndexPath:(NSIndexPath *)indexPath
{
    if (popoverListView.tag == 99)
    {
        static NSString *identifier = @"cell";
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET*2, 12, 30, 30)];
        [cell.contentView addSubview:iconImage];
        
        Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(70, 15, DeviceWidth-70, 23)
                                            andTextFont:[UIFont systemFontOfSize:15]
                                           andTextColor:[UIColor blackColor]
                                           andTextLines:0];
        
        [cell.contentView addSubview:titleLabel];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSInteger row = indexPath.row;
        if(row == 0){
            titleLabel.text = @"Edit Review";
            titleLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:15];
            titleLabel.textColor = [UIColor color222222];
            iconImage.image = [UIImage imageNamed:@"listEditGreen"];
        }
        else
        {
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.textLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:18];
            cell.textLabel.textColor = [UIColor colorWithRed:23/255.0 green:127/255.0 blue:252/255.0 alpha:1];
            cell.textLabel.text = @"Cancel";
        }
        return cell;
    }
    return nil;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section
{
    
    return 2;
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 53.0f;
}
-(void)popoverListView:(UIPopoverListView *)popoverListView  didSelectIndexPath:(NSIndexPath *)indexPath{
    
    if (popoverListView.tag == 99)
    {
        if (indexPath.row == 0)
        {
//            ReviewPublishViewController *rpvc = [[ReviewPublishViewController alloc] initWithRestaurant:restaurant andOverallRating:[userReviewTemp.reviewScore floatValue]/2 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
//            //                rpvc.userReview = userReviewTemp;
//            rpvc.isFromDetail = YES;
//            rpvc.isUpdateReview = YES;
//            rpvc.isEditReview = YES;
//            rpvc.amplitudeType = @"Restaurant detail page";
//            rpvc.isEdit = YES;
//            rpvc.reviewId = userReviewTemp.reviewId;
//            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:rpvc] animated:YES completion:^{
//                
//            }];
            IMGRestaurant *restaurantT = [[IMGRestaurant alloc] init];
            restaurantT.restaurantId = userReviewTemp.restaurantId;
            restaurantT.title = userReviewTemp.restaurantTitle;
            ReviewPublishViewController *rpvc = [[ReviewPublishViewController alloc] initWithRestaurant:self.restaurant andOverallRating:[userReviewTemp.reviewScore floatValue]/2 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
//            rpvc.userReview = userReviewTemp;
            rpvc.isEdit = YES;
            rpvc.isEditReview = YES;
            rpvc.isUpdateReview = YES;
            rpvc.amplitudeType = @"Card detail page";
            rpvc.reviewId = userReviewTemp.reviewId;

            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:rpvc] animated:YES completion:^{
                
            }];
        }
        
        [popoverListView dismiss];
    }
}
- (void)reloadUserReviews
{
    currentPointY=reviewsView.endPointY;
    for (UIView *view in scrollView.subviews)
    {
        if (view.tag == 99)
        {
            continue;
        }
        [view removeFromSuperview];
    }
    [self addPullToRefreshFooter];
    //    _total = _total - 1;
    [self displayReviewCount:_total];
    [self displayReviewView:reviewArray withDish:dishDic];
    
}
- (void)deleteReviewForServiceWithReviewId:(NSNumber *)reviewId
{
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",reviewId,@"reviewId", nil];
    [[IMGNetWork sharedManager]GET:@"delreview" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {

        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"deleteReviewForServiceWithReviewId  --- error");
        
    }];
    
    
}

- (void)userReviewWriteReviewWithIMGUserReview:(IMGUserReview *)review
{
    userReviewTemp = review;
    IMGRestaurant *restaurant_ = [[IMGRestaurant alloc] init];
    restaurant_.restaurantId = userReviewTemp.restaurantId;
    restaurant_.title = userReviewTemp.restaurantTitle;
    ReviewPublishViewController *rpvc = [[ReviewPublishViewController alloc] initWithRestaurant:_restaurant andOverallRating:[userReviewTemp.reviewScore floatValue]/2 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
    rpvc.userReview = userReviewTemp;
    rpvc.isUpdateReview = YES;
    rpvc.isEdit = YES;
    rpvc.amplitudeType = @"Card detail page";
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:rpvc] animated:YES completion:^{
        
    }];
    
}





-(void)likeCommentShareView:(UILikeCommentShareView *)likeCommentShareView_ likeButtonTapped:(UIButton *)button{
    
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
    [[LoadingView sharedLoadingView] startLoading];
    //like service
    
    IMGReview *review=[reviewArray objectAtIndex:likeCommentShareView_.tag];
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    //Review_ID, ReviewerUser_ID, Restaurant_ID, Location
    [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
    [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
    [eventProperties setValue:review.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:@"Review card detail page" forKey:@"Location"];
    [ReviewHandler likeReview:review.isLike reviewId:review.reviewId andBlock:^{
        if (!review.isLike) {
            [[Amplitude instance] logEvent:@"UC - UnLike Review Succeed" withEventProperties:eventProperties];
        }else{
            [[Amplitude instance] logEvent:@"UC - Like Review Succeed" withEventProperties:eventProperties];
        }
        
    }failure:^(NSString *exceptionMsg){
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
    }];
    review.isLike=!review.isLike;
    if(review.isLike){
        review.likeCount=[NSNumber numberWithInt:[review.likeCount intValue]+1];
        [[Amplitude instance] logEvent:@"UC - Like Review Initiate" withEventProperties:eventProperties];
    }else{
        review.likeCount=[NSNumber numberWithInt:[review.likeCount intValue]-1];
        
        [[Amplitude instance] logEvent:@"UC - UnLike Review Initiate" withEventProperties:eventProperties];
    }
    [likeCommentShareView_ updateLikedStatus];
    [self reloadUserReviews];
    
    [[LoadingView sharedLoadingView] stopLoading];
    
}
- (void)likeCommentShareView:(UILikeCommentShareView *)likeCommentShareView_ commentInputView:(UIView *)commentInputView postButtonTapped:(UIButton *)postButton commentInputText:(NSString *)text{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
    IMGReview *review=[reviewArray objectAtIndex:likeCommentShareView_.tag];
    postButton.enabled = NO;
    [ReviewHandler postComment:text reviewId:review.reviewId restaurantId:_restaurant.restaurantId andBlock:^(IMGReviewComment *reviewComment) {
        postButton.enabled = YES;
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
        [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
        [eventProperties setValue:review.fullName forKey:@"ReviewFullName"];
        [eventProperties setValue:@"Restaurant detail page - review lists" forKey:@"Location"];
        if ([self.amplitudeType isEqualToString:@"My Profile - review list"]) {
            [eventProperties setValue:@"My Profile - review list" forKey:@"Location"];
        }
        [eventProperties setValue:reviewComment.commentId forKey:@"Comment_ID"];
        [eventProperties setValue:_restaurant.restaurantId forKey:@"Restaurant_ID"];
        
        [[Amplitude instance] logEvent:@"UC - Comment Review Succeed" withEventProperties:eventProperties];
        [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Review Succeed" withValues:eventProperties];
        
        if (review.commentList) {
            if (review.commentList.count>=3) {
                [review.commentList removeObjectAtIndex:review.commentList.count-1];
            }
            [review.commentList insertObject:reviewComment atIndex:0];
        }else{
            review.commentList = [[NSMutableArray alloc]initWithObjects:reviewComment, nil];
        }
        
        NSMutableArray *reviewArrM = [[NSMutableArray alloc] initWithArray:reviewArray];
        [reviewArrM replaceObjectAtIndex:likeCommentShareView_.tag withObject:review];
        reviewArray = [NSArray arrayWithArray:reviewArrM];
        [self reloadUserReviews];
        
    } failure:^(NSString *errorReason){
        if([errorReason isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
        [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
        [eventProperties setValue:_restaurant.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:[NSString stringWithFormat:@"%@",errorReason] forKey:@"Reason"];
        [eventProperties setValue:@"Restaurant detail page - review lists" forKey:@"Location"];
        if ([self.amplitudeType isEqualToString:@"My Profile - review list"]) {
            [eventProperties setValue:@"My Profile - review list" forKey:@"Location"];
        }
        [[Amplitude instance] logEvent:@"UC - Comment Review Failed" withEventProperties:eventProperties];
    }];
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:review.reviewId forKey:@"ReviewID"];
    [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
    [eventProperties setValue:review.fullName forKey:@"ReviewFullName"];
    [eventProperties setValue:@"Restaurant detail page - review lists" forKey:@"Location"];
    if ([self.amplitudeType isEqualToString:@"My Profile - review list"]) {
        [eventProperties setValue:@"My Profile - review list" forKey:@"Location"];
    }
    
    [eventProperties setValue:_restaurant.restaurantId forKey:@"Restaurant_ID"];
    [[Amplitude instance] logEvent:@"UC - Comment Review Initiate" withEventProperties:eventProperties];
    
    
}
- (void)likeCommentShareView:(UIView*)likeCommentShareView_ commentInputView:(UIView *)view inputViewDidBeginEditing:(UITextView *)textView{
    thisCommentIndex = likeCommentShareView_.tag;
}
-(void)likeCommentShareView:(UIView *)likeCommentShareView commentCell:(UITableViewCell *)cell readMoreTapped:(IMGReviewComment *)comment offset:(CGFloat)offset{
    IMGReview *review=[reviewArray objectAtIndex:likeCommentShareView.tag];
    for(IMGReviewComment *commentItem in review.commentList){
        if(commentItem.commentId == comment.commentId){
            commentItem.readMore = YES;
        }
    }
    [self reloadUserReviews];
}

- (void)likeCommentShareView:(UILikeCommentShareView *)likeCommentShareView_ viewPreviousCommentsTapped:(UIButton*)button lastComment:(id)comment{
    
    
    
    [[Amplitude instance] logEvent:@"CL - View Previous Comments CTA" withEventProperties:@{@"Location":@"Review card detail page"}];
    
    [[LoadingView sharedLoadingView] startLoading];
    NSString *lastCommentIdStr=@"0";
    if([comment isKindOfClass:[IMGReviewComment class]]){
        IMGReviewComment *reviewComment=(IMGReviewComment*)comment;
        if(reviewComment){
            lastCommentIdStr=[NSString stringWithFormat:@"%@",reviewComment.commentId];
        }
        IMGReview *review=[reviewArray objectAtIndex:likeCommentShareView_.tag];
        
        [ReviewHandler previousComment:lastCommentIdStr targetId:review.reviewId andBlock:^(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount) {
            if(commentList&&commentList.count>0){
                [self.view endEditing:YES];
                
                if(commentList){
                    
                    for (id item in commentList) {
                        [review.commentList addObject:item];
                    }
                    NSMutableArray *reviewArrM = [[NSMutableArray alloc] initWithArray:reviewArray];
                    [reviewArrM replaceObjectAtIndex:likeCommentShareView_.tag withObject:review];
                    reviewArray = [NSArray arrayWithArray:reviewArrM];
                    [self reloadUserReviews];
                    
                    
                }
            }
        }];
    }else{
        [[LoadingView sharedLoadingView] stopLoading];
    }
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView commentButtonTapped:(UIButton *)button{
    //home card cell都留空
    thisCommentIndex = likeCommentShareView.tag;
}

- (void)likeCommentShareViewUserNameOrImageTapped:(id)sender
{
    if ([sender isKindOfClass:[IMGUser class]])
    {
        IMGUser *user_ = (IMGUser *)sender;
        if (![user.userId intValue]) {
            return;
        }
        OtherProfileViewController *opvc = [[OtherProfileViewController alloc] init];
        IMGUser *currentUser = [IMGUser currentUser];
        if ([currentUser.userId intValue] == [user_.userId intValue])
        {
            opvc.isOtherUser = NO;
        }else
        {
            opvc.isOtherUser = YES;
        }
        opvc.otherUser = user_;
        [self.navigationController pushViewController:opvc animated:YES];
    }
    else if ([sender isKindOfClass:[IMGRestaurant class]])
    {
        IMGRestaurant *restaurant_ = (IMGRestaurant *)sender;
        if (![restaurant_.restaurantId intValue]) {
            return;
        }
        
        DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:restaurant_.restaurantId];
        [self.navigationController pushViewController:dvc animated:YES];
    }
}

-(void)likeCommentShareView:(UILikeCommentShareView *)likeCommentShareView shareButtonTapped:(UIButton *)button{
    
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
    [[LoadingView sharedLoadingView] startLoading];
    
    
    IMGReview *review=[reviewArray objectAtIndex:likeCommentShareView.tag];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/review/%@",QRAVED_WEB_SERVER_OLD,review.reviewId]];
    NSString *shareContentString = [NSString stringWithFormat:@"CHECK IT OUT NOW! A Restaurant Review by %@ on Qraved",review.fullName];
    ReviewActivityItemProvider *provider=[[ReviewActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
    provider.username = review.fullName;
    provider.restaurantTitle = review.restaurantTitle;
    provider.url=url;
    provider.title=shareContentString;
    
    TwitterActivityItemProvider *twitterProvider=[[TwitterActivityItemProvider alloc] initWithPlaceholderItem:@""];
    twitterProvider.title=[NSString stringWithFormat:L(@"Check out %@ review at %@ on #Qraved!"),review.fullName,review.restaurantTitle];
    
    
    UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterProvider] applicationActivities:nil];
    activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
    [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK IT OUT NOW!A Restaurant Review By %@ on Qraved!"),review.fullName] forKey:@"subject"];
    activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
        if (completed) {
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:@"My Profile - review list" forKey:@"Location"];
            [eventProperties setValue:provider.activityType forKey:@"Channel"];
            [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
            [eventProperties setValue:review.fullName forKey:@"ReviewerName"];
            [eventProperties setValue:review.restaurantId forKey:@"Restaurant_ID"];
            
            [[Amplitude instance] logEvent:@"SH - Share Review" withEventProperties:eventProperties];
        }else{
        }
    };
    if ([activityCtl respondsToSelector:@selector(popoverPresentationController)]) {
        
        activityCtl.popoverPresentationController.sourceView = button;
        
    }
    
    
    [self presentViewController:activityCtl animated:YES completion:^{
        
    }];
    [[LoadingView sharedLoadingView] stopLoading];
}

-(void)keyboardChange:(NSNotification *)notification
{
    if (notification.name == UIKeyboardDidShowNotification) {
        
    }else{
        keyboardShown=NO;
    }
    
    if(noKeyboardChangeFired){
        noKeyboardChangeFired=NO;
        return;
    }else{
        noKeyboardChangeFired=NO;
    }
    
    NSDictionary *userInfo = [notification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    keyboardHeight = keyboardEndFrame.size.height;
    //adjust ChatTableView's height
    if (notification.name == UIKeyboardDidShowNotification) {
        keyboardShown=YES;
        if(thisCommentIndex>=0){
            if (0==reviewArray.count) {
                return;
            }
            float scrollToHeight =  [reviewHeight[thisCommentIndex] floatValue] - (DeviceHeight - keyboardHeight - 45);
            [scrollView setContentOffset:CGPointMake(0, scrollToHeight) animated:YES];
        }
    }else{
        keyboardShown=NO;
        thisCommentIndex = -1;
    }
    
    [self.view layoutIfNeeded];
    
    //adjust UUInputFunctionView's originPoint
    if(!keyboardShown){
    }else{
    }
    [UIView commitAnimations];
    
}

- (void)reviewUserNameOrImageTapped:(id)sender{
    if ([sender isKindOfClass:[IMGUser class]])
    {
        IMGUser *user_ = (IMGUser *)sender;
        OtherProfileViewController *opvc = [[OtherProfileViewController alloc] init];
        IMGUser *currentUser = [IMGUser currentUser];
        if ([currentUser.userId intValue] == [user_.userId intValue])
        {
            opvc.isOtherUser = NO;
        }else
        {
            opvc.isOtherUser = YES;
        }
        opvc.otherUser = user_;
        [self.navigationController pushViewController:opvc animated:YES];
    }
}
-(void)reviewViewMsgClick:(ReviewView *)reviewView readMoreTapped:(id)review isReadMore:(BOOL)isReadMore{
    IMGReview *thisReview = (IMGReview *)review;
    
    [[Amplitude instance] logEvent:@"CL - Read More on Review " withEventProperties:@{@"Location":@"Restaurant list page"}];
    [reveiwCardIsReadMore setObject:isReadMore?@1:@0 forKey:thisReview.targetId];
    //
    //
    //    [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[_tableView indexPathForCell:tableViewCell]] withRowAnimation:UITableViewRowAnimationFade];
    [self reloadViewWithNSNotificationCenter];
}

- (void)sendManualScreenName:(NSString*)screenName
{
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:screenName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}
-(void)addLineViewWithPointY:(CGFloat)pointY andSuperView:(UIView*)superView{
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, superView.endPointY-1,DeviceWidth, 1)];
    lineView.backgroundColor = [UIColor blackColor];
    lineView.alpha = 0.2;
    [superView addSubview:lineView];
//    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
//    gradientLayer.frame = lineView.bounds;
//    gradientLayer.colors = @[(id)[UIColor whiteColor].CGColor,
//                             (id)[UIColor blackColor].CGColor,
//                             (id)[UIColor whiteColor].CGColor];
//    gradientLayer.locations = @[@(0) ,@(0.5f)];
//    gradientLayer.startPoint = CGPointMake(0, 0);
//    gradientLayer.endPoint = CGPointMake(1,0);
//    [lineView.layer addSublayer:gradientLayer];

}

@end
