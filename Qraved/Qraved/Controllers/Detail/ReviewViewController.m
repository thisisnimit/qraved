//
//  LatestReviewViewController.m
//  Qraved
//
//  Created by Shine Wang on 8/2/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "ReviewViewController.h"
#import "LoadingView.h"
#import "RestaurantHandler.h"
#import "ReviewTableViewCell.h"
#import "ReviewActivityItemProvider.h"
#import "TwitterActivityItemProvider.h"
#import "ReviewViewController.m"
#import "OtherProfileViewController.h"
#import "AlbumViewController.h"
#import "IMGUserReview.h"
#import "UIPopoverListView.h"
#import "ReviewPublishViewController.h"
#import "InstagramReviewTableViewCell.h"
#import "V2_PhotoDetailViewController.h"
#import "V2_InstagramModel.h"
#import "IMGDish.h"
#import "ReviewRatingHeaderView.h"
#import "JournalDetailViewController.h"


@interface ReviewViewController ()<UITableViewDelegate,UITableViewDataSource,ReviewTableViewCellDelegate,UIPopoverListViewDelegate,UIPopoverListViewDataSource,InstagramReviewTableViewCellDelegate>
@property (nonatomic, strong) IMGRestaurant *restaurant;
@end


@implementation ReviewViewController{
    UITableView *reviewTableView;
    NSMutableArray *allReviewArray;
    NSMutableArray *reviewArray;
    NSMutableArray *instagramReviewArray;
    int offset;
    int instagramOffset;
    int allOffset;
    IMGUserReview *userReviewTemp;
    NSInteger selectedIndex;
    UILabel *lblNoReviews;
}

-(void)viewWillDisappear:(BOOL)animated
{
    //[[LoadingView sharedLoadingView] stopLoading];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];

    [self.navigationItem setTitle:self.restaurant.title];
    self.navigationController.navigationBarHidden = NO;

}
-(id)initWithRestaurant:(IMGRestaurant*)restaurantT
{
    if (self = [super init]) {
        self.restaurant=restaurantT;
    }
    return self;
}

- (void)viewDidLoad
{
    [self addBackBtn];
    [self requestData];
    [self initData];
    [self loadMainView];
}

- (void)initData{
    offset = 0;
    instagramOffset = 0;
    allOffset = 0;
    reviewArray = [NSMutableArray array];
    instagramReviewArray = [NSMutableArray array];
    allReviewArray = [NSMutableArray array];
}

- (void)loadMainView{

    reviewTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHEIGHT-64) style:UITableViewStyleGrouped];
    reviewTableView.backgroundColor = [UIColor whiteColor];
    reviewTableView.delegate = self;
    reviewTableView.dataSource = self;
    reviewTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:reviewTableView];
    if (@available(iOS 11.0, *)) {
        reviewTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        reviewTableView.estimatedRowHeight = 0;
    }
    
    reviewTableView.mj_footer = [MJRefreshBackFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
}

- (void)requestData{
    
    [self getAllReviews];
    [self getReviews];
    [self getInstagramReviews];
}

- (void)loadMore{
    if (selectedIndex==0) {
        [self getAllReviews];
    }else if(selectedIndex==1){
        [self getReviews];
    }else{
         [self getInstagramReviews];
    }
}

- (void)getAllReviews{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:@"10" forKey:@"max"];
    [params setValue:[NSNumber numberWithInt:allOffset] forKey:@"offset"];
    [params setValue:self.restaurant.restaurantId forKey:@"restaurantId"];
    [params setValue:[IMGUser currentUser].userId forKey:@"userId"];
    [[LoadingView sharedLoadingView] startLoading];
    [RestaurantHandler getAllReviews:params andBlock:^(NSArray *reviewList, IMGRestaurant *restaurant) {
        [[LoadingView sharedLoadingView] stopLoading];
        [reviewTableView.mj_footer endRefreshing];
        self.restaurant = restaurant;
        if (reviewList.count>0) {
            [allReviewArray addObjectsFromArray:reviewList];
            allOffset+=10;
            [reviewTableView reloadData];
        }
    } failure:^{
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

- (void)getReviews{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:@"10" forKey:@"max"];
    [params setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
    [params setValue:self.restaurant.restaurantId forKey:@"restaurantId"];
    
    [RestaurantHandler getReviews:params andBlock:^(NSArray *reviewList) {
        
        [reviewTableView.mj_footer endRefreshing];
        if (reviewList.count>0) {
            [reviewArray addObjectsFromArray:reviewList];
            offset+=10;
            [reviewTableView reloadData];
        }else{
            [self createNoReviewsView];
        }
        
    } failure:^{
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

- (void)getInstagramReviews{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:@"10" forKey:@"max"];
    [params setValue:[NSNumber numberWithInt:instagramOffset] forKey:@"offset"];
    [params setValue:self.restaurant.restaurantId forKey:@"restaurantId"];
    [params setValue:[IMGUser currentUser].userId forKey:@"userId"];
    [RestaurantHandler getInstagramReviews:params andBlock:^(NSArray *instagramReviewList) {
        [reviewTableView.mj_footer endRefreshing];
        if (instagramReviewList.count>0) {
            [instagramReviewArray addObjectsFromArray:instagramReviewList];
            instagramOffset+=10;
        }else{
            [self createNoReviewsView];
        }
        
    } failure:^{
        
    }];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (selectedIndex==0) {
        return allReviewArray.count;
    }else if(selectedIndex==1){
        return reviewArray.count;
    }else{
        return instagramReviewArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (selectedIndex==0) {
        NSString *idenStr = [NSString stringWithFormat:@"all%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        IMGReview *review = [allReviewArray objectAtIndex:indexPath.row];
        if ([review.reviewType isEqual:@1]) {
            ReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
            if (!cell) {
                cell = [[ReviewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.controller = self;
                cell.navController = self.navigationController;
                cell.delegate = self;
            }
            cell.review = review;
            cell.indexPath = indexPath;
            return cell;
        }else if ([review.reviewType isEqual:@2]){
            InstagramReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
            if (!cell) {
                cell = [[InstagramReviewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.delegate = self;
            }
            cell.review = review;
            return cell;
        }else{
            return nil;
        }
    }else if (selectedIndex==1){
        NSString *idenStr = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        ReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
        if (!cell) {
            cell = [[ReviewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.controller = self;
            cell.navController = self.navigationController;
            cell.delegate = self;
        }
        cell.indexPath = indexPath;
        cell.review = [reviewArray objectAtIndex:indexPath.row];
        
        return cell;
    }else{
        NSString *idenStr = [NSString stringWithFormat:@"selectCell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        InstagramReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
        if (!cell) {
            cell = [[InstagramReviewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.delegate = self;
        }
        cell.review = [instagramReviewArray objectAtIndex:indexPath.row];
        return cell;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (selectedIndex == 0) {
        NSString *idenStr = [NSString stringWithFormat:@"all%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        IMGReview *review = [allReviewArray objectAtIndex:indexPath.row];
        if ([review.reviewType isEqual:@1]) {
            ReviewTableViewCell *cell = [[ReviewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
            cell.review = review;
            return cell.currentPointY;
        }else if ([review.reviewType isEqual:@2]){
            InstagramReviewTableViewCell *cell = [[InstagramReviewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
            cell.review = review;
            return cell.currentPointY;
        }else{
            return 0;
        }
    }else if (selectedIndex == 1){
        NSString *idenStr = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        ReviewTableViewCell *cell = [[ReviewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
        cell.review = [reviewArray objectAtIndex:indexPath.row];
        return cell.currentPointY;
    }else{
        NSString *idenStr = [NSString stringWithFormat:@"selectCell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        InstagramReviewTableViewCell *cell = [[InstagramReviewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
        cell.review = [instagramReviewArray objectAtIndex:indexPath.row];
        return cell.currentPointY;
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 220)];
    view.backgroundColor = [UIColor whiteColor];
    
    ReviewRatingHeaderView *ratingView = [[ReviewRatingHeaderView alloc] initWithFrame:CGRectMake(0, 10, DeviceWidth, 145)];
    ratingView.restaurant = self.restaurant;
    [view addSubview:ratingView];
    
    NSArray *tabs = @[@"ALL",@"QRAVED",@"INSTAGRAM"];
    
    HMSegmentedControl *tab=[[HMSegmentedControl alloc] initWithSectionTitles:tabs];
    tab.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationNone;
    tab.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    tab.selectionStyle = HMSegmentedControlSelectionStyleBox;
    tab.selectionIndicatorBoxColor = [UIColor colorWithHexString:@"DE2029"];
    tab.selectionIndicatorBoxOpacity = 1.0f;
    tab.selectedTitleTextAttributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:12],NSForegroundColorAttributeName : [UIColor whiteColor]};
    tab.titleTextAttributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:12],NSForegroundColorAttributeName : [UIColor color333333]};
    [tab setFrame:CGRectMake(15,ratingView.endPointY+20,DeviceWidth-30,36)];
    tab.selectedSegmentIndex = selectedIndex;
    __weak typeof(self) weakSelf = self;
    __weak typeof (reviewTableView) weakReviewTableView = reviewTableView;
    [tab setIndexChangeBlock:^(NSInteger index){
        selectedIndex= index;
        [weakSelf changeNoReviewsText];
        [weakReviewTableView reloadData];
    }];
    [view addSubview:tab];
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 220;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

- (void)gotoJournalDetail:(NSNumber *)journalId{
    IMGMediaComment *journal = [[IMGMediaComment alloc] init];
    journal.mediaCommentId = journalId;
    JournalDetailViewController *detailVC = [[JournalDetailViewController alloc] init];
    detailVC.journal = journal;
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (void)instagramReviewImagePressed:(IMGReview *)review withTag:(int)tag{
    NSMutableArray *imgArray = [NSMutableArray array];
    for (IMGDish *dish in review.imageArray) {
        V2_InstagramModel *model = [[V2_InstagramModel alloc] init];
        model.restaurant_id = self.restaurant.restaurantId;
        model.restaurant_name = self.restaurant.title;
        model.instagram_user_name = review.userName;
        model.instagram_user_profile_picture = review.userPicture;
        model.instagram_create_date = review.createTimeStr;
        model.low_resolution_image = dish.lowResolutionImage;
        model.thumbnail_image  =dish.thumbnailImage;
        model.standard_resolution_image = dish.standardResolutionImage;
        model.instagram_link = dish.instagramLink;
        [imgArray addObject:model];
    }
    
    V2_PhotoDetailViewController *photoDetail = [[V2_PhotoDetailViewController alloc] init];
    photoDetail.currentInstagram = [imgArray objectAtIndex:tag];
    photoDetail.photoArray = imgArray;
    photoDetail.currentIndex = tag;
    [self.navigationController pushViewController:photoDetail animated:YES];
}

-(void)reviewMsgReadMoreTapped:(NSIndexPath *)indexPath{
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            if ([[reviewTableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                return;
            }
            [reviewTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        @catch ( NSException *e ) {
            return;
        }
    });
}

- (void)shareButtonTapped:(IMGReview *)reviewView shareButton:(UIButton *)button{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    [[LoadingView sharedLoadingView] startLoading];
    //    IMGReview *review=[reviewsArray objectAtIndex:index];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/review/%@",QRAVED_WEB_SERVER_OLD,reviewView.reviewId]];
    NSString *shareContentString = [NSString stringWithFormat:@"CHECK IT OUT NOW! A Restaurant Review by %@ on Qraved",reviewView.fullName];
    ReviewActivityItemProvider *provider=[[ReviewActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
    provider.username = reviewView.fullName;
    provider.restaurantTitle = reviewView.restaurantTitle;
    provider.url=url;
    provider.title=shareContentString;
    
    TwitterActivityItemProvider *twitterProvider=[[TwitterActivityItemProvider alloc] initWithPlaceholderItem:@""];
    twitterProvider.title=[NSString stringWithFormat:L(@"Check out %@ review at %@ on #Qraved!"),reviewView.fullName,reviewView.restaurantTitle];
    UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterProvider] applicationActivities:nil];
    activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
    [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK IT OUT NOW!A Restaurant Review By %@ on Qraved!"),reviewView.fullName] forKey:@"subject"];
    activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
        if (completed) {
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:reviewView.reviewId forKey:@"Review_ID"];
            [eventProperties setValue:reviewView.fullName forKey:@"ReviewerName"];
            [eventProperties setValue:self.restaurant.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:@"Restaurant detail page - review lists" forKey:@"Location"];
            [eventProperties setValue:@"Channel" forKey:provider.activityType];
            
            [[Amplitude instance] logEvent:@"SH - Share Review" withEventProperties:eventProperties];
        }else{
        }
    };
    if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
        // iOS8
        activityCtl.popoverPresentationController.sourceView = button;
    }
    
    [self presentViewController:activityCtl animated:YES completion:NULL];
    [[LoadingView sharedLoadingView] stopLoading];
}

-(void)reviewUserNameOrImageTapped:(id)sender{
    if ([sender isKindOfClass:[IMGUser class]])
    {
        IMGUser *user = (IMGUser *)sender;
        OtherProfileViewController *opvc = [[OtherProfileViewController alloc] init];
        IMGUser *currentUser = [IMGUser currentUser];
        if ([currentUser.userId intValue] == [user.userId intValue])
        {
            opvc.isOtherUser = NO;
        }else
        {
            opvc.isOtherUser = YES;
        }
        opvc.otherUser = user;
        [self.navigationController pushViewController:opvc animated:YES];
    }
}

- (void)reviewImagePressed:(IMGReview *)review withTag:(int)tag{
    AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:review.imageArray andPhotoTag:tag andRestaurant:self.restaurant andFromDetail:YES];
    albumViewController.title = [review.title filterHtml];
    albumViewController.review= review;
    albumViewController.showPage=NO;
    [self.navigationController pushViewController:albumViewController animated:YES];
}

- (void)userReviewMoreBtnClickWithIMGUserReview:(IMGUserReview*)review
{
    
    userReviewTemp = review;
    CGFloat yHeight = 152.0f;
    UIPopoverListView *poplistviewWithUser = [[UIPopoverListView alloc] initWithFrame:CGRectMake(0, DeviceHeight-yHeight, DeviceWidth, yHeight)];
    poplistviewWithUser.delegate = self;
    poplistviewWithUser.datasource = self;
    poplistviewWithUser.listView.scrollEnabled = FALSE;
    [poplistviewWithUser setTitle:nil];
    [poplistviewWithUser show];
}

#pragma mark - UIPopoverListViewDataSource and delegate
-(UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                   cellForIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    
    UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET*2, 12, 30, 30)];
    [cell.contentView addSubview:iconImage];
    
    Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(70, 15, DeviceWidth-70, 23)
                                        andTextFont:[UIFont systemFontOfSize:15]
                                       andTextColor:[UIColor color333333]
                                       andTextLines:0];
    
    [cell.contentView addSubview:titleLabel];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSInteger row = indexPath.row;
    if(row == 0){
        titleLabel.text = @"Edit Review";
        titleLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:15];
        titleLabel.textColor = [UIColor color222222];
        iconImage.image = [UIImage imageNamed:@"listEditGreen"];
    }
    else
    {
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:18];
        cell.textLabel.textColor = [UIColor colorWithRed:23/255.0 green:127/255.0 blue:252/255.0 alpha:1];
        cell.textLabel.text = L(@"Cancel");
    }
    return cell;
}
-(NSInteger)popoverListView:(UIPopoverListView *)popoverListView
      numberOfRowsInSection:(NSInteger)section{
    return 2;
}
-(void)popoverListView:(UIPopoverListView *)popoverListView didSelectIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0)
    {
        ReviewPublishViewController *rpvc = [[ReviewPublishViewController alloc] initWithRestaurant:self.restaurant andOverallRating:[userReviewTemp.reviewScore floatValue]/2 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
        rpvc.isFromDetail = YES;
        rpvc.isUpdateReview = YES;
        rpvc.isEditReview = YES;
        rpvc.amplitudeType = @"RDP";
        rpvc.isEdit = YES;
        rpvc.reviewId = userReviewTemp.reviewId;
        //rpvc.reviewPublishViewControllerDelegate=self;
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:rpvc] animated:YES completion:^{
            
        }];
    }
    [popoverListView dismiss];
}
-(CGFloat)popoverListView:(UIPopoverListView *)popoverListView
  heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 53.0f;
}

- (void)createNoReviewsView{
    
    if (lblNoReviews) {
        [lblNoReviews removeFromSuperview];
    }
    
    lblNoReviews = [[UILabel alloc] initWithFrame:CGRectMake(15, 220, DeviceWidth-30, 20)];
    lblNoReviews.font = [UIFont systemFontOfSize:12];
    lblNoReviews.textColor = [UIColor color333333];
    lblNoReviews.hidden = YES;
    
    [reviewTableView addSubview:lblNoReviews];
    
    [self changeNoReviewsText];
}

- (void)changeNoReviewsText{
    if (selectedIndex==1 && reviewArray.count==0) {
        lblNoReviews.hidden = NO;
        lblNoReviews.text = @"This restaurant doesn’t have any reviews yet on Qraved.";
    }else if (selectedIndex == 2 && instagramReviewArray.count==0){
        lblNoReviews.hidden = NO;
        lblNoReviews.text = @"This restaurant doesn’t have any reviews yet on Instagram.";
    }else{
        lblNoReviews.hidden = YES;
    }
}

@end
