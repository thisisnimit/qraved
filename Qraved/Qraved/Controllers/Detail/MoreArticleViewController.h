//
//  MoreArticleViewController.h
//  Qraved
//
//  Created by Laura on 15/2/16.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
@class IMGMediaComment;

@interface MoreArticleViewController : BaseChildViewController

@property (nonatomic,retain) NSNumber *restaurantId;
//@property (nonatomic,copy) NSString *title;

-(instancetype)initWithRestaurant:(NSNumber*)restaurantId;
-(void)gotoJournal:(IMGMediaComment*)media inView:(UIView*)view;
-(void)loadRestaurantJournals:(void(^)(BOOL))block;

@property(nonatomic,retain) NSString *amplitudeType;

@end
