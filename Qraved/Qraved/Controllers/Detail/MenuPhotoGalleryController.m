//
//  SearchPhotosViewController.m
//  Qraved
//
//  Created by Admin on 8/2/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "MenuPhotoGalleryController.h"
#import "PhotosDetailViewController.h"

#import "SDWebImageDownloader.h"
#import "Consts.h"
#import "AppDelegate.h"
#import "AddPhotoViewController.h"
#import "MenuPhotoViewColltroller.h"

#import "Tools.h"
#import "UIColor+Helper.h"
#import "UIViewController+Helper.h"
#import "UIView+Helper.h"
#import "UIButton+LinkButton.h"
#import "UIImage+Resize.h"
 
#import "UIImageView+WebCache.h"
#import "UIImage+Helper.h"
#import "Label.h"
#import "UIDevice+Util.h"
#import "NSString+Helper.h"
#import "IMGUser.h"
#import "IMGRestaurant.h"
#import "IMGDish.h"
#import "PostDataHandler.h"
#import "LoadingImageView.h"
#import "DetailViewController.h"
//#import "AFHTTPRequestOperationManager.h"
#import "IMGRestaurantImage.h"
#import "IMGRestaurantEvent.h"
#import "NavigationBarButtonItem.h"
#import "MenuPhotoGalleryHandler.h"
#import "DetailBottomButtonView.h"
#define qraveWidth 305.0/2
#define IMAGE_WIDTH 60

@interface MenuPhotoGalleryController ()
{
    NSInteger currentCount;
    UIScrollView *scrollView;
    CGFloat currentHeight;
    float leftCurrentHeight;
    float rightCurrentHeight;

    float startPointY;
    
    float currentY;
    NSMutableArray *menuPhotoArray;
    NSMutableArray *menuPhotoList;
    IMGRestaurant *restaurant;
    NSMutableArray *photosArray;
    
    int menuType;
    NSMutableArray *restaurantMenu;
    NSMutableArray *barMenu;
    NSMutableArray *deliveryMenu;
    NSArray *lastPageMenuPhotoArray;
    IMGMenu *menu;
    int serveCout;
    NSNumber *offset;
}
@property (nonatomic, retain) UIScrollView *mScrollView;
@end

@implementation MenuPhotoGalleryController


-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant{
    self = [super init];
    if (self != nil) {
        restaurant = paramRestaurant;
    }
    return self;
}
-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant andIsSelfUpload:(BOOL)isSelfUpload{
    self = [super init];
    if (self != nil) {
        restaurant = paramRestaurant;
        self.isSelfUpload = isSelfUpload;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=NO;
    [super viewWillAppear:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Restaurant Menu list page";
    offset = [NSNumber numberWithInt:0];
    NavigationBarButtonItem *navigationBarLeftButton = [[NavigationBarButtonItem alloc ]initWithImage:[UIImage imageNamed:NavigationBackImage] andTitle:@"  MENU" target:self action:@selector(goToBackViewController) width:70.0f offset:-[UIDevice heightDifference]+15];
    self.navigationItem.leftBarButtonItem = navigationBarLeftButton;
    
    leftCurrentHeight = 5;
    rightCurrentHeight = 5;
    
    currentCount=0;

    IMGUser *user = [IMGUser currentUser];
    
    [self  addScrollView];
    if (self.isSelfUpload)
    {
        [self getSelfDatafromeServer:restaurant.restaurantId userId:user.userId offset:offset];
    }else
    {
        [self getDishesFromServer:restaurant.restaurantId offset:offset];
    }
    
}

-(void)setPhotoUI{
    for (NSDictionary *dic in menuPhotoList) {
        IMGMenu *menu_ = [[IMGMenu alloc] init];
        [menu_ setValuesForKeysWithDictionary:dic];
        menu_.imageUrl = [dic objectForKey:@"imageUrl"];
        menu_.type = [dic objectForKey:@"type"];
        [menuPhotoArray addObject:menu_];
    }
    for (int i = 0; i<menuPhotoArray.count; i++) {
        IMGMenu * menu_ = menuPhotoArray[i];
        if ([menu_.type intValue]==1||[menu_.type intValue]==0)
        {
            [restaurantMenu addObject:menu_];
        }else if ([menu_.type intValue] == 2)
        {
            [barMenu addObject:menu_];
        }else{
            [deliveryMenu addObject:menu_];
        }
    }
    [menuPhotoArray removeAllObjects];
}
- (void)getAllImageUrl{
    NSMutableArray *photoImageUrl = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in menuPhotoList) {
        IMGMenu *menu_ = [[IMGMenu alloc] init];
        [menu_ setValuesForKeysWithDictionary:dict];
        menu_.imageUrl = [dict objectForKey:@"imageUrl"];
        menu_.menuId = [dict objectForKey:@"id"];
        [photoImageUrl addObject:menu_];
        
    }
    photosArray = [[NSMutableArray alloc] initWithArray:photoImageUrl];
    
}

- (void)createBottomButtonView{
    DetailBottomButtonView *bottomButtonView = [[DetailBottomButtonView alloc] initWithFrame:CGRectMake(0, DeviceHEIGHT - IMG_StatusBarAndNavigationBarHeight - 72, DeviceWidth, 72)];
    [self.view addSubview:bottomButtonView];
    
    FunctionButton *goFoodButton = [FunctionButton buttonWithType:UIButtonTypeCustom];
    goFoodButton.frame = CGRectMake(LEFTLEFTSET, LEFTLEFTSET, DeviceWidth - LEFTLEFTSET*2, FUNCTION_BUTTON_HEIGHT);
    [goFoodButton addTarget:self action:@selector(goFoodTapped) forControlEvents:UIControlEventTouchUpInside];
    [bottomButtonView addSubview:goFoodButton];
}

- (void)goFoodTapped{
    [IMGAmplitudeUtil trackGoFoodWithName:@"CL - Go-Food CTA" andJournalId:nil andRestaurantId:restaurant.restaurantId andPhotoId:nil andLocation:@"Restaurant Menu List" andOrigin:nil andOrder:nil];
    [CommonMethod goFoodButtonTapped:restaurant andViewController:self andCallBackBlock:^(BOOL isSaved) {
        
    }];
}

-(void)addScrollView
{
    if (self.mScrollView == nil) {
        CGFloat scrollHeight = [Tools isBlankString:restaurant.goFoodLink] ? DeviceHEIGHT - IMG_StatusBarAndNavigationBarHeight : DeviceHEIGHT - IMG_StatusBarAndNavigationBarHeight - 72;
        self.mScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, scrollHeight)];
        self.mScrollView.backgroundColor = [UIColor whiteColor];
        self.mScrollView.showsVerticalScrollIndicator = NO;
        //        self.mScrollView.delegate = self;
        self.mScrollView.contentSize = CGSizeZero;
        self.mScrollView.delegate = self;
        currentY = 0;
       
        [self.view addSubview:self.mScrollView];
    }
    
    if (![Tools isBlankString:restaurant.goFoodLink]) {
        [self createBottomButtonView];
    }
}

-(void)addRestaurantMenu
{
    currentY=10;
    if(restaurantMenu.count==0) return;

    
    Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, currentY, DeviceWidth/2, 30) andTextFont:[UIFont boldSystemFontOfSize:12] andTextColor:[UIColor color333333] andTextLines:1];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.text = @"Restaurant Menu";
    [self.mScrollView addSubview:titleLabel];
    
    Label *numlabel = [[Label alloc]initWithFrame:CGRectMake(DeviceWidth-15-60, currentY, 60, 35) andTextFont:[UIFont fontWithName:FONT_GOTHAM_LIGHT size:12] andTextColor:[UIColor color999999] andTextLines:1];
    numlabel.textAlignment = NSTextAlignmentLeft;
    numlabel.text = [NSString stringWithFormat:@"%d %@",[menu.menuPhotoCountTypeRestant intValue],([menu.menuPhotoCountTypeRestant intValue]>1)?@"photos":@"photo"];
    
    [self.mScrollView addSubview:numlabel];
    
//    if (restaurantMenu.count>50*serveCout)
//    {
//        int restPotoCout =(int)restaurantMenu.count - 50*serveCout;
//        for (int i=0; i<restPotoCout; i++) {
//            int restPotoCoutI = i+50*serveCout;
//            UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(6+(3.5+(DeviceWidth-19)/3)*(restPotoCoutI%3), titleLabel.endPointY+((DeviceWidth-19)/3+3)*(restPotoCoutI/3), (DeviceWidth-19)/3, (DeviceWidth-19)/3)];
//            imageView.tag =restPotoCoutI;
//            UITapGestureRecognizer *Tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagePressed:)];
//            [Tap setNumberOfTapsRequired:1];
//            [Tap setNumberOfTouchesRequired:1];
//            imageView.userInteractionEnabled=YES;
//            [imageView addGestureRecognizer:Tap];
//            [imageView setImageWithURL:[NSURL URLWithString:[((IMGMenu *)[restaurantMenu objectAtIndex:restPotoCoutI]).imageUrl returnFullImageUrlWithWidth:(DeviceWidth-19)/3]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] sizeFitWidth:YES];
//            [self.mScrollView addSubview:imageView];
//            currentY = imageView.endPointY;
//            
//        }
//    }else
//    {
        for (int i=0; i<restaurantMenu.count; i++) {
          
            UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake((1+(DeviceWidth-2)/3)*(i%3), titleLabel.endPointY+(1+(DeviceWidth-2)/3)*(i/3), (DeviceWidth-2)/3, (DeviceWidth-2)/3)];
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            imageView.clipsToBounds = YES;
            imageView.tag =i;
            UITapGestureRecognizer *Tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagePressed:)];
            [Tap setNumberOfTapsRequired:1];
            [Tap setNumberOfTouchesRequired:1];
            imageView.userInteractionEnabled=YES;
            [imageView addGestureRecognizer:Tap];
            NSString *url;
            if (self.isUseWebP) {
                url = [((IMGMenu *)[restaurantMenu objectAtIndex:i]).imageUrl returnFullWebPImageUrlWithWidth:(DeviceWidth-2)/3];
            }else{
                url = [((IMGMenu *)[restaurantMenu objectAtIndex:i]).imageUrl returnFullImageUrlWithWidth:(DeviceWidth-2)/3];
            }
            UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake( (DeviceWidth-2)/3, (DeviceWidth-2)/3)];
//            __weak typeof(imageView) weakImageView = imageView;
            [imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                image = [image imageByScalingAndCroppingForSize:CGSizeMake( (DeviceWidth-2)/3,(DeviceWidth-2)/3)];
//
//                [weakImageView setImage:image];

            }];

//            [imageView setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] sizeFitWidth:YES];
            [self.mScrollView addSubview:imageView];
            currentY = imageView.endPointY;
//        }
    }
    
    startPointY=currentY;
    
    self.mScrollView.contentSize = CGSizeMake(DeviceWidth, currentY);
    [restaurantMenu removeAllObjects];
}
-(void)addRestaurantMenuSelf{
    currentY=10;
    if(restaurantMenu.count==0) return;
    
    
    Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, currentY, DeviceWidth/2, 30) andTextFont:[UIFont boldSystemFontOfSize:12] andTextColor:[UIColor color333333] andTextLines:1];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.text = @"Restaurant Menu";
    [self.mScrollView addSubview:titleLabel];
    
    Label *numlabel = [[Label alloc]initWithFrame:CGRectMake(DeviceWidth-15-60, currentY, 60, 35) andTextFont:[UIFont fontWithName:FONT_GOTHAM_LIGHT size:12] andTextColor:[UIColor  color999999] andTextLines:1];
    numlabel.textAlignment = NSTextAlignmentLeft;
    numlabel.text = [NSString stringWithFormat:@"%d %@",[menu.menuPhotoCount intValue],([menu.menuPhotoCount intValue]>1)?@"photos":@"photo"];
    
    [self.mScrollView addSubview:numlabel];
    

    for (int i=0; i<restaurantMenu.count; i++) {
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake((1+(DeviceWidth-2)/3)*(i%3), titleLabel.endPointY+(1+(DeviceWidth-2)/3)*(i/3), (DeviceWidth-2)/3, (DeviceWidth-2)/3)];
        imageView.tag =i;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        UITapGestureRecognizer *Tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagePressed:)];
        [Tap setNumberOfTapsRequired:1];
        [Tap setNumberOfTouchesRequired:1];
        imageView.userInteractionEnabled=YES;
        [imageView addGestureRecognizer:Tap];
        NSString *url;
        if (self.isUseWebP) {
            url = [((IMGMenu *)[restaurantMenu objectAtIndex:i]).imageUrl returnFullWebPImageUrlWithWidth:(DeviceWidth-2)/3];
        }else{
            url = [((IMGMenu *)[restaurantMenu objectAtIndex:i]).imageUrl returnFullImageUrlWithWidth:(DeviceWidth-2)/3];
        }
        UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake( (DeviceWidth-2)/3, (DeviceWidth-2)/3)];
        __weak typeof(imageView) weakImageView = imageView;
        [imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            image = [image imageByScalingAndCroppingForSize:CGSizeMake( (DeviceWidth-2)/3,(DeviceWidth-2)/3)];
            
            [weakImageView setImage:image];

        }];

//        [imageView setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] sizeFitWidth:YES];
        [self.mScrollView addSubview:imageView];
        currentY = imageView.endPointY;

    }
    
    startPointY=currentY;
    
    self.mScrollView.contentSize = CGSizeMake(DeviceWidth, currentY);
    [restaurantMenu removeAllObjects];
}
-(void)addBarMenu{
    
    if(barMenu.count==0) return;
    Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, startPointY+10, DeviceWidth/2, 30) andTextFont:[UIFont boldSystemFontOfSize:12] andTextColor:[UIColor color333333] andTextLines:1];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.text = @"Bar Menu";
    titleLabel.tag=1800;
    [self.mScrollView addSubview:titleLabel];
    
    Label* numLabel = [[Label alloc]initWithFrame:CGRectMake(DeviceWidth-15-60, startPointY+10, 60, 35) andTextFont:[UIFont fontWithName:FONT_GOTHAM_LIGHT size:12] andTextColor:[UIColor color999999] andTextLines:1];
    numLabel.textAlignment = NSTextAlignmentLeft;
    numLabel.text = [NSString stringWithFormat:@"%d %@",[menu.menuPhotoCountTypeBar intValue],([menu.menuPhotoCountTypeBar intValue]>1)?@"photos":@"photo"];
    numLabel.tag=1811;
    [self.mScrollView addSubview:numLabel];
    
    currentY = numLabel.endPointY;
    
    float userPhotoHeight=currentY;
    
//    int restBarMenuPhoto;
//    restBarMenuPhoto = 50 - (restaurantMenu.count-50*(serveCout-1));
//
//    if (barMenu.count<=restBarMenuPhoto) {
        for (NSInteger i = 0 ; i<barMenu.count; i++)
        {
            UIImageView *dishImageView = [[UIImageView alloc]initWithFrame:CGRectMake((1+(DeviceWidth-2)/3)*(i%3), titleLabel.endPointY+(1+(DeviceWidth-2)/3)*(i/3), (DeviceWidth-2)/3, (DeviceWidth-2)/3)];
            dishImageView.contentMode = UIViewContentModeScaleAspectFill;
            dishImageView.clipsToBounds = YES;
            dishImageView.tag = i+[menu.menuPhotoCountTypeRestant intValue];
            UITapGestureRecognizer *Tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagePressed:)];
            [Tap setNumberOfTapsRequired:1];
            [Tap setNumberOfTouchesRequired:1];
            dishImageView.userInteractionEnabled=YES;
            [dishImageView addGestureRecognizer:Tap];
            IMGMenu *dish=[barMenu objectAtIndex:i];
            //        dishImageView.image = [UIImage imageNamed:dish.imageUrl];
            [self.mScrollView addSubview:dishImageView];
            
            NSString *urlString;
            if (self.isUseWebP) {
                urlString = [dish.imageUrl returnFullWebPImageUrlWithWidth:(DeviceWidth-2)/3];
            }else{
                urlString = [dish.imageUrl returnFullImageUrlWithWidth:(DeviceWidth-2)/3];
            }
            __weak typeof(dishImageView) weakRestaurantImageView = dishImageView;
//            [dishImageView setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//                [UIView animateWithDuration:1.0 animations:^{
//                    [weakRestaurantImageView setAlpha:1.0];
//                }];
//            }];
            [dishImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [UIView animateWithDuration:1.0 animations:^{
                                        [weakRestaurantImageView setAlpha:1.0];
                                    }];

            }];
            
            currentY = dishImageView.endPointY;
        }
       
    startPointY = currentY;
    self.mScrollView.contentSize = CGSizeMake(DeviceWidth, currentY+80);
    [barMenu removeAllObjects];
}
-(void)addDeliveryMenu
{
    if(deliveryMenu.count==0) return;
    
    Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, startPointY+5, DeviceWidth/2, 30) andTextFont:[UIFont boldSystemFontOfSize:12] andTextColor:[UIColor color333333] andTextLines:1];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.text = @"Delivery Menu";
    [self.mScrollView addSubview:titleLabel];
    
    Label *numlabel = [[Label alloc]initWithFrame:CGRectMake(DeviceWidth-15-60, startPointY+5, 60, 35) andTextFont:[UIFont fontWithName:FONT_GOTHAM_LIGHT size:12] andTextColor:[UIColor color999999] andTextLines:1];
    numlabel.textAlignment = NSTextAlignmentLeft;
    numlabel.text = [NSString stringWithFormat:@"%d %@",[menu.menuPhotoCountTypeDelivery intValue],([menu.menuPhotoCountTypeDelivery intValue]>1)?@"photos":@"photo"];
    
    [self.mScrollView addSubview:numlabel];
    
    
    for (int i=0; i<deliveryMenu.count; i++) {
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake((1+(DeviceWidth-2)/3)*(i%3), titleLabel.endPointY+(1+(DeviceWidth-2)/3)*(i/3), (DeviceWidth-2)/3, (DeviceWidth-2)/3)];
        imageView.tag =i+[menu.menuPhotoCountTypeRestant intValue]+[menu.menuPhotoCountTypeBar intValue];
        UITapGestureRecognizer *Tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagePressed:)];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        [Tap setNumberOfTapsRequired:1];
        [Tap setNumberOfTouchesRequired:1];
        imageView.userInteractionEnabled=YES;
        [imageView addGestureRecognizer:Tap];
        NSString *url;
        if (self.isUseWebP) {
            url = [((IMGMenu *)[deliveryMenu objectAtIndex:i]).imageUrl returnFullWebPImageUrlWithWidth:(DeviceWidth-2)/3];
        }else{
            url = [((IMGMenu *)[deliveryMenu objectAtIndex:i]).imageUrl returnFullImageUrlWithWidth:(DeviceWidth-2)/3];
        }
        UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake( (DeviceWidth-2)/3, (DeviceWidth-2)/3)];
        __weak typeof(imageView) weakImageView = imageView;

        [imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            image = [image imageByScalingAndCroppingForSize:CGSizeMake( (DeviceWidth-2)/3,(DeviceWidth-2)/3)];
            
            [weakImageView setImage:image];

        }];

//        [imageView setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] sizeFitWidth:YES];
        [self.mScrollView addSubview:imageView];
        currentY = imageView.endPointY;
        
    }
    
    startPointY=currentY;
    
    self.mScrollView.contentSize = CGSizeMake(DeviceWidth, currentY);
    [deliveryMenu removeAllObjects];
}

-(void)imagePressed:(UIGestureRecognizer*)gesture{
    NSInteger tag = gesture.view.tag;
//    NSMutableArray *images=[[NSMutableArray alloc] init];
    
    MenuPhotoViewColltroller *menuPhotoViewColltroller = [[MenuPhotoViewColltroller alloc]initWithPhotosArray:photosArray andPhotoTag:tag andRestaurant:restaurant andFromDetail:YES];
    menuPhotoViewColltroller.isUseWebP = self.isUseWebP;
    menuPhotoViewColltroller.menuPhotoCount = menu.menuPhotoCount;
    menuPhotoViewColltroller.amplitudeType = @"Restaurant detail page - menu list";
    [self.navigationController pushViewController:menuPhotoViewColltroller animated:YES];
    
    NSLog(@"图片被点击了%ld",(long)tag);
    
}

- (void)getDishesFromServer:(NSNumber*)restaurantId offset:(NSNumber*)offset_
{
    serveCout=1;
    menuPhotoArray = [[NSMutableArray alloc] init];
    restaurantMenu = [[NSMutableArray alloc] init];
    barMenu = [[NSMutableArray alloc] init];
    deliveryMenu = [[NSMutableArray alloc] init];
    lastPageMenuPhotoArray = [[NSArray alloc] init];
    [MenuPhotoGalleryHandler getMenuGalleryDataFromService:restaurantId offset:offset_ andBlock:^(NSArray *menuList, NSNumber *menuPhotoCount, NSNumber *menuPhotoCountTypeRestant, NSNumber *menuPhotoCountTypeBar, NSNumber *menuPhotoCountTypeDelivery, IMGMenu *menu_) {
        menu = menu_;
        menuPhotoList = [NSMutableArray arrayWithArray:menuList];
        lastPageMenuPhotoArray = [NSArray arrayWithArray:menuPhotoList];
        [self getAllImageUrl];
        [self setPhotoUI];
        [self addRestaurantMenu];
        [self addBarMenu];
        [self addDeliveryMenu];
        [self getNextPageData];
    }];

}

-(void)getNextPageData{
    offset = [NSNumber numberWithInt:([offset intValue]+50)];
    [MenuPhotoGalleryHandler getMenuGalleryDataFromService:restaurant.restaurantId offset:offset andBlock:^(NSArray *menuList, NSNumber *menuPhotoCount, NSNumber *menuPhotoCountTypeRestant, NSNumber *menuPhotoCountTypeBar, NSNumber *menuPhotoCountTypeDelivery, IMGMenu *menu_) {
        lastPageMenuPhotoArray = menuList;
        if (!menuList.count) {
            return ;
        }
        menu = menu_;
        [menuPhotoList addObjectsFromArray:lastPageMenuPhotoArray];
        for (UIView *view in self.mScrollView.subviews)
        {
            [view removeFromSuperview];
        }
        [self getAllImageUrl];
        [self setPhotoUI];
        [self addRestaurantMenu];
        [self addBarMenu];
        [self addDeliveryMenu];
        serveCout++;
        NSLog(@"%d",serveCout);
        if ([menu.menuPhotoCount intValue] - 50*(serveCout+1)>0) {
            [self getNextPageData];
        }

    }];
}
- (void)getSelfDatafromeServer:(NSNumber*)restaurantId userId:(NSNumber*)userId offset:(NSNumber*)offset_{
    serveCout=1;
    menuPhotoArray = [[NSMutableArray alloc] init];
    restaurantMenu = [[NSMutableArray alloc] init];
    barMenu = [[NSMutableArray alloc] init];
    deliveryMenu = [[NSMutableArray alloc] init];
    lastPageMenuPhotoArray = [[NSArray alloc] init];
    [MenuPhotoGalleryHandler getSelfMenuGalleryDataFromService:restaurantId userId:userId  offset:offset andBlock:^(NSArray *menuList, NSNumber *menuPhotoCount, NSNumber *menuPhotoCountTypeRestant, NSNumber *menuPhotoCountTypeBar, NSNumber *menuPhotoCountTypeDelivery, IMGMenu *menu_) {
        menu = menu_;
        menuPhotoList = [NSMutableArray arrayWithArray:menuList];
        lastPageMenuPhotoArray = [NSArray arrayWithArray:menuPhotoList];
        [self getAllImageUrl];
        [self setPhotoUI];
        [self addRestaurantMenuSelf];
    }];


}
 


@end
