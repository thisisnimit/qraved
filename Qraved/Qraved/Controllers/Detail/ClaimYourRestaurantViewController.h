//
//  ClaimYourRestaurantViewController.h
//  Qraved
//
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol ClaimYourRestaurantDelegate <NSObject>
@optional
-(void)claimYourRestaurantGoToSuccessPage;
@end

@interface ClaimYourRestaurantViewController : BaseViewController
@property(nonatomic,strong) NSNumber *restaurantId;
@property(nonatomic,weak)id<ClaimYourRestaurantDelegate> claimYourRestaurantDelegate;
@end
