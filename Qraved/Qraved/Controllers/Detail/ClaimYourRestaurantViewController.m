//
//  ClaimYourRestaurantViewController.m
//  Qraved
//
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "ClaimYourRestaurantViewController.h"
#import "NavigationBarButtonItem.h"
#import "UIDevice+Util.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import "SuccessClaimViewController.h"
#import "LoadingView.h"
#import "UIColor+Hex.h"
#import "DetailDataHandler.h"
#import "IMGUser.h"
#import "UIAlertView+BlocksKit.h"

@interface ClaimYourRestaurantViewController ()<UITextFieldDelegate,UIScrollViewDelegate>{
    UITextField *fullNameTextField;
    UITextField *emailTextField;
    UITextField *phoneNumberTextField;
    UITextField *onFocusTextField;
    
    UIScrollView *mainScrollView;
    
    float prevKeyboardHeight;
    UIButton *contactMeBtn;
    NSInteger sexnum;
    UIImageView *imageview;
    UIImageView *mrsimageview;
    UIImageView *missimageview;

}

@end

@implementation ClaimYourRestaurantViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavagationBack];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.navigationItem setTitle:@"Claim Your Restaurant"];
    [self loadMainView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardShowChange:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHiddenChange:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)setNavagationBack
{
    NavigationBarButtonItem *navigationBarLeftButton = [[NavigationBarButtonItem alloc ]initWithImage:[UIImage imageNamed:NavigationBackImage] andTitle:@"" target:self action:@selector(goToBackViewController) width:43.0f offset:-15-[UIDevice heightDifference]];
    self.navigationItem.leftBarButtonItem = navigationBarLeftButton;
}

-(void)loadMainView {
    //mainScrollView
    mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-64)];
    mainScrollView.showsVerticalScrollIndicator = NO;
    mainScrollView.showsHorizontalScrollIndicator = NO;
    mainScrollView.delegate = self;
    mainScrollView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:mainScrollView];
    //noticeLabel
    UILabel *noticeLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 35, DeviceWidth - 40, 40)];
    noticeLabel.text= L(@"Claim your restaurant. Fill this form and our team will contact you within 3 business days");
    noticeLabel.font=[UIFont fontWithName:DEFAULT_FONT_NAME size:14];
    noticeLabel.textColor=[UIColor colorWithHexString:@"#141823"];
    noticeLabel.textAlignment = NSTextAlignmentCenter;
    noticeLabel.numberOfLines = 0;
    [noticeLabel sizeToFit];
    noticeLabel.backgroundColor = [UIColor whiteColor];
    
    CGRect noticeLabelFrame = noticeLabel.frame;
//    noticeLabelFrame.origin.x = (DeviceWidth - noticeLabel.frame.size.width)/2;
//    noticeLabelFrame.origin.y = 35;
    if ([UIDevice isIphone4Now]) {
        noticeLabelFrame.origin.y =28;

    }
    
//    noticeLabel.frame = noticeLabelFrame;
    
    [mainScrollView addSubview:noticeLabel];
    //sex
    UILabel *title=[[UILabel alloc]initWithFrame:CGRectMake(0, noticeLabel.endPointY+10, DeviceWidth, 30)];
    title.text=@"      CONTACT INFORMATION";
    title.textAlignment=NSTextAlignmentLeft;
    title.font=[UIFont fontWithName:DEFAULT_FONT_NAME size:13];
    title.textColor=[UIColor lightGrayColor];
    title.backgroundColor = [UIColor defaultColor];
    [mainScrollView addSubview:title];
    
    UIButton *mrBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, title.endPointY+10, DeviceWidth/2, 40)];
    mrBtn.layer.borderWidth = 0.3;
    mrBtn.layer.borderColor = [UIColor colorLineGray].CGColor;
     [mainScrollView addSubview:mrBtn];
    UILabel *mrlable=[[UILabel alloc]initWithFrame:CGRectMake(15, 10, 30, 20)];
    mrlable.tag=1001;
    mrlable.text=@"Mr.";
    mrlable.font=[UIFont fontWithName:DEFAULT_FONT_NAME size:14];
    mrlable.textColor=[UIColor colorWithHexString:@"#141823"];
    
    [mrBtn addSubview:mrlable];
    mrlable.userInteractionEnabled=YES;
    UITapGestureRecognizer *msLableTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sexTapAct:)];
    [mrlable addGestureRecognizer:msLableTap];
    
   
    imageview=[[UIImageView alloc]initWithFrame:CGRectMake(mrlable.right +30, 10, 20, 20)];
    imageview.image=[UIImage imageNamed:@"rad_but copy"];
    imageview.userInteractionEnabled=YES;
    imageview.tag=101;
    [mrBtn addSubview:imageview];
    UITapGestureRecognizer *msTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sexTapAct:)];
    [imageview addGestureRecognizer:msTap];
    
    
    UIButton *mrsBtn=[[UIButton alloc]initWithFrame:CGRectMake(mrBtn.right, title.endPointY+10, DeviceWidth/2, 40)];
    mrsBtn.layer.borderWidth = 0.3;
    mrsBtn.layer.borderColor = [UIColor colorLineGray].CGColor;
    [mainScrollView addSubview:mrsBtn];
    
    UILabel *mrslable=[[UILabel alloc]initWithFrame:CGRectMake(15, 10, 30, 20)];
    mrslable.text=@"Mrs.";
    mrslable.tag=1002;
    mrslable.font=[UIFont fontWithName:DEFAULT_FONT_NAME size:14];
    mrslable.textColor=[UIColor colorWithHexString:@"#141823"];
    
    [mrsBtn addSubview:mrslable];
    mrslable.userInteractionEnabled=YES;
    UITapGestureRecognizer *mrsLableTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sexTapAct:)];
    [mrslable addGestureRecognizer:mrsLableTap];
    
    mrsimageview=[[UIImageView alloc]initWithFrame:CGRectMake(mrslable.right +30, 10, 20, 20)];
    mrsimageview.image=[UIImage imageNamed:@"rad_but copy"];
    mrsimageview.tag=102;
    mrsimageview.userInteractionEnabled=YES;
    [mrsBtn addSubview:mrsimageview];
    UITapGestureRecognizer *mrsTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sexTapAct:)];
    [mrsimageview addGestureRecognizer:mrsTap];

    
    
//    UIButton *missBtn=[[UIButton alloc]initWithFrame:CGRectMake(mrsBtn.endPointX+20, title.endPointY+10, 70, 30)];
//    [mainScrollView addSubview:missBtn];
//    missimageview=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
////    missimageview.backgroundColor=[UIColor yellowColor];
//    missimageview.image=[UIImage imageNamed:@"rad_but copy"];
//    missimageview.tag=103;
//    missimageview.userInteractionEnabled=YES;
//    [missBtn addSubview:missimageview];
//    UITapGestureRecognizer *missTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sexTapAct:)];
//    [missimageview addGestureRecognizer:missTap];
//    
//    UILabel *misslable=[[UILabel alloc]initWithFrame:CGRectMake(missimageview.endPointX+10, 0, 30, 20)];
//    misslable.text=@"Miss";
//    misslable.tag=1003;
//    misslable.font=[UIFont fontWithName:DEFAULT_FONT_NAME size:14];
//    misslable.textColor=[UIColor colorWithHexString:@"#141823"];
//    [missBtn addSubview:misslable];
//    misslable.userInteractionEnabled=YES;
//    UITapGestureRecognizer *missLableTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sexTapAct:)];
//    [misslable addGestureRecognizer:missLableTap];
    
    //fullNameLabel
    UILabel *fullNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, mrBtn.bottom+10, 100, 31)];
    fullNameLabel.text= @"Full Name";
    fullNameLabel.font=[UIFont fontWithName:DEFAULT_FONT_NAME size:17];
    fullNameLabel.textColor=[UIColor colorWithHexString:@"#141823"];
    fullNameLabel.textAlignment = NSTextAlignmentLeft;
    fullNameLabel.numberOfLines = 0;
//    [fullNameLabel sizeToFit];
    [mainScrollView addSubview:fullNameLabel];
    //fullNameTextField
    fullNameTextField = [[UITextField alloc] initWithFrame:CGRectMake(fullNameLabel.right +15, mrBtn.bottom+10, DeviceWidth - 145, 31)];
    fullNameTextField.layer.borderColor = [UIColor colorWithHexString:@"#e2e7ec"].CGColor;
    fullNameTextField.layer.borderWidth =1.0;
    fullNameTextField.layer.cornerRadius =5.0;
    fullNameTextField.delegate = self;
    fullNameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [fullNameTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [mainScrollView addSubview:fullNameTextField];
    
//    noticeLabelFrame.origin.x = fullNameTextField.frame.origin.x;
//    noticeLabel.frame = noticeLabelFrame;
    
    //emailLabel
    UILabel *emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, fullNameLabel.bottom +20, 100, 31)];
    emailLabel.text= @"Email";
    emailLabel.font=[UIFont fontWithName:DEFAULT_FONT_NAME size:17];
    emailLabel.textColor=[UIColor colorWithHexString:@"#141823"];
    emailLabel.textAlignment = NSTextAlignmentLeft;
    emailLabel.numberOfLines = 0;
    NSLog(@"%@",NSStringFromCGRect(emailLabel.frame));
//    [emailLabel sizeToFit];
    NSLog(@"%@",NSStringFromCGRect(emailLabel.frame));
    [mainScrollView addSubview:emailLabel];
    //emailTextField
    emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(emailLabel.right +15, fullNameLabel.bottom+20, DeviceWidth - 145, 31)];
    emailTextField.layer.borderColor = [UIColor colorWithHexString:@"#e2e7ec"].CGColor;
    emailTextField.layer.borderWidth =1.0;
    emailTextField.layer.cornerRadius =5.0;
    emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    emailTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    emailTextField.delegate = self;
    [emailTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [mainScrollView addSubview:emailTextField];
    
    
    //phoneNumberLabel
    UILabel *phoneNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, emailLabel.bottom+20, 100, 31)];
    phoneNumberLabel.text= @"Phone";
    phoneNumberLabel.font=[UIFont fontWithName:DEFAULT_FONT_NAME size:17];
    phoneNumberLabel.textColor=[UIColor colorWithHexString:@"#141823"];
    phoneNumberLabel.textAlignment = NSTextAlignmentLeft;
    phoneNumberLabel.numberOfLines = 0;
//    [phoneNumberLabel sizeToFit];
    [mainScrollView addSubview:phoneNumberLabel];
    //phoneNumberTextField
    phoneNumberTextField = [[UITextField alloc] initWithFrame:CGRectMake(phoneNumberLabel.right +15, emailLabel.bottom+20, DeviceWidth - 145, 31)];
    phoneNumberTextField.layer.borderColor = [UIColor colorWithHexString:@"#e2e7ec"].CGColor;
    phoneNumberTextField.layer.borderWidth =1.0;
    phoneNumberTextField.layer.cornerRadius =5.0;
    phoneNumberTextField.keyboardType = UIKeyboardTypePhonePad;
    phoneNumberTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    phoneNumberTextField.delegate = self;
    [phoneNumberTextField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [mainScrollView addSubview:phoneNumberTextField];
    
//    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(10, (DeviceHeight - 94), DeviceWidth - 20, 1)];
//    line.backgroundColor = [UIColor colorWithRed:200/255.0 green:199/255.0 blue:204/255.0 alpha:1];
//    [mainScrollView addSubview:line];
    
    contactMeBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, phoneNumberLabel.bottom +20, 200, 40)];
    contactMeBtn.centerX = mainScrollView.centerX;
    contactMeBtn.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:14];
//    [contactMeBtn setTintColor:[UIColor colorWithHexString:@"#ffffff"]];
    contactMeBtn.backgroundColor = [UIColor whiteColor];
    [contactMeBtn setTitle:@"Contact Me" forState:UIControlStateNormal];
    [contactMeBtn setTitleColor:[UIColor colorWithRed:241/255.0f green:27/255.0f blue:61/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [contactMeBtn addTarget:self action:@selector(contactMeBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [contactMeBtn.layer setMasksToBounds:YES];
    [contactMeBtn.layer setCornerRadius:20];
    contactMeBtn.layer.borderColor = [UIColor colorWithRed:241/255.0f green:27/255.0f blue:61/255.0f alpha:1.0f].CGColor;
    contactMeBtn.layer.borderWidth = 1.0;
    [mainScrollView addSubview:contactMeBtn];
    
//    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, contactMeBtn.endPointY+10);
}
-(void)sexTapAct:(UITapGestureRecognizer*)tap{
    UIView *view=tap.view;
    if (view.tag==101||view.tag==1001) {
        sexnum=1;
        imageview.image=[UIImage imageNamed:@"Checkmark"];
        mrsimageview.image=[UIImage imageNamed:@"rad_but copy"];
        missimageview.image=[UIImage imageNamed:@"rad_but copy"];

    }else if (view.tag==102||view.tag==1002){
    
        sexnum=2;
        mrsimageview.image=[UIImage imageNamed:@"Checkmark"];
        missimageview.image=[UIImage imageNamed:@"rad_but copy"];
        imageview.image=[UIImage imageNamed:@"rad_but copy"];
    
    }
//    else if (view.tag==103||view.tag==1003){
//        sexnum=3;
//        missimageview.image=[UIImage imageNamed:@"Mr"];
//        mrsimageview.image=[UIImage imageNamed:@"rad_but copy"];
//        imageview.image=[UIImage imageNamed:@"rad_but copy"];
//
//
//    }
    
}

-(void)contactMeBtnClick{
    
    [onFocusTextField resignFirstResponder];
    NSString *sex=[NSString stringWithFormat:@"%ld",(long)sexnum];
    if (sexnum==0) {
        [self showErrorMsg:L(@"Please select your salutation")];
        return;
        
    }
    NSString *fullName = [fullNameTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([fullName isBlankString]) {
        [self showErrorMsg:L(@"Please enter your full name.")];
        return;
    }
    NSString *email = [emailTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (![email isValidateEmail] ) {
        [self showErrorMsg:L(@"The format of the email address is invalid.")];
        return;
    }
    NSString *phoneNumber = [phoneNumberTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if (![phoneNumber isClaimYourRestaurantValidatePhone]) {
        [self showErrorMsg:L(@"Incorrect phone number format")];
        return;
    }
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:fullName forKey:@"fullName"];
    [param setObject:email forKey:@"email"];
    [param setObject:phoneNumber forKey:@"phone"];
    [param setObject:sex forKey:@"title"];
    IMGUser * user = [IMGUser currentUser];
    if(user.userId==nil){
        [param setObject:@(0) forKey:@"userId"];
    }else{
        [param setObject:user.userId forKey:@"userId"];
    }
    [param setObject:self.restaurantId forKey:@"restaurantId"];

    contactMeBtn.userInteractionEnabled = NO;
    [[LoadingView sharedLoadingView] startLoading];
    [DetailDataHandler claimYourRestaurant:param block:^{
        contactMeBtn.userInteractionEnabled = YES;
        [[LoadingView sharedLoadingView] stopLoading];
        [self.navigationController popViewControllerAnimated:NO];
        [[Amplitude instance] logEvent:@"CL - Contact Me in Restaurant Claim" withEventProperties:@{@"Source":@"Restaurant Claim Page"}];
        if ( (self.claimYourRestaurantDelegate != nil) && [self.claimYourRestaurantDelegate respondsToSelector:@selector(claimYourRestaurantGoToSuccessPage)]) {
            [self.claimYourRestaurantDelegate claimYourRestaurantGoToSuccessPage];
        }
    } failure:^(NSString *errorMsg) {
        if (errorMsg!=nil) {
            [self showErrorMsg:errorMsg];
        }
        contactMeBtn.userInteractionEnabled = YES;
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

-(void)showErrorMsg:(NSString*)errorMsg{
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:ALERT_TITLE message:errorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
  
    [alert show];
}

#pragma mark keyboard notification
-(void)keyboardHiddenChange:(NSNotification *)notification{
    NSDictionary *userInfo = [notification userInfo];
    
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    prevKeyboardHeight = 0;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.frame.size.width, mainScrollView.frame.size.height);
    mainScrollView.contentOffset = CGPointMake(0, 0);
    
    [UIView commitAnimations];
}

-(void)keyboardShowChange:(NSNotification *)notification{
    NSDictionary *userInfo = [notification userInfo];
    
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    CGFloat keyboardOffset = keyboardEndFrame.size.height+65;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.contentSize.width, mainScrollView.contentSize.height-prevKeyboardHeight+keyboardOffset);
    mainScrollView.contentOffset = CGPointMake(mainScrollView.contentOffset.x, onFocusTextField.frame.origin.y-30);
    prevKeyboardHeight = keyboardOffset;
    
    [UIView commitAnimations];
}
#pragma mark textfield delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    onFocusTextField = textField;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
#pragma mark scrollView delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [onFocusTextField resignFirstResponder];
}

 
@end
