//
//  MenuDetailViewController.m
//  Qraved
//
//  Created by Laura on 14-8-1.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "MenuDetailViewController.h"
#import "TYAttributedLabel.h"

#import "Label.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "UIColor+Helper.h"
#import "UIViewController+Helper.h"
#import "NavigationBarButtonItem.h"
#import "LoadingView.h"
#import "RestaurantHandler.h"
#import "IMGMenu.h"
#import "IMGUser.h"

@interface MenuDetailViewController ()
{
    UIScrollView *_scrollView;
    UIView *menuNameVIew;
    NSNumber *sectionId;
}
@property(nonatomic,copy)NSArray *menusArray;
@property(nonatomic,copy)NSString *restaurantTitle;
@property(nonatomic,copy)NSString *sectionTypeName;
@property(nonatomic,copy)NSString *sectionName;
@end

@implementation MenuDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initMenusArray:(NSArray*)menus andRestaurantName:(NSString *)resName andSectionTypeName:(NSString *)sectionTypeName andSectionName:(NSString *)sectionName
{
    if (self = [super init]) {
        self.menusArray = menus;
        self.restaurantTitle = resName;
        self.sectionTypeName = sectionTypeName;
        self.sectionName = sectionName;
    }
    return self;
}

-(id)initWithSectionId:(NSNumber*)_sectionId andRestaurantName:(NSString *)resName andSectionTypeName:(NSString *)sectionTypeName andSectionName:(NSString *)sectionName{

    if (self = [super init]) {
        sectionId=_sectionId;
        self.restaurantTitle = resName;
        self.sectionTypeName = sectionTypeName;
        self.sectionName = sectionName;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self setBackBarButtonOffset30];
    [self addMenuNameView];
    [[LoadingView sharedLoadingView] startLoading];
    IMGUser *user=[IMGUser currentUser];
    NSDictionary *params=nil;
    if(user && user.userId){
        params=@{@"sectionId":sectionId,@"userId":user.userId,@"menuUnsign":[NSNumber numberWithInt:1]};    
    }else{
        params=@{@"sectionId":sectionId};
    }
    [RestaurantHandler getMenusFromServer:params andBlock:^(BOOL status,NSArray *menuArrary){
        self.menusArray = menuArrary;
        [[LoadingView sharedLoadingView] stopLoading];
        [self addDetailMenuView];
    }];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationItem setTitle:self.restaurantTitle];
    [super viewWillAppear:YES];
}
-(void)addMenuNameView
{
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    [self.view addSubview:_scrollView];
    menuNameVIew = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 80)];
    [_scrollView addSubview:menuNameVIew];

    Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(15, 22, DeviceWidth-30, 8) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:10] andTextColor:[UIColor color999999] andTextLines:1];
    titleLabel.text = [self.sectionTypeName uppercaseString];
    
    [menuNameVIew addSubview:titleLabel];
    
    Label *nameLabel = [[Label alloc]initWithFrame:CGRectMake(15, titleLabel.endPointY+10 , DeviceWidth-30,26) andTextFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:20] andTextColor:[UIColor color333333] andTextLines:1];
    nameLabel.text = self.sectionName;
    [menuNameVIew addSubview:nameLabel];
    _scrollView.contentSize = CGSizeMake(DeviceWidth, menuNameVIew.endPointY);
    
    UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, nameLabel.endPointY+20, DeviceWidth, 1)];
    lineImage.backgroundColor = [UIColor colorEDEDED];
    [_scrollView addSubview:lineImage];
}
-(void)addDetailMenuView
{
    CGFloat startY = 0;

    for (int i=0; i<self.menusArray.count; i++) {
        IMGMenu *menu = [self.menusArray objectAtIndex:i];

        NSMutableString *str = [NSMutableString stringWithString:menu.title];
        NSString *menuDescription=menu.descriptionStr;
        if(![menuDescription isEqualToString:@"type description"]){
            [str appendFormat:@" , %@",[menu.descriptionStr filterHtml]];
        }
        
        CGSize titleSize = [str sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_ITALIC size:13] constrainedToSize:CGSizeMake(DeviceWidth-30, 200) lineBreakMode:NSLineBreakByWordWrapping];
        
        TYAttributedLabel *titleLabel = [[TYAttributedLabel alloc] initWithFrame:CGRectMake(15, 15+startY+menuNameVIew.endPointY, DeviceWidth-30, titleSize.height)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.text = str;
        titleLabel.font =[UIFont fontWithName:FONT_OPEN_SANS_ITALIC size:13];
        [_scrollView addSubview:titleLabel];
        
        NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:str];
        NSRange range = [str rangeOfString:[NSString stringWithFormat:@" %@",[menu.descriptionStr filterHtml]]];
        [AttributedStr addAttributes:@{NSForegroundColorAttributeName:[UIColor colorAAAAAA],NSFontAttributeName:[UIFont fontWithName:FONT_OPEN_SANS_ITALIC size:13]} range:NSMakeRange(range.location, range.length)];
        titleLabel.attributedText = AttributedStr;

        CGFloat position=titleLabel.endPointY;
        CGSize priceSize;
        Label *priceLabel;
        if(menu.price){
            priceSize = [menu.price sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13] constrainedToSize:CGSizeMake(DeviceWidth-30, 100) lineBreakMode:NSLineBreakByWordWrapping];
            priceLabel = [[Label alloc]initWithFrame:CGRectMake(15, titleLabel.endPointY, DeviceWidth-30, priceSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13] andTextColor:[UIColor color999999] andTextLines:0];
            [_scrollView addSubview:priceLabel];
            
            NSString *priceString = [NSString stringWithFormat:@"%@",menu.price];
            float price = [priceString floatValue];
            
            if(![priceString isEqualToString:@"0.0"] && ![priceString isEqualToString:@""] && ![priceString isEqualToString:@"0"] && ![[NSString stringWithFormat:@"%.4f",price] isEqualToString:@"9999.9912"]) {
                NSString *priceStr = [[NSString stringWithFormat:@"%.f",price]toFormatNumberString];
                [priceLabel setText:[NSString stringWithFormat:@"Rp. %@",priceStr]];
            }else{
                [priceLabel setText:@""];
            }
            position=priceLabel.endPointY;
        }

        if(menu.hasNewMenuSign){
            UIImageView *imageView=[[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-LEFTLEFTSET-32-5,titleLabel.endPointY-8,32,32)];
            imageView.image=[UIImage imageNamed:@"newflag"];
            [_scrollView addSubview:imageView];
        }
        
        startY += titleSize.height+priceSize.height+15;
        [_scrollView setContentSize:CGSizeMake(DeviceWidth, position+80)];
    }
}

 

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
