//
//  DetailViewController.h
//  Qraved
//
//  Copyright (c) 2013 Imaginato. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "BaseViewController.h"
#import "PostDataHandler.h"
#import "HMSegmentedControl.h"
#import <MapKit/MapKit.h>
#import "UIPopoverListView.h"
#import "EScrollerView.h"
#import <MessageUI/MessageUI.h>
#import "IMGRestaurant.h"
#import "ReviewView.h"
#import "ZYQAssetPickerController.h"
#import "IMGUserReview.h"
#import "UILikeCommentShareView.h"

@interface DetailViewController : BaseViewController<MKMapViewDelegate,UIScrollViewDelegate,UIAlertViewDelegate,UIPopoverListViewDataSource, UIPopoverListViewDelegate,EScrollerViewDelegate,MFMailComposeViewControllerDelegate,UIImagePickerControllerDelegate,ZYQAssetPickerControllerDelegate,UINavigationControllerDelegate,reviewDelegate,LikeCommentShareDelegate>

@property(nonatomic,retain) NSDate *bookDate;
@property(nonatomic,retain) NSString *bookTime;
@property(nonatomic,retain) NSNumber *pax;
@property(nonatomic,assign) BOOL isFromAppDelegate;
@property(nonatomic,assign) BOOL isFromURL;
@property(nonatomic,assign) BOOL isGoMenuSection;


@property(nonatomic,retain) NSString *amplitudeType;
@property(nonatomic,retain) NSString *amplitudeTypePage;
@property(nonatomic,retain) NSNumber *amplitudeTypePageId;



-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant;
-(id)initWithRestaurantId:(NSNumber *)restaurantId;

@end
