//
//  ReadAboutMoreViewController.m
//  Qraved
//
//  Created by Laura on 14-8-27.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "ReadAboutMoreViewController.h"

#import "Label.h"
#import "UIColor+Helper.h"
#import "UIViewController+Helper.h"
#import "UIImageView+Helper.h"
#import "NSString+Helper.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "UIColor+Hex.h"
#import "IMGTag.h"
#import "TagButtonGridView.h"
#import "WebViewController.h"
//#import "MixpanelHelper.h"
#import "MoreInfoViewController.h"
#import "TrackHandler.h"
#import "DetailDataHandler.h"
#import "LoadingView.h"
#define CELLOFFSET 6

#define TITLE_MARGIN 15
#define SCORLLVIEW_Y 23


@interface ReadAboutMoreViewController ()<UIScrollViewDelegate>
{
    IMGRestaurant *_restaurant;
    UIScrollView *_scrollView;
    CGFloat _currentY;

}
@end

@implementation ReadAboutMoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithRestaurant:(IMGRestaurant *)restaurant
{
    if (self = [super init]) {
        
        _restaurant = restaurant;
    }
    
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setBackBarButtonOffset30];
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    _scrollView = [[UIScrollView alloc] init];
    _scrollView.delegate = self;
    _scrollView.frame = self.view.bounds;
    _scrollView.bounces = YES;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_scrollView];

   
    [[LoadingView sharedLoadingView] startLoading];
    
    [DetailDataHandler getMoreInfoFromService:_restaurant andBlock:^{
        
        [self addAboutView];
        [self addInfoView];
        [self addMoreInfo];
        [self addopeningHoursView];
        [self addFeaturesView];
        
        [[LoadingView sharedLoadingView] stopLoading];
        
    }];
    
//    CGSize expectSize = [_readMoreString sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] constrainedToSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
//    Label *aboutLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, LEFTLEFTSET, DeviceWidth-2*LEFTLEFTSET, expectSize.height*1.4) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor color666666] andTextLines:0];
//    aboutLabel.text = _readMoreString;
//    [self.view addSubview:aboutLabel];
//    
//    
//    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
//    paragraphStyle.lineSpacing = 6;
//    
//    NSDictionary *attributes = @{ NSFontAttributeName:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13], NSParagraphStyleAttributeName:paragraphStyle};
//    aboutLabel.attributedText = [[NSAttributedString alloc]initWithString:aboutLabel.text attributes:attributes];

    //标题下阴影
//    UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:0 andHeight:4];
//    [self.view addSubview:shadowImageView];
}
- (void)addInfoView
{
    UIView *infoView = [[UIView alloc] init];;
    NSMutableArray *btnNamesArr = [[NSMutableArray alloc] init];
    NSMutableString *phoneNumber = [[NSMutableString alloc]initWithString:_restaurant.phoneNumber];
    for (int i=(int)phoneNumber.length-1; i>-1; i--) {
        char c = [phoneNumber characterAtIndex:i];
        if ([[NSString stringWithFormat:@"%c",c] isEqualToString:@"+"] || [[NSString stringWithFormat:@"%c",c] isEqualToString:@"("] || [[NSString stringWithFormat:@"%c",c] isEqualToString:@")"] || [[NSString stringWithFormat:@"%c",c] isEqualToString:@" "]) {
            [phoneNumber deleteCharactersInRange:NSMakeRange(i, 1)];
        }
    }
    if (_restaurant.phoneNumber.length == 0 || [phoneNumber integerValue] == 0) {
        
    }else{
        NSString *str = [[NSString alloc] initWithFormat:@"100%@",_restaurant.phoneNumber];
        [btnNamesArr addObject:str];
    }
    
    if ([_restaurant.website isKindOfClass:[NSString class]] && _restaurant.website.length>0) {
        NSString *str = [[NSString alloc] initWithFormat:@"102%@",[_restaurant.website filterHtml]];
        [btnNamesArr addObject:str];
    }
    CGFloat currentPointY = 0;
    
    if (btnNamesArr.count>0)
    {
        for (NSString *str in btnNamesArr)
        {
            NSRange range;
            range.location = 0;
            range.length = 3;
            NSString *btnTagStr = [str substringWithRange:range];
            NSString *btnNameStr = [str substringFromIndex:3];
            
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake(0, currentPointY, DeviceWidth, 30);
            btn.tag = [btnTagStr intValue];
            [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
            UILabel *label = [[UILabel alloc] init];
            label.text = btnNameStr;
//            label.font = [UIFont fontWithName:nil size:13];
            label.textColor = [UIColor color222222];
            label.frame = CGRectMake(50, 0, DeviceWidth-50, 30);
            [btn addSubview:label];
            
            UIImageView *btnImage = [[UIImageView alloc]initWithFrame:CGRectMake(15, 7, 16, 16)];
            btnImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"btnImage%@",btnTagStr]];
            [btn addSubview:btnImage];
            
            [infoView addSubview:btn];
            currentPointY = currentPointY + 35;
        }

    }
    else
    {
        return;
    }
    
    
    infoView.frame = CGRectMake(0, _currentY+8, DeviceWidth, currentPointY);
    [_scrollView addSubview:infoView];
    _currentY = infoView.endPointY;
    _scrollView.contentSize = CGSizeMake(DeviceWidth, _currentY);
}
- (void)btnClick:(UIButton *)btn
{
    switch (btn.tag)
    {
        case 100:
        {
            IMGUser *user = [IMGUser currentUser];
            NSNumber *userId = [[NSNumber alloc] init];
            if (user.userId != nil)
            {
                userId = user.userId;
            }
            else
                userId = [NSNumber numberWithInt:0];
            
            [TrackHandler trackWithUserId:userId andRestaurantId:_restaurant.restaurantId andContent:[NSDictionary dictionaryWithObject:@"0" forKey:@"type"]];
            
//            [MixpanelHelper trackTapPageCallButtonPropertiesWithRestaurant:_restaurant];
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"" message:_restaurant.phoneNumber delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
            alertView.tag=1;
            [alertView show];
        }
            break;
            
        case 102:
        {
            WebViewController *webViewController = [[WebViewController alloc]init];
            webViewController.webSite = _restaurant.website;
            webViewController.title = _restaurant.title;
            webViewController.fromDetail = YES;
            //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:restaurant.website]];
            [self.navigationController pushViewController:webViewController animated:YES];
        }
            break;
        default:
            break;
    }


}
- (void)addAboutView
{
    UIView *aboutView = [[UIView alloc] init];
    Label *titleLabel=[[Label alloc] initWithFrame:CGRectMake(LEFTLEFTSET, 15, DeviceWidth-2*LEFTLEFTSET, 20) andTextFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:20] andTextColor:[UIColor color222222] andTextLines:0];
    [titleLabel setText:@"About"];

  
    
    UILabel *webViewLabel = [[UILabel alloc] init];
    webViewLabel.text = [_restaurant.descriptionStr removeHTML];
    webViewLabel.font = [UIFont systemFontOfSize:13];
    webViewLabel.numberOfLines = 0;
    CGSize size = [webViewLabel.text sizeWithFont:webViewLabel.font constrainedToSize:CGSizeMake(DeviceWidth-LEFTLEFTSET-LEFTLEFTSET, 10000) lineBreakMode:NSLineBreakByWordWrapping];
    webViewLabel.frame = CGRectMake(LEFTLEFTSET, titleLabel.endPointY+5, size.width, size.height);
    
    [aboutView addSubview:titleLabel];
    //[aboutView addSubview:webView];
    
    
    [aboutView addSubview:webViewLabel];
    aboutView.frame = CGRectMake(0, 0, DeviceWidth, webViewLabel.endPointY);

    _currentY = aboutView.endPointY+1;

    [_scrollView addSubview:aboutView];
    [self addLineImage:aboutView.endPointY withCurrentView:aboutView];
}
- (void)addLineImage:(CGFloat)currentPointY withCurrentView:(UIView *)currentView
{
    UIImageView *lineImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, currentPointY+5, DeviceWidth, 1)];
    lineImage3.image = [UIImage imageNamed:@"CutOffRule"];
    currentPointY = lineImage3.endPointY;
    [currentView addSubview:lineImage3];
}

-(void)addFeaturesView{
   
    NSMutableArray *featureArrary = [[NSMutableArray alloc]initWithCapacity:0];
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT name FROM IMGTag t left join IMGRestaurantTag rt on t.tagId=rt.tagId WHERE rt.restaurantId = '%@' and t.type='Features' ORDER BY t.name;",_restaurant.restaurantId] successBlock:^(FMResultSet *resultSet) {
        while([resultSet next]){
            [featureArrary addObject:[resultSet stringForColumn:@"name"]];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"addFeaturesView error when get datas:%@",error.description);
    }];
    
    //SELECT * FROM IMGTag where status=1 and type='Features';
    NSMutableArray *allFeatureArrary = [[NSMutableArray alloc]initWithCapacity:0];
    //    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT name FROM IMGTag t left join IMGRestaurantTag rt on t.tagId=rt.tagId WHERE rt.restaurantId = '%@' and t.type='Features' ORDER BY t.name;",restaurant.restaurantId] successBlock:^(FMResultSet *resultSet) {
    //        while([resultSet next]){
    //            [allFeatureArrary addObject:[resultSet stringForColumn:@"name"]];
    //        }
    //        [resultSet close];
    //    }failureBlock:^(NSError *error) {
    //        NSLog(@"addFeaturesView error when get datas:%@",error.description);
    //    }];
    [allFeatureArrary addObjectsFromArray:featureArrary];
    
    UIView * featuresView = [[UIView alloc]init];
    [_scrollView addSubview:featuresView];
    
    UIView *openView  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 26)];
    [featuresView addSubview:openView];
    openView.backgroundColor = [UIColor colorFBFBFB];
    
    UIView *featuresLabel = [[UIView alloc]initDetailFeatureViewWithOrignY:0 andText:@"Features"];
    [openView addSubview:featuresLabel];
    
    
    UIView *featureImage = [[UIView alloc]initWithFrame:CGRectMake(0, featuresLabel.endPointY, DeviceWidth, 20)];
    [featuresView addSubview:featureImage];
    
    
    UIView *featureView = [[UIView alloc]init];
    [featuresView addSubview:featureView];
    
    CGFloat startY=0;
    for (int i=0; i<allFeatureArrary.count; i++) {
        CGSize size = [[allFeatureArrary objectAtIndex:i] sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(DeviceWidth-30, 200) lineBreakMode:NSLineBreakByTruncatingTail];
        
        Label *featureLabel = [[Label alloc]initWithFrame:CGRectMake(15, startY, DeviceWidth-30, size.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor color222222] andTextLines:1];
        featureLabel.text = [allFeatureArrary objectAtIndex:i];
        
        UIImageView *retimageV = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-35, startY, 13, 10)];
        retimageV.image = [UIImage imageNamed:@"Checkmark"];
        [featureView addSubview:retimageV];
        
//        Label *retLabel = [[Label alloc]initWithFrame:CGRectMake(DeviceWidth-60, startY, 40, size.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor blackColor] andTextLines:1];
//        retLabel.textAlignment = NSTextAlignmentCenter;
//        [featureView addSubview:retLabel];
//        retLabel.text = @"YES";
//        retLabel.textColor = [UIColor color3BAF24];
        //        if([featureArrary indexOfObject:[allFeatureArrary objectAtIndex:i]]==NSNotFound){
        //            retLabel.text = @"NO";
        //            featureLabel.textColor = [UIColor colorCCCCCC];
        //            retLabel.textColor = [UIColor colorC2060A];
        //        }else{
        //            retLabel.text = @"YES";
        //            retLabel.textColor = [UIColor color3BAF24];
        //        }
        
        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, featureLabel.endPointY+5, DeviceWidth, 1)];
        lineImage.image = [UIImage imageNamed:@"CutOffRule"];
        [featureView addSubview:lineImage];
        
        [featureView addSubview:featureLabel];
        startY = featureLabel.endPointY+10;
    }
    featureView.frame = CGRectMake(0, featureImage.endPointY-10, DeviceWidth, startY);
    featuresView.frame = CGRectMake(0, _currentY, DeviceWidth, featureView.endPointY);
    _currentY = featuresView.endPointY;
    [_scrollView setContentSize:CGSizeMake(DeviceWidth, _currentY+80)];
    
}

-(void)addopeningHoursView
{
    NSMutableArray *openingHours = [[NSMutableArray alloc]init];
    [openingHours addObjectsFromArray:[_restaurant openingHours]];
    if(openingHours==nil || openingHours.count==0){
        return;
    }
    
    
    UIView * openingHoursView = [[UIView alloc]init];
    [_scrollView addSubview:openingHoursView];
    
    UIView *openView  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 26)];
    [openingHoursView addSubview:openView];
    openView.backgroundColor = [UIColor colorFBFBFB];
    
    UIView *openingHoursLabel = [[UIView alloc]initDetailFeatureViewWithOrignY:0 andText:@"Opening Hours"];
    [openingHoursView addSubview:openingHoursLabel];
    
    
    float startY = 12+openingHoursLabel.endPointY;
    for (int i=0; i<openingHours.count; i++) {
        UILabel *hoursLabel = [[UILabel alloc]initWithFrame:CGRectMake(15, startY, DeviceWidth, 20)];
        hoursLabel.text = [openingHours objectAtIndex:i];
        hoursLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
        hoursLabel.textColor = [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1];
        hoursLabel.alpha = 0.8;
        [openingHoursView addSubview:hoursLabel];
        startY = hoursLabel.endPointY+5;
    }
    
    openingHoursView.frame = CGRectMake(0, _currentY, DeviceWidth, startY+12);
    _currentY = openingHoursView.endPointY;
    [_scrollView setContentSize:CGSizeMake(DeviceWidth, _currentY+80)];
    
    //标题下阴影
    UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:0 andHeight:4];
    [self.view addSubview:shadowImageView];
}
- (void)addWebSiteView
{
    if ([_restaurant.website isKindOfClass:[NSString class]] && _restaurant.website.length>0)
    {
        
    }
}

- (void)addMoreInfo
{
    UIView *moreInfoView = [[UIView alloc] init];
    UIView *titleLabel = [[UIView alloc] initDetailFeatureViewWithOrignY:0 andText:@"More Info"];
    [moreInfoView addSubview:titleLabel];
    __block float startY = 12+titleLabel.endPointY;

    NSMutableArray *infoArr=[[NSMutableArray alloc] initWithCapacity:3];
    [[DBManager manager] selectWithSql:[NSString stringWithFormat:@"SELECT GROUP_CONCAT(t.name,\", \") as name,t.type,case t.type when \"Restaurant Type\" then 2 when \"Food Type\" then 1 when \"Payment\" then 3 end typename FROM IMGTag t left join IMGRestaurantTag rt on t.tagId=rt.tagId WHERE rt.restaurantId = '%@' and t.type in ('Food Type','Restaurant Type','Payment') GROUP BY t.type ORDER BY typename asc;",_restaurant.restaurantId] successBlock:^(FMResultSet *resultSet) {
        while([resultSet next]){
            NSString *name=[resultSet stringForColumn:@"name"];
            NSString *type=[resultSet stringForColumn:@"type"];
            if([type isEqualToString:@"Restaurant Type"]){
                [infoArr addObject:[NSString stringWithFormat:@"%@: %@",@"Type",name]];
            }else if([type isEqualToString:@"Food Type"]){
                [infoArr addObject:[NSString stringWithFormat:@"%@: %@",@"Food",name]];
            }else if([type isEqualToString:@"Payment"]){
                [infoArr addObject:[NSString stringWithFormat:@"%@: %@",@"Payment",name]];
            }
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"addFeaturesView error when get datas:%@",error.description);
    }];
    [infoArr enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
        UIFont *font=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
        UILabel *infoLabel = [[UILabel alloc] init];
        CGSize size=[obj sizeWithFont:font constrainedToSize:CGSizeMake(DeviceWidth-30,1000) lineBreakMode:NSLineBreakByCharWrapping];
        infoLabel.frame=CGRectMake(15,startY,DeviceWidth-30,size.height);
        infoLabel.text = [NSString stringWithFormat:@"%@",obj];
        infoLabel.font = font;
        infoLabel.numberOfLines=0;
        infoLabel.textColor = [UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1];
        infoLabel.alpha = 0.8;
        [moreInfoView addSubview:infoLabel];
        startY = infoLabel.endPointY+5;
    }];

    moreInfoView.frame = CGRectMake(0, _currentY, DeviceWidth, startY+12);
    [_scrollView addSubview:moreInfoView];
    _currentY = moreInfoView.endPointY;
    [_scrollView setContentSize:CGSizeMake(DeviceWidth, _currentY+80)];
    
    //标题下阴影
    UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:0 andHeight:4];
    [self.view addSubview:shadowImageView];
}


-(NSString *)retaurantCurrentWeekDayDispalyTime
{
    NSDate *currentDate = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents*comps1;
    comps1 = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth |NSCalendarUnitDay | NSCalendarUnitWeekday) fromDate:currentDate];
    NSInteger currentWeekDay = [comps1 weekday]; // 星期几（注意，周日是“1”，周一是“2”。。。。）
    return [_restaurant dispalyTimeAtWeekday:currentWeekDay weekdayName:@"Today"];
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = _restaurant.title;
    self.navigationController.navigationBarHidden = NO;
    [super viewWillAppear:YES];
}
 

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
