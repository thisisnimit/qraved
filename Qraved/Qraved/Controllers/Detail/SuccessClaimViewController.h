//
//  SuccessClaimViewController.h
//  Qraved
//
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface SuccessClaimViewController : BaseViewController
@property(nonatomic,copy)   NSString * restaurantTitle;
@end
