//
//  CardDetailViewController.h
//  Qraved
//
//  Created by lucky on 16/3/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "IMGRestaurant.h"
#import "CDPDelegate.h"
#import "UILikeCommentShareView.h"
#import "UICommentInputView.h"

#define TITLE_LABEL_Y 13
#define TITLE_LABEL_HEIGHT 20
#define TITLE_LABEL_FONT 15

#define GAP_BETWEEN_TITLE_AND_CUISINE 1

#define CUISINE_DISTRICT_LABEL_HEIGHT 20

#define GAP_BETWEEN_CUISINE_AND_REVIEW 3

#define CELL_RATING_STAR_WIDTH 10

#define RESTAURANT_IMAGE_SIZE 70


@protocol CardDetailViewControllerDelegate

- (void)gotoRestaurantWithRestaurantId:(NSNumber *)restaurantId;

- (void)homeCardTableViewCell:(UITableViewCell*)cell shareButtonTapped:(UIButton*)button entity:(id)entity;
- (void)homeCardTableViewCell:(UITableViewCell*)cell likeButtonTapped:(UIButton*)button entity:(id)entity;
- (void)homeCardTableViewCell:(UITableViewCell*)cell newCommentPosted:(id)comment;
- (void)homeCardTableViewCell:(UITableViewCell*)cell likeCommentShareView:(UIView*)likeCommentShareView commentInputView:(UIView *)commentInputView inputViewExpand:(UITextView *)inputTextView commentInputViewYOffset:(CGFloat)commentInputViewYOffset;
- (void)homeCardTableViewCell:(UITableViewCell*)cell likeCommentShareView:(UIView*)likeCommentShareView commentInputView:(UIView *)view inputViewDidBeginEditing:(UITextView *)textView;

@end

@interface CardDetailViewController : BaseViewController<LikeCommentShareDelegate>

@property(nonatomic,retain) NSIndexPath *fromCellIndexPath;
@property (nonatomic,assign) id<CDPDelegate>cdpDelegate;

@property (nonatomic,retain) UILikeCommentShareView *likeCommentShareView;
@property (nonatomic,retain) UIScrollView *scrollView;
@property (nonatomic,retain) UICommentInputView *commentInputView;
@property (nonatomic,assign) float currentPointY;
@property (nonatomic,assign) float currentOffsetY;
@property (nonatomic,assign) float previousKeyboardHeight;

@property (nonatomic,assign) id<CardDetailViewControllerDelegate>cardDetailViewControllerDelegate;

-(void)keyboardChange:(NSNotification *)notification;

@end
