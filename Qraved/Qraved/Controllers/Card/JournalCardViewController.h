//
//  JournalCardViewController.h
//  Qraved
//
//  Created by lucky on 16/3/14.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "CardDetailViewController.h"
#import "IMGRestaurant.h"
#import "UILikeCommentShareView.h"
#import "IMGMediaComment.h"
#import "CDPDelegate.h"

@interface JournalCardViewController : CardDetailViewController

@property(nonatomic,strong) IMGMediaComment *journal;
-(id)initWithJournal:(IMGMediaComment *)paramJournal;
@end
