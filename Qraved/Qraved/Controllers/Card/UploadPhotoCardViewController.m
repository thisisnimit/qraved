//
//  UploadPhotoCardViewController.m
//  Qraved
//
//  Created by lucky on 16/3/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "UploadPhotoCardViewController.h"
#import "UIConstants.h"
#import "DLStarRatingControl.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"
#import "IMGRestaurant.h"
#import "UILabel+Helper.h"
#import "IMGDish.h"
#import "UIButton+WebCache.h"
#import "CXAHyperlinkLabel.h"
#import "NSString+CXAHyperlinkParser.h"
#import "IMGUser.h"
#import "UILikeCommentShareView.h"
#import "LoadingView.h"
#import "UICommentInputView.h"
#import "AppDelegate.h"
#import "UploadPhotoHandler.h"
#import "DetailViewController.h"
#import "RestaurantActivityItemProvider.h"
#import "ReviewHandler.h"
#import "Date.h"
#import "TwitterActivityItemProvider.h"
#import "ReviewPublishViewController.h"
#import "AlbumViewController.h"
#import "OtherProfileViewController.h"
#import "LikeView.h"
#import "HomeUtil.h"

@interface UploadPhotoCardViewController ()<UIScrollViewDelegate,LikeCommentShareDelegate,CommentInputViewDelegate,CardPhotosViewDelegate,LikeListViewDelegate,TYAttributedLabelDelegate>
{
    UIButton *avatarLinkButton;
//    CXAHyperlinkLabel *titleLabel;
    TYAttributedLabel *titleLabel;
    UILabel *reviewAndPhotoCountLabel;
    UILabel *timeLable;
    
    UIButton *writeReviewBtn;
    
    UILabel *likeLabel;
    UILabel *commentLabel;
    
//    CardPhotosView *cardPhotosView;
    IMGDish *dish;
    NSMutableArray *commentsList;

    NSArray *dishArr;
    IMGUser *dishUser;
    IMGRestaurant *restaurant;
    UIView *mainView;
    UIImageView *testimage;
    
    NSMutableArray* likeListArr;
    LikeView*  likepopView;
}
@end

@implementation UploadPhotoCardViewController
- (id)initWithRestaurant:(IMGRestaurant *)paramRestaurant andDishList:(NSArray *)dishList andUser:(IMGUser *)user
{
    
    if(self= [super init]){
        restaurant = paramRestaurant;
        dishArr = [NSArray arrayWithArray:dishList];
        dishUser = user;
    }
    return self;
    
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    if (!self.isFromNotification) {
        NSURL *urlString ;
        dishUser.avatar = [dishUser.avatar stringByReplacingPercentEscapesUsingEncoding:
                           NSUTF8StringEncoding];
        
        if ([dishUser.avatar hasPrefix:@"http"])
        {
            urlString = [NSURL URLWithString:dishUser.avatar];
        }
        else
        {
            urlString = [NSURL URLWithString:[dishUser.avatar returnFullImageUrl]];
        }
        
        dish = [dishArr firstObject];
        NSString *path_sandox = NSHomeDirectory();
        //设置一个图片的存储路径
        NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/avastar.png"];
        UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
        image =[image cutCircleImage];
        if (image&&[dishUser.userId intValue] == [[IMGUser currentUser].userId intValue]) {
            [avatarLinkButton setImage:image forState:UIControlStateNormal];
        }else{
            __weak UIButton *weakAvatarLinkButton = avatarLinkButton;
            [avatarLinkButton sd_setImageWithURL:urlString forState:UIControlStateNormal placeholderImage:[[UIImage imageNamed:@"headDefault.jpg"] cutCircleImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (image.size.height>0&&image.size.width>0) {
                                        image = [image cutCircleImage];
                                        [weakAvatarLinkButton setImage:image forState:UIControlStateNormal];
                                    }
            }];
        }

    }
    
    if (self.amplitudeType!=nil) {
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:self.amplitudeType forKey:@"Origin"];
        [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:dishUser.userId forKey:@"UploaderUser_ID"];
        NSString *photoSource;
        if ([@"User ID" isEqualToString:dish.photoCreditTypeDic]) {
            photoSource = @"User";
        }else if([@"Qraved Team" isEqualToString:dish.photoCreditTypeDic]){
            photoSource = @"Qraved Admin";
        }
        [eventProperties setValue:photoSource forKey:@"Photo Source"];
        [[Amplitude instance] logEvent:@"RC - View Photo Card detail" withEventProperties:eventProperties];
    }

}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"User Photo Card Detail page";
    self.currentPointY = 0;
    self.previousKeyboardHeight = 0.0f;
    self.currentOffsetY = 0.0f;    
    self.view.backgroundColor = [UIColor whiteColor];
    self.homeUploadPhotoTableViewCellDelegate = self;
    [self setBackBarButtonOffset30];
    [self addScrollView];
    [self addMainView];
    [self getDataFromService];
}
- (void)getDataFromService
{
    IMGUser *user = [IMGUser currentUser];
    
    if (self.isFromNotification)
    {
//        if (![user.userId intValue] || !user.token.length)
//        {
//            return;
//        }
//        IMGDish *dishTemp = [dishArr firstObject];
//        NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",dishTemp.dishId,@"dishId",@"10",@"max", nil];
        
        NSDictionary *dic;
        IMGDish *dishTemp = [dishArr firstObject];

        if (!user.userId || !user.token)
        {
            dic = [[NSDictionary alloc] initWithObjectsAndKeys:dishTemp.dishId,@"dishId",@"10",@"max", nil];
        }
        else
        {
            dic = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",dishTemp.dishId,@"dishId",@"10",@"max", nil];
        }
        
        
        [UploadPhotoHandler getUploadPhotoCardData:dic andBlock:^(NSArray *dataArr) {
            restaurant = [dataArr objectAtIndex:0];
            dish = [dataArr objectAtIndex:1];
            commentsList = [NSMutableArray arrayWithArray:dish.commentList];
            dishArr = [[NSArray alloc] initWithObjects:dish, nil];
            dishUser = [dataArr objectAtIndex:2];
            [self setData];
            if (dishArr.count == 1)
            {
                [self addLikeCommentShareView:NO];
            }
        }];
    }
    else
    {
        [self setData];
        [[LoadingView sharedLoadingView] startLoading];
        [UploadPhotoHandler previousComment:@"" targetId:dish.dishId andBlock:^(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount) {
            commentsList = [NSMutableArray arrayWithArray:commentList];
            if(commentsList.count>3){
                dish.commentList = [NSMutableArray arrayWithArray:[commentsList subarrayWithRange:NSMakeRange(0, 3)]];
            }else{
                dish.commentList = commentsList;
            }
            if (commentCount) {
                dish.commentCount =commentCount;
            }
            if (likeCount) {
                dish.likeCount = likeCount;
            }
            [self addLikeCommentShareView:NO];
            if(self.cdpDelegate){
                [self.cdpDelegate CDP:self reloadTableCell:nil];
            }
            [[LoadingView sharedLoadingView] stopLoading];
        }];
    }
    

}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[LoadingView sharedLoadingView] stopLoading];
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillHideNotification];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)addScrollView
{
    self.scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:self.scrollView];
    self.scrollView.backgroundColor = [UIColor whiteColor];
    self.scrollView.frame = CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height-UICOMMENTINPUTVIEWHEIGHT-44);
    self.scrollView.delegate=self;
    self.scrollView.userInteractionEnabled = YES;
}
- (void)addMainView
{
    
    mainView = [[UIView alloc] init];
    mainView.userInteractionEnabled =YES;
    mainView.frame = self.scrollView.bounds;
    [self.scrollView addSubview:mainView];

    avatarLinkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    avatarLinkButton.frame = CGRectMake(LEFTLEFTSET, 8, 40, 40);
//    avatarLinkButton.clipsToBounds = YES;
//    avatarLinkButton.layer.cornerRadius = 20;
    [avatarLinkButton addTarget:self action:@selector(gotoOtherProfileWithUser) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:avatarLinkButton];
    
    titleLabel = [[TYAttributedLabel alloc] init];
    titleLabel.frame = CGRectMake(avatarLinkButton.endPointX + 15, 5, DeviceWidth - LEFTLEFTSET*2 - avatarLinkButton.endPointX, 20);
    //        titleLabel.font = [UIFont systemFontOfSize:18];
    titleLabel.numberOfLines = 0;
    titleLabel.delegate = self;
    titleLabel.characterSpacing = 0.1;
    [titleLabel setLineBreakMode:kCTLineBreakByWordWrapping];
    titleLabel.textColor = [UIColor grayColor];
    titleLabel.font = [UIFont systemFontOfSize:15];

    [mainView addSubview:titleLabel];
    
    reviewAndPhotoCountLabel = [[UILabel alloc] init];
    reviewAndPhotoCountLabel.font = [UIFont systemFontOfSize:13];
    reviewAndPhotoCountLabel.textColor = [UIColor grayColor];
    reviewAndPhotoCountLabel.alpha = 0.7f;
    [mainView addSubview:reviewAndPhotoCountLabel];
    
    timeLable = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, avatarLinkButton.endPointY + 15, 200, 14)];
    timeLable.textColor = [UIColor grayColor];
    timeLable.alpha = 0.7f;
    timeLable.font = [UIFont systemFontOfSize:12];
    [mainView addSubview:timeLable];
    
    writeReviewBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [writeReviewBtn setTitle:@"Write Review" forState:UIControlStateNormal];
    writeReviewBtn.backgroundColor = [UIColor colorGreen];
    [writeReviewBtn.layer setMasksToBounds:YES];
    [writeReviewBtn.layer setCornerRadius:12];
    [writeReviewBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [writeReviewBtn addTarget:self action:@selector(writeReviewBtnClick) forControlEvents:UIControlEventTouchUpInside];
    writeReviewBtn.userInteractionEnabled = YES;
    [mainView addSubview:writeReviewBtn];
    
    likeLabel=[[UILabel alloc] init];
    
    likeLabel.userInteractionEnabled=YES;
    UITapGestureRecognizer* likeTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(likeLabelTap:)];
    likeTap.numberOfTapsRequired=1;
    likeTap.numberOfTouchesRequired=1;
    [likeLabel addGestureRecognizer:likeTap];
    likeLabel.font=[UIFont systemFontOfSize:12];
    likeLabel.textColor = [UIColor colorC2060A];
    likeLabel.hidden=YES;
    
    commentLabel=[[UILabel alloc] init];
    commentLabel.font=[UIFont systemFontOfSize:12];
    commentLabel.textColor = [UIColor colorC2060A];
    commentLabel.hidden=YES;
    [mainView addSubview:likeLabel];
    [mainView addSubview:commentLabel];
    
//    cardPhotosView = [[CardPhotosView alloc] init];
//    cardPhotosView.cardPhotosViewDelegate = self;
//    cardPhotosView.userInteractionEnabled = YES;
//    [mainView addSubview:cardPhotosView];
    
    testimage = [[UIImageView alloc] init];
    testimage.userInteractionEnabled = YES;
    [mainView addSubview:testimage];
}
-(void)likeLabelTap:(UITapGestureRecognizer*)gesture
{
    [[Amplitude instance] logEvent:@" RC - View People Who Likes"];

    if (likepopView) {
        [likepopView removeFromSuperview];
    }

    [[LoadingView sharedLoadingView]startLoading];

    IMGDish *dishTemp = [dishArr firstObject];
    [HomeUtil getLikeListFromServiceWithDishId:dishTemp.dishId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
        
        likeListArr=[[NSMutableArray alloc]initWithArray:likeListArray];
        [[LoadingView sharedLoadingView]stopLoading];
        likepopView.likecount=likeviewcount;
        likepopView.likelistArray=likeListArr;
    }];
    likepopView=[[LikeView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    likepopView.delegate=self;
  [self.view addSubview:likepopView];


}
-(void)LikeviewLoadMoreData:(NSInteger)offset
{

       IMGDish *dishTemp = [dishArr firstObject];
    [HomeUtil getLikeListFromServiceWithDishId:dishTemp.dishId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
        [likeListArr addObjectsFromArray:likeListArray];
        likepopView.likecount=likeviewcount;
        [likepopView.likeUserTableView reloadData];
        
    }];
}
-(void)returnLikeCount:(NSInteger)count
{
    if (count!=0) {
        
   
    NSString *likeString=[NSString stringWithFormat:@"%ld %@",(long)count,count>1?L(@"Likes"):L(@"Like")];
    [likeLabel setText:likeString];
     likeLabel.frame=CGRectMake(likeLabel.frame.origin.x, likeLabel.frame.origin.y, likeLabel.expectedWidth, likeLabel.frame.size.height);
    }
}
- (void)setData
{
    NSURL *urlString ;
    dishUser.avatar = [dishUser.avatar stringByReplacingPercentEscapesUsingEncoding:
                   NSUTF8StringEncoding];

    if ([dishUser.avatar hasPrefix:@"http"])
    {
        urlString = [NSURL URLWithString:dishUser.avatar];
    }
    else
    {
        urlString = [NSURL URLWithString:[dishUser.avatar returnFullImageUrl]];
    }
   
    dish = [dishArr firstObject];
    NSString *path_sandox = NSHomeDirectory();
    //设置一个图片的存储路径
    NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/avastar.png"];
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    image = [image cutCircleImage];
    if (image&&[dishUser.userId intValue] == [[IMGUser currentUser].userId intValue]) {
        [avatarLinkButton setImage:image forState:UIControlStateNormal];
    }else{
        __weak UIButton *weakAvatarLinkButton = avatarLinkButton;
        [avatarLinkButton sd_setImageWithURL:urlString forState:UIControlStateNormal placeholderImage:[[UIImage imageNamed:@"headDefault.jpg"] cutCircleImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image.size.height>0&&image.size.width>0) {
                image = [image cutCircleImage];
                [weakAvatarLinkButton setImage:image forState:UIControlStateNormal];
            }
        }];
    }


    dishUser.userName = [dishUser.userName filterHtml];
    NSString *uploadPhotoStr = [NSString stringWithFormat:@"userId:%@:%@:%@",dishUser.userId,[dishUser.userName stringByReplacingOccurrencesOfString:@" " withString:@"0"],dishUser.avatar];
    
    NSString *textString;
    dishUser.userName = [dishUser.userName filterHtml];
    if (dishArr.count > 1)
    {
        [titleLabel setText:[NSString stringWithFormat:@"%@ uploaded %ld photos for %@",dishUser.userName,(unsigned long)dishArr.count,restaurant.title]];
        textString = [NSString stringWithFormat:@"%@ uploaded %ld photos for %@",[dishUser.userName inFilterHtmlWithHref:uploadPhotoStr],(unsigned long)dishArr.count,[restaurant.title inFilterHtmlWithHref:[NSString stringWithFormat:@"restId:%@",restaurant.restaurantId]]];
    }
    else
    {
        [titleLabel setText:[NSString stringWithFormat:@"%@ uploaded a photo for %@",dishUser.userName,restaurant.title]];
        textString = [NSString stringWithFormat:@"%@ uploaded a photo for %@",[dishUser.userName  inFilterHtmlWithHref:uploadPhotoStr],[restaurant.title inFilterHtmlWithHref:[NSString stringWithFormat:@"restId:%@",restaurant.restaurantId]]];
    }
    [titleLabel setFrameWithOrign:CGPointMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y) Width:DeviceWidth-LEFTLEFTSET*2-avatarLinkButton.endPointX];
    TYLinkTextStorage *linkTextStorage = [[TYLinkTextStorage alloc]init];
    linkTextStorage.range = NSMakeRange(0, dishUser.userName.length);
    linkTextStorage.textColor = [UIColor blackColor];
    linkTextStorage.linkData = @"user";
    linkTextStorage.underLineStyle = kCTUnderlineStyleNone;
    NSMutableArray *temArry = [NSMutableArray array];
    [temArry addObject:linkTextStorage];
    TYLinkTextStorage *linkTextStorageRestaurant = [[TYLinkTextStorage alloc]init];
    if (dishArr.count > 1){
        linkTextStorageRestaurant.range = NSMakeRange(dishUser.userName.length+22, restaurant.title.length+1);
    }else{
        linkTextStorageRestaurant.range = NSMakeRange(dishUser.userName.length+21, restaurant.title.length+1);
        
    }
    linkTextStorageRestaurant.textColor = [UIColor blackColor];
    linkTextStorageRestaurant.linkData = @"restaurant";
    linkTextStorageRestaurant.underLineStyle = kCTUnderlineStyleNone;
    
    [temArry addObject:linkTextStorageRestaurant];
    
    
    [titleLabel addTextStorageArray:temArry];

    if ([dishUser.photoCount intValue]!=0&&[dishUser.reviewCount intValue]!=0) {

    
    [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@ - %@ %@",dishUser.reviewCount,[dishUser.reviewCount intValue] > 1 ? @"reviews":@"review",dishUser.photoCount,[dishUser.photoCount intValue] > 1 ? @"photos":@"photo"]];
    reviewAndPhotoCountLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.endPointY, reviewAndPhotoCountLabel.expectedWidth, reviewAndPhotoCountLabel.font.lineHeight);
    }else if ([dishUser.photoCount intValue]==0||[dishUser.reviewCount intValue]==0){
        if ([dishUser.reviewCount intValue]==0&&[dishUser.photoCount intValue]!=0) {
            
            [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@",dishUser.photoCount,[dishUser.photoCount intValue] > 1 ? @"photos":@"photo"]];
            reviewAndPhotoCountLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.endPointY, reviewAndPhotoCountLabel.expectedWidth, reviewAndPhotoCountLabel.font.lineHeight);
            
            
            
        }
        if ([dishUser.photoCount intValue]==0&&[dishUser.reviewCount intValue]!=0) {
            
            [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@",dishUser.reviewCount,[dishUser.reviewCount intValue] > 1 ? @"reviews":@"review"]];
            reviewAndPhotoCountLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.endPointY, reviewAndPhotoCountLabel.expectedWidth, reviewAndPhotoCountLabel.font.lineHeight);
            }
        
    }

    [timeLable setText:[Date getTimeInteval_v4:[restaurant.timeline longLongValue]/1000]];
    timeLable.frame = CGRectMake(avatarLinkButton.frame.origin.x, reviewAndPhotoCountLabel.endPointY, timeLable.expectedWidth, timeLable.expectedHeight);
    if (timeLable.frame.origin.y<avatarLinkButton.endPointY+15) {
        timeLable.frame=CGRectMake(LEFTLEFTSET, avatarLinkButton.endPointY + 15, 240, 14);
    }
    
    
    
//    cardPhotosView.frame = CGRectMake(LEFTLEFTSET, timeLable.endPointY+13, DeviceWidth-LEFTLEFTSET*2, 160);
//   cardPhotosView.homeTableViewCellDelegate=self.homeTableViewCellDelegate;
//    
//    [cardPhotosView setPhotos:dishArr];
    if ([dish.imageHeight floatValue])
    {
        testimage.frame = CGRectMake(LEFTLEFTSET, timeLable.endPointY+13, DeviceWidth-LEFTLEFTSET*2, [dish.imageHeight floatValue]);
    }
    else
    {
        testimage.frame = CGRectMake(LEFTLEFTSET, timeLable.endPointY+13, DeviceWidth-LEFTLEFTSET*2, 160);
    }
    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagetap)];
    [testimage addGestureRecognizer:tap];
    
    dish = dishArr.firstObject;
    __weak typeof(testimage) weakImageView = testimage;
    [testimage sd_setImageWithURL:[NSURL URLWithString:[[dish valueForKey:@"imageUrl"] returnFullImageUrlWithWidth:DeviceWidth - LEFTLEFTSET*2]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [UIView animateWithDuration:1.0 animations:^{
            [weakImageView setAlpha:1.0];
        }];
    }];
    
    float currentPoint = testimage.endPointY + 10;
    
    IMGUser *currentUser = [IMGUser currentUser];
    if ([currentUser.userId intValue] == [dishUser.userId intValue] )
    {
        writeReviewBtn.hidden = NO;
        writeReviewBtn.frame = CGRectMake(DeviceWidth/2 - 90, testimage.endPointY + 20, 180, 35);
        currentPoint = writeReviewBtn.endPointY + 10;
    }
    else
    {
        writeReviewBtn.hidden = YES;
    }
    
    if (dishArr.count > 1)
    {
        if(self.likeCommentShareView){
            [self.likeCommentShareView removeFromSuperview];
        }
        self.currentPointY=currentPoint;
        return;
    }
    
    
    self.currentPointY=currentPoint;
    mainView.frame = CGRectMake(0, 0, DeviceWidth, currentPoint+30);
    [self addLikeCommentShareView:NO andIsTem:YES];

}
- (void)gotoOtherProfileWithUser
{
    OtherProfileViewController *opvc = [[OtherProfileViewController alloc] init];
    opvc.amplitude = @"Card Detail Page";

    IMGUser *currentUser = [IMGUser currentUser];
    if ([currentUser.userId intValue] == [dishUser.userId intValue])
    {
        opvc.isOtherUser = NO;
    }else
    {
        opvc.isOtherUser = YES;
    }
    opvc.otherUser = dishUser;
    [self.navigationController pushViewController:opvc animated:YES];

}
-(void)imagetap{
    
    NSArray *photoArray = [[NSArray alloc]initWithObjects:dish, nil];
    AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:photoArray andPhotoTag:0 andRestaurant:nil andFromDetail:NO];
    //        albumViewController.title = dish.userName;
    albumViewController.showPage=NO;
    albumViewController.amplitudeType = self.amplitudeType;
    [self.navigationController pushViewController:albumViewController animated:YES];
    
}
- (void)addLikeCommentShareView:(BOOL)readMoreTap
{
    [self updateLikeCommentListCountLabel:YES andIsreadMoreTap:readMoreTap];
    
    if(self.likeCommentShareView){
        [self.likeCommentShareView removeFromSuperview];
    }

    self.likeCommentShareView = [[UILikeCommentShareView alloc]initInSuperView:self.scrollView atStartX:0 startY:self.currentPointY+10 commentCount:[dish.commentCount intValue] commentArray:commentsList liked:dish.isLike withFooter:NO needCommentListView:YES];
    self.likeCommentShareView.likeCommentShareDelegate=self;
    self.currentPointY = self.likeCommentShareView.endPointY+10;
    self.scrollView.contentSize = CGSizeMake(0, self.currentPointY+10);
    self.currentPointY=self.scrollView.contentSize.height;
    
    self.commentInputView = [[UICommentInputView alloc]initView:NO];
    self.commentInputView.frame=CGRectMake(0, DeviceHeight-44-self.commentInputView.frame.size.height, self.commentInputView.frame.size.width, self.commentInputView.frame.size.height);
    
    self.commentInputView.commentInputViewDelegate=self;
    [self.view addSubview:self.commentInputView];

}
- (void)addLikeCommentShareView:(BOOL)readMoreTap andIsTem:(BOOL)isTemp
{
    [self updateLikeCommentListCountLabel:YES andIsreadMoreTap:readMoreTap];
    
    if(self.likeCommentShareView){
        [self.likeCommentShareView removeFromSuperview];
    }
 
    self.likeCommentShareView = [[UILikeCommentShareView alloc]initInSuperView:self.scrollView atStartX:0 startY:self.currentPointY+10 commentCount:[dish.commentCount intValue] commentArray:commentsList liked:dish.isLike withFooter:NO needCommentListView:!isTemp];
    self.likeCommentShareView.likeCommentShareDelegate=self;
    self.currentPointY = self.likeCommentShareView.endPointY+10;
    self.scrollView.contentSize = CGSizeMake(0, self.currentPointY+10);
    self.currentPointY=self.scrollView.contentSize.height;
    
    self.commentInputView = [[UICommentInputView alloc]initView:NO];
    self.commentInputView.frame=CGRectMake(0, DeviceHeight-44-self.commentInputView.frame.size.height, self.commentInputView.frame.size.width, self.commentInputView.frame.size.height);
    
    self.commentInputView.commentInputViewDelegate=self;
    [self.view addSubview:self.commentInputView];
    
}


-(void)updateLikeCommentListCountLabel:(BOOL)initial andIsreadMoreTap:(BOOL)isreadMoreTap{
    
    if(likeLabel.hidden==NO){
        self.currentPointY=likeLabel.startPointY-10;
    }else if(commentLabel.hidden==NO){
        self.currentPointY=commentLabel.startPointY-10;
    }else if(!initial){
        self.currentPointY=self.likeCommentShareView.startPointY-10;
    }
    
    CGFloat startPointY=self.currentPointY;
    
    float start=LEFTLEFTSET;
    if([dish.likeCount intValue]>0){
        NSString *likeString=[NSString stringWithFormat:@"%@ %@",dish.likeCount,[dish.likeCount intValue]>1?L(@"Likes"):L(@"Like")];
        CGSize likeSize=[likeString sizeWithFont:likeLabel.font constrainedToSize:CGSizeMake(DeviceWidth,20) lineBreakMode:NSLineBreakByWordWrapping];
        likeLabel.frame=CGRectMake(start, startPointY+10, likeSize.width,20);
        likeLabel.text=likeString;
        start+=likeSize.width+15;
        self.currentPointY=likeLabel.endPointY;
        [likeLabel setHidden:NO];
    }else if(likeLabel.hidden==NO){
        [likeLabel setHidden:YES];
    }
    if([dish.commentCount intValue]>0){
        NSString *commentString=[NSString stringWithFormat:@"%@ %@",dish.commentCount,[dish.commentCount intValue]>1?L(@"Comments"):L(@"Comment")];
        CGSize commentSize=[commentString sizeWithFont:commentLabel.font constrainedToSize:CGSizeMake(DeviceWidth,20) lineBreakMode:NSLineBreakByWordWrapping];
        commentLabel.frame=CGRectMake(start, startPointY+10, commentSize.width,20);
        commentLabel.text=commentString;
        start+=commentSize.width+15;
        self.currentPointY=commentLabel.endPointY;
        [commentLabel setHidden:NO];
    }else if(commentLabel.hidden==NO){
        [commentLabel setHidden:YES];
    }
    
    if(!initial){
        [self.view endEditing:YES];
        self.likeCommentShareView.frame=CGRectMake(0, self.currentPointY+10, self.likeCommentShareView.frame.size.width, self.likeCommentShareView.frame.size.height);
        
        self.currentPointY = self.likeCommentShareView.endPointY+10;
        if (!isreadMoreTap) {
            self.scrollView.contentSize = CGSizeMake(0, self.currentPointY+10);
            self.currentPointY=self.scrollView.contentSize.height;
        }
       
    }
}

#pragma mark - privates
- (NSAttributedString *)attributedString:(NSArray *__autoreleasing *)outURLs
                               URLRanges:(NSArray *__autoreleasing *)outURLRanges andText:(NSString *)text
{
    NSString *HTMLText = text;
    NSArray *URLs;
    NSArray *URLRanges;
    UIColor *color = [UIColor grayColor];
    UIFont *font = [UIFont systemFontOfSize:TITLE_LABEL_FONT];
    NSMutableParagraphStyle *mps = [[NSMutableParagraphStyle alloc] init];
    //    mps.lineSpacing = ceilf(font.pointSize * .5);
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor whiteColor];
    shadow.shadowOffset = CGSizeMake(0, 1);
    NSString *str = [NSString stringWithHTMLText:HTMLText baseURL:nil URLs:&URLs URLRanges:&URLRanges];
    NSMutableAttributedString *mas = [[NSMutableAttributedString alloc] initWithString:str attributes:@
                                      {
                                          NSForegroundColorAttributeName : color,
                                          NSFontAttributeName            : font,
                                          NSParagraphStyleAttributeName  : mps,
                                          NSShadowAttributeName          : shadow,
                                      }];
    [URLRanges enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
        [mas addAttributes:@
         {
             NSForegroundColorAttributeName : [UIColor color222222],
             //       NSUnderlineStyleAttributeName  : @(NSUnderlineStyleSingle)
             NSForegroundColorAttributeName : [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:TITLE_LABEL_FONT]
             
         } range:[obj rangeValue]];
    }];
    
    *outURLs = URLs;
    *outURLRanges = URLRanges;
    
    return [mas copy];
}


//-(void)cardPhotosViewImageClick:(int)imageIndex{
//    NSLog(@"代理方法被调用");
//    ImageListViewController *imageListViewController=[[ImageListViewController alloc] initWithRestaurant:restaurant andUser:dishUser andDishArr:dishArr andIndex:imageIndex];
//    imageListViewController.delegate = self;
//    [self.navigationController pushViewController:imageListViewController animated:YES];
//    
//}

 

-(void)shareButtonClickwithbutton:(UIButton*)btn{
    
  
    IMGRestaurant *restaurant_ = [[IMGRestaurant alloc] init];
    restaurant_.restaurantId = dish.restaurantId;
    restaurant_.title = dish.restaurantTitle;
    restaurant_.seoKeyword = dish.restaurantSeo;
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/photo/%@",QRAVED_WEB_SERVER_OLD,dish.dishId]];
    NSString *shareContentString = [NSString stringWithFormat:@"Check out this photo at %@ on Qraved. %@",restaurant_.title,url];
    RestaurantActivityItemProvider *provider=[[RestaurantActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
    provider.url=url;
    provider.title=shareContentString;
    provider.restaurantTitle=restaurant_.title;
    provider.cuisineAreaPrice = dish.userName;
    
    TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
    twitterActivityItemProvider.title = [NSString stringWithFormat:@"Check out this photo at %@ on Qraved",restaurant_.title];
    
    UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
    
    
    //去除airDorp
    activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
    [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK IT OUT NOW! A Restaurant Photo by %@ on Qraved!"),dish.userName] forKey:@"subject"];
    activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
        if (completed) {
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:restaurant.title forKey:@"RestaurantTitle"];
            [eventProperties setValue:@"Photo card detail page" forKey:@"Locaton"];
            [eventProperties setValue:provider.activityType forKey:@"Channel"];
            [eventProperties setValue:dish.dishId forKey:@"Photo_ID"];
            
            [[Amplitude instance] logEvent:@"SH - Share Photo" withEventProperties:eventProperties];
        }else{
        }
    };
    if ([activityCtl respondsToSelector:@selector(popoverPresentationController)]) {
        
        activityCtl.popoverPresentationController.sourceView = btn;
        
    }

    [self presentViewController:activityCtl animated:YES completion:^{
        
    }];
}

- (void)commentInputView:(UIView*)view postButtonTapped:(UIButton*)postButton commentInputText:(NSString *)text{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
    
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:dish.dishId forKey:@"Photo_ID"];
    [eventProperties setValue:dish.userId forKey:@"UploaderUser_ID"];
    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:@"Photo card detail page" forKey:@"Location"];
    
    postButton.enabled = NO;
    [UploadPhotoHandler postComment:text dishId:dish.dishId restaurantId:restaurant.restaurantId andBlock:^(IMGDishComment *dishComment) {
       postButton.enabled = YES;
        
       [eventProperties setValue:dishComment.commentId forKey:@"Comment_ID"];
        [[Amplitude instance] logEvent:@"UC - Comment Photo Succeed" withEventProperties:eventProperties];
        [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Photo Succeed" withValues:eventProperties];

        [self.commentInputView clearInputTextView];
        [self.view endEditing:YES];
        
        if(dishComment){
            dish.commentCount=[NSNumber numberWithInteger:[dish.commentCount integerValue]+1];
            
            [self.likeCommentShareView addNewComment:dishComment];
            
            dish.commentCount=[NSNumber numberWithInteger:[dish.commentCount integerValue]+1];
            if(commentsList){
                if([commentsList isKindOfClass:[NSArray class]]){
                    commentsList=[NSMutableArray arrayWithArray:commentsList];
                }
                [commentsList insertObject:dishComment atIndex:0];
            }else{
                commentsList = [NSMutableArray arrayWithObject:dishComment];
            }
            if(commentsList.count>3){
                dish.commentList = [NSMutableArray arrayWithArray:[commentsList subarrayWithRange:NSMakeRange(0, 3)]];
            }else{
                dish.commentList = commentsList;
            }
            
            [self updateLikeCommentListCountLabel:NO andIsreadMoreTap:NO];
            
            
            
            CGPoint point=CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
            
            if (point.y>0) {
                [self.scrollView setContentOffset:point];
                
            }
            if(self.cdpDelegate){
                [self.cdpDelegate CDP:self reloadTableCell:nil];
            }
        }
    } failure:^(NSString *errorReason){
        if([errorReason isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
        [eventProperties setValue:errorReason forKey:@"Reason"];
        [[Amplitude instance] logEvent:@"UC – Comment Photo Failed" withEventProperties:eventProperties];
    }];
    [[Amplitude instance] logEvent:@"UC - Comment Photo Initiate" withEventProperties:eventProperties];
    
}


- (void)likeCommentShareView:(UIView*)likeCommentShareView_ viewPreviousCommentsTapped:(UIButton*)button lastComment:(id)comment{
    [[LoadingView sharedLoadingView] startLoading];
    NSString *lastCommentIdStr=@"0";
    if([comment isKindOfClass:[IMGDishComment class]]){
        IMGDishComment *dishComment=(IMGDishComment*)comment;
        if(dishComment){
            lastCommentIdStr=[NSString stringWithFormat:@"%@",dishComment.commentId];
        }
        
        [UploadPhotoHandler previousComment:lastCommentIdStr targetId:dish.dishId andBlock:^(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount) {
            if(commentList&&commentList.count>0){
                [self.view endEditing:YES];
                
                if(commentList){
                    CGFloat likeCommentShareViewHeight=self.likeCommentShareView.frame.size.height;
                    [self.likeCommentShareView addPreviousComments:commentList];
                    
                    CGFloat height=self.likeCommentShareView.frame.size.height-likeCommentShareViewHeight;
                    self.currentPointY=self.scrollView.contentSize.height+height;
                    self.scrollView.contentSize=CGSizeMake(self.scrollView.contentSize.width, self.currentPointY);
                    
                    CGFloat height1 = [UILikeCommentShareView calculatedTableViewHeight:commentsList withFooter:NO];
                    
                    
                    CGPoint point= CGPointMake(0, self.scrollView.contentSize.height-height1-DeviceHeight+65);
                    if (point.y>0) {
                        self.scrollView.contentOffset =point;

                    }
                    
                    [commentsList addObjectsFromArray:commentList];
                }
            }
        }];
    }else{
        [[LoadingView sharedLoadingView] stopLoading];
    }
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView_ likeButtonTapped:(UIButton *)button{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    //Photo_ID, UploaderUser_ID, Restaurant_ID, Location
    [eventProperties setValue:dish.dishId forKey:@"Photo_ID"];
    [eventProperties setValue:dish.userId forKey:@"UploaderUser_ID"];
    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:@"Photo card detail page" forKey:@"Location"];

    [UploadPhotoHandler likeDish:dish.isLike dishId:dish.dishId andBlock:^{
        if (!dish.isLike) {
            [[Amplitude instance] logEvent:@"UC - UnLike Photo Succeed" withEventProperties:eventProperties];
        }else{
            [[Amplitude instance] logEvent:@"UC - Like Photo Succeed" withEventProperties:eventProperties];
        }
    }failure:^(NSString *exceptionMsg){
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
    }];
    [self.view endEditing:YES];
    dish.isLike=!dish.isLike;
    if(dish.isLike){
        dish.likeCount=[NSNumber numberWithInt:[dish.likeCount intValue]+1];
        [[Amplitude instance] logEvent:@"UC - Like Photo Initiate" withEventProperties:eventProperties];
    }else{
        dish.likeCount=[NSNumber numberWithInt:[dish.likeCount intValue]-1];

        [[Amplitude instance] logEvent:@"UC - UnLike Photo Initiate" withEventProperties:eventProperties];
    }
    [self.likeCommentShareView updateLikedStatus];
    [self updateLikeCommentListCountLabel:NO andIsreadMoreTap:NO];
    if(self.cdpDelegate){
        [self.cdpDelegate CDP:self reloadTableCell:nil];
    }

}

-(void)likeCommentShareView:(UIView *)likeCommentShareView commentCell:(UITableViewCell *)cell readMoreTapped:(id)comment offset:(CGFloat)offset{
    [self.view endEditing:YES];    
    self.currentPointY+=offset;
    self.scrollView.contentSize=CGSizeMake(self.scrollView.contentSize.width, self.currentPointY);
    [self addLikeCommentShareView:YES];
}

- (void)writeReviewBtnClick{
    ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc]initWithRestaurant:restaurant andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
    publishReviewVC.amplitudeType = @"Card detail page";
    publishReviewVC.isEdit = YES;
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:publishReviewVC] animated:YES completion:^{

    }];
}

- (void)homeCardTableViewUserNameOrImageTapped:(id)sender{
    if ([sender isKindOfClass:[IMGUser class]])
    {
        IMGUser *user = (IMGUser *)sender;
        OtherProfileViewController *opvc = [[OtherProfileViewController alloc] init];
        opvc.amplitude = @"Card Detail Page";

        IMGUser *currentUser = [IMGUser currentUser];
        if ([currentUser.userId intValue] == [user.userId intValue])
        {
            opvc.isOtherUser = NO;
        }else
        {
            opvc.isOtherUser = YES;
        }
        opvc.otherUser = user;
        [self.navigationController pushViewController:opvc animated:YES];
    }
    else if ([sender isKindOfClass:[IMGRestaurant class]])
    {
        IMGRestaurant *currentRestaurant = (IMGRestaurant *)sender;
        DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:currentRestaurant.restaurantId];
        dvc.amplitudeType = self.amplitudeType;
        [self.navigationController pushViewController:dvc animated:YES];
    }
}
- (void)cardPhotosChangeHeight:(CGFloat)height
{
    
}
- (void)attributedLabel:(TYAttributedLabel *)attributedLabel textStorageClicked:(id<TYTextStorageProtocol>)TextRun atPoint:(CGPoint)point{
    
    if ([TextRun isKindOfClass:[TYLinkTextStorage class]]) {
        
        NSString *linkStr = ((TYLinkTextStorage*)TextRun).linkData;
        
        if ([linkStr hasPrefix:@"user"]) {
            
            OtherProfileViewController *opvc = [[OtherProfileViewController alloc] init];
            opvc.amplitude = @"Card Detail Page";
            IMGUser *currentUser = [IMGUser currentUser];
            if ([currentUser.userId intValue] == [dishUser.userId intValue])
            {
                opvc.isOtherUser = NO;
            }else
            {
                opvc.isOtherUser = YES;
            }
            opvc.otherUser = dishUser;
            [self.navigationController pushViewController:opvc animated:YES];
            
        }else {
            DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:restaurant.restaurantId];
            dvc.amplitudeType = self.amplitudeType;
            [self.navigationController pushViewController:dvc animated:YES];
        }
    }
    

    
}

@end
