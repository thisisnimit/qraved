//
//  ReviewCardViewController.m
//  Qraved
//
//  Created by lucky on 16/3/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "ReviewCardViewController.h"
#import "UIConstants.h"
#import "DLStarRatingControl.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"
#import "IMGRestaurant.h"
#import "UILabel+Helper.h"
#import "IMGDish.h"
#import "UIButton+WebCache.h"
#import "CXAHyperlinkLabel.h"
#import "NSString+CXAHyperlinkParser.h"
#import "CardPhotosView.h"
#import "UILikeCommentShareView.h"
#import "LoadingView.h"
#import "UICommentInputView.h"
#import "AppDelegate.h"
#import "ReviewHandler.h"
#import "DetailViewController.h"
#import "RestaurantActivityItemProvider.h"
#import "Date.h"
#import "ReviewActivityItemProvider.h"
#import "TwitterActivityItemProvider.h"

#import "OtherProfileViewController.h"
#import "ImageListViewController.h"
#import "AlbumViewController.h"
#import "ReviewPublishViewController.h"
#import "HomeUtil.h"

#import "LikeView.h"

#define LEFTMARGIN 15
#define TEXTLABELTOPMARGIN 35
#define RIGHTMARGIN 30
#define TEXTLABELLEFTMARGIN 11+PROFILEIMAGEVIEWSIZE+8
#define PROFILEIMAGEVIEWSIZE 20
#define CONTENTLABELWIDTH DeviceWidth-LEFTMARGIN-PROFILEIMAGEVIEWSIZE-RIGHTMARGIN

#define SUMMARIZETEXTLENGTH 300

#define LINK_TEXT @"<a style=\"text-decoration:none;color:#3A8CE2;font-family:'OpenSans'\" href=\"http://www.qraved.com/\">...read more</a>"



@interface ReviewCardViewController ()<UIScrollViewDelegate,LikeCommentShareDelegate,CommentInputViewDelegate,ImageListViewControllerDelegate,LikeListViewDelegate>
{
    UIButton *avatarLinkButton;
//    CXAHyperlinkLabel *titleLabel;
    TYAttributedLabel *titleLabel;
    UILabel *reviewAndPhotoCountLabel;
    UILabel *timeLabel;
    UIImageView *restaurantImageView;
    
    UILabel *reviewTitleLabel;
    UILabel *scoreLabel;
    DLStarRatingControl *ratingScoreControl;
    UILabel *reviewSummarizeLabel;
    IMGReview *review;
    NSMutableArray *commentsList;
    
    UIButton *uploadPhotoBtn;
    UILabel *likeLabel;
    UILabel *commentLabel;
    CardPhotosView *cardPhotosView;

    NSArray *dishArrM;
    
    
    UIView *mainView;
    IMGRestaurant *restaurant;
    

    NSString *tempStr;
    CGFloat tempHeight;
    BOOL readMore;
    
    float textOrPhotoEndY;
    LikeView*  likepopView;
    NSMutableArray* likeListArr;
}
@end

@implementation ReviewCardViewController

-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant andReview:(IMGReview *)reviewTemp andDishList:(NSArray *)dishList
{
    if(self= [super init]){
        restaurant = paramRestaurant;
        review = reviewTemp;
        dishArrM = [NSArray arrayWithArray:dishList];
    }
    return self;

}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;

    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
    [eventProperties setValue:self.amplitudeType forKey:@"Origin"];
    [[Amplitude instance] logEvent:@"RC - View Review Card detail" withEventProperties:eventProperties];
    
    if (!self.isFromNotification) {
        NSURL *urlString;
        review.avatar = [review.avatar stringByReplacingPercentEscapesUsingEncoding:
                         NSASCIIStringEncoding];
        
        if ([review.avatar hasPrefix:@"http"])
        {
            urlString = [NSURL URLWithString:review.avatar];
        }
        else
        {
            urlString = [NSURL URLWithString:[review.avatar returnFullImageUrl]];
        }
        NSString *path_sandox = NSHomeDirectory();
        //设置一个图片的存储路径
        NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/avastar.png"];
        UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
        image = [image cutCircleImage];
        if (image&&[review.userId intValue] == [[IMGUser currentUser].userId intValue]) {
            [avatarLinkButton setImage:image forState:UIControlStateNormal];
        }else{
            __weak UIButton *weakAvatarLinkButton = avatarLinkButton;
            [avatarLinkButton sd_setImageWithURL:urlString forState:UIControlStateNormal placeholderImage:[[UIImage imageNamed:@"headDefault.jpg"] cutCircleImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (image.size.height>0&&image.size.width>0) {
                                        image = [image cutCircleImage];
                                        [weakAvatarLinkButton setImage:image forState:UIControlStateNormal];
                                    }
            }];
        }

    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"User Review Card Detail page";
    self.currentPointY = 0.0f;
    self.previousKeyboardHeight = 0.0f;
    self.currentOffsetY = 0.0f;
    textOrPhotoEndY = 0;
    readMore = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    [self setBackBarButtonOffset30];
    [self addScrollView];
    [self addMainView];
    [self getDataFromService];
}
- (void)getDataFromService
{
    
   
    IMGUser *user = [IMGUser currentUser];
    if (self.isFromNotification)
    {
        
        NSDictionary *dic;
        
        if (!user.userId || !user.token)
        {
            dic = [[NSDictionary alloc] initWithObjectsAndKeys:review.reviewId,@"reviewId",@"10",@"max", nil];
        }else {
        
             dic = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",review.reviewId,@"reviewId",@"10",@"max", nil];
        
        }
        
        [ReviewHandler getReviewCardData:dic andBlock:^(NSArray *dataArr) {
            restaurant = [dataArr objectAtIndex:0];
            review = [dataArr objectAtIndex:1];
            commentsList = [NSMutableArray arrayWithArray:review.commentList];
            dishArrM = [dataArr objectAtIndex:2];
            if (dishArrM.count==1) {
                IMGDish *temDish = [dishArrM firstObject];
                review.imageHeight = temDish.imageHeight;

            }
            [self setData];
            [self addLikeCommentShareView:NO andIsDataUnload:NO];
        }];
        
    }
    else
    {
        [self setData];
        [[LoadingView sharedLoadingView] startLoading];
        [ReviewHandler previousComment:@"" targetId:review.reviewId andBlock:^(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount) {
            commentsList = [NSMutableArray arrayWithArray:commentList];
            if(commentsList.count>3){
                review.commentList = [NSMutableArray arrayWithArray:[commentsList subarrayWithRange:NSMakeRange(0, 3)]];
            }else{
                review.commentList = commentsList;
            }
            if (commentCount) {
                review.commentCount =commentCount;
            }
            if (likeCount) {
                review.likeCount = likeCount;
            }
            [self addLikeCommentShareView:NO andIsDataUnload:NO];
            if(self.cdpDelegate){
                [self.cdpDelegate CDP:self reloadTableCell:nil];
            }
            [[LoadingView sharedLoadingView] stopLoading];
       }];

    }
    [self addLikeCommentShareView:YES andIsDataUnload:YES andIsTem:YES];

}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[LoadingView sharedLoadingView] stopLoading];
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillHideNotification];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)addScrollView
{
    self.scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:self.scrollView];
    self.scrollView.backgroundColor = [UIColor whiteColor];
    self.scrollView.frame = CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height-UICOMMENTINPUTVIEWHEIGHT-44);
    self.scrollView.delegate=self;
}
- (void)addMainView
{
    mainView = [[UIView alloc] init];
    [self.scrollView addSubview:mainView];
    avatarLinkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    avatarLinkButton.frame = CGRectMake(LEFTLEFTSET, 8, 40, 40);
    [avatarLinkButton addTarget:self action:@selector(gotoOtherProfileWithUser) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:avatarLinkButton];
    
    titleLabel = [[TYAttributedLabel alloc] init];
    titleLabel.frame = CGRectMake(avatarLinkButton.endPointX + 15, 5, DeviceWidth - LEFTLEFTSET*2 - avatarLinkButton.endPointX, 1000);
    titleLabel.numberOfLines = 0;
    titleLabel.delegate = self;
    titleLabel.characterSpacing = 0.1;
    [titleLabel setLineBreakMode:kCTLineBreakByCharWrapping];
    titleLabel.textColor = [UIColor grayColor];
    [mainView addSubview:titleLabel];
    
    reviewAndPhotoCountLabel = [[UILabel alloc] init];
    reviewAndPhotoCountLabel.font = [UIFont systemFontOfSize:13];
    reviewAndPhotoCountLabel.textColor = [UIColor grayColor];
    reviewAndPhotoCountLabel.alpha = 0.7f;
    [mainView addSubview:reviewAndPhotoCountLabel];
    
    timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, avatarLinkButton.endPointY + 15, 200, 14)];
    timeLabel.textColor = [UIColor grayColor];
    timeLabel.alpha = 0.7f;
    timeLabel.font = [UIFont systemFontOfSize:12];
    [mainView addSubview:timeLabel];
    
    reviewTitleLabel = [[UILabel alloc] init];
    reviewTitleLabel.font = [UIFont boldSystemFontOfSize:15];
    reviewTitleLabel.numberOfLines = 0;
    [mainView addSubview:reviewTitleLabel];
    reviewTitleLabel.frame = CGRectMake(LEFTLEFTSET, timeLabel.endPointY + 13, 0, 0);
    
    scoreLabel = [[UILabel alloc] init];
    scoreLabel.font = [UIFont systemFontOfSize:12.5];
    scoreLabel.textColor = [UIColor colorFFC000];
    scoreLabel.frame = CGRectMake(LEFTLEFTSET, titleLabel.endPointY, scoreLabel.expectedWidth, scoreLabel.font.lineHeight);
    [mainView addSubview:scoreLabel];
    
    
    ratingScoreControl=[[DLStarRatingControl alloc] initWithFrame:CGRectMake(LEFTLEFTSET + 25,titleLabel.endPointY, 74, 15) andStars:5 andStarWidth:10 isFractional:NO spaceWidth:5];
    ratingScoreControl.hidden = YES;
    ratingScoreControl.userInteractionEnabled=NO;
    [mainView addSubview:ratingScoreControl];
    

    
    reviewSummarizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, scoreLabel.endPointY + 5, DeviceWidth - LEFTLEFTSET*2, 40)];
    [mainView addSubview:reviewSummarizeLabel];

    uploadPhotoBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    uploadPhotoBtn.hidden = YES;
    [uploadPhotoBtn setTitle:@"Upload Photo" forState:UIControlStateNormal];
    uploadPhotoBtn.backgroundColor = [UIColor colorGreen];
    [uploadPhotoBtn.layer setMasksToBounds:YES];
    [uploadPhotoBtn.layer setCornerRadius:12];
    [uploadPhotoBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [uploadPhotoBtn addTarget:self action:@selector(uploadPhotoBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:uploadPhotoBtn];
    
    likeLabel=[[UILabel alloc] init];
    
    likeLabel.userInteractionEnabled=YES;
    UITapGestureRecognizer* likeTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(likeLabelTap:)];
    likeTap.numberOfTapsRequired=1;
    [likeLabel addGestureRecognizer:likeTap];
    likeLabel.font=[UIFont systemFontOfSize:12];
    likeLabel.textColor = [UIColor colorC2060A];
    likeLabel.hidden=YES;
    
    commentLabel=[[UILabel alloc] init];
    commentLabel.font=[UIFont systemFontOfSize:12];
    commentLabel.textColor = [UIColor colorC2060A];
    commentLabel.hidden=YES;
    
    [mainView addSubview:likeLabel];
    [mainView addSubview:commentLabel];
    
    cardPhotosView = [[CardPhotosView alloc] init];
    [mainView addSubview:cardPhotosView];

}
-(void)likeLabelTap:(UITapGestureRecognizer*)gesture

{
    [[Amplitude instance] logEvent:@" RC - View People Who Likes"];

    if (likepopView) {
        [likepopView removeFromSuperview];
    }

    [[LoadingView sharedLoadingView]startLoading];
    [HomeUtil getLikeListFromServiceWithReviewId:review.reviewId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
        
        likeListArr=[NSMutableArray arrayWithArray:likeListArray];
        [[LoadingView sharedLoadingView]stopLoading];

                likepopView.likecount=likeviewcount;
       
        likepopView.likelistArray=likeListArr;
    
        
    }];
    likepopView=[[LikeView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    likepopView.delegate=self;
    [self.view addSubview:likepopView];
    
}
-(void)LikeviewLoadMoreData:(NSInteger)offset{
    
   
      [HomeUtil getLikeListFromServiceWithReviewId:review.reviewId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset]andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
          likepopView.likecount=likeviewcount;
          [likeListArr addObjectsFromArray:likeListArray];
        [likepopView.likeUserTableView reloadData];
    }];
}
-(void)returnLikeCount:(NSInteger)count
{
    if (count!=0) {
        
  
    CGFloat startPointY=textOrPhotoEndY;
    
    float start=LEFTLEFTSET;
    NSString *likeString=[NSString stringWithFormat:@"%ld %@",(long)count,count>1?L(@"Likes"):L(@"Like")];
    CGSize likeSize=[likeString sizeWithFont:likeLabel.font constrainedToSize:CGSizeMake(DeviceWidth,20) lineBreakMode:NSLineBreakByWordWrapping];
    likeLabel.frame=CGRectMake(start, startPointY-5, likeSize.width,20);

    [likeLabel setText:likeString];
    }
}
- (void)setData
{
    ratingScoreControl.hidden = NO;

    NSURL *urlString;
    review.avatar = [review.avatar stringByReplacingPercentEscapesUsingEncoding:
                     NSASCIIStringEncoding];

    if ([review.avatar hasPrefix:@"http"])
    {
        urlString = [NSURL URLWithString:review.avatar];
    }
    else
    {
        urlString = [NSURL URLWithString:[review.avatar returnFullImageUrl]];
    }
    NSString *path_sandox = NSHomeDirectory();
    //设置一个图片的存储路径
    NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/avastar.png"];
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    image = [image cutCircleImage];
    if (image&&[review.userId intValue] == [[IMGUser currentUser].userId intValue]) {
        [avatarLinkButton setImage:image forState:UIControlStateNormal];
    }else{
        __weak UIButton *weakAvatarLinkButton = avatarLinkButton;
        [avatarLinkButton sd_setImageWithURL:urlString forState:UIControlStateNormal placeholderImage:[[UIImage imageNamed:@"headDefault.jpg"] cutCircleImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image.size.height>0&&image.size.width>0) {
                                image = [image cutCircleImage];
                                [weakAvatarLinkButton setImage:image forState:UIControlStateNormal];
                            }
        }];
    }
    review.fullName = [review.fullName filterHtml];
    [titleLabel setText:[NSString stringWithFormat:@"%@ wrote a review for %@",review.fullName,restaurant.title]];    
    [titleLabel setFrameWithOrign:CGPointMake(avatarLinkButton.endPointX + 15, 5) Width:DeviceWidth - LEFTLEFTSET*2 - avatarLinkButton.endPointX];
    TYLinkTextStorage *linkTextStorage = [[TYLinkTextStorage alloc]init];
    linkTextStorage.range = NSMakeRange(0, review.fullName.length);
    linkTextStorage.textColor = [UIColor blackColor];
    linkTextStorage.linkData = @"user";
    linkTextStorage.underLineStyle = kCTUnderlineStyleNone;
    NSMutableArray *temArry = [NSMutableArray array];
    [temArry addObject:linkTextStorage];
    TYLinkTextStorage *linkTextStorageRestaurant = [[TYLinkTextStorage alloc]init];
    linkTextStorageRestaurant.range = NSMakeRange(review.fullName.length+19, restaurant.title.length+1);
    linkTextStorageRestaurant.textColor = [UIColor blackColor];
    linkTextStorageRestaurant.linkData = @"restaurant";
    linkTextStorageRestaurant.underLineStyle = kCTUnderlineStyleNone;
    
    [temArry addObject:linkTextStorageRestaurant];
    
    
    [titleLabel addTextStorageArray:temArry];

    if ([review.userDishCount intValue]!=0&&[review.userReviewCount intValue]!=0) {
    
    [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@ - %@ %@",review.userReviewCount,[review.userReviewCount intValue] > 1 ? @"reviews":@"review",review.userDishCount,[review.userDishCount intValue] > 1 ? @"photos":@"photo"]];
    reviewAndPhotoCountLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.endPointY, reviewAndPhotoCountLabel.expectedWidth, reviewAndPhotoCountLabel.font.lineHeight);
    }else if ([review.userReviewCount intValue]==0||[review.userDishCount intValue]==0){
        if ([review.userReviewCount intValue]==0&&[review.userDishCount intValue]!=0) {
            
            [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@",review.userDishCount,[review.userDishCount intValue] > 1 ? @"photos":@"photo"]];
            reviewAndPhotoCountLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.endPointY, reviewAndPhotoCountLabel.expectedWidth, reviewAndPhotoCountLabel.font.lineHeight);
            
            
            
        }
        if ([review.userDishCount intValue]==0&&[review.userReviewCount intValue]!=0) {
            
            [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@",review.userReviewCount,[review.userReviewCount intValue] > 1 ? @"reviews":@"review"]];
            reviewAndPhotoCountLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.endPointY, reviewAndPhotoCountLabel.expectedWidth, reviewAndPhotoCountLabel.font.lineHeight);
            
            
            
        }
        
    }

    [timeLabel setText:[Date getTimeInteval_v4:[review.timeline longLongValue]/1000]];
    timeLabel.frame = CGRectMake(avatarLinkButton.frame.origin.x, reviewAndPhotoCountLabel.endPointY, timeLabel.expectedWidth, timeLabel.expectedHeight);
    if (timeLabel.frame.origin.y<avatarLinkButton.endPointY+15) {
        timeLabel.frame=CGRectMake(LEFTLEFTSET, avatarLinkButton.endPointY + 15, 240, 14);
    }
    
    [reviewTitleLabel setText:review.title];
    CGSize reviewTitleLabelSize = [reviewTitleLabel.text sizeWithFont:reviewTitleLabel.font constrainedToSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 1000)];
    if (review.title.length>0) {

        reviewTitleLabel.frame = CGRectMake(reviewTitleLabel.pointX, timeLabel.endPointY + 8, reviewTitleLabelSize.width, reviewTitleLabelSize.height);
    }else{
        reviewTitleLabel.frame = CGRectMake(reviewTitleLabel.pointX, timeLabel.endPointY, reviewTitleLabel.expectedWidth, 0);
    }

    
    
    float score = [review.score floatValue];
    [scoreLabel setText:[NSString stringWithFormat:@"%.1f",score/2]];
    scoreLabel.frame = CGRectMake(LEFTLEFTSET, reviewTitleLabel.endPointY + 3, scoreLabel.expectedWidth, scoreLabel.font.lineHeight);
    
    
    ratingScoreControl.frame = CGRectMake(ratingScoreControl.frame.origin.x, scoreLabel.frame.origin.y, 74, 15);
    [ratingScoreControl updateRating:[NSNumber numberWithFloat:score]];
    
   
    NSString *summarize=review.summarize;

  float  contentLabelHeight = [summarize sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 10000)].height;
    
    reviewSummarizeLabel.frame=CGRectMake(LEFTLEFTSET, scoreLabel.endPointY + 5, DeviceWidth - LEFTLEFTSET*2, contentLabelHeight);
    reviewSummarizeLabel.numberOfLines = 0;
    reviewSummarizeLabel.font = [UIFont systemFontOfSize:12];
    reviewSummarizeLabel.alpha=0.3;
    reviewSummarizeLabel.text=review.summarize;
    float currentPoint=reviewSummarizeLabel.endPointY;
    textOrPhotoEndY = reviewSummarizeLabel.endPointY + 10;
    if (dishArrM.count)
    {
        if (dishArrM.count == 1 && [review.imageHeight floatValue])
        {
            cardPhotosView.frame = CGRectMake(LEFTLEFTSET, reviewSummarizeLabel.endPointY+20, DeviceWidth-LEFTLEFTSET*2, [review.imageHeight floatValue]);
            cardPhotosView.cardPhotoHeight = [review.imageHeight floatValue];
        }
        else
        {
            cardPhotosView.frame = CGRectMake(LEFTLEFTSET, reviewSummarizeLabel.endPointY+20, DeviceWidth-LEFTLEFTSET*2, [CardPhotosView cardPhotoHeightWithPhotoCout:dishArrM.count]);
        }
        
        cardPhotosView.cardPhotosViewDelegate = self;
        [cardPhotosView setPhotos:dishArrM];
        
        cardPhotosView.hidden = NO;
        currentPoint = cardPhotosView.endPointY + 10;
        textOrPhotoEndY = cardPhotosView.endPointY + 10;
    }
    else
    {
        IMGUser *user = [IMGUser currentUser];
        if ([user.userId intValue] == [review.userId intValue])
        {
            uploadPhotoBtn.hidden = NO;
            uploadPhotoBtn.frame = CGRectMake(DeviceWidth/2 - 90, reviewSummarizeLabel.endPointY + 10, 180, 35);
            currentPoint = uploadPhotoBtn.endPointY + 10;
            textOrPhotoEndY = uploadPhotoBtn.endPointY + 20;

        }
    }
    
    //    IMGDish *dish = [dishArr firstObject];
    self.currentPointY=currentPoint;
    mainView.frame = CGRectMake(0, 0, DeviceWidth, currentPoint+30);

}
- (void)gotoOtherProfileWithUser
{
    IMGUser *user = [[IMGUser alloc] init];
    user.userId = [NSNumber numberWithInt:[review.userId intValue] ];
    user.userName = review.fullName;
    user.token = @"";
    user.avatar = review.avatar;

    OtherProfileViewController *opvc = [[OtherProfileViewController alloc] init];
    opvc.amplitude = @"Card Detail Page";

    IMGUser *currentUser = [IMGUser currentUser];
    if ([currentUser.userId intValue] == [user.userId intValue])
    {
        opvc.isOtherUser = NO;
    }else
    {
        opvc.isOtherUser = YES;
    }
    opvc.otherUser = user;
    [self.navigationController pushViewController:opvc animated:YES];
}


- (void)uploadPhotoBtnClick
{
    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        LoginViewController *loginVC=[[LoginViewController alloc]initWithFromWhere:[NSNumber numberWithInt:1]];
        UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
        [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
        return;
    }
    IMGUserReview *userReview = [[IMGUserReview alloc] init];
    userReview.userId = [NSNumber numberWithInt:[review.userId intValue]];
    userReview.userFullName = review.fullName;
    userReview.reviewId = review.reviewId;
    userReview.reviewSummarize = review.summarize;
    userReview.reviewTitle = review.title;
    userReview.restaurantId = restaurant.restaurantId;
    userReview.restaurantTitle = restaurant.title;
    ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc]initWithRestaurant:restaurant andOverallRating:[review.score floatValue]/2 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
    publishReviewVC.isEdit = YES;
    publishReviewVC.userReview = userReview;
    publishReviewVC.isUpdateReview = YES;
    publishReviewVC.amplitudeType = @"Review Card Detail";
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:publishReviewVC] animated:YES completion:^{

    }];
}

- (void)addLikeCommentShareView:(BOOL)isReadMoreTap andIsDataUnload:(BOOL)isDataUnload
{
    [self updateLikeCommentListCountLabel:YES andisReadMoreTap:isReadMoreTap];

    if(self.likeCommentShareView){
        [self.likeCommentShareView removeFromSuperview];
    }
   
    self.likeCommentShareView = [[UILikeCommentShareView alloc]initInSuperView:self.scrollView atStartX:0 startY:self.currentPointY+10 commentCount:[review.commentCount intValue] commentArray:commentsList liked:review.isLike withFooter:NO needCommentListView:YES];

    
    self.likeCommentShareView.likeCommentShareDelegate=self;
    
//    self.currentPointY = self.likeCommentShareView.endPointY+10;
    self.scrollView.contentSize = CGSizeMake(0, self.likeCommentShareView.endPointY+10+10);
    if (!isDataUnload) {
        self.currentPointY=self.scrollView.contentSize.height;

    }


    
    
    self.commentInputView = [[UICommentInputView alloc]initView:NO];
    self.commentInputView.frame=CGRectMake(0, DeviceHeight-44-self.commentInputView.frame.size.height, self.commentInputView.frame.size.width, self.commentInputView.frame.size.height);

    self.commentInputView.commentInputViewDelegate=self;
    [self.view addSubview:self.commentInputView];
}
- (void)addLikeCommentShareView:(BOOL)isReadMoreTap andIsDataUnload:(BOOL)isDataUnload andIsTem:(BOOL)isTemp
{
    [self updateLikeCommentListCountLabel:YES andisReadMoreTap:isReadMoreTap];
    
    if(self.likeCommentShareView){
        [self.likeCommentShareView removeFromSuperview];
    }
//    BOOL needCommentListView;

    self.likeCommentShareView = [[UILikeCommentShareView alloc]initInSuperView:self.scrollView atStartX:0 startY:self.currentPointY+10 commentCount:[review.commentCount intValue] commentArray:commentsList liked:review.isLike withFooter:NO needCommentListView:!isTemp];
    
    
    self.likeCommentShareView.likeCommentShareDelegate=self;
    
    //    self.currentPointY = self.likeCommentShareView.endPointY+10;
    self.scrollView.contentSize = CGSizeMake(0, self.likeCommentShareView.endPointY+10+10);
    if (!isDataUnload) {
        self.currentPointY=self.scrollView.contentSize.height;
        
    }
    
    
    
    
    self.commentInputView = [[UICommentInputView alloc]initView:NO];
    self.commentInputView.frame=CGRectMake(0, DeviceHeight-44-self.commentInputView.frame.size.height, self.commentInputView.frame.size.width, self.commentInputView.frame.size.height);
    
    self.commentInputView.commentInputViewDelegate=self;
    [self.view addSubview:self.commentInputView];
}
-(void)updateLikeCommentListCountLabel:(BOOL)initial andisReadMoreTap:(BOOL)isReadMoreTap{
    
    if(likeLabel.hidden==NO){
        self.currentPointY=likeLabel.startPointY-5;
    }else if(commentLabel.hidden==NO){
        self.currentPointY=commentLabel.startPointY-5;
    }else if(!initial){
        self.currentPointY=self.likeCommentShareView.startPointY-5;
    }
    
    CGFloat startPointY=textOrPhotoEndY;
    
    float start=LEFTLEFTSET;
    if([review.likeCount intValue]>0){
        NSString *likeString=[NSString stringWithFormat:@"%@ %@",review.likeCount,[review.likeCount intValue]>1?L(@"Likes"):L(@"Like")];
        CGSize likeSize=[likeString sizeWithFont:likeLabel.font constrainedToSize:CGSizeMake(DeviceWidth,20) lineBreakMode:NSLineBreakByWordWrapping];
        likeLabel.frame=CGRectMake(start, startPointY-5, likeSize.width,20);
        likeLabel.text=likeString;
        start+=likeSize.width+15;
        self.currentPointY=likeLabel.endPointY;
        [likeLabel setHidden:NO];
    }else if(likeLabel.hidden==NO){
        [likeLabel setHidden:YES];
    }
    if([review.commentCount intValue]>0){
        NSString *commentString=[NSString stringWithFormat:@"%@ %@",review.commentCount,[review.commentCount intValue]>1?L(@"Comments"):L(@"Comment")];
        CGSize commentSize=[commentString sizeWithFont:commentLabel.font constrainedToSize:CGSizeMake(DeviceWidth,20) lineBreakMode:NSLineBreakByWordWrapping];
        commentLabel.frame=CGRectMake(start, startPointY-5, commentSize.width,20);
        commentLabel.text=commentString;
        start+=commentSize.width+15;
        self.currentPointY=commentLabel.endPointY;
        [commentLabel setHidden:NO];
    }else if(commentLabel.hidden==NO){
        [commentLabel setHidden:YES];
    }
    
    if(!initial){
        [self.view endEditing:YES];
        
        self.likeCommentShareView.frame=CGRectMake(0, self.currentPointY+10, self.likeCommentShareView.frame.size.width, self.likeCommentShareView.frame.size.height);
        self.currentPointY = self.likeCommentShareView.endPointY+10;
        
        if (!isReadMoreTap) {
            self.scrollView.contentSize = CGSizeMake(0, self.currentPointY+10);
            self.currentPointY=self.scrollView.contentSize.height;
        }
        
        
        
    }
    else
    {
        if (!isReadMoreTap) {
            self.scrollView.contentSize = CGSizeMake(0, self.currentPointY+10);
            
        }

    }

}

#pragma mark - privates
- (NSAttributedString *)attributedString:(NSArray *__autoreleasing *)outURLs
                               URLRanges:(NSArray *__autoreleasing *)outURLRanges andText:(NSString *)text
{
    NSString *HTMLText = text;
    NSArray *URLs;
    NSArray *URLRanges;
    UIColor *color = [UIColor grayColor];
    UIFont *font = [UIFont systemFontOfSize:TITLE_LABEL_FONT];
    NSMutableParagraphStyle *mps = [[NSMutableParagraphStyle alloc] init];
    //    mps.lineSpacing = ceilf(font.pointSize * .5);
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor whiteColor];
    shadow.shadowOffset = CGSizeMake(0, 1);
    NSString *str = [NSString stringWithHTMLText:HTMLText baseURL:nil URLs:&URLs URLRanges:&URLRanges];
    NSMutableAttributedString *mas = [[NSMutableAttributedString alloc] initWithString:str attributes:@
                                      {
                                          NSForegroundColorAttributeName : color,
                                          NSFontAttributeName            : font,
                                          NSParagraphStyleAttributeName  : mps,
                                          NSShadowAttributeName          : shadow,
                                      }];
    [URLRanges enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
        [mas addAttributes:@
         {
             NSForegroundColorAttributeName : [UIColor color222222],
             //       NSUnderlineStyleAttributeName  : @(NSUnderlineStyleSingle)
             NSForegroundColorAttributeName : [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:TITLE_LABEL_FONT]
             
         } range:[obj rangeValue]];
    }];
    
    *outURLs = URLs;
    *outURLRanges = URLRanges;
    
    return [mas copy];
}

-(void)cardPhotosViewImageClick:(int)imageIndex{
    ImageListViewController *imageListViewController=[[ImageListViewController alloc] initWithRestaurant:restaurant andReview:review andDishArr:dishArrM andIndex:imageIndex andFromCellIndexPath:self.fromCellIndexPath];
    imageListViewController.delegate = self;
    [self.navigationController pushViewController:imageListViewController animated:YES];

}
-(void)viewFullImage:(IMGEntity *)dish andRestaurant:(IMGRestaurant*)restaurant_ andReview:(IMGReview*)review_{
    IMGDish *dish_ = (IMGDish *)[dishArrM firstObject];

    AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:dishArrM andPhotoTag:0 andRestaurant:restaurant_ andFromDetail:NO];
    albumViewController.title = dish_.userName;
    albumViewController.showPage=NO;
    albumViewController.review = review_;
//    albumViewController.albumReloadCellDelegate = self;
    albumViewController.amplitudeType = self.amplitudeType;
    [self.navigationController pushViewController:albumViewController animated:YES];
    
}
 
-(void)shareButtonClickwithbutton:(UIButton*)btn {
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/review/%@",QRAVED_WEB_SERVER_OLD,review.reviewId]];
    
    NSString *shareContentString = [NSString stringWithFormat:@"CHECK IT OUT NOW! A Restaurant Review by %@ on Qraved",review.fullName];
    ReviewActivityItemProvider *provider=[[ReviewActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
    provider.username = review.fullName;
    provider.restaurantTitle = review.restaurantTitle;
    provider.url=url;
    provider.title=shareContentString;
    TwitterActivityItemProvider *twitterProvider=[[TwitterActivityItemProvider alloc] initWithPlaceholderItem:@""];
    twitterProvider.title=[NSString stringWithFormat:L(@"Check out %@ review at %@ on #Qraved!"),review.fullName,review.restaurantTitle];
    
    
    UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterProvider] applicationActivities:nil];
    activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
    [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK IT OUT NOW!A Restaurant Review By %@ on Qraved!"),review.fullName] forKey:@"subject"];
    
    activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
        if (completed) {
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:@"Review card detail page" forKey:@"Location"];
            [eventProperties setValue:provider.activityType forKey:@"Channel"];
            [eventProperties setValue:review.reviewId forKey:@"ReviewID"];
            [eventProperties setValue:review.fullName forKey:@"ReviewerName"];

            [[Amplitude instance] logEvent:@"SH – Share Review" withEventProperties:eventProperties];
        }else{

        }
    };
    
    if ([activityCtl respondsToSelector:@selector(popoverPresentationController)]) {
        
        activityCtl.popoverPresentationController.sourceView = btn;
        
    }

    [self presentViewController:activityCtl animated:YES completion:^{
        
    }];

}


- (void)commentInputView:(UIView*)view postButtonTapped:(UIButton*)postButton commentInputText:(NSString *)text{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    postButton.enabled = NO;
    
    [ReviewHandler postComment:text reviewId:review.reviewId restaurantId:restaurant.restaurantId andBlock:^(IMGReviewComment *reviewComment) {
        postButton.enabled = YES;
        [self.commentInputView clearInputTextView];
        [self.view endEditing:YES];
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
        [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
        [eventProperties setValue:review.fullName forKey:@"ReviewFullName"];
        [eventProperties setValue:@"Review card detail page" forKey:@"Location"];
        [eventProperties setValue:reviewComment.commentId forKey:@"Comment_ID"];
        [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];

        [[Amplitude instance] logEvent:@"UC - Comment Review Succeed" withEventProperties:eventProperties];
        [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Review Succeed" withValues:eventProperties];

        
        if(reviewComment){
            
            [self.likeCommentShareView addNewComment:reviewComment];
            review.commentCount=[NSNumber numberWithInteger:[review.commentCount integerValue]+1];
            if(commentsList){
                if([commentsList isKindOfClass:[NSArray class]]){
                    commentsList=[NSMutableArray arrayWithArray:commentsList];
                }
                [commentsList insertObject:reviewComment atIndex:0];
            }else{
                commentsList = [NSMutableArray arrayWithObject:reviewComment];
            }
            if(commentsList.count>3){
                review.commentList = [NSMutableArray arrayWithArray:[commentsList subarrayWithRange:NSMakeRange(0, 3)]];
            }else{
                review.commentList = commentsList;
            }
            

            [self updateLikeCommentListCountLabel:NO andisReadMoreTap:NO];
            
            
            CGPoint ponit=CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
            if (ponit.y>0) {
                [self.scrollView setContentOffset:ponit];

            }
            
            if(self.cdpDelegate){
                [self.cdpDelegate CDP:self reloadTableCell:nil];
            }
        }
    } failure:^(NSString *errorReason){
        if([errorReason isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
        [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
        [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:[NSString stringWithFormat:@"%@",errorReason] forKey:@"Reason"];
        [eventProperties setValue:@"Review card detail page" forKey:@"Location"];
        [[Amplitude instance] logEvent:@"UC - Comment Review Failed" withEventProperties:eventProperties];
    }];
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
    [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
    [eventProperties setValue:review.fullName forKey:@"ReviewFullName"];
    [eventProperties setValue:@"Review card detail page" forKey:@"Location"];
    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];

    [[Amplitude instance] logEvent:@"UC - Comment Review Initiate" withEventProperties:eventProperties];
}


- (void)likeCommentShareView:(UIView*)likeCommentShareView_ viewPreviousCommentsTapped:(UIButton*)button lastComment:(id)comment{
    [[LoadingView sharedLoadingView] startLoading];
    NSString *lastCommentIdStr=@"0";
    if([comment isKindOfClass:[IMGReviewComment class]]){
        IMGReviewComment *reviewComment=(IMGReviewComment*)comment;
        if(reviewComment){
            lastCommentIdStr=[NSString stringWithFormat:@"%@",reviewComment.commentId];
        }
        
        [ReviewHandler previousComment:lastCommentIdStr targetId:review.reviewId andBlock:^(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount) {
            if(commentList&&commentList.count>0){
                [self.view endEditing:YES];
                
                if(commentList){
                    CGFloat likeCommentShareViewHeight=self.likeCommentShareView.frame.size.height;
                    [self.likeCommentShareView addPreviousComments:commentList];
                    
                    CGFloat height=self.likeCommentShareView.frame.size.height-likeCommentShareViewHeight;
                    self.currentPointY=self.scrollView.contentSize.height+height;
                    self.scrollView.contentSize=CGSizeMake(self.scrollView.contentSize.width, self.currentPointY);
                    
                    CGFloat height1 = [UILikeCommentShareView calculatedTableViewHeight:commentsList withFooter:NO];
                    
                    CGPoint ponit=CGPointMake(0, self.scrollView.contentSize.height-height1-DeviceHeight+65);
                    if (ponit.y>0) {
                        self.scrollView.contentOffset = ponit;

                    }
                    
                    [commentsList addObjectsFromArray:commentList];
                }
            }
        }];
    }else{
        [[LoadingView sharedLoadingView] stopLoading];
    }
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView_ likeButtonTapped:(UIButton *)button{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    //Review_ID, ReviewerUser_ID, Restaurant_ID, Location
    [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
    [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
    [eventProperties setValue:review.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:@"Review card detail page" forKey:@"Location"];
    [ReviewHandler likeReview:review.isLike reviewId:review.reviewId andBlock:^{
        if (!review.isLike) {
            [[Amplitude instance] logEvent:@"UC - UnLike Review Succeed" withEventProperties:eventProperties];
        }else{
            [[Amplitude instance] logEvent:@"UC - Like Review Succeed" withEventProperties:eventProperties];
        }
    }failure:^(NSString *exceptionMsg){
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
    }];
    [self.view endEditing:YES];
    review.isLike=!review.isLike;
    if(review.isLike){
        review.likeCount=[NSNumber numberWithInt:[review.likeCount intValue]+1];
        [[Amplitude instance] logEvent:@"UC - Like Review Initiate" withEventProperties:eventProperties];
    }else{
        review.likeCount=[NSNumber numberWithInt:[review.likeCount intValue]-1];
        [[Amplitude instance] logEvent:@"UC - UnLike Review Initiate" withEventProperties:eventProperties];
    }
    [self.likeCommentShareView updateLikedStatus];
    [self updateLikeCommentListCountLabel:NO andisReadMoreTap:NO];
    if(self.cdpDelegate){
        [self.cdpDelegate CDP:self reloadTableCell:nil];
    }

}


-(void)likeCommentShareView:(UIView *)likeCommentShareView commentCell:(UITableViewCell *)cell readMoreTapped:(id)comment offset:(CGFloat)offset{
    [self.view endEditing:YES];    
    self.currentPointY+=offset;
    self.scrollView.contentSize=CGSizeMake(self.scrollView.contentSize.width, self.currentPointY);
    [self addLikeCommentShareView:YES andIsDataUnload:NO];
}
- (void)cardPhotosChangeHeight:(CGFloat)height
{
//    review.imageHeight = [NSNumber numberWithFloat:height];
//    [self setData];
}
- (void)attributedLabel:(TYAttributedLabel *)attributedLabel textStorageClicked:(id<TYTextStorageProtocol>)TextRun atPoint:(CGPoint)point{
    
    
    if ([TextRun isKindOfClass:[TYLinkTextStorage class]]) {
        
        NSString *linkStr = ((TYLinkTextStorage*)TextRun).linkData;
        
        if ([linkStr hasPrefix:@"user"]) {

            IMGUser *user = [[IMGUser alloc] init];
            user.userId = [NSNumber numberWithInt:[review.userId intValue]];
            user.userName = review.fullName;
            user.token = @"";
            user.avatar = review.avatar;
            
            OtherProfileViewController *opvc = [[OtherProfileViewController alloc] init];
            opvc.amplitude = @"Card Detail Page";
            IMGUser *currentUser = [IMGUser currentUser];
            if ([currentUser.userId intValue] == [user.userId intValue])
            {
                opvc.isOtherUser = NO;
            }else
            {
                opvc.isOtherUser = YES;
            }
            opvc.otherUser = user;
            [self.navigationController pushViewController:opvc animated:YES];
            
        }else {
            DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:restaurant.restaurantId];
            dvc.amplitudeType = self.amplitudeType;
            [self.navigationController pushViewController:dvc animated:YES];
        }
    }


}

@end

