//
//  JournalCardViewController.m
//  Qraved
//
//  Created by lucky on 16/3/14.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "JournalCardViewController.h"
#import "IMGUser.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "UIImage+Helper.h"
#import "UIConstants.h"
#import "AppDelegate.h"
#import "OfferLabel.h"
#import "JournalHandler.h"
#import "UIImageView+WebCache.h"
#import "JournalActivityItemProvider.h"
#import "UILikeCommentShareView.h"
#import "LoadingView.h"
#import "UICommentInputView.h"
#import "Date.h"
#import "JournalDetailViewController.h"
#import "LikeView.h"
#import "HomeUtil.h"
#define RESTAURANT_IMAGE_HEIGHT 160

@interface JournalCardViewController ()<UIScrollViewDelegate,LikeCommentShareDelegate,CommentInputViewDelegate,JournalDetailViewControllerDelegate,LikeListViewDelegate>
{
    UIView *mainView;
    UIButton *saveBtn;
    UILabel *headerLabel;
    UILabel *dateLabel;
    UILabel *titleLabel;
    UILabel *categoryLabel;
    UIImageView *restaurantImageView;
    UIImageView *lineImageView;
    UILabel *likeLabel;
    UILabel *commentLabel;
    UILabel *listLabel;
    IMGUser *user;

    UIView *photoAndTitleView;
    
    NSMutableArray *commentsList;
    NSMutableArray* likelistArr;
    LikeView*  likepopView;
}
@end

@implementation JournalCardViewController

-(id)initWithJournal:(IMGMediaComment *)paramJournal {
    if(self= [super init]){
        _journal=paramJournal;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"Journal Card Detail page";
    self.currentPointY = 0;
    self.previousKeyboardHeight = 0.0f;
    self.currentOffsetY = 0.0f;    
    self.view.backgroundColor = [UIColor whiteColor];
    [self addBackBtn];
    [self addScrollView];
    [self addMainView];
    [self setData];
    [self getDataFromService];
}
- (void)getDataFromService
{
    [[LoadingView sharedLoadingView] startLoading];
    [JournalHandler previousComment:@"" targetId:_journal.mediaCommentId andBlock:^(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount) {
        commentsList = [NSMutableArray arrayWithArray:commentList];
        if(commentsList.count>3){
            _journal.commentList = [NSMutableArray arrayWithArray:[commentsList subarrayWithRange:NSMakeRange(0, 3)]];
        }else{
            _journal.commentList = commentsList;
        }
        if (commentCount) {
            _journal.commentCount =commentCount;
        }
        if (likeCount) {
            _journal.likeCount = likeCount;
        }
        [self addLikeCommentShareView:NO];

        if(self.cdpDelegate){
            [self.cdpDelegate CDP:self reloadTableCell:nil];
        }
        [[LoadingView sharedLoadingView] stopLoading];
    }];
    [self addLikeCommentShareView:NO andIstemp:YES];

}
-(void)viewWillAppear:(BOOL)animated
{


    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=NO;

}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[LoadingView sharedLoadingView] stopLoading];
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillHideNotification];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)addScrollView
{
    self.scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:self.scrollView];
    self.scrollView.backgroundColor = [UIColor whiteColor];
    self.scrollView.frame = CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height-UICOMMENTINPUTVIEWHEIGHT-44);
    self.scrollView.delegate=self;
}
- (void)addMainView
{
    mainView = [[UIView alloc] init];
    [self.scrollView addSubview:mainView];
    
    photoAndTitleView = [[UIView alloc] init];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoAndTitleViewClick)];
    [photoAndTitleView addGestureRecognizer:tap];
    photoAndTitleView.userInteractionEnabled = YES;
    [mainView addSubview:photoAndTitleView];
    
    headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET,5,DeviceWidth,20)];
    headerLabel.font=[UIFont systemFontOfSize:14];
    
    dateLabel=[[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET,headerLabel.endPointY,DeviceWidth,16)];
    dateLabel.font=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12];
    dateLabel.textColor=[UIColor colorCCCCCC];
    
    restaurantImageView = [[UIImageView alloc] initWithFrame:CGRectMake(LEFTLEFTSET,0,DeviceWidth-LEFTLEFTSET*2,RESTAURANT_IMAGE_HEIGHT)];
    
    titleLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    titleLabel.font=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:16];
    titleLabel.textColor=[UIColor color222222];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
    titleLabel.numberOfLines=0;
    
    categoryLabel=[[UILabel alloc] init];
    categoryLabel.lineBreakMode=NSLineBreakByWordWrapping;
    categoryLabel.numberOfLines=0;
    categoryLabel.font=[UIFont systemFontOfSize:14];
    categoryLabel.textColor=[UIColor colorCCCCCC];
    
    likeLabel=[[UILabel alloc] init];
    likeLabel.userInteractionEnabled=YES;
    UITapGestureRecognizer* likeGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(likeGestureTap:)];
    likeGesture.numberOfTapsRequired=1;
    [likeLabel addGestureRecognizer:likeGesture];
    likeLabel.font=[UIFont systemFontOfSize:12];
    likeLabel.textColor = [UIColor colorC2060A];
    likeLabel.hidden=YES;
    
    commentLabel=[[UILabel alloc] init];
    commentLabel.font=[UIFont systemFontOfSize:12];
    commentLabel.textColor = [UIColor colorC2060A];
    commentLabel.hidden=YES;
    
    listLabel=[[UILabel alloc] init];
    listLabel.font=[UIFont systemFontOfSize:12];
    listLabel.textColor = [UIColor colorC2060A];
    listLabel.hidden=YES;
    
    lineImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    [lineImageView setImage:[UIImage imageNamed:@"search_line"] ];
    
    [mainView addSubview:headerLabel];
    [mainView addSubview:dateLabel];
    [photoAndTitleView addSubview:restaurantImageView];
    [photoAndTitleView addSubview:titleLabel];
    [photoAndTitleView addSubview:categoryLabel];
    [mainView addSubview:likeLabel];
    [mainView addSubview:commentLabel];
    [mainView addSubview:listLabel];
    [mainView addSubview:lineImageView];

}
-(void)likeGestureTap:(UITapGestureRecognizer*)gesture
{
    if (likepopView) {
        [likepopView removeFromSuperview];
    }

    [[LoadingView sharedLoadingView]startLoading];
    [HomeUtil getLikeListFromServiceWithJournalArticleId:_journal andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
        
        
        likelistArr=[[NSMutableArray alloc]initWithArray:likeListArray];
        [[LoadingView sharedLoadingView]stopLoading];
       
        likepopView.likecount=likeviewcount;
      
        likepopView.likelistArray=likelistArr;
        }];
      likepopView=[[LikeView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
     likepopView.delegate=self;
     [self.view addSubview:likepopView];
}
-(void)LikeviewLoadMoreData:(NSInteger)offset
{
       [HomeUtil getLikeListFromServiceWithJournalArticleId:_journal andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
        
       likepopView.likecount=likeviewcount;
        [likelistArr addObjectsFromArray:likeListArray];
        [likepopView.likeUserTableView reloadData];
        
        
    }];


}
-(void)returnLikeCount:(NSInteger)count
{
//    CGFloat startPointY=self.currentPointY;
//    
//    float start=LEFTLEFTSET;
    if (count!=0) {
        
   
    NSString *likeString=[NSString stringWithFormat:@"%ld %@",(long)count,count>1?L(@"Likes"):L(@"Like")];
    [likeLabel setText:likeString];
     likeLabel.frame=CGRectMake(likeLabel.frame.origin.x, likeLabel.frame.origin.y, likeLabel.expectedWidth, likeLabel.frame.size.height);
    }
//    CGSize likeSize=[likeString sizeWithFont:likeLabel.font constrainedToSize:CGSizeMake(DeviceWidth,20) lineBreakMode:NSLineBreakByWordWrapping];
//    likeLabel.frame=CGRectMake(start, startPointY+10, likeSize.width,20);
    
}
- (void)setData
{
    NSString *urlString = [_journal.journalImageUrl returnFullImageUrlWithWidth:RESTAURANT_IMAGE_HEIGHT];
    
    __weak typeof(restaurantImageView) weakRestaurantImageView = restaurantImageView;
    UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 160)];

    [restaurantImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            image = [image imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 160)];
            
            [weakRestaurantImageView setImage:image];
    }];
    
    headerLabel.text = _journal.typeName;
    dateLabel.text = [Date getTimeInteval_v4:[_journal.createdDate longLongValue]/1000];
    titleLabel.text=[_journal.journalTitle filterHtml];
    titleLabel.frame=CGRectMake(LEFTLEFTSET,restaurantImageView.endPointY+10,DeviceWidth-LEFTLEFTSET*2,[self titleLabelHeight:_journal]);
    
    categoryLabel.text=_journal.categoryName;
    categoryLabel.frame=CGRectMake(LEFTLEFTSET,titleLabel.endPointY+5,DeviceWidth-LEFTLEFTSET*2,[self categoryLabelHeight:_journal]);
    
    photoAndTitleView.frame = CGRectMake(0, dateLabel.endPointY+5, DeviceWidth, categoryLabel.endPointY + 5);
    mainView.frame = CGRectMake(0, 0, DeviceWidth, dateLabel.endPointY+5+photoAndTitleView.frame.size.height+30);
    self.currentPointY=photoAndTitleView.endPointY;
    
    [self addLikeCommentShareView:NO];
    
}
- (void)addLikeCommentShareView:(BOOL)isReadMoreTap
{
    [self updateLikeCommentListCountLabel:YES andIsReadMoreTap:isReadMoreTap];
    
    if(self.likeCommentShareView){
        [self.likeCommentShareView removeFromSuperview];
    }

    self.likeCommentShareView = [[UILikeCommentShareView alloc]initInSuperView:self.scrollView atStartX:0 startY:self.currentPointY+10 commentCount:[_journal.commentCount intValue] commentArray:commentsList liked:_journal.isLike withFooter:NO needCommentListView:YES];
    self.likeCommentShareView.likeCommentShareDelegate=self;
    self.currentPointY = self.likeCommentShareView.endPointY+10;
    self.scrollView.contentSize = CGSizeMake(0, self.currentPointY+10);
    self.currentPointY=self.scrollView.contentSize.height;
    
    self.commentInputView = [[UICommentInputView alloc]initView:NO];
    self.commentInputView.frame=CGRectMake(0, DeviceHeight-44-self.commentInputView.frame.size.height, self.commentInputView.frame.size.width, self.commentInputView.frame.size.height);
    
    self.commentInputView.commentInputViewDelegate=self;
    [self.view addSubview:self.commentInputView];

}
- (void)addLikeCommentShareView:(BOOL)isReadMoreTap andIstemp:(BOOL)isTemp
{
    [self updateLikeCommentListCountLabel:YES andIsReadMoreTap:isReadMoreTap];
    
    if(self.likeCommentShareView){
        [self.likeCommentShareView removeFromSuperview];
    }
    
    self.likeCommentShareView = [[UILikeCommentShareView alloc]initInSuperView:self.scrollView atStartX:0 startY:self.currentPointY+10 commentCount:[_journal.commentCount intValue] commentArray:commentsList liked:_journal.isLike withFooter:NO needCommentListView:NO];
    self.likeCommentShareView.likeCommentShareDelegate=self;
    self.currentPointY = self.likeCommentShareView.endPointY+10;
    self.scrollView.contentSize = CGSizeMake(0, self.currentPointY+10);
    self.currentPointY=self.scrollView.contentSize.height;
    
    self.commentInputView = [[UICommentInputView alloc]initView:NO];
    self.commentInputView.frame=CGRectMake(0, DeviceHeight-44-self.commentInputView.frame.size.height, self.commentInputView.frame.size.width, self.commentInputView.frame.size.height);
    
    self.commentInputView.commentInputViewDelegate=self;
    [self.view addSubview:self.commentInputView];
    
}


-(void)updateLikeCommentListCountLabel:(BOOL)initial andIsReadMoreTap:(BOOL)isReadMoretap{
    
    if(likeLabel.hidden==NO){
        self.currentPointY=likeLabel.startPointY-10;
    }else if(commentLabel.hidden==NO){
        self.currentPointY=commentLabel.startPointY-10;
    }else if(!initial){
        self.currentPointY=self.likeCommentShareView.startPointY-10;
    }
    
    CGFloat startPointY=self.currentPointY;
    
    float start=LEFTLEFTSET;
    if([_journal.likeCount intValue]>0){
        NSString *likeString=[NSString stringWithFormat:@"%@ %@",_journal.likeCount,[_journal.likeCount intValue]>1?L(@"Likes"):L(@"Like")];
        CGSize likeSize=[likeString sizeWithFont:likeLabel.font constrainedToSize:CGSizeMake(DeviceWidth,20) lineBreakMode:NSLineBreakByWordWrapping];
        likeLabel.frame=CGRectMake(start, startPointY+10, likeSize.width,20);
        likeLabel.text=likeString;
        start+=likeSize.width+15;
        self.currentPointY=likeLabel.endPointY;
        [likeLabel setHidden:NO];
    }else if(likeLabel.hidden==NO){
        [likeLabel setHidden:YES];
    }
    if([_journal.commentCount intValue]>0){
        NSString *commentString=[NSString stringWithFormat:@"%@ %@",_journal.commentCount,[_journal.commentCount intValue]>1?L(@"Comments"):L(@"Comment")];
        CGSize commentSize=[commentString sizeWithFont:commentLabel.font constrainedToSize:CGSizeMake(DeviceWidth,20) lineBreakMode:NSLineBreakByWordWrapping];
        commentLabel.frame=CGRectMake(start, startPointY+10, commentSize.width,20);
        commentLabel.text=commentString;
        start+=commentSize.width+15;
        self.currentPointY=commentLabel.endPointY;
        [commentLabel setHidden:NO];
    }else if(commentLabel.hidden==NO){
        [commentLabel setHidden:YES];
    }
    if([_journal.listCount intValue]>0){
        NSString *listString=[NSString stringWithFormat:@"%@ %@",_journal.listCount,L(@"Saved")];
        CGSize listSize=[listString sizeWithFont:listLabel.font constrainedToSize:CGSizeMake(DeviceWidth,20) lineBreakMode:NSLineBreakByWordWrapping];
        listLabel.frame=CGRectMake(start, startPointY+10, listSize.width,20);
        listLabel.text=listString;
        start+=listSize.width+15;
        self.currentPointY=listLabel.endPointY;
        [listLabel setHidden:NO];
    }else if(listLabel.hidden==NO){
        [listLabel setHidden:YES];
    }
    
    if(!initial){
        [self.view endEditing:YES];
        if(self.currentPointY-startPointY>0.1){
            self.likeCommentShareView.frame=CGRectMake(0, self.currentPointY+10, self.likeCommentShareView.frame.size.width, self.likeCommentShareView.frame.size.height);
            
            self.currentPointY = self.likeCommentShareView.endPointY+10;
            
        }
        if (!isReadMoretap) {
            self.scrollView.contentSize = CGSizeMake(0, self.currentPointY+10);
            self.currentPointY=self.scrollView.contentSize.height;
        }

    }
}

-(CGFloat)titleLabelHeight:(IMGMediaComment *)journal{
    CGSize titleSize=[[journal.journalTitle filterHtml] sizeWithFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:18] constrainedToSize:CGSizeMake(DeviceWidth-LEFTLEFTSET*2,1000) lineBreakMode:NSLineBreakByWordWrapping];
    return titleSize.height;
}
-(CGFloat)categoryLabelHeight:(IMGMediaComment*)journal{
    CGSize categoryLabelSize=[journal.categoryName sizeWithFont:[UIFont systemFontOfSize:16] constrainedToSize:CGSizeMake(DeviceWidth-LEFTLEFTSET*2,1000) lineBreakMode:NSLineBreakByWordWrapping];
    return categoryLabelSize.height;
}



 


-(void)shareButtonClickwithbutton:(UIButton*)btn{
    NSString *journal_link=_journal.link;
//    if([[_journal.link lowercaseString] hasPrefix:@"https://"]){
//        journal_link=[NSString stringWithFormat:@"http://%@",[_journal.link substringFromIndex:[@"https://" length]]];
//    }
//    
    NSString *shareContentString = [NSString stringWithFormat:L(@"%@ on Qraved. %@"),_journal.journalTitle,journal_link];
    // NSString *shareContentString=_journal.link;
    NSURL *url=[NSURL URLWithString:journal_link];
    JournalActivityItemProvider *provider=[[JournalActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
    provider.url=url;
    provider.title=_journal.journalTitle;
    provider.journalId=_journal.mediaCommentId;
    UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider] applicationActivities:nil];
    activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
    [activityCtl setValue:[NSString stringWithFormat:L(@"Must Go! %@"),_journal.journalTitle] forKey:@"subject"];
    activityCtl.completionWithItemsHandler = ^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
        if (completed) {
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:_journal.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:_journal.restaurantTitle forKey:@"RestaurantTitle"];
            [eventProperties setValue:@"Journal card detail page" forKey:@"Location"];
            [eventProperties setValue:provider.activityType forKey:@"Channel"];
            [eventProperties setValue:_journal.mediaCommentId forKey:@"Journal_ID"];

            [[Amplitude instance] logEvent:@"SH - Share Journal" withEventProperties:eventProperties];
            [[AppsFlyerTracker sharedTracker] trackEvent:@"SH - Share Journal" withValues:eventProperties];

        }else{
        }
    };
    if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
        // iOS8
        activityCtl.popoverPresentationController.sourceView =btn;
    }


    [self presentViewController:activityCtl animated:YES completion:^{
        
    }];
}

- (void)commentInputView:(UIView*)view postButtonTapped:(UIButton*)postButton commentInputText:(NSString *)text{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:_journal.mediaCommentId forKey:@"Journal_ID"];
    [eventProperties setValue:@"Journal card detail page" forKey:@"Location"];
    
    [JournalHandler postComment:text articleId:self.journal.mediaCommentId andBlock:^(IMGJournalComment *journalComment){
        [self.commentInputView clearInputTextView];
        [self.view endEditing:YES];
        
        [eventProperties setValue:journalComment.commentId forKey:@"Comment_ID"];
        [[Amplitude instance] logEvent:@"UC - Comment Journal Succeed" withEventProperties:eventProperties];
        [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Journal Succeed" withValues:eventProperties];

        if(journalComment){
            
            [self.likeCommentShareView addNewComment:journalComment];
            _journal.commentCount=[NSNumber numberWithInteger:[_journal.commentCount integerValue]+1];
            
          
            
            
            
            if(commentsList){
                if([commentsList isKindOfClass:[NSArray class]]){
                    commentsList=[NSMutableArray arrayWithArray:commentsList];
                }
                [commentsList insertObject:journalComment atIndex:0];
            }else{
                commentsList = [NSMutableArray arrayWithObject:journalComment];
            }
            if(commentsList.count>3){
                _journal.commentList = [NSMutableArray arrayWithArray:[commentsList subarrayWithRange:NSMakeRange(0, 3)]];
            }else{
                _journal.commentList = commentsList;
            }
            
            [self updateLikeCommentListCountLabel:NO andIsReadMoreTap:NO];
            CGPoint point=CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
            if (point.y>0) {
                [self.scrollView setContentOffset:point];
                

            }
            if(self.cdpDelegate){
                [self.cdpDelegate CDP:self reloadTableCell:nil];
            }
        }
    } failure:^(NSString *errorReason){
        if([errorReason isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
        [eventProperties setValue:errorReason forKey:@"Reason"];
        [[Amplitude instance] logEvent:@"UC - Comment Journal Failed" withEventProperties:eventProperties];
    }];
    [[Amplitude instance] logEvent:@"UC - Comment Journal Initiate" withEventProperties:eventProperties];
}

- (void)likeCommentShareView:(UIView*)likeCommentShareView_ viewPreviousCommentsTapped:(UIButton*)button lastComment:(id)comment{
    [[LoadingView sharedLoadingView] startLoading];
    NSString *lastCommentIdStr=@"0";
    if([comment isKindOfClass:[IMGJournalComment class]]){
        IMGJournalComment *journalComment=(IMGJournalComment*)comment;
        if(journalComment){
            lastCommentIdStr=[NSString stringWithFormat:@"%@",journalComment.commentId];
        }
        
        [JournalHandler previousComment:lastCommentIdStr targetId:_journal.mediaCommentId andBlock:^(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount) {
            if(commentList&&commentList.count>0){
                [self.view endEditing:YES];
                
                if(commentList){
                    CGFloat likeCommentShareViewHeight=self.likeCommentShareView.frame.size.height;
                    [self.likeCommentShareView addPreviousComments:commentList];
                    
                    CGFloat height=self.likeCommentShareView.frame.size.height-likeCommentShareViewHeight;
                    self.currentPointY=self.scrollView.contentSize.height+height;
                    self.scrollView.contentSize=CGSizeMake(self.scrollView.contentSize.width, self.currentPointY);
                    
                    CGFloat height1 = [UILikeCommentShareView calculatedTableViewHeight:commentsList withFooter:NO];
                    CGPoint ponit=CGPointMake(0, self.scrollView.contentSize.height-height1-DeviceHeight+65);
                    if (ponit.y>0) {
                        self.scrollView.contentOffset = ponit;

                    }
                    
                    
                    [commentsList addObjectsFromArray:commentList];
                }
            }
        }];
    }else{
        [[LoadingView sharedLoadingView] stopLoading];
    }
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView_ likeButtonTapped:(UIButton *)button{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:_journal.mediaCommentId forKey:@"Journal_ID"];
    [eventProperties setValue:@"Journal card detail page" forKey:@"Location"];
    
    [JournalHandler likeJournal:_journal.isLike articleId:_journal.mediaCommentId andBlock:^{
        if (!_journal.isLike) {
            [[Amplitude instance] logEvent:@"UC – UnLike Journal Succeed" withEventProperties:eventProperties];
        }else{
            [[Amplitude instance] logEvent:@"UC - Like Journal Succeed" withEventProperties:eventProperties];
        }
    } failure:^(NSString *exceptionMsg){
        
    }];
    [self.view endEditing:YES];
    _journal.isLike=!_journal.isLike;
    if(_journal.isLike){
        _journal.likeCount=[NSNumber numberWithInt:[_journal.likeCount intValue]+1];
        [[Amplitude instance] logEvent:@"UC - Like Journal Initiate" withEventProperties:eventProperties];
    }else{
        _journal.likeCount=[NSNumber numberWithInt:[_journal.likeCount intValue]-1];
        [[Amplitude instance] logEvent:@"UC - Unlike Journal Initiate" withEventProperties:eventProperties];
    }
    [self.likeCommentShareView updateLikedStatus];
    [self updateLikeCommentListCountLabel:NO andIsReadMoreTap:NO];
    if(self.cdpDelegate){
        [self.cdpDelegate CDP:self reloadTableCell:nil];
    }

}

-(void)likeCommentShareView:(UIView *)likeCommentShareView commentCell:(UITableViewCell *)cell readMoreTapped:(id)comment offset:(CGFloat)offset{
    [self.view endEditing:YES];
    self.currentPointY+=offset;
    self.scrollView.contentSize=CGSizeMake(self.scrollView.contentSize.width, self.currentPointY);
    [self addLikeCommentShareView:YES];
}
- (void)photoAndTitleViewClick
{
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:self.journal.restaurantId forKey:@"RestaurantID"];
    [eventProperties setValue:self.journal.restaurantTitle forKey:@"RestaurantTitle"];
    [eventProperties setValue:[IMGUser currentUser].userId forKey:@"UserID"];
    [eventProperties setValue:[IMGUser currentUser].userName forKey:@"UserName"];
    [[Amplitude instance] logEvent:@"Click -  Article card" withEventProperties:eventProperties];
    
    JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
    jdvc.amplitudeType = @"Journal card detail page";
    jdvc.journal = _journal;
    jdvc.journalDetailViewControllerDelegate=self;
    jdvc.fromCellIndexPath=self.fromCellIndexPath;
    [self.navigationController pushViewController:jdvc animated:YES];
}

- (void)reloadComment{
    [self addLikeCommentShareView:NO];
    if(self.cdpDelegate){
        [self.cdpDelegate CDP:self reloadTableCell:nil];
    }
}


@end
