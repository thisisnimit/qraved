//
//  CardDetailViewController.m
//  Qraved
//
//  Created by lucky on 16/3/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "CardDetailViewController.h"
#import "UIView+Helper.h"
#import "IMGUser.h"
#import "IMGRestaurant.h"
#import "DetailViewController.h"
#import "OtherProfileViewController.h"

@interface CardDetailViewController ()

@end

@implementation CardDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

 - (void)likeCommentShareViewUserNameOrImageTapped:(id)sender
{
    if ([sender isKindOfClass:[IMGUser class]])
    {
        IMGUser *user = (IMGUser *)sender;
        if (![user.userId intValue]) {
            return;
        }
        OtherProfileViewController *opvc = [[OtherProfileViewController alloc] init];
        opvc.amplitude = @"Card detail Page";
        IMGUser *currentUser = [IMGUser currentUser];
        if ([currentUser.userId intValue] == [user.userId intValue])
        {
            opvc.isOtherUser = NO;
        }else
        {
            opvc.isOtherUser = YES;
        }
        opvc.otherUser = user;
        [self.navigationController pushViewController:opvc animated:YES];
    }
    else if ([sender isKindOfClass:[IMGRestaurant class]])
    {
        IMGRestaurant *restaurant = (IMGRestaurant *)sender;
        DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:restaurant.restaurantId];
        dvc.amplitudeType = @"Card detail page";
        [self.navigationController pushViewController:dvc animated:YES];
    }

}


-(void)shareButtonClickwithbutton:(UIButton*)btn{
    
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView_{
    if(scrollView_==self.scrollView){
        self.currentOffsetY = self.scrollView.contentOffset.y;
    }else{
        NSLog(@"abc");
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView_{
    [self.view endEditing:YES];
}

-(void)keyboardChange:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    if (notification.name == UIKeyboardWillShowNotification) {
        CGFloat keyboardOffset = keyboardEndFrame.size.height;
        if(self.previousKeyboardHeight>0){
            keyboardOffset=keyboardEndFrame.size.height-self.previousKeyboardHeight;
        }
        
        if(self.currentPointY<DeviceHeight){
            CGFloat diff = DeviceHeight-self.currentPointY-20;

            if(diff>keyboardEndFrame.size.height+self.commentInputView.frame.size.height){
                
            }else{
                CGFloat moveOffset = keyboardEndFrame.size.height+self.commentInputView.frame.size.height-diff;
                self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, self.currentPointY+keyboardEndFrame.size.height);
                [self.scrollView setContentOffset:CGPointMake(0, moveOffset) animated:YES];
            }
        }else{
            self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, self.currentPointY+keyboardEndFrame.size.height);
            [self.scrollView setContentOffset:CGPointMake(0, self.currentOffsetY+keyboardOffset) animated:YES];
        }
        
        
        self.commentInputView.frame=CGRectMake(self.commentInputView.startPointX, self.commentInputView.startPointY-keyboardOffset, self.commentInputView.frame.size.width, self.commentInputView.frame.size.height);
        self.previousKeyboardHeight=keyboardEndFrame.size.height;
    }else{
        self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, self.currentPointY);
        self.commentInputView.frame=CGRectMake(0, DeviceHeight-44-self.commentInputView.frame.size.height, self.commentInputView.frame.size.width, self.commentInputView.frame.size.height);
        self.previousKeyboardHeight=0.0f;
    }
    
    [UIView commitAnimations];
}


- (void)commentInputView:(UIView*)view inputViewExpand:(UITextView*)inputTextView commentInputViewYOffset:(CGFloat)commentInputViewYOffset{
    [UIView animateWithDuration:.5f
                     animations:^{
                         self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, self.scrollView.contentSize.height+commentInputViewYOffset);
                         self.currentPointY+=commentInputViewYOffset;
                     } completion:^(BOOL finished) {
                         
                     }
     ];
    
}

-(void)commentInputView:(UIView *)view inputViewDidBeginEditing:(UITextView *)textView{
    
}

- (void)likeCommentShareView:(UIView*)likeCommentShareView commentInputView:(UIView *)view inputViewDidBeginEditing:(UITextView *)textView{
    
}


-(void)likeCommentShareView:(UIView *)likeCommentShareView shareButtonTapped:(UIButton *)button{
    [self shareButtonClickwithbutton:button];
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView commentButtonTapped:(UIButton *)button{
    [self.commentInputView becomeFirstResponderToInputTextView];
}

@end
