//
//  OfferCardViewController.m
//  Qraved
//
//  Created by josn.liu on 2016/12/7.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "OfferCardViewController.h"
#import "IMGOfferCard.h"
#import "DLStarRatingControl.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "UIImageView+WebCache.h"
#import "LoadingView.h"
#import "RestaurantHandler.h"
#import "AppDelegate.h"
#import "RestaurantEventActivityItemProvider.h"
#import "TwitterActivityItemProvider.h"
#import "LikeView.h"
#import "HomeUtil.h"
#import "AddToListController.h"
#import "IMGRestaurant.h"
#import "Date.h"
#import "DetailViewController.h"
#import "SpecialOffersViewController.h"

#define OFFSET 12.0
@interface OfferCardViewController ()<UIScrollViewDelegate,CommentInputViewDelegate,LikeCommentShareDelegate,LikeListViewDelegate>
{
    UIView *mainView;
    UILabel *topTitleLabel;
    UIImageView *titleImageViewLine;
    UILabel *restaurantTitleLabel;
    UIButton *saveBtn;
    UILabel *creatTimelable;
    DLStarRatingControl *ratingScoreControl;
    UILabel *ratingScoreLabel;
    UIImageView *imageView;
    UILabel *titleLabel;
    UIButton *btn;
    UIImageView *lineButtom;
    UIImageView *lineLeft;
    UIImageView *lineRight;
    UILabel *likeLabel;
    UILabel *commentLabel;
    UILikeCommentShareView *likeCommentShareView;
    UIView *separateView;
    IMGOfferCard *_offermodel;
    NSMutableArray *commentsList;
    LikeView *likepopView;
    NSMutableArray *likeListArr;

}
@end

@implementation OfferCardViewController
-(id)initWithRestaurant:(IMGOfferCard *)paramRestaurant andRestaurantEventList:(NSArray *)restaurantEventList
{
    if(self= [super init]){
      _offermodel=paramRestaurant;
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillHideNotification object:nil];
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[LoadingView sharedLoadingView] stopLoading];
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillHideNotification];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setBackBarButtonOffset30];

    [self addScrollView];
    [self addmainView];
    [self getDataFromService];
}
- (void)addScrollView
{
    self.scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:self.scrollView];
    self.scrollView.backgroundColor = [UIColor whiteColor];
    self.scrollView.frame = CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height-UICOMMENTINPUTVIEWHEIGHT-44);
    self.scrollView.delegate=self;
}

-(void)addmainView{
    mainView = [[UIView alloc] init];
    [self.scrollView addSubview:mainView];

    topTitleLabel = [[UILabel alloc] init];
    topTitleLabel.numberOfLines = 0;
    topTitleLabel.textAlignment = NSTextAlignmentLeft;
    [mainView addSubview:topTitleLabel];
    //
    titleImageViewLine = [[UIImageView alloc] init];
    [titleImageViewLine setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"search_line"]]];
    [mainView addSubview:titleImageViewLine];
    creatTimelable=[[UILabel alloc]init];
    creatTimelable.textAlignment=NSTextAlignmentLeft;
    creatTimelable.textColor = [UIColor grayColor];
    creatTimelable.alpha = 0.7f;
    creatTimelable.font = [UIFont systemFontOfSize:12];
    [mainView addSubview:creatTimelable];
    
    
    //
    restaurantTitleLabel = [[UILabel alloc] init];
    restaurantTitleLabel.numberOfLines = 2;
    restaurantTitleLabel.textColor=[UIColor colorWithRed:97/255.0 green:97/255.0 blue:97/255.0 alpha:1];
    restaurantTitleLabel.font=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:17];
    restaurantTitleLabel.textAlignment = NSTextAlignmentLeft;
    restaurantTitleLabel.userInteractionEnabled=YES;
    UITapGestureRecognizer *restaurantTitleTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(restaurantTitleTapAct:)];
    [restaurantTitleLabel addGestureRecognizer:restaurantTitleTap];

    [mainView addSubview:restaurantTitleLabel];
    //
    saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [saveBtn setImage:[UIImage imageNamed:@"unsave_list"] forState:UIControlStateNormal];
    [saveBtn setImage:[UIImage imageNamed:@"save_list__"] forState:UIControlStateSelected];

    [saveBtn addTarget:self action:@selector(saveBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [mainView addSubview:saveBtn];
    //
    //
    ratingScoreLabel = [[UILabel alloc] init];
    ratingScoreLabel.numberOfLines = 0;
    ratingScoreLabel.textAlignment =NSTextAlignmentCenter;
    ratingScoreLabel.textColor = [UIColor colorWithRed:238.0/255.0 green:187.0/255.0 blue:56.0/255.0 alpha:1];
    ratingScoreLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:12];
    [mainView addSubview:ratingScoreLabel];
    ratingScoreControl=[[DLStarRatingControl alloc] initWithFrame:CGRectZero andStars:5 andStarWidth:12 isFractional:YES spaceWidth:5];
    ratingScoreControl.userInteractionEnabled=NO;
    [mainView addSubview:ratingScoreControl];

    
    //
    imageView = [[UIImageView alloc] init];
    [mainView addSubview:imageView];
    //
    titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.textColor = [UIColor color333333];
    titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:14];
    titleLabel.userInteractionEnabled=YES;
    UITapGestureRecognizer *titleLabelTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gotoCardDetailView:)];
    [titleLabel addGestureRecognizer:titleLabelTap];

    [mainView addSubview:titleLabel];
    //
    lineButtom = [[UIImageView alloc] init];
    [lineButtom setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"search_line"]]];
    [mainView addSubview:lineButtom];
    //
    lineLeft = [[UIImageView alloc] init];
    [lineLeft setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"search_line"]]];
    [mainView addSubview:lineLeft];
    //
    lineRight = [[UIImageView alloc] init];
    [lineRight setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"search_line"]]];
    [mainView addSubview:lineRight];
    //
    
    likeLabel=[[UILabel alloc] init];
    likeLabel.textAlignment = NSTextAlignmentCenter;
    likeLabel.font=[UIFont systemFontOfSize:12];
    likeLabel.textColor = [UIColor colorC2060A];
    likeLabel.hidden=YES;
    likeLabel.userInteractionEnabled=YES;
    UITapGestureRecognizer* likeTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(likeLabelTap:)];
    likeTap.numberOfTapsRequired=1;
    [likeLabel addGestureRecognizer:likeTap];
    [mainView addSubview:likeLabel];
    
    commentLabel=[[UILabel alloc] init];
    commentLabel.textAlignment = NSTextAlignmentCenter;
    commentLabel.hidden=YES;
    commentLabel.font=[UIFont systemFontOfSize:12];
    commentLabel.textColor = [UIColor colorC2060A];
//    commentLabel.userInteractionEnabled = YES;
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoCardDetailPage)];
//    [commentLabel addGestureRecognizer:tap];
    [mainView addSubview:commentLabel];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"A new Promo posted"];
    [attributedString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:DEFAULT_FONT_NAME size:16],NSForegroundColorAttributeName:[UIColor colorWithRed:97/255.0 green:97/255.0 blue:97/255.0 alpha:1]} range:NSMakeRange(0,6)];
    [attributedString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:16],NSForegroundColorAttributeName:[UIColor colorWithRed:3/255.0 green:3/255.0 blue:3/255.0 alpha:1]} range:NSMakeRange(6,5)];
    [attributedString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:DEFAULT_FONT_NAME size:16],NSForegroundColorAttributeName:[UIColor colorWithRed:97/255.0 green:97/255.0 blue:97/255.0 alpha:1]} range:NSMakeRange(12,6)];
    topTitleLabel.attributedText = attributedString;
    topTitleLabel.frame = CGRectMake(OFFSET, 0, DeviceWidth - 2*OFFSET, 38);
    //
    titleImageViewLine.frame = CGRectMake(0, topTitleLabel.endPointY, DeviceWidth, 1);
    [creatTimelable setText:[Date getTimeInteval_v4:[_offermodel.timeline longLongValue]/1000]];
    creatTimelable.frame=CGRectMake(15, titleImageViewLine.endPointY+3, DeviceWidth/2, 15);
    
    NSString *resturantsTitle=[NSString stringWithFormat:@"%@ - %@",[_offermodel.restaurantTitle removeHTML],[_offermodel.landMark removeHTML]];
    if (_offermodel.landMark.length==0) {
        resturantsTitle=[NSString stringWithFormat:@"%@",_offermodel.restaurantTitle];
    }
    attributedString =[ [NSMutableAttributedString alloc] initWithString:resturantsTitle];
    [attributedString setAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:18],NSForegroundColorAttributeName:[UIColor color222222]} range:NSMakeRange(0,_offermodel.restaurantTitle.length)];
    restaurantTitleLabel.attributedText = attributedString;
    UIFont *tiltleFont=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:18];
    CGSize titleSize=[restaurantTitleLabel.text sizeWithFont:tiltleFont constrainedToSize:CGSizeMake(DeviceWidth-50,tiltleFont.lineHeight*2) lineBreakMode:NSLineBreakByTruncatingTail];

    restaurantTitleLabel.frame = CGRectMake(OFFSET, creatTimelable.endPointY + OFFSET, DeviceWidth-50, titleSize.height);
    //
    if (_offermodel.isSaved)
    {
        [saveBtn setImage:[UIImage imageNamed:@"save_list__"] forState:UIControlStateNormal];
    }
    else{
        [saveBtn setImage:[UIImage imageNamed:@"unsave_list"] forState:UIControlStateNormal];
        
    }

    saveBtn.frame = CGRectMake(DeviceWidth - OFFSET - 32, restaurantTitleLabel.frame.origin.y - 5, 32, 32);
    //
    float score = [_offermodel.rating floatValue];
    [ratingScoreLabel setText:[NSString stringWithFormat:@"%.1f",score/2]];
    [ratingScoreLabel sizeToFit];
    ratingScoreLabel.frame = CGRectMake(OFFSET, restaurantTitleLabel.endPointY+2, ratingScoreLabel.frame.size.width, ratingScoreLabel.frame.size.height);
    ratingScoreControl.frame=CGRectMake(ratingScoreLabel.endPointX+5, restaurantTitleLabel.endPointY+4, 80, 12);
    [ratingScoreControl updateRating:[NSNumber numberWithFloat:[_offermodel.rating floatValue]]];
    //
    imageView.frame = CGRectMake(OFFSET, ratingScoreLabel.endPointY + OFFSET, DeviceWidth - 2*OFFSET, [_offermodel.offerImageHeight floatValue]);
    imageView.userInteractionEnabled=YES;
    UITapGestureRecognizer *imageviewTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gotoCardDetailView:)];
    [imageView addGestureRecognizer:imageviewTap];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[_offermodel.offerImageUrl returnFullImageUrlWithWidth:imageView.frame.size.width]]  placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
    
    //
    titleLabel.text =[_offermodel.offerTitle removeHTML];
    titleLabel.frame = CGRectMake(imageView.frame.origin.x+OFFSET, imageView.endPointY, (imageView.frame.size.width - OFFSET *3), 55);
    //
    lineButtom.frame = CGRectMake(imageView.frame.origin.x,titleLabel.endPointY,imageView.frame.size.width,1);
    lineLeft.frame = CGRectMake(imageView.frame.origin.x,imageView.endPointY,1,titleLabel.frame.size.height);
    lineRight.frame = CGRectMake(imageView.endPointX,imageView.endPointY,1,titleLabel.frame.size.height);

    self.currentPointY=lineButtom.endPointY;
    mainView.frame = CGRectMake(0, 0, DeviceWidth, lineButtom.endPointY+30);
    [self addLikeCommentShareView:YES];
}
-(void)restaurantTitleTapAct:(UITapGestureRecognizer*)tap{
    
    DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:_offermodel.restaurantId];
    
    [self.navigationController pushViewController:dvc animated:YES];

}
-(void)gotoCardDetailView:(UITapGestureRecognizer*)tap{

    IMGRestaurant *restaurant=[[IMGRestaurant alloc]init];
    restaurant.restaurantId=_offermodel.restaurantId;
    SpecialOffersViewController *specialOffersViewController = [[SpecialOffersViewController alloc]initWithRestaurant:restaurant andTimeArray:nil andOfferArray:nil];
    specialOffersViewController.amplitudeType = @"Offer card detail page";
    [self.navigationController pushViewController:specialOffersViewController animated:YES];
}
-(void)saveBtnClick:(UIButton*)button{

    button.selected=!button.selected;
    if (!_offermodel.isSaved){
        [saveBtn setImage:[UIImage imageNamed:@"save_list__"] forState:UIControlStateNormal];
    }else{
        [saveBtn setImage:[UIImage imageNamed:@"unsave_list"] forState:UIControlStateNormal];
    }

    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        LoginViewController *loginVC=[[LoginViewController alloc]initWithFromWhere:[NSNumber numberWithInt:1]];
        UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
        [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
        return;
    }
    IMGRestaurant *restaurant=[[IMGRestaurant alloc]init];
    restaurant.saved=[NSNumber numberWithBool:_offermodel.isSaved];
    restaurant.restaurantId=_offermodel.restaurantId;
    AddToListController *addToList=[[AddToListController alloc] initWithRestaurant:restaurant];
    addToList.amplitudeType = @"Restaurant update card detail page";
    void(^callback)(BOOL saved)=^(BOOL saved){
        _offermodel.isSaved=saved;
        [self.cdpDelegate reloadCell];
      
    };
    addToList.addToListBlock=callback;
    addToList.removeFromListBlock=callback;
    [self.navigationController pushViewController:addToList animated:YES];



}
-(void)likeLabelTap:(UITapGestureRecognizer*)gesture
{
    [[Amplitude instance] logEvent:@" RC - View People Who Likes"];
    
    if (likepopView) {
        [likepopView removeFromSuperview];
    }
    
    [[LoadingView sharedLoadingView]startLoading];
    [HomeUtil getLikeListFromServiceWithOfferCardId:_offermodel.offerId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
        
        likeListArr=[[NSMutableArray alloc]initWithArray:likeListArray];
        [[LoadingView sharedLoadingView]stopLoading];
        
        likepopView.likecount=likeviewcount;
        likepopView.likelistArray=likeListArr;
    }];
    likepopView=[[LikeView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    likepopView.delegate=self;
    [self.view addSubview:likepopView];
    
    
    
}
-(void)LikeviewLoadMoreData:(NSInteger)offset
{
    [HomeUtil getLikeListFromServiceWithResturantId:_offermodel.restaurantUpdateId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
        likepopView.likecount=likeviewcount;
        [likeListArr addObjectsFromArray:likeListArray];
        [likepopView.likeUserTableView reloadData];
        
    }];
}
-(void)returnLikeCount:(NSInteger)count
{
    if (count!=0) {
        
        
        NSString *likeString=[NSString stringWithFormat:@"%ld %@",(long)count,count>1?L(@"Likes"):L(@"Like")];
        
        [likeLabel setText:likeString];
        likeLabel.frame=CGRectMake(likeLabel.frame.origin.x, likeLabel.frame.origin.y, likeLabel.expectedWidth, likeLabel.frame.size.height);
    }
}

- (void)addLikeCommentShareView:(BOOL)isReadMoreTap
{
    [self updateLikeCommentListCountLabel:YES andIsReadMoreTap:isReadMoreTap];
    
    if(self.likeCommentShareView){
        [self.likeCommentShareView removeFromSuperview];
    }
    
    self.likeCommentShareView = [[UILikeCommentShareView alloc]initInSuperView:self.scrollView atStartX:0 startY:self.currentPointY+10 commentCount:[_offermodel.commentCount intValue] commentArray:_offermodel.commentCardList liked:_offermodel.isLike withFooter:NO needCommentListView:YES];
    self.likeCommentShareView.likeCommentShareDelegate=self;
    self.currentPointY = self.likeCommentShareView.endPointY+10;
    self.scrollView.contentSize = CGSizeMake(0, self.currentPointY+10);
    self.currentPointY=self.scrollView.contentSize.height;
    
    self.commentInputView = [[UICommentInputView alloc]initView:NO];
    self.commentInputView.frame=CGRectMake(0, DeviceHeight-44-self.commentInputView.frame.size.height, self.commentInputView.frame.size.width, self.commentInputView.frame.size.height);
    
    self.commentInputView.commentInputViewDelegate=self;
    [self.view addSubview:self.commentInputView];
}
- (void)addLikeCommentShareView:(BOOL)isReadMoreTap andIsTemp:(BOOL)isTemp
{
    [self updateLikeCommentListCountLabel:YES andIsReadMoreTap:isReadMoreTap];
    
    if(self.likeCommentShareView){
        [self.likeCommentShareView removeFromSuperview];
    }
    
    self.likeCommentShareView = [[UILikeCommentShareView alloc]initInSuperView:self.scrollView atStartX:0 startY:self.currentPointY+10 commentCount:[_offermodel.commentCount intValue] commentArray:commentsList liked:_offermodel.isLike withFooter:NO needCommentListView:!isTemp];
    self.likeCommentShareView.likeCommentShareDelegate=self;
    self.currentPointY = self.likeCommentShareView.endPointY+10;
    self.scrollView.contentSize = CGSizeMake(0, self.currentPointY+10);
    self.currentPointY=self.scrollView.contentSize.height;
    
    self.commentInputView = [[UICommentInputView alloc]initView:NO];
    self.commentInputView.frame=CGRectMake(0, DeviceHeight-44-self.commentInputView.frame.size.height, self.commentInputView.frame.size.width, self.commentInputView.frame.size.height);
    
    self.commentInputView.commentInputViewDelegate=self;
    [self.view addSubview:self.commentInputView];
}

-(void)updateLikeCommentListCountLabel:(BOOL)initial andIsReadMoreTap:(BOOL)isReadMoreTap{
    
    if(likeLabel.hidden==NO){
        self.currentPointY=likeLabel.startPointY-10;
    }else if(commentLabel.hidden==NO){
        self.currentPointY=commentLabel.startPointY-10;
    }else if(!initial){
        self.currentPointY=self.likeCommentShareView.startPointY-10;
    }
    
    CGFloat startPointY=self.currentPointY;
    
    float start=LEFTLEFTSET;
    if([_offermodel.likeCount intValue]>0){
        NSString *likeString=[NSString stringWithFormat:@"%@ %@",_offermodel.likeCount,[_offermodel.likeCount intValue]>1?L(@"Likes"):L(@"Like")];
        CGSize likeSize=[likeString sizeWithFont:likeLabel.font constrainedToSize:CGSizeMake(DeviceWidth,20) lineBreakMode:NSLineBreakByWordWrapping];
        likeLabel.frame=CGRectMake(start, startPointY+10, likeSize.width,20);
        likeLabel.text=likeString;
        start+=likeSize.width+15;
        self.currentPointY=likeLabel.endPointY;
        [likeLabel setHidden:NO];
    }else if(likeLabel.hidden==NO){
        [likeLabel setHidden:YES];
        self.currentPointY=self.currentPointY-30;
    }
    if([_offermodel.commentCount intValue]>0){
        NSString *commentString=[NSString stringWithFormat:@"%@ %@",_offermodel.commentCount,[_offermodel.commentCount intValue]>1?L(@"Comments"):L(@"Comment")];
        CGSize commentSize=[commentString sizeWithFont:commentLabel.font constrainedToSize:CGSizeMake(DeviceWidth,20) lineBreakMode:NSLineBreakByWordWrapping];
        commentLabel.frame=CGRectMake(start, startPointY+10, commentSize.width,20);
        commentLabel.text=commentString;
        start+=commentSize.width+15;
        self.currentPointY=commentLabel.endPointY;
        [commentLabel setHidden:NO];
    }else if(commentLabel.hidden==NO){
        [commentLabel setHidden:YES];
        self.currentPointY=self.currentPointY-30;

    }
    if(!initial){
        [self.view endEditing:YES];
        if(self.currentPointY-startPointY>0.1){
            self.likeCommentShareView.frame=CGRectMake(0, self.currentPointY+10, self.likeCommentShareView.frame.size.width, self.likeCommentShareView.frame.size.height);
            if (!isReadMoreTap) {
                self.currentPointY = self.likeCommentShareView.endPointY+10;
                
            }
            
        }
        if (!isReadMoreTap) {
            self.scrollView.contentSize = CGSizeMake(0, self.currentPointY+10);
            self.currentPointY=self.scrollView.contentSize.height;
        }
        
    }
}
- (void)getDataFromService
{
    [[LoadingView sharedLoadingView] startLoading];
    
    [RestaurantHandler previousComment:@"" offerComentId:_offermodel.offerId andBlock:^(NSArray *commentList, NSNumber *likeCount, NSNumber *commentCount) {
        
        commentsList = [NSMutableArray arrayWithArray:commentList];
//        if(commentsList.count>3){
//            _offermodel.commentCardList = [NSMutableArray arrayWithArray:[commentsList subarrayWithRange:NSMakeRange(0, 3)]];
//        }else{
//            _offermodel.commentCardList = commentsList;
//        }
        _offermodel.commentCardList = commentsList;
        if (commentCount) {
            _offermodel.commentCount =commentCount;
        }
        if (likeCount) {
            _offermodel.likeCount = likeCount;
        }
        [self addLikeCommentShareView:NO];
        if(self.cdpDelegate){
            [self.cdpDelegate CDP:self reloadTableCell:self.fromCellIndexPath];
        }
        [[LoadingView sharedLoadingView] stopLoading];
        
    }];
    [self addLikeCommentShareView:NO andIsTemp:YES];

//    [RestaurantHandler previousComment:@"" targetId:_offermodel.offerId andBlock:^(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount) {
//        commentsList = [NSMutableArray arrayWithArray:commentList];
//        if(commentsList.count>3){
//            _offermodel.commentCardList = [NSMutableArray arrayWithArray:[commentsList subarrayWithRange:NSMakeRange(0, 3)]];
//        }else{
//            _offermodel.commentCardList = commentsList;
//        }
//        if (commentCount) {
//            _offermodel.commentCount =commentCount;
//        }
//        if (likeCount) {
//            _offermodel.likeCount = likeCount;
//        }
//        [self addLikeCommentShareView:NO];
//        if(self.cdpDelegate){
//            [self.cdpDelegate CDP:self reloadTableCell:nil];
//        }
//        [[LoadingView sharedLoadingView] stopLoading];
//    }];
//    [self addLikeCommentShareView:NO andIsTemp:YES];
    
}
- (void)likeCommentShareView:(UIView*)likeCommentShareView_ viewPreviousCommentsTapped:(UIButton*)button lastComment:(id)comment{
    [[LoadingView sharedLoadingView] startLoading];
    NSString *lastCommentIdStr=@"0";
    if([comment isKindOfClass:[IMGRestaurantEventComment class]]){
        IMGRestaurantEventComment *dishComment=(IMGRestaurantEventComment*)comment;
        if(dishComment){
            lastCommentIdStr=[NSString stringWithFormat:@"%@",dishComment.commentId];
        }
        
        [RestaurantHandler previousComment:lastCommentIdStr offerComentId:_offermodel.offerId andBlock:^(NSArray *commentList, NSNumber *likeCount, NSNumber *commentCount) {
            if(commentList&&commentList.count>0){
                [self.view endEditing:YES];
                
                if(commentList){
                    CGFloat likeCommentShareViewHeight=self.likeCommentShareView.frame.size.height;
                    [self.likeCommentShareView addPreviousComments:commentList];
                    
                    CGFloat height=self.likeCommentShareView.frame.size.height-likeCommentShareViewHeight;
                    self.currentPointY=self.scrollView.contentSize.height+height;
                    self.scrollView.contentSize=CGSizeMake(self.scrollView.contentSize.width, self.currentPointY);
                    
                    CGFloat height1 = [UILikeCommentShareView calculatedTableViewHeight:commentsList withFooter:NO];
                    
                    CGPoint point= CGPointMake(0, self.scrollView.contentSize.height-height1-DeviceHeight+65);
                    if (point.y>0) {
                        self.scrollView.contentOffset =point;
                        
                    }
                    
                    [commentsList addObjectsFromArray:commentList];
                }
            }
  
        }];
        
        
    }else{
        [[LoadingView sharedLoadingView] stopLoading];
    }
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView_ likeButtonTapped:(UIButton *)button{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
    //    [[LoadingView sharedLoadingView] startLoading];
    
    
    [RestaurantHandler likeRestaurantOfferCard:_offermodel.isLike OfferCardId:_offermodel.offerId andBlock:^{
        
    } failure:^(NSString *exceptionMsg) {
        
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }

        
        
    }];
    
//    [RestaurantHandler likeRestaurantOfferCard:_offermodel.isLike restaurantEventId:_offermodel.restaurantUpdateId andBlock:^{
//        
////        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
////        [eventProperties setValue:restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
////        [eventProperties setValue:restaurantEvent.restaurantId forKey:@"Restaurant_ID"];
////        [eventProperties setValue:@"Restaurant event card detail page" forKey:@"Location"];
////        
////        if (!restaurantEvent.isLike) {
////            [[Amplitude instance] logEvent:@"UC - UnLike Restaurant Event Succeed" withEventProperties:eventProperties];
////        }else{
////            [[Amplitude instance] logEvent:@"UC - Like Restaurant Event Succeed" withEventProperties:eventProperties];
////        }
//    }failure:^(NSString *exceptionMsg){
//        if([exceptionMsg isLogout]){
//            [[LoadingView sharedLoadingView] stopLoading];
//            [[IMGUser currentUser]logOut];
//            [[AppDelegate ShareApp] goToLoginController];
//        }
//    }];
    [self.view endEditing:YES];
    _offermodel.isLike=!_offermodel.isLike;
    if(_offermodel.isLike){
        _offermodel.likeCount=[NSNumber numberWithInt:[_offermodel.likeCount intValue]+1];
//        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
//        [eventProperties setValue:restaurantEvent.eventId forKey:@"EventID"];
//        [eventProperties setValue:restaurantEvent.title forKey:@"RestaurantTitle"];
//        [eventProperties setValue:@"Restaurant event card detail page" forKey:@"Location"];
//        [[Amplitude instance] logEvent:@"UC - Like Restaurant Event" withEventProperties:eventProperties];
    }else{
        _offermodel.likeCount=[NSNumber numberWithInt:[_offermodel.likeCount intValue]-1];
//        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
//        [eventProperties setValue:restaurantEvent.eventId forKey:@"EventID"];
//        [eventProperties setValue:restaurantEvent.title forKey:@"RestaurantTitle"];
//        [eventProperties setValue:@"Restaurant event card detail page" forKey:@"Location"];
//        
//        [[Amplitude instance] logEvent:@"UC - UnLike Restaurant Event" withEventProperties:eventProperties];
    }
    if (_offermodel.likeCount==0) {
        likeLabel.hidden=YES;
    }else if (_offermodel.commentCount==0){
    
        commentLabel.hidden=YES;
    
    
    }
    [self.likeCommentShareView updateLikedStatus];
    [self updateLikeCommentListCountLabel:NO andIsReadMoreTap:YES];
    if(self.cdpDelegate){
        [self.cdpDelegate CDP:self reloadTableCell:nil];
    }
    
}
-(void)likeCommentShareView:(UIView *)likeCommentShareView shareButtonTapped:(UIButton *)button{

    IMGRestaurant *_restaurant = [[IMGRestaurant alloc] init];
    _restaurant.restaurantId = _offermodel.restaurantId;
    _restaurant.title = _offermodel.restaurantTitle;
    _restaurant.seoKeyword = _offermodel.restaurantSeo;
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/offer/%@",QRAVED_WEB_SERVER_OLD,_offermodel.offerId]];
    NSString *shareContentString = [NSString stringWithFormat:@"%@ on Qraved. %@",_restaurant.title,url];
    RestaurantEventActivityItemProvider *provider=[[RestaurantEventActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
    provider.url=url;
    provider.title=shareContentString;
    provider.restaurantTitle=_restaurant.title;
    
    TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
    twitterActivityItemProvider.title = [NSString stringWithFormat:@"Check out this promo at %@ on Qraved",_restaurant.title];
    
    UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
    
    
    
    activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
    [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK %@'s promo on Qraved!"),_restaurant.title] forKey:@"subject"];
    activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
        if (completed) {
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:_restaurant.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:_restaurant.title forKey:@"RestaurantTitle"];
            [eventProperties setValue:@"Restaurant event card detail page" forKey:@"Location"];
            [eventProperties setValue:provider.activityType forKey:@"Channel"];
            [eventProperties setValue:_offermodel.restaurantUpdateId forKey:@"RestaurantEvent_ID"];
            [[Amplitude instance] logEvent:@"SH - Share Restaurant Event" withEventProperties:eventProperties];
        }else{
        }
    };
    if ([activityCtl respondsToSelector:@selector(popoverPresentationController)]) {
        
        activityCtl.popoverPresentationController.sourceView = btn;
        
    }
    
    [self presentViewController:activityCtl animated:YES completion:^{
        
    }];
}
- (void)commentInputView:(UIView*)view postButtonTapped:(UIButton*)postButton commentInputText:(NSString *)text{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
//    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
//    [eventProperties setValue:restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
//    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
//    [eventProperties setValue:@"Restaurant event card detail page" forKey:@"Location"];
    
    [RestaurantHandler postComment:text restaurantOfferCardId:_offermodel.offerId andBlock:^(IMGDishComment *eventComment) {
        [self.commentInputView clearInputTextView];
        [self.view endEditing:YES];
        
//        [eventProperties setValue:eventComment.commentId forKey:@"Comment_ID"];
//        [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Succeed" withEventProperties:eventProperties];
//        [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Restaurant Event Succeed" withValues:eventProperties];
        
        if(eventComment){
            
            [self.likeCommentShareView addNewComment:eventComment];
            
            _offermodel.commentCount=[NSNumber numberWithInteger:[_offermodel.commentCount integerValue]+1];
            
            
            if(commentsList){
                if([commentsList isKindOfClass:[NSArray class]]){
                    commentsList=[NSMutableArray arrayWithArray:commentsList];
                }
                [commentsList insertObject:eventComment atIndex:0];
            }else{
                commentsList = [NSMutableArray arrayWithObject:eventComment];
            }
            if(commentsList.count>3){
                _offermodel.commentCardList = [NSMutableArray arrayWithArray:[commentsList subarrayWithRange:NSMakeRange(0, 3)]];
            }else{
                _offermodel.commentCardList = commentsList;
            }
            
            [self updateLikeCommentListCountLabel:NO andIsReadMoreTap:NO];
            CGPoint point=CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
            if (point.y>0) {
                [self.scrollView setContentOffset:point];
                
            }
            
            
            if(self.cdpDelegate){
                [self.cdpDelegate CDP:self reloadTableCell:nil];
            }
        }
    } failure:^(NSString *errorReason){
        if([errorReason isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
//        [eventProperties setValue:errorReason forKey:@"Reason"];
//        [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Failed" withEventProperties:eventProperties];
    }];
//    [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Initiate" withEventProperties:eventProperties];
}
//- (void)likeCommentShareView:(UIView*)likeCommentShareView_ viewPreviousCommentsTapped:(UIButton*)button lastComment:(id)comment{
//    [[LoadingView sharedLoadingView] startLoading];
//    NSString *lastCommentIdStr=@"0";
//    if([comment isKindOfClass:[IMGRestaurantEventComment class]]){
//        IMGRestaurantEventComment *restaurantEventComment=(IMGRestaurantEventComment*)comment;
//        if(restaurantEventComment){
//            lastCommentIdStr=[NSString stringWithFormat:@"%@",restaurantEventComment.commentId];
//        }
//        
//        [RestaurantHandler previousComment:lastCommentIdStr targetId:restaurantEvent.eventId andBlock:^(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount) {
//            if(commentList&&commentList.count>0){
//                [self.view endEditing:YES];
//                
//                if(commentList){
//                    CGFloat likeCommentShareViewHeight=self.likeCommentShareView.frame.size.height;
//                    [self.likeCommentShareView addPreviousComments:commentList];
//                    
//                    CGFloat height=self.likeCommentShareView.frame.size.height-likeCommentShareViewHeight;
//                    self.currentPointY=self.scrollView.contentSize.height+height;
//                    self.scrollView.contentSize=CGSizeMake(self.scrollView.contentSize.width, self.currentPointY);
//                    
//                    CGFloat height1 = [UILikeCommentShareView calculatedTableViewHeight:commentsList withFooter:NO];
//                    
//                    CGPoint point= CGPointMake(0, self.scrollView.contentSize.height-height1-DeviceHeight+65);
//                    if (point.y>0) {
//                        self.scrollView.contentOffset =point;
//                        
//                    }
//                    
//                    [commentsList addObjectsFromArray:commentList];
//                }
//            }
//        }];
//        
//    }else{
//        [[LoadingView sharedLoadingView] stopLoading];
//    }
//}
//
-(void)likeCommentShareView:(UIView *)likeCommentShareView commentCell:(UITableViewCell *)cell readMoreTapped:(id)comment offset:(CGFloat)offset{
    [self.view endEditing:YES];
    self.currentPointY+=offset;
    self.scrollView.contentSize=CGSizeMake(self.scrollView.contentSize.width, self.currentPointY);
    [self addLikeCommentShareView:YES];
}
-(void)keyboardChange:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    if (notification.name == UIKeyboardWillShowNotification) {
        CGFloat keyboardOffset = keyboardEndFrame.size.height;
        if(self.previousKeyboardHeight>0){
            keyboardOffset=keyboardEndFrame.size.height-self.previousKeyboardHeight;
        }
        
        if(self.currentPointY<DeviceHeight){
            CGFloat diff = DeviceHeight-self.currentPointY-20;
            
            if(diff>keyboardEndFrame.size.height+self.commentInputView.frame.size.height){
                
            }else{
                CGFloat moveOffset = keyboardEndFrame.size.height+self.commentInputView.frame.size.height-diff;
                self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, self.currentPointY+keyboardEndFrame.size.height);
                [self.scrollView setContentOffset:CGPointMake(0, moveOffset) animated:YES];
            }
        }else{
            self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, self.currentPointY+keyboardEndFrame.size.height);
            [self.scrollView setContentOffset:CGPointMake(0, self.currentOffsetY+keyboardOffset) animated:YES];
        }
        
        
        self.commentInputView.frame=CGRectMake(self.commentInputView.startPointX, self.commentInputView.startPointY-keyboardOffset, self.commentInputView.frame.size.width, self.commentInputView.frame.size.height);
        self.previousKeyboardHeight=keyboardEndFrame.size.height;
    }else{
        self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, self.currentPointY);
        self.commentInputView.frame=CGRectMake(0, DeviceHeight-44-self.commentInputView.frame.size.height, self.commentInputView.frame.size.width, self.commentInputView.frame.size.height);
        self.previousKeyboardHeight=0.0f;
    }
    
    [UIView commitAnimations];
}



 @end
