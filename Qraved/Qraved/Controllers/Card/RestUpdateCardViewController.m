//
//  RestUpdateViewController.m
//  Qraved
//
//  Created by lucky on 16/3/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "RestUpdateCardViewController.h"
#import "UIConstants.h"
#import "DLStarRatingControl.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"
#import "IMGRestaurant.h"
#import "UILabel+Helper.h"
#import "IMGDish.h"
#import "CXAHyperlinkLabel.h"
#import "NSString+CXAHyperlinkParser.h"
#import "CardPhotosView.h"
#import "IMGRestaurantEvent.h"
#import "LoadingView.h"
#import "UICommentInputView.h"
#import "RestaurantHandler.h"
#import "AppDelegate.h"
#import "IMGUser.h"
#import "DetailViewController.h"
#import "RestaurantActivityItemProvider.h"
#import "RestaurantEventActivityItemProvider.h"
#import "CDPDelegate.h"
#import "Date.h"
#import "TwitterActivityItemProvider.h"
#import "ImageListViewController.h"
#import "AlbumViewController.h"
#import "AddToListController.h"
#import "LikeView.h"
#import "HomeUtil.h"

@interface RestUpdateCardViewController ()<CardPhotosViewDelegate,UIScrollViewDelegate,CommentInputViewDelegate,LikeCommentShareDelegate,CDPDelegate,ImageListViewControllerDelegate,LikeListViewDelegate,TYAttributedLabelDelegate>
{
    IMGRestaurant *restaurant;
//    CXAHyperlinkLabel *titleLabel;
    TYAttributedLabel *titleLabel;
    UILabel *reviewCountLabel;
    UIImageView *restaurantImageView;
    UILabel *cuisineAndDistrictLabel;
    DLStarRatingControl *ratingScoreControl;
    UIImageView *reviewCountImageView;
    UILabel *scoreLabel;
    UILabel *timeLabel;
    IMGRestaurantEvent *restaurantEvent;
    NSMutableArray *commentsList;
    
    UILabel *likeLabel;
    UILabel *commentLabel;
    UILabel *listLabel;
    CardPhotosView *cardPhotosView;
    UIView *mainView;
    UIButton *saveBtn;
    NSMutableArray *restaurantEventArrM;
    NSMutableArray* likeListArr;
    LikeView*  likepopView;

}
@end

@implementation RestUpdateCardViewController

-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant andRestaurantEventList:(NSArray *)restaurantEventList
{
    if(self= [super init]){
        restaurant=paramRestaurant;
        restaurantEventArrM = [NSMutableArray arrayWithArray:restaurantEventList];
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"Restaurant Promo Card Detail page";
    self.view.backgroundColor = [UIColor whiteColor];
    self.currentPointY = 0;
    self.previousKeyboardHeight = 0.0f;
    self.currentOffsetY = 0.0f;
    [self setBackBarButtonOffset30];
    [self addScrollView];
    [self addMainView];
    [self setData];
    [self getDataFromService];
}
- (void)getDataFromService
{
    [[LoadingView sharedLoadingView] startLoading];
    [RestaurantHandler previousComment:@"" targetId:restaurantEvent.eventId andBlock:^(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount) {
        commentsList = [NSMutableArray arrayWithArray:commentList];
        if(commentsList.count>3){
            restaurantEvent.commentList = [NSMutableArray arrayWithArray:[commentsList subarrayWithRange:NSMakeRange(0, 3)]];
        }else{
            restaurantEvent.commentList = commentsList;
        }
        if (commentCount) {
            restaurantEvent.commentCount =commentCount;
        }
        if (likeCount) {
            restaurantEvent.likeCount = likeCount;
        }
        [self addLikeCommentShareView:NO];
        if(self.cdpDelegate){
            [self.cdpDelegate CDP:self reloadTableCell:nil];
        }
        [[LoadingView sharedLoadingView] stopLoading];
    }];
    [self addLikeCommentShareView:NO andIsTemp:YES];

}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[LoadingView sharedLoadingView] stopLoading];
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillHideNotification];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)addScrollView
{
    self.scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:self.scrollView];
    self.scrollView.backgroundColor = [UIColor whiteColor];
    self.scrollView.frame = CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height-UICOMMENTINPUTVIEWHEIGHT-44);
    self.scrollView.delegate=self;
}
- (void)addMainView
{
    mainView = [[UIView alloc] init];
    [self.scrollView addSubview:mainView];
    
    titleLabel = [[TYAttributedLabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, TITLE_LABEL_Y, DeviceWidth-4*LEFTLEFTSET, TITLE_LABEL_HEIGHT)];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.numberOfLines = 0;
    titleLabel.delegate = self;
    titleLabel.characterSpacing = 0.1;
    titleLabel.font = [UIFont systemFontOfSize:TITLE_LABEL_FONT];
    [titleLabel setLineBreakMode:kCTLineBreakByCharWrapping];
    titleLabel.textColor = [UIColor grayColor];

    [mainView addSubview:titleLabel];

    
    
    saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    saveBtn.frame = CGRectMake(DeviceWidth - LEFTLEFTSET - 25, titleLabel.frame.origin.y + 5, 25, 25);
    [saveBtn setImage:[UIImage imageNamed:@"save_list__"] forState:UIControlStateNormal];
    [mainView addSubview:saveBtn];
    [saveBtn addTarget:self action:@selector(saveBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    

    
    reviewCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET+cuisineAndDistrictLabel.frame.size.width+11, TITLE_LABEL_Y+TITLE_LABEL_HEIGHT+GAP_BETWEEN_TITLE_AND_CUISINE+CUISINE_DISTRICT_LABEL_HEIGHT+GAP_BETWEEN_CUISINE_AND_REVIEW, 18,CELL_RATING_STAR_WIDTH)];
    [reviewCountLabel setTextAlignment:NSTextAlignmentRight];
    [reviewCountLabel setFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:10]];
    [reviewCountLabel setTextColor:[UIColor colorFFC000]];
    [reviewCountLabel setBackgroundColor:[UIColor clearColor]];
    [mainView addSubview:reviewCountLabel];
    
    scoreLabel = [[UILabel alloc] init];
    scoreLabel.font = [UIFont systemFontOfSize:12.5];
    scoreLabel.textColor = [UIColor colorFFC000];
    scoreLabel.frame = CGRectMake(LEFTLEFTSET, titleLabel.endPointY, scoreLabel.expectedWidth, scoreLabel.font.lineHeight);
    [mainView addSubview:scoreLabel];
    
    
    ratingScoreControl=[[DLStarRatingControl alloc] initWithFrame:CGRectMake(LEFTLEFTSET, TITLE_LABEL_Y+TITLE_LABEL_HEIGHT+GAP_BETWEEN_TITLE_AND_CUISINE+CUISINE_DISTRICT_LABEL_HEIGHT+GAP_BETWEEN_CUISINE_AND_REVIEW, 74, 10) andStars:5 andStarWidth:CELL_RATING_STAR_WIDTH isFractional:YES spaceWidth:5];
    ratingScoreControl.userInteractionEnabled=NO;
    [mainView addSubview:ratingScoreControl];

    timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, scoreLabel.endPointY + 13, 100, 15)];
    timeLabel.textColor = [UIColor grayColor];
    timeLabel.alpha = 0.7f;
    timeLabel.font = [UIFont systemFontOfSize:12];
    [mainView addSubview:timeLabel];
    
    restaurantImageView = [[UIImageView alloc] initWithFrame:CGRectMake(LEFTLEFTSET, timeLabel.endPointY+13, DeviceWidth-LEFTLEFTSET*2, 160)];
    [restaurantImageView setAlpha:0.0];
    [mainView addSubview:restaurantImageView];

    cardPhotosView = [[CardPhotosView alloc] init];
    cardPhotosView.cardPhotosViewDelegate = self;
//    cardPhotosView.homeTableViewCellDelegate = self.homeTableViewCellDelegate;
    [mainView addSubview:cardPhotosView];
    
    
    likeLabel=[[UILabel alloc] init];
    likeLabel.userInteractionEnabled=YES;
    UITapGestureRecognizer* likeTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(likeLabelTap:)];
    likeTap.numberOfTapsRequired=1;
    [likeLabel addGestureRecognizer:likeTap];
    likeLabel.font=[UIFont systemFontOfSize:12];
    likeLabel.textColor = [UIColor colorC2060A];
    likeLabel.hidden=YES;
    [mainView addSubview:likeLabel];
    

    commentLabel=[[UILabel alloc] init];
    commentLabel.font=[UIFont systemFontOfSize:12];
    commentLabel.textColor = [UIColor colorC2060A];
    commentLabel.hidden=YES;
    [mainView addSubview:commentLabel];

    listLabel=[[UILabel alloc] init];
    listLabel.font=[UIFont systemFontOfSize:12];
    listLabel.textColor = [UIColor colorC2060A];
    listLabel.hidden=YES;
    [mainView addSubview:listLabel];
}
-(void)likeLabelTap:(UITapGestureRecognizer*)gesture
{
    [[Amplitude instance] logEvent:@" RC - View People Who Likes"];

    if (likepopView) {
        [likepopView removeFromSuperview];
    }

    [[LoadingView sharedLoadingView]startLoading];
    [HomeUtil getLikeListFromServiceWithResturantId:restaurantEvent.eventId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
        
        
        likeListArr=[[NSMutableArray alloc]initWithArray:likeListArray];
        [[LoadingView sharedLoadingView]stopLoading];
       
        likepopView.likecount=likeviewcount;
             likepopView.likelistArray=likeListArr;
          }];
    likepopView=[[LikeView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    likepopView.delegate=self;
    [self.view addSubview:likepopView];


    
}
-(void)LikeviewLoadMoreData:(NSInteger)offset
{
        [HomeUtil getLikeListFromServiceWithResturantId:restaurantEvent.eventId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
            likepopView.likecount=likeviewcount;
            [likeListArr addObjectsFromArray:likeListArray];
            [likepopView.likeUserTableView reloadData];
    
    }];
}
-(void)returnLikeCount:(NSInteger)count
{
    if (count!=0) {
        
   
    NSString *likeString=[NSString stringWithFormat:@"%ld %@",(long)count,count>1?L(@"Likes"):L(@"Like")];

    [likeLabel setText:likeString];
     likeLabel.frame=CGRectMake(likeLabel.frame.origin.x, likeLabel.frame.origin.y, likeLabel.expectedWidth, likeLabel.frame.size.height);
    }
}
-(void)setData
{
    
    NSString *title = [NSString stringWithFormat:@"Promo from %@",restaurant.title];
    titleLabel.text = title;
    [titleLabel setFrameWithOrign:CGPointMake(LEFTLEFTSET, TITLE_LABEL_Y) Width:DeviceWidth-4*LEFTLEFTSET];
    
    TYLinkTextStorage *linkTextStorageRestaurant = [[TYLinkTextStorage alloc]init];
    linkTextStorageRestaurant.range = [titleLabel.text rangeOfString:restaurant.title];
    linkTextStorageRestaurant.textColor = [UIColor blackColor];
    linkTextStorageRestaurant.linkData = @"restaurant";
    linkTextStorageRestaurant.underLineStyle = kCTUnderlineStyleNone;
    [titleLabel addTextStorage:linkTextStorageRestaurant];
    
    
    
    if ([restaurant.saved boolValue])
    {
        [saveBtn setImage:[UIImage imageNamed:@"save_list__"] forState:UIControlStateNormal];
    }
    else
        [saveBtn setImage:[UIImage imageNamed:@"unsave_list"] forState:UIControlStateNormal];
    
    
    float score = [restaurant.ratingScore floatValue];
    [scoreLabel setText:[NSString stringWithFormat:@"%.1f",score/2]];
    scoreLabel.frame = CGRectMake(LEFTLEFTSET, titleLabel.endPointY, scoreLabel.expectedWidth, scoreLabel.font.lineHeight);
    
    
    ratingScoreControl.frame = CGRectMake(scoreLabel.endPointX + 8, titleLabel.endPointY +3, 74, 10);
    
    [ratingScoreControl updateRating:[NSNumber numberWithFloat:[restaurant.ratingScore floatValue]]];
    
    [timeLabel setText:[Date getTimeInteval_v4:[restaurant.timeline longLongValue]/1000]];
    timeLabel.frame = CGRectMake(LEFTLEFTSET, scoreLabel.endPointY + 13, 240, 15);
    
    
    if (restaurantEventArrM.count > 1)
    {
        cardPhotosView.frame = CGRectMake(LEFTLEFTSET, timeLabel.endPointY+13, DeviceWidth-LEFTLEFTSET*2, [CardPhotosView cardPhotoHeightWithPhotoCout:restaurantEventArrM.count]);
        
        cardPhotosView.cardPhotosViewDelegate = self;
//        cardPhotosView.homeTableViewCellDelegate = self.homeTableViewCellDelegate;
        
        [cardPhotosView setPhotos:restaurantEventArrM];
        mainView.frame = CGRectMake(0, 0, DeviceWidth, cardPhotosView.endPointY+30);
    }
    else if (restaurantEventArrM.count == 1)
    {
        restaurantEvent = [restaurantEventArrM firstObject];
        
        restaurantEvent = [restaurantEventArrM firstObject];
        if ([restaurantEvent.imageHeight floatValue])
        {
            cardPhotosView.frame = CGRectMake(LEFTLEFTSET, timeLabel.endPointY+13, DeviceWidth-LEFTLEFTSET*2, [restaurantEvent.imageHeight floatValue]);
            cardPhotosView.cardPhotoHeight = [restaurantEvent.imageHeight floatValue];
        }
        else
        {
            cardPhotosView.frame = CGRectMake(LEFTLEFTSET, timeLabel.endPointY+13, DeviceWidth-LEFTLEFTSET*2, 160);
        }
        cardPhotosView.cardPhotosViewDelegate = self;
//        cardPhotosView.homeTableViewCellDelegate = self.homeTableViewCellDelegate;
        [cardPhotosView setPhotos:restaurantEventArrM];
        
        self.currentPointY = cardPhotosView.endPointY;
        
        mainView.frame = CGRectMake(0, 0, DeviceWidth, cardPhotosView.endPointY+30);

    }
    [self addLikeCommentShareView:YES];
}

- (void)addLikeCommentShareView:(BOOL)isReadMoreTap
{
    [self updateLikeCommentListCountLabel:YES andIsReadMoreTap:isReadMoreTap];
    
    if(self.likeCommentShareView){
        [self.likeCommentShareView removeFromSuperview];
    }
  
    self.likeCommentShareView = [[UILikeCommentShareView alloc]initInSuperView:self.scrollView atStartX:0 startY:self.currentPointY+10 commentCount:[restaurantEvent.commentCount intValue] commentArray:commentsList liked:restaurantEvent.isLike withFooter:NO needCommentListView:YES];
    self.likeCommentShareView.likeCommentShareDelegate=self;
    self.currentPointY = self.likeCommentShareView.endPointY+10;
    self.scrollView.contentSize = CGSizeMake(0, self.currentPointY+10);
    self.currentPointY=self.scrollView.contentSize.height;
    
    self.commentInputView = [[UICommentInputView alloc]initView:NO];
    self.commentInputView.frame=CGRectMake(0, DeviceHeight-44-self.commentInputView.frame.size.height, self.commentInputView.frame.size.width, self.commentInputView.frame.size.height);
    
    self.commentInputView.commentInputViewDelegate=self;
    [self.view addSubview:self.commentInputView];
}
- (void)addLikeCommentShareView:(BOOL)isReadMoreTap andIsTemp:(BOOL)isTemp
{
    [self updateLikeCommentListCountLabel:YES andIsReadMoreTap:isReadMoreTap];
    
    if(self.likeCommentShareView){
        [self.likeCommentShareView removeFromSuperview];
    }
    
    self.likeCommentShareView = [[UILikeCommentShareView alloc]initInSuperView:self.scrollView atStartX:0 startY:self.currentPointY+10 commentCount:[restaurantEvent.commentCount intValue] commentArray:commentsList liked:restaurantEvent.isLike withFooter:NO needCommentListView:!isTemp];
    self.likeCommentShareView.likeCommentShareDelegate=self;
    self.currentPointY = self.likeCommentShareView.endPointY+10;
    self.scrollView.contentSize = CGSizeMake(0, self.currentPointY+10);
    self.currentPointY=self.scrollView.contentSize.height;
    
    self.commentInputView = [[UICommentInputView alloc]initView:NO];
    self.commentInputView.frame=CGRectMake(0, DeviceHeight-44-self.commentInputView.frame.size.height, self.commentInputView.frame.size.width, self.commentInputView.frame.size.height);
    
    self.commentInputView.commentInputViewDelegate=self;
    [self.view addSubview:self.commentInputView];
}

-(void)updateLikeCommentListCountLabel:(BOOL)initial andIsReadMoreTap:(BOOL)isReadMoreTap{
    
    if(likeLabel.hidden==NO){
        self.currentPointY=likeLabel.startPointY-10;
    }else if(commentLabel.hidden==NO){
        self.currentPointY=commentLabel.startPointY-10;
    }else if(listLabel.hidden==NO){
        self.currentPointY=listLabel.startPointY-10;
    }else if(!initial){
        self.currentPointY=self.likeCommentShareView.startPointY-10;
    }
    
    CGFloat startPointY=self.currentPointY;
    
    float start=LEFTLEFTSET;
    if([restaurantEvent.likeCount intValue]>0){
        NSString *likeString=[NSString stringWithFormat:@"%@ %@",restaurantEvent.likeCount,[restaurantEvent.likeCount intValue]>1?L(@"Likes"):L(@"Like")];
        CGSize likeSize=[likeString sizeWithFont:likeLabel.font constrainedToSize:CGSizeMake(DeviceWidth,20) lineBreakMode:NSLineBreakByWordWrapping];
        likeLabel.frame=CGRectMake(start, startPointY+10, likeSize.width,20);
        likeLabel.text=likeString;
        start+=likeSize.width+15;
        self.currentPointY=likeLabel.endPointY;
        [likeLabel setHidden:NO];
    }else if(likeLabel.hidden==NO){
        [likeLabel setHidden:YES];
    }
    if([restaurantEvent.commentCount intValue]>0){
        NSString *commentString=[NSString stringWithFormat:@"%@ %@",restaurantEvent.commentCount,[restaurantEvent.commentCount intValue]>1?L(@"Comments"):L(@"Comment")];
        CGSize commentSize=[commentString sizeWithFont:commentLabel.font constrainedToSize:CGSizeMake(DeviceWidth,20) lineBreakMode:NSLineBreakByWordWrapping];
        commentLabel.frame=CGRectMake(start, startPointY+10, commentSize.width,20);
        commentLabel.text=commentString;
        start+=commentSize.width+15;
        self.currentPointY=commentLabel.endPointY;
        [commentLabel setHidden:NO];
    }else if(commentLabel.hidden==NO){
        [commentLabel setHidden:YES];
    }

    if(!initial){
        [self.view endEditing:YES];
        if(self.currentPointY-startPointY>0.1){
            self.likeCommentShareView.frame=CGRectMake(0, self.currentPointY+10, self.likeCommentShareView.frame.size.width, self.likeCommentShareView.frame.size.height);
            if (!isReadMoreTap) {
                self.currentPointY = self.likeCommentShareView.endPointY+10;

            }
            
        }
        if (!isReadMoreTap) {
            self.scrollView.contentSize = CGSizeMake(0, self.currentPointY+10);
            self.currentPointY=self.scrollView.contentSize.height;
        }

    }
}


#pragma mark - privates
- (NSAttributedString *)attributedString:(NSArray *__autoreleasing *)outURLs
                               URLRanges:(NSArray *__autoreleasing *)outURLRanges andText:(NSString *)text
{
    NSString *HTMLText = text;
    NSArray *URLs;
    NSArray *URLRanges;
    UIColor *color = [UIColor grayColor];
    UIFont *font = [UIFont systemFontOfSize:TITLE_LABEL_FONT];
    NSMutableParagraphStyle *mps = [[NSMutableParagraphStyle alloc] init];
    //    mps.lineSpacing = ceilf(font.pointSize * .5);
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor whiteColor];
    shadow.shadowOffset = CGSizeMake(0, 1);
    NSString *str = [NSString stringWithHTMLText:HTMLText baseURL:nil URLs:&URLs URLRanges:&URLRanges];
    NSMutableAttributedString *mas = [[NSMutableAttributedString alloc] initWithString:str attributes:@
                                      {
                                          NSForegroundColorAttributeName : color,
                                          NSFontAttributeName            : font,
                                          NSParagraphStyleAttributeName  : mps,
                                          NSShadowAttributeName          : shadow,
                                      }];
    [URLRanges enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
        [mas addAttributes:@
         {
             NSForegroundColorAttributeName : [UIColor color222222],
             //       NSUnderlineStyleAttributeName  : @(NSUnderlineStyleSingle)
             NSForegroundColorAttributeName : [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:TITLE_LABEL_FONT]
             
         } range:[obj rangeValue]];
    }];
    
    *outURLs = URLs;
    *outURLRanges = URLRanges;
    
    return [mas copy];
}

-(void)cardPhotosViewImageClick:(int)imageIndex{
    ImageListViewController *imageListViewController=[[ImageListViewController alloc] initWithRestaurant:restaurant andDishArr:restaurantEventArrM andIndex:imageIndex andFromCellIndexPath:self.fromCellIndexPath];
    imageListViewController.delegate = self;
    [self.navigationController pushViewController:imageListViewController animated:YES];
    
}
-(void)viewFullImage:(IMGEntity *)dish andRestaurant:(IMGRestaurant*)restaurant andReview:(IMGReview*)review{
    
    IMGRestaurantEvent *dish_ = (IMGRestaurantEvent *)dish;
    NSArray *photoArray = [[NSArray alloc]initWithObjects:dish_, nil];
    AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:photoArray andPhotoTag:0 andRestaurant:nil andFromDetail:NO];
    albumViewController.title = dish_.title;
    albumViewController.showPage=NO;
//    albumViewController.albumReloadCellDelegate = self;
    albumViewController.amplitudeType = self.amplitudeType;
    [self.navigationController pushViewController:albumViewController animated:YES];

    
}

 
-(void)shareButtonClickwithbutton:(UIButton*)btn{
    if (restaurantEventArrM.count == 1){
        IMGRestaurant *_restaurant = [[IMGRestaurant alloc] init];
        _restaurant.restaurantId = restaurantEvent.restaurantId;
        _restaurant.title = restaurantEvent.restaurantTitle;
        _restaurant.seoKeyword = restaurantEvent.restaurantSeo;
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/promo/%@",QRAVED_WEB_SERVER_OLD,restaurantEvent.eventId]];
        NSString *shareContentString = [NSString stringWithFormat:@"%@ on Qraved. %@",_restaurant.title,url];
        RestaurantEventActivityItemProvider *provider=[[RestaurantEventActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
        provider.url=url;
        provider.title=shareContentString;
        provider.restaurantTitle=_restaurant.title;
        
        TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
        twitterActivityItemProvider.title = [NSString stringWithFormat:@"Check out this event at %@ on Qraved",_restaurant.title];
        
        UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
        activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
        [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK %@'s event on Qraved!"),_restaurant.title] forKey:@"subject"];
        activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
            if (completed) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:restaurant.title forKey:@"RestaurantTitle"];
                [eventProperties setValue:@"Restaurant event card detail page" forKey:@"Location"];
                [eventProperties setValue:provider.activityType forKey:@"Channel"];
                [eventProperties setValue:restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
                [[Amplitude instance] logEvent:@"SH - Share Restaurant Event" withEventProperties:eventProperties];
            }else{
            }
        };
        if ([activityCtl respondsToSelector:@selector(popoverPresentationController)]) {
            
            activityCtl.popoverPresentationController.sourceView = btn;
            
        }

        [self presentViewController:activityCtl animated:YES completion:^{
        
        }];
    }
}


- (void)commentInputView:(UIView*)view postButtonTapped:(UIButton*)postButton commentInputText:(NSString *)text{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:@"Restaurant event card detail page" forKey:@"Location"];
    
    [RestaurantHandler postComment:text restaurantEventId:restaurantEvent.eventId andBlock:^(IMGRestaurantEventComment *eventComment) {
        [self.commentInputView clearInputTextView];
        [self.view endEditing:YES];
        
        [eventProperties setValue:eventComment.commentId forKey:@"Comment_ID"];
        [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Succeed" withEventProperties:eventProperties];
        [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Restaurant Event Succeed" withValues:eventProperties];
        
        if(eventComment){

            [self.likeCommentShareView addNewComment:eventComment];
            
            restaurantEvent.commentCount=[NSNumber numberWithInteger:[restaurantEvent.commentCount integerValue]+1];

            
            if(commentsList){
                if([commentsList isKindOfClass:[NSArray class]]){
                    commentsList=[NSMutableArray arrayWithArray:commentsList];
                }
                [commentsList insertObject:eventComment atIndex:0];
            }else{
                commentsList = [NSMutableArray arrayWithObject:eventComment];
            }
            if(commentsList.count>3){
                restaurantEvent.commentList = [NSMutableArray arrayWithArray:[commentsList subarrayWithRange:NSMakeRange(0, 3)]];
            }else{
                restaurantEvent.commentList = commentsList;
            }
            
            [self updateLikeCommentListCountLabel:NO andIsReadMoreTap:NO];
            CGPoint point=CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
            if (point.y>0) {
                [self.scrollView setContentOffset:point];

            }
            
            
            if(self.cdpDelegate){
                [self.cdpDelegate CDP:self reloadTableCell:nil];
            }
        }
    } failure:^(NSString *errorReason){
        if([errorReason isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
        [eventProperties setValue:errorReason forKey:@"Reason"];
        [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Failed" withEventProperties:eventProperties];
    }];
    [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Initiate" withEventProperties:eventProperties];
}

- (void)likeCommentShareView:(UIView*)likeCommentShareView_ viewPreviousCommentsTapped:(UIButton*)button lastComment:(id)comment{
    [[LoadingView sharedLoadingView] startLoading];
    NSString *lastCommentIdStr=@"0";
    if([comment isKindOfClass:[IMGRestaurantEventComment class]]){
        IMGRestaurantEventComment *restaurantEventComment=(IMGRestaurantEventComment*)comment;
        if(restaurantEventComment){
            lastCommentIdStr=[NSString stringWithFormat:@"%@",restaurantEventComment.commentId];
        }
        
        [RestaurantHandler previousComment:lastCommentIdStr targetId:restaurantEvent.eventId andBlock:^(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount) {
            if(commentList&&commentList.count>0){
                [self.view endEditing:YES];
                
                if(commentList){
                    CGFloat likeCommentShareViewHeight=self.likeCommentShareView.frame.size.height;
                    [self.likeCommentShareView addPreviousComments:commentList];
                    
                    CGFloat height=self.likeCommentShareView.frame.size.height-likeCommentShareViewHeight;
                    self.currentPointY=self.scrollView.contentSize.height+height;
                    self.scrollView.contentSize=CGSizeMake(self.scrollView.contentSize.width, self.currentPointY);
                    
                    CGFloat height1 = [UILikeCommentShareView calculatedTableViewHeight:commentsList withFooter:NO];
                    
                    CGPoint point= CGPointMake(0, self.scrollView.contentSize.height-height1-DeviceHeight+65);
                    if (point.y>0) {
                        self.scrollView.contentOffset =point;

                    }
                    
                    [commentsList addObjectsFromArray:commentList];
                }
            }
        }];
        
    }else{
        [[LoadingView sharedLoadingView] stopLoading];
    }
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView_ likeButtonTapped:(UIButton *)button{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
//    [[LoadingView sharedLoadingView] startLoading];
    
    [RestaurantHandler likeRestaurantEvent:restaurantEvent.isLike restaurantEventId:restaurantEvent.eventId andBlock:^{
        
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
        [eventProperties setValue:restaurantEvent.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:@"Restaurant event card detail page" forKey:@"Location"];
        
        if (!restaurantEvent.isLike) {
            [[Amplitude instance] logEvent:@"UC - UnLike Restaurant Event Succeed" withEventProperties:eventProperties];
        }else{
            [[Amplitude instance] logEvent:@"UC - Like Restaurant Event Succeed" withEventProperties:eventProperties];
        }
    }failure:^(NSString *exceptionMsg){
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
    }];
    [self.view endEditing:YES];
    restaurantEvent.isLike=!restaurantEvent.isLike;
    if(restaurantEvent.isLike){
        restaurantEvent.likeCount=[NSNumber numberWithInt:[restaurantEvent.likeCount intValue]+1];
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:restaurantEvent.eventId forKey:@"EventID"];
        [eventProperties setValue:restaurantEvent.title forKey:@"RestaurantTitle"];
        [eventProperties setValue:@"Restaurant event card detail page" forKey:@"Location"];
        [[Amplitude instance] logEvent:@"UC - Like Restaurant Event" withEventProperties:eventProperties];
    }else{
        restaurantEvent.likeCount=[NSNumber numberWithInt:[restaurantEvent.likeCount intValue]-1];
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:restaurantEvent.eventId forKey:@"EventID"];
        [eventProperties setValue:restaurantEvent.title forKey:@"RestaurantTitle"];
        [eventProperties setValue:@"Restaurant event card detail page" forKey:@"Location"];
        
        [[Amplitude instance] logEvent:@"UC - UnLike Restaurant Event" withEventProperties:eventProperties];
    }
    [self.likeCommentShareView updateLikedStatus];
    [self updateLikeCommentListCountLabel:NO andIsReadMoreTap:NO];
    if(self.cdpDelegate){
        [self.cdpDelegate CDP:self reloadTableCell:nil];
    }

}

-(void)likeCommentShareView:(UIView *)likeCommentShareView commentCell:(UITableViewCell *)cell readMoreTapped:(id)comment offset:(CGFloat)offset{
    [self.view endEditing:YES];
    self.currentPointY+=offset;
    self.scrollView.contentSize=CGSizeMake(self.scrollView.contentSize.width, self.currentPointY);
    [self addLikeCommentShareView:YES];
}
- (void)saveBtnClick:(UIButton *)btn{
    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        LoginViewController *loginVC=[[LoginViewController alloc]initWithFromWhere:[NSNumber numberWithInt:1]];
        UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
        [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
        return;
    }
    AddToListController *addToList=[[AddToListController alloc] initWithRestaurant:restaurant];
    addToList.amplitudeType = @"Restaurant update card detail page";
    void(^callback)(BOOL saved)=^(BOOL saved){
        [self.cdpDelegate reloadCell];
        if ([restaurant.saved boolValue]){
            [saveBtn setImage:[UIImage imageNamed:@"save_list__"] forState:UIControlStateNormal];
        }else{
            [saveBtn setImage:[UIImage imageNamed:@"unsave_list"] forState:UIControlStateNormal];
        }
    };
    addToList.addToListBlock=callback;
    addToList.removeFromListBlock=callback;
    [self.navigationController pushViewController:addToList animated:YES];
}
-(void)cardPhotosChangeHeight:(CGFloat)height{
}
- (void)attributedLabel:(TYAttributedLabel *)attributedLabel textStorageClicked:(id<TYTextStorageProtocol>)TextRun atPoint:(CGPoint)point{
    
    
    if ([TextRun isKindOfClass:[TYLinkTextStorage class]]) {
        
        
        DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:restaurant.restaurantId];
        dvc.amplitudeType = self.amplitudeType;
        [self.navigationController pushViewController:dvc animated:YES];
    }
    
}
@end
