//
//  UploadPhotoCardViewController.h
//  Qraved
//
//  Created by lucky on 16/3/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "CardDetailViewController.h"
#import "IMGUser.h"
#import "CardPhotosView.h"
#import "CDPDelegate.h"
#import "HomeUploadPhotoTableViewCell.h"
#import "ImageListViewController.h"
#import "TYAttributedLabel.h"


@interface UploadPhotoCardViewController : CardDetailViewController<HomeUploadPhotoTableViewCellDelegate,ImageListViewControllerDelegate>

@property (nonatomic,assign) BOOL isFromNotification;
@property(nonatomic,retain) NSString *amplitudeType;
@property(nonatomic,weak) id<HomeUploadPhotoTableViewCellDelegate> homeUploadPhotoTableViewCellDelegate;

-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant andDishList:(NSArray *)dishArr andUser:(IMGUser *)user;


@end
