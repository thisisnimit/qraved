//
//  OfferCardViewController.h
//  Qraved
//
//  Created by josn.liu on 2016/12/7.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "CardDetailViewController.h"
#import "IMGOfferCard.h"
@interface OfferCardViewController : CardDetailViewController
-(id)initWithRestaurant:(IMGOfferCard *)paramRestaurant andRestaurantEventList:(NSArray *)restaurantEventList;
@end
