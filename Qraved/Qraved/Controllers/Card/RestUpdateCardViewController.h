//
//  RestUpdateViewController.h
//  Qraved
//
//  Created by lucky on 16/3/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "CardDetailViewController.h"
#import "IMGRestaurant.h"
#import "UILikeCommentShareView.h"
#import "CDPDelegate.h"
#import "TYAttributedLabel.h"


@protocol RestUpdateViewCardControllerDelegate <NSObject>

- (void)saveBtnClick:(BOOL)isSaved andRestaurant:(IMGRestaurant *)restaurant;

@end

@interface RestUpdateCardViewController : CardDetailViewController
@property(nonatomic,retain) NSString *amplitudeType;
@property (nonatomic,assign) id<RestUpdateViewCardControllerDelegate>restUpdateCardDetailDelegate;



-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant andRestaurantEventList:(NSArray *)restaurantEventList;


@end
