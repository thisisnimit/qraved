//
//  ReviewCardViewController.h
//  Qraved
//
//  Created by lucky on 16/3/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "CardDetailViewController.h"
#import "IMGRestaurant.h"
#import "IMGUser.h"
#import "IMGReview.h"
#import "HomeTableViewCell.h"
#import "IMGReviewComment.h"
#import "UILikeCommentShareView.h"
#import "CardPhotosView.h"
#import "CDPDelegate.h"

#import "TYAttributedLabel.h"
@interface ReviewCardViewController : CardDetailViewController<CardPhotosViewDelegate,TYAttributedLabelDelegate>

@property (nonatomic,assign) BOOL isFromNotification;
@property(nonatomic,retain) NSString *amplitudeType;
-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant andReview:(IMGReview *)review andDishList:(NSArray *)dishList;


@end
