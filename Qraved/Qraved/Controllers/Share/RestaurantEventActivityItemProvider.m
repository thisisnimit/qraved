//
//  RestaurantEventActivityItemProvider.m
//  Qraved
//
//  Created by Evan on 16/3/17.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "RestaurantEventActivityItemProvider.h"

@implementation RestaurantEventActivityItemProvider

- (id)item{
    if([self.activityType isEqualToString:UIActivityTypePostToFacebook]){
        //    	if(self.url){
        //    		return @{@"text":self.title,@"url":self.url};
        //    	}else{
        //    		return @{@"text":self.title,@"image":self.image};
        //    	}
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=FacebookEvent",self.url]];
        return url;
        
    }else if([self.activityType isEqualToString:UIActivityTypePostToTwitter]){
        //        return self.url;
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=TwitterEvent",self.url]];
        return url;
    }else if([self.activityType isEqualToString:UIActivityTypeMail]){
        return [NSString stringWithFormat:@"<html><body> \
                Hi\n  <br /> \
                Check out this event at %@ on Qraved!<br />\
                <a href=\"%@\">%@</a></body></html>",self.restaurantTitle,self.url,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeMessage]){
        return [NSString stringWithFormat:@"Hi\nCheck out this event at %@ on Qraved!\n%@",self.restaurantTitle,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeCopyToPasteboard]){
        return [self returnCopyUrl:self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeAddToReadingList]){
        return self.url;
    }else if([self.activityType isEqualToString:@"jp.naver.line.Share"]){
        return [NSString stringWithFormat:@"Look! It's %@ %@ on Qraved!\n%@",self.restaurantTitle,self.districtName,self.url];
    }
    else if([self.activityType isEqualToString:@"net.whatsapp.WhatsApp.ShareExtension"])
    {
        NSURL *tmpUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=WhatsappEvent",self.url]];
        return @{@"title":[NSString stringWithFormat:@"Check out this event at %@ on Qraved!",self.restaurantTitle],@"url":tmpUrl};
        
    }
    else {
        return self.placeholderItem;
    }
}


@end
