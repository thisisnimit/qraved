//
//  FBActivityItemProvider.h
//  Qraved
//
//  Created by System Administrator on 12/3/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FBActivityItemProvider : UIActivityItemProvider

@property(nonatomic,copy) NSString *text;
@property(nonatomic,strong) UIImage *image;
@property(nonatomic,strong) NSURL *url;

@end
