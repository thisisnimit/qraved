//
//  FBActivityItemProvider.m
//  Qraved
//
//  Created by System Administrator on 12/3/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import "FBActivityItemProvider.h"

@implementation FBActivityItemProvider

- (id)item{
    if ([self.placeholderItem isKindOfClass:[NSString class]]) {
        if ([self.activityType isEqualToString:UIActivityTypeMessage]) {
            return [NSString stringWithFormat:@"%@\n%@",self.text,self.url];
        } else {
            NSDictionary *d = [[NSDictionary alloc] initWithObjects:@[self.text,self.image] forKeys:@[@"text",@"image"]];
            return d;
        }
    }
    return self.placeholderItem;
}

@end
