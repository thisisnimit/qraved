//
//  IMGActivityItemProvider.h
//  Qraved
//
//  Created by harry on 2018/3/16.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMGActivityItemProvider : UIActivityItemProvider
@property(nonatomic,copy) NSString *title;
@property(nonatomic,strong) NSURL *url;

@end
