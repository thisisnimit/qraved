//
//  DiningGuideActivityItemProvider.m
//  Qraved
//
//  Created by System Administrator on 12/4/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import "DiningGuideActivityItemProvider.h"

@implementation DiningGuideActivityItemProvider

- (id)item{
    if([self.activityType isEqualToString:UIActivityTypePostToFacebook]){
    	if(self.url){
    		return @{@"text":self.title,@"url":self.url};
    	}else{
    		return @{@"text":self.title,@"image":self.image};
    	}
    }else if([self.activityType isEqualToString:UIActivityTypePostToTwitter]){
        return self.url;
    }else if([self.activityType isEqualToString:UIActivityTypeMail]){
        //We should try this sometimes...<br /> \
        //            Try these %ld Restaurants <br /> \ (unsigned long)self.restaurantCount
        //<b>Want to go for %@</b><br /> \  ,self.title
        
        return [NSString stringWithFormat:@"<html><body> \
            <a href=\"%@\">%@</a></body></html>",self.url,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeMessage]){
        
        //Want to go for %@ ? \n%@  self.title,
        return [NSString stringWithFormat:@"%@",self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeCopyToPasteboard]){
        return [NSString stringWithFormat:@"Check out: %@",self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeAddToReadingList]){
        return self.url;
    }else if([self.activityType isEqualToString:@"pinterest.ShareExtension"]){
        return self.url;
    }else if([self.activityType isEqualToString:@"jp.naver.line.Share"]){
        return [self returnCopyUrl:self.url];
    }
    else if([self.activityType isEqualToString:@"net.whatsapp.WhatsApp.ShareExtension"])
    {
        NSURL *tmpUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=WhatsappDiningGuide",self.url]];
        //[NSString stringWithFormat:@"Want to go for %@ ?",self.title]
        return @{@"title":@"",@"text":tmpUrl,@"url":tmpUrl};
    }else {
        return self.placeholderItem;
    }
}
@end
