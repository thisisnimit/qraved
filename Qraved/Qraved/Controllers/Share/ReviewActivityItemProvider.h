//
//  ReviewActivityItemProvider.h
//  Qraved
//
//  Created by System Administrator on 3/5/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewActivityItemProvider : UIActivityItemProvider

@property(nonatomic,copy) 	NSString *title;
@property(nonatomic,copy) 	NSString *username;
@property(nonatomic,copy) 	NSString *restaurantTitle;
@property(nonatomic,strong) UIImage *image;
@property(nonatomic,strong) NSURL *url;

@end
