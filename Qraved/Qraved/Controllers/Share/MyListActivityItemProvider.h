//
//  MyListActivityItemProvider.h
//  Qraved
//
//  Created by System Administrator on 12/7/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyListActivityItemProvider : UIActivityItemProvider


@property(nonatomic,copy) 	NSString *title;
@property(nonatomic,copy) 	NSString *myListTitle;
@property(nonatomic,copy) 	NSString *restaurantTitles;
@property(nonatomic,strong) NSURL *url;

@end
