//
//  RestaurantOfferActivityItemProvider.h
//  Qraved
//
//  Created by josn.liu on 2016/12/20.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestaurantOfferActivityItemProvider : UIActivityItemProvider
@property(nonatomic,copy) 	NSString *title;
@property(nonatomic,copy) 	NSString *restaurantTitle;
@property(nonatomic,copy) 	NSString *cuisineAreaPrice;
//@property(nonatomic,copy) 	NSString *description;
@property(nonatomic,copy) 	NSString *districtName;
@property(nonatomic,strong) UIImage *image;
@property(nonatomic,strong) NSURL *url;
@end
