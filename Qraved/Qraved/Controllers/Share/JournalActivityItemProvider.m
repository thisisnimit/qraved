//
//  JournalActivityItemProvider.m
//  Qraved
//
//  Created by System Administrator on 12/7/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import "JournalActivityItemProvider.h"
#import "TrackHandler.h"
#import "IMGUser.h"

@implementation JournalActivityItemProvider

- (id)item{
    if([self.activityType isEqualToString:UIActivityTypePostToFacebook]){
        [self track:1];
		return @{@"text":self.title,@"url":self.url};
    }else if([self.activityType isEqualToString:UIActivityTypePostToTwitter]){
        [self track:2];
        return self.url;
    }else if([self.activityType isEqualToString:UIActivityTypeMail]){
        [self track:4];
        return [NSString stringWithFormat:@"<html><body> \
            Dare to try? <br /> \
            <b>%@</b><br /> \
            <a href=\"%@\">%@</a></body></html>",self.title,self.url,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeMessage]){
        [self track:3];
    	return [NSString stringWithFormat:@"%@.\nOpen %@ to check out.",self.title,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeCopyToPasteboard]){
    	return [self returnCopyUrl:self.url];
    }else if([self.activityType isEqualToString:@"com.path.Path.ShareExtension"]){
        [self track:6];
        return self.url;
    }else if([self.activityType isEqualToString:@"pinterest.ShareExtension"]){
        [self track:8];
    	return self.url;
    }else if([self.activityType isEqualToString:@"net.whatsapp.WhatsApp.ShareExtension"]){
        [self track:5];
        return @{@"text":self.title,@"url":self.url};
    }else if([self.activityType isEqualToString:UIActivityTypeAddToReadingList]){
        return self.url;
    }else {
        return self.placeholderItem;
    }
}

-(void)track:(NSInteger)type{
    IMGUser *user = [IMGUser currentUser];
    [TrackHandler trackWithUserId:user.userId andJournalId:self.journalId type:[NSNumber numberWithInt:(int)type] other:nil];
}
@end
