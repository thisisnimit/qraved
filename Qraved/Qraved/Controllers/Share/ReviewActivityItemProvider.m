//
//  ReviewActivityItemProvider.m
//  Qraved
//
//  Created by System Administrator on 3/5/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "ReviewActivityItemProvider.h"

@implementation ReviewActivityItemProvider


- (id)item{
    if([self.activityType isEqualToString:UIActivityTypePostToFacebook]){
//    	if(self.url){
//    		return @{@"text":self.title,@"url":self.url};
//    	}else{
//    		return @{@"text":self.title,@"image":self.image};
//    	}
        NSURL *tmpUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=FacebookReview",self.url]];
        return tmpUrl;
    }else if([self.activityType isEqualToString:UIActivityTypePostToTwitter]){
        NSURL *tmpUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=TwitterReview",self.url]];
        return tmpUrl;
    }else if([self.activityType isEqualToString:UIActivityTypeMail]){
        return [NSString stringWithFormat:@"<html><body> \
            Hi, <br /> \
            Check out %@ review at %@ on Qraved!<br /> \
            <a href=\"%@\">%@</a></body></html>",self.username,self.restaurantTitle,self.url,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeMessage]){
        return [NSString stringWithFormat:@"Hi,\nCheck out %@ review at %@ on Qraved!\n%@",self.username,self.restaurantTitle,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeCopyToPasteboard]){
        return [self returnCopyUrl:self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeAddToReadingList]){
        return self.url;
    }else if([self.activityType isEqualToString:@"jp.naver.line.Share"]){
        return [NSString stringWithFormat:@"Hi,\nCheck out %@ review at %@ on Qraved!\n%@",self.username,self.restaurantTitle,self.url];
    }
    else if([self.activityType isEqualToString:@"net.whatsapp.WhatsApp.ShareExtension"])
    {
        NSURL *tmpUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=WhatsappRDP",self.url]];
        return @{@"title":[NSString stringWithFormat:@"Hi,\n Check out %@ review at %@ on Qraved!\n\n",self.username,self.restaurantTitle],@"url":tmpUrl};

    }
    else {
        return self.placeholderItem;
    }
}

@end
