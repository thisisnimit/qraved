//
//  RestaurantMutiPhotosActivityItemProvider.m
//  Qraved
//
//  Created by imaginato on 16/4/27.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "RestaurantMutiPhotosActivityItemProvider.h"

@implementation RestaurantMutiPhotosActivityItemProvider

- (id)item{
    if([self.activityType isEqualToString:UIActivityTypePostToFacebook]){
        //    	if(self.url){
        //    		return @{@"text":self.title,@"url":self.url};
        //    	}else{
        //    		return @{@"text":self.title,@"image":self.image};
        //    	}
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=FacebookMultiPhoto",self.url]];
        return url;
        
    }else if([self.activityType isEqualToString:UIActivityTypePostToTwitter]){
        //        return self.url;
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=TwitterMultiPhoto",self.url]];
        return url;
    }else if([self.activityType isEqualToString:UIActivityTypeMail]){
        return [NSString stringWithFormat:@"<html><body> \
                Hi\n  <br /> \
                Check out %@ photos at %@ on Qraved<br /> \
                <a href=\"%@\">%@</a></body></html>",self.cuisineAreaPrice,self.restaurantTitle,self.url,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeMessage]){
        return [NSString stringWithFormat:@"Hi\nCheck out this photos at %@ on Qraved!\n%@",self.restaurantTitle,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeCopyToPasteboard]){
        return [self returnCopyUrl:self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeAddToReadingList]){
        return self.url;
    }else if([self.activityType isEqualToString:@"jp.naver.line.Share"]){
        return [NSString stringWithFormat:@"Look! It's %@ %@ on Qraved!\n%@",self.restaurantTitle,self.districtName,self.url];
    }
    else if([self.activityType isEqualToString:@"net.whatsapp.WhatsApp.ShareExtension"])
    {
        NSURL *tmpUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=WhatsappMultiPhoto",self.url]];
        return @{@"title":[NSString stringWithFormat:@"Check out this photo at %@ on Qraved!\n",self.restaurantTitle],@"url":tmpUrl};
        
    }
    else {
        return self.placeholderItem;
    }
}


@end
