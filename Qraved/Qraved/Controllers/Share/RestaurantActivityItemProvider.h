//
//  RestaurantActivityItemProvider.h
//  Qraved
//
//  Created by System Administrator on 12/7/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestaurantActivityItemProvider : UIActivityItemProvider

@property(nonatomic,copy) 	NSString *title;
@property(nonatomic,copy) 	NSString *restaurantTitle;
@property(nonatomic,copy) 	NSString *cuisineAreaPrice;
//@property(nonatomic,copy) 	NSString *description;
@property(nonatomic,copy) 	NSString *districtName;
@property(nonatomic,strong) UIImage *image;
@property(nonatomic,strong) NSURL *url;

@end
