//
//  RestaurantActivityItemProvider.m
//  Qraved
//
//  Created by System Administrator on 12/7/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import "RestaurantActivityItemProvider.h"

@implementation RestaurantActivityItemProvider

- (id)item{
    if([self.activityType isEqualToString:UIActivityTypePostToFacebook]){
//    	if(self.url){
//    		return @{@"text":self.title,@"url":self.url};
//    	}else{
//    		return @{@"text":self.title,@"image":self.image};
//    	}
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=FacebookPhoto",self.url]];
        return url;
        
    }else if([self.activityType isEqualToString:UIActivityTypePostToTwitter]){
//        return self.url;
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=TwitterPhoto",self.url]];
        return url;
    }else if([self.activityType isEqualToString:UIActivityTypeMail]){
        //                Hi\n  <br /> \
        Check out %@ photo at %@ on Qraved<br /> \  self.cuisineAreaPrice,self.restaurantTitle,
        
        return [NSString stringWithFormat:@"<html><body> \
                <a href=\"%@\">%@</a></body></html>",self.url,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeMessage]){
        //Hi\nCheck out this photo at %@ on Qraved!\n  ,self.restaurantTitle
    	return [NSString stringWithFormat:@"%@",self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeCopyToPasteboard]){
        return [self returnCopyUrl:self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeAddToReadingList]){
        return self.url;
    }else if([self.activityType isEqualToString:@"jp.naver.line.Share"]){
        //Look! It's %@ %@ on Qraved!\n   self.restaurantTitle,self.districtName,
        return [NSString stringWithFormat:@"%@",self.url];
    }
    else if([self.activityType isEqualToString:@"net.whatsapp.WhatsApp.ShareExtension"])
    {
        NSURL *tmpUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=WhatsappPhoto",self.url]];
        //[NSString stringWithFormat:@"Check out this photo at %@ on Qraved!\n",self.restaurantTitle]
        return @{@"title":@"",@"url":tmpUrl};

    }
    else {
        return self.placeholderItem;
    }
}

@end
