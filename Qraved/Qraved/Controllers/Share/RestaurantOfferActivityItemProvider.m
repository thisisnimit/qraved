//
//  RestaurantOfferActivityItemProvider.m
//  Qraved
//
//  Created by josn.liu on 2016/12/20.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "RestaurantOfferActivityItemProvider.h"

@implementation RestaurantOfferActivityItemProvider
- (id)item{
    if([self.activityType isEqualToString:UIActivityTypePostToFacebook]){
        //    	if(self.url){
        //    		return @{@"text":self.title,@"url":self.url};
        //    	}else{
        //    		return @{@"text":self.title,@"image":self.image};
        //    	}
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=FacebookPromo",self.url]];
        return url;
        
    }else if([self.activityType isEqualToString:UIActivityTypePostToTwitter]){
        //        return self.url;
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=TwitterPromo",self.url]];
        return url;
    }else if([self.activityType isEqualToString:UIActivityTypeMail]){
        return [NSString stringWithFormat:@"<html><body> \
                Hi\n  <br /> \
                Check out this promo at %@ on Qraved!<br />\
                <a href=\"%@\">%@</a></body></html>",self.restaurantTitle,self.url,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeMessage]){
        return [NSString stringWithFormat:@"Hi\nCheck out this promo at %@ on Qraved!\n%@",self.restaurantTitle,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeCopyToPasteboard]){
        return [self returnCopyUrl:self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeAddToReadingList]){
        return self.url;
    }else if([self.activityType isEqualToString:@"jp.naver.line.Share"]){
        return [NSString stringWithFormat:@"Look! It's %@ %@ on Qraved!\n%@",self.restaurantTitle,self.districtName,self.url];
    }
    else if([self.activityType isEqualToString:@"net.whatsapp.WhatsApp.ShareExtension"])
    {
        NSURL *tmpUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=WhatsappEvent",self.url]];
        return @{@"title":[NSString stringWithFormat:@"Check out this promo at %@ on Qraved!",self.restaurantTitle],@"url":tmpUrl};
        
    }
    else {
        return self.placeholderItem;
    }
}



@end
