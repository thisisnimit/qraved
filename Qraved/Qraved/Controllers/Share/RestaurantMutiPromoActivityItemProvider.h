//
//  RestaurantMutiPromoActivityItemProvider.h
//  Qraved
//
//  Created by imaginato on 16/4/29.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestaurantMutiPromoActivityItemProvider : UIActivityItemProvider
@property(nonatomic,copy) 	NSString *title;
@property(nonatomic,copy) 	NSString *restaurantTitle;
@property(nonatomic,copy) 	NSString *cuisineAreaPrice;
//@property(nonatomic,copy) 	NSString *description;
@property(nonatomic,copy) 	NSString *districtName;
@property(nonatomic,strong) UIImage *image;
@property(nonatomic,strong) NSURL *url;

@end
