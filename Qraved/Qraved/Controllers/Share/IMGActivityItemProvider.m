//
//  IMGActivityItemProvider.m
//  Qraved
//
//  Created by harry on 2018/3/16.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "IMGActivityItemProvider.h"

@implementation IMGActivityItemProvider

- (id)item{
    if([self.activityType isEqualToString:UIActivityTypePostToFacebook]){

        NSURL *tmpUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",self.url]];
        return tmpUrl;
    }else if([self.activityType isEqualToString:UIActivityTypePostToTwitter]){
        NSURL *tmpUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",self.url]];
        return tmpUrl;
    }else if([self.activityType isEqualToString:UIActivityTypeMail]){
        return [NSString stringWithFormat:@"<html><body> \
                %@<br /> \
                <a href=\"%@\">%@</a></body></html>",self.title,self.url,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeMessage]){
        return [NSString stringWithFormat:@"%@ %@",self.title,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeCopyToPasteboard]){
        return [NSString stringWithFormat:@"%@",self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeAddToReadingList]){
        return self.url;
    }else if([self.activityType isEqualToString:@"jp.naver.line.Share"]){
        return [NSString stringWithFormat:@"%@ %@",self.title,self.url];
    }else if ([self.activityType isEqualToString:@"com.google.Gmail.ShareExtension"]){
        return [NSString stringWithFormat:@"%@ %@",self.title,self.url];
    }
    else if([self.activityType isEqualToString:@"net.whatsapp.WhatsApp.ShareExtension"])
    {
        NSURL *tmpUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@",self.url]];
        return @{@"title":self.title,@"url":tmpUrl};
    }
    else {
        return self.placeholderItem;
    }
}

@end
