//
//  FBActivity.h
//  Qraved
//
//  Created by System Administrator on 12/4/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString* const UIActivityTypeFB=@"facebook.activity";

@interface FBActivity : UIActivity

@end
