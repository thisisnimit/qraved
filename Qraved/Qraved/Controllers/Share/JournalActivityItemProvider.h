//
//  JournalActivityItemProvider.h
//  Qraved
//
//  Created by System Administrator on 12/7/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JournalActivityItemProvider : UIActivityItemProvider

@property(nonatomic,copy) 	NSString *title;
@property(nonatomic,strong) NSURL *url;
@property(nonatomic,strong) NSNumber *journalId;

@end
