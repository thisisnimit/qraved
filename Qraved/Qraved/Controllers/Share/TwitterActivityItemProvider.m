//
//  TwitterActivityItemProvider.m
//  Qraved
//
//  Created by System Administrator on 1/15/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "TwitterActivityItemProvider.h"

@implementation TwitterActivityItemProvider

-(id)item{
	if([self.activityType isEqualToString:UIActivityTypePostToTwitter]){
        return self.title;
    }
    return self.placeholderItem;
}

@end
