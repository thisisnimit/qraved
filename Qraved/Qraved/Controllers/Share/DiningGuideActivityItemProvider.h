//
//  DiningGuideActivityItemProvider.h
//  Qraved
//
//  Created by System Administrator on 12/4/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiningGuideActivityItemProvider : UIActivityItemProvider

@property(nonatomic,copy) 	NSString *title;
//@property(nonatomic,copy) 	NSString *description;
@property(nonatomic,strong) UIImage *image;
@property(nonatomic,assign) NSUInteger restaurantCount;
@property(nonatomic,strong) NSURL *url;

@end
