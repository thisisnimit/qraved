//
//  UplodePhotoActivityItemProvider.h
//  Qraved
//
//  Created by Evan on 16/3/29.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UplodePhotoActivityItemProvider : UIActivityItemProvider

@property(nonatomic,copy) 	NSString *title;
@property(nonatomic,copy) 	NSString *restaurantTitle;
@property(nonatomic,copy) 	NSString *cuisineAreaPrice;
//@property(nonatomic,copy) 	NSString *description;
@property(nonatomic,copy) 	NSString *districtName;
@property(nonatomic,strong) UIImage *image;
@property(nonatomic,strong) NSURL *url;
@property(nonatomic,strong) NSString *userName;

@end
