//
//  MyListActivityItemProvider.m
//  Qraved
//
//  Created by System Administrator on 12/7/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import "MyListActivityItemProvider.h"

@implementation MyListActivityItemProvider

- (id)item{
    if([self.activityType isEqualToString:UIActivityTypePostToFacebook]){
        return @{@"text":self.title,@"url":self.url};
    }else if([self.activityType isEqualToString:UIActivityTypePostToTwitter]){
        return self.url;
    }else if([self.activityType isEqualToString:UIActivityTypeMail]){
        return [NSString stringWithFormat:@"<html><body> \
            Check out My List! <br /> \
            <b>%@</b><br /> \
            %@ <br />\
            <a href=\"%@\">%@</a></body></html>",self.myListTitle,self.restaurantTitles,self.url,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeMessage]){
        return [NSString stringWithFormat:@"I made a list called %@ on Qraved!\nYou should see it!\n%@",self.myListTitle,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeCopyToPasteboard]){
        return [self returnCopyUrl:self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeAddToReadingList]){
        return self.url;
    }else if([self.activityType isEqualToString:@"jp.naver.line.Share"]){
        return [NSString stringWithFormat:@"I made a list called %@ on Qraved!\nYou should see it!\n %@",self.myListTitle,self.url];
    }
    else if([self.activityType isEqualToString:@"net.whatsapp.WhatsApp.ShareExtension"])
    {
        NSURL *tmpUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=WhatsappMylist",self.url]];
        return @{@"text":@"Check out my awesome list!",@"url":tmpUrl};
    }else {
        return self.placeholderItem;
    }
}

@end
