//
//  MenuPhotoActivityItemProvider.m
//  Qraved
//
//  Created by Evan on 16/4/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "MenuPhotoActivityItemProvider.h"

@implementation MenuPhotoActivityItemProvider

- (id)item{
    if([self.activityType isEqualToString:UIActivityTypePostToFacebook]){
        //    	if(self.url){
        //    		return @{@"text":self.title,@"url":self.url};
        //    	}else{
        //    		return @{@"text":self.title,@"image":self.image};
        //    	}
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=FacebookMenu",self.url]];
        return url;
        
    }else if([self.activityType isEqualToString:UIActivityTypePostToTwitter]){
        //        return self.url;
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=TwitterMenu",self.url]];
        return url;
    }else if([self.activityType isEqualToString:UIActivityTypeMail]){
        return [NSString stringWithFormat:@"<html><body> \
                Hi Qravers,\n  <br /> \
                Check this out delicious menu at %@ on Qraved!\n<br />\
                <a href=\"%@\">%@</a></body></html>",self.restaurantTitle,self.url,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeMessage]){
        return [NSString stringWithFormat:@"Hi Qravers,\nLet’s find out delicious menu at %@ on Qraved!\n%@",self.restaurantTitle,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeCopyToPasteboard]){
        return [self returnCopyUrl:self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeAddToReadingList]){
        return self.url;
    }else if([self.activityType isEqualToString:@"jp.naver.line.Share"]){
        return [NSString stringWithFormat:@"Hi Qravers,\nLet’s find out delicious menu at %@ on Qraved!\n%@",self.restaurantTitle,self.url];
    }
    else if([self.activityType isEqualToString:@"net.whatsapp.WhatsApp.ShareExtension"])
    {
        NSURL *tmpUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=WhatsappMenu",self.url]];
        return @{@"key":@"Hi Qravers,",@"title":[NSString stringWithFormat:@"Let’s find out delicious menu at %@ on Qraved!",self.restaurantTitle],@"url":tmpUrl};
        
    }
    else {
        return self.placeholderItem;
    }
}


@end
