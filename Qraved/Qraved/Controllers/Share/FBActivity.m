//
//  FBActivity.m
//  Qraved
//
//  Created by System Administrator on 12/4/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import "FBActivity.h"
#import <Social/Social.h>
#import <Accounts/Accounts.h>
//#import <FacebookSDK/FacebookSDK.h>
@implementation FBActivity{
	SLComposeViewController *viewController;
}

-(instancetype)init{
	if(self=[super init]){
		viewController=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
		// viewController=[[SLComposeServiceViewController alloc ] init];
	}
	return self;
}

-(NSString*)activityType{
	return UIActivityTypeFB;
}

-(NSString*)activityTitle{
	return @"Facebook";
}

-(UIImage*)activityImage{
	return [UIImage imageNamed:@"BookFlowFacebook"];
}

+(UIActivityCategory)activityCategory{
	return UIActivityCategoryShare;
}

-(BOOL)canPerformWithActivityItems:(NSArray*)activityItems{
	return YES;
}

-(void)prepareWithActivityItems:(NSArray*)activityItems{
	UIView *shareView=[[UIView alloc] initWithFrame:CGRectMake(30,DeviceHeight-220,DeviceWidth-60,100)];
	shareView.backgroundColor=[UIColor grayColor];
	[viewController.view  addSubview:shareView];
}
 
-(UIViewController*)activityViewController{
	return viewController;
}

-(void)performActivity{
	ACAccountStore *store=[[ACAccountStore alloc] init];
	__block ACAccount *fbAccount=nil;
	ACAccountType *fbAccountType=[store accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
	NSArray *accounts=[store accountsWithAccountType:fbAccountType];
	if(!fbAccountType.accessGranted || accounts.count==0){
        NSDictionary *parameter=@{ACFacebookAppIdKey:[NSNumber numberWithLong:(long)376175509162709],ACFacebookPermissionsKey: @[@"email", @"user_about_me", @"user_likes"],ACFacebookAudienceKey: ACFacebookAudienceFriends};
		[store requestAccessToAccountsWithType:ACAccountTypeIdentifierFacebook options:parameter completion:^(BOOL granted, NSError *error){
			if(granted){
				NSArray *accounts=[store accountsWithAccountType:fbAccountType];
				fbAccount=[accounts lastObject];
				[self feed:fbAccount];

			}
		}];
	}else{
		fbAccount=[accounts lastObject];
		[self feed:fbAccount];
	}
}

-(void)feed:(ACAccount*)account{
	NSDictionary *parameters=@{@"message":@"this is a test"};
    SLRequest *request=[SLRequest requestForServiceType:SLServiceTypeFacebook requestMethod:SLRequestMethodPOST URL:[NSURL URLWithString:@"https://graph.facebook.com/me/feed"] parameters:parameters];
	request.account=account;
	[request performRequestWithHandler:^(NSData *responseData,NSHTTPURLResponse *urlResponse,NSError *error){
		NSLog(@"%@",responseData);
		NSLog(@"%@",urlResponse);
	}];
}


@end
