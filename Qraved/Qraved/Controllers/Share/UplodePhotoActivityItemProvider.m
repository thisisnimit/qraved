//
//  UplodePhotoActivityItemProvider.m
//  Qraved
//
//  Created by Evan on 16/3/29.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "UplodePhotoActivityItemProvider.h"

@implementation UplodePhotoActivityItemProvider

- (id)item{
    
    if([self.activityType isEqualToString:UIActivityTypePostToFacebook]){
     
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=FacebookPhoto",self.url]];
        return url;
        
    }else if([self.activityType isEqualToString:UIActivityTypePostToTwitter]){
   
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=TwitterPhoto",self.url]];
        return url;
    }else if([self.activityType isEqualToString:UIActivityTypeMail]){
        return [NSString stringWithFormat:@"<html><body> \
                Hi\n  <br /> \
                Check out %@ photo at %@ on Qraved<br /> \
                <a href=\"%@\">%@</a></body></html>",self.userName,self.restaurantTitle,self.url,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeMessage]){
        return [NSString stringWithFormat:@"Hi\nCheck out this photo at %@ on Qraved!\n%@",self.restaurantTitle,self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeCopyToPasteboard]){
        return [self returnCopyUrl:self.url];
    }else if([self.activityType isEqualToString:UIActivityTypeAddToReadingList]){
        return self.url;
    }else if([self.activityType isEqualToString:@"jp.naver.line.Share"]){
        return [NSString stringWithFormat:@"Look! It's %@ %@ on Qraved!\n%@",self.restaurantTitle,self.districtName,self.url];
    }
    else if([self.activityType isEqualToString:@"net.whatsapp.WhatsApp.ShareExtension"])
    {
        NSURL *tmpUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=WhatsappPhoto",self.url]];
        return @{@"title":[NSString stringWithFormat:@"Check out this photo at %@ on Qraved!\n",self.restaurantTitle],@"url":tmpUrl};
        
    }
    else {
        return self.placeholderItem;
    }
}


@end
