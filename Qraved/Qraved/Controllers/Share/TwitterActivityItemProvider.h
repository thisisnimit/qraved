//
//  twitterActivityItemProvider.h
//  Qraved
//
//  Created by System Administrator on 1/15/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TwitterActivityItemProvider : UIActivityItemProvider

@property(nonatomic,copy) NSString *title;

@end
