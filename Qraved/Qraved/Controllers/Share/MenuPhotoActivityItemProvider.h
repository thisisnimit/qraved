//
//  MenuPhotoActivityItemProvider.h
//  Qraved
//
//  Created by Evan on 16/4/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuPhotoActivityItemProvider : UIActivityItemProvider

@property(nonatomic,strong) NSURL *url;

@property(nonatomic,copy) 	NSString *title;

@property(nonatomic,copy) 	NSString *restaurantTitle;

@property(nonatomic,copy) 	NSString *districtName;

@end
