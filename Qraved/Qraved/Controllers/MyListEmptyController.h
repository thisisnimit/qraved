//
//  MyListEmptyController.h
//  Qraved
//
//  Created by System Administrator on 9/24/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "MyListDelegate.h"

@class IMGMyList;


@interface MyListEmptyController : BaseChildViewController<MyListDelegate>

@property(nonatomic,strong) IMGMyList *mylist;
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,assign) BOOL expandSideMenu;
@property(nonatomic,assign) BOOL canBack;
@property(nonatomic,assign) BOOL isOtherUser;
@property(nonatomic,assign) BOOL isFromMenu;
@property(nonatomic,assign) BOOL isNewList;


-(instancetype)initWithMyList:(IMGMyList*)mylist;

@end
