//
//  SelectTimeViewController.m
//  Qraved
//
//  Created by Laura on 14-8-13.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "SelectBookTimeViewController.h"
#import <CoreText/CoreText.h>

#import "SpecialOffersViewController.h"
#import "BookingInfoViewController.h"

#import "Label.h"
#import "UIView+Helper.h"
#import "UIViewController+Helper.h"
#import "UIColor+Helper.h"
#import "NSDate+Helper.h"
#import "UIImage+Helper.h"
#import "UIImageView+Helper.h"
#import "NSString+Helper.h"
#import "BookUtil.h"

#import "TYAttributedLabel.h"
#import "CustomIOS7AlertView.h"

#import "IMGRestaurant.h"
#import "IMGRestaurantOffer.h"
#import "IMGMeal.h"
#import "LoadingView.h"
#import "DBManager.h"
#import "AppDelegate.h"
//#import "MixpanelHelper.h"

@interface SelectBookTimeViewController ()
{
    UIScrollView *scrollView;
}
@end

@implementation SelectBookTimeViewController{
    IMGRestaurant *restaurant;
    NSArray *regularTimeArray;
    NSArray *restaurantOfferArray;
    IMGRestaurantOffer *restaurantOffer;
    NSDate *currentHHMMDate;
    NSDateFormatter *dateFormatterHHMM;
}
#pragma mark - init method
-(id)initWithRestaurant: (IMGRestaurant *)pRestaurant andTimeArray:(NSArray *)timeArray andOfferArray:(NSArray *)offerArray andOffer:(IMGRestaurantOffer *)tmpRestaurantOffer{
    self = [super init];
    if (self) {
        restaurant = pRestaurant;
        regularTimeArray = timeArray;
        restaurantOfferArray = offerArray;
        restaurantOffer=tmpRestaurantOffer;
    }
    return self;
}
#pragma mark - life cycle
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = L(@"Let's find you a table");
    self.navigationController.navigationBarHidden = NO;
    [super viewWillAppear:YES];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[LoadingView sharedLoadingView] stopLoading];

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"Reservation form page - select time";
    self.view.backgroundColor = [UIColor whiteColor];
    [self setBackBarButtonOffset30];
    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    [self.view addSubview:scrollView];
    [self loadMainView];
}
#pragma mark - add subviews
-(void)loadMainView
{
    //标题下阴影
    UIView *shadowImageView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 75)];
    shadowImageView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:shadowImageView];
    
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    imageV.centerX = shadowImageView.centerX;
    imageV.centerY = shadowImageView.centerY;
    imageV.backgroundColor =  [UIColor colorWithRed:241/255.0f green:15/255.0f blue:51/255.0f alpha:1.0];
    imageV.layer.cornerRadius = imageV.height/2;
    imageV.layer.masksToBounds = YES;
    [shadowImageView addSubview:imageV];
    
    UIView *redLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth - imageV.left, 2)];
    redLineView.centerY = imageV.centerY;
    redLineView.backgroundColor =  [UIColor colorWithRed:241/255.0f green:15/255.0f blue:51/255.0f alpha:1.0];
    [shadowImageView addSubview:redLineView];
    
    
    UIView *dinersView = [[UIView alloc]initWithFrame:CGRectMake(0, imageV.bottom +15, DeviceWidth, 20)];
    [shadowImageView addSubview:dinersView];
    
    NSString *dinersStrr =  L(@"TIME");
    CGSize maxSizeD = [dinersStrr sizeWithFont:[UIFont systemFontOfSize:14]];
    
    Label *DinersLabel = [[Label alloc]initWithFrame:CGRectMake(0, 0, maxSizeD.width, 20) andTextFont:[UIFont systemFontOfSize:14] andTextColor:[UIColor blackColor] andTextLines:1];
    DinersLabel.centerX = shadowImageView.centerX;
    DinersLabel.textAlignment = NSTextAlignmentLeft;
    DinersLabel.text = dinersStrr;
    DinersLabel.backgroundColor = [UIColor clearColor];
    [dinersView addSubview:DinersLabel];
    
    
    NSString *dateStr = [self.bookDate getDayMonthString];
    CGSize maxSize1 = [dateStr sizeWithFont:[UIFont systemFontOfSize:14]];
    
    Label *selectedDateLabel = [[Label alloc]initWithFrame:CGRectMake(0,0, maxSize1.width, 20) andTextFont:[UIFont systemFontOfSize:14] andTextColor:[UIColor lightGrayColor] andTextLines:1];
    selectedDateLabel.right = DinersLabel.left - 20;
    selectedDateLabel.text = dateStr;
    selectedDateLabel.textAlignment = NSTextAlignmentRight;
    selectedDateLabel.backgroundColor = [UIColor clearColor];
    [dinersView addSubview:selectedDateLabel];
    
    
    NSString *dinersStr = [NSString stringWithFormat:@"%d PAX",[self.pax intValue]];
//    CGSize maxSize = [dinersStr sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13]];
    
    TYAttributedLabel *dinerNumberLabel = [[TYAttributedLabel alloc]initWithFrame:CGRectMake(0,0, 60, 20)];
    dinerNumberLabel.right = selectedDateLabel.left - 15;
    dinerNumberLabel.backgroundColor = [UIColor clearColor];
    dinerNumberLabel.text = dinersStr;
    dinerNumberLabel.textColor = [UIColor lightGrayColor];
    dinerNumberLabel.font = [UIFont systemFontOfSize:14];
    [dinersView addSubview:dinerNumberLabel];

    NSMutableArray *timesArray=[[NSMutableArray alloc]init];
    NSMutableArray *currentOfferTimesArray;
    NSMutableArray *otherOfferTimesArray;
    NSMutableArray *noOfferTimesArray;
    
    if ([self bookDateCompareWithCurrentDate]) {
        dateFormatterHHMM =[[NSDateFormatter alloc] init];
        [dateFormatterHHMM setDateFormat:@"HH:mm"];
        
        NSString *currentDateString = [dateFormatterHHMM stringFromDate:[NSDate date]];
        currentHHMMDate = [dateFormatterHHMM dateFromString:currentDateString];
    }
    
    
    if([self.whichTypeSelected isEqualToString:@"1"]){
        currentOfferTimesArray = [[NSMutableArray alloc] initWithArray:[BookUtil availableTimesFromOffer:restaurantOffer inOfferArray:restaurantOfferArray  withPax:self.pax]];
        otherOfferTimesArray = [[NSMutableArray alloc] initWithArray:[BookUtil availableTimesNotFromOffer:restaurantOffer inOfferArray:restaurantOfferArray withPax:self.pax withAvailableTimeArray:currentOfferTimesArray]];
        if ([self bookDateCompareWithCurrentDate]) {
            for (int i=0; i<currentOfferTimesArray.count; i++) {
                NSDate *offerDate = [dateFormatterHHMM dateFromString:[currentOfferTimesArray objectAtIndex:i]];
                if ([offerDate compare:currentHHMMDate] == 1) {
                    [timesArray addObject:[currentOfferTimesArray objectAtIndex:i]];
                }
            }
            for(NSString *tmpBookTime in otherOfferTimesArray){
                if([timesArray indexOfObject:tmpBookTime]==NSNotFound){
                    NSDate *offerDate = [dateFormatterHHMM dateFromString:tmpBookTime];
                    if ([offerDate compare:currentHHMMDate] == 1) {
                        [timesArray addObject:tmpBookTime];
                    }
                }
            }
        }
        else{
            [timesArray addObjectsFromArray:currentOfferTimesArray];
            for(NSString *tmpBookTime in otherOfferTimesArray){
                if([timesArray indexOfObject:tmpBookTime]==NSNotFound){
                    [timesArray addObject:tmpBookTime];
                }
            }
        }

    }else if([self.whichTypeSelected isEqualToString:@"2"]){
        otherOfferTimesArray = [[NSMutableArray alloc] initWithArray:[BookUtil availableTimesNotFromOffer:restaurantOffer inOfferArray:restaurantOfferArray withPax:self.pax withAvailableTimeArray:[[NSMutableArray alloc] initWithArray:[BookUtil availableTimesFromOffer:restaurantOffer inOfferArray:restaurantOfferArray  withPax:self.pax]]]];
        if ([self bookDateCompareWithCurrentDate]) {
            for (int i=0; i<otherOfferTimesArray.count; i++) {
                NSDate *offerDate = [dateFormatterHHMM dateFromString:[otherOfferTimesArray objectAtIndex:i]];
                if ([offerDate compare:currentHHMMDate] == 1) {
                    [timesArray addObject:[otherOfferTimesArray objectAtIndex:i]];
                }
            }
        }else{
            [timesArray addObjectsFromArray:otherOfferTimesArray];
        }
    }
    noOfferTimesArray = [[NSMutableArray alloc] initWithArray:[BookUtil availableTimesFromRegular:regularTimeArray withPax:self.pax withAllTimeArray:timesArray]];
    for(NSString *tmpBookTime in noOfferTimesArray){
        if([timesArray indexOfObject:tmpBookTime]==NSNotFound){
            if ([self bookDateCompareWithCurrentDate]) {
                NSDate *offerDate = [dateFormatterHHMM dateFromString:tmpBookTime];
                if ([offerDate compare:currentHHMMDate] == 1) {
                    [timesArray addObject:tmpBookTime];
                }
            }else{
                [timesArray addObject:tmpBookTime];

            }
        }
    }
    
    NSSet *set = [NSSet setWithArray:timesArray];
    NSArray *tempArr = [set allObjects];
    timesArray = [NSMutableArray arrayWithArray:tempArr];
    
    [timesArray sortUsingComparator: ^NSComparisonResult(__strong id obj1,__strong id obj2){
        NSString *str1=(NSString *)obj1;
        NSString *str2=(NSString *)obj2;
        return [str1 compare:str2];
    }];
    
    
    NSMutableArray *mealArray = [[NSMutableArray alloc]initWithCapacity:0];
    NSMutableDictionary *timesDictionary = [[NSMutableDictionary alloc]init];
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGMeal ORDER BY sortOrder desc;"] successBlock:^(FMResultSet *resultSet) {
        int i=0;
        while ([resultSet next]) {
            IMGMeal  *meal=[[IMGMeal alloc]init];
            [meal setValueWithResultSet:resultSet];
            [mealArray addObject:meal];
            int intFromTime = [BookUtil timeStringToInt:meal.fromTime ];
            int intToTime = [BookUtil timeStringToInt:meal.toTime];
            NSMutableArray *mealTimeArray=[[NSMutableArray alloc]initWithCapacity:0];
            for(NSString *time in timesArray){
                int intTime = [BookUtil timeStringToInt:time];
                if(i==0){
                    if(intTime>=intFromTime && intTime<=intToTime){
                        [mealTimeArray addObject:time];
                    }
                }else{
                    if(intTime>=intFromTime && intTime<intToTime){
                        [mealTimeArray addObject:time];
                    }
                }
            }
            [timesDictionary setObject:mealTimeArray forKey:meal.mealName];
            i++;
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"addMenuView error when get datas:%@",error.description);
    }];
     UIButton *button;
    float startY=dinersView.endPointY;
    for(NSInteger ii=mealArray.count-1;ii>=0;ii--){
        IMGMeal *meal = [mealArray objectAtIndex:ii];
        NSArray *mealTimeArray = [timesDictionary objectForKey:meal.mealName];
       
        if(mealTimeArray!=nil && mealTimeArray.count>0){
            for (int i=0; i<mealTimeArray.count; i++) {
                if (i==0) {
                    Label *label = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, startY+10, DeviceWidth-2*LEFTLEFTSET, 30) andTextFont:[UIFont systemFontOfSize:14] andTextColor:[UIColor lightGrayColor] andTextLines:1];
                    label.text = [meal.mealName uppercaseString];
                    [scrollView addSubview:label];
                    startY = label.endPointY;
                }
                
                NSString *time = [mealTimeArray objectAtIndex:i];
                button = [UIButton buttonWithType:UIButtonTypeCustom];
                [button setTitle:time forState:UIControlStateNormal];
                button.frame = CGRectMake(LEFTLEFTSET+(63+13)*(i%4)*[AppDelegate ShareApp].autoSizeScaleX, startY+10+40*(i/4), 63, 32);
                
                button.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14];
                button.titleLabel.text = time;
                
                [button addTarget:self action:@selector(timeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
                if(currentOfferTimesArray!=nil && [currentOfferTimesArray indexOfObject:time]!=NSNotFound){
                    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    [button setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];//没有
                    button.tag=1;
                }else if(otherOfferTimesArray!=nil && [otherOfferTimesArray indexOfObject:time]!=NSNotFound){
                    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    [button setBackgroundImage:[[UIImage imageNamed:@"redRectangle@2x"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]forState:UIControlStateNormal] ;
                    button.tag=2;
                }else{
                    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                    [button setBackgroundImage:[UIImage imageNamed:@"whiteRectangle@2x"] forState:UIControlStateNormal];
                    button.tag=3;
                }
                
                [scrollView addSubview:button];
                if(i==mealTimeArray.count-1){
                    startY = button.endPointY;
                }
            }
        }
    }

    scrollView.contentSize = CGSizeMake(DeviceWidth, startY+120);
    
//    UIImageView *explainView = [[UIImageView alloc]initWithFrame:CGRectMake(0, DeviceHeight-90, DeviceWidth, 47)];
//    explainView.image = [UIImage imageNamed:@"BookFlowInformatOnTipsBottom"];
//    [self.view addSubview:explainView];
//    //    NSArray *imageArr = @[@"CurrentOfferAvailable",@"OtherOfferAvailable"];
//    NSArray *titleArray = @[@"",L(@"Other Promo")];
//    if(restaurantOffer!=nil&&[restaurantOffer.offerSlotMaxDisc intValue]>0){
//        titleArray = @[[NSString stringWithFormat:L(@"%@%@ offer available"),restaurantOffer.offerSlotMaxDisc,@"%"],L(@"Other Promo")];
//    }else if(restaurantOffer==nil){
//        titleArray = @[L(@"Current Promo"),L(@"Other Promo")];
//    }
//    
//    for (int i=0; i<titleArray.count; i++) {
//        CGSize expectSize = [[titleArray objectAtIndex:i] sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13]];
//        if (i==0) {
//            Label *label = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET+40, 47/2-expectSize.height/2, expectSize.width, expectSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor blackColor] andTextLines:1];
//            label.text = [titleArray objectAtIndex:i];
//            [explainView addSubview:label];
//        }
//        else
//        {
//            Label *label = [[Label alloc]initWithFrame:CGRectMake(DeviceWidth/3*2+10, 47/2-expectSize.height/2, expectSize.width, expectSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor blackColor] andTextLines:1];
//            label.text = [titleArray objectAtIndex:i];
//            [explainView addSubview:label];
//        }
//    }
    UIView *explainView = [[UIView alloc]initWithFrame:CGRectMake(0, button.bottom +20, DeviceWidth, 47)];
    explainView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:explainView];
    
    UIView *pointView = [[UIView alloc] initWithFrame:CGRectMake(LEFTLEFTSET, 47/2 - 5/2, 5, 5)];
    pointView.backgroundColor = [UIColor redColor];
    pointView.layer.cornerRadius = pointView.height/2;
    pointView.layer.masksToBounds = YES;
    [explainView addSubview:pointView];
    
    CGSize expectSize = [L(@"Available Promo") sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13]];
    
    Label *label = [[Label alloc]initWithFrame:CGRectMake(pointView.right +5, 47/2-expectSize.height/2, expectSize.width, expectSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor lightGrayColor] andTextLines:1];
    label.text = L(@"Available Promo");
    [explainView addSubview:label];
}
#pragma mark - view click event
-(void)timeButtonClick:(UIButton *)button
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:self.pax forKey:@"dinners"];
    [userDefaults setObject:self.bookDate forKey:@"date"];
    [userDefaults setObject:button.titleLabel.text forKey:@"time"];
    [userDefaults setObject:nil forKey:@"voucharCode"];
//    [MixpanelHelper trackChooseBookingPropertiesWithRestaurant:restaurant andReservation:restaurantOffer];
    
//    if(button.tag==1 || button.tag==3){
//        BookingInfoViewController *bookingInfoViewController;
//        if (button.tag==1) {
//          bookingInfoViewController  = [[BookingInfoViewController alloc]initWithRestaurant:restaurant andOffer:restaurantOffer];
//        }else{
//        bookingInfoViewController  = [[BookingInfoViewController alloc]initWithRestaurant:restaurant andOffer:nil];
//
//        }
//        bookingInfoViewController.pax = self.pax;
//        bookingInfoViewController.bookDate = self.bookDate;
//        bookingInfoViewController.bookTime = button.titleLabel.text;
//        bookingInfoViewController.iconImage = self.iconImage;
//        bookingInfoViewController.shopStr = self.shopStr;
//        bookingInfoViewController.detailStr = self.detailStr;
//        [self.navigationController pushViewController:bookingInfoViewController animated:YES];
//    }else if(button.tag==2){
        SpecialOffersViewController *specialOffersViewController = [[SpecialOffersViewController alloc]initWithRestaurant:restaurant andTimeArray:regularTimeArray andOfferArray:restaurantOfferArray];
        specialOffersViewController.pax = self.pax;
        specialOffersViewController.bookDate = self.bookDate;
        specialOffersViewController.bookTime = button.titleLabel.text;
        [self.navigationController pushViewController:specialOffersViewController animated:YES];
  //  }
    
    
    
}
//-(void)dinerNumClick
//{
//    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-3] animated:YES];
//}
//-(void)dateNumClick
//{
//    [self.navigationController popViewControllerAnimated:YES];
//}
#pragma mark - private method
-(BOOL)bookDateCompareWithCurrentDate{
    NSDateFormatter *dateFormatter1=[[NSDateFormatter alloc]init];
    [dateFormatter1 setDateFormat:@"MM/dd/yyyy"];
    NSString *currentDateString = [[NSDate date] get_MM_DD_YYYYFormatString];
    NSDate *currentDate = [dateFormatter1 dateFromString:currentDateString];
    NSString *bookedDateString = [self.bookDate get_MM_DD_YYYYFormatString];
    NSDate *bookedDate = [dateFormatter1 dateFromString:bookedDateString];
    
    return [bookedDate isEqualToDate:currentDate];
}


@end
