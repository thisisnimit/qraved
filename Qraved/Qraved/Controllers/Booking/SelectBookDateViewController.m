//
//  SelectBookDateViewController.m
//  Qraved
//
//  Created by Jeff on 10/21/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "SelectBookDateViewController.h"

#import <CoreText/CoreText.h>

#import "SelectBookTimeViewController.h"
#import "NSString+Helper.h"
#import "Label.h"
#import "UIView+Helper.h"
#import "UIViewController+Helper.h"
#import "UIColor+Helper.h"
#import "UIImage+Helper.h"
#import "UIImageView+Helper.h"
#import "DateView.h"
#import "LoadingView.h"
#import "AppDelegate.h"
#import "BookUtil.h"
#import "BookHandler.h"
#import "IMGRestaurantOffer.h"
#import "TYAttributedLabel.h"
@interface SelectBookDateViewController ()
{
    UIScrollView *scrollView;
    NSArray *customDates;
    NSArray *customWeeks;
    
    IMGRestaurant *restaurant;
    IMGRestaurantOffer *restaurantOffer;
    NSMutableArray *regularTimeArray;
    NSMutableArray *restaurantOfferArray;
    
    NSArray *currentMonthArr;
    NSArray *nextMonthArr;
    BOOL isButtonSelect;
}

@end

@implementation SelectBookDateViewController

#pragma mark - init method
-(id)initWithRestaurant: (IMGRestaurant *)pRestaurant andTimeArray:(NSArray *)timeArray andOfferArray:(NSArray *)offerArray andOffer:(IMGRestaurantOffer *)tmpRestaurantOffer{
    self = [super init];
    if (self) {
        restaurant = pRestaurant;
        regularTimeArray = [[NSMutableArray alloc] initWithArray:timeArray];
        restaurantOfferArray = [[NSMutableArray alloc] initWithArray:offerArray];
        restaurantOffer=tmpRestaurantOffer;
    }
    return self;
}
#pragma mark - life cycle
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.title = L(@"Let's find you a table");
    self.navigationController.navigationBarHidden = NO;
    [super viewWillAppear:YES];
    
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[LoadingView sharedLoadingView ] stopLoading];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Reservation form page - select date";
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self selector:@selector(dateSelected:) name:@"SelectedDate" object:scrollView];
    
    self.view.backgroundColor = [UIColor whiteColor];
    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    [self.view addSubview:scrollView];
    
    [self setBackBarButtonOffset30];
    [self getCalendarCurrentOfferDate];
    //[self loadMainView];
}
#pragma mark - get data
- (void)getCalendarCurrentOfferDate
{
    
    if (restaurantOffer)
    {
        NSDictionary * parameters = @{@"offerId":restaurantOffer.offerId,@"peopleCount":self.pax,@"restaurantID":restaurant.restaurantId};
        __block SelectBookDateViewController *blockSelf = self;
        [BookHandler getOfferDateWithParams:parameters andBlock:^(NSArray *thisMonthArr, NSArray *nextMonthArry) {
            currentMonthArr = thisMonthArr;
            nextMonthArr = nextMonthArry;
            [blockSelf loadMainView];

        }];

    }
    else
        [self loadMainView];
    
}
#pragma mark - add subview
-(void)loadMainView
{
    //标题下阴影
    UIView *shadowImageView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 75)];
    shadowImageView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:shadowImageView];
    
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    imageV.centerX = shadowImageView.centerX;
    imageV.centerY = shadowImageView.centerY;
    imageV.backgroundColor =  [UIColor colorWithRed:241/255.0f green:15/255.0f blue:51/255.0f alpha:1.0];
    imageV.layer.cornerRadius = imageV.height/2;
    imageV.layer.masksToBounds = YES;
    [shadowImageView addSubview:imageV];
    
    UIView *redLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 2)];
    redLineView.centerY = imageV.centerY;
    redLineView.backgroundColor =  [UIColor colorWithRed:241/255.0f green:15/255.0f blue:51/255.0f alpha:1.0];
    [shadowImageView addSubview:redLineView];

    
    UIView *dinersView = [[UIView alloc]initWithFrame:CGRectMake(0, imageV.bottom +15, DeviceWidth, 27)];
    [shadowImageView addSubview:dinersView];
    

    TYAttributedLabel *dataLabel = [[TYAttributedLabel alloc]initWithFrame:CGRectMake(0, 27/2- 10, 40, 20)];
    dataLabel.centerX = dinersView.centerX;
    dataLabel.text = @"DATE";
    dataLabel.textColor = [UIColor blackColor];
    dataLabel.font = [UIFont systemFontOfSize:14];
    [dinersView addSubview:dataLabel];

    
    NSString *dinersStr = [NSString stringWithFormat:L(@"%d PAX"),[self.pax intValue]];
//    CGSize maxSize = [dinersStr sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15]];
    
    TYAttributedLabel *dinerNumberLabel = [[TYAttributedLabel alloc]initWithFrame:CGRectMake(0, 0, 60, 20)];
    dinerNumberLabel.right = dataLabel.left - 20;
    dinerNumberLabel.centerY = dataLabel.centerY;
    dinerNumberLabel.text = dinersStr;
    dinerNumberLabel.textColor = [UIColor lightGrayColor];
    dinerNumberLabel.font = [UIFont systemFontOfSize:14];
    [dinersView addSubview:dinerNumberLabel];

    NSArray *currentOfferAvailableWeekDayArray;
    NSArray *otherOfferAvailableWeekDayArray;
    if([self.whichTypeSelected isEqualToString:@"1"]){
        currentOfferAvailableWeekDayArray = [BookUtil availableWeekDaysFromOffer:restaurantOffer withPax:self.pax];
        
        otherOfferAvailableWeekDayArray = [BookUtil availableWeekDaysNotFromOffer:restaurantOffer inOfferArray:restaurantOfferArray withPax:self.pax withAvailableWeekDays:currentOfferAvailableWeekDayArray];
    }else if([self.whichTypeSelected isEqualToString:@"2"]){
        otherOfferAvailableWeekDayArray = [BookUtil availableWeekDaysNotFromOffer:restaurantOffer inOfferArray:restaurantOfferArray withPax:self.pax withAvailableWeekDays:[BookUtil availableWeekDaysFromOffer:restaurantOffer withPax:self.pax]];
    }
    
    DateView *dateView = [[DateView alloc]initWithRestaurant:restaurant andTimeArray:regularTimeArray andOfferArray:restaurantOfferArray andOffer:restaurantOffer andCurrentOfferAvailableWeekDayArray:currentOfferAvailableWeekDayArray andOtherOfferAvailableWeekDayArray:otherOfferAvailableWeekDayArray andPax:self.pax andSlotTimeBookedArray:nil andOfferTimeBookedArray:nil andThisMonthArr:currentMonthArr andNextMonthArr:nextMonthArr];
    dateView.backgroundColor = [UIColor whiteColor];
    dateView.frame = CGRectMake(0, shadowImageView.endPointY+8, DeviceWidth, dateView.heightOfView);
    [scrollView addSubview:dateView];
    scrollView.contentSize = CGSizeMake(DeviceWidth, dateView.endPointY+80);
 
    UIView *explainView = [[UIView alloc]initWithFrame:CGRectMake(0, DeviceHeight-90, DeviceWidth, 47)];
    explainView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:explainView];
    
    UIView *pointView = [[UIView alloc] initWithFrame:CGRectMake(LEFTLEFTSET, 47/2 - 5/2, 5, 5)];
    pointView.backgroundColor = [UIColor redColor];
    pointView.layer.cornerRadius = pointView.height/2;
    pointView.layer.masksToBounds = YES;
    [explainView addSubview:pointView];
    
    CGSize expectSize = [L(@"Available Promo") sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13]];
    
    Label *label = [[Label alloc]initWithFrame:CGRectMake(pointView.right +5, 47/2-expectSize.height/2, expectSize.width, expectSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor lightGrayColor] andTextLines:1];
    label.text = L(@"Available Promo");
    [explainView addSubview:label];

    
}
#pragma mark - view click event
//-(void)paxClicked
//{
//    [self.navigationController popViewControllerAnimated:YES];
//}

-(void)dateSelected:(NSNotification*)notification
{
    if (isButtonSelect)
    {
        return;
    }
    isButtonSelect = YES;
    //add day/times to go to next view controller
    [[LoadingView sharedLoadingView]startLoading];
    NSDictionary *dateSelectedDictionary = notification.object;
    NSDateFormatter *formatter = [[NSDateFormatter  alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *bookDate = [dateSelectedDictionary objectForKey:@"date"];
    NSString *currentDateStr= [formatter stringFromDate:bookDate];
    NSString *whichDateTypeSelected = [dateSelectedDictionary objectForKey:@"whichDateTypeSelected"];
    
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:currentDateStr,@"bookDate",self.pax,@"peopleCount",restaurant.restaurantId,@"restaurantID",nil];
    __block SelectBookDateViewController *blockSelf = self;
    [BookHandler getDayAndTimesWithParams:parameters andBlock:^(NSArray *responseArray) {
        if(responseArray!=nil && [responseArray count]>0){
            NSDictionary *timeSlotDictionary = [responseArray objectAtIndex:0];
            NSMutableArray *tmpRestaurantOfferArray = [responseArray objectAtIndex:1];
            if(regularTimeArray!=nil){
                [regularTimeArray removeAllObjects];
            }
            regularTimeArray = [[NSMutableArray alloc] initWithArray: [timeSlotDictionary objectForKey:@"times"]];
            if(restaurantOfferArray!=nil){
                [restaurantOfferArray removeAllObjects];
            }
            for(int i=0;i<tmpRestaurantOfferArray.count;i++){
                NSDictionary *offerDictionary = [tmpRestaurantOfferArray objectAtIndex:i];
                IMGRestaurantOffer *tmpRestaurantOffer = [[IMGRestaurantOffer alloc]init];
                [tmpRestaurantOffer setValuesForKeysWithDictionary:offerDictionary];
                NSDictionary *pictureInfo=[offerDictionary objectForKey:@"pictureInfo"];
                double width = [[pictureInfo objectForKey:@"width"] doubleValue];
                double height = [[pictureInfo objectForKey:@"height"] doubleValue];
                if (width!=0||height!=0) {
                    restaurantOffer.offerImageH = [NSNumber numberWithInt:(DeviceWidth*height)/width];
                }
                
                tmpRestaurantOffer.restaurantId = restaurant.restaurantId;
                [restaurantOfferArray addObject:tmpRestaurantOffer];
            }
            [blockSelf loadSelectBookTimeViewController:bookDate andWhichDateTypeSelected:whichDateTypeSelected];
            isButtonSelect = NO;
        }else{
            [blockSelf loadSelectBookTimeViewController:bookDate  andWhichDateTypeSelected:whichDateTypeSelected];
            isButtonSelect = NO;
        }
    } andErroBlock:^{
        [blockSelf loadSelectBookTimeViewController:bookDate  andWhichDateTypeSelected:whichDateTypeSelected];
        isButtonSelect = NO;

    }];
}

-(void)loadSelectBookTimeViewController:(NSDate *)bookDate andWhichDateTypeSelected:(NSString *)whichDateTypeSelected{
    SelectBookTimeViewController *selectBookTimeViewController;
    if([whichDateTypeSelected isEqualToString:@"1"] || [whichDateTypeSelected isEqualToString:@"2"]){
        selectBookTimeViewController = [[SelectBookTimeViewController alloc]initWithRestaurant:restaurant andTimeArray:regularTimeArray andOfferArray:restaurantOfferArray andOffer:restaurantOffer];
    }else if([whichDateTypeSelected isEqualToString:@"3"]){
        selectBookTimeViewController = [[SelectBookTimeViewController alloc]initWithRestaurant:restaurant andTimeArray:regularTimeArray andOfferArray:nil andOffer:nil];
    }
    selectBookTimeViewController.whichTypeSelected=whichDateTypeSelected;
    selectBookTimeViewController.pax = self.pax;
    selectBookTimeViewController.bookDate = bookDate;
    selectBookTimeViewController.iconImage = self.iconImage;
    selectBookTimeViewController.shopStr = self.shopStr;
    selectBookTimeViewController.detailStr = self.detailStr;
    
    [[LoadingView sharedLoadingView]stopLoading];
    [self.navigationController pushViewController:selectBookTimeViewController animated:YES];
}

@end
