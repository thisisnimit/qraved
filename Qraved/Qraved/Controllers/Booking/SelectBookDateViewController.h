//
//  SelectBookDateViewController.h
//  Qraved
//
//  Created by Jeff on 10/21/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "IMGRestaurant.h"
#import "IMGRestaurantOffer.h"

@interface SelectBookDateViewController : BaseChildViewController
@property (nonatomic,retain)NSNumber *pax;
@property (nonatomic,retain)NSString *whichTypeSelected;//1,current offer. 2,other offer. 3,no offer

@property (nonatomic,retain)  NSString  *iconImage;
@property (nonatomic,retain)  NSString  *shopStr;
@property (nonatomic,retain)  NSString  *detailStr;

-(id)initWithRestaurant: (IMGRestaurant *)pRestaurant andTimeArray:(NSArray *)timeArray andOfferArray:(NSArray *)offerArray andOffer:(IMGRestaurantOffer *)tmpRestaurantOffer;

@end
