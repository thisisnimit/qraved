//
//  BookingInfoViewController.m
//  Qraved
//
//  Created by Laura on 14-8-14.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BookingInfoViewController.h"
#import <CoreText/CoreText.h>
#import "AppDelegate.h"
#import "LoginParamter.h"
#import "EmailLoginViewController.h"
#import "BookingConfirmViewController.h"
#import "SelectCityController.h"
#import "FBSignUpViewController.h"
#import "Label.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "NSDate+Helper.h"
#import "NSString+Helper.h"
#import "UIConstants.h"
#import "UILabel+Helper.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Helper.h"
#import "UIViewController+Helper.h"
#import "LoadingView.h"
#import "BookHandler.h"
#import "DBManager.h"
#import "IMGUser.h"
#import "IMGReservation.h"
#import "Helper.h"
//#import "MixpanelHelper.h"
#import "IMGCountryCode.h"
#import "DetailDataHandler.h"
#import "NavigationBarButtonItem.h"
#import "LoginParamter.h"
#import "EntityKind.h"

@interface BookingInfoViewController ()<UIPickerViewDataSource,UIPickerViewDelegate,GIDSignInDelegate,GIDSignInUIDelegate,UIScrollViewDelegate, UITextViewDelegate>
{
    UIScrollView *_scrollView;
    UIView *signInView;
    UIView *bookInfoView;
    UITextField *firstNameTextField;
    UITextField *lastNameTextField;
    UITextField *emailTextField;
    UITextField *countryCodeTextField;
    UITextField *mobileNumberTextField;
    UITextView *specialRequestTextField;
    UITextField *voucherCodeTextField;
    UIButton *submitButton;
    float navigationHeight;
    
    IMGRestaurant *restaurant;
    IMGRestaurantOffer *restaurantOffer;
    IMGReservation *reservation;
    
    int gender;
    BOOL fromManualEntry;
    
    UIView *codeNumView;
    UIPickerView *codeNumPicker;
    UIButton *doneButton2;
    
    NSMutableArray *codeArray;
    NSString *selectCode;
    NSInteger selectCodeIndex;
    BOOL isViewUp;
    NSString *phoneWithoutCountryCode;
    UITextField *passWordTextField;
    int num;
    NSArray *placeholderArray;
    int subnum;
    UIImageView *lineImageViewMonbiel;
    UIControl *dateControl;
    UIImageView *lineImageView4;
    UIImageView *lineImageView5;
    UIButton* selectedbtn;
    UIImageView *lineImageView6;
    UILabel* lable;
    UITextField *emailAccountTextField;
    UITextField *passwordTextField;
    UIView *bottomView;
    UILabel *lblEmail;
    UILabel *lblMobileNumber;
    UILabel *lblSpecialRequest;
    UIView *specialLine;
    UILabel *lblVoucherCode;
    UIView *specialTVLine;
}
@end

@implementation BookingInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithRestaurant: (IMGRestaurant *)pRestaurant andOffer:(IMGRestaurantOffer *)tmpRestaurantOffer{
    self = [super init];
    if (self) {
        restaurant = pRestaurant;
        restaurantOffer=tmpRestaurantOffer;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    submitButton.enabled = YES;
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = L(@"Summary");
    [super viewWillAppear:YES];
    
    [self.navigationController.navigationBar setHidden:NO];
    [[LoadingView sharedLoadingView] stopLoading];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isFacebookSignUp"]) {
        [self loadMainView];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFacebookSignUp"];

    }

}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [bottomView removeFromSuperview];
    [[LoadingView sharedLoadingView] stopLoading];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"Reservation form page - personal information";
   //[self setBackBarButtonOffset30];
    NavigationBarButtonItem *navigationBarLeftButton = [[NavigationBarButtonItem alloc ]initWithImage:[UIImage imageNamed:NavigationBackImage] andTitle:@"" target:self action:@selector(goToBackViewController) width:43.0f offset:-15-[UIDevice heightDifference]];
    self.navigationItem.leftBarButtonItem = navigationBarLeftButton;
    
    [GIDSignIn sharedInstance].uiDelegate=self;
    [GIDSignIn sharedInstance].delegate = self;
    if ([UIDevice isIphone5]) {
        navigationHeight = 0;
    }
    else
        navigationHeight = 84;
    codeArray = [[NSMutableArray alloc]init];
    selectCode=@"62";
    if ([AppDelegate ShareApp].userLogined && [IMGUser currentUser].phone) {
        
        phoneWithoutCountryCode=[IMGUser currentUser].phone;
        NSRange range =[[IMGUser currentUser].phone rangeOfString:@"-"];
        if(range.location!=NSNotFound){
            selectCode=[[IMGUser currentUser].phone substringToIndex:range.location];
            NSRange range2 =[selectCode rangeOfString:@"+"];
            if (range2.location != NSNotFound) {
                selectCode = [selectCode substringFromIndex:range2.location+1];
            }
            
            phoneWithoutCountryCode = [[IMGUser currentUser].phone substringFromIndex:range.location+1];
        }else{
            range =[[IMGUser currentUser].phone rangeOfString:@"+"];
            if(range.location!=NSNotFound){
                phoneWithoutCountryCode = [[IMGUser currentUser].phone substringFromIndex:range.location+1];
            }
            
        }
        
    }
    self.view.backgroundColor = [UIColor whiteColor];
    
    [[LoadingView sharedLoadingView] startLoading];
    [DetailDataHandler getBookInfoFromService:restaurant andBlock:^{
        [self loadMainView];
        [self addNotification];
        [[LoadingView sharedLoadingView] stopLoading];
    }];

    
}
- (void)goToBackViewController{
    if (fromManualEntry) {
        [signInView setHidden:NO];
        [bookInfoView setHidden:YES];
        [bottomView setHidden:NO];
        fromManualEntry=NO;
        [self.view endEditing:YES];
        _scrollView.contentOffset=CGPointMake(0, 0);
        return;

    }
    [self removeNotifications];
    [self.navigationController popViewControllerAnimated:YES];
    NSMutableDictionary* eventDic=[[NSMutableDictionary alloc]init];
    [eventDic setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
    [[Amplitude instance] logEvent:@"TR - Reservation Cancel" withEventProperties:eventDic];

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[DBManager manager]selectWithSql:@"select *from IMGCountryCode order by CAST(code AS INTeger)" successBlock:^(FMResultSet *resultSet) {
        while ([resultSet next]) {
            IMGCountryCode *countryCode = [[IMGCountryCode alloc]init];
            [countryCode setValueWithResultSet:resultSet];
            [codeArray addObject:countryCode];
        }
        [resultSet close];
        for (int i =0;i<codeArray.count;i++) {
            IMGCountryCode *code = [codeArray objectAtIndex:i];
            if ([code.code intValue] == [selectCode intValue]) {
                selectCode = [NSString stringWithFormat:@"%@",code.code];
                selectCodeIndex = i;
                break;
            }
        }
    } failureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

-(void)addNotification{
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(loginSucceed) name:@"loginNotification" object:nil];

    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(loginWithTwitterInQraved:) name:TwitterLoginSelfPartDone object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccessful) name:@"loginSuccessfulFromBooking" object:nil];
}
-(void)removeNotification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"loginNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TwitterLoginSelfPartDone object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"loginSuccessfulFromBooking" object:nil];
}

-(void)loginSucceed{
    [self loadMainView];
    if (bottomView) {
        [bottomView removeFromSuperview];
    }
    [signInView setHidden:YES];
    [bookInfoView setHidden:NO];
    IMGUser *user=[IMGUser currentUser];
    [firstNameTextField setText:user.firstName];
    [lastNameTextField setText:user.lastName];
    [emailTextField setText:user.email];
//    [mobileNumberTextField setText:user.phone];
    if (user.phone == nil) {
        return;
    }
    phoneWithoutCountryCode=user.phone;
    selectCode=@"62";
    NSRange range =[user.phone rangeOfString:@"-"];
    if(range.location!=NSNotFound){
        selectCode=[user.phone substringToIndex:range.location];
        NSRange range2 =[selectCode rangeOfString:@"+"];
        if (range2.location != NSNotFound) {
            selectCode = [selectCode substringFromIndex:range2.location+1];
        }
        phoneWithoutCountryCode = [user.phone substringFromIndex:range.location+1];
    }else{
        range =[user.phone rangeOfString:@"+"];
        if(range.location!=NSNotFound){
            phoneWithoutCountryCode = [user.phone substringFromIndex:range.location+1];
        }
    }
    countryCodeTextField.text = [NSString stringWithFormat:@"+%@",selectCode];
    mobileNumberTextField.text = phoneWithoutCountryCode;
}

-(void)loadMainView
{
    if (_scrollView) {
        [_scrollView removeFromSuperview];
    }
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    _scrollView.contentSize = CGSizeMake(DeviceWidth, DeviceHeight+navigationHeight+40 +200);
    _scrollView.delegate = self;
    _scrollView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_scrollView];
    
    
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 100)];
    topView.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:topView];
    
    UIImageView *iconImageV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 40, 40)];
    [iconImageV sd_setImageWithURL:[NSURL URLWithString:[self.iconImage returnFullImageUrl]]];
    
    [topView addSubview:iconImageV];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(iconImageV.right +5, 0, DeviceWidth - iconImageV.right - 10, 20)];
    lblTitle.centerY = iconImageV.centerY - 10;
    lblTitle.text =self.shopStr ;//@"Pepenero";
    lblTitle.textColor = [UIColor color333333];
    lblTitle.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12];
    lblTitle.numberOfLines = 1;
    [topView addSubview:lblTitle];
    
    UILabel *lblSubTitle = [[UILabel alloc] initWithFrame:CGRectMake(iconImageV.right +5, lblTitle.bottom, DeviceWidth - iconImageV.right - 10, 20)];
    lblSubTitle.text = self.detailStr;//@"Western . Senopati";
    lblSubTitle.textColor = [UIColor lightGrayColor];
    lblSubTitle.numberOfLines = 1;
    lblSubTitle.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12];
    [topView addSubview:lblSubTitle];
    
    UILabel *lblPeopleNo = [[UILabel alloc] initWithFrame:CGRectMake(10, iconImageV.bottom + 5, DeviceWidth - 20, 20)];
    lblPeopleNo.text = [NSString stringWithFormat:@"Pax for %d people",[self.pax intValue]];
    lblPeopleNo.textColor = [UIColor color333333];
    lblPeopleNo.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12];
    [topView addSubview:lblPeopleNo];

    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:self.bookDate.getYYYY_MM_DDCenterLineFormatString];
    [dateFormatter setDateFormat:@"EEEE, MMM dd yyyy"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    [dateFormatter setDateFormat:@", dd MMM"];
    
    NSDateFormatter *dateFormat4 = [ [NSDateFormatter alloc] init];
    [dateFormat4 setDateFormat:@"HH:mm"];
    NSDate *date1 = [dateFormat4 dateFromString:self.bookTime];
    NSDateFormatter *dateFormat3 = [ [NSDateFormatter alloc] init];
    [dateFormat3 setDateFormat:@"hh:mm a"];
    NSString  *string =  [dateFormat3 stringFromDate: date1];

    
    
    UILabel *lblDate = [[UILabel alloc] initWithFrame:CGRectMake(10, lblPeopleNo.bottom + 5, DeviceWidth - 20, 20)];
    lblDate.text = [NSString stringWithFormat:@"%@ %@",dateString,string];//self.bookTime;
    lblDate.textColor = [UIColor color333333];
    lblDate.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12];
    [topView addSubview:lblDate];
    
//    UIView *backDownV = [[UIView alloc] initWithFrame:CGRectMake(0, lblDate.bottom, DeviceWidth, 40)];
//    backDownV.backgroundColor = [UIColor redColor];
//    [topView addSubview:backDownV];
//    
//    UILabel *lblDownTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, DeviceWidth - 20, 20)];
//    lblDownTitle.text = @"CONTACT INFORMATION";
//    lblDownTitle.textColor = [UIColor lightGrayColor];
//    lblDownTitle.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14];
//    [backDownV addSubview:lblDownTitle];

    UIView *offerView = [[UIView alloc]initWithFrame:CGRectMake(10, topView.endPointY+1, DeviceWidth - 20, 60)];
    offerView.backgroundColor = [UIColor whiteColor];
    offerView.layer.borderWidth = 1;
    offerView.layer.borderColor = [UIColor colorEDEDED].CGColor;
    [_scrollView addSubview:offerView];
    if (bottomView) {
        [bottomView removeFromSuperview];
    }
    if(restaurantOffer!=nil){
        NSString *discountStr;
        discountStr = [restaurantOffer.offerTitle removeHTML];

//        CGSize expectSize = [discountStr sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13]];
        Label *discountLabel = [[Label alloc]initWithFrame:CGRectMake(10, 10, offerView.width - 80, 40) andTextFont:[UIFont systemFontOfSize:14] andTextColor:[UIColor colorC2060A] andTextLines:1];
        discountLabel.text = discountStr;
        discountLabel.backgroundColor = [UIColor whiteColor];
        discountLabel.numberOfLines = 2;
     
        [offerView addSubview:discountLabel];
        UIImageView *discountImageView = [[UIImageView alloc]initWithFrame:CGRectMake(offerView.right - 60, 10, 40, 40)];
        [offerView addSubview:discountImageView];
        
        NSURL *urlString;
        if ([restaurantOffer.typeIcon hasPrefix:@"http"])
        {
            urlString = [NSURL URLWithString:[restaurantOffer.typeIcon filterHtml]];
        }
        else
        {
            urlString = [NSURL URLWithString:[restaurantOffer.typeIcon returnFullImageUrl]];
        }
        
        __weak UIImageView *weakimageView = discountImageView;
        
        if ([restaurantOffer.typeIcon isEqualToString:@""]) {
            
//            discountImageView.image = [UIImage imageNamed:@"BookFlowOfferTypeTag0"];
            [discountImageView sd_setImageWithURL:[NSURL URLWithString:[self.iconImage returnFullImageUrl]]];
        }else{
            [discountImageView sd_setImageWithURL:urlString placeholderImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[self.iconImage returnFullImageUrl]]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (image.size.height>0&&image.size.width>0) {
                    image = [image cutCircleImage];
                    weakimageView.image=image;
                }else{
//                    weakimageView.image=[UIImage imageNamed:@"BookFlowOfferTypeTag0"];
                    [discountImageView sd_setImageWithURL:[NSURL URLWithString:[self.iconImage returnFullImageUrl]]];
                }
            }];
        }
        
        [self initBookInfoView:offerView];
        [self initSignInView:offerView];


    }else{
    
        [self initBookInfoView:topView];
        [self initSignInView:topView];
    
    }
    
       if ([AppDelegate ShareApp].userLogined) {
        [bookInfoView setHidden:NO];
        [signInView setHidden:YES];
        bottomView.hidden = YES;
    }else{
        [bookInfoView setHidden:YES];
        [signInView setHidden:NO];
    }
    
    UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:0 andHeight:4];
    [self.view addSubview:shadowImageView];
}

-(void)initBookInfoView:(UIView *)offerView{
    UILabel *yourDtailLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, offerView.endPointY, DeviceWidth, 40)];
    yourDtailLabel.text = @"Your Details";
    yourDtailLabel.textAlignment = NSTextAlignmentCenter;
    yourDtailLabel.font = [UIFont systemFontOfSize:25];
    [_scrollView addSubview:yourDtailLabel];
    bookInfoView = [[UIView alloc]initWithFrame:CGRectMake(0, yourDtailLabel.endPointY, DeviceWidth, DeviceHeight-offerView.endPointY)];
    
    bookInfoView = [[UIView alloc]initWithFrame:CGRectMake(0, offerView.endPointY, DeviceWidth, DeviceHeight-offerView.endPointY +100)];
    if (iPhone5) {
        bookInfoView = [[UIView alloc]initWithFrame:CGRectMake(0, offerView.endPointY, DeviceWidth, DeviceHeight-offerView.endPointY +200)];
    }
    bookInfoView.backgroundColor = [UIColor defaultColor];
    [_scrollView addSubview:bookInfoView];
//    UIImageView *shadowView1=[[UIImageView alloc]initShadowImageViewWithShadowOriginY:0 andHeight:8];
//    [bookInfoView addSubview:shadowView1];
    
    
    UIView *backDownV = [[UIView alloc] initWithFrame:CGRectMake(0, 10, DeviceWidth, 40)];
    backDownV.backgroundColor = [UIColor defaultColor];
    [bookInfoView addSubview:backDownV];
    
    UILabel *lblDownTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, DeviceWidth - 20, 20)];
    lblDownTitle.text = @"CONTACT INFORMATION";
    lblDownTitle.textColor = [UIColor color999999];
    lblDownTitle.font = [UIFont systemFontOfSize:14];
    [backDownV addSubview:lblDownTitle];
    
    UIImageView *lineImageV = [[UIImageView alloc]initWithFrame:CGRectMake(0, backDownV.bottom, DeviceWidth, 1)];
    lineImageV.backgroundColor = [UIColor colorEDEDED];
    [bookInfoView addSubview:lineImageV];
    
//    NSArray *sexsArr = @[L(@"Mr."),L(@"Ms.")];
//    Label *sexLabel;
//    for (int i = 0; i<sexsArr.count; i++) {
//        sexLabel = [[Label alloc]initWithFrame:CGRectMake(0+DeviceWidth/2*i, backDownV.bottom, (DeviceWidth-2)/2, 47) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13] andTextColor:[UIColor color999999] andTextLines:1];
//        if (i==(gender)) {
//            sexLabel.textColor = [UIColor color3BAF24];
//        }
//        sexLabel.tag = 1000+i;
//        sexLabel.textAlignment = NSTextAlignmentCenter;
//        sexLabel.text = [sexsArr objectAtIndex:i];
//        [bookInfoView addSubview:sexLabel];
//        
//        UIImageView *lineImageView = [[UIImageView alloc]initWithFrame:CGRectMake(sexLabel.frame.size.width, 0, 1, 47)];
//        lineImageView.backgroundColor = [UIColor colorEDEDED];
//        [sexLabel addSubview:lineImageView];
//        
//        
//        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sexSelected:)];
//        [sexLabel addGestureRecognizer:tapGesture];
//    }
//    UIImageView *lineImageView7 = [[UIImageView alloc]initWithFrame:CGRectMake(0, sexLabel.bottom, DeviceWidth, 1)];
//    lineImageView7.backgroundColor = [UIColor colorEDEDED];
//    [bookInfoView addSubview:lineImageView7];
    
    
    NSMutableArray *bookInfoArray;
    IMGUser *user=[IMGUser currentUser];
    if(user.userId!=nil){
        bookInfoArray = [[NSMutableArray alloc]init];
        if(user.firstName!=nil){
            [bookInfoArray addObject:user.firstName];
        }else{
            [bookInfoArray addObject:@""];
        }
        if(user.lastName!=nil){
            [bookInfoArray addObject:user.lastName];
        }else{
            [bookInfoArray addObject:@""];
        }
        if(user.email!=nil){
            [bookInfoArray addObject:user.email];
        }else{
            [bookInfoArray addObject:@""];
        }
        if(user.phone!=nil){
            [bookInfoArray addObject:user.phone];
        }else{
            [bookInfoArray addObject:@""];
        }
        gender = [user.gender intValue];
        [bookInfoArray addObject:@""];
        [bookInfoArray addObject:@""];
         placeholderArray = @[@"First Name",@"Last Name",@"Email",@"Mobile Number",@"Special Request",@"Voucher Code"];
        
    }else{
        bookInfoArray = [[NSMutableArray alloc]initWithArray:@[@"",@"",@"",@"",@"",@"",@""]];
        placeholderArray = @[@"First Name",@"Last Name",@"Email",@"Password",@"Mobile Number",@"Special Request",@"Voucher Code"];
    }

    //
    UILabel *lblFirstName = [[UILabel alloc] initWithFrame:CGRectMake(10, backDownV.bottom, 80, 49)];
    lblFirstName.textColor = [UIColor color333333];
    lblFirstName.textAlignment = NSTextAlignmentLeft;
    lblFirstName.text = @"First Name";
    lblFirstName.font = [UIFont systemFontOfSize:14];
    [bookInfoView addSubview:lblFirstName];
    
    firstNameTextField = [[UITextField alloc]initWithFrame:CGRectMake(lblFirstName.right, backDownV.bottom,DeviceWidth - 100, 49)];
    firstNameTextField.tag = 100;
    firstNameTextField.placeholder = [placeholderArray objectAtIndex:0];
    firstNameTextField.delegate = self;
    firstNameTextField.text = [bookInfoArray objectAtIndex:0];
    firstNameTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    [bookInfoView addSubview:firstNameTextField];
//    UIImageView *lineImageView1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, firstNameTextField.endPointY-1, DeviceWidth, 1)];
//    lineImageView1.backgroundColor = [UIColor colorEDEDED];
//    [bookInfoView addSubview:lineImageView1];
    //
    UILabel *lblLastName = [[UILabel alloc] initWithFrame:CGRectMake(10, lblFirstName.bottom, 80, 49)];
    lblLastName.textColor = [UIColor color333333];
    lblLastName.textAlignment = NSTextAlignmentLeft;
    lblLastName.text = @"Last Name";
    lblLastName.font = [UIFont systemFontOfSize:14];
    [bookInfoView addSubview:lblLastName];
    
    lastNameTextField = [[UITextField alloc]initWithFrame:CGRectMake(lblLastName.right,  lblFirstName.bottom, DeviceWidth - 100, 49)];
    lastNameTextField.tag = 100+1;
    lastNameTextField.placeholder = [placeholderArray objectAtIndex:1];
    lastNameTextField.delegate = self;
    lastNameTextField.text = [bookInfoArray objectAtIndex:1];
    lastNameTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    [bookInfoView addSubview:lastNameTextField];
    
//    UIImageView *lineImageView2 = [[UIImageView alloc]initWithFrame:CGRectMake(0, lastNameTextField.endPointY-1, DeviceWidth, 1)];
//    lineImageView2.backgroundColor = [UIColor colorEDEDED];
//    [bookInfoView addSubview:lineImageView2];
    //
    lblEmail = [[UILabel alloc] initWithFrame:CGRectMake(10, lblLastName.bottom, 80, 49)];
    lblEmail.textColor = [UIColor color333333];
    lblEmail.textAlignment = NSTextAlignmentLeft;
    lblEmail.text = @"Email";
    lblEmail.font = [UIFont systemFontOfSize:14];
    [bookInfoView addSubview:lblEmail];
    
    emailTextField = [[UITextField alloc]initWithFrame:CGRectMake(lblEmail.right, lblLastName.bottom, DeviceWidth-100, 49)];
    emailTextField.tag = 100+2;
    emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    emailTextField.placeholder = [placeholderArray objectAtIndex:2];
    emailTextField.delegate = self;
    emailTextField.text = [bookInfoArray objectAtIndex:2];
    emailTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    [bookInfoView addSubview:emailTextField];
//    UIImageView *lineImageView3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, emailTextField.endPointY-1, DeviceWidth, 1)];
//    lineImageView3.backgroundColor = [UIColor colorEDEDED];
//    [bookInfoView addSubview:lineImageView3];
   //
    if (user.userId==nil) {
        passWordTextField = [[UITextField alloc]initWithFrame:CGRectMake(10, lblEmail.bottom, DeviceWidth-2*LEFTLEFTSET, 49)];
        passWordTextField.tag = 100+3;
        passWordTextField.keyboardType = UIKeyboardTypeEmailAddress;
        passWordTextField.secureTextEntry=YES;
        passWordTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        passWordTextField.placeholder = [placeholderArray objectAtIndex:3];
        passWordTextField.delegate = self;
        passWordTextField.text = [bookInfoArray objectAtIndex:3];
        passWordTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
        [bookInfoView addSubview:passWordTextField];
        UIImageView *lineImageViewPassWord = [[UIImageView alloc]initWithFrame:CGRectMake(0, passWordTextField.endPointY-1, DeviceWidth, 1)];
        lineImageViewPassWord.backgroundColor = [UIColor colorEDEDED];
        [bookInfoView addSubview:lineImageViewPassWord];
        subnum=50;
        num=3;
    }else{
        num=2;
         subnum=0;
    
    }
    //
    lblMobileNumber = [[UILabel alloc] initWithFrame:CGRectMake(10, lblEmail.bottom +subnum, 80, 49)];
    lblMobileNumber.textColor = [UIColor color333333];
    lblMobileNumber.textAlignment = NSTextAlignmentLeft;
    lblMobileNumber.text = @"Phone";
    lblMobileNumber.font = [UIFont systemFontOfSize:14];
    [bookInfoView addSubview:lblMobileNumber];
    
    
    countryCodeTextField = [[UITextField alloc]initWithFrame:CGRectMake(lblMobileNumber.right, lblEmail.bottom +subnum, 30, 49)];
    countryCodeTextField.text = [NSString stringWithFormat:@"+%@",selectCode];
//    countryCodeTextField.backgroundColor = [UIColor greenColor];
    countryCodeTextField.enabled = NO;
//    countryCodeTextField.userInteractionEnabled  = YES;
//    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addCodeNumberPicker)];
//    [countryCodeTextField addGestureRecognizer:tapGesture];
    
    countryCodeTextField.placeholder = L(@"code_v2");
    countryCodeTextField.delegate = self;
    countryCodeTextField.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16];
    countryCodeTextField.textColor = [UIColor color222222];
    countryCodeTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    [bookInfoView addSubview:countryCodeTextField];
    
//    lineImageViewMonbiel = [[UIImageView alloc]initWithFrame:CGRectMake(countryCodeTextField.right, lblEmail.bottom +subnum, 15, 15)];
//    lineImageViewMonbiel.centerY =countryCodeTextField.centerY;
////    lineImageViewMonbiel.backgroundColor = [UIColor colorEDEDED];
//    lineImageViewMonbiel.image = [UIImage imageNamed:@"downarrowChevron"];
//   
//    [bookInfoView addSubview:lineImageViewMonbiel];
//
//    dateControl = [[UIControl alloc]initWithFrame:countryCodeTextField.frame];
//    [bookInfoView addSubview:dateControl];
//    [dateControl addTarget:self action:@selector(addCodeNumberPicker) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    mobileNumberTextField = [[UITextField alloc]initWithFrame:CGRectMake(countryCodeTextField.right, lblEmail.bottom +subnum, DeviceWidth-lineImageViewMonbiel.right - 5, 49)];
    mobileNumberTextField.tag = 100+(1+num);
    mobileNumberTextField.keyboardType = UIKeyboardTypePhonePad;
    mobileNumberTextField.placeholder = [placeholderArray objectAtIndex:(1+num)];
    mobileNumberTextField.delegate = self;
    mobileNumberTextField.text = phoneWithoutCountryCode;
    mobileNumberTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    [bookInfoView addSubview:mobileNumberTextField];
    lineImageView4 = [[UIImageView alloc]initWithFrame:CGRectMake(0, mobileNumberTextField.endPointY-1, DeviceWidth, 1)];
    lineImageView4.backgroundColor = [UIColor colorEDEDED];
    [bookInfoView addSubview:lineImageView4];
    //
    lblSpecialRequest = [[UILabel alloc] initWithFrame:CGRectMake(10, lblMobileNumber.bottom, DeviceWidth - 20, 40)];
    lblSpecialRequest.textColor = [UIColor color999999];
    lblSpecialRequest.textAlignment = NSTextAlignmentLeft;
    lblSpecialRequest.text = @"SPECIAL REQUEST";
    lblSpecialRequest.font = [UIFont systemFontOfSize:14];
    lblSpecialRequest.backgroundColor = [UIColor defaultColor];
    [bookInfoView addSubview:lblSpecialRequest];
    
    specialLine = [[UIView alloc] initWithFrame:CGRectMake(0, lblSpecialRequest.bottom, DeviceWidth, 1)];
    specialLine.backgroundColor = [UIColor colorEDEDED];
    [bookInfoView addSubview:specialLine];
    
    specialRequestTextField = [[UITextView alloc]initWithFrame:CGRectMake(10, specialLine.bottom, DeviceWidth-20, 60)];
    specialRequestTextField.backgroundColor = [UIColor whiteColor];
    specialRequestTextField.tag = 100+(2+num);
//    specialRequestTextField.placeholder = [placeholderArray objectAtIndex:(2+num)];
    specialRequestTextField.delegate = self;
    specialRequestTextField.text = [bookInfoArray objectAtIndex:(2+num)];
//    specialRequestTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    [bookInfoView addSubview:specialRequestTextField];

    specialTVLine = [[UIView alloc] initWithFrame:CGRectMake(0, specialRequestTextField.bottom, DeviceWidth, 1)];
    specialTVLine.backgroundColor = [UIColor colorEDEDED];
    [bookInfoView addSubview:specialTVLine];
    
    lblVoucherCode = [[UILabel alloc] initWithFrame:CGRectMake(10, specialTVLine.bottom, 90, 49)];
    lblVoucherCode.textColor = [UIColor colortextBlue];
    lblVoucherCode.textAlignment = NSTextAlignmentLeft;
    lblVoucherCode.text = @"Use Voucher";
    lblVoucherCode.font = [UIFont systemFontOfSize:14];
    [bookInfoView addSubview:lblVoucherCode];
    
    voucherCodeTextField = [[UITextField alloc]initWithFrame:CGRectMake(lblVoucherCode.right, specialRequestTextField.bottom, DeviceWidth-110, 49)];
    voucherCodeTextField.tag = 100+(3+num);
    voucherCodeTextField.placeholder = [placeholderArray objectAtIndex:(3+num)];
    voucherCodeTextField.delegate = self;
    voucherCodeTextField.text = [bookInfoArray objectAtIndex:(3+num)];
    voucherCodeTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    voucherCodeTextField.font = [UIFont systemFontOfSize:14];
    [bookInfoView addSubview:voucherCodeTextField];
    lineImageView6 = [[UIImageView alloc]initWithFrame:CGRectMake(0, voucherCodeTextField.bottom, DeviceWidth, 1)];
    lineImageView6.backgroundColor = [UIColor colorEDEDED];
    [bookInfoView addSubview:lineImageView6];
    if ([IMGUser currentUser].userId==nil) {
        selectedbtn=[[UIButton alloc]initWithFrame:CGRectMake(10, lblVoucherCode.bottom +20, 25, 25)];
        //    selectedbtn.backgroundColor=[UIColor redColor];
        [selectedbtn addTarget:self action:@selector(selectedAct:) forControlEvents:UIControlEventTouchUpInside];
        [selectedbtn setImage:[UIImage imageNamed:@"checkbox"] forState:UIControlStateNormal];
        [selectedbtn setImage:[UIImage imageNamed:@"checkbox-on"] forState:UIControlStateSelected];
        [bookInfoView addSubview:selectedbtn];
        lable=[[UILabel alloc]initWithFrame:CGRectMake(selectedbtn.endPointX+6,lblVoucherCode.bottom +20, DeviceWidth-30, 20)];
        lable.text=@"Register me using my details above";
        lable.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16];
        lable.textColor = [UIColor color222222];
        [bookInfoView addSubview:lable];
        subnum=subnum+28;
    }
    submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    submitButton.frame = CGRectMake(30, lineImageView6.bottom+subnum +20, DeviceWidth - 60, 40);
    [submitButton setBackgroundImage:[UIImage imageNamed:@"bg_bt_book_confirm"] forState:UIControlStateNormal];
    [submitButton setTitle:L(@"CONFIRM") forState:UIControlStateNormal];
    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    submitButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [submitButton addTarget:self action:@selector(confirmButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//    submitButton.layer.borderWidth = 1;
//    submitButton.layer.borderColor = [UIColor colorWithRed:241/255.0f green:15/255.0f blue:51/255.0f alpha:1.0].CGColor;
//    submitButton.layer.cornerRadius = 20;
//    submitButton.layer.masksToBounds = YES;
    submitButton.centerX = bookInfoView.centerX;
    [bookInfoView addSubview:submitButton];
    _scrollView.contentSize = CGSizeMake(DeviceWidth, DeviceHeight+6+navigationHeight+60);
    if ([UIDevice isIphone5]) {
        _scrollView.contentSize = CGSizeMake(DeviceWidth, DeviceHeight+6+navigationHeight+60 +100);
        
    }
    bookInfoView.backgroundColor = [UIColor whiteColor];
    
   
    
   
}
-(void)selectedAct:(UIButton*)btn{
    if (!btn.selected) {
        num=2;
        subnum=0;
        passWordTextField.hidden=YES;
    }else{
        num=3;
        subnum=50;
    
        passWordTextField.hidden=NO;
    }
    btn.selected=!btn.selected;

    
    lblMobileNumber.frame = CGRectMake(10, lblEmail.bottom +subnum, 80, 49);
    countryCodeTextField.frame=CGRectMake(lblMobileNumber.right,lblEmail.bottom +subnum, 30, 49);
    lineImageViewMonbiel.frame=CGRectMake(countryCodeTextField.right, lblEmail.bottom +subnum, 15, 15);
    lineImageViewMonbiel.centerY =countryCodeTextField.centerY;
    mobileNumberTextField.frame=CGRectMake(lineImageViewMonbiel.right +5, lblEmail.bottom +subnum, DeviceWidth-LEFTLEFTSET-80, 49);
    
    lineImageView4.frame=CGRectMake(0, mobileNumberTextField.endPointY-1, DeviceWidth, 1);
    
    lblSpecialRequest.frame = CGRectMake(10, lblMobileNumber.bottom, DeviceWidth - 20, 40);
    specialLine.frame = CGRectMake(0, lblSpecialRequest.bottom, DeviceWidth, 1);
    specialRequestTextField.frame=CGRectMake(10, specialLine.bottom, DeviceWidth-2*LEFTLEFTSET, 49);
    specialTVLine.frame = CGRectMake(0, specialRequestTextField.bottom, DeviceWidth, 1);
    
    lblVoucherCode.frame = CGRectMake(10, specialTVLine.bottom, 90, 49);
    voucherCodeTextField.frame=CGRectMake(lblVoucherCode.right, specialTVLine.bottom, DeviceWidth-2*LEFTLEFTSET, 49);
    lineImageView5.frame=CGRectMake(0, lblVoucherCode.endPointY-1, DeviceWidth, 1);
    lineImageView6.frame=CGRectMake(0, lblVoucherCode.endPointY-1, DeviceWidth, 1);
    
    selectedbtn.frame=CGRectMake(10, lblVoucherCode.bottom +20, 25, 25);
    lable.frame=CGRectMake(selectedbtn.endPointX+6,lblVoucherCode.bottom +20, DeviceWidth-30, 20);
    submitButton.frame= CGRectMake(0,selectedbtn.bottom +20, DeviceWidth/2, 40);
    submitButton.centerX = bookInfoView.centerX;
    
}
-(void)initSignInView:(UIView *)offerView{
    signInView = [[UIView alloc]initWithFrame:CGRectMake(0, offerView.endPointY, DeviceWidth, DeviceHeight-offerView.endPointY+[UIDevice heightDifference] +140)];
    signInView.backgroundColor = [UIColor whiteColor];
    [_scrollView addSubview:signInView];
//    Label *signInLabel = [[Label alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 55) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16] andTextColor:[UIColor blackColor] andTextLines:1];
//    signInLabel.text = L(@"Sign in");
//    signInLabel.textAlignment = NSTextAlignmentCenter;
//    [signInView addSubview:signInLabel];
    
    
    UIButton*fbLoginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    fbLoginButton.frame = CGRectMake(2*17, 20, DeviceWidth-4*17, 44);
    [fbLoginButton setBackgroundImage:[UIImage imageNamed:@"BookFlowFacebookBtn"] forState:UIControlStateNormal];
    [fbLoginButton addTarget:self action:@selector(facebookLogin:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *fbLoginLabel = [[UILabel alloc]initWithFrame:CGRectMake(fbLoginButton.frame.size.width/5.0, (fbLoginButton.frame.size.height - 20)/2.0, fbLoginButton.frame.size.width/5.0*4, 20)];
    fbLoginLabel.text = L(@"Continue with Facebook");
    fbLoginLabel.backgroundColor = [UIColor clearColor];
    fbLoginLabel.font =[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:14*[AppDelegate ShareApp].autoSizeScaleY];
    fbLoginLabel.textColor = [UIColor whiteColor];
    [fbLoginButton addSubview:fbLoginLabel];
    [signInView addSubview:fbLoginButton];
    
    UIButton*googleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    googleButton.frame = CGRectMake(2*17,44+40, DeviceWidth-4*17, 44);
    UIImage *image;
    if ([UIDevice isIphone6Plus]) {
        image=[UIImage imageNamed:@"BookFlowGoogleBtn6P"];

    }else{
        image=[UIImage imageNamed:@"BookFlowGoogleBtn"];
    }
    [googleButton setBackgroundImage:image forState:UIControlStateNormal];
    [googleButton addTarget:self action:@selector(googleLogin:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *gooleLabel = [[UILabel alloc]initWithFrame:CGRectMake(googleButton.frame.size.width/5.0, (googleButton.frame.size.height - 20)/2.0, googleButton.frame.size.width/5.0*4, 20)];
    gooleLabel.text = L(@"Continue with Google");
    gooleLabel.backgroundColor = [UIColor clearColor];
    gooleLabel.font =[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:14*[AppDelegate ShareApp].autoSizeScaleY];
    gooleLabel.textColor = [UIColor colorWithRed:150/255.0 green:150/255.0 blue:150/255.0 alpha:1];
    [googleButton addSubview:gooleLabel];
    [signInView addSubview:googleButton];

    
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(34, 54*3+10, (DeviceWidth-168)/2, 1)];
    lineView.backgroundColor = [UIColor colorWithRed:150/255.0 green:150/255.0 blue:150/255.0 alpha:1];
    [signInView addSubview:lineView];
    NSString *promptStr = @"or sign in";
    CGSize expectSize1 = [promptStr sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14] constrainedToSize:CGSizeMake(DeviceWidth-8*LEFTLEFTSET, 200)];
    Label *promptLabel = [[Label alloc]initWithFrame:CGRectMake(lineView.endPointX,54*3, 100, expectSize1.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14] andTextColor:[UIColor color333333] andTextLines:1];
    promptLabel.text = promptStr;
    [promptLabel setTextColor:[UIColor colorWithRed:150/255.0 green:150/255.0 blue:150/255.0 alpha:1]];
    promptLabel.textAlignment = NSTextAlignmentCenter;
    [signInView addSubview:promptLabel];
    UIView *lineViewR = [[UIView alloc] initWithFrame:CGRectMake(promptLabel.endPointX, 54*3+10, (DeviceWidth-168)/2, 1)];
    lineViewR.backgroundColor = [UIColor colorWithRed:150/255.0 green:150/255.0 blue:150/255.0 alpha:1];
    [signInView addSubview:lineViewR];
//    UIImageView *emailImageView = [[UIImageView alloc]initWithFrame:CGRectMake(2*17, promptLabel.endPointY+20, DeviceWidth-4*17, 44)];
//    emailImageView.image = [UIImage imageNamed:@"BookFlowEmailBtn"];
    UIImageView *emailimageView = [[UIImageView alloc] initWithFrame:CGRectMake(34, promptLabel.endPointY+20, DeviceWidth-4*17, 44)];
    emailimageView.userInteractionEnabled = YES;
    emailimageView.image = [UIImage imageNamed:@"BookTextFiled"];
    [signInView addSubview:emailimageView];
    emailAccountTextField = [[UITextField alloc] initWithFrame:CGRectMake(5, 0, DeviceWidth-4*17, 44)];
    emailAccountTextField.placeholder = @"Email";
    emailAccountTextField.delegate = self;
    UIImageView *passwordImageView = [[UIImageView alloc] initWithFrame:CGRectMake(34, emailimageView.endPointY+20, DeviceWidth-4*17, 44)];
    passwordImageView.image = [UIImage imageNamed:@"BookTextFiled"];
    passwordImageView.userInteractionEnabled = YES;
    [signInView addSubview:passwordImageView];
    passwordTextField = [[UITextField alloc] initWithFrame:CGRectMake(5, 0, DeviceWidth-4*17, 44)];
    passwordTextField.placeholder = @"Password";
    passwordTextField.keyboardType = UIKeyboardTypeEmailAddress;
    passwordTextField.secureTextEntry=YES;
    passwordTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    passwordTextField.delegate = self;
    UIButton *signUpBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    signUpBtn.frame = CGRectMake(34, passwordImageView.endPointY+20, DeviceWidth-2*34, 44);
    signUpBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    signUpBtn.clipsToBounds = YES;
    signUpBtn.layer.cornerRadius = 10;
    [signUpBtn setBackgroundColor:[UIColor colorC2060A]];
    [signUpBtn setTitle:L(@"Sign In") forState:UIControlStateNormal];
    signUpBtn.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:15];
    [signUpBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [signUpBtn addTarget:self action:@selector(regButtonClick:) forControlEvents:UIControlEventTouchUpInside];

    [emailimageView addSubview:emailAccountTextField];
    [passwordImageView addSubview:passwordTextField];
    [signInView addSubview:signUpBtn];
    UILabel *noticeLabel = [[UILabel alloc] initWithFrame:CGRectMake(34, signUpBtn.endPointY+10, DeviceWidth-68, 40)];
    noticeLabel.text = @"We'll never post without your permission. Promise!";
    if ([UIDevice isIphone6Plus]) {
        noticeLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14];

    }
    if ([UIDevice isIphone6]) {
        noticeLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12.9];

    }
    if ([UIDevice isIphone5]||[UIDevice isIphone4Now]) {
        noticeLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:10.5];

    }
    [noticeLabel setTextColor:[UIColor colorWithRed:150/255.0 green:150/255.0 blue:150/255.0 alpha:1]];
    noticeLabel.numberOfLines = 2;
    [signInView addSubview:noticeLabel];

    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, DeviceHeight+20-60, DeviceWidth, 60)];
    bottomView.backgroundColor = [UIColor colorEEEEEE];
    UIButton *signUpButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 20, DeviceWidth, 20)];
    NSMutableAttributedString *signUpAttributeString = [[NSMutableAttributedString alloc] initWithString:@"or sign up via email"];
    [signUpAttributeString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14],NSForegroundColorAttributeName:[UIColor colorRed]} range:NSMakeRange(11,9)];
    [signUpButton setAttributedTitle:signUpAttributeString forState:UIControlStateNormal];
    [signUpButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [signUpButton addTarget:self action:@selector(manualEntry:) forControlEvents:UIControlEventTouchUpInside];
    signUpButton.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14];
    [bottomView addSubview:signUpButton];
    [[AppDelegate ShareApp].window addSubview:bottomView];
}
-(void)regButtonClick:(id)sender{
    UIButton *tempBtn = (UIButton*)sender;
    tempBtn.enabled = NO;
    if ([emailAccountTextField.text isBlankString]) {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:L(@"Email address cannot be empty.") message:nil];
//        [alert addButtonWithTitle:@"OK" handler:^(){
//            
//        }];
//        [alert show];
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:L(@"Email address cannot be empty.") message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alerView show];

        tempBtn.enabled = YES;
        return ;
    }
    
    
    if ([passwordTextField.text isBlankString]&&!selectedbtn.selected&&[IMGUser currentUser].userId==nil) {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:L(@"Password cannot be empty.") message:nil];
//        [alert addButtonWithTitle:@"OK" handler:^(){
//            
//        }];
//        [alert show];
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:L(@"Password cannot be empty.") message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alerView show];

        tempBtn.enabled = YES;
        return ;
    }
    if (passwordTextField.text.length >18 || passwordTextField.text.length <6) {
        //        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:L(@"Password cannot be empty.") message:nil];
        //        [alert addButtonWithTitle:@"OK" handler:^(){
        //
        //        }];
        //        [alert show];
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:L(@"Password should be between 6~18 characters.") message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alerView show];
        
        tempBtn.enabled = YES;
        return ;
    }
    
    if (![emailAccountTextField.text isValidateEmail]) {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:L(@"The format of the email address is invalid.") message:nil];
//        [alert addButtonWithTitle:@"OK" handler:^(){
//            
//        }];
//        [alert show];
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:L(@"The format of the email address is invalid.") message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alerView show];

        tempBtn.enabled = YES;
        return ;
    }
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]initWithCapacity:0];
    [parameters setObject:emailAccountTextField.text forKey:@"email"];
    [parameters setObject:[passwordTextField.text MD5String] forKey:@"password"];
    [parameters setObject:@"self" forKey:@"loginwith"];
    [LoginParamter sharedParameter].loginwith=USER_LOGINTYPE_WITHSELF;
    UserDataHandler *userDataHandler = [[UserDataHandler alloc] init];
    
    [userDataHandler emailLogin:parameters withError:nil withDelegateBlock:^{
        [bottomView removeFromSuperview];
        
        [AppDelegate ShareApp].userLogined=YES;
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
        [[AppDelegate ShareApp] requestToShowNotificationCount];
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"loginNotification" object:nil];
        tempBtn.enabled = YES;
        
    } failBlock:^(NSString *errorMsg) {
        tempBtn.enabled = YES;
    }];


}
-(void)manualEntry:(id)sender{
    UIButton *tempBtn = (UIButton*)sender;
    fromManualEntry=YES;
    [signInView setHidden:YES];
    [bookInfoView setHidden:NO];
    [tempBtn.superview setHidden:YES];
}

- (void)twitterLogin:(UIButton*)sender{
    sender.userInteractionEnabled=NO;
    [[NSUserDefaults standardUserDefaults]setObject:@"login" forKey:TWITTERNOTIFICATIONTYPE];    
    [SHK setRootViewController:self];
    SHKTwitter *twitter = [[SHKTwitter alloc] initWithNextDelegate:self];
    [twitter authorize];
    [self performSelector:@selector(senderEnabel:) withObject:sender afterDelay:5.0f];
    
    
}
-(void)googleLogin:(id)sender{
    [[GIDSignIn sharedInstance]signIn];
    [bottomView removeFromSuperview];
    [self performSelector:@selector(senderEnabel:) withObject:nil afterDelay:5.0f];
    
}
-(void)senderEnabel:(UIButton*)sender{
    sender.userInteractionEnabled = YES;
    
}


-(void)loginWithTwitterInQraved:(NSNotification *)notification{
    NSDictionary *twitterResult = [notification.userInfo objectForKey:@"twitterResult"];
    NSError *error = (NSError *)[notification.userInfo objectForKey:@"error"];
    UserDataHandler *userDataController = [[UserDataHandler alloc] init];
    [[LoadingView sharedLoadingView]startLoading];
    [userDataController twitterLogin:twitterResult withError:error withDelegateBlock: ^(){
        [[LoadingView sharedLoadingView] stopLoading];
        NSDictionary *dataDic = [[NSDictionary alloc] initWithDictionary:[userDataController getParameters]];
        if ([userDataController isSignUp])
        {
            [self loginSuccessful];
        }
        else
        {
            FBSignUpViewController *fbsuvc = [[FBSignUpViewController alloc] init];
            fbsuvc.isFromBooking = YES;
            fbsuvc.dataDic = dataDic;
            fbsuvc.amplitudeType = @"Twitter";
            fbsuvc.isFromBooking=YES;
            [self.navigationController pushViewController:fbsuvc animated:YES];
        }
    }failedBlock:^{
        [[LoadingView sharedLoadingView] stopLoading];
        [self enterEmail];
    }];
}
-(void)facebookLogin:(id)sender{
    [bottomView removeFromSuperview];

    NSArray *permissions = @[@"public_profile", @"email"];
    UserDataHandler *userDataHandler = [[UserDataHandler alloc] init];
    [userDataHandler faceBookLogin:permissions delegateWithBlock:^{
        [[LoadingView sharedLoadingView] stopLoading];
        NSDictionary *dataDic = [[NSDictionary alloc] initWithDictionary:[userDataHandler getParameters]];
        if ([userDataHandler isSignUp])
        {
            [self loginSuccessful];
        }
        else
        {
            [self loginWithFacebook:dataDic];
        }
    } failedBlock:^{
         [self enterEmail];
    }];
//    [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:YES fromViewController:self completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
//        UserDataHandler *userDataHandler = [[UserDataHandler alloc] init];
//        [userDataHandler faceBookLogin:session state:state error:error delegateWithBlock: ^() {
//            [[LoadingView sharedLoadingView] stopLoading];
//            NSDictionary *dataDic = [[NSDictionary alloc] initWithDictionary:[userDataHandler getParameters]];
//            if ([userDataHandler isSignUp])
//            {
//                
//                [self loginSuccessful];
//            
//            }
//            else
//            {
//                [self loginWithFacebook:dataDic];
//
////                FBSignUpViewController *fbsuvc = [[FBSignUpViewController alloc] init];
////                fbsuvc.dataDic = dataDic;
////                fbsuvc.isFromBooking = YES;
////                [self.navigationController pushViewController:fbsuvc animated:YES];
//            }
//        }failedBlock:^{
//            [self enterEmail];
//        }];
//    }];
}
-(void)loginWithFacebook:(NSDictionary*)dataDict{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:[dataDict objectForKey:@"first_name"] forKey:@"firstName"];
    [parameters setObject:[dataDict objectForKey:@"last_name"] forKey:@"lastName"];
    if ([[dataDict allKeys] containsObject:@"email"]) {
        [parameters setObject:[dataDict objectForKey:@"email"] forKey:@"email"];
        
    }else{
        FBSignUpViewController *fbsuvc = [[FBSignUpViewController alloc] init];
        fbsuvc.dataDic = dataDict;
        fbsuvc.isFromBooking=YES;
        fbsuvc.amplitudeType = @"Facebook";
        [self presentViewController:fbsuvc animated:YES completion:^{
        }];
//        [self.navigationController pushViewController:fbsuvc animated:YES];
        return;
        
    }
    [parameters setObject:@"signupwithself" forKey:@"loginwith"];
    [parameters setObject:@"1" forKey:@"sourceType"];
    [parameters setObject:@"4" forKey:@"productSource"];
    [parameters setObject:[dataDict objectForKey:@"thirdPartyType"] forKey:@"thirdPartyType"];
    [parameters setObject:[dataDict objectForKey:@"thirdPartyId"] forKey:@"thirdPartyId"];
    [parameters setObject:[dataDict objectForKey:@"picture"] forKey:@"picture"];
    
    [LoginParamter sharedParameter].loginwith=USER_LOGINTYPE_WITHSELF;
    
    UserDataHandler *userDataHandler=[[UserDataHandler alloc]init];
    [userDataHandler faceBookSignUp:parameters withBlock:^() {
        [[LoadingView sharedLoadingView] stopLoading];
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"loginSuccessful" object:nil];
        [AppDelegate ShareApp].userLogined=YES;
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
        if ([AppDelegate ShareApp].userLogined && [IMGUser currentUser].phone) {
            
            phoneWithoutCountryCode=[IMGUser currentUser].phone;
            NSRange range =[[IMGUser currentUser].phone rangeOfString:@"-"];
            if(range.location!=NSNotFound){
                selectCode=[[IMGUser currentUser].phone substringToIndex:range.location];
                NSRange range2 =[selectCode rangeOfString:@"+"];
                if (range2.location != NSNotFound) {
                    selectCode = [selectCode substringFromIndex:range2.location+1];
                }
                
                phoneWithoutCountryCode = [[IMGUser currentUser].phone substringFromIndex:range.location+1];
            }else{
                range =[[IMGUser currentUser].phone rangeOfString:@"+"];
                if(range.location!=NSNotFound){
                    phoneWithoutCountryCode = [[IMGUser currentUser].phone substringFromIndex:range.location+1];
                }
                
            }
            
        }
        self.view.backgroundColor = [UIColor whiteColor];
        
        [[LoadingView sharedLoadingView] startLoading];
        [DetailDataHandler getBookInfoFromService:restaurant andBlock:^{
            [self loadMainView];
            [self addNotification];
            [[LoadingView sharedLoadingView] stopLoading];
        }];
        } failedBlock:^(NSString *errorMsg) {
            
        [[Amplitude instance] logEvent:@"SU - Sign Up Failed" withEventProperties:@{@"Reason":errorMsg,@"Sign in type":@"Facebook"}];
        [self enterEmail];
    }];
    
}

-(void)loginSuccessful {
    [AppDelegate ShareApp].userLogined=YES;
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
    [[AppDelegate ShareApp] requestToShowNotificationCount];
    [self performSelector:@selector(login) withObject:nil afterDelay:0.5];
}
-(void)login
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loginNotification" object:nil];
}
-(void)enterEmail{
    EnterEmailViewController *enterEmail=[[EnterEmailViewController alloc]init];
    enterEmail.delegate=self;
    [self.navigationController pushViewController:enterEmail animated:YES];
}
-(void)sexSelected:(UITapGestureRecognizer*)gesture
{
    NSInteger tag = gesture.view.tag;
    Label *sexLabel = (id)[self.view viewWithTag:tag];
    switch (tag) {
        case 1000:
        {
            sexLabel.textColor = [UIColor color3BAF24];
            Label *sexLabel1 = (id)[self.view viewWithTag:tag+1];
            Label *sexLabel2 = (id)[self.view viewWithTag:tag+2];
            sexLabel1.textColor = [UIColor color999999];
            sexLabel2.textColor = [UIColor color999999];
            gender=1;
        }
            break;
        case 1001:
        {
            sexLabel.textColor = [UIColor color3BAF24];
            Label *sexLabel1 = (id)[self.view viewWithTag:tag-1];
            Label *sexLabel2 = (id)[self.view viewWithTag:tag+1];
            sexLabel1.textColor = [UIColor color999999];
            sexLabel2.textColor = [UIColor color999999];
            gender=2;
        }
            break;
        case 1002:
        {
            sexLabel.textColor = [UIColor color3BAF24];
            Label *sexLabel1 = (id)[self.view viewWithTag:tag-1];
            Label *sexLabel2 = (id)[self.view viewWithTag:tag-2];
            sexLabel1.textColor = [UIColor color999999];
            sexLabel2.textColor = [UIColor color999999];
            gender=3;
        }
            break;
        default:
            break;
    }
}


- (void)emailLogin:(id)sender {
    [bottomView removeFromSuperview];
    EmailLoginViewController *emailLoginViewController = [[EmailLoginViewController alloc]init];
    emailLoginViewController.isfromBookingInfo = YES;
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:emailLoginViewController] animated:YES completion:nil];
}


-(void)confirmButtonClicked:(id)sender
{
    [[Amplitude instance] logEvent:@"CL - Edit Selection in Booking Confirmation Page" withEventProperties:@{@"Location":@"Booking Confirmation Page"}];
    
    [[Amplitude instance] logEvent:@"CL - Confirm Button in Booking Confirmation Page" withEventProperties:@{@"Location":@"Booking Confirmation Page"}];
    submitButton.enabled = NO;
    [firstNameTextField resignFirstResponder];
    [lastNameTextField resignFirstResponder];
    [emailTextField resignFirstResponder];
    [mobileNumberTextField resignFirstResponder];
    [specialRequestTextField resignFirstResponder];
    [voucherCodeTextField resignFirstResponder];
    [passWordTextField resignFirstResponder];
    
    _scrollView.contentSize = CGSizeMake(DeviceWidth, DeviceHeight+6+navigationHeight );
    if ([UIDevice isIphone5]) {
        _scrollView.contentSize = CGSizeMake(DeviceWidth, DeviceHeight+6+navigationHeight +100);
    }
    isViewUp = NO;
    submitButton.enabled=NO;
    
    if (mobileNumberTextField.text.length >13 || mobileNumberTextField.text.length <8) {
        //        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:L(@"Password cannot be empty.") message:nil];
        //        [alert addButtonWithTitle:@"OK" handler:^(){
        //
        //        }];
        //        [alert show];
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:L(@"Phone number should be between 8~13 digital.") message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alerView show];
        
        submitButton.enabled = YES;
        return ;
    }
    
    if ([firstNameTextField.text isBlankString]) {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:L(@"First name cannot be empty.") message:nil];
//        [alert addButtonWithTitle:@"OK" handler:^(){
//            
//        }];
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:L(@"First name cannot be empty.") message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alerView show];

//        [alert show];
        submitButton.enabled = YES;
        return ;
    }
    if ([lastNameTextField.text isBlankString]) {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:L(@"Last name cannot be empty.") message:nil];
//        [alert addButtonWithTitle:@"OK" handler:^(){
//            
//        }];
//        [alert show];
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:L(@"Last name cannot be empty.") message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alerView show];

        submitButton.enabled = YES;
        return ;
    }
    if ([emailTextField.text isBlankString]) {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:L(@"Email address cannot be empty.") message:nil];
//        [alert addButtonWithTitle:@"OK" handler:^(){
//            
//        }];
//        [alert show];
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:L(@"Email address cannot be empty.") message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alerView show];

        submitButton.enabled = YES;
        return ;
    }
    if ([passWordTextField.text isBlankString]&&!selectedbtn.selected&&[IMGUser currentUser].userId==nil) {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:L(@"Password cannot be empty.") message:nil];
//        [alert addButtonWithTitle:@"OK" handler:^(){
//            
//        }];
//        [alert show];
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:L(@"Password cannot be empty.") message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alerView show];

        submitButton.enabled = YES;
        return ;
    }

    if (![emailTextField.text isValidateEmail]) {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:L(@"The format of the email address is invalid.") message:nil];
//        [alert addButtonWithTitle:@"OK" handler:^(){
//            
//        }];
//        [alert show];
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:L(@"The format of the email address is invalid.") message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alerView show];

        submitButton.enabled = YES;
        return ;
    }
    
    if (![[[countryCodeTextField.text stringByAppendingString:@"-"] stringByAppendingString:mobileNumberTextField.text] isValidateInternationPhone]) {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:L(@"Incorrect phone number format.") message:nil];
//        [alert addButtonWithTitle:@"OK" handler:^(){
//            
//        }];
//        [alert show];
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:L(@"Incorrect phone number format.")message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alerView show];

        submitButton.enabled = YES;
        return ;
    }
    if (![voucherCodeTextField.text isBlankString])
    {
        NSString *voucherCode=voucherCodeTextField.text;
        
        NSMutableDictionary *parameters=[NSMutableDictionary dictionary];
        if([IMGUser currentUser].userId!=nil){
            [parameters setObject:[IMGUser currentUser].userId forKey:@"userID"];
        }
        [parameters setObject:restaurant.restaurantId forKey:@"restaurantID"];
        [parameters setObject:voucherCode forKey:@"voucherCode"];
       
        [[LoadingView sharedLoadingView] startLoading];
        [BookHandler judgeVoucherCodeWithParams:parameters andBlock:^(NSString *status, NSString *exceptionType) {
            if([status isEqualToString:@"failed"])
            {
                NSString *errorString=@"";
                if([exceptionType isEqual:[NSNumber numberWithInt:1]])
                {
                    errorString=L(@"Voucher code expired.");
                    [[Amplitude instance] logEvent:@"TR - Reservation Failed" withEventProperties:@{@"Reson":errorString,@"Restaurant_ID":restaurant.restaurantId,@"Location":@"Reservation detail page"}];
                }
                else if([exceptionType isEqual:[NSNumber numberWithInt:2]])
                {
                    errorString=L(@"Voucher code has been used");
                    [[Amplitude instance] logEvent:@"TR - Reservation Failed" withEventProperties:@{@"Reson":errorString,@"Restaurant_ID":restaurant.restaurantId,@"Location":@"Reservation detail page"}];
                }
                else if([exceptionType isEqual:[NSNumber numberWithInt:3]])
                {
                    errorString=L(@"Invalid voucher code.");
                    [[Amplitude instance] logEvent:@"TR - Reservation Failed" withEventProperties:@{@"Reson":errorString,@"Restaurant_ID":restaurant.restaurantId,@"Location":@"Reservation detail page"}];
                }
                submitButton.enabled = YES;
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: [NSString stringWithFormat:@"Error"]
                                                                    message: errorString
                                                                   delegate: nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                [alertView show];
                
            }
            
            else
            {
                reservation = [[IMGReservation alloc]init];
                reservation.firstName = firstNameTextField.text;
                reservation.lastName = lastNameTextField.text;
                reservation.fullName = [NSString stringWithFormat:@"%@ %@",firstNameTextField.text,lastNameTextField.text];
                
                reservation.email = emailTextField.text;
                reservation.phone = mobileNumberTextField.text;
                reservation.specialRequest = specialRequestTextField.text;
                reservation.voucherCode = voucherCodeTextField.text;
                reservation.gender = [NSNumber numberWithInt:gender];
                
                if(restaurantOffer!=nil){
                    reservation.off = restaurantOffer.offerSlotMaxDisc;
                    reservation.offerId = restaurantOffer.offerId;
                    reservation.offerName = restaurantOffer.offerTitle;
                }
                reservation.restaurantId = restaurant.restaurantId;
                reservation.restaurantName = restaurant.title;
                reservation.restaurantSeoKeywords = restaurant.seoKeyword;
                reservation.restaurantImage = [[restaurant.imageUrl componentsSeparatedByString:@","] firstObject];
                reservation.bookPax = self.pax;
                reservation.bookDate = self.bookDate.getYYYY_MM_DDCenterLineFormatString;
                reservation.bookTime = self.bookTime;
                reservation.title = restaurant.title;
                reservation.address = [restaurant address];
                
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:voucherCode forKey:@"voucherCode"];
                //                [MixpanelHelper trackApplyVoucharCodeWithRestaurant:restaurant andReservation:reservation];
                [self requestBook];
            }
            
            [[LoadingView sharedLoadingView] stopLoading];
        } andErrorBlock:^(NSString *errorStr) {
            [[Amplitude instance] logEvent:@"TR - Reservation Failed" withEventProperties:@{@" Reson":errorStr,@"Restaurant_ID":restaurant.restaurantId,@"Location":@"Reservation detail page"}];
            
            [[LoadingView sharedLoadingView] stopLoading];
            submitButton.enabled = YES;
        }];

    }
    else
    {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:@"" forKey:@"voucherCode"];
        
        reservation = [[IMGReservation alloc]init];
        reservation.firstName = firstNameTextField.text;
        reservation.lastName = lastNameTextField.text;
        reservation.fullName = [NSString stringWithFormat:@"%@ %@",firstNameTextField.text,lastNameTextField.text];
        
        reservation.email = emailTextField.text;
        reservation.phone = [[countryCodeTextField.text stringByAppendingString:@"-"] stringByAppendingString:mobileNumberTextField.text];
        reservation.specialRequest = specialRequestTextField.text;
        reservation.voucherCode = voucherCodeTextField.text;
        reservation.gender = [NSNumber numberWithInt:gender];
        
        if(restaurantOffer!=nil){
            reservation.off = restaurantOffer.offerSlotMaxDisc;
            reservation.offerId = restaurantOffer.offerId;
            reservation.offerName = restaurantOffer.offerTitle;
        }
        reservation.restaurantId = restaurant.restaurantId;
        reservation.restaurantName = restaurant.title;
        reservation.restaurantSeoKeywords = restaurant.seoKeyword;
        reservation.restaurantImage = [[restaurant.imageUrl componentsSeparatedByString:@","] firstObject];
        reservation.bookPax = self.pax;
        reservation.bookDate = self.bookDate.getYYYY_MM_DDCenterLineFormatString;
        reservation.bookTime = self.bookTime;
        reservation.title = restaurant.title;
        reservation.address = [restaurant address];
        [self requestBook];
    }
    
    
}

-(void)requestBook{
    IMGUser *user=[IMGUser currentUser];
    
    NSString *urlPath = [NSString stringWithFormat:@"book"];
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]initWithCapacity:0];
    if(reservation.off!=nil){
        [parameters setObject:reservation.off forKey:@"off"];
    }else{
        [parameters setObject:[NSNumber numberWithInt:0] forKey:@"off"];
    }
    if(reservation.gender!=nil){
        [parameters setObject:reservation.gender forKey:@"gender"];
    }else{
        [parameters setObject:[NSNumber numberWithInt:0] forKey:@"gender"];
    }
    [parameters setObject:reservation.restaurantId forKey:@"restaurantID"];
    [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    
    if(fromManualEntry==NO){
        if(user!=nil && user.userId!=nil){
            [parameters setObject:user.userId forKey:@"userID"];
            reservation.userId=user.userId;
        }
        if(user!=nil && user.token!=nil){
            [parameters setObject:user.token forKey:@"t"];
        }
    }else{
        [parameters setObject:[NSNumber numberWithInt:0] forKey:@"userID"];
        [parameters setObject:[NSNumber numberWithInt:1] forKey:@"doRegister"];
    }
    if (selectedbtn.selected) {
        [parameters setObject:[NSNumber numberWithInt:0] forKey:@"doRegister"];
    }
    [parameters setObject:reservation.bookPax forKey:@"peopleCount"];
    [parameters setObject:reservation.bookDate forKey:@"bookDate"];
    [parameters setObject:reservation.bookTime forKey:@"bookTime"];
    [parameters setObject:reservation.fullName forKey:@"fullname"];
    if(reservation.phone!=nil){
        [parameters setObject:reservation.phone forKey:@"phone"];
    }
    if ([IMGUser currentUser].userId==nil) {
        [parameters setObject:passWordTextField.text forKey:@"password"];
    }
    [parameters setObject:reservation.email forKey:@"email"];
    if (reservation.offerId!=nil) {
        [parameters setObject:reservation.offerId forKey:@"offerId"];
    }
    
    if(reservation.voucherCode!=nil)
    {
        [parameters setObject:reservation.voucherCode forKey:@"voucherCode"];
    }
    
    if (reservation.specialRequest) {
        [parameters setObject:reservation.specialRequest forKey:@"remark"];
    }
    
    [[LoadingView sharedLoadingView] startLoading];
    NSDictionary *dic;
    if ([parameters objectForKey:@"offId"]) {
        dic= @{@"offer":@"withOffer"};
    }else{
        dic = @{@"offer":@"withoutOffer"};
    }

    [[Amplitude instance] logEvent:@"CL - Book Now Initiate" withEventProperties:dic];
    [BookHandler confirmBookingInfomationWithParams:parameters andBlock:^(id responseObject) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadJourneyBook" object:nil userInfo:nil];
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:reservation.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:@"Reservation detail page" forKey:@"Location"];
        [[Amplitude instance] logEvent:@"TR - Reservation Succeed" withEventProperties:eventProperties];
        [[AppsFlyerTracker sharedTracker] trackEvent:@"TR - Reservation Succeed" withValues:eventProperties];
        
        
        if ([IMGUser currentUser].userId==nil&&!passWordTextField.hidden) {
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc]initWithCapacity:0];
            [parameters setObject:reservation.email forKey:@"email"];
            [parameters setObject:[passWordTextField.text MD5String] forKey:@"password"];
            [parameters setObject:@"self" forKey:@"loginwith"];
            [LoginParamter sharedParameter].loginwith=[NSString stringWithFormat:@"self"];
            UserDataHandler *userDataHandler = [[UserDataHandler alloc] init];
            
            [userDataHandler emailLogin:parameters withError:nil withDelegateBlock:^{
                [self loginSuccessful];
            }failBlock:^(NSString *errorMsg) {
                
            }];
            
        }

        [[Amplitude instance] logEvent:@"CL - Book Now Succeed" withEventProperties:dic];
        [self serverBookOk:responseObject];
        [[LoadingView sharedLoadingView]stopLoading];
        submitButton.enabled = YES;

    }];
//    [[IMGNetWork sharedManager]POST:urlPath parameters:parameters  progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
//
//        NSString *exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
//        if([exceptionMsg isLogout]){
//            [[LoadingView sharedLoadingView] stopLoading];
//            [[IMGUser currentUser]logOut];
//            [[AppDelegate ShareApp] goToLoginController];
//            return;
//        }
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadJourneyBook" object:nil userInfo:nil];
//        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
//        [eventProperties setValue:reservation.restaurantId forKey:@"Restaurant_ID"];
//        [eventProperties setValue:@"Reservation detail page" forKey:@"Location"];
//        [[Amplitude instance] logEvent:@"TR - Reservation Succeed" withEventProperties:eventProperties];
//        [[AppsFlyerTracker sharedTracker] trackEvent:@"TR - Reservation Succeed" withValues:eventProperties];
//        
//        
//        if ([IMGUser currentUser].userId==nil&&!passWordTextField.hidden) {
//            NSMutableDictionary *parameters = [[NSMutableDictionary alloc]initWithCapacity:0];
//            [parameters setObject:reservation.email forKey:@"email"];
//            [parameters setObject:[passWordTextField.text MD5String] forKey:@"password"];
//            [parameters setObject:@"self" forKey:@"loginwith"];
//            [LoginParamter sharedParameter].loginwith=[NSString stringWithFormat:@"self"];
//            UserDataHandler *userDataHandler = [[UserDataHandler alloc] init];
//            
//            [userDataHandler emailLogin:parameters withError:nil withDelegateBlock:^{
//                //        if(self.loginViewController!=nil){
//                //            [self.loginViewController loginSuccessful];
//                //        }else{
//                //            [AppDelegate ShareApp].userLogined=YES;
//                //            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
//                //            [[AppDelegate ShareApp] requestToShowNotificationCount];
//                //            [self dismissViewControllerAnimated:YES completion:nil];
//                //        }
//                
//                [self loginSuccessful];
//            }];
// 
//        }
//        
//
//        
//        
////        [MixpanelHelper trackConfirmBookingWithRestaurant:restaurant andReservation:reservation];
//        [[Amplitude instance] logEvent:@"CL - Book Now Succeed" withEventProperties:dic];
//        [self serverBookOk:responseObject];
//        [[LoadingView sharedLoadingView]stopLoading];
//        submitButton.enabled = YES;
//    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
//        NSLog(@"BookingInfoViewController.requestBook.error: %@",error.localizedDescription);
//        [[Amplitude instance] logEvent:@"CL - Book Now Failed" withEventProperties:dic];
//
//        [[LoadingView sharedLoadingView]stopLoading];
//        submitButton.enabled = YES;
// //       UIAlertView *alert=[[UIAlertView alloc]initWithTitle:ALERT_TITLE message:[NSString stringWithFormat:@"%@",error.localizedDescription]];
////        [alert addButtonWithTitle:@"OK" handler:^(){
////            
////        }];
////        [alert show];
//        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alerView show];
//
//        
//        
//    }];
}


-(void)serverBookOk:(id)data{
    
    NSString *noteString = @"Oops, this time slot is no longer available. Please select a different time.";
    if ([[data objectForKey:@"status"] isEqualToString:@"succeed"]) {
        reservation.code =[data objectForKey:@"code"];
        reservation.reservationId = [data objectForKey:@"reservationId"];
        if([data objectForKey:@"voucherCode"]==nil){
            reservation.voucherCode=nil;
        }
        reservation.status=@"1";

        [[NSNotificationCenter defaultCenter] postNotificationName:@"upcommingUpdatedNotification" object:nil];
        
        [IMGAmplitudeUtil trackBookWithName:@"TR - Reservation Succeed" andRestaurantId:restaurant.restaurantId andReservationId:reservation.reservationId andLocation:@"Reservation detail page" andChannel:nil];

        BookingConfirmViewController *bookingConfirmViewController = [[BookingConfirmViewController alloc]initWithReservation:reservation];
        bookingConfirmViewController.bookInfoVC = self;
//        bookingConfirmViewController.shopStr = self.shopStr;
//        bookingConfirmViewController.iconImage = self.iconImage;
//        bookingConfirmViewController.detailStr = self.detailStr;
//        bookingConfirmViewController.specialRequestStr = specialRequestTextField.text;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:bookingConfirmViewController];
        [self presentViewController:nav animated:YES completion:nil];
//        [self.navigationController pushViewController:bookingConfirmViewController animated:YES];
        
    }else if ([[data objectForKey:@"status"] intValue]==1||[[data objectForKey:@"status"] intValue]==2) {
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:ALERT_TITLE message:noteString];
//        [alert addButtonWithTitle:@"OK" handler:^(){
//        }];
//        [alert show];
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:noteString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alerView show];

    }else{
        NSString *alertString =[NSString stringWithFormat:@"%@",data];
        if ([data objectForKey:@"exceptionmsg"]) {
            alertString=[data objectForKey:@"exceptionmsg"];
//            if ([alertString isLogout]) {
//                [[IMGUser currentUser]logOut];
//                
//            }
        }
//        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:ALERT_TITLE message:alertString];
//        [alert addButtonWithTitle:@"OK" handler:^(){
//            
//        }];
//        [alert show];
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:alertString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alerView show];

//        [[AppDelegate ShareApp]goToLoginController];
//        [self emailLogin:nil];
    }
    
}

-(void)registerSuccess
{
    
}

#pragma mark textfield delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(isViewUp)
    {
        [UIView animateWithDuration:0.3 animations:^{
            _scrollView.contentSize = CGSizeMake(DeviceWidth, DeviceHeight+6+navigationHeight +100);
            if (textField==emailAccountTextField||textField==passwordTextField) {
                _scrollView.contentSize = CGSizeMake(DeviceWidth, DeviceHeight+160 +100);
            }
            _scrollView.contentOffset = CGPointMake(0,0);
        }];
        isViewUp=NO;
    }
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if(isViewUp)
    {
        [UIView animateWithDuration:0.3 animations:^{
            _scrollView.contentSize = CGSizeMake(DeviceWidth, DeviceHeight+6+navigationHeight +100);
            if (textField==emailAccountTextField||textField==passwordTextField) {
                _scrollView.contentSize = CGSizeMake(DeviceWidth, DeviceHeight +100);
            }
            _scrollView.contentOffset = CGPointMake(0, 0);
        }];
        isViewUp=NO;
    }
    [textField resignFirstResponder];

}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    CGFloat viewUpHeight;
    if (textField == firstNameTextField){
        viewUpHeight = ([UIDevice isLaterThanIphone5]?0:0)+bookInfoView.startPointY;
    }else if (textField == lastNameTextField){
        viewUpHeight = ([UIDevice isLaterThanIphone5]?0:40)+bookInfoView.startPointY;
    }else if (textField == emailTextField){
        viewUpHeight = ([UIDevice isLaterThanIphone5]?(emailTextField.startPointY-130):(emailTextField.startPointY-40))+bookInfoView.startPointY;
    }else if (textField == mobileNumberTextField){
        viewUpHeight = ([UIDevice isLaterThanIphone5]?(mobileNumberTextField.startPointY-130):(mobileNumberTextField.startPointY-40))+bookInfoView.startPointY;
    }else if (textField == specialRequestTextField){
        viewUpHeight = ([UIDevice isLaterThanIphone5]?(specialRequestTextField.startPointY-120):(specialRequestTextField.startPointY-40))+bookInfoView.startPointY;
    }else if (textField == voucherCodeTextField){
        viewUpHeight = ([UIDevice isLaterThanIphone5]?(voucherCodeTextField.startPointY-120):(voucherCodeTextField.startPointY-40))+bookInfoView.startPointY;
    }else if(textField == emailAccountTextField){
        viewUpHeight = ([UIDevice isLaterThanIphone5]?(20):(80))+bookInfoView.startPointY;
    
    }else if(textField == passwordTextField){
        viewUpHeight = ([UIDevice isLaterThanIphone5]?(80):(140))+bookInfoView.startPointY;
    }
    if(!isViewUp)
    {
        [UIView animateWithDuration:0.3 animations:^{
            _scrollView.contentSize = CGSizeMake(DeviceWidth, submitButton.endPointY+320+bookInfoView.startPointY +100);
            if (textField==emailAccountTextField||textField==passwordTextField) {
                _scrollView.contentSize = CGSizeMake(DeviceWidth, DeviceHeight+160 +100);
            }
            _scrollView.contentOffset = CGPointMake(0, viewUpHeight);
        }];
        isViewUp=YES;
    }
    
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    isViewUp = NO;
    return YES;
}
-(void)addCodeNumberPicker{
    for (int i=0;i<8; i++) {
        UITextField *textfield = (UITextField*)[self.view viewWithTag:100+i];
        [textfield resignFirstResponder];
    }
    
    if (codeNumView == nil) {
        codeNumView = [[UIView alloc]initWithFrame:CGRectMake(0, DeviceHeight-290+20, DeviceWidth, 290)];
    }
    codeNumView.backgroundColor = [UIColor whiteColor];
    if (codeNumPicker == nil) {
        codeNumPicker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 40, DeviceWidth, 250)];
    }
     [EntityKind unfoldAnimationWith:lineImageViewMonbiel];
    codeNumPicker.delegate = self;
    codeNumPicker.dataSource = self;
    [codeNumPicker selectRow:selectCodeIndex inComponent:1 animated:YES];
    [codeNumView addSubview:codeNumPicker];
    [self.view addSubview:codeNumView];
    
    doneButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
    doneButton2.frame = CGRectMake(DeviceWidth-60, 0, 50, 40);
    [doneButton2 setTitleColor:[UIColor color222222] forState:UIControlStateNormal];
    [doneButton2 setTitle:L(@"Done") forState:UIControlStateNormal];
    [codeNumView addSubview:doneButton2];
    [doneButton2 addTarget:self action:@selector(codeNumDone) forControlEvents:UIControlEventTouchUpInside];
}
-(void)codeNumDone{
    [EntityKind foldAnimationWith:lineImageViewMonbiel];
    [codeNumPicker removeFromSuperview];
    [codeNumView removeFromSuperview];
    if (selectCode) {
        countryCodeTextField.text = [NSString stringWithFormat:@"+%@",selectCode];
    }else{
        IMGCountryCode *code = [codeArray firstObject];
        countryCodeTextField.text = [NSString stringWithFormat:@"+%@",code.code];
    }
}
-(void)hideCodeNumPicker{
    [codeNumPicker removeFromSuperview];
    [codeNumView removeFromSuperview];
}
#pragma mark picker view delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component == 0) {
        return;
    }
    IMGCountryCode *code = [codeArray objectAtIndex:row];
    selectCode = [NSString stringWithFormat:@"%@",code.code];
    selectCodeIndex = row;
    
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0) {
        return 1;
    }
    return codeArray.count;
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}
-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    if (component == 0) {
        return 40.0f;
    }
    return 100.0f;
}
#pragma mark picker view dataSource
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (component == 0) {
        return @"+";
    }
    IMGCountryCode *code = [codeArray objectAtIndex:row];
    return [NSString stringWithFormat:@"%@",code.code];
}
-(NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (component == 0) {
        return nil;
    }
    IMGCountryCode *code = [codeArray objectAtIndex:row];
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",code.code]];
    [attriString addAttribute:(NSString *)kCTFontAttributeName
                        value:(id)CFBridgingRelease(CTFontCreateWithName((CFStringRef)[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13].fontName,13,NULL))range:NSMakeRange(0, [NSString stringWithFormat:@"%@",code.code].length-1)];
    return attriString;
}
 

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *name = user.profile.name;
    NSString *email = user.profile.email;
    NSLog(@"Customer details: %@ %@ %@ %@", userId, idToken, name, email);
    [[LoadingView sharedLoadingView] stopLoading];
    if (![user.userID intValue]) {
        [[Amplitude instance] logEvent: @"SU - Sign up Cancel" withEventProperties:@{@"Sign Up Type":@"Google"}];
        
    }
    UserDataHandler *userDataHandler = [[UserDataHandler alloc] init];
    [userDataHandler googleLogin:user error:error delegateWithBlock:^{
        
        NSDictionary *dataDic = [[NSDictionary alloc] initWithDictionary:[userDataHandler getParameters]];
        if ([userDataHandler isSignUp])
        {
            [self loginSuccessful];
        }
        else
        {
            FBSignUpViewController *fbsuvc = [[FBSignUpViewController alloc] init];
            fbsuvc.dataDic = dataDic;
            fbsuvc.amplitudeType = @"Google";
            fbsuvc.isFromBooking = YES;
            [self.navigationController pushViewController:fbsuvc animated:YES];
        }
        
        
        
        
    } failedBlock:^{
        
        [self enterEmail];
        
    }];
    
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [emailAccountTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
//    [self.view endEditing:YES];
    isViewUp = NO;
}
@end
