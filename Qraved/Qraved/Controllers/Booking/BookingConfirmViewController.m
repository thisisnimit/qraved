//
//  BookingConfirmViewController.m
//  Qraved
//
//  Created by Laura on 14-8-14.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BookingConfirmViewController.h"
#import "AppDelegate.h"
#import <CoreText/CoreText.h>
#import "NSDate+Helper.h"
#import "UIConstants.h"
//#import "MixpanelHelper.h"
#import "MapViewController.h"
#import "Label.h"
#import "UIView+Helper.h"
#import "UIViewController+Helper.h"
#import "UIColor+Helper.h"
#import "NSDate+Helper.h"
#import "NSString+Helper.h"
#import "UIImage+Helper.h"
#import "LoadingView.h"
#import "IMGUser.h"
#import "SendCall.h"
#import "NavigationBarButtonItem.h"
#import "CustomIOS7AlertView.h"
#import "TYAttributedLabel.h"
#import "ShareToPathView.h"
#import "NXOAuth2AccountStore.h"
#import "TrackHandler.h"
#import "BookUtil.h"

@interface BookingConfirmViewController ()<UIScrollViewDelegate>
{
    UIScrollView *bookInfoView;
}
@end

@implementation BookingConfirmViewController{
    IMGReservation *reservation;
    IMGRestaurant *restaurant;
    UILabel *statusLabel;
    UIButton *cancelButton;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(id)initWithReservation:(IMGReservation *)tmpReservation{
    self = [super init];
    if(self){
        reservation = tmpReservation;
    }
    return self;
}


-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = L(@"Booking Confirmation");
    [super viewWillAppear:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"Reservation form page - final page";
    self.view.backgroundColor = [UIColor defaultColor];
    [self setDoneBarButton];
    
    IMGUser *user = [IMGUser currentUser];
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",reservation.reservationId,@"reservationId", nil];
    [BookUtil getReservationInfoWithparameters:dic andBlock:^(IMGReservation *reservationT) {
        reservation = reservationT;
        restaurant = [[IMGRestaurant alloc] init];
        restaurant.restaurantId = reservation.restaurantId;
        restaurant.title = reservation.restaurantTitle;
        restaurant.restaurantId = reservation.restaurantId;
        restaurant.cuisineName = reservation.restaurantCuisine;
        restaurant.districtName = reservation.restaurantDistrict;
        restaurant.latitude = reservation.latitude;
        restaurant.longitude = reservation.longitude;
        restaurant.seoKeyword = reservation.restaurantSeoKeywords;
        restaurant.ratingScore = reservation.ratingScore;
        [self loadMainView];
        
    }];
//    [self loadMainView];
    
}

-(void)setDoneBarButton
{
    //doneButtonTapped
    UIBarButtonItem *saveBtn=[[UIBarButtonItem alloc] initWithTitle:L(@"Done") style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonTapped)];
    saveBtn.tintColor=[UIColor colorWithRed:9/255.0f green:191/255.0f blue:211/255.0f alpha:1.0f];
    self.navigationItem.rightBarButtonItem = saveBtn;
}
-(void)doneButtonTapped
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[NSDate date] forKey:@"dateDone"];
    if (self.isFromProfile) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self dismissViewControllerAnimated:NO completion:^{
            [self.bookInfoVC.navigationController popToViewController:[self.bookInfoVC.navigationController.viewControllers objectAtIndex:(self.bookInfoVC.navigationController.viewControllers.count - 6)] animated:YES];
        }];
    }
    
    
    
    
//    if([AppDelegate ShareApp].userLogined){
//        IMGUser *user=[IMGUser currentUser];
//        if(user.userId!=nil){
//
//  
//            [self dismissViewControllerAnimated:NO completion:^{
////                [self.bookInfoVC dismissViewControllerAnimated:NO completion:nil];
//                [self.bookInfoVC.navigationController popToRootViewControllerAnimated:NO];
//            }];
//            [[AppDelegate ShareApp ] goToPage:1];
//        }else{
////            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:2] animated:YES];
//            [self dismissViewControllerAnimated:YES completion:^{
//                
//            }];
//
//        }
//    }else{
//        
//
//    }
    
}
-(void)loadMainView
{
    bookInfoView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    bookInfoView.backgroundColor = [UIColor defaultColor];
    bookInfoView.delegate = self;
    [self.view addSubview:bookInfoView];
    bookInfoView.sd_layout
    .topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .heightIs(DeviceHeight +20)
    .widthIs(DeviceWidth);
    
    UIImageView *iconImageV = [UIImageView new];
    iconImageV.backgroundColor = [UIColor whiteColor];
    [iconImageV sd_setImageWithURL:[NSURL URLWithString:[reservation.restaurantImage returnFullImageUrl]]];
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.text = reservation.restaurantTitle;
    lblTitle.textColor = [UIColor color333333];
    lblTitle.font = [UIFont systemFontOfSize:14];
    [lblTitle setNumberOfLines:1];
    
    UILabel *lblDetail = [UILabel new];
    lblDetail.text = reservation.restaurantCuisine;
    lblDetail.textColor = [UIColor lightGrayColor];
    lblDetail.font = [UIFont systemFontOfSize:14];
    lblDetail.numberOfLines = 1;
    
    
    NSString *partyString = [NSString stringWithFormat:L(@"Pax for %d people"),[reservation.party intValue]];
    UILabel *personLabel = [[UILabel alloc] init];
    personLabel.backgroundColor = [UIColor clearColor];
    personLabel.text = partyString;
    personLabel.textColor = [UIColor color333333];
    personLabel.font = [UIFont systemFontOfSize:14];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:reservation.bookDate];
    [dateFormatter setDateFormat:@"EEEE, MMM dd yyyy"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    [dateFormatter setDateFormat:@", dd MMM"];
    
    NSDateFormatter *dateFormat4 = [ [NSDateFormatter alloc] init];
    [dateFormat4 setDateFormat:@"HH:mm"];
    NSDate *date1 = [dateFormat4 dateFromString:reservation.bookTime];
    NSDateFormatter *dateFormat3 = [ [NSDateFormatter alloc] init];
    [dateFormat3 setDateFormat:@"hh:mm a"];
    NSString  *string =  [dateFormat3 stringFromDate: date1];
    
    UILabel *dateLabel = [[UILabel alloc] init];
    dateLabel.backgroundColor = [UIColor clearColor];
    dateLabel.text = [NSString stringWithFormat:@"%@ %@",dateString,string];
    dateLabel.textColor =[UIColor color333333];
    dateLabel.font = [UIFont systemFontOfSize:14];
   
    UIView *lineA = [UIView new];
    lineA.backgroundColor = [UIColor colorEDEDED];
    
    UILabel *promptLabel = [[UILabel alloc]init];
    promptLabel.text = L(@"Thank you for booking through Qraved. You’ll receive a notification upon confirmation");
    promptLabel.textColor = [UIColor color333333];
    promptLabel.font = [UIFont systemFontOfSize:14];
    promptLabel.textAlignment = NSTextAlignmentCenter;
    
    UIView *lineB = [UIView new];
    lineB.backgroundColor = [UIColor colorEDEDED];
    
    UILabel *bookCodeLabel = [[UILabel alloc] init];
    bookCodeLabel.text = @"Book Code";//@"Special Request";
    bookCodeLabel.textColor =[UIColor color333333];
    bookCodeLabel.font = [UIFont systemFontOfSize:14];
    
    UILabel *lblBookCode = [[UILabel alloc] init];
    lblBookCode.text = reservation.code;//@"Book Code";//@"Special Request";
    lblBookCode.textColor =[UIColor color333333];
    lblBookCode.font = [UIFont systemFontOfSize:14];
    
    UILabel *voucherLabel = [[UILabel alloc] init];
    voucherLabel.text = @"Voucher";//@"Special Request";
    voucherLabel.textColor =[UIColor color333333];
    voucherLabel.font = [UIFont systemFontOfSize:14];
    
    UILabel *lblvoucher = [[UILabel alloc] init];
    lblvoucher.text = reservation.voucherCode;//@"Book Code";//@"Special Request";
    lblvoucher.textColor =[UIColor color333333];
    lblvoucher.font = [UIFont systemFontOfSize:14];
    
    UILabel *SpecialLabel = [[UILabel alloc] init];
    SpecialLabel.text = @"Special Request";
    SpecialLabel.textColor =[UIColor color333333];
    SpecialLabel.font = [UIFont systemFontOfSize:14];
    
    UILabel *lblSpecial = [[UILabel alloc] init];
    lblSpecial.text = reservation.specialRequest;
    lblSpecial.textColor =[UIColor color333333];
    lblSpecial.font = [UIFont systemFontOfSize:14];
    
    UIView *lineC = [UIView new];
    lineC.backgroundColor = [UIColor colorEDEDED];
    
    UIButton *btnPhone = [UIButton buttonWithType:UIButtonTypeCustom];
//    btnPhone.backgroundColor = [UIColor redColor];
    [btnPhone addTarget:self action:@selector(telephoneButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [btnPhone setTitle:@"Call" forState:UIControlStateNormal];
    btnPhone.titleLabel.font = [UIFont systemFontOfSize:12];
    [btnPhone setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [btnPhone setImage:[UIImage imageNamed:@"Shapephone"] forState:UIControlStateNormal];
    [btnPhone setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
    
    UIButton *btnMap = [UIButton buttonWithType:UIButtonTypeCustom];
//    btnMap.backgroundColor = [UIColor brownColor];
    [btnMap addTarget:self action:@selector(gotoMap) forControlEvents:UIControlEventTouchUpInside];
    [btnMap setTitle:@"View Map" forState:UIControlStateNormal];
    btnMap.titleLabel.font = [UIFont systemFontOfSize:12];
    [btnMap setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [btnMap setImage:[UIImage imageNamed:@"ic_home_map"] forState:UIControlStateNormal];
    [btnMap setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
    
    UIButton *btnReminder = [UIButton buttonWithType:UIButtonTypeCustom];
//    btnReminder.backgroundColor = [UIColor blueColor];
    [btnReminder addTarget:self action:@selector(addToCalendar) forControlEvents:UIControlEventTouchUpInside];
    [btnReminder setTitle:@"Reminder" forState:UIControlStateNormal];
    btnReminder.titleLabel.font = [UIFont systemFontOfSize:12];
    [btnReminder setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [btnReminder setImage:[UIImage imageNamed:@"Shapebtn"] forState:UIControlStateNormal];
    [btnReminder setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
    
    UILabel *inviteLabel = [[UILabel alloc]init];
    inviteLabel.text = L(@"Share to friends");
    inviteLabel.textColor = [UIColor color333333];
    inviteLabel.font = [UIFont systemFontOfSize:14];

    
    [bookInfoView sd_addSubviews:@[iconImageV, lblTitle, lblDetail, personLabel, dateLabel, lineA, promptLabel, lineB, bookCodeLabel, lblBookCode,voucherLabel, lblvoucher, SpecialLabel, lblSpecial, lineC, btnPhone, btnMap, btnReminder, inviteLabel]];
    
    iconImageV.sd_layout
    .topSpaceToView(bookInfoView, 10)
    .leftSpaceToView(bookInfoView, 10)
    .widthIs(45)
    .heightIs(45);
    
    lblTitle.sd_layout
    .topSpaceToView(bookInfoView, 15)
    .leftSpaceToView(iconImageV ,5)
    .heightIs(20);
    [lblTitle setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 70];
    
    
    lblDetail.sd_layout
    .topSpaceToView(lblTitle, 0)
    .leftEqualToView(lblTitle)
    .heightIs(20);
    [lblDetail setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
    personLabel.sd_layout
    .topSpaceToView(iconImageV, 10)
    .leftEqualToView(iconImageV)
    .autoHeightRatio(0);
    [personLabel setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
    dateLabel.sd_layout
    .topSpaceToView(personLabel, 0)
    .leftEqualToView(personLabel)
    .autoHeightRatio(0);
    [dateLabel setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
    lineA.sd_layout
    .topSpaceToView(dateLabel, 10)
    .leftSpaceToView(bookInfoView, 0)
    .widthIs(DeviceWidth)
    .heightIs(1);
    
    promptLabel.sd_layout
    .topSpaceToView(lineA, 10)
    .centerXEqualToView(bookInfoView)
    .autoHeightRatio(0);
    [promptLabel setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 40];
    
    lineB.sd_layout
    .topSpaceToView(promptLabel, 10)
    .leftSpaceToView(bookInfoView, 0)
    .widthIs(DeviceWidth)
    .heightIs(1);
    
    bookCodeLabel.sd_layout
    .topSpaceToView(lineB, 20)
    .leftSpaceToView(bookInfoView, 10)
    .heightIs(20)
    .widthIs(120);
    
    lblBookCode.sd_layout
    .topEqualToView(bookCodeLabel)
    .leftSpaceToView(bookCodeLabel,0)
    .heightIs(20);
    [lblBookCode setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 140];
    
    voucherLabel.sd_layout
    .topSpaceToView(bookCodeLabel, 20)
    .leftSpaceToView(bookInfoView, 10)
    .heightIs(20)
    .widthIs(120);
    
    lblvoucher.sd_layout
    .topEqualToView(voucherLabel)
    .leftSpaceToView(voucherLabel,0)
    .heightIs(20);
    [lblvoucher setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 140];
    
    SpecialLabel.sd_layout
    .topSpaceToView(voucherLabel, 20)
    .leftSpaceToView(bookInfoView, 10)
    .heightIs(20)
    .widthIs(120);
    
    lblSpecial.sd_layout
    .topEqualToView(SpecialLabel)
    .leftSpaceToView(SpecialLabel,0)
    .heightIs(20);
    [lblSpecial setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 140];
    
    lineC.sd_layout
    .topSpaceToView(SpecialLabel, 20)
    .leftSpaceToView(bookInfoView, 0)
    .widthIs(DeviceWidth)
    .heightIs(1);
    
    btnPhone.sd_layout
    .topSpaceToView(lineC, 20)
    .leftSpaceToView(bookInfoView, 0)
    .widthIs(DeviceWidth/3)
    .heightIs(30);
    
    btnMap.sd_layout
    .topSpaceToView(lineC, 20)
    .leftSpaceToView(btnPhone, 0)
    .widthIs(DeviceWidth/3)
    .heightIs(30);
    
    btnReminder.sd_layout
    .topSpaceToView(lineC, 20)
    .leftSpaceToView(btnMap, 0)
    .widthIs(DeviceWidth/3)
    .heightIs(30);
    
    inviteLabel.sd_layout
    .topSpaceToView(btnPhone, 30)
    .leftSpaceToView(bookInfoView, 10)
    .autoHeightRatio(0);
    [inviteLabel setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 20];
   
    NSArray *imagesArr = @[@"share link",@"whatsapp",@"line",@"sms",@"rc_email"];//,@"TelGreen"
    UIImageView *imageView;
    
  
    CGFloat HorizontalSpacing = 10;
    CGFloat imageBtnWidth = (DeviceWidth - (imagesArr.count +1)*HorizontalSpacing)/imagesArr.count;
    for (int i=0; i<imagesArr.count; i++) {
        CGFloat skilledViewX = i *HorizontalSpacing +i * imageBtnWidth +HorizontalSpacing;
        imageView = [[UIImageView alloc]init];
        imageView.image = [UIImage imageNamed:[imagesArr objectAtIndex:i]];
        imageView.tag = i;
        imageView.userInteractionEnabled = YES;
        [bookInfoView sd_addSubviews:@[imageView]];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(inviteButtonClick:)];
        [imageView addGestureRecognizer:tap];
        
        imageView.sd_layout
        .topSpaceToView(inviteLabel, HorizontalSpacing)
        .leftSpaceToView(bookInfoView, skilledViewX)
        .widthIs(imageBtnWidth)
        .heightIs(imageBtnWidth);
    }
    
    cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setTitle:L(@"CANCEL BOOKING") forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    cancelButton.tag = 1000+3;
    if([reservation.status isEqualToString:@"1"]||[reservation.status isEqualToString:@"11"]||[reservation.status isEqualToString:@"2"]){
        cancelButton.hidden = NO;
    }else{
        cancelButton.hidden = YES;
    }
    [cancelButton addTarget:self action:@selector(cancelBooking:) forControlEvents:UIControlEventTouchUpInside];
    cancelButton.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    cancelButton.backgroundColor = [UIColor colorFBFBFB];
    [bookInfoView sd_addSubviews:@[cancelButton]];
    
    cancelButton.sd_layout
    .topSpaceToView(imageView, 20)
    .centerXEqualToView(bookInfoView)
    .heightIs(45)
    .widthIs(DeviceWidth - 40);
    cancelButton.layer.cornerRadius = 22;
    cancelButton.layer.masksToBounds = YES;
    cancelButton.layer.borderWidth = 1.0f;
    cancelButton.layer.borderColor = [UIColor colorE4E4E4].CGColor;
    
    [bookInfoView setupAutoContentSizeWithBottomView:cancelButton bottomMargin:100];
    
}

-(void)cancelBooking:(UIButton*)sender{
    [IMGAmplitudeUtil trackBookWithName:@"TR - Reservation Cancel Initiate" andRestaurantId:reservation.restaurantId andReservationId:reservation.reservationId andLocation:@"Booking Confirmation Page" andChannel:nil];
 
    UIView *popUpView = [[UIView alloc]init];
    
    NSString *offerTitleStr = L(@"Are you sure want to cancel your booking?");
    
    CGSize maxSize = [offerTitleStr sizeWithFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
    Label *seeMenuTitle = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 64/2-(maxSize.height+4)/2, maxSize.width, maxSize.height+4) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] andTextColor:[UIColor color333333] andTextLines:0];
    seeMenuTitle.text = offerTitleStr;
    seeMenuTitle.textAlignment = NSTextAlignmentCenter;
    [popUpView addSubview:seeMenuTitle];
    
    NSArray *menusArr = @[L(@"We appreciate the gesture. This means other guests can get a seat if you can't attend.")];
    float startY = seeMenuTitle.endPointY;
    for (int i=0; i<menusArr.count; i++) {
        CGSize maxSize1 = [[menusArr objectAtIndex:i] sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
        
        Label *seeMenuLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, startY, DeviceWidth-4*LEFTLEFTSET, maxSize1.height+20) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15] andTextColor:[UIColor color333333] andTextLines:0];
        seeMenuLabel.textAlignment = NSTextAlignmentCenter;
        seeMenuLabel.text = [menusArr objectAtIndex:i];
        seeMenuLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [popUpView addSubview:seeMenuLabel];
        startY = seeMenuLabel.endPointY;
    }
    popUpView.frame = CGRectMake(0, 0, DeviceWidth-2*LEFTLEFTSET, startY);
    
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc]init];
    alertView.buttonTitles = @[@"No, keep it",@"Yes"];
    [alertView setContainerView:popUpView];
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, NSInteger buttonIndex) {
        if(buttonIndex==1){
            IMGUser * user = [IMGUser currentUser];
            if(user.userId==nil){
                //lead to login
                return;
            }
            if(user.token==nil){
                //lead to login
                return;
            }
            NSString *newStatus=@"13";
            if([reservation.status isEqualToString:@"11"]){
                newStatus=@"14";
            }else if([reservation.status isEqualToString:@"2"]){
                newStatus=@"3";
            }
            [[LoadingView sharedLoadingView] startLoading];

            NSDictionary * parameters = @{@"userID":user.userId,@"t":user.token,@"id":reservation.reservationId,@"status":newStatus};
            [[IMGNetWork sharedManager]POST:@"reseravtion/status/update" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
                
                NSString *exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
                if([exceptionMsg isLogout]){
                    [[LoadingView sharedLoadingView] stopLoading];
                    [[IMGUser currentUser]logOut];
                    [[AppDelegate ShareApp] goToLoginController];
                    return;
                }
                
                NSString *returnStatusString = [responseObject objectForKey:@"status"];
                if([returnStatusString isEqualToString:@"succeed"]){
//                    [MixpanelHelper trackCancelBookingWithRestaurant:restaurant andReservation:reservation];
                    
                    [IMGAmplitudeUtil trackBookWithName:@"TR - Reservation Cancel Succeed" andRestaurantId:reservation.restaurantId andReservationId:reservation.reservationId andLocation:@"Booking Confirmation Page" andChannel:nil];
                    
                    [statusLabel setText:@"Cancelled"];
                    [cancelButton setHidden:YES];
                }
                [[LoadingView sharedLoadingView]stopLoading];
            } failure:^(NSURLSessionDataTask *operation, NSError *error) {
                NSLog(@"MainMenuViewController.request cancel book.error: %@",error.localizedDescription);
                [[LoadingView sharedLoadingView]stopLoading];
            }];
            
        }
        if (buttonIndex==0) {
            NSMutableDictionary* eventDic=[[NSMutableDictionary alloc]init];
            [eventDic setValue:reservation.reservationId forKey:@"Reservation_ID"];
            [eventDic setValue:reservation.restaurantId forKey:@"Restaurant_ID"];
            [[Amplitude instance] logEvent:@"TR - Reservation Cancel Cancel" withEventProperties:eventDic];
        }
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    [alertView show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==1){
        if(buttonIndex==0){
            [alertView dismissWithClickedButtonIndex:0 animated:NO];
        }else if (buttonIndex==1){
            
            [IMGAmplitudeUtil trackBookWithName:@"CL - Call Restaurant CTA" andRestaurantId:restaurant.restaurantId andReservationId:nil andLocation:@"Booking Confirmation Page" andChannel:nil];
            
            [SendCall sendCall:alertView.message];
        }
    }
}
-(void)telephoneButtonTapped:(UIButton*)button{
    
    IMGUser *user = [IMGUser currentUser];
    NSNumber *userId = [[NSNumber alloc] init];
    if (user.userId != nil)
    {
        userId = user.userId;
    }
    else
        userId = [NSNumber numberWithInt:0];
    
    [TrackHandler trackWithUserId:userId andRestaurantId:reservation.restaurantId andContent:[NSDictionary dictionaryWithObject:@"0" forKey:@"type"]];
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"" message:reservation.phone delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
    alertView.tag=1;
    [alertView show];
    
   
}
-(void)gotoMap{
    IMGUser *user = [IMGUser currentUser];
    NSNumber *userId = [[NSNumber alloc] init];
    if (user.userId != nil)
    {
        userId = user.userId;
    }
    else
        userId = [NSNumber numberWithInt:0];
    
    [TrackHandler trackWithUserId:userId andRestaurantId:reservation.restaurantId andContent:[NSDictionary dictionaryWithObject:@"1" forKey:@"type"]];
    
    [IMGAmplitudeUtil trackBookWithName:@"RC - View Restaurant Map" andRestaurantId:reservation.restaurantId andReservationId:nil andLocation:@"Booking Confirmation Page" andChannel:nil];

    MapViewController *mapViewController=[[MapViewController alloc]initWithRestaurant:restaurant];
    mapViewController.fromDetail = YES;
    [self.navigationController pushViewController:mapViewController animated:YES];
}
-(void)addToCalendar{
    IMGReservation *currentReservation=reservation;
    
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    if ([eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)]){
        // the selector is available, so we must be on iOS 6 or newer
        [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(error){
                    // display error message here
                }else if(!granted){
                    // display access denied error message here
                }else{
                    // access granted
                    // ***** do the important stuff here *****
                    EKEvent *ekEvent  = [EKEvent eventWithEventStore:eventStore];
                    ekEvent.title     = restaurant.title;
                    ekEvent.location = [restaurant address];
                    
                    [IMGAmplitudeUtil trackBookWithName:@"CL - Booking Reminder CTA" andRestaurantId:reservation.restaurantId andReservationId:reservation.reservationId andLocation:@"Booking Confirmation Page" andChannel:nil];
                    
                    NSDateFormatter *tempFormatter = [[NSDateFormatter alloc]init];
                    [tempFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
                    ekEvent.startDate=[tempFormatter dateFromString:[NSString stringWithFormat:@"%@ %@",currentReservation.bookDate,currentReservation.bookTime]];
                    ekEvent.endDate=[tempFormatter dateFromString:[NSString stringWithFormat:@"%@ %@",currentReservation.bookDate,currentReservation.bookTime]];
                    
                    [tempFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
                    ekEvent.allDay = NO;
                    
                    //添加提醒
                    [ekEvent addAlarm:[EKAlarm alarmWithRelativeOffset:60.0f * -60.0f * 24]];
                    [ekEvent addAlarm:[EKAlarm alarmWithRelativeOffset:60.0f * -15.0f]];
                    
                    [ekEvent setCalendar:[eventStore defaultCalendarForNewEvents]];
                    NSError *err;
                    [eventStore saveEvent:ekEvent span:EKSpanThisEvent error:&err];
                    
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:L(@"Calendar") message:L(@"Reservation is added to calendar.") delegate:nil cancelButtonTitle:L(@"OK") otherButtonTitles:nil];
                    [alert show];
                    
                }
            });
        }];
    }
    
    
}

-(void)upcommingButtonClick:(UIButton*)button
{
    switch (button.tag - 1000) {
        case 0:
        {//phone
            
        }
            break;
            
        case 1:
        {//location
            
        }
            break;
            
        case 2:
        {//calendar
            
        }
            break;
            
        case 3:
        {//cancel
        }
            break;
            
        default:
            break;
    }
}

-(void)inviteButtonClick:(UITapGestureRecognizer*)gesture
{    //[self shareToPath];
    // [self shareWithFacebook];
    
    NSInteger tag = gesture.view.tag;
    switch (tag) {
        case 0:
        {
            //share link
            
            [self shareLink];
        }
            break;
        case 1:
        {
            //whatsapp
                        [self shareWithWhatsapp];
        }
            break;
        case 2:
        {
            //line
            [self shareWithLine];
        }
            break;
        case 3:
        {
            //sms
            [self shareWithSMS];
            
        }
            break;
        case 4:
        {
            //email
            [self shareWithEmail];
        }
            break;
        
        default:
            break;
    }
}

//URL复制
- (void)shareLink{
    [IMGAmplitudeUtil trackBookWithName:@"SH - Share Reservation" andRestaurantId:nil andReservationId:reservation.reservationId andLocation:@"Booking Confirmation Page" andChannel:@"link"];
    
    //IMGReservation *currentReservation=reservation;
    IMGRestaurant *currentRestaurant=restaurant;
//    NSString *shareContentString = [NSString stringWithFormat:L(@"I just made a booking for us at %@ on %@ %@. Visit %@ if you want to check out what the restaurant has got to offer."),currentRestaurant.title,currentReservation.bookDate,currentReservation.bookTime,[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],currentRestaurant.seoKeyword]];
    
    NSString *shareContentString = [NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],currentRestaurant.seoKeyword];
    //复制文字
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = shareContentString;
    
    
    //复制图片
    /*
     UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
     [pasteboard setData:UIImageJPEGRepresentation([UIImage imageNamed:@"account_icon_friend.png"] , 1.0) forPasteboardType:@"public.jpeg"];*/
    
    [UIAlertView showAlertView:@"The content has been copied to the clipboard" andMessage:nil];
}
- (void)shareWithWhatsapp{
    [IMGAmplitudeUtil trackBookWithName:@"SH - Share Reservation" andRestaurantId:nil andReservationId:reservation.reservationId andLocation:@"Booking Confirmation Page" andChannel:@"Whatsapp"];
        //IMGReservation *currentReservation=reservation;
        IMGRestaurant *currentRestaurant=restaurant;
//        NSString *shareContentString = [NSString stringWithFormat:L(@"I just made a booking for us at %@ on %@ %@. Visit %@ if you want to check out what the restaurant has got to offer."),currentRestaurant.title,currentReservation.bookDate,currentReservation.bookTime,[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],currentRestaurant.seoKeyword]];
    
    NSString *shareContentString = [NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],currentRestaurant.seoKeyword];
        
        NSString *url = [NSString stringWithFormat:@"whatsapp://send?text=%@", [shareContentString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]]];
        NSURL *whatsappURL = [NSURL URLWithString: url];
        if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
            [[UIApplication sharedApplication] openURL: whatsappURL];
        } else {
            // Cannot open whatsapp
        }
   
}
//LINE
- (void)shareWithLine
{
    [IMGAmplitudeUtil trackBookWithName:@"SH - Share Reservation" andRestaurantId:nil andReservationId:reservation.reservationId andLocation:@"Booking Confirmation Page" andChannel:@"LINE"];
    //IMGReservation *currentReservation=reservation;
    IMGRestaurant *currentRestaurant=restaurant;
//    NSString *shareContentString = [NSString stringWithFormat:L(@"I just made a booking for us at %@ on %@ %@. Visit %@ if you want to check out what the restaurant has got to offer."),currentRestaurant.title,currentReservation.bookDate,currentReservation.bookTime,[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],currentRestaurant.seoKeyword]];
    
    NSString *shareContentString = [NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],currentRestaurant.seoKeyword];
    //分享文字
    NSString *contentType = @"text";
    NSString *urlString = [NSString
                           stringWithFormat:@"line://msg/%@/%@",
                           contentType, [shareContentString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    /******分享图片
     UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
     [pasteboard setData:UIImageJPEGRepresentation([UIImage imageNamed:@"account_icon_friend.png"] , 1.0) forPasteboardType:@"public.jpeg"];
     
     NSString *contentType = @"image";
     NSString *urlString = [NSString
     stringWithFormat:@"line://msg/%@/%@",
     contentType, pasteboard.name]; //从剪切板中获取图片，文字亦可以如此
     */
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"%@",url);
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url];
    }
    else{
        [UIAlertView showAlertView:@"Sharing failed" andMessage:nil];
    }
}
-(void)shareToPath{
    NSString *pathToken = [[NSUserDefaults standardUserDefaults] objectForKey:QRAVED_PATH_TOKEN_KEY];
    if(pathToken==nil){
        [[NXOAuth2AccountStore sharedStore] requestAccessToAccountWithType:@"Path"];
    }else{
        NSLog(@"get path token directly from NSUserDefaults");
    }
    
//    IMGReservation *currentReservation=reservation;
    IMGRestaurant *currentRestaurant=restaurant;
    NSString *shareContentString = [NSString stringWithFormat:L(@"I just made a booking at %@ on Qraved.com"),currentRestaurant.title];
    
    NSString *imgURL;
//    FMResultSet *resultSet=[[DBManager manager]executeQuery:[NSString stringWithFormat:@"SELECT imageUrl FROM IMGRestaurantImage WHERE restaurantId = '%@' limit 1;",restaurant.restaurantId]];
//    if ([resultSet next]) {
//        imgURL=[resultSet stringForColumn:@"imageUrl"];
//    }
//    [resultSet close];
    [IMGAmplitudeUtil trackBookWithName:@"SH - Share Reservation" andRestaurantId:nil andReservationId:reservation.reservationId andLocation:@"Booking Confirmation Page" andChannel:@"Path"];
    ShareToPathView *shareToPathView = [[ShareToPathView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+[UIDevice heightDifference]) title:shareContentString url:imgURL latitude:restaurant.latitude longitude:restaurant.longitude restaurantTitle:restaurant.title phone:restaurant.phoneNumber address:[restaurant address]];
    [self.view addSubview:shareToPathView];
//    [MixpanelHelper trackShareToPathWithshareObject:restaurant.title andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
}
-(void)shareWithTwitter{
    [[LoadingView sharedLoadingView]startLoading];

    [IMGAmplitudeUtil trackBookWithName:@"SH - Share Reservation" andRestaurantId:nil andReservationId:reservation.reservationId andLocation:@"Booking Confirmation Page" andChannel:@"Twitter"];
    IMGRestaurant *currentRestaurant=restaurant;
    NSString *shareContentString = [NSString stringWithFormat:L(@"I just made a booking at %@ on Qraved.com"),currentRestaurant.title];
    SLComposeViewController *slComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    [slComposerSheet setInitialText:shareContentString];
    [slComposerSheet addURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],currentRestaurant.seoKeyword]]];
    if(slComposerSheet){
        [self presentViewController:slComposerSheet animated:YES completion:^{
            [[LoadingView sharedLoadingView]stopLoading];
        }];
        [slComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            NSString *output;
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    output = L(@"Action Cancelled");
                    break;
                case SLComposeViewControllerResultDone:{
                    output = L(@"Post Successful");
//                    [MixpanelHelper trackShareToTwitterWithshareObject:restaurant.title andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
                }
                    break;
                default:
                    break;
            }
            if (result != SLComposeViewControllerResultCancelled){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:L(@"Twitter Message") message:output delegate:nil cancelButtonTitle:L(@"OK") otherButtonTitles:nil];
                [alert show];
            }
        }];
    }
}

-(void)shareWithFacebook{
    [[LoadingView sharedLoadingView]startLoading];
    [IMGAmplitudeUtil trackBookWithName:@"SH - Share Reservation" andRestaurantId:nil andReservationId:reservation.reservationId andLocation:@"Booking Confirmation Page" andChannel:@"Facebook"];

    IMGRestaurant *currentRestaurant=restaurant;
    NSString *shareContentString = [NSString stringWithFormat:@"I just made a booking at %@ on Qraved.com",currentRestaurant.title];
    SLComposeViewController *slComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [slComposerSheet setInitialText:shareContentString];
    [slComposerSheet addURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@?shareType=booking",QRAVED_WEB_SERVER_OLD,[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],currentRestaurant.seoKeyword]]];
    if(slComposerSheet){
        [self presentViewController:slComposerSheet animated:YES completion:^{
            [[LoadingView sharedLoadingView]stopLoading];
        }];
        [slComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result){
            NSString *output;
            switch(result){
                case SLComposeViewControllerResultCancelled:
                    output = L(@"Action Cancelled");
                    break;
                case SLComposeViewControllerResultDone:{
                    output = L(@"Post Successful");
//                    [MixpanelHelper trackShareToFacebookWithshareObject:restaurant.title andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
                }
                    break;
                default:
                    break;
            }
            if (result != SLComposeViewControllerResultCancelled){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:L(@"Facebook Message") message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
        }];
    }
}

-(void)shareWithEmail{
    //IMGReservation *currentReservation=reservation;
    IMGRestaurant *currentRestaurant=restaurant;
    [IMGAmplitudeUtil trackBookWithName:@"SH - Share Reservation" andRestaurantId:nil andReservationId:reservation.reservationId andLocation:@"Booking Confirmation Page" andChannel:@"Email"];
    
    if ([MFMailComposeViewController canSendMail]){
//        NSString *shareContentString = [NSString stringWithFormat:L(@"I just made a booking for us at %@ on %@ %@. Click <a href='%@'>this</a> if you want to check out what the restaurant has got to offer."),currentRestaurant.title,currentReservation.bookDate,currentReservation.bookTime,[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],currentRestaurant.seoKeyword]];
        NSString *shareContentString = [NSString stringWithFormat:@"<a href=\"%@\">%@</a>",[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],currentRestaurant.seoKeyword],[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],currentRestaurant.seoKeyword]];
        
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:[NSString stringWithFormat:L(@"You're Invited To %@"),currentRestaurant.title]];
        [mailer setMessageBody:shareContentString isHTML:YES];
        [self presentViewController:mailer animated:YES completion:nil];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Oops! This device does not support email."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
    }
}

-(void)shareWithSMS{
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Oops! This device does not support sms."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        return;
    }
    [IMGAmplitudeUtil trackBookWithName:@"SH - Share Reservation" andRestaurantId:nil andReservationId:reservation.reservationId andLocation:@"Booking Confirmation Page" andChannel:@"SMS"];
    //IMGReservation *currentReservation=reservation;
    IMGRestaurant *currentRestaurant=restaurant;
//    NSString *shareContentString = [NSString stringWithFormat:L(@"I just made a booking for us at %@ on %@ %@. Visit %@ if you want to check out what the restaurant has got to offer."),currentRestaurant.title,currentReservation.bookDate,currentReservation.bookTime,[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],currentRestaurant.seoKeyword]];
    
    NSString *shareContentString = [NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],currentRestaurant.seoKeyword];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setBody:shareContentString];
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
//            [MixpanelHelper trackShareToEmailWithshareObject:restaurant.title andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
            break;
        case MFMailComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:L(@"Failure") message:L(@"Failed to send E-Mail!") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
        }
            break;
            
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:L(@"Failure") message:L(@"Failed to send SMS!") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
//            [MixpanelHelper trackShareToSMSWithshareObject:restaurant.title andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


 

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
