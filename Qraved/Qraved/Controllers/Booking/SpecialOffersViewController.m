//
//  SpecialOffersViewController.m
//  Qraved
//
//  Created by Laura on 14-8-6.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "SpecialOffersViewController.h"

#import <CoreText/CoreText.h>
#import "UIConstants.h"
#import "EScrollerView.h"
#import "AppDelegate.h"

#import "BookingInfoViewController.h"
#import "SelectPaxViewController.h"

#import "NSString+Helper.h"
#import "UILabel+Helper.h"
#import "UIImageView+Helper.h"
#import "UIImageView+WebCache.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "UIColor+Hex.h"
#import "NSDate+Helper.h"
#import "Label.h"
#import "UIViewController+Helper.h"
#import "UIDevice+Util.h"
#import "UIImage+Helper.h"
#import "CustomIOS7AlertView.h"
#import "BookUtil.h"
#import "UIBookTimeButton.h"
#import "UIBookTimeLabel.h"

#import "DBManager.h"

#import "IMGRestaurant.h"
#import "IMGRestaurantOffer.h"
#import "IMGSetting.h"
#import "RestaurantHandler.h"
#import "IMGRestaurantImage.h"
#import "DetailViewController.h"

#import "NSString+Helper.h"
#import "LoadingView.h"
#import "DetailDataHandler.h"


@interface SpecialOffersViewController ()
{
    UIScrollView *_scrollView;
    
    IMGRestaurant *restaurant;
    NSArray *regularTimeArray;
    NSMutableArray *restaurantOfferArray;
    
    int webViewCount;
    int webViewLoadFinishedCount;
    
    NSMutableDictionary *timeSlotsArrayDictionary;
    NSMutableArray *seeMenuOfferArray;
    NSMutableArray *newOfferBadgeArrM;
    
    NSTimeInterval beginLoadingData;
    NSTimeInterval finishUI;
    CGFloat offsetY;
    NSMutableArray *originYArrs;
    int numIndex;
}
@end

@implementation SpecialOffersViewController
#pragma mark init method
-(id)initWithRestaurant: (IMGRestaurant *)pRestaurant andTimeArray:(NSArray *)timeArray andOfferArray:(NSArray *)offerArray{
    self = [super init];
    if (self) {
        restaurant = pRestaurant;
        // restaurant.bookStatus = @1;
        regularTimeArray = timeArray;
        restaurantOfferArray = [NSMutableArray arrayWithArray:offerArray];
        timeSlotsArrayDictionary = [[NSMutableDictionary alloc]initWithCapacity:0];
        webViewCount = 0;
        webViewLoadFinishedCount = 0;
        self.shopStr = pRestaurant.title;
        self.iconImage = pRestaurant.imageUrl;
        self.detailStr = [NSString stringWithFormat:@"%@ · %@",pRestaurant.cuisineName,pRestaurant.districtName];
    }
    return self;
}

-(void)initRestaurantOfferFromLocal:(NSDate *)bookDate{
    NSString *currentDateStr;
    NSDateFormatter *formatter = [[NSDateFormatter  alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    if(bookDate==nil){
        NSDate *date = [NSDate date];
        currentDateStr = [formatter stringFromDate:date];
    }else
        currentDateStr = [formatter stringFromDate:bookDate];
    
    if(restaurantOfferArray!=nil){
        [restaurantOfferArray removeAllObjects];
    }else{
        restaurantOfferArray=[[NSMutableArray alloc]initWithCapacity:0];
    }
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGRestaurantOffer WHERE restaurantId = '%@';",restaurant.restaurantId] successBlock:^(FMResultSet *resultSet) {
        while ([resultSet next]) {
            IMGRestaurantOffer  *tmpRestaurantOffer=[[IMGRestaurantOffer alloc]init];
            [tmpRestaurantOffer setValueWithResultSet:resultSet];
            [restaurantOfferArray addObject:tmpRestaurantOffer];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"initRestaurantOfferFromLocal error when get datas:%@",error.description);
    }];
    
}
#pragma mark - life cycle
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = L(@"Promo");
    [super viewWillAppear:YES];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[LoadingView sharedLoadingView] stopLoading];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // [self setBackBarButtonOffset30];
    self.screenName = @"Restaurant Promo list page";
    beginLoadingData = [[NSDate date]timeIntervalSince1970];
    newOfferBadgeArrM = [[NSMutableArray alloc] init];
    [self addBackButton];
    
    [[LoadingView sharedLoadingView] startLoading];
    if (self.isFromHomePromo) {
        [DetailDataHandler getRestaurantFirstDataFromServiceNow:restaurant.restaurantId andBlock:^(IMGRestaurant *restaurantpromo, NSArray *eventList, IMGRestaurantOffer *restaurantOffer, NSArray *banerImageArr, IMGRDPRestaurant *rdpRest, NSNumber *showClaim,NSArray *couponArray) {
            restaurant=restaurantpromo;
            [RestaurantHandler geOfferFromServer:restaurant.restaurantId andBookDate:self.bookDate andBlock:^(NSMutableArray *offerArray) {
//                [self initRestaurantOfferFromLocal:self.bookDate];
                [restaurantOfferArray addObjectsFromArray:offerArray];
                [self addMainView];
                
                [[LoadingView sharedLoadingView] stopLoading];
                finishUI = [[NSDate date]timeIntervalSince1970];
                float duringTime = finishUI - beginLoadingData;
                duringTime = duringTime * 1000;
                int duringtime = duringTime;
                [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createTimingWithCategory:@"see offer"  interval:[NSNumber numberWithInt:duringtime] name:@"" label:@""] build]];
            }];

        }];

    }else{
        [RestaurantHandler geOfferFromServer:restaurant.restaurantId andBookDate:self.bookDate andBlock:^(NSMutableArray *offerArray) {
            //[self initRestaurantOfferFromLocal:self.bookDate];
            [restaurantOfferArray addObjectsFromArray:offerArray];
            [self addMainView];
            
            [[LoadingView sharedLoadingView] stopLoading];
            finishUI = [[NSDate date]timeIntervalSince1970];
            float duringTime = finishUI - beginLoadingData;
            duringTime = duringTime * 1000;
            int duringtime = duringTime;
            [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createTimingWithCategory:@"see offer"  interval:[NSNumber numberWithInt:duringtime] name:@"" label:@""] build]];
        }];

    
    }

    
    self.view.backgroundColor = [UIColor defaultColor];
    if (self.amplitudeType != nil) {
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        [param setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
        [param setValue:restaurant.imageUrl forKey:@"Restaurant image"];
        [param setValue:restaurant.title forKey:@"Restaurant name"];
        [param setValue:restaurant.cuisineName forKey:@"Cuisine"];
        [param setValue:restaurant.districtName forKey:@"district"];
        if (restaurant.priceLevel != nil) {
            NSString *dollar=nil;
            switch(restaurant.priceLevel.intValue){
                case 1:dollar=@"Below 100K";break;
                case 2:dollar=@"100K - 200K";break;
                case 3:dollar=@"200K - 300K";break;
                case 4:dollar=@"Start from 300K";break;
            }
            [param setValue:dollar forKey:@"Price range"];
        }else if (restaurant.priceName != nil) {
            [param setValue:restaurant.priceName forKey:@"Price range"];
        }
        
        [param setValue:self.amplitudeType forKey:@"Location"];
        [[Amplitude instance] logEvent:@"RC - View Restaurant Offer detail" withEventProperties:param];
    }
}
#pragma mark add subviews
-(void)addBackButton
{
    UIBarButtonItem *leftBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:NavigationBackImage] style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
    leftBtn.tintColor=[UIColor blackColor];
    self.navigationItem.leftBarButtonItem=leftBtn;
    
}

-(void)addMainView
{
    //标题下阴影
    UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:0 andHeight:4];
    [self.view addSubview:shadowImageView];
    
    if (self.pax!=nil&&self.bookDate!=nil&&self.bookTime!=nil&&![self.bookTime isEqualToString:@""]) {
        
        UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 45)];
        topView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:topView];
        
        NSString *dinersStr = [NSString stringWithFormat:L(@"%d PAX"),[self.pax intValue]];
        UILabel *lblPAX = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth/3, 45)];
        //            lblPAX.backgroundColor = [UIColor yellowColor];
        lblPAX.textAlignment = NSTextAlignmentCenter;
        lblPAX.text = dinersStr;
        lblPAX.textColor = [UIColor blackColor];
        lblPAX.font = [UIFont systemFontOfSize:14];
        [topView addSubview:lblPAX];
        
        UILabel *lblDATE = [[UILabel alloc] initWithFrame:CGRectMake(lblPAX.right, 0, DeviceWidth/3, 45)];
        //            lblDATE.backgroundColor = [UIColor redColor];
        lblDATE.textAlignment = NSTextAlignmentCenter;
        lblDATE.text = [NSString stringWithFormat:@"%@",[self.bookDate getDayMonthString]];
        lblDATE.textColor = [UIColor blackColor];
        lblDATE.font = [UIFont systemFontOfSize:14];
        [topView addSubview:lblDATE];
        
        UILabel *lblTIME = [[UILabel alloc] initWithFrame:CGRectMake(lblDATE.right, 0, DeviceWidth/3, 45)];
        //            lblTIME.backgroundColor = [UIColor cyanColor];
        lblTIME.textAlignment = NSTextAlignmentCenter;
        lblTIME.text = self.bookTime;
        lblTIME.textColor = [UIColor blackColor];
        lblTIME.font = [UIFont systemFontOfSize:14];
        [topView addSubview:lblTIME];
        
//        currectPointY = titleInOfferView.endPointY+15;
//
//        [offerView addSubview:titleInOfferView];
        
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, topView.endPointY, DeviceWidth, DeviceHeight-topView.endPointY)];
        _scrollView.contentSize = CGSizeMake(DeviceWidth, DeviceHeight);
        [self.view addSubview:_scrollView];
        
    }else{
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
        _scrollView.contentSize = CGSizeMake(DeviceWidth, DeviceHeight);
        [self.view addSubview:_scrollView];
    }

    
    seeMenuOfferArray = [[NSMutableArray alloc]init];
    CGFloat startYY = 0.0;
    //if (self.pax!=nil&&self.bookDate!=nil&&self.bookTime!=nil&&![self.bookTime isEqualToString:@""]) {
//        
//        NSArray *arr = @[[NSString stringWithFormat:L(@"%d PAX"),[self.pax intValue]],[self.bookDate getDayMonthString],self.bookTime];
//        for (int i=0; i<3; i++) {
//            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//            float offset = (DeviceWidth - 95*3)/4.0;
//            button.frame = CGRectMake(offset + (offset+95)*i  , 0, 95, 60);
//            button.tag = 10000;
//            [topView addSubview:button];
//            if (i==0) {
//                NSString *dinersStr = [arr objectAtIndex:i];
//                NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc]initWithString:dinersStr];
//                
//                NSRange range = [dinersStr rangeOfString:[NSString stringWithFormat:@"%d",[self.pax intValue]]];
//                
//                NSRange range1 = [dinersStr rangeOfString:dinersStr];
//                CTFontRef font = CTFontCreateWithName((CFStringRef)[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14].fontName, 14, NULL);
//                CTFontRef font1 = CTFontCreateWithName((CFStringRef)[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:20].fontName, 20, NULL);
//                
//                [attStr addAttribute:(id)kCTFontAttributeName value:(id)CFBridgingRelease(font) range:range1];
//                [attStr addAttribute:(id)kCTFontAttributeName value:(id)CFBridgingRelease(font1) range:range];
//                [attStr addAttribute:(id)kCTForegroundColorAttributeName value:(id)[UIColor blackColor].CGColor range:range1];
//                
//                CGSize maxSize = [dinersStr sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16]];
//                
//                TYAttributedLabel *dinerNumLabel = [[TYAttributedLabel alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 60/2-(maxSize.height+4)/2, maxSize.width, maxSize.height+4)];
//                dinerNumLabel.backgroundColor = [UIColor clearColor];
////                [dinerNumLabel setAttributedString:attStr];
//                dinerNumLabel.attributedText = attStr;
//                [button addSubview:dinerNumLabel];
//            }
//            else
//            {
//                [button setTitle:[arr objectAtIndex:i] forState:UIControlStateNormal];
//                [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//                button.titleLabel.font =[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:20];
//            }
//        }
//        UIImageView *shadowView=[[UIImageView alloc]initShadowImageViewWithShadowOriginY:topView.frame.size.height andHeight:8];
//        [topView addSubview:shadowView];
//    }else{
//        startYY = 0;
//    }
    
    int peopleCount = 1;
    if(self.bookDate!=nil&&self.bookTime!=nil&&self.pax!=nil&&![self.bookTime isEqualToString:@""]){
        peopleCount = [self.pax intValue];
    }
    
    for (int offerArrayIndex=0; offerArrayIndex<restaurantOfferArray.count; offerArrayIndex++) {
        CGFloat currectPointY=0;
        IMGRestaurantOffer  *restaurantOffer= [restaurantOfferArray objectAtIndex:offerArrayIndex];
        
        UIView *offerView = [[UIView alloc]init ];
        offerView.backgroundColor = [UIColor whiteColor];
        offerView.tag = 1000;
        [_scrollView addSubview:offerView];
        
//        if (offerArrayIndex>0) {
//            UIImageView *shadowInOfferView=[[UIImageView alloc]initShadowImageViewWithShadowOriginY:-8 andHeight:8];
//            [offerView addSubview:shadowInOfferView];
//        }
        
//        UIView *titleInOfferView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 7.5+45)];
//        titleInOfferView.backgroundColor = [UIColor redColor];
//        titleInOfferView.tag = 0;
       
//        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 7.5, 45, 45)];
////        NSString *offerType = [NSString stringWithFormat:@"%@",restaurantOffer.offerType ];
////        if([restaurantOffer.offerType intValue]==0 || [restaurantOffer.offerType intValue]==1 || [restaurantOffer.offerType intValue]==2 || [restaurantOffer.offerType intValue]==3 || [restaurantOffer.offerType intValue] == 4){
////            imageView.image = [UIImage imageNamed:[@"BookFlowOfferTypeTag" stringByAppendingString:offerType]];
////        }else{
////            imageView.image = [UIImage imageNamed:@"GeneralTag"];
////        }
//        NSURL *urlString;
//        if ([restaurantOffer.typeIcon hasPrefix:@"http"])
//        {
//            urlString = [NSURL URLWithString:[restaurantOffer.typeIcon filterHtml]];
//        }
//        else
//        {
//            urlString = [NSURL URLWithString:[restaurantOffer.typeIcon returnFullImageUrl]];
//        }
//        
//        __weak UIImageView *weakimageView = imageView;
//        
//        if ([restaurantOffer.typeIcon isEqualToString:@""]) {
//            
//            imageView.image = [UIImage imageNamed:@"BookFlowOfferTypeTag0"];
//          
//        }else{
//            [imageView sd_setImageWithURL:urlString placeholderImage:[UIImage imageNamed:@"BookFlowOfferTypeTag0"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                if (image.size.height>0&&image.size.width>0) {
//                    image = [image cutCircleImage];
//                    weakimageView.image=image;
//                }else{
//                    weakimageView.image=[UIImage imageNamed:@"BookFlowOfferTypeTag0"];
//                }
//            }];
//        }
//        [titleInOfferView addSubview:imageView];
       
        
                //标题放在大图片下面=======================
//        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, 7.5, DeviceWidth-LEFTLEFTSET-LEFTLEFTSET, 46)];
//        titleLabel.numberOfLines = 2;
//        titleLabel.font=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:16];
//        titleLabel.textColor = [UIColor colorC2060A];
//        titleLabel.backgroundColor = [UIColor blueColor];
//        if ([restaurantOffer.offerType intValue] == 0 && [restaurantOffer.offerSlotMaxDisc intValue]) {
//            titleLabel.text = [restaurantOffer.offerTitle stringByAppendingString:[NSString stringWithFormat:L(@"(%@%@ Off)"),restaurantOffer.offerSlotMaxDisc,@"%"]];
//        }
//        else
//        {
//            titleLabel.text = restaurantOffer.offerTitle;
//        }
//        titleLabel.lineBreakMode =  NSLineBreakByTruncatingTail;
//        
//        [titleInOfferView addSubview:titleLabel];
//        
//        if (restaurantOffer.hasNewOfferSign)
//        {
//            UIImageView *newOfferBadgeImageView = [[UIImageView alloc] init];
//            [newOfferBadgeImageView setImage:[UIImage imageNamed:@"newflag"]];
//            newOfferBadgeImageView.backgroundColor = [UIColor whiteColor];
//            newOfferBadgeImageView.frame = CGRectMake(DeviceWidth - 40, titleLabel.frame.origin.y + 6, 32, 32);
//            newOfferBadgeImageView.tag = offerArrayIndex;
//            [titleInOfferView addSubview:newOfferBadgeImageView];
//            [newOfferBadgeArrM addObject:newOfferBadgeImageView];
//            
//            titleLabel.frame=CGRectMake(LEFTLEFTSET, 7.5, DeviceWidth-LEFTLEFTSET-LEFTLEFTSET-40, 46);
//            
//            
//        }
//        
//        UILabel *taglineLabel=[[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET,titleLabel.endPointY - 3,DeviceWidth-LEFTLEFTSET*2,15)];
//        taglineLabel.numberOfLines=1;
//        taglineLabel.text=restaurantOffer.tagLine;
//        taglineLabel.font=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:12];
//        taglineLabel.textColor = [UIColor colorWithHexString:@"ffc000"];
//        taglineLabel.backgroundColor = [UIColor redColor]; //[UIColor clearColor]
//        [titleInOfferView addSubview:taglineLabel];
//
//        currectPointY = taglineLabel.endPointY+15;
//
        //======================
        
        if (restaurantOffer.offerImage.length >1)
        {
            NSArray *imageUrlArr = [restaurantOffer.offerImage componentsSeparatedByString:@","];
            NSMutableArray *imageUrlArrM = [[NSMutableArray alloc] init];
            for (NSString *str in imageUrlArr)
            {
                IMGRestaurantImage *image = [[IMGRestaurantImage alloc] init];
                //NSString* encodedUrl = [str stringByAddingPercentEscapesUsingEncoding:
                //NSASCIIStringEncoding];
                image.imageUrl = str;
                
                [imageUrlArrM addObject:image];
            }
            EScrollerView *banerImageView =[[EScrollerView alloc] initWithFrameRect:CGRectMake(0, currectPointY, DeviceWidth, [restaurantOffer.offerImageH floatValue]) objectArray:imageUrlArrM isStartScroll:YES withPhotoTag:0 pageControlCenter:YES isCirculation:NO isPageControl:YES isEvent:NO isPlaceHolder:YES];
            [offerView addSubview:banerImageView];
            currectPointY = banerImageView.endPointY + 15;
        }
        //===================================
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, currectPointY, DeviceWidth-LEFTLEFTSET-LEFTLEFTSET, 46)];
        titleLabel.numberOfLines = 2;
        titleLabel.font=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:16];
        titleLabel.textColor = [UIColor colorC2060A];
        titleLabel.backgroundColor = [UIColor clearColor];
        if ([restaurantOffer.offerType intValue] == 0 && [restaurantOffer.offerSlotMaxDisc intValue]) {
            titleLabel.text = [restaurantOffer.offerTitle stringByAppendingString:[NSString stringWithFormat:L(@"(%@%@ Off)"),restaurantOffer.offerSlotMaxDisc,@"%"]];
        }
        else
        {
            titleLabel.text = restaurantOffer.offerTitle;
        }
        titleLabel.lineBreakMode =  NSLineBreakByTruncatingTail;
        
        [offerView addSubview:titleLabel];
        
        if (restaurantOffer.hasNewOfferSign)
        {
            UIImageView *newOfferBadgeImageView = [[UIImageView alloc] init];
            [newOfferBadgeImageView setImage:[UIImage imageNamed:@"newflag"]];
            newOfferBadgeImageView.backgroundColor = [UIColor whiteColor];
            newOfferBadgeImageView.frame = CGRectMake(DeviceWidth - 40, titleLabel.frame.origin.y + 6, 32, 32);
            newOfferBadgeImageView.tag = offerArrayIndex;
            [offerView addSubview:newOfferBadgeImageView];
            [newOfferBadgeArrM addObject:newOfferBadgeImageView];
            
            titleLabel.frame=CGRectMake(LEFTLEFTSET, 7.5, DeviceWidth-LEFTLEFTSET-LEFTLEFTSET-40, 46);
            
            
        }
        
        UILabel *taglineLabel=[[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET,titleLabel.endPointY - 3,DeviceWidth-LEFTLEFTSET*2,15)];
        taglineLabel.numberOfLines=1;
        taglineLabel.text=restaurantOffer.tagLine;
        taglineLabel.font=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:12];
        taglineLabel.textColor = [UIColor colorWithHexString:@"ffc000"];
        taglineLabel.backgroundColor = [UIColor clearColor];
        [offerView addSubview:taglineLabel];
        
        currectPointY = taglineLabel.endPointY+15;
//===================================
        
        
        if(![restaurantOffer.offerDescription isKindOfClass:[NSNull class]]&&![restaurantOffer.offerDescription isEqualToString:@""])
        {
            UIWebView *descriptionInOfferView=[[UIWebView alloc] initWithFrame:CGRectMake(15, currectPointY, DeviceWidth-2*15, 50)];
            webViewCount++;
            [descriptionInOfferView setBackgroundColor:[UIColor defaultColor]];
            [descriptionInOfferView setDelegate:self];
            [descriptionInOfferView loadHTMLString:[[restaurantOffer.offerDescription html] changeStyle] baseURL:nil];
            descriptionInOfferView.scalesPageToFit = YES;
            descriptionInOfferView.tag = 1;
            descriptionInOfferView.scrollView.scrollEnabled = NO;
            
            [offerView addSubview:descriptionInOfferView];
            currectPointY=descriptionInOfferView.endPointY+15;
        }
        
        if([restaurantOffer.offerType intValue]==1){
            [seeMenuOfferArray addObject:restaurantOffer];
            NSMutableAttributedString *mabstring = [[NSMutableAttributedString alloc]initWithString:L(@"See Menu")];
            [mabstring addAttribute:(id)kCTUnderlineStyleAttributeName value:(id)[NSNumber numberWithInt:kCTUnderlineStyleSingle] range:NSMakeRange(0, 8)];
            [mabstring addAttribute:(id)kCTForegroundColorAttributeName value:(id)[UIColor colorC2060A].CGColor range:NSMakeRange(0, 8)];
            
            TYAttributedLabel *menuLabel = [[TYAttributedLabel alloc]initWithFrame:CGRectMake(0, 0, 60, 15) ];
            menuLabel.backgroundColor = [UIColor clearColor];
//            [menuLabel setLineBreakMode:NSLineBreakByWordWrapping];
//            menuLabel.numberOfLines = 0;
//            [menuLabel setAttributedString:mabstring];
            menuLabel.attributedText = mabstring;
            menuLabel.tag = 1000+seeMenuOfferArray.count;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(seeMenuLabelClick:)];
            [menuLabel addGestureRecognizer:tap];
            
            UIButton *menuInOfferView = [[UIButton alloc]initWithFrame:CGRectMake(15, currectPointY, 60, 15)];
            menuInOfferView.tag = 1000+seeMenuOfferArray.count;
            [menuInOfferView addTarget:self action:@selector(seeMenuButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            [menuInOfferView addSubview:menuLabel];
            [offerView addSubview:menuInOfferView];
            
            currectPointY=menuInOfferView.endPointY+10;
        }
        
        if(![restaurantOffer.offerTerms isKindOfClass:[NSNull class]] && ![restaurantOffer.offerTerms  isEqual:@""]&&[restaurantOffer.offerType intValue]==0)
        {
            UILabel *termTitleInOfferView = [[UILabel alloc]initWithFrame:CGRectMake(15, currectPointY, DeviceWidth-2*15, 30)];
            termTitleInOfferView.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
            termTitleInOfferView.text = L(@"TERMS");
            termTitleInOfferView.tag = 3;
            [offerView addSubview:termTitleInOfferView];
            currectPointY = termTitleInOfferView.endPointY + 10;
            
            
            IMGSetting *setting = [[IMGSetting alloc]init];
            [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGSetting WHERE key = 'generalfineprint' limit 1;"] successBlock:^(FMResultSet *resultSet) {
                if([resultSet next]) {
                    [setting setValueWithResultSet:resultSet];
                }
                [resultSet close];
            }failureBlock:^(NSError *error) {
                NSLog(@"addMenuView error when get datas:%@",error.description);
            }];
            
            NSString *offerTerm = [NSString stringWithFormat:@"%@\n%@",(restaurantOffer.offerTerms==nil || [[NSString stringWithFormat:@"%@",restaurantOffer.offerTerms] isBlankString])?@"":restaurantOffer.offerTerms,(setting.value==nil || [[NSString stringWithFormat:@"%@",setting.value] isBlankString])?@"":setting.value];
            UIWebView *termInOfferView=[[UIWebView alloc] initWithFrame:CGRectMake(15, currectPointY, DeviceWidth-2*15, 50)];
            webViewCount++;
            [termInOfferView setBackgroundColor:[UIColor defaultColor]];
            [termInOfferView setDelegate:self];
            [termInOfferView loadHTMLString:[[offerTerm html] changeStyle] baseURL:nil];
            termInOfferView.scalesPageToFit = YES;
            termInOfferView.tag = 4;
            termInOfferView.scrollView.scrollEnabled = NO;
            [offerView addSubview:termInOfferView];
            currectPointY=termInOfferView.endPointY+10;
            
        }else if ([restaurantOffer.offerType intValue]==0&&[restaurantOffer.offerTerms  isEqual:@""]){
        
            UILabel *termTitleInOfferView = [[UILabel alloc]initWithFrame:CGRectMake(15, currectPointY, DeviceWidth-2*15, 30)];
            termTitleInOfferView.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
            termTitleInOfferView.text = L(@"TERMS");
            termTitleInOfferView.tag = 3;
            [offerView addSubview:termTitleInOfferView];
            currectPointY = termTitleInOfferView.endPointY + 10;
            IMGSetting *setting = [[IMGSetting alloc]init];
            [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGSetting WHERE key = 'generalfineprint' limit 1;"] successBlock:^(FMResultSet *resultSet) {
                if([resultSet next]) {
                    [setting setValueWithResultSet:resultSet];
                }
                [resultSet close];
            }failureBlock:^(NSError *error) {
                NSLog(@"addMenuView error when get datas:%@",error.description);
            }];
            
            NSString *offerTerm = [NSString stringWithFormat:@"%@",(setting.value==nil || [[NSString stringWithFormat:@"%@",setting.value] isBlankString])?@"":setting.value];
            UIWebView *termInOfferView=[[UIWebView alloc] initWithFrame:CGRectMake(15, currectPointY, DeviceWidth-2*15, 50)];
            webViewCount++;
            [termInOfferView setBackgroundColor:[UIColor defaultColor]];
            [termInOfferView setDelegate:self];
            [termInOfferView loadHTMLString:[[offerTerm html] changeStyle] baseURL:nil];
            termInOfferView.scalesPageToFit = YES;
            termInOfferView.tag = 4;
            termInOfferView.scrollView.scrollEnabled = NO;
            [offerView addSubview:termInOfferView];
            currectPointY=termInOfferView.endPointY+10;
        
        }
        
        if([restaurant.bookStatus intValue]==1 && ([restaurant.state intValue]==1 || [restaurant.state intValue]==6)&&restaurantOffer.requireBooking){
            if(self.bookDate!=nil&&self.bookTime!=nil&&self.pax!=nil&&![self.bookTime isEqualToString:@""]){
                UIView *bookButtonInOfferView = [[UIView alloc]initWithFrame:CGRectMake(0, currectPointY, DeviceWidth, 65)];
                NSArray *offerTimeSlotsArray = [BookUtil selectOfferSlotBy:restaurantOffer andPeopleCount:peopleCount andDate:self.bookDate andStartTime:self.bookTime];
                for (int buttonIndex=0; buttonIndex<3; buttonIndex++) {
                    NSArray *timeSlot = [offerTimeSlotsArray objectAtIndex:buttonIndex];
                    NSString *bookAvailableString = [timeSlot objectAtIndex:0];
                    NSString *bookTimeString = [timeSlot objectAtIndex:1];
                    UIBookTimeButton *button = [UIBookTimeButton buttonWithType:UIButtonTypeCustom];
                    button.tag = offerArrayIndex;
                    button.bookDate = self.bookDate;
                    button.bookTime = bookTimeString;
                    button.pax = self.pax;
                    button.layer.cornerRadius = 5;
                    button.layer.masksToBounds = YES;
                    BOOL addTagAction=NO;
                   
                    
                    float offset = (DeviceWidth - 102*3)/4.0;
                    button.frame = CGRectMake(offset + (offset+102)*buttonIndex, 5, 100, 50);
                    [bookButtonInOfferView addSubview:button];
                    
                    UIBookTimeLabel *timeLabel = [[UIBookTimeLabel alloc]initWithFrame:CGRectMake(0, 8, 102, 20)];
                    timeLabel.text = bookTimeString;
                    timeLabel.tag = offerArrayIndex;
                    timeLabel.bookDate = self.bookDate;
                    timeLabel.bookTime = bookTimeString;
                    timeLabel.pax = self.pax;
                    timeLabel.backgroundColor = [UIColor clearColor];
                    timeLabel.font =[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:15];
                    timeLabel.textColor = [UIColor whiteColor];
                    timeLabel.textAlignment = NSTextAlignmentCenter;
                    [button addSubview:timeLabel];
                    
                    UIBookTimeLabel *bookLabel = [[UIBookTimeLabel alloc]initWithFrame:CGRectMake(0, timeLabel.endPointY, 102, 20)];
                    bookLabel.text = bookAvailableString;
                    bookLabel.tag = offerArrayIndex;
                    bookLabel.bookDate = self.bookDate;
                    bookLabel.bookTime = bookTimeString;
                    bookLabel.pax = self.pax;
                    bookLabel.backgroundColor = [UIColor clearColor];
                    bookLabel.font =[UIFont fontWithName:FONT_GOTHAM_LIGHT size:12];
                    bookLabel.textColor = [UIColor whiteColor];
                    bookLabel.textAlignment = NSTextAlignmentCenter;
                    [button addSubview:bookLabel];
                    
                    if(addTagAction==YES){
                        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(bookTimeLabelsTapped:)];
                        [tap setNumberOfTouchesRequired:1];
                        [bookLabel addGestureRecognizer:tap];
                        [timeLabel addGestureRecognizer:tap];
                    }
                    if ([bookAvailableString isEqualToString:L(@"Book Now")]) {
//                        [button setBackgroundImage:[UIImage imageNamed:@"BookNow"] forState:UIControlStateNormal];
                         [button setBackgroundColor:[UIColor colorWithRed:9/255.0f green:191/255.0f blue:211/255.0f alpha:1.0]];
                        [button addTarget:self action:@selector(bookTimeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
                        addTagAction=YES;
                    }else{
                        //                [button setBackgroundImage:[UIImage imageNamed:@"NotAvailable"] forState:UIControlStateNormal];
                        [button setBackgroundColor:[UIColor colorWithRed:241/255.0f green:241/255.0f blue:241/255.0f alpha:1.0]];
                        bookLabel.textColor = [UIColor colorWithRed:129/255.0f green:129/255.0f blue:129/255.0f alpha:1.0f];
                        timeLabel.textColor = [UIColor colorWithRed:129/255.0f green:129/255.0f blue:129/255.0f alpha:1.0f];
                    }
                }
                bookButtonInOfferView.tag = 5;
                currectPointY=bookButtonInOfferView.endPointY+10;
                [offerView addSubview:bookButtonInOfferView];
                
                if([offerTimeSlotsArray count]>3){
                    int tag=500+offerArrayIndex;
                    [timeSlotsArrayDictionary setObject:offerTimeSlotsArray forKey:[NSNumber numberWithInt:tag]];
                    NSMutableAttributedString *mabstring = [[NSMutableAttributedString alloc]initWithString:L(@"Other times")];
                    UIFont *font = [UIFont systemFontOfSize:14];
                    [mabstring addAttribute:(id)kCTFontAttributeName value:font range:NSMakeRange(0, 11)];
                    [mabstring addAttribute:(id)kCTUnderlineStyleAttributeName value:(id)[NSNumber numberWithInt:kCTUnderlineStyleSingle] range:NSMakeRange(0, 11)];
                    [mabstring addAttribute:(id)kCTForegroundColorAttributeName value:(id)[UIColor blackColor].CGColor range:NSMakeRange(0, 11)];
                    
                    TYAttributedLabel *otherTimeLabel = [[TYAttributedLabel alloc]initWithFrame:CGRectMake(0, 0, 80, 25) ];
                    otherTimeLabel.backgroundColor = [UIColor clearColor];
//                    [otherTimeLabel setLineBreakMode:NSLineBreakByWordWrapping];
//                    otherTimeLabel.numberOfLines = 0;
//                    [otherTimeLabel setAttributedString:mabstring];
                    otherTimeLabel.attributedText = mabstring;
                    otherTimeLabel.tag = tag;
                    otherTimeLabel.textColor = [UIColor colorWithRed:9/255.0f green:191/255.0f blue:211/255.0f alpha:1.0];
                    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(otherTimesTapped:)];
                    [otherTimeLabel addGestureRecognizer:tap];
                    
                    UIButton *otherTimeInOfferView = [[UIButton alloc]initWithFrame:CGRectMake(DeviceWidth/2-38, currectPointY, 76, 25)];
                    otherTimeInOfferView.tag = tag;
                    [otherTimeInOfferView addTarget:self action:@selector(otherTimesClicked:) forControlEvents:UIControlEventTouchUpInside];
                    [otherTimeInOfferView addSubview:otherTimeLabel];
                    currectPointY=otherTimeInOfferView.endPointY+16;
                    [offerView addSubview:otherTimeInOfferView];
                }
            }else if ([self isHadOfferSlotList:restaurantOffer.offerSlotList]&&restaurantOffer.requireBooking)
            {
                UIView *bookButtonInOfferView = [[UIView alloc]initWithFrame:CGRectMake(0, currectPointY, DeviceWidth, 65)];
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                button.tag = offerArrayIndex;
                [button addTarget:self action:@selector(bookButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//                [button setBackgroundImage:[UIImage imageNamed:@"BookNowWithoutText"] forState:UIControlStateNormal];
                button.backgroundColor = [UIColor colortextBlue];
                button.layer.cornerRadius = 10;
                button.layer.masksToBounds = YES;
                button.frame = CGRectMake(7, 5, DeviceWidth-14, 55);
                [bookButtonInOfferView addSubview:button];
                UILabel *bookLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, DeviceWidth-14, 25)];
                bookLabel.text = L(@"Book Now");
                bookLabel.backgroundColor = [UIColor clearColor];
                bookLabel.font =[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:15];
                bookLabel.textColor = [UIColor whiteColor];
                bookLabel.textAlignment = NSTextAlignmentCenter;
                [button addSubview:bookLabel];
                
                currectPointY=bookButtonInOfferView.endPointY+10;
                [offerView addSubview:bookButtonInOfferView];
                
            }
        }
        offerView.frame = CGRectMake(0, startYY, DeviceWidth, currectPointY);
        startYY = offerView.endPointY;
        _scrollView.contentSize = CGSizeMake(DeviceWidth, offerView.endPointY+100);
        if ([self.notificationOfferId intValue] && [self.notificationOfferId intValue] == [restaurantOffer.offerId intValue]) {
            [_scrollView setContentOffset:CGPointMake(0, offerView.frame.origin.y)];
        }
        if ([self.journalDetailOfferId intValue]&&[self.journalDetailOfferId intValue]==[restaurantOffer.offerId intValue]) {
             numIndex=offerArrayIndex;
            
        }
    }
    
    if([restaurant.bookStatus intValue]==1 && ([restaurant.state intValue]==1 || [restaurant.state intValue]==6)){
        UIView *regularBookingView = [[UIView  alloc]init];
        regularBookingView.backgroundColor = [UIColor whiteColor];
        regularBookingView.tag = 1000;
        [_scrollView addSubview:regularBookingView];
        if(restaurantOfferArray.count>0){
            UIImageView *shadowView1=[[UIImageView alloc]initShadowImageViewWithShadowOriginY:-8 andHeight:8];
            [regularBookingView addSubview:shadowView1];
        }
        
        float currectPointY=0;
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 7.5, 45, 45)];
        imageView.image = [UIImage imageNamed:@"RegularTag"];
        [regularBookingView addSubview:imageView];
        
        Label *regularBookingLabel = [[Label alloc]initWithFrame:CGRectMake(imageView.endPointX+LEFTLEFTSET, 15, DeviceWidth-30, 7.5+45)andTextFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:15] andTextColor:[UIColor color222222] andTextLines:0];
        regularBookingLabel.text = L(@"Regular Booking");
        regularBookingLabel.tag = 0;
        currectPointY = regularBookingLabel.endPointY;
        [regularBookingView addSubview:regularBookingLabel];
        
        IMGSetting *setting = [[IMGSetting alloc]init];
        [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGSetting WHERE key = 'generalfineprint' limit 1;"] successBlock:^(FMResultSet *resultSet) {
            if([resultSet next]) {
                [setting setValueWithResultSet:resultSet];
            }
            [resultSet close];
        }failureBlock:^(NSError *error) {
            NSLog(@"addMenuView error when get datas:%@",error.description);
        }];
        if(!(restaurant.discountContent==nil || [[NSString stringWithFormat:@"%@",restaurant.discountContent] isBlankString]) || !(setting.value==nil || [[NSString stringWithFormat:@"%@",setting.value] isBlankString]))
        {
            NSString *regularTerm = [NSString stringWithFormat:@"%@\n%@",(restaurant.discountContent==nil || [[NSString stringWithFormat:@"%@",restaurant.discountContent] isBlankString])?@"":restaurant.discountContent,(setting.value==nil || [[NSString stringWithFormat:@"%@",setting.value] isBlankString])?@"":setting.value];
            UILabel *termTitleInOfferView = [[UILabel alloc]initWithFrame:CGRectMake(15, 10+currectPointY, DeviceWidth-2*15, 30)];
            termTitleInOfferView.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
            termTitleInOfferView.text = L(@"TERMS");
            termTitleInOfferView.tag = 3;
            [regularBookingView addSubview:termTitleInOfferView];
            
            UIWebView *termInOfferView=[[UIWebView alloc] initWithFrame:CGRectMake(15, termTitleInOfferView.endPointY+15, DeviceWidth-2*15, 50)];
            webViewCount++;
            [termInOfferView setBackgroundColor:[UIColor defaultColor]];
            [termInOfferView setDelegate:self];
            [termInOfferView loadHTMLString:[[regularTerm html] changeStyle] baseURL:nil];
            termInOfferView.scalesPageToFit = YES;
            termInOfferView.tag = 4;
            termInOfferView.scrollView.scrollEnabled = NO;
            [regularBookingView addSubview:termInOfferView];
            currectPointY = termInOfferView.endPointY;
        }
        
        if(self.bookDate!=nil&&self.bookTime!=nil&&self.pax!=nil&&![self.bookTime isEqualToString:@""]){
            NSArray *regularTimeSlotsArray = [BookUtil selectRegularTimeSlotBy:peopleCount andDate:self.bookDate inRegularTimeArray:regularTimeArray andStartTime:self.bookTime];
            UIView *bookButtonView = [[UIView alloc]initWithFrame:CGRectMake(0, currectPointY, DeviceWidth, 85)];
            for (int i=0; i<3; i++) {
                NSArray *timeSlot = [regularTimeSlotsArray objectAtIndex:i];
                NSString *bookAvailableString = [timeSlot objectAtIndex:0];
                NSString *bookTimeString = [timeSlot objectAtIndex:1];
                UIBookTimeButton *button = [UIBookTimeButton buttonWithType:UIButtonTypeCustom];
                button.tag = restaurantOfferArray.count+i;
                button.bookDate = self.bookDate;
                button.bookTime = bookTimeString;
                button.pax = self.pax;
                button.layer.cornerRadius = 5;
                button.layer.masksToBounds = YES;
                BOOL addTagAction=NO;
              
                
                float offset = ((DeviceWidth - 102*3)/4.0);
                button.frame = CGRectMake(offset+(offset+102)*i, 5, 100, 50);
                [bookButtonView addSubview:button];
                
                UIBookTimeLabel *timeLabel = [[UIBookTimeLabel alloc]initWithFrame:CGRectMake(0, 8, 102, 20)];
                timeLabel.text = bookTimeString;
                timeLabel.tag = restaurantOfferArray.count+i;
                timeLabel.bookDate = self.bookDate;
                timeLabel.bookTime = bookTimeString;
                timeLabel.pax = self.pax;
                timeLabel.backgroundColor = [UIColor clearColor];
                timeLabel.font =[UIFont boldSystemFontOfSize:16];
                timeLabel.textColor = [UIColor whiteColor];
                timeLabel.textAlignment = NSTextAlignmentCenter;
                timeLabel.tag = restaurantOfferArray.count+i;
                [button addSubview:timeLabel];
                
                UIBookTimeLabel *bookLabel = [[UIBookTimeLabel alloc]initWithFrame:CGRectMake(0, timeLabel.endPointY, 102, 20)];
                bookLabel.text = bookAvailableString;
                bookLabel.tag = restaurantOfferArray.count+i;
                bookLabel.bookDate = self.bookDate;
                bookLabel.bookTime = bookTimeString;
                bookLabel.pax = self.pax;
                bookLabel.backgroundColor = [UIColor clearColor];
                bookLabel.font =[UIFont fontWithName:FONT_GOTHAM_LIGHT size:12];
                bookLabel.textColor = [UIColor whiteColor];
                bookLabel.textAlignment = NSTextAlignmentCenter;
                bookLabel.tag = restaurantOfferArray.count+i;
                [button addSubview:bookLabel];
                if(addTagAction==YES){
                    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(bookTimeLabelsTapped:)];
                    [tap setNumberOfTouchesRequired:1];
                    [bookLabel addGestureRecognizer:tap];
                    [timeLabel addGestureRecognizer:tap];
                }
                if ([bookAvailableString isEqualToString:L(@"Book Now")]) {
//                    [button setBackgroundImage:[UIImage imageNamed:@"BookNow"] forState:UIControlStateNormal];
                     [button setBackgroundColor:[UIColor colorWithRed:9/255.0f green:191/255.0f blue:211/255.0f alpha:1.0]];
                    [button addTarget:self action:@selector(bookTimeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
                    addTagAction=YES;
                }else{
                    //                [button setBackgroundImage:[UIImage imageNamed:@"NotAvailable"] forState:UIControlStateNormal];
                    [button setBackgroundColor:[UIColor colorWithRed:241/255.0f green:241/255.0f blue:241/255.0f alpha:1.0]];
                    bookLabel.textColor = [UIColor colorWithRed:129/255.0f green:129/255.0f blue:129/255.0f alpha:1.0f];
                    timeLabel.textColor = [UIColor colorWithRed:129/255.0f green:129/255.0f blue:129/255.0f alpha:1.0f];
                }
            }
            bookButtonView.tag = 5;
            currectPointY = bookButtonView.endPointY;
            [regularBookingView addSubview:bookButtonView];
            
            if([regularTimeSlotsArray count]>3){
                NSInteger tag = 500+restaurantOfferArray.count;
                [timeSlotsArrayDictionary setObject:regularTimeSlotsArray forKey:[NSNumber numberWithLong:tag]];
                NSMutableAttributedString *mabstring = [[NSMutableAttributedString alloc] initWithString:L(@"Other times")];
                UIFont *font = [UIFont systemFontOfSize:14];
                [mabstring addAttribute:(id)kCTFontAttributeName value:font range:NSMakeRange(0, 11)];
                [mabstring addAttribute:(id)kCTUnderlineStyleAttributeName value:(id)[NSNumber numberWithInt:kCTUnderlineStyleSingle] range:NSMakeRange(0, 11)];
                [mabstring addAttribute:(id)kCTForegroundColorAttributeName value:(id)[UIColor blackColor].CGColor range:NSMakeRange(0, 11)];
                
                TYAttributedLabel *otherTimeLabel = [[TYAttributedLabel alloc]initWithFrame:CGRectMake(0, 0, 80, 20) ];
                otherTimeLabel.backgroundColor = [UIColor clearColor];
//                [otherTimeLabel setLineBreakMode:NSLineBreakByWordWrapping];
//                otherTimeLabel.numberOfLines = 0;
//                [otherTimeLabel setAttributedString:mabstring];
                otherTimeLabel.attributedText = mabstring;
                otherTimeLabel.textColor = [UIColor colorWithRed:9/255.0f green:191/255.0f blue:211/255.0f alpha:1.0];
                otherTimeLabel.tag = tag;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(otherTimesTapped:)];
                [otherTimeLabel addGestureRecognizer:tap];
                
                
                UIButton *otherTimeButton = [[UIButton alloc]initWithFrame:CGRectMake(DeviceWidth/2-38, currectPointY+10, 76, 20)];
                otherTimeButton.tag = tag;
                [otherTimeButton addTarget:self action:@selector(otherTimesClicked:) forControlEvents:UIControlEventTouchUpInside];
                [otherTimeButton addSubview:otherTimeLabel];
                currectPointY = otherTimeButton.endPointY;
                [regularBookingView addSubview:otherTimeButton];
            }
        }else{
            UIView *bookButtonView = [[UIView alloc]initWithFrame:CGRectMake(0, currectPointY, DeviceWidth, 65)];
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.tag = restaurantOfferArray.count;
            [button addTarget:self action:@selector(bookButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//            [button setBackgroundImage:[UIImage imageNamed:@"BookNowWithoutText"] forState:UIControlStateNormal];
            button.backgroundColor = [UIColor colortextBlue];
            button.layer.cornerRadius = 10;
            button.layer.masksToBounds = YES;
            button.frame = CGRectMake(7, 5, DeviceWidth-14, 55);
            [bookButtonView addSubview:button];
            UILabel *bookLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 13, DeviceWidth-14, 25)];
            bookLabel.text = L(@"Book Now");
            bookLabel.backgroundColor = [UIColor clearColor];
            bookLabel.font =[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:15];
            bookLabel.textColor = [UIColor whiteColor];
            bookLabel.textAlignment = NSTextAlignmentCenter;
            [button addSubview:bookLabel];
            
            currectPointY=bookButtonView.endPointY+10;
            [regularBookingView addSubview:bookButtonView];
        }
        
        regularBookingView.frame = CGRectMake(0, startYY, DeviceWidth, currectPointY);
        
        _scrollView.contentSize = CGSizeMake(DeviceWidth, regularBookingView.endPointY+100);
        
    }
}
//}
#pragma mark - view click event
-(void)btnact:(UIButton*)btn{
    _scrollView.contentOffset=CGPointMake(0, offsetY);
}
-(void)otherTimesTapped:(UIGestureRecognizer*)gesture
{
    [self updateUIWithTag:gesture.view.tag];
    [gesture.view setHidden:YES];
}
-(void)backClick
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)otherTimesClicked:(UIButton*)button
{
    [self updateUIWithTag:button.tag];
    [button setHidden:YES];
}

-(void)updateUIWithTag:(NSInteger)tag
{
    NSMutableArray *subViewsArray = [[NSMutableArray alloc]init];
    //WithCapacity:restaurantOfferArray.count+1];
    for (UIView *view in _scrollView.subviews)
    {
        if (view.tag == 1000)
        {
            [subViewsArray addObject:view];
        }
    }
    NSInteger i=tag-500;
    if (i>=0) {
        UIView *view = [subViewsArray objectAtIndex:i];
        CGRect viewFrame = view.frame;
        float height=0;
        NSArray *timeSlotsArray = [timeSlotsArrayDictionary objectForKey:[NSNumber numberWithLong:tag]];
        
        for (int i=0; i<timeSlotsArray.count-3; i++) {
            NSArray *timeSlot = [timeSlotsArray objectAtIndex:i+3];
            NSString *bookAvailableString = [timeSlot objectAtIndex:0];
            NSString *bookTimeString = [timeSlot objectAtIndex:1];
            
            height = viewFrame.size.height - 45 +65*(i/3);
            
            UIBookTimeButton *button = [UIBookTimeButton buttonWithType:UIButtonTypeCustom];
            button.tag = tag-500;
            button.bookDate = self.bookDate;
            button.bookTime = bookTimeString;
            button.pax = self.pax;
            button.layer.cornerRadius = 5;
            button.layer.masksToBounds = YES;
            BOOL addTagAction = NO;
            
            float offset = (DeviceWidth - 102*3)/4.0;

            button.frame = CGRectMake(offset+(102+offset)*(i%3), height, 100, 50);
            
            [view addSubview:button];
            
            UIBookTimeLabel *timeLabel = [[UIBookTimeLabel alloc]initWithFrame:CGRectMake(0, 8, 102, 20)];
            timeLabel.text = bookTimeString;
            timeLabel.tag = i;
            timeLabel.bookDate = self.bookDate;
            timeLabel.bookTime = bookTimeString;
            timeLabel.pax = self.pax;
            timeLabel.backgroundColor = [UIColor clearColor];
            timeLabel.font =[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:15];
            timeLabel.textColor = [UIColor whiteColor];
            timeLabel.textAlignment = NSTextAlignmentCenter;
            [button addSubview:timeLabel];
            
            UIBookTimeLabel *bookLabel = [[UIBookTimeLabel alloc]initWithFrame:CGRectMake(0, timeLabel.endPointY, 102, 20)];
            bookLabel.text = bookAvailableString;
            bookLabel.tag = i;
            bookLabel.bookDate = self.bookDate;
            bookLabel.bookTime = bookTimeString;
            bookLabel.pax = self.pax;
            bookLabel.backgroundColor = [UIColor clearColor];
            bookLabel.font =[UIFont fontWithName:FONT_OPEN_SANS_LIGHT size:13];
            bookLabel.textColor = [UIColor whiteColor];
            bookLabel.textAlignment = NSTextAlignmentCenter;
            [button addSubview:bookLabel];
            if(addTagAction==YES){
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(bookTimeLabelsTapped:)];
                [tap setNumberOfTouchesRequired:1];
                [bookLabel addGestureRecognizer:tap];
                [timeLabel addGestureRecognizer:tap];
            }
            
            if ([bookAvailableString isEqualToString:L(@"Book Now")]) {
//                [button setBackgroundImage:[UIImage imageNamed:@"BookNow"] forState:UIControlStateNormal];
                 [button setBackgroundColor:[UIColor colorWithRed:9/255.0f green:191/255.0f blue:211/255.0f alpha:1.0]];
                [button addTarget:self action:@selector(bookTimeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
                addTagAction=YES;
            }else{
                //                [button setBackgroundImage:[UIImage imageNamed:@"NotAvailable"] forState:UIControlStateNormal];
                [button setBackgroundColor:[UIColor colorWithRed:241/255.0f green:241/255.0f blue:241/255.0f alpha:1.0]];
                bookLabel.textColor = [UIColor colorWithRed:129/255.0f green:129/255.0f blue:129/255.0f alpha:1.0f];
                timeLabel.textColor = [UIColor colorWithRed:129/255.0f green:129/255.0f blue:129/255.0f alpha:1.0f];
            }
        }
        
        view.frame = CGRectMake(0, viewFrame.origin.y, DeviceWidth, height+65);
        _scrollView.contentSize = CGSizeMake(DeviceWidth, view.endPointY+100);
        
        UIView * frameView;
        frameView = view;
        for (NSInteger j=i+1; j<subViewsArray.count; j++) {
            UIView *subView = [subViewsArray objectAtIndex:j];
            CGRect viewFrame = subView.frame;
            
            subView.frame = CGRectMake(0, frameView.endPointY+8, DeviceWidth, viewFrame.size.height);
            
            frameView = subView;
            _scrollView.contentSize = CGSizeMake(DeviceWidth, subView.endPointY+100);
        }
        //            break;
    }
    //    }
    
}



-(void)bookTimeLabelsTapped:(UITapGestureRecognizer*)gesture
{
    NSInteger tag = gesture.view.tag;
    UIBookTimeLabel *label=(UIBookTimeLabel *)gesture.view;
    IMGRestaurantOffer *restaurantOffer;
    if(tag<restaurantOfferArray.count){
        restaurantOffer = [restaurantOfferArray objectAtIndex:tag];
    }
    
    for (UIView *view in newOfferBadgeArrM)
    {
        if (view.tag == tag)
        {
            [view removeFromSuperview];
            break;
        }
    }
    
    
    BookingInfoViewController *bookingInfoViewController = [[BookingInfoViewController alloc]initWithRestaurant:restaurant andOffer:restaurantOffer];
    bookingInfoViewController.bookDate = label.bookDate;
    bookingInfoViewController.bookTime = label.bookTime;
    bookingInfoViewController.pax = label.pax;
    [self.navigationController pushViewController:bookingInfoViewController animated:YES];
}

-(void)bookButtonTapped:(UIButton*)button{
    NSInteger tag = button.tag;
    IMGRestaurantOffer *restaurantOffer;
    if(tag<restaurantOfferArray.count){
        restaurantOffer = [restaurantOfferArray objectAtIndex:tag];
        
    }
    
    for (UIView *view in newOfferBadgeArrM)
    {
        if (view.tag == tag)
        {
            [view removeFromSuperview];
            break;
        }
    }
    NSMutableDictionary* eventDic=[[NSMutableDictionary alloc]init];
    [eventDic setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
    [eventDic setValue:@"Reguler" forKey:@"Type"];
    if (restaurantOffer) {
        [eventDic setValue:@"With Offer" forKey:@"Type"];
        
    }
    
    [[Amplitude instance] logEvent:@" TR - Reservation Initiate" withEventProperties:eventDic];
    SelectPaxViewController *selectDinersViewController = [[SelectPaxViewController alloc]initWithRestaurant:restaurant andTimeArray:regularTimeArray andOfferArray:restaurantOfferArray andOffer:restaurantOffer];
    [self.navigationController pushViewController:selectDinersViewController animated:YES];
}

-(void)bookTimeButtonTapped:(UIBookTimeButton*)button
{
    NSInteger tag=button.tag;
    IMGRestaurantOffer *restaurantOffer;
    if(tag<restaurantOfferArray.count){
        restaurantOffer = [restaurantOfferArray objectAtIndex:tag];
    }
    
    for (UIView *view in newOfferBadgeArrM)
    {
        if (view.tag == tag)
        {
            [view removeFromSuperview];
            break;
        }
    }
    
    
    BookingInfoViewController *bookingInfoViewController = [[BookingInfoViewController alloc]initWithRestaurant:restaurant andOffer:restaurantOffer];
    bookingInfoViewController.bookDate = button.bookDate;
    bookingInfoViewController.bookTime = button.bookTime;
    bookingInfoViewController.pax = button.pax;
    bookingInfoViewController.iconImage = self.iconImage;
    bookingInfoViewController.detailStr =self.detailStr;
    bookingInfoViewController.shopStr = self.shopStr;
    [self.navigationController pushViewController:bookingInfoViewController animated:YES];
    
}
-(void)seeMenuLabelClick:(UITapGestureRecognizer*)button
{
    IMGRestaurantOffer *restaurantOffer = [seeMenuOfferArray objectAtIndex:button.view.tag - 1001];
    UIView *popUpView = [[UIView alloc]init];
    
    NSString *offerTitleStr = restaurantOffer.offerTitle;
    
    CGSize maxSize = [offerTitleStr sizeWithFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
    Label *seeMenuTitle = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 54/2-(maxSize.height+4)/2, DeviceWidth-4*LEFTLEFTSET, maxSize.height+4) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] andTextColor:[UIColor color333333] andTextLines:0];
    seeMenuTitle.text = offerTitleStr;
    seeMenuTitle.textAlignment = NSTextAlignmentCenter;
    [popUpView addSubview:seeMenuTitle];
    
    
    
    UIWebView *webView=[[UIWebView alloc] initWithFrame:CGRectMake(0, seeMenuTitle.endPointY, DeviceWidth-2*LEFTLEFTSET, 160)];
    webView.tag = 3000;
    [webView setBackgroundColor:[UIColor clearColor]];
    [webView setDelegate:self];
    
    [webView loadHTMLString:[[restaurantOffer.offerMenu html] changeStyle] baseURL:nil];
    
    webView.scalesPageToFit = YES;
    [popUpView addSubview:webView];
    
    popUpView.frame = CGRectMake(0, 0, DeviceWidth-2*LEFTLEFTSET, webView.endPointY);
    
    
    
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc]init];
    
    // Add some custom content to the alert view
    [alertView setContainerView:popUpView];
    
    //    [alertView setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, NSInteger buttonIndex) {
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
    
}

-(void)seeMenuButtonClick:(UIButton*)button
{
    IMGRestaurantOffer *restaurantOffer = [seeMenuOfferArray objectAtIndex:button.tag - 1001];
    UIView *popUpView = [[UIView alloc]init];
    
    NSString *offerTitleStr = restaurantOffer.offerTitle;
    
    CGSize maxSize = [offerTitleStr sizeWithFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
    Label *seeMenuTitle = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 54/2-(maxSize.height+4)/2, DeviceWidth-4*LEFTLEFTSET, maxSize.height+4) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] andTextColor:[UIColor color333333] andTextLines:0];
    seeMenuTitle.text = offerTitleStr;
    seeMenuTitle.textAlignment = NSTextAlignmentCenter;
    [popUpView addSubview:seeMenuTitle];
    
    
    
    UIWebView *webView=[[UIWebView alloc] initWithFrame:CGRectMake(0, seeMenuTitle.endPointY, DeviceWidth-2*LEFTLEFTSET, 160)];
    webView.tag = 3000;
    [webView setBackgroundColor:[UIColor clearColor]];
    [webView setDelegate:self];
    
    [webView loadHTMLString:[[restaurantOffer.offerMenu html] changeStyle] baseURL:nil];
    
    webView.scalesPageToFit = YES;
    [popUpView addSubview:webView];
    
    popUpView.frame = CGRectMake(0, 0, DeviceWidth-2*LEFTLEFTSET, webView.endPointY);
    
    
    
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc]init];
    
    // Add some custom content to the alert view
    [alertView setContainerView:popUpView];
    
    //    [alertView setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, NSInteger buttonIndex) {
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    if (webView.tag !=3000) {
        NSString *webViewString= [webView stringByEvaluatingJavaScriptFromString: @"document.body.offsetHeight"];
        int webViewStringHeight = [webViewString intValue]+20;
        webView.frame = CGRectMake(webView.frame.origin.x, webView.frame.origin.y, DeviceWidth-2*15, webViewStringHeight);
        if(++webViewLoadFinishedCount>=webViewCount){
            [self updateScrollViewUI];
        }
    }
    
}

-(void)updateScrollViewUI{
    NSArray *offerViews = [_scrollView subviews];
    if(offerViews!=nil){
        UIView *topView = [offerViews objectAtIndex:0];
        topView.frame = CGRectMake(topView.frame.origin.x, 0, topView.frame.size.width, topView.frame.size.height);
        CGFloat offerViewY = 0;
        int j=1;
        if (self.pax!=nil&&self.bookDate!=nil&&self.bookTime!=nil&&![self.bookTime isEqualToString:@""]) {
            offerViewY = topView.endPointY;
        }
        originYArrs=[[NSMutableArray alloc] init];
        for(;j<offerViews.count;j++){
            UIView *offerView = [offerViews objectAtIndex:j];
            
            NSArray *subViews = [offerView subviews];
            CGFloat nextSubViewY = 0;
            for(int i=0; i<subViews.count;i++){
                UIView *subView = [subViews objectAtIndex:i];
                
                UIButton *btn = (UIButton *)subView;
                if (btn.tag == 10000)
                {
                    btn.frame = CGRectMake(btn.frame.origin.x, btn.frame.origin.y, btn.frame.size.width, btn.frame.size.height);
                    nextSubViewY = btn.endPointY + 16;
                    continue;
                }
                if(![subView isKindOfClass:[UIImageView class]]){
                    
                    subView.frame = CGRectMake(subView.frame.origin.x, nextSubViewY, subView.frame.size.width, subView.frame.size.height);
                    if(subView.tag==2 || subView.tag==3 || subView.tag==4 || subView.tag==5){
                        nextSubViewY = subView.endPointY+10;
                    }else if(subView.tag==0 || subView.tag==1){
                        nextSubViewY = subView.endPointY+15;
                    }else{
                        nextSubViewY = subView.endPointY+16;
                    }
                    
                }
                
            }
            
            offerView.frame = CGRectMake(0, offerViewY, DeviceWidth, nextSubViewY);
            if (offerView.tag == 1000) {
                [originYArrs addObject:[NSNumber numberWithFloat:offerViewY]];

            }
            offerViewY = offerView.endPointY+9;
            _scrollView.contentSize = CGSizeMake(DeviceWidth, offerView.endPointY+100);
        }
    }
    offsetY=[originYArrs[numIndex] floatValue];
    [self performSelector:@selector(btnact:) withObject:nil afterDelay:0.2f];
}

- (BOOL)isHadOfferSlotList:(NSString *)offerSlotList
{
    NSArray *offerSlotArr = [offerSlotList componentsSeparatedByString:@"|"];
    for (NSString *str in offerSlotArr)
    {
        if (![str isEqualToString:@"0"])
        {
            return YES;
        }
    }
    return NO;
}
//- (void)customIOS7dialogButtonTouchUpInside: (CustomIOS7AlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
//{
//    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
//    [alertView close];
//}

 


@end
