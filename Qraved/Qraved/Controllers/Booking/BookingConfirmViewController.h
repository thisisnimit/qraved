//
//  BookingConfirmViewController.h
//  Qraved
//
//  Created by Laura on 14-8-14.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "IMGReservation.h"
#import "IMGRestaurant.h"
#import <MessageUI/MessageUI.h>
#import "BookingInfoViewController.h"

@interface BookingConfirmViewController : BaseViewController<MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate>

@property (nonatomic,assign)NSInteger selectedPax;
@property (nonatomic,retain)NSDate *selectedBookDate;
@property (nonatomic,retain)NSString *selectedBookTime;
@property (nonatomic,retain)NSString *offerTitle;
@property (nonatomic,retain)BookingInfoViewController *bookInfoVC;

@property (nonatomic,assign) BOOL isFromProfile;

-(id)initWithReservation:(IMGReservation *)tmpReservation;

@end
