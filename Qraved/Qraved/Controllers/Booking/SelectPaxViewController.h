//
//  SelectDinersViewController.h
//  Qraved
//
//  Created by Laura on 14-8-13.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "IMGRestaurant.h"
#import "IMGRestaurantOffer.h"

@interface SelectPaxViewController : BaseChildViewController

@property (nonatomic,retain)  NSString  *iconImage;
@property (nonatomic,retain)  NSString  *shopStr;
@property (nonatomic,retain)  NSString  *detailStr;

-(id)initWithRestaurant: (IMGRestaurant *)pRestaurant andTimeArray:(NSArray *)timeArray andOfferArray:(NSArray *)offerArray andOffer:(IMGRestaurantOffer *)tmpRestaurantOffer;

@end
