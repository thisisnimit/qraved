//
//  SelectDinersViewController.m
//  Qraved
//
//  Created by Laura on 14-8-13.
//  Updated by Jeff on 14-10-21.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "SelectPaxViewController.h"
#import "SelectBookDateViewController.h"
#import "Label.h"
#import "UIView+Helper.h"
#import "UIViewController+Helper.h"
#import "UIColor+Helper.h"
#import "UIImageView+Helper.h"
#import "BookUtil.h"
#import "LoadingView.h"
#import "UIDevice+Util.h"
#import "DetailDataHandler.h"
#import "AppDelegate.h"
#import "BookHandler.h"
#import "NSString+Helper.h"
@interface SelectPaxViewController ()
{
    UIScrollView *scrollView;
    NSArray *customDates;
    
    IMGRestaurant *restaurant;
    IMGRestaurantOffer *restaurantOffer;
    NSArray *regularTimeArray;
    NSArray *restaurantOfferArray;
    
    int maxAvailableCurrentOfferPax;
    int maxAvailableOtherOfferpax;
    UIImageView *firstImageView;
    BOOL isButtonSelect;
}
@end

@implementation SelectPaxViewController
#pragma mark - init method
-(id)initWithRestaurant: (IMGRestaurant *)pRestaurant andTimeArray:(NSArray *)timeArray andOfferArray:(NSArray *)offerArray andOffer:(IMGRestaurantOffer *)tmpRestaurantOffer{
    self = [super init];
    if (self) {
        restaurant = pRestaurant;
        regularTimeArray = timeArray;
        restaurantOfferArray = offerArray;
        restaurantOffer=tmpRestaurantOffer;
        self.shopStr = pRestaurant.title;
        self.iconImage = pRestaurant.imageUrl;
        self.detailStr = [NSString stringWithFormat:@"%@ · %@",pRestaurant.cuisineName,pRestaurant.districtName];
        
    }
    return self;
}
#pragma mark - life cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"Reservation form page - select diners";
    self.view.backgroundColor = [UIColor whiteColor];
    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    [self.view addSubview:scrollView];
    [self setBackBarButtonOffset30];
    [[LoadingView sharedLoadingView] startLoading];
    [DetailDataHandler getBookInfoFromService:restaurant andBlock:^{
        [self loadMainView];
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = L(@"Let's find you a table");
    self.navigationController.navigationBarHidden = NO;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[LoadingView sharedLoadingView] stopLoading];
}
#pragma mark - add subviews
-(void)loadMainView
{
    //标题下阴影
    UIView *shadowImageView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 75)];
    shadowImageView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:shadowImageView];
    
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    imageV.centerX = shadowImageView.centerX;
    imageV.centerY = shadowImageView.centerY;
    imageV.backgroundColor = [UIColor colorWithRed:241/255.0f green:15/255.0f blue:51/255.0f alpha:1.0];
    imageV.layer.cornerRadius = imageV.height/2;
    imageV.layer.masksToBounds = YES;
    [shadowImageView addSubview:imageV];
    
    UIView *redLineView = [[UIView alloc] initWithFrame:CGRectMake(imageV.right, 0, DeviceWidth - imageV.left, 2)];
    redLineView.centerY = imageV.centerY;
    redLineView.backgroundColor = [UIColor colorWithRed:241/255.0f green:15/255.0f blue:51/255.0f alpha:1.0];
    [shadowImageView addSubview:redLineView];
    
    
    Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, imageV.bottom+15, DeviceWidth-2*LEFTLEFTSET, 20) andTextFont:[UIFont systemFontOfSize:14] andTextColor:[UIColor blackColor] andTextLines:1];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = L(@"PAX");
    [shadowImageView addSubview:titleLabel];
    
    int minAvailablePax = [restaurant.minimumPax intValue];
    int maxAvailablePax = [restaurant.maximumPax intValue];
    maxAvailableCurrentOfferPax = [BookUtil maxAvailablePaxFromOffer:restaurantOffer];
    maxAvailableOtherOfferpax = [BookUtil maxAvailablePaxNotFromOffer:restaurantOffer inOfferArray:restaurantOfferArray];
    UIButton *button;
    for (int j=minAvailablePax; j<=maxAvailablePax; j++) {
        button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:[NSString stringWithFormat:@"%d",j] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:18];
        
        if (j<=maxAvailableCurrentOfferPax) {
            [button setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];//old: CurrentOfferAvailable //new:  quan hong (zanshimeiyou)
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }else if(j<=maxAvailableOtherOfferpax && j>maxAvailableCurrentOfferPax){
            [button setBackgroundImage:[UIImage imageNamed:@"red_point"] forState:UIControlStateNormal];//hong dian
            [button setTitleColor:[UIColor colorWithRed:241/255.0f green:15/255.0f blue:51/255.0f alpha:1.0] forState:UIControlStateNormal];
        }else{
            [button setBackgroundImage:[UIImage imageNamed:@"white_Container"] forState:UIControlStateNormal];
            [button setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
        }
        button.tag = j;
        [button addTarget:self action:@selector(paxSelected:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(LEFTLEFTSET+(62+15)*((j-minAvailablePax)%4)* [AppDelegate ShareApp].autoSizeScaleX, shadowImageView.endPointY+70*((j-minAvailablePax)/4) +30, 62, 62);
//        button.layer.cornerRadius = button.height/2;
//        button.layer.masksToBounds = YES;
        [scrollView addSubview:button];
        scrollView.contentSize = CGSizeMake(DeviceWidth, button.endPointY+120);
    }
    
    UIView *explainView = [[UIView alloc]initWithFrame:CGRectMake(0, button.bottom +20, DeviceWidth, 47)];
    explainView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:explainView];
    
//    if (restaurantOffer==nil)
//    {
//        explainView.image = [UIImage imageNamed:@"BookFlowInformatOnTipsBottom_2.png"];
//    }
//    //    NSArray *imageArr = @[@"CurrentOfferAvailable",@"OtherOfferAvailable"];
//    NSArray *titleArr;
//    if(restaurantOffer!=nil){
//        titleArr = @[[NSString stringWithFormat:L(@"%@%@ Promo available"),restaurantOffer.offerSlotMaxDisc,@"%"],L(@"Other Promo")];
//    }else{
//        titleArr = @[@"",L(@"Other Promo")];
//    }
//    for (int i=0; i<titleArr.count; i++) {
//        CGSize expectSize = [[titleArr objectAtIndex:i] sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13]];
//        if (i==0) {
//            Label *label = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET+40, 47/2-expectSize.height/2, expectSize.width, expectSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor blackColor] andTextLines:1];
//            label.text = [titleArr objectAtIndex:i];
//            [explainView addSubview:label];
//        }
//        else
//        {
//            Label *label = [[Label alloc]initWithFrame:CGRectMake(DeviceWidth/3*2+10, 47/2-expectSize.height/2, expectSize.width, expectSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor blackColor] andTextLines:1];
//            label.text = [titleArr objectAtIndex:i];
//            [explainView addSubview:label];
//        }
//        
//    }
    UIView *pointView = [[UIView alloc] initWithFrame:CGRectMake(LEFTLEFTSET, 47/2 - 5/2, 5, 5)];
    pointView.backgroundColor = [UIColor redColor];
    pointView.layer.cornerRadius = pointView.height/2;
    pointView.layer.masksToBounds = YES;
    [explainView addSubview:pointView];
    
    CGSize expectSize = [L(@"Available Promo") sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13]];

    Label *label = [[Label alloc]initWithFrame:CGRectMake(pointView.right +5, 47/2-expectSize.height/2, expectSize.width, expectSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor lightGrayColor] andTextLines:1];
    label.text = L(@"Available Promo");
    [explainView addSubview:label];
}
#pragma mark - view clicked events
-(void)paxSelected:(UIButton *)button
{
    if (isButtonSelect)
    {
        return;
    }
    isButtonSelect = YES;
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy/MM/dd"];
    
    NSDate *startDate=[NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents*comps1;
    comps1 = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth |NSCalendarUnitDay | NSCalendarUnitWeekday) fromDate:startDate];

    
    //获得当前日期
//    long  currentDay = [comps1 day];

//    NSDate *endDate=[NSDate dateWithTimeIntervalSinceNow:(24*60*60*(63-currentDay))];
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:[dateFormatter stringFromDate:startDate],@"startDate",restaurant.restaurantId,@"restaurantID",[NSNumber numberWithInteger:button.tag],@"peopleCount",nil];
    
    [[LoadingView sharedLoadingView]startLoading];
    __block SelectPaxViewController *blockSelf = self;
    [BookHandler getMonthTimeWithParams:parameters andBlock:^(NSArray *returnArray) {
        [blockSelf goToSelectDate:button.tag withTimeArray:returnArray];
        isButtonSelect = NO;

    }];
}

-(void)goToSelectDate:(long)tag withTimeArray:(NSArray*)tmpRegularTimeArray{
    SelectBookDateViewController *selectBookDateViewController;
    if(tag<=maxAvailableCurrentOfferPax){
        selectBookDateViewController = [[SelectBookDateViewController alloc]initWithRestaurant:restaurant andTimeArray:tmpRegularTimeArray andOfferArray:restaurantOfferArray andOffer:restaurantOffer];
        selectBookDateViewController.whichTypeSelected=@"1";
    }else if(tag>maxAvailableCurrentOfferPax && tag<=maxAvailableOtherOfferpax){
        selectBookDateViewController = [[SelectBookDateViewController alloc]initWithRestaurant:restaurant andTimeArray:tmpRegularTimeArray andOfferArray:restaurantOfferArray andOffer:restaurantOffer];
        selectBookDateViewController.whichTypeSelected=@"2";
    }else{
        selectBookDateViewController = [[SelectBookDateViewController alloc]initWithRestaurant:restaurant andTimeArray:tmpRegularTimeArray andOfferArray:restaurantOfferArray andOffer:nil];
        selectBookDateViewController.whichTypeSelected=@"3";
    }
    selectBookDateViewController.pax = [NSNumber numberWithLong:tag];
    selectBookDateViewController.iconImage = self.iconImage;
    selectBookDateViewController.detailStr =self.detailStr;
    selectBookDateViewController.shopStr = self.shopStr;
    [self.navigationController pushViewController:selectBookDateViewController animated:YES];
}
@end
