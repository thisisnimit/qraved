//
//  BookingInfoViewController.h
//  Qraved
//
//  Created by Laura on 14-8-14.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "EnterEmailViewController.h"
#import "IMGRestaurant.h"
#import "IMGRestaurantOffer.h"

@interface BookingInfoViewController : BaseChildViewController<UITextFieldDelegate,registerDelegate>

@property (nonatomic,retain)NSNumber *pax;
@property (nonatomic,retain)NSDate *bookDate;
@property (nonatomic,retain)NSString *bookTime;

@property (nonatomic,retain)  NSString  *iconImage;
@property (nonatomic,retain)  NSString  *shopStr;
@property (nonatomic,retain)  NSString  *detailStr;

-(id)initWithRestaurant: (IMGRestaurant *)pRestaurant andOffer:(IMGRestaurantOffer *)tmpRestaurantOffer;

@end
