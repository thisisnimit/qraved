//
//  SelectTimeViewController.h
//  Qraved
//
//  Created by Laura on 14-8-13.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "IMGRestaurant.h"
#import "IMGRestaurantOffer.h"

@interface SelectBookTimeViewController : BaseChildViewController

@property (nonatomic,retain)NSNumber *pax;
@property (nonatomic,retain)NSDate *bookDate;

@property (nonatomic,retain)  NSString  *iconImage;
@property (nonatomic,retain)  NSString  *shopStr;
@property (nonatomic,retain)  NSString  *detailStr;

@property (nonatomic,retain)NSString *whichTypeSelected;//1,current offer. 2,other offer. 3,no offer

-(id)initWithRestaurant: (IMGRestaurant *)pRestaurant andTimeArray:(NSArray *)timeArray andOfferArray:(NSArray *)offerArray andOffer:(IMGRestaurantOffer *)tmpRestaurantOffer;

@end
