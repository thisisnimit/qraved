//
//  SpecialOffersViewController.h
//  Qraved
//
//  Created by Laura on 14-8-6.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "IMGRestaurant.h"

@interface SpecialOffersViewController : BaseChildViewController<UIWebViewDelegate>

@property (nonatomic,retain)NSNumber *pax;
@property (nonatomic,retain)NSDate *bookDate;
@property (nonatomic,retain)NSString *bookTime;
@property (nonatomic,retain)NSString *whichTypeSelected;//1,current offer. 2,other offer. 3,no offer
@property (nonatomic,strong)NSNumber *notificationOfferId;
@property (nonatomic,strong)NSNumber *journalDetailOfferId;
@property(nonatomic,copy)NSString* amplitudeType;
@property(nonatomic,assign)BOOL isFromHomePromo;

@property (nonatomic,retain)  NSString  *iconImage;
@property (nonatomic,retain)  NSString  *shopStr;
@property (nonatomic,retain)  NSString  *detailStr;
-(id)initWithRestaurant: (IMGRestaurant *)pRestaurant andTimeArray:(NSArray *)timeArray andOfferArray:(NSArray *)offerArray;

@end
