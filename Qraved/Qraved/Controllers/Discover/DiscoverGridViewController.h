//
//  GridViewController.h
//  Qraved
//
//  Created by Jeff on 8/5/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseChildViewController.h"
#import "EGORefreshTableHeaderView.h"

@interface DiscoverGridViewController : BaseChildViewController<UITableViewDataSource,UITableViewDelegate,EGORefreshTableHeaderDelegate>
@property (nonatomic,retain)NSMutableArray *discoverArray;
@end
