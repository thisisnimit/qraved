//
//  GridViewController.m
//  Qraved
//
//  Created by Jeff on 8/5/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "DiscoverGridViewController.h"
#import "IMGDiscover.h"
#import "Consts.h"
#import "UIConstants.h"
#import "LoadingImageView.h"
#import "LocationListViewController.h"
#import "UIColor+Helper.h"
#import "UIViewController+Helper.h"
#import "UIImageView+Helper.h"
#import "DiscoverGridCell.h"
#import "DiscoverGridCellView.h"
#import "UIImageButton.h"
#import "DiscoverTitleLabel.h"
#import "DBManager.h"
#import "IMGTag.h"
#import "IMGDiscoverEntity.h"
#import "EGORefreshTableHeaderView.h"
#import "RestaurantsViewController.h"
#import "SearchUtil.h"
#import "DiscoverUtil.h"
//#import "MixpanelHelper.h"
#import "TagHandler.h"

#define CELL_HEIGHT  105
#define REFRESH_HEADER_HEIGHT 52.0f
#define HEAD_VIEW_HEIGHT 80

@interface DiscoverGridViewController ()

{
    BOOL isLoading;
    EGORefreshTableHeaderView*_refreshTableHeaderView;
    LoadingImageView *loadingImageView;
}

@property(nonatomic,strong)UITableView *discoverCategoryTableView;
@property(nonatomic,strong)UIView *headView;

@end

@implementation DiscoverGridViewController
{
    
}
- (void)viewDidLoad{
    [super viewDidLoad];
    self.navigationItem.title = @"Quick Search";
    self.view.backgroundColor = [UIColor whiteColor];
    self.discoverArray = [[NSMutableArray alloc]init];
    [self setBackBarButtonOffset30];
    [self loadHeadView];
    [self loadTableView];
    [self addPullToRefreshHeader];
    [[TagHandler alloc] getDatasFromServer];
    [self refreshData];
}
#pragma mark - ego refresh table view
-(void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view{
    isLoading = YES;
    [self refreshData];
}
-(void)refreshData
{
    if (!isLoading) {
        loadingImageView = [[LoadingImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, self.discoverCategoryTableView.frame.size.height) andViewNum:1 andShadowWidth:0];
        [self.discoverCategoryTableView addSubview:loadingImageView];
        [loadingImageView startLoading];
    }
    [DiscoverUtil refreshDiscoverAndHandle:^(NSArray *discoverArray) {
        [self.discoverArray removeAllObjects];
        [self.discoverArray addObjectsFromArray:discoverArray];
        [self.discoverCategoryTableView  reloadData];
        if (isLoading) {
            isLoading = NO;
            [_refreshTableHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.discoverCategoryTableView];
        }else{
            [loadingImageView stopLoading];
        }
    }];
}
-(void)getDataFromSqlite{
    NSMutableArray* discoverArray = [[NSMutableArray alloc]init];
    [[DBManager manager]selectWithSql:@"select * from IMGDiscoverEntity" successBlock:^(FMResultSet *resultSet) {
        while ([resultSet next]) {
            IMGDiscoverEntity *discover=[[IMGDiscoverEntity alloc]init];
            [discover setValueWithResultSet:resultSet];
            IMGDiscover *tempDiscover = [[IMGDiscover alloc]init];
            [tempDiscover initFromDiscoverEntity:discover];
            [discoverArray addObject:tempDiscover];
        }
        _discoverArray = discoverArray;
        [self.discoverCategoryTableView  reloadData];
        [resultSet close];
    } failureBlock:^(NSError *error) {
        
    }];
}
- (void)addPullToRefreshHeader {
    if (_refreshTableHeaderView && [_refreshTableHeaderView superview]) {
        [_refreshTableHeaderView removeFromSuperview];
    }
    _refreshTableHeaderView =[[EGORefreshTableHeaderView alloc]initWithFrame:CGRectMake(0, - REFRESH_HEADER_HEIGHT, DeviceWidth, REFRESH_HEADER_HEIGHT) andNormalStatusString:@"Pull down for latest Discovers"];
    _refreshTableHeaderView.delegate=self;
    [_refreshTableHeaderView setBackgroundColor:[UIColor clearColor] textColor:nil arrowImage:[UIImage imageNamed:@"arrowDown.png"]];
    _refreshTableHeaderView.statusLabel.font=[UIFont boldSystemFontOfSize:12.0];
    [self.discoverCategoryTableView addSubview:_refreshTableHeaderView];
    [_refreshTableHeaderView refreshLastUpdatedDate];
}

- (void)loadHeadView
{
    self.headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, HEAD_VIEW_HEIGHT+5)];
    
    DiscoverTitleLabel *titleLabel = [[DiscoverTitleLabel alloc] initWithFrame:CGRectMake(25, -5, DeviceWidth-50, self.headView.frame.size.height ) title:@"What occasion are you looking for?"];
    [self.headView addSubview:titleLabel];
    [self.headView setBackgroundColor:[UIColor colorC2060A]];
    [self.view addSubview:self.headView];
    //标题下阴影
    UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:self.headView.frame.size.height andHeight:4];
    [self.headView addSubview:shadowImageView];
    
}

- (void)loadTableView
{
    self.discoverCategoryTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, self.headView.frame.size.height, DeviceWidth, DeviceHeight-50-HEAD_VIEW_HEIGHT) style:UITableViewStylePlain];
    self.discoverCategoryTableView.dataSource = self;
    self.discoverCategoryTableView.separatorColor = [UIColor clearColor];
    self.discoverCategoryTableView.delegate = self;
    self.discoverCategoryTableView.backgroundColor = [UIColor whiteColor];
    self.discoverCategoryTableView.showsVerticalScrollIndicator = NO;
    [self.view insertSubview:self.discoverCategoryTableView belowSubview:self.headView];
    
    //标题下阴影
    UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:self.headView.frame.size.height andHeight:4];
    [self.headView addSubview:shadowImageView];
}


- (void)popNextViewController:(NSNotification *)notification{
    NSNumber *discoverId=[notification.userInfo objectForKey:@"discoverId"];
    IMGDiscover *discover;
    for(IMGDiscover *obj in _discoverArray){
        if([obj.discoverId intValue]==[discoverId intValue]){
            discover=obj;
            discover.fromWhere = @3;
            RestaurantsViewController *restaurantsViewController=[[RestaurantsViewController alloc]initWithMap];
            [restaurantsViewController initDiscover:discover];
            [self.navigationController pushViewController:restaurantsViewController animated:YES];
//            [MixpanelHelper trackViewDiscoverWithDiscoverName:discover.name];
            break;
        }
    }
    //    LocationListViewController *locationListViewController = [[LocationListViewController alloc]initWithHead:discover];
    //    [self.navigationController pushViewController:locationListViewController animated:YES];
}


#pragma mark UITable datasource and delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _discoverArray.count/2+_discoverArray.count%2;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identifier = [[NSString alloc] initWithFormat:@"Cell%ld",(long)indexPath.row];
    
    DiscoverGridCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[DiscoverGridCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.selectedBackgroundView = [[UIView alloc] init];
        
        for (int i=0; i<2; i++) {
            
            if(indexPath.row*2+i<_discoverArray.count){
                IMGDiscover *tmpDiscover=_discoverArray[indexPath.row*2+i];
                
                DiscoverGridCellView *discoverGridCellView = [[DiscoverGridCellView alloc] initWithDiscover:tmpDiscover andRow:indexPath.row andColumn:i];
                [cell addSubview:discoverGridCellView];
            }
        }
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CELL_HEIGHT;
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    [_refreshTableHeaderView egoRefreshScrollViewDidEndDragging:self.discoverCategoryTableView];
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_refreshTableHeaderView egoRefreshScrollViewDidScroll:self.discoverCategoryTableView];
}
@end
