//
//  V2_DiscoverListResultFiltersViewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverListResultFiltersViewController.h"
#import "V2_DiscoverListResultFiltersSortsByTableViewCell.h"
#import "V2_DiscoverListResultFiltersAvailablityTableViewCell.h"
#import "V2_DiscoverListResultFiltersRatingTableViewCell.h"
#import "V2_DiscoverListResultFiltersCuisinesTableViewCell.h"
#import "V2_DiscoverListResultFiltersPriceTableViewCell.h"
#import "V2_DiscoverListResultFiltersFeaturesTableViewCell.h"
#import "V2_DiscoverListResultFiltersOhtersTableViewCell.h"
#import "IMGTag.h"
#import "IMGDiscover.h"
#import "IMGTagUpdate.h"
#import "V2_DiscoverListResultFiltersPromoTableViewCell.h"
#import "V2_DiscoverListResultFiltersDetailViewController.h"
#import "OfferTypeListViewController.h"
#import "AreaListViewController.h"
#import "CuisineListViewController.h"
#import "LocationListViewController.h"
#import "TagListViewController.h"
#import "FilterItemOfferCellView.h"
#import "FilterItemCellView.h"

@interface V2_DiscoverListResultFiltersViewController ()<UITableViewDelegate, UITableViewDataSource, V2_DiscoverListResultFiltersSortsByTableViewCellDelegate, V2_DiscoverListResultFiltersAvailablityTableViewCellDelegate, V2_DiscoverListResultFiltersRatingTableViewCellDelegate, V2_DiscoverListResultFiltersPriceTableViewCellDelegate, V2_DiscoverListResultFiltersCuisinesTableViewCellDelegate, V2_DiscoverListResultFiltersFeaturesTableViewCellDelegate>
{
    NSMutableArray *feartureArr;
    IMGDiscover *discover;
     IMGTagUpdate *featureTagUpdate;
    NSMutableArray * tagsArray;
    NSMutableArray *cuisineArray;
    NSMutableArray *tagUpdateArrM;
    NSMutableArray *tempArray;
    NSString * sortbyString;
    NSString * cuisineString;
    NSString * featureString;
    NSString * ratingsString;
    NSString * priceString;
    UIButton *btnApply;
    FilterType filterType;
}
@property (nonatomic, strong)  UITableView  *tableView;

@end

@implementation V2_DiscoverListResultFiltersViewController

- (void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    [[AppDelegate ShareApp].window addSubview:btnApply];
}
- (void)viewWillDisappear:(BOOL)animated{

    [super viewWillDisappear:animated];
    [btnApply removeFromSuperview];
}
-(id)initWithDiscover:(IMGDiscover *)paramDiscover{
    if (self = [super init]) {
        discover = paramDiscover;
        if(discover==nil){
            discover = [[IMGDiscover alloc]init];
        }
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor greenColor];
     [self addNotifications];
     [self addBackBtn];
    [self addRightItem];
    [self addUI];
    [self addApplyButton];
     [self loadFearureData];
    [self cuisineData];
    [self bottomData];
    tempArray = [[NSMutableArray alloc] init];
    
}
- (void)addApplyButton{

    btnApply = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnApply setTitle:@"APPLY" forState:UIControlStateNormal];
    [btnApply setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    btnApply.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    btnApply.backgroundColor = [UIColor whiteColor];
    [btnApply addTarget:self action:@selector(applyClick:) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:btnApply];
//    [self.view bringSubviewToFront:btnApply];
    [[AppDelegate ShareApp].window addSubview:btnApply];
    
    btnApply.sd_layout
    .bottomSpaceToView([AppDelegate ShareApp].window, 20)
    .centerXEqualToView([AppDelegate ShareApp].window)
    .widthIs(80)
    .heightIs(40);
    btnApply.layer.cornerRadius = 20;
    btnApply.layer.masksToBounds = YES;
    btnApply.layer.borderColor = [UIColor color234Gray].CGColor;
    btnApply.layer.borderWidth = 1;

}
- (void)applyClick:(UIButton *)applyClick{

    NSLog(@"apply");
    [self saveFilters];
}

- (void)saveFilters{
//    discover.priceLevelArray = priceLavels;
    [self performSelectorOnMainThread:@selector(createData) withObject:nil waitUntilDone:NO];
//    [self dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController popViewControllerAnimated:YES];

}

-(void)createData
{
   
    NSNotification *notification = [[NSNotification alloc] initWithName:SEARCH_RESTAURANT object:nil userInfo:@{@"price":priceString ? priceString : @"", @"sortby":sortbyString ? sortbyString : @"", @"rating":ratingsString ? ratingsString : @"", @"cuisines":cuisineString ? cuisineString : @"", @"feature":featureString ? featureString : @"",@"availablity":[NSNumber numberWithInt:filterType]}];
    if([self.fvcDelegate respondsToSelector:@selector(search:)])
    {
        [self.fvcDelegate search:notification];
//        [self dismissViewControllerAnimated:YES completion:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (void)addRightItem{

    UIBarButtonItem *ApplyBtn=[[UIBarButtonItem alloc] initWithTitle:@"Reset" style:UIBarButtonItemStyleDone target:self action:@selector(ApplyClick)];
    ApplyBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=ApplyBtn;
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor colortextBlue];
    [self.navigationItem.rightBarButtonItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont boldSystemFontOfSize:17],NSFontAttributeName, nil] forState:UIControlStateNormal];
}
- (void)ApplyClick{

    NSLog(@"Reset");
    discover.offArray=[[NSMutableArray alloc]initWithCapacity:0];
    discover.discoverId=nil;
    discover.districtArray=[[NSMutableArray alloc]initWithCapacity:0];
    discover.offerTypeArray=[[NSMutableArray alloc]initWithCapacity:0];
    discover.eventArray = [[NSMutableArray alloc]initWithCapacity:0];
    discover.tagsArr=[[NSMutableArray alloc]initWithCapacity:0];
    discover.occasionArray=[[NSMutableArray alloc]initWithCapacity:0];
    discover.cuisineArray=[[NSMutableArray alloc]initWithCapacity:0];
    discover.priceLevelArray=[[NSMutableArray alloc]initWithCapacity:0];
    discover.areaArray=[[NSMutableArray alloc]initWithCapacity:0];
    discover.searchDistrictArray=[[NSMutableArray alloc]initWithCapacity:0];
    discover.ratingNumber=nil;
    discover.sortby=nil;
//    NSNotification *notification = [[NSNotification alloc] initWithName:SEARCH_RESTAURANT object:nil userInfo:@{@"discover":discover}];
    
    priceString = nil;
    sortbyString = nil;
    ratingsString = nil;
    cuisineString = nil;
    featureString = nil;
    filterType = PageTypeNone;
    
    NSNotification *notification = [[NSNotification alloc] initWithName:SEARCH_RESTAURANT object:nil userInfo:@{@"price":priceString ? priceString : @"", @"sortby":sortbyString ? sortbyString : @"", @"rating":ratingsString ? ratingsString :@"", @"cuisines":cuisineString ? cuisineString : @"", @"feature":featureString ? featureString : @""}];
    if([self.fvcDelegate respondsToSelector:@selector(search:)])
    {
        [self.fvcDelegate search:notification];
//        [self dismissViewControllerAnimated:YES completion:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }

}
- (void)addUI{

    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight +20 - 64) style:UITableViewStyleGrouped];
    self.tableView.delegate =self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 7;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (section == 6) {
        return tagUpdateArrM.count +1;;
        
    }else{
    
        return 1;
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    if (section == 6) {
        return 0.0001;
    }else{
        return 40;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{

    return 0.0001;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    if (section == 0) {
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
        headerView.backgroundColor = [UIColor whiteColor];
        UILabel * lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, DeviceWidth - 30, 30)];
        lblTitle.text = @"Sorts by";
        lblTitle.textColor = [UIColor color333333];
        lblTitle.font = [UIFont boldSystemFontOfSize:14];
        [headerView addSubview:lblTitle];
        
        return headerView;
    }else if (section == 1){
    
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
        headerView.backgroundColor = [UIColor whiteColor];
        UILabel * lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, DeviceWidth - 30, 30)];
        lblTitle.text = @"Availablity";
        lblTitle.textColor = [UIColor color333333];
        lblTitle.font = [UIFont boldSystemFontOfSize:14];
        [headerView addSubview:lblTitle];
        
        return headerView;
    }else if (section == 2){
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
        headerView.backgroundColor = [UIColor whiteColor];
        UILabel * lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, DeviceWidth - 30, 30)];
        lblTitle.text = @"Ratings";
        lblTitle.textColor = [UIColor color333333];
        lblTitle.font = [UIFont boldSystemFontOfSize:14];
        [headerView addSubview:lblTitle];
        
        return headerView;
    }else if (section == 3){
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
        headerView.backgroundColor = [UIColor whiteColor];
        UILabel * lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, DeviceWidth - 30, 30)];
        lblTitle.text = @"Cuisines";
        lblTitle.textColor = [UIColor color333333];
        lblTitle.font = [UIFont boldSystemFontOfSize:14];
        [headerView addSubview:lblTitle];
        return headerView;
    }else if (section == 4){
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
        headerView.backgroundColor = [UIColor whiteColor];
        UILabel * lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, DeviceWidth - 30, 30)];
        lblTitle.text = @"Price";
        lblTitle.textColor = [UIColor color333333];
        lblTitle.font = [UIFont boldSystemFontOfSize:14];
        [headerView addSubview:lblTitle];
        
        return headerView;
    }else if (section == 5){
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
        headerView.backgroundColor = [UIColor whiteColor];
        UILabel * lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, DeviceWidth - 30, 30)];
        lblTitle.text = @"Features";
        lblTitle.textColor = [UIColor color333333];
        lblTitle.font = [UIFont boldSystemFontOfSize:14];
        [headerView addSubview:lblTitle];
        
        return headerView;
    }else{
    
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section == 0) {
        return 40;
    }else if (indexPath.section == 1){
    
        return 40;
    }else if (indexPath.section == 2){
        
        return 40;
    }else if (indexPath.section == 3){
        
        return 115;
    }else if (indexPath.section == 4){
        
        return 40;
    }else if (indexPath.section == 5){
        
        return 75;
    }else{
        
        switch (indexPath.row) {
            case 0:{
                
                return 50;
            }
                break;
                
            default:{
                IMGTagUpdate *tagUpdate = [tagUpdateArrM objectAtIndex:(int)indexPath.row - 1];
                if ([tagUpdate.typeId intValue] == 3 || [tagUpdate.typeId intValue] == 5 ) {
                    return 0;
                }else{
                    
                    return 50;
                };

            }
                break;
        }
        }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section == 0) {
        
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        V2_DiscoverListResultFiltersSortsByTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            
            cell = [[V2_DiscoverListResultFiltersSortsByTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.Delegate = self;
        return cell;
        
    }else if (indexPath.section == 1){
    
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell1%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        V2_DiscoverListResultFiltersAvailablityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            
            cell = [[V2_DiscoverListResultFiltersAvailablityTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.Delegate =self;
        return cell;
    
    }else if (indexPath.section == 2){
        
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell2%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        V2_DiscoverListResultFiltersRatingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            
            cell = [[V2_DiscoverListResultFiltersRatingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.Delegate = self;
        return cell;
        
    }else if (indexPath.section == 3){
        
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell3%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        V2_DiscoverListResultFiltersCuisinesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            
            cell = [[V2_DiscoverListResultFiltersCuisinesTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.Delegate = self;
            [cell initUI:cuisineArray];
        }
        
        return cell;
        
    }else if (indexPath.section == 4){
        
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell4%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        V2_DiscoverListResultFiltersPriceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            
            cell = [[V2_DiscoverListResultFiltersPriceTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.Delegate = self;
        return cell;
        
    }else if (indexPath.section == 5){
        
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell5%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        V2_DiscoverListResultFiltersFeaturesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            
            cell = [[V2_DiscoverListResultFiltersFeaturesTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.Delegate =self;
            [cell initUI:tagsArray];
        }
        
        return cell;
        
    }else {
//        if (indexPath.row == 0) {
//            NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
//            V2_DiscoverListResultFiltersPromoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//            if (!cell) {
//                cell = [[V2_DiscoverListResultFiltersPromoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//            }
//            cell.lblTitle.text = @"Promo";
//            return cell;
//
//        }
//        else{
//            NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
//            V2_DiscoverListResultFiltersOhtersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
//            if (!cell) {
//                
//                cell = [[V2_DiscoverListResultFiltersOhtersTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
//                cell.selectionStyle = UITableViewCellSelectionStyleNone;
//            }
//            
//            cell.model = tagUpdateArrM[indexPath.row-1] ;
//            return cell;
// 
//        }
        //==-1-1-1-1-1-1-
        NSString *identifier = @"FilterCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        
        FilterItemOfferCellView *filterItemOfferCellView;
        FilterItemCellView *filterItemCellView;
        @autoreleasepool {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier ];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            cell.contentView.backgroundColor = [UIColor whiteColor];
            switch (indexPath.row) {
                case 0:
                {
                    filterItemOfferCellView = [[FilterItemOfferCellView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, CELL_HEIGHT_50) discover:discover title:L(@"Promo") andTagUpdate:nil];
                    [cell.contentView addSubview:filterItemOfferCellView];
                    UIImageView *shadowTopImage = [[UIImageView alloc]initWithFrame:CGRectMake(15,0, DeviceWidth, 1)];
                    shadowTopImage.backgroundColor=[UIColor colorWithRed:220.0/255.0 green:225.0/255.0 blue:225.0/255.0 alpha:1];
                    [filterItemOfferCellView addSubview:shadowTopImage];
                    
                    UIImageView *shadowImage = [[UIImageView alloc]initWithFrame:CGRectMake(15, filterItemOfferCellView.height, DeviceWidth, 1)];
                    shadowImage.backgroundColor=[UIColor colorWithRed:220.0/255.0 green:225.0/255.0 blue:225.0/255.0 alpha:1];
                    [filterItemOfferCellView addSubview:shadowImage];
                }
                    break;
                    
                default:
                {
                    NSArray * array =  cell.contentView.subviews ;
                    for (UIView * view in array) {
                        [view removeFromSuperview];
                    }
                    IMGTagUpdate *tagUpdate = [tagUpdateArrM objectAtIndex:(int)indexPath.row - 1];
                    filterItemCellView = [[FilterItemCellView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, CELL_HEIGHT_50) discover:discover  title:tagUpdate.shows andTagUpdate:tagUpdate];
                    
                    
                    [cell.contentView addSubview:filterItemCellView];
                    UIImageView *shadowImage = [[UIImageView alloc]initWithFrame:CGRectMake(15, filterItemCellView.height, DeviceWidth, 1)];
                    shadowImage.backgroundColor=[UIColor colorWithRed:220.0/255.0 green:225.0/255.0 blue:225.0/255.0 alpha:1];
                    [filterItemCellView addSubview:shadowImage];
                }
                    break;
            }
        }
        return cell;
        
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        
        NSLog(@"%@",discover.eventArray);
        OfferTypeListViewController *offerTypeListViewController = [[OfferTypeListViewController alloc]initWithDiscover:discover];
        [self.navigationController pushViewController:offerTypeListViewController animated:YES];

        
    }else{
        
        IMGTagUpdate *tagUpdate = tagUpdateArrM[indexPath.row - 1];
        NSLog(@"====1111111=====%@",tagUpdate.typeId);
        if ([tagUpdate.typeId intValue] == 1)
        {
//            priceLevelLower = standardSlider.lowerValue;
//            priceLevelUpper = standardSlider.upperValue;
            
            AreaListViewController *areaListViewController = [[AreaListViewController alloc]initWithDiscover:discover];
            [self.navigationController pushViewController:areaListViewController animated:YES];
        }
        else if ([tagUpdate.typeId intValue] == 3)
        {
//            priceLevelLower = standardSlider.lowerValue;
//            priceLevelUpper = standardSlider.upperValue;
            
            CuisineListViewController *cuisineListViewController = [[CuisineListViewController alloc]initWithDiscover:discover];
            [self.navigationController pushViewController:cuisineListViewController animated:YES];
        }
        else if ([tagUpdate.typeId intValue] == 2)
        {
//            priceLevelLower = standardSlider.lowerValue;
//            priceLevelUpper = standardSlider.upperValue;
            
            LocationListViewController *locationListViewController = [[LocationListViewController alloc]initWithHead:discover];
            [self.navigationController pushViewController:locationListViewController animated:YES];
        }
        else
        {
//            priceLevelLower = standardSlider.lowerValue;
//            priceLevelUpper = standardSlider.upperValue;
            
            TagListViewController *tagListViewController = [[TagListViewController alloc]initWithDiscover:discover andTagUpdate:tagUpdate];
            [self.navigationController pushViewController:tagListViewController animated:YES];
        }

    }
    
//    V2_DiscoverListResultFiltersDetailViewController *V2_DiscoverListResultFiltersDetailVC = [[V2_DiscoverListResultFiltersDetailViewController alloc] init];
//    [self.navigationController pushViewController:V2_DiscoverListResultFiltersDetailVC animated:YES];
}


//==========所有数据获取

#pragma mark - FearureData
- (void)loadFearureData{

    NSMutableArray *featureArrs=[[NSMutableArray alloc]init];
    NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM IMGTagUpdate where type='Features';"];
    FMResultSet *resultSet = [[DBManager manager] executeQuery:sqlString];
    while ([resultSet next])
    {
        IMGTagUpdate *tagUpdate=[[IMGTagUpdate alloc]init];
        [tagUpdate setValueWithResultSet:resultSet];
        [featureArrs addObject:tagUpdate];
    }
    if (featureArrs.count>0) {
        featureTagUpdate=[featureArrs objectAtIndex:0];
    }
    [self requestdata];
   


}
- (void)requestdata{
    
    tagsArray = [[NSMutableArray alloc] initWithCapacity:0];
    NSString *sqlStr;
    sqlStr = [NSString stringWithFormat:@"SELECT * FROM IMGTag where cityId=%@ and status=1 and tagTypeId='%@' order by ranking,name;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID],featureTagUpdate.tagTypeId];
    FMResultSet *resultSet =[[DBManager manager] executeQuery:sqlStr];
    while([resultSet next]) {
        IMGTag *tag = [[IMGTag alloc] init];
        [tag setValueWithResultSet:resultSet];
        [tagsArray addObject:tag];
    }
    [resultSet close];

}

#pragma mark - cuisine
- (void)cuisineData
{
    cuisineArray = [[NSMutableArray alloc] initWithCapacity:0];
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGCuisine order by name;"] successBlock:^(FMResultSet *resultSet) {
        while([resultSet next]) {
            IMGCuisine *cuisine = [[IMGCuisine alloc]init];
            [cuisine setValueWithResultSet:resultSet];
            [cuisineArray addObject:cuisine];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"CuisineListViewController initData error when get datas:%@",error.description);
    }];
}

#pragma mark - 底下列表数据
- (void)bottomData{

    tagUpdateArrM = [[NSMutableArray alloc] init];
//    NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM IMGTagUpdate WHERE tagTypeId = 0 AND typeId !=50  ORDER BY typeId;"];
//    FMResultSet *resultSet = [[DBManager manager] executeQuery:sqlString];
//    while ([resultSet next])
//    {
//        IMGTagUpdate *tagUpdate=[[IMGTagUpdate alloc]init];
//        [tagUpdate setValueWithResultSet:resultSet];
//        [tagUpdateArrM addObject:tagUpdate];
//    }
    
     NSString *sqlString= [NSString stringWithFormat:@"SELECT * FROM IMGTagUpdate WHERE shows != \"\" and tagTypeId == 11 and visible = 1 ORDER BY rank;"];
    FMResultSet *resultSet2 = [[DBManager manager] executeQuery:sqlString];
    while ([resultSet2 next])
    {
        IMGTagUpdate *tagUpdate=[[IMGTagUpdate alloc]init];
        [tagUpdate setValueWithResultSet:resultSet2];
        
        sqlString = [NSString stringWithFormat:@"SELECT * FROM IMGTag where cityId=%@ and status=1 and tagTypeId='%@' order by ranking;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID],tagUpdate.tagTypeId];
        FMResultSet *resultSet3 = [[DBManager manager] executeQuery:sqlString];
        if ([resultSet3 next])
        {
            [tagUpdateArrM addObject:tagUpdate];
        }
    }

}

-(void)addNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(filterd:) name:SEARCH_FILTER object:nil];
}

-(void)filterd:(NSNotification*)notification
{
    NSDictionary * userInfo  = notification.userInfo;
    discover = [userInfo objectForKey:@"discover"];
    discover.districtArray = discover.searchDistrictArray;
    if ([userInfo objectForKey:@"tempArray"]!=nil) {
        [tempArray addObject:[userInfo objectForKey:@"tempArray"]];
    }
//    if (topview) {
//        [topview removeAllSubviews];
//        [self loadTopView];
//    }
    
    [self.tableView reloadData];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SEARCH_FILTER object:nil];
    
}
-(void)close{
    if ( (tempArray !=nil ) && (tempArray.count !=0 )) {
        for (id item in tempArray) {
            if ([item isKindOfClass:[IMGArea class]]) {
                [discover removeArea:((IMGArea*)item)];
            }if ([item isKindOfClass:[IMGCuisine class]]) {
                [discover removeCuisine:((IMGCuisine*)item)];
            }if ([item isKindOfClass:[IMGTag class]]) {
                [discover removeTag:((IMGTag*)item)];
            }if ([item isKindOfClass:[IMGOfferType class]]) {
                [discover removeOfferType:((IMGOfferType*)item)];
            }if ([item isKindOfClass:[IMGDistrict class]]) {
                [discover removeDistrict:((IMGDistrict*)item)];
            }
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
//    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma delegate  sort BY
- (void)delegateSortBy:(NSInteger)buttonTag{
    switch (buttonTag) {
        case 1:
        {
            discover.sortby=SORTBY_POPULARITY;
            sortbyString = SORTBY_POPULARITY;
        }
            break;
        case 2:
        {
            discover.sortby=@"rating";
            sortbyString = @"rating";
        }
            break;
        case 3:
        {
            discover.sortby=@"price";
            sortbyString = @"price";
        }
            break;
        case 4:
        {
            discover.sortby=@"distance";
            sortbyString = @"distance";
        }
            break;
            
            
        default:
            break;
    }


    
}
#pragma mark - Availablity
- (void)delegateAvailablity:(NSInteger)availablityTag{

    filterType = availablityTag;
}
#pragma mark - rating
- (void)delegateRating:(NSInteger)RatingTag{

    NSLog(@"%ld",(long)RatingTag);
    
        switch (RatingTag) {
            case 20:
            {
                discover.ratingNumber=nil;
                ratingsString  =nil;
            }
                break;
            case 21:
            {
                discover.ratingNumber=@3;
                ratingsString  =@"3";
    
            }
                break;
            case 22:
            {
                discover.ratingNumber=@4;
                ratingsString  =@"4";
    
            }
                break;
            case 23:
            {
                discover.ratingNumber=@5;
                ratingsString  =@"5";
    
            }
                break;
                
            default:
                break;
        }

}
#pragma mark - price
- (void)delegatePrice:(NSInteger)PriceTag{

    IMGPriceLevel *priceLevel1 = [[IMGPriceLevel alloc] init];
    priceLevel1.priceLevelId = [NSNumber numberWithInt:1];
    priceLevel1.title=@"100k-199k";
    priceLevel1.dollors=@"$";
    IMGPriceLevel *priceLevel2 = [[IMGPriceLevel alloc] init];
    priceLevel2.priceLevelId = [NSNumber numberWithInt:2];
    priceLevel2.title=@"200k-299k";
    priceLevel2.dollors=@"$$";
    IMGPriceLevel *priceLevel3 = [[IMGPriceLevel alloc] init];
    priceLevel3.priceLevelId = [NSNumber numberWithInt:3];
    priceLevel3.title=@"300k-399k";
    priceLevel3.dollors=@"$$$";
    IMGPriceLevel *priceLevel4 = [[IMGPriceLevel alloc] init];
    priceLevel4.priceLevelId = [NSNumber numberWithInt:4];
    priceLevel4.title=@">400k";
    priceLevel4.dollors=@"$$$$";

    NSMutableArray *priceLavels = [[NSMutableArray alloc]init];
    
    switch (PriceTag) {
        case 30:
        {
            [priceLavels addObject:priceLevel1];
            discover.priceLevelArray = priceLavels;
            priceString = priceLevel1.title;
        }
            break;
        case 31:
        {
         
            [priceLavels addObject:priceLevel2];
            discover.priceLevelArray = priceLavels;
             priceString = priceLevel2.title;
        }
            break;
        case 32:
        {
            [priceLavels addObject:priceLevel3];
            discover.priceLevelArray = priceLavels;
             priceString = priceLevel3.title;
            
        }
            break;
        case 33:
        {
           
            [priceLavels addObject:priceLevel4];
            discover.priceLevelArray = priceLavels;
             priceString = priceLevel4.title;
        }
            break;
            
        default:
            break;
    }


}
#pragma mark - Cuisines
- (void)delegateCuisines:(NSString *)Cuisines CuisinesArray:(NSMutableArray *)CuisinesArray indexArray:(NSMutableArray *)indexArray{

    NSLog(@"Cuisines===%@CuisinesArray==%@indexArray===%@",Cuisines, CuisinesArray, indexArray);
//   discover.cuisineArray = CuisinesArray;
    cuisineString = Cuisines;

}
#pragma mark - Features
- (void)delegateFeatures:(NSString *)Features FeaturesArray:(NSMutableArray *)FeaturesArray indexArray:(NSMutableArray *)indexArray{

    NSLog(@"Features==%@FeaturesArray===%@indexArray====%@",Features, FeaturesArray, indexArray);
    featureString = Features;
//    discover.tagsArr = FeaturesArray;
    
//    if (discover.tagsArr.count>0) {
    
//        for (int a=0; a<discover.tagsArr.count; a++) {
//            IMGTag *tag=[discover.tagsArr objectAtIndex:a];
//            [discover addTag:tag];
////            for (int i=0; i<7; i++) {
////                
////                if ([tag.name isEqualToString:featureTagArr[i]]) {
////                    [selectArr addObject:[NSNumber numberWithInt:i]];
////                }
////            }
//            
//        }
//        
//        
//    }

}










@end
