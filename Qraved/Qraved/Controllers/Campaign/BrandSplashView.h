//
//  BrandSplashView.h
//  Qraved
//
//  Created by harry on 2018/3/19.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrandSplashView : UIView

@property (nonatomic, copy) void(^dismissSplash)();
@property (nonatomic, copy) void(^reminderTapped)();
@end
