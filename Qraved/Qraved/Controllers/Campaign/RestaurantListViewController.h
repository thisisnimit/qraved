//
//  RestaurantListViewController.h
//  Qraved
//
//  Created by harry on 2018/1/17.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "BrandModel.h"
@interface RestaurantListViewController : BaseViewController

@property (nonatomic, assign) BOOL isMall;
@property (nonatomic, strong) BrandModel *model;

@end
