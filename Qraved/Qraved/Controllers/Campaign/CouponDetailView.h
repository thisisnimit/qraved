//
//  CouponDetailView.h
//  Qraved
//
//  Created by harry on 2018/1/22.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CouponModel.h"
@interface CouponDetailView : UIView <UIWebViewDelegate>

@property (nonatomic, copy) CouponModel *model;
@property (nonatomic, copy) void(^buttonTapped)(UIButton *button);
@property (nonatomic, copy) void(^shareCoupon)();
@property (nonatomic, copy) void(^turnToBrand)();

@end
