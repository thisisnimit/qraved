//
//  CouponCollectionViewCell.h
//  Qraved
//
//  Created by harry on 2018/1/10.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CouponModel.h"
@interface CouponCollectionViewCell : UICollectionViewCell

@property (nonatomic, copy) CouponModel *model;

@end
