//
//  BrandViewController.h
//  Qraved
//
//  Created by harry on 2018/1/9.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"

@interface BrandViewController : BaseViewController

@property (nonatomic, assign) BOOL isMall;
@property (nonatomic, strong) NSString *brandId;
@property (nonatomic, strong) NSString *mallId;

@end
