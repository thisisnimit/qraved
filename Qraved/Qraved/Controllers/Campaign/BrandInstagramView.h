//
//  BrandInstagramView.h
//  Qraved
//
//  Created by harry on 2018/1/10.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrandInstagramView : UIView

@property (nonatomic, copy) NSArray *instagramArr;
@property (nonatomic, copy) void (^instagramSeeAll)();

@end
