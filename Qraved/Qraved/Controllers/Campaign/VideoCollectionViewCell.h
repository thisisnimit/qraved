//
//  VideoCollectionViewCell.h
//  Qraved
//
//  Created by harry on 2018/1/11.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BrandVideoModel.h"
#import "YTPlayerView.h"
@interface VideoCollectionViewCell : UICollectionViewCell

@property (nonatomic, copy) BrandVideoModel *model;
//@property (nonatomic, copy) YTPlayerView *playerView;

@end
