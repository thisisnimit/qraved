//
//  CouponMapViewController.m
//  Qraved
//
//  Created by harry on 2018/1/29.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "CouponMapViewController.h"
#import <MapKit/MapKit.h>
#import "CustomAnnotation.h"
@interface CouponMapViewController ()<MKMapViewDelegate,UIActionSheetDelegate>
{
    MKMapView *mapView;
}
@end

@implementation CouponMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createUI];
    [self loadNavButton];
    
}

- (void)loadNavButton{
    
    [self setBackBarButtonOffset30];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 105, 40)];
    [btn setTitle:@"Get Directions" forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colortextBlue] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:16];
    [btn addTarget:self action:@selector(getDirection) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *btnDirection = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem=btnDirection;
    
    [self addLocationButton];
}

-(void)addLocationButton
{
    UIButton *mapButton = [[UIButton alloc] initWithFrame:CGRectMake(DeviceWidth-50, 10, 35, 35)];
    [mapButton  setBackgroundColor:[UIColor clearColor]];
    [mapButton  addTarget:self action:@selector(gotoCurrentLocation) forControlEvents:UIControlEventTouchUpInside];
    [mapButton  setImage:[UIImage imageNamed:@"ic_located"] forState:UIControlStateNormal];
    [self.view addSubview:mapButton];
    mapView.showsUserLocation = YES;
    
}

-(void)gotoCurrentLocation
{
    CLLocationCoordinate2D center;
    center.latitude=[AppDelegate ShareApp].locationManager.location.coordinate.latitude;
    center.longitude = [AppDelegate ShareApp].locationManager.location.coordinate.longitude;
    MKCoordinateSpan span;
    span.latitudeDelta=0.03;
    span.longitudeDelta=0.005;
    MKCoordinateRegion region={center,span};
    [mapView setRegion:region];
    
}

- (void)createUI{
    mapView=[[MKMapView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, self.view.frame.size.height)];
    [mapView setMapType:MKMapTypeStandard];
    mapView.delegate=self;
    [self.view addSubview:mapView];
    
    [self addAnnotation];
}

-(void)addAnnotation
{
    CLLocationCoordinate2D coor;
    coor.latitude = [self.model.latitude doubleValue];
    coor.longitude = [self.model.longitude doubleValue];
    CLLocationCoordinate2D theCoordinate =CLLocationCoordinate2DMake(coor.latitude, coor.longitude);
    MKCoordinateSpan  theSpan;
    theSpan.latitudeDelta=0.01;
    theSpan.longitudeDelta=0.01;
    MKCoordinateRegion theRegion;
    theRegion.center=theCoordinate;
    theRegion.span=theSpan;
    MKPointAnnotation *pointAnnatation= [[MKPointAnnotation alloc] init];
    if(CLLocationCoordinate2DIsValid(theCoordinate))
    {
        [mapView setRegion:theRegion animated:NO];
        
        [mapView regionThatFits:theRegion];
        
        pointAnnatation.coordinate=theCoordinate;
        
        [mapView addAnnotation:pointAnnatation];
    }
}

#pragma mark - MapViewDelegate
- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation{
    
    if ([annotation isKindOfClass:[MKPointAnnotation class]]){
        MKAnnotationView *pinView = nil;
        static NSString *defaultPinID = @"imaginato_map";
        pinView = (MKAnnotationView *)[theMapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinView == nil ) pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        pinView.canShowCallout = YES;
        pinView.image = [[UIImage imageNamed:@"ic_red_pin.png"] imageByScalingAndCroppingForSize:CGSizeMake(27, 33)];
        return pinView;
    }
    
    return nil;
}

- (void)getDirection{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]]) {
        NSLog(@"YES");
        
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Open in Google Maps",@"Open in Apple Maps", nil];
        
        [sheet showInView:self.view];
        
    }else{
        [self openInAppleMap];
    }
}

- (void)openInAppleMap{
    CLLocationCoordinate2D _end = CLLocationCoordinate2DMake([self.model.latitude doubleValue], [self.model.longitude doubleValue]);
    MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:_end addressDictionary:nil] ];
    toLocation.name = self.model.name;
    [MKMapItem openMapsWithItems:[NSArray arrayWithObjects: toLocation, nil]
                   launchOptions:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:MKLaunchOptionsDirectionsModeDriving, [NSNumber numberWithBool:YES], nil]
                                                             forKeys:[NSArray arrayWithObjects:MKLaunchOptionsDirectionsModeKey, MKLaunchOptionsShowsTrafficKey, nil]]];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
        NSString *appName = [infoDictionary objectForKey:@"CFBundleDisplayName"];
        
        CLLocationCoordinate2D _end = CLLocationCoordinate2DMake([self.model.latitude doubleValue], [self.model.longitude doubleValue]);
        NSString *urlString = [[NSString stringWithFormat:@"comgooglemaps://?x-source=%@&x-success=%@&saddr=&daddr=%f,%f&directionsmode=driving",appName,@"comgooglemaps",_end.latitude,_end.longitude]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *aURL = [NSURL URLWithString:urlString];
        
        [[UIApplication sharedApplication] openURL:aURL];
        
    }else if (buttonIndex==1){
        [self openInAppleMap];
    }
}


@end
