//
//  BrandSplashView.m
//  Qraved
//
//  Created by harry on 2018/3/19.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "BrandSplashView.h"

@implementation BrandSplashView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.9];
        [self createUI];
    }
    return self;
}

- (void)createUI{
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor colorWithRed:222/255.0 green:32/255.0 blue:41/255.0 alpha:1.0];
    bgView.layer.masksToBounds = YES;
    bgView.layer.cornerRadius = 5;
    [self addSubview:bgView];
   
    UIImageView *logoImageView = [UIImageView new];
    logoImageView.image = [UIImage imageNamed:@"ic_bkSplash"];
    
    UIButton *btnShow = [UIButton new];
    btnShow.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.15];
    [btnShow setTitle:@"Don’t show it again" forState:UIControlStateNormal];
    btnShow.titleLabel.font = [UIFont systemFontOfSize:12];
    [btnShow setTitleColor:[UIColor colorWithHexString:@"#EAEAEA"] forState:UIControlStateNormal];
    [btnShow addTarget:self action:@selector(reminderButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnClose = [UIButton new];
    btnClose.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.15];
    [btnClose setTitle:@"Close" forState:UIControlStateNormal];
    btnClose.titleLabel.font = [UIFont systemFontOfSize:12];
    [btnClose setTitleColor:[UIColor colorWithHexString:@"#EAEAEA"] forState:UIControlStateNormal];
    [btnClose addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *spaceView = [UIView new];
    spaceView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    
    [bgView sd_addSubviews:@[logoImageView, btnShow, spaceView, btnClose]];
    
    bgView.sd_layout
    .centerYEqualToView(self)
    .leftSpaceToView(self, 27)
    .rightSpaceToView(self, 27)
    .heightIs((DeviceWidth-27*2)*1.65);
    
    logoImageView.sd_layout
    .leftSpaceToView(bgView, 0)
    .rightSpaceToView(bgView, 0)
    .topSpaceToView(bgView, 0)
    .bottomSpaceToView(bgView, 0);
    
    spaceView.sd_layout
    .bottomSpaceToView(bgView, 8)
    .widthIs(2)
    .heightIs(22)
    .centerXEqualToView(bgView);
    
    btnShow.sd_layout
    .bottomSpaceToView(bgView, 0)
    .leftSpaceToView(bgView, 0)
    .heightIs(38)
    .rightSpaceToView(spaceView, 0);
    
    btnClose.sd_layout
    .topEqualToView(btnShow)
    .rightSpaceToView(bgView, 0)
    .leftSpaceToView(btnShow, 0)
    .heightIs(38);
    
    [bgView setupAutoHeightWithBottomView:btnShow bottomMargin:0];
}

- (void)close{
    if (self.dismissSplash) {
        self.dismissSplash();
    }
}

- (void)reminderButtonTapped{
    if (self.reminderTapped) {
        self.reminderTapped();
    }
}


@end
