//
//  CouponView.h
//  Qraved
//
//  Created by harry on 2018/1/10.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CouponModel.h"
@interface CouponView : UIView

@property (nonatomic, strong) NSArray *couponArray;

@property (nonatomic, copy) void(^couponTapped)(CouponModel *model, NSInteger index);
@property (nonatomic, copy) void(^couponSeeAll)();

@end
