//
//  CouponEmployeeView.h
//  Qraved
//
//  Created by harry on 2018/1/25.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CouponModel.h"
@interface CouponEmployeeView : UIView


@property (nonatomic, copy) CouponModel *model;
@property (nonatomic, copy) void (^redeemTapped)(UIButton *button);

@end
