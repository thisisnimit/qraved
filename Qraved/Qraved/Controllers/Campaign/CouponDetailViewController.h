//
//  CouponDetailViewController.h
//  Qraved
//
//  Created by harry on 2018/1/22.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"

@interface CouponDetailViewController : BaseViewController

@property (nonatomic, copy) NSNumber *couponId;
@property (nonatomic, assign) BOOL canReturn;

@end
