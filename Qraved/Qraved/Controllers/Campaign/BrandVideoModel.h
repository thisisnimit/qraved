//
//  BrandVideoModel.h
//  Qraved
//
//  Created by harry on 2018/1/11.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BrandVideoModel : NSObject

@property (nonatomic, copy) NSNumber *status;
@property (nonatomic, copy) NSNumber *sort_order;
@property (nonatomic, copy) NSString *video_id;
@property (nonatomic, copy) NSString *source_type;
@property (nonatomic, copy) NSString *video_url;
@property (nonatomic, copy) NSString *videoDescription;
@property (nonatomic, copy) NSString *title;
@end
