//
//  BrandModel.m
//  Qraved
//
//  Created by harry on 2018/1/10.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "BrandModel.h"

@implementation BrandModel


- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"id"]) {
        self.brandId = value;
    }else if ([key isEqualToString:@"description"]){
        self.brandDescription = value;
    }else if ([key isEqualToString:@"instagram_link"]){
        self.instagram = value;
    }else if ([key isEqualToString:@"logo_image_url"]){
        self.image_url = value;
    }else if ([key isEqualToString:@"restaurant_count"]){
        self.outlet_count = value;
    }
}

@end
