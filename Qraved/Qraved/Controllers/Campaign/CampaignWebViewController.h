//
//  CampaignWebViewController.h
//  Qraved
//
//  Created by harry on 2018/2/2.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"

@interface CampaignWebViewController : BaseViewController

@property (nonatomic, copy) NSString *campaignUrl;

@end
