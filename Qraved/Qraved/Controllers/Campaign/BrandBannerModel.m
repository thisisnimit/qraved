//
//  BrandBannerModel.m
//  Qraved
//
//  Created by harry on 2018/1/19.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "BrandBannerModel.h"

@implementation BrandBannerModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"id"]) {
        self.bannerId = value;
    }
}

@end
