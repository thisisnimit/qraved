//
//  CouponListViewController.m
//  Qraved
//
//  Created by harry on 2018/3/16.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "CouponListViewController.h"
#import "CouponHandler.h"
#import "CouponListTableViewCell.h"
#import "CouponDetailViewController.h"
@interface CouponListViewController ()<UITableViewDelegate, UITableViewDataSource>
{
    UITableView *couponTableView;
    NSMutableArray *couponArr;
    int offset;
}
@end

@implementation CouponListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initNav];
    [self loadData];
    [self initUI];
    [self requestData];
}

- (void)initNav{
    self.navigationItem.title = @"Coupon of the Week";
    UIBarButtonItem *leftBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_back"] style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
    leftBtn.tintColor=[UIColor blackColor];
    self.navigationItem.leftBarButtonItem=leftBtn;
}

- (void)backClick{
    [IMGAmplitudeUtil trackCampaignWithName:@"CL - Close See All Coupons" andCampaignId:nil andBrandId:self.targetId andCouponId:nil andLocation:nil andOrigin:nil andIsMall:self.isMall];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadData{
    offset = 0;
    couponArr = [NSMutableArray array];
}

- (void)requestData{
    if (self.couponArray.count>0) {
        couponTableView.mj_footer.hidden = YES;
        [couponArr addObjectsFromArray:self.couponArray];
        [couponTableView reloadData];
    }else{
        NSMutableDictionary *pamars = [[NSMutableDictionary alloc] init];
        if (self.isMall) {
            [pamars setValue:self.targetId forKey:@"mallId"];
        }else{
            [pamars setValue:self.targetId forKey:@"brandId"];
        }
        
        [pamars setValue:@"10" forKey:@"limit"];
        [pamars setValue:[NSString stringWithFormat:@"%d",offset] forKey:@"offset"];
        [[LoadingView sharedLoadingView] startLoading];
        [CouponHandler couponList:pamars andSuccessBlock:^(NSArray *couponList) {
            [[LoadingView sharedLoadingView] stopLoading];
            if (offset == 0) {
                [couponArr removeAllObjects];
            }
            if (couponList.count > 0) {
                couponTableView.mj_footer.hidden = NO;
            }else{
                couponTableView.mj_footer.hidden = YES;
            }
            [couponArr addObjectsFromArray:couponList];
            [couponTableView reloadData];
            [couponTableView.mj_footer endRefreshing];
            
        } anderrorBlock:^{
            [[LoadingView sharedLoadingView] stopLoading];
            [couponTableView.mj_footer endRefreshing];
        }];
    }
}

- (void)initUI{
    self.view.backgroundColor = [UIColor whiteColor];
    couponTableView = [UITableView new];
    couponTableView.delegate = self;
    couponTableView.dataSource = self;
    couponTableView.backgroundColor = [UIColor whiteColor];
    couponTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    [couponTableView setSeparatorColor:[UIColor colorWithHexString:@"#EAEAEA"]];
    couponTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [couponTableView setSeparatorInset:UIEdgeInsetsZero];
    
    if (@available(iOS 11.0, *)) {
        couponTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        couponTableView.estimatedRowHeight = 0;
    }
    [self.view addSubview:couponTableView];
    
    couponTableView.sd_layout
    .topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .bottomSpaceToView(self.view, 0);
    
    couponTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
}

- (void)loadMoreData{
    offset+=10;
    [self requestData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return couponArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CouponModel *model = [couponArr objectAtIndex:indexPath.row];
    return [couponTableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[CouponListTableViewCell class] contentViewWidth:DeviceWidth];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *idenStr = @"couponCell";
    CouponListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
    if (!cell) {
        cell = [[CouponListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
        cell.selectionStyle = UITableViewCellStyleDefault;
    }
    cell.model = [couponArr objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    CouponModel *model = [couponArr objectAtIndex:indexPath.row];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    if (model.coupon_id != nil) {
        [param setValue:model.coupon_id forKey:@"Coupon_ID"];
    }
    NSString *idStr = self.isMall ? @"Mall_ID" : @"Brand_ID";
    if (self.targetId != nil) {
        [param setValue:self.targetId forKey:idStr];
    }
    [param setValue:[NSNumber numberWithInteger:indexPath.row + 1] forKey:@"Position"];
    
    [param setValue:self.isMall?@"Mall Page":@"Brand Page" forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL - Coupon Detail Page" withEventProperties:param];
    
    [IMGAmplitudeUtil trackCampaignWithName:@"RC - View Coupon Detail" andCampaignId:nil andBrandId:nil andCouponId:model.coupon_id andLocation:nil andOrigin:@"See All Coupons Page" andIsMall:self.isMall];
    
    CouponDetailViewController *couponDetail = [[CouponDetailViewController alloc] init];
    couponDetail.couponId = model.coupon_id;
    MLNavigationController *nav = [[MLNavigationController alloc] initWithRootViewController:couponDetail];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

@end
