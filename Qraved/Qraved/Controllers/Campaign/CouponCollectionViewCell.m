//
//  CouponCollectionViewCell.m
//  Qraved
//
//  Created by harry on 2018/1/10.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "CouponCollectionViewCell.h"

@implementation CouponCollectionViewCell
{
    UIImageView *couponImageView;
    UILabel *lblTitle;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    couponImageView = [UIImageView new];
    couponImageView.contentMode = UIViewContentModeScaleAspectFill;
    couponImageView.clipsToBounds = YES;
    
    lblTitle = [UILabel new];
    lblTitle.font = [UIFont systemFontOfSize:14];
    lblTitle.textColor = [UIColor color333333];
    
    [self.contentView sd_addSubviews:@[couponImageView,lblTitle]];
    
    couponImageView.sd_layout
    .topSpaceToView(self.contentView, 0)
    .leftSpaceToView(self.contentView, 0)
    .widthIs(216)
    .heightIs(130);
    couponImageView.layer.masksToBounds = YES;
    couponImageView.layer.cornerRadius = 5;
    
    lblTitle.sd_layout
    .topSpaceToView(couponImageView, 7)
    .leftEqualToView(couponImageView)
    .widthIs(216)
    .heightIs(36);
    
    [lblTitle setNumberOfLines:2];
    
}

- (void)setModel:(CouponModel *)model{
    _model = model;
    if ([model.default_image_url hasPrefix:@"http"]) {
        [couponImageView sd_setImageWithURL:[NSURL URLWithString:model.default_image_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    }else{
        [couponImageView sd_setImageWithURL:[NSURL URLWithString:[model.default_image_url returnFullImageUrlWithWidth:216 andHeight:130]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    }
    
    lblTitle.text = model.name;
}

@end
