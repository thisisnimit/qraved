//
//  BrandHeaderView.m
//  Qraved
//
//  Created by harry on 2018/1/10.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "BrandHeaderView.h"
#import "V2_InstagramModel.h"
#import "BrandPageControl.h"
#import "EventView.h"
#import "MallMapView.h"
#import "CouponView.h"
#import "BrandInstagramView.h"
#import "V2_ArticleView.h"
#import "BrandVideoView.h"
#import "BrandBannerModel.h"
@implementation BrandHeaderView
{
    SDCycleScrollView *bannerScrollView;
    UIImageView *brandImageView;
    UILabel *lblTitle;
    //UILabel *lblCount;
    UILabel *lblInfo;
    //UILabel *lblPrice;
    BrandPageControl *pageControl;
    EventView *eventView;
    MallMapView *mapView;
    CouponView *couponView;
    BrandInstagramView *instagramView;
    V2_ArticleView *articleView;
    BrandVideoView *videoView;
    UIView *informationView;
    UIView *restaurantView;
    UILabel *lblResTitle;
    
    UIButton *btnFaceBook;
    UIButton *btnInstagram;
    UIButton *btnWeb;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    
    self.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    
    bannerScrollView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceWidth) delegate:self placeholderImage:[UIImage imageNamed:@"placeholder"]];
    bannerScrollView.hidden = YES;
    bannerScrollView.autoScroll = NO;
    bannerScrollView.pageControlStyle = SDCycleScrollViewPageContolStyleNone;
    
    pageControl = [BrandPageControl new];
    pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    pageControl.currentPageIndicatorTintColor = [UIColor colorWithHexString:@"D20000"];
    [pageControl setCurrentPage:0];
    
    informationView = [UIView new];
    informationView.backgroundColor = [UIColor whiteColor];
    
    brandImageView = [UIImageView new];
    brandImageView.contentMode = UIViewContentModeScaleAspectFill;
    brandImageView.clipsToBounds = YES;
    
    lblTitle = [UILabel new];
    lblTitle.font = [UIFont boldSystemFontOfSize:19];
    lblTitle.textColor = [UIColor color333333];
    
    lblInfo = [UILabel new];
    lblInfo.font = [UIFont systemFontOfSize:12];
    lblInfo.textColor = [UIColor color999999];
    
    btnFaceBook = [UIButton new];
    [btnFaceBook setImage:[UIImage imageNamed:@"ic_fb_brand"] forState:UIControlStateNormal];
    [btnFaceBook addTarget:self action:@selector(faceBookTapped) forControlEvents:UIControlEventTouchUpInside];
    
    btnInstagram = [UIButton new];
    [btnInstagram setImage:[UIImage imageNamed:@"ic_ig_brand"] forState:UIControlStateNormal];
    [btnInstagram addTarget:self action:@selector(instagramTapped) forControlEvents:UIControlEventTouchUpInside];
    
    btnWeb = [UIButton new];
    [btnWeb setImage:[UIImage imageNamed:@"ic_youtube_brand"] forState:UIControlStateNormal];
    [btnWeb addTarget:self action:@selector(webTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [informationView sd_addSubviews:@[brandImageView, lblTitle, lblInfo, btnFaceBook, btnInstagram, btnWeb]];
    
    eventView = [[EventView alloc] init];
    eventView.backgroundColor = [UIColor whiteColor];
    __weak typeof(self) weakSelf = self;
    eventView.eventTapped = ^(CampaignModel *model, NSInteger index) {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(eventCardTapped: andIndex:)]) {
            [weakSelf.delegate eventCardTapped:model andIndex:index];
        }
    };
    
    mapView = [[MallMapView alloc] init];
    mapView.backgroundColor = [UIColor whiteColor];
    mapView.instagramTapped = ^(NSNumber *locationId) {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(instagramPlaces:)]) {
            [weakSelf.delegate instagramPlaces:locationId];
        }
    };
    
    mapView.phoneTapped = ^(NSString *phoneNumber) {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(phoneTapped:)]) {
            [weakSelf.delegate phoneTapped:phoneNumber];
        }
    };
    
    mapView.getDirection = ^{
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(getDirection)]) {
            [weakSelf.delegate getDirection];
        }
    };
    
    mapView.mapTapped = ^{
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(mapButtonTapped)]) {
            [weakSelf.delegate mapButtonTapped];
        }
    };
    
    couponView = [[CouponView alloc] init];
    couponView.backgroundColor = [UIColor whiteColor];
    couponView.couponTapped = ^(CouponModel *model, NSInteger index) {
        if (weakSelf.delegate != nil && [weakSelf.delegate respondsToSelector:@selector(couponCardTapped: andIndex:)]) {
            [weakSelf.delegate couponCardTapped:model andIndex:index];
        }
    };
    
    couponView.couponSeeAll = ^{
        if (weakSelf.delegate != nil && [weakSelf.delegate respondsToSelector:@selector(seeAllCoupon)]) {
            [weakSelf.delegate seeAllCoupon];
        }
    };
    
    instagramView = [[BrandInstagramView alloc] init];
    instagramView.backgroundColor = [UIColor whiteColor];
    instagramView.instagramSeeAll = ^{
        if (weakSelf.delegate != nil && [weakSelf.delegate respondsToSelector:@selector(seeAllInstagram)]) {
            [weakSelf.delegate seeAllInstagram];
        }
    };
    
    articleView = [[V2_ArticleView alloc] init];
    articleView.backgroundColor = [UIColor whiteColor];
    articleView.gotoJournalDetail = ^(IMGMediaComment *journal) {
        if (weakSelf.delegate != nil && [weakSelf.delegate respondsToSelector:@selector(journalCardTapped:)]) {
            [weakSelf.delegate journalCardTapped:journal];
        }
    };
    
    videoView = [[BrandVideoView alloc] init];
    videoView.backgroundColor = [UIColor whiteColor];
    videoView.playingVideo = ^{
        if (weakSelf.delegate != nil && [weakSelf.delegate respondsToSelector:@selector(playingVideo)]) {
            [weakSelf.delegate playingVideo];
        }
    };
    
    [self sd_addSubviews:@[bannerScrollView, pageControl, informationView, eventView, mapView, couponView, instagramView, articleView, videoView]];
    
    bannerScrollView.sd_layout
    .topSpaceToView(self, 0)
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .heightIs(0);
    
    informationView.sd_layout
    .topSpaceToView(bannerScrollView, 0)
    .leftEqualToView(bannerScrollView)
    .rightEqualToView(bannerScrollView)
    .heightIs(0);
    
    pageControl.sd_layout
    .bottomSpaceToView(informationView, 26)
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .heightIs(5);
    
    brandImageView.sd_layout
    .topSpaceToView(informationView, 15)
    .leftSpaceToView(informationView, 15)
    .widthIs(55)
    .heightIs(55);
    
    brandImageView.layer.masksToBounds = YES;
    brandImageView.layer.cornerRadius = 55/2;
    
    lblTitle.sd_layout
    .leftSpaceToView(brandImageView, 12)
    .rightSpaceToView(informationView, 125)
    .topSpaceToView(informationView, 15)
    .autoHeightRatio(0);
    
    lblInfo.sd_layout
    .topSpaceToView(lblTitle, 5)
    .leftEqualToView(lblTitle)
    .rightEqualToView(lblTitle)
    .autoHeightRatio(0);
    [lblInfo setMaxNumberOfLinesToShow:2];
    
    btnWeb.sd_layout
    .topSpaceToView(informationView, 15)
    .rightSpaceToView(informationView, 15)
    .widthIs(30)
    .heightEqualToWidth();
    
    btnInstagram.sd_layout
    .topEqualToView(btnWeb)
    .rightSpaceToView(btnWeb, 7)
    .widthIs(30)
    .heightEqualToWidth();
    
    btnFaceBook.sd_layout
    .topEqualToView(btnInstagram)
    .rightSpaceToView(btnInstagram, 7)
    .widthIs(30)
    .heightEqualToWidth();
    
    
    eventView.sd_layout
    .topSpaceToView(informationView, 0)
    .leftEqualToView(informationView)
    .rightEqualToView(informationView)
    .heightIs(0);
    
    mapView.sd_layout
    .topSpaceToView(eventView, 0)
    .leftEqualToView(eventView)
    .rightEqualToView(eventView)
    .heightIs(0);
    
    couponView.sd_layout
    .topSpaceToView(mapView, 0)
    .leftEqualToView(mapView)
    .rightEqualToView(mapView)
    .heightIs(0);
    
    instagramView.sd_layout
    .topSpaceToView(couponView, 0)
    .leftEqualToView(couponView)
    .rightEqualToView(couponView)
    .heightIs(0);
    
    articleView.sd_layout
    .topSpaceToView(instagramView, 0)
    .leftEqualToView(instagramView)
    .rightEqualToView(instagramView)
    .heightIs(0);
    
    videoView.sd_layout
    .topSpaceToView(articleView, 0)
    .leftEqualToView(articleView)
    .rightEqualToView(articleView)
    .heightIs(0);
    
}

- (void)setModel:(BrandModel *)model{
    _model = model;

    bannerScrollView.hidden = NO;
    bannerScrollView.sd_layout
    .heightEqualToWidth();
    
    NSMutableArray *imgArr = [NSMutableArray array];
    if (model.bannerArr != nil && model.bannerArr.count>0) {
        for (BrandBannerModel *brandModel in model.bannerArr) {
            [imgArr addObject:brandModel.image_url];
        }
        bannerScrollView.imageURLStringsGroup = imgArr;
        if (imgArr.count>1) {
            pageControl.numberOfPages = imgArr.count;
        }else{
            pageControl.hidden = YES;
        }
        
    }else{
        bannerScrollView.sd_layout
        .heightIs(0);
    }
   
    
    [brandImageView sd_setImageWithURL:[NSURL URLWithString:[model.image_url returnFullImageUrl]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    BOOL hasWeb = [Tools isBlankString:model.website];
    btnWeb.sd_layout
    .topSpaceToView(informationView, 15)
    .rightSpaceToView(informationView, 15)
    .widthIs(hasWeb ? 0 : 30)
    .heightEqualToWidth();
    
    BOOL hasInstagram = [Tools isBlankString:model.instagram];
    btnInstagram.sd_layout
    .topEqualToView(btnWeb)
    .rightSpaceToView(btnWeb,hasWeb ? 0 : 7)
    .widthIs(hasInstagram ? 0 : 30)
    .heightEqualToWidth();
    
    BOOL hasFaceBook = [Tools isBlankString:model.facebook];
    btnFaceBook.sd_layout
    .topEqualToView(btnInstagram)
    .rightSpaceToView(btnInstagram, hasInstagram ? 0 : 7)
    .widthIs(hasFaceBook ? 0 : 30)
    .heightEqualToWidth();
    
    lblTitle.text = model.name;
    
    if (self.isMall) {
        [lblTitle setMaxNumberOfLinesToShow:4];
        lblInfo.text = [NSString stringWithFormat:@"%@ outlets in %@",model.outlet_count,model.name];
    }else{
        [lblTitle setMaxNumberOfLinesToShow:1];
        lblInfo.text = [NSString stringWithFormat:@"%@ outlets in %@ • %@ • %@",model.outlet_count,[model.city_name capitalizedString],model.cuisine_name_list,model.price_name];
    }
    CGSize size = [model.name sizeWithFont:[UIFont boldSystemFontOfSize:19] constrainedToSize:CGSizeMake(DeviceWidth-210, 100) lineBreakMode:NSLineBreakByWordWrapping];
    informationView.sd_layout
    .heightIs(self.isMall?65+size.height:85);
    
    
    if (model.eventArr != nil && model.eventArr.count > 0) {
        eventView.campaignArray = model.eventArr;
        eventView.sd_layout
        .topSpaceToView(informationView, 5)
        .autoHeightRatio(0);
    }else{
        eventView.hidden = YES;
    }
    
    if (self.isMall) {
        mapView.model = model;
        mapView.sd_layout
        .topSpaceToView(eventView, 5)
        .heightIs(496);
    }else{
        mapView.hidden = YES;
    }
    
    if (model.couponArr != nil && model.couponArr.count) {
        couponView.couponArray = model.couponArr;
        couponView.sd_layout
        .topSpaceToView(mapView, 5)
        .autoHeightRatio(0);
    }else{
        couponView.hidden = YES;
    }
    
    if (model.instagramArr != nil && model.instagramArr.count > 0) {
        instagramView.instagramArr = model.instagramArr;
        instagramView.sd_layout
        .topSpaceToView(couponView, 5)
        .autoHeightRatio(0);
    }else{
        instagramView.hidden = YES;
    }
    
    if (model.journalArr != nil && model.journalArr.count > 0) {
        articleView.articleArray = model.journalArr;
        articleView.sd_layout
        .topSpaceToView(instagramView, 5)
        .autoHeightRatio(0);
    }else{
        articleView.hidden = YES;
    }
    
    if (model.videoArr != nil && model.videoArr.count > 0) {
        videoView.videoArray = model.videoArr;
        videoView.sd_layout
        .topSpaceToView(articleView, 5)
        .autoHeightRatio(0);
    }else{
        videoView.hidden = YES;
    }
    
    
    [self setupAutoHeightWithBottomView:videoView bottomMargin:0];
}

- (CGFloat)heightForBrand:(BrandModel *)model{
    
    CGFloat campaignHeight = 0.0f;
    if (model.eventArr.count >1) {
        campaignHeight = (DeviceWidth-30)*.6 + 170;
    }else if (model.eventArr.count==1){
        campaignHeight = (DeviceWidth-30)*.6 + 135;
    }
    
    CGFloat instagramSize = 0.0f;
    if (model.instagramArr.count > 6) {
        instagramSize = 50 + (DeviceWidth-2)/3 * 2 + 55;
    }else if (model.instagramArr.count <= 6 && model.instagramArr.count > 0){
        instagramSize = 70 + (DeviceWidth-2)/3 * 2;
    }else{
        instagramSize = 0.0f;
    }
    CGFloat instagramHeight = model.instagramArr.count > 0 ? instagramSize : 0;
    CGFloat couponHeight = model.couponArr.count > 0 ? 250 : 0;
    CGFloat journalHeight = model.journalArr.count > 0 ? 213 : 0;
    CGFloat videoHeight = 0.0f;
    if (model.videoArr.count > 1) {
        videoHeight = (DeviceWidth-30)*.55 + 185;
    }else if (model.videoArr.count == 1){
        videoHeight = (DeviceWidth-30)*.55 + 150;
    }
    CGFloat mapHeight = self.isMall ? 496 : 0;
    CGSize size = [model.name sizeWithFont:[UIFont boldSystemFontOfSize:19] constrainedToSize:CGSizeMake(DeviceWidth-210, 100) lineBreakMode:NSLineBreakByWordWrapping];
    CGFloat informationHeight = self.isMall ? size.height + 65 : 85;
    
    return campaignHeight  + instagramHeight + couponHeight + journalHeight + videoHeight +DeviceWidth + informationHeight + mapHeight;
}

- (void)faceBookTapped{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(webSiteTapped:)]) {
        [self.delegate webSiteTapped:self.model.facebook];
    }
}

- (void)instagramTapped{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(webSiteTapped:)]) {
        [self.delegate webSiteTapped:self.model.instagram];
    }
}

- (void)webTapped{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(webSiteTapped:)]) {
        [self.delegate webSiteTapped:self.model.website];
    }
}



@end
