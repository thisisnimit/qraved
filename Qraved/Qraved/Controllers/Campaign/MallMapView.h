//
//  MallMapView.h
//  Qraved
//
//  Created by harry on 2018/1/17.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BrandModel.h"
@interface MallMapView : UIView

@property (nonatomic, strong) BrandModel *model;

@property (nonatomic, copy) void (^instagramTapped)(NSNumber *locationId);
@property (nonatomic, copy) void (^phoneTapped)(NSString *phoneNumber);
@property (nonatomic, copy) void (^getDirection)();
@property (nonatomic, copy) void (^mapTapped)();

@end
