//
//  CouponListViewController.h
//  Qraved
//
//  Created by harry on 2018/3/16.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"

@interface CouponListViewController : BaseViewController

@property (nonatomic, assign) BOOL isMall;
@property (nonatomic, strong) NSString *targetId;
@property (nonatomic, strong) NSArray *couponArray;

@end
