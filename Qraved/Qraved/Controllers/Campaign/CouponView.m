//
//  CouponView.m
//  Qraved
//
//  Created by harry on 2018/1/10.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "CouponView.h"
#import "CouponCollectionViewCell.h"

@interface CouponView ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    UICollectionView *couponCollectionView;
    UILabel *lblTitle;
}
@end

@implementation CouponView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    
    self.backgroundColor = [UIColor whiteColor];
    
    lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 20, DeviceWidth-30, 16)];
    lblTitle.text = @"Coupon of the Week";
    lblTitle.font = [UIFont boldSystemFontOfSize:14];
    lblTitle.textColor = [UIColor color333333];
    lblTitle.hidden = YES;
    [self addSubview:lblTitle];
    
    UIButton *btnChange = [UIButton buttonWithType:UIButtonTypeCustom];
    btnChange.frame = CGRectMake(DeviceWidth-93, 5, 78, 45);
    [btnChange addTarget:self action:@selector(seeAllClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnChange];
    [btnChange setImage:[UIImage imageNamed:@"ic_home_arrow"] forState:UIControlStateNormal];
    btnChange.imageEdgeInsets = UIEdgeInsetsMake(0, 70, 0, 0);

    [btnChange setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
    btnChange.titleLabel.font = [UIFont systemFontOfSize:12];
    [btnChange setTitle:@"See All" forState:UIControlStateNormal];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(216, 195);
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.minimumLineSpacing = 10;
    layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 15);
    
    couponCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 50, DeviceWidth, 195) collectionViewLayout:layout];
    couponCollectionView.backgroundColor = [UIColor whiteColor];
    couponCollectionView.delegate = self;
    couponCollectionView.dataSource = self;
    [self addSubview:couponCollectionView];
    
    [couponCollectionView registerClass:[CouponCollectionViewCell class] forCellWithReuseIdentifier:@"couponCell"];
    [couponCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"couponHeader"];
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.couponArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CouponCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"couponCell" forIndexPath:indexPath];
    
    CouponModel *couponModel = [self.couponArray objectAtIndex:indexPath.item];
    cell.model = couponModel;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    CouponModel *couponModel = [self.couponArray objectAtIndex:indexPath.item];
    if (self.couponTapped) {
        self.couponTapped(couponModel, indexPath.row);
    }
}

- (void)setCouponArray:(NSArray *)couponArray{
    _couponArray = couponArray;
    [couponCollectionView reloadData];
    
    if (couponArray.count>0) {
        lblTitle.hidden = NO;
    }else{
        lblTitle.hidden = YES;
    }
    
    [self setupAutoHeightWithBottomView:couponCollectionView bottomMargin:0];
}

- (void)seeAllClick{
    if (self.couponSeeAll) {
        self.couponSeeAll();
    }
}

@end
