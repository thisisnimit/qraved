//
//  BrandModel.h
//  Qraved
//
//  Created by harry on 2018/1/10.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BrandModel : NSObject

@property (nonatomic, copy) NSString *created_date;
@property (nonatomic, copy) NSNumber *created_user_id;
@property (nonatomic, copy) NSString *cuisine_name_list;
@property (nonatomic, copy) NSString *brandDescription;
@property (nonatomic, copy) NSString *facebook;
@property (nonatomic, copy) NSString *brandId;
@property (nonatomic, copy) NSString *image_url;
@property (nonatomic, copy) NSString *instagram;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *outlet_count;
@property (nonatomic, copy) NSString *price_name;
@property (nonatomic, copy) NSString *twitter;
@property (nonatomic, copy) NSNumber *type;
@property (nonatomic, copy) NSString *updated_date;
@property (nonatomic, copy) NSNumber *updated_user_id;
@property (nonatomic, copy) NSString *website;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *instagram_username;
@property (nonatomic, copy) NSString *latitude;
@property (nonatomic, copy) NSString *longitude;
@property (nonatomic, copy) NSString *opening_hours;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *postal_code;
@property (nonatomic, copy) NSString *seo_keyword;
@property (nonatomic, copy) NSNumber *instagram_location_id;
@property (nonatomic, copy) NSNumber *city_id;
@property (nonatomic, copy) NSString *city_name;


@property (nonatomic, copy) NSArray *bannerArr;
@property (nonatomic, copy) NSArray *instagramArr;
@property (nonatomic, copy) NSArray *eventArr;
@property (nonatomic, copy) NSArray *couponArr;
@property (nonatomic, copy) NSArray *journalArr;
@property (nonatomic, copy) NSArray *videoArr;


@end
