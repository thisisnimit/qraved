//
//  CouponDetailViewController.m
//  Qraved
//
//  Created by harry on 2018/1/22.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "CouponDetailViewController.h"
#import "CouponHandler.h"
#import "CouponDetailView.h"
#import "CouponEmployeeView.h"
#import "IMGActivityItemProvider.h"
#import "BrandViewController.h"
@interface CouponDetailViewController ()<UIAlertViewDelegate>
{
    UIScrollView *couponScrollView;
    CouponModel *couponModel;
    CouponDetailView *couponView;
    BOOL isShowLoading;
    UIView *bottomView;
    CouponEmployeeView *employeeView;
    
    UIView *savedPopView;
    UIAlertView *redeemAlertView;
}
@end

@implementation CouponDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self loadNavButton];
    [self loadData];
    [self requestData];
    [self loadMainUI];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (isShowLoading) {
        [[LoadingView sharedLoadingView] startLoading];
    }

}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [savedPopView removeFromSuperview];
}

- (void)loadNavButton{
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"icon_black_wrong"] forState:UIControlStateNormal];
    backButton.frame = CGRectMake(LEFTLEFTSET, IMG_StatusBarHeight, 60,44);
    [backButton addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    backButton.imageEdgeInsets = UIEdgeInsetsMake(12, 0, 12, 40);
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"ic_share_guide_black"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(shareButtonClick)];
}

- (void)loadData{
    isShowLoading = YES;
}

- (void)requestData{
    
    NSMutableDictionary *pamars = [[NSMutableDictionary alloc] init];
    [pamars setValue:self.couponId forKey:@"couponId"];
    [pamars setValue:[IMGUser currentUser].userId forKey:@"userId"];
    [CouponHandler getCouponDetail:pamars andSuccessBlock:^(CouponModel *model) {
        
        isShowLoading = NO;
        couponModel = model;
        [self setCouponView];
        
        [[LoadingView sharedLoadingView] stopLoading];
        
    } anderrorBlock:^{
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

- (void)setCouponView{
    
    employeeView = [CouponEmployeeView new];
    employeeView.model = couponModel;
    __weak typeof(self) weakSelf = self;

    employeeView.redeemTapped = ^(UIButton *button) {
        [weakSelf useButtonTapped:button];
    };
    
    [self.view addSubview:employeeView];
    
    employeeView.sd_layout
    .bottomSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .autoHeightRatio(0);
    
    couponScrollView.sd_layout
    .topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .bottomSpaceToView(employeeView, 0);
    
    couponView = [CouponDetailView new];
    couponView.model = couponModel;
    couponView.buttonTapped = ^(UIButton *button) {
        [weakSelf saveButtonTapped:button];
    };
    couponView.shareCoupon = ^{
        [weakSelf shareButtonClick];
    };
    couponView.turnToBrand = ^{
        [weakSelf brandTapped];
    };
    [couponScrollView addSubview:couponView];
    
    couponView.sd_layout
    .topSpaceToView(couponScrollView, 0)
    .leftSpaceToView(couponScrollView, 0)
    .rightSpaceToView(couponScrollView, 0)
    .autoHeightRatio(0);
    
    [couponScrollView setupAutoContentSizeWithBottomView:couponView bottomMargin:0];
}

- (void)loadMainUI{
    self.view.backgroundColor = [UIColor whiteColor];
    
    couponScrollView = [UIScrollView new];
    
    if (@available(iOS 11.0, *)) {
        couponScrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.view addSubview:couponScrollView];
}

- (void)backClick{
    
    [IMGAmplitudeUtil trackCampaignWithName:@"CL - Close Coupon" andCampaignId:nil andBrandId:nil andCouponId:couponModel.coupon_id andLocation:nil andOrigin:nil andIsMall:NO];
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)brandTapped{
    if (![Tools isBlankString:couponModel.partner_type]) {
        if (self.canReturn) {
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }else{
            BrandViewController *brandViewController = [[BrandViewController alloc] init];
            if ([couponModel.partner_type isEqualToString:@"mall"]) {
                brandViewController.isMall = YES;
                brandViewController.mallId = [NSString stringWithFormat:@"%@",couponModel.partner_object_id];
            }else{
                brandViewController.isMall = NO;
                brandViewController.brandId = [NSString stringWithFormat:@"%@",couponModel.partner_object_id];
            }
            [self.navigationController pushViewController:brandViewController animated:YES];
            
        }
    }
}

- (void)shareButtonClick{
    NSString *shareText = [NSString stringWithFormat:@"Check out %@ coupon from %@ @Qraved!",couponModel.name,couponModel.partner_name];
    NSString *shareStr = [NSString stringWithFormat:@"%@coupon/%@",QRAVED_WEB_SERVER_OLD,couponModel.coupon_id];
    NSURL *shareUrl = [NSURL URLWithString:shareStr];
    
    IMGActivityItemProvider *itemProvider = [[IMGActivityItemProvider alloc] initWithPlaceholderItem:@""];
    itemProvider.title = shareText;
    itemProvider.url = shareUrl;
    
    UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[itemProvider] applicationActivities:nil];
    activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
    
    activityCtl.completionWithItemsHandler =^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
        if (![Tools isBlankString:couponModel.partner_type]) {
            if ([couponModel.partner_type isEqualToString:@"mall"]) {
                [IMGAmplitudeUtil trackCampaignWithName:@"SH - Share Coupon" andCampaignId:nil andBrandId:[NSString stringWithFormat:@"%@",couponModel.partner_object_id] andCouponId:couponModel.coupon_id andLocation:@"Coupon Detail Page" andOrigin:nil andIsMall:YES];
            }else{
                [IMGAmplitudeUtil trackCampaignWithName:@"SH - Share Coupon" andCampaignId:nil andBrandId:[NSString stringWithFormat:@"%@",couponModel.partner_object_id] andCouponId:couponModel.coupon_id andLocation:@"Coupon Detail Page" andOrigin:nil andIsMall:NO];
            }
        }

    };
    if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
        // iOS8
    }
    
    
    [self presentViewController:activityCtl animated:YES completion:^{
        
    }];
}

- (void)createSavedView{
    savedPopView = [[UIView alloc] initWithFrame:[AppDelegate ShareApp].window.bounds];
    savedPopView.backgroundColor =[UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    
    UIView *saveView = [UIView new];
    saveView.backgroundColor = [UIColor whiteColor];
    saveView.layer.masksToBounds = YES;
    saveView.layer.cornerRadius = 3;
    
    [savedPopView addSubview:saveView];
    
    saveView.sd_layout
    .centerXEqualToView(savedPopView)
    .centerYEqualToView(savedPopView)
    .widthIs(DeviceWidth-46)
    .autoHeightRatio(0);
    
    UIImageView *imgCoupon = [UIImageView new];
    imgCoupon.image = [UIImage imageNamed:@"ic_coupon_save"];
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.font = [UIFont boldSystemFontOfSize:18];
    lblTitle.textColor = [UIColor color333333];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.text = @"Your coupon has been saved to Profile";
    
    UILabel *lblDes = [UILabel new];
    lblDes.font = [UIFont systemFontOfSize:14];
    lblDes.textColor = [UIColor color999999];
    lblDes.textAlignment = NSTextAlignmentCenter;
    lblDes.text = @"You can use it anytime during the promo campaign.";
    
    UIButton *btnOk = [UIButton new];
    btnOk.layer.masksToBounds = YES;
    btnOk.layer.cornerRadius = 4;
    btnOk.backgroundColor = [UIColor colorWithHexString:@"D20000"];
    [btnOk setTitle:@"OK" forState:UIControlStateNormal];
    [btnOk setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnOk addTarget:self action:@selector(okButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [saveView sd_addSubviews:@[imgCoupon, lblTitle, lblDes, btnOk]];
    imgCoupon.sd_layout
    .topSpaceToView(saveView, 34)
    .leftSpaceToView(saveView, 25)
    .rightSpaceToView(saveView, 25)
    .heightIs((DeviceWidth-100) * .34);
    
    lblTitle.sd_layout
    .topSpaceToView(imgCoupon, 12)
    .leftSpaceToView(saveView, 39)
    .rightSpaceToView(saveView, 39)
    .autoHeightRatio(0);
    [lblTitle setMaxNumberOfLinesToShow:2];
    
    lblDes.sd_layout
    .topSpaceToView(lblTitle, 7)
    .leftEqualToView(lblTitle)
    .rightEqualToView(lblTitle)
    .autoHeightRatio(0);
    
    btnOk.sd_layout
    .topSpaceToView(lblDes, 25)
    .widthIs(109)
    .heightIs(42)
    .centerXEqualToView(saveView);
    
    [saveView setupAutoHeightWithBottomView:btnOk bottomMargin:35];
    
    [[AppDelegate ShareApp].window addSubview:savedPopView];
}

- (void)okButtonTapped{
    [savedPopView removeFromSuperview];
}

- (void)saveButtonTapped:(UIButton *)button{
    button.enabled = NO;
    NSMutableDictionary *pamars = [[NSMutableDictionary alloc] init];
    [pamars setValue:self.couponId forKey:@"couponId"];
    [pamars setValue:[IMGUser currentUser].userId forKey:@"user_id"];
    if ([couponModel.saved intValue] == 1) {
//        [CouponHandler couponUnsave:pamars andSuccessBlock:^{
//            couponModel.saved = @0;
//            NSInteger totalCount = [couponModel.total_inventory integerValue] + 1;
//            couponModel.total_inventory = [NSNumber numberWithInteger:totalCount];
//            couponView.model = couponModel;
//            employeeView.model = couponModel;
//            button.enabled = YES;
//        } anderrorBlock:^{
//            button.enabled = YES;
//        }];
    }else{
        [IMGAmplitudeUtil trackCampaignWithName:@"CL - Save Coupon" andCampaignId:nil andBrandId:nil andCouponId:couponModel.coupon_id andLocation:@"Coupon Detail Page" andOrigin:nil andIsMall:NO];
        
        [CouponHandler couponSave:pamars andSuccessBlock:^{
            [self createSavedView];
            
            couponModel.saved = @1;
            NSInteger totalCount = [couponModel.total_inventory integerValue] - 1;
            couponModel.total_inventory = [NSNumber numberWithInteger:totalCount];
            couponView.model = couponModel;
            employeeView.model = couponModel;
            button.enabled = YES;
            
        } anderrorBlock:^{
            button.enabled = YES;
        }];
    }
    
}

- (void)useButtonTapped:(UIButton *)button{
    if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"To continue coupon redeem, turn on your location services" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        [alert show];
        
    }else{
        redeemAlertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Change to Used (Store Use)\nTap Ok to use this coupon.\nThe action can’t be undone." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [redeemAlertView show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView == redeemAlertView) {
        if (buttonIndex == 1){
            [self useCoupon];
        }
    }else{
        if (buttonIndex == 1) {
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            if ([[UIApplication sharedApplication] canOpenURL:url]) {
                [[UIApplication sharedApplication] openURL:url];
            }
        }
    }
}

- (void)useCoupon{
    
    [IMGAmplitudeUtil trackCampaignWithName:@"CL - Use Coupon" andCampaignId:nil andBrandId:nil andCouponId:couponModel.coupon_id andLocation:@"Coupon Detail Page" andOrigin:nil andIsMall:NO];
    
    NSMutableDictionary *pamars = [[NSMutableDictionary alloc] init];
    [pamars setValue:self.couponId forKey:@"couponId"];
    [pamars setValue:[IMGUser currentUser].userId forKey:@"userId"];
    [pamars setValue:couponModel.shop_id forKey:@"restaurantId"];
    
    [CouponHandler couponRedeem:pamars andSuccessBlock:^{
        couponModel.redeemed = @1;
        couponView.model = couponModel;
        employeeView.model = couponModel;
        
    } anderrorBlock:^{
        
    }];
}


@end
