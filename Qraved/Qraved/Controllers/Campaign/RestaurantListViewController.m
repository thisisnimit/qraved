//
//  RestaurantListViewController.m
//  Qraved
//
//  Created by harry on 2018/1/17.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "RestaurantListViewController.h"
#import "V2_DiscoverListHandler.h"
#import "HeaderForJournalView.h"
#import "V2_DiscoverListJournalSeeAllViewController.h"
#import "IMGMediaComment.h"
#import "JournalDetailViewController.h"
#import "LoadingView.h"
#import "SearchUtil.h"
#import "HomeService.h"
#import "V2_DiscoverListResultRestaurantTableViewCell.h"
#import "DetailViewController.h"
#import "V2_ArticleView.h"
#import "V2_FoodTagSuggestionView.h"
#import "V2_DistrictView.h"
#import "V2_DistrictModel.h"
#import "V2_SearchBar.h"
#import "SearchEmptyResultView.h"
#import "RestaurantSuggestViewController.h"

@interface RestaurantListViewController ()<UITableViewDelegate, UITableViewDataSource,V2_DiscoverListResultRestaurantTableViewCellDelegate,UISearchBarDelegate,SearchEmptyResultViewDelegate>
{
    int offset;
    UIButton *cityButton;
    NSMutableArray *restaurantArr;
    NSMutableArray *districtArr;
    NSMutableArray *foodTagArr;
    UIView *searchView;
    UITableView *restaurantTableView;
    V2_ArticleView *articleView;

    V2_DiscoverListResultRestaurantTableViewCell *currentCell;
    
    UIView *foodTagView;
    DiscoverCategoriesModel *selectFoodTag;
    BOOL isUpdateFoodTag;
    
    UIButton *btnSearch;
    V2_DistrictView *districtView;
    NSString *currentDistrict;
    NSString *currentLandMark;
    NSString *currentArea;
    BOOL isCheckLocation;
    
    V2_SearchBar *searchBar;
    UIImageView *imgSearch;
    NSString *keyWordStr;
    SearchEmptyResultView *emptyView;
    NSNumber *numOfRestaurant;
    
    IMGShimmerView *shimmerView;
}

@end

@implementation RestaurantListViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar addSubview:cityButton];
    [self.navigationController.navigationBar layoutIfNeeded];
    
    if (isCheckLocation && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
        isCheckLocation = NO;
        [self refreshInstagram:NEARBYTITLE];
    }
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [cityButton removeFromSuperview];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self getDistrict];
    [self loadNavbutton];
    [self loadData];
    [self requestData];
    [self loadMainUI];
}

- (void)backClick{
    [IMGAmplitudeUtil trackCampaignWithName:@"CL - Close See All Outlets" andCampaignId:nil andBrandId:self.model.brandId andCouponId:nil andLocation:nil andOrigin:nil andIsMall:self.isMall];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - btn city
- (void)loadNavbutton{
    UIBarButtonItem *leftBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_back"] style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
    leftBtn.tintColor=[UIColor blackColor];
    self.navigationItem.leftBarButtonItem=leftBtn;
    
    cityButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if (self.isMall) {
        [cityButton setTitle:self.model.name forState:UIControlStateNormal];
    }else{
        if ([[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT] != nil ) {
            [cityButton setTitle:[[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT] capitalizedString] forState:UIControlStateNormal];
        }
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"currentCityId"] != nil && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied){
            [cityButton setTitle:NEARBYTITLE forState:UIControlStateNormal];
        }
    }
    
    
    [cityButton setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    cityButton.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    if (!self.isMall) {
        [cityButton setImage:[UIImage imageNamed:@"ic_city_down"] forState:UIControlStateNormal];
        [cityButton addTarget:self action:@selector(doSelectCity:) forControlEvents:UIControlEventTouchUpInside];
        cityButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 15);
        [cityButton.layer setAffineTransform:CGAffineTransformMakeScale(-1, 1)];
        [cityButton.titleLabel.layer setAffineTransform:CGAffineTransformMakeScale(-1, 1)];
    }
    
    
    CGSize titleSize = [cityButton.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17]}];
    
    cityButton.height = 35;
    cityButton.width= titleSize.width + 50;
    
    cityButton.layer.masksToBounds = YES;
    cityButton.layer.cornerRadius = 18;
    cityButton.backgroundColor = [UIColor colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1];;
    
    cityButton.x = (self.view.width - cityButton.width)/2;
    cityButton.y = 4;
    
    btnSearch = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSearch.frame = CGRectMake(DeviceWidth-65, 20, 50, 44);
    btnSearch.imageEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0);
    [btnSearch setImage:[UIImage imageNamed:@"ic_home_search_black"] forState:UIControlStateNormal];
    [btnSearch addTarget:self action:@selector(searchClick) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSearch];
}

- (void)searchClick{
    [IMGAmplitudeUtil trackCampaignWithName:@"CL - Search Outlets" andCampaignId:nil andBrandId:nil andCouponId:nil andLocation:@"See All Outlets Page" andOrigin:nil andIsMall:self.isMall];
    
    searchView.sd_layout
    .heightIs(55);
}

- (void)loadData{
    offset = 0;
    isUpdateFoodTag = YES;
    districtArr = [NSMutableArray array];
    foodTagArr = [NSMutableArray array];
    restaurantArr = [NSMutableArray array];
}

- (void)getDistrict{
    [HomeService getLocation:nil andIsPreferred:NO andBlock:^(id locationArr) {
        [districtArr addObjectsFromArray:locationArr];
    } andError:^(NSError *error) {
        
    }];
}

- (void)requestData{
    [self getFoodTag];
    [self getRestaurant];
}

- (void)getFoodTag{
    NSMutableDictionary *pamars = [[NSMutableDictionary alloc] init];
    if (selectFoodTag != nil) {
        [pamars setValue:selectFoodTag.myID forKey:@"footTagId"];
    }
    if (![Tools isBlankString:currentDistrict]) {
        [pamars setValue:currentDistrict  forKey:@"districtIds"];
    }

    if (![Tools isBlankString:currentLandMark]) {
        [pamars setValue:currentLandMark  forKey:@"landmarkIds"];
    }
    
    [HomeService getFoodTag:pamars andBlock:^(id locationArr) {
        [foodTagArr removeAllObjects];
        isUpdateFoodTag = YES;
        if (selectFoodTag !=nil && [locationArr count]>0) {
            for (DiscoverCategoriesModel *model in locationArr) {
                if ([model.myID isEqual:selectFoodTag.myID] && [model.name isEqualToString:selectFoodTag.name]) {
                    [foodTagArr insertObject:model atIndex:0];
                }else{
                    [foodTagArr addObject:model];
                }
            }
        }else if ([locationArr count]>0){
            [foodTagArr addObjectsFromArray:locationArr];
        }
        [restaurantTableView reloadData];
        
    } andError:^(NSError *error) {
        
    }];
}

- (void)getRestaurant{
    NSString *latitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"];
    NSString *longitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"];
    
    NSMutableDictionary *pamars = [[NSMutableDictionary alloc] init];
    [pamars setValue:@"popularity" forKey:@"sortby"];
    
    [pamars setValue:self.model.city_id forKey:@"cityID"];
    [pamars setValue:latitude forKey:@"latitude"];
    [pamars setValue:longitude forKey:@"longitude"];
    [pamars setValue:[NSString stringWithFormat:@"%d",offset] forKey:@"offset"];
    [pamars setValue:@"10" forKey:@"max"];
    
    if (selectFoodTag != nil) {
        [pamars setValue:selectFoodTag.myID forKey:@"foodTagIds"];
    }
    if (self.isMall) {
        [pamars setValue:self.model.brandId forKey:@"landmarkIDs"];
    }else{
        [pamars setValue:self.model.brandId forKey:@"brandId"];
    }
    
    if (![Tools isBlankString:currentDistrict]) {
        [pamars setValue:currentDistrict  forKey:@"districtIDs"];
    }
    
    if (![Tools isBlankString:currentLandMark]) {
        [pamars setValue:currentLandMark  forKey:@"landmarkIDs"];
    }
    
    if (![Tools isBlankString:currentArea]) {
        [pamars setValue:currentArea  forKey:@"areaIDs"];
    }
    
    if (!self.isMall) {
        if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
            [pamars setValue:@"distance" forKey:@"sortby"];
            [pamars setValue:@"asc" forKey:@"order"];
        }
    }
    
    if (![Tools isBlankString:keyWordStr]) {
        [pamars setValue:keyWordStr forKey:@"keyword"];
    }
    
    [SearchUtil getSearchResault:pamars andSuccessBlock:^(NSMutableArray *array, NSNumber *restaurantCount) {
        [restaurantTableView.mj_footer endRefreshing];
        
        if (offset == 0) {
            [restaurantArr removeAllObjects];
            [shimmerView removeFromSuperview];
        }
        numOfRestaurant = restaurantCount;
        if ([array isKindOfClass:[NSMutableArray class]]) {
            if (array.count>0) {
                offset += 10;
            }
            [restaurantArr addObjectsFromArray:array];
        }
        
        if (restaurantArr.count==0) {
            emptyView.hidden = NO;
        }else{
            emptyView.hidden = YES;
        }
        
        [restaurantTableView reloadData];
    } anderrorBlock:^{
        [shimmerView removeFromSuperview];
        [restaurantTableView.mj_footer endRefreshing];
    }];
}

- (void)createEmptyView{
    emptyView = [[SearchEmptyResultView alloc]initWithFrame:CGRectMake(0, 55, DeviceWidth, DeviceHeight-100)];
    emptyView.delegate = self;
    emptyView.hidden = YES;
    [self.view addSubview:emptyView];
}

- (void)loadMainUI{
    self.view.backgroundColor = [UIColor whiteColor];
    
    searchView = [UIView new];
    [self loadSearchBar];
    
    restaurantTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 0) style:UITableViewStyleGrouped];
    restaurantTableView.delegate = self;
    restaurantTableView.dataSource = self;
    restaurantTableView.backgroundColor = [UIColor whiteColor];
    restaurantTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    if (@available(iOS 11.0, *)) {
        restaurantTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        restaurantTableView.estimatedRowHeight = 0;
    }else{
        restaurantTableView.contentInset = UIEdgeInsetsMake(-35, 0, 0, 0);
    }
    
    [self.view sd_addSubviews:@[searchView, restaurantTableView]];
    
    searchView.sd_layout
    .topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .heightIs(0);
    
    restaurantTableView.sd_layout
    .topSpaceToView(searchView, 0)
    .leftEqualToView(searchView)
    .rightEqualToView(searchView)
    .bottomSpaceToView(self.view, 0);
    
    restaurantTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    
    [self createEmptyView];
    shimmerView = [[IMGShimmerView alloc] initWithFrame:self.view.bounds];
    [shimmerView createRestaurantShimmerView];
    [self.view addSubview:shimmerView];
}

- (void)loadMoreData{
    [self getRestaurant];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return restaurantArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *idenStr = @"restaurantCell";
    V2_DiscoverListResultRestaurantTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
    if (!cell) {
        cell = [[V2_DiscoverListResultRestaurantTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
        cell.selectionStyle = UITableViewCellStyleDefault;
        cell.Delegate = self;
    }
    cell.model = restaurantArr[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    IMGRestaurant *model =  restaurantArr[indexPath.row];
    return [restaurantTableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[V2_DiscoverListResultRestaurantTableViewCell class] contentViewWidth:DeviceWidth];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    if (self.isMall) {
        if (isUpdateFoodTag) {
            foodTagView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 62)];
            foodTagView.backgroundColor = [UIColor whiteColor];
            
            V2_FoodTagSuggestionView *tagView = [[V2_FoodTagSuggestionView alloc] initWithFrame:CGRectMake(0, 7, DeviceWidth, 48)];
            [foodTagView addSubview:tagView];
            
            if (foodTagArr.count > 0) {
                tagView.foodTagArr = foodTagArr;
                tagView.selectModel = selectFoodTag;
                __weak typeof(self) weakSelf = self;
                 __weak typeof(tagView) weakTagView = tagView;
                tagView.selectFoodTag = ^(DiscoverCategoriesModel *model, NSInteger index) {
                    if ([selectFoodTag.myID isEqual:model.myID] && [selectFoodTag.name isEqualToString:model.name]) {
                        selectFoodTag = nil;
                    }else{
                        selectFoodTag = model;
                        weakTagView.selectModel = model;
                    }
                    offset=0;
                    isUpdateFoodTag = NO;
                    [weakSelf getRestaurant];
                };
            }
            
            [view addSubview:foodTagView];
        }else{
            [view addSubview:foodTagView];
        }
    }
    UIView *restaurantHeader = [[UIView alloc] init];
    restaurantHeader.backgroundColor = [UIColor whiteColor];
    if (restaurantArr.count > 0) {
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 20, DeviceWidth-30, 15)];
        lblTitle.font = [UIFont boldSystemFontOfSize:14];
        
        NSString *numStr;
        if ([numOfRestaurant isEqual:@1]) {
            numStr = [NSString stringWithFormat:@"%@ outlet in",numOfRestaurant];
        }else{
            numStr = [NSString stringWithFormat:@"%@ outlets in",numOfRestaurant];
        }
        
        if (self.isMall) {
            lblTitle.text = [NSString stringWithFormat:@"%@ %@",numStr,self.model.name];
        }else{
            lblTitle.text = [NSString stringWithFormat:@"%@ %@",numStr, [self.model.city_name capitalizedString]];
        }
        lblTitle.textColor = [UIColor color333333];
        [restaurantHeader addSubview:lblTitle];
        
        restaurantHeader.frame = CGRectMake(0,self.isMall ? foodTagView.endPointY + 5 : 0, DeviceWidth, 35);
    }else{
        restaurantHeader.hidden = YES;
        restaurantHeader.frame = CGRectMake(0, self.isMall ? foodTagView.endPointY : 0, DeviceWidth, 0);
    }
    [view addSubview:restaurantHeader];

    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    CGFloat foodTagHeight = foodTagArr.count>0 ? 62 : 0.0f;
    CGFloat restaurantHeight = 0.0f;
    if (restaurantArr.count >0) {
        restaurantHeight = self.isMall ? 40 : 35;
    }else{
        restaurantHeight = 0;
    }
    return self.isMall ? foodTagHeight + restaurantHeight : restaurantHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    IMGRestaurant *model = [restaurantArr objectAtIndex:indexPath.row];
    [IMGAmplitudeUtil trackCampaignWithName:@"CL - View RDP" andCampaignId:nil andBrandId:self.model.brandId andCouponId:nil andLocation:@"See All Outlets Page" andOrigin:nil andIsMall:self.isMall];
    
    [IMGAmplitudeUtil trackCampaignWithName:@"RC - View RDP" andCampaignId:nil andBrandId:self.model.brandId andCouponId:nil andLocation:nil andOrigin:@"See All Outlets Page" andIsMall:self.isMall];
    
    [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Page" andRestaurantId:model.restaurantId andRestaurantTitle:model.title andLocation:nil andOrigin:@"Restaurant search result page" andType:nil];
    DetailViewController *DetailVC = [[DetailViewController alloc] initWithRestaurant:model];
    [self.navigationController pushViewController:DetailVC animated:YES];
}

#pragma mark - cell delegate
- (void)delegateLikeBtn:(UITableViewCell *)cell andRestaurant:(IMGRestaurant *)restaurant{
    
    currentCell = (V2_DiscoverListResultRestaurantTableViewCell *)cell;
    [CommonMethod doSaveWithRestaurant:restaurant andViewController:self andCallBackBlock:^(BOOL isSaved) {
        [currentCell refreshSavedButton:isSaved];
        if (isSaved) {
            [IMGAmplitudeUtil trackSavedWithName:@"BM - Save Restaurant Succeed" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andOrigin:nil andListId:nil andType:@"Custom List" andLocation:@"See All Outlets Page"];
        }
    }];
}

- (void)doSelectCity:(UIButton *)btn{
    if (districtView) {
        [districtView removeFromSuperview];
    }
    [IMGAmplitudeUtil trackCampaignWithName:@"CL - Restaurant Nearby" andCampaignId:nil andBrandId:nil andCouponId:nil andLocation:@"See All Outlets Page" andOrigin:nil andIsMall:NO];
    districtView = [[V2_DistrictView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+20)];
    districtView.districtArr = districtArr;
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
        [districtView changeLocationSkip];
    }else{
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"currentCityId"] == nil ) {
            [districtView changeLocationSkip];
        }
    }
    
    __weak typeof(self) weakSelf = self;
    districtView.selectedDistrict = ^(id model){
        
        if ([model isKindOfClass:[V2_AreaModel class]]) {
            [weakSelf filterByArea:model];
        }else if ([model isKindOfClass:[V2_DistrictModel class]]){
            [weakSelf filterByDistrict:model];
        }
    };
    
    districtView.nearbyCheck = ^{
        if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied) {
            isCheckLocation = YES;
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"To continue, let your device turn on location" delegate:weakSelf cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
            [alert show];
            
        }else{
            [weakSelf refreshInstagram:NEARBYTITLE];
        }
    };
    
    
    districtView.alpha = 0;
    [[AppDelegate ShareApp].window addSubview:districtView];
    [UIView animateWithDuration:0.3 animations:^{
        districtView.alpha = 1;
    }];
    
}

- (void)filterByArea:(V2_AreaModel *)model{
    [self resetSelectArea];
    if ([model.type isEqualToString:@"landmark"]) {
        currentLandMark = [NSString stringWithFormat:@"%@",model.districtId];
    }else{
        currentDistrict = [NSString stringWithFormat:@"%@",model.districtId];
    }
    
    [IMGAmplitudeUtil trackDiscoverWithName:@"SC - Filter Location Succeed" andFoodName:nil andLocationName:model.name andLocation:nil];
    
    [self refreshInstagram:model.name];
    isUpdateFoodTag = YES;
    [self getFoodTag];
}

- (void)resetSelectArea{
    currentLandMark = @"";
    currentDistrict = @"";
    currentArea = @"";
}

- (void)filterByDistrict:(V2_DistrictModel *)model{
    [self resetSelectArea];
    
    [IMGAmplitudeUtil trackDiscoverWithName:@"SC - Filter Location Succeed" andFoodName:nil andLocationName:model.name andLocation:nil];
    
    currentArea = [NSString stringWithFormat:@"%@",model.districtId];
    [self refreshInstagram:model.name];
}

- (void)refreshInstagram:(NSString *)name{
    
    [cityButton setTitle:name forState:UIControlStateNormal];
    CGSize titleSize = [cityButton.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17]}];
    cityButton.height = 35;
    cityButton.width=titleSize.width+50;
    cityButton.y = 4;
    cityButton.x = (self.view.width - cityButton.width) / 2;
    
    offset=0;
    [self getRestaurant];
    
}

#pragma mark  -loadSearchBar
- (void)loadSearchBar{
    
    searchBar = [[V2_SearchBar alloc] initWithFrame:CGRectMake(5, 0, DeviceWidth - 90, 55)];
    searchBar.delegate = self;
    searchBar.enablesReturnKeyAutomatically=NO;
    searchBar.returnKeyType=UIReturnKeySearch;
    [searchView addSubview:searchBar];
    searchBar.showsCancelButton = NO;
    searchBar.placeholder = L(@"Search restaurants");
    
    UIButton *cancelBtn =[UIButton buttonWithType: UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(DeviceWidth - 80, 13, 70, 30);
    [cancelBtn setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:[UIColor colortextBlue] forState:UIControlStateNormal];
    cancelBtn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    [cancelBtn addTarget:self action:@selector(cancelBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [searchView addSubview:cancelBtn];
    
    [[[searchBar.subviews objectAtIndex:0].subviews objectAtIndex:1] setTintColor:[UIColor lightGrayColor]];
    UIView *searchTextField = [[[searchBar.subviews firstObject]subviews]lastObject];
    searchTextField.backgroundColor =[UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1];
    for (UIView *subview in searchBar.subviews) {
        for(UIView* grandSonView in subview.subviews){
            if ([grandSonView isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                grandSonView.alpha = 0.0f;
            }else if([grandSonView isKindOfClass:NSClassFromString(@"UISearchBarTextField")] ){
            }else{
                grandSonView.alpha = 0.0f;
            }
        }
        
    }
    
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    [imgSearch removeObserver:self forKeyPath:@"frame"];
    imgSearch.frame = CGRectMake(8, 7.5, 13, 13);
    [imgSearch addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];
}
#pragma mark -UISearchBarDelegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    if ([searchBar.text isEqualToString:searchBar.placeholder]) {
        searchBar.text = @"";
    }
    UITextField *searchField = [searchBar valueForKey:@"_searchField"];
    searchField.textColor = [UIColor color333333];
    searchField.clearButtonMode = UITextFieldViewModeWhileEditing;
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self updateSeachBar];
}
#pragma mark -clearButtonMode
- (void)updateSeachBar
{
    if ([searchBar.text isEqualToString:@""]) {
        UITextField *searchField = [searchBar valueForKey:@"_searchField"];
        searchField.textColor = [searchField valueForKeyPath:@"_placeholderLabel.textColor"];
        searchField.text = searchField.placeholder;
        searchField.clearButtonMode = UITextFieldViewModeNever;
    }
}
- (void)dealloc
{
    [imgSearch removeObserver:self forKeyPath:@"frame"];
}

- (void)cancelBtnClick:(UIButton *)cancel{
    
    searchView.sd_layout
    .heightIs(0);
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    keyWordStr = searchBar.text;
    [searchBar resignFirstResponder];
    [IMGAmplitudeUtil trackCampaignWithName:@"SC - Search Outlets" andCampaignId:nil andBrandId:self.model.brandId andCouponId:nil andLocation:@"See All Outlets Page" andOrigin:nil andIsMall:self.isMall];
    offset = 0;
    [self getRestaurant];
}

- (void)gotoSuggest{
    RestaurantSuggestViewController *rsvc = [[RestaurantSuggestViewController alloc] init];
    rsvc.amplitudeType = @"Restaurant search result page";
    [self.navigationController pushViewController:rsvc animated:YES];
}

@end
