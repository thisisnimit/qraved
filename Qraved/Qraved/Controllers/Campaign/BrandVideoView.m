//
//  BrandVideoView.m
//  Qraved
//
//  Created by harry on 2018/1/11.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "BrandVideoView.h"
#import "VideoView.h"
@interface BrandVideoView ()<UIScrollViewDelegate>{
    UIScrollView *videoScrollView;
    UILabel *lblTitle;
    UIPageControl *pageControl;
}

@end
#define VIDEO_SIZE_HEIGHT (DeviceWidth-30)*.55
@implementation BrandVideoView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    self.backgroundColor = [UIColor whiteColor];
    
    lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 20, DeviceWidth-30, 16)];
    lblTitle.text = @"Video";
    lblTitle.font = [UIFont boldSystemFontOfSize:14];
    lblTitle.textColor = [UIColor color333333];
    lblTitle.hidden = YES;
    [self addSubview:lblTitle];
    
    videoScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 50, DeviceWidth, VIDEO_SIZE_HEIGHT + 80)];
    videoScrollView.pagingEnabled = YES;
    videoScrollView.delegate = self;
    videoScrollView.showsHorizontalScrollIndicator = NO;
    [self addSubview:videoScrollView];
    
    
    
    pageControl = [[UIPageControl alloc] init];
    pageControl.hidden = YES;
    pageControl.frame = CGRectMake(0, videoScrollView.endPointY+15, 20, 20);
    pageControl.centerX = self.centerX;
    pageControl.currentPage = 0;
    pageControl.currentPageIndicatorTintColor = [UIColor colorWithHexString:@"D20000"];
    pageControl.pageIndicatorTintColor = [UIColor colorWithHexString:@"C0C0C0"];
    [self addSubview:pageControl];
}

//- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
//    return self.videoArray.count;
//}
//
//- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    VideoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"videoCell" forIndexPath:indexPath];
//    BrandVideoModel *videoModel = [self.videoArray objectAtIndex:indexPath.item];
//    cell.model = videoModel;
//
//    return cell;
//}

- (void)setVideoArray:(NSArray *)videoArray{
    _videoArray = videoArray;
    
    if (videoArray.count>0) {
        lblTitle.hidden = NO;
    }else{
        lblTitle.hidden = YES;
    }
    
    if (videoArray.count > 1) {
        pageControl.hidden = NO;
        pageControl.numberOfPages = videoArray.count;
        [self setupAutoHeightWithBottomView:videoScrollView bottomMargin:50];
    }else{
        pageControl.hidden = YES;
        [self setupAutoHeightWithBottomView:videoScrollView bottomMargin:15];
    }
    
    [videoScrollView setContentSize:CGSizeMake(DeviceWidth*videoArray.count, 0)];
    
    
    for (int i = 0; i < videoArray.count; i ++) {
        BrandVideoModel *videoModel = [videoArray objectAtIndex:i];
        VideoView *videoView = [[VideoView alloc] initWithFrame:CGRectMake(DeviceWidth*i, 0, DeviceWidth, videoScrollView.size.height)];
        videoView.videoPlay = ^{
            if (self.playingVideo) {
                self.playingVideo();
            }
        };
        videoView.model = videoModel;
        [videoScrollView addSubview:videoView];
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == videoScrollView) {
        int index =  scrollView.contentOffset.x / DeviceWidth;
        pageControl.currentPage = index;
    }
    
}

@end
