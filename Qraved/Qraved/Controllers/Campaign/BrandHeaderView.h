//
//  BrandHeaderView.h
//  Qraved
//
//  Created by harry on 2018/1/10.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BrandModel.h"
#import "CouponModel.h"
#import "IMGMediaComment.h"
#import "CampaignModel.h"
@protocol BrandHeaderViewDelegate <NSObject>

- (void)journalCardTapped:(IMGMediaComment *)model;
- (void)couponCardTapped:(CouponModel *)model andIndex:(NSInteger)index;
- (void)eventCardTapped:(CampaignModel *)model andIndex:(NSInteger)index;
- (void)webSiteTapped:(NSString *)urlString;
- (void)instagramPlaces:(NSNumber *)locationId;
- (void)phoneTapped:(NSString *)phoneNumber;
- (void)seeAllCoupon;
- (void)seeAllInstagram;
- (void)playingVideo;
- (void)getDirection;
- (void)mapButtonTapped;

@end

@interface BrandHeaderView : UIView<SDCycleScrollViewDelegate>

@property (nonatomic, assign) BOOL isMall;
@property (nonatomic, strong) BrandModel *model;
@property (nonatomic, weak) id <BrandHeaderViewDelegate> delegate;

- (CGFloat)heightForBrand:(BrandModel *)model;

@end
