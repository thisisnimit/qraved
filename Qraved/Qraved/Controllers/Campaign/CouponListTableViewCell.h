//
//  CouponTableViewCell.h
//  Qraved
//
//  Created by harry on 2018/3/16.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CouponModel.h"
@interface CouponListTableViewCell : UITableViewCell

@property (nonatomic, strong) CouponModel *model;
@end
