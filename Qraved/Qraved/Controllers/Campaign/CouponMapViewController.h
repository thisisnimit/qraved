//
//  CouponMapViewController.h
//  Qraved
//
//  Created by harry on 2018/1/29.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "BrandModel.h"
@interface CouponMapViewController : BaseChildViewController

@property (nonatomic, strong)BrandModel *model;

@end
