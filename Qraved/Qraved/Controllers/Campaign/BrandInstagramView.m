//
//  BrandInstagramView.m
//  Qraved
//
//  Created by harry on 2018/1/10.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "BrandInstagramView.h"
#import "V2_InstagramModel.h"
#import "UIColor+Hex.h"
#import <BlocksKit/BlocksKit+UIKit.h>
#import "InstagramFilterView.h"
@interface BrandInstagramView ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    UICollectionView *instagramCollectionView;
    UILabel *lblTitle;
    UIPageControl *pageControl;
    UICollectionViewFlowLayout *layout;
}

@end
#define ITEM_SIZE_WIDTH (DeviceWidth-2)/3
@implementation BrandInstagramView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    self.backgroundColor = [UIColor whiteColor];
    
    lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 20, DeviceWidth-30, 16)];
    lblTitle.text = @"Trending Photos on Instagram";
    lblTitle.font = [UIFont boldSystemFontOfSize:14];
    lblTitle.textColor = [UIColor color333333];
    lblTitle.hidden = YES;
    [self addSubview:lblTitle];
    
    UIButton *btnChange = [UIButton buttonWithType:UIButtonTypeCustom];
    btnChange.frame = CGRectMake(DeviceWidth-93, 5, 78, 45);
    [btnChange addTarget:self action:@selector(seeAllClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnChange];
    [btnChange setImage:[UIImage imageNamed:@"ic_home_arrow"] forState:UIControlStateNormal];
    btnChange.imageEdgeInsets = UIEdgeInsetsMake(0, 70, 0, 0);
    
    [btnChange setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
    btnChange.titleLabel.font = [UIFont systemFontOfSize:12];
    [btnChange setTitle:@"See All" forState:UIControlStateNormal];
    
    layout  = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(ITEM_SIZE_WIDTH, ITEM_SIZE_WIDTH);
    layout.minimumLineSpacing = 1;
    layout.minimumInteritemSpacing = 1;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    instagramCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,50, DeviceWidth, ITEM_SIZE_WIDTH*2+1) collectionViewLayout:layout];
    instagramCollectionView.backgroundColor = [UIColor whiteColor];
    instagramCollectionView.delegate = self;
    instagramCollectionView.dataSource = self;
    instagramCollectionView.pagingEnabled = YES;
    instagramCollectionView.showsHorizontalScrollIndicator = NO;
    [self addSubview:instagramCollectionView];
    
    
    [instagramCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"instagramCell"];
    
    pageControl = [[UIPageControl alloc] init];
    pageControl.hidden = YES;
    pageControl.frame = CGRectMake(0, instagramCollectionView.endPointY+15, 20, 20);
    pageControl.centerX = self.centerX;
    pageControl.currentPage = 0;
    pageControl.currentPageIndicatorTintColor = [UIColor colorWithHexString:@"D20000"];
    pageControl.pageIndicatorTintColor = [UIColor colorWithHexString:@"C0C0C0"];
    [self addSubview:pageControl];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.instagramArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"instagramCell" forIndexPath:indexPath];
    V2_InstagramModel *model = [self.instagramArr objectAtIndex:indexPath.row];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.contentView.bounds];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    [imageView sd_setImageWithURL:[NSURL URLWithString:model.low_resolution_image] placeholderImage:[UIImage imageNamed:@"placeholder"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];

    [cell.contentView addSubview:imageView];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self seeAllClick];
}

- (void)setInstagramArr:(NSArray *)instagramArr{
    _instagramArr = instagramArr;
    [instagramCollectionView reloadData];

    if (instagramArr.count > 0) {
        
        lblTitle.hidden = NO;
        NSInteger count;
        if (instagramArr.count%6 > 0) {
            count = instagramArr.count/6 + 1;
        }else{
            count = instagramArr.count/6;
        }
        if (count==1) {
            layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        }
        pageControl.numberOfPages = count;
    }else{
        lblTitle.hidden = YES;
    }
    
    if (instagramArr.count>6) {
        pageControl.hidden = NO;
        [self setupAutoHeightWithBottomView:instagramCollectionView bottomMargin:50];
    }else{
        pageControl.hidden = YES;
        [self setupAutoHeightWithBottomView:instagramCollectionView bottomMargin:15];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == instagramCollectionView) {
        int index =  scrollView.contentOffset.x / DeviceWidth;
        if (scrollView.contentOffset.x - DeviceWidth * index > 0) {
            index+=1;
        }
        pageControl.currentPage = index;
        
    }
}

- (void)seeAllClick{
    if (self.instagramSeeAll) {
        self.instagramSeeAll();
    }
}

@end
