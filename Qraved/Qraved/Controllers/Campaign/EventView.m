//
//  EventView.m
//  Qraved
//
//  Created by harry on 2018/1/16.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "EventView.h"

@interface EventView ()<UIScrollViewDelegate>{
    UIScrollView *campaignScrollView;
    UILabel *lblTitle;
    UIPageControl *pageControl;
}

@end

#define CAMPAIGN_SIZE_HEIGHT (DeviceWidth-30)*.6
@implementation EventView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    self.backgroundColor = [UIColor whiteColor];
    
    lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 20, DeviceWidth-30, 16)];
    lblTitle.text = @"Events & Updates";
    lblTitle.font = [UIFont boldSystemFontOfSize:14];
    lblTitle.textColor = [UIColor color333333];
    lblTitle.hidden = YES;
    [self addSubview:lblTitle];
    
    campaignScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 50, DeviceWidth, CAMPAIGN_SIZE_HEIGHT + 64)];
    campaignScrollView.pagingEnabled = YES;
    campaignScrollView.delegate = self;
    campaignScrollView.showsHorizontalScrollIndicator = NO;
    [self addSubview:campaignScrollView];
    
    
    pageControl = [[UIPageControl alloc] init];
    pageControl.hidden = YES;
    pageControl.frame = CGRectMake(0, campaignScrollView.endPointY+15, 20, 20);
    pageControl.centerX = self.centerX;
    pageControl.currentPage = 0;
    pageControl.currentPageIndicatorTintColor = [UIColor colorWithHexString:@"D20000"];
    pageControl.pageIndicatorTintColor = [UIColor colorWithHexString:@"C0C0C0"];
    [self addSubview:pageControl];
}

- (void)setCampaignArray:(NSArray *)campaignArray{
    _campaignArray = campaignArray;
    
    if (campaignArray.count>0) {
        lblTitle.hidden = NO;
    }else{
        lblTitle.hidden = YES;
    }
    if (campaignArray.count>1) {
        pageControl.hidden = NO;
        pageControl.numberOfPages = campaignArray.count;
        
        [self setupAutoHeightWithBottomView:campaignScrollView bottomMargin:50];
    }else{
        pageControl.hidden = YES;
        [self setupAutoHeightWithBottomView:campaignScrollView bottomMargin:15];
    }
    
    [campaignScrollView setContentSize:CGSizeMake(DeviceWidth*campaignArray.count, 0)];
    
    
    
    for (int i = 0; i < campaignArray.count; i ++) {
        CampaignModel *campaignModel = [campaignArray objectAtIndex:i];
        UIView *view = [self createCampaignViewWithModel:campaignModel];
        
        view.userInteractionEnabled = YES;
        view.tag = i;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(eventTapped:)];
        [view addGestureRecognizer:tap];
        
        view.frame = CGRectMake(DeviceWidth*i, 0, DeviceWidth, campaignScrollView.size.height);
        [campaignScrollView addSubview:view];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == campaignScrollView) {
        int index =  scrollView.contentOffset.x / DeviceWidth;
        pageControl.currentPage = index;
    }
}

- (UIView *)createCampaignViewWithModel:(CampaignModel *)model{
    
    UIView *view = [[UIView alloc] init];
    
    UIImageView *campaignImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 0, DeviceWidth-30, CAMPAIGN_SIZE_HEIGHT)];
    campaignImageView.contentMode = UIViewContentModeScaleAspectFill;
    campaignImageView.clipsToBounds = YES;
    if ([model.main_photo hasPrefix:@"http"]) {
        [campaignImageView sd_setImageWithURL:[NSURL URLWithString:model.main_photo] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    }else{
        [campaignImageView sd_setImageWithURL:[NSURL URLWithString:[model.main_photo returnFullImageUrl]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    }
    
    [view addSubview:campaignImageView];
    
    UILabel *lblCampaign = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, campaignImageView.endPointY + 7, DeviceWidth - LEFTLEFTSET*2, 18)];
    lblCampaign.font = [UIFont boldSystemFontOfSize:14];
    lblCampaign.text = model.title;
    lblCampaign.textColor = [UIColor color333333];
    [view addSubview:lblCampaign];
    
    UILabel *lblInfo = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, lblCampaign.endPointY + 5, DeviceWidth - LEFTLEFTSET*2, 36)];
    lblInfo.textColor = [UIColor color333333];
    lblInfo.font = [UIFont systemFontOfSize:14];
    lblInfo.text = model.campaignDescription;
    lblInfo.numberOfLines = 2;
    [view addSubview:lblInfo];
    
    return view;
}

- (void)eventTapped:(UITapGestureRecognizer *)tap{
    CampaignModel *model = [self.campaignArray objectAtIndex:tap.view.tag];
    
    if (self.eventTapped) {
        self.eventTapped(model, tap.view.tag);
    }
}

@end
