//
//  BrandViewController.m
//  Qraved
//
//  Created by harry on 2018/1/9.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "BrandViewController.h"
#import "BrandHeaderView.h"
#import "BrandModel.h"
#import "HomeService.h"
#import "CouponView.h"
#import "CouponModel.h"
#import "BrandInstagramView.h"
#import "BrandVideoView.h"
#import "BrandVideoModel.h"
#import "V2_DiscoverListResultRestaurantTableViewCell.h"
#import "EventView.h"
#import "CampaignModel.h"
#import "V2_DiscoverListHandler.h"
#import "V2_ArticleView.h"
#import "MallMapView.h"
#import "RestaurantListViewController.h"
#import "CouponDetailViewController.h"
#import "V2_PhotoDetailOpenInstagramViewController.h"
#import "JournalDetailViewController.h"
#import "DetailViewController.h"
#import "CouponHandler.h"
#import "SearchUtil.h"
#import "SendCall.h"
#import "V2_InstagramViewController.h"
#import "MapViewController.h"
#import "CouponMapViewController.h"
#import "CampaignWebViewController.h"
#import "IMGActivityItemProvider.h"
#import "CouponListViewController.h"
#import "BrandSplashView.h"
#import "InstagramPlacesViewController.h"

@interface BrandViewController ()<UITableViewDelegate,UITableViewDataSource,V2_DiscoverListResultRestaurantTableViewCellDelegate,BrandHeaderViewDelegate,UIActionSheetDelegate>
{
    UITableView *brandTableView;
    NSMutableArray *restaurantArr;
    NSMutableArray *campaignArr;
    NSMutableArray *journalArr;
    BrandModel *brandModel;
    int offset;
    
    UIView *navigationView;
    UIButton *backButton;
    UIButton *shareButton;
    
    V2_DiscoverListResultRestaurantTableViewCell *currentCell;
    NSNumber *numOfRestaurant;
    
    IMGShimmerView *shimmerView;
}
@end

@implementation BrandViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

//    NSString *splashStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"BrandSplash"];
//    if (!self.isMall && [self.brandId isEqualToString:@"3"] && splashStr == nil) {
//        [self createSpalashView];
//    }
    [self loadData];
    [self requestData];
    [self loadMainUI];
    [self loadNavButton];
}

- (void)createSpalashView{
    [UIView animateWithDuration:0.5 animations:^{
        BrandSplashView *splashView = [[BrandSplashView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHEIGHT)];
        splashView.layer.zPosition = 1000;
        __weak typeof(splashView) weakSplashView = splashView;
        splashView.dismissSplash = ^{
            [weakSplashView removeFromSuperview];
        };
        
        splashView.reminderTapped = ^{
            [weakSplashView removeFromSuperview];
            [[NSUserDefaults standardUserDefaults] setObject:@"BrandSplash" forKey:@"BrandSplash"];
        };
        [[UIApplication sharedApplication].keyWindow addSubview:splashView];
    }];
}

- (void)loadNavButton{
    navigationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, IMG_StatusBarAndNavigationBarHeight)];
    navigationView.backgroundColor=[UIColor clearColor];
    navigationView.userInteractionEnabled=YES;
    navigationView.layer.zPosition=999999;
    
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"icon_wrong"] forState:UIControlStateNormal];
    backButton.frame = CGRectMake(LEFTLEFTSET, IMG_StatusBarHeight, 60,44);
    [backButton addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    backButton.imageEdgeInsets = UIEdgeInsetsMake(12, 0, 12, 40);
    
    shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareButton setImage:[UIImage imageNamed:@"ic_share_guide"] forState:UIControlStateNormal];
    shareButton.frame = CGRectMake(DeviceWidth-LEFTLEFTSET-44, IMG_StatusBarHeight, 60, 44);
    [shareButton addTarget:self action:@selector(shareButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    [navigationView addSubview:backButton];
    [navigationView addSubview:shareButton];
    [self.view addSubview:navigationView];

}

- (void)loadData{
    offset = 0;
    campaignArr = [NSMutableArray array];
    journalArr = [NSMutableArray array];
    restaurantArr = [NSMutableArray array];
}

- (void)requestData{
    if (self.isMall) {
        [self getMall];
    }else{
        [self getBrand];
    }
}

- (void)loadMainUI{
    brandTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+20) style:UITableViewStyleGrouped];
    brandTableView.backgroundColor = [UIColor whiteColor];
    if (@available(iOS 11.0, *)) {
        brandTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    brandTableView.delegate = self;
    brandTableView.dataSource = self;
    brandTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    brandTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:brandTableView];
    brandTableView.bounces = NO;
    
    shimmerView = [[IMGShimmerView alloc] initWithFrame:self.view.bounds];
    [shimmerView createBrandAndMallShimmerView];
    [self.view addSubview:shimmerView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return restaurantArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
    V2_DiscoverListResultRestaurantTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[V2_DiscoverListResultRestaurantTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellStyleDefault;
        cell.Delegate = self;
    }
    cell.model = restaurantArr[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    IMGRestaurant *model =  restaurantArr[indexPath.row];
    return [brandTableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[V2_DiscoverListResultRestaurantTableViewCell class] contentViewWidth:DeviceWidth];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    if (restaurantArr.count>0 ) {
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 48)];
        
        UIButton *btnSeeAll = [UIButton buttonWithType:UIButtonTypeCustom];
        btnSeeAll.frame = CGRectMake(0, 0, DeviceWidth, 48);
        [btnSeeAll setTitle:@"See All" forState:UIControlStateNormal];
        [btnSeeAll setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
        btnSeeAll.titleLabel.font = [UIFont systemFontOfSize:12];
        [btnSeeAll addTarget:self action:@selector(resturantSeeAll) forControlEvents:UIControlEventTouchUpInside];
        [footerView addSubview:btnSeeAll];
        
        CGSize size = [@"See All" sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(DeviceWidth, 48) lineBreakMode:NSLineBreakByWordWrapping];
        
        [btnSeeAll setImage:[UIImage imageNamed:@"ic_home_arrow"] forState:UIControlStateNormal];
        btnSeeAll.imageEdgeInsets = UIEdgeInsetsMake(0, size.width+60, 0, 0);
        
        return footerView;
    }else{
        return [UIView new];
    }
   
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (restaurantArr.count > 0) {
        return 48;
    }else{
        return 0.001;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    UIView *view = [UIView new];
    view.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    
    BrandHeaderView *headerView = [[BrandHeaderView alloc] init];
    headerView.delegate = self;
    headerView.isMall = self.isMall;
    if (brandModel != nil) {
        headerView.model = brandModel;
    }else{
        headerView.hidden = YES;
    }
    
    UIView *restaurantView = [UIView new];
    restaurantView.backgroundColor = [UIColor whiteColor];
    restaurantView.userInteractionEnabled = YES;
    
    [view sd_addSubviews:@[headerView, restaurantView]];
    
    UILabel *lblResTitle = [UILabel new];
    lblResTitle.font = [UIFont boldSystemFontOfSize:14];
    lblResTitle.textColor = [UIColor color333333];
    if (restaurantArr.count >0) {
        NSString *numStr;
        if ([numOfRestaurant isEqual:@1]) {
            numStr = [NSString stringWithFormat:@"%@ outlet in",numOfRestaurant];
        }else{
            numStr = [NSString stringWithFormat:@"%@ outlets in",numOfRestaurant];
        }
        
        if (self.isMall) {
            lblResTitle.text = [NSString stringWithFormat:@"%@ %@",numStr,brandModel.name];
        }else{
            lblResTitle.text = [NSString stringWithFormat:@"%@ %@",numStr, [brandModel.city_name capitalizedString]];
        }
        
    }else{
        restaurantView.hidden = YES;
    }
    
    [restaurantView addSubview:lblResTitle];
    
    headerView.sd_layout
    .topSpaceToView(view, 0)
    .leftSpaceToView(view, 0)
    .rightSpaceToView(view, 0)
    .autoHeightRatio(0);
    
    restaurantView.sd_layout
    .topSpaceToView(headerView, 5)
    .leftEqualToView(headerView)
    .rightEqualToView(headerView)
    .heightIs(restaurantArr.count>0 ? 35 : 0);
    
    lblResTitle.sd_layout
    .topSpaceToView(restaurantView, 20)
    .leftSpaceToView(restaurantView, 15)
    .rightSpaceToView(restaurantView, 98)
    .heightIs(15);
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    BrandHeaderView *view = [BrandHeaderView new];
    view.isMall = self.isMall;
    if (brandModel != nil) {
        if (restaurantArr.count >0 ) {
            return [view heightForBrand:brandModel] + 40;
        }else{
            return [view heightForBrand:brandModel];
        }
    }else{
        return 0.001;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [IMGAmplitudeUtil trackCampaignWithName:@"RC - View RDP" andCampaignId:nil andBrandId:brandModel.brandId andCouponId:nil andLocation:nil andOrigin:@"See All Outlets Page" andIsMall:self.isMall];
    [IMGAmplitudeUtil trackCampaignWithName:@"CL - View RDP" andCampaignId:nil andBrandId:brandModel.brandId andCouponId:nil andLocation:@"See All Outlets Page" andOrigin:nil andIsMall:self.isMall];
    IMGRestaurant *model = [restaurantArr objectAtIndex:indexPath.row];
    NSLog(@"%@",model.restaurantId);
    
    DetailViewController *DetailVC = [[DetailViewController alloc] initWithRestaurant:model];
    [self.navigationController pushViewController:DetailVC animated:YES];
}

#pragma mark - cell delegate
- (void)delegateLikeBtn:(UITableViewCell *)cell andRestaurant:(IMGRestaurant *)restaurant{
    currentCell = (V2_DiscoverListResultRestaurantTableViewCell *)cell;
    [CommonMethod doSaveWithRestaurant:restaurant andViewController:self andCallBackBlock:^(BOOL isSaved) {
        [currentCell refreshSavedButton:isSaved];
    }];
}

#pragma mark - BrandHeaderViewDelegate
- (void)couponCardTapped:(CouponModel *)model andIndex:(NSInteger)index{
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    if (model.coupon_id != nil) {
        [param setValue:model.coupon_id forKey:@"Coupon_ID"];
    }
    NSString *idStr = self.isMall ? @"Mall_ID" : @"Brand_ID";
    if (brandModel.brandId != nil) {
        [param setValue:brandModel.brandId forKey:idStr];
    }
    [param setValue:[NSNumber numberWithInteger:index+1] forKey:@"Position"];
    
    [param setValue:self.isMall?@"Mall Page":@"Brand Page" forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL - Coupon Detail Page" withEventProperties:param];
    
    [IMGAmplitudeUtil trackCampaignWithName:@"RC - View Coupon Detail" andCampaignId:nil andBrandId:nil andCouponId:model.coupon_id andLocation:nil andOrigin:self.isMall?@"Mall Page":@"Brand Page" andIsMall:self.isMall];
    
    CouponDetailViewController *couponDetail = [[CouponDetailViewController alloc] init];
    couponDetail.couponId = model.coupon_id;
    couponDetail.canReturn = YES;
    MLNavigationController *nav = [[MLNavigationController alloc] initWithRootViewController:couponDetail];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

- (void)eventCardTapped:(CampaignModel *)model andIndex:(NSInteger)index{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    if (model.campaignId != nil) {
        [param setValue:model.campaignId forKey:@"Campaign_ID"];
    }
    NSString *idStr = self.isMall ? @"Mall_ID" : @"Brand_ID";
    if (brandModel.brandId != nil) {
        [param setValue:brandModel.brandId forKey:idStr];
    }
    
    if (model.article_id != nil) {
        [param setValue:model.article_id forKey:@"Journal_ID"];
    }
    
    if (model.campaign_url != nil) {
        [param setValue:model.campaign_url forKey:@"URL"];
    }
    
    [param setValue:[NSNumber numberWithInteger:index+1] forKey:@"Position"];
    
    [param setValue:self.isMall?@"Mall Page":@"Brand Page" forKey:@"Origin"];
    [[Amplitude instance] logEvent:@"CL - Campaign Detail Page" withEventProperties:param];
    
    NSString *urlStr;
    if ([model.type isEqualToString:@"url"]) {
        urlStr = model.outlink_url;
    }else if ([model.type isEqualToString:@"jdp"]){
        urlStr = model.campaign_url;
    }
    
    CampaignWebViewController *webViewController = [[CampaignWebViewController alloc] init];
    webViewController.campaignUrl = urlStr;
    [self.navigationController pushViewController:webViewController animated:YES];
}

- (void)phoneTapped:(NSString *)phoneNumber{
    if(@available(iOS 11.0, *)){
        [SendCall sendCall:phoneNumber];
    }else{
        [UIAlertView bk_showAlertViewWithTitle:nil message:phoneNumber cancelButtonTitle:@"Cancel" otherButtonTitles:@[@"Call"] handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if(buttonIndex==0){
                [alertView dismissWithClickedButtonIndex:0 animated:NO];
            }else if (buttonIndex==1){
                [SendCall sendCall:phoneNumber];
            }
        }];
    }
}

- (void)mapButtonTapped{
    
    CouponMapViewController *mapViewController = [[CouponMapViewController alloc] init];
    mapViewController.model = brandModel;
    [self.navigationController pushViewController:mapViewController animated:YES];
}

- (void)getDirection{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]]) {
        NSLog(@"YES");
        
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Open in Google Maps",@"Open in Apple Maps", nil];
        
        [sheet showInView:self.view];
        
    }else{
        [self openInAppleMap];
    }
}

- (void)seeAllInstagram{
    
    [IMGAmplitudeUtil trackCampaignWithName:@"CL - Trending Instagram" andCampaignId:nil andBrandId:nil andCouponId:nil andLocation:self.isMall?@"Mall Page":@"Brand Page" andOrigin:nil andIsMall:self.isMall];
    
    V2_InstagramViewController *instagramViewController = [[V2_InstagramViewController alloc] init];
    instagramViewController.isMall = self.isMall;
    instagramViewController.model = brandModel;
    [self.navigationController pushViewController:instagramViewController animated:YES];
}

- (void)seeAllCoupon{
    [IMGAmplitudeUtil trackCampaignWithName:@"CL - See All Coupons" andCampaignId:nil andBrandId:nil andCouponId:nil andLocation:self.isMall?@"Mall Page":@"Brand Page" andOrigin:nil andIsMall:NO];
    
    [IMGAmplitudeUtil trackCampaignWithName:@"RC - See All Coupons" andCampaignId:nil andBrandId:brandModel.brandId andCouponId:nil andLocation:nil andOrigin:self.isMall?@"Mall Page":@"Brand Page" andIsMall:self.isMall];
    
    CouponListViewController *couponListViewController = [[CouponListViewController alloc] init];
    couponListViewController.isMall = self.isMall;
    couponListViewController.targetId = brandModel.brandId;
    
    [self.navigationController pushViewController:couponListViewController animated:YES];
}

- (void)seeAllRestaurant{
    [IMGAmplitudeUtil trackCampaignWithName:@"CL - See All Outlets Page" andCampaignId:nil andBrandId:brandModel.brandId andCouponId:nil andLocation:self.isMall?@"Mall Page":@"Brand Page" andOrigin:nil andIsMall:self.isMall];
    [IMGAmplitudeUtil trackCampaignWithName:@"RC - See All Outlets" andCampaignId:nil andBrandId:brandModel.brandId andCouponId:nil andLocation:nil andOrigin:self.isMall?@"Mall Page":@"Brand Page" andIsMall:self.isMall];
    
    RestaurantListViewController *restaurantList = [[RestaurantListViewController alloc] init];
    [self.navigationController pushViewController:restaurantList animated:YES];
}

- (void)instagramPlaces:(NSNumber *)locationId{
    InstagramPlacesViewController *instagramPlacesViewController = [[InstagramPlacesViewController alloc] init];
    instagramPlacesViewController.instagramLocationId = [NSString stringWithFormat:@"%@",locationId];
    [self.navigationController pushViewController:instagramPlacesViewController animated:YES];
}

- (void)webSiteTapped:(NSString *)urlString{
    V2_PhotoDetailOpenInstagramViewController *webSite = [[V2_PhotoDetailOpenInstagramViewController alloc] init];
    webSite.instagram_link = urlString;
    webSite.isFromWebSite = YES;
    [self.navigationController pushViewController:webSite animated:YES];
}

- (void)journalCardTapped:(IMGMediaComment *)model{
    
    [IMGAmplitudeUtil trackCampaignWithName:@"RC - View Journal Detail" andCampaignId:nil andBrandId:nil andCouponId:nil andLocation:nil andOrigin:self.isMall?@"Mall Page":@"Brand Page" andIsMall:self.isMall];
    
    JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
    jdvc.journal = model;
    [self.navigationController pushViewController:jdvc animated:YES];
}

- (void)playingVideo{
    [IMGAmplitudeUtil trackCampaignWithName:@"CL - Campaign Video" andCampaignId:nil andBrandId:brandModel.brandId andCouponId:nil andLocation:self.isMall?@"Mall Page":@"Brand Page" andOrigin:nil andIsMall:self.isMall];
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
        NSString *appName = [infoDictionary objectForKey:@"CFBundleDisplayName"];
        
        CLLocationCoordinate2D _end = CLLocationCoordinate2DMake([brandModel.latitude doubleValue], [brandModel.longitude doubleValue]);
        NSString *urlString = [[NSString stringWithFormat:@"comgooglemaps://?x-source=%@&x-success=%@&saddr=&daddr=%f,%f&directionsmode=driving",appName,@"comgooglemaps",_end.latitude,_end.longitude]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *aURL = [NSURL URLWithString:urlString];
        
        [[UIApplication sharedApplication] openURL:aURL];
        
    }else if (buttonIndex==1){
        [self openInAppleMap];
    }
}



- (void)openInAppleMap{
    CLLocationCoordinate2D _end = CLLocationCoordinate2DMake([brandModel.latitude doubleValue], [brandModel.longitude doubleValue]);
    MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:_end addressDictionary:nil] ];
    toLocation.name = brandModel.name;
    [MKMapItem openMapsWithItems:[NSArray arrayWithObjects: toLocation, nil]
                   launchOptions:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:MKLaunchOptionsDirectionsModeDriving, [NSNumber numberWithBool:YES], nil]
                                                             forKeys:[NSArray arrayWithObjects:MKLaunchOptionsDirectionsModeKey, MKLaunchOptionsShowsTrafficKey, nil]]];
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
 
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)requestRestaurant{

    NSString *latitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"];
    NSString *longitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"];
     NSMutableDictionary *pamars = [[NSMutableDictionary alloc] init];
    [pamars setValue:brandModel.city_id forKey:@"cityID"];
    [pamars setValue:latitude forKey:@"latitude"];
    [pamars setValue:longitude forKey:@"longitude"];
    [pamars setValue:[NSString stringWithFormat:@"%d",offset] forKey:@"offset"];
    [pamars setValue:@"4" forKey:@"max"];
    [pamars setValue:@"popularity" forKey:@"sortby"];
    if (self.isMall) {
        [pamars setValue:brandModel.brandId forKey:@"landmarkIDs"];
    }else{
        [pamars setValue:brandModel.brandId forKey:@"brandId"];
    }
    
    if (!self.isMall) {
        if ([CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
            [pamars setValue:@"distance" forKey:@"sortby"];
            [pamars setValue:@"asc" forKey:@"order"];
        }
    }
    
    
    [SearchUtil getSearchResault:pamars andSuccessBlock:^(NSMutableArray *array, NSNumber *restaurantCount) {
   
        [shimmerView removeFromSuperview];
        numOfRestaurant = restaurantCount;
        if ([array isKindOfClass:[NSMutableArray class]]) {
            [restaurantArr addObjectsFromArray:array];
        }
        [brandTableView reloadData];
        
    } anderrorBlock:^{
        [brandTableView reloadData];
        [shimmerView removeFromSuperview];
    }];
}

- (void)getMall{
    NSMutableDictionary *pamars = [NSMutableDictionary dictionary];
    [pamars setValue:self.mallId forKey:@"mallId"];
    
    [CouponHandler getMall:pamars andSuccessBlock:^(BrandModel *model) {
    
        brandModel = model;
        
        [self requestRestaurant];
    } anderrorBlock:^{
        [self requestRestaurant];
    }];
}

- (void)getBrand{
    NSNumber *cityID = [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    NSString *latitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"];
    NSString *longitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"];
    NSMutableDictionary *pamars = [[NSMutableDictionary alloc] init];
    [pamars setValue:cityID ? cityID : @2 forKey:@"cityId"];
    [pamars setValue:latitude forKey:@"latitude"];
    [pamars setValue:longitude forKey:@"longitude"];
    [pamars setValue:self.brandId forKey:@"brandId"];
    
    
    [CouponHandler getBrand:pamars andSuccessBlock:^(BrandModel *model) {
        
        brandModel = model;
         [self requestRestaurant];
        
    } anderrorBlock:^{
         [self requestRestaurant];
    }];
}

- (void)backClick{
    [IMGAmplitudeUtil trackBrandPageWithName:self.isMall?@"CL - Close Mall Page":@"CL - Close Brand Page" andBrandId:brandModel.brandId andIsMall:self.isMall];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)shareButtonClick{
    NSString *shareText = [NSString stringWithFormat:@"Check out %@ @Qraved!", brandModel.name];
    NSString *shareStr;
    if (self.isMall) {
        shareStr = [NSString stringWithFormat:@"%@mall/%@",QRAVED_WEB_SERVER_OLD,brandModel.brandId];
    }else{
        shareStr = [NSString stringWithFormat:@"%@brand/%@",QRAVED_WEB_SERVER_OLD,brandModel.brandId];
    }
    NSURL *shareUrl = [NSURL URLWithString:shareStr];
    
    IMGActivityItemProvider *itemProvider = [[IMGActivityItemProvider alloc] initWithPlaceholderItem:@""];
    itemProvider.title = shareText;
    itemProvider.url = shareUrl;

    UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[itemProvider] applicationActivities:nil];
    activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];

    activityCtl.completionWithItemsHandler =^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
        [IMGAmplitudeUtil trackBrandPageWithName:self.isMall?@"SH - Share Mall Page":@"SH - Share Brand Page" andBrandId:brandModel.brandId andIsMall:self.isMall];
    };
    if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
        // iOS8
    }
    
    
    [self presentViewController:activityCtl animated:YES completion:^{
        
    }];
}

- (void)resturantSeeAll{
    [IMGAmplitudeUtil trackCampaignWithName:@"CL - See All Outlets Page" andCampaignId:nil andBrandId:brandModel.brandId andCouponId:nil andLocation:self.isMall?@"Mall Page":@"Brand Page" andOrigin:nil andIsMall:self.isMall];
    
    RestaurantListViewController *restaurantList = [[RestaurantListViewController alloc] init];
    restaurantList.isMall = self.isMall;
    restaurantList.model = brandModel;
    [self.navigationController pushViewController:restaurantList animated:YES];
}

-(void)scrollViewDidScroll:(UIScrollView *)scroll {
    if(scroll.contentOffset.y>IMG_StatusBarAndNavigationBarHeight){
        [UIView animateWithDuration:0.3 animations:^{
            
            [self blackNav];
        }];
    }else{
        [UIView animateWithDuration:0.3 animations:^{
            [self whiteNav];
            
        }];
    }
}

- (void)whiteNav{
    navigationView.backgroundColor = [UIColor clearColor];
    [backButton setImage:[UIImage imageNamed:@"icon_wrong"] forState:UIControlStateNormal];
    [shareButton setImage:[UIImage imageNamed:@"ic_share_guide"] forState:UIControlStateNormal];
}

- (void)blackNav{
    navigationView.backgroundColor = [UIColor whiteColor];
    [backButton setImage:[UIImage imageNamed:@"icon_black_wrong"] forState:UIControlStateNormal];
    [shareButton setImage:[UIImage imageNamed:@"ic_share_guide_black"] forState:UIControlStateNormal];

}

@end
