//
//  BrandBannerModel.h
//  Qraved
//
//  Created by harry on 2018/1/19.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BrandBannerModel : NSObject

@property (nonatomic, copy) NSString *image_url;
@property (nonatomic, copy) NSString *object_type;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *sub_title;
@property (nonatomic, copy) NSString *object_value;
@property (nonatomic, copy) NSNumber *object_id;
@property (nonatomic, copy) NSNumber *bannerId;

@end
