//
//  CampaignWebViewController.m
//  Qraved
//
//  Created by harry on 2018/2/2.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "CampaignWebViewController.h"

@interface CampaignWebViewController ()<UIWebViewDelegate>

@end

@implementation CampaignWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
    [self loadNavButton];
}

- (void)loadNavButton{
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"icon_black_wrong"] forState:UIControlStateNormal];
    backButton.frame = CGRectMake(LEFTLEFTSET, IMG_StatusBarHeight, 60,44);
    [backButton addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    backButton.imageEdgeInsets = UIEdgeInsetsMake(12, 0, 12, 40);
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[LoadingView sharedLoadingView] stopLoading];;
}

- (void)initUI{
    UIWebView *webv = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight +20 - IMG_StatusBarAndNavigationBarHeight)];
    webv.backgroundColor = [UIColor whiteColor];
    webv.delegate = self;
    NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:self.campaignUrl]];
    [webv loadRequest:request];
    [self.view addSubview:webv];
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [[LoadingView sharedLoadingView] startLoading];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [[LoadingView sharedLoadingView] stopLoading];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSLog(@"%@",request.URL.absoluteString);
    if ([request.URL.absoluteString hasPrefix:@"https://www.qraved.com/coupon/"]) {
        return NO;
    }
    return YES;
}

- (void)backClick{
 
    [self.navigationController popViewControllerAnimated:YES];
}

@end
