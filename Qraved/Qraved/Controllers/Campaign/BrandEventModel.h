//
//  BrandEventModel.h
//  Qraved
//
//  Created by harry on 2018/1/19.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BrandEventModel : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *start_date;
@property (nonatomic, copy) NSString *main_photo;
@property (nonatomic, copy) NSString *end_date;
@property (nonatomic, copy) NSNumber *article_id;
@property (nonatomic, copy) NSNumber *sort_order;
@property (nonatomic, copy) NSNumber *status;

@end
