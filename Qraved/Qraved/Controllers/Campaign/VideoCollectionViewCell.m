//
//  VideoCollectionViewCell.m
//  Qraved
//
//  Created by harry on 2018/1/11.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "VideoCollectionViewCell.h"

@implementation VideoCollectionViewCell
{
    UILabel *lblTitle;
    UILabel *lblDescription;
    YTPlayerView *playerView;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    playerView = [[YTPlayerView alloc] initWithFrame:CGRectMake(15, 0, DeviceWidth-30, (DeviceWidth-30)*.55)];
    
    lblTitle = [UILabel new];
    lblTitle.font = [UIFont boldSystemFontOfSize:14];
    lblTitle.textColor = [UIColor color333333];
    
    lblDescription = [UILabel new];
    lblDescription.font = [UIFont systemFontOfSize:14];
    lblDescription.textColor = [UIColor color333333];
    
    [self.contentView sd_addSubviews:@[playerView, lblTitle, lblDescription]];
    
//    self.playerView.sd_layout
//    .topSpaceToView(self.contentView, 0)
//    .leftSpaceToView(self.contentView, 15)
//    .rightSpaceToView(self.contentView, 15)
//    .heightIs((DeviceWidth-30)*.55);
    
    lblTitle.sd_layout
    .topSpaceToView(playerView, 5)
    .leftEqualToView(playerView)
    .rightEqualToView(playerView)
    .heightIs(36);
    
    [lblTitle setNumberOfLines:2];
    
    lblDescription.sd_layout
    .topSpaceToView(lblTitle, 5)
    .leftEqualToView(lblTitle)
    .rightEqualToView(lblTitle)
    .heightIs(36);
    
}

- (void)setModel:(BrandVideoModel *)model{
    _model = model;
    
    NSString* URL=[[NSString alloc]init];;
    if ([model.video_url rangeOfString:@"embed/"].location != NSNotFound) {
        NSRange range=  [model.video_url rangeOfString:@"embed/"];
        URL=[model.video_url substringFromIndex:range.location+range.length];
    }else if ([model.video_url rangeOfString:@"https://youtu.be/"].location != NSNotFound){
        NSRange range = [model.video_url rangeOfString:@"https://youtu.be/"];
        URL = [model.video_url substringFromIndex:range.location+range.length];
    }else if ([model.video_url rangeOfString:@"https://www.youtube.com/watch?v="].location != NSNotFound){
        NSRange range = [model.video_url rangeOfString:@"https://www.youtube.com/watch?v="];
        URL = [model.video_url substringFromIndex:range.location+range.length];
    }else{
        URL = model.video_url;
    }
    
    if ([URL rangeOfString:@"?"].location!=NSNotFound) {
        URL=[URL substringToIndex:[URL rangeOfString:@"?"].location];
    }
    [playerView loadWithVideoId:URL];
    [playerView playVideo];
    
//    lblTitle.text = model.videoName;
//    lblDescription.text = model.videoDescription;

}

@end
