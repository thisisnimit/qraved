//
//  CouponEmployeeView.m
//  Qraved
//
//  Created by harry on 2018/1/25.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "CouponEmployeeView.h"

@implementation CouponEmployeeView
{
    UIButton *btnRedeem;
    UIView *employeeView;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    self.backgroundColor = [UIColor whiteColor];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor colorWithHexString:@"DCE0E0"];
    
    employeeView = [self createEmployeeView];
    
    
    btnRedeem = [UIButton new];
    btnRedeem.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    btnRedeem.backgroundColor = [UIColor colorWithHexString:@"D20000"];
    [btnRedeem setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnRedeem setTitle:@"Use Now" forState:UIControlStateNormal];
    [btnRedeem addTarget:self action:@selector(redeemButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self sd_addSubviews:@[lineView, employeeView, btnRedeem]];
    
    lineView.sd_layout
    .topSpaceToView(self, 0)
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .heightIs(1);
    
    employeeView.sd_layout
    .topSpaceToView(self, 21)
    .leftSpaceToView(self, 15)
    .rightSpaceToView(self, 15)
    .heightIs(64);
    
    employeeView.layer.masksToBounds = YES;
    employeeView.layer.borderWidth = 1;
    employeeView.layer.borderColor = [UIColor colorWithHexString:@"EAEAEA"].CGColor;
    employeeView.layer.cornerRadius = 3;
    
    btnRedeem.sd_layout
    .topSpaceToView(employeeView, 15)
    .leftSpaceToView(self, 15)
    .rightSpaceToView(self, 15)
    .heightIs(42);
    btnRedeem.layer.masksToBounds = YES;
    btnRedeem.layer.cornerRadius = 4;
    
    
    
}

- (UIView *)createEmployeeView{
    UIView *view = [UIView new];
    
    UIImageView *imgEmployee = [UIImageView new];
    imgEmployee.image = [UIImage imageNamed:@"ic_employee"];
    
    UILabel *lblEmployee = [UILabel new];
    lblEmployee.font = [UIFont systemFontOfSize:14];
    lblEmployee.textColor = [UIColor color333333];
    lblEmployee.text = @"Please show this screen to an employee";
    [view sd_addSubviews:@[imgEmployee, lblEmployee]];
    
    imgEmployee.sd_layout
    .topSpaceToView(view, 11)
    .leftSpaceToView(view, 11)
    .widthIs(42)
    .heightEqualToWidth();
    
    lblEmployee.sd_layout
    .centerYEqualToView(imgEmployee)
    .leftSpaceToView(imgEmployee, 12)
    .rightSpaceToView(view, 15)
    .autoHeightRatio(0);
    
    [lblEmployee setMaxNumberOfLinesToShow:2];
    
    return view;
}


- (void)setModel:(CouponModel *)model{
    _model = model;

    if ([model.redeemed isEqual:@1]) {
        [btnRedeem setTitle:@"Used" forState:UIControlStateNormal];
        btnRedeem.backgroundColor = [UIColor colorWithHexString:@"#999999"];
        btnRedeem.enabled = NO;
    }
    
    if (![Tools isBlankString:model.status_text]) {
        btnRedeem.backgroundColor = [UIColor colorWithHexString:@"#999999"];
        btnRedeem.enabled = NO;
        employeeView.sd_layout
        .topSpaceToView(self, 0)
        .heightIs(0);
    }
    
    if ([model.total_inventory intValue]<=0 && [model.saved isEqual:@0]) {
        btnRedeem.backgroundColor = [UIColor colorWithHexString:@"#999999"];
        btnRedeem.enabled = NO;
        employeeView.sd_layout
        .topSpaceToView(self, 0)
        .heightIs(0);
    }
    
    
    [self setupAutoHeightWithBottomView:btnRedeem bottomMargin:15];
}

- (void)redeemButtonTapped{
    if (self.redeemTapped) {
        self.redeemTapped(btnRedeem);
    }
}


@end
