//
//  CouponTableViewCell.m
//  Qraved
//
//  Created by harry on 2018/3/16.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "CouponListTableViewCell.h"

@implementation CouponListTableViewCell
{
    UIImageView *couponImageView;
    UILabel *lblTitle;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initUI];
    }
    return self;
}

- (void)initUI{
    couponImageView = [UIImageView new];
    couponImageView.contentMode = UIViewContentModeScaleAspectFill;
    couponImageView.clipsToBounds = YES;
    
    lblTitle = [UILabel new];
    lblTitle.font = [UIFont systemFontOfSize:14];
    lblTitle.textColor = [UIColor color333333];
    lblTitle.numberOfLines = 0;
    
    [self.contentView sd_addSubviews:@[couponImageView, lblTitle]];
    
    couponImageView.sd_layout
    .topSpaceToView(self.contentView, 20)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .heightIs((DeviceWidth-30)*0.6);
    
    lblTitle.sd_layout
    .topSpaceToView(couponImageView, 11)
    .leftEqualToView(couponImageView)
    .rightEqualToView(couponImageView)
    .autoHeightRatio(0);
}

- (void)setModel:(CouponModel *)model{
    _model = model;
    
    [couponImageView sd_setImageWithURL:[NSURL URLWithString:[model.thumbnail_image_url returnCurrentImageString]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:nil];
    
    lblTitle.text = model.name;
    
    [self setupAutoHeightWithBottomView:lblTitle bottomMargin:20];
}

@end

