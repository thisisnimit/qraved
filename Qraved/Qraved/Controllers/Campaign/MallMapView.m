//
//  MallMapView.m
//  Qraved
//
//  Created by harry on 2018/1/17.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "MallMapView.h"
#import <MapKit/MapKit.h>

@interface MallMapView ()<MKMapViewDelegate>{
    UIView *mapView;
    MKMapView *map;
    UILabel *lblTitle;
    UILabel *lblAddress;
}

@end

@implementation MallMapView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    self.backgroundColor = [UIColor whiteColor];
    
    lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 20, DeviceWidth-30, 16)];
    lblTitle.text = @"Maps";
    lblTitle.font = [UIFont boldSystemFontOfSize:14];
    lblTitle.textColor = [UIColor color333333];
    [self addSubview:lblTitle];
    
    mapView=[[UIView alloc]init];
    mapView.backgroundColor = [UIColor whiteColor];
    [self addSubview:mapView];
    map=[[MKMapView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 115)];
    [map setMapType:MKMapTypeStandard];
    [map setDelegate:self];
    [mapView addSubview:map];
    
    [mapView setFrame:CGRectMake(0,50, DeviceWidth, 115)];

    UIButton *mapBackground=[UIButton buttonWithType:UIButtonTypeCustom];
    [mapBackground setFrame:CGRectMake(0, 0, DeviceWidth, mapView.frame.size.height)];
    [mapBackground addTarget:self action:@selector(gotoMap) forControlEvents:UIControlEventTouchUpInside];
    [mapView addSubview:mapBackground];
    
    NSArray *imgArr = @[@"ic_location_on",@"instagram",@"btnImage100",@"btnImage101"];
    for (int i = 0; i < imgArr.count; i ++) {
        CGFloat btnHeight = 0.0f;
        if (i == 0) {
            btnHeight = 69;
        }else if (i == 3) {
            btnHeight = 74;
        }else{
            btnHeight = 52;
        }
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        if (i == 0) {
            btn.frame = CGRectMake(0, mapView.endPointY + 52 * i, DeviceWidth, btnHeight);
        }else{
            btn.frame = CGRectMake(0, mapView.endPointY + 69 +52 * (i-1), DeviceWidth, btnHeight);
        }
        
        btn.tag = 100 + i;
        [self addSubview:btn];
        
        [btn addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(15, (btnHeight - 24)/2, 24, 24)];
        imgView.image = [UIImage imageNamed:[imgArr objectAtIndex:i]];
        [btn addSubview:imgView];
        
        UILabel *lblName = [[UILabel alloc] initWithFrame:CGRectMake(59, (btnHeight - 36)/2, DeviceWidth-110, 36)];
        lblName.font = [UIFont systemFontOfSize:14];
        lblName.textColor = [UIColor color333333];
        lblName.tag = 200 + i;
        lblName.numberOfLines = 2;
        [btn addSubview:lblName];
        
        if (i!=3) {
            UIImageView *imgArrow = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth - 25, (btnHeight - 13)/2, 8, 13)];
            imgArrow.image = [UIImage imageNamed:@"ic_arrow_mall"];
            [btn addSubview:imgArrow];
        }

        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(59, btnHeight-1, DeviceWidth-59, 1)];
        lineView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
        [btn addSubview:lineView];
    }
    
    UIButton *btnDirection = [UIButton buttonWithType:UIButtonTypeCustom];
    btnDirection.frame = CGRectMake(15, mapView.endPointY + 52 * imgArr.count + 60, DeviceWidth-30, 42);
    btnDirection.backgroundColor = [UIColor colorWithHexString:@"D20000"];
    btnDirection.layer.cornerRadius = 4;
    [btnDirection setTitle:@"Get Directions" forState:UIControlStateNormal];
    [btnDirection setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnDirection addTarget:self action:@selector(directionTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnDirection];
}

- (void)setModel:(BrandModel *)model{
    _model = model;
    
    CLLocationCoordinate2D coor;
    coor.latitude = [model.latitude doubleValue];
    coor.longitude = [model.longitude doubleValue];
    CLLocationCoordinate2D theCoordinate =CLLocationCoordinate2DMake(coor.latitude, coor.longitude);
    MKCoordinateSpan  theSpan;
    theSpan.latitudeDelta=0.01;
    theSpan.longitudeDelta=0.01;
    MKCoordinateRegion theRegion;
    theRegion.center=theCoordinate;
    theRegion.span=theSpan;
    MKPointAnnotation *pointAnnatation= [[MKPointAnnotation alloc] init];
    if(CLLocationCoordinate2DIsValid(theCoordinate))
    {
        [map setRegion:theRegion animated:NO];
        
        [map regionThatFits:theRegion];
        
        pointAnnatation.coordinate=theCoordinate;
        
        [map addAnnotation:pointAnnatation];
    }
    
//    CGSize expectedSize = [[[restaurant address] filterHtml] sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14] constrainedToSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET-50, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
    
    UILabel *lblLocation = [self viewWithTag:200];
    lblLocation.text = model.address;
    
    UILabel *lblInstagram = [self viewWithTag:201];
    lblInstagram.text = @"Instagram Places";
    
    UILabel *lblPhone = [self viewWithTag:202];
    lblPhone.text = model.phone;
    
    UILabel *lblTime = [self viewWithTag:203];
    lblTime.text = [NSString stringWithFormat:@"Opening Hours\n%@",model.opening_hours];
    
}

#pragma mark - MapViewDelegate
- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation{
    MKAnnotationView *pinView = nil;
    static NSString *defaultPinID = @"imaginato_map";
    pinView = (MKAnnotationView *)[theMapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    if ( pinView == nil ) pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
    pinView.canShowCallout = YES;
    pinView.image = [[UIImage imageNamed:@"ic_red_pin.png"] imageByScalingAndCroppingForSize:CGSizeMake(27, 33)];
    return pinView;
}

-(void)gotoMap{
    
    IMGUser *user = [IMGUser currentUser];
    NSNumber *userId = [[NSNumber alloc] init];
    if (user.userId != nil)
    {
        userId = user.userId;
    }
    else
        userId = [NSNumber numberWithInt:0];
    if (self.mapTapped) {
        self.mapTapped();
    }
    //    [TrackHandler trackWithUserId:userId andRestaurantId:restaurant.restaurantId andContent:[NSDictionary dictionaryWithObject:@"1" forKey:@"type"]];
    
//    MapViewController *mapViewController=[[MapViewController alloc]initWithRestaurant:self.restaurant];
//    mapViewController.amplitudeType=@"Restaurant detail page";
//    mapViewController.restaurantId=self.restaurant.restaurantId;
//    mapViewController.fromDetail = YES;
//    [self.controller pushViewController:mapViewController animated:YES];
}

- (void)buttonTapped:(UIButton *)button{
    NSInteger tag = button.tag;
    if (tag == 100) {
        [self gotoMap];
    }else if (tag == 101){
        
        if (self.instagramTapped) {
            self.instagramTapped(self.model.instagram_location_id);
        }
        
    }else if (tag == 102){
        
        if (self.phoneTapped) {
            self.phoneTapped(self.model.phone);
        }
    }else{
        
    }
}

- (void)directionTapped:(UIButton *)button{
    if (self.getDirection) {
        self.getDirection();
    }
}


@end
