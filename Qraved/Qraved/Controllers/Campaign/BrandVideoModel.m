//
//  BrandVideoModel.m
//  Qraved
//
//  Created by harry on 2018/1/11.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "BrandVideoModel.h"

@implementation BrandVideoModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"description"]) {
        self.videoDescription = value;
    }
}

@end
