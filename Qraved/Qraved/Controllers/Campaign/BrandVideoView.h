//
//  BrandVideoView.h
//  Qraved
//
//  Created by harry on 2018/1/11.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BrandVideoModel.h"
@interface BrandVideoView : UIView

@property (nonatomic, copy) NSArray *videoArray;
@property (nonatomic, copy) void(^playingVideo)();
@end
