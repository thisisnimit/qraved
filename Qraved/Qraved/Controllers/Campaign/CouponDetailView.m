//
//  CouponDetailView.m
//  Qraved
//
//  Created by harry on 2018/1/22.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "CouponDetailView.h"
@implementation CouponDetailView
{
    UIImageView *brandImageView;
    UILabel *lblBrandTitle;
    UILabel *lblBrandType;
    
    UIView *informationView;
    UILabel *lblTitle;
    UILabel *lblRemaining;
    UILabel *lblDate;
    UILabel *lblCount;
    
    UIImageView *couponImageView;
    
    UILabel *lblConditionTitle;
    UILabel *lblCondition;
    
    UIButton *btnState;
    UIButton *btnShare;
    
    UILabel *lblDetail;
    UILabel *lblInfoDes;
    UILabel *lblInfoDate;
    UILabel *lblInfoRemaining;
 
    UILabel *lblStatus;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    brandImageView = [UIImageView new];
    brandImageView.contentMode = UIViewContentModeScaleAspectFill;
    brandImageView.clipsToBounds = YES;
    brandImageView.userInteractionEnabled = YES;
    [brandImageView bk_whenTapped:^{
        if (self.turnToBrand) {
            self.turnToBrand();
        }
    }];
    //    brandImageView sd_setImageWithURL:<#(NSURL *)#> placeholderImage:<#(UIImage *)#>
    
    lblBrandTitle = [UILabel new];
    lblBrandTitle.font = [UIFont boldSystemFontOfSize:18];
    lblBrandTitle.textColor = [UIColor color333333];
    lblBrandTitle.userInteractionEnabled = YES;
    [lblBrandTitle bk_whenTapped:^{
        if (self.turnToBrand) {
            self.turnToBrand();
        }
    }];
    //    lblBrandTitle
    
    lblBrandType = [UILabel new];
    lblBrandType.font = [UIFont systemFontOfSize:15];
    lblBrandType.textColor = [UIColor color999999];
    lblBrandType.text = @"Promo";
    
    [self sd_addSubviews:@[brandImageView, lblBrandTitle, lblBrandType]];
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(brandTapped)];
//    [brandImageView addGestureRecognizer:tap];
//    [lblBrandTitle addGestureRecognizer:tap];
    
    
    brandImageView.sd_layout
    .leftSpaceToView(self, 15)
    .topSpaceToView(self, 15)
    .widthIs(55)
    .heightEqualToWidth();
    brandImageView.layer.masksToBounds = YES;
    brandImageView.layer.cornerRadius = 55/2;
    
    lblBrandTitle.sd_layout
    .topSpaceToView(self, 24)
    .leftSpaceToView(brandImageView, 12)
    .rightSpaceToView(self, 15)
    .heightIs(20);
    
    lblBrandType.sd_layout
    .topSpaceToView(lblBrandTitle, 3)
    .leftEqualToView(lblBrandTitle)
    .rightEqualToView(lblBrandTitle)
    .heightIs(17);
    
    informationView = [UIView new];
    [self addSubview:informationView];
    informationView.layer.masksToBounds = YES;
    informationView.layer.cornerRadius = 3;
    
    informationView.sd_layout
    .topSpaceToView(brandImageView, 15)
    .leftSpaceToView(self, 15)
    .rightSpaceToView(self, 15)
    .heightIs(187);
    
    lblTitle = [UILabel new];
    lblTitle.font = [UIFont boldSystemFontOfSize:20];
    
    lblRemaining = [UILabel new];
    lblRemaining.font = [UIFont systemFontOfSize:14];
    
    lblDate = [UILabel new];
    lblDate.font = [UIFont systemFontOfSize:12];
    
    lblCount = [UILabel new];
    lblCount.font = [UIFont boldSystemFontOfSize:14];
    
    btnState = [UIButton new];
    [btnState setTitle:@"Save" forState:UIControlStateNormal];
    btnState.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [btnState addTarget:self action:@selector(stateButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [informationView sd_addSubviews:@[lblTitle, lblRemaining, lblDate, lblCount, btnState]];
    
    
    lblTitle.sd_layout
    .topSpaceToView(informationView, 15)
    .leftSpaceToView(informationView, 15)
    .rightSpaceToView(informationView, 15)
    .autoHeightRatio(0);
    
    [lblTitle setMaxNumberOfLinesToShow:3];
    
    lblRemaining.sd_layout
    .topSpaceToView(lblTitle, 15)
    .leftEqualToView(lblTitle)
    .rightEqualToView(lblTitle)
    .heightIs(16);
    
    lblDate.sd_layout
    .topSpaceToView(lblRemaining, 7)
    .leftEqualToView(lblRemaining)
    .rightEqualToView(lblRemaining)
    .heightIs(14);
    
    lblCount.sd_layout
    .bottomSpaceToView(informationView, 22)
    .leftSpaceToView(informationView, 15)
    .heightIs(16)
    .rightSpaceToView(informationView, 15);
    
    btnState.sd_layout
    .bottomSpaceToView(informationView, 15)
    .rightSpaceToView(informationView, 15)
    .widthIs(104)
    .heightIs(32);
    
    btnState.layer.masksToBounds = YES;
    btnState.layer.cornerRadius = 4;
    
    lblStatus = [UILabel new];
    lblStatus.textColor = [UIColor colorWithHexString:@"D20000"];
    lblStatus.font = [UIFont boldSystemFontOfSize:14];
    lblStatus.textAlignment = NSTextAlignmentCenter;
    [self addSubview:lblStatus];
    
    lblStatus.sd_layout
    .topSpaceToView(informationView, 0)
    .leftSpaceToView(self, 15)
    .rightSpaceToView(self, 15)
    .heightIs(0);
    
    couponImageView = [UIImageView new];
    couponImageView.contentMode = UIViewContentModeScaleAspectFill;
    couponImageView.clipsToBounds = YES;
    couponImageView.sd_cornerRadius = @(3);
    
    UILabel *lblDetailTitle = [UILabel new];
    lblDetailTitle.font = [UIFont boldSystemFontOfSize:18];
    lblDetailTitle.textColor = [UIColor color333333];
    lblDetailTitle.text = @"Promo Details";
    
    lblDetail = [UILabel new];
    lblDetail.font = [UIFont systemFontOfSize:14];
    lblDetail.textColor = [UIColor color333333];
    
    UIView *detailView = [UIView new];
    detailView.backgroundColor = [UIColor colorWithHexString:@"DCE0E0"];
    
    [self sd_addSubviews:@[couponImageView, lblDetailTitle, lblDetail, detailView]];
    
    couponImageView.sd_layout
    .topSpaceToView(lblStatus, 15)
    .leftEqualToView(lblStatus)
    .rightEqualToView(lblStatus)
    .heightIs((DeviceWidth-30) * .6);
    
    lblDetailTitle.sd_layout
    .topSpaceToView(couponImageView, 15)
    .leftSpaceToView(self, 15)
    .rightSpaceToView(self, 15)
    .heightIs(20);
    
    lblDetail.sd_layout
    .topSpaceToView(lblDetailTitle, 7)
    .leftEqualToView(lblDetailTitle)
    .rightEqualToView(lblDetailTitle)
    .autoHeightRatio(0);
    
    detailView.sd_layout
    .topSpaceToView(lblDetail, 15)
    .leftEqualToView(lblDetail)
    .rightEqualToView(lblDetail)
    .heightIs(1);
    
    UIView *infoView = [self createInfoView];
    [self addSubview:infoView];
    
    infoView.sd_layout
    .topSpaceToView(detailView, 0)
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .autoHeightRatio(0);
    
    lblConditionTitle = [UILabel new];
    lblConditionTitle.font = [UIFont boldSystemFontOfSize:18];
    lblConditionTitle.textColor = [UIColor color333333];
    
    lblCondition = [UILabel new];
    lblCondition.font = [UIFont systemFontOfSize:14];
    lblCondition.textColor = [UIColor color333333];
    lblCondition.numberOfLines = 0;
    [self sd_addSubviews:@[lblConditionTitle, lblCondition]];
    
    lblConditionTitle.sd_layout
    .topSpaceToView(infoView, 21)
    .leftSpaceToView(self, 15)
    .rightSpaceToView(self, 15)
    .heightIs(22);
    
    lblCondition.sd_layout
    .topSpaceToView(lblConditionTitle, 15)
    .leftSpaceToView(self, 6)
    .rightSpaceToView(self, 15)
    .heightIs(0);
    
    btnShare = [UIButton new];
    btnShare.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    btnShare.backgroundColor = [UIColor colorWithHexString:@"09BFD3"];
    [btnShare setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btnShare setTitle:@"Share Coupon as gift" forState:UIControlStateNormal];
    [btnShare addTarget:self action:@selector(couponShare) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnShare];
    
    btnShare.sd_layout
    .topSpaceToView(lblCondition, 21)
    .leftSpaceToView(self, 15)
    .rightSpaceToView(self, 15)
    .heightIs(42);
    btnShare.sd_cornerRadius = @(4);
}

- (UIView *)createInfoView{
    UIView *view = [UIView new];
    
    UILabel *lblInfo = [UILabel new];
    lblInfo.font = [UIFont boldSystemFontOfSize:18];
    lblInfo.textColor = [UIColor color333333];
    lblInfo.text = @"Promo Info";
    
    UILabel *lblCustomer = [UILabel new];
    lblCustomer.font = [UIFont systemFontOfSize:14];
    lblCustomer.textColor = [UIColor color333333];
    lblCustomer.text = @"•     Only valid for one time per customer";

    lblInfoDate = [UILabel new];
    lblInfoDate.font = [UIFont systemFontOfSize:14];
    lblInfoDate.textColor = [UIColor color333333];

    lblInfoRemaining = [UILabel new];
    lblInfoRemaining.font = [UIFont systemFontOfSize:14];
    lblInfoRemaining.textColor = [UIColor color333333];

    UILabel *lblInstant = [UILabel new];
    lblInstant.font = [UIFont systemFontOfSize:14];
    lblInstant.textColor = [UIColor color333333];
    lblInstant.text = @"•     Instant redemption";
    
//    lblInfoDes = [UILabel new];
//    lblInfoDes.font = [UIFont systemFontOfSize:14];
//    lblInfoDes.textColor = [UIColor color333333];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor colorWithHexString:@"DCE0E0"];
    
    [view sd_addSubviews:@[lblInfo,lblCustomer,lblInfoDate,lblInfoRemaining,lblInstant, lineView]];
    
    lblInfo.sd_layout
    .topSpaceToView(view, 15)
    .leftSpaceToView(view, 15)
    .rightSpaceToView(view, 15)
    .heightIs(22);
    
//    lblInfoDes.sd_layout
//    .topSpaceToView(lblInfo, 15)
//    .leftSpaceToView(view, 5)
//    .rightSpaceToView(view, 15)
//    .autoHeightRatio(0);
    
    lblCustomer.sd_layout
    .topSpaceToView(lblInfo, 15)
    .leftSpaceToView(view, 15)
    .rightSpaceToView(view, 15)
    .heightIs(16);

    lblInfoDate.sd_layout
    .topSpaceToView(lblCustomer, 5)
    .leftEqualToView(lblCustomer)
    .rightEqualToView(lblCustomer)
    .heightIs(16);

    lblInfoRemaining.sd_layout
    .topSpaceToView(lblInfoDate, 5)
    .leftEqualToView(lblInfoDate)
    .rightEqualToView(lblInfoDate)
    .heightIs(16);

    lblInstant.sd_layout
    .topSpaceToView(lblInfoRemaining, 5)
    .leftEqualToView(lblInfoRemaining)
    .rightEqualToView(lblInfoRemaining)
    .heightIs(16);
    
    lineView.sd_layout
    .topSpaceToView(lblInstant, 21)
    .leftSpaceToView(view, 15)
    .rightSpaceToView(view, 15)
    .heightIs(1);
    
    [view setupAutoHeightWithBottomView:lineView bottomMargin:0];
    
    return view;
}

- (void)stateButtonDisableStyle{
    btnState.enabled = NO;
    [btnState setTitleColor:[UIColor colorWithHexString:self.model.button_font_color_disabled] forState:UIControlStateNormal];
    btnState.backgroundColor = [UIColor colorWithHexString:self.model.button_background_color_disabled];
    btnState.layer.borderColor = [UIColor colorWithHexString:self.model.button_outline_color_disabled].CGColor;
}

- (void)setModel:(CouponModel *)model{
    _model = model;
    
    informationView.backgroundColor = [UIColor colorWithHexString:model.main_background_color];
    lblTitle.textColor = [UIColor colorWithHexString:model.main_font_color];
    
    lblRemaining.textColor = [UIColor colorWithHexString:model.main_font_color];
    
    lblDate.textColor = [UIColor colorWithHexString:model.sub_font_color];
    
    lblCount.textColor = [UIColor colorWithHexString:model.main_font_color];
    
    lblDate.text = [NSString stringWithFormat:@"%@ ~ %@",[NSString dateFormartForBrand:model.start_date],[NSString dateFormartForBrand:model.end_date]];
    lblDetail.text = model.couponDescription;
    btnState.backgroundColor = [UIColor colorWithHexString:model.button_background_color];
    [btnState setTitleColor:[UIColor colorWithHexString:model.button_font_color] forState:UIControlStateNormal];
    btnState.layer.borderColor = [UIColor colorWithHexString:model.button_outline_color].CGColor;
    btnState.layer.borderWidth = 1;
    
    if ([model.saved isEqual:@1]) {
        [btnState setTitle:@"Saved" forState:UIControlStateNormal];
        [self stateButtonDisableStyle];
    }else{
        [btnState setTitle:@"Save" forState:UIControlStateNormal];
    }
    
    if ([model.redeemed isEqual:@1]) {
        [self stateButtonDisableStyle];
    }
    
    if (![Tools isBlankString:model.status_text]) {
        lblStatus.text = model.status_text;
        
        if ([model.status_text rangeOfString:@"expired"].location != NSNotFound) {
            [self stateButtonDisableStyle];
        }
        lblStatus.sd_layout
        .topSpaceToView(informationView, 20)
        .heightIs(20);
    }
    
    if ([model.total_inventory intValue]<=0 && [model.saved isEqual:@0]) {
        [self stateButtonDisableStyle];
    }
    
    
    if ([model.partner_logo_image_url hasPrefix:@"http"]) {
        [brandImageView sd_setImageWithURL:[NSURL URLWithString:model.partner_logo_image_url] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    }else{
        [brandImageView sd_setImageWithURL:[NSURL URLWithString:[model.partner_logo_image_url returnFullImageUrl]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    }
    
    lblBrandTitle.text = model.partner_name;
    
    lblInfoDate.text = [NSString stringWithFormat:@"•     Available from %@ - %@",[NSString dateFormartNOHour:model.start_date],[NSString dateFormartNOHour:model.end_date]];
    lblInfoRemaining.text = [NSString stringWithFormat:@"•     Remaining time: %@",model.remaining_time];
    
    lblTitle.text = model.name;
    lblRemaining.text = [NSString stringWithFormat:@"Remaining time: %@",model.remaining_time];
    
    lblCount.text = [NSString stringWithFormat:@"%d Coupons Left",[model.total_inventory intValue]];
    
    if ([model.thumbnail_image_url hasPrefix:@"http"]) {
        [couponImageView sd_setImageWithURL:[NSURL URLWithString:model.thumbnail_image_url] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        }];
    }else{
        [couponImageView sd_setImageWithURL:[NSURL URLWithString:[model.thumbnail_image_url returnFullImageUrl]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        }];
    }
    
    lblConditionTitle.text = [model.terms_and_condition objectForKey: @"title"];
    NSMutableArray *termsArr = [NSMutableArray array];

    for (NSString *str in [model.terms_and_condition objectForKey:@"items"]) {
        NSString *termStr = [NSString stringWithFormat:@"<li style=\"margin:5px\">%@</li>",str];
        [termsArr addObject:termStr];
    }
    
    NSString *htmlStr = [NSString stringWithFormat:@"<ul>%@</ul>",[termsArr componentsJoinedByString:@""]];

    NSMutableAttributedString *conditionStr=  [[NSMutableAttributedString alloc] initWithData:[htmlStr dataUsingEncoding:NSUnicodeStringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    NSRange range = NSMakeRange(0, conditionStr.length);
    [conditionStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:range];
    [conditionStr addAttribute:NSForegroundColorAttributeName value:[UIColor color333333] range:range];
    CGRect rect = [conditionStr boundingRectWithSize:CGSizeMake(DeviceWidth-21, 0) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];

    lblCondition.attributedText = conditionStr;
    
    lblCondition.sd_layout
    .heightIs(rect.size.height);
    
    [self setupAutoHeightWithBottomView:btnShare bottomMargin:20];
}

- (void)couponShare{
    if (self.shareCoupon) {
        self.shareCoupon();
    }
}

- (void)stateButtonTapped{
    if (self.buttonTapped) {
        self.buttonTapped(btnState);
    }
}


@end
