//
//  EventView.h
//  Qraved
//
//  Created by harry on 2018/1/16.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CampaignModel.h"
@interface EventView : UIView

@property (nonatomic, strong) NSArray *campaignArray;
@property (nonatomic, copy) void (^eventTapped)(CampaignModel *model, NSInteger index);

@end
