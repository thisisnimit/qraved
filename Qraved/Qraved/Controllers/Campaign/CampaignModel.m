//
//  CampaignModel.m
//  Qraved
//
//  Created by harry on 2018/1/16.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "CampaignModel.h"

@implementation CampaignModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"description"]) {
        self.campaignDescription = value;
    }else if ([key isEqualToString:@"id"]){
        self.campaignId = value;
    }
}

@end
