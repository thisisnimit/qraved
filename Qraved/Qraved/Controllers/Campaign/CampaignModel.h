//
//  CampaignModel.h
//  Qraved
//
//  Created by harry on 2018/1/16.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CampaignModel : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *start_date;
@property (nonatomic, copy) NSString *main_photo;
@property (nonatomic, copy) NSString *end_date;
@property (nonatomic, copy) NSNumber *article_id;
@property (nonatomic, copy) NSNumber *sort_order;
@property (nonatomic, copy) NSNumber *status;
@property (nonatomic, copy) NSString *campaignDescription;
@property (nonatomic, copy) NSString *campaign_url;
@property (nonatomic, copy) NSString *outlink_url;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSNumber *campaignId;

@end
