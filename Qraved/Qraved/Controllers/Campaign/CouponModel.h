//
//  CouponModel.h
//  Qraved
//
//  Created by harry on 2018/1/10.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CouponModel : NSObject

@property (nonatomic, copy) NSString *default_image_url;
@property (nonatomic, copy) NSString *couponDescription;
@property (nonatomic, copy) NSString *end_date;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *start_date;
@property (nonatomic, copy) NSString *strip_banner_image_url;
@property (nonatomic, copy) NSString *style;
@property (nonatomic, copy) NSString *thumbnail_image_url;
@property (nonatomic, copy) NSNumber *coupon_id;
@property (nonatomic, copy) NSNumber *sort_order;
@property (nonatomic, copy) NSNumber *status;
@property (nonatomic, copy) NSNumber *type;
@property (nonatomic, copy) NSNumber *shop_id;
@property (nonatomic, copy) NSString *remaining_time;
@property (nonatomic, copy) NSString *server_time;
@property (nonatomic, copy) NSDictionary *terms_and_condition;
@property (nonatomic, copy) NSNumber *total_inventory;
@property (nonatomic, copy) NSNumber *save_count;
@property (nonatomic, copy) NSString *main_background_color;
@property (nonatomic, copy) NSString *sub_background_color;
@property (nonatomic, copy) NSString *main_font_color;
@property (nonatomic, copy) NSString *sub_font_color;
@property (nonatomic, copy) NSString *button_background_color;
@property (nonatomic, copy) NSString *button_font_color;
@property (nonatomic, copy) NSString *button_font_color_disabled;
@property (nonatomic, copy) NSString *button_background_color_disabled;
@property (nonatomic, copy) NSString *button_outline_color_disabled;
@property (nonatomic, copy) NSString *button_outline_color;
@property (nonatomic, copy) NSString *partner_name;
@property (nonatomic, copy) NSString *partner_logo_image_url;
@property (nonatomic, copy) NSString *partner_description;
@property (nonatomic, copy) NSNumber *partner_id;
@property (nonatomic, copy) NSNumber *partner_object_id;
@property (nonatomic, copy) NSString *partner_type;
@property (nonatomic, copy) NSNumber *saved;
@property (nonatomic, copy) NSNumber *redeemed;
@property (nonatomic, copy) NSString *status_text;
@property (nonatomic, copy) NSNumber *endDate;
@property (nonatomic, copy) NSNumber *startDate;

@end
