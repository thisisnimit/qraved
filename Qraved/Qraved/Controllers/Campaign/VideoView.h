//
//  VideoView.h
//  Qraved
//
//  Created by harry on 2018/1/11.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YTPlayerView.h"
#import "BrandVideoModel.h"
@interface VideoView : UIView<YTPlayerViewDelegate>

@property (nonatomic, copy) BrandVideoModel *model;
@property (nonatomic, copy) void(^videoPlay)();
@end
