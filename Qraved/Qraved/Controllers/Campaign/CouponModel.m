//
//  CouponModel.m
//  Qraved
//
//  Created by harry on 2018/1/10.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "CouponModel.h"

@implementation CouponModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"description"]) {
        self.couponDescription = value;
    }else if ([key isEqualToString:@"id"]){
        self.coupon_id = value;
    }else if ([key isEqualToString:@"defaultImageUrl"]){
        self.default_image_url = value;
    }else if ([key isEqualToString:@"stripBannerImageUrl"]){
        self.strip_banner_image_url = value;
    }else if ([key isEqualToString:@"thumbnailImageUrl"]){
        self.thumbnail_image_url = value;
    }else if ([key isEqualToString:@"partnerName"]){
        self.partner_name = value;
    }else if ([key isEqualToString:@"remainingTime"]){
        self.remaining_time = value;
    }else if ([key isEqualToString:@"statusText"]){
        self.status_text = value;
    }
}


@end
