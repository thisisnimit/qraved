//
//  NotificationsViewController.m
//  Qraved
//
//  Created by Laura on 14-8-25.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "NotificationsViewController.h"

#import "UIViewController+Helper.h"
#import "AppDelegate.h"
#import "UIColor+Helper.h"
#import "Label.h"
#import "UIDevice+Util.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"
#import "NotificationCell.h"
#import "IMGUser.h"
#import "IMGNotification.h"
#import "EGORefreshTableHeaderView.h"
#import "EGORefreshTableFooterView.h"
#import "LoadingImageView.h"
#import "DBManager.h"
#import "NotificationHandler.h"
#import "DetailViewController.h"
#import "SplashViewController.h"
#import "JournalHandler.h"
#import "JournalListViewController.h"
#import "WebViewController.h"
#import "JournalDetailViewController.h"
#import "IMGDiscover.h"
#import "RestaurantsViewController.h"
#import "ReviewViewController.h"
#import "SpecialOffersViewController.h"
#import "SectionViewController.h"
#import "TrackHandler.h"
#import "IMGDiningGuide.h"
#import "SearchPhotosViewController.h"
#import "MapViewController.h"
#import "AppDelegate.h"
#import "DiningGuideListViewController.h"
#import "DiningGuideRestaurantsViewController.h"
#import "HomeUtil.h"
#import "BookingConfirmViewController.h"
#import "IMGReservation.h"
#import "PullTableView.h"
#import "UILabel+Helper.h"
#import "UploadPhotoCardViewController.h"
#import "ReviewCardViewController.h"
#import "ReviewHandler.h"
#import "LoadingView.h"
#import "ImageListViewController.h"
#import "UploadPhotoHandler.h"
#import "ReviewPublishViewController.h"
#import "NotificationHandler.h"
#import "V2_DiscoverListResultViewController.h"
#import "V2_JournalListViewController.h"
#import "V2_DiningGuideListViewController.h"
#import "CouponDetailViewController.h"
#import "BrandViewController.h"
#import "CampaignWebViewController.h"
#import "DeliveryViewController.h"
#import "SettingNotificationView.h"
#import "DetailSeeAllPhotoViewController.h"

#define REFRESH_HEADER_HEIGHT 65.0f
#define NOTIFICATION_CLEAR @"notification/status/clear"
#define navigationHeight  (44+[UIDevice heightDifference])

@interface NotificationsViewController ()<UITableViewDataSource,UITableViewDelegate,EGORefreshTableHeaderDelegate,EGORefreshTableFooterDelegate,SplashViewControllerDelegate,PullTableViewDelegate,ZYQAssetPickerControllerDelegate>
{
    LoadingImageView *loadingImageView;
    EGORefreshTableHeaderView *_refreshHeaderView;
    EGORefreshTableFooterView *_loadMoreFooterView;
    NSMutableArray *notificationsArray;
    UITableView *tableView;
    Label *numLabel;
    NSNumber *max;
    NSNumber *offset;
    BOOL isRefresh;
    BOOL isRefreshNotificationsCount;
    
    BOOL isGAIOnce;
    IMGUser *user;
    NSTimeInterval beginLoadingData;
    NSTimeInterval finishUI;
    IMGRestaurant*  restaurant;
    NSArray* dishArr;
    IMGUser* Notificationuser;
    ImageListViewController* imageListVC;
    int index;
    RDVTabBarItem *notificationItem;
    SettingNotificationView *setView;
    
    IMGShimmerView *shimmerView;
}
@end

@implementation NotificationsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"Notifications";
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self getNotificationCount];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"Notifications list page";
    isGAIOnce = YES;
    beginLoadingData = [[NSDate date]timeIntervalSince1970];
    if (!self.isFromTabMenu) {
        [self addBackBtn];
    }
    max = [NSNumber numberWithInt:10];
    offset = [NSNumber numberWithInt:0];
    notificationsArray = [[NSMutableArray alloc]init];
    [self loadTableView];
    [self initData];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(clickTabMenuRefreshPage) name:NOTIFICATION_REFRESH_TAB_NOTIFICATION object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSettingUI) name:@"RefreshNotificationSettingView" object:nil];
}

//-(void)clickTabMenuRefreshPage{
//    if (isFreshing) {
//        return;
//    }
//    offset = [NSNumber numberWithInt:0];
//    notificationsArray = [[NSMutableArray alloc]init];
//    [_tableView reloadData];
//    [self initData];
//}

-(void)dealloc{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_REFRESH_TAB_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RefreshNotificationSettingView" object:nil];;
}
- (void)initData
{
    user = [IMGUser currentUser];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:[IMGUser currentUser].userId forKey:@"userId"];
    [param setValue:[IMGUser currentUser].token forKey:@"t"];
    [param setValue:offset forKey:@"offset"];
    [param setValue:max forKey:@"max"];

    [NotificationHandler getNotificationListWithParams:param andBlock:^(NSArray *dataArray) {

        if ([offset isEqual:@0]) {
            [notificationsArray removeAllObjects];
            [shimmerView removeFromSuperview];
        }
        [notificationsArray addObjectsFromArray:dataArray];
        [tableView reloadData];
        [tableView.mj_footer endRefreshing];
        finishUI = [[NSDate date]timeIntervalSince1970];
        if (isGAIOnce) {
            float duringTime = finishUI - beginLoadingData;
            duringTime = duringTime * 1000;
            int duringtime = duringTime;
            [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createTimingWithCategory:@"notification"  interval:[NSNumber numberWithInt:duringtime] name:@"" label:@""] build]];
            isGAIOnce = NO;
        }
    } failure:^(NSString *exceptionMsg){
        [tableView.mj_footer endRefreshing];
        if([exceptionMsg isLogout]){
            [self.navigationController popViewControllerAnimated:NO];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
    }];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
//    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
}

- (BOOL)checkNotificationState{
    UIUserNotificationSettings *setting = [[UIApplication sharedApplication] currentUserNotificationSettings];
    if (setting.types == UIUserNotificationTypeNone) {
        return NO;
    }else{
        return YES;
    }
}

- (void)refreshSettingUI{
    [UIView animateWithDuration:0.3 animations:^{
        setView.sd_layout
        .heightIs(0);
        [tableView updateLayout];
    }];
}

- (void)loadTableView
{
    self.view.backgroundColor = [UIColor whiteColor];
    setView = [SettingNotificationView new];
    __weak typeof(self) weakSelf = self;
    setView.closeView = ^{
        [weakSelf refreshSettingUI];
    };
    
    setView.settingTapped = ^{
        [AppDelegate ShareApp].isSettingNotification = YES;;
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
        }
    };
    [self.view addSubview:setView];
    
    tableView = [[UITableView alloc] init];
    tableView.delegate = self;
    tableView.dataSource = self;
    
    if (@available(iOS 11.0, *)) {
        tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    [self.view addSubview:tableView];
    
    tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    
    setView.sd_layout
    .topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .widthIs(DeviceWidth)
    .heightIs([self checkNotificationState] ? 0 : 130);
    
    tableView.sd_layout
    .topSpaceToView(setView, 0)
    .bottomSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .widthIs(DeviceWidth);
    
    shimmerView = [[IMGShimmerView alloc] initWithFrame:self.view.bounds];
    [shimmerView createNotificationShimmerView];
    [self.view addSubview:shimmerView];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cellIdentifier";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    IMGNotifications *notifications = [notificationsArray objectAtIndex:indexPath.row];
    if (nil == cell) {
        cell = [[NotificationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    [cell setNotifications:notifications];
    //    cell.smallIconImageView.image = [UIImage imageNamed:@"OfferTypeTag0"];
    
    return cell;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return notificationsArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IMGNotifications *notifications = [notificationsArray objectAtIndex:indexPath.row];
    int type = [notifications.type intValue];
    UILabel *labelTemp = [[UILabel alloc] init];
    labelTemp.font = [UIFont systemFontOfSize:14];
    labelTemp.frame = CGRectMake(0, 0, DeviceWidth - (40 + LEFTLEFTSET) - 30, 30);

    switch (type)
    {
        case 0:
        {
            labelTemp.text = [notifications.message removeHTML];
        }
            break;
            
        case 1:
        {
            labelTemp.text = [notifications.message removeHTML];
        }
            break;
            
        case 2:
        {
            labelTemp.text = notifications.contentTitle;
        }
            break;
            
        case 3:
        {
            if ([notifications.subtype intValue] == 31)
            {
                NSString *messageString = [[NSString alloc] initWithFormat:@"Hi %@, kamu ditunggu di %@ pada pukul %@! See ya!",[notifications.userName removeHTML],notifications.restaurantName,notifications.bookDate];
                labelTemp.text = messageString;
            }
            else if ([notifications.subtype intValue] == 32)
            {
                NSString *messageString = [[NSString alloc] initWithFormat:@"Hi %@, reservasi kamu di %@ telah dikonfirmasi oleh restoran! Bon Appetit!",[notifications.userName removeHTML],notifications.restaurantName];
                labelTemp.text = messageString;
            }

        }
            break;
            
        case 4:
        {
            NSString *messageString = [[NSString alloc] initWithFormat:@"Pst! Check %@ out! They just updated their page!",notifications.restaurantName];
            labelTemp.text = messageString;
        }
            break;
        case 5:
        {
            if ([notifications.subtype intValue] == 51)
            {
                if ([user.userId intValue] == [notifications.reviewUserId intValue])
                {
                    NSString *messageString;
                    if (!notifications.isVendor && [notifications.userType intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ liked your review",[notifications.restaurantName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ liked your review",[notifications.guestUserName removeHTML]];
                    }
                    labelTemp.text = messageString;
                }
                else
                {
                    NSString *messageString;
                    if (!notifications.isVendor && [notifications.userType intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also liked %@'s review",[notifications.restaurantName removeHTML],[notifications.reviewUserName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also liked %@'s review",[notifications.guestUserName removeHTML],[notifications.reviewUserName removeHTML]];
                    }
                    labelTemp.text = messageString;
                }
            }
            else if ([notifications.subtype intValue] == 52)
            {
                if ([user.userId intValue] == [notifications.reviewUserId intValue])
                {
                    NSString *messageString;
                    if (!notifications.isVendor && [notifications.userType intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ commented on your review",[notifications.restaurantName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ commented on your review",[notifications.guestUserName removeHTML]];
                    }
                    labelTemp.text = messageString;
                }
                else
                {
                    NSString *messageString;
                    if (!notifications.isVendor && [notifications.userType intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also commented on %@'s review",[notifications.restaurantName removeHTML],[notifications.reviewUserName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also commented on %@'s review",[notifications.guestUserName removeHTML],[notifications.reviewUserName removeHTML]];
                    }
                    labelTemp.text = messageString;
                }
            }
        }
            break;
        case 6:
        {
            if ([notifications.subtype intValue] == 51)
            {
                if ([user.userId intValue] == [notifications.dishUserId intValue])
                {
                    NSString *messageString;
                    if (!notifications.isVendor && [notifications.userType intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ liked your photo",[notifications.restaurantName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ liked your photo",[notifications.guestUserName removeHTML]];
                    }
                    labelTemp.text = messageString;
                }
                else
                {
                    NSString *messageString;
                    if (!notifications.isVendor && [notifications.userType intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also liked %@'s photo",[notifications.restaurantName removeHTML],[notifications.dishUserName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also liked %@'s photo",[notifications.guestUserName removeHTML],[notifications.dishUserName removeHTML]];
                    }
                    labelTemp.text = messageString;
                }
            }
            else if ([notifications.subtype intValue] == 52)
            {
                if ([user.userId intValue] == [notifications.dishUserId intValue])
                {
                    NSString *messageString;
                    if (!notifications.isVendor && [notifications.userType intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ commented on your photo",[notifications.restaurantName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ commented on your photo",[notifications.guestUserName removeHTML]];
                    }
                    labelTemp.text = messageString;
                }
                else
                {
                    NSString *messageString;
                    if (!notifications.isVendor && [notifications.userType intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also commented on %@'s photo",[notifications.restaurantName removeHTML],[notifications.dishUserName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also commented on %@'s photo",[notifications.guestUserName removeHTML],[notifications.dishUserName removeHTML]];
                    }
                    labelTemp.text = messageString;
                }
            }
        }
            break;
            
            
            
        case 7:
        {
            if ([notifications.subtype intValue] == 71)
            {
                if ([user.userId intValue] == [notifications.hostUserId intValue])
                {
                    NSString *messageString;
                    if (!notifications.hostIsVendor && [notifications.guestIsVendor intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ liked your photos",[notifications.restaurantName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ liked your photos",[notifications.guestUserName removeHTML]];
                    }
                    labelTemp.text = messageString;
                }
                else
                {
                    NSString *messageString;
                    if (!notifications.hostIsVendor && [notifications.guestIsVendor intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also liked %@'s photos",[notifications.restaurantName removeHTML],[notifications.dishUserName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also liked %@'s photos",[notifications.guestUserName removeHTML],[notifications.dishUserName removeHTML]];
                    }
                    labelTemp.text = messageString;
                }
            }
            else if ([notifications.subtype intValue] == 72)
            {
                if ([user.userId intValue] == [notifications.hostUserId intValue])
                {
                    NSString *messageString;
                    if (!notifications.hostIsVendor && [notifications.guestIsVendor intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ commented your photos",[notifications.restaurantName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ commented your photos",[notifications.guestUserName removeHTML]];
                    }
                    labelTemp.text = messageString;
                }
                else
                {
                    NSString *messageString;
                    if (!notifications.hostIsVendor && [notifications.guestIsVendor intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also commented on%@'s photos",[notifications.restaurantName removeHTML],[notifications.dishUserName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also commented on%@'s photos",[notifications.guestUserName removeHTML],[notifications.dishUserName removeHTML]];
                    }
                    labelTemp.text = messageString;
                }
            }
        }
            break;
            
        case 12:{
            labelTemp.text = notifications.message;
            break;
        }
        default:
            break;
    }
    float contentHeight = 0;
    if (notifications.content.length)
    {
        UILabel *contentLabel = [[UILabel alloc] init];
        contentLabel.font = [UIFont systemFontOfSize:14];
        contentLabel.frame = CGRectMake(0, 0, DeviceWidth - (40 + LEFTLEFTSET) - 30, 30);

        
        [contentLabel setNumberOfLines:2];
        [contentLabel setLineBreakMode:NSLineBreakByWordWrapping];
        CGSize maximumLabelSize = CGSizeMake(contentLabel.frame.size.width,9999);
        
        CGSize expectedLabelSize = [[contentLabel text] sizeWithFont:[contentLabel font]
                                           constrainedToSize:maximumLabelSize
                                               lineBreakMode:[contentLabel lineBreakMode]];
        contentHeight = expectedLabelSize.height + 30;
    }
    else if ([notifications.subtype intValue] == 52)
    {
        NSString *commentStr = [notifications.commentContext removeHTML];
        if(commentStr.length>140){
            commentStr=[NSString stringWithFormat:@"\"%@...\"",[commentStr substringToIndex:140]];
        }
        else
            commentStr = [NSString stringWithFormat:@"\"%@\"",commentStr];
        contentHeight = [self calculatedCommentTextHeight:commentStr];
    }
    else if ([notifications.subtype intValue] == 72)
    {
        NSString *commentStr = [notifications.comment removeHTML];
        if(commentStr.length>140){
            commentStr=[NSString stringWithFormat:@"\"%@...\"",[commentStr substringToIndex:140]];
        }
        else
            commentStr = [NSString stringWithFormat:@"\"%@\"",commentStr];
        contentHeight = [self calculatedCommentTextHeight:commentStr];
    }

    else if ([notifications.type intValue] == 6 && [notifications.subtype intValue] == 51)
    {
        contentHeight = 45;
    }
    else if ([notifications.type intValue] == 7 && [notifications.subtype intValue] == 71)
    {
        contentHeight = 45;
    }

    
    return labelTemp.expectedHeight + 60 + contentHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotificationCell *cell = (NotificationCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    [[Amplitude instance] logEvent:@"CL - Notifications on In-app Notification page" withEventProperties:@{@"Landing Page":@"Notification"}];

    
    if([self.nvcDelegate respondsToSelector:@selector(notificationViewControllerBtnClick)])
    {
        [self.nvcDelegate notificationViewControllerBtnClick];
    }
    IMGNotifications *notifications = [notificationsArray objectAtIndex:indexPath.row];
    int type = [notifications.type intValue];
    switch (type)
    {
        case 0:
        {
            cell.userInteractionEnabled=NO;

            NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",notifications.notificationId,@"notificationId", nil];
            [NotificationHandler readNotificationWithParams:params];
            [self jumpToAppWithUrl:notifications.landingPageUrl withCell:cell];
        }
            break;
            
        case 1:
        {
            cell.userInteractionEnabled=NO;

            NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",notifications.notificationId,@"notificationId", nil];
            [NotificationHandler readNotificationWithParams:params];
            [self jumpToAppWithUrl:notifications.landingPageUrl withCell:cell];

        }
            break;
            
        case 2:
        {
            SplashViewController *svc = [[SplashViewController alloc] init];
            svc.svcDelegate = self;
            svc.splashIdFromNotification = notifications.splashId;
            NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",notifications.notificationId,@"notificationId", nil];
            [NotificationHandler readNotificationWithParams:params];

            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:svc];
            [self.navigationController presentViewController:nav animated:NO completion:^{
                
            }];
            //修改为push控制器
//            [self.navigationController pushViewController:svc animated:YES];
        }
            break;
            
        case 3:
        {

            IMGReservation *reservation = [[IMGReservation alloc] init];
            reservation.reservationId = notifications.reservationId;
         
            BookingConfirmViewController *bookConfirmVC = [[BookingConfirmViewController alloc] initWithReservation:reservation];
            bookConfirmVC.isFromProfile = YES;
            [self.navigationController pushViewController:bookConfirmVC animated:YES];

            NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",notifications.notificationId,@"notificationId", nil];
            [NotificationHandler readNotificationWithParams:params];

        }
            break;
            
        case 4:
        {
            DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:notifications.restaurantId];
            [self.navigationController pushViewController:dvc animated:YES];
            NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",notifications.notificationId,@"notificationId", nil];
            [NotificationHandler readNotificationWithParams:params];

        }
            break;
        case 5:
        {
//            [[Amplitude instance] logEvent:@"RC - View Review Card detail" withEventProperties:@{@"Origin":@"Notification"}];
//            
            
            IMGReview *review = [[IMGReview alloc] init];
            review.reviewId = notifications.reviewId;
            ReviewCardViewController *rcvc = [[ReviewCardViewController alloc] initWithRestaurant:nil andReview:review andDishList:nil];
            rcvc.isFromNotification = YES;
            rcvc.amplitudeType = @"Notification";
            [self.navigationController pushViewController:rcvc animated:YES];

            NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",notifications.notificationId,@"notificationId", nil];
            [NotificationHandler readNotificationWithParams:params];

        }
            break;
        case 6:
        {
            UploadPhotoCardViewController *upcvc = [[UploadPhotoCardViewController alloc] initWithRestaurant:nil andDishList:notifications.dishList andUser:nil];
            upcvc.isFromNotification = YES;
            upcvc.amplitudeType = @"Notification";
            [self.navigationController pushViewController:upcvc animated:YES];

            NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",notifications.notificationId,@"notificationId", nil];
            [NotificationHandler readNotificationWithParams:params];

        }
            break;
        case 7:
        {
            ImageListViewController*  imageListVC1=[[ImageListViewController alloc]init];
            imageListVC1.isfromNotification=YES;
            imageListVC1.moderateId=notifications.moderateReviewId;
            [self.navigationController pushViewController:imageListVC1 animated:YES];

            NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",notifications.notificationId,@"notificationId", nil];
            [NotificationHandler readNotificationWithParams:params];
            
        }
            break;
            
        case 12:{
            [IMGAmplitudeUtil trackCampaignWithName:@"RC - View Coupon Detail" andCampaignId:nil andBrandId:nil andCouponId:[notifications.couponSaveInfo objectForKey:@"id"] andLocation:nil andOrigin:@"Notification" andIsMall:NO];
            CouponDetailViewController *couponDetailViewController = [[CouponDetailViewController alloc] init];
            couponDetailViewController.couponId = [notifications.couponSaveInfo objectForKey:@"id"];
            //couponDetailViewController.isFromProfile = YES;
            
            MLNavigationController *nav = [[MLNavigationController alloc] initWithRootViewController:couponDetailViewController];
            [self.navigationController presentViewController:nav animated:YES completion:nil];
            
            NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",notifications.notificationId,@"notificationId", nil];
            [NotificationHandler readNotificationWithParams:params];
            break;
        }
            

        default:
            break;
    }
    
    notifications.isRead = YES;
    cell.pointImageView.hidden = YES;
}
- (void)getNotificationCount{
    
    
    for (RDVTabBarItem *item in [[[AppDelegate ShareApp].tabMenuViewController tabBar] items]) {
        
        if (index==3) {
            notificationItem=item;
            notificationItem.badgePositionAdjustment = UIOffsetMake(-9, 4);
        }
        
        index++;
    }

    if (user.userId && user.token)
    {
//        NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t", nil];
        NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:kRemoteNotificationDeviceToken];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        if (deviceToken!=nil) {
            NSMutableDictionary *pargrammer=[[NSMutableDictionary alloc]init];
            [pargrammer setObject:deviceToken forKey:@"deviceToken"];
            [pargrammer setObject:[NSNumber numberWithInt:0] forKey:@"badge"];
            [NotificationHandler updateNotificationCountWithParams:pargrammer andBlock:^(id responseObject) {
                
            }];
        }
    }
    else
    {
        notificationItem.badgeValue=@"";
        NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:kRemoteNotificationDeviceToken];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        NSMutableDictionary *pargrammer=[[NSMutableDictionary alloc]init];
        [pargrammer setObject:deviceToken forKey:@"deviceToken"];
        [pargrammer setObject:[NSNumber numberWithInt:0] forKey:@"badge"];
        [NotificationHandler updateNotificationCountWithParams:pargrammer andBlock:^(id responseObject) {
            
        }];
    }
}

- (void)btnClickWithSplash:(IMGSplash *)splash
{
    IMGSplash *_splash = splash;
    if ([_splash.buttonType isEqualToString:@"My Qraved"])
    {
        
        IMGUser *user_ = [IMGUser currentUser];
        if (user_.userId && user_.token)
        {
            ProfileViewController *myProfileViewController = [[ProfileViewController alloc] init];
            MLNavigationController *ProfileNavigationController=[[MLNavigationController alloc] initWithRootViewController:myProfileViewController];
            myProfileViewController.amplitude = @"Email notification";
            [self.sideMenuViewController setContentViewController:ProfileNavigationController animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
    }
    else if ([_splash.buttonType isEqualToString:@"Journal Detail"])
    {
        [JournalHandler getJournalDetailWithJournalId:_splash.buttonValue andJournalBlock:^(IMGMediaComment *journal) {
            
            if(journal==nil){
                JournalListViewController *JVC = [[JournalListViewController alloc] init];
                JVC.amplitudeType = @"Notification";
                [self.navigationController pushViewController:JVC animated:YES];
            }else if ([journal.imported intValue]== 1)
            {
                [JournalHandler getJournalDetailWithJournalArticleId:journal.mediaCommentId andBlock:^(NSString *content,NSString *webSite) {
                    journal.contents = content;
                    WebViewController *wvc = [[WebViewController alloc] init];
                    wvc.content = journal.contents;
                    wvc.journal = journal;
                    wvc.title = journal.title;
                    wvc.webSite = webSite;
                    wvc.fromDetail = NO;
                    [self.navigationController pushViewController:wvc animated:YES];
                }];
                
                WebViewController *wvc = [[WebViewController alloc] init];
                wvc.journal = journal;
                wvc.fromDetail = NO;
                [self.navigationController pushViewController:wvc animated:YES];

            }
            else
            {
                JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
                jdvc.journal = journal;
                [self.navigationController pushViewController:jdvc animated:YES];
            }
            
            
        }];
        
    }
    else if ([_splash.buttonType isEqualToString:@"Restaurant Detail"])
    {
        DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:_splash.buttonValue];
        [self.navigationController pushViewController:dvc animated:YES];
    }
    else if ([_splash.buttonType isEqualToString:@"URL"])
    {
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:_splash.buttonUrl]];
    }
    
}


- (void)jumpToAppWithUrl:(NSString *)urlString withCell:(UITableViewCell*)cell
{
    if([urlString rangeOfString:@"qraved731842943"].location == NSNotFound)
    {
        [[ UIApplication sharedApplication]openURL:[NSURL URLWithString:urlString]];
        cell.userInteractionEnabled=YES;

        return;
    }
    NSRange range=[urlString rangeOfString:@"app/restaurant"];
    NSRange reviewRange=[urlString rangeOfString:@"app/review"];
    NSRange offerRanger=[urlString rangeOfString:@"app/offer"];
    NSRange photoRanger=[urlString rangeOfString:@"app/photo"];
    NSRange menuRanger=[urlString rangeOfString:@"app/menu"];
    NSRange mapRanger=[urlString rangeOfString:@"app/map"];
    NSRange homeRange = [urlString rangeOfString:@"app/home"];
    NSRange diningguideRange = [urlString rangeOfString:@"app/diningguide"];
    NSRange eventRange = [urlString rangeOfString:@"app/event"];
    
    NSString *restString=@"restaurantid=";
    
    if (range.location!=NSNotFound)
    {
        NSRange idRange = [urlString rangeOfString:@"id="];
        NSString *restaurantId = [urlString substringFromIndex:idRange.location+3];
        
        IMGRestaurant *tmpRestaurant=[IMGRestaurant findById:[NSNumber numberWithInt:[restaurantId intValue]]];
        
        if (tmpRestaurant)
        {
            DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurant:tmpRestaurant];
            //UIViewController *currentVC = [self getCurrentVC];
            //[currentVC.navigationController pushViewController:detailViewController animated:YES];
            detailViewController.isFromAppDelegate = YES;
            cell.userInteractionEnabled=YES;
            [self.navigationController pushViewController:detailViewController animated:YES];
            //            [self.window.rootViewController.navigationController pushViewController:detailViewController animated:YES];
            
        }
    }
    else if (homeRange.location != NSNotFound)
    {
        if ([urlString rangeOfString:@"type=journal"].location != NSNotFound)
        {
            NSRange idRange = [urlString rangeOfString:@"journalid="];
            if (idRange.location != NSNotFound)
            {
                NSString *journalId = [urlString substringFromIndex:idRange.location+10];
                
                [JournalHandler getJournalDetailWithJournalId:[NSNumber numberWithInt:[journalId intValue]] andJournalBlock:^(IMGMediaComment *journal) {
                    
                    if(journal==nil){
                        V2_JournalListViewController *JVC = [[V2_JournalListViewController alloc] init];
                        JVC.amplitudeType = @"Notification";
                        cell.userInteractionEnabled=YES;

                        [self.navigationController pushViewController:JVC animated:YES];
                        NSLog(@"jump to app");
                    }else if ([journal.imported intValue]== 1)
                    {
                        [JournalHandler getJournalDetailWithJournalArticleId:journal.mediaCommentId andBlock:^(NSString *content,NSString *webSite) {
                            journal.contents = content;
                            WebViewController *wvc = [[WebViewController alloc] init];
                            wvc.content = journal.contents;
                            wvc.journal = journal;
                            wvc.webSite = webSite;
                            wvc.title = journal.title;
                            wvc.fromDetail = NO;
                            cell.userInteractionEnabled=YES;
                            [self.navigationController pushViewController:wvc animated:YES];
                        }];
                    }
                    else
                    {
                        JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
                        jdvc.journal = journal;
                        cell.userInteractionEnabled=YES;

                        [self.navigationController pushViewController:jdvc animated:YES];
                    }
                    
                    
                }];
            }
            else
            {
                V2_JournalListViewController *JVC = [[V2_JournalListViewController alloc] init];
                JVC.amplitudeType = @"Notification";
                cell.userInteractionEnabled=YES;

                [self.navigationController pushViewController:JVC animated:YES];
                NSLog(@"jump to app");
            }
        }
        else if ([urlString rangeOfString:@"type=search"].location != NSNotFound)
        {
//            IMGDiscover *discover=[[IMGDiscover alloc]init];
//            discover.sortby=SORTBY_POPULARITY;
//            cell.userInteractionEnabled=YES;
//
//            discover.cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
//            RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc]initWithMap];
//            restaurantsViewController.amplitudeType = @"Notification";
//            [restaurantsViewController initDiscover:discover];
//            [self.navigationController pushViewController:restaurantsViewController animated:YES];
            V2_DiscoverListResultViewController *discoverListResultVC = [[V2_DiscoverListResultViewController alloc] init];
            discoverListResultVC.keywordString = @"";
            [self.navigationController pushViewController:discoverListResultVC animated:YES];
        }
        else
        {
            [self.navigationController popViewControllerAnimated:NO];
            [[AppDelegate ShareApp] gotoHomePage];
            
            cell.userInteractionEnabled=YES;

        }
        
    }
    else if ([urlString rangeOfString:@"app/journal"].location != NSNotFound)
    {
        
        NSRange idRange = [urlString rangeOfString:@"journalid="];
        if (idRange.location != NSNotFound)
        {
            NSString *journalId = [urlString substringFromIndex:idRange.location+10];
            IMGMediaComment *journal = [[IMGMediaComment alloc] init];
            journal.mediaCommentId = [NSNumber numberWithInt:[journalId intValue]];
            [JournalHandler getJournalDetailTypeArticleIdWithJournalId:journal.mediaCommentId andJournalBlock:^(IMGMediaComment *journal) {
                
                if(journal==nil){
                    V2_JournalListViewController *JVC = [[V2_JournalListViewController alloc] init];
                    JVC.amplitudeType = @"Notification";
                    cell.userInteractionEnabled=YES;

                    [self.navigationController pushViewController:JVC animated:YES];
                    NSLog(@"jump to app");
                }else if ([journal.imported intValue]== 1)
                {
                    [JournalHandler getJournalDetailWithJournalArticleId:journal.mediaCommentId andBlock:^(NSString *content,NSString *webSite) {
                        journal.contents = content;
                        WebViewController *wvc = [[WebViewController alloc] init];
                        wvc.content = journal.contents;
                        wvc.journal = journal;
                        wvc.webSite = webSite;
                        wvc.title = journal.title;
                        wvc.fromDetail = NO;
                        cell.userInteractionEnabled=YES;

                        [self.navigationController pushViewController:wvc animated:YES];
                    }];
                }
                else
                {
                    JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
                    jdvc.journal = journal;
                    cell.userInteractionEnabled=YES;

                    [self.navigationController pushViewController:jdvc animated:YES];
                }
                
                
            }];
        }
        else if ([urlString rangeOfString:@"trending"].location!=NSNotFound)
        {
            
            V2_JournalListViewController *JVC = [[V2_JournalListViewController alloc] init];
            //JVC.isTrending=YES;
            [self.navigationController pushViewController:JVC animated:YES];
            cell.userInteractionEnabled=YES;

            
        }else if ([urlString rangeOfString:@"?s="].location!=NSNotFound){
            
            NSRange rang=[urlString rangeOfString:@"?s="];
            NSString *str=[urlString substringFromIndex:rang.location+3];
            
            V2_JournalListViewController *jlvc = [[V2_JournalListViewController alloc] init];
            //jlvc.isPop = YES;
            jlvc.needLog = YES;
            //jlvc.searchText =str;
            [self.navigationController pushViewController:jlvc animated:YES];
            cell.userInteractionEnabled=YES;

        }else{
            
            V2_JournalListViewController *JVC = [[V2_JournalListViewController alloc] init];
            [self.navigationController pushViewController:JVC animated:YES];
            cell.userInteractionEnabled=YES;

        }
        
        
    }else if(reviewRange.location !=NSNotFound){
        
        NSRange idRange = [urlString rangeOfString:restString];
        if(idRange.location != NSNotFound){
            NSString *restaurantId = [urlString substringFromIndex:idRange.location+restString.length];
            IMGRestaurant *tmpRestaurant = [[IMGRestaurant alloc] init];
            tmpRestaurant.restaurantId = [NSNumber numberWithInt:[restaurantId intValue]];
            
            if (tmpRestaurant){
                ReviewViewController *reviewController = [[ReviewViewController alloc] initWithRestaurant:tmpRestaurant];
                [self.navigationController pushViewController:reviewController animated:YES];
                cell.userInteractionEnabled=YES;
            }
        }
        
    }else if(offerRanger.location != NSNotFound){
        NSRange idRange = [urlString rangeOfString:restString];
        if(idRange.location != NSNotFound){
            NSString *restaurantId = [urlString substringFromIndex:idRange.location+restString.length];

            IMGRestaurant *tmpRestaurant=[IMGRestaurant findById:[NSNumber numberWithInt:[restaurantId intValue]]];
            
            if (tmpRestaurant){
                NSMutableArray *viewControllers=[self.navigationController.viewControllers mutableCopy];
                DetailViewController *detailViewController = [[DetailViewController alloc] initWithRestaurant:tmpRestaurant];
                [viewControllers addObject:detailViewController];
                SpecialOffersViewController *specialOffersViewController = [[SpecialOffersViewController alloc]initWithRestaurant:tmpRestaurant andTimeArray:nil andOfferArray:nil];
                [viewControllers addObject:specialOffersViewController];
                cell.userInteractionEnabled=YES;

                [self.navigationController setViewControllers:viewControllers animated:YES];
            }
        }
    }else if(menuRanger.location != NSNotFound){
        NSRange idRange = [urlString rangeOfString:restString];
        if(idRange.location != NSNotFound){
            NSString *restaurantId = [urlString substringFromIndex:idRange.location+restString.length];

            IMGRestaurant *tmpRestaurant=[IMGRestaurant findById:[NSNumber numberWithInt:[restaurantId intValue]]];
            
            if (tmpRestaurant){
                NSMutableArray *viewControllers=[self.navigationController.viewControllers mutableCopy];
                DetailViewController *detailViewController = [[DetailViewController alloc] initWithRestaurant:tmpRestaurant];
                [viewControllers addObject:detailViewController];
                SectionViewController *menusViewController=[[SectionViewController alloc]initWithRestaurant:tmpRestaurant];
                [viewControllers addObject:menusViewController];
                cell.userInteractionEnabled=YES;

                [self.navigationController setViewControllers:viewControllers animated:YES];
            }
        }
    }else if(photoRanger.location != NSNotFound){
        NSRange idRange = [urlString rangeOfString:restString];
        if(idRange.location != NSNotFound){
            NSString *restaurantId = [urlString substringFromIndex:idRange.location+restString.length];
            IMGRestaurant *tmpRestaurant=[[IMGRestaurant alloc] init];
            tmpRestaurant.restaurantId = [NSNumber numberWithInt:[restaurantId intValue]];
            if (tmpRestaurant){
                DetailSeeAllPhotoViewController *photoListViewController = [[DetailSeeAllPhotoViewController alloc] initWithRestaurant:tmpRestaurant restaurantPhotoArray:nil];
                [self.navigationController pushViewController:photoListViewController animated:YES];
                cell.userInteractionEnabled=YES;
            }
        }
    }else if(mapRanger.location != NSNotFound){
        NSRange idRange = [urlString rangeOfString:restString];
        if(idRange.location != NSNotFound){
            NSString *restaurantId = [urlString substringFromIndex:idRange.location+restString.length];

            IMGRestaurant *tmpRestaurant=[IMGRestaurant findById:[NSNumber numberWithInt:[restaurantId intValue]]];
            
            if (tmpRestaurant){
                NSNumber *userId = [[NSNumber alloc] init];
                if (user.userId != nil)
                {
                    userId = user.userId;
                }
                else
                    userId = [NSNumber numberWithInt:0];
                
                [TrackHandler trackWithUserId:userId andRestaurantId:(NSNumber*)restaurantId andContent:[NSDictionary dictionaryWithObject:@"1" forKey:@"type"]];
                NSMutableArray *viewControllers=[self.navigationController.viewControllers mutableCopy];
                DetailViewController *detailViewController = [[DetailViewController alloc] initWithRestaurant:tmpRestaurant];
                [viewControllers addObject:detailViewController];
                MapViewController *mapViewController=[[MapViewController alloc]initWithRestaurant:tmpRestaurant];
                mapViewController.amplitudeType = @"Notification";
                [viewControllers addObject:mapViewController];
                cell.userInteractionEnabled=YES;

                [self.navigationController setViewControllers:viewControllers animated:YES];
            }
        }
    }
    else if (diningguideRange.location != NSNotFound)
    {
        NSRange idRange = [urlString rangeOfString:@"diningguideid="];
        if (idRange.location != NSNotFound)
        {
            NSString *diningguideId = [urlString substringFromIndex:idRange.location+14];
            IMGDiningGuide *diningGuide = [IMGDiningGuide findDiningGuideById:[NSNumber numberWithInt:[diningguideId intValue]]];
            if (diningGuide)
            {
                DiningGuideRestaurantsViewController *diningResList = [[DiningGuideRestaurantsViewController alloc]initWithDiningGuide:diningGuide];
                cell.userInteractionEnabled=YES;

                [self.navigationController pushViewController:diningResList animated:YES];
            }
            else
            {
                [HomeUtil getSingleDiningGuideFromServiceWithDiningGuideId:[NSNumber numberWithInt:[diningguideId intValue]] andBlock:^(IMGDiningGuide *guide) {
//                    IMGDiningGuide *diningGuide = [IMGDiningGuide findDiningGuideById:[NSNumber numberWithInt:[diningguideId intValue]]];
                    DiningGuideRestaurantsViewController *diningResList = [[DiningGuideRestaurantsViewController alloc]initWithDiningGuide:guide];
                    cell.userInteractionEnabled=YES;

                    [self.navigationController pushViewController:diningResList animated:YES];
                }];
            }
        }
        else
        {
            V2_DiningGuideListViewController *dglvc = [[V2_DiningGuideListViewController alloc] init];
            cell.userInteractionEnabled=YES;

            [self.navigationController pushViewController:dglvc animated:YES];
            NSLog(@"jump to app");
        }
        
    }
    else if (eventRange.location != NSNotFound)
    {
        NSRange idRange = [urlString rangeOfString:@"eventid="];
        if (idRange.location != NSNotFound)
        {
            NSString *eventId = [urlString substringFromIndex:idRange.location+8];
            
            IMGEvent *event = [IMGEvent findById:[NSNumber numberWithInt:[eventId intValue]]];
            
            if (event)
            {
                IMGDiscover *discover=[[IMGDiscover alloc]init];
                discover.sortby = SORTBY_POPULARITY;
                if(event!=nil && [event.eventId intValue]>0){
                    discover.eventArray = [[NSMutableArray alloc]initWithCapacity:0];
                    IMGOfferType *offerType = [[IMGOfferType alloc]init];
                    offerType.offerId = event.eventId;
                    offerType.eventType=[NSNumber numberWithInt:1];
                    offerType.typeId = [NSNumber numberWithInt:4];
                    offerType.off = [NSNumber numberWithInt:0];
                    offerType.name = event.name;
                    [discover.eventArray addObject:offerType];
                }
                discover.fromWhere=@4;
                
                RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc]initWithEventId:[event.eventId intValue]];
                restaurantsViewController.amplitudeType = @"Notification";
                [restaurantsViewController initDiscover:discover];
                cell.userInteractionEnabled=YES;

                [self.navigationController pushViewController:restaurantsViewController animated:YES];
            }
            else
            {
                [HomeUtil getSingleEventFromServiceWithEventId:[NSNumber numberWithInt:[eventId intValue]] andBlock:^{
                    IMGEvent *event = [IMGEvent findById:[NSNumber numberWithInt:[eventId intValue]]];
                    IMGDiscover *discover=[[IMGDiscover alloc]init];
                    discover.sortby = SORTBY_POPULARITY;
                    if(event!=nil && [event.eventId intValue]>0){
                        discover.eventArray = [[NSMutableArray alloc]initWithCapacity:0];
                        IMGOfferType *offerType = [[IMGOfferType alloc]init];
                        offerType.offerId = event.eventId;
                        offerType.eventType=[NSNumber numberWithInt:1];
                        offerType.typeId = [NSNumber numberWithInt:4];
                        offerType.off = [NSNumber numberWithInt:0];
                        offerType.name = event.name;
                        [discover.eventArray addObject:offerType];
                    }
                    discover.fromWhere=@4;
                    RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc]initWithEventId:[event.eventId intValue]];
                    restaurantsViewController.amplitudeType = @"Notification";
                    [restaurantsViewController initDiscover:discover];
                    cell.userInteractionEnabled=YES;

                    [self.navigationController pushViewController:restaurantsViewController animated:YES];
                } failure:^{
                    cell.userInteractionEnabled=YES;

                }];
            }
            
        }
        else
        {
            NSLog(@"jump to app");
            cell.userInteractionEnabled=YES;
        }
    }
    else if ([urlString rangeOfString:@"app/search"].location != NSNotFound)
    {
//        IMGDiscover *discover=[[IMGDiscover alloc]init];
//        discover.sortby=SORTBY_POPULARITY;
//
//        discover.cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
//        RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc]initWithMap];
//        restaurantsViewController.amplitudeType = @"Notification";
//        [restaurantsViewController initDiscover:discover];
        
        V2_DiscoverListResultViewController *discoverListResultVC = [[V2_DiscoverListResultViewController alloc] init];
        discoverListResultVC.keywordString = @"";
        //discoverListResultVC.cityName = cityButton.titleLabel.text;
        
        
        NSRange range = [urlString rangeOfString:@"componentid="];
        if (range.location != NSNotFound) {
            discoverListResultVC.isFromHome = YES;
            discoverListResultVC.homeSectionComponentId = [NSNumber numberWithInt:[[urlString substringFromIndex:range.location+range.length] intValue]];
        }
        
        
        //[self.navigationController pushViewController:discoverListResultVC animated:YES];
        
        [self.navigationController pushViewController:discoverListResultVC animated:YES];
        
        cell.userInteractionEnabled=YES;

        //[self.navigationController pushViewController:restaurantsViewController animated:YES];
        
    }else if ([urlString rangeOfString:@"app/sortbyOffer"].location != NSNotFound){
//        IMGDiscover *discover=[[IMGDiscover alloc]init];
//        discover.sortby=SORTBY_OFFERS;
//
//        discover.cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
//        RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc]initWithMap];
//
//        [restaurantsViewController initDiscover:discover];
//        [self.navigationController pushViewController:restaurantsViewController animated:YES];
        
        V2_DiscoverListResultViewController *discoverListResultVC = [[V2_DiscoverListResultViewController alloc] init];
        discoverListResultVC.keywordString = @"";

        [self.navigationController pushViewController:discoverListResultVC animated:YES];
        cell.userInteractionEnabled=YES;
        
    }else if ([urlString rangeOfString:@"app/nearby"].location!=NSNotFound){
        
//        IMGDiscover *discover=[[IMGDiscover alloc]init];
//        discover.sortby=SORTBY_DISTANCE;
//        discover.fromWhere=[NSNumber numberWithInt:1];
//        discover.cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
//        RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc] initWithMap];
//        [restaurantsViewController initDiscover:discover];
//
//        [self.navigationController pushViewController:restaurantsViewController animated:NO];
        
        V2_DiscoverListResultViewController *discoverListResultVC = [[V2_DiscoverListResultViewController alloc] init];
        discoverListResultVC.keywordString = @"";
        discoverListResultVC.isHomeNearBy = YES;
        [self.navigationController pushViewController:discoverListResultVC animated:YES];
        
        cell.userInteractionEnabled=YES;

//      [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 1;
        
    }else if ([urlString rangeOfString:@"app/uploadphoto"].location!=NSNotFound){
        
        ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] initWithRestaurantTitle:nil andIsFromUploadPhoto:YES andIsUploadMenuPhoto:NO andCurrentRestaurantId:nil];
        picker.publishViewController = self;
        picker.maximumNumberOfSelection = 9;
        picker.assetsFilter = [ALAssetsFilter allPhotos];
        picker.showEmptyGroups=NO;
        picker.delegate = self;
        picker.isFromUploadPhoto = YES;
        picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
            if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
                NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
                return duration >= 5;
            } else {
                return YES;
            }
        }];
        [self.navigationController presentViewController:picker animated:YES completion:nil];
        
        cell.userInteractionEnabled=YES;

        
        
        
    }else if ([urlString rangeOfString:@"app/writereview"].location!=NSNotFound){
        
        
        ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc] init];
        publishReviewVC.isFromHotkey = YES;
        publishReviewVC.amplitudeType = @"Hot Key";
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:publishReviewVC];
        [self.navigationController presentViewController:navigationController animated:YES completion:nil];
        
        cell.userInteractionEnabled=YES;
    }else if ([urlString rangeOfString:@"app/coupon"].location != NSNotFound){
        
        NSRange idRange = [urlString rangeOfString:@"couponId="];
        if (idRange.location != NSNotFound)
        {
            NSString *couponId = [self returnPushId:urlString withIdRangeStr:@"couponId="];
            
            [IMGAmplitudeUtil trackCampaignWithName:@"RC - View Coupon Detail" andCampaignId:nil andBrandId:nil andCouponId:[NSNumber numberWithInt:[couponId intValue]] andLocation:nil andOrigin:@"Notification" andIsMall:NO];
            
            CouponDetailViewController *detailViewController = [[CouponDetailViewController alloc] init];
            detailViewController.couponId = [NSNumber numberWithInt:[couponId intValue]];
            MLNavigationController *nav = [[MLNavigationController alloc] initWithRootViewController:detailViewController];
            [self.navigationController presentViewController:nav animated:YES completion:nil];
            cell.userInteractionEnabled=YES;
        }
    }else if ([urlString rangeOfString:@"app/brand"].location != NSNotFound){
        NSRange idRange = [urlString rangeOfString:@"brandId="];
        if (idRange.location != NSNotFound) {
            NSString *brandId = [self returnPushId:urlString withIdRangeStr:@"brandId="];
            BrandViewController *brandViewController = [[BrandViewController alloc] init];
            brandViewController.brandId = brandId;
            [self.navigationController pushViewController:brandViewController animated:YES];
            cell.userInteractionEnabled=YES;
        }
    }else if ([urlString rangeOfString:@"app/mall"].location != NSNotFound){
        NSRange idRange = [urlString rangeOfString:@"mallId="];
        if (idRange.location != NSNotFound) {
            NSString *mallId = [self returnPushId:urlString withIdRangeStr:@"mallId="];
            BrandViewController *brandViewController = [[BrandViewController alloc] init];
            brandViewController.mallId = mallId;
            brandViewController.isMall = YES;
            [self.navigationController pushViewController:brandViewController animated:YES];
            cell.userInteractionEnabled=YES;
        }
    }else if ([urlString rangeOfString:@"app/campaign"].location != NSNotFound){
        NSRange idRange = [urlString rangeOfString:@"url="];
        if (idRange.location != NSNotFound) {
            NSString *campaignStr = [self returnPushId:urlString withIdRangeStr:@"url="];
            CampaignWebViewController *campaignController = [[CampaignWebViewController alloc] init];
            campaignController.campaignUrl = campaignStr;
            [self.navigationController pushViewController:campaignController animated:YES];
            cell.userInteractionEnabled=YES;
        }
    }else if ([urlString rangeOfString:@"app/delivery"].location != NSNotFound){
        DeliveryViewController *deliveryVC = [[DeliveryViewController alloc] init];
        [self.navigationController pushViewController:deliveryVC animated:YES];
        cell.userInteractionEnabled=YES;
    }

}

- (NSString *)returnPushId:(NSString *)urlString withIdRangeStr:(NSString *)idRangeStr{
    NSArray *arr = [urlString componentsSeparatedByString:@"&"];
    for (NSString *str in arr) {
        NSRange idRange = [str rangeOfString:idRangeStr];
        if (idRange.location != NSNotFound) {
            return [str substringFromIndex:idRange.location+idRange.length];
        }
    }
    return @"";
}
- (void)loadMoreData{
    offset = [NSNumber numberWithInt:[offset intValue]+ [max intValue]];
    
    [self initData];
}
//- (void)loadData{
//
//    if([offset intValue]<notificationsArray.count)
//    {
////        (int)offset+=(int)max;
//
//
//    }else{
//        _tableView.pullTableIsLoadingMore = NO;
//    }
//
//}
#pragma mark pullTableDelegate


//
//- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
//{
//
//    [self performSelector:@selector(loadData) withObject:nil afterDelay:1.0f];
//
//}

-(CGFloat)calculatedCommentTextHeight:(NSString*)comment{
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:10]};
    CGSize singleSize = [@"test" boundingRectWithSize:CGSizeMake(DeviceWidth - (40 + LEFTLEFTSET) - 30, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    CGSize textSize = [comment boundingRectWithSize:CGSizeMake(DeviceWidth - (40 + LEFTLEFTSET) - 30, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    NSInteger lines = textSize.height/singleSize.height;
    CGFloat height=0.0f;
    if(lines>3){
        height=3*singleSize.height;
    }else{
        height=textSize.height;
    }
    return height;
}

-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    
    NSMutableArray *photosArrM = [[NSMutableArray alloc] init];
    
    for (int i=0; i<assets.count; i++) {
        ALAsset *asset=assets[i];
        UIImageView *imgview=[[UIImageView alloc] init];
        imgview.contentMode=UIViewContentModeScaleAspectFill;
        imgview.clipsToBounds=YES;
        UIImage *tempImg=[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage];
        // [imgview setImage:tempImg];
        IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
        photo.image = tempImg;
        photo.photoUrl = [NSString stringWithFormat:@"%@",[asset valueForProperty:ALAssetPropertyAssetURL]];
        
        [photosArrM addObject:photo];
    }
    
    [picker dismissViewControllerAnimated:NO completion:^{
        ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc]initWithRestaurant:nil andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
        publishReviewVC.isFromHotkey = NO;
        publishReviewVC.amplitudeType = @"Notification Page";
        publishReviewVC.isUploadPhoto = YES;
        publishReviewVC.photosArrM = [NSMutableArray arrayWithArray:photosArrM];
        publishReviewVC.isFromOnboard = NO;
        publishReviewVC.ifSuccessLoginClickPost = YES;
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:publishReviewVC] animated:YES completion:nil];
        
        
    }];
}



@end
