//
//  NotificationsViewController.h
//  Qraved
//
//  Created by Laura on 14-8-25.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"

@protocol NotificationsViewControllerDelegate <NSObject>
@optional
- (void)notificationViewControllerBtnClick;

@end

@interface NotificationsViewController : BaseViewController

@property (nonatomic,assign) id<NotificationsViewControllerDelegate>nvcDelegate;
@property (nonatomic,assign) BOOL isFromTabMenu;
@end
