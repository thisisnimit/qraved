//
//  MyListAddRestaurantController.m
//  Qraved
//
//  Created by System Administrator on 9/24/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "MyListAddRestaurantController.h"
#import "UIViewController+Helper.h"
#import "UIColor+Helper.h"
#import "IMGMyList.h"
#import "IMGUser.h"
#import "ListHandler.h"
#import "MyListAddRestaurantView.h"
#import "MyListRestaurantsViewController.h"
#import "MyListEmptyController.h"
#import "LoadingView.h"
#import "UIConstants.h"
#import "ProfileViewController.h"
#import "AppDelegate.h"
#import <BlocksKit/BlocksKit+UIKit.h>

@implementation MyListAddRestaurantController{
	NSInteger max;
	NSInteger offset;
	NSMutableArray *restaurants;
	IMGUser *user;
	BOOL hasData;
	NSURLSessionTask *operation;
    NSString *searchString;
    NSMutableArray *searchRestaurants;
}


-(instancetype)initWithMyList:(IMGMyList*)mylist{
	if(self=[super init]){
		self.mylist=mylist;
		user=[IMGUser currentUser];
	}
	return self;
}

-(void)viewDidLoad{
	[super viewDidLoad];
	max=10;
	offset=0;
	hasData=YES;
    searchString = @"";
    searchRestaurants = [NSMutableArray array];
	[self setNavigation];
	_restaurantView=[[MyListAddRestaurantView alloc] initWithFrame:CGRectMake(LEFTLEFTSET,0,DeviceWidth-LEFTLEFTSET,DeviceHeight-44) andList:self.mylist.listId];
	_restaurantView.delegate=self;
    _restaurantView.listName=self.mylist.listName;
    [self.view addSubview:_restaurantView];
    [self loadRestaurantData:@"" block:^(BOOL status){
    	
    }];

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(searchRestaurants:) name:UITextFieldTextDidChangeNotification object:nil];
    for (UIImageView *imgV in _restaurantView.searchBar.subviews.firstObject.subviews.lastObject.subviews) {
        if ([imgV isMemberOfClass:[UIImageView class]]) {
            _restaurantView.imgV.frame = CGRectMake(8, 7.5, 13, 13);
            _restaurantView.imgV = imgV;
            [_restaurantView.imgV addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];
        }
    }
    // 设置searchBar文本颜色
    [_restaurantView updateSeachBar];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
    [[LoadingView sharedLoadingView] stopLoading];

}

-(void)setNavigation{

	self.navigationController.navigationBarHidden=NO;
	
	self.navigationItem.title=L(@"Add Restaurant");

	self.view.backgroundColor=[UIColor whiteColor];
	
	UIBarButtonItem *leftBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:NavigationBackImage] style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
	leftBtn.tintColor=[UIColor blackColor];
    self.navigationItem.leftBarButtonItem=leftBtn;

    UIBarButtonItem *saveBtn=[[UIBarButtonItem alloc] initWithTitle:L(@"Done") style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonClick)];
    saveBtn.tintColor=[UIColor colorWithRed:17/255.0 green:183/255.0 blue:206/255.0 alpha:1];
    self.navigationItem.rightBarButtonItem=saveBtn;
}

-(void)backClick{
	// [[NSNotificationCenter defaultCenter] postNotification:@"reload_list_detail"];
	 [self.navigationController popViewControllerAnimated:YES];
	//[self saveButtonClick];
	
}

-(void)loadRestaurantData:(NSString*)searchText block:(void(^)(BOOL))block{
	[[LoadingView sharedLoadingView] startLoading];
	// __weak MyListAddRestaurantController *weakSelf=self;
	if(hasData){
		if(searchText.length){
            NSDictionary *params;
            if ([self.mylist.listName isEqualToString:@"Favorites"]||[self.mylist.listName isEqualToString:@"Want to go"]) {
                params =@{@"userId":user.userId,@"listId":self.mylist.listId,@"max":[NSNumber numberWithInteger:max],@"offset":[NSNumber numberWithInteger:offset],@"listName":self.mylist.listName};

            }else{
                params =@{@"userId":user.userId,@"listId":self.mylist.listId,@"max":[NSNumber numberWithInteger:max],@"offset":[NSNumber numberWithInteger:offset],@"listName":self.mylist.listName};

            }
			operation=[ListHandler mylist_searchRestaurant:searchText withParams:params andBlock:^(NSNumber *total,NSArray *restaurants_){
				[self callback:searchText total:total restaurants:restaurants_ block:block];
			}];
		}else{
            NSDictionary *params;
            if ([self.mylist.listName isEqualToString:@"Favorites"]||[self.mylist.listName isEqualToString:@"Want to go"]) {
                params=@{@"userId":user.userId,@"listId":self.mylist.listId,@"max":[NSNumber numberWithInteger:max],@"offset":[NSNumber numberWithInteger:offset],@"listName":self.mylist.listName};
            }else{
                params=@{@"userId":user.userId,@"listId":self.mylist.listId,@"max":[NSNumber numberWithInteger:max],@"offset":[NSNumber numberWithInteger:offset],@"listName":self.mylist.listName};

            }
			
			operation=[ListHandler mylist_recentlyViewRestaurants:params block:^(NSNumber *total,NSArray *restaurants_){
				[self callback:searchText total:total restaurants:restaurants_ block:block];
			}];
		}
    }else{
        [[LoadingView sharedLoadingView] stopLoading];
    }
}

-(void)callback:(NSString*)searchText total:(NSNumber*)total restaurants:(NSArray*)restaurants_ block:(void(^)(BOOL))block{
    NSString *str = self.restaurantView.searchBar.text;
    if ([str isEqualToString:@"Search restaurants by name"]) {
        str = @"";
    }
	if(![searchText isEqualToString:str]){
        return;
    }
//    NSMutableArray *searchRestaurants=[NSMutableArray array];
    if (offset==0) {
        [searchRestaurants removeAllObjects];
        
    }
	
	[searchRestaurants addObjectsFromArray:restaurants_];
	self.restaurantView.restaurants=searchRestaurants;
	offset+=max;
	hasData=offset>[total intValue]?NO:YES;
	if(self.restaurantView.refreshView==nil){
		[self.restaurantView addRefreshView];
	}
	if(hasData){
		[self.restaurantView resetRefreshViewFrame];
		self.restaurantView.refreshView.hidden=NO;
	}
	if(!hasData && self.restaurantView.refreshView){
		self.restaurantView.refreshView.hidden=YES;
		// [self.resturantView.refreshView removeFromSuperview];
	}
	[[LoadingView sharedLoadingView] stopLoading];
	block(hasData);
}

-(void)saveButtonClick{
    NSMutableArray *restaurantIdArr = [NSMutableArray array];
    for (NSDictionary *dic in _restaurantView.restaurants) {
        if ([[dic objectForKey:@"inList"] intValue]==3) {
            [restaurantIdArr addObject:[dic objectForKey:@"restaurantId"]];
        }
    }
    if (restaurantIdArr.count>0) {
        [[LoadingView sharedLoadingView] startLoading];
        [ListHandler listAddRestaurantsWithUser:[IMGUser currentUser] andListIds:@[self.mylist.listId] andRestaurantIds:restaurantIdArr andBlock:^(BOOL succeed, id status) {
            [[LoadingView sharedLoadingView] stopLoading];

            if (succeed) {
                
                for (int i = 0; i < restaurantIdArr.count; i ++) {
                    for (int j = 0; j < searchRestaurants.count; j ++) {
                        NSDictionary *dic = searchRestaurants[j];
                        if ([restaurantIdArr[i] isEqual:[dic objectForKey:@"restaurantId"]]) {
                            
                            [dic setValue:self.mylist.listName forKey:@"listName"];
                            [dic setValue:@1 forKey:@"inList"];
                        }
                    }
                }
                NSString *restaurantIds = [restaurantIdArr componentsJoinedByString:@","];
                
                [IMGAmplitudeUtil trackSavedWithName:@"BM - Save Restaurant Succeed" andRestaurantId:[NSNumber numberWithInt:[restaurantIds intValue]] andRestaurantTitle:nil andOrigin:nil andListId:self.mylist.listId andType:nil andLocation:nil];
                
                self.restaurantView.restaurants = searchRestaurants;
                if (self.refreshRestaurantList != nil) {
                    self.refreshRestaurantList();
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshList" object:nil];
                UIAlertView *alert = [UIAlertView bk_showAlertViewWithTitle:@"Your restaurant was successfully added" message:@"" cancelButtonTitle:@"OK" otherButtonTitles:nil handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    [self.navigationController popViewControllerAnimated:YES];
                }];
                [alert show];
            }
        }];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)searchRestaurants:(NSNotification*)notification{
	offset=0;
	hasData=YES;
	UITextField *field=(UITextField*)notification.object;
	self.restaurantView.restaurants=nil;
	[operation cancel];
	if(field.text.length){
		field.textColor=[UIColor color333333];
        self.restaurantView.lblViewed.text = @"Restaurants";
	}else{
		field.textColor=[UIColor colorCCCCCC];
		field.placeholder=L(@"Search restaurants by name");
        self.restaurantView.lblViewed.text = @"Recently Viewed";
	}
    //[self.view endEditing:YES];
    searchString = field.text;
	[self loadRestaurantData:searchString block:^(BOOL hasData){
			
	}];
}

-(void)myList:(NSNumber*)listId addRestaurant:(NSNumber*)restaurantId block:(void(^)(BOOL,id))block{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil){
        [[LoadingView sharedLoadingView] stopLoading];
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    if ([user.userId isEqualToNumber:(currentUser.userId)]) {
        user.token = currentUser.token;
    }
	[ListHandler listAddRestaurantWithUser:user andListId:listId andRestaurantId:restaurantId andBlock:^(BOOL succeed,id status){
        if (!succeed) {
            if([status isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
                [[AppDelegate ShareApp] goToLoginController];
                return;
            }
        }
		if(succeed){
        }
		block(succeed,status);
	}];
}

-(void)myList:(NSNumber*)listId removeRestaurant:(NSNumber*)restaurantId block:(void(^)(BOOL))block failure:(void(^)(NSString *exceptionMsg))failureBlock{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil){
        [[LoadingView sharedLoadingView] stopLoading];
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    if ([user.userId isEqualToNumber:(currentUser.userId)]) {
        user.token = currentUser.token;
    }
	[ListHandler listRemoveWithUser:user andListId:listId andRestaurantId:restaurantId andBlock:^{
		block(YES);
    }failure:^(NSString *exceptionMsg){
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
        failureBlock(exceptionMsg);
    }];
}

@end
