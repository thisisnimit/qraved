//
//  V2_DiscoverListResultViewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverListResultViewController.h"
#import "V2_CityView.h"
#import "CityUtils.h"
#import "V2_DiscoverListResultTableViewCell.h"
#import "V2_DiscoverListResultRestaurantTableViewCell.h"
#import "V2_DiscoverListHandler.h"
#import "SearchUtil.h"
#import "V2_DiscoverListJournalSeeAllViewController.h"
#import "V2_DiscoverListResultRestaurantTableViewCellModel.h"
#import "DiscoverUtil.h"
#import "LoadingView.h"
#import "MapViewController.h"
#import "MapUtil.h"
#import "V2_DiscoverListHandler.h"
#import "selecteCityView.h"
#import "V2_DiscoverListResultFiltersViewController.h"
#import "JournalDetailViewController.h"
#import "DetailViewController.h"
#import "SearchEmptyResultView.h"
#import "RestaurantSuggestViewController.h"
#import "V2_DistrictView.h"
#import "HomeService.h"
#import "V2_DistrictModel.h"
#import "V2_DiscoverListViewController.h"

@interface V2_DiscoverListResultViewController ()<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate,UISearchDisplayDelegate, V2_DiscoverListResultTableViewCellDelegate, V2_DiscoverListResultRestaurantTableViewCellDelegate, SearchEmptyResultViewDelegate,UIAlertViewDelegate>
{
    
    UIView *navView;
    UIButton *cityButton;
    NSNumber *citySelectIndex;
    V2_CityView *cityView;
    int offset;
    MapViewController *mapViewController;
    NSNumber *minLatitudeTemp;
    NSNumber *maxLatitudeTemp;
    NSNumber *minLongitudeTemp;
    NSNumber *maxLongitudeTemp;
    NSMutableArray *restaurantArr;
    NSMutableArray *dataArray;
    MKMapView *mapView;
    UIButton *mapBtn;
    UIView *btnView;
    selecteCityView *selecteCityV;
    
    NSMutableArray * CuisineArray;
    NSMutableArray * FoodtypeArray;
    NSMutableArray * LandmarkAnddistrictArray;
    NSMutableArray * RestaurantArray;
    NSMutableArray * JournalArray;
    NSMutableArray *districtArr;
    IMGDiscover *discover;
    V2_DiscoverListResultRestaurantTableViewCell *currentCell;
    V2_DistrictView *districtView;
    NSString *currentDistrict;
    NSString *currentLandMark;
    NSString *currentArea;
    UIButton *navigateButton;
    
    //notification send
    NSString *notificationcuisines;
    NSString *notificationfeature;
    NSString *notificationprice;
    NSString *notificationrating;
    NSString *notificationsortby;
    SearchEmptyResultView *headerView;
    
    NSString *offsStr;
    NSString *offersStr;
    BOOL isCheckLocation;
    BOOL isSearchThisArea;
    FilterType filterType;
    IMGShimmerView *shimmerView;
}
@property (nonatomic, strong) NSMutableArray *cityArray;
@property (nonatomic, strong)   UISearchBar *locationSearchBar;
@property (nonatomic, strong)  UITableView  *tableView;
@property (nonatomic, strong)  UITableView  *searchTableView;
@property (nonatomic, strong)  V2_DiscoverListResultRestaurantTableViewCellModel  *V2_DiscoverListResultRestaurantModel;
@property (nonatomic, strong) NSMutableArray *instagramArr;

@property (nonatomic, weak) UIImageView *imgV;
@end

@implementation V2_DiscoverListResultViewController

-(void)initDiscover:(IMGDiscover *)tmpDiscover{
    discover=tmpDiscover;
}

-(IMGDiscover *)getDiscover{
    return discover;
}



- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    [self.navigationController.navigationItem setHidesBackButton:YES];
    [self.navigationController.navigationBar addSubview:cityButton];
    [self.navigationController.navigationBar layoutIfNeeded];
    [[AppDelegate ShareApp].window addSubview:btnView];
    for (UIImageView *imgV in _locationSearchBar.subviews.firstObject.subviews.lastObject.subviews) {
        if ([imgV isMemberOfClass:[UIImageView class]]) {
            imgV.frame = CGRectMake(8, 7.5, 13, 13);
            _imgV = imgV;
            [_imgV addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];
        }
    }
    // 设置searchBar文本颜色
    //    [self updateSeachBar];
    
    
    if (isCheckLocation && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
        isCheckLocation = NO;
        [self refreshInstagram:NEARBYTITLE];
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    [cityButton removeFromSuperview];
    [btnView removeFromSuperview];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self addSearchbar];
    [self initTableview];
    [self loadEmptyTableVIewHeadView];
    //    [self addsearchTableView];
    [self bottomBtn];
    self.cityArray = [NSMutableArray array];
    
    [self  requestCity];
    
    [self loadNavbutton];
    //    [self addBackBtn];
    [self setBackBarButtonOffset30];
    [self requestGetTrendingJournal];
    
    
    [self requestSelecteCityViewData];
    
    [self getOffers];
    
    offset = 0;
    restaurantArr = [NSMutableArray array];
    dataArray = [NSMutableArray array];
    
    CuisineArray = [NSMutableArray array];
    FoodtypeArray = [NSMutableArray array];
    LandmarkAnddistrictArray = [NSMutableArray array];
    RestaurantArray = [NSMutableArray array];
    JournalArray = [NSMutableArray array];
    districtArr= [NSMutableArray array];
    self.instagramArr = [NSMutableArray array];
    [self initMap];
    
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setValue:self.keywordString forKey:@"Keyword"];
    if (self.keywordString.length>0) {
        [param setValue:@"Keyword Search" forKey:@"Type"];
    }else{
        [param setValue:@"Suggestion Search" forKey:@"Type"];
    }
    [[Amplitude instance] logEvent:@"SC – View Search Result Page" withEventProperties:param];
    
}
- (void)goToBackViewController{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - requestSelecteCityViewData
- (void)requestSelecteCityViewData{
    if (self.isLocation) {
        [self doSelectCity:cityButton];
    }
    [HomeService getLocation:nil andIsPreferred:NO andBlock:^(id locationArr) {
        
        //cityButton.enabled  = YES;
        [districtArr addObjectsFromArray:locationArr];
        
    } andError:^(NSError *error) {
        
    }];
}

#pragma mark - requestGetTrendingJournal
- (void)requestGetTrendingJournal{
    NSString *journalString;
    if (self.keywordString != nil ) {
        journalString = self.keywordString;
    }else if (self.placeholderString != nil){
        journalString = self.placeholderString;
    }else{
        journalString = @"";
    }
    
    NSDictionary *dict = @{@"keyword":journalString};//@"Cafe"
    [[LoadingView sharedLoadingView] startLoading];
    [V2_DiscoverListHandler getgetTrendingJournal:dict andSuccessBlock:^(V2_DiscoverListResultRestaurantTableViewCellModel * model) {
        [[LoadingView sharedLoadingView] stopLoading];
        if (model) {
            _V2_DiscoverListResultRestaurantModel = model;
            
            [dataArray addObjectsFromArray:model.data];
            //            NSLog(@"=====%lu",(unsigned long)dataArray.count);
        }
       [self requestGetRestaurant:nil cuisines:nil feature:nil price:nil rating:nil sortby:nil loadMore:NO];
    } anderrorBlock:^{
        [self requestGetRestaurant:nil cuisines:nil feature:nil price:nil rating:nil sortby:nil loadMore:NO];
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

- (void)loadMoreData{
    [self requestGetRestaurant:nil cuisines:notificationcuisines ? notificationcuisines: @"" feature:notificationfeature ? notificationfeature: @"" price:notificationprice ? notificationprice: @"" rating:notificationrating ? notificationrating: @"" sortby:notificationsortby ? notificationsortby: @"" loadMore:YES];
}
#pragma amrk - requestGetRestaurant
- (void)requestGetRestaurant:(NSString *)districtId cuisines:(NSString *)cuisines feature:(NSString *)feature price:(NSString *)price rating:(NSString *)rating sortby:(NSString *)sortby loadMore:(BOOL)isLoadMore{
    
    IMGUser * user = [IMGUser currentUser];
    NSNumber *cityID = [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    NSString *latitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"];
    NSString *longitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"];
    
    NSString *homeSectionQuery = [NSString stringWithFormat:@"os=%@&app=%@&versionName=%@&versionCode=%@&deviceId=%@&userId=%@&cityId=%@&districtId=%@&latitude=%@&longitude=%@",@"ios",@"mobileapp",QRAVED_WEB_SERVICE_VERSION,QRAVED_VERSION,user.token,user.userId,cityID ? cityID : @2,@0,latitude,longitude];
    
    
    NSMutableDictionary *pamars = [[NSMutableDictionary alloc] init];
    
    if ([Tools isBlankString:sortby]) {
        [pamars setValue:@"popularity" forKey:@"sortby"];
    }else{
        [pamars setValue:sortby forKey:@"sortby"];
        if ([sortby isEqualToString:@"distance"]) {
            [pamars setValue:@"asc" forKey:@"order"];
        }
    }
//    [pamars setValue:[Tools isBlankString:sortby]?@"popularity":sortby forKey:@"sortby"];
    
    [pamars setValue:cityID ? cityID : @1 forKey:@"cityID"];
    [pamars setValue:latitude forKey:@"latitude"];
    [pamars setValue:longitude forKey:@"longitude"];
    [pamars setValue:[NSString stringWithFormat:@"%d",offset] forKey:@"offset"];
    [pamars setValue:@"10" forKey:@"max"];
    
    if (self.keywordString != nil) {
        [pamars setValue:self.keywordString forKey:@"keyword"];
    }else{
        [pamars setValue:self.placeholderString forKey:@"keyword"];
    }
    
    if ([_isFrom isEqualToString:@"1"]) {//location
        
        if ([_typeString isEqualToString:@"landmark"]) {
            [pamars setValue:_locationID ? _locationID : @"" forKey:@"landmarkIDs"];
        }else if ([_typeString isEqualToString:@"district"]){
            [pamars setValue:_locationID ? _locationID : @"" forKey:@"districtIDs"];
        }
    }else if ([_isFrom isEqualToString:@"2"]){//cuisine
        
        [pamars setValue:_cuisineID ? _cuisineID : @"" forKey:@"cuisineIDs"];
        
    }else if ([_isFrom isEqualToString:@"3"]){//food
        
        [pamars setValue:_foodID ? _foodID : @"" forKey:@"foodTagIds"];
        
    }
    
    
    if (![Tools isBlankString:currentDistrict]) {
        [pamars setValue:currentDistrict  forKey:@"districtIDs"];
    }
    
    if (![Tools isBlankString:currentLandMark]) {
        [pamars setValue:currentLandMark  forKey:@"landmarkIDs"];
    }
    
    if (![Tools isBlankString:currentArea]) {
        [pamars setValue:currentArea  forKey:@"areaIDs"];
    }
    
    if (![Tools isBlankString:districtId]) {
        [pamars setValue:districtId forKey:@"districtIDs"];
    }
    
    if (![Tools isBlankString:cuisines]) {
        
        [pamars setValue:cuisines forKey:@"cuisineIDs"];
    }
    if (![Tools isBlankString:feature]) {
        
        [pamars setValue:feature forKey:@"tagIDs"];
        
    }
    if (![Tools isBlankString:price]) {
        
        [pamars setValue:price forKey:@"price"];
    }
    if (![Tools isBlankString:rating]) {
        
        [pamars setValue:rating forKey:@"rating"];
    }
    
    if (filterType == FilterTypePromo) {
        [pamars setValue:offsStr forKey:@"offs"];
        [pamars setValue:offersStr forKey:@"offerTypes"];
    }else if (filterType == FilterTypeOpenNow){
        [pamars setValue:@"1" forKey:@"openNowToggle"];
    }else if (filterType == FilterTypeSaved){
        [pamars setValue:@"1" forKey:@"myListToggle"];
    }
    
    if (self.isFromHome == YES) {
        [pamars setValue: homeSectionQuery ? homeSectionQuery : @"" forKey:@"homeSectionQuery"];
        [pamars setValue:self.homeSectionComponentId ? self.homeSectionComponentId : @"" forKey:@"homeSectionComponentId"];
    }
    NSString *nearByStr = [cityButton titleForState:UIControlStateNormal];
    if ([nearByStr isEqualToString:NEARBYTITLE]) {
        [pamars setValue:@"nearby" forKey:@"specialLandmark"];
    }
    
    if (isSearchThisArea) {
        double deltaLatitude = mapViewController.mapView.region.span.latitudeDelta;
        double deltaLongitude = mapViewController.mapView.region.span.longitudeDelta;
        
        double centerLatitude = mapViewController.mapView.region.center.latitude;
        double centerLongitude = mapViewController.mapView.region.center.longitude;
        
        NSNumber *minLatitudeTemp = [NSNumber numberWithDouble:[MapUtil minLatitude:centerLatitude deltaLatitude:deltaLatitude]];
        NSNumber *maxLatitudeTemp = [NSNumber numberWithDouble:[MapUtil maxLatitude:centerLatitude deltaLatitude:deltaLatitude]];
        NSNumber *minLongitudeTemp = [NSNumber numberWithDouble:[MapUtil minLongitude:centerLongitude deltaLongitude:deltaLongitude]];
        NSNumber *maxLongitudeTemp = [NSNumber numberWithDouble:[MapUtil maxLongitude:centerLongitude deltaLongitude:deltaLongitude]];
        
        [pamars setValue:minLatitudeTemp forKey:@"minLatitude"];
        [pamars setValue:maxLatitudeTemp forKey:@"maxLatitude"];
        [pamars setValue:minLongitudeTemp forKey:@"minLongitude"];
        [pamars setValue:maxLongitudeTemp forKey:@"maxLongitude"];
    }
    
    [[LoadingView sharedLoadingView]startLoading];
    [SearchUtil getSearchResault:pamars andSuccessBlock:^(NSMutableArray *array, NSNumber *restaurantCount) {
        [[LoadingView sharedLoadingView]stopLoading];
        [self.tableView.mj_footer endRefreshing];
        if (!isLoadMore) {
            [shimmerView removeFromSuperview];
            [restaurantArr removeAllObjects];
        }
        
        if ([array isKindOfClass:[NSMutableArray class]]) {
            if (array.count>0) {
                offset += 10;
            }
            [restaurantArr addObjectsFromArray:array];
            
            [mapViewController setMapDatas:restaurantArr offset:0];
        }
        
        if (restaurantArr.count==0 && dataArray.count == 0) {
            headerView.hidden = NO;
        }else{
            headerView.hidden = YES;
        }
        [self.tableView reloadData];
    } anderrorBlock:^{
        [self.tableView.mj_footer endRefreshing];
        [shimmerView removeFromSuperview];
        [[LoadingView sharedLoadingView]stopLoading];
    }];
}

#pragma mark - request selecte city
- (void)requestCity{
    [CityUtils getCitiesWithCountryId:0 andBlock:^(NSArray *cityArray){
        [self.cityArray addObjectsFromArray:cityArray];
    }];
}
#pragma mark - add search bar
- (void)addSearchbar{
    
    UIView *headerview= [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 50)];
    headerview.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:headerview];
    [self.view bringSubviewToFront:headerview];
    
    UIButton *button =[UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(15, 5, DeviceWidth - 30 , 40);
    button.backgroundColor = [UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1];
    button.layer.cornerRadius = 3;
    button.layer.masksToBounds = YES;
    [button addTarget:self action:@selector(butonBack:) forControlEvents:UIControlEventTouchUpInside];
    [headerview addSubview:button];
    
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(5, 25/2, 15, 15)];
    imageV.image = [UIImage imageNamed:@"ic_search_medium"];
    //    imageV.backgroundColor = [UIColor  greenColor];
    [button addSubview:imageV];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(imageV.right +5, 0, button.width - 25, 40)];
    if (self.keywordString != nil ) {
        
        lblTitle.text = self.keywordString;
    }else{
        
        lblTitle.text = self.placeholderString;
    }
    lblTitle.textColor = [UIColor color333333];
    lblTitle.font = [UIFont systemFontOfSize:14];
    [button addSubview:lblTitle];
    
    
}
- (void)butonBack:(UIButton *)button{
    if (self.isFromHome == YES) {
        V2_DiscoverListViewController *V2_DiscoverListVC = [[V2_DiscoverListViewController alloc] init];
        V2_DiscoverListVC.isFromHome = YES;
        [self.navigationController pushViewController:V2_DiscoverListVC animated:YES];
        
    }else{
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}
#pragma mark - btn city
- (void)loadNavbutton{
    
    cityButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"currentCityId"] != nil && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied){
        [cityButton setTitle:NEARBYTITLE forState:UIControlStateNormal];
    }else{
        [cityButton setTitle:[[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT] capitalizedString] forState:UIControlStateNormal];
    }
    
    
    if (self.isHomeNearBy) {
        [cityButton setTitle:NEARBYTITLE forState:UIControlStateNormal];
    }
    
    if ([_isFrom isEqualToString:@"1"]) {
        [cityButton setTitle:self.cityName forState:UIControlStateNormal];
    }
    
    citySelectIndex = [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    [cityButton setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    [cityButton setImage:[UIImage imageNamed:@"ic_city_down"] forState:UIControlStateNormal];
    [cityButton setImage:[UIImage imageNamed:@"ic_city_up"] forState:UIControlStateSelected];
    cityButton.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    [cityButton addTarget:self action:@selector(doSelectCity:) forControlEvents:UIControlEventTouchUpInside];
    
    
    CGSize titleSize = [cityButton.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17]}];
    // [cityButton sizeToFit];
    
    cityButton.height = 35;
    cityButton.width= titleSize.width + 50;
    
    cityButton.backgroundColor = [UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1];
    cityButton.layer.masksToBounds = YES;
    cityButton.layer.cornerRadius = 18;
    cityButton.alpha = 0.9;
    
    cityButton.x = (self.view.width - cityButton.width)/2;
    cityButton.y = 4;
    cityButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 15);
    [cityButton.layer setAffineTransform:CGAffineTransformMakeScale(-1, 1)];
    [cityButton.titleLabel.layer setAffineTransform:CGAffineTransformMakeScale(-1, 1)];
}

- (void)doSelectCity:(UIButton *)btn{
    if (districtView) {
        [districtView removeFromSuperview];
    }
    districtView = [[V2_DistrictView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+20)];
    if (districtArr.count==0) {
        districtView.districtArr = self.locationArr;
    }else{
        districtView.districtArr = districtArr;
    }
    
    if (self.isLocation) {
        [districtView changeLocationSkip];
    }
    
    __weak typeof(self) weakSelf = self;
    districtView.selectedDistrict = ^(id model) {
        if ([model isKindOfClass:[V2_AreaModel class]]) {
            [weakSelf filterByArea:model];
        }else if ([model isKindOfClass:[V2_DistrictModel class]]){
            [weakSelf filterByDistrict:model];
        }
    };
    
    districtView.nearbyCheck = ^{
        if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied) {
            isCheckLocation = YES;
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"To continue, let your device turn on location" delegate:weakSelf cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
            [alert show];
            
        }else{
            [weakSelf refreshInstagram:NEARBYTITLE];
        }
    };
    
    [[AppDelegate ShareApp].window addSubview:districtView];
}

- (void)filterByArea:(V2_AreaModel *)model{
    [self resetSelectArea];
    if ([model.type isEqualToString:@"landmark"]) {
        currentLandMark = [NSString stringWithFormat:@"%@",model.districtId];
    }else{
        currentDistrict = [NSString stringWithFormat:@"%@",model.districtId];
    }
    [self refreshInstagram:model.name];
}

- (void)resetSelectArea{
    self.isFrom = nil;
    isSearchThisArea = NO;
    currentLandMark = @"";
    currentDistrict = @"";
    currentArea = @"";
}

- (void)filterByDistrict:(V2_DistrictModel *)model{
    [self resetSelectArea];
    currentArea = [NSString stringWithFormat:@"%@",model.districtId];
    [self refreshInstagram:model.name];
}

- (void)refreshInstagram:(NSString *)name{
    
    [cityButton setTitle:name forState:UIControlStateNormal];
    CGSize titleSize = [cityButton.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17]}];
    cityButton.height = 35;
    cityButton.width=titleSize.width+50;
    cityButton.y = 4;
    cityButton.x = (self.view.width - cityButton.width) / 2;
    
    offset=0;
    [self requestGetRestaurant:nil cuisines:nil feature:nil price:nil rating:nil sortby:nil loadMore:NO];
    
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
        }
    }
}


#pragma marm - map
- (void)initMap{
    
    mapViewController=[[MapViewController alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-44) andSearchThisArea:YES];
    mapViewController.fromSearchPage=YES;
    mapViewController.fromSaved = YES;
    mapViewController.srpAmplitude = YES;
    
    mapViewController.restaurantsViewController=self;
    
    __weak typeof(self) weakSelf = self;
    mapViewController.searchThisArea = ^{
        offset=0;
        isSearchThisArea = YES;
        [weakSelf requestGetRestaurant:nil cuisines:nil feature:nil price:nil rating:nil sortby:nil loadMore:NO];
    };
    
    mapView = mapViewController.mapView;
    
    mapView.frame = CGRectMake(0,0, DeviceWidth, self.view.frame.size.height);
    mapView.hidden = YES;
    [self.view addSubview:mapViewController.mapView];
    
    //     [self bottomBtn];
    
    navigateButton = [[UIButton alloc] initWithFrame:CGRectMake(DeviceWidth-50, 10, 35, 35)];
    
    navigateButton.backgroundColor = [UIColor clearColor];
    [navigateButton addTarget:self action:@selector(nearYouBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navigateButton setImage:[UIImage imageNamed:@"ic_located"] forState:UIControlStateNormal];
    
    [mapView addSubview:navigateButton];
    [self.view bringSubviewToFront:mapBtn];
}
- (void)nearYouBtnClick
{
    CLLocationCoordinate2D center;

    center.latitude=[AppDelegate ShareApp].locationManager.location.coordinate.latitude;
    center.longitude = [AppDelegate ShareApp].locationManager.location.coordinate.longitude;
    CLLocationCoordinate2D theCoordinate =CLLocationCoordinate2DMake(center.latitude, center.longitude);
    MKCoordinateSpan span;
    span.latitudeDelta=0.03;
    span.longitudeDelta=0.005;
    MKCoordinateRegion theRegion;
    theRegion.center=theCoordinate;
    theRegion.span=span;
    [mapView setRegion:theRegion animated:NO];
    
    [mapView regionThatFits:theRegion];
    mapView.showsUserLocation = YES;

}



#pragma mark - createUI
- (void)initTableview{
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, DeviceWidth, DeviceHeight - 64 - 50 +20) style:UITableViewStyleGrouped];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    //    [self bottomBtn];
    
    shimmerView = [[IMGShimmerView alloc] initWithFrame:CGRectMake(0, 50, DeviceWidth, self.view.frame.size.height - 50)];
    [shimmerView createRestaurantShimmerView];
    [self.view addSubview:shimmerView];
}
- (void)bottomBtn{
    
    btnView =[[UIView alloc] init];
    btnView.backgroundColor = [UIColor greenColor];
    //    [self.view addSubview:btnView];
    //    [self.view bringSubviewToFront:btnView];
    [[AppDelegate ShareApp].window addSubview:btnView];
    
    btnView.sd_layout
    .bottomSpaceToView([AppDelegate ShareApp].window , 20)
    .centerXEqualToView([AppDelegate ShareApp].window)
    .heightIs(36)
    .widthIs(184);
    btnView.layer.borderColor = [UIColor colorCCCCCC].CGColor;
    btnView.layer.borderWidth = 1;
    btnView.layer.cornerRadius = 18;
    btnView.layer.masksToBounds = YES;
    
    mapBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [mapBtn setTitle:@"MAP" forState:UIControlStateNormal];
    [mapBtn setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    mapBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [mapBtn setImage:[UIImage imageNamed:@"ic_map_small"] forState:UIControlStateNormal];
    [btnView addSubview:mapBtn];
    mapBtn.backgroundColor = [UIColor whiteColor];
    [mapBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 0)];
    [mapBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -40, 0, 0)];
    [mapBtn addTarget:self action:@selector(btnMapClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnFilters = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnFilters setTitle:@"FILTERS" forState:UIControlStateNormal];
    [btnFilters setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    [btnFilters setImage:[UIImage imageNamed:@"ic_tune_small"] forState:UIControlStateNormal];
    btnFilters.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [btnFilters addTarget:self action:@selector(btnFiltersClick:) forControlEvents:UIControlEventTouchUpInside];
    btnFilters.backgroundColor = [UIColor whiteColor];
    [btnView addSubview:btnFilters];
    btnFilters.backgroundColor = [UIColor whiteColor];
    [btnFilters setImageEdgeInsets:UIEdgeInsetsMake(0, 70, 0, 0)];
    [btnFilters setTitleEdgeInsets:UIEdgeInsetsMake(0, -40, 0, 0)];
    [btnFilters addTarget:self action:@selector(btnFiltersClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *lineview= [[UIView alloc] init];
    lineview.backgroundColor = [UIColor colorCCCCCC];
    [btnView addSubview:lineview];
    
    mapBtn.sd_layout
    .topSpaceToView(btnView, 0)
    .leftSpaceToView(btnView, 0)
    .heightIs(36)
    .widthIs(85);
    
    btnFilters.sd_layout
    .topEqualToView(mapBtn)
    .leftSpaceToView(mapBtn, 0)
    .heightIs(36)
    .widthIs(99);
    
    lineview.sd_layout
    .topSpaceToView(btnView, 5)
    .bottomSpaceToView(btnView, 5)
    .widthIs(1)
    .leftSpaceToView(mapBtn, 0);
    //    .centerXEqualToView(btnView);
    
}
- (void)btnMapClick:(UIButton *)btnMap{
    NSLog(@"map");
    mapBtn.selected = !mapBtn.selected;
    if (mapBtn.selected) {
        //        wantToGoTab.hidden = YES;
        btnView.sd_resetLayout
        .bottomSpaceToView([AppDelegate ShareApp].window , 165)
        .centerXEqualToView([AppDelegate ShareApp].window)
        .heightIs(36)
        .widthIs(184);
        self.tableView.hidden = YES;
        mapView.hidden = NO;
        [mapBtn setTitle:@"LIST" forState:UIControlStateNormal];
        [mapBtn setImage:[UIImage imageNamed:@"ic_list_view"] forState:UIControlStateNormal];
        [mapBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 0)];
        [mapBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -40, 0, 0)];
        
        [[Amplitude instance] logEvent:@"SC - View Search Result Restaurant on Map" withEventProperties:@{}];
    }else{
        
        //        wantToGoTab.hidden = NO;
        btnView.sd_resetLayout
        .bottomSpaceToView([AppDelegate ShareApp].window , 20)
        .centerXEqualToView([AppDelegate ShareApp].window)
        .heightIs(36)
        .widthIs(184);
        self.tableView.hidden = NO;
        mapView.hidden = YES;
        [mapBtn setTitle:@"MAP" forState:UIControlStateNormal];
        [mapBtn setImage:[UIImage imageNamed:@"ic_map_small"] forState:UIControlStateNormal];
        [mapBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 50, 0, 0)];
        [mapBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, -30, 0, 0)];
        
    }
    
    
}
- (void)btnFiltersClick:(UIButton *)btnFilters{
    
    NSLog(@"btnFilters");
    V2_DiscoverListResultFiltersViewController *V2_DiscoverListResultFiltersVC = [[V2_DiscoverListResultFiltersViewController alloc] initWithDiscover:discover];
    V2_DiscoverListResultFiltersVC.fvcDelegate = self;
    [self.navigationController pushViewController:V2_DiscoverListResultFiltersVC animated:YES];
    
}

#pragma mark - tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 2;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    //    if (tableView == self.tableView) {
    
    if (section == 0) {
        if (dataArray.count >0) {
            return 1;
        }else{
            return 0;
        }
    }else if (section == 1){
        
        return restaurantArr.count;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    //    if (tableView == self.tableView) {
    if (section == 0) {
        if (dataArray.count >0) {
            
            return 40;
        }else{
            return 0.001;
        }
    }else if (section == 1){
        if (restaurantArr.count == 0 || dataArray.count == 0) {
            return 0.0001;
        }else{
            
            return 5;
        }
    }
    return 0.001;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    return  0.0001;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    //    if (tableView == self.tableView) {
    if (section == 0) {
        if (dataArray.count >0) {
            UIButton *headerView = [[UIButton alloc ]initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
            headerView.backgroundColor = [UIColor whiteColor];
            [headerView addTarget:self action:@selector(SeeAllClick:) forControlEvents:UIControlEventTouchUpInside];
            
            UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, DeviceWidth - 80, 40)];
            lblTitle.text = @"Journals";
            lblTitle.textColor = [UIColor color333333];
            lblTitle.font = [UIFont boldSystemFontOfSize:14];
            [headerView addSubview:lblTitle];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth - 25, 0, 10, 10)];
            imageView.centerY = lblTitle.centerY;
            imageView.image = [UIImage imageNamed:@"Chevron_ic"];
            [headerView addSubview:imageView];
            
            UILabel *lblSeeAll = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth - 85, 0, 60, 40)];
            lblSeeAll.centerY = lblTitle.centerY;
            lblSeeAll.text = @"See All";
            lblSeeAll.textColor = [UIColor color999999];
            lblSeeAll.font = [UIFont systemFontOfSize:14];
            [headerView addSubview:lblSeeAll];
            
            return headerView;
            
        }else{
            
            return nil;
        }
        
    }else if (section == 1){
        if (restaurantArr.count == 0 || dataArray.count == 0) {
            return nil;
            
        }else{
            
            UIView *headerView = [[UIView alloc ]initWithFrame:CGRectMake(0, 0, DeviceWidth, 5)];
            headerView.backgroundColor = [UIColor colorE4E4E4];
            
            return headerView;
        }
        
        
    }
    return nil;
}

- (void)SeeAllClick:(UIButton *)SeeAll{
    NSLog(@"see all");
    
    [IMGAmplitudeUtil trackJournalListWithName:@"RC - View Journal list" andSorting:nil andFilter:nil andOrigin:@"SRP"];
    
    V2_DiscoverListJournalSeeAllViewController *V2_DiscoverListJournalSeeAllVC = [[V2_DiscoverListJournalSeeAllViewController alloc] init];
    if (self.keywordString != nil) {
        V2_DiscoverListJournalSeeAllVC.keyword = self.keywordString;
    }else if (self.placeholderString != nil){
        V2_DiscoverListJournalSeeAllVC.keyword = self.placeholderString;
    }else{
        V2_DiscoverListJournalSeeAllVC.keyword = @"";
    }
    
    [self.navigationController pushViewController:V2_DiscoverListJournalSeeAllVC animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //    if (tableView == self.tableView) {
    if (indexPath.section == 0) {
        if (dataArray.count >0) {
            
            return 195;
        }else{
            return 0;
        }
        
    }else if (indexPath.section == 1){
        IMGRestaurant *model =  restaurantArr[indexPath.row];
        return [self.tableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[V2_DiscoverListResultRestaurantTableViewCell class] contentViewWidth:DeviceWidth];
        
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //    if (tableView == self.tableView) {
    if (indexPath.section == 0) {
        if (dataArray.count >0) {
            
            NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
            V2_DiscoverListResultTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (!cell) {
                cell = [[V2_DiscoverListResultTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            cell.Delegate = self;
            [cell createUIWithModel:_V2_DiscoverListResultRestaurantModel];
            return cell;
            
        }else{
            
            return nil;
        }
        
    }else if (indexPath.section == 1){
        
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        V2_DiscoverListResultRestaurantTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[V2_DiscoverListResultRestaurantTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellStyleDefault;
            cell.Delegate = self;
        }
        cell.model = restaurantArr[indexPath.row];
        return cell;
        
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //    if (tableView == self.tableView) {
    if (indexPath.section == 0) {
        
    }else if (indexPath.section == 1){
        IMGRestaurant *model = [restaurantArr objectAtIndex:indexPath.row];
        NSLog(@"%@",model.restaurantId);
        
        DetailViewController *DetailVC = [[DetailViewController alloc] initWithRestaurant:model];
        
        [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Page" andRestaurantId:model.restaurantId andRestaurantTitle:model.title andLocation:nil andOrigin:@"Restaurant search result page" andType:nil];
        [self.navigationController pushViewController:DetailVC animated:YES];
        
    }
    
}
#pragma mark - delegate
- (void)delegateSellegateIndexpath:(NSNumber *)MyID{
    
    [IMGAmplitudeUtil trackJDPWithName:@"RC - View Journal detail" andJournalId:MyID andOrigin:@"SRP" andChannel:nil andLocation:nil];
    NSLog(@"被点击了%@",MyID);
    IMGMediaComment *journal = [[IMGMediaComment alloc] init];
    journal.mediaCommentId = MyID;
    JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
    jdvc.journal = journal;
    
    [[Amplitude instance] logEvent:@"RC – View Journal detail" withEventProperties:@{@"Journal_ID":[NSString stringWithFormat:@"%@",MyID],@"Origin":@"Restaurant search result page"}];
    [self.navigationController pushViewController:jdvc animated:YES];
    
}

- (void)delegateLikeBtn:(UITableViewCell *)cell andRestaurant:(IMGRestaurant *)restaurant{
    
    currentCell = (V2_DiscoverListResultRestaurantTableViewCell *)cell;
    [CommonMethod doSaveWithRestaurant:restaurant andViewController:self andCallBackBlock:^(BOOL isSaved) {
        [currentCell refreshSavedButton:isSaved];
    }];
}

#pragma mark -  RecentSearchesTableViewCellDelegate

- (void)delegateSelecteIndexpath:(NSString *)title{
    
    NSLog(@"被点击了%@",title);
    V2_DiscoverListResultViewController *V2_DiscoverListResultVC = [[V2_DiscoverListResultViewController alloc] init];
    V2_DiscoverListResultVC.keywordString = title;
    [self.navigationController pushViewController:V2_DiscoverListResultVC animated:YES];
    
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    
    V2_DiscoverListResultViewController *V2_DiscoverListResultVC = [[V2_DiscoverListResultViewController alloc] init];
    V2_DiscoverListResultVC.keywordString = _locationSearchBar.text;
    [self.navigationController pushViewController:V2_DiscoverListResultVC animated:YES];
    
}

- (BOOL) isEmpty:(NSString *) str {
    
    if (!str) {
        return true;
    } else {
        //A character set containing only the whitespace characters space (U+0020) and tab (U+0009) and the newline and nextline characters (U+000A–U+000D, U+0085).
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        
        //Returns a new string made by removing from both ends of the receiver characters contained in a given character set.
        NSString *trimedString = [str stringByTrimmingCharactersInSet:set];
        
        if ([trimedString length] == 0) {
            return true;
        } else {
            return false;
        }
    }
}

#pragma  makr - 存储搜索内容
- (void)saveData:(NSDictionary *)dict{
    
    
    [V2_DiscoverListHandler setRecentKeywords:dict userID:@"" andSuccessBlock:^(NSMutableArray *array) {
        
        
    } anderrorBlock:^{
        
        
    }];
}

-(NSString *)convertToJsonData:(NSDictionary *)dict

{
    
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *jsonString;
    
    if (!jsonData) {
        
        NSLog(@"%@",error);
        
    }else{
        
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        
    }
    
    NSMutableString *mutStr = [NSMutableString stringWithString:jsonString];
    
    NSRange range = {0,jsonString.length};
    
    //去掉字符串中的空格
    
    [mutStr replaceOccurrencesOfString:@" " withString:@"" options:NSLiteralSearch range:range];
    
    NSRange range2 = {0,mutStr.length};
    
    //去掉字符串中的换行符
    
    [mutStr replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:range2];
    
    return mutStr;
    
}


-(void)search:(NSNotification*)notification
{
    NSLog(@"=====%@",notification.userInfo);
    notificationcuisines = notification.userInfo[@"cuisines"];
    notificationfeature  = notification.userInfo[@"feature"];
    notificationprice = notification.userInfo[@"price"];
    notificationrating = notification.userInfo[@"rating"];
    notificationsortby = notification.userInfo[@"sortby"];
    filterType = [notification.userInfo[@"availablity"] integerValue];
    
    offset=0;
    isSearchThisArea = NO;
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setValue:notificationcuisines forKey:@"Cuisine"];
    [param setValue:notificationprice forKey:@"Price"];
    [param setValue:notificationfeature forKey:@"Features"];
    [param setValue:notificationrating forKey:@"Rating"];
    [param setValue:notificationsortby?notificationsortby:@"Popularity" forKey:@"Sorting"];
    [[Amplitude instance] logEvent:@"SC – Filter Search Succeed" withEventProperties:param];
    NSLog(@"%@==%@==%@==%@==%@",notificationcuisines,notificationfeature,notificationprice,notificationrating,notificationsortby);
    [self requestGetRestaurant:nil cuisines:notification.userInfo[@"cuisines"] feature:notification.userInfo[@"feature"] price:notification.userInfo[@"price"] rating:notification.userInfo[@"rating"] sortby:notification.userInfo[@"sortby"] loadMore:NO];
}

-(void)loadEmptyTableVIewHeadView{
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    if ( (discover.keyword != nil) && (![@"" isEqualToString:discover.keyword] ) ) {
        [eventProperties setValue:discover.keyword forKey:@"keyword"];
    }
    [[Amplitude instance] logEvent:@"RC - View Empty Result on Search" withEventProperties:eventProperties];
    headerView = [[SearchEmptyResultView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-_locationSearchBar.bottom)];
    headerView.delegate = self;
    headerView.hidden = YES;
    //    [searchResultsTableView setTableHeaderView:headerView];
    [self.view addSubview:headerView];
    //    [self initDiscovers];
    
}
- (void)gotoSuggest
{
    NSLog(@"gotoSuggest");
    RestaurantSuggestViewController *rsvc = [[RestaurantSuggestViewController alloc] init];
    rsvc.amplitudeType = @"Restaurant search result page";
    [self.navigationController pushViewController:rsvc animated:YES];
}

- (void)getOffers{
    [SearchUtil getOffers:^(NSString *offs, NSString *offers) {
        offsStr = offs;
        offersStr = offers;
    } anderrorBlock:^{
        offsStr = @"";
        offersStr = @"";
    }];
}



@end
