//
//  FirstViewController.h
//  Qraved
//
//  Created by Libo Liu on 14-4-18.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EScrollerView.h"
@interface FirstViewController : UIViewController<EScrollerViewDelegate>
{
    EScrollerView *_scorllView ;
    UIPageControl *_pageControl;
    NSMutableArray *_images;
}
@property(nonatomic,retain) UIImageView *bgImage;
@end
