//
//  V2_LikeCommentShareView.h
//  Qraved
//
//  Created by harry on 2017/6/27.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICommunityButton.h"

@protocol V2_LikeCommentShareViewDelegete <NSObject>

- (void)likeCommentShareView:(UIView*)likeCommentShareView commentButtonTapped:(UIButton*)button;
- (void)likeCommentShareView:(UIView*)likeCommentShareView shareButtonTapped:(UIButton*)button;
- (void)likeCommentShareView:(UIView*)likeCommentShareView likeButtonTapped:(UIButton*)button;

@end

@interface V2_LikeCommentShareView : UIView<CommunityButtonTappedDelegate>

@property (nonatomic, assign) BOOL isFromPhotoViewer;
- (void)setCommentCount:(int)commentCount likeCount:(int)likeCount liked:(BOOL)isLike;

@property (nonatomic, assign) id<V2_LikeCommentShareViewDelegete>delegate;


@end

