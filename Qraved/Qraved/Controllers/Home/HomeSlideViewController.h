//
//  HomeSlideViewController.h
//  Qraved
//
//  Created by apple on 16/10/19.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <CoreLocation/CoreLocation.h>
typedef void (^locationHandler)(CLLocation *currentLocation);
@interface HomeSlideViewController : BaseViewController
@property(nonatomic,strong)UIToolbar *toolBar;
-(void)setCurrentTabIndex:(NSUInteger)index;
@property(nonatomic,assign)BOOL isBocomeActive;

@end
