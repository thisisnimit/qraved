//
//  V2_RelatedJournalView.m
//  Qraved
//
//  Created by harry on 2017/7/7.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_RelatedJournalView.h"
#import "IMGMediaComment.h"
@interface V2_RelatedJournalView ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *relatedTableView;
}
@end

@implementation V2_RelatedJournalView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    relatedTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 75) style:UITableViewStylePlain];
    relatedTableView.delegate = self;
    relatedTableView.dataSource = self;
    relatedTableView.scrollEnabled = NO;
    relatedTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:relatedTableView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.relatedArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *idenStr = @"relatedCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    IMGMediaComment *journal = [self.relatedArray objectAtIndex:indexPath.row];
    
    UIImageView *relatedImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 92, 69)];
    [cell.contentView addSubview:relatedImageView];
    
    journal.imageUrl = [journal.imageUrl stringByReplacingPercentEscapesUsingEncoding:
                        NSUTF8StringEncoding];
    CGFloat imageWidth = 92;
    CGFloat imageHeigh = 69;
    if ([journal.imageUrl hasPrefix:@"http://"]||[journal.imageUrl hasPrefix:@"https://"])
    {
        [relatedImageView sd_setImageWithURL:[NSURL URLWithString:journal.imageUrl] placeholderImage:[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(imageWidth, imageHeigh)] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
    }
    else
    {
        NSString *urlStr = [journal.imageUrl returnFullImageUrlWithWidth:imageWidth andHeight:imageHeigh];
        [relatedImageView sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(imageWidth, imageHeigh)] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
    }

    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(102, 10, DeviceWidth-102-15, 59)];
    lblTitle.font = [UIFont systemFontOfSize:14];
    lblTitle.textColor = [UIColor color333333];
    lblTitle.numberOfLines = 3;
    [cell.contentView addSubview:lblTitle];
    lblTitle.text = journal.title;
    [lblTitle sizeToFit];
    
    UIView *spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, 69, DeviceWidth, 6)];
    spaceView.backgroundColor = [UIColor colorEBEBEB];
    [cell.contentView addSubview:spaceView];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    IMGMediaComment *journal = [self.relatedArray objectAtIndex:indexPath.row];
    if (self.gotoJournalDetail) {
        self.gotoJournalDetail(journal);
    }
}

- (void)setRelatedArray:(NSArray *)relatedArray{
    _relatedArray = relatedArray;
    
    relatedTableView.frame = CGRectMake(0, 0, DeviceWidth, 75*relatedArray.count);
    self.relatedHeight = 75*relatedArray.count;
    [relatedTableView reloadData];
    
}

@end
