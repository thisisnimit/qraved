//
//  PhotosImageListViewController.m
//  Qraved
//
//  Created by DeliveLee on 16/3/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "AlbumViewController.h"
#import "EScrollerView.h"
#import "PhotosEScrollerView.h"

#import "UIDevice+Util.h"
#import "NSString+Helper.h"
#import "UIViewController+Helper.h"
#import "Label.h"
#import "SearchPhotosViewController.h"
#import "Date.h"
#import "EntityKind.h"

#import "IMGUser.h"
#import "LoadingView.h"
#import "RestaurantHandler.h"
#import "UploadPhotoHandler.h"

#import "popReviewView.h"

#import "RestaurantActivityItemProvider.h"
#import "UplodePhotoActivityItemProvider.h"
#import "RestaurantEventActivityItemProvider.h"
#import "TwitterActivityItemProvider.h"
#import "IMGReviewComment.h"
#import "ReviewHandler.h"
#import "OtherProfileViewController.h"
#import "DetailViewController.h"
#import "UIButton+WebCache.h"
#import "IMGEvent.h"
#import "UIImageView+WebCache.h"
#import "MenuPhotosEScrollerView.h"
#import "HomeUtil.h"
#import "LikeView.h"
#import "V2_LikeCommentShareView.h"

#import "V2_InstagramBottomView.h"
#import "WebServiceV2.h"
#import "LoadingView.h"
#import "V2_PhotoDetailOpenInstagramViewController.h"

@interface AlbumViewController ()<AlbumActionButtonsDelegate, PopReviewViewDelegate,LikeListViewDelegate,AutoPostDelegate,V2_LikeCommentShareViewDelegete>
{
    NSMutableArray *photosArray;
    long photoTag;
    Label *numLabel;
    Label *userLabel;
    UIButton *avatarLinkButton;
    Label *createLabel;
    //Label *nameLabel;
    BOOL isFromDetail;
    
    IMGType imgType;
    UIView *infoAndLikeAndCommentView;
    UILabel *lblInfo;
    IMGEntity *currentDish;
    IMGRestaurant *currentRestaurant;
    //    IMGReview *review;
    PopReviewView *popReviewView;
    UILabel *reviewAndPhotoCountLabel;
    NSMutableDictionary *photoArrayStatus;
    NSNumber *offset;
    MenuPhotosEScrollerView *escrollView;
    BOOL isHiddenInfo;
//    UILabel *restaurantTitle;
    UIButton *closeButton;
    NSNumber *dishCount;
    NSNumber *restaurantEventCount;
    NSMutableArray* likelistArr;
    LikeView* likepopView;
//    BOOL currentISRestaurantPhoto;
    long validPhotosArrayCount;
    V2_LikeCommentShareView *likeCommentShareView;
    V2_InstagramBottomView *instagramBottomView;
    IMGRestaurant *savedRestaurant;
    UIScrollView *scroll;
    int lastContentOffset;
    FunctionButton *restaurantGoFoodButton;
    FunctionButton *openInstagramButton;
    FunctionButton *instagramGoFoodButton;
}
@end

@implementation AlbumViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithPhotosArray:(NSArray *)photos andPhotoTag:(NSInteger)tag andRestaurant:(IMGRestaurant *)restaurant andFromDetail:(BOOL)isDetail
{
    self=[super init];
    if(self)
    {
        offset = [NSNumber numberWithLong:0];
        photoArrayStatus = [[NSMutableDictionary alloc] init];
        imgType = TypeForUnknown;
        photosArray = [photos mutableCopy];
        photoTag = tag;
        isFromDetail = isDetail;
        currentRestaurant = restaurant;
        self.showMenu = NO;
        self.showPage = YES;
        imgType = [EntityKind typeKindFromEntity:[photosArray firstObject]];
    }
    
    return self;
}

-(void)updateRestautantDetailData:(NSMutableArray *)moreDishList {
    self.ifFromRestaurantDetail = YES;
    if (self.ifFromRestaurantDetail) {
        dishCount = currentRestaurant.disCount;
        restaurantEventCount = currentRestaurant.restaurantEventCount;
        int addNullCount = 0;
        validPhotosArrayCount = photosArray.count;
        if(imgType == TypeForIMGDish){
            
            if ( (moreDishList!=nil) && (moreDishList.count!=0) ) {
                validPhotosArrayCount += moreDishList.count;
                offset = [NSNumber numberWithLong:10];
                [photosArray addObjectsFromArray:moreDishList];
            }
            
            addNullCount = [dishCount intValue] - (int)photosArray.count;
            if (addNullCount > 0) {
                IMGDish *nullDish= [[IMGDish alloc] init];
                nullDish.entityId = [NSNumber numberWithInt:-1];
                for (int i = 0; i < addNullCount; i++) {
                    [photosArray addObject:nullDish];
                }
            }
        }else if(imgType == TypeForIMGRestaurantEvent){
            addNullCount = [restaurantEventCount intValue]  - (int)photosArray.count;
            if (addNullCount > 0) {
                IMGRestaurantEvent *nullEvent= [[IMGRestaurantEvent alloc] init];
                nullEvent.entityId = [NSNumber numberWithInt:-1];
                for (int i = 0; i < addNullCount; i++) {
                    [photosArray addObject:nullEvent];
                }
            }
        }
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    if(self.showMenu){
        self.navigationController.navigationBarHidden =  NO;
    }else{
        self.navigationController.navigationBarHidden =  YES;
        self.view.backgroundColor=[UIColor color333333];
    }
    if (self.amplitudeTitle!=nil) {
        NSMutableDictionary* eventDic=[[NSMutableDictionary alloc]init];
        [eventDic setValue:self.journalId forKey:@"Journal_ID"];
        [[Amplitude instance] logEvent:self.amplitudeTitle withEventProperties:eventDic];
    }
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden =  YES;
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if(self.showMenu){
        self.navigationController.navigationBarHidden =  NO;
    }else{
        self.navigationController.navigationBarHidden =  YES;
        self.view.backgroundColor=[UIColor color333333];
    }
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden =  NO;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden =  YES;
    if (self.isJournal)
    {
        self.screenName = @"Journal photo page";
    }
    else
    {
        self.screenName = @"Restaurant Photo detail page";
    }

    if(self.useNew){
        PhotosEScrollerView *scrollView = [[PhotosEScrollerView alloc] initWithFrameRect:CGRectMake(0, 34, DeviceWidth, DeviceHeight-34) objectArray:photosArray isStartScroll:NO withPhotoTag:photoTag pageControlCenter:YES isCirculation:YES isPageControl:self.showPage isEvent:NO isPlaceHolder:NO sizeFitWidth:NO];
        [self.view addSubview:scrollView];
        scrollView.delegate = self;
    }else{
        escrollView = [[MenuPhotosEScrollerView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+20) imagesArray:photosArray index:photoTag isMenuPhotoView:NO];
        if (@available(iOS 11.0, *)){
            escrollView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0);
        }
        
        [self.view addSubview:escrollView];
        escrollView.escrollerViewDelegate = self;
        escrollView.ifCanNotFromBeginToEnd = self.ifFromRestaurantDetail;
        escrollView.isUseWebP = self.isUseWebP;

    }
    UIPanGestureRecognizer *panTap = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    
    [self.view addGestureRecognizer:panTap];
    
    if(self.showMenu){
        [self addNavigationBar];
    }
    else
    {
        closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        closeButton.frame = CGRectMake(DeviceWidth-55, IMG_StatusBarHeight+TOPOFFSET, 40, 40);
        closeButton.imageEdgeInsets = UIEdgeInsetsMake(0, 18, 18, 0);
        [closeButton setImage:[UIImage imageNamed:@"ic_close_photo"] forState:UIControlStateNormal];
        [self.view addSubview:closeButton];
        [closeButton addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
        
    }
    [self showPhotoNumAndName];
}

- (void)handlePan:(UIPanGestureRecognizer *)pan{
    CGPoint pt = [pan translationInView:self.view];
    if (pt.y < 0) {
        scroll.contentOffset = CGPointMake(0,  -pt.y);
        //            lastContentOffset = 0;
        NSLog(@"%f======",escrollView.contentOffset.y);
        if (scroll.contentOffset.y > 10) {
            if (pan.state == UIGestureRecognizerStateEnded) {
                if (@available(iOS 11.0, *)) {
                    [UIView animateWithDuration:0.5 animations:^{
                        scroll.contentOffset = CGPointMake(0,  DeviceHeight + 20 +20);
                    }];
                }else{
                    [UIView animateWithDuration:0.5 animations:^{
                        scroll.contentOffset = CGPointMake(0,  DeviceHeight + 20);
                    }];
                }
            }
        }
    }

}
-(void)addNavigationBar
{
    self.view.backgroundColor=[UIColor blackColor];
    [self setBackBarButtonOffset30];
    
    self.navigationItem.title=nil;
    
    if(currentRestaurant && isFromDetail){
        UIBarButtonItem *menuBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"MenuTop"] style:UIBarButtonItemStylePlain target:self action:@selector(menuClick)];
        menuBtn.tintColor=[UIColor whiteColor];
        self.navigationItem.rightBarButtonItem=menuBtn;
        
    }
}
-(void)backClick{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)menuClick{
    if (isFromDetail){
        SearchPhotosViewController *searchPhotosViewController=[[SearchPhotosViewController alloc]initWithRestaurant:currentRestaurant restaurantPhotoArray:photosArray];
        [self.navigationController pushViewController:searchPhotosViewController animated:YES];
    }
    else
        [self.navigationController popViewControllerAnimated:YES];
}

-(void)showPhotoNumAndName
{
    
    CGFloat margin_top=10;
    userLabel=[[Label alloc] initWithFrame:CGRectMake(LEFTLEFTSET,margin_top,DeviceWidth-LEFTLEFTSET-100,18)];
    userLabel.font=[UIFont systemFontOfSize:16];
    userLabel.textColor=[UIColor whiteColor];
    createLabel=[[Label alloc] initWithFrame:CGRectMake(LEFTLEFTSET,margin_top+20,DeviceWidth-LEFTLEFTSET-100,14)];
    createLabel.font=[UIFont systemFontOfSize:12];
    createLabel.textColor=[UIColor whiteColor];
    [self.view addSubview:userLabel];
    [self.view addSubview:createLabel];
    
    avatarLinkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    avatarLinkButton.frame = CGRectMake(LEFTLEFTSET, IMG_StatusBarHeight+TOPOFFSET, 40, 40);
    avatarLinkButton.clipsToBounds = YES;
    avatarLinkButton.hidden = YES;
    avatarLinkButton.layer.cornerRadius = 20;
    [self.view addSubview:avatarLinkButton];
    
    infoAndLikeAndCommentView  = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, MAXFLOAT)];
    infoAndLikeAndCommentView.backgroundColor = [UIColor clearColor];
    lblInfo = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, MAXFLOAT)];
    lblInfo.font = [UIFont systemFontOfSize:12];
    lblInfo.numberOfLines = 0;
    lblInfo.textColor = [UIColor whiteColor];
    
    
    [infoAndLikeAndCommentView addSubview:lblInfo];
    [self.view addSubview:infoAndLikeAndCommentView];
    
    likeCommentShareView = [[V2_LikeCommentShareView alloc] initWithFrame:CGRectMake(0, DeviceHeight+20 - 50, DeviceWidth, 50)];
    likeCommentShareView.isFromPhotoViewer = YES;
    likeCommentShareView.delegate = self;
    [self.view addSubview:likeCommentShareView];
    
    UIButton *btnLike = [UIButton buttonWithType:UIButtonTypeCustom];
    btnLike.frame = CGRectMake(DeviceWidth/3 - 60, 0, 60, 50);
    [btnLike addTarget:self action:@selector(likeCountClick:) forControlEvents:UIControlEventTouchUpInside];
    [likeCommentShareView addSubview:btnLike];
    
    [self createGoFoodButton];
    [self createInstagramButton];
    if (currentRestaurant != nil && currentRestaurant.restaurantId != nil) {
        [self createInstagramView];
    }
    
    [self updateUIWhenScrollAndLoad:imgType andDish:(IMGEntity *)[photosArray objectAtIndex:photoTag]];
    [self EScrollerViewDidClicked:photoTag+1 ContentOffset:CGPointMake(0, 0)];
}

- (void)likeCountClick:(UIButton *)button{
    [[LoadingView sharedLoadingView]startLoading];
    
    if (likepopView) {
        [likepopView removeFromSuperview];
    }
    
    if (imgType == TypeForIMGDish) {
        
        
        [HomeUtil getLikeListFromServiceWithDishId:self.dishId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray, NSNumber *likeviewcount) {
            
            [[LoadingView sharedLoadingView]stopLoading];
            
            
            likelistArr=[NSMutableArray arrayWithArray:likeListArray];
            
            likepopView.likelistArray=likelistArr;
            likepopView.likecount=likeviewcount;
            [[LoadingView sharedLoadingView]stopLoading];
            
        }];
    }else if (imgType==TypeForIMGRestaurantEvent)
    {
        [HomeUtil getLikeListFromServiceWithResturantId:self.restaurantEventId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
            [[LoadingView sharedLoadingView]stopLoading];
            
            
            likelistArr=[NSMutableArray arrayWithArray:likeListArray];
            likepopView.likecount=likeviewcount;
            likepopView.likelistArray=likelistArr;
            [[LoadingView sharedLoadingView]stopLoading];
            
        }];
        
    }
    likepopView=[[LikeView alloc]initWithFrame:CGRectMake(0, 60, DeviceWidth, DeviceHeight)];
    likepopView.delegate=self;
    [self.view addSubview:likepopView];
}
-(void)likeNumTap:(UITapGestureRecognizer*)tap
{
    [[LoadingView sharedLoadingView]startLoading];
    
    if (likepopView) {
        [likepopView removeFromSuperview];
    }
    
    if (imgType == TypeForIMGDish) {
        
        
        [HomeUtil getLikeListFromServiceWithDishId:self.dishId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray, NSNumber *likeviewcount) {
            
            [[LoadingView sharedLoadingView]stopLoading];
            
            
            likelistArr=[NSMutableArray arrayWithArray:likeListArray];
            
            likepopView.likelistArray=likelistArr;
            likepopView.likecount=likeviewcount;
            [[LoadingView sharedLoadingView]stopLoading];
            
        }];
    }else if (imgType==TypeForIMGRestaurantEvent)
    {
        [HomeUtil getLikeListFromServiceWithResturantId:self.restaurantEventId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
            [[LoadingView sharedLoadingView]stopLoading];
            
            
            likelistArr=[NSMutableArray arrayWithArray:likeListArray];
            likepopView.likecount=likeviewcount;
            likepopView.likelistArray=likelistArr;
            [[LoadingView sharedLoadingView]stopLoading];
            
        }];
        
    }
    likepopView=[[LikeView alloc]initWithFrame:CGRectMake(0, 60, DeviceWidth, DeviceHeight)];
    likepopView.delegate=self;
    [self.view addSubview:likepopView];
}
-(void)LikeviewLoadMoreData:(NSInteger)offset_
{
    
    if (imgType == TypeForIMGDish) {
        
        
        [HomeUtil getLikeListFromServiceWithDishId:self.dishId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset_] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
            [likelistArr addObjectsFromArray:likeListArray];
            likepopView.likecount=likeviewcount;
            [likepopView.likeUserTableView reloadData];
            
            
        }];
    }else if (imgType ==TypeForIMGRestaurantEvent){
        
        
        [HomeUtil getLikeListFromServiceWithResturantId:self.restaurantEventId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset_] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
            likepopView.likecount=likeviewcount;
            [likelistArr addObjectsFromArray:likeListArray];
            
            [likepopView.likeUserTableView reloadData];
            
        }];
    }
}
-(void)returnLikeCount:(NSInteger)count
{
    
}

-(void)updateCommentCount:(IMGType)dishType andDish:(IMGEntity *)thisDish{
    currentDish = thisDish;
}

-(void)updateUIWhenScrollAndLoad:(IMGType)dishType andDish:(IMGEntity *)thisDish{
    
//    if (likeCommentShareView) {
//        [likeCommentShareView removeFromSuperview];
//    }
//    if (instagramBottomView) {
//        [instagramBottomView removeFromSuperview];
//    }
    float leftGap = 15;
    float width = DeviceWidth - leftGap*2;
    CGRect defaultFrame = CGRectMake(leftGap, 0, width, MAXFLOAT);
    
    infoAndLikeAndCommentView.frame = defaultFrame;
    lblInfo.frame = defaultFrame;
    lblInfo.text = @"";

    
    
    currentDish = thisDish;
    restaurantGoFoodButton.hidden = YES;
    openInstagramButton.hidden = YES;
    instagramGoFoodButton.hidden = YES;
    [likeCommentShareView setCommentCount:[[currentDish valueForKey:@"commentCount"] intValue] likeCount:[[currentDish valueForKey:@"likeCount"] intValue] liked:[[currentDish valueForKey:@"isLike"] boolValue]];
    
    if(imgType == TypeForIMGDish && ![currentDish valueForKey:@"dishId"]){
        likeCommentShareView.hidden = YES;
    }
    if ([currentDish isKindOfClass:[IMGDish class]]) {
        
        if ([((IMGDish*)currentDish).type intValue]==2) {
            likeCommentShareView.hidden = YES;
            openInstagramButton.hidden = NO;
            scroll.hidden = NO;
            
            if (![Tools isBlankString:currentRestaurant.goFoodLink]) {
                instagramGoFoodButton.hidden = NO;
                openInstagramButton.frame = CGRectMake(15, DeviceHEIGHT- 89 - FUNCTION_BUTTON_HEIGHT, (DeviceWidth - 40)/2, FUNCTION_BUTTON_HEIGHT);
            }else{
                instagramGoFoodButton.hidden = YES;
                openInstagramButton.frame = CGRectMake(15, DeviceHEIGHT- 89 - FUNCTION_BUTTON_HEIGHT, DeviceWidth-30, FUNCTION_BUTTON_HEIGHT);
            }
        }else{
            scroll.hidden = YES;
            if (![Tools isBlankString:currentRestaurant.goFoodLink]) {
                restaurantGoFoodButton.hidden = NO;
                if (!((IMGDish*)currentDish).isRestaurantPhoto) {
                    restaurantGoFoodButton.frame = CGRectMake(LEFTLEFTSET, DeviceHEIGHT - 110, DeviceWidth - LEFTLEFTSET * 2, FUNCTION_BUTTON_HEIGHT);
                }else{
                    restaurantGoFoodButton.frame = CGRectMake(LEFTLEFTSET, DeviceHEIGHT - FUNCTION_BUTTON_HEIGHT - 15, DeviceWidth - LEFTLEFTSET * 2, FUNCTION_BUTTON_HEIGHT);;
                }
            }
        }
    }
    
    //lblInfo.text = [currentDish valueForKey:@"descriptionStr"];
    [lblInfo sizeToFit];

    lblInfo.frame = CGRectMake(0, 0, lblInfo.frame.size.width, lblInfo.frame.size.height);
    
    float infoViewHeight = MAX(lblInfo.endPointY, lblInfo.endPointY)+10;
    infoAndLikeAndCommentView.frame = CGRectMake(leftGap, DeviceHeight+20-90-infoViewHeight, DeviceWidth, infoViewHeight);
}

- (void)createInstagramView{
    scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, DeviceHeight+20-79, DeviceWidth, DeviceHeight+99)];
    scroll.layer.zPosition = 10000;
    scroll.delegate = self;
    [self.view addSubview:scroll];
    
    instagramBottomView = [[V2_InstagramBottomView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 79+DeviceHeight+20)];
    __weak typeof(scroll) weakScroll = scroll;
    instagramBottomView.closeMiniRDP = ^(){
        
        [UIView animateWithDuration:0.5 animations:^{
            
            weakScroll.contentOffset = CGPointMake(0, -20);
        }];
    };
    __weak typeof(self) weakSelf = self;
    instagramBottomView.gotoRDP = ^(){
        [weakSelf gotoRDP];
    };
    instagramBottomView.controller = self.navigationController;
    [scroll addSubview:instagramBottomView];
    [self requestData:currentRestaurant.restaurantId];
}

- (void)createInstagramButton{
    openInstagramButton = [FunctionButton buttonWithType:UIButtonTypeCustom andTitle:@"Open in Instagram" andBGColor:[UIColor clearColor] andTitleColor:[UIColor whiteColor]];
    openInstagramButton.frame = CGRectMake(15, DeviceHEIGHT- 89 - FUNCTION_BUTTON_HEIGHT, (DeviceWidth - 40)/2, FUNCTION_BUTTON_HEIGHT);
    openInstagramButton.layer.masksToBounds = YES;
    openInstagramButton.layer.borderWidth = 1;
    openInstagramButton.layer.borderColor = [UIColor whiteColor].CGColor;
    [self.view addSubview:openInstagramButton];
    [openInstagramButton addTarget:self action:@selector(openInstagramTapped) forControlEvents:UIControlEventTouchUpInside];
    openInstagramButton.hidden = YES;
    
    instagramGoFoodButton = [FunctionButton buttonWithType:UIButtonTypeCustom];
    instagramGoFoodButton.frame = CGRectMake(openInstagramButton.endPointX + 10, openInstagramButton.startPointY, (DeviceWidth - 40)/2, FUNCTION_BUTTON_HEIGHT);
    [instagramGoFoodButton addTarget:self action:@selector(goFoodTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:instagramGoFoodButton];
    instagramGoFoodButton.hidden = YES;
}

- (void)openInstagramTapped{
    if (![Tools isBlankString:((IMGDish*)currentDish).instagramLink]) {
        V2_PhotoDetailOpenInstagramViewController *V2_PhotoDetailOpenInstagramV = [[V2_PhotoDetailOpenInstagramViewController alloc] init];
        V2_PhotoDetailOpenInstagramV.instagram_link = ((IMGDish*)currentDish).instagramLink;
        [self.navigationController pushViewController:V2_PhotoDetailOpenInstagramV animated:YES];
    }
}

- (void)createGoFoodButton{
    restaurantGoFoodButton = [FunctionButton buttonWithType:UIButtonTypeCustom];
    restaurantGoFoodButton.frame = CGRectMake(LEFTLEFTSET, DeviceHEIGHT - 110, DeviceWidth - LEFTLEFTSET * 2, FUNCTION_BUTTON_HEIGHT);
    [restaurantGoFoodButton addTarget:self action:@selector(goFoodTapped) forControlEvents:UIControlEventTouchUpInside];
    restaurantGoFoodButton.hidden = YES;
    [self.view addSubview:restaurantGoFoodButton];
}

- (void)goFoodTapped{
    NSString *locationStr;
    if ([((IMGDish *)currentDish).type intValue] == 2) {
        locationStr = @"Instagram Photo Viewer";
    }else{
        locationStr = @"Restaurant Photo Viewer";
    }
    [IMGAmplitudeUtil trackGoFoodWithName:@"CL - Go-Food CTA" andJournalId:nil andRestaurantId:currentRestaurant.restaurantId andPhotoId:((IMGDish *)currentDish).dishId andLocation:locationStr andOrigin:nil andOrder:nil];

    [CommonMethod goFoodButtonTapped:currentRestaurant andViewController:self andRestaurantState:[currentRestaurant isOpenRestaurant] andCallBackBlock:^(BOOL isSaved) {
        
    }];
}

- (void)gotoRDP{
    DetailViewController *detailViewController = [[DetailViewController alloc] initWithRestaurantId:currentRestaurant.restaurantId];
    [self.navigationController pushViewController:detailViewController animated:YES];
}


//start

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    lastContentOffset = scrollView.contentOffset.y;
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSLog(@"%f",scrollView.contentOffset.y);
    
    scroll.frame = CGRectMake(0, DeviceHeight+20-79 - scrollView.contentOffset.y, DeviceWidth, scroll.size.height);
    instagramBottomView.frame = CGRectMake(0, scrollView.contentOffset.y, DeviceWidth, instagramBottomView.size.height);
    
    
}
- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    
    [scrollView setContentOffset:scrollView.contentOffset animated:NO];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    NSLog(@"====huadong====%f=======%d",scrollView.contentOffset.y, lastContentOffset);
    
    if (lastContentOffset == 0) {

        if (scrollView.contentOffset.y <  150) {
            [UIView animateWithDuration:0.5 animations:^{
                CGPoint offset1 = scroll.contentOffset;
                offset1.y = 0;
                scrollView.contentOffset = offset1;
                
            }];
            
        }
        
        
        if (scrollView.contentOffset.y < DeviceHeight +20 && scrollView.contentOffset.y > DeviceHeight +20 -150 ) {
            [UIView animateWithDuration:0.5 animations:^{
                
                CGPoint offset1 = scroll.contentOffset;
                offset1.y = 0;
                scrollView.contentOffset = offset1;
            }];
            
        }
        
        if (scrollView.contentOffset.y >  150 &&  scrollView.contentOffset.y < (DeviceHeight+20)/2 - 79) {
            [UIView animateWithDuration:0.5 animations:^{
                CGPoint offset1 = scroll.contentOffset;
                offset1.y = DeviceHeight+20;
                scrollView.contentOffset = offset1;
            }];
        }
        
        
        if (scrollView.contentOffset.y <  DeviceHeight -20 &&  scrollView.contentOffset.y > (DeviceHeight+20)/2 - 79) {
            [UIView animateWithDuration:0.5 animations:^{
                CGPoint offset1 = scroll.contentOffset;
                offset1.y = DeviceHeight+20;
                scrollView.contentOffset = offset1;
            }];
        }
        
    }
    if (lastContentOffset >0) {
        if (lastContentOffset > DeviceHeight +20) {
            [UIView animateWithDuration:0.5 animations:^{
                
            }];
            
            if (scrollView.contentOffset.y < DeviceHeight +20) {
                [UIView animateWithDuration:0.5 animations:^{
                    
                    CGPoint offset1 = scroll.contentOffset;
                    offset1.y = 0;
                    scrollView.contentOffset = offset1;
                }];
            }
        }else{
            if (scrollView.contentOffset.y < DeviceHeight +20) {
                [UIView animateWithDuration:0.5 animations:^{
                    
                    CGPoint offset1 = scroll.contentOffset;
                    offset1.y = 0;
                    scrollView.contentOffset = offset1;
                }];
                
                
            }
        }
    }
}

-(void)EScrollerViewDidClicked:(NSUInteger)index ContentOffset:(CGPoint)contentoffset{
    NSLog(@"index = %lu",(unsigned long)index);
    //    numLabel.text = [NSString stringWithFormat:@"%lu / %lu",(unsigned long)index,(unsigned long)photosArray.count];
//    if(userLabel!=nil) {
//        [userLabel removeFromSuperview];
//        userLabel = nil;
//    }
//    if(avatarLinkButton!=nil) {
//        [avatarLinkButton removeFromSuperview];
//        avatarLinkButton = nil;
//    }
    if(reviewAndPhotoCountLabel!=nil) {
        [reviewAndPhotoCountLabel removeFromSuperview];
        reviewAndPhotoCountLabel = nil;
    }
//    if(restaurantTitle!=nil) {
//        [restaurantTitle removeFromSuperview];
//        restaurantTitle = nil;
//    }
    createLabel.text=@"";
    if(imgType == TypeForIMGDish){
        
        if ( (![photoArrayStatus objectForKey:@(index)]) && self.ifFromRestaurantDetail) {
            if (index%5 == 0) {
                [photoArrayStatus setObject:@(YES) forKey:@(index)];
                
                if ((dishCount == nil)||(validPhotosArrayCount < [dishCount longValue])) {
                    offset = [NSNumber numberWithLong:([offset longValue]+10)];
                    [RestaurantHandler getRestaurantDishListFromService:self.restaurantId offset:offset andBlock:^(NSMutableArray *dishArray,NSNumber *dCount,NSNumber *offsetBlock) {
                        if (!dishArray.count) {
                            return ;
                        }
                        dishCount = dCount;
                        validPhotosArrayCount += dishArray.count;
                        for (long i = 0; i < dishArray.count; i++) {
                            [photosArray replaceObjectAtIndex:([offsetBlock intValue] + i) withObject:[dishArray objectAtIndex:i]];
                        }
                        [escrollView updateImagesArray:photosArray];
                    }];
                }
            }
        }
        
        IMGDish *dish=[photosArray objectAtIndex:index-1];
        if (self.amplitudeType!=nil) {
            NSString *origin;
            if ([self.amplitudeType isEqualToString:@"Restaurant detail page - "]) {
                origin = @"Restaurant detail page";
            }else if([self.amplitudeType isEqualToString:@"card detail page"]){
                origin = @"Photo card detail page";
                
            }else{
                origin = self.amplitudeType;
            }
            
            [IMGAmplitudeUtil trackPhotoWithName:@"RC - View Restaurant Photo detail" andRestaurantId:currentRestaurant.restaurantId andRestaurantTitle:nil andOrigin:origin andUploaderUser_ID:nil andPhoto_ID:dish.dishId andRestaurantMenu_ID:nil andRestaurantEvent_ID:nil andPhotoSource:nil andLocation:nil];
        }
        avatarLinkButton.hidden = YES;
        if (dish.entityId == [NSNumber numberWithInt:-1]) {
            createLabel.text=@"";
            likeCommentShareView.hidden = YES;
            infoAndLikeAndCommentView.hidden = YES;
            return;
        }else{
            likeCommentShareView.hidden = NO;
            infoAndLikeAndCommentView.hidden = NO;
        }
        self.dishId=dish.dishId;
        if(dish.title && dish.title.length){
            userLabel.text = dish.title;
        }else{
            
            if ([Tools isBlankString:dish.descriptionStr]) {
                userLabel.text = @"";
            }else{
                userLabel.text = dish.descriptionStr;
            }
        }
        if([NSString stringWithFormat:@"%@",dish.createTime].length<11){
            dish.createTime = [NSNumber numberWithLongLong:[dish.createTime longLongValue]*1000];
        }
        NSString *photoCredit = dish.photoCreditDic;
        if (photoCredit == nil) {
            photoCredit = dish.userName;
        }
        BOOL createTimeBool = [dish.createTime longLongValue]/1000;
        if (dish.isRestaurantPhoto) {
            createTimeBool = YES;
            likeCommentShareView.hidden = YES;
            infoAndLikeAndCommentView.hidden = YES;
        }else{
            likeCommentShareView.hidden = NO;
            infoAndLikeAndCommentView.hidden = NO;
        }
        
        if (self.isJournal && (dish.restaurantTitleDic != nil) && (![@"" isEqualToString:dish.restaurantTitleDic]) ) {
//            restaurantTitle=[[Label alloc] initWithFrame:CGRectMake(LEFTLEFTSET,20+TOPOFFSET,DeviceWidth-LEFTLEFTSET-80,50)];
            userLabel.frame = CGRectMake(LEFTLEFTSET,IMG_StatusBarHeight+TOPOFFSET,DeviceWidth-LEFTLEFTSET-80,50);
            userLabel.text = dish.restaurantTitleDic;
            userLabel.font=[UIFont systemFontOfSize:16];
            userLabel.textColor=[UIColor whiteColor];
            userLabel.tag = 100+index;
            userLabel.numberOfLines = 0;
            userLabel.text=[NSString stringWithFormat:@"​%@",dish.restaurantTitleDic];//'s photo
            NSString *userName = [NSString stringWithFormat:@"%@",dish.restaurantTitleDic];
            NSMutableAttributedString *userNameAttributeString = [[NSMutableAttributedString alloc] initWithString:userLabel.text];
            [userNameAttributeString setAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:16]} range:NSMakeRange(0, userName.length+1)];
            [userLabel setAttributedText:userNameAttributeString];
            [userLabel sizeToFit];
//            [self.view addSubview:userLabel];
            
            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoResDetailController:)];
            userLabel.userInteractionEnabled = YES;
            tap.numberOfTapsRequired=1;
            tap.numberOfTouchesRequired=1;
            [userLabel addGestureRecognizer:tap];
            
            createLabel.frame = CGRectMake(LEFTLEFTSET, userLabel.endPointY+10, createLabel.frame.size.width, createLabel.frame.size.height);
            
            if ((dish.createTime !=nil) && ([dish.createTime longLongValue]!=0)) {
                NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"MM/dd/yyyy"];// HH:mm:ss"
                createLabel.text = [Date getTimeInteval_v10:[dish.createTime longLongValue]/1000];
            }
        }
        
        if(photoCredit && ![photoCredit isBlankString] && createTimeBool){
            
//            userLabel=[[Label alloc] initWithFrame:CGRectMake(LEFTLEFTSET,20+TOPOFFSET,DeviceWidth-LEFTLEFTSET-80,50)];
            userLabel.frame = CGRectMake(LEFTLEFTSET,IMG_StatusBarHeight+TOPOFFSET,DeviceWidth-LEFTLEFTSET-80,50);
            userLabel.font=[UIFont systemFontOfSize:16];
            userLabel.textColor=[UIColor whiteColor];
            userLabel.tag = 100+index;
            userLabel.numberOfLines = 0;
            
            userLabel.text=[[NSString stringWithFormat:@"​%@",photoCredit] filterHtml];//'s photo
            if ([dish.type intValue]==2) {
                userLabel.text=[[NSString stringWithFormat:@"@​%@",photoCredit] filterHtml];//'s photo
                
            }
            
            NSString *userNameString = userLabel.text;
            NSMutableAttributedString *userNameAttributeString = [[NSMutableAttributedString alloc] initWithString:userLabel.text];
            [userNameAttributeString setAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:16]} range:NSMakeRange(0, userNameString.length)];
            [userLabel setAttributedText:userNameAttributeString];
            [userLabel sizeToFit];
//            [self.view addSubview:userLabel];
            
            createLabel.frame = CGRectMake(LEFTLEFTSET, userLabel.endPointY+10, createLabel.frame.size.width, createLabel.frame.size.height);
            if ([dish.type intValue]==2){
                UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(jumpToInstagram)];
                userLabel.userInteractionEnabled = YES;
                tap.numberOfTapsRequired=1;
                tap.numberOfTouchesRequired=1;
                [userLabel addGestureRecognizer:tap];
            }else{
                UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotToDishPage)];
                userLabel.userInteractionEnabled = YES;
                tap.numberOfTapsRequired=1;
                tap.numberOfTouchesRequired=1;
                [userLabel addGestureRecognizer:tap];
            }
            
            if ([dish.photoCreditTypeDic isEqualToString:@"User ID"]) {
                
                avatarLinkButton.hidden = NO;
//                avatarLinkButton = [UIButton buttonWithType:UIButtonTypeCustom];
                avatarLinkButton.frame = CGRectMake(LEFTLEFTSET, IMG_StatusBarHeight+TOPOFFSET, 40, 40);
                if ([dish.type intValue]!=2){
                    [avatarLinkButton addTarget:self action:@selector(gotoOtherProfileWithUser) forControlEvents:UIControlEventTouchUpInside];
                }
                
                if (![Tools isBlankString:dish.userAvatarDic]) {
                    [avatarLinkButton sd_setImageWithURL:[NSURL URLWithString:[dish.userAvatarDic returnCurrentImageString]] forState:UIControlStateNormal placeholderImage:[[UIImage imageNamed:@"headDefault.jpg"] cutCircleImage]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    }];
                }else{
                    [avatarLinkButton setImage:[UIImage imageNamed:@"headDefault.jpg"] forState:UIControlStateNormal];
                }
                
                
                long userReviewCount = [dish.userReviewCountDic longValue];
                long userPhotoCount = [dish.userPhotoCountDic longValue];
                
                userLabel.frame = CGRectMake(avatarLinkButton.endPointX+10, IMG_StatusBarHeight+TOPOFFSET, userLabel.frame.size.width, userLabel.frame.size.height);
                
                if ([dish.type intValue]==2){
                    createLabel.frame = CGRectMake(avatarLinkButton.endPointX+10, userLabel.endPointY+5, createLabel.frame.size.width, createLabel.frame.size.height);
                }else{
                    reviewAndPhotoCountLabel = [[UILabel alloc] init];
                    reviewAndPhotoCountLabel.font = [UIFont systemFontOfSize:13];
                    reviewAndPhotoCountLabel.textColor = [UIColor whiteColor];
                    if (userPhotoCount!=0&&userReviewCount!=0) {
                        
                        [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%ld %@ • %ld %@",userReviewCount,userReviewCount > 1 ? @"reviews":@"review",userPhotoCount,userPhotoCount > 1 ? @"photos":@"photo"]];
                        reviewAndPhotoCountLabel.frame = CGRectMake(userLabel.frame.origin.x, userLabel.endPointY,DeviceWidth-(avatarLinkButton.endPointX+10)-80,50);
                        [reviewAndPhotoCountLabel sizeToFit];
                        [self.view addSubview:reviewAndPhotoCountLabel];
                    }else if (userPhotoCount==0||userReviewCount==0){
                        if (userReviewCount==0&&userPhotoCount!=0) {
                            
                            [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%ld %@",userPhotoCount,userPhotoCount > 1 ? @"photos":@"photo"]];
                            reviewAndPhotoCountLabel.frame = CGRectMake(userLabel.frame.origin.x, userLabel.endPointY, DeviceWidth-(avatarLinkButton.endPointX+10)-80, 50);
                            
                            [reviewAndPhotoCountLabel sizeToFit];
                            [self.view addSubview:reviewAndPhotoCountLabel];
                            
                        }
                        if (userPhotoCount==0&&userReviewCount!=0) {
                            
                            [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%ld %@",userReviewCount,userReviewCount > 1 ? @"reviews":@"review"]];
                            reviewAndPhotoCountLabel.frame = CGRectMake(userLabel.frame.origin.x, userLabel.endPointY, DeviceWidth-(avatarLinkButton.endPointX+10)-80, 50);
                            [reviewAndPhotoCountLabel sizeToFit];
                            [self.view addSubview:reviewAndPhotoCountLabel];
                            
                            
                        }
                        
                    }
                    createLabel.frame = CGRectMake(LEFTLEFTSET, avatarLinkButton.endPointY+10, createLabel.frame.size.width, createLabel.frame.size.height);
                }
                
            }
            if ([dish.createTime longLongValue]!=0) {
                NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"MM/dd/yyyy"];//HH:mm:ss
                createLabel.text = [Date getTimeInteval_v10:[dish.createTime longLongValue]/1000];
            }
            
        }
        [self updateUIWhenScrollAndLoad:imgType andDish:dish];
    }else if(imgType == TypeForIMGRestaurantEvent){
        
        IMGRestaurantEvent *dish=[photosArray objectAtIndex:index-1];
        
        if (self.amplitudeType!=nil) {
            NSString *origin;
            if ([self.amplitudeType isEqualToString:@"Restaurant detail page - "]) {
                origin = @"Restaurant detail page - event list";
            }else if([self.amplitudeType isEqualToString:@"card detail page"]){
                origin = @"Restaurant event card detail page";
                
            }else{
                origin = self.amplitudeType;
            }

            NSString *photoSource;
            if ([@"User ID" isEqualToString:dish.photoCreditTypeDic]) {
                photoSource = @"User";
            }else if([@"Qraved Team" isEqualToString:dish.photoCreditTypeDic]){
                photoSource = @"Qraved Admin";
            }
            
            [IMGAmplitudeUtil trackPhotoWithName:@"RC - View Restaurant Event detail" andRestaurantId:dish.restaurantId andRestaurantTitle:nil andOrigin:origin andUploaderUser_ID:dish.userIdDic andPhoto_ID:nil andRestaurantMenu_ID:nil andRestaurantEvent_ID:dish.eventId andPhotoSource:photoSource andLocation:nil];
        }
        
        if ( (![photoArrayStatus objectForKey:@(index)]) && self.ifFromRestaurantDetail) {
            if (index%5 == 0) {
                [photoArrayStatus setObject:@(YES) forKey:@(index)];
                
                if ( (restaurantEventCount == nil) || (validPhotosArrayCount < [restaurantEventCount longValue]) ) {
                    offset = [NSNumber numberWithLong:([offset longValue]+10)];
                    [RestaurantHandler getRestaurantEventPhotoListFromService:self.restaurantId offset:offset andBlock:^(NSMutableArray *eventArray,NSNumber *eCount,NSNumber *offsetBlock) {
                        if (!eventArray.count) {
                            return ;
                        }
                        restaurantEventCount = eCount;
                        validPhotosArrayCount += eventArray.count;
                        for (long i = 0; i < eventArray.count; i++) {
                            [photosArray replaceObjectAtIndex:([offsetBlock intValue] + i) withObject:[eventArray objectAtIndex:i]];
                        }
                        [escrollView updateImagesArray:photosArray];
                    }];
                }
            }
        }
        
        
        if (dish.entityId == [NSNumber numberWithInt:-1]) {
            createLabel.text=@"";
            likeCommentShareView.hidden = YES;
            infoAndLikeAndCommentView.hidden = YES;
            return;
        }else{
            likeCommentShareView.hidden = NO;
            infoAndLikeAndCommentView.hidden = NO;
        }
        self.restaurantEventId=dish.eventId;
        if(dish.title && dish.title.length){
            
            userLabel.text = dish.title;
        }else{
            
            userLabel.text = dish.descriptionStr;
        }
        if([NSString stringWithFormat:@"%@",dish.createTime].length<11){
            dish.createTime = [NSNumber numberWithLongLong:[dish.createTime longLongValue]*1000];
        }
        if(dish.photoCreditDic && ![dish.photoCreditDic isBlankString] && [dish.createTime longLongValue]/1000){
            
            userLabel=[[Label alloc] initWithFrame:CGRectMake(LEFTLEFTSET,IMG_StatusBarHeight+TOPOFFSET,DeviceWidth-LEFTLEFTSET-80,50)];
            userLabel.font=[UIFont systemFontOfSize:16];
            userLabel.textColor=[UIColor whiteColor];
            userLabel.tag = 100+index;
            userLabel.numberOfLines = 0;
            userLabel.text = [[NSString stringWithFormat:@"%@",dish.photoCreditDic] filterHtml];//'s photo
            NSString *userName = [NSString stringWithFormat:@"%@",dish.photoCreditDic];
            NSMutableAttributedString *userNameAttributeString = [[NSMutableAttributedString alloc] initWithString:userLabel.text];
            [userNameAttributeString setAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:16]} range:NSMakeRange(0, userName.length)];
            [userLabel setAttributedText:userNameAttributeString];            [userLabel sizeToFit];
            [self.view addSubview:userLabel];
            
            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotToRestaurantEventPage)];
            
            userLabel.userInteractionEnabled = YES;
            tap.numberOfTapsRequired=1;
            tap.numberOfTouchesRequired=1;
            [userLabel addGestureRecognizer:tap];
            
            createLabel.frame = CGRectMake(LEFTLEFTSET, userLabel.endPointY+10, createLabel.frame.size.width, createLabel.frame.size.height);
            NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"MM/dd/yyyy"];// HH:mm:ss
            createLabel.text = [Date getTimeInteval_v10:[dish.createTime longLongValue]/1000];
            
            if([dish.photoCreditTypeDic isEqualToString:@"User ID"]){
                avatarLinkButton = [UIButton buttonWithType:UIButtonTypeCustom];
                avatarLinkButton.frame = CGRectMake(LEFTLEFTSET, IMG_StatusBarHeight+TOPOFFSET, 40, 40);
                avatarLinkButton.clipsToBounds = YES;
                avatarLinkButton.layer.cornerRadius = 20;
                [avatarLinkButton addTarget:self action:@selector(gotoOtherProfileWithUser) forControlEvents:UIControlEventTouchUpInside];
                NSURL *urlString ;
                NSString *userAvatar = dish.userAvatarDic;
                userAvatar = [userAvatar stringByReplacingPercentEscapesUsingEncoding:
                              NSUTF8StringEncoding];
                if ([userAvatar hasPrefix:@"http"]){
                    urlString = [NSURL URLWithString:userAvatar];
                }else{
                    urlString = [NSURL URLWithString:[userAvatar returnFullImageUrl]];
                }
                [avatarLinkButton sd_setImageWithURL:urlString forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"headDefault.jpg"] completed:^(UIImage *image, NSError *error,SDImageCacheType cacheType, NSURL *imageURL) {
                }];
                [self.view addSubview:avatarLinkButton];
                
                userLabel.frame = CGRectMake(avatarLinkButton.endPointX+10, IMG_StatusBarHeight+TOPOFFSET, userLabel.frame.size.width, userLabel.frame.size.height);
                
                long userReviewCount = [dish.userReviewCountDic longValue];
                long userPhotoCount = [dish.userPhotoCountDic longValue];
                reviewAndPhotoCountLabel = [[UILabel alloc] init];
                reviewAndPhotoCountLabel.font = [UIFont systemFontOfSize:13];
                reviewAndPhotoCountLabel.textColor = [UIColor whiteColor];
                
                if (userPhotoCount!=0&&userReviewCount!=0) {
                    
                    [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%ld %@ • %ld %@",userReviewCount,userReviewCount > 1 ? @"reviews":@"review",userPhotoCount,userPhotoCount > 1 ? @"photos":@"photo"]];
                    reviewAndPhotoCountLabel.frame = CGRectMake(userLabel.frame.origin.x, userLabel.endPointY,DeviceWidth-(avatarLinkButton.endPointX+10)-80,50);
                    [reviewAndPhotoCountLabel sizeToFit];
                    [self.view addSubview:reviewAndPhotoCountLabel];
                }else if (userPhotoCount==0||userReviewCount==0){
                    if (userReviewCount==0&&userPhotoCount!=0) {
                        
                        [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%ld %@",userPhotoCount,userPhotoCount > 1 ? @"photos":@"photo"]];
                        reviewAndPhotoCountLabel.frame = CGRectMake(userLabel.frame.origin.x, userLabel.endPointY, DeviceWidth-(avatarLinkButton.endPointX+10)-80, 50);
                        
                        [reviewAndPhotoCountLabel sizeToFit];
                        [self.view addSubview:reviewAndPhotoCountLabel];
                        
                    }
                    if (userPhotoCount==0&&userReviewCount!=0) {
                        
                        [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%ld %@",userReviewCount,userReviewCount > 1 ? @"reviews":@"review"]];
                        reviewAndPhotoCountLabel.frame = CGRectMake(userLabel.frame.origin.x, userLabel.endPointY, DeviceWidth-(avatarLinkButton.endPointX+10)-80, 50);
                        [reviewAndPhotoCountLabel sizeToFit];
                        [self.view addSubview:reviewAndPhotoCountLabel];
                        
                        
                    }
                    
                }
                
//                
                createLabel.frame = CGRectMake(LEFTLEFTSET, avatarLinkButton.endPointY+10, createLabel.frame.size.width, createLabel.frame.size.height);

                
            }
            
        }
        [self updateUIWhenScrollAndLoad:imgType andDish:dish];
    }
}
-(void)gotToDishPage{
    IMGDish *dish=[photosArray objectAtIndex:((userLabel.tag-100)-1)];
    NSString *photoCreditType = dish.photoCreditTypeDic;
    if ( [photoCreditType isEqualToString:@"Qraved Team"]  ||   [photoCreditType isEqualToString:@"Restaurant"]) {
        NSNumber *restaurantId = dish.restaurantIdDic;
        if (restaurantId && (![restaurantId isEqualToNumber:[NSNumber numberWithLong:0]]) ) {
            DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:restaurantId];
            dvc.amplitudeType = @"Card detail page";
            [self.navigationController pushViewController:dvc animated:YES];
        }
    }else if([photoCreditType isEqualToString:@"URL"]){
        NSString *photoCreditUrl =dish.photoCreditUrlDic;
        if (photoCreditUrl && (![photoCreditUrl isEqualToString:@""]) && [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:photoCreditUrl]]){
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:photoCreditUrl]];
        }
    }else if([photoCreditType isEqualToString:@"User ID"]){
        [self gotoOtherProfileWithUser];
    }
}
-(void)gotToRestaurantEventPage{
    IMGRestaurantEvent *dish=[photosArray objectAtIndex:((userLabel.tag-100)-1)];
    NSString *photoCreditType = dish.photoCreditTypeDic;
    if ( [photoCreditType isEqualToString:@"Qraved Team"]  ||   [photoCreditType isEqualToString:@"Restaurant"]) {
        NSNumber *restaurantId = dish.restaurantIdDic;
        if (restaurantId && (![restaurantId isEqualToNumber:[NSNumber numberWithLong:0]]) ) {
            DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:restaurantId];
            dvc.amplitudeType = @"Card detail page";
            [self.navigationController pushViewController:dvc animated:YES];
        }
    }else if([photoCreditType isEqualToString:@"URL"]){
        NSString *photoCreditUrl =dish.photoCreditUrlDic;
        if (photoCreditUrl && (![photoCreditUrl isEqualToString:@""]) && [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:photoCreditUrl]]){
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:photoCreditUrl]];
        }
    }else if([photoCreditType isEqualToString:@"User ID"]){
        [self gotoOtherProfileWithUser];
    }
}

-(void)EScrollerViewDidDoubleClicked:(NSUInteger)index{
    
}

-(void)PhotosEScrollerViewDidClicked:(NSUInteger)index ContentOffset:(CGPoint)contentoffset{
    //    numLabel.text = [NSString stringWithFormat:@"%lu/%lu",(unsigned long)index,(unsigned long)photosArray.count];
    if(imgType == TypeForIMGDish){
        IMGDish *dish=[photosArray objectAtIndex:index-1];
        if(dish.title && dish.title.length){
            
            userLabel.text = dish.title;
        }
    }else if(imgType == TypeForIMGRestaurantEvent){
        IMGRestaurantEvent *dish=[photosArray objectAtIndex:index-1];
        if(dish.title && dish.title.length){
            
            userLabel.text = dish.title;
        }else{
            
            userLabel.text = dish.descriptionStr;
        }
    }else{
        return;
    }
}

-(void)PhotosEScrollerViewDidClicked:(NSUInteger)index{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)likeCommentShareView:(UIView *)likeCommentShareView likeButtonTapped:(UIButton *)button{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp] goToLoginControllerByDelegate:self dictionary:@{@"AlbumActionButtonsType":@(TypeForLike)}];
        return;
    }
    
    //            [[LoadingView sharedLoadingView] startLoading];
    
    if(imgType == TypeForIMGRestaurantEvent){
        IMGRestaurantEvent *dish = (IMGRestaurantEvent *)currentDish;
        [RestaurantHandler likeRestaurantEvent:dish.isLike restaurantEventId:dish.eventId andBlock:^{
        }failure:^(NSString *exceptionMsg){
            if([exceptionMsg isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
                [[AppDelegate ShareApp] goToLoginController];
            }
        }];
        dish.isLike=!dish.isLike;
        if (dish.isLike) {
            dish.likeCount = [NSNumber numberWithInt:([dish.likeCount intValue]+1)];
        }else{
            dish.likeCount = [NSNumber numberWithInt:([dish.likeCount intValue]-1)];
        }
        [self updateUIWhenScrollAndLoad:imgType andDish:dish];
        [self.albumReloadCellDelegate reloadCell];
        
    }else if(imgType == TypeForIMGDish){
        IMGDish *dish = (IMGDish *)currentDish;
        [UploadPhotoHandler likeDish:dish.isLike dishId:dish.dishId andBlock:^{
        }failure:^(NSString *exceptionMsg){
            if([exceptionMsg isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
                [[AppDelegate ShareApp] goToLoginController];
            }
        }];
        dish.isLike=!dish.isLike;
        if (dish.isLike) {
            dish.likeCount = [NSNumber numberWithInt:([dish.likeCount intValue]+1)];
        }else{
            dish.likeCount = [NSNumber numberWithInt:([dish.likeCount intValue]-1)];
        }
        [self updateUIWhenScrollAndLoad:imgType andDish:dish];
        [self.albumReloadCellDelegate reloadCell];
    }
    
}

- (void)likeCommentShareView:(UIView *)likeCommentShareView commentButtonTapped:(UIButton *)button{
    if(popReviewView){
        [popReviewView removeFromSuperview];
        popReviewView = nil;
    }
    NSMutableArray *dishArrayM = [[NSMutableArray alloc]init];
    if(imgType == TypeForIMGRestaurantEvent){
        //                dishArrayM = [IMGRestaurantEventComment commentListFromArray:[currentDish valueForKey:@"commentList"] andIsVendor:NO andRestaurant:currentRestaurant];
        dishArrayM = [currentDish valueForKey:@"commentList"];
    }else if(imgType == TypeForIMGDish){
        dishArrayM = [IMGDishComment commentListFromArray:[currentDish valueForKey:@"commentList"] andIsVendor:NO andRestaurant:currentRestaurant];
    }
    popReviewView = [[PopReviewView alloc]initInSuperView:self.view commentCount:[[currentDish valueForKey:@"commentCount"] intValue] commentArray:dishArrayM];
    popReviewView.frame = CGRectMake(0, 20, DeviceWidth, DeviceHeight);
    popReviewView.delegate = self;
    if(([currentDish valueForKey:@"commentCount"]>0&&(![currentDish valueForKey:@"commentList"]||((NSArray *)[currentDish valueForKey:@"commentList"]).count == 0))){
        if(imgType == TypeForIMGRestaurantEvent){
            [self popReviewView:popReviewView viewPreviousCommentsTapped:nil lastComment:[[IMGRestaurantEventComment alloc]init]];
            
        }else if(imgType == TypeForIMGDish){
            [self popReviewView:popReviewView viewPreviousCommentsTapped:nil lastComment:[[IMGDishComment alloc]init]];
        }
        [popReviewView showPopReviewView:YES];
    }else{
        [popReviewView showPopReviewView:NO];
    }
}

- (void)likeCommentShareView:(UIView *)likeCommentShareView shareButtonTapped:(UIButton *)button{
    if(imgType == TypeForIMGRestaurantEvent){
        IMGRestaurantEvent *_restaurantEvent = (IMGRestaurantEvent *)currentDish;
        IMGRestaurant *_restaurant = [[IMGRestaurant alloc] init];
        _restaurant.restaurantId = _restaurantEvent.restaurantId;
        _restaurant.title = _restaurantEvent.restaurantTitle;
        if (!_restaurantEvent.restaurantTitle.length) {
            _restaurant.title = currentRestaurant.title;
        }
        _restaurant.seoKeyword = _restaurantEvent.restaurantSeo;
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/promo/%@",QRAVED_WEB_SERVER_OLD,_restaurantEvent.eventId]];
        NSString *shareContentString = [NSString stringWithFormat:@"%@ on Qraved. %@",_restaurant.title,url];
        RestaurantEventActivityItemProvider *provider=[[RestaurantEventActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
        provider.url=url;
        provider.title=shareContentString;
        provider.restaurantTitle=_restaurant.title;
        
        TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
        twitterActivityItemProvider.title = [NSString stringWithFormat:@"Check out this event at %@ on Qraved",_restaurant.title];
        
        UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
        
        
        
        activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
        [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK %@'s event on Qraved!"),_restaurant.title] forKey:@"subject"];
        activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
            if (completed) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:_restaurant.title forKey:@"RestaurantTitle"];
                [eventProperties setValue:@"Restaurant detail page - event photo viewer，RDP" forKey:@"Location"];
                [eventProperties setValue:_restaurantEventId forKey:@"RestaurantEvent_ID"];
                [eventProperties setValue:_restaurant.restaurantId forKey:@" Restaurant_ID"];
                
                [eventProperties setValue:@"Channel" forKey:provider.activityType];
                
                [[Amplitude instance] logEvent:@"SH - Share Restaurant Event" withEventProperties:eventProperties];
            }else{
            }
        };
        if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
            // iOS8
            activityCtl.popoverPresentationController.sourceView =
            button;
        }
        
        [self presentViewController:activityCtl animated:YES completion:^{
            
        }];
    }else if(imgType == TypeForIMGDish){
        IMGDish *_dish = (IMGDish*)currentDish;
        IMGRestaurant *_restaurant = [[IMGRestaurant alloc] init];
        _restaurant.restaurantId = _dish.restaurantId;
        _restaurant.title = _dish.restaurantTitle;
        _restaurant.seoKeyword = _dish.restaurantSeo;
        if (!_dish.restaurantTitle.length) {
            _restaurant.title = currentRestaurant.title;
        }
        //                NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/photo/%@/%@",QRAVED_WEB_SERVER,[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],_restaurant.seoKeyword]];
        
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/photo/%@",QRAVED_WEB_SERVER_OLD,_dish.dishId]];
        
        NSString *shareContentString = [NSString stringWithFormat:@"Check out this photo at %@ on Qraved. %@",currentRestaurant.title,url];
        UplodePhotoActivityItemProvider *provider=[[UplodePhotoActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
        provider.url=url;
        provider.title=shareContentString;
        provider.restaurantTitle=_restaurant.title;
        provider.userName = _dish.userName;
        if (!_dish.userName.length) {
            provider.userName = self.review.fullName;
            _dish.userName = self.review.fullName;
        }
        
        TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
        twitterActivityItemProvider.title = [NSString stringWithFormat:@"Check out this photo at %@ on Qraved",_restaurant.title];
        
        UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
        
        
        //去除airDorp
        activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
        [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK IT OUT NOW! A Restaurant Photo by %@ on Qraved!"),_dish.userName] forKey:@"subject"];
        activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
            if (completed) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:_dish.dishId forKey:@"Photo_ID"];
                [eventProperties setValue:_dish.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:_dish.title forKey:@"DishTitle"];
                [eventProperties setValue:@"Restaurant detail page - photo viewer" forKey:@"Location"];
                [eventProperties setValue:@"Channel" forKey:provider.activityType];
                
                [[Amplitude instance] logEvent:@"SH - Share Photo" withEventProperties:eventProperties];
            }else{
            }
        };
        if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
            // iOS8
            activityCtl.popoverPresentationController.sourceView =
            button;
        }
        
        [self presentViewController:activityCtl animated:YES completion:^{
            
        }];
    }

}

-(void)albumActionButtonsClick:(AlbumActionButtonsType)btnType withBtn:(UIButton *)btn{
    switch (btnType) {
        case TypeForLike:{
            IMGUser *currentUser = [IMGUser currentUser];
            if (currentUser.userId == nil || currentUser.token == nil)
            {
                [[AppDelegate ShareApp] goToLoginControllerByDelegate:self dictionary:@{@"AlbumActionButtonsType":@(btnType)}];
                return;
            }
            
            //            [[LoadingView sharedLoadingView] startLoading];
            
            if(imgType == TypeForIMGRestaurantEvent){
                IMGRestaurantEvent *dish = (IMGRestaurantEvent *)currentDish;
                [RestaurantHandler likeRestaurantEvent:dish.isLike restaurantEventId:dish.eventId andBlock:^{
                }failure:^(NSString *exceptionMsg){
                    if([exceptionMsg isLogout]){
                        [[LoadingView sharedLoadingView] stopLoading];
                        [[IMGUser currentUser]logOut];
                        [[AppDelegate ShareApp] goToLoginController];
                    }
                }];
                dish.isLike=!dish.isLike;
                if (dish.isLike) {
                    dish.likeCount = [NSNumber numberWithInt:([dish.likeCount intValue]+1)];
                }else{
                    dish.likeCount = [NSNumber numberWithInt:([dish.likeCount intValue]-1)];
                }
                [self updateUIWhenScrollAndLoad:imgType andDish:dish];
                [self.albumReloadCellDelegate reloadCell];
                
            }else if(imgType == TypeForIMGDish){
                IMGDish *dish = (IMGDish *)currentDish;
                [UploadPhotoHandler likeDish:dish.isLike dishId:dish.dishId andBlock:^{
                }failure:^(NSString *exceptionMsg){
                    if([exceptionMsg isLogout]){
                        [[LoadingView sharedLoadingView] stopLoading];
                        [[IMGUser currentUser]logOut];
                        [[AppDelegate ShareApp] goToLoginController];
                    }
                }];
                dish.isLike=!dish.isLike;
                if (dish.isLike) {
                    dish.likeCount = [NSNumber numberWithInt:([dish.likeCount intValue]+1)];
                }else{
                    dish.likeCount = [NSNumber numberWithInt:([dish.likeCount intValue]-1)];
                }
                [self updateUIWhenScrollAndLoad:imgType andDish:dish];
                [self.albumReloadCellDelegate reloadCell];
            }
            
        }
            break;
        case TypeForComment:{
            if(popReviewView){
                [popReviewView removeFromSuperview];
                popReviewView = nil;
            }
            NSMutableArray *dishArrayM = [[NSMutableArray alloc]init];
            if(imgType == TypeForIMGRestaurantEvent){
                //                dishArrayM = [IMGRestaurantEventComment commentListFromArray:[currentDish valueForKey:@"commentList"] andIsVendor:NO andRestaurant:currentRestaurant];
                dishArrayM = [currentDish valueForKey:@"commentList"];
            }else if(imgType == TypeForIMGDish){
                dishArrayM = [IMGDishComment commentListFromArray:[currentDish valueForKey:@"commentList"] andIsVendor:NO andRestaurant:currentRestaurant];
            }
            popReviewView = [[PopReviewView alloc]initInSuperView:self.view commentCount:[[currentDish valueForKey:@"commentCount"] intValue] commentArray:dishArrayM];
            popReviewView.frame = CGRectMake(0, 20, DeviceWidth, DeviceHeight);
            popReviewView.delegate = self;
            if(([currentDish valueForKey:@"commentCount"]>0&&(![currentDish valueForKey:@"commentList"]||((NSArray *)[currentDish valueForKey:@"commentList"]).count == 0))){
                if(imgType == TypeForIMGRestaurantEvent){
                    [self popReviewView:popReviewView viewPreviousCommentsTapped:nil lastComment:[[IMGRestaurantEventComment alloc]init]];
                    
                }else if(imgType == TypeForIMGDish){
                    [self popReviewView:popReviewView viewPreviousCommentsTapped:nil lastComment:[[IMGDishComment alloc]init]];
                }
                [popReviewView showPopReviewView:YES];
            }else{
                [popReviewView showPopReviewView:NO];
            }
        }
            break;
        case TypeForShare:{
            if(imgType == TypeForIMGRestaurantEvent){
                IMGRestaurantEvent *_restaurantEvent = (IMGRestaurantEvent *)currentDish;
                IMGRestaurant *_restaurant = [[IMGRestaurant alloc] init];
                _restaurant.restaurantId = _restaurantEvent.restaurantId;
                _restaurant.title = _restaurantEvent.restaurantTitle;
                if (!_restaurantEvent.restaurantTitle.length) {
                    _restaurant.title = currentRestaurant.title;
                }
                _restaurant.seoKeyword = _restaurantEvent.restaurantSeo;
                NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/promo/%@",QRAVED_WEB_SERVER_OLD,_restaurantEvent.eventId]];
                NSString *shareContentString = [NSString stringWithFormat:@"%@ on Qraved. %@",_restaurant.title,url];
                RestaurantEventActivityItemProvider *provider=[[RestaurantEventActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
                provider.url=url;
                provider.title=shareContentString;
                provider.restaurantTitle=_restaurant.title;
                
                TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
                twitterActivityItemProvider.title = [NSString stringWithFormat:@"Check out this event at %@ on Qraved",_restaurant.title];
                
                UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
                
                
                
                activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
                [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK %@'s event on Qraved!"),_restaurant.title] forKey:@"subject"];
                activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
                    if (completed) {
                        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                        [eventProperties setValue:_restaurant.title forKey:@"RestaurantTitle"];
                        [eventProperties setValue:@"Restaurant detail page - event photo viewer，RDP" forKey:@"Location"];
                        [eventProperties setValue:_restaurantEventId forKey:@"RestaurantEvent_ID"];
                        [eventProperties setValue:_restaurant.restaurantId forKey:@" Restaurant_ID"];
                        
                        [eventProperties setValue:@"Channel" forKey:provider.activityType];
                        
                        [[Amplitude instance] logEvent:@"SH - Share Restaurant Event" withEventProperties:eventProperties];
                    }else{
                    }
                };
                if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
                    // iOS8
                    activityCtl.popoverPresentationController.sourceView =
                    btn;
                }
                
                [self presentViewController:activityCtl animated:YES completion:^{
                    
                }];
            }else if(imgType == TypeForIMGDish){
                IMGDish *_dish = (IMGDish*)currentDish;
                IMGRestaurant *_restaurant = [[IMGRestaurant alloc] init];
                _restaurant.restaurantId = _dish.restaurantId;
                _restaurant.title = _dish.restaurantTitle;
                _restaurant.seoKeyword = _dish.restaurantSeo;
                if (!_dish.restaurantTitle.length) {
                    _restaurant.title = currentRestaurant.title;
                }
                //                NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/photo/%@/%@",QRAVED_WEB_SERVER,[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],_restaurant.seoKeyword]];
                
                NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/photo/%@",QRAVED_WEB_SERVER_OLD,_dish.dishId]];
                
                NSString *shareContentString = [NSString stringWithFormat:@"Check out this photo at %@ on Qraved. %@",currentRestaurant.title,url];
                UplodePhotoActivityItemProvider *provider=[[UplodePhotoActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
                provider.url=url;
                provider.title=shareContentString;
                provider.restaurantTitle=_restaurant.title;
                provider.userName = _dish.userName;
                if (!_dish.userName.length) {
                    provider.userName = self.review.fullName;
                    _dish.userName = self.review.fullName;
                }
                
                TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
                twitterActivityItemProvider.title = [NSString stringWithFormat:@"Check out this photo at %@ on Qraved",_restaurant.title];
                
                UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
                
                
                //去除airDorp
                activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
                [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK IT OUT NOW! A Restaurant Photo by %@ on Qraved!"),_dish.userName] forKey:@"subject"];
                activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
                    if (completed) {
                        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                        [eventProperties setValue:_dish.dishId forKey:@"Photo_ID"];
                        [eventProperties setValue:_dish.restaurantId forKey:@"Restaurant_ID"];
                        [eventProperties setValue:_dish.title forKey:@"DishTitle"];
                        [eventProperties setValue:@"Restaurant detail page - photo viewer" forKey:@"Location"];
                        [eventProperties setValue:@"Channel" forKey:provider.activityType];
                        
                        [[Amplitude instance] logEvent:@"SH - Share Photo" withEventProperties:eventProperties];
                    }else{
                    }
                };
                if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
                    // iOS8
                    activityCtl.popoverPresentationController.sourceView =
                    btn;
                }
                
                [self presentViewController:activityCtl animated:YES completion:^{
                    
                }];
            }
            
        }
            break;
        default:
            break;
    }
}

- (void)popReviewView:(PopReviewView *)popReviewView_ viewPreviousCommentsTapped:(UIButton *)button lastComment:(id)comment{
    if(imgType == TypeForIMGRestaurantEvent){
        IMGRestaurantEvent *_restaurantEvent = (IMGRestaurantEvent *)currentDish;
        [[LoadingView sharedLoadingView] startLoading];
        NSString *lastCommentIdStr=@"0";
        if([comment isKindOfClass:[IMGRestaurantEventComment class]]){
            IMGRestaurantEventComment *reviewComment=(IMGRestaurantEventComment *)comment;
            if(reviewComment&&reviewComment.commentId){
                lastCommentIdStr=[NSString stringWithFormat:@"%@",reviewComment.commentId];
            }
            
            [RestaurantHandler previousComment:lastCommentIdStr targetId:_restaurantEvent.eventId andBlock:^(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount) {
                if(commentList&&commentList.count>0){
                    //                    [self.view endEditing:YES];
                    
                    if(commentList){
                        [popReviewView_ addPreviousComments:commentList];
                    }
                }
            }];
        }else{
            [[LoadingView sharedLoadingView] stopLoading];
        }
        
        
    }else if(imgType == TypeForIMGDish){
        IMGDish *_dish = (IMGDish*)currentDish;
        [[LoadingView sharedLoadingView] startLoading];
        NSString *lastCommentIdStr=@"0";
        if([comment isKindOfClass:[IMGDishComment class]]){
            IMGDishComment *dishComment=(IMGDishComment *)comment;
            if(dishComment&&dishComment.commentId){
                lastCommentIdStr=[NSString stringWithFormat:@"%@",dishComment.commentId];
            }
            
            [UploadPhotoHandler previousComment:lastCommentIdStr targetId:_dish.dishId andBlock:^(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount) {
                if(commentList&&commentList.count>0){
                    //                    [self.view endEditing:YES];
                    
                    if(commentList){
                        [popReviewView_ addPreviousComments:commentList];
                    }
                }
            }];
        }else{
            [[LoadingView sharedLoadingView] stopLoading];
        }
    }
}

-(void)postComment:(UITextView *)textInput{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp] goToLoginControllerByDelegate:self dictionary:@{@"postComment":textInput}];
        return;
    }
    NSLog(@"%f write comment in photo start time",[[NSDate date]timeIntervalSinceReferenceDate]);
    if(imgType == TypeForIMGRestaurantEvent){
        //write comment
        IMGRestaurantEvent *_restaurantEvent = (IMGRestaurantEvent *)currentDish;
        
        
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:_restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
        [eventProperties setValue:_restaurantEvent.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:@"Restaurant detail page - event photo viewer" forKey:@"Location"];
        
        [RestaurantHandler postComment:textInput.text restaurantEventId:_restaurantEvent.eventId andBlock:^(IMGRestaurantEventComment *eventComment) {
            NSLog(@"%f write comment in photo service",[[NSDate date]timeIntervalSinceReferenceDate]);
            [eventProperties setValue:eventComment.commentId forKey:@"Comment_ID"];
            [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Succeed" withEventProperties:eventProperties];
            [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Restaurant Event Succeed" withValues:eventProperties];
            _restaurantEvent.commentCount=[NSNumber numberWithInt:([_restaurantEvent.commentCount intValue]+1)];
            NSLog(@"%f write comment in photo deal data",[[NSDate date]timeIntervalSinceReferenceDate]);
            //            [self updateUIWhenScrollAndLoad:imgType andDish:_restaurantEvent];
            
            if (_restaurantEvent.commentList) {
                if([_restaurantEvent.commentList isKindOfClass:[NSArray class]]){
                    _restaurantEvent.commentList=[NSMutableArray arrayWithArray:_restaurantEvent.commentList];
                }
                if (_restaurantEvent.commentList.count >= 3) {
                    [_restaurantEvent.commentList removeObjectAtIndex:_restaurantEvent.commentList.count-1];
                }
                [_restaurantEvent.commentList insertObject:eventComment atIndex:0];
            }else{
                _restaurantEvent.commentList = [[NSMutableArray alloc] initWithObjects:eventComment, nil];
            }
            [self updateUIWhenScrollAndLoad:imgType andDish:_restaurantEvent];
            [self updateCommentCount:imgType andDish:_restaurantEvent];
            [self.albumReloadCellDelegate reloadCell];
            [popReviewView addNewComment:eventComment];
            NSLog(@"%f write comment in photo UI",[[NSDate date]timeIntervalSinceReferenceDate]);
        } failure:^(NSString *errorReason){
            if([errorReason isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
                [[AppDelegate ShareApp] goToLoginController];
            }
            [eventProperties setValue:errorReason forKey:@"Reason"];
            [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Failed" withEventProperties:eventProperties];
        }];
        [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Initiate" withEventProperties:eventProperties];
    }else if(imgType == TypeForIMGDish){
        IMGDish *_dish = (IMGDish*)currentDish;
        
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:_dish.dishId forKey:@"Photo_ID"];
        [eventProperties setValue:_dish.userId forKey:@"UploaderUser_ID"];
        [eventProperties setValue:_dish.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:@"Restaurant detail page - photo viewer" forKey:@"Location"];
        
        [UploadPhotoHandler postComment:textInput.text dishId:_dish.dishId restaurantId:_dish.restaurantId andBlock:^(IMGDishComment *dishComment) {
            NSLog(@"%f write comment in photo service",[[NSDate date]timeIntervalSinceReferenceDate]);
            
            [eventProperties setValue:dishComment.commentId forKey:@"Comment_ID"];
            [[Amplitude instance] logEvent:@"UC - Comment Photo Succeed" withEventProperties:eventProperties];
            [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Photo Succeed" withValues:eventProperties];
            
            _dish.commentCount=[NSNumber numberWithInt:([_dish.commentCount intValue]+1)];
            //            [self updateUIWhenScrollAndLoad:imgType andDish:_dish];
            NSLog(@"%f write comment in photo deal data",[[NSDate date]timeIntervalSinceReferenceDate]);
            [self updateUIWhenScrollAndLoad:imgType andDish:_dish];
            [self updateCommentCount:imgType andDish:_dish];
            [self.albumReloadCellDelegate reloadCell];
            if(_dish.commentList){
                if([_dish.commentList isKindOfClass:[NSArray class]]){
                    _dish.commentList=[NSMutableArray arrayWithArray:_dish.commentList];
                }
                if(_dish.commentList.count>=3){
                    [_dish.commentList removeObjectAtIndex:_dish.commentList.count-1];
                }
                [_dish.commentList insertObject:dishComment atIndex:0];
            }else{
                _dish.commentList = [[NSMutableArray alloc]initWithObjects:dishComment, nil];
            }
            
            
            [popReviewView addNewComment:dishComment];
            NSLog(@"%f write comment in photo UI",[[NSDate date]timeIntervalSinceReferenceDate]);
            
        } failure:^(NSString *errorReason){
            if([errorReason isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
                [[AppDelegate ShareApp] goToLoginController];
            }
            [eventProperties setValue:errorReason forKey:@"Reason"];
            [[Amplitude instance] logEvent:@"UC – Comment Photo Failed" withEventProperties:eventProperties];
        }];
        [[Amplitude instance] logEvent:@"UC - Comment Photo Initiate" withEventProperties:eventProperties];
    }
    
}

- (void)popReviewViewUserNameOrImageTapped:(id)sender{
    if ([sender isKindOfClass:[IMGUser class]])
    {
        IMGUser *user = (IMGUser *)sender;
        OtherProfileViewController *opvc = [[OtherProfileViewController alloc] init];
        opvc.amplitude = self.amplitudeType;
        IMGUser *currentUser = [IMGUser currentUser];
        if ([currentUser.userId intValue] == [user.userId intValue])
        {
            opvc.isOtherUser = NO;
        }else
        {
            opvc.isOtherUser = YES;
        }
        opvc.otherUser = user;
        [self.navigationController pushViewController:opvc animated:YES];
    }
    else if ([sender isKindOfClass:[IMGRestaurant class]])
    {
        IMGRestaurant *restaurant = (IMGRestaurant *)sender;
        DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:restaurant.restaurantId];
        [self.navigationController pushViewController:dvc animated:YES];
    }
    
}

-(void)gotoOtherProfileWithUser{
    id obj=[photosArray objectAtIndex:((userLabel.tag-100)-1)];
    
    IMGUser *user = [[IMGUser alloc] init];
    
    if ([obj isKindOfClass:[IMGDish class]]) {
        IMGDish *dish = (IMGDish*)obj;
        if (dish.userIdDic == nil) {
            user.userId = dish.userId;
        }else{
            user.userId = dish.userIdDic;
        }
        NSString *nameStr = dish.photoCreditDic;
        user.userName = [nameStr stringByReplacingOccurrencesOfString:@"0" withString:@" "];
        user.token = @"";
        user.avatar = dish.userAvatarDic;
    }else{
        IMGRestaurantEvent *dish = (IMGRestaurantEvent*)obj;
        user.userId = dish.userIdDic;
        NSString *nameStr = dish.photoCreditDic;
        user.userName = [nameStr stringByReplacingOccurrencesOfString:@"0" withString:@" "];
        user.token = @"";
        user.avatar = dish.userAvatarDic;
    }
    
    IMGUser *currentUser = [IMGUser currentUser];
    OtherProfileViewController *opvc = [[OtherProfileViewController alloc] init];
    opvc.amplitude = self.amplitudeType;
    if ([currentUser.userId intValue]==[user.userId intValue]){
        opvc.isOtherUser = NO;
    }else{
        opvc.isOtherUser = YES;
    }
    opvc.otherUser = user;
    [self.navigationController pushViewController:opvc animated:YES];
}
- (void) gotoResDetailController:(UITapGestureRecognizer *)sender {
    UIView *view = sender.view;
    IMGDish *dish=[photosArray objectAtIndex:((view.tag-100)-1)];
    
    DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurantId:dish.restaurantIdDic];
    [self.navigationController pushViewController:detailViewController animated:YES];
}
-(NSString*)getPageIdByAmplitudeType{
    NSString *pageId = @"";
    if ([@"RC - View Journal photo" isEqualToString:self.amplitudeType]) {
        pageId = @",Journal detail page";
    }
    return [NSString stringWithFormat:@"%@%@",self.amplitudeType,pageId];
}

- (void)requestData:(NSNumber *)restaurantId{
    [[LoadingView sharedLoadingView] startLoading];
    NSDictionary *dic= @{@"restaurantId":restaurantId};
    [WebServiceV2 instagramRestaurant:dic andBlock:^(IMGRestaurant *restaurant,NSNumber *instagramLocationId,NSDictionary *photoDic) {
        instagramBottomView.restaurant = restaurant;
        instagramBottomView.locationId = instagramLocationId;
        instagramBottomView.photoDictionary = photoDic;
        
        if (instagramBottomView.currentPointY>DeviceHeight+20) {
            scroll.frame =  CGRectMake(0, DeviceHeight+20-79, DeviceWidth, instagramBottomView.currentPointY);
            instagramBottomView.frame = CGRectMake(0, 0, DeviceWidth, instagramBottomView.currentPointY);
            scroll.contentSize = CGSizeMake(0, instagramBottomView.currentPointY*2-79);
            
        }else{
            scroll.frame =  CGRectMake(0, DeviceHeight+20-79, DeviceWidth, DeviceHeight+99);
            instagramBottomView.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight+99);
            scroll.contentSize = CGSizeMake(0, DeviceHeight+20+DeviceHeight+20+79);
        }
        
        instagramBottomView.gotoMiniRDP = ^(){
            if (@available(iOS 11.0, *)) {
                [UIView animateWithDuration:0.5 animations:^{
                    scroll.contentOffset = CGPointMake(0, DeviceHeight+20 +20);
                }];
            }else{
                [UIView animateWithDuration:0.5 animations:^{
                    scroll.contentOffset = CGPointMake(0, DeviceHeight+20);
                }];
            }
        };
        __weak typeof(self) weakSelf = self;
        __weak typeof(instagramBottomView) weakinstagramBottomView = instagramBottomView;
        instagramBottomView.saveRestaurant = ^(IMGRestaurant *restaurant) {
            [CommonMethod doSaveWithRestaurant:restaurant andViewController:weakSelf andCallBackBlock:^(BOOL isSaved) {
                [weakinstagramBottomView freshSavedButton:isSaved];
            }];
        };
        [[LoadingView sharedLoadingView] stopLoading];
        
    } andError:^(NSError *error) {
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

-(void)jumpToInstagram{
        IMGDish *dishTemp =(IMGDish*)currentDish;
        if (dishTemp.instagramLink.length>0) {
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:dishTemp.instagramLink]];
        }else{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://instagram.com"]];

       }
}



@end
