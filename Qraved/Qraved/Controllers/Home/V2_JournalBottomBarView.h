//
//  V2_JournalBottomBarView.h
//  Qraved
//
//  Created by harry on 2017/8/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGMediaComment.h"

@interface V2_JournalBottomBarView : UIView<CommunityButtonTappedDelegate>

@property (nonatomic, strong) IMGMediaComment *journal;
@property (nonatomic, copy) void (^ shareToLine)();
@property (nonatomic, copy) void (^ copyLink)();
@property (nonatomic, copy) void (^ shareToFB)();
@property (nonatomic, copy) void (^ pressLiked)();
@property (nonatomic, copy) void (^ pressLikedCount)();
@property (nonatomic, copy) void (^ pressComment)();

- (void)updateLikeButtonStatus:(BOOL)liked_ likeCount:(int)likeCount;
- (void)updateCommentButton:(int)commentCount;

@end
