//
//  PhotosImageListViewController.h
//  Qraved
//
//  Created by DeliveLee on 16/3/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "IMGRestaurant.h"
#import "AppDelegate.h"
#import "IMGReview.h"
#import "PhotosEScrollerView.h"

@protocol AlbumReloadCellDelegate <NSObject>
-(void)reloadCell;
@end

@protocol EScrollerViewDelegate;

@interface AlbumViewController : BaseChildViewController<UIScrollViewDelegate,EScrollerViewDelegate,PhotosEScrollerViewDelegate>

//@property (nonatomic,retain) NSString *title;

@property (nonatomic,assign) BOOL showMenu;
@property (nonatomic,assign) BOOL popDismiss;
@property (nonatomic,assign) BOOL useNew;
@property (nonatomic,assign) BOOL showPage;
@property (nonatomic,assign) BOOL isJournal;
@property (nonatomic,assign) BOOL isFromJourney;
@property (nonatomic ,strong) IMGReview *review;
@property(nonatomic,retain) NSNumber *restaurantId;
@property(nonatomic,weak)id<AlbumReloadCellDelegate> albumReloadCellDelegate;
@property (nonatomic,assign) BOOL ifFromRestaurantDetail;
@property(nonatomic,retain)NSNumber* dishId;
@property(nonatomic,retain)NSNumber* restaurantid;
@property(nonatomic,retain) NSString *amplitudeTitle;
@property(nonatomic,retain) NSString *amplitudeType;
@property(nonatomic,retain)NSNumber* restaurantEventId;
@property(nonatomic,retain)NSNumber* journalId;
@property(nonatomic,retain)NSNumber* photoId;
@property (nonatomic,assign) BOOL isUseWebP;
-(id)initWithPhotosArray:(NSArray *)photos andPhotoTag:(NSInteger)tag andRestaurant:(IMGRestaurant *)restaurant andFromDetail:(BOOL)isDetail;
-(void)updateRestautantDetailData:(NSMutableArray *)moreDishList;
@end
