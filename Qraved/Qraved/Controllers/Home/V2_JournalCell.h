//
//  V2_JournalCell.h
//  Qraved
//
//  Created by harry on 2017/7/5.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGMediaComment.h"
@interface V2_JournalCell : UICollectionViewCell


@property (nonatomic, strong) IMGMediaComment *mediaComment;

@end
