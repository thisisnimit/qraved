//
//  JournalDetailViewController.m
//  Qraved
//
//  Created by Lucky on 15/6/30.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#define RESTAURANT_TAG @"0"
#define TITLE_LABEL_TAG @"1"
#define PHOTO_IMAGE_TAG @"2"
#define CONTENT_LABEL_TAG @"3"
#define BUTTON_TAG @"4"

#import "JournalDetailViewController.h"
#import "GDataXMLNode.h"
#import "IMGMediaComment.h"
#import "MoreArticleViewController.h"
#import "PhotosDetailViewController.h"
#import "MenuPhotoViewColltroller.h"

#import "DBManager.h"
#import "UIViewController+Helper.h"
#import "IMGMediaComment.h"
#import "UIConstants.h"
#import "UIImage+Resize.h"
#import "UIImageView+WebCache.h"
#import "Label.h"
#import "UIView+Helper.h"
#import <CoreText/CoreText.h>
#import "UIColor+Helper.h"
#import "WebViewController.h"
#import "NSString+Helper.h"
#import "LoadingView.h"
#import "DBManager.h"
#import "UIConstants.h"
#import "JournalListTableViewCell.h"
#import "JournalHandler.h"
#import "JournalDetailViewController.h"
#import "IMGJournalRestaurant.h"
#import "IMGRestaurant.h"
#import "DetailViewController.h"
#import "RestaurantHandler.h"
#import "UILabel+Helper.h"

#import "IMGJournalDetail.h"
#import "DLStarRatingControl.h"
#import "IMGCity.h"
#import "IMGDish.h"
#import "IMGUser.h"
#import "LoginViewController.h"
#import "JournalRestaurant.h"
#import "SearchPhotosViewController.h"
#import "SectionViewController.h"
#import "UIViewController+LewPopupViewController.h"
#import "LewPopupViewAnimationFade.h"
#import "LewPopupViewAnimationSlide.h"
#import "LewPopupViewAnimationSpring.h"
#import "LewPopupViewAnimationDrop.h"
#import "RegisterViewController.h"
#import "PopupView.h"
#import "MapViewController.h"
#import "IMGRestaurantOffer.h"
#import "SpecialOffersViewController.h"

#import "LoadingImageView.h"

#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "UIPopoverListView.h"
#import "UIDevice+Util.h"
#import "AppDelegate.h"
#import "RestaurantsViewController.h"
#import "TrackHandler.h"
#import "AddToListController.h"
#import "JournalActivityItemProvider.h"
#import "UILikeCommentShareView.h"
#import "UICommentInputView.h"
#import "Date.h"
#import "AlbumViewController.h"
#import "LikeView.h"
#import "HomeUtil.h"
#import "YTPlayerView.h"
#import "OtherProfileViewController.h"
#import <GoogleAnalytics/GAIFields.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import "SelectPaxViewController.h"
#import "JournalListViewController.h"
#import "IMGDiningGuide.h"
#import "DiningGuideRestaurantsViewController.h"
#import "LoadingImageViewController.h"
#import "DetailDataHandler.h"
#import "V2_JournalHeaderView.h"
#import "V2_RelatedJournalView.h"
#import "WebServiceV2.h"
#import "V2_JournalBottomBarView.h"
#import "V2_CommentListViewController.h"
#import "DetailSeeAllPhotoViewController.h"
#import "V2_DiningGuideListViewController.h"
#import "DetailBottomButtonView.h"
#import "JournalGoFoodListView.h"

@interface JournalDetailViewController ()<MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate,UIPopoverListViewDataSource,UIPopoverListViewDelegate,UIScrollViewDelegate,CommentInputViewDelegate,LikeCommentShareDelegate,LikeListViewDelegate,JournalDetailViewControllerDelegate,JournalRestaurantDelegate,UITextViewDelegate>
{
    UIPopoverListView *poplistview;

    UIImageView *_photoImageView;
    float _currentPointY;
    NSMutableDictionary *_dataDicM;
    NSMutableArray *components;
    UIScrollView *_scrollView;
    NSMutableArray *_relatedArrM;
    int _currentRestaurantTag;
    NSMutableDictionary *_saveBtnsDicM;
    NSMutableDictionary *_likeBtnsDicM;
    NSMutableArray *_imageArrM;
    UILabel *_likeCountLabel;
    NSMutableArray *regularTimeArray;
    NSMutableArray *restaurantOfferArray;
    
    UIView *relatedPostView;
    
    UILikeCommentShareView *likeCommentShareView;
   // UICommentInputView *commentInputView;

    NSNumber *currentOffsetY;
    NSNumber *_menuPhotoCount;
    
    UILabel *likeLabel;
    UILabel *commentLabel;
    UILabel *listLabel;
    BOOL isEditComment;
    LikeView* likepopView;
    NSMutableArray* likeListArr;
    
    NSMutableArray *titleViewYArr;
    NSMutableArray *titleViewArr;
    long currentTitleIndex;
    CGFloat lastY;
    CGFloat preOffset;
    UIView *currentView;
    
    CGFloat leftY;
    CGFloat rightY;
    NSMutableArray* webViewArr;
    CGRect likesharecommentFrame;
    NSTimeInterval beginLoadingData;
    NSTimeInterval finishUI;
    
    NSNumber* componentoffset;
    NSNumber* componentCount;
    NSMutableDictionary *loadStatus;
    UIActivityIndicatorView *activityIndicator;
    CGFloat freshY;
    UIButton *rightBarItem;
    MapViewController *mapViewController;
    MKMapView *mapView;
    NSMutableArray *restaurantArray;
    JournalRestaurant *currentJournalResView;
    BOOL isMapSaved;
    V2_JournalBottomBarView *bottomBarView;
    SavedMapListView *currentSavedMapView;
    
    IMGShimmerView *shimmerView;
    
    NSNumber *currentGoFoodCount;
    CGFloat bottomHeight;
    NSMutableArray *goFoodArr;
}
@end

@implementation JournalDetailViewController


- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
//    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillShowNotification];
//    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillHideNotification];
    [[Amplitude instance] logEvent:@"AT - Article Page Dismiss"];
    
    for (UIView* view in webViewArr) {
        if ([view isKindOfClass:[YTPlayerView class]]) {
//            NSURL *url = [NSURL URLWithString:@""];
//            NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
//            [(UIWebView*)view loadRequest:requestObj];
            [(YTPlayerView*)view stopVideo];
        }
    }
    
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //[self.navigationItem setTitle:@"Qraved Journal"];
    
    self.navigationController.navigationBarHidden = NO;
    
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    if (self.amplitudeType!=nil) {
        [eventProperties setValue:self.amplitudeType forKey:@"Location"];
    }
    if (self.journal) {
        if (self.journal.title) {
            [eventProperties setValue:self.journal.title forKey:@"Article title"];
        }
        if (self.journal.journalTitle) {
            [eventProperties setValue:self.journal.journalTitle forKey:@"Article title"];
        }
        if (self.journal.categoryName) {
            [eventProperties setValue:self.journal.categoryName forKey:@"Article category"];
        }
        if (self.journal.contents) {
            [eventProperties setValue:self.journal.contents forKey:@"Article content"];
        }
        if (self.journal.createdDate) {
            [eventProperties setValue:self.journal.createdDate forKey:@"Post time"];
        }
    }

    [[Amplitude instance] logEvent:@"RC - View Journal detail" withEventProperties:eventProperties];
    [[AppsFlyerTracker sharedTracker] trackEvent:@"RC - View Journal detail" withValues:eventProperties];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // [MixpanelHelper trackOpenJournalArticleWithArticleId:_journal.mediaCommentId andArticleTitle:_journal.title];
    self.screenName = @"Journal detail page";
    goFoodArr = [NSMutableArray array];
    restaurantArray = [NSMutableArray array];
    
    IMGUser *user = [IMGUser currentUser];
    if ([_journal.mediaCommentId intValue]) {
        [TrackHandler trackWithUserId:user.userId andJournalId:_journal.mediaCommentId type:[NSNumber numberWithInt:0] other:nil];
    }
    _currentPointY = 0;
    _currentRestaurantTag = 1;
    _saveBtnsDicM = [[NSMutableDictionary alloc] init];
    _likeBtnsDicM = [[NSMutableDictionary alloc] init];
    _imageArrM = [[NSMutableArray alloc] init];
    
    [self setBackBarButtonOffset30];

    _scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:_scrollView];
    _scrollView.backgroundColor = [UIColor whiteColor];
    _scrollView.frame = self.view.bounds;
    _scrollView.delegate=self;
    NSLog(@"%f,getJournalDetailData start time",[[NSDate date]timeIntervalSince1970]);
    beginLoadingData = [[NSDate date]timeIntervalSince1970];

    activityIndicator = [[UIActivityIndicatorView alloc]
                         initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.frame = CGRectMake(DeviceWidth/2 - 10, _scrollView.contentSize.height-5, 10, 10);
    [_scrollView addSubview:activityIndicator];
    activityIndicator.hidden = YES;
    
    shimmerView = [[IMGShimmerView alloc] initWithFrame:self.view.bounds];
    [shimmerView createJDPShimmerView];
    [self.view addSubview:shimmerView];
    
    componentoffset = [NSNumber numberWithLong:0];
    loadStatus = [[NSMutableDictionary alloc] init];
    
    [JournalHandler getJournalDetailWithJournal:_journal andJournalBlock:^(IMGMediaComment *journal, NSNumber *goFoodCount) {
        _journal = journal;
        currentGoFoodCount = goFoodCount;
        if (goFoodCount && [goFoodCount intValue]>0) {
            [self loadGoFoodRestaurantList];
        }
        NSLog(@"%f,getJournalDetailData deal 1.0",[[NSDate date]timeIntervalSince1970]);
        [self addMainView];
        [shimmerView removeFromSuperview];
        
        finishUI = [[NSDate date] timeIntervalSince1970];
        float duringTime = finishUI - beginLoadingData;
        duringTime = duringTime * 1000;
        int duringtime = duringTime;
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createTimingWithCategory:@"JDP"  interval:[NSNumber numberWithFloat:duringtime] name:@"" label:@""] build]];
    
        NSMutableDictionary *eventDic = [NSMutableDictionary dictionary];
        [eventDic setValue:journal.mediaCommentId forKey:@"Journal_ID"];
        [eventDic setValue:self.amplitudeType forKey:@"Location"];
        
        [eventDic setValue:journal.journalTitle forKey:@"Journal title"];
        [eventDic setValue:journal.categoryName forKey:@"Journal category"];
        [eventDic setValue:journal.contents forKey:@"Journal content"];
        [eventDic setValue:journal.createdDate forKey:@"Post time"];
        
        [[Amplitude instance] logEvent:@"RC - View Journal detail" withEventProperties:eventDic];
        

    } andAllDataBlock:^(NSNumber *_componentCount,NSMutableArray *componentArr,NSArray *commentList_,NSNumber *commentCount_,NSNumber *menuPhotoCount) {
        NSLog(@"%f,getJournalDetailData deal 2.0",[[NSDate date]timeIntervalSince1970]);
        //[_dataArrM removeAllObjects];
        components=componentArr;
        
        for (NSDictionary *dic in componentArr) {
            [dic enumerateKeysAndObjectsUsingBlock:^(id key,id objc,BOOL *stop){
                if([key isEqualToString:@"restaurant"]){
            
                    IMGRestaurant * restaurant = [self returnRestaurantWithRestaurantDic:dic];
                    
                    [restaurantArray addObject:restaurant];
                }
            }];
        }
        
//
        if (restaurantArray.count>0) {
            [self createMap];
        }
        componentCount = _componentCount;
        if ( ([componentCount longValue] > 10) && (components.count < [componentCount longValue])) {
            componentoffset = [NSNumber numberWithLong:([componentoffset longValue] + 10)];
        }else{
            
            [mapViewController setMapDatas:restaurantArray offset:0];
        }
        
        _journal.commentList = [[NSMutableArray alloc]initWithArray:commentList_];
        _journal.commentCount=commentCount_;
        _menuPhotoCount = menuPhotoCount;
        // _dataDicM = [NSMutableDictionary dictionaryWithDictionary:dataDic];
        [self addNewRestaurantView];

    } andRelatedPostBlock:^(NSArray *relatedArr) {
        NSLog(@"%f,getJournalDetailData deal 3.0",[[NSDate date]timeIntervalSince1970]);
        [self getRelatedArticle];
            if ( ([componentCount longValue] > 10) && (components.count < [componentCount longValue])) {
            [self loadMoreNewRestaurantView];
        }
    }];
    
    
    

   //判断是否首次进去journal
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isFirstJournalPage"])

    {
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isFirstJournalPage"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
}

- (void)loadGoFoodRestaurantList{
    [JournalHandler getGoFoodRestaurantList:self.journal andBlock:^(NSArray *restaurantArr) {
        [goFoodArr addObjectsFromArray:restaurantArr];
    } andError:^(NSError *error) {
        
    }];
}

- (void)getRelatedArticle{
    
    [WebServiceV2 relatedArticleArray:@{@"article_id":_journal.mediaCommentId} andBlock:^(NSArray *relatedArr) {
        _relatedArrM = [NSMutableArray arrayWithArray:relatedArr];
        if (components.count == [componentCount longValue]) {
            [self addRelatedPostView];
        }
        
    } andError:^(NSError *error) {
        
    }];
}

- (void)createMap{
    
    rightBarItem = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBarItem.frame = CGRectMake(0, 0, 95, 22);
    rightBarItem.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [rightBarItem setTitle:@"View in Map" forState:UIControlStateNormal];
    [rightBarItem setTitleColor:[UIColor colorWithHexString:@"09BFD3"] forState:UIControlStateNormal];
    rightBarItem.selected = NO;
    rightBarItem.titleLabel.font = [UIFont systemFontOfSize:17];
    [rightBarItem addTarget:self action:@selector(mapButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc] initWithCustomView:rightBarItem];

    
    mapViewController=[[MapViewController alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-44)];
    mapViewController.fromSearchPage=YES;
    mapViewController.fromSaved = YES;
    mapViewController.restaurantsViewController=self;
    __weak typeof(self) weakSelf = self;
  
    mapViewController.savedClick = ^(IMGRestaurant *pressRestaurant, SavedMapListView *savedMapView){
        isMapSaved = YES;
        currentSavedMapView = savedMapView;
        JournalRestaurant *restaurantView = [weakSelf.view viewWithTag:[pressRestaurant.restaurantId intValue]];
        [weakSelf JournalRestaurantSaved:pressRestaurant andView:restaurantView];
    };
    mapView = mapViewController.mapView;
    
    mapView.frame = CGRectMake(0,0, DeviceWidth, self.view.frame.size.height);
    mapView.hidden = YES;
    [self.view addSubview:mapViewController.mapView];
    [self.view bringSubviewToFront:mapViewController.mapView];
    
//    [self.view bringSubviewToFront:mapBtn];
}

-(void)mapButtonTapped{
    rightBarItem.selected = !rightBarItem.selected;
    if (rightBarItem.selected) {
        
        [IMGAmplitudeUtil trackJDPWithName:@"RC - View Journal Listed Restaurant on Map" andJournalId:self.journal.mediaCommentId andOrigin:nil andChannel:nil andLocation:nil];
        
        _scrollView.hidden = YES;
        mapView.hidden = NO;
        [rightBarItem setTitle:@"List" forState:UIControlStateNormal];
        currentView.hidden = YES;
    }else{
        _scrollView.hidden = NO;
        mapView.hidden = YES;
        [rightBarItem setTitle:@"View in Map" forState:UIControlStateNormal];
        if (currentView) {
            currentView.hidden = NO;
        }
        
    }
}


- (void)addMainView
{

    IMGDish *dish = [[IMGDish alloc] init];
    dish.imageUrl = _journal.imageUrl;
    dish.sectionId = [NSNumber numberWithInt:0];
    
    dish.userPhotoCountDic = _journal.userPhotoCountDic;
    dish.photoCreditTypeDic  = _journal.photoCreditTypeDic;
    dish.userIdDic  = _journal.userIdDic;
    dish.photoCreditDic  = _journal.photoCreditDic;
    dish.userReviewCountDic  = _journal.userReviewCountDic;
    dish.userAvatarDic  = _journal.userAvatarDic;
    dish.photoCreditUrlDic  = _journal.photoCreditUrlDic;
    dish.restaurantIdDic  = _journal.restaurantIdDic;
    dish.createTime = [NSNumber numberWithLongLong:[_journal.createTimeUnixTimestamp longLongValue]];
    dish.JournalGifH=_journal.JournalGifH;
    dish.JournalGifW=_journal.JournalGifW;
    
    [_imageArrM addObject:dish];
    V2_JournalHeaderView *headerView = [[V2_JournalHeaderView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 100)];
    headerView.journal = _journal;
    headerView.imageClick = ^{
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:@"Journal detail page" forKey:@"Location"];
        [eventProperties setValue:self.journal.restaurantIdDic forKey:@"Restaurant_ID"];
        [eventProperties setValue:@"Restaurant photo" forKey:@"Type"];
        [[Amplitude instance] logEvent:@"CL - See All Photos " withEventProperties:eventProperties];
        
        NSInteger tag;
        for (IMGDish *dish in _imageArrM)
        {
            if ([dish.sectionId intValue] == 0) {
                
                tag=[_imageArrM indexOfObject:dish];
                break;
            }
        }
        
        NSMutableDictionary *param = [NSMutableDictionary dictionary];
        [param setValue:self.journal.mediaCommentId forKey:@"Journal_ID"];
        [param setValue:self.journal.restaurantIdDic forKey:@"Restaurant_ID"];
        [[Amplitude instance] logEvent:@"RC - View Journal photo" withEventProperties:param];
        
        AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:_imageArrM andPhotoTag:tag andRestaurant:nil andFromDetail:YES];
        albumViewController.amplitudeTitle = @"RC - View Journal photo";
        albumViewController.journalId=_journal.mediaCommentId;
        albumViewController.showPage=NO;
        albumViewController.isJournal = YES;
        [self.navigationController pushViewController:albumViewController animated:YES];
    };
   
    [_scrollView addSubview:headerView];
    headerView.frame = CGRectMake(0, 0, DeviceWidth, headerView.journalHeight);
    
    _currentPointY = headerView.journalHeight;
    _scrollView.contentSize = CGSizeMake(0, headerView.endPointY);

    NSLog(@"%f,getJournalDetailData UI 1.0",[[NSDate date]timeIntervalSince1970]);
    
    [self createBottomBar];
}

- (void)refreshGoFoodRestaurantList:(IMGRestaurant *)restaurant andIsSaved:(BOOL)isSaved{
    for (IMGRestaurant *model in goFoodArr) {
        if ([model.restaurantId isEqual:restaurant.restaurantId]) {
            model.saved = [NSNumber numberWithBool:isSaved];
            break;
        }
    }
}

- (void)reFreshRestaurantView:(IMGRestaurant *)restaurant{
    JournalRestaurant *restaurantView = [self.view viewWithTag:[restaurant.restaurantId intValue]];
    [restaurantView refreshSavedButton:[restaurant.saved boolValue]];
    for (IMGRestaurant *res  in restaurantArray) {
        if ([res.restaurantId isEqual:restaurant.restaurantId]) {
            res.saved = restaurant.saved;
            break;
        }
    }
    [mapViewController setMapDatas:restaurantArray offset:0];
}

- (void)goFoodTapped{
    [IMGAmplitudeUtil trackJDPWithName:@"CL - Order Now" andJournalId:self.journal.mediaCommentId andOrigin:nil andChannel:nil andLocation:nil];
    
    if (goFoodArr.count > 1) {
        JournalGoFoodListView *goFoodListView = [[JournalGoFoodListView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHEIGHT)];
        goFoodListView.restaurantArray = goFoodArr;
        goFoodListView.journalId = self.journal.mediaCommentId;
        goFoodListView.controller = self;
        goFoodListView.freshRestaurantList = ^(IMGRestaurant *restaurant) {
            [self reFreshRestaurantView:restaurant];
        };
        [[AppDelegate ShareApp].window addSubview:goFoodListView];
        
    }else if (goFoodArr.count == 1){
        IMGRestaurant *restaurant = [goFoodArr firstObject];
        [self goFoodLink:restaurant];
    }
}


- (void)goFoodLink:(IMGRestaurant *)restaurant{
    [IMGAmplitudeUtil trackGoFoodWithName:@"CL - Go-Food CTA" andJournalId:self.journal.mediaCommentId andRestaurantId:restaurant.restaurantId andPhotoId:nil andLocation:@"JDP" andOrigin:nil andOrder:nil];
    [CommonMethod goFoodButtonTapped:restaurant andViewController:self andRestaurantState:[restaurant isOpenRestaurant] andCallBackBlock:^(BOOL isSaved) {
        restaurant.saved = [NSNumber numberWithBool:isSaved];
        [self reFreshRestaurantView:restaurant];
    }];
}

- (void)createBottomBar{
    if (currentGoFoodCount != nil && [currentGoFoodCount intValue] > 0) {
        bottomHeight = 72;
        DetailBottomButtonView *bottomButtonView = [[DetailBottomButtonView alloc] initWithFrame:CGRectMake(0, DeviceHEIGHT - IMG_StatusBarAndNavigationBarHeight - 72, DeviceWidth, 72)];
        [self.view addSubview:bottomButtonView];
        
        FunctionButton *goFoodButton = [FunctionButton buttonWithType:UIButtonTypeCustom];
        goFoodButton.frame = CGRectMake(LEFTLEFTSET, LEFTLEFTSET, DeviceWidth - LEFTLEFTSET*2, FUNCTION_BUTTON_HEIGHT);
        [goFoodButton addTarget:self action:@selector(goFoodTapped) forControlEvents:UIControlEventTouchUpInside];
        [bottomButtonView addSubview:goFoodButton];
    }else{
        bottomHeight = 50;
        bottomBarView = [[V2_JournalBottomBarView alloc] initWithFrame:CGRectMake(0, DeviceHeight+20-64-50, DeviceWidth, 50)];
        __weak typeof(self) weakSelf = self;
        bottomBarView.copyLink = ^(){
            [weakSelf copyLink];
        };
        bottomBarView.shareToLine = ^(){
            [weakSelf shareToLine];
        };
        bottomBarView.shareToFB = ^(){
            [weakSelf shareToFaceBook];
        };
        bottomBarView.pressLiked = ^(){
            [weakSelf likeButtonTapped];
        };
        bottomBarView.pressLikedCount = ^(){
            [weakSelf likeListButtonTapped];
        };
        bottomBarView.pressComment = ^(){
            [weakSelf commentButtonTapped];
        };
        
        bottomBarView.journal = self.journal;
        [self.view addSubview:bottomBarView];
    }
}

- (void)likeButtonTapped{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
    [JournalHandler likeJournal:_journal.isLike articleId:_journal.mediaCommentId andBlock:^{
    }failure:^(NSString *exceptionMsg){
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
    }];
    [self.view endEditing:YES];
    _journal.isLike=!_journal.isLike;
    if(_journal.isLike){
        _journal.likeCount=[NSNumber numberWithInt:[_journal.likeCount intValue]+1];
        [IMGAmplitudeUtil trackJDPWithName:@"UC - Like Journal Succeed" andJournalId:self.journal.mediaCommentId andOrigin:nil andChannel:nil andLocation:@"Journal detail page"];
    }else{
        _journal.likeCount=[NSNumber numberWithInt:[_journal.likeCount intValue]-1];
        [IMGAmplitudeUtil trackJDPWithName:@"UC - UnLike Journal Succeed" andJournalId:self.journal.mediaCommentId andOrigin:nil andChannel:nil andLocation:@"Journal detail page"];
    }
    [likeCommentShareView updateLikeButtonStatus:_journal.isLike likeCount:[_journal.likeCount intValue]];
    [bottomBarView updateLikeButtonStatus:_journal.isLike likeCount:[_journal.likeCount intValue]];
}

-(void)likeLabelTap:(UITapGestureRecognizer*)tap
{
    [[Amplitude instance] logEvent:@" RC - View People Who Likes"];

    if (likepopView) {
        [likepopView removeFromSuperview];
    }

    [[LoadingView sharedLoadingView]startLoading];
    [HomeUtil getLikeListFromServiceWithJournalArticleId:_journal andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
        [[LoadingView sharedLoadingView]stopLoading];
        likeListArr=[[NSMutableArray alloc]initWithArray:likeListArray];
        likepopView.likecount=likeviewcount;
        likepopView.likelistArray=likeListArr;
        
    }];

    likepopView=[[LikeView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    likepopView.delegate=self;
    [self.view addSubview:likepopView];
 
}
-(void)LikeviewLoadMoreData:(NSInteger)offset
{
        [HomeUtil getLikeListFromServiceWithJournalArticleId:_journal andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
             likepopView.likecount=likeviewcount;
            [likeListArr addObjectsFromArray:likeListArray];
            [likepopView.likeUserTableView reloadData];
    }];

}
-(void)returnLikeCount:(NSInteger)count
{
//    CGFloat startPointY=_currentPointY;
//    
//    float start=LEFTLEFTSET;
    if (count!=0) {
        
    
    NSString *likeString=[NSString stringWithFormat:@"%ld %@",(long)count,count>1?L(@"Likes"):L(@"Like")];
      // CGSize likeSize=[likeString sizeWithFont:likeLabel.font constrainedToSize:CGSizeMake(DeviceWidth,20) lineBreakMode:NSLineBreakByWordWrapping];
  //  likeLabel.frame=CGRectMake(start, startPointY+10, likeSize.width,20);
    [likeLabel setText:likeString];
     likeLabel.frame=CGRectMake(likeLabel.frame.origin.x, likeLabel.frame.origin.y, likeLabel.expectedWidth, likeLabel.frame.size.height);
    }
}
- (void)likeCountClick:(UIButton *)likeCountBtn
{
    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        LoginViewController *loginVC=[[LoginViewController alloc]initWithFromWhere:[NSNumber numberWithInt:1]];
        UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
        [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
        
        return;
    }
    [_likeCountLabel setText:[NSString stringWithFormat:@"%d %@",[_journal.likeCount intValue]+1,[_journal.likeCount intValue] +1 >1?L(@"likes"):L(@"like")]];
    likeCountBtn.userInteractionEnabled = NO;
    [JournalHandler journalLikeWithJournal:_journal andUser:user];
    
}
- (void)addRelatedPostView
{
    if (relatedPostView) {
        [relatedPostView removeFromSuperview];
    }
     _scrollView.contentSize = CGSizeMake(0, _currentPointY+bottomHeight);
    relatedPostView = [[UIView alloc]initWithFrame:CGRectMake(0, likeCommentShareView.endPointY, DeviceWidth, 0)];
    [_scrollView addSubview:relatedPostView];
    if (_relatedArrM.count == 0)
    {
        _scrollView.contentSize = CGSizeMake(0, _currentPointY+bottomHeight);
    }else{
        
        UIView *spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 6)];
        spaceView.backgroundColor = [UIColor colorEBEBEB];
        [relatedPostView addSubview:spaceView];
        
        V2_RelatedJournalView *relatedView = [[V2_RelatedJournalView alloc] initWithFrame:CGRectMake(0, 6, DeviceWidth, 0)];
        relatedView.relatedArray = _relatedArrM;
        [relatedView setGotoJournalDetail:^(IMGMediaComment *relatedjournal) {
            [self relatedJournalClick:relatedjournal];
        }];
        [relatedPostView addSubview:relatedView];
        relatedView.frame = CGRectMake(0, 6, DeviceWidth, relatedView.relatedHeight);

        relatedView.backgroundColor = [UIColor whiteColor];
        relatedPostView.frame=CGRectMake(relatedPostView.startPointX, relatedPostView.startPointY, relatedPostView.frame.size.width, relatedView.relatedHeight+6);
        _currentPointY+=relatedView.relatedHeight+6;
//        _scrollView.contentSize = CGSizeMake(0, _currentPointY+75);
    }
//    if (commentInputView) {
//        [commentInputView removeFromSuperview];
//    }
//    commentInputView = [[UICommentInputView alloc]initView:NO];
//    commentInputView.frame=CGRectMake(0, relatedPostView.endPointY, commentInputView.frame.size.width, commentInputView.frame.size.height);
//    commentInputView.commentInputViewDelegate=self;
//    [_scrollView addSubview:commentInputView];
//    _currentPointY = commentInputView.endPointY;
    _scrollView.contentSize = CGSizeMake(0, _currentPointY + 44 + bottomHeight);
    //_currentPointY=_scrollView.contentSize.height;
            NSLog(@"%f,getJournalDetailData   UI 3.0",[[NSDate date]timeIntervalSince1970]);
}

- (void)addLineImage:(CGFloat)currentPointY withCurrentView:(UIView *)currentView
{
    UIImageView *lineImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, currentPointY+5, DeviceWidth, 1)];
    lineImage3.image = [UIImage imageNamed:@"CutOffRule"];
    currentPointY = lineImage3.endPointY;
    [currentView addSubview:lineImage3];
}

- (void)addShortLineImage:(CGFloat)currentPointY withCurrentView:(UIView *)currentView
{
    UIImageView *lineImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(15, currentPointY, DeviceWidth-LEFTLEFTSET*2, 1)];
    lineImage3.image = [UIImage imageNamed:@"CutOffRule"];
    currentPointY = lineImage3.endPointY;
    [currentView addSubview:lineImage3];
}

- (void)relatedJournalClick:(IMGMediaComment *)journal{
    
    [IMGAmplitudeUtil trackJDPWithName:@"RC - View Journal detail" andJournalId:journal.mediaCommentId andOrigin:@"Related journal on JDP" andChannel:nil andLocation:nil];
    
    if ([journal.imported intValue]== 1)
    {
        //imageview.userInteractionEnabled=NO;
        [JournalHandler getJournalDetailWithJournalArticleId:journal.mediaCommentId andBlock:^(NSString *content,NSString *webSite) {
            journal.contents = content;
            WebViewController *wvc = [[WebViewController alloc] init];
            wvc.content = journal.contents;
            wvc.journal = journal;
            wvc.title = journal.title;
            wvc.fromDetail = NO;
            wvc.webSite = webSite;
            [self.navigationController pushViewController:wvc animated:YES];
            //imageview.userInteractionEnabled=YES;
            
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:journal.title forKey:@"JournalTitle"];
            [eventProperties setValue:journal.restaurantTitle forKey:@"RestaurantTitle"];
            [eventProperties setValue:journal.restaurantId forKey:@"RestaurantId"];
            [eventProperties setValue:@"Journal detail page" forKey:@"Location"];
            [[Amplitude instance] logEvent:@"CL - Related Journal List" withEventProperties:eventProperties];
        }];
        
        
        
    }
    else
    {
        JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
        jdvc.amplitudeType = @"Journal detail page - related journal";
        jdvc.journal = journal;
        jdvc.categoryDataArrM = [NSMutableArray arrayWithArray:_categoryDataArrM];
        [self.navigationController pushViewController:jdvc animated:YES];
        
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:journal.title forKey:@"JournalTitle"];
        [eventProperties setValue:journal.restaurantTitle forKey:@"RestaurantTitle"];
        [eventProperties setValue:journal.restaurantId forKey:@"RestaurantId"];
        [eventProperties setValue:@"Journal detail page" forKey:@"Location"];
        [[Amplitude instance] logEvent:@"CL - Related Journal List" withEventProperties:eventProperties];
    }

}

 - (void)imageClick:(UITapGestureRecognizer *)tap
{
    NSInteger tag = tap.view.tag;
    UIImageView* imageview=(UIImageView*)tap.view;
    IMGMediaComment *journal = [_relatedArrM objectAtIndex:tag];
    if ([journal.imported intValue]== 1)
    {
        imageview.userInteractionEnabled=NO;
        [JournalHandler getJournalDetailWithJournalArticleId:journal.mediaCommentId andBlock:^(NSString *content,NSString *webSite) {
            journal.contents = content;
            WebViewController *wvc = [[WebViewController alloc] init];
            wvc.content = journal.contents;
            wvc.journal = journal;
            wvc.title = journal.title;
            wvc.fromDetail = NO;
            wvc.webSite = webSite;
            [self.navigationController pushViewController:wvc animated:YES];
            imageview.userInteractionEnabled=YES;

            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:journal.title forKey:@"JournalTitle"];
            [eventProperties setValue:journal.restaurantTitle forKey:@"RestaurantTitle"];
            [eventProperties setValue:journal.restaurantId forKey:@"RestaurantId"];
            [eventProperties setValue:@"Journal detail page" forKey:@"Location"];
            [[Amplitude instance] logEvent:@"CL - Related Journal List" withEventProperties:eventProperties];
        }];
        
        
        
    }
    else
    {
        JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
        jdvc.amplitudeType = @"Journal detail page - related journal";
        jdvc.journal = journal;
        jdvc.categoryDataArrM = [NSMutableArray arrayWithArray:_categoryDataArrM];
        [self.navigationController pushViewController:jdvc animated:YES];
        
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:journal.title forKey:@"JournalTitle"];
        [eventProperties setValue:journal.restaurantTitle forKey:@"RestaurantTitle"];
        [eventProperties setValue:journal.restaurantId forKey:@"RestaurantId"];
        [eventProperties setValue:@"Journal detail page" forKey:@"Location"];
        [[Amplitude instance] logEvent:@"CL - Related Journal List" withEventProperties:eventProperties];
    }
}
- (void)imageClicked
{
    IMGMediaComment *journal = [_relatedArrM objectAtIndex:0];
    if ([journal.imported intValue]== 1)
    {
        WebViewController *wvc = [[WebViewController alloc] init];
        wvc.journal = journal;
        wvc.fromDetail = NO;
        [self.navigationController pushViewController:wvc animated:YES];
    }
    else
    {
        JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
        jdvc.journal = journal;
        jdvc.categoryDataArrM = [NSMutableArray arrayWithArray:_categoryDataArrM];
        [self.navigationController pushViewController:jdvc animated:YES];
    }
}
-(void)gotoRestaurantDetail:(UIGestureRecognizer*)tap{
    IMGRestaurant *restaurant = [_dataDicM objectForKey:[NSString stringWithFormat:@"%d",(int)tap.view.tag]];
    DetailViewController *detailViewController = [[DetailViewController alloc] initWithRestaurant:restaurant];
    detailViewController.amplitudeType = @"JDP";
    detailViewController.amplitudeTypePage = @"Journal_ID";
    detailViewController.amplitudeTypePageId = _journal.mediaCommentId;
    [self.navigationController pushViewController:detailViewController animated:YES];
}

- (void)addNewRestaurantView
{
    titleViewYArr = [[NSMutableArray alloc] init];
    titleViewArr = [[NSMutableArray alloc] init];
    webViewArr=[[NSMutableArray alloc]init];
    float currentY;
    NSInteger count=components.count;
    for(int i=0;i<count;i++){
        currentY = _currentPointY;
        NSDictionary *component=[components objectAtIndex:i];
        JournalRestaurant *journalrest=[[JournalRestaurant alloc] initWithDictionary:component delegate:self menuPhotoCount:_menuPhotoCount];
        journalrest.backgroundColor=[UIColor clearColor];
        [journalrest setFrame:CGRectMake(0,_currentPointY+10,DeviceWidth,journalrest.viewHeight)];
        [_scrollView addSubview:journalrest];
        
        [component enumerateKeysAndObjectsUsingBlock:^(id key,id objc,BOOL *stop){
            if([key isEqualToString:@"restaurant"]){
                journalrest.tag = [[objc objectForKey:@"restaurantId"] intValue];
            }
        }];
        
        if ([journalrest.webview isKindOfClass:[YTPlayerView class]]) {
            [webViewArr addObject:journalrest.webview];
        }
        
        if([component valueForKey:@"photo"]){
            NSDictionary *photo=[component objectForKey:@"photo"];
            IMGDish *dish = [[IMGDish alloc] init];
            dish.imageUrl = [photo objectForKey:@"photo"];
            dish.sectionId = [photo objectForKey:@"order"];
            [dish updatePhotoCredit:[photo objectForKey:@"photoCreditImpl"]];
            dish.restaurantTitleDic = [[photo objectForKey:@"photoCreditImpl"] objectForKey:@"restaurantTitle"];
            dish.JournalGifH=[[photo objectForKey:@"height"] floatValue];
            dish.JournalGifW=[[photo objectForKey:@"width"] floatValue];
            dish.createTime = [NSNumber numberWithLongLong:[_journal.createTimeUnixTimestamp longLongValue]];
            [_imageArrM addObject:dish];
        }
        _currentPointY=journalrest.endPointY;
        
        UIView *titleBackgroundCopyView = journalrest.titleBackgroundCopyView;
        if (titleBackgroundCopyView!=nil) {
            
            titleBackgroundCopyView.frame = CGRectMake(0, 0, titleBackgroundCopyView.frame.size.width, titleBackgroundCopyView.frame.size.height);
            [self.view addSubview:titleBackgroundCopyView];
            
            [titleViewArr addObject:titleBackgroundCopyView];
            [titleViewYArr addObject:[NSNumber numberWithFloat:([journalrest.titleBackgroundCopyViewY floatValue]+currentY+10)]];
            
        }
    }
    lastY = _currentPointY;
    currentTitleIndex = -1;
    leftY = 0;
    rightY = 0;
    _scrollView.contentSize = CGSizeMake(0, _currentPointY+90);
    
    if (components.count == [componentCount longValue]) {
        [self addLikeCommentShareView];
    }
}


-(void)addLikeCommentShareView{
    
    [self updateLikeCommentListCountLabel:YES];

    likeCommentShareView = [[UILikeCommentShareView alloc]initInSuperView:_scrollView atStartX:0 startY:_currentPointY+10 commentCount:[_journal.commentCount intValue] commentArray:_journal.commentList liked:_journal.isLike withFooter:NO needCommentListView:NO likeCount:[_journal.likeCount intValue]];
    
    likesharecommentFrame=likeCommentShareView.frame;
    likeCommentShareView.likeCommentShareDelegate=self;
    _currentPointY = likeCommentShareView.endPointY+10;
    _scrollView.contentSize = CGSizeMake(0, _currentPointY+10);
    _currentPointY=_scrollView.contentSize.height;
           NSLog(@"%f,getJournalDetailData  UI 2.0",[[NSDate date]timeIntervalSince1970]);
}

-(void)updateLikeCommentListCountLabel:(BOOL)initial{
    
    if(likeLabel.hidden==NO){
        //_currentPointY=likeLabel.startPointY-10;
    }else if(commentLabel.hidden==NO){
        //_currentPointY=commentLabel.startPointY-10;
    }else if(listLabel.hidden==NO){
        //_currentPointY=listLabel.startPointY-10;
    }else if(!initial){
        _currentPointY=likeCommentShareView.startPointY-10;
    }
    //_currentPointY=likeCommentShareView.startPointY-10;
    CGFloat startPointY=_currentPointY;

    
    if(!initial){
        [self.view endEditing:YES];
        if(_currentPointY-startPointY>0.1){
            likeCommentShareView.frame=CGRectMake(0, _currentPointY+10, likeCommentShareView.frame.size.width, likeCommentShareView.frame.size.height);
            
            _currentPointY = likeCommentShareView.endPointY+10;

        }
        if(relatedPostView){
            relatedPostView.frame=CGRectMake(0, likeCommentShareView.endPointY+10-20, relatedPostView.frame.size.width, relatedPostView.frame.size.height);
            _currentPointY = relatedPostView.endPointY+10;
        }
        
        _currentPointY = _currentPointY+COMMUNITYBUTTONVIEWHEIGHT+10;
        _scrollView.contentSize = CGSizeMake(0, _currentPointY+bottomHeight);
        _currentPointY=_scrollView.contentSize.height;
    }
}

-(void)JournalRestaurantViewDetail:(IMGRestaurant*)restaurant{
    
    
    DetailViewController *detailViewController = [[DetailViewController alloc] initWithRestaurant:restaurant];
    detailViewController.amplitudeType = @"JDP";
    detailViewController.amplitudeTypePage = @"Journal_ID";
    detailViewController.amplitudeTypePageId = _journal.mediaCommentId;
    detailViewController.isFromAppDelegate = YES;
    
    
    
    [self.navigationController pushViewController:detailViewController animated:YES];
}

-(void)seeMoreRestaurantPhotos:(IMGRestaurant*)restaurant{
//    SearchPhotosViewController *searchPhotosViewController=[[SearchPhotosViewController alloc] initWithRestaurant:restaurant];
//    [self.navigationController pushViewController:searchPhotosViewController animated:YES];
    DetailSeeAllPhotoViewController *detailSeeAllPhotoV = [[DetailSeeAllPhotoViewController alloc] initWithRestaurant:restaurant restaurantPhotoArray:nil];
    detailSeeAllPhotoV.isUseWebP = YES;
    [self.navigationController pushViewController:detailSeeAllPhotoV animated:YES];
}

-(void)JournalRestaurantBtnClick:(NSNumber *)resturantId withOfferId:(NSNumber *)offerId{
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:_journal.mediaCommentId forKey:@"Journal_ID"];
    [param setObject:@"Journal detail page" forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL - Journal main CTA" withEventProperties:param];
    if ([offerId intValue]!=0) {
        [[LoadingView sharedLoadingView] startLoading];
        [DetailDataHandler getRestaurantFirstDataFromServiceNow:resturantId andBlock:^(IMGRestaurant *restaurant, NSArray *eventList, IMGRestaurantOffer *restaurantOffer, NSArray *banerImageArr, IMGRDPRestaurant *rdpRest, NSNumber *showClaim,NSArray *couponArray) {
            [[LoadingView sharedLoadingView] stopLoading];
            SpecialOffersViewController *specialOffersViewController = [[SpecialOffersViewController alloc]initWithRestaurant:restaurant andTimeArray:nil andOfferArray:nil];
            specialOffersViewController.journalDetailOfferId=offerId;
            [self.navigationController pushViewController:specialOffersViewController animated:YES];
        }];
    }else{
    
        [IMGAmplitudeUtil trackJDPWithName:@"CL - Journal Main CTA" andJournalId:self.journal.mediaCommentId andOrigin:nil andChannel:nil andLocation:@"Journal detail page"];
        
        DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:resturantId];
        dvc.amplitudeType = @"JDP";
        dvc.amplitudeTypePage = @"Journal_ID";
        dvc.amplitudeTypePageId = _journal.mediaCommentId;
        [self.navigationController pushViewController:dvc animated:YES];

    }
}

-(void)JournalRestaurantSeeMenu:(IMGRestaurant*)restaurant{
    
    [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Menu list" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:nil andOrigin:@"Journal detail page" andType:nil];

    MenuPhotoViewColltroller *menuPhotoViewColltroller = [[MenuPhotoViewColltroller alloc]initWithPhotosArray:nil andPhotoTag:0 andRestaurant:restaurant andFromDetail:NO];
    menuPhotoViewColltroller.amplitudeType = @"Article detail page";
    menuPhotoViewColltroller.ifFromJournal = YES;
    [self.navigationController pushViewController:menuPhotoViewColltroller animated:YES];
}
-(void)JournalRestaurantSeeMap:(IMGRestaurant*)restaurant{
    
    [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Map" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:@"Journal detail page" andOrigin:nil andType:nil];
    
    IMGUser *user = [IMGUser currentUser];
    NSNumber *userId = [[NSNumber alloc] init];
    if (user.userId != nil){
        userId = user.userId;
    }
    else
        userId = [NSNumber numberWithInt:0];
    
    // [TrackHandler trackWithUserId:userId andRestaurantId:restaurant.restaurantId andContent:[NSDictionary dictionaryWithObject:@"1" forKey:@"type"]];
    
    MapViewController *mapViewController=[[MapViewController alloc] initWithRestaurant:restaurant];
    mapViewController.amplitudeType = @"Article detail page";
    mapViewController.fromDetail = NO;
    [self.navigationController pushViewController:mapViewController animated:YES];
}
-(void)JournalRestaurantBookNow:(IMGRestaurant*)restaurant{
    NSMutableDictionary *eventProperties = [[NSMutableDictionary alloc] init];
    [eventProperties setObject:@"Journal detail page" forKey:@"Location"];
    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
    [[Amplitude instance] logEvent:@"CL - Book Now" withEventProperties:eventProperties];
    
    [self initRestaurantOfferFromLocal:restaurant];
    [RestaurantHandler geOfferFromServer:restaurant.restaurantId andBookDate:nil andBlock:^(NSMutableArray *restaurantOfferArray_) {
        SelectPaxViewController *selectDinersViewController = [[SelectPaxViewController alloc]initWithRestaurant:restaurant andTimeArray:nil andOfferArray:restaurantOfferArray_ andOffer:nil];
        [self.navigationController pushViewController:selectDinersViewController animated:YES];
        
    }];

    
}

-(void)JournalRestaurantViewOffer:(IMGRestaurant*)restaurant{
    SpecialOffersViewController *specialOffersViewController = [[SpecialOffersViewController alloc] initWithRestaurant:restaurant andTimeArray:nil andOfferArray:nil];
    specialOffersViewController.amplitudeType = @"Journal detail page";
    [self.navigationController pushViewController:specialOffersViewController animated:YES];
}


- (void)JournalRestaurantSaved:(IMGRestaurant*)restaurant andView:(UIView *)journalView{

    currentJournalResView = (JournalRestaurant *)journalView;
    restaurant.menuCount = _menuPhotoCount;
    
    [CommonMethod doSaveWithRestaurant:restaurant andViewController:self andCallBackBlock:^(BOOL isSaved) {
        [currentJournalResView refreshSavedButton:isSaved];
        if (currentSavedMapView) {
            [currentSavedMapView refreshSavedButtonStyle:isSaved];
        }
        [self refreshGoFoodRestaurantList:restaurant andIsSaved:isSaved];
        if (isSaved) {
            [IMGAmplitudeUtil trackRestaurantWithName:@"BM - Save Restaurant Succeed" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:@"Want to go Lis" andOrigin:@"JDP" andType:@"Custom List"];
        }
        for (IMGRestaurant *res  in restaurantArray) {
            if ([res.restaurantId isEqual:restaurant.restaurantId]) {
                res.saved = [NSNumber numberWithBool:isSaved];
                break;
            }
        }
        if (!isMapSaved) {
            isMapSaved = NO;
            [mapViewController setMapDatas:restaurantArray offset:0];
        }
    }];
}


- (void)JournalRestaurantLike:(NSNumber*)restaurantId liked:(BOOL)liked andBlock:(void(^)(BOOL))block
{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:@"Favorite" forKey:@"Type"];
    [param setObject:@"Article detail" forKey:@"Location"];
    [param setObject:restaurantId forKey:@"Restaurant_ID"];
    [param setObject:@"userFavorite" forKey:@"List_ID"];
    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        LoginViewController *loginVC=[[LoginViewController alloc] initWithFromWhere:[NSNumber numberWithInt:1]];
        UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
        [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
        return;
    }
    NSString *postUrl = @"user/restaurant/favorite";
    if(liked){
        postUrl = @"user/restaurant/unfavorite";
        [[Amplitude instance] logEvent:@"BM - Unsave Restaurant Initiate" withEventProperties:param];
    }else{
        [[Amplitude instance] logEvent:@"BM - Save Restaurant Initiate" withEventProperties:param];
    }
    //[[LoadingView sharedLoadingView]startLoading];
    NSDictionary * parameters = @{@"userID":user.userId,@"t":user.token,@"restaurantID":restaurantId,@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]};
    [[IMGNetWork sharedManager]POST:postUrl parameters:parameters progress:nil  success:^(NSURLSessionDataTask *operation, id responseObject) {
        
        NSString *exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
            return;
        }
        
        NSString *returnStatusString = [responseObject objectForKey:@"status"];
        if([returnStatusString isEqualToString:@"succeed"]){
            if(liked){
                [[Amplitude instance] logEvent:@"BM - Unsave Restaurant Succeed" withEventProperties:param];
            }else{
                [[Amplitude instance] logEvent:@"BM - Save Restaurant Succeed" withEventProperties:param];
                [[AppsFlyerTracker sharedTracker]trackEvent:@"BM - Save Restaurant Succeed" withValues:param];
            }
            block(YES);
            
            // if([restaurant.liked intValue] != 0){
            //     restaurant.liked = [NSNumber numberWithInt:0];
            //     for (UIView *view in likeBtn.subviews)
            //     {
            //         [view removeFromSuperview];
            //     }
            //     UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, likeBtn.frame.size.width, likeBtn.frame.size.height)];
            //     backgroundImage.image = [UIImage imageNamed:@"favoriteGrayImage"];
            //     [likeBtn addSubview:backgroundImage];
                
            // }else{
            //     restaurant.liked = [NSNumber numberWithInt:1];
            //     for (UIView *view in likeBtn.subviews)
            //     {
            //         [view removeFromSuperview];
            //     }
            //     UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, likeBtn.frame.size.width, likeBtn.frame.size.height)];
            //     backgroundImage.image = [UIImage imageNamed:@"favoriteRedImage"];
            //     [likeBtn addSubview:backgroundImage];
            // }
        }else{
            NSString *exceptionmsg = [responseObject objectForKey:@"exceptionmsg"];
            if (exceptionmsg!=nil) {
                [param setObject:exceptionmsg forKey:@"Reason"];
            }

            [[Amplitude instance] logEvent:@"BM - Save Restaurant Failed" withEventProperties:param];
        }
        //[[LoadingView sharedLoadingView]stopLoading];
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        if(liked){
            [param setObject:error.localizedDescription forKey:@"Reason"];
            [[Amplitude instance] logEvent:@"BM - Unsave Restaurant Failed" withEventProperties:param];
        }else{
            [[Amplitude instance] logEvent:@"BM - Save Restaurant Failed" withEventProperties:param];
        }
        NSLog(@"likeButtonTapped error: %@",error.localizedDescription);
        
        //[[LoadingView sharedLoadingView]stopLoading];
    }];
}

-(void)initRestaurantOfferFromLocal:(IMGRestaurant *)restaurant{
    NSString *currentDateStr;
    NSDateFormatter *formatter = [[NSDateFormatter  alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [NSDate date];
    currentDateStr = [formatter stringFromDate:date];
    regularTimeArray = [[NSMutableArray alloc] initWithCapacity:0];
    if(restaurantOfferArray!=nil){
        [restaurantOfferArray removeAllObjects];
    }else{
        restaurantOfferArray=[[NSMutableArray alloc] initWithCapacity:0];
    }
    
    [[DBManager manager] selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGRestaurantOffer WHERE restaurantId = '%@' and offerStartDate<='%@' and offerEndDate>='%@' and offerType=4 ORDER BY offerSlotMaxDisc desc;",restaurant.restaurantId,currentDateStr,currentDateStr] successBlock:^(FMResultSet *resultSet) {
        while ([resultSet next]) {
            IMGRestaurantOffer  *tmpRestaurantOffer=[[IMGRestaurantOffer alloc]init];
            [tmpRestaurantOffer setValueWithResultSet:resultSet];
            [restaurantOfferArray addObject:tmpRestaurantOffer];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"initRestaurantOfferFromLocal error when get datas:%@",error.description);
    }];
    
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGRestaurantOffer WHERE restaurantId = '%@' and offerStartDate<='%@' and offerEndDate>='%@' and offerType!=4 ORDER BY offerType,offerSlotMaxDisc desc;",restaurant.restaurantId,currentDateStr,currentDateStr] successBlock:^(FMResultSet *resultSet) {
        while ([resultSet next]) {
            IMGRestaurantOffer  *tmpRestaurantOffer=[[IMGRestaurantOffer alloc]init];
            [tmpRestaurantOffer setValueWithResultSet:resultSet];
            [restaurantOfferArray addObject:tmpRestaurantOffer];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"initRestaurantOfferFromLocal error when get datas:%@",error.description);
    }];

}

- (void)journalCreditClick:(NSString*)urlString
{
    
    NSLog(@"shouldStartLoadWithRequest----%@",urlString);
    
    NSRange cityRange = [urlString rangeOfString:QRAVED_WEB_SERVER];
    
    if (cityRange.location != NSNotFound) {
        
        
        if ([urlString rangeOfString:@"/restaurant/"].location == NSNotFound && [urlString rangeOfString:@"/jakarta/"].location == NSNotFound && [urlString rangeOfString:@"/bali/"].location == NSNotFound)
        {
            WebViewController *webViewController = [[WebViewController alloc] init];
            webViewController.webSite = urlString;
            webViewController.title = _journal.title;
            webViewController.fromDetail = NO;
            [self.navigationController pushViewController:webViewController animated:YES];
            return;
        }
        
        NSString *lastPart = [urlString substringFromIndex:cityRange.length];
        NSRange range = [lastPart rangeOfString:@"/"];
        
        IMGRestaurant *restaurant=nil;
        NSString *keyWord;
        
        if (range.location != NSNotFound) {
            NSString *cityName;
            NSNumber *cityId;
            
            cityName = [lastPart substringToIndex:range.location];
            keyWord = [lastPart substringFromIndex:range.location+1];
            
            restaurant=[self getRestaurantBySeoKeyword:keyWord andCityId:cityId];
            
        }
        
        
        if(restaurant){
            DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurant:restaurant];
            detailViewController.amplitudeType = @"JDP";
            detailViewController.amplitudeTypePage = @"Journal_ID";
            detailViewController.amplitudeTypePageId = _journal.mediaCommentId;
            [self.navigationController pushViewController:detailViewController animated:YES];
            return;
        }else{
            
            if (!keyWord || !keyWord.length)
            {
                return;
            }
            
            IMGNetWork *manager = [IMGNetWork sharedManager];
            
            NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:keyWord,@"seoKeyword", nil];
            [manager GET:@"restaurant/bykeyword" parameters:parameters progress:nil  success:^(NSURLSessionDataTask *operation, id responseObject) {
                
                NSLog(@"status_str = %@",responseObject);
                
                
                NSArray *returnRestaurantArray=(NSArray *)responseObject;
                IMGRestaurant *tmpRestaurant;
                if ([IMGRestaurant findById:[returnRestaurantArray objectAtIndex:0]])
                {
                    tmpRestaurant = [IMGRestaurant findById:[returnRestaurantArray objectAtIndex:0]];
                }
                else
                    tmpRestaurant = [RestaurantHandler updateRestaurant:returnRestaurantArray];
                
                NSLog(@"keyWord = %@",keyWord);
                
                if(tmpRestaurant){
                    DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurant:tmpRestaurant];
                    detailViewController.amplitudeType = @"JDP";
                    detailViewController.amplitudeTypePage = @"Journal_ID";
                    detailViewController.amplitudeTypePageId = _journal.mediaCommentId;
                    [self.navigationController pushViewController:detailViewController animated:YES];
                }
                
            }failure:^(NSURLSessionDataTask *operation, NSError *error) {
                
                NSLog(@"submit erors when get datas:%@",error.localizedDescription);
                
            }];
            return;
        }
        
    }
    else {
        WebViewController *webViewController = [[WebViewController alloc] init];
        webViewController.webSite = urlString;
        webViewController.title = _journal.title;
        webViewController.fromDetail = NO;
        [self.navigationController pushViewController:webViewController animated:YES];
        return;
    }
}
- (void)journalImageOrJournalContentClick:(UITapGestureRecognizer *)tap
{
    
    NSInteger tag = tap.view.tag;
    IMGJournalDetail *journalDetail = [[IMGJournalDetail alloc] init];
    journalDetail = [_dataDicM objectForKey:[NSString stringWithFormat:@"%ld",tag - 200]];
    
    if (journalDetail.restaurantId && [journalDetail.restaurantId intValue] != 0)
    {
        DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:journalDetail.restaurantId];
        dvc.amplitudeType = @"JDP";
        dvc.amplitudeTypePage = @"Journal_ID";
        dvc.amplitudeTypePageId = _journal.mediaCommentId;
        [self.navigationController pushViewController:dvc animated:YES];
        
    }
    else
        return;
}

- (void)journalRestaurantGojekTapped:(IMGRestaurant *)restaurant{
    [IMGAmplitudeUtil trackGoFoodWithName:@"CL - Go-Food CTA" andJournalId:self.journal.mediaCommentId andRestaurantId:restaurant.restaurantId andPhotoId:nil andLocation:@"Journal Detail Page" andOrigin:nil andOrder:nil];
    __weak typeof(self) weakSelf = self;
    [CommonMethod goFoodButtonTapped:restaurant andViewController:self andRestaurantState:[restaurant isOpenRestaurant] andCallBackBlock:^(BOOL isSaved) {
        JournalRestaurant *restaurantView = [weakSelf.view viewWithTag:[restaurant.restaurantId intValue]];
        [self refreshGoFoodRestaurantList:restaurant andIsSaved:isSaved];
        [restaurantView refreshSavedButton:isSaved];
        if (isSaved) {
            [IMGAmplitudeUtil trackRestaurantWithName:@"BM - Save Restaurant Succeed" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:@"Want to go Lis" andOrigin:@"JDP" andType:@"Custom List"];
        }
        for (IMGRestaurant *res  in restaurantArray) {
            if ([res.restaurantId isEqual:restaurant.restaurantId]) {
                res.saved = [NSNumber numberWithBool:isSaved];
                break;
            }
        }
        [mapViewController setMapDatas:restaurantArray offset:0];
    }];
}

- (void)JournalRestaurantImageClick:(UITapGestureRecognizer*)tap
{
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:@"Journal detail page" forKey:@"Location"];
    [eventProperties setValue:self.journal.restaurantIdDic forKey:@"Restaurant_ID"];
    [eventProperties setValue:@"Restaurant photo" forKey:@"Type"];
    [[Amplitude instance] logEvent:@"CL - See All Photos " withEventProperties:eventProperties];
    
    NSInteger tag;
    for (IMGDish *dish in _imageArrM)
    {
        if ([dish.sectionId intValue] == tap.view.tag) {
            
            tag=[_imageArrM indexOfObject:dish];
            break;
        }
    }
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:self.journal.mediaCommentId forKey:@"Journal_ID"];
    [param setValue:self.journal.restaurantIdDic forKey:@"Restaurant_ID"];
    [[Amplitude instance] logEvent:@"RC - View Journal photo" withEventProperties:param];
    
    AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:_imageArrM andPhotoTag:tag andRestaurant:nil andFromDetail:YES];
    albumViewController.amplitudeTitle = @"RC - View Journal photo";
    albumViewController.journalId=_journal.mediaCommentId;
    albumViewController.showPage=NO;
    albumViewController.isJournal = YES;
    [self.navigationController pushViewController:albumViewController animated:YES];
}

-(void)JournalRestaurantImageClick:(UITapGestureRecognizer*)tap withTargatURL:(NSString*)url andRestaurantId:(NSNumber *)restaurantId{

    

    if (![url isEqualToString:@""]) {
        
//        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:url]];
        BaseViewController * tempViewControoler = [[BaseViewController alloc] init];
        [tempViewControoler addBackBtn];
        UIWebView* web = [[UIWebView alloc] initWithFrame:self.view.bounds];
        [tempViewControoler.view addSubview:web];
        [web loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
        [self.navigationController pushViewController:tempViewControoler animated:YES];

        
        
    }else{
    
//        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
//        [eventProperties setValue:@"Journal detail page" forKey:@"Location"];
//        [eventProperties setValue:restaurantId forKey:@"Restaurant_ID"];
//        [eventProperties setValue:@"User photo" forKey:@"Type"];
//        [[Amplitude instance] logEvent:@"CL - See All Photos " withEventProperties:eventProperties];
        
        
        NSInteger tag;
        for (IMGDish *dish in _imageArrM)
        {
            if ([dish.sectionId intValue] == tap.view.tag) {
                
                tag=[_imageArrM indexOfObject:dish];
                
                [IMGAmplitudeUtil trackJDPPhotoWithName:@"RC - View Restaurant Photo detail" andJournalId:self.journal.mediaCommentId andOrigin:@"JDP" andPhotoId:dish.dishId];
                
                break;
            }
        }
        if (tap.view.tag==0&&_imageArrM.count>1) {
            tag=1;
        }
        
        
        NSMutableDictionary *param = [NSMutableDictionary dictionary];
        [param setValue:self.journal.mediaCommentId forKey:@"Journal_ID"];
        [param setValue:restaurantId forKey:@"Restaurant_ID"];
        [[Amplitude instance] logEvent:@"RC - View Journal photo" withEventProperties:param];
        
        AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:_imageArrM andPhotoTag:tag andRestaurant:nil andFromDetail:YES];
        albumViewController.amplitudeTitle = @"RC - View Journal photo";
        albumViewController.journalId=_journal.mediaCommentId;
        albumViewController.showPage=NO;
        albumViewController.isJournal = YES;
        [self.navigationController pushViewController:albumViewController animated:YES];

    
    }

    

}





//- (void)JournalRestaurantImageClick:(UITapGestureRecognizer*)tap
//{
//    [[Amplitude instance] logEvent:@"CL - See All Photos " withEventProperties:@{@"Location":@"Article detail page"}];
//    NSInteger tag;
//    for (IMGDish *dish in _imageArrM)
//    {
//        if ([dish.sectionId intValue] == tap.view.tag) {
//            
//            tag=[_imageArrM indexOfObject:dish];
//            break;
//        }
//    }
//    AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:_imageArrM andPhotoTag:tag andRestaurant:nil andFromDetail:YES];
//    albumViewController.amplitudeTitle = @"RC - View Journal photo";
//    albumViewController.showPage=NO;
//    albumViewController.isJournal = YES;
//    [self.navigationController pushViewController:albumViewController animated:YES];
//}
//
-(void)journalCuisineClick:(IMGDiscover*)discover{
    RestaurantsViewController *restaurantsViewController=[[RestaurantsViewController alloc] initWithMap];
    restaurantsViewController.amplitudeType = @"Journal detail page";
    [restaurantsViewController initDiscover:discover];
    restaurantsViewController.backToPrevious=YES;
    [self.navigationController pushViewController:restaurantsViewController animated:YES];
}

-(void)journalDistrictClick:(IMGDiscover*)discover{
    RestaurantsViewController *restaurantsViewController=[[RestaurantsViewController alloc] initWithMap];
    restaurantsViewController.amplitudeType = @"Journal detail page";
    [restaurantsViewController initDiscover:discover];
    restaurantsViewController.backToPrevious=YES;
    [self.navigationController pushViewController:restaurantsViewController animated:YES];
}

- (void)btnClick:(UIButton *)btn
{
    
    IMGJournalDetail *journalDetail = [[IMGJournalDetail alloc] init];
    journalDetail = [_dataDicM objectForKey:[NSString stringWithFormat:@"%ld",btn.tag - 100]];
    
    if (journalDetail.restaurantId && [journalDetail.restaurantId intValue] != 0)
    {
        DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:journalDetail.restaurantId];
        dvc.amplitudeType = @"JDP";
        dvc.amplitudeTypePage = @"Journal_ID";
        dvc.amplitudeTypePageId = _journal.mediaCommentId;
        [self.navigationController pushViewController:dvc animated:YES];
        
    }
    else
        return;
}

-(IMGRestaurant *)getRestaurantBySeoKeyword:(NSString *)keyWord andCityId:(NSNumber *)cityId{
    IMGRestaurant *restaurant;
    NSString *sqlString = [NSString stringWithFormat:@"select * from IMGRestaurant where seoKeyword='%@' and cityId=%@ limit 1",keyWord,cityId];
    FMResultSet *resultSet1 = [[DBManager manager] executeQuery:sqlString];
    if ([resultSet1 next]) {
        restaurant = [[IMGRestaurant alloc]init];
        [restaurant setValueWithResultSet:resultSet1];
        [resultSet1 close];
    }
    return restaurant;
}
- (void)shareButtonClickwithbutton:(UIButton*)btn {
    NSString *journal_link=_journal.link;
//    if([[_journal.link lowercaseString] hasPrefix:@"https://"]){
//        journal_link=[NSString stringWithFormat:@"http://%@",[_journal.link substringFromIndex:[@"https://" length]]];
//    }

    NSString *shareContentString = [NSString stringWithFormat:L(@"%@ on Qraved. %@"),_journal.title,journal_link];
    // NSString *shareContentString=_journal.link;
    NSURL *url=[NSURL URLWithString:journal_link];
    JournalActivityItemProvider *provider=[[JournalActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
    provider.url=url;
    provider.title=_journal.title;
    provider.journalId=_journal.mediaCommentId;
    UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider] applicationActivities:nil];
    activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
    [activityCtl setValue:[NSString stringWithFormat:L(@"Must Go! %@"),_journal.title] forKey:@"subject"];
    activityCtl.completionWithItemsHandler = ^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
        if (completed) {
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:_journal.restaurantId forKey:@"RestaurantID"];
            [eventProperties setValue:_journal.restaurantTitle forKey:@"RestaurantTitle"];
            [eventProperties setValue:@"Journal detail page" forKey:@"Location"];
            [eventProperties setValue:provider.activityType forKey:@"Channel"];
            [eventProperties setValue:_journal.mediaCommentId forKey:@"Journal_ID"];

            [[Amplitude instance] logEvent:@"SH - Share Journal" withEventProperties:eventProperties];
            [[AppsFlyerTracker sharedTracker] trackEvent:@"SH - Share Journal" withValues:eventProperties];

        }else{
        }
    };
    if ([activityCtl respondsToSelector:@selector(popoverPresentationController)]) {
        
        activityCtl.popoverPresentationController.sourceView = btn;
        
    }

    [self presentViewController:activityCtl animated:YES completion:^{
        
    }];
}

-(void)loadMoreNewRestaurantView{
    [loadStatus setObject:@(YES) forKey:componentoffset];
    
    activityIndicator.hidden = NO;
    activityIndicator.frame = CGRectMake(DeviceWidth/2 - 10, _scrollView.contentSize.height-80, 10, 10);
    [activityIndicator startAnimating];
    
    
    [JournalHandler getMoreJournalDetailWithJournal:_journal andComponentoffset:(NSNumber*)componentoffset andComponents:components andAllDataBlock:^(NSNumber *_componentCount, NSMutableArray *dataDic) {
        [activityIndicator stopAnimating];
        activityIndicator.hidden = YES;
        
        [components addObjectsFromArray:dataDic];
        for (NSDictionary *dic in dataDic) {
            [dic enumerateKeysAndObjectsUsingBlock:^(id key,id objc,BOOL *stop){
                if([key isEqualToString:@"restaurant"]){
                    
                    IMGRestaurant * restaurant = [self returnRestaurantWithRestaurantDic:dic];
                    
                    [restaurantArray addObject:restaurant];
                }
            }];
        }
//        if (restaurantArray.count>0) {
//            [self createMap];
//        }
        [mapViewController setMapDatas:restaurantArray offset:0];
        
        float currentY;
        int count=(int)dataDic.count;
        for(int i=0;i<count;i++){
            currentY = _currentPointY;
            if (i == 0) {
                freshY = _currentPointY;
            }
            NSDictionary *component=[dataDic objectAtIndex:i];
            JournalRestaurant *journalrest=[[JournalRestaurant alloc] initWithDictionary:component delegate:self menuPhotoCount:_menuPhotoCount];
            journalrest.backgroundColor=[UIColor clearColor];
            [journalrest setFrame:CGRectMake(0,_currentPointY+10,DeviceWidth,journalrest.viewHeight)];
            [_scrollView addSubview:journalrest];
            
            if ([journalrest.webview isKindOfClass:[YTPlayerView class]]) {
                [webViewArr addObject:journalrest.webview];
            }
            
            if([component valueForKey:@"photo"]){
                NSDictionary *photo=[component objectForKey:@"photo"];
                IMGDish *dish = [[IMGDish alloc] init];
                dish.imageUrl = [photo objectForKey:@"photo"];
                dish.sectionId = [photo objectForKey:@"order"];
                [dish updatePhotoCredit:[photo objectForKey:@"photoCreditImpl"]];
                dish.restaurantTitleDic = [[photo objectForKey:@"photoCreditImpl"] objectForKey:@"restaurantTitle"];
                dish.JournalGifH=[[photo objectForKey:@"height"] floatValue];
                dish.JournalGifW=[[photo objectForKey:@"width"] floatValue];
                dish.createTime = [NSNumber numberWithLongLong:[_journal.createTimeUnixTimestamp longLongValue]];
                [_imageArrM addObject:dish];
            }
            _currentPointY=journalrest.endPointY;
            
            UIView *titleBackgroundCopyView = journalrest.titleBackgroundCopyView;
            if (titleBackgroundCopyView!=nil) {
                
                titleBackgroundCopyView.frame = CGRectMake(0, 0, titleBackgroundCopyView.frame.size.width, titleBackgroundCopyView.frame.size.height);
                [self.view addSubview:titleBackgroundCopyView];
                
                [titleViewArr addObject:titleBackgroundCopyView];
                [titleViewYArr addObject:[NSNumber numberWithFloat:([journalrest.titleBackgroundCopyViewY floatValue]+currentY+10)]];
                
            }
        }
        lastY = _currentPointY;
        currentTitleIndex = -1;
        leftY = 0;
        rightY = 0;
        
        _scrollView.contentSize = CGSizeMake(0, _currentPointY+90);
        
        if (([componentCount longValue] > 10) && (components.count < [componentCount longValue])) {
            componentoffset = [NSNumber numberWithLong:([componentoffset longValue] + 10)];
        }else{
            [self addLikeCommentShareView];
            [self addRelatedPostView];
        }
    } failure:^(NSError *error){
        activityIndicator.hidden = YES;
        [loadStatus setObject:@(NO) forKey:componentoffset];
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(scrollView==_scrollView){
        
        CGFloat currOffset = scrollView.contentOffset.y;
        if (([componentCount longValue]>10)
            &&(components.count < [componentCount longValue])
            && !([[loadStatus objectForKey:componentoffset] boolValue])
            ) {
            if (freshY<currOffset) {
                [self loadMoreNewRestaurantView];
            }
        }
        
        if (   (titleViewYArr != nil)
            && (titleViewYArr.count !=0)
            && (currOffset >= [[titleViewYArr objectAtIndex:0] floatValue])
            && (currOffset <= lastY) ) {
            
            if ((currentTitleIndex == -1)
                || (leftY == 0 && rightY == 0)
                || (currOffset < leftY || currOffset > rightY) ) {

                for (int i = 0 ; i < titleViewYArr.count ; i++) {
                    
                    CGFloat itemY = [[titleViewYArr objectAtIndex:i] floatValue];
                    UIView *itemView = [titleViewArr objectAtIndex:i];
                    
                    if ( (i  != (titleViewYArr.count-1))
                        && (currOffset >= itemY)
                        && (currOffset < [[titleViewYArr objectAtIndex:(i+1)] floatValue]) ) {
                        
                        itemView.hidden = NO;
                        currentView = itemView;
                        currentTitleIndex = i;
                        leftY = itemY;
                        rightY = [[titleViewYArr objectAtIndex:(i+1)] floatValue];
                        
                    }else if((i  == (titleViewYArr.count-1)) && (currOffset>=itemY)) {
                        
                        itemView.hidden = NO;
                        currentView = itemView;
                        currentTitleIndex = i;
                        leftY = itemY;
                        rightY = lastY;
                        
                    }else{
                        itemView.hidden = YES;
                    }
                }
            }
            
            BOOL isLastIndex = (currentTitleIndex == (titleViewYArr.count-1));
            CGFloat currTitleViewHight = currentView.frame.size.height;
            if ((isLastIndex
                 && (currOffset <= lastY)
                 && (currOffset >= (lastY - currTitleViewHight)))
                ||
                (!isLastIndex
                 && (currOffset >= ([[titleViewYArr objectAtIndex:currentTitleIndex+1] floatValue]-currTitleViewHight))
                 && (currOffset <= [[titleViewYArr objectAtIndex:currentTitleIndex+1] floatValue]))
                ) {
                
                if (currOffset > preOffset) {
                    CGRect frame = currentView.frame;
                    frame.origin.y = frame.origin.y-(currOffset-preOffset);
                    if ( (-frame.origin.y) > currTitleViewHight) {
                        frame.origin.y = -currTitleViewHight;
                    }
                    currentView.frame = frame;
                }else{
                    CGRect frame = currentView.frame;
                    frame.origin.y = frame.origin.y+(preOffset - currOffset);
                    if ( frame.origin.y > 0 ) {
                        frame.origin.y = 0;
                    }
                    currentView.frame = frame;
                }
                
            }else{
                if (currentView.frame.origin.y!=0) {
                    CGRect frame = currentView.frame;
                    frame.origin.y = 0;
                    currentView.frame = frame;
                }
            }
        }else{
            if (currentTitleIndex != -1 && (titleViewArr!=nil) && (titleViewArr.count!=0) ) {
                for (UIView *item in titleViewArr) {
                    item.hidden = YES;
                    currentView=nil;
                }
            }
            currentTitleIndex = -1;
        }
        preOffset = currOffset;
        
//        if(_scrollView.contentSize.height<=_currentPointY){
//            currentOffsetY = [NSNumber numberWithFloat:scrollView.contentOffset.y];
//            if(likeCommentShareView){
//                if(scrollView.contentOffset.y+DeviceHeight-44>likeCommentShareView.startPointY+COMMUNITYBUTTONVIEWHEIGHT+commentInputView.frame.size.height){
//                    commentInputView.frame=CGRectMake(0, scrollView.contentOffset.y+DeviceHeight-44-commentInputView.frame.size.height, commentInputView.frame.size.width, commentInputView.frame.size.height);
//                }else{
//                    commentInputView.frame=CGRectMake(0, likeCommentShareView.startPointY+COMMUNITYBUTTONVIEWHEIGHT, commentInputView.frame.size.width, commentInputView.frame.size.height);
//                }
//            }
//        }else{
//            commentInputView.frame=CGRectMake(0, scrollView.contentOffset.y+DeviceHeight-44-commentInputView.frame.size.height-(scrollView.contentSize.height-_currentPointY), commentInputView.frame.size.width, commentInputView.frame.size.height);
//        }
    }else{
        NSLog(@"abc");
    }
}
-(void)JournalRestaurantContenClik:(NSString *)url{
    NSRange range=[url rangeOfString:@"app/restaurant"];

    if([url rangeOfString:@"qraved731842943"].location == NSNotFound)
    {
        BaseViewController * tempViewControoler = [[BaseViewController alloc] init];
        [tempViewControoler addBackBtn];
        UIWebView* web = [[UIWebView alloc] initWithFrame:self.view.bounds];
        [tempViewControoler.view addSubview:web];
        [web loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
        [self.navigationController pushViewController:tempViewControoler animated:YES];
    }else{
        
        if(range.location!=NSNotFound) {
            NSRange idRange = [url rangeOfString:@"id="];
            NSString *restaurantId = [url substringFromIndex:idRange.location+3];
            DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurantId:[NSNumber numberWithInt:[restaurantId intValue] ]];
            detailViewController.amplitudeType = @"Notification";
            
            [self.navigationController pushViewController:detailViewController animated:YES];
            
        }else if ([url rangeOfString:@"app/journal"].location != NSNotFound){
            
            NSRange idRange = [url rangeOfString:@"journalid="];
            if (idRange.location != NSNotFound)
            {
                NSString *journalId = [url substringFromIndex:idRange.location+10];
                IMGMediaComment *journal = [[IMGMediaComment alloc] init];
                journal.mediaCommentId = [NSNumber numberWithInt:[journalId intValue]];
//                [JournalHandler getJournalDetailTypeArticleIdWithJournalId:journal.mediaCommentId andJournalBlock:^(IMGMediaComment *journal) {
//                    
//                    if(journal==nil){
//                        JournalListViewController *JVC = [[JournalListViewController alloc] init];
//                        [self.navigationController pushViewController:JVC animated:YES];
//                        
//                    }else if ([journal.imported intValue]== 1)
//                    {
//                        [JournalHandler getJournalDetailWithJournalArticleId:journal.mediaCommentId andBlock:^(NSString *content,NSString *webSite) {
//                            journal.contents = content;
//                            WebViewController *wvc = [[WebViewController alloc] init];
//                            wvc.content = journal.contents;
//                            wvc.journal = journal;
//                            wvc.webSite = webSite;
//                            wvc.title = journal.title;
//                            wvc.fromDetail = NO;
//                            [self.navigationController pushViewController:wvc animated:YES];
//                        }];
//                    }
//                    else
//                    {
//                        JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
//                        jdvc.journal = journal;
//                        [self.navigationController pushViewController:jdvc animated:YES];
//                    }
//                    
//                    
//                }];

                JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
                jdvc.journal = journal;
                [self.navigationController pushViewController:jdvc animated:YES];

                
            }
        }else if ([url rangeOfString:@"app/diningguide"].location != NSNotFound){
            NSRange idRange = [url rangeOfString:@"diningguideid="];
            if (idRange.location != NSNotFound){
                NSString *diningguideId = [url substringFromIndex:idRange.location+14];
                IMGDiningGuide *diningGuide = [IMGDiningGuide findDiningGuideById:[NSNumber numberWithInt:[diningguideId intValue]]];
                if (diningGuide){
                    DiningGuideRestaurantsViewController *diningResList = [[DiningGuideRestaurantsViewController alloc]initWithDiningGuide:diningGuide];
                    diningResList.hiddenNavigationBarStatus = @(1);
                    [self.navigationController pushViewController:diningResList animated:YES];
                }else{
                    LoadingImageViewController *loadController = [[LoadingImageViewController alloc] init];
                    loadController.hiddenNavigationBarStatus = @(1);
                    loadController.isShowLoadingView = YES;
                    [self.navigationController pushViewController:loadController animated:YES];
                    
                    [HomeUtil getSingleDiningGuideFromServiceWithDiningGuideId:[NSNumber numberWithInt:[diningguideId intValue]] andBlock:^(IMGDiningGuide *guide) {
                        [self.navigationController popViewControllerAnimated:NO];
//                        IMGDiningGuide *diningGuide = [IMGDiningGuide findDiningGuideById:[NSNumber numberWithInt:[diningguideId intValue]]];
                        DiningGuideRestaurantsViewController *diningResList = [[DiningGuideRestaurantsViewController alloc]initWithDiningGuide:guide];
                        diningResList.hiddenNavigationBarStatus = @(1);
                        [self.navigationController pushViewController:diningResList animated:NO];
                    }];
                }
            }else{
                V2_DiningGuideListViewController *dglvc = [[V2_DiningGuideListViewController alloc] init];
                [self.navigationController pushViewController:dglvc animated:YES];
            }
        }else if ([url rangeOfString:@"app/searchResultPage"].location != NSNotFound){
            IMGDiscoverEntity *discoverEntity = [[IMGDiscoverEntity alloc] init];
            [discoverEntity initFromUrl:url];
            
            IMGDiscover *discover=[[IMGDiscover alloc ]init];
            [discover initFromDiscoverEntity:discoverEntity];
            discover.fromWhere = @3;
            discover.searchDistrictArray = discover.districtArray;
            discover.needLog = YES;
            
            RestaurantsViewController *restaurantsViewController=[[RestaurantsViewController alloc]initWithMap];
            [restaurantsViewController initDiscover:discover];
            [self.navigationController pushViewController:restaurantsViewController animated:YES];
        }else if ([url rangeOfString:@"app/journalResultPage"].location != NSNotFound){
            NSRange range = [url rangeOfString:@"?"];
            if (range.location != NSNotFound){
                NSString *paramStr = [url substringFromIndex:range.location+1];
                NSArray *paramArr = [paramStr componentsSeparatedByString:@"&"];
                if ( (paramArr!=nil) && (paramArr.count!=0) ) {
                    NSMutableDictionary *paramDic = [[NSMutableDictionary alloc] init];
                    for (NSString *item in paramArr) {
                        NSRange range = [item rangeOfString:@"="];
                        if (range.location != NSNotFound){
                            NSString *key = [item substringToIndex:range.location];
                            NSString *value = [item substringFromIndex:range.location+1];
                            [paramDic setValue:value forKey:key];
                        }
                    }
                    
                    JournalListViewController *jlvc = [[JournalListViewController alloc] init];
                    jlvc.isPop = YES;
                    jlvc.needLog = YES;
                    jlvc.searchText = [paramDic objectForKey:@"keywords"];
                    jlvc.currentCategoryName = [paramDic objectForKey:@"categoryname"];
                    jlvc.currentCategoryId = [NSNumber numberWithLongLong:[[paramDic objectForKey:@"categoryid"] longLongValue]];
                    [self.navigationController pushViewController:jlvc animated:YES];
                    
                }
            }
        }

    }
    
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.view endEditing:YES];
}

//-(void)keyboardChange:(NSNotification *)notification
//{
//    if (!isEditComment) {
//        return;
//    }
//    NSDictionary *userInfo = [notification userInfo];
//    NSTimeInterval animationDuration;
//    UIViewAnimationCurve animationCurve;
//    CGRect keyboardEndFrame;
//    
//    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
//    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
//    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
//    
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:animationDuration];
//    [UIView setAnimationCurve:animationCurve];
//    
//    if (notification.name == UIKeyboardWillShowNotification) {
//        _scrollView.contentSize = CGSizeMake(_scrollView.contentSize.width, _currentPointY+keyboardEndFrame.size.height);
//        [_scrollView setContentOffset:CGPointMake(0, [currentOffsetY floatValue]+keyboardEndFrame.size.height) animated:YES];
//
//    }else{
//        _scrollView.contentSize = CGSizeMake(_scrollView.contentSize.width, _currentPointY);
//    }
//    
//    [UIView commitAnimations];
//}
//
//- (void)commentInputView:(UIView*)view inputViewExpand:(UITextView*)inputTextView commentInputViewYOffset:(CGFloat)commentInputViewYOffset{
//    [UIView animateWithDuration:.5f
//                     animations:^{
//                         _scrollView.contentSize = CGSizeMake(_scrollView.contentSize.width, _scrollView.contentSize.height+commentInputViewYOffset);
//                     } completion:^(BOOL finished) {
//                         
//                     }
//     ];
//
//}
//
//- (void)commentInputView:(UIView*)view postButtonTapped:(UIButton*)postButton commentInputText:(NSString *)text{
//    IMGUser *currentUser = [IMGUser currentUser];
//    if (currentUser.userId == nil || currentUser.token == nil)
//    {
//        [[AppDelegate ShareApp]goToLoginController];
//        return;
//    }
//    
//    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
//    [eventProperties setValue:_journal.mediaCommentId forKey:@"Journal_ID"];
//    [eventProperties setValue:@"Journal detail page" forKey:@"Location"];
//    
//    [JournalHandler postComment:text articleId:self.journal.mediaCommentId andBlock:^(IMGJournalComment *journalComment){
//       // [commentInputView clearInputTextView];
//        [self.view endEditing:YES];
//        
//        [eventProperties setValue:journalComment.commentId forKey:@"Comment_ID"];
//        [[Amplitude instance] logEvent:@"UC - Comment Journal Succeed" withEventProperties:eventProperties];
//        [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Journal Succeed" withValues:eventProperties];
//        
//        if(journalComment){
//
//            [likeCommentShareView addNewComment:journalComment];
//            _journal.commentCount=[NSNumber numberWithInteger:[_journal.commentCount integerValue]+1];
//            
//            if(_journal.commentList){
//                if([_journal.commentList isKindOfClass:[NSArray class]]){
//                    _journal.commentList=[NSMutableArray arrayWithArray:_journal.commentList];
//                }
//                [_journal.commentList insertObject:journalComment atIndex:0];
//            }else{
//                _journal.commentList = [NSMutableArray arrayWithObject:journalComment];
//            }
//           
////            [self updateLikeCommentListCountLabel:NO];
//            [self addLikeCommentShareView:NO];
//            [_scrollView setContentOffset:CGPointMake(0, _scrollView.contentSize.height - _scrollView.bounds.size.height)];
//   
//            if(self.cdpDelegate){
//                [self.cdpDelegate CDP:self reloadTableCell:nil];
//            }
//            if (self.journalDetailViewControllerDelegate) {
//                [self.journalDetailViewControllerDelegate reloadComment];
//            }
//        }
//    } failure:^(NSString *errorReason){
//        if([errorReason isLogout]){
//            [[LoadingView sharedLoadingView] stopLoading];
//            [[IMGUser currentUser]logOut];
//            [[AppDelegate ShareApp] goToLoginController];
//        }
//        [eventProperties setValue:errorReason forKey:@"Reason"];
//        [[Amplitude instance] logEvent:@"UC - Comment Journal Failed" withEventProperties:eventProperties];
//    }];
//    [[Amplitude instance] logEvent:@"UC - Comment Journal Initiate" withEventProperties:eventProperties];
//}
//
//-(void)commentInputView:(UIView *)view inputViewDidBeginEditing:(UITextView *)textView{
//
//}
//
//- (void)likeCommentShareView:(UIView*)likeCommentShareView commentInputView:(UIView *)view inputViewDidBeginEditing:(UITextView *)textView{
//
//}
//
//- (void)likeCommentShareView:(UIView*)likeCommentShareView_ viewPreviousCommentsTapped:(UIButton*)button lastComment:(id)comment{
//    [[Amplitude instance] logEvent:@"CL - View Previous Comments CTA" withEventProperties:@{@"Location":@"Article detail page"}];
//    [[LoadingView sharedLoadingView] startLoading];
//    NSString *lastCommentIdStr=@"0";
//    if([comment isKindOfClass:[IMGJournalComment class]]){
//        IMGJournalComment *journalComment=(IMGJournalComment*)comment;
//        if(journalComment){
//            lastCommentIdStr=[NSString stringWithFormat:@"%@",journalComment.commentId];
//        }
//        
//        [JournalHandler previousComment:lastCommentIdStr targetId:_journal.mediaCommentId andBlock:^(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount) {
//            if(commentList&&commentList.count>0){
//                [self.view endEditing:YES];
//                
//                if(commentList){
//                    CGFloat likeCommentShareViewHeight=likeCommentShareView.frame.size.height;
//                    [likeCommentShareView addPreviousComments:commentList];
//                    
//                    CGFloat height=likeCommentShareView.frame.size.height-likeCommentShareViewHeight;
//                    _currentPointY=_scrollView.contentSize.height+height;
//                    _scrollView.contentSize=CGSizeMake(_scrollView.contentSize.width, _currentPointY);
//                    relatedPostView.frame=CGRectMake(relatedPostView.startPointX, relatedPostView.startPointY+height, relatedPostView.frame.size.width, relatedPostView.frame.size.height);
//                    
//                    CGFloat height1 = [UILikeCommentShareView calculatedTableViewHeight:_journal.commentList withFooter:NO];
//                    _scrollView.contentOffset = CGPointMake(0, _scrollView.contentSize.height-height1-DeviceHeight+65);
//                    
//                    [_journal.commentList addObjectsFromArray:commentList];
//                }
//            }
//        }];
//    }else{
//        [[LoadingView sharedLoadingView] stopLoading];
//    }
//}

-(void)likeCommentShareView:(UIView *)likeCommentShareView_ likeButtonTapped:(UIButton *)button{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:_journal.mediaCommentId forKey:@"Journal_ID"];
    [eventProperties setValue:@"Article detail page" forKey:@"Location"];
    
    [JournalHandler likeJournal:_journal.isLike articleId:_journal.mediaCommentId andBlock:^{
        if (!_journal.isLike) {
            [[Amplitude instance] logEvent:@"UC – UnLike Journal Succeed" withEventProperties:eventProperties];
        }else{
            [[Amplitude instance] logEvent:@"UC - Like Journal Succeed" withEventProperties:eventProperties];
        }
    }failure:^(NSString *exceptionMsg){
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
    }];
    [self.view endEditing:YES];
    _journal.isLike=!_journal.isLike;
    if(_journal.isLike){
        _journal.likeCount=[NSNumber numberWithInt:[_journal.likeCount intValue]+1];
        [[Amplitude instance] logEvent:@"UC - Like Journal Initiate" withEventProperties:eventProperties];
    }else{
        _journal.likeCount=[NSNumber numberWithInt:[_journal.likeCount intValue]-1];
        [[Amplitude instance] logEvent:@"UC - UnLike Journal Initiate" withEventProperties:eventProperties];
    }
    [likeCommentShareView updateLikeButtonStatus:_journal.isLike likeCount:[_journal.likeCount intValue]];
    [bottomBarView updateLikeButtonStatus:_journal.isLike likeCount:[_journal.likeCount intValue]];
   // [self updateLikeCommentListCountLabel:NO];
    if(self.cdpDelegate){
        [self.cdpDelegate CDP:self reloadTableCell:nil];
    }
    if (self.journalDetailViewControllerDelegate) {
        [self.journalDetailViewControllerDelegate reloadComment];
    }

}

- (void)likeListButtonTapped{
    [[Amplitude instance] logEvent:@" RC - View People Who Likes"];
    
    if (likepopView) {
        [likepopView removeFromSuperview];
    }
    
    [[LoadingView sharedLoadingView]startLoading];
    [HomeUtil getLikeListFromServiceWithJournalArticleId:_journal andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
        [[LoadingView sharedLoadingView]stopLoading];
        likeListArr=[[NSMutableArray alloc]initWithArray:likeListArray];
        likepopView.likecount=likeviewcount;
        likepopView.likelistArray=likeListArr;
        
    }];
    
    likepopView=[[LikeView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    likepopView.delegate=self;
    [self.view addSubview:likepopView];
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView shareButtonTapped:(UIButton *)button{
    [self shareButtonClickwithbutton:button];
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView commentButtonTapped:(UIButton *)button{
    isEditComment = YES;

    //[commentInputView becomeFirstResponderToInputTextView];
    [self commentButtonTapped];
}

- (void)commentButtonTapped{
    
    [IMGAmplitudeUtil trackJDPWithName:@"UC - Comment Journal Initiate" andJournalId:self.journal.mediaCommentId andOrigin:nil andChannel:nil andLocation:@"Journal detail page"];
    
    V2_CommentListViewController *commentListVC = [[V2_CommentListViewController alloc] initWithTypeID:2 andModel:_journal];
    [commentListVC setRefreshComment:^{
        int commentCount =  [_journal.commentCount intValue];
        commentCount ++;
        _journal.commentCount = [NSNumber numberWithInt:commentCount];
        
        [IMGAmplitudeUtil trackJDPWithName:@"UC - Comment Journal Succeed" andJournalId:self.journal.mediaCommentId andOrigin:nil andChannel:nil andLocation:@"Journal detail page"];
        
        [bottomBarView updateCommentButton:[_journal.commentCount intValue]];
        [likeCommentShareView.likeCommentShare setCommentCount:[_journal.commentCount intValue] likeCount:[_journal.likeCount intValue] liked:_journal.isLike];
        
       // [self refreshCell:cellIndexPath];
    }];
    
    [self.navigationController pushViewController:commentListVC animated:YES];
}

- (void)likeCommentShareViewUserNameOrImageTapped:(id)sender
{
    if ([sender isKindOfClass:[IMGUser class]])
    {
        IMGUser *user = (IMGUser *)sender;
        if (![user.userId intValue]) {
            return;
        }
        OtherProfileViewController *opvc = [[OtherProfileViewController alloc] init];
        opvc.amplitude = @"Article detail page";
        IMGUser *currentUser = [IMGUser currentUser];
        if ([currentUser.userId intValue] == [user.userId intValue])
        {
            opvc.isOtherUser = NO;
        }else
        {
            opvc.isOtherUser = YES;
        }
        opvc.otherUser = user;
        [self.navigationController pushViewController:opvc animated:YES];
    }
    else if ([sender isKindOfClass:[IMGRestaurant class]])
    {
        IMGRestaurant *restaurant = (IMGRestaurant *)sender;
        if (![restaurant.restaurantId intValue]) {
            return;
        }
        DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:restaurant.restaurantId];
        [self.navigationController pushViewController:dvc animated:YES];
    }
    
}

//- (void)commentInputView:(UIView*)view inputViewShouldBeginEditing:(UITextView *)textView{
//    isEditComment = YES;
//}
-(void)goToBackViewController{
    [self removeNotifications];
    if (self.isFromURL) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
        
    }
}
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{

}

- (IMGRestaurant *)returnRestaurantWithRestaurantDic:(NSDictionary *)dic{
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    [restaurant setValuesForKeysWithDictionary:[dic objectForKey:@"restaurant"]];
    NSDictionary *restaurantDic = [dic objectForKey:@"restaurant"];
    restaurant.cuisineName = [restaurantDic objectForKey:@"cuisine"];
    restaurant.districtName = [restaurantDic objectForKey:@"district"];
    restaurant.ratingScore = [restaurantDic objectForKey:@"score"];
    
    return restaurant;
}

- (void)copyLink{
    
    [IMGAmplitudeUtil trackJDPWithName:@"SH - Share Journal" andJournalId:self.journal.mediaCommentId andOrigin:nil andChannel:@"Copy Link" andLocation:@"Journal detail page"];
    
    NSString *journal_link=_journal.link;
    
    NSString *shareContentString = [NSString stringWithFormat:L(@"%@ on Qraved. %@"),_journal.title,journal_link];
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = shareContentString;
     [UIAlertView showAlertView:@"The content has been copied to the clipboard" andMessage:nil];
}

- (void)shareToLine{
    NSString *journal_link=_journal.link;
    
    NSString *shareContentString = [NSString stringWithFormat:L(@"%@ on Qraved. %@"),_journal.title,journal_link];
    
    NSString *contentType = @"text";
    NSString *urlString = [NSString
                           stringWithFormat:@"line://msg/%@/%@",
                           contentType, [shareContentString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSURL *url = [NSURL URLWithString:urlString];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        
        [IMGAmplitudeUtil trackJDPWithName:@"SH - Share Journal" andJournalId:self.journal.mediaCommentId andOrigin:nil andChannel:@"LINE" andLocation:@"Journal detail page"];
        
        [[UIApplication sharedApplication] openURL:url];
    }
    else{
        [UIAlertView showAlertView:@"Sharing failed" andMessage:nil];
    }

}

- (void)shareToFaceBook{
    
    NSString *journal_link=_journal.link;
   
    NSString *shareContentString = [NSString stringWithFormat:L(@"%@ on Qraved. %@"),_journal.title,journal_link];
    NSURL *url=[NSURL URLWithString:journal_link];
    
    
    SLComposeViewController *composeVC = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [composeVC addURL:url];

    [self presentViewController:composeVC animated:YES completion:nil];

    composeVC.completionHandler = ^(SLComposeViewControllerResult result){
        if (result == SLComposeViewControllerResultDone) {
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:_journal.restaurantId forKey:@"RestaurantID"];
            [eventProperties setValue:_journal.restaurantTitle forKey:@"RestaurantTitle"];
            [eventProperties setValue:@"Journal detail page" forKey:@"Location"];
            [eventProperties setValue:SLServiceTypeFacebook forKey:@"Channel"];
            [eventProperties setValue:_journal.mediaCommentId forKey:@"Journal_ID"];
            
            [[Amplitude instance] logEvent:@"SH - Share Journal" withEventProperties:eventProperties];
            [[AppsFlyerTracker sharedTracker] trackEvent:@"SH - Share Journal" withValues:eventProperties];
            
            [IMGAmplitudeUtil trackJDPWithName:@"SH - Share Journal" andJournalId:self.journal.mediaCommentId andOrigin:nil andChannel:@"Facebook" andLocation:@"Journal detail page"];
        }
        else if (result == SLComposeViewControllerResultCancelled)
        {
           
        }
    };
    

}

@end
