//
//  JournalCategoryViewController.m
//  Qraved
//
//  Created by Lucky on 15/10/8.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import "JournalCategoryViewController.h"
#import "IMGJournalCategory.h"
#import "UIViewController+Helper.h"
#import "UIView+Helper.h"
#import "UIDevice+Util.h"
#import "UIColor+Helper.h"
@interface JournalCategoryViewController ()

@end

@implementation JournalCategoryViewController{
    UIImageView *journalImageView;
    UITableViewCell *cellTmp;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"Journal list filter page";
    IMGJournalCategory *allCategory = [[IMGJournalCategory alloc] init];
    allCategory.categoryName = @"All";
    allCategory.categoryOrder = @0;
    allCategory.categoryType = @0;
    [self.categoryDataArrM addObject:allCategory];
    [self setBackBarButtonOffset30];
    UITableView *tableView = [[UITableView alloc] init];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight+20-IMG_StatusBarAndNavigationBarHeight);
    [self.view addSubview:tableView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.categoryDataArrM.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = [[NSString alloc] initWithFormat:@"JournalCell"];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil){
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    IMGJournalCategory *journalCategory = [self.categoryDataArrM objectAtIndex:indexPath.row];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell.textLabel setText:journalCategory.categoryName];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    NSString *temp = [[NSUserDefaults standardUserDefaults] objectForKey:@"categoryName"];
//    if ([journalCategory.categoryName isEqualToString:temp]) {
//        cell.textLabel.textColor = [UIColor color3BAF24];
//        journalImageView = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-40, 5, 20, 20)];
//        journalImageView.image = [UIImage imageNamed:@"checked.png"];
//        cellTmp = cell;
//        [cell addSubview:journalImageView];
//        return cellTmp;
//    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    cellTmp.textLabel.textColor = [UIColor color333333];
    UITableViewCell *cellTemp = (UITableViewCell *)[tableView  cellForRowAtIndexPath:indexPath];
    cellTemp.textLabel.textColor = [UIColor color3BAF24];
    [journalImageView removeFromSuperview];
    [cellTemp addSubview:journalImageView];

    IMGJournalCategory *journalCategory = [self.categoryDataArrM objectAtIndex:indexPath.row];
    if (self.refreshJournal) {
        self.refreshJournal(journalCategory.categoryType);
    }
    [[NSUserDefaults standardUserDefaults] setValue:journalCategory.categoryName forKey:@"categoryName"];
    [self.navigationController popViewControllerAnimated:YES];
    
    if([self.jcvcDelegate respondsToSelector:@selector(sendValueWithCategoryId: andCategoryName:)])
    {
        
        
        [self.jcvcDelegate sendValueWithCategoryId:journalCategory.categoryType andCategoryName:journalCategory.categoryName];
    }
}

 
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
