//
//  ViewController.m
//  QuickStart
//
//  Created by Abir Majumdar on 12/3/14.
//  Copyright (c) 2014 Layer, Inc. All rights reserved.
//

#import "LayerViewController.h"
#import "ChatMessageCell.h"
#import "AppDelegate.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "MessageFrame.h"
#import "IMGMessage.h"
#import "IMGUser.h"

#define Width (DeviceWidth-30)
#define Height DeviceHeight-100+80

@interface LayerViewController () <UITextViewDelegate, LYRClientDelegate, LYRQueryControllerDelegate>

@property (nonatomic,retain)NSMutableArray *allMessagesFrame;
@end

@implementation LayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 80, Width, 20)];
    topView.backgroundColor = [UIColor colorC2060A];
    NSString *title = L(@"Welcome to Qraved app");
    CGSize titleSize = [title sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13]];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(Width/2-titleSize.width/2, 0, titleSize.width, 20)];
    titleLabel.text = title;
    titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    titleLabel.tintColor = [UIColor whiteColor];
    [topView addSubview:titleLabel];
    [self.view addSubview:topView];

    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, topView.endPointY, Width, Height-58-topView.endPointY) style:UITableViewStylePlain];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    UITapGestureRecognizer *tapgesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tableviewSelect)];
    [self.tableView addGestureRecognizer:tapgesture];
    
    
    self.inputTextView = [[UITextView alloc]initWithFrame:CGRectMake(0, self.tableView.endPointY, DeviceWidth-50, 44)];
    [self.view addSubview:self.inputTextView];
    self.inputTextView.layer.cornerRadius = 0.5;
    self.inputTextView.layer.borderColor = [UIColor colorC2060A].CGColor;
    self.inputTextView.layer.borderWidth = 0.5;
    self.inputTextView.delegate = self;
    self.inputTextView.text = kInitialMessage;
    self.inputTextView.returnKeyType = UIReturnKeySend;
    
    self.sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.sendBtn setTitle:L(@"send") forState:UIControlStateNormal];
    [self.sendBtn setBackgroundColor:[UIColor colorC2060A]];
    [self.sendBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.sendBtn.frame = CGRectMake(Width-50, self.tableView.endPointY, 50, 44);
    [self.sendBtn addTarget:self action:@selector(sendMessageAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.sendBtn];

    
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:closeButton];
    closeButton.frame = CGRectMake(Width-20, 80, 20, 20);
    [closeButton setTitle:@"X" forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(closeViewController) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [clearButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.view addSubview:clearButton];
    clearButton.frame = CGRectMake(0, 80, 50, 20);
    [clearButton setTitle:L(@"Clear") forState:UIControlStateNormal];
    [clearButton addTarget:self action:@selector(clearAllMessages) forControlEvents:UIControlEventTouchUpInside];
    

    // Set up Layer Client delegate and Layer Change Notification
    self.layerClient.delegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveLayerObjectsDidChangeNotification:) name:LYRClientObjectsDidChangeNotification object:self.layerClient];

    _allMessagesFrame = [NSMutableArray array];
    
    // Fetches all conversations between the authenticated user and the supplied user
    NSArray *participants = @[kUserID, kParticipant];
    LYRQuery *query = [LYRQuery queryWithClass:[LYRConversation class]];
    query.predicate = [LYRPredicate predicateWithProperty:@"participants" operator:LYRPredicateOperatorIsEqualTo value:participants];

    NSError *error;
    NSOrderedSet *conversations = [self.layerClient executeQuery:query error:&error];
    if (!error) {
        NSLog(@"%tu conversations with participants %@", conversations.count, participants);
    } else {
        NSLog(@"Query failed with error %@", error);
    }
    
    // Retrieve the last conversation
    if (conversations.count != 0)
    {
        self.conversation = [conversations lastObject];
        [self logMessage:[NSString stringWithFormat:@"Get last conversation object: %@",self.conversation.identifier]];
        // setup query controller with messages from last conversation
        [self setupQueryController];

    }
}
-(void)closeViewController{
    [[AppDelegate ShareApp]HideLayerWindow];
}
-(void)clearAllMessages{
    NSError *error=nil;
    for (int i=0; i<[self.queryController numberOfObjectsInSection:0]; i++) {
        LYRMessage*message = [self.queryController objectAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        [message delete:LYRDeletionModeAllParticipants error:&error];
        
        
    }
     if(error){
         DLog(@"%@",error);
     }else{
         [self closeViewController];
         
     }
}
-(void)tableviewSelect{
    [self.inputTextView resignFirstResponder];
    [self setViewMovedUp:NO];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    // Setup for Shake
    [self becomeFirstResponder];
    
    // Register for typing indicator notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveTypingIndicator:)
                                                 name:LYRConversationDidReceiveTypingIndicatorNotification object:self.conversation];
    if (self.tableView.contentSize.height>Height-44) {
        self.tableView.contentOffset = CGPointMake(0, self.tableView.contentSize.height-([UIDevice isLaterThanIphone5]?372:292));

    }
}
- (void)viewWillDisappear:(BOOL)animated
{
    // Setup for Shake
    [self resignFirstResponder];
    [super viewWillDisappear:animated];
    
    self.queryController = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:LYRConversationDidReceiveTypingIndicatorNotification
                                                  object:self.conversation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Receiving Typing Indicator Method

- (void)didReceiveTypingIndicator:(NSNotification *)notification
{
    NSString *participantID = notification.userInfo[LYRTypingIndicatorParticipantUserInfoKey];
    LYRTypingIndicator typingIndicator = [notification.userInfo[LYRTypingIndicatorValueUserInfoKey] unsignedIntegerValue];
    
    if (typingIndicator == LYRTypingDidBegin) {
        self.typingIndicatorLabel.alpha = 1;
        self.typingIndicatorLabel.text = [NSString stringWithFormat:L(@"%@ is typing..."),participantID];
    }
    else {
        self.typingIndicatorLabel.alpha = 0;
        self.typingIndicatorLabel.text = @"";
    }
}

#pragma - IBActions

- (void)sendMessageAction:(id)sender
{
    self.sendBtn.enabled = NO;
    
    // Send Message
    [self sendMessage:self.inputTextView.text];
    
    // Lower the keyboard
//    [self setViewMovedUp:NO];
//    [self.inputTextView resignFirstResponder];
    
}

- (void)sendMessage:(NSString*) messageText{
    if (messageText.length == 0) {
        return;
    }
    // If no conversations exist, create a new conversation object with a single participant
    if(self.conversation == nil) {
        LYRConversation *conversation = [self.layerClient newConversationWithParticipants:[NSSet setWithArray:@[kUserID,kParticipant]] options:nil error:nil];
        self.conversation = conversation;
    }
    
    // Creates a message part with text/plain MIME Type
    LYRMessagePart *messagePart = [LYRMessagePart messagePartWithText:messageText];
    // Creates and returns a new message object with the given conversation and array of message parts
    LYRMessage *message = [self.layerClient newMessageWithParts:@[messagePart] options:@{LYRMessageOptionsPushNotificationAlertKey: messageText} error:nil];
    
    // Sends the specified message
    NSError *e;
    BOOL success = [self.conversation sendMessage:message error:&e];
    message.sentAt = [NSDate date];

    if (success) {
        [self logMessage:[NSString stringWithFormat: @"Message Queued Up to Be Sent: %@",messageText]];
    }
    else {
        [self logMessage:[NSString stringWithFormat: @"Message Send Failed: %@",e]];
    }
    self.sendBtn.enabled = YES;

}

#pragma - Set up for Shake

-(BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    // If user shakes the phone, change the navbar color and set metadata
    if (motion == UIEventSubtypeMotionShake)
    {
        UIColor *newNavBarBackgroundColor = [self getRandomColor];
        self.navigationController.navigationBar.barTintColor = newNavBarBackgroundColor;

        CGFloat redFloat = 0.0, greenFloat = 0.0, blueFloat = 0.0, alpha =0.0;
        [newNavBarBackgroundColor getRed:&redFloat green:&greenFloat blue:&blueFloat alpha:&alpha];
        
        NSDictionary *metadata = @{@"backgroundColorRed" : [[NSNumber numberWithFloat:redFloat] stringValue],
                                   @"backgroundColorGreen" : [[NSNumber numberWithFloat:greenFloat] stringValue],
                                   @"backgroundColorBlue" : [[NSNumber numberWithFloat:blueFloat] stringValue]};
        [self.conversation setValuesForMetadataKeyPathsWithDictionary:metadata merge:YES];
    }
}

#pragma - mark TableView Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return number of objects in queryController
    return _allMessagesFrame.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"ChatMessageCell";
    ChatMessageCell *cell = (ChatMessageCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[ChatMessageCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        
    }
    cell.messageFrame = _allMessagesFrame[indexPath.row];

    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    return [_allMessagesFrame[indexPath.row] cellHeight];
    return UITableViewAutomaticDimension;
}

#pragma - mark TextView Delegate Methods

- (void)textViewDidBeginEditing:(UITextView *)textView {
    // Sends a typing indicator event to the given conversation.
    [self.conversation sendTypingIndicator:LYRTypingDidBegin];
    [self setViewMovedUp:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    // Sends a typing indicator event to the given conversation.
    [self.conversation sendTypingIndicator:LYRTypingDidFinish];
}

// Move up the view when the keyboard is shown
- (void)setViewMovedUp:(BOOL)movedUp{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];

    CGRect rect = self.view.frame;
    float height = [UIDevice isLaterThanIphone5]?255:185;
    if (movedUp){
        if(rect.origin.y == 0)
            rect.origin.y = self.view.frame.origin.y - height;
    }
    else{
        if(rect.origin.y < 0)
            rect.origin.y = self.view.frame.origin.y + height;
    }
    self.view.frame = rect;
    [UIView commitAnimations];
}

// If the user hits Return then dismiss the keyboard and move the view back down
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]) {
//        [self.inputTextView resignFirstResponder];
        [self sendMessageAction:nil];
//        [self setViewMovedUp:NO];
        return NO;
    }
    return YES;
}
#pragma - mark LYRClientDelegate Delegate Methods
- (void)layerClient:(LYRClient *)client didReceiveAuthenticationChallengeWithNonce:(NSString *)nonce
{
    NSLog(@"Layer Client did recieve authentication challenge with nonce: %@", nonce);
}

- (void)layerClient:(LYRClient *)client didAuthenticateAsUserID:(NSString *)userID
{
    NSLog(@"Layer Client did recieve authentication nonce");
}

- (void)layerClientDidDeauthenticate:(LYRClient *)client
{
    NSLog(@"Layer Client did deauthenticate");
}

- (void)layerClient:(LYRClient *)client didFinishSynchronizationWithChanges:(NSArray *)changes
{
    NSLog(@"Layer Client did finish sychronization");
}

- (void)layerClient:(LYRClient *)client didFailSynchronizationWithError:(NSError *)error
{
    NSLog(@"Layer Client did fail synchronization with error: %@", error);
}

- (void)layerClient:(LYRClient *)client willAttemptToConnect:(NSUInteger)attemptNumber afterDelay:(NSTimeInterval)delayInterval maximumNumberOfAttempts:(NSUInteger)attemptLimit
{
    NSLog(@"Layer Client will attempt to connect");
}

- (void)layerClientDidConnect:(LYRClient *)client
{
    NSLog(@"Layer Client did connect");
}

- (void)layerClient:(LYRClient *)client didLoseConnectionWithError:(NSError *)error
{
    NSLog(@"Layer Client did lose connection with error: %@", error);
}

- (void)layerClientDidDisconnect:(LYRClient *)client
{
    NSLog(@"Layer Client did disconnect with error");
}

#pragma - mark LYRQueryControllerDelegate Delegate Methods

- (void)didReceiveLayerClientWillBeginSynchronizationNotification:(NSNotification *)notification
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didReceiveLayerClientDidFinishSynchronizationNotification:(NSNotification *)notification
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)queryControllerWillChangeContent:(LYRQueryController *)queryController
{
    [self.tableView beginUpdates];
}

- (void)queryController:(LYRQueryController *)controller
        didChangeObject:(id)object
            atIndexPath:(NSIndexPath *)indexPath
          forChangeType:(LYRQueryControllerChangeType)type
           newIndexPath:(NSIndexPath *)newIndexPath
{
    // Automatically update tableview when there are change events
//    switch (type) {
//        case LYRQueryControllerChangeTypeInsert:
//            [self.tableView insertRowsAtIndexPaths:@[newIndexPath]
//                                  withRowAnimation:UITableViewRowAnimationAutomatic];
//            break;
//        case LYRQueryControllerChangeTypeUpdate:
//            [self.tableView reloadRowsAtIndexPaths:@[indexPath]
//                                  withRowAnimation:UITableViewRowAnimationAutomatic];
//            break;
//        case LYRQueryControllerChangeTypeMove:
//            [self.tableView deleteRowsAtIndexPaths:@[indexPath]
//                                  withRowAnimation:UITableViewRowAnimationAutomatic];
//            [self.tableView insertRowsAtIndexPaths:@[newIndexPath]
//                                  withRowAnimation:UITableViewRowAnimationAutomatic];
//            break;
//        case LYRQueryControllerChangeTypeDelete:
//            [self.tableView deleteRowsAtIndexPaths:@[indexPath]
//                                  withRowAnimation:UITableViewRowAnimationAutomatic];
//            break;
//        default:
//            break;
//    }
}

- (void)queryControllerDidChangeContent:(LYRQueryController *)queryController
{
    [self.tableView endUpdates];
}

- (void) didReceiveLayerObjectsDidChangeNotification:(NSNotification *)notification;
{
    // Listen for Conversation Updates
    NSArray *changes = [notification.userInfo objectForKey:LYRClientObjectChangesUserInfoKey];
    for (NSDictionary *change in changes) {
        id changeObject = [change objectForKey:LYRObjectChangeObjectKey];
        if ([[change objectForKey:LYRObjectChangeObjectKey] isKindOfClass:[LYRConversation class]]) {
            NSLog(@"Conversation Updated");

            // setup query controller with messages from updated conversation
            self.conversation = (LYRConversation*)changeObject;
            [self setupQueryController];
            [self.tableView reloadData];
        }
        
        if ([[change objectForKey:LYRObjectChangeObjectKey]isKindOfClass:[LYRMessage class]]) {
            NSLog(@"Message Updated");
        }
        if (self.tableView.contentSize.height>Height-44) {
            self.tableView.contentOffset = CGPointMake(0, self.tableView.contentSize.height-([UIDevice isLaterThanIphone5]?372:292));
        }

    }
}

#pragma - mark General Helper Methods

- (void)logMessage:(NSString*) messageText{
    NSLog(@"MSG: %@",messageText);
    //    [self.textView performSelectorOnMainThread:@selector(setText:) withObject:[NSString stringWithFormat:@"%@\n%@", self.textView.text, messageText] waitUntilDone:YES];
    
}

-(void) setupQueryController
{
    // Retrieve all the messages in conversation
    LYRQuery *query = [LYRQuery queryWithClass:[LYRMessage class]];
    NSError *error;
//    [self.conversation delete:LYRDeletionModeAllParticipants error:nil];
    query.predicate = [LYRPredicate predicateWithProperty:@"conversation" operator:LYRPredicateOperatorIsEqualTo value:self.conversation];
    query.sortDescriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"index" ascending:NO]];
    
    // Set up query controller
    self.queryController = [self.layerClient queryControllerWithQuery:query];
    self.queryController.delegate = self;
    
    BOOL success = [self.queryController execute:&error];
    
    if (success) {
        NSLog(@"Query fetched %tu message objects", [self.queryController numberOfObjectsInSection:0]);

        
    } else {
        NSLog(@"Query failed with error %@", error);
    }
    
    // Mark all conversations as read on launch
    NSError *markAllMessagesAsReadError = nil;
    [self.conversation markAllMessagesAsRead:&markAllMessagesAsReadError];
    if (error) {
        DLog(@"%@",markAllMessagesAsReadError);
    }
    
    // Get initial nav bar colors from conversation metadata
    [self setNavbarColorFromConversationMetadata:self.conversation.metadata];

    [self initMessages];

    
}
-(void)initMessages{
    
    NSString *previousTime = nil;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss"];
    
    [_allMessagesFrame removeAllObjects];
    for (int i=0; i<[self.queryController numberOfObjectsInSection:0]; i++) {
        LYRMessage*message = [self.queryController objectAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        LYRMessagePart *messagePart = message.parts[0];
        
        NSString *messageContent;
        NSString *timeStr;
        
        if ([messagePart.MIMEType isEqualToString:kMIMETypeTextPlain]) {
            messageContent = [[NSString alloc] initWithData:messagePart.data encoding:NSUTF8StringEncoding];
        } else {
            messageContent = [NSString stringWithFormat:@"Cannot display '%@'", messagePart.MIMEType];
        }
        
        switch ([message recipientStatusForUserID:kParticipant]) {
            case LYRRecipientStatusSent:
                timeStr = [NSString stringWithFormat:@"Sent: %@",[formatter stringFromDate:message.sentAt]];
                NSLog(@"sentAt      %d---%@--%@",i,message.sentAt,timeStr);

                break;
                
            case LYRRecipientStatusDelivered:
                timeStr = [NSString stringWithFormat:@"Delivered: %@",[formatter stringFromDate:message.sentAt]];
                break;
                
            case LYRRecipientStatusRead:
                timeStr = [NSString stringWithFormat:@"Read: %@",[formatter stringFromDate:message.receivedAt]];
                NSLog(@"receivedAt  %d---%@--%@",i,message.receivedAt,timeStr);

                break;
                
            case LYRRecipientStatusInvalid:
                NSLog(@"Participant: Invalid");
                break;
                
            default:
                break;
        }
        NSString *messageType;
        NSString *imageStr;
        
#if TARGET_IPHONE_SIMULATOR
        if ([message.sentByUserID isEqualToString:@"Simulator"]) {
            messageType = @"0";
            imageStr = [IMGUser currentUser].avatar;
        }else{
            messageType = @"1";
            imageStr = @"qraved_icon_default";
        }
#else
        if ([message.sentByUserID isEqualToString:@"Device"]) {
            messageType = @"0";
            imageStr = [IMGUser currentUser].avatar;
        }else{
            messageType = @"1";
            imageStr = @"qraved_icon_default";
        }
#endif
        if (imageStr.length == 0) {
            imageStr = @"avatar_simple_visitor";
        }
        NSDictionary *dict = @{@"content":messageContent,@"icon":imageStr,@"time":timeStr,@"type":messageType};
        MessageFrame *messageFrame = [[MessageFrame alloc] init];
        IMGMessage *messageModel = [[IMGMessage alloc] init];
        messageModel.dict = dict;
        
        messageFrame.showTime = ![previousTime isEqualToString:messageModel.time];
        
        messageFrame.message = messageModel;
        
        previousTime = messageModel.time;
        
        [_allMessagesFrame insertObject:messageFrame atIndex:0];
        
        
        
    }
}
- (UIColor *) getRandomColor
{
    float redFloat = arc4random() % 100 / 100.0f;
    float greenFloat = arc4random() % 100 / 100.0f;
    float blueFloat = arc4random() % 100 / 100.0f;
    
    return [UIColor colorWithRed:redFloat
                           green:greenFloat
                            blue:blueFloat
                           alpha:1.0f];
}

-(void) setNavbarColorFromConversationMetadata:(NSDictionary *)metadata
{
    float redColor = (float)[[metadata valueForKey:@"backgroundColorRed"] floatValue];
    float blueColor = (float)[[metadata valueForKey:@"backgroundColorBlue"] floatValue];
    float greenColor = (float)[[metadata valueForKey:@"backgroundColorGreen"] floatValue];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:redColor
                                                                           green:greenColor
                                                                            blue:blueColor
                                                                           alpha:1.0f];
}

@end
