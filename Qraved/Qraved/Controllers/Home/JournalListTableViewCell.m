//
//  JournalListTableViewCell.m
//  Qraved
//
//  Created by Lucky on 15/6/29.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "JournalListTableViewCell.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import "IMGMediaComment.h"
#import "NSString+Helper.h"
 
#import "UIImageView+WebCache.h"
#import "UIConstants.h"
#import "PPLabel.h"
#import "UIImage+Resize.h"

@implementation JournalListTableViewCell
{
    IMGMediaComment *_journal;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _photoImageView = [[UIImageView alloc] init];
        _photoImageView.frame = CGRectMake(LEFTLEFTSET, 10, DeviceWidth-LEFTLEFTSET*2, 160);
        _photoImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_photoImageView];
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.numberOfLines = 2;
        _titleLabel.textColor = [UIColor color333333];

//        _titleLabel.frame = CGRectMake(LEFTLEFTSET + 8, _photoImageView.endPointY + 3, DeviceWidth/2+50, 45);
        _titleLabel.frame = CGRectMake(LEFTLEFTSET + 8, _photoImageView.endPointY + 2, DeviceWidth - LEFTLEFTSET*2 - 16, 40);

        _titleLabel.font = [UIFont systemFontOfSize:16];
        [self addSubview:_titleLabel];
        
        _categoryNameLabel = [[PPLabel alloc] init];
//        _categoryNameLabel.delegate = self;
        _categoryNameLabel.numberOfLines = 1;
        _categoryNameLabel.textColor = [UIColor grayColor];
        _categoryNameLabel.font = [UIFont systemFontOfSize:11];
        _categoryNameLabel.frame = CGRectMake(LEFTLEFTSET + 8, _titleLabel.endPointY + 1, _titleLabel.frame.size.width, 15);
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = _categoryNameLabel.frame;
        [self addSubview:btn];
        
        _categoryNameLabel.frame = CGRectMake(0, 0 , _titleLabel.frame.size.width, 15);

        [btn addSubview:_categoryNameLabel];

        
//        UILabel *lineLabel = [[UILabel alloc] init];
//        lineLabel.frame = CGRectMake(_categoryNameLabel.endPointX + 16, _titleLabel.frame.origin.y + 10, 1, _categoryNameLabel.frame.size.height+_titleLabel.frame.size.height - 10);
//        lineLabel.backgroundColor = [UIColor grayColor];
//        lineLabel.alpha = 0.4f;
//        [self addSubview:lineLabel];

        UILabel *leftLine = [[UILabel alloc] init];
        leftLine.frame = CGRectMake(LEFTLEFTSET, _photoImageView.endPointY, 1, _categoryNameLabel.frame.size.height+_titleLabel.frame.size.height+6);
        leftLine.backgroundColor = [UIColor grayColor];
        leftLine.alpha = 0.2f;
        [self addSubview:leftLine];

        UILabel *rightLine = [[UILabel alloc] init];
        rightLine.frame = CGRectMake(_photoImageView.endPointX, _photoImageView.endPointY, 1, _categoryNameLabel.frame.size.height+_titleLabel.frame.size.height+6);
        rightLine.backgroundColor = [UIColor grayColor];
        rightLine.alpha = 0.2f;
        [self addSubview:rightLine];


//        _shareCountLabel = [[UILabel alloc] init];
//        _shareCountLabel.textColor = [UIColor blackColor];
//        _shareCountLabel.frame = CGRectMake(lineLabel.endPointX, _titleLabel.frame.origin.y + 10, DeviceWidth-lineLabel.endPointX-LEFTLEFTSET, 20);
//        _shareCountLabel.textAlignment = NSTextAlignmentCenter;
//        [self addSubview:_shareCountLabel];
        
//        UIImageView *shareImageView = [[UIImageView alloc] init];
//        shareImageView.frame = CGRectMake(lineLabel.endPointX+(DeviceWidth-lineLabel.endPointX-LEFTLEFTSET-15)/2, _shareCountLabel.endPointY + 8, 15, 14);
//        shareImageView.image = [UIImage imageNamed:@"share2"];
//        [self addSubview:shareImageView];
        
        [self addShortLineImage:rightLine.endPointY-5 withCurrentView:self];

        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    return self;
}

- (void)setJournal:(IMGMediaComment *)journal
{
    _journal = journal;
    __weak typeof (_photoImageView) weakPhotoImageView = _photoImageView;
    
    if ([_journal.imageUrl hasPrefix:@"http://"]||[_journal.imageUrl hasPrefix:@"https://"])
    {
//        [_photoImageView setImageWithURL:[NSURL URLWithString:_journal.imageUrl] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//            [UIView animateWithDuration:1.0 animations:^{
//                [weakPhotoImageView setAlpha:1.0];
//            }];
//        }];
        UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 160)];
        [_photoImageView sd_setImageWithURL:[NSURL URLWithString:_journal.imageUrl] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            image = [image imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 160)];
            
            [weakPhotoImageView setImage:image];
        }];
    }
    else
    {
        NSString *urlStr = [_journal.imageUrl returnFullImageUrlWithWidth:DeviceWidth-LEFTLEFTSET*2];
//        [_photoImageView setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//            [UIView animateWithDuration:1.0 animations:^{
//                [weakPhotoImageView setAlpha:1.0];
//            }];
//        }];
        UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 160)];

        [_photoImageView sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            image = [image imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 160)];
            [weakPhotoImageView setImage:image];

         
        }];

    }

    
    [_titleLabel setText:_journal.title];
    
//    [_shareCountLabel setText:[NSString stringWithFormat:@"%@",_journal.shareCount]];
    [_categoryNameLabel setText:_journal.categoryName];
    
}
- (void)addShortLineImage:(CGFloat)currentPointY withCurrentView:(UIView *)currentView
{
    UIImageView *lineImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(15, currentPointY+5, DeviceWidth-30, 1)];
    lineImage3.image = [UIImage imageNamed:@"CutOffRule"];
    currentPointY = lineImage3.endPointY;
    [currentView addSubview:lineImage3];
}

#pragma mark --

- (void)label:(PPLabel *)label didBeginTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
    
    [self highlightWordContainingCharacterAtIndex:charIndex];
}

- (void)label:(PPLabel *)label didMoveTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
    
    //[self highlightWordContainingCharacterAtIndex:charIndex];
}

- (void)label:(PPLabel *)label didEndTouch:(UITouch *)touch onCharacterAtIndex:(CFIndex)charIndex {
    
    //[self removeHighlight];
}

- (void)label:(PPLabel *)label didCancelTouch:(UITouch *)touch {
    
    //[self removeHighlight];
}

#pragma mark --

- (void)highlightWordContainingCharacterAtIndex:(CFIndex)charIndex {
    
    if (charIndex==NSNotFound) {
        
        //user did nat click on any word
        [self removeHighlight];
        return;
    }
    
    NSString* string = _categoryNameLabel.text;
    
    //compute the positions of space characters next to the charIndex
    NSRange end = [string rangeOfString:@", " options:0 range:NSMakeRange(charIndex, string.length - charIndex)];
    NSRange front = [string rangeOfString:@", " options:NSBackwardsSearch range:NSMakeRange(0, charIndex)];
    
    if (front.location == NSNotFound) {
        front.location = 0; //first word was selected
    }
    else{
        front.location = front.location+1;
    }
    
    if (end.location == NSNotFound) {
        end.location = string.length; //last word was selected
    }
    
    NSRange wordRange = NSMakeRange(front.location, end.location-front.location);
    
    if (front.location!=0) { //fix trimming
        wordRange.location += 1;
        wordRange.length -= 1;
    }
    
//    if (wordRange.location == self.highlightedRange.location) {
//        return; //this word is already highlighted
//    }
//    else {
//        [self removeHighlight]; //remove highlight on previously selected word
//    }
    
    self.highlightedRange = wordRange;
    
    //    //highlight selected word
    //    NSMutableAttributedString* attributedString = [self.label.attributedText mutableCopy];
    //    [attributedString addAttribute:NSBackgroundColorAttributeName value:[UIColor redColor] range:wordRange];
    //    self.label.attributedText = attributedString;
    
    NSLog(@"%@",[_categoryNameLabel.text substringWithRange:wordRange]);
    if([self.delegate respondsToSelector:@selector(categoryNameLabelClick:)])
    {
        [self.delegate categoryNameLabelClick:([_categoryNameLabel.text substringWithRange:wordRange])];
    }
    
}

- (void)removeHighlight {
    
    if (self.highlightedRange.location != NSNotFound) {
        
        //remove highlight from previously selected word
        NSMutableAttributedString* attributedString = [_categoryNameLabel.attributedText mutableCopy];
        [attributedString removeAttribute:NSBackgroundColorAttributeName range:self.highlightedRange];
        _categoryNameLabel.attributedText = attributedString;
        
        self.highlightedRange = NSMakeRange(NSNotFound, 0);
    }
}
-(UIImage *)OriginImage:(UIImage *)image scaleToSize:(CGSize)size{
    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}
@end
