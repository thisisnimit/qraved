//
//  CardJournalViewController.h
//  Qraved
//
//  Created by System Administrator on 3/4/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "HomeTableViewCell.h"
#import "BaseChildViewController.h"
#import "CardJournalView.h"
@class IMGMediaComment;

@interface CardJournalViewController : BaseChildViewController<HomeTableViewCellDelegate>


@property(nonatomic,strong) CardJournalView *journalView;

-(instancetype)initWithJournal:(IMGMediaComment*)journal;

@end
