//
//  V2_JournalHeaderView.h
//  Qraved
//
//  Created by harry on 2017/7/6.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGMediaComment.h"
@interface V2_JournalHeaderView : UIView


@property (nonatomic, strong) IMGMediaComment *journal;
@property (nonatomic,assign) CGFloat journalHeight;
@property (nonatomic, copy) void (^imageClick)();

@end
