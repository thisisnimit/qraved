//
//  FeedViewController.m
//  Qraved
//
//  Created by harry on 2018/3/16.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "FeedViewController.h"
#import "HMSegmentedControl.h"
#import "V2_TimeLineViewController.h"
#import "NotificationsViewController.h"

@interface FeedViewController ()<UIScrollViewDelegate>
{
    HMSegmentedControl *tab;
}
@end

@implementation FeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBarHidden = YES;
    [[self rdv_tabBarController] setTabBarHidden:NO animated:NO];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    self.navigationController.navigationBarHidden = NO;
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
}

- (void)initUI{
    self.view.backgroundColor = [UIColor whiteColor];
    
    NSArray *items = @[@"Notifications", @"Feeds"];
    tab=[[HMSegmentedControl alloc] initWithSectionTitles:items];
    tab.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    tab.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    tab.selectionIndicatorColor = [UIColor colorWithHexString:@"#D20000"];
    tab.selectedTitleTextAttributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:14],NSForegroundColorAttributeName : [UIColor colorWithHexString:@"#D20000"]};
    tab.titleTextAttributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:14],NSForegroundColorAttributeName : [UIColor color333333]};
    
    tab.selectionIndicatorHeight = 2.5;
    tab.selectedSegmentIndex = 0;
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    [tab addSubview:lineView];
    [self.view addSubview:tab];
    
    UIScrollView *feedScrollView = [UIScrollView new];
    feedScrollView.delegate = self;
    feedScrollView.showsHorizontalScrollIndicator = NO;
    feedScrollView.pagingEnabled = YES;
    [self.view sd_addSubviews:@[tab, lineView, feedScrollView]];
    
    tab.sd_layout
    .topSpaceToView(self.view, IMG_StatusBarHeight)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .heightIs(44);
    
    lineView.sd_layout
    .topSpaceToView(tab, 0)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .heightIs(1);
    
    feedScrollView.sd_layout
    .topSpaceToView(lineView, 0)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .bottomSpaceToView(self.view, 0);
    
    V2_TimeLineViewController *timelineViewController = [[V2_TimeLineViewController alloc] init];
    timelineViewController.view.frame = CGRectMake(DeviceWidth, 0, DeviceWidth, feedScrollView.bounds.size.height);
    [self addChildViewController:timelineViewController];
    
    
    NotificationsViewController *notificationViewController = [[NotificationsViewController alloc] init];
    notificationViewController.view.frame = CGRectMake(0, 0, DeviceWidth, feedScrollView.bounds.size.height);
    [self addChildViewController:notificationViewController];
    
    [feedScrollView sd_addSubviews:@[notificationViewController.view, timelineViewController.view]];
    
    feedScrollView.contentSize = CGSizeMake(DeviceWidth*2, 0);
    
    [tab setIndexChangeBlock:^(NSInteger index){
        [UIView animateWithDuration:0.5 animations:^{
            [feedScrollView setContentOffset:CGPointMake(DeviceWidth*index, 0)];
        }];
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    int index = scrollView.contentOffset.x/DeviceWidth;
    [UIView animateWithDuration:0.5 animations:^{
        tab.selectedSegmentIndex = index;
    }];
}


@end
