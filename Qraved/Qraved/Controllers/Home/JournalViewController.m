    //
//  JournalViewController.m
//  Qraved
//
//  Created by Lucky on 15/4/22.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "JournalViewController.h"
#import "GDataXMLNode.h"
#import "IMGMediaComment.h"
#import "MoreArticleViewController.h"
#import "AppDelegate.h"

#import "UIViewController+Helper.h"
#import "IMGMediaComment.h"
#import "UIConstants.h"
#import "UIImage+Resize.h"
#import "UIImageView+WebCache.h"
#import "Label.h"
#import "UIView+Helper.h"
#import <CoreText/CoreText.h>
#import "UIColor+Helper.h"
#import "WebViewController.h"
#import "NSString+Helper.h"
#import "LoadingView.h"
#import "DBManager.h"
#import "UILabel+Helper.h"
#import "UIDevice+Util.h"

@interface JournalViewController ()<NSXMLParserDelegate,UIScrollViewDelegate>
{
    UIScrollView *scrollView;
    NSMutableArray *_journaleArray;
    BOOL _isLoad;
    float journalStartY;
    NSNumber *bottomJournalId;
    long journalViewTag;
}
@end

@implementation JournalViewController




-(void)viewWillDisappear:(BOOL)animated
{

    [super viewWillDisappear:animated];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    journalStartY= 10.0;
    journalViewTag = 10000;
    self.navigationController.navigationBarHidden = NO;
    
    UILabel *navTitleLabel = [[UILabel alloc] init];
    navTitleLabel.textColor = [UIColor whiteColor];
    navTitleLabel.text = L(@"Qraved Journal");
    navTitleLabel.font = [UIFont boldSystemFontOfSize:20];
    navTitleLabel.frame = CGRectMake(DeviceWidth/2-navTitleLabel.expectedWidth/2, 2, navTitleLabel.expectedWidth, 38);
    self.navigationItem.titleView = navTitleLabel;


    _journaleArray = [[NSMutableArray alloc] init];
    

    
    self.view.backgroundColor = [UIColor whiteColor];
    //self.navigationController.title = self.title;
    [self setBackBarButtonOffset30];
    scrollView = [[UIScrollView alloc]initWithFrame:self.view.bounds];
    scrollView.delegate = self;
    [self.view addSubview:scrollView];
    
    [[LoadingView sharedLoadingView] startLoading];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView_
{
     CGPoint offset = scrollView_.contentOffset;
    CGSize size = scrollView_.contentSize;
    if (size.height - offset.y < 1500)
    {
        scrollView_.contentSize = CGSizeMake(0, scrollView_.contentSize.height + 2000);
        [self addMoreJournal];
    }

}
- (void)addView
{
    NSLog(@"%f,0.3",[[NSDate date]timeIntervalSince1970]);

    UIImage *placeHolderImage = [[UIImage imageNamed:DEFAULT_IMAGE_STRING2 ] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth, 160)];
    
    for (long i=journalViewTag-10000; i<_journaleArray.count; i++) {
        UIView *journalView = [[UIView alloc]initWithFrame:CGRectMake(0, journalStartY, DeviceWidth, DeviceWidth/3+40)];
        [scrollView addSubview:journalView];
        
        IMGMediaComment *journal = [_journaleArray objectAtIndex:i];
        bottomJournalId = journal.mediaCommentId;
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET,  0, DeviceWidth-2*LEFTLEFTSET, DeviceWidth/3)];
        [imageView sd_setImageWithURL:[NSURL URLWithString:journal.journalImageUrl] placeholderImage:placeHolderImage];
        imageView.userInteractionEnabled = YES;
        
        [journalView addSubview:imageView];
        CGSize size = [journal.journalTitle sizeWithFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:13] constrainedToSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 100)];
        Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, imageView.endPointY + 10, DeviceWidth-2*LEFTLEFTSET, size.height) andTextFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:13] andTextColor:[UIColor color222222] andTextLines:0];
        titleLabel.text = journal.journalTitle;
        titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [journalView addSubview:titleLabel];
        NSURL *url = [NSURL URLWithString:journal.link];
        NSString *urlHost = [url host];
        
        NSMutableString *titleAndDescription = [NSMutableString stringWithString:@""] ;
        [titleAndDescription appendFormat:@"%@",urlHost];
        
        CGSize titleSize = [titleAndDescription sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_ITALIC size:14] constrainedToSize:CGSizeMake(DeviceWidth-30, 100) lineBreakMode:NSLineBreakByWordWrapping];
        TYAttributedLabel *titleAndDescriptionLabel = [[TYAttributedLabel alloc] initWithFrame:CGRectMake(15, titleLabel.endPointY, DeviceWidth-30, titleSize.height)];
        titleAndDescriptionLabel.text =titleAndDescription;
        titleAndDescriptionLabel.textColor = [UIColor color333333];
        titleAndDescriptionLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12];
        [journalView addSubview:titleAndDescriptionLabel];
        
        NSMutableAttributedString *AttributedString = [[NSMutableAttributedString alloc] initWithString:titleAndDescription];
        NSRange range = [titleAndDescription rangeOfString:[NSString stringWithFormat:@"%@",urlHost]];
        [AttributedString addAttributes:@{NSForegroundColorAttributeName:[UIColor color222222],NSFontAttributeName:[UIFont fontWithName:FONT_OPEN_SANS_ITALIC size:14]} range:NSMakeRange(range.location, range.length)];
        titleAndDescriptionLabel.attributedText = AttributedString;
        titleAndDescriptionLabel.verticalAlignment = TYVerticalAlignmentCenter;
        //====1
        journalStartY = journalView.endPointY+27;
        
        journalView.tag = journalViewTag;
        journalViewTag++;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gotoJournal:)];
        [journalView addGestureRecognizer:tapGesture];
    }
    NSLog(@"%f,0.4",[[NSDate date]timeIntervalSince1970]);

    if (journalStartY<scrollView.frame.size.height) {
        scrollView.contentSize = CGSizeMake(DeviceWidth, scrollView.frame.size.height+64);
    }else{
        scrollView.contentSize = CGSizeMake(DeviceWidth, journalStartY+64);
    }
    [[LoadingView sharedLoadingView] stopLoading];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    if (_isLoad==FALSE)
    {
        _isLoad = TRUE;
        [self getInfo];
    }
}

- (void)getInfo
{
    NSLog(@"%f,0.1",[[NSDate date]timeIntervalSince1970]);

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *urlStr = @"http://www.qraved.com/journal/feed/";
        //NSString *urlStr = @"http://staging.qraved.com/journal/feed/";
        NSString* url = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString* xml = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:nil];
        
        GDataXMLDocument* doc = [[GDataXMLDocument alloc]initWithXMLString:xml options:0 error:nil];
        
        //获取根节点（Users）
        GDataXMLElement *rootElement = [doc rootElement];
        
        //获取根节点下的节点（User）
        NSArray *channel = [rootElement elementsForName:@"channel"];
        
        NSMutableArray *linkAry = [[NSMutableArray alloc] init];
        
        //NSLog(@"channelCount = %ld",channel.count);
        for (GDataXMLElement *channelEle in channel)
        {
            NSArray *itemAry = [channelEle elementsForName:@"item"];
            for (GDataXMLElement *item in itemAry)
            {
                GDataXMLElement *titleElement = [[item elementsForName:@"title"] objectAtIndex:0];
                NSString *title = [titleElement stringValue];
                NSLog(@"title = %@",title);
                
                
                GDataXMLElement *linkElement = [[item elementsForName:@"link"] objectAtIndex:0];
                NSString *linkStr = [linkElement stringValue];
                NSLog(@"link = %@",linkStr);
                [linkAry addObject:linkStr];
                
                GDataXMLElement *descriptionElement = [[item elementsForName:@"description"] objectAtIndex:0];
                NSString *descriptionStr = [descriptionElement stringValue];
                NSString *imageUrl = [descriptionStr getHtmlJournalImageUrl];
                NSLog(@"imageUrl = %@",imageUrl);
                
                GDataXMLElement *contentElement = [[item elementsForName:@"content:encoded"] objectAtIndex:0];
                NSString *contentStr = [contentElement stringValue];
                //NSLog(@"contentStr = %@",contentStr);
                NSMutableString *contentFirstStr = [[NSMutableString alloc] init];
                                
                NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"contentStyle" ofType:@"plist"];
                NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
                contentFirstStr = [data objectForKey:@"content"];
                NSString *cssTitleStr = [NSString stringWithFormat:@"<div class=\"title\">%@</div>",title];
                NSString *newContentsStr = [[NSString alloc] initWithFormat:@"%@%@%@%@",contentFirstStr,cssTitleStr,contentStr,[data objectForKey:@"contentEnd"]];
                //category
                GDataXMLElement *guidElement = [[item elementsForName:@"guid"] objectAtIndex:0];
                NSString *guidStr = [guidElement stringValue];
                NSArray *guiIdArr = [guidStr componentsSeparatedByString:@"?p="];
                
                IMGMediaComment *journal = [[IMGMediaComment alloc]init];
                journal.title = title;
                journal.link = linkStr;
                journal.imageUrl = imageUrl;
                journal.contents = newContentsStr;
                journal.journalImageUrl = imageUrl;
                journal.journalTitle = title;

                if (guiIdArr.count!= 2)
                {
                    journal.mediaCommentId = 0 ;
                }
                else
                journal.mediaCommentId = [NSNumber numberWithInt:[[guiIdArr objectAtIndex:1] intValue]] ;
                
                [_journaleArray addObject:journal];
                
                [[DBManager manager] insertModel:journal selectField:@"mediaCommentId" andSelectID:journal.mediaCommentId];
            }
        }
        NSLog(@"%f,0.2",[[NSDate date]timeIntervalSince1970]);

    
    if (_journaleArray.count)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self addView];
        });
    }
    });
    
}
- (void)addMoreJournal
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        FMResultSet *resultSet = [[DBManager manager] executeQuery:[NSString stringWithFormat:@"select  * from IMGMediaComment WHERE mediaCommentId < '%@' order by mediaCommentId desc limit 10;",bottomJournalId]];
        //[_journaleArray removeAllObjects];
        while ([resultSet next])
        {
            IMGMediaComment *journal = [[IMGMediaComment alloc] init];
            [journal setValueWithResultSet:resultSet];
            [_journaleArray addObject:journal];
        }
        [resultSet close];
    
        dispatch_async(dispatch_get_main_queue(), ^{
            [self addView];
        });
    });

    
    //[self addView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationItem setTitle:L(@"Journal")];
    self.navigationController.navigationBarHidden = NO;
}
-(void)gotoJournal:(UITapGestureRecognizer*)tapGesture{
    IMGMediaComment *media = [_journaleArray objectAtIndex:tapGesture.view.tag - 10000];
    NSLog(@"media contents = %@",media.contents);
    WebViewController *webViewController = [[WebViewController alloc]init];
    webViewController.webSite = media.link;
    webViewController.title = media.title;
    webViewController.content = media.contents;
    webViewController.fromDetail = NO;
    [self.navigationController pushViewController:webViewController animated:YES];
    
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[LoadingView sharedLoadingView] stopLoading];
}
 
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
