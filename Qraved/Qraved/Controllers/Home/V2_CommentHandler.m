//
//  V2_CommentHandler.m
//  Qraved
//
//  Created by harry on 2017/6/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_CommentHandler.h"
#import "IMGMediaComment.h"
#import "V2_CommentModel.h"

@implementation V2_CommentHandler

+ (void)getCommentListWithParams:(NSDictionary *)params andPath:(NSString *)path andBlock:(void (^)(V2_CommentsModel *model,int total))block{
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:params];
    [dic setObject:[IMGUser currentUser].userId forKey:@"userId"];
    [[IMGNetWork sharedManager] GET:path parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        V2_CommentsModel *model = [[V2_CommentsModel alloc] init];
        NSMutableArray *commentList = [NSMutableArray array];
        NSArray *commentArray = [responseObject objectForKey:@"commentList"];
        for (NSDictionary *dic in commentArray) {
            V2_CommentModel *commentModel = [[V2_CommentModel alloc] init];
            [commentModel setValuesForKeysWithDictionary:dic];
            
            [commentList addObject:commentModel];
        }
        IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
        [restaurant setValuesForKeysWithDictionary:[responseObject objectForKey:@"restaurant"]];
        

//        commentList=[IMGMediaComment commentListFromArray:commentArray];
        model.isVendor = [responseObject objectForKey:@"isVendor"];
        model.restaurant = restaurant;
        model.commentList = commentList;
        
        block(model,[[responseObject objectForKey:@"commentCount"] intValue]);

        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

+ (void)postCommentWithParams:(NSDictionary *)params andPath:(NSString *)path andBlock:(void (^)(V2_CommentModel *model))block{
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:params];
    [dic setObject:[IMGUser currentUser].userId forKey:@"userId"];
    [dic setObject:[IMGUser currentUser].token forKey:@"t"];
    [dic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [dic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    
    [[IMGNetWork sharedManager]POST:path parameters:dic progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         
         V2_CommentModel *model = [[V2_CommentModel alloc] init];
         if ([responseObject objectForKey:@"comment"] ==nil) {
             [model setValuesForKeysWithDictionary:[responseObject objectForKey:@"commentInfo"]];
         }else{
             [model setValuesForKeysWithDictionary:[responseObject objectForKey:@"comment"]];
         }
         
         block(model);
      
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
      
     }];

}


@end
