//
//  HomeViewController.m
//  DDScrollViewController Example
//
//  Created by Hirat on 13-11-8.
//  Copyright (c) 2013年 Hirat. All rights reserved.
//

#import "TempViewControler.h"
#import "HomeViewController.h"
#import "JournalListViewController.h"
#import "DiningGuideListViewController.h"
@interface TempViewControler ()
@property (nonatomic, strong) NSMutableArray* viewControllerArray;
@end

@implementation TempViewControler

- (void)viewDidLoad
{
    self.dataSource = self;
    
    [super viewDidLoad];
    [self initViewcontrllers];
}

-(void)initViewcontrllers{
    NSUInteger numberOfPages = 3;
    self.viewControllerArray = [[NSMutableArray alloc] initWithCapacity:numberOfPages];
    for (NSUInteger k = 0; k < numberOfPages; ++k)
    {
        [self.viewControllerArray addObject:[NSNull null]];
    }
    HomeViewController *homeViewController = [[HomeViewController alloc] init];
    JournalListViewController *JVC = [[JournalListViewController alloc] init];
    DiningGuideListViewController *dglvc = [[DiningGuideListViewController alloc] init];

    [self.viewControllerArray replaceObjectAtIndex: 0 withObject:homeViewController];
    [self.viewControllerArray replaceObjectAtIndex: 1 withObject: JVC];
    [self.viewControllerArray replaceObjectAtIndex: 2 withObject: dglvc];


}
#pragma mark - DDScrollViewDataSource

- (NSUInteger)numberOfViewControllerInDDScrollView:(DDScrollViewController *)DDScrollView
{
    return [self.viewControllerArray count];
}

- (UIViewController*)ddScrollView:(DDScrollViewController *)ddScrollView contentViewControllerAtIndex:(NSUInteger)index
{
    return [self.viewControllerArray objectAtIndex:index];
}

@end
