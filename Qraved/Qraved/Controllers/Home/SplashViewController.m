//
//  SplashViewController.m
//  Qraved
//
//  Created by Lucky on 15/11/4.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import "SplashViewController.h"
#import "HomeUtil.h"
#import "UIImageView+WebCache.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import "UILabel+Helper.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "DetailViewController.h"
#import "UIViewController+Helper.h"
#import "NavigationBarButtonItem.h"
#import "UIDevice+Util.h"
#import "NotificationHandler.h"
#import "AppDelegate.h"
#import "UIImage+Resize.h"

@interface SplashViewController ()
{
    UIScrollView *_scrollView;
    IMGSplash *_splash;
}
@end

@implementation SplashViewController

- (id)initWithSplash:(IMGSplash *)splash
{
    self = [super init];
    if (self) {
        _splash = splash;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBarHidden = YES;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _scrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    _scrollView.backgroundColor = [UIColor whiteColor];
    _scrollView.showsVerticalScrollIndicator = NO;
    
    //解决冲突 禁止系统偏移 
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self.view addSubview:_scrollView];

    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token && [_splashIdFromNotification intValue])
    {
        NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",_splashIdFromNotification,@"splashId", nil];
        [NotificationHandler readSplashWithParams:params];
    }
    
//    NavigationBarButtonItem *navigationBarLeftButton = [[NavigationBarButtonItem alloc ]initWithImage:[UIImage imageNamed:NavigationCloseImage] andTitle:@"" target:self action:@selector(goToBackViewController) width:53.0f offset:[UIDevice isLaterThanIos6]?30:0];
//    self.navigationItem.rightBarButtonItem = navigationBarLeftButton;

    if (self.isFromHome)
    {
        [self loadMainView];
    }
    else
    {
        [self initData];
    }
}

- (void)goToBackViewController
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
- (void)initData
{
    [HomeUtil getSplashWithBlock:^(IMGSplash *splash) {
        _splash = splash;
        [self loadMainView];

    }];

}
- (void)loadMainView
{
    CGFloat currentY=0.0f;
    UIImageView *topImageView = [[UIImageView alloc] init];
    topImageView.frame = CGRectMake(0, 0, DeviceWidth, DeviceWidth/1.6);
    NSString *urlString = [_splash.image returnFullImageUrlWithWidth:DeviceWidth];
    __weak typeof (topImageView) weakPhotoImageView = topImageView;
    
    [topImageView setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth, DeviceWidth/1.6)]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        if (image) {
            weakPhotoImageView.image = [image imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth, DeviceWidth/1.6) ];
        }
        
    }];
    
    //创建返回按钮
    UIButton *backButton = [[UIButton alloc]init];
    [self.view addSubview:backButton];
//    [backButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.view.mas_top).offset(15);
//        make.right.equalTo(self.view.mas_right).offset(-20);
//    }];
    float margin = 10;
    float btnWidth = 35;
    float btnHeight = btnWidth;
    backButton.frame = CGRectMake(DeviceWidth-btnWidth-margin,margin, btnWidth, btnHeight);
    
    //设置默认和高亮图片
    [backButton setImage:[UIImage imageNamed:@"closeWhite.png"] forState:UIControlStateNormal];
//    [backButton setImage:[UIImage imageNamed:@"closeWhite@2x.png"] forState:UIControlStateHighlighted];
    
//    //按钮大小与图片大小相同
//    [backButton sizeToFit];
    
    //用户交互
    topImageView.userInteractionEnabled = YES;
    
    //添加按钮的点击方法
//    [backButton addTarget:self action:@selector(goToBackViewController) forControlEvents:UIControlEventTouchUpInside];
    
    [backButton addTarget:self action:@selector(backbuttonClick) forControlEvents:UIControlEventTouchUpInside];
    
    [_scrollView addSubview:topImageView];
    
    UIFont *titleFont=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:19];
    CGSize titleSize=[_splash.contentTitle sizeWithFont:titleFont constrainedToSize:CGSizeMake(DeviceWidth-LEFTLEFTSET*2,1000) lineBreakMode:NSLineBreakByWordWrapping];

    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = [_splash.contentTitle removeHTML];
    titleLabel.frame = CGRectMake(LEFTLEFTSET, topImageView.endPointY + 20, titleSize.width, titleSize.height);
    titleLabel.font = titleFont;
    titleLabel.numberOfLines=0;
    titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
    titleLabel.textColor = [UIColor color333333];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [_scrollView addSubview:titleLabel];
    
    UILabel *descriptionLabel = [[UILabel alloc] init];
    descriptionLabel.text = [_splash.splashDescription removeHTML];
    descriptionLabel.numberOfLines = 0;
    descriptionLabel.font = [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:13];
    descriptionLabel.textColor  = [UIColor colorWithRed:136/255.0 green:136/255.0 blue:136/255.0 alpha:1];
    CGSize size = [descriptionLabel.text sizeWithFont:descriptionLabel.font constrainedToSize:CGSizeMake(DeviceWidth - LEFTLEFTSET*2, 10000) lineBreakMode:NSLineBreakByWordWrapping];
    descriptionLabel.frame = CGRectMake(LEFTLEFTSET, titleLabel.endPointY + 15, size.width, size.height);
    [_scrollView addSubview:descriptionLabel];

    currentY=descriptionLabel.endPointY + 30;

    if(![_splash.button isBlankString] && _splash.buttonType.length != 0){
        UIFont *btnFont=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:15];
        CGSize btnSize=[_splash.button sizeWithFont:btnFont constrainedToSize:CGSizeMake(DeviceWidth-LEFTLEFTSET*2,1000) lineBreakMode:NSLineBreakByWordWrapping];    

        UIButton *viewBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        viewBtn.backgroundColor = [UIColor colorRed];
        [viewBtn setTitle:[_splash.button removeHTML] forState:UIControlStateNormal];
        viewBtn.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
        // viewBtn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
        viewBtn.titleLabel.textColor=[UIColor whiteColor];
        viewBtn.titleLabel.textAlignment=NSTextAlignmentCenter;
        viewBtn.titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
        viewBtn.titleLabel.numberOfLines=0;
        viewBtn.titleLabel.font=btnFont;
        viewBtn.frame = CGRectMake(LEFTLEFTSET,currentY, DeviceWidth-LEFTLEFTSET*2, btnSize.height+30);
        [viewBtn.layer setCornerRadius:5.0];
        [viewBtn addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];

//        UIView *btnBackgroundView = [[UIView alloc] init];
//        btnBackgroundView.frame = CGRectMake(0, DeviceHeight - 100, DeviceWidth-LEFTLEFTSET*2, btnSize.height+30);
//        [self.view addSubview:btnBackgroundView];    
        [_scrollView addSubview:viewBtn];

        currentY+=btnSize.height+30;
    }
    
    _scrollView.contentSize = CGSizeMake(0, currentY);
}
//backButton点击方法
-(void)backbuttonClick
{
    //判断
    if (self.isFromHome) {
        [self goToBackViewController];
        [[AppDelegate ShareApp] addPopLoginNSTimer];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)btnClick
{
    [self dismissViewControllerAnimated:YES completion:^{
        if([self.svcDelegate respondsToSelector:@selector(btnClickWithSplash:)])
        {
            [self.svcDelegate btnClickWithSplash:_splash];
            if (self.isFromHome){
                [[AppDelegate ShareApp] addPopLoginNSTimer];
            }
        }
    }];
    
}

 
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
