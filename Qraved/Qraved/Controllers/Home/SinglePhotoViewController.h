//
//  SinglePhotoViewController.h
//  Qraved
//
//  Created by System Administrator on 3/9/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IMGDish;

@interface SinglePhotoViewController : UIViewController

-(instancetype)initWithDish:(IMGDish*)dish;

@end
