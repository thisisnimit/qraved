//
//  V2_JournalBottomBarView.m
//  Qraved
//
//  Created by harry on 2017/8/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_JournalBottomBarView.h"

@implementation V2_JournalBottomBarView
{
    UIButton *btnLike;
    UIButton *btnComment;
    UIButton *btnLikeCount;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        //[self createUI];
    }
    return self;
}

- (void)createUI:(IMGMediaComment *)currentJournal{
    
    self.backgroundColor = [UIColor whiteColor];
    
    UIView *topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 0.5)];
    topLine.backgroundColor = [UIColor colorWithHexString:@"C1C1C1"];
    [self addSubview:topLine];
    
    UIView *btnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0.5, DeviceWidth-150, 50)];
    [self addSubview:btnView];
    
//    UILikeButton *likeButton = [[UILikeButton alloc] initInSuperView:btnView liked:_journal.isLike likeCount:[_journal.likeCount intValue] isFromPhotoViewer:NO];
//    likeButton.communityDelegate = self;
    
    btnLike = [UIButton buttonWithType:UIButtonTypeCustom];
    btnLike.frame = CGRectMake(0, 0, 100, 50);
    btnLike.titleLabel.font = [UIFont systemFontOfSize:12];
    
    [btnView addSubview:btnLike];
    

    if(currentJournal.isLike){
        [btnLike setTitleColor:[UIColor colorWithRed:234/255.0f green:98/255.0f blue:98/255.0f alpha:1] forState:UIControlStateNormal];
        [btnLike setImage:[UIImage imageNamed:@"like_red1.png"] forState:UIControlStateNormal];
    }else{
        [btnLike setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
        [btnLike setImage:[UIImage imageNamed:@"ic_like.png"] forState:UIControlStateNormal];
    }
    
    if ([currentJournal.likeCount intValue]>0 && [currentJournal.likeCount intValue] ==1) {
        [btnLike setTitle:@"1 Like" forState:UIControlStateNormal];
    }else if ([currentJournal.likeCount intValue] >1){
        [btnLike setTitle:[NSString stringWithFormat:@"%d Likes",[currentJournal.likeCount intValue]] forState:UIControlStateNormal];
    }else{
        [btnLike setTitle:@"Like" forState:UIControlStateNormal];

    }
    [btnLike bk_whenTapped:^{
        if (self.pressLiked) {
            self.pressLiked();
        }
    }];
    
    btnLike.adjustsImageWhenHighlighted = NO;
    btnLike.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    
    
    btnLikeCount = [UIButton buttonWithType:UIButtonTypeSystem];
    btnLikeCount.frame = CGRectMake(45, 0, 55, 50);
    [self addSubview:btnLikeCount];
    [btnLikeCount bk_whenTapped:^{
        if (self.pressLikedCount) {
            self.pressLikedCount();
        }
    }];
    
    btnComment = [UIButton buttonWithType:UIButtonTypeCustom];
    btnComment.frame = CGRectMake(btnLike.endPointX+10, 0, 105, 50);
    btnComment.titleLabel.font = [UIFont systemFontOfSize:12];
    [btnComment setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
    [btnComment setImage:[UIImage imageNamed:@"ic_comment.png"] forState:UIControlStateNormal];
    [btnView addSubview:btnComment];
    btnComment.adjustsImageWhenHighlighted = NO;
    
    btnComment.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    
    if ([currentJournal.commentCount intValue]>0 && [currentJournal.commentCount intValue] ==1) {
        [btnComment setTitle:@"1 Comment" forState:UIControlStateNormal];
    }else if ([currentJournal.commentCount intValue] >1){
        [btnComment setTitle:[NSString stringWithFormat:@"%d Comments",[currentJournal.commentCount intValue]] forState:UIControlStateNormal];
    }else{
        [btnComment setTitle:@"Comment" forState:UIControlStateNormal];
        
    }
    
    [btnComment bk_whenTapped:^{
        if (self.pressComment) {
            self.pressComment();
        }
    }];
    
//    UICommentButton *commentButton = [[UICommentButton alloc] initInSuperView:btnView commentCount:[_journal.commentCount intValue] isFromPhotoViewer:NO];
//    commentButton.communityDelegate = self;
    
    
    UIButton *btnLine = [UIButton buttonWithType:UIButtonTypeCustom];
    btnLine.frame = CGRectMake(DeviceWidth-15-24, 0, 24, 50);
    [btnLine setImage:[UIImage imageNamed:@"ic_line"] forState:UIControlStateNormal];
    [btnLine bk_whenTapped:^{
        if (self.shareToLine) {
            self.shareToLine();
        }
    }];
    [self addSubview:btnLine];
    
    UIButton *btnFacebook = [UIButton buttonWithType:UIButtonTypeCustom];
    btnFacebook.frame = CGRectMake(DeviceWidth-49-24, 0, 24, 50);
    [btnFacebook setImage:[UIImage imageNamed:@"ic_fb"] forState:UIControlStateNormal];
    [btnFacebook bk_whenTapped:^{
        if (self.shareToFB) {
            self.shareToFB();
        }
    }];
    [self addSubview:btnFacebook];
    
    UIButton *btnLink = [UIButton buttonWithType:UIButtonTypeCustom];
    btnLink.frame = CGRectMake(DeviceWidth-73-34, 0, 24, 50);
    [btnLink setImage:[UIImage imageNamed:@"ic_insert_link"] forState:UIControlStateNormal];
    [btnLink bk_whenTapped:^{
        if (self.copyLink) {
            self.copyLink();
        }
    }];
    [self addSubview:btnLink];
}

- (void)setJournal:(IMGMediaComment *)journal{
    _journal = journal;
    
    [self createUI:journal];
}


- (void)updateLikeButtonStatus:(BOOL)liked_ likeCount:(int)likeCount{
    if(liked_){
        [btnLike setTitleColor:[UIColor colorWithRed:234/255.0f green:98/255.0f blue:98/255.0f alpha:1] forState:UIControlStateNormal];
        [btnLike setImage:[UIImage imageNamed:@"like_red1.png"] forState:UIControlStateNormal];
    }else{
        [btnLike setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
        [btnLike setImage:[UIImage imageNamed:@"ic_like.png"] forState:UIControlStateNormal];
    }
    
    if (likeCount>0 && likeCount ==1) {
        [btnLike setTitle:@"1 Like" forState:UIControlStateNormal];
    }else if (likeCount >1){
        [btnLike setTitle:[NSString stringWithFormat:@"%d Likes",likeCount] forState:UIControlStateNormal];
    }else{
        [btnLike setTitle:@"Like" forState:UIControlStateNormal];
        
    }

}

- (void)updateCommentButton:(int)commentCount{
    if (commentCount>0 && commentCount ==1) {
        [btnComment setTitle:@"1 Comment" forState:UIControlStateNormal];
    }else if (commentCount >1){
        [btnComment setTitle:[NSString stringWithFormat:@"%d Comments",commentCount] forState:UIControlStateNormal];
    }else{
        [btnComment setTitle:@"Comment" forState:UIControlStateNormal];
        
    }

}

- (void)communityButtonTapped:(UIButton *)button{
    if(button.tag==0){
        IMGUser *currentUser = [IMGUser currentUser];
        if (currentUser.userId == nil || currentUser.token == nil){
            [[AppDelegate ShareApp] goToLoginControllerByDelegate:self dictionary:@{@"likeCommentShareButton":button}];
            return;
        }
       // [self.delegate likeCommentShareView:self likeButtonTapped:button];
    }else if(button.tag==1){
        
       // [self.delegate likeCommentShareView:self commentButtonTapped:button];
    }
}
@end
