//
//  V2_TimeLineViewController.m
//  Qraved
//
//  Created by harry on 2017/8/10.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_TimeLineViewController.h"
#import "HomeUtil.h"
#import "EntityKind.h"
#import "ListHandler.h"
#import "RestaurantHandler.h"

#import "IMGMediaComment.h"
#import "IMGDiningGuide.h"
#import "IMGMenuCard.h"
#import "IMGUserReview.h"
#import "IMGReservation.h"

#import "HomeJournalTableViewCell.h"
#import "HomeDiningGuideTableViewCell.h"
#import "HomeEngageTableViewCell.h"
#import "HomeEngageOfferTableViewCell.h"
#import "HomeRestUpdateTableViewCell.h"
#import "HomeTrendingRestTableViewCell.h"
#import "HomeReviewTableViewCell.h"
#import "HomeUploadPhotoTableViewCell.h"
#import "HomeMenuTableViewCell.h"
#import <SDWebImage/SDWebImagePrefetcher.h>
#import "SavedAddListView.h"
#import "BookRatingSplashView.h"
#import "SimpleLoadingImageView.h"

#import "V2_CommentListViewController.h"
#import "AlbumViewController.h"
#import "MapViewController.h"
#import "MenuPhotoViewColltroller.h"
#import "V2_DiningGuideListViewController.h"
#import "DiningGuideRestaurantsViewController.h"
#import "ReviewPublishViewController.h"
#import "AddToListController.h"
#import "JournalDetailViewController.h"
#import "DetailViewController.h"
#import "OtherProfileViewController.h"
#import "ReviewCardViewController.h"
#import "RestUpdateCardViewController.h"
#import "UploadPhotoCardViewController.h"
#import "JournalCardViewController.h"
#import "OfferCardViewController.h"
#import "SplashViewController.h"

#import "MenuPhotoActivityItemProvider.h"
#import "JournalActivityItemProvider.h"
#import "RestaurantActivityItemProvider.h"
#import "TwitterActivityItemProvider.h"
#import "RestaurantOfferActivityItemProvider.h"
#import "RestaurantEventActivityItemProvider.h"
#import "ReviewActivityItemProvider.h"
#import "RestaurantMutiPromoActivityItemProvider.h"
#import "RestaurantMutiPhotosActivityItemProvider.h"

@interface V2_TimeLineViewController ()<UITableViewDelegate,UITableViewDataSource,HomeTableViewCellDelegate,HomeJournalTableViewCellDelegate,homeEngageOfferDelegate,HomeRestUpdateTableViewCellDelegate,HomeRestUpdateTableViewCellDelegate,HomeTrendingRestTableViewCellDelegate,HomeReviewTableViewCellDelegate,HomeUploadPhotoTableViewCellDelegate,HomeMenuTableViewCellDelegate,AlbumReloadCellDelegate,SavedAddListViewDelegate,BookRatingSplashViewDelegate,ZYQAssetPickerControllerDelegate,UINavigationControllerDelegate,CDPDelegate,SplashViewControllerDelegate>{
    UITableView *timelineTableView;
    NSMutableArray *timeLineArray;
    int minId;
    NSInteger pressRestaurantId;
    IMGRestaurant *pressRestaurant;
    NSIndexPath *pressIndexPath;
    IMGReservation *restaurantSplash;
    SimpleLoadingImageView *loadImageView;
}

@end

@implementation V2_TimeLineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"Feeds";
    
    [self loadMainUI];
    [self loadData];
    [self loadCacheData];
    //[self requestData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
//    [AppDelegate ShareApp].isSlideHomeHidden = NO;
//    [[self rdv_tabBarController] setTabBarHidden:NO animated:NO];
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
//    self.navigationController.navigationBarHidden=NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:YES];
    
    self.screenName=@"Home_page_1";
    
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillHideNotification object:nil];
    //[self popupSplashView];
    [[Amplitude instance] logEvent:@"RC - View Timeline" withEventProperties:@{}];
    [[Amplitude instance] logEvent:@"RC - View Homepage" withEventProperties:@{}];
    
}

#pragma mark - splash
- (void)popupSplashView
{
    [HomeUtil getSplashWithBlock:^(IMGSplash *splash) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isShowSplashView"])
        {
            if ([splash.splashId intValue] == 0)
            {
                [[AppDelegate ShareApp] addPopLoginNSTimer];
                return ;
            }
            
            [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isShowSplashView"];
            SplashViewController *svc = [[SplashViewController alloc] initWithSplash:splash];
            svc.svcDelegate = self;
            svc.isFromHome = YES;
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:svc];
            [self.navigationController presentViewController:nav animated:NO completion:^{
                
            }];
            return;
        }
        
    }];
}



- (void)loadData{
    timeLineArray = [NSMutableArray array];
    minId = 0;
}

- (void)loadMainUI{
    timelineTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+20-49-64) style:UITableViewStylePlain];
    timelineTableView.delegate = self;
    timelineTableView.dataSource = self;
    timelineTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:timelineTableView];
    
    if (@available(iOS 11.0, *)) {
        timelineTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        timelineTableView.estimatedRowHeight = 0;
    }
    
    timelineTableView.mj_header= [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        minId = 0;
        [timeLineArray removeAllObjects];
        [self requestData];
    }];
    
    timelineTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [[SDImageCache sharedImageCache] setValue:nil forKey:@"memCache"];
        [self requestData];
    }];
    
}

- (void)loadCacheData{
    NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *pathStr = [pathArray objectAtIndex:0];
    //获取文件的完整路径
    NSString *filePatchOld = [pathStr stringByAppendingPathComponent:@"HomeLastData.plist"];
    NSString *filePatchStickCards = [pathStr stringByAppendingPathComponent:@"HomeLastDatastickCards.plist"];
    //读取plist文件的内容
    id dataArr;
    NSMutableArray *dataArrOld = [[NSMutableArray alloc] initWithContentsOfFile:filePatchOld];
    NSDictionary *dataDicStickCards = [[NSDictionary alloc] initWithContentsOfFile:filePatchStickCards];
    //  NSLog(@"---plist一开始保存时候的内容---%@",dataArrM);
    
    if ((dataArrOld !=nil) || (dataDicStickCards !=nil))
    {
        if(dataArrOld !=nil) {
            dataArr = dataArrOld;
        }
        if (dataDicStickCards !=nil){
            dataArr = dataDicStickCards;
        }
        NSDictionary *processingDataArr = [HomeUtil processingData:dataArr];
      
        [timeLineArray addObjectsFromArray:[processingDataArr objectForKey:@"data"]];
        [timelineTableView reloadData];
   
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //刷新完成
            [self requestData];
        });
        if (dataArrOld !=nil) {
            [[AppDelegate ShareApp] removeEcodingFile:filePatchOld];
        }
    }
    else
    {
        loadImageView= [[SimpleLoadingImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-44/2+42)];
        [self.view addSubview:loadImageView];
        
        [self requestData];
        
    }

}

- (void)requestData{
    IMGUser *user = [IMGUser currentUser];
    
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    if (user.userId && user.token)
    {
        [dic setObject:user.userId forKey:@"userId"];
    }
    [dic setObject:[NSNumber numberWithInt:10] forKey:@"max"];
    [dic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
    [dic setObject:[NSNumber numberWithInt:minId] forKey:@"minId"];
    NSLog(@"%f,getHomeData 1.0",[[NSDate date]timeIntervalSince1970]);
    [HomeUtil getNewHomeDataFromService:dic andBlock:^(NSArray *dataArr,int minIdTemp,NSArray *imageUrlsArr) {
        
        [loadImageView removeFromSuperview];
        
        if (dataArr.count==0) {
            timelineTableView.mj_footer.hidden = YES;
        }else{
            timelineTableView.mj_footer.hidden = NO;
        }
        if (minId == 0) {
            [timeLineArray removeAllObjects];
        }
        
        [[SDWebImagePrefetcher sharedImagePrefetcher] prefetchURLs:imageUrlsArr];
        [timeLineArray addObjectsFromArray:dataArr];
        [timelineTableView reloadData];
        
        [timelineTableView.mj_header endRefreshing];
        [timelineTableView.mj_footer endRefreshing];
        minId = minIdTemp;
        
//        dispatch_async(dispatch_get_main_queue(), ^{
//            //刷新完成
////            minId = minIdTemp;
////            [self getNextPageData];
//        });
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (timeLineArray.count>0) {
        return timeLineArray.count;
    }else{
        return 0;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *timeline=[timeLineArray objectAtIndex:indexPath.row];
    NSNumber *typeId = [[timeline allKeys] firstObject];
    
    switch ([typeId intValue]) {
        case 2:{
            static NSString *idenStr = @"home_journal";
            HomeJournalTableViewCell* cell=[tableView dequeueReusableCellWithIdentifier:idenStr];
            if(cell==nil){
                cell=[[HomeJournalTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
            }
            cell.homeTableViewCellDelegate=self;
            cell.homeJournalTableViewCellDelegate=self;
            IMGMediaComment *journal=[timeline objectForKey:typeId];
            cell.journal = journal;
            cell.fromCellIndexPath=indexPath;
            [[Amplitude instance] logEvent:@"RC - View Journal Card" withEventProperties:@{@"Location":@"Timeline"}];
            //[cell useCellFrameCacheWithIndexPath:indexPath tableView:tableView];
            return cell;
        }
            break;
        case 4:{
            static NSString *idenStr=@"home_dining_guide";
            HomeDiningGuideTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:idenStr];
            if(cell==nil){
                cell=[[HomeDiningGuideTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
            }
            cell.delegate=self;
            IMGDiningGuide *diningGuide=[timeline objectForKey:typeId];
            cell.title=diningGuide.typeName;
            cell.date=[Date getTimeInteval_v4:[diningGuide.timeline longLongValue]/1000];
            cell.fromCellIndexPath=indexPath;
            [cell setDiningguide:diningGuide];
            //[cell useCellFrameCacheWithIndexPath:indexPath tableView:tableView];
            return cell;
        }
        case 8:{
            
            static NSString* identifier = @"cellEngageOffer";
            HomeEngageOfferTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell){
                cell = [[HomeEngageOfferTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                cell.homeOfferCellDelegate=self;
                cell.homeTableViewCellDelegate=self;
                cell.fromCellIndexPath=indexPath;
            }
            [cell setHomeEngageTableViewCellType:HomeEngageTableViewCellOffer];
            IMGOfferCard  *offermodel=[timeline objectForKey:typeId];
            cell.offerModel = offermodel;
            //[cell useCellFrameCacheWithIndexPath:indexPath tableView:tableView];
            return cell;
            
        }
        case 10:
        {
            static NSString* identifier = @"cellRestUpdate";
            HomeRestUpdateTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            
            if (!cell)
            {
                cell = [[HomeRestUpdateTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            }
            cell.homeTableViewCellDelegate=self;
            cell.restUpdateDelegate = self;
            
            
            IMGUpdateCard* updateCardModel=[timeline objectForKey:typeId];
            cell.updateCard = updateCardModel;
            cell.fromCellIndexPath=indexPath;
            //[cell useCellFrameCacheWithIndexPath:indexPath tableView:tableView];
            [[Amplitude instance] logEvent:@"RC - View Promo Card" withEventProperties:@{@"Location":@"Timeline"}];
            return cell;
            
        }
        case 11:
        case 12:{
            
            static NSString* identifier=@"home_trending_restaurant";
            HomeTrendingRestTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
            if(cell==nil){
                cell=[[HomeTrendingRestTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            }
            cell.delegate=self;
            IMGRestaurant * restaurant=[timeline objectForKey:typeId];
            cell.homeTrendingRestTableViewCellDelegate = self;
            
            [cell setRestaurant:restaurant];
            cell.fromCellIndexPath=indexPath;
            //[cell useCellFrameCacheWithIndexPath:indexPath tableView:tableView];
            return cell;
        }
        case 14:
        {
            static NSString* identifier = @"cellReview";
            HomeReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            
            if (!cell)
            {
                cell = [[HomeReviewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            }
            IMGReviewCard *reviewCard = [timeline objectForKey:typeId];
            
            
            //reviewCard.reveiwCardIsReadMore = reveiwCardIsReadMore;
            
            
            
            cell.homeTableViewCellDelegate=self;
            cell.homeReviewDelegate = self;
            cell.fromCellIndexPath=indexPath;
            cell.reviewCard = reviewCard;
            
            [[Amplitude instance] logEvent:@"RC - View Review Card" withEventProperties:@{@"Location":@"Timeline"}];
            //[cell useCellFrameCacheWithIndexPath:indexPath tableView:tableView];
            return cell;
        }
        case 15:
        {
            static NSString* identifier = @"cellUploadPhoto";
            
            HomeUploadPhotoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            
            if (!cell)
            {
                cell = [[HomeUploadPhotoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            }
            
            cell.homeTableViewCellDelegate=self;
            cell.homeUploadPhotoTableViewCellDelegate = self;
            IMGUploadPhotoCard *uploadCard =  [timeline objectForKey:typeId];
            cell.uploadCard = uploadCard;
            cell.fromCellIndexPath=indexPath;
            [[Amplitude instance] logEvent:@"RC - View Photo Card" withEventProperties:@{@"Location":@"Timeline"}];
            //[cell useCellFrameCacheWithIndexPath:indexPath tableView:tableView];
            return cell;
            
        }
        case 16:
        {
            static NSString* identifier = @"cellMenu";
            HomeMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            
            if (!cell)
            {
                cell = [[HomeMenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            }
            cell.homeTableViewCellDelegate=self;
            cell.menuDelegate = self;
            
            IMGMenuCard *menuCard = [timeline objectForKey:typeId];
            [cell setMuneCard:menuCard];
            cell.fromCellIndexPath=indexPath;
            //[cell useCellFrameCacheWithIndexPath:indexPath tableView:tableView];
            return cell;
            
        }
            break;
        case 1001:{
            //Engage Dish
         
            static NSString* identifier = @"cellEngageDish";
            HomeUploadPhotoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell){
                cell = [[HomeUploadPhotoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            }
            [cell setHomeEngageTableViewCellType:HomeEngageTableViewCellDish];
            cell.homeTableViewCellDelegate=self;
            cell.homeUploadPhotoTableViewCellDelegate = self;
            IMGUploadPhotoCard *uploadCard =  [timeline objectForKey:typeId];
            cell.uploadCard = uploadCard;
            cell.fromCellIndexPath=indexPath;
            //[cell useCellFrameCacheWithIndexPath:indexPath tableView:tableView];
            return cell;
        }
            break;
        case 1002:{
            //Engage Journal
        
            static NSString* identifier = @"cellEngageJournal";
            HomeJournalTableViewCell* cell=[tableView dequeueReusableCellWithIdentifier:identifier];
            if(cell==nil){
                cell=[[HomeJournalTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            }
            [cell setHomeEngageTableViewCellType:HomeEngageTableViewCellJournal];
            cell.homeTableViewCellDelegate=self;
            cell.homeJournalTableViewCellDelegate=self;
            IMGMediaComment *journal=[timeline objectForKey:typeId];
            cell.journal = journal;
            //[cell useCellFrameCacheWithIndexPath:indexPath tableView:tableView];
          
            return cell;
        }
            break;
        case 1003:{
            
        }
            break;
        case 1004:{
            //Engage Restaurant
          
            static NSString* identifier = @"cellEngageTrendingRestaurant";
            HomeTrendingRestTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:identifier];
            if(cell==nil){
                cell=[[HomeTrendingRestTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            }
            [cell setHomeEngageTableViewCellType:HomeEngageTableViewCellRestaurant];
            cell.delegate=self;
            IMGRestaurant * restaurant=[timeline objectForKey:typeId];
            cell.homeTrendingRestTableViewCellDelegate = self;
            
            [cell setRestaurant:restaurant];
            cell.fromCellIndexPath=indexPath;
            //[cell useCellFrameCacheWithIndexPath:indexPath tableView:tableView];
            return cell;
        }
            break;
        case 1005:{
            //Engage write review
           
            static NSString* identifier = @"cellEngageWriteReview";
            HomeEngageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell){
                cell = [[HomeEngageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            }
            [cell setHomeEngageTableViewCellType:HomeEngageTableViewCellWriteReview];
            IMGEngageCard *engageCard = [timeline objectForKey:typeId];
            [cell setEngageCard:engageCard];
            cell.homeEngageTableViewCellDelegate = self;
            //[cell useCellFrameCacheWithIndexPath:indexPath tableView:tableView];
            return cell;
        }
            break;
        case 1006:{
            //Engage upload photo
         
            static NSString* identifier = @"cellEngageUploadPhoto";
            HomeEngageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell){
                cell = [[HomeEngageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
            }
            [cell setHomeEngageTableViewCellType:HomeEngageTableViewCellUploadPhoto];
            IMGEngageCard *engageCard = [timeline objectForKey:typeId];
            [cell setEngageCard:engageCard];
            cell.homeEngageTableViewCellDelegate = self;
            //[cell useCellFrameCacheWithIndexPath:indexPath tableView:tableView];
            return cell;
        }
            break;
            
        default:
        {
            
        }
            break;
    }
    return nil;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *timeline=[timeLineArray objectAtIndex:indexPath.row];
    NSNumber *typeId = [[timeline allKeys] firstObject];
    
    
    switch ([typeId intValue])
    {
        case 2:
        {
            IMGMediaComment *journal = (IMGMediaComment*)[timeline objectForKey:typeId];
            return [tableView cellHeightForIndexPath:indexPath model:journal keyPath:@"journal" cellClass:[HomeJournalTableViewCell class] contentViewWidth:DeviceWidth];
        }
        case 4:
        {
            IMGDiningGuide *diningGuide= (IMGDiningGuide *)[timeline objectForKey:typeId];
            return [tableView cellHeightForIndexPath:indexPath model:diningGuide keyPath:@"diningguide" cellClass:[HomeDiningGuideTableViewCell class] contentViewWidth:DeviceWidth]; // dining guide
        }
        case 8:{
            IMGOfferCard *offerModel = [timeline objectForKey:typeId];
            return [tableView cellHeightForIndexPath:indexPath model:offerModel keyPath:@"offerModel" cellClass:[HomeEngageOfferTableViewCell class] contentViewWidth:DeviceWidth];
        }
        case 10:
        {
            IMGUpdateCard *updateModel = [timeline objectForKey:typeId];
            return [tableView cellHeightForIndexPath:indexPath model:updateModel keyPath:@"updateCard" cellClass:[HomeRestUpdateTableViewCell class] contentViewWidth:DeviceWidth];
        }
            break;
        case 11:
        case 12:
        {
            IMGRestaurant *restaurant =(IMGRestaurant *) [timeline objectForKey:typeId];
            return [tableView cellHeightForIndexPath:indexPath model:restaurant keyPath:@"restaurant" cellClass:[HomeTrendingRestTableViewCell class] contentViewWidth:DeviceWidth];
        }
        case 14:
        {
            IMGReviewCard *reviewCard = [timeline objectForKey:typeId];

            return [tableView cellHeightForIndexPath:indexPath model:reviewCard keyPath:@"reviewCard" cellClass:[HomeReviewTableViewCell class] contentViewWidth:DeviceWidth];

        }
            break;

        case 15:
        {
            IMGUploadPhotoCard *uploadCard = [timeline objectForKey:typeId];

            return [tableView cellHeightForIndexPath:indexPath model:uploadCard keyPath:@"uploadCard" cellClass:[HomeUploadPhotoTableViewCell class] contentViewWidth:DeviceWidth];
        }
        case 16:
        {
            IMGMenuCard *menuCard = [timeline objectForKey:typeId];
            return [HomeMenuTableViewCell calculatedHeight:menuCard]+cellMargin;
        }
        case 1001:
        {
            IMGUploadPhotoCard *uploadCard = [timeline objectForKey:typeId];

            return [tableView cellHeightForIndexPath:indexPath model:uploadCard keyPath:@"uploadCard" cellClass:[HomeUploadPhotoTableViewCell class] contentViewWidth:DeviceWidth];
        }
        case 1002:
        {
            IMGMediaComment *journal = (IMGMediaComment*)[timeline objectForKey:typeId];
            return [tableView cellHeightForIndexPath:indexPath model:journal keyPath:@"journal" cellClass:[HomeJournalTableViewCell class] contentViewWidth:DeviceWidth];
        }
        case 1003:
        {
            //                return [HomeEngageOfferTableViewCell calculatedHeight];
        }
        case 1004:
        {
            IMGRestaurant *restaurant =(IMGRestaurant *) [timeline objectForKey:typeId];
            return [tableView cellHeightForIndexPath:indexPath model:restaurant keyPath:@"restaurant" cellClass:[HomeTrendingRestTableViewCell class] contentViewWidth:DeviceWidth];
        }
        case 1005:
        {
            IMGEngageCard *engageCard = [timeline objectForKey:typeId];
            return [HomeEngageTableViewCell calculatedHeight:engageCard];
        }
        case 1006:
        {
            IMGEngageCard *engageCard = [timeline objectForKey:typeId];
            return [HomeEngageTableViewCell calculatedHeight:engageCard];
        }
        default:
            return 1000;
            break;
    }
}

#pragma mark - cell delegate
- (void)goToCardDetailPageWithType:(int)type andData:(NSDictionary *)dataDic andCellIndexPath:(NSIndexPath*)cellIndexPath
{
    switch (type)
    {
        case 2:
        {
            IMGMediaComment *journal = [dataDic objectForKey:@"journal"];
            V2_CommentListViewController *commentListVC = [[V2_CommentListViewController alloc] initWithTypeID:type andModel:journal];
            [commentListVC setRefreshComment:^{
                int commentCount =  [journal.commentCount intValue];
                commentCount ++;
                journal.commentCount = [NSNumber numberWithInt:commentCount];
                
                [self refreshCell:cellIndexPath];
            }];
            
            [self.navigationController pushViewController:commentListVC animated:YES];
            
        }
            break;
        case 8:
        {
            IMGOfferCard *offerModel = [dataDic objectForKey:@"offerModel"];
            V2_CommentListViewController *commentListVC = [[V2_CommentListViewController alloc] initWithTypeID:type andModel:offerModel];
            [commentListVC setRefreshComment:^{
                int commentCount = [offerModel.commentCount intValue];
                commentCount ++;
                offerModel.commentCount = [NSNumber numberWithInt:commentCount];
                
                [self refreshCell:cellIndexPath];
            }];
            
            [self.navigationController pushViewController:commentListVC animated:YES];
        }
            break;
        case 10:
        {
            NSArray *eventArr = [dataDic objectForKey:@"dataArr"];
            if (eventArr.count>1) {
                IMGRestaurant *restaurant = [dataDic objectForKey:@"restaurant"];
                V2_CommentListViewController *commentListVC = [[V2_CommentListViewController alloc] initWithTypeID:type andModel:restaurant];
                [commentListVC setRefreshComment:^{
                    int commentCount = [restaurant.commentCardCount intValue];
                    commentCount ++;
                    restaurant.commentCardCount = [NSNumber numberWithInt:commentCount];
                    
                    [self refreshCell:cellIndexPath];
                }];
                
                
                [self.navigationController pushViewController:commentListVC animated:YES];
            }else if (eventArr.count==1){
                IMGRestaurantEvent *restaurantEvent = [eventArr firstObject];
                V2_CommentListViewController *commentListVC = [[V2_CommentListViewController alloc] initWithTypeID:type andModel:restaurantEvent];
                [commentListVC setRefreshComment:^{
                    int commentCount = [restaurantEvent.commentCount intValue];
                    commentCount ++;
                    restaurantEvent.commentCount = [NSNumber numberWithInt:commentCount];
                    
                    [self refreshCell:cellIndexPath];
                }];
                
                [self.navigationController pushViewController:commentListVC animated:YES];
            }
            
        }
            break;
        case 14:
        {
            //            IMGRestaurant *restaurant = [dataDic objectForKey:@"restaurant"];
            //            NSArray *dishArr = [dataDic objectForKey:@"dataArr"];
            IMGReview *review = [dataDic objectForKey:@"review"];
            V2_CommentListViewController *commentListVC = [[V2_CommentListViewController alloc] initWithTypeID:type andModel:review];
            [commentListVC setRefreshComment:^{
                int commentCount = [review.commentCount intValue];
                commentCount ++;
                review.commentCount = [NSNumber numberWithInt:commentCount];
                
                [self refreshCell:cellIndexPath];
            }];
            
            [self.navigationController pushViewController:commentListVC animated:YES];
            
        }
            break;
        case 15:
        {
            IMGRestaurant *restaurant = [dataDic objectForKey:@"restaurant"];
            NSArray *dishArr = [dataDic objectForKey:@"dataArr"];
            IMGUser *user = [dataDic objectForKey:@"user"];
            if (dishArr.count>1) {
                V2_CommentListViewController *commentListVC = [[V2_CommentListViewController alloc] initWithTypeID:type andModel:user];
                [commentListVC setRefreshComment:^{
                    int commentCount = [restaurant.commentCardCount intValue];
                    commentCount ++;
                    restaurant.commentCardCount = [NSNumber numberWithInt:commentCount];
                    
                    [self refreshCell:cellIndexPath];
                }];
                
                [self.navigationController pushViewController:commentListVC animated:YES];
            }else{
                IMGDish *dish = [dishArr firstObject];
                V2_CommentListViewController *commentListVC = [[V2_CommentListViewController alloc] initWithTypeID:type andModel:dish];
                [commentListVC setRefreshComment:^{
                    int commentCount = [dish.commentCount intValue];
                    commentCount ++;
                    dish.commentCount = [NSNumber numberWithInt:commentCount];
                    
                    [self refreshCell:cellIndexPath];
                }];
                
                
                [self.navigationController pushViewController:commentListVC animated:YES];
            }
            
            
        }
            break;
            
        default:
            break;
    }
    
}
- (void)refreshCell:(NSIndexPath *)indexPath{
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            if ([[timelineTableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                return;
            }
            if(timeLineArray.count == 0) {
                return;
            }
            [timelineTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            
        }
        
        @catch ( NSException *e ) {
            return;
        }
    });
}

-(void)mapBtnClick:(IMGRestaurant*)restaurant{
    MapViewController *mapViewController_=[[MapViewController alloc] initWithRestaurant:restaurant];
    mapViewController_.amplitudeType = @"Homepage";
    mapViewController_.fromHomeCard = YES;
    [self.navigationController pushViewController:mapViewController_ animated:YES];
}

-(void)viewFullImage:(IMGEntity *)_dish{
    IMGType imgType = [EntityKind typeKindFromEntity:_dish];
    if (imgType == TypeForIMGDish) {
        IMGDish *dish = (IMGDish *)_dish;
        NSArray *photoArray = [[NSArray alloc]initWithObjects:dish, nil];
        AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:photoArray andPhotoTag:0 andRestaurant:nil andFromDetail:NO];
        albumViewController.amplitudeType = @"Homepage";
        albumViewController.title = dish.userName;
        albumViewController.showPage=NO;
        [self.navigationController pushViewController:albumViewController animated:YES];
        
    }else if(imgType == TypeForIMGRestaurantEvent){
        IMGRestaurantEvent *dish = (IMGRestaurantEvent *)_dish;
        NSArray *photoArray = [[NSArray alloc]initWithObjects:dish, nil];
        AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:photoArray andPhotoTag:0 andRestaurant:nil andFromDetail:NO];
        albumViewController.amplitudeType = @"Homepage";
        albumViewController.title = dish.title;
        albumViewController.showPage=NO;
        [self.navigationController pushViewController:albumViewController animated:YES];
    }
}
-(void)viewFullImage:(IMGEntity *)dish andRestaurant:(IMGRestaurant*)restaurant andReview:(IMGReview *)review{
    IMGType imgType = [EntityKind typeKindFromEntity:dish];
    
    if (imgType == TypeForIMGDish) {
        IMGDish *dish_ = (IMGDish *)dish;
        NSArray *photoArray = [[NSArray alloc]initWithObjects:dish, nil];
        AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:photoArray andPhotoTag:0 andRestaurant:restaurant andFromDetail:NO];
        albumViewController.title = dish_.userName;
        albumViewController.showPage=NO;
        albumViewController.review = review;
        albumViewController.albumReloadCellDelegate = self;
        albumViewController.amplitudeType = @"Homepage";
        
        [self.navigationController pushViewController:albumViewController animated:YES];
        
        
        
        if (review ==nil) {
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:dish_.userId forKey:@"UploaderUser_ID"];
            [eventProperties setValue:@"Homepage" forKey:@"Location"];
            [[Amplitude instance] logEvent:@"CL - User Photo Card" withEventProperties:eventProperties];
        }else{
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:review.reviewId forKey:@"review_ID"];
            [eventProperties setValue:review.userId forKey:@"reviewUser_ID"];
            [eventProperties setValue:@"Homepage" forKey:@"Location"];
            [[Amplitude instance] logEvent:@"CL - User Review Card" withEventProperties:eventProperties];
        }
        
    }else if(imgType == TypeForIMGRestaurantEvent){
        IMGRestaurantEvent *dish_ = (IMGRestaurantEvent *)dish;
        NSArray *photoArray = [[NSArray alloc]initWithObjects:dish_, nil];
        AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:photoArray andPhotoTag:0 andRestaurant:nil andFromDetail:NO];
        albumViewController.title = dish_.title;
        albumViewController.showPage=NO;
        albumViewController.albumReloadCellDelegate = self;
        albumViewController.amplitudeType = @"Homepage";
        [self.navigationController pushViewController:albumViewController animated:YES];
        
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:dish_.eventId forKey:@"RestaurantEvent_ID"];
        [eventProperties setValue:dish_.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:@"Homepage" forKey:@"Location"];
        [[Amplitude instance] logEvent:@"CL - Restaurant Event Photo Card" withEventProperties:eventProperties];
        
    }else if(imgType == TypeForIMGMenu){
        NSArray *dishArr = [[NSArray alloc] initWithObjects:dish, nil];
        MenuPhotoViewColltroller *menuPhotoViewColltroller = [[MenuPhotoViewColltroller alloc]initWithPhotosArray:[dishArr mutableCopy] andPhotoTag:0 andRestaurant:restaurant andFromDetail:NO];
        menuPhotoViewColltroller.menuPhotoCount = [NSNumber numberWithLong:dishArr.count];
        menuPhotoViewColltroller.amplitudeType = @"Homepage";
        [self.navigationController pushViewController:menuPhotoViewColltroller animated:YES];
        
    }
}

- (void)goToDiningGuideList
{
    
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:@"Dining Guide card on homepage" forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL - See All Dining Guides" withEventProperties:eventProperties];
    
    V2_DiningGuideListViewController *dglvc = [[V2_DiningGuideListViewController alloc] init];
    [self.navigationController pushViewController:dglvc animated:YES];
}
-(void)goToDiningGuide:(IMGDiningGuide*)dingingGudie{
//    [IMGAmplitudeUtil trackGuideWithName:@"RC - View Dining Guide page" andDiningGuideId:dingingGudie.diningGuideId andDiningGuideName:dingingGudie.pageName andOrigin:@"Timeline"];
    DiningGuideRestaurantsViewController *diningResList = [[DiningGuideRestaurantsViewController alloc] initWithDiningGuide:dingingGudie];
    diningResList.isfromHome=YES;
    [self.navigationController pushViewController:diningResList animated:YES];
}


- (void)writeReviewWithRestaurant:(IMGRestaurant *)restaurant cellIndexPath:(NSIndexPath *)cellIndexPath
{
    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        LoginViewController *loginVC=[[LoginViewController alloc]initWithFromWhere:[NSNumber numberWithInt:1]];
        UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
        [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
        return;
    }
    
    ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc]initWithRestaurant:restaurant andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
    publishReviewVC.isEdit = YES;
    //    publishReviewVC.isEditReview = YES;
    //    publishReviewVC.reviewPublishViewControllerDelegate = self;
    publishReviewVC.fromCellIndexPath = cellIndexPath;
    publishReviewVC.amplitudeType =@"HomePage";
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:publishReviewVC] animated:YES completion:^{

    }];
    
}
- (void)uploadPhotoButtonClickWithRestaurant:(IMGRestaurant *)restaurant andReview:(IMGReview *)review
{
    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        LoginViewController *loginVC=[[LoginViewController alloc]initWithFromWhere:[NSNumber numberWithInt:1]];
        UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
        [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
        return;
    }
    IMGUserReview *userReview = [[IMGUserReview alloc] init];
    userReview.userId = [NSNumber numberWithInt:[review.userId intValue]];
    userReview.userFullName = review.fullName;
    userReview.reviewId = review.reviewId;
    userReview.reviewSummarize = review.summarize;
    userReview.reviewTitle = review.title;
    userReview.restaurantId = restaurant.restaurantId;
    userReview.restaurantTitle = restaurant.title;
    ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc]initWithRestaurant:restaurant andOverallRating:[review.score floatValue]/2 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
    publishReviewVC.userReview = userReview;
    publishReviewVC.isUpdateReview = YES;
    publishReviewVC.isEdit = YES;
    publishReviewVC.amplitudeType = @"HomePage";
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:publishReviewVC] animated:YES completion:^{
        
    }];
    
    
    
}
-(void)saveBtnClick:(BOOL)isSaved andRestaurant:(IMGRestaurant *)restaurant withSaveBlock:(void (^)(BOOL))saveBlock{
    
    
    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        LoginViewController *loginVC=[[LoginViewController alloc]initWithFromWhere:[NSNumber numberWithInt:1]];
        UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
        [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
        return;
    }
    AddToListController *addToList=[[AddToListController alloc] initWithRestaurant:restaurant];
    addToList.amplitudeType = @"Restaurant update card on homepage";
    void(^callback)(BOOL saved)=^(BOOL saved){
        //        __weak HomeViewController *homeSelf=self;
        [self reloadCell];
        //        [ListHandler getRestaurantSaved:restaurant.restaurantId withUser:[IMGUser currentUser] andBlock:^(BOOL status,NSInteger savedCount){
        //            [detailSelf changeSaveBtn:status];
        //        }];
    };
    addToList.addToListBlock=callback;
    addToList.removeFromListBlock=callback;
    addToList.toListBlock=saveBlock;
    [self.navigationController pushViewController:addToList animated:YES];
    
}

- (void)saveBtnClick:(BOOL)isSaved andRestaurant:(IMGRestaurant *)restaurant
{
    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        LoginViewController *loginVC=[[LoginViewController alloc]initWithFromWhere:[NSNumber numberWithInt:1]];
        UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
        [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
        return;
    }
    AddToListController *addToList=[[AddToListController alloc] initWithRestaurant:restaurant];
    addToList.amplitudeType = @"Restaurant update card on homepage";
    void(^callback)(BOOL saved)=^(BOOL saved){
        //        __weak HomeViewController *homeSelf=self;
        [self reloadCell];

    };
    addToList.addToListBlock=callback;
    addToList.removeFromListBlock=callback;
    [self.navigationController pushViewController:addToList animated:YES];
    
}
- (void)homeTrendingSaveBtnClick:(BOOL)isSaved andRestaurant:(IMGRestaurant *)restaurant
{
    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        LoginViewController *loginVC=[[LoginViewController alloc]initWithFromWhere:[NSNumber numberWithInt:1]];
        UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
        [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
        return;
    }
    AddToListController *addToList=[[AddToListController alloc] initWithRestaurant:restaurant];
    addToList.amplitudeType = @"Trending Restaurant card on homepage";
    void(^callback)(BOOL saved)=^(BOOL saved){
        //        __weak HomeViewController *homeSelf=self;
        [self reloadCell];
    
    };
    addToList.addToListBlock=callback;
    addToList.removeFromListBlock=callback;
    [self.navigationController pushViewController:addToList animated:YES];
    
}

- (void)homeRestUpdateTableViewCellImageClick:(IMGRestaurant *)restaurant andDishArr:(NSArray *)dishArr andIndex:(int)index andFromCellIndexPath:(NSIndexPath*)fromCellIndexPath{
    
    AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:dishArr andPhotoTag:index-1 andRestaurant:restaurant andFromDetail:NO];
    albumViewController.title = restaurant.title;
    
    albumViewController.albumReloadCellDelegate = self;
    
    albumViewController.showPage=NO;
    albumViewController.amplitudeType = @"Homepage";
    [self.navigationController pushViewController:albumViewController animated:YES];
    
    
}

- (void)homeUploadPhotoTableViewCellImageClick:(IMGRestaurant *)restaurant andUser:(IMGUser *)user andDishArr:(NSArray *)dishArr andIndex:(int)index andFromCellIndexPath:(NSIndexPath *)fromCellIndexPath
{
    
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:user.userId forKey:@"UploaderUser_ID"];
    [eventProperties setValue:@"Homepage" forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL - User Photo Card" withEventProperties:eventProperties];
    
    
    AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:dishArr andPhotoTag:index-1 andRestaurant:restaurant andFromDetail:NO];
    albumViewController.title = restaurant.title;
    
    albumViewController.albumReloadCellDelegate = self;
    
    albumViewController.showPage=NO;
    albumViewController.amplitudeType = @"Homepage";
    [self.navigationController pushViewController:albumViewController animated:YES];
}
- (void)HomeReviewTableViewCellImageClick:(IMGRestaurant *)restaurant andReview:(IMGReview *)review andDishArr:(NSArray *)dishArr andIndex:(int)index andFromCellIndexPath:(NSIndexPath*)fromCellIndexPath{
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:review.reviewId forKey:@"review_ID"];
    [eventProperties setValue:review.userId forKey:@"reviewUser_ID"];
    [eventProperties setValue:@"Homepage" forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL - User Review Card" withEventProperties:eventProperties];
    
    AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:dishArr andPhotoTag:index-1 andRestaurant:restaurant andFromDetail:NO];
    albumViewController.title = restaurant.title;
    
    albumViewController.albumReloadCellDelegate = self;
    
    albumViewController.showPage=NO;
    albumViewController.amplitudeType = @"Homepage";
    [self.navigationController pushViewController:albumViewController animated:YES];
    
}

- (void)gotoJournalDetailPage:(IMGMediaComment *)journal cellIndexPath:(NSIndexPath*)cellIndexPath
{
    JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
    jdvc.amplitudeType = @"Journal card on homepage";
    jdvc.journal = journal;
    jdvc.cdpDelegate=self;
    jdvc.fromCellIndexPath=cellIndexPath;
    [self.navigationController pushViewController:jdvc animated:YES];
    
}
- (void)gotoRestaurantWithRestaurantId:(NSNumber *)restaurantId
{
    
    
    DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:restaurantId];
    dvc.amplitudeType = @"Homepage";
    
    [self.navigationController pushViewController:dvc animated:YES];
}

-(void)homeCardTableViewCell:(UITableViewCell *)cell shareButtonTapped:(UIButton *)button entity:(id)entity{
    if([entity isKindOfClass:[IMGMediaComment class]]){
        IMGMediaComment *_journal=(IMGMediaComment*)entity;
        NSString *journal_link=_journal.link;
        //        if([[_journal.link lowercaseString] hasPrefix:@"https://"]){
        //            journal_link=[NSString stringWithFormat:@"http://%@",[_journal.link substringFromIndex:[@"https://" length]]];
        //        }
        NSString *shareContentString = [NSString stringWithFormat:@"%@ on Qraved. %@",_journal.journalTitle,journal_link];
        
        NSURL *url= [NSURL URLWithString:journal_link];
        JournalActivityItemProvider *provider=[[JournalActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
        provider.url=url;
        provider.title=_journal.journalTitle;
        provider.journalId=_journal.mediaCommentId;
        TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
        twitterActivityItemProvider.title = _journal.journalTitle;
        UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
        activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
        [activityCtl setValue:[NSString stringWithFormat:L(@"Must Go! %@"),_journal.journalTitle] forKey:@"subject"];
        activityCtl.completionWithItemsHandler = ^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
            if (completed) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:_journal.restaurantId forKey:@"restaurant_ID"];
                [eventProperties setValue:_journal.restaurantTitle forKey:@"ReataurantTitle"];
                [eventProperties setValue:@"Journal card on homepage" forKey:@"Location"];
                [eventProperties setValue:provider.activityType forKey:@"Channel"];
                [eventProperties setValue:_journal.mediaCommentId forKey:@"Journal_ID"];
                
                [[Amplitude instance] logEvent:@"SH - Share Journal" withEventProperties:eventProperties];
                [[AppsFlyerTracker sharedTracker] trackEvent:@"SH - Share Journal" withValues:eventProperties];
                
            }else{
            }
        };
        if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
            // iOS8
            activityCtl.popoverPresentationController.sourceView =
            button;
        }
        
        
        [self presentViewController:activityCtl animated:YES completion:^{
            
        }];
    }else if([entity isKindOfClass:[IMGRestaurant class]]){
        IMGRestaurant *_restaurant = (IMGRestaurant*)entity;
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",QRAVED_WEB_SERVER_OLD,[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],_restaurant.seoKeyword]];
        NSString *shareContentString = [NSString stringWithFormat:@"%@ on Qraved. %@",_restaurant.title,url];
        RestaurantActivityItemProvider *provider=[[RestaurantActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
        provider.url=url;
        provider.title=shareContentString;
        provider.restaurantTitle=_restaurant.title;
        UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider] applicationActivities:nil];
        activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
        [activityCtl setValue:[NSString stringWithFormat:L(@"I Found %@ on Qraved!"),_restaurant.title] forKey:@"subject"];
        activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
            if (completed) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:_restaurant.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:_restaurant.title forKey:@"RestaurantTitle"];
                [eventProperties setValue:provider.activityType forKey:@"Channel"];
                [eventProperties setValue:@"Homepage" forKey:@"Location"];
                [[Amplitude instance] logEvent:@"SH - Share Restaurant" withEventProperties:eventProperties];
            }else{
            }
        };
        if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
            // iOS8
            activityCtl.popoverPresentationController.sourceView =
            button;
        }
        
        
        [self presentViewController:activityCtl animated:YES completion:^{
            
        }];
        
    }else if([entity isKindOfClass:[IMGOfferCard class]]){
        IMGOfferCard *_restaurantEvent = (IMGOfferCard*)entity;
        IMGRestaurant *_restaurant = [[IMGRestaurant alloc] init];
        _restaurant.restaurantId = _restaurantEvent.restaurantId;
        _restaurant.title = _restaurantEvent.restaurantTitle;
        _restaurant.seoKeyword = _restaurantEvent.restaurantSeo;
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/offer/%@",QRAVED_WEB_SERVER_OLD,_restaurantEvent.offerId]];
        NSString *shareContentString = [NSString stringWithFormat:@"%@ on Qraved. %@",_restaurant.title,url];
        RestaurantOfferActivityItemProvider *provider=[[RestaurantOfferActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
        provider.url=url;
        provider.title=shareContentString;
        provider.restaurantTitle=_restaurant.title;
        
        TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
        twitterActivityItemProvider.title = [NSString stringWithFormat:@"Check out this promo at %@ on Qraved",_restaurant.title];
        
        UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
        
        activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
        [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK %@'s promo on Qraved!"),_restaurant.title] forKey:@"subject"];
        activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
            if (completed) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:_restaurantEvent.restaurantTitle forKey:@"RestaurantTitle"];
                [eventProperties setValue:@"Restaurant trending card on homepage" forKey:@"Location"];
                [eventProperties setValue:_restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
                [eventProperties setValue:_restaurantEvent.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:provider.activityType forKey:@"Channel"];
                
                [[Amplitude instance] logEvent:@"SH - Share Restaurant Event" withEventProperties:eventProperties];
            }else{
            }
        };
        if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
            // iOS8
            activityCtl.popoverPresentationController.sourceView =
            button;
        }
        
        
        [self presentViewController:activityCtl animated:YES completion:^{
            
        }];
        
    }else if([entity isKindOfClass:[IMGRestaurantEvent class]]){
        IMGRestaurantEvent *_restaurantEvent = (IMGRestaurantEvent*)entity;
        IMGRestaurant *_restaurant = [[IMGRestaurant alloc] init];
        _restaurant.restaurantId = _restaurantEvent.restaurantId;
        _restaurant.title = _restaurantEvent.restaurantTitle;
        _restaurant.seoKeyword = _restaurantEvent.restaurantSeo;
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/promo/%@",QRAVED_WEB_SERVER_OLD,_restaurantEvent.eventId]];
        NSString *shareContentString = [NSString stringWithFormat:@"%@ on Qraved. %@",_restaurant.title,url];
        RestaurantEventActivityItemProvider *provider=[[RestaurantEventActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
        provider.url=url;
        provider.title=shareContentString;
        provider.restaurantTitle=_restaurant.title;
        
        TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
        twitterActivityItemProvider.title = [NSString stringWithFormat:@"Check out this event at %@ on Qraved",_restaurant.title];
        
        UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
        
        activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
        [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK %@'s event on Qraved!"),_restaurant.title] forKey:@"subject"];
        activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
            if (completed) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:_restaurantEvent.restaurantTitle forKey:@"RestaurantTitle"];
                [eventProperties setValue:@"Restaurant trending card on homepage" forKey:@"Location"];
                [eventProperties setValue:_restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
                [eventProperties setValue:_restaurantEvent.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:provider.activityType forKey:@"Channel"];
                
                [[Amplitude instance] logEvent:@"SH - Share Restaurant Event" withEventProperties:eventProperties];
            }else{
            }
        };
        if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
            // iOS8
            activityCtl.popoverPresentationController.sourceView =
            button;
        }
        
        
        [self presentViewController:activityCtl animated:YES completion:^{
            
        }];
        
    }else if([entity isKindOfClass:[IMGReview class]]){
        IMGReview *_review = (IMGReview*)entity;
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/review/%@",QRAVED_WEB_SERVER_OLD,_review.reviewId]];
        
        NSString *shareContentString = [NSString stringWithFormat:@"CHECK IT OUT NOW! A Restaurant Review by %@ on Qraved",_review.fullName];
        ReviewActivityItemProvider *provider=[[ReviewActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
        provider.username = _review.fullName;
        provider.restaurantTitle = _review.restaurantTitle;
        provider.url=url;
        provider.title=shareContentString;
        TwitterActivityItemProvider *twitterProvider=[[TwitterActivityItemProvider alloc] initWithPlaceholderItem:@""];
        twitterProvider.title=[NSString stringWithFormat:L(@"Check out %@ review at %@ on #Qraved!"),_review.fullName,_review.restaurantTitle];
        
        
        UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterProvider] applicationActivities:nil];
        activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
        [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK IT OUT NOW!A Restaurant Review By %@ on Qraved!"),_review.fullName] forKey:@"subject"];
        
        activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
            if (completed) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:_review.reviewId forKey:@"Review_ID"];
                [eventProperties setValue:_review.fullName forKey:@"ReviewerName"];
                [eventProperties setValue:@"Review card on homepage" forKey:@"Location"];
                [eventProperties setValue:_review.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:provider.activityType forKey:@"Channel"];
                
                [[Amplitude instance] logEvent:@"SH - Share Review" withEventProperties:eventProperties];
            }else{
            }
        };
        if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
            // iOS8
            activityCtl.popoverPresentationController.sourceView =
            button;
        }
        
        
        [self presentViewController:activityCtl animated:YES completion:^{
            
        }];
        
    }else if([entity isKindOfClass:[IMGDish class]]){
        IMGDish *_dish = (IMGDish*)entity;
        IMGRestaurant *_restaurant = [[IMGRestaurant alloc] init];
        _restaurant.restaurantId = _dish.restaurantId;
        _restaurant.title = _dish.restaurantTitle;
        _restaurant.seoKeyword = _dish.restaurantSeo;
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/photo/%@",QRAVED_WEB_SERVER_OLD,_dish.dishId]];
        NSString *shareContentString = [NSString stringWithFormat:@"Check out this photo at %@ on Qraved. %@",_restaurant.title,url];
        RestaurantActivityItemProvider *provider=[[RestaurantActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
        provider.url=url;
        provider.title=shareContentString;
        provider.restaurantTitle=_restaurant.title;
        provider.cuisineAreaPrice = _dish.userName;
        
        TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
        twitterActivityItemProvider.title = [NSString stringWithFormat:@"Check out this photo at %@ on Qraved",_restaurant.title];
        
        
        UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
        activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
        [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK IT OUT NOW! A Restaurant Photo by %@ on Qraved!"),_dish.userName] forKey:@"subject"];
        activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
            if (completed) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:_dish.dishId forKey:@"Photo_ID"];
                [eventProperties setValue:_dish.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:_dish.title forKey:@"DishName"];
                [eventProperties setValue:@"Photo card on homepage" forKey:@"Location" ];
                [eventProperties setValue:provider.activityType forKey:@"Channel"];
                
                [[Amplitude instance] logEvent:@"SH - Share Photo" withEventProperties:eventProperties];
            }else{
            }
        };
        if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
            // iOS8
            activityCtl.popoverPresentationController.sourceView =
            button;
        }
        
        [self presentViewController:activityCtl animated:YES completion:^{
            
        }];
    }
}
- (void)homeCardTableViewCell:(UITableViewCell*)cell shareMutiPhotosCardButtonTapped:(UIButton*)button entity:(id)entity{
    if ([entity isKindOfClass:[IMGRestaurant class]]) {
        IMGRestaurant *_restaurant = (IMGRestaurant*)entity;
        IMGRestaurantEvent *_event = (IMGRestaurantEvent*)entity;
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/multipromo/%@",QRAVED_WEB_SERVER_OLD,_restaurant.homeTimeLineId]];
        NSString *shareContentString = [NSString stringWithFormat:@"%@ on Qraved. %@",_restaurant.title,url];
        RestaurantMutiPromoActivityItemProvider *provider=[[RestaurantMutiPromoActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
        provider.url=url;
        provider.title=shareContentString;
        provider.restaurantTitle=_restaurant.title;
        
        TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
        twitterActivityItemProvider.title = [NSString stringWithFormat:@"Check out this event at %@ on Qraved",_restaurant.title];
        
        UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
        
        activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
        [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK %@'s event on Qraved!"),_restaurant.title] forKey:@"subject"];
        activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
            if (completed) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:_restaurant.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:_restaurant.title forKey:@"RestaurantTitle"];
                [eventProperties setValue:@"Restaurant event card on homepage" forKey:@"Location"];
                [eventProperties setValue:provider.activityType forKey:@"Channel"];
                
                [eventProperties setValue:_event.eventId forKey:@"RestaurantEvent_ID"];
                [[Amplitude instance] logEvent:@"SH - Share Restaurant Event" withEventProperties:eventProperties];
            }else{
            }
        };
        if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
            // iOS8
            activityCtl.popoverPresentationController.sourceView =
            button;
        }
        
        [self presentViewController:activityCtl animated:YES completion:^{
            
        }];
        
        
    }else if([entity isKindOfClass:[IMGDish class]]){
        IMGDish *_dish = (IMGDish*)entity;
        IMGRestaurant *_restaurant = [[IMGRestaurant alloc] init];
        _restaurant.restaurantId = _dish.restaurantId;
        _restaurant.title = _dish.restaurantTitle;
        _restaurant.seoKeyword = _dish.restaurantSeo;
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/multiphoto/%@",QRAVED_WEB_SERVER_OLD,_dish.moderateReviewId]];
        NSString *shareContentString = [NSString stringWithFormat:@"Check out %@ photos at %@ on Qraved. %@",_dish.userName,_restaurant.title,url];
        RestaurantMutiPhotosActivityItemProvider *provider=[[RestaurantMutiPhotosActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
        provider.url=url;
        provider.title=shareContentString;
        provider.restaurantTitle=_restaurant.title;
        provider.cuisineAreaPrice = _dish.userName;
        
        TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
        twitterActivityItemProvider.title = [NSString stringWithFormat:@"Check out this photos at %@ on Qraved",_restaurant.title];
        
        
        UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
        activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
        [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK IT OUT NOW! A Restaurant Photos by %@ on Qraved!"),_dish.userName] forKey:@"subject"];
        activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
            if (completed) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:_dish.dishId forKey:@"DishID"];
                [eventProperties setValue:_dish.title forKey:@"DishName"];
                [[Amplitude instance] logEvent:@"Share Content - Share Dish" withEventProperties:eventProperties];
            }else{
            }
        };
        if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
            // iOS8
            activityCtl.popoverPresentationController.sourceView =
            button;
        }
        
        [self presentViewController:activityCtl animated:YES completion:^{
            
        }];
    }
    
    
}

//- (void)homeCardTableViewCell:(UITableViewCell*)cell likeCommentShareView:(UIView*)likeCommentShareView commentInputView:(UIView *)view inputViewDidBeginEditing:(UITextView *)textView{
//    CGRect cellRect = [timelineTableView rectForRowAtIndexPath:[timelineTableView indexPathForCell:cell]];
//    
//    cellRect=CGRectMake(cellRect.origin.x, cellRect.origin.y+keyboardHeight+40, cellRect.size.width, cellRect.size.height);
//    [timelineTableView scrollToRowAtIndexPath:[timelineTableView indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
//    [timelineTableView scrollRectToVisible:cellRect animated:NO];
//    
//}
- (void)homeCardTableViewReloadCell:(UITableViewCell *)cell
{
    NSIndexPath *indexPath = [timelineTableView indexPathForCell:cell];
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            [timelineTableView reloadData];
            if ([[timelineTableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                return;
            }
            if(timeLineArray.count == 0) {
                return;
            }
    
            [timelineTableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:UITableViewRowAnimationNone];
        }
        
        @catch ( NSException *e ) {
            return;
        }
    });
    
}

- (void)homeCardTableViewReloadCell:(NSIndexPath *)indexPath entity:(id)entity{
    pressIndexPath = indexPath;
    IMGRestaurant *restaurant = (IMGRestaurant*)entity;
    pressRestaurantId = [restaurant.restaurantId integerValue];
    pressRestaurant = restaurant;
    
    if([entity isKindOfClass:[IMGRestaurant class]]){
        
        
        if ([restaurant.saved isEqual:@1]) {
            [[LoadingView sharedLoadingView] startLoading];
            [ListHandler removeRestaurantFromListWithRestaurantId:restaurant.restaurantId andListId:nil andBlock:^{
                
                [[LoadingView sharedLoadingView] stopLoading];
                restaurant.saved = @0;
                [self refreshCell:indexPath];
                
            } failure:^(NSString *exceptionMsg) {
                
            }];
            
        }else{
            SavedAddListView *savedAddListView = [[SavedAddListView alloc] init];
            savedAddListView.delegate = self;
            savedAddListView.frame = [AppDelegate ShareApp].window.bounds;
            [[AppDelegate ShareApp].window addSubview:savedAddListView];
        }
    }
}
#pragma mark - savedAddList Delegate
- (void)addProductToList:(NSNumber *)listId{
    NSLog(@"%@",listId);
    [[LoadingView sharedLoadingView] startLoading];
    NSArray *listArr = @[listId];
    NSArray *restaurantArr = @[[NSNumber numberWithInteger:pressRestaurantId]];
    [self addRestaurantToList:listArr andRestaurantId:restaurantArr];
}

- (void)addRestaurantToList:(NSArray *)listArray andRestaurantId:(NSArray *)restaurantArray{
    [ListHandler listAddRestaurantsWithUser:[IMGUser currentUser] andListIds:listArray andRestaurantIds:restaurantArray andBlock:^(BOOL succeed, id status) {
        [[LoadingView sharedLoadingView] stopLoading];
        pressRestaurant.saved = @1;
        [self refreshCell:pressIndexPath];
    }];
}
- (void)addNewList{
    [self createNewlist];
}
- (void)createNewlist{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Create New List" message:@"Enter your list name" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder  =@"List Name";
        
    }];
    
    UIAlertAction *submitAction = [UIAlertAction actionWithTitle:@"Submit" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSString *str = alertController.textFields.firstObject.text;
        if (str.length>0 && str.length <=100) {
            [[LoadingView sharedLoadingView] startLoading];
            [ListHandler addListWithUser:[IMGUser currentUser] andListName:str andCityId:@0 andBlock:^(NSDictionary *dataDic) {
                
                NSArray *listArr= @[[dataDic objectForKey:@"id"]];
                NSArray *restaurantArr = @[[NSNumber numberWithInteger:pressRestaurantId]];
                [self addRestaurantToList:listArr andRestaurantId:restaurantArr];
                [[LoadingView sharedLoadingView] stopLoading];
            } failure:^(NSString *exceptionMsg) {
                [[LoadingView sharedLoadingView] stopLoading];
            }];
            return ;
        }
        NSString *message;
        if (str.length==0) {
            message = @"List name cannot be empty";
        }else if (str.length > 100){
            message = L(@"Maximum 100 characters for list name.");
        }
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:L(@"OK") otherButtonTitles:nil, nil];
        [alertView show];
    }];
    //[submitAction setValue:[UIColor redColor] forKey:@"_titleTextColor"];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:submitAction];
    
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void)CDP:(id)cdp reloadTableCell:(id)cell{
    if([cdp isKindOfClass:[JournalDetailViewController class]]){
        JournalDetailViewController *journalDetailViewController=(JournalDetailViewController*)cdp;
        NSIndexPath *indexPath = journalDetailViewController.fromCellIndexPath;
        dispatch_async(dispatch_get_main_queue(), ^{
            @try {
                if ([[timelineTableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                    return;
                }
                if(timeLineArray.count == 0) {
                    return;
                }
                [timelineTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }
            
            @catch ( NSException *e ) {
                return;
            }
        });
    }else if([cdp isKindOfClass:[ReviewCardViewController class]]){
        ReviewCardViewController *reviewCardViewController=(ReviewCardViewController*)cdp;
        NSIndexPath *indexPath = reviewCardViewController.fromCellIndexPath;
        dispatch_async(dispatch_get_main_queue(), ^{
            @try {
                if ([[timelineTableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                    return;
                }
                if(timeLineArray.count == 0) {
                    return;
                }
                [timelineTableView beginUpdates];
                [timelineTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                [timelineTableView endUpdates];
            }
            
            @catch ( NSException *e ) {
                return;
            }
        });
    }else if([cdp isKindOfClass:[RestUpdateCardViewController class]]){
        RestUpdateCardViewController *restUpdateCardViewController=(RestUpdateCardViewController*)cdp;
        NSIndexPath *indexPath = restUpdateCardViewController.fromCellIndexPath;
        dispatch_async(dispatch_get_main_queue(), ^{
            @try {
                if ([[timelineTableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                    return;
                }
                if(timeLineArray.count == 0) {
                    return;
                }
                [timelineTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }
            
            @catch ( NSException *e ) {
                return;
            }
        });
        
    }else if([cdp isKindOfClass:[UploadPhotoCardViewController class]]){
        UploadPhotoCardViewController *uploadPhotoCardViewController=(UploadPhotoCardViewController*)cdp;
        NSIndexPath *indexPath = uploadPhotoCardViewController.fromCellIndexPath;
        dispatch_async(dispatch_get_main_queue(), ^{
            @try {
                if ([[timelineTableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                    return;
                }
                if(timeLineArray.count == 0) {
                    return;
                }
                [timelineTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }
            
            @catch ( NSException *e ) {
                return;
            }
        });
    }else if([cdp isKindOfClass:[JournalCardViewController class]]){
        JournalCardViewController *journalCardViewController=(JournalCardViewController*)cdp;
        NSIndexPath *indexPath = journalCardViewController.fromCellIndexPath;
        dispatch_async(dispatch_get_main_queue(), ^{
            @try {
                if ([[timelineTableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                    return;
                }
                if(timeLineArray.count == 0) {
                    return;
                }
                [timelineTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }
            
            @catch ( NSException *e ) {
                return;
            }
        });
    }else if ([cdp isKindOfClass:[ImageListViewController class]]){
        ImageListViewController *imageListViewController=(ImageListViewController*)cdp;
        NSIndexPath *indexPath = imageListViewController.fromCellIndexPath;
        dispatch_async(dispatch_get_main_queue(), ^{
            @try {
                if ([[timelineTableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                    return;
                }
                if(timeLineArray.count == 0) {
                    return;
                }
                [timelineTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }
            
            @catch ( NSException *e ) {
                return;
            }
        });
        
        
    }else if ([cdp isKindOfClass:[OfferCardViewController class]]){
        OfferCardViewController *imageListViewController=(OfferCardViewController*)cdp;
        NSIndexPath *indexPath = imageListViewController.fromCellIndexPath;
        dispatch_async(dispatch_get_main_queue(), ^{
            @try {
                if ([[timelineTableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                    return;
                }
                if(timeLineArray.count == 0) {
                    return;
                }
                [timelineTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }
            
            @catch ( NSException *e ) {
                return;
            }
        });
        
        
    }
    
}

- (void)writeReviewReloadUploadPhotoCell:(id)reviewPublishViewController
{
    ReviewPublishViewController *rpvc = (ReviewPublishViewController *)reviewPublishViewController;
    NSDictionary *timeline=[timeLineArray objectAtIndex:rpvc.fromCellIndexPath.row];
    NSNumber *typeId = [[timeline allKeys] firstObject];
    IMGUploadPhotoCard *uploadPhotoCard = [timeline objectForKey:typeId];
    NSMutableArray *dishArrM = uploadPhotoCard.dishArrM;
    for (IMGDish *dish in dishArrM)
    {
        dish.showWriteReview = NO;
    }
    
    NSIndexPath *indexPath = rpvc.fromCellIndexPath;
    if(timeLineArray.count == 0) {
        return;
    }
    [timelineTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    
}
- (void)homeCardTableViewUserNameOrImageTapped:(id)sender{
    if ([sender isKindOfClass:[IMGUser class]])
    {
        IMGUser *user = (IMGUser *)sender;
        OtherProfileViewController *opvc = [[OtherProfileViewController alloc] init];
        opvc.amplitude = @"Homepage";
        IMGUser *currentUser = [IMGUser currentUser];
        if ([currentUser.userId intValue] == [user.userId intValue])
        {
            opvc.isOtherUser = NO;
        }else
        {
            opvc.isOtherUser = YES;
        }
        opvc.otherUser = user;
        [self.navigationController pushViewController:opvc animated:YES];
    }
    else if ([sender isKindOfClass:[IMGRestaurant class]])
    {
        IMGRestaurant *restaurant = (IMGRestaurant *)sender;
        DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:restaurant.restaurantId];
        dvc.amplitudeType = @"Homepage";
        [self.navigationController pushViewController:dvc animated:YES];
    }
}

- (void)HomeReviewTableCell:(UITableViewCell *)tableViewCell readMoreTapped:(id)review indexPath:(NSIndexPath *)path{
    
    IMGReviewCard *thisReview = (IMGReviewCard *)review;
    
    //[reveiwCardIsReadMore setObject:isReadMore?@1:@0 forKey:thisReview.targetId];
    thisReview.isReadMore = YES;
    
    //    NSIndexPath *indexPath = [_tableView indexPathForCell:tableViewCell];
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            if ([[timelineTableView indexPathsForVisibleRows] indexOfObject:path] == NSNotFound) {
                return;
            }
            if(timeLineArray.count == 0) {
                return;
            }
            [timelineTableView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
        }
        
        @catch ( NSException *e ) {
            return;
        }
    });
    
    
}

-(void)homeCardTableViewCell:(UITableViewCell *)cell likeButtonTapped:(UIButton *)button entity:(id)entity{
    NSIndexPath *indexPath = [timelineTableView indexPathForCell:cell];
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            if ([[timelineTableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                return;
            }
            if(timeLineArray.count == 0) {
                return;
            }
            [timelineTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        
        @catch ( NSException *e ) {
            return;
        }
    });
}
- (void)gotoOtherProfileWithUser:(IMGUser *)user
{
    IMGUser *currentUser = [IMGUser currentUser];
    
    OtherProfileViewController *opvc = [[OtherProfileViewController alloc] init];
    opvc.amplitude = @"Homepage";
    if ([currentUser.userId intValue]==[user.userId intValue])
    {
        opvc.isOtherUser = NO;
    }
    else
    {
        opvc.isOtherUser = YES;
    }
    opvc.otherUser = user;
    [self.navigationController pushViewController:opvc animated:YES];
}
-(void)reloadCell{
    [timelineTableView reloadData];
}
- (void)homeMenuSaveBtnClick:(BOOL)isSaved andRestaurant:(IMGRestaurant *)restaurant
{
    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        LoginViewController *loginVC=[[LoginViewController alloc]initWithFromWhere:[NSNumber numberWithInt:1]];
        UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
        [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
        return;
    }
    AddToListController *addToList=[[AddToListController alloc] initWithRestaurant:restaurant];
    addToList.amplitudeType = @"Menu card on homepage";
    void(^callback)(BOOL saved)=^(BOOL saved){
        //        __weak HomeViewController *homeSelf=self;
        [self reloadCell];
        //        [ListHandler getRestaurantSaved:restaurant.restaurantId withUser:[IMGUser currentUser] andBlock:^(BOOL status,NSInteger savedCount){
        //            [detailSelf changeSaveBtn:status];
        //        }];
    };
    addToList.addToListBlock=callback;
    addToList.removeFromListBlock=callback;
    [self.navigationController pushViewController:addToList animated:YES];
    
}
- (void)homeMenuTableViewCellImageClick:(IMGRestaurant *)restaurant andDishArr:(NSArray *)dishArr andIndex:(int)index
{
    
    
    MenuPhotoViewColltroller *menuPhotoViewColltroller = [[MenuPhotoViewColltroller alloc]initWithPhotosArray:[dishArr mutableCopy] andPhotoTag:index andRestaurant:restaurant andFromDetail:NO];
    menuPhotoViewColltroller.menuPhotoCount = [NSNumber numberWithLong:dishArr.count];
    menuPhotoViewColltroller.amplitudeType = @"Menu photo card detail page";
    [self.navigationController pushViewController:menuPhotoViewColltroller animated:YES];
    
}
- (void)homeMenuShareBtnClick:(IMGMenu *)menu withbtn:(UIView*)btn
{
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/menu/%@",QRAVED_WEB_SERVER_OLD,menu.menuId]];
    NSString *shareContentString = [NSString stringWithFormat:@"%@ on Qraved. %@",menu.restaurantTitle,url];
    MenuPhotoActivityItemProvider *provider = [[MenuPhotoActivityItemProvider alloc]initWithPlaceholderItem:shareContentString];
    
    provider.url=url;
    provider.title=shareContentString;
    provider.restaurantTitle= menu.restaurantTitle;
    //    provider.districtName = self.currentRestaurant.districtName;
    
    TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
    twitterActivityItemProvider.title = [NSString stringWithFormat:@"Check out this menu at %@ on Qraved",menu.restaurantTitle];
    
    UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
    
    activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
    [activityCtl setValue:[NSString stringWithFormat:L(@"Let’s find out great menu on Qraved"),menu.restaurantTitle] forKey:@"subject"];
    activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
        
    };
    if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
        // iOS8
        activityCtl.popoverPresentationController.sourceView =
        btn;
    }
    
    [self presentViewController:activityCtl animated:YES completion:^{
        
    }];
    
}

-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    
    NSMutableArray *photosArrM = [[NSMutableArray alloc] init];
    
    for (int i=0; i<assets.count; i++) {
        ALAsset *asset=assets[i];
        UIImageView *imgview=[[UIImageView alloc] init];
        imgview.contentMode=UIViewContentModeScaleAspectFill;
        imgview.clipsToBounds=YES;
        UIImage *tempImg=[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage];
        // [imgview setImage:tempImg];
        IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
        photo.image = tempImg;
        photo.photoUrl =[NSString stringWithFormat:@"%@",[asset valueForProperty:ALAssetPropertyAssetURL]];
        [photosArrM addObject:photo];
    }
    
    [picker dismissViewControllerAnimated:NO completion:^{
        ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc]initWithRestaurant:picker.restaurant andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
        publishReviewVC.amplitudeType = @"Existing photo on device image gallery";
        publishReviewVC.isUploadPhoto = YES;
        publishReviewVC.photosArrM = [NSMutableArray arrayWithArray:photosArrM];
        publishReviewVC.isFromOnboard = NO;
        publishReviewVC.ifSuccessLoginClickPost = YES;
        [self.navigationController presentViewController:[[UINavigationController alloc] initWithRootViewController:publishReviewVC] animated:YES completion:nil];
        
        
    }];
}
-(void)engageBtnClick:(HomeEngageTableViewCellType)type engageCard:(IMGEngageCard *)engageCard{
    if (type == HomeEngageTableViewCellWriteReview) {
        [RestaurantHandler getRestaurantDetailByReviewIdFromServer:engageCard.reviewId andBlock:^(IMGRestaurant* tmpRestaurant,NSNumber *reviewScore){
            ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc] initWithRestaurant:tmpRestaurant andOverallRating:[reviewScore floatValue]/2.0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
            publishReviewVC.reviewId = engageCard.reviewId;
            publishReviewVC.isEditReview = YES;
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:publishReviewVC];
            [self.navigationController presentViewController:navigationController animated:YES completion:nil];
        }];
    }else if(type == HomeEngageTableViewCellUploadPhoto) {
        [RestaurantHandler getRestaurantDetailFromServer:engageCard.restaurantId andBlock:^(IMGRestaurant* tmpRestaurant){
            
            ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] initWithRestaurantTitle:tmpRestaurant.title andIsFromUploadPhoto:YES andIsUploadMenuPhoto:NO andCurrentRestaurantId:tmpRestaurant.restaurantId andRestaurant:tmpRestaurant];
            picker.maximumNumberOfSelection = 9;
            picker.assetsFilter = [ALAssetsFilter allPhotos];
            picker.showEmptyGroups=NO;
            picker.delegate = self;
            picker.isFromUploadPhoto = YES;
            picker.restaurant = tmpRestaurant;
            picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
                    NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
                    return duration >= 5;
                } else {
                    return YES;
                }
            }];
            [self.navigationController presentViewController:picker animated:YES completion:nil];
        }];
    }
}



-(void)reloadPhotosImageView:(NSArray *)dishArr{
}
-(void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error{
}

#pragma mark - book splash
-(void)addBookSplash{
    [HomeUtil getBookRatingSplashWithBlock:^(NSMutableArray *splashArray) {
        NSMutableArray *tempSplashArray = [NSMutableArray arrayWithArray:splashArray];
        if (splashArray.count>0) {
            BookRatingSplashView *bookRatingView = [[BookRatingSplashView alloc] init];
            bookRatingView.frame =  [AppDelegate ShareApp].window.bounds;
            restaurantSplash = [[IMGReservation alloc] init];
            restaurantSplash.restaurantName  = [tempSplashArray.firstObject objectForKey:@"restaurantName"];
            restaurantSplash.districtName = [tempSplashArray.firstObject objectForKey:@"district"];
            restaurantSplash.imageUrl = [tempSplashArray.firstObject objectForKey:@"imagePath"];
            restaurantSplash.restaurantId = [tempSplashArray.firstObject objectForKey:@"restaurantId"];
            restaurantSplash.reservationId = [tempSplashArray.firstObject objectForKey:@"reservationId"];
            bookRatingView.splashRestaurant = restaurantSplash;
            bookRatingView.bookRatingSplashViewDelegate = self;
            [[AppDelegate ShareApp].window addSubview:bookRatingView];
            [tempSplashArray removeObjectAtIndex:0];
            [tempSplashArray writeToFile:[self documentsPath:@"bookRatingArray.txt"] atomically:YES];
            [[Amplitude instance] logEvent:@"RC - Post Booking Splash Screen" withEventProperties:@{@"Restaurant_ID":restaurantSplash.restaurantId}];
            
        }else{
            return ;
        }
        
    }];
}
-(void)donotGoLabelTap:(IMGReservation*)restaurant_{
    IMGUser *user = [IMGUser currentUser];
    NSNumber *ratingNum = [NSNumber numberWithFloat:0];
    NSDictionary *dic=@{@"restaurantID":restaurantSplash.restaurantId,@"reviewerID":user.userId,@"rating":ratingNum,@"serviceScore":ratingNum,@"foodScore": ratingNum,@"ambianceScore":ratingNum,@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],@"reservationId":restaurantSplash.reservationId};
    
    [HomeUtil postBookRatingScore:dic andBlock:^(BOOL isSucceed) {
        if (!isSucceed) {
            [self donotGoLabelTap:restaurant_];
            return ;
            
        }
        [[Amplitude instance] logEvent:@"CL - Not Give Rating" withEventProperties:@{@"Restaurant_ID":restaurantSplash.restaurantId}];
        
        NSString *filePath = [self documentsPath:@"bookRatingArray.txt"];
        NSMutableArray *array = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
        if (array.count>0) {
            BookRatingSplashView *bookRatingView = [[BookRatingSplashView alloc] init];
            bookRatingView.frame =  [AppDelegate ShareApp].window.bounds;
            restaurantSplash = [[IMGReservation alloc] init];
            restaurantSplash.restaurantName = [array.firstObject objectForKey:@"restaurantName"];
            restaurantSplash.districtName = [array.firstObject objectForKey:@"district"];
            restaurantSplash.imageUrl = [array.firstObject objectForKey:@"imagePath"];
            restaurantSplash.restaurantId = [array.firstObject objectForKey:@"restaurantId"];
            restaurantSplash.reservationId = [array.firstObject objectForKey:@"reservationId"];
            bookRatingView.splashRestaurant = restaurantSplash;
            bookRatingView.bookRatingSplashViewDelegate = self;
            
            [[AppDelegate ShareApp].window addSubview:bookRatingView];
            [array removeObjectAtIndex:0];
            [array writeToFile:[self documentsPath:@"bookRatingArray.txt"] atomically:YES];
            
        }
        
    }];
}
-(void)ratingTap:(float)rating andRestaurant:(IMGReservation*)restaurant_{
    IMGUser *user = [IMGUser currentUser];
    NSNumber *ratingNum = [NSNumber numberWithFloat:rating*2];
    NSDictionary *dic=@{@"restaurantID":restaurantSplash.restaurantId,@"reviewerID":user.userId,@"rating":ratingNum,@"serviceScore":ratingNum,@"foodScore": ratingNum,@"ambianceScore":ratingNum,@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],@"reservationId":restaurantSplash.reservationId};
    
    [HomeUtil postBookRatingScore:dic andBlock:^(BOOL isSucceed) {
        if (!isSucceed) {
            [self ratingTap:rating andRestaurant:restaurant_];
            return ;
        }
        [[Amplitude instance] logEvent:@"UC - Give Rating Splash Screen" withEventProperties:@{@"Rating_ID":restaurantSplash.reservationId,@"Restaurant_ID":restaurantSplash.restaurantId,@"Rating_Value":[NSNumber numberWithFloat:rating]}];
        NSString *filePath = [self documentsPath:@"bookRatingArray.txt"];
        NSMutableArray *array = [[NSMutableArray alloc] initWithContentsOfFile:filePath];
        if (array.count>0) {
            BookRatingSplashView *bookRatingView = [[BookRatingSplashView alloc] init];
            bookRatingView.frame =  [AppDelegate ShareApp].window.bounds;
            restaurantSplash = [[IMGReservation alloc] init];
            restaurantSplash.restaurantName = [array.firstObject objectForKey:@"restaurantName"];
            restaurantSplash.districtName = [array.firstObject objectForKey:@"district"];
            restaurantSplash.imageUrl = [array.firstObject objectForKey:@"imagePath"];
            restaurantSplash.reservationId = [array.firstObject objectForKey:@"reservationId"];
            restaurantSplash.restaurantId = [array.firstObject objectForKey:@"restaurantId"];
            bookRatingView.splashRestaurant = restaurantSplash;
            bookRatingView.bookRatingSplashViewDelegate = self;
            [[AppDelegate ShareApp].window addSubview:bookRatingView];
            [array removeObjectAtIndex:0];
            [array writeToFile:[self documentsPath:@"bookRatingArray.txt"] atomically:YES];
            
        }
        
    }];
}
-(NSString *)bundlePath:(NSString *)fileName {
    return [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:fileName];
}

-(NSString *)documentsPath:(NSString *)fileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [documentsDirectory stringByAppendingPathComponent:fileName];
}

-(NSString *)documentsPath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}



- (void)didReceiveMemoryWarning{
    [[SDImageCache sharedImageCache] setValue:nil forKey:@"memCache"];
    [[SDImageCache sharedImageCache] clearMemory];
}

@end
