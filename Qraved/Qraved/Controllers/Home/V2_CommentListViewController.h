//
//  V2_CommentListViewController.h
//  Qraved
//
//  Created by harry on 2017/6/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "IMGMediaComment.h"
#import "IMGOfferCard.h"
#import "IMGRestaurantEvent.h"
#import "IMGDish.h"
#import "IMGJourneyReview.h"
#import "IMGJourneyPhoto.h"
#import "PersonalSummaryTopReviewModel.h"
@interface V2_CommentListViewController : BaseViewController

@property (nonatomic, assign) int typeId;
@property (nonatomic, strong) IMGMediaComment *mediaComment;
@property (nonatomic, strong) IMGOfferCard *offerCard;
@property (nonatomic, strong) IMGRestaurantEvent *restaurantEvent;
@property (nonatomic, strong) IMGRestaurant *restaurant;
@property (nonatomic, strong) IMGReview *review;
@property (nonatomic, strong) IMGUser *user;
@property (nonatomic, strong) IMGDish *dish;
@property (nonatomic, strong)  IMGJourneyReview  *typeOne;
@property (nonatomic, strong)  IMGJourneyPhoto  *typeThree;
@property (nonatomic, strong)  PersonalSummaryTopReviewModel *summaryModel;

@property (nonatomic, copy) void (^refreshComment)();

-(id)initWithTypeID:(int)typeId andModel:(id)model;

@end
