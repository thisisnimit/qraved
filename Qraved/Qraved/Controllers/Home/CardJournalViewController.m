//
//  CardJournalViewController.m
//  Qraved
//
//  Created by System Administrator on 3/4/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "CardJournalViewController.h"
#import "DiningGuideRestaurantsViewController.h"
#import "IMGMediaComment.h"
#import "IMGDiningGuide.h"

@interface CardJournalViewController ()

@property(nonatomic,strong) IMGMediaComment *journal;

@end

@implementation CardJournalViewController

@synthesize journal=_journal;

-(instancetype)initWithJournal:(IMGMediaComment*)journal{
    if(self=[super init]){
        _journal=journal;
        self.journalView=[[CardJournalView alloc] initWithFrame:self.view.bounds journal:_journal];
        self.journalView.delegate=self;
        [self.view addSubview:self.journalView];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=[UIColor clearColor];
    [self addBackBtn];
}

-(void)shareBtnClick:(UIActivityViewController*)activityCtl{
    [self presentViewController:activityCtl animated:YES completion:^{
        
    }];
}

-(void)commentBtnClick:(NSDictionary*)context{
    
}


@end
