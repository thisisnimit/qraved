//
//  SelectCityController.m
//  Qraved
//
//  Created by root on 9/8/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "SelectCityController.h"
#import "CityUtils.h"
#import "CityHandler.h"
#import "IMGCity.h"
#import "UIColor+Helper.h"
#import "AppDelegate.h"
#import "HomeViewController.h"
#import "IMGUser.h"
#import "NavigationBarButtonItem.h"
#import "BookingInfoViewController.h"
#import "V2_PreferenceViewController.h"

extern BOOL ChangeCity;

@implementation SelectCityController{
	
	NSMutableArray *cities;
	NSInteger selected;
    BOOL isHiddenNavigationBar;
}

-(instancetype)initWithSelectedIndex:(NSNumber*)index{
	if(self=[super init]){
		self.tableView=[[UITableView alloc] initWithFrame:CGRectMake(0,0,DeviceWidth,DeviceHeight)];
		self.tableView.dataSource=self;
		self.tableView.delegate=self;
		self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
		cities=[[NSMutableArray alloc] init];
		self.title=L(@"Change City");
		selected=index.integerValue;
	}
	return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.isAutoPost) {
        isHiddenNavigationBar = self.navigationController.navigationBar.hidden;
        [self.navigationController.navigationBar setHidden:NO];
    }
    if (self.isNotHiddenTitle) {
        self.navigationController.navigationBarHidden=NO;
        [self.navigationController.navigationBar setHidden:NO];
    }
    [[LoadingView sharedLoadingView] stopLoading];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (self.isAutoPost) {
        self.navigationController.navigationBar.hidden = isHiddenNavigationBar;
    }
}

-(void)viewDidLoad{
	[super viewDidLoad];
    self.screenName = @"City selection page";
	self.view.backgroundColor=[UIColor whiteColor];
	if (self.isAutoPost) {
        NavigationBarButtonItem *navigationBarLeftButton = [[NavigationBarButtonItem alloc ]initWithImage:[UIImage imageNamed:NavigationBackImage] andTitle:@"" target:self action:@selector(goToBackView) width:43.0f offset:-15-[UIDevice heightDifference]];
        self.navigationItem.leftBarButtonItem = navigationBarLeftButton;
    }else{
        [self setBackBarButtonOffset30];
    }
	[CityUtils getCitiesWithCountryId:0 andBlock:^(NSArray *cityArray){
		cities=[cityArray mutableCopy];
		[self.tableView reloadData];
	}];
	[self.tableView reloadData];
	[self.view addSubview:self.tableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return cities.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"city_cell"];
	if(!cell){
		cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"city_cell"];
	}
	cell.frame=CGRectMake(LEFTLEFTSET,0,DeviceWidth-LEFTLEFTSET*2,DeviceHeight);
	IMGCity *city=(IMGCity*)[cities objectAtIndex:indexPath.row];
	cell.textLabel.text=[city.name capitalizedString];
	if(city.cityId.integerValue==selected){
		cell.contentView.backgroundColor=[UIColor colorEEEEEE];
	}else{
		cell.contentView.backgroundColor=[UIColor clearColor];
	}
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	IMGCity *city=(IMGCity*)[cities objectAtIndex:indexPath.row];
	selected=city.cityId.integerValue;
	[tableView reloadData];
    NSMutableDictionary* eventDic=[[NSMutableDictionary alloc]init];
    NSString *pageId= @"";
    if ([@"Dining guide list page" isEqualToString:self.amplitudeType]) {
        pageId = @",Dining guide list page";
    }
    if ([@"Homepage" isEqualToString:self.amplitudeType]) {
        pageId = @",Homepage";
    }
    [eventDic setValue:[NSString stringWithFormat:@"Header%@",pageId] forKey:@"Location"];
    [eventDic setValue:city.cityId forKey:@"Switch To"];
    [[Amplitude instance] logEvent:@"CL - City Selection" withEventProperties:eventDic];
	IMGUser *user=[IMGUser currentUser];
	if(!self.isFromList){
        if (user.userId) {
            [CityHandler changeCity:city.cityId withUser:user.userId block:nil];

        }
        [[NSUserDefaults standardUserDefaults] setObject:city.name forKey:CITY_SELECT];
        [[NSUserDefaults standardUserDefaults] setObject:city.cityId forKey:CITY_SELECT_ID];
        [[NSNotificationCenter defaultCenter] postNotificationName:CITY_SELECT_NOTIFICATION object:nil userInfo:nil];
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:city.name forKey:@"listCityname"];
        [[NSUserDefaults standardUserDefaults] setObject:city.cityId forKey:@"listCityId"];

    }

    BOOL isFirstOpen=[[NSUserDefaults standardUserDefaults] boolForKey:@"isFirstOpen"];//yes
    if(self.isShowPersonalization&&isFirstOpen){
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstOpen"];

        V2_PreferenceViewController *preference = [[V2_PreferenceViewController alloc] init];
        UINavigationController *preferenceNav=[[UINavigationController alloc]initWithRootViewController:preference];
        
//        PersonalizeViewController *personalizeViewController = [[PersonalizeViewController alloc] init];
//        UINavigationController *personalizeNavagationController=[[UINavigationController alloc]initWithRootViewController:personalizeViewController];
        [[AppDelegate ShareApp].window.rootViewController presentViewController:preferenceNav animated:NO completion:nil];
        [[AppDelegate ShareApp].selectedNavigationController popToRootViewControllerAnimated:NO];
    }else if (self.isFromBooking) {
        [self goToBookingInfoView];
    }else if (self.isAutoPost && (self.selectCityDelegate!=nil) && [self.selectCityDelegate respondsToSelector:@selector(successSelectCity)]) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.selectCityDelegate successSelectCity];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
	// HomeViewController *homeViewController=[[HomeViewController alloc] init];
	// [[[AppDelegate ShareApp] homeNavigationController] popToRootViewControllerAnimated:YES];
	// [[[AppDelegate ShareApp] homeNavigationController] setViewControllers:@[homeViewController]];
}

- (void)goToBackView{
    
    if (self.isAutoPost && (self.selectCityDelegate!=nil) && [self.selectCityDelegate respondsToSelector:@selector(successSelectCity)]) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.selectCityDelegate successSelectCity];
    }else{
        [self removeNotifications];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)goToBackViewController{
     BOOL isFirstOpen=[[NSUserDefaults standardUserDefaults] boolForKey:@"isFirstOpen"];
    if(self.isShowPersonalization&&isFirstOpen){
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstOpen"];

//        PersonalizeViewController *personalizeViewController = [[PersonalizeViewController alloc] init];
//        UINavigationController *personalizeNavagationController=[[UINavigationController alloc]initWithRootViewController:personalizeViewController];
        V2_PreferenceViewController *preference = [[V2_PreferenceViewController alloc] init];
        UINavigationController *preferenceNav=[[UINavigationController alloc]initWithRootViewController:preference];
        [[AppDelegate ShareApp].window.rootViewController presentViewController:preferenceNav animated:NO completion:nil];
        
    }else if (self.isFromBooking) {
        [self goToBookingInfoView];
    }else{
        [self removeNotifications];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)goToBookingInfoView{
    for(UIViewController *item in self.navigationController.viewControllers) {
        if ([item isKindOfClass:[BookingInfoViewController class]]) {
            [self.navigationController popToViewController:item animated:YES];
            return;
        }
    }
}
@end
