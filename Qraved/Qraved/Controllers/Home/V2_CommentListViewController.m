//
//  V2_CommentListViewController.m
//  Qraved
//
//  Created by harry on 2017/6/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_CommentListViewController.h"
#import "V2_CommentHandler.h"
#import "V2_CommentTableViewCell.h"
@interface V2_CommentListViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    UITableView *commentListTableView;
    NSMutableArray *commentListArr;
    NSNumber *lastCommentId;
    UIView *postView;
    UITextField *tfPost;
}
@property (nonatomic, strong) V2_CommentsModel *commentsModel;
@end

@implementation V2_CommentListViewController


-(id)initWithTypeID:(int)typeId andModel:(id)model;{
    if(self= [super init]){
        
        if ([model isKindOfClass:[IMGMediaComment class]]) {
            _mediaComment = model;
        }else if ([model isKindOfClass:[IMGOfferCard class]])
        {
            _offerCard = model;
        }else if ([model isKindOfClass:[IMGRestaurantEvent class]]){
            _restaurantEvent = model;
        }else if ([model isKindOfClass:[IMGRestaurant class]]){
            _restaurant = model;
        }else if ([model isKindOfClass:[IMGReview class]]){
            _review = model;
        }else if ([model isKindOfClass:[IMGDish class]]){
            _dish = model;
        }else if ([model isKindOfClass:[IMGUser class]]){
            _user = model;
        }else if ([model isKindOfClass:[IMGJourneyReview class]]){
        
            _typeOne = model;
        }else if ([model isKindOfClass:[IMGJourneyPhoto class]]){
            
            _typeThree = model;
        }else if ([model isKindOfClass:[PersonalSummaryTopReviewModel class]]){
            _summaryModel = model;
        }
        
        _typeId = typeId;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadData];
    [self requestData:@""];
    [self loadMainUI];
    [self addBackBtn];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    //NSArray *textFields = @[emailTextField, passwordTextField];
    UIView *focusView = postView;
//    for (UITextField *view in textFields) {
//        if ([view isFirstResponder]) {
//            focusView = view;
//            break;
//        }
//    }
    if (focusView) {
        double duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        CGFloat keyboardY = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].origin.y;
        CGRect rect = [focusView convertRect:focusView.bounds toView:[[[UIApplication sharedApplication] delegate] window]];
        CGPoint tmp = rect.origin;
        CGFloat inputBoxY = tmp.y + focusView.frame.size.height;
        CGFloat ty = keyboardY - inputBoxY;
        [UIView animateWithDuration:duration animations:^{
            if (ty < 0) {
               postView.frame = CGRectMake(0, DeviceHeight+20-44-64+ty, DeviceWidth, 44);
            }
        }];
    }
}
- (void)keyboardWillHide:(NSNotification *)notification
{
    double duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
       postView.frame = CGRectMake(0, DeviceHeight+20-44-64, DeviceWidth, 44);
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"Comments";
}

- (void)loadData{
    commentListArr = [NSMutableArray array];
}
- (void)requestData:(NSString *)lastId{
    
    NSString *urlString;
    NSDictionary *params;
    switch (_typeId) {
        case 2:
            urlString = @"journal/comments/previous";
            params = @{@"articleId":_mediaComment.mediaCommentId,@"lastJournalArticleCommentId":lastId};
            break;
        case 8:
        
            urlString = @"restaurantoffer/comments/previous";
            params = @{@"restaurantEventId":_offerCard.offerId,@"lastRestaurantOfferCommentId":lastId};
            break;
        case 10:
            if (_restaurantEvent !=nil) {
                urlString = @"restaurantevent/comments/previous";
                params = @{@"restaurantEventId":_restaurantEvent.eventId,@"lastRestaurantEventCommentId":lastId};
                
            }else{
                urlString = @"restaurantupdate/comment/previous";
                params = @{@"homeTimelineRestaurantUpdateId":_restaurant.homeTimeLineId,@"lastRestaurantEventHomeCommentId":lastId};
            }
            
            break;
            
        case 14:
            urlString = @"review/comments/previous";
            params = @{@"reviewId":_review.reviewId,@"lastReviewCommentId":lastId};
            
            break;
        case 15:
            if (_dish == nil) {
                urlString = @"moderatereview/comment/previous";
                params = @{@"moderateReviewId":_user.moderateReviewId,@"lastModerateReviewCommentId":lastId};
            }else{
                urlString = @"dish/comments/previous";
                params = @{@"dishId":_dish.dishId,@"lastDishCommentId":lastId};
            }
            break;
        case 16:
            urlString = @"review/comments/previous";
            params = @{@"reviewId":_typeOne.reviewId,@"lastReviewCommentId":lastId};
            
            break;
        case 17:
            if (_typeOne.dishList.count > 1) {
                urlString = @"moderatereview/comment/previous";
                params = @{@"moderateReviewId":_typeThree.uploadedPhotoId,@"lastModerateReviewCommentId":lastId};
            }else{
                urlString = @"dish/comments/previous";
                params = @{@"dishId":_typeThree.dishList[0][@"id"],@"lastDishCommentId":lastId};
            }
            
            break;
            
        case 20:
            urlString = @"review/comments/previous";
            params = @{@"reviewId":_summaryModel.myId,@"lastReviewCommentId":lastId};

        default:
            break;
    }
    //372  382
    [V2_CommentHandler getCommentListWithParams:params andPath:urlString andBlock:^(V2_CommentsModel *model,int total) {
        self.commentsModel = model;
        [commentListTableView.mj_footer endRefreshing];
        [commentListArr addObjectsFromArray:model.commentList];
        
        if (commentListArr.count>=total) {
            commentListTableView.mj_footer.hidden = YES;
        }else{
            commentListTableView.mj_footer.hidden = NO;
        }
        
        [commentListTableView reloadData];
        
        
    }];
    
}

- (void)loadMainUI{
    self.view.backgroundColor = [UIColor whiteColor];
    
    commentListTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+20-64-44) style:UITableViewStylePlain];
    commentListTableView.delegate = self;
    commentListTableView.dataSource = self;
    commentListTableView.separatorStyle = UITableViewCellSelectionStyleNone;
    [self.view addSubview:commentListTableView];
    
    commentListTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
    
    
    postView = [[UIView alloc] initWithFrame:CGRectMake(0, DeviceHeight+20-44-64, DeviceWidth, 44)];
    postView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:postView];
    
    UIView *topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 1)];
    topLine.backgroundColor = [UIColor colorEBEBEB];
    [postView addSubview:topLine];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(5, 4, DeviceWidth-68-5, 36)];
    bgView.backgroundColor = [UIColor colorWithHexString:@"FAFAFA"];
    bgView.layer.masksToBounds = YES;
    bgView.layer.cornerRadius = 16;
    bgView.layer.borderWidth = 1;
    bgView.layer.borderColor = [UIColor colorEBEBEB].CGColor;
    [postView addSubview:bgView];
    
    tfPost = [[UITextField alloc] initWithFrame:CGRectMake(22, 0, DeviceWidth-68-5-44, 36)];
    tfPost.placeholder = @"Write a comment...";
    tfPost.textColor = [UIColor color999999];
    tfPost.font = [UIFont systemFontOfSize:17];
    tfPost.delegate = self;
    [bgView addSubview:tfPost];
    
    UIButton *btnPost = [UIButton buttonWithType:UIButtonTypeCustom];
    btnPost.frame = CGRectMake(DeviceWidth-68, 0, 68, 44);
    [btnPost setTitle:@"Post" forState:UIControlStateNormal];
    [btnPost setTitleColor:[UIColor colorWithHexString:@"09BFD3"] forState:UIControlStateNormal];
    btnPost.titleLabel.font = [UIFont systemFontOfSize:17];
    [btnPost addTarget:self action:@selector(postClick:) forControlEvents:UIControlEventTouchUpInside];
    [postView addSubview:btnPost];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [tfPost resignFirstResponder];
}
- (void)loadMore{
    V2_CommentModel *model = [commentListArr lastObject];
    lastCommentId = model.commentId;
    [self requestData:[NSString stringWithFormat:@"%@",lastCommentId]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return commentListArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *idenStr = @"commentCell";
    V2_CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
    if (cell == nil) {
        cell = [[V2_CommentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
    }
    cell.selectionStyle =  UITableViewCellSelectionStyleNone;
    V2_CommentModel *model = [commentListArr objectAtIndex:indexPath.row];
    cell.commentModel = model;
    
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    V2_CommentModel *model = [commentListArr objectAtIndex:indexPath.row];
    
    return [tableView cellHeightForIndexPath:indexPath model:model keyPath:@"commentModel" cellClass:[V2_CommentTableViewCell class] contentViewWidth:DeviceWidth];
}

- (void)postClick:(UIButton *)btn{
    
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
    if (tfPost.text.length==0) {
        return;
    }
    
    NSString *trimmedString = [tfPost.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    
    NSString *path;
    NSDictionary *param;
   
    switch (_typeId) {
        case 2:
            path = @"journal/comment/write";
            param = @{@"articleId":_mediaComment.mediaCommentId,@"commentContent":trimmedString};
            break;
        case 8:
            path = @"restaurantoffer/comment";
            param = @{@"restaurantOfferId":_offerCard.offerId,@"comment":trimmedString};
            break;
        case 10:
            if (_restaurantEvent !=nil) {
                path = @"restaurantevent/comment";
                param = @{@"restaurantEventId":_restaurantEvent.eventId,@"comment":trimmedString};
                
            }else{
                path = @"restaurantEvent/saveHomeTimeLineComment";
                param = @{@"homeTimelineRestaurantUpdateId":_restaurant.homeTimeLineId,@"restaurantId":_restaurant.restaurantId,@"comment":trimmedString};
            }
            
            break;
        case 14:
            path = @"review/comment/writev2";
            param = @{@"reviewId":_review.reviewId,@"comment":trimmedString,@"restaurantId":self.commentsModel.restaurant.restaurantId};
            break;
        case 15:
            if (_dish == nil) {
               
                path = @"moderateReview/saveComment";
                param = @{@"comment":trimmedString,@"moderateReviewId":_user.moderateReviewId,@"restaurantId":self.commentsModel.restaurant.restaurantId};
            }else{
                path = @"dish/comment/write";
                param = @{@"comment":trimmedString,@"dishID":_dish.dishId,@"restaurantID":self.commentsModel.restaurant.restaurantId};
            }
            
            break;
        case 16:
            path = @"review/comment/writev2";
             param = @{@"reviewId":_typeOne.reviewId,@"comment":trimmedString,@"restaurantId":self.commentsModel.restaurant.restaurantId};

            
            break;
        case 17:
            if (_typeOne.dishList.count > 1) {
                path = @"moderatereview/comment/saveComment";
                param = @{@"comment":trimmedString,@"moderateReviewId":_typeThree.uploadedPhotoId,@"restaurantId":self.commentsModel.restaurant.restaurantId};
            }else{
                path = @"dish/comment/write";
                param = @{@"comment":trimmedString,@"dishID":_typeThree.dishList[0][@"id"],@"restaurantID":self.commentsModel.restaurant.restaurantId};
            }
            
            break;
            
        case 20:
            path = @"review/comment/writev2";
            param = @{@"reviewId":_summaryModel.myId,@"comment":trimmedString,@"restaurantId":self.commentsModel.restaurant.restaurantId};
        default:
            break;
    }
    
    [V2_CommentHandler postCommentWithParams:param andPath:path andBlock:^(V2_CommentModel *model) {
        [commentListArr insertObject:model atIndex:0];
        [commentListTableView reloadData];
        tfPost.text = @"";
        [tfPost resignFirstResponder];
        
        if (self.refreshComment) {
            self.refreshComment();
        }
    }];
}

@end
