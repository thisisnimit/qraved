//
//  JournalListViewController.h
//  Qraved
//
//  Created by Lucky on 15/6/29.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseChildViewController.h"
#import "JournalListTableViewCell.h"
#import "JournalCategoryViewController.h"

@interface JournalListViewController : BaseChildViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,JournalListTableViewCellDelegate>

@property (nonatomic,retain) NSString *searchText;
@property (nonatomic,retain) NSNumber *currentCategoryId;
@property (nonatomic,retain) NSString *currentCategoryName;

@property (nonatomic,assign) BOOL needLog;
@property (nonatomic,assign) BOOL isTrending;
@property (nonatomic,assign) BOOL isPop;
@property (nonatomic,assign) BOOL isFromSearch;

@property(nonatomic,retain) NSString *amplitudeType;
@property (nonatomic,assign) BOOL isFromTabMenu;
@end
