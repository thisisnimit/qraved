//
//  JournalDetailViewController.h
//  Qraved
//
//  Created by Lucky on 15/6/30.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseChildViewController.h"
#import "IMGMediaComment.h"
#import "CDPDelegate.h"
#import <Foundation/Foundation.h>
//#import "PPLabel.h"
@protocol JournalDetailViewControllerDelegate
@optional
- (void)reloadComment;
@end

@interface JournalDetailViewController : BaseChildViewController

@property (nonatomic)       IMGMediaComment *journal;

@property (nonatomic)       NSMutableArray *categoryDataArrM;

//@property (nonatomic,retain,readonly) PPLabel *categoryNameLabel;

@property (nonatomic) UILabel *categoryNameLabel;

@property(nonatomic) NSRange highlightedRange;

@property (nonatomic,assign) BOOL isFromAppDelegate;
@property (nonatomic,assign) id<CDPDelegate>cdpDelegate;
@property (nonatomic,assign) id<JournalDetailViewControllerDelegate>journalDetailViewControllerDelegate;
@property(nonatomic,assign) NSIndexPath *fromCellIndexPath;
@property(nonatomic,retain) NSString *amplitudeType;
@property(nonatomic,retain) NSString *gifCache;
@property (nonatomic,assign) BOOL isFromURL;

@end
