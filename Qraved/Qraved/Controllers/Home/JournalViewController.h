//
//  JournalViewController.h
//  Qraved
//
//  Created by Lucky on 15/4/22.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseChildViewController.h"
#import "EGORefreshTableHeaderView.h"

@interface JournalViewController : BaseChildViewController<EGORefreshTableHeaderDelegate>

@end
