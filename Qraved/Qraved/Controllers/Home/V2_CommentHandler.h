//
//  V2_CommentHandler.h
//  Qraved
//
//  Created by harry on 2017/6/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "V2_CommentModel.h"
#import "V2_CommentsModel.h"
@interface V2_CommentHandler : NSObject

+ (void)getCommentListWithParams:(NSDictionary *)params andPath:(NSString *)path andBlock:(void (^)(V2_CommentsModel *model,int total))block;

+ (void)postCommentWithParams:(NSDictionary *)params andPath:(NSString *)path andBlock:(void (^)(V2_CommentModel *model))block;

@end
