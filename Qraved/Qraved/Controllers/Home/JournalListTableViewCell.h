//
//  JournalListTableViewCell.h
//  Qraved
//
//  Created by Lucky on 15/6/29.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGMediaComment.h"
#import "PPLabel.h"

@protocol JournalListTableViewCellDelegate <NSObject>
@optional
- (void)categoryNameLabelClick:(NSString *)categoryName;

@end

@interface JournalListTableViewCell : UITableViewCell<PPLabelDelegate>

@property (nonatomic,retain) UIImageView *photoImageView;

@property (nonatomic,retain,readonly) UILabel *titleLabel;

@property (nonatomic,retain,readonly) PPLabel *categoryNameLabel;

@property(nonatomic) NSRange highlightedRange;

@property (nonatomic,assign) id<JournalListTableViewCellDelegate>delegate;


// @property (nonatomic,retain,readonly) UILabel *shareCountLabel;

- (void)setJournal:(IMGMediaComment *)journal;

@end
