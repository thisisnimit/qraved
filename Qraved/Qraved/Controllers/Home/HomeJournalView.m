//
//  HomeJournalView.m
//  Qraved
//
//  Created by Lucky on 15/9/8.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "HomeJournalView.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import "IMGMediaComment.h"
#import "NSString+Helper.h"
 
#import "UIImageView+WebCache.h"
#import "UIConstants.h"
#import "PPLabel.h"

@implementation HomeJournalView
{
    IMGMediaComment *_journal;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
//        self.frame  = frame;
        self.backgroundColor = [UIColor whiteColor];
        self.userInteractionEnabled = YES;
        
        _photoImageView = [[UIImageView alloc] init];
        _photoImageView.frame = CGRectMake(LEFTLEFTSET, 10, DeviceWidth-LEFTLEFTSET*2, 160);
        
        [self addSubview:_photoImageView];
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.numberOfLines = 2;
        _titleLabel.textColor = [UIColor color333333];
        
        //        _titleLabel.frame = CGRectMake(LEFTLEFTSET + 8, _photoImageView.endPointY + 3, DeviceWidth/2+50, 45);
        _titleLabel.frame = CGRectMake(LEFTLEFTSET + 8, _photoImageView.endPointY + 2, DeviceWidth - LEFTLEFTSET*2 - 16, 40);
        
        _titleLabel.font = [UIFont systemFontOfSize:16];
        [self addSubview:_titleLabel];
        
        _categoryNameLabel = [[PPLabel alloc] init];
        //        _categoryNameLabel.delegate = self;
        _categoryNameLabel.numberOfLines = 1;
        _categoryNameLabel.textColor = [UIColor grayColor];
        _categoryNameLabel.font = [UIFont systemFontOfSize:11];
        _categoryNameLabel.frame = CGRectMake(LEFTLEFTSET + 8, _titleLabel.endPointY + 1, _titleLabel.frame.size.width, 15);
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = _categoryNameLabel.frame;
        [self addSubview:btn];
        
        _categoryNameLabel.frame = CGRectMake(0, 0 , _titleLabel.frame.size.width, 15);
        
        [btn addSubview:_categoryNameLabel];
        
        
        UILabel *leftLine = [[UILabel alloc] init];
        leftLine.frame = CGRectMake(LEFTLEFTSET, _photoImageView.endPointY, 1, _categoryNameLabel.frame.size.height+_titleLabel.frame.size.height+6);
        leftLine.backgroundColor = [UIColor grayColor];
        leftLine.alpha = 0.2f;
        [self addSubview:leftLine];
        
        UILabel *rightLine = [[UILabel alloc] init];
        rightLine.frame = CGRectMake(_photoImageView.endPointX, _photoImageView.endPointY, 1, _categoryNameLabel.frame.size.height+_titleLabel.frame.size.height+6);
        rightLine.backgroundColor = [UIColor grayColor];
        rightLine.alpha = 0.2f;
        [self addSubview:rightLine];
        
        [self addShortLineImage:rightLine.endPointY-5 withCurrentView:self];
        
        
    }
    return self;
}


- (void)setJournal:(IMGMediaComment *)journal
{
    _journal = journal;
    __weak typeof (_photoImageView) weakPhotoImageView = _photoImageView;
    _journal.imageUrl = [_journal.imageUrl stringByReplacingPercentEscapesUsingEncoding:
                   NSUTF8StringEncoding];

    if ([_journal.imageUrl hasPrefix:@"http://"]||[_journal.imageUrl hasPrefix:@"https://"])
    {
//        [_photoImageView setImageWithURL:[NSURL URLWithString:_journal.imageUrl] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//            [UIView animateWithDuration:1.0 animations:^{
//                [weakPhotoImageView setAlpha:1.0];
//            }];
//        }];
        [_photoImageView sd_setImageWithURL:[NSURL URLWithString:_journal.imageUrl] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [UIView animateWithDuration:1.0 animations:^{
                                [weakPhotoImageView setAlpha:1.0];
                            }];
        }];
    }
    else
    {
        NSString *urlStr = [_journal.imageUrl returnFullImageUrlWithWidth:DeviceWidth - LEFTLEFTSET *2 ];
//        [_photoImageView setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//            [UIView animateWithDuration:1.0 animations:^{
//                [weakPhotoImageView setAlpha:1.0];
//            }];
//        }];
        [_photoImageView sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [UIView animateWithDuration:1.0 animations:^{
                                [weakPhotoImageView setAlpha:1.0];
                            }];
        }];
    }
    
    
    [_titleLabel setText:_journal.title];
    
    //    [_shareCountLabel setText:[NSString stringWithFormat:@"%@",_journal.shareCount]];
    [_categoryNameLabel setText:_journal.categoryName];
    
}

- (void)addShortLineImage:(CGFloat)currentPointY withCurrentView:(UIView *)currentView
{
    UIImageView *lineImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(15, currentPointY+5, DeviceWidth-30, 1)];
    lineImage3.image = [UIImage imageNamed:@"CutOffRule"];
    currentPointY = lineImage3.endPointY;
    [currentView addSubview:lineImage3];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
