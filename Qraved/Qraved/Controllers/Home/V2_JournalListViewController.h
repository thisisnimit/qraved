//
//  V2_JournalListViewController.h
//  Qraved
//
//  Created by harry on 2017/7/5.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"

@interface V2_JournalListViewController : BaseViewController

@property (nonatomic, assign) BOOL needLog;
@property (nonatomic, retain) NSString *amplitudeType;
@property (nonatomic, assign) BOOL isHomeSlide;
@property (nonatomic, copy) NSNumber *categoryId;

@end
