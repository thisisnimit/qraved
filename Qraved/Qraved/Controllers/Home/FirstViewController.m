////
////  FirstViewController.m
////  Qraved
////
////  Created by Libo Liu on 14-4-18.
////  Copyright (c) 2014年 Imaginato. All rights reserved.
////
//
//#import "FirstViewController.h"
//#import "AppDelegate.h"
//
//#import "NSString+Helper.h"
//#import "UIImageView+WebCache.h"
//#import "UIViewController+Helper.h"
//
//#import "IMGOnboard.h"
//#import "IMGSplash.h"
//#import "RestaurantsSelectionViewController.h"
//#import "IMGUploadPhoto.h"
//#import "ReviewPublishViewController.h"
//
//
//@interface FirstViewController ()<ZYQAssetPickerControllerDelegate>
//{
//    EScrollerView *onboardScrollView;
//    //    NSArray *onboardArr3;
//    NSArray *onboardArr4;
//    NSArray *onboardArr5;
//    NSArray *onboardArr6;
//    NSArray *onboardArr7;
//}
//@end
//
//@implementation FirstViewController
//
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//#pragma mark 初始化数据
//- (void)initData
//{
//    IMGOnboard *onboard01 = [[IMGOnboard alloc] init];
//    onboard01.imageUrl = @"1.jpg";
//    
//    IMGOnboard *onboard02 = [[IMGOnboard alloc] init];
//    onboard02.imageUrl = @"2.jpg";
//    
//    IMGOnboard *onboard03 = [[IMGOnboard alloc] init];
//    onboard03.imageUrl = @"3.jpg";
//    
//    IMGOnboard *onboard04 = [[IMGOnboard alloc] init];
//    onboard04.imageUrl = @"4.jpg";
//    IMGOnboard *onboard05= [[IMGOnboard alloc] init];
//    onboard05.imageUrl = @"5.jpg";
//    
//    onboardArr5 = [[NSArray alloc] initWithObjects:onboard01,onboard02,onboard03,onboard04, onboard05,nil];
//    
//    IMGOnboard *onboard11 = [[IMGOnboard alloc] init];
//    onboard11.imageUrl = @"11.jpg";
//    
//    IMGOnboard *onboard12 = [[IMGOnboard alloc] init];
//    onboard12.imageUrl = @"12.jpg";
//    
//    IMGOnboard *onboard13 = [[IMGOnboard alloc] init];
//    onboard13.imageUrl = @"13.jpg";
//    
//    IMGOnboard *onboard14 = [[IMGOnboard alloc] init];
//    onboard14.imageUrl = @"14.jpg";
//    IMGOnboard *onboard15 = [[IMGOnboard alloc] init];
//    onboard15.imageUrl = @"15.jpg";
//    onboardArr4 = [[NSArray alloc] initWithObjects:onboard11,onboard12,onboard13,onboard14,onboard15, nil];
//    
//    
//    IMGOnboard *onboard21 = [[IMGOnboard alloc] init];
//    onboard21.imageUrl = @"21.jpg";
//    
//    IMGOnboard *onboard22 = [[IMGOnboard alloc] init];
//    onboard22.imageUrl = @"22.jpg";
//    
//    IMGOnboard *onboard23 = [[IMGOnboard alloc] init];
//    onboard23.imageUrl = @"23.jpg";
//    
//    IMGOnboard *onboard24 = [[IMGOnboard alloc] init];
//    onboard24.imageUrl = @"24.jpg";
//    IMGOnboard *onboard25 = [[IMGOnboard alloc] init];
//    onboard25.imageUrl = @"25.jpg";
//    onboardArr6 = [[NSArray alloc] initWithObjects:onboard22,onboard22,onboard23,onboard24,onboard25, nil];
//    
//    
//    IMGOnboard *onboard31 = [[IMGOnboard alloc] init];
//    onboard31.imageUrl = @"31.jpg";
//    
//    IMGOnboard *onboard32 = [[IMGOnboard alloc] init];
//    onboard32.imageUrl = @"32.jpg";
//    
//    IMGOnboard *onboard33 = [[IMGOnboard alloc] init];
//    onboard33.imageUrl = @"33.jpg";
//    
//    IMGOnboard *onboard34 = [[IMGOnboard alloc] init];
//    onboard34.imageUrl = @"34.jpg";
//    IMGOnboard *onboard35 = [[IMGOnboard alloc] init];
//    onboard35.imageUrl = @"35.jpg";
//    onboardArr7 = [[NSArray alloc] initWithObjects:onboard31,onboard32,onboard33,onboard34, onboard35,nil];
//    
//}
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//    
//    [self initData];
//    
//#pragma mark 背景图片 bgImage
//    self.bgImage = [[UIImageView alloc]initWithFrame:self.view.frame];
//    self.bgImage.backgroundColor = [UIColor whiteColor];
//    
//    [self.view addSubview:self.bgImage];
//    
//#pragma mark 第一次登录 显示引导页
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    
//    if (![userDefaults boolForKey:[NSString stringWithFormat:@"%@%@",@"isFirstTimeOpenApp",QRAVED_VERSION]])
//        //    if (1)
//    {
//        [[Amplitude instance] logEvent:@"OB - View Start Page"];
//        UIButton *startJourneyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        startJourneyBtn.frame = CGRectMake(40, DeviceHeight-40-70, DeviceWidth-80, 49);
//        startJourneyBtn.backgroundColor = [UIColor colorC2060A];
//        startJourneyBtn.clipsToBounds = YES;
//        startJourneyBtn.layer.cornerRadius=10;
//        [startJourneyBtn setTitle:L(@"Start my food journey now!") forState:UIControlStateNormal];
//        [startJourneyBtn setTintColor:[UIColor whiteColor]];
//        UIImageView *logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"qraved_new_logo"]];
//        logoImageView.frame = CGRectMake(60, 60, DeviceWidth-120, 60);
//        logoImageView.contentMode = UIViewContentModeScaleAspectFill;
//        if ([UIDevice isRunningOniPhone])
//        {
//            if ([UIDevice isLaterThanIphone5]) {
//                onboardScrollView = [[EScrollerView alloc]initWithFrameRect:self.view.bounds objectArray:onboardArr5  isStartScroll:YES withPhotoTag:0 pageControlCenter:YES isCirculation:NO isPageControl:NO isEvent:NO isOnBoarding:YES];
//            }
//            else
//            {
//                onboardScrollView = [[EScrollerView alloc]initWithFrameRect:self.view.bounds objectArray:onboardArr4  isStartScroll:YES withPhotoTag:0 pageControlCenter:YES isCirculation:NO isPageControl:NO isEvent:NO isOnBoarding:YES];
//            }
//            
//        }
//        else
//        {
//            if ([UIDevice isiPadHiRes])
//            {
//                onboardScrollView = [[EScrollerView alloc]initWithFrameRect:self.view.bounds objectArray:onboardArr7  isStartScroll:YES withPhotoTag:0 pageControlCenter:YES isCirculation:NO isPageControl:NO isEvent:NO isOnBoarding:YES];
//            }
//            else
//            {
//                onboardScrollView = [[EScrollerView alloc]initWithFrameRect:self.view.bounds objectArray:onboardArr6  isStartScroll:YES withPhotoTag:0 pageControlCenter:YES isCirculation:NO isPageControl:NO isEvent:NO isOnBoarding:YES];
//            }
//        }
//        [self.view addSubview:onboardScrollView];
//        [startJourneyBtn addTarget:self action:@selector(gotoHomeViewController:) forControlEvents:UIControlEventTouchUpInside];
//
//        onboardScrollView.delegate = self;
//        [self.view addSubview:logoImageView];
//        [self.view addSubview:startJourneyBtn];
//        
//    }
//    else
//    {
//        [self gotoEvent];
//    }
//    
//    
//    
//}
//
//-(void)gotoEvent
//{
//#pragma mark 显示event的splash页面
//    
//    if ([[AppDelegate ShareApp] isShowEvent]) {
//        _images = [[NSMutableArray alloc]init];
//        for (NSDictionary *dict in [AppDelegate ShareApp].splashsArray) {
//            NSString *urlString = [[dict objectForKey:@"splash"] returnFullImageUrlWithWidth:self.view.frame.size.width];
//            IMGSplash *splash = [[IMGSplash alloc] init];
//            splash.imageUrl = urlString;
//            [_images addObject:splash];
//        }
//        if (_images.count == 1)
//        {
//            self.bgImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+20)];
//            self.bgImage.backgroundColor = [UIColor whiteColor];
//            
//            [self.view addSubview:self.bgImage];
//            
//            UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
//            [self.view addGestureRecognizer:panGestureRecognizer];
//            UISwipeGestureRecognizer* recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
//            recognizer.direction = UISwipeGestureRecognizerDirectionLeft;
//            [self.view addGestureRecognizer:recognizer];
////            [self.bgImage setImageWithURL:[NSURL URLWithString:((IMGSplash *)[_images objectAtIndex:0]).imageUrl] placeholderImage:[UIImage imageNamed:@"LaunchImage"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) { }];
//            [self.bgImage sd_setImageWithURL:[NSURL URLWithString:((IMGSplash *)[_images objectAtIndex:0]).imageUrl] placeholderImage:[UIImage imageNamed:@"LaunchImage"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//            }];
//            [self performSelector:@selector(gotoHomeViewController) withObject:self afterDelay:3];
//        }
//        else if (_images.count>1) {
//            _scorllView=[[EScrollerView alloc] initWithFrameRect:CGRectMake(0, 0, DeviceWidth, DeviceHeight+20) objectArray:_images  isStartScroll:YES withPhotoTag:0 pageControlCenter:YES isCirculation:NO isPageControl:YES isEvent:NO];
//            _scorllView.backgroundColor = [UIColor grayColor];
//            _scorllView.delegate=self;
//            
//            [self.view addSubview:_scorllView];
//            [_scorllView startScrolling];
//        }
//        
//        else
//        {
//            [[AppDelegate ShareApp]loadLayerButton];
//            [self gotoHomeViewController];
//        }
//    }
//    else
//    {
//        
//        [[AppDelegate ShareApp]loadLayerButton];
//        IMGUser *user = [IMGUser currentUser];
//        if (user.userId) {
//            [self gotoHomeViewController];
//        }else{
//            LoginViewController *loginVC=[[LoginViewController alloc]init];
//            loginVC.isFromonboard = YES;
//            UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
//            [AppDelegate ShareApp].window.rootViewController = [AppDelegate ShareApp].tabMenuViewController;
//            [[AppDelegate ShareApp].window.rootViewController presentViewController:loginNavagationController animated:NO completion:^{
//                
//            }];
//        }
//        
//    }
//}
//
//#pragma mark ecrollView Delegate
//-(void)EScrollerViewDidClicked:(NSUInteger)index ContentOffset:(CGPoint)contentoffset
//{
//    if (_scorllView) {
//        if (contentoffset.x>DeviceWidth*(_images.count-1)) {
//            [self gotoHomeViewController];
//        }
//    }
//    else if(onboardScrollView)
//    {
//        if (contentoffset.x>DeviceWidth*(onboardArr5.count-1)) {
//            [self gotoEvent];
//        }
//    }
//}
//
//- (void)EScrollerViewBtnClicked:(NSUInteger)btnTag
//{
//    if (btnTag == 100)
//    {
//        
//        //        RestaurantsSelectionViewController *selectUploadRestaurantViewController = [[RestaurantsSelectionViewController alloc] init];
//        //        selectUploadRestaurantViewController.sourceType = selectionSourceUploadType;
//        //        selectUploadRestaurantViewController.isFromOnboard = YES;
//        //        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:selectUploadRestaurantViewController];
//        //
//        //        [self presentViewController:navigationController animated:YES completion:^{
//        //
//        //        }];
//        ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] initWithRestaurantTitle:nil andIsFromUploadPhoto:NO andIsUploadMenuPhoto:NO andCurrentRestaurantId:nil];
//        picker.publishViewController = self;
//        picker.maximumNumberOfSelection = 9;
//        picker.assetsFilter = [ALAssetsFilter allPhotos];
//        picker.showEmptyGroups=NO;
//        picker.delegate = self;
//        picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
//            if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
//                NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
//                return duration >= 5;
//            } else {
//                return YES;
//            }
//        }];
//        
//        [self presentViewController:picker animated:YES completion:nil];
//    }
//    else if (btnTag == 101)
//    {
//        
//        LoginViewController *loginVC=[[LoginViewController alloc]init];
//        loginVC.isFromonboard = YES;
//        UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
//        [AppDelegate ShareApp].window.rootViewController = [AppDelegate ShareApp].tabMenuViewController;
//        [[AppDelegate ShareApp].window.rootViewController presentViewController:loginNavagationController animated:NO completion:^{
//            
//        }];
//    }
//}
//#pragma mark 手势处理
//- (void) handlePan:(UIPanGestureRecognizer*) recognizer
//{
//    [[AppDelegate ShareApp]loadLayerButton];
//    [self gotoHomeViewController];
//}
//- (void)handleSwipeFrom:(UISwipeGestureRecognizer*)recognizer {
//    [[AppDelegate ShareApp]loadLayerButton];
//    [self gotoHomeViewController];
//}
//
//#pragma mark 跳转到Home页面
//-(void)gotoHomeViewController:(int)index{
//    index = onboardScrollView.currentIndex;
//    NSString *deviceId =[[[UIDevice currentDevice] identifierForVendor] UUIDString];
//    //sign up
//    NSNumber *subAction = [NSNumber numberWithInt:3];
//    NSNumber *slideIndex = [NSNumber numberWithInt:index];
//    NSDictionary *parameters = @{@"slideIndex":slideIndex,@"subAction":subAction,@"deviceId":deviceId};
//    [[IMGNetWork sharedManager] POST:@"useraction/add/onboarding" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        
//    }];
//    LoginViewController *loginVC=[[LoginViewController alloc]init];
//    loginVC.isFromonboard = YES;
//    UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
//    [AppDelegate ShareApp].window.rootViewController = [AppDelegate ShareApp].tabMenuViewController;
//    [[AppDelegate ShareApp].window.rootViewController presentViewController:loginNavagationController animated:NO completion:^{
//        
//    }];
//    [[Amplitude instance] logEvent:@"OB - Start Onboarding CTA" withEventProperties:@{@"Source":[NSString stringWithFormat:@"Screen %d",index+1]}];
//}
//
//-(void)gotoHomeViewController{
//    LoginViewController *loginVC=[[LoginViewController alloc]init];
//    loginVC.isFromonboard = YES;
//    UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
//    [AppDelegate ShareApp].window.rootViewController = [AppDelegate ShareApp].tabMenuViewController;
//    [[AppDelegate ShareApp].window.rootViewController presentViewController:loginNavagationController animated:NO completion:^{
//        
//    }];
//}
// 
//-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
//    
//    NSMutableArray *photosArrM = [[NSMutableArray alloc] init];
//    
//    for (int i=0; i<assets.count; i++) {
//        ALAsset *asset=assets[i];
//        UIImageView *imgview=[[UIImageView alloc] init];
//        imgview.contentMode=UIViewContentModeScaleAspectFill;
//        imgview.clipsToBounds=YES;
//        UIImage *tempImg=[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage];
//        // [imgview setImage:tempImg];
//        IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
//        photo.image = tempImg;
//        [photosArrM addObject:photo];
//    }
//    
//    [picker dismissViewControllerAnimated:NO completion:^{
//        ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc]initWithRestaurant:nil andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
//        publishReviewVC.amplitudeType = @"Existing photo on device image gallery";
//        publishReviewVC.isUploadPhoto = YES;
//        publishReviewVC.photosArrM = [NSMutableArray arrayWithArray:photosArrM];
//        publishReviewVC.isFromOnboard = YES;
//        publishReviewVC.ifSuccessLoginClickPost = YES;
//        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:publishReviewVC] animated:YES completion:nil];
//        
//    }];
//}
//@end

