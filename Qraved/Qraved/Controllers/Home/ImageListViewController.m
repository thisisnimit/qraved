//
//  ImageListViewController.m
//  Qraved
//
//  Created by DeliveLee on 16/3/9.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "ImageListViewController.h"
#import "IMGRestaurant.h"
#import "UIColor+Helper.h"
#import "UIDevice+Util.h"
#import "ImageListTableViewCell.h"
#import "IMGDish.h"
#import "CXAHyperlinkLabel.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import "UILabel+Helper.h"
#import "DLStarRatingControl.h"
#import "CXAHyperlinkLabel.h"
#import "NSString+Helper.h"
#import "NSString+CXAHyperlinkParser.h"
#import "UIImageView+Helper.h"
#import "SinglePhotoViewController.h"
#import "AlbumViewController.h"
#import "IMGRestaurantEvent.h"
#import "IMGUser.h"
#import "IMGReview.h"
#import "EntityKind.h"
#import "UIButton+WebCache.h"
#import "popReviewView.h"
#import "Date.h"
#import "ReviewActivityItemProvider.h"
#import "RestaurantActivityItemProvider.h"
#import "RestaurantEventActivityItemProvider.h"
#import "UplodePhotoActivityItemProvider.h"
#import "TwitterActivityItemProvider.h"
#import "IMGReviewComment.h"
#import "ReviewHandler.h"
#import "LoadingView.h"
#import "RestaurantHandler.h"
#import "UploadPhotoHandler.h"
#import "OtherProfileViewController.h"
#import "DetailViewController.h"
#import "RestaurantMutiPhotosActivityItemProvider.h"
#import "LikeView.h"
#import "HomeUtil.h"

#define LEFT_X 15

#define TITLE_LABEL_Y 5
#define TITLE_LABEL_HEIGHT 15
#define TITLE_LABEL_FONT 15

#define GAP_BETWEEN_TITLE_AND_CUISINE 1

#define CUISINE_DISTRICT_LABEL_HEIGHT 20

#define GAP_BETWEEN_CUISINE_AND_REVIEW 3

#define CELL_RATING_STAR_WIDTH 10

#define RESTAURANT_IMAGE_SIZE 70

#define LINK_TEXT @"<a style=\"text-decoration:none;color:#3A8CE2;font-family:'OpenSans'\" href=\"http://www.qraved.com/\">...read more</a>"


#define LEFTMARGIN 15
#define TEXTLABELTOPMARGIN 35
#define RIGHTMARGIN 30
#define TEXTLABELLEFTMARGIN 11+PROFILEIMAGEVIEWSIZE+8
#define PROFILEIMAGEVIEWSIZE 20
#define CONTENTLABELWIDTH DeviceWidth-LEFTMARGIN-PROFILEIMAGEVIEWSIZE-RIGHTMARGIN

#define SUMMARIZETEXTLENGTH 400



@interface ImageListViewController ()<UITableViewDataSource, UITableViewDelegate,ImageListTableViewCellDelegate,PopReviewViewDelegate,AlbumReloadCellDelegate,LikeCommentShareDelegate,CDPDelegate,LikeListViewDelegate>


@end

@implementation ImageListViewController
{
    
//    CXAHyperlinkLabel *titleLabel;
    TYAttributedLabel *titleLabel;
    UILabel *scoreLabel;
    //    UIImageView *celllineImageView;
    DLStarRatingControl *ratingScoreControl;
    
    
    UITableView *imageTableView;
    IMGRestaurant *restaurant;
    IMGUser *user;
    IMGReview *review;
    NSArray *dishArr;
    int index;
    CGFloat keyboardHeight;
    IMGType imgType;
    
    UIView *innerView;
    PopReviewView *popReviewView;
    IMGEntity *currentDish;
    UIButton *avatarLinkButton;
    UILabel *reviewAndPhotoCountLabel;
    UILabel *timeLabel;
    UILabel *reviewSummarizeLabel;
    BOOL readMore;
    UILabel* reviewTitleLabel;
    UILabel *likeCardLabel;
    UILabel *commentCardLabel;
    UILikeCommentShareView *likeCommentShareCardView;
    float currentPoint;
    BOOL isHeadLikeCommentShare;
    NSMutableArray* likelistArr;
    LikeView* likepopView;
    IMGRestaurantEvent *amplitudeRestaurantEvent;

}

-(instancetype)initWithRestaurant:(IMGRestaurant*)_restaurant andDishArr:(NSArray *)_dishArr andIndex:(int)_index andFromCellIndexPath:(NSIndexPath*)fromCellIndexPath{
    if(self=[super init]){
        restaurant = _restaurant;
        dishArr = _dishArr;
        index = _index-1;
        keyboardHeight=0.0f;
        self.fromCellIndexPath = fromCellIndexPath;
        imgType = [EntityKind typeKindFromEntity:[_dishArr firstObject]];
    }
    return self;
}
-(instancetype)initWithRestaurant:(IMGRestaurant*)_restaurant andUser:(IMGUser *)_user andDishArr:(NSArray *)_dishArr andIndex:(int)_index andFromCellIndexPath:(NSIndexPath*)fromCellIndexPath{
    if(self=[super init]){
        restaurant = _restaurant;
        user = _user;
        dishArr = _dishArr;
        index = _index-1;
        keyboardHeight=0.0f;
        self.fromCellIndexPath = fromCellIndexPath;
        imgType = TypeForIMGDish;
    }
    return self;
}

-(instancetype)initWithRestaurant:(IMGRestaurant*)_restaurant andReview:(IMGReview *)_review andDishArr:(NSArray *)_dishArr andIndex:(int)_index andFromCellIndexPath:(NSIndexPath*)fromCellIndexPath{
    if(self=[super init]){
        restaurant = _restaurant;
        review = _review;
        dishArr = _dishArr;
        index = _index-1;
        keyboardHeight=0.0f;
        self.fromCellIndexPath = fromCellIndexPath;
        imgType = TypeForIMGReview;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.view.backgroundColor=[UIColor clearColor];
    isHeadLikeCommentShare = NO;
    if (!self.isfromNotification) {
        [self createUI];

    }
    if(index!=-1&&!self.isfromNotification){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        [imageTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
        
    }
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    self.isfromNotification=NO;


}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self addBackButton];
    if (self.isfromNotification) {
        NSMutableDictionary* pragram=[[NSMutableDictionary alloc]init];
        [pragram setValue:self.moderateId forKey:@"moderateReviewId"];
        [pragram setValue:user.userId forKey:@"userId"];
        [[LoadingView sharedLoadingView]startLoading];
        self.view.backgroundColor=[UIColor whiteColor];
        UIView* backgroundView=[[UIView alloc]initWithFrame:self.view.bounds];
        backgroundView.backgroundColor=[UIColor whiteColor];
        [self.view addSubview:backgroundView];
        [UploadPhotoHandler getUploadPhotoDetailCardData:pragram andBlock:^(NSArray *dataArr) {
            restaurant = [dataArr objectAtIndex:0];
            dishArr=[dataArr objectAtIndex:3];
            user = [dataArr objectAtIndex:2];
            keyboardHeight=0.0f;
            imgType = TypeForIMGDish;
            
            [[LoadingView sharedLoadingView]stopLoading];
            [self createUI];
            
        }];
        
    }

    switch (imgType) {
        case TypeForIMGDish:
        {
            NSURL *urlString;
            user.avatar = [user.avatar stringByReplacingPercentEscapesUsingEncoding:
                             NSUTF8StringEncoding];
            if ([user.avatar hasPrefix:@"http"])
            {
                urlString = [NSURL URLWithString:user.avatar];
            }
            else
            {
                urlString = [NSURL URLWithString:[user.avatar returnFullImageUrl]];
            }

            NSString *path_sandox = NSHomeDirectory();

            //设置一个图片的存储路径
            NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/avastar.png"];
            UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
            image = [image cutCircleImage];
            if (image&&[user.userId intValue] == [[IMGUser currentUser].userId intValue]) {
                [avatarLinkButton setImage:image forState:UIControlStateNormal];
            }else{
                __weak UIButton *weakAvatarLinkButton = avatarLinkButton;

                [avatarLinkButton sd_setImageWithURL:urlString forState:UIControlStateNormal placeholderImage:[[UIImage imageNamed:@"headDefault.jpg"] cutCircleImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (image.size.height>0&&image.size.width>0) {
                                                image = [image cutCircleImage];
                                                [weakAvatarLinkButton setImage:image forState:UIControlStateNormal];
                                            }
                }];
            }

        }
            break;
        case TypeForIMGReview:
        {
            NSURL *urlString;
            review.avatar = [review.avatar stringByReplacingPercentEscapesUsingEncoding:
                           NSUTF8StringEncoding];
            if ([review.avatar hasPrefix:@"http"])
            {
                urlString = [NSURL URLWithString:review.avatar];
            }
            else
            {
                urlString = [NSURL URLWithString:[review.avatar returnFullImageUrl]];
            }
            //                urlString = [NSURL URLWithString:review.avatar];
            NSString *path_sandox = NSHomeDirectory();
            //设置一个图片的存储路径
            NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/avastar.png"];
            UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
            image = [image cutCircleImage];
            if (image&&[review.userId intValue] == [[IMGUser currentUser].userId intValue]) {
                [avatarLinkButton setImage:image forState:UIControlStateNormal];
            }else{
                __weak UIButton *weakAvatarLinkButton = avatarLinkButton;
                [avatarLinkButton sd_setImageWithURL:urlString forState:UIControlStateNormal placeholderImage:[[UIImage imageNamed:@"headDefault.jpg"] cutCircleImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (image.size.height>0&&image.size.width>0) {
                        image = [image cutCircleImage];
                        [weakAvatarLinkButton setImage:image forState:UIControlStateNormal];
                    }
                }];
            }

        }
            break;
            
        default:
            break;
    }
    
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

-(void)createUI{
    self.view.backgroundColor = [UIColor colorD9D9D9];
    [self createTableView];
    [self createTableHeader];
}
-(void)createTableHeader{
    
    imageTableView.tableHeaderView = ({
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 200)];
        
        
        innerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 200)];
        innerView.backgroundColor = [UIColor whiteColor];
        [view addSubview:innerView];
        
        
        //set data
        switch (imgType) {
            case TypeForIMGRestaurantEvent:
            {
                titleLabel = [[TYAttributedLabel alloc] initWithFrame:CGRectMake(LEFT_X, TITLE_LABEL_Y, DeviceWidth-2*LEFTLEFTSET, 1000)];
                titleLabel.textColor=[UIColor grayColor];
                titleLabel.backgroundColor=[UIColor clearColor];
                titleLabel.numberOfLines = 0;
                titleLabel.delegate = self;
                titleLabel.characterSpacing = 0.1;
                [titleLabel setLineBreakMode:kCTLineBreakByCharWrapping];
        
                titleLabel.font = [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:TITLE_LABEL_FONT];
                NSString *title = [NSString stringWithFormat:@"Promo from %@",restaurant.title];
                titleLabel.text = title;
                [titleLabel setFrameWithOrign:CGPointMake(LEFT_X, TITLE_LABEL_Y) Width:DeviceWidth-2*LEFTLEFTSET];
                TYLinkTextStorage *linkTextStorageRestaurant = [[TYLinkTextStorage alloc]init];
                linkTextStorageRestaurant.range = [titleLabel.text rangeOfString:restaurant.title];
                linkTextStorageRestaurant.textColor = [UIColor color333333];
                linkTextStorageRestaurant.linkData = @"restaurant";
                linkTextStorageRestaurant.underLineStyle = kCTUnderlineStyleNone;
                [titleLabel addTextStorage:linkTextStorageRestaurant];


                
                [innerView addSubview:titleLabel];
                
                scoreLabel = [[UILabel alloc] init];
                scoreLabel.font = [UIFont systemFontOfSize:12.5];
                scoreLabel.textColor = [UIColor colorFFC000];
                scoreLabel.frame = CGRectMake(LEFTLEFTSET, titleLabel.endPointY, scoreLabel.expectedWidth, scoreLabel.font.lineHeight);
                [innerView addSubview:scoreLabel];
                
                ratingScoreControl.hidden = YES;
                ratingScoreControl=[[DLStarRatingControl alloc] initWithFrame:CGRectMake(LEFT_X, TITLE_LABEL_Y+TITLE_LABEL_HEIGHT+GAP_BETWEEN_TITLE_AND_CUISINE+CUISINE_DISTRICT_LABEL_HEIGHT+GAP_BETWEEN_CUISINE_AND_REVIEW, 74, 10) andStars:5 andStarWidth:CELL_RATING_STAR_WIDTH isFractional:YES spaceWidth:2];
                ratingScoreControl.userInteractionEnabled=NO;
                ratingScoreControl.hidden = NO;
                [innerView addSubview:ratingScoreControl];
                
                timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, scoreLabel.endPointY + 13, 200, 15)];
                timeLabel.textColor = [UIColor grayColor];
                timeLabel.alpha = 0.7f;
                timeLabel.font = [UIFont systemFontOfSize:12];
                [timeLabel setText:[Date getTimeInteval_v4:[restaurant.timeline longLongValue]/1000]];
                [innerView addSubview:timeLabel];
                
                float score = [restaurant.ratingScore floatValue];
                [scoreLabel setText:[NSString stringWithFormat:@"%.1f",score/2]];
                scoreLabel.frame = CGRectMake(LEFTLEFTSET, titleLabel.endPointY, scoreLabel.expectedWidth, scoreLabel.font.lineHeight);
                
                
                ratingScoreControl.frame = CGRectMake(scoreLabel.endPointX + 3, titleLabel.endPointY +3, 74, 10);
                
                [ratingScoreControl updateRating:[NSNumber numberWithFloat:[restaurant.ratingScore floatValue]]];
                
                likeCardLabel = [[UILabel alloc] init];
                likeCardLabel.userInteractionEnabled=YES;
                UITapGestureRecognizer* likeCardLabelGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(likeCardTap:)];
                likeCardLabelGesture.numberOfTapsRequired=1;
                [likeCardLabel addGestureRecognizer:likeCardLabelGesture];
                currentPoint = timeLabel.endPointY;
                if (reviewSummarizeLabel) {
                    currentPoint = reviewSummarizeLabel.endPointY;
                }
                likeCardLabel.font = [UIFont systemFontOfSize:12];
                likeCardLabel.textColor = [UIColor colorC2060A];
                [innerView addSubview:likeCardLabel];
                
                commentCardLabel = [[UILabel alloc] init];
                commentCardLabel.userInteractionEnabled = YES;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(commentCardLabelTapped)];
                [commentCardLabel addGestureRecognizer:tap];
                
                commentCardLabel.font = [UIFont systemFontOfSize:12];
                commentCardLabel.textColor = [UIColor colorC2060A];
                [innerView addSubview:commentCardLabel];
                float start = LEFTLEFTSET;
                float top = currentPoint;
                
                if([restaurant.likeCardCount intValue]>0){
                    NSString *likeString=[NSString stringWithFormat:@"%@ %@",restaurant.likeCardCount,[restaurant.likeCardCount intValue]>1?L(@"Likes"):L(@"Like")];
                    [likeCardLabel setText:likeString];
                    likeCardLabel.frame=CGRectMake(start, top, likeCardLabel.expectedWidth,20);
                    currentPoint = likeCardLabel.endPointY;
                    start+=likeCardLabel.expectedWidth+15;
                    [likeCardLabel setHidden:NO];
                }
                if([restaurant.commentCardCount intValue]>0){
                    NSString *commentString=[NSString stringWithFormat:@"%@ %@",restaurant.commentCardCount,[restaurant.commentCardCount intValue]>1?L(@"Comments"):L(@"Comment")];
                    [commentCardLabel setText:commentString];
                    commentCardLabel.frame=CGRectMake(start, top, commentCardLabel.expectedWidth,20);
                    start+=commentCardLabel.expectedWidth+15;
                    currentPoint = commentCardLabel.endPointY;
                    [commentCardLabel setHidden:NO];
                }
                
                likeCommentShareCardView = [[UILikeCommentShareView alloc] initInSuperView:innerView atStartX:0 startY:ratingScoreControl.endPointY+55 commentCount:[restaurant.commentCardCount intValue]  commentArray:restaurant.commentCardList liked:restaurant.isLikeCard withFooter:NO needCommentListView:NO];
                UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, likeCommentShareCardView.endPointY, DeviceWidth, 1)];
                lineView.backgroundColor = [UIColor colorWithRed:232/255.0 green:233/255.0 blue:232/255.0 alpha:1];
                [innerView addSubview:lineView];
                
                likeCommentShareCardView.tag = 1001;
                likeCommentShareCardView.likeCommentShareDelegate = self;
                
            }
                
                break;
            case TypeForIMGDish:
            {
                avatarLinkButton = [UIButton buttonWithType:UIButtonTypeCustom];
                [avatarLinkButton addTarget:self action:@selector(gotoOtherProfileWithUserUpload) forControlEvents:UIControlEventTouchUpInside];
                avatarLinkButton.frame = CGRectMake(LEFTLEFTSET, 8, 40, 40);
//                avatarLinkButton.clipsToBounds = YES;
//                avatarLinkButton.layer.cornerRadius = 20;
                
                [innerView addSubview:avatarLinkButton];
                
                titleLabel = [[TYAttributedLabel alloc] init];
                titleLabel.frame = CGRectMake(avatarLinkButton.endPointX + 15, 5, DeviceWidth - LEFTLEFTSET*2 - avatarLinkButton.endPointX, 1000);
                titleLabel.font = [UIFont systemFontOfSize:TITLE_LABEL_FONT];
                titleLabel.numberOfLines = 0;
                titleLabel.delegate = self;
                titleLabel.characterSpacing = 0.1;
                [titleLabel setLineBreakMode:kCTLineBreakByCharWrapping];
                titleLabel.textColor = [UIColor grayColor];
                [innerView addSubview:titleLabel];
                
                reviewAndPhotoCountLabel = [[UILabel alloc] init];
                reviewAndPhotoCountLabel.font = [UIFont systemFontOfSize:13];
                reviewAndPhotoCountLabel.textColor = [UIColor grayColor];
                reviewAndPhotoCountLabel.alpha = 0.7f;
                [innerView addSubview:reviewAndPhotoCountLabel];
                
                timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, reviewAndPhotoCountLabel.endPointY , 240, 14)];
                
                timeLabel.textColor = [UIColor grayColor];
                timeLabel.alpha = 0.7f;
                timeLabel.font = [UIFont systemFontOfSize:12];
                [innerView addSubview:timeLabel];
                
                NSURL *urlString ;
                user.avatar = [user.avatar stringByReplacingPercentEscapesUsingEncoding:
                               NSUTF8StringEncoding];
                
                if ([user.avatar hasPrefix:@"http"])
                {
                    urlString = [NSURL URLWithString:user.avatar];
                }
                else
                {
                    urlString = [NSURL URLWithString:[user.avatar returnFullImageUrl]];
                }
                NSString *path_sandox = NSHomeDirectory();
                //设置一个图片的存储路径
                NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/avastar.png"];
                UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
                image = [image cutCircleImage];
                if (image&&[user.userId intValue] == [[IMGUser currentUser].userId intValue]) {
                    [avatarLinkButton setImage:image forState:UIControlStateNormal];
                }else{
                    __weak UIButton *weakAvatarLinkButton = avatarLinkButton;
                    [avatarLinkButton sd_setImageWithURL:urlString forState:UIControlStateNormal placeholderImage:[[UIImage imageNamed:@"headDefault.jpg"] cutCircleImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        if (image.size.height>0&&image.size.width>0) {
                            image = [image cutCircleImage];
                            [weakAvatarLinkButton setImage:image forState:UIControlStateNormal];
                        }
                    }];
                }
                
                
                NSString *uploadPhotoStr = [NSString stringWithFormat:@"userId:%@:%@:%@",user.userId,[user.userName stringByReplacingOccurrencesOfString:@" " withString:@"0"],user.avatar];
                NSString *textString;
                user.userName = [user.userName filterHtml];
                [titleLabel setText:[NSString stringWithFormat:@"%@ uploaded %ld photos for %@",user.userName,(unsigned long)dishArr.count,restaurant.title]];
                textString = [NSString stringWithFormat:@"%@ uploaded %ld photos for %@",[user.userName inFilterHtmlWithHref:uploadPhotoStr],(unsigned long)dishArr.count,[restaurant.title inFilterHtmlWithHref:[NSString stringWithFormat:@"restId:%@",restaurant.restaurantId]]];
                [titleLabel setFrameWithOrign:CGPointMake(avatarLinkButton.endPointX+15, 5) Width:DeviceWidth-LEFTLEFTSET*2-avatarLinkButton.endPointX];
                TYLinkTextStorage *linkTextStorage = [[TYLinkTextStorage alloc]init];
                linkTextStorage.range = NSMakeRange(0, user.userName.length);
                linkTextStorage.textColor = [UIColor color333333];
                linkTextStorage.linkData = @"user";
                linkTextStorage.underLineStyle = kCTUnderlineStyleNone;
                NSMutableArray *temArry = [NSMutableArray array];
                [temArry addObject:linkTextStorage];
                TYLinkTextStorage *linkTextStorageRestaurant = [[TYLinkTextStorage alloc]init];
                linkTextStorageRestaurant.range = NSMakeRange(user.userName.length+22, restaurant.title.length+1);
                linkTextStorageRestaurant.textColor = [UIColor color333333];
                linkTextStorageRestaurant.linkData = @"restaurant";
                linkTextStorageRestaurant.underLineStyle = kCTUnderlineStyleNone;
                
                [temArry addObject:linkTextStorageRestaurant];
                
                
                [titleLabel addTextStorageArray:temArry];
                
                if ([user.photoCount intValue]!=0&&[user.reviewCount intValue]!=0) {
                
                    [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@ - %@ %@",user.reviewCount,[user.reviewCount intValue] > 1 ? @"reviews":@"review",user.photoCount,[user.photoCount intValue] > 1 ? @"photos":@"photo"]];
                    reviewAndPhotoCountLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.endPointY, reviewAndPhotoCountLabel.expectedWidth, reviewAndPhotoCountLabel.font.lineHeight);
                }else if ([user.photoCount intValue]==0||[user.reviewCount intValue]==0){
                    if ([user.reviewCount intValue]==0&&[user.photoCount intValue]!=0) {
                        
                        [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@",user.photoCount,[user.photoCount intValue] > 1 ? @"photos":@"photo"]];
                        reviewAndPhotoCountLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.endPointY, reviewAndPhotoCountLabel.expectedWidth, reviewAndPhotoCountLabel.font.lineHeight);
                        
                        
                        
                    }
                    if ([user.photoCount intValue]==0&&[user.reviewCount intValue]!=0) {
                        
                        [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@",user.reviewCount,[user.reviewCount intValue] > 1 ? @"reviews":@"review"]];
                        reviewAndPhotoCountLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.endPointY, reviewAndPhotoCountLabel.expectedWidth, reviewAndPhotoCountLabel.font.lineHeight);
                        
                        
                        
                    }
                    
                }
                timeLabel.frame = CGRectMake(LEFTLEFTSET, reviewAndPhotoCountLabel.endPointY , 240, 14);
                if (reviewAndPhotoCountLabel.endPointY<avatarLinkButton.endPointY+15) {
                    timeLabel.frame = CGRectMake(LEFTLEFTSET, avatarLinkButton.endPointY+15, 240, 14);
                }
                
                [timeLabel setText:[Date getTimeInteval_v4:[restaurant.timeline longLongValue]/1000]];
                
                likeCardLabel = [[UILabel alloc] init];
                likeCardLabel.userInteractionEnabled=YES;
                UITapGestureRecognizer* likeCardLabelGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(likeCardTap:)];
                likeCardLabelGesture.numberOfTapsRequired=1;
                [likeCardLabel addGestureRecognizer:likeCardLabelGesture];
                currentPoint = timeLabel.endPointY;
                if (reviewSummarizeLabel) {
                    currentPoint = reviewSummarizeLabel.endPointY;
                }
                likeCardLabel.font = [UIFont systemFontOfSize:12];
                likeCardLabel.textColor = [UIColor colorC2060A];
                [innerView addSubview:likeCardLabel];
                
                commentCardLabel = [[UILabel alloc] init];
                commentCardLabel.userInteractionEnabled = YES;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(commentCardLabelTapped)];
                [commentCardLabel addGestureRecognizer:tap];
                commentCardLabel.font = [UIFont systemFontOfSize:12];
                commentCardLabel.textColor = [UIColor colorC2060A];
                [innerView addSubview:commentCardLabel];
                float start = LEFTLEFTSET;
                float top = currentPoint;
                
                if([restaurant.likeCardCount intValue]>0){
                    NSString *likeString=[NSString stringWithFormat:@"%@ %@",restaurant.likeCardCount,[restaurant.likeCardCount intValue]>1?L(@"Likes"):L(@"Like")];
                    [likeCardLabel setText:likeString];
                    likeCardLabel.frame=CGRectMake(start, top, likeCardLabel.expectedWidth,20);
                    currentPoint = likeCardLabel.endPointY;
                    start+=likeCardLabel.expectedWidth+15;
                    [likeCardLabel setHidden:NO];
                }
                if([restaurant.commentCardCount intValue]>0){
                    NSString *commentString=[NSString stringWithFormat:@"%@ %@",restaurant.commentCardCount,[restaurant.commentCardCount intValue]>1?L(@"Comments"):L(@"Comment")];
                    [commentCardLabel setText:commentString];
                    commentCardLabel.frame=CGRectMake(start, top, commentCardLabel.expectedWidth,20);
                    start+=commentCardLabel.expectedWidth+15;
                    currentPoint = commentCardLabel.endPointY;
                    [commentCardLabel setHidden:NO];
                }
                
                likeCommentShareCardView = [[UILikeCommentShareView alloc] initInSuperView:innerView atStartX:0 startY:timeLabel.endPointY+25 commentCount:[restaurant.commentCardCount intValue]  commentArray:restaurant.commentCardList liked:restaurant.isLikeCard withFooter:NO needCommentListView:NO];
                UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, likeCommentShareCardView.endPointY, DeviceWidth, 1)];
                lineView.backgroundColor = [UIColor colorWithRed:232/255.0 green:233/255.0 blue:232/255.0 alpha:1];
                [innerView addSubview:lineView];
                likeCommentShareCardView.tag = 1002;
                likeCommentShareCardView.likeCommentShareDelegate = self;
            }
                break;
                
            case TypeForIMGReview:
            {
                avatarLinkButton = [UIButton buttonWithType:UIButtonTypeCustom];
                avatarLinkButton.frame = CGRectMake(LEFTLEFTSET, 8, 40, 40);
//                avatarLinkButton.clipsToBounds = YES;
//                avatarLinkButton.layer.cornerRadius = 20;
                [avatarLinkButton addTarget:self action:@selector(gotoOtherProfileWithUserReview) forControlEvents:UIControlEventTouchUpInside];
                
                [innerView addSubview:avatarLinkButton];
                
                titleLabel = [[TYAttributedLabel alloc] init];
                titleLabel.frame = CGRectMake(avatarLinkButton.endPointX + 15, 5, DeviceWidth - LEFTLEFTSET*2 - avatarLinkButton.endPointX, 20);
                titleLabel.numberOfLines = 0;
                titleLabel.delegate = self;
                titleLabel.characterSpacing = 0.1;
                [titleLabel setLineBreakMode:kCTLineBreakByCharWrapping];
                titleLabel.textColor = [UIColor grayColor];
                [innerView addSubview:titleLabel];
                
                reviewAndPhotoCountLabel = [[UILabel alloc] init];
                reviewAndPhotoCountLabel.font = [UIFont systemFontOfSize:13];
                reviewAndPhotoCountLabel.textColor = [UIColor grayColor];
                reviewAndPhotoCountLabel.alpha = 0.7f;
                [innerView addSubview:reviewAndPhotoCountLabel];
                
                timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, reviewAndPhotoCountLabel.endPointY, 240, 14)];
                
                timeLabel.textColor = [UIColor grayColor];
                timeLabel.alpha = 0.7f;
                timeLabel.font = [UIFont systemFontOfSize:12];
                [innerView addSubview:timeLabel];
                NSURL *urlString;
                user.avatar = [user.avatar stringByReplacingPercentEscapesUsingEncoding:
                               NSUTF8StringEncoding];
                if ([review.avatar hasPrefix:@"http"])
                {
                    urlString = [NSURL URLWithString:review.avatar];
                }
                else
                {
                    urlString = [NSURL URLWithString:[review.avatar returnFullImageUrl]];
                }
                //                urlString = [NSURL URLWithString:review.avatar];
                NSString *path_sandox = NSHomeDirectory();
                //设置一个图片的存储路径
                NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/avastar.png"];
                UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
                image = [image cutCircleImage];
                if (image&&[review.userId intValue] == [[IMGUser currentUser].userId intValue]) {
                    [avatarLinkButton setImage:image forState:UIControlStateNormal];
                }else{
                    __weak UIButton *weakAvatarLinkButton = avatarLinkButton;

                    [avatarLinkButton sd_setImageWithURL:urlString forState:UIControlStateNormal placeholderImage:[[UIImage imageNamed:@"headDefault.jpg"] cutCircleImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        if (image.size.height>0&&image.size.width>0) {
                            image = [image cutCircleImage];
                            [weakAvatarLinkButton setImage:image forState:UIControlStateNormal];
                        }
                    }];
                }
                
                review.fullName = [review.fullName filterHtml];
                [titleLabel setText:[NSString stringWithFormat:@"%@ wrote a review for %@",review.fullName,restaurant.title]];
                [titleLabel setFrameWithOrign:CGPointMake(avatarLinkButton.endPointX + 15,5) Width:DeviceWidth-2*LEFTLEFTSET-avatarLinkButton.endPointX];
                TYLinkTextStorage *linkTextStorage = [[TYLinkTextStorage alloc]init];
                linkTextStorage.range = NSMakeRange(0, review.fullName.length);
                linkTextStorage.textColor = [UIColor color333333];
                linkTextStorage.linkData = @"user";
                linkTextStorage.underLineStyle = kCTUnderlineStyleNone;
                NSMutableArray *temArry = [NSMutableArray array];
                [temArry addObject:linkTextStorage];
                TYLinkTextStorage *linkTextStorageRestaurant = [[TYLinkTextStorage alloc]init];
                linkTextStorageRestaurant.range = NSMakeRange(review.fullName.length+19, restaurant.title.length+1);
                linkTextStorageRestaurant.textColor = [UIColor color333333];
                linkTextStorageRestaurant.linkData = @"restaurant";
                linkTextStorageRestaurant.underLineStyle = kCTUnderlineStyleNone;
                
                [temArry addObject:linkTextStorageRestaurant];
                
                
                [titleLabel addTextStorageArray:temArry];
                
                if ([user.photoCount intValue]!=0&&[user.reviewCount intValue]!=0) {
                    
                    [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@ - %@ %@",user.reviewCount,[user.reviewCount intValue] > 1 ? @"reviews":@"review",user.photoCount,[user.photoCount intValue] > 1 ? @"photos":@"photo"]];
                    reviewAndPhotoCountLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.endPointY, reviewAndPhotoCountLabel.expectedWidth, reviewAndPhotoCountLabel.font.lineHeight);
                }else if ([user.photoCount intValue]==0||[user.reviewCount intValue]==0){
                    if ([user.reviewCount intValue]==0&&[user.photoCount intValue]!=0) {
                        
                        [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@",user.photoCount,[user.photoCount intValue] > 1 ? @"photos":@"photo"]];
                        reviewAndPhotoCountLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.endPointY, reviewAndPhotoCountLabel.expectedWidth, reviewAndPhotoCountLabel.font.lineHeight);
                                              
                    }
                    if ([user.photoCount intValue]==0&&[user.reviewCount intValue]!=0) {
                        
                        [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@",user.reviewCount,[user.reviewCount intValue] > 1 ? @"reviews":@"review"]];
                        reviewAndPhotoCountLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.endPointY, reviewAndPhotoCountLabel.expectedWidth, reviewAndPhotoCountLabel.font.lineHeight);
                     
                    }
                    
                }


                timeLabel.frame = CGRectMake(LEFTLEFTSET, reviewAndPhotoCountLabel.endPointY, 240, 14);
                if (reviewAndPhotoCountLabel.endPointY<avatarLinkButton.endPointY+15) {
                    timeLabel.frame = CGRectMake(LEFTLEFTSET, avatarLinkButton.endPointY+15, 240, 14);
                }
                
                [timeLabel setText:[Date getTimeInteval_v4:[review.timeline longLongValue]/1000]];
                
                
                scoreLabel = [[UILabel alloc] init];
                scoreLabel.font = [UIFont systemFontOfSize:12.5];
                scoreLabel.textColor = [UIColor colorFFC000];
                scoreLabel.frame = CGRectMake(LEFTLEFTSET, timeLabel.endPointY, scoreLabel.expectedWidth, scoreLabel.font.lineHeight);
                [innerView addSubview:scoreLabel];
                
                ratingScoreControl=[[DLStarRatingControl alloc] initWithFrame:CGRectMake(scoreLabel.endPointX+3, timeLabel.endPointY, 74, 10) andStars:5 andStarWidth:CELL_RATING_STAR_WIDTH isFractional:YES spaceWidth:2];
                ratingScoreControl.userInteractionEnabled=NO;
                [innerView addSubview:ratingScoreControl];
                
                
                
                
                
                reviewTitleLabel = [[UILabel alloc] init];
                reviewTitleLabel.numberOfLines =0;
                reviewTitleLabel.font = [UIFont boldSystemFontOfSize:15];
                
                [reviewTitleLabel setText:review.title];
                CGSize reviewTitleLabelSize = [reviewTitleLabel.text sizeWithFont:reviewTitleLabel.font constrainedToSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 1000)];
                if (reviewTitleLabel.text.length)
                {
                    reviewTitleLabel.frame = CGRectMake(LEFTLEFTSET, scoreLabel.endPointY, DeviceWidth-2*LEFTLEFTSET, reviewTitleLabelSize.height);
                }else
                {
                    reviewTitleLabel.frame = CGRectMake(LEFTLEFTSET, scoreLabel.endPointY, reviewTitleLabel.expectedWidth, 0);
                }
                [innerView addSubview:reviewTitleLabel];
                
                
                float score = [review.score floatValue];
                [scoreLabel setText:[NSString stringWithFormat:@"%.1f",score/2]];
                scoreLabel.frame = CGRectMake(LEFTLEFTSET, timeLabel.endPointY + 3, scoreLabel.expectedWidth, 10);
                
                
                ratingScoreControl.frame = CGRectMake(scoreLabel.endPointX+3, timeLabel.endPointY + 3, 74, 10);
                [ratingScoreControl updateRating:[NSNumber numberWithFloat:score]];
                
                reviewSummarizeLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, 300, DeviceWidth - LEFTLEFTSET*2, 1000)];
                reviewSummarizeLabel.tag=711;
                
                reviewSummarizeLabel.text = review.summarize;
                reviewSummarizeLabel.numberOfLines = 0;
                reviewSummarizeLabel.font = [UIFont systemFontOfSize:13];
                reviewSummarizeLabel.textColor  = [UIColor grayColor];
                reviewSummarizeLabel.alpha = 0.7f;
                
                CGSize reviewSummarizeLabelSize = [review.summarize sizeWithFont:reviewSummarizeLabel.font constrainedToSize:CGSizeMake(DeviceWidth - LEFTLEFTSET*2, 1000) lineBreakMode:NSLineBreakByWordWrapping];
                
                reviewSummarizeLabel.frame=CGRectMake(LEFTLEFTSET, reviewTitleLabel.endPointY+5, DeviceWidth - LEFTLEFTSET*2, reviewSummarizeLabelSize.height);;
                
                [reviewSummarizeLabel layoutSubviews];
                
                [innerView addSubview:scoreLabel];
                [innerView addSubview:reviewSummarizeLabel];
                if(review){
                    [timeLabel setText:[Date getTimeInteval_v4:[review.timeline longLongValue]/1000]];
                }else{
                    [timeLabel setText:[Date getTimeInteval_v4:[restaurant.timeline longLongValue]/1000]];
                }
                
                likeCardLabel = [[UILabel alloc] init];
                
                likeCardLabel.userInteractionEnabled=YES;
                UITapGestureRecognizer* likeCardLabelGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(likeCardTap:)];
                likeCardLabelGesture.numberOfTapsRequired=1;
                [likeCardLabel addGestureRecognizer:likeCardLabelGesture];
                currentPoint = timeLabel.endPointY;
                if (reviewSummarizeLabel) {
                    currentPoint = reviewSummarizeLabel.endPointY;
                }
                likeCardLabel.font = [UIFont systemFontOfSize:12];
                likeCardLabel.textColor = [UIColor colorC2060A];
                [innerView addSubview:likeCardLabel];
                
                commentCardLabel = [[UILabel alloc] init];
                commentCardLabel.userInteractionEnabled = YES;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(commentCardLabelTapped)];
                [commentCardLabel addGestureRecognizer:tap];
                
                commentCardLabel.font = [UIFont systemFontOfSize:12];
                commentCardLabel.textColor = [UIColor colorC2060A];
                [innerView addSubview:commentCardLabel];
                float start = LEFTLEFTSET;
                float top = currentPoint;
                
                if([review.likeCount intValue]>0){
                    NSString *likeString=[NSString stringWithFormat:@"%@ %@",review.likeCount,[review.likeCount intValue]>1?L(@"Likes"):L(@"Like")];
                    [likeCardLabel setText:likeString];
                    likeCardLabel.frame=CGRectMake(start, top, likeCardLabel.expectedWidth,20);
                    currentPoint = likeCardLabel.endPointY;
                    start+=likeCardLabel.expectedWidth+15;
                    [likeCardLabel setHidden:NO];
                }
                if([review.commentCount intValue]>0){
                    NSString *commentString=[NSString stringWithFormat:@"%@ %@",review.commentCount,[review.commentCount intValue]>1?L(@"Comments"):L(@"Comment")];
                    [commentCardLabel setText:commentString];
                    commentCardLabel.frame=CGRectMake(start, top, commentCardLabel.expectedWidth,20);
                    start+=commentCardLabel.expectedWidth+15;
                    currentPoint = commentCardLabel.endPointY;
                    [commentCardLabel setHidden:NO];
                }
                
                likeCommentShareCardView = [[UILikeCommentShareView alloc] initInSuperView:innerView atStartX:0 startY:reviewSummarizeLabel.endPointY+25 commentCount:[review.commentCount intValue]  commentArray:review.commentList liked:review.isLike withFooter:NO needCommentListView:NO];
                UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, likeCommentShareCardView.endPointY, DeviceWidth, 1)];
                lineView.backgroundColor = [UIColor colorWithRed:232/255.0 green:233/255.0 blue:232/255.0 alpha:1];
                [innerView addSubview:lineView];
                likeCommentShareCardView.tag = 1003;
                likeCommentShareCardView.likeCommentShareDelegate = self;
                
                
            }
                break;
                
            default:
                break;
        }
        
        
       UIView* cellGapView = [[UIView alloc]init];
        cellGapView.backgroundColor = [UIColor colorD9D9D9];
        cellGapView.frame = CGRectMake(0, likeCommentShareCardView.endPointY, DeviceWidth, 15);
        UIImageView *shadowImage = [[UIImageView alloc] initShadowImageViewWithShadowOriginY_v2:0 andHeight:15];
        [cellGapView addSubview:shadowImage];
        [innerView addSubview:cellGapView];
        innerView.frame = CGRectMake(0, 0, DeviceWidth, cellGapView.endPointY);
        
        view.frame = CGRectMake(0, 0, DeviceWidth, innerView.frame.size.height);
        
        view;
    });
    
    
}
-(void)likeCardTap:(UITapGestureRecognizer*)tap
{
    if (likepopView) {
        [likepopView removeFromSuperview];
    }
    [[LoadingView sharedLoadingView]startLoading];
    switch (imgType) {
        case TypeForIMGDish:{
            [HomeUtil getLikeListMuchPhotoFromServiceWithmoderateReviewId:user.moderateReviewId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray, NSNumber *likeviewcount) {
                likelistArr=[NSMutableArray arrayWithArray:likeListArray];
                likepopView.likecount=likeviewcount;
                likepopView.likelistArray=likelistArr;
                [[LoadingView sharedLoadingView]stopLoading];
                
            }];
            
        }
            break;
        case TypeForIMGRestaurantEvent:{
            [HomeUtil getLikeListMuchPhotoFromServiceWithHomeTimelineRestaurantUpdateId:restaurant.homeTimeLineId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray, NSNumber *likeviewcount) {
                
                likelistArr=[NSMutableArray arrayWithArray:likeListArray];
                likepopView.likecount=likeviewcount;
                likepopView.likelistArray=likelistArr;
                [[LoadingView sharedLoadingView]stopLoading];

            }];
            
            
        }
            break;
        case TypeForIMGReview:{
        
        
            [HomeUtil getLikeListFromServiceWithReviewId:review.reviewId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray, NSNumber *likeviewcount) {
                
                
                likelistArr=[NSMutableArray arrayWithArray:likeListArray];
                likepopView.likecount=likeviewcount;
                likepopView.likelistArray=likelistArr;
                
                [[LoadingView sharedLoadingView]stopLoading];

        }];
        
        
        
        }
            
            
            break;
        default:
            break;
    }
    
    likepopView=[[LikeView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    likepopView.delegate=self;
    [self.view addSubview:likepopView];
   
}
-(void)LikeviewLoadMoreData:(NSInteger)offset
{

    switch (imgType) {
        case TypeForIMGDish:{
            [HomeUtil getLikeListMuchPhotoFromServiceWithmoderateReviewId:user.moderateReviewId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset] andBlock:^(NSArray *likeListArray, NSNumber *likeviewcount) {
                likelistArr=[NSMutableArray arrayWithArray:likeListArray];
                likepopView.likecount=likeviewcount;
                [likelistArr addObjectsFromArray:likeListArray];
                [likepopView.likeUserTableView reloadData];
                
            }];
            
        }
            break;
        case TypeForIMGRestaurantEvent:{
            [HomeUtil getLikeListMuchPhotoFromServiceWithHomeTimelineRestaurantUpdateId:restaurant.homeTimeLineId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset] andBlock:^(NSArray *likeListArray, NSNumber *likeviewcount) {
                [likelistArr addObjectsFromArray:likeListArray];
                 likepopView.likecount=likeviewcount;
                [likepopView.likeUserTableView reloadData];
            }];
            
            
        }
            break;
        case TypeForIMGReview:{
            
            
            [HomeUtil getLikeListFromServiceWithReviewId:review.reviewId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset] andBlock:^(NSArray *likeListArray, NSNumber *likeviewcount) {
                 likepopView.likecount=likeviewcount;
                [likelistArr addObjectsFromArray:likeListArray];
                [likepopView.likeUserTableView reloadData];
            }];
         
        }
     
            break;
        default:
            break;
    }


}
-(void)returnLikeCount:(NSInteger)count
{
    if (count!=0) {
        
   
    float start = LEFTLEFTSET;
    float top = currentPoint-20;

    NSString *likeString=[NSString stringWithFormat:@"%ld %@",(long)count,count>1?L(@"Likes"):L(@"Like")];
    [likeCardLabel setText:likeString];
    likeCardLabel.frame=CGRectMake(start, top, likeCardLabel.expectedWidth,20);
    }
}
-(void)createTableView{
    imageTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-44) style:UITableViewStylePlain];
    imageTableView.delegate = self;
    imageTableView.dataSource = self;
    imageTableView.backgroundColor = [UIColor clearColor];
    imageTableView.tableFooterView = [[UIView alloc]init];
    imageTableView.bounces = YES;
    imageTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:imageTableView];
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dishArr.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [ImageListTableViewCell calculateHeight:dishArr[indexPath.row]];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * showUserInfoCellIdentifier = @"ShowUserInfoCell";
    ImageListTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:showUserInfoCellIdentifier];
    if (cell != nil)
    {
        cell = nil;
    }
    cell = [[ImageListTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                         reuseIdentifier:showUserInfoCellIdentifier];
    cell.delegate = self;
    [cell setCellData:dishArr[indexPath.row]];
    
    return cell;
}

-(void)addBackButton
{
    self.navigationController.navigationBarHidden = NO;
    
    UIBarButtonItem *leftBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:NavigationBackImage] style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
    leftBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.leftBarButtonItem=leftBtn;
}

- (NSAttributedString *)attributedString:(NSArray *__autoreleasing *)outURLs
                               URLRanges:(NSArray *__autoreleasing *)outURLRanges andText:(NSString *)text
{
    NSString *HTMLText = text;
    NSArray *URLs;
    NSArray *URLRanges;
    UIColor *color = [UIColor grayColor];
    UIFont *font = [UIFont systemFontOfSize:TITLE_LABEL_FONT];
    NSMutableParagraphStyle *mps = [[NSMutableParagraphStyle alloc] init];
    //    mps.lineSpacing = ceilf(font.pointSize * .5);
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor whiteColor];
    shadow.shadowOffset = CGSizeMake(0, 1);
    NSString *str = [NSString stringWithHTMLText:HTMLText baseURL:nil URLs:&URLs URLRanges:&URLRanges];
    NSMutableAttributedString *mas = [[NSMutableAttributedString alloc] initWithString:str attributes:@
                                      {
                                          NSForegroundColorAttributeName : color,
                                          NSFontAttributeName            : font,
                                          NSParagraphStyleAttributeName  : mps,
                                          NSShadowAttributeName          : shadow,
                                      }];
    [URLRanges enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
        [mas addAttributes:@
         {
             NSForegroundColorAttributeName : [UIColor color222222],
             //       NSUnderlineStyleAttributeName  : @(NSUnderlineStyleSingle)
             NSForegroundColorAttributeName : [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:TITLE_LABEL_FONT]
             
         } range:[obj rangeValue]];
    }];
    
    *outURLs = URLs;
    *outURLRanges = URLRanges;
    
    return [mas copy];
}

-(void)backClick
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewFullImage:(int)_index{
    
    //find photo index
    int i = 0;
    if(imgType == TypeForIMGDish || imgType == TypeForIMGReview){
        for(i = 0; i<dishArr.count; i++){
            if([((IMGDish *)dishArr[i]).dishId intValue]==_index){
                break;
            }
        }
    }else if(imgType == TypeForIMGRestaurantEvent){
        for(i = 0; i<dishArr.count; i++){
            if([((IMGRestaurantEvent *)dishArr[i]).eventId intValue]==_index){
                break;
            }
        }
    }
    
    
    AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:dishArr andPhotoTag:i andRestaurant:restaurant andFromDetail:NO];
    albumViewController.title = restaurant.title;
    albumViewController.review = review;
    //    UIColor *color = [UIColor clearColor];
    //
    //    NSDictionary *dic = [NSDictionary dictionaryWithObject:color forKey:UITextAttributeTextColor];
    //    albumViewController.navigationController.navigationBar.titleTextAttributes = dic;
    albumViewController.albumReloadCellDelegate = self;
    
    albumViewController.showPage=NO;
    albumViewController.amplitudeType = @"card detail page";
    [self.navigationController pushViewController:albumViewController animated:YES];
    
}


-(void)imageListTableViewCell:(UITableViewCell *)cell commentButtonTapped:(UIButton *)button entity:(id)entity{
    currentDish = entity;
    isHeadLikeCommentShare = NO;
    if(popReviewView){
        [popReviewView removeFromSuperview];
        popReviewView = nil;
    }
    popReviewView = [[PopReviewView alloc]initInSuperView:self.view commentCount:[[entity valueForKey:@"commentCount"] intValue] commentArray:[entity valueForKey:@"commentList"]];
    popReviewView.delegate = self;
    
    if([entity valueForKey:@"commentCount"]>0&&(![entity valueForKey:@"commentList"]||((NSArray *)[entity valueForKey:@"commentList"]).count == 0)){
        if(imgType == TypeForIMGRestaurantEvent){
            [self popReviewView:popReviewView viewPreviousCommentsTapped:nil lastComment:[[IMGRestaurantEventComment alloc]init]];
            
        }else if(imgType == TypeForIMGDish || imgType == TypeForIMGReview){
            [self popReviewView:popReviewView viewPreviousCommentsTapped:nil lastComment:[[IMGDishComment alloc]init]];
        }
        [popReviewView showPopReviewView:YES];
    }else{
        [popReviewView showPopReviewView:NO];
    }
}

- (void)popReviewView:(PopReviewView *)popReviewView_ viewPreviousCommentsTapped:(UIButton *)button lastComment:(id)comment{
    if(imgType == TypeForIMGRestaurantEvent){
        if (isHeadLikeCommentShare) {
            [[LoadingView sharedLoadingView] startLoading];
            NSString *lastCommentIdStr=@"0";
            if([comment isKindOfClass:[IMGDishComment class]]){
                IMGDishComment *dishComment=(IMGDishComment *)comment;
                if(dishComment&&dishComment.commentId){
                    lastCommentIdStr=[NSString stringWithFormat:@"%@",dishComment.commentId];
                }
            }
            
            [RestaurantHandler previousCardComment:lastCommentIdStr homeTimelineRestaurantUpdateId:restaurant.homeTimeLineId andBlock:^(NSArray *commentList, NSNumber *likeCount, NSNumber *commentCount) {
                if (commentList&&commentList.count>0) {
                    if (commentList) {
                        
                        [popReviewView_ addPreviousComments:commentList];
                    }
                    
                }
                
                
            }];
            return;
        }else{
            IMGRestaurantEvent *_restaurantEvent = (IMGRestaurantEvent *)currentDish;
            [[LoadingView sharedLoadingView] startLoading];
            NSString *lastCommentIdStr=@"0";
            if([comment isKindOfClass:[IMGRestaurantEventComment class]]){
                IMGRestaurantEventComment *reviewComment=(IMGRestaurantEventComment *)comment;
                if(reviewComment&&reviewComment.commentId){
                    lastCommentIdStr=[NSString stringWithFormat:@"%@",reviewComment.commentId];
                }
                
                [RestaurantHandler previousComment:lastCommentIdStr targetId:_restaurantEvent.eventId andBlock:^(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount) {
                    if(commentList&&commentList.count>0){
                        
                        if(commentList){
                            [popReviewView_ addPreviousComments:commentList];
                        }
                    }
                }];
            }else{
                [[LoadingView sharedLoadingView] stopLoading];
            }
        }
        
        
    }else if(imgType == TypeForIMGDish){
        if (isHeadLikeCommentShare) {
            NSString *lastCommentIdStr=@"0";
            if([comment isKindOfClass:[IMGDishComment class]]){
                IMGDishComment *dishComment=(IMGDishComment *)comment;
                if(dishComment&&dishComment.commentId){
                    lastCommentIdStr=[NSString stringWithFormat:@"%@",dishComment.commentId];
                }
            }

            [UploadPhotoHandler previousCardComment:lastCommentIdStr targetId:user.moderateReviewId andBlock:^(NSArray *commentList, NSNumber *likeCount, NSNumber *commentCount) {
                if (commentList&&commentList.count>0) {
                    if (commentList) {
                        [popReviewView_ addPreviousComments:commentList];
                    }
                    
                }
            }];
            return;
            
        }
        IMGDish *_dish = (IMGDish*)currentDish;
        [[LoadingView sharedLoadingView] startLoading];
        NSString *lastCommentIdStr=@"0";
        if([comment isKindOfClass:[IMGDishComment class]]){
            IMGDishComment *dishComment=(IMGDishComment *)comment;
            if(dishComment&&dishComment.commentId){
                lastCommentIdStr=[NSString stringWithFormat:@"%@",dishComment.commentId];
            }
            
            [UploadPhotoHandler previousComment:lastCommentIdStr targetId:_dish.dishId andBlock:^(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount) {
                if(commentList&&commentList.count>0){
                    
                    if(commentList){
                        [popReviewView_ addPreviousComments:commentList];
                    }
                }
            }];
        }else{
            [[LoadingView sharedLoadingView] stopLoading];
        }
    }else if(imgType == TypeForIMGReview){
        if (isHeadLikeCommentShare) {
            [ReviewHandler previousComment:@"0" targetId:review.reviewId andBlock:^(NSArray *commentList, NSNumber *likeCount, NSNumber *commentCount) {
                if (commentList&&commentList.count>0) {
                    if (commentList) {
                        [popReviewView_ addPreviousComments:commentList];
                    }
                }
            }];
            return;
        }
        
        IMGDish *_dish = (IMGDish*)currentDish;
        [[LoadingView sharedLoadingView] startLoading];
        NSString *lastCommentIdStr=@"0";
        if([comment isKindOfClass:[IMGDishComment class]]){
            IMGDishComment *dishComment=(IMGDishComment *)comment;
            if(dishComment&&dishComment.commentId){
                lastCommentIdStr=[NSString stringWithFormat:@"%@",dishComment.commentId];
            }
            
            [UploadPhotoHandler previousComment:lastCommentIdStr targetId:_dish.dishId andBlock:^(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount) {
                if(commentList&&commentList.count>0){
                    //                    [self.view endEditing:YES];
                    
                    if(commentList){
                        [popReviewView_ addPreviousComments:commentList];
                    }
                }
            }];
        }else{
            [[LoadingView sharedLoadingView] stopLoading];
        }
        
    }
}

-(void)postComment:(UITextView *)textInput{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    if(imgType == TypeForIMGRestaurantEvent){
        //write comment
        if (isHeadLikeCommentShare) {
            [RestaurantHandler postCardComment:textInput.text restaurantId:restaurant.restaurantId homeTimeLineId:restaurant.homeTimeLineId andBlock:^(IMGRestaurantEventComment *eventComment) {
                [self.cdpDelegate CDP:self reloadTableCell:nil];
                IMGRestaurantEvent *_restaurantEvent = (IMGRestaurantEvent *)currentDish;

                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:_restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
                [eventProperties setValue:_restaurantEvent.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:eventComment.commentId forKey:@"Comment_ID"];
                [eventProperties setValue:@"Restaurant event card detail page" forKey:@"Location"];
                [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Succeed" withEventProperties:eventProperties];
                [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Restaurant Event Succeed" withValues:eventProperties];

                restaurant.commentCardCount=[NSNumber numberWithInt:([restaurant.commentCardCount intValue]+1)];
                if (restaurant.commentCardList) {
                    if([restaurant.commentCardList isKindOfClass:[NSArray class]]){
                        restaurant.commentCardList=[NSMutableArray arrayWithArray:restaurant.commentCardList];
                    }
                    [restaurant.commentCardList insertObject:eventComment atIndex:0];
                }else{
                    restaurant.commentCardList  = [[NSMutableArray alloc]initWithObjects:eventComment, nil];
                }
                [popReviewView addNewComment:eventComment];
                
                [self refreshLikeAndCommentLabel];
                
            } failure:^(NSString *errorReason){
                if([errorReason isLogout]){
                    [[LoadingView sharedLoadingView] stopLoading];
                    [[IMGUser currentUser]logOut];
                    [[AppDelegate ShareApp] goToLoginController];
                }
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:errorReason forKey:@"Reason"];
                [eventProperties setValue:@"Restaurant event card detail page" forKey:@"Location"];
                [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Failed" withEventProperties:eventProperties];
            }];
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];

            [eventProperties setValue:@"Restaurant event card detail page" forKey:@"Location"];
            [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Initiate" withEventProperties:eventProperties];
            return;
        }
        IMGRestaurantEvent *_restaurantEvent = (IMGRestaurantEvent *)currentDish;
        
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:_restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
        [eventProperties setValue:_restaurantEvent.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:@"Restaurant event card detail page" forKey:@"Location"];
        
        [RestaurantHandler postComment:textInput.text restaurantEventId:_restaurantEvent.eventId andBlock:^(IMGRestaurantEventComment *eventComment) {
            
            [eventProperties setValue:eventComment.commentId forKey:@"Comment_ID"];
            [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Succeed" withEventProperties:eventProperties];
            [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Restaurant Event Succeed" withValues:eventProperties];

            _restaurantEvent.commentCount=[NSNumber numberWithInt:([_restaurantEvent.commentCount intValue]+1)];
            if (_restaurantEvent.commentList) {
                
                if([_restaurantEvent.commentList isKindOfClass:[NSArray class]]){
                    _restaurantEvent.commentList=[NSMutableArray arrayWithArray:_restaurantEvent.commentList];
                }
                if (_restaurantEvent.commentList.count >= 3) {
                    [_restaurantEvent.commentList removeObjectAtIndex:_restaurantEvent.commentList.count-1];
                }
                [_restaurantEvent.commentList insertObject:eventComment atIndex:0];
            }else{
                _restaurantEvent.commentList = [[NSMutableArray alloc] initWithObjects:eventComment, nil];
            }

            [popReviewView addNewComment:eventComment];
            [imageTableView reloadData];
        }failure:^(NSString *errorReason){
            if([errorReason isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
                [[AppDelegate ShareApp] goToLoginController];
            }
            [eventProperties setValue:errorReason forKey:@"Reason"];
            [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Failed" withEventProperties:eventProperties];
        }];
        [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Initiate" withEventProperties:eventProperties];
        
        
        
    }else if(imgType == TypeForIMGDish){
        if (isHeadLikeCommentShare) {
            [UploadPhotoHandler postCommentCard:textInput.text moderateReviewId:user.moderateReviewId restaurantId:restaurant.restaurantId andBlock:^(IMGDishComment *dishComment) {
                [self.cdpDelegate CDP:self reloadTableCell:nil];
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:@"Photo card detail page" forKey:@"Location"];
                [eventProperties setValue:dishComment.commentId forKey:@"Comment_ID"];
                [eventProperties setValue:((IMGDish*)currentDish).dishId forKey:@"Photo_ID"];
                [eventProperties setValue:((IMGDish*)currentDish).userId forKey:@"UploaderUser_ID"];
                [eventProperties setValue:((IMGDish*)currentDish).restaurantId forKey:@"Restaurant_ID"];
                [[Amplitude instance] logEvent:@"UC - Comment Photo Succeed" withEventProperties:eventProperties];
                [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Photo Succeed" withValues:eventProperties];

                restaurant.commentCardCount=[NSNumber numberWithInt:([restaurant.commentCardCount intValue]+1)];
                if (restaurant.commentCardList) {
                    if([restaurant.commentCardList isKindOfClass:[NSArray class]]){
                        restaurant.commentCardList=[NSMutableArray arrayWithArray:restaurant.commentCardList];
                    }
                    [restaurant.commentCardList insertObject:dishComment atIndex:0];
                }else{
                    restaurant.commentCardList  = [[NSMutableArray alloc]initWithObjects:dishComment, nil];
                }
                [popReviewView addNewComment:dishComment];
               
                
                [self refreshLikeAndCommentLabel];
                
            }failure:^(NSString *errorReason){
                if([errorReason isLogout]){
                    [[LoadingView sharedLoadingView] stopLoading];
                    [[IMGUser currentUser]logOut];
                    [[AppDelegate ShareApp] goToLoginController];
                }
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:@"Photo card detail page" forKey:@"Location" ];
                [eventProperties setValue:errorReason forKey:@"Reason"];
                [[Amplitude instance] logEvent:@"UC – Comment Photo Failed" withEventProperties:eventProperties];
            }];
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:@"Photo card detail page" forKey:@"Location" ];
            
            [[Amplitude instance] logEvent:@"UC - Comment Photo Initiate" withEventProperties:eventProperties];
            
            return;
            
        }
        IMGDish *_dish = (IMGDish*)currentDish;
        
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:_dish.dishId forKey:@"Photo_ID"];
        [eventProperties setValue:_dish.userId forKey:@"UploaderUser_ID"];
        [eventProperties setValue:_dish.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:@"Photo card detail page" forKey:@"Location"];
        
        [UploadPhotoHandler postComment:textInput.text dishId:_dish.dishId restaurantId:_dish.restaurantId andBlock:^(IMGDishComment *dishComment) {
            
            [eventProperties setValue:dishComment.commentId forKey:@"Comment_ID"];
            [[Amplitude instance] logEvent:@"UC - Comment Photo Succeed" withEventProperties:eventProperties];
            [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Photo Succeed" withValues:eventProperties];

            
            _dish.commentCount=[NSNumber numberWithInt:([_dish.commentCount intValue]+1)];
            if(_dish.commentList){
                if([_dish.commentList isKindOfClass:[NSArray class]]){
                    _dish.commentList=[NSMutableArray arrayWithArray:_dish.commentList];
                }
                if(_dish.commentList.count>=3){
                    [_dish.commentList removeObjectAtIndex:_dish.commentList.count-1];
                }
                [_dish.commentList insertObject:dishComment atIndex:0];
            }else{
                _dish.commentList = [[NSMutableArray alloc]initWithObjects:dishComment, nil];
            }
           
            [popReviewView addNewComment:dishComment];
            [imageTableView reloadData];
            
            
        } failure:^(NSString *errorReason){
            if([errorReason isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
                [[AppDelegate ShareApp] goToLoginController];
            }
            [eventProperties setValue:errorReason forKey:@"Reason"];
            [[Amplitude instance] logEvent:@"UC – Comment Photo Failed" withEventProperties:eventProperties];
        }];
        [[Amplitude instance] logEvent:@"UC - Comment Photo Initiate" withEventProperties:eventProperties];
    }else if (imgType == TypeForIMGReview){
        
        if (isHeadLikeCommentShare) {
            [ReviewHandler postComment:textInput.text reviewId:review.reviewId restaurantId:restaurant.restaurantId andBlock:^(IMGReviewComment *reviewComment) {
                [self.cdpDelegate CDP:self reloadTableCell:nil];
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
                [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
                [eventProperties setValue:review.fullName forKey:@"ReviewFullName"];
                [eventProperties setValue:@"Review card detail page" forKey:@"Location"];
                [eventProperties setValue:reviewComment.commentId forKey:@"Comment_ID"];
                [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];

                [[Amplitude instance] logEvent:@"UC - Comment Review Succeed" withEventProperties:eventProperties];
                [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Review Succeed" withValues:eventProperties];


                review.commentCount=[NSNumber numberWithInt:([review.commentCount intValue]+1)];
                if (review.commentList) {
                    if([review.commentList isKindOfClass:[NSArray class]]){
                        review.commentList=[NSMutableArray arrayWithArray:review.commentList];
                    }
                    [review.commentList insertObject:reviewComment atIndex:0];
                }else{
                    review.commentList  = [[NSMutableArray alloc]initWithObjects:reviewComment, nil];
                }
                [popReviewView addNewComment:reviewComment];
                
                [self refreshLikeAndCommentLabel];
                
                
                
            }  failure:^(NSString *errorReason){
                if([errorReason isLogout]){
                    [[LoadingView sharedLoadingView] stopLoading];
                    [[IMGUser currentUser]logOut];
                    [[AppDelegate ShareApp] goToLoginController];
                }
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
                [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
                [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:[NSString stringWithFormat:@"%@",errorReason] forKey:@"Reason"];
                [eventProperties setValue:@"Review card detail page" forKey:@"Location"];
                [[Amplitude instance] logEvent:@"UC - Comment Review Failed" withEventProperties:eventProperties];
            }];
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
            [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
            [eventProperties setValue:review.fullName forKey:@"ReviewFullName"];
            [eventProperties setValue:@"Review card detail page" forKey:@"Location"];
            [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];

            [[Amplitude instance] logEvent:@"UC - Comment Review Initiate" withEventProperties:eventProperties];
            
            return;
        }
        IMGDish *_dish = (IMGDish*)currentDish;
        
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:_dish.dishId forKey:@"Photo_ID"];
        [eventProperties setValue:_dish.userId forKey:@"UploaderUser_ID"];
        [eventProperties setValue:_dish.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:@"Photo card detail page" forKey:@"Location"];
        
        [UploadPhotoHandler postComment:textInput.text dishId:_dish.dishId restaurantId:_dish.restaurantId andBlock:^(IMGDishComment *dishComment) {
            
            [eventProperties setValue:dishComment.commentId forKey:@"Comment_ID"];
            [[Amplitude instance] logEvent:@"UC - Comment Photo Succeed" withEventProperties:eventProperties];
            [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Photo Succeed" withValues:eventProperties];

            _dish.commentCount=[NSNumber numberWithInt:([_dish.commentCount intValue]+1)];
            if(_dish.commentList){
                if([_dish.commentList isKindOfClass:[NSArray class]]){
                    _dish.commentList=[NSMutableArray arrayWithArray:_dish.commentList];
                }
                if(_dish.commentList.count>=3){
                    [_dish.commentList removeObjectAtIndex:_dish.commentList.count-1];
                }
                [_dish.commentList insertObject:dishComment atIndex:0];
            }else{
                _dish.commentList = [[NSMutableArray alloc]initWithObjects:dishComment, nil];
            }
     
            
            
            [popReviewView addNewComment:dishComment];
            [imageTableView reloadData];
            
        } failure:^(NSString *errorReason){
            if([errorReason isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
                [[AppDelegate ShareApp] goToLoginController];
            }
            [eventProperties setValue:errorReason forKey:@"Reason"];
            [[Amplitude instance] logEvent:@"UC – Comment Photo Failed" withEventProperties:eventProperties];
        }];
        [[Amplitude instance] logEvent:@"UC – Comment Photo Initiate" withEventProperties:eventProperties];
    }
}


- (void)popReviewViewUserNameOrImageTapped:(id)sender{
    if ([sender isKindOfClass:[IMGUser class]])
    {
        IMGUser *user_ = (IMGUser *)sender;
        OtherProfileViewController *opvc = [[OtherProfileViewController alloc] init];
        
        opvc.amplitude = @"Card Detail Page";

        IMGUser *currentUser = [IMGUser currentUser];
        if ([currentUser.userId intValue] == [user_.userId intValue])
        {
            opvc.isOtherUser = NO;
        }else
        {
            opvc.isOtherUser = YES;
        }
        opvc.otherUser = user_;
        [self.navigationController pushViewController:opvc animated:YES];
    }
    else if ([sender isKindOfClass:[IMGRestaurant class]])
    {
        IMGRestaurant *restaurant_ = (IMGRestaurant *)sender;
        DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:restaurant_.restaurantId];
        [self.navigationController pushViewController:dvc animated:YES];
    }
    
}


-(void)imageListTableViewCell:(UITableViewCell *)cell shareButtonTapped:(UIButton *)button entity:(id)entity{
    if(imgType == TypeForIMGRestaurantEvent){
        //        IMGRestaurantEvent *_restaurantEvent = (IMGRestaurantEvent *)currentDish;
        IMGRestaurantEvent *_restaurantEvent = (IMGRestaurantEvent *)entity;
        amplitudeRestaurantEvent = _restaurantEvent;
        IMGRestaurant *_restaurant = [[IMGRestaurant alloc] init];
        _restaurant.restaurantId = _restaurantEvent.restaurantId;
        _restaurant.title = restaurant.title;
        _restaurant.seoKeyword = _restaurantEvent.restaurantSeo;
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/promo/%@",QRAVED_WEB_SERVER_OLD,_restaurantEvent.eventId]];
        NSString *shareContentString = [NSString stringWithFormat:@"%@ on Qraved. %@",_restaurant.title,url];
        RestaurantEventActivityItemProvider *provider=[[RestaurantEventActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
        provider.url=url;
        provider.title=shareContentString;
        provider.restaurantTitle=_restaurant.title;
        
        provider.cuisineAreaPrice = _restaurant.name;
        
        TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
        twitterActivityItemProvider.title = [NSString stringWithFormat:@"Check out this event at %@ on Qraved",_restaurant.title];
        
        UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
        
        activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
        [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK %@'s event on Qraved!"),_restaurant.title] forKey:@"subject"];
        activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
            if (completed) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:_restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
                [eventProperties setValue:_restaurantEvent.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:_restaurantEvent.title forKey:@"RestaurantTitle"];
                [eventProperties setValue:provider.activityType forKey:@"Channel"];
                [eventProperties setValue:@"Restaurant event card detail page" forKey:@"Location"];

                [[Amplitude instance] logEvent:@"SH - Share Restaurant Event" withEventProperties:eventProperties];
            }else{
            }
        };
        if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
            // iOS8
            activityCtl.popoverPresentationController.sourceView =
            button;
        }

        [self presentViewController:activityCtl animated:YES completion:^{
            
        }];
    }else if(imgType == TypeForIMGDish || imgType == TypeForIMGReview){
        //        IMGDish *_dish = (IMGDish*)currentDish;
        IMGDish *_dish = dishArr.firstObject;
        IMGRestaurant *_restaurant = [[IMGRestaurant alloc] init];
        _restaurant.restaurantId = _dish.restaurantId;
        _restaurant.title = restaurant.title;
        _restaurant.seoKeyword = _dish.restaurantSeo;
        //        if (!_dish.restaurantTitle.length) {
        //            _restaurant.title = currentRestaurant.title;
        //        }
        //                NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/photo/%@/%@",QRAVED_WEB_SERVER,[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],_restaurant.seoKeyword]];
        
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/photo/%@",QRAVED_WEB_SERVER_OLD,_dish.dishId]];
        
        NSString *shareContentString = [NSString stringWithFormat:@"Check out this photo at %@ on Qraved. %@",_restaurant.title,url];
        UplodePhotoActivityItemProvider *provider=[[UplodePhotoActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
        provider.url=url;
        provider.title=shareContentString;
        provider.restaurantTitle=_restaurant.title;
        provider.userName = _dish.userName;
        if (!_dish.userName.length) {
            provider.userName = review.fullName;
            _dish.userName = review.fullName;
        }
        TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
        twitterActivityItemProvider.title = [NSString stringWithFormat:@"Check out this photo at %@ on Qraved",_restaurant.title];
        
        UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
        
        
        //去除airDorp
        activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
        [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK IT OUT NOW! A Restaurant Photo by %@ on Qraved!"),_dish.userName] forKey:@"subject"];
        activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
            if (completed) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:_dish.dishId forKey:@"Photo_ID"];
                [eventProperties setValue:_dish.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:@"Photo card detail page" forKey:@"Locaton"];
                [eventProperties setValue:provider.activityType forKey:@"Channel"];

                [[Amplitude instance] logEvent:@"SH - Share Photo" withEventProperties:eventProperties];
            }else{
            }
        };
        if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
            // iOS8
            activityCtl.popoverPresentationController.sourceView =
            button;
        }

        [self presentViewController:activityCtl animated:YES completion:^{
            
        }];
        
    }else if (imgType == TypeForIMGReview) {
        
        IMGReview *_review = review;
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/review/%@",QRAVED_WEB_SERVER_OLD,_review.reviewId]];
        
        NSString *shareContentString = [NSString stringWithFormat:@"CHECK IT OUT NOW! A Restaurant Review by %@ on Qraved",_review.fullName];
        ReviewActivityItemProvider *provider=[[ReviewActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
        provider.username = _review.fullName;
        provider.restaurantTitle = _review.restaurantTitle;
        provider.url=url;
        provider.title=shareContentString;
        TwitterActivityItemProvider *twitterProvider=[[TwitterActivityItemProvider alloc] initWithPlaceholderItem:@""];
        twitterProvider.title=[NSString stringWithFormat:L(@"Check out %@ review at %@ on #Qraved!"),_review.fullName,_review.restaurantTitle];
        
        UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterProvider] applicationActivities:nil];
        activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
        [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK IT OUT NOW!A Restaurant Review By %@ on Qraved!"),_review.fullName] forKey:@"subject"];
        
        activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
            if (completed) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:_review.reviewId forKey:@"Review_ID"];
                [eventProperties setValue:_review.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:@"Photo card detail page" forKey:@"Locaton"];
                [eventProperties setValue:provider.activityType forKey:@"Channel"];

                [[Amplitude instance] logEvent:@"SH - Share Review" withEventProperties:eventProperties];
            }else{
            }
            
        };
        if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
            // iOS8
            activityCtl.popoverPresentationController.sourceView =
            button;
        }

        [self presentViewController:activityCtl animated:YES completion:^{
            
        }];
        
        
    }
}


- (void)imageListTableViewCell:(UITableViewCell*)cell likeCommentShareView:(UIView*)likeCommentShareView commentInputView:(UIView *)view inputViewDidBeginEditing:(UITextView *)textView{
    CGRect cellRect = [imageTableView rectForRowAtIndexPath:[imageTableView indexPathForCell:cell]];
    
    cellRect=CGRectMake(cellRect.origin.x, cellRect.origin.y+keyboardHeight, cellRect.size.width, cellRect.size.height);
    [imageTableView scrollToRowAtIndexPath:[imageTableView indexPathForCell:cell] atScrollPosition:UITableViewScrollPositionTop animated:NO];
    [imageTableView scrollRectToVisible:cellRect animated:NO];
    
}

-(void)keyboardChange:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    if (notification.name == UIKeyboardWillShowNotification) {
        //        CGFloat cellRectY=[_tableView rectForRowAtIndexPath:[_tableView indexPathForCell:cell]].origin.y;
        keyboardHeight=keyboardEndFrame.size.height;
    }else{
        keyboardHeight=0.0f;
    }
    
    [UIView commitAnimations];
}

- (void)imageListTableViewCell:(UITableViewCell*)cell newCommentPosted:(id)comment{
    NSIndexPath *indexPath = [imageTableView indexPathForCell:cell];
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            if ([[imageTableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                return;
            }
            [imageTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        
        @catch ( NSException *e ) {
            return;
        }
    });
}



- (void)imageListTableViewCell:(UITableViewCell*)cell likeCommentShareView:(UIView*)likeCommentShareView commentInputView:(UIView *)commentInputView inputViewExpand:(UITextView *)inputTextView commentInputViewYOffset:(CGFloat)commentInputViewYOffset{
    
}
- (void)imageListTableViewCell:(UITableViewCell*)cell likeButtonTapped:(UIButton*)button entity:(id)entity{
    NSIndexPath *indexPath = [imageTableView indexPathForCell:cell];
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            if ([[imageTableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                return;
            }
            [imageTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        
        @catch ( NSException *e ) {
            return;
        }
    });
    
}
-(void)reloadCell{
    [imageTableView reloadData];
}
- (void)gotoOtherProfileWithUserReview
{
    IMGUser *user_ = [[IMGUser alloc] init];
    user_.userId = [NSNumber numberWithInt:[review.userId intValue] ];
    user_.userName = review.fullName;
    user_.token = @"";
    user_.avatar = review.avatar;
    [self gotoOtherProfileWithUser:user_];
    
}
-(void)gotoOtherProfileWithUserUpload{
    [self.delegate gotoOtherProfileWithUser:user];
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView likeButtonTapped:(UIButton *)button{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
    //    [[LoadingView sharedLoadingView] startLoading];
    //like service
    switch (imgType) {
        case TypeForIMGReview:
        {
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            //Review_ID, ReviewerUser_ID, Restaurant_ID, Location
            [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
            [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
            [eventProperties setValue:review.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:@"Restaurant detail page - review list" forKey:@"Location"];
            [ReviewHandler likeReview:review.isLike reviewId:review.reviewId andBlock:^{
                if (!review.isLike) {
                    [[Amplitude instance] logEvent:@"UC - UnLike Review Succeed" withEventProperties:eventProperties];
                }else{
                    [[Amplitude instance] logEvent:@"UC - Like Review Succeed" withEventProperties:eventProperties];
                }
                
            }failure:^(NSString *exceptionMsg){
                if([exceptionMsg isLogout]){
                    [[LoadingView sharedLoadingView] stopLoading];
                    [[IMGUser currentUser]logOut];
                    [[AppDelegate ShareApp] goToLoginController];
                }
            }];
            [self.cdpDelegate CDP:self reloadTableCell:nil];

            
            review.isLike=!review.isLike;
            if(review.isLike){
                review.likeCount=[NSNumber numberWithInt:[review.likeCount intValue]+1];
                
                [[Amplitude instance] logEvent:@"UC – Like Review Initiate" withEventProperties:eventProperties];
                
            }else{
                review.likeCount=[NSNumber numberWithInt:[review.likeCount intValue]-1];
                [[Amplitude instance] logEvent:@"UC - UnLike Review Initiate" withEventProperties:eventProperties];
            }
            
            [self refreshLikeAndCommentLabel];
            
            if (likeCommentShareCardView) {
                [likeCommentShareCardView removeFromSuperview];
            }
            likeCommentShareCardView = [[UILikeCommentShareView alloc] initInSuperView:innerView atStartX:0 startY:reviewSummarizeLabel.endPointY+25 commentCount:[review.commentCount intValue]  commentArray:review.commentList liked:review.isLike withFooter:NO needCommentListView:NO];
            likeCommentShareCardView.likeCommentShareDelegate = self;
            
        }
            break;
        case TypeForIMGDish:
        {
            {
                
                [UploadPhotoHandler likeCard:restaurant.isLikeCard moderateReviewId:user.moderateReviewId andBlock:^{
                    
                }failure:^(NSString *exceptionMsg){
                    if([exceptionMsg isLogout]){
                        [[LoadingView sharedLoadingView] stopLoading];
                        [[IMGUser currentUser]logOut];
                        [[AppDelegate ShareApp] goToLoginController];
                    }
                }];
                [self.cdpDelegate CDP:self reloadTableCell:nil];

                
                restaurant.isLikeCard=!restaurant.isLikeCard;
                if(restaurant.isLikeCard){
                    restaurant.likeCardCount=[NSNumber numberWithInt:[restaurant.likeCardCount intValue]+1];
                    
                }else{
                    restaurant.likeCardCount=[NSNumber numberWithInt:[restaurant.likeCardCount intValue]-1];
                    
                }
                
                [self refreshLikeAndCommentLabel];
                
                if (likeCommentShareCardView) {
                    [likeCommentShareCardView removeFromSuperview];
                }
                likeCommentShareCardView = [[UILikeCommentShareView alloc] initInSuperView:innerView atStartX:0 startY:timeLabel.endPointY+25 commentCount:[restaurant.commentCardCount intValue]  commentArray:restaurant.commentCardList liked:restaurant.isLikeCard withFooter:NO needCommentListView:NO];
                likeCommentShareCardView.likeCommentShareDelegate = self;
                
            }
            
            break;
            
        }
        case TypeForIMGRestaurantEvent:
        {
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            //RestaurantEvent_ID, Restaurant_ID, Location
            [eventProperties setValue:amplitudeRestaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
            [eventProperties setValue:amplitudeRestaurantEvent.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:@"Restaurant detail page - event photo list" forKey:@"Location"];
            [RestaurantHandler likeRestaurantEventCard:restaurant.isLikeCard homeTimelineRestaurantUpdateId:restaurant.homeTimeLineId andBlock:^{
                if (!restaurant.isLikeCard)
                {
                    [[Amplitude instance] logEvent:@"UC - UnLike Restaurant Event Succeed" withEventProperties:eventProperties];
                }else
                {
                    [[Amplitude instance] logEvent:@"UC - Like Restaurant Event Succeed" withEventProperties:eventProperties];
                }
                

            }failure:^(NSString *exceptionMsg){
                if([exceptionMsg isLogout]){
                    [[LoadingView sharedLoadingView] stopLoading];
                    [[IMGUser currentUser]logOut];
                    [[AppDelegate ShareApp] goToLoginController];
                }
            }];
           
            [self.cdpDelegate CDP:self reloadTableCell:nil];
            
            restaurant.isLikeCard=!restaurant.isLikeCard;
            if(restaurant.isLikeCard){
                restaurant.likeCardCount=[NSNumber numberWithInt:[restaurant.likeCardCount intValue]+1];

                [[Amplitude instance] logEvent:@"UC - Like Restaurant Event Initiate" withEventProperties:eventProperties];
                
            }else{
                restaurant.likeCardCount=[NSNumber numberWithInt:[restaurant.likeCardCount intValue]-1];

                [[Amplitude instance] logEvent:@"UC - UnLike Restaurant Event Initiate" withEventProperties:eventProperties];
                
            }
            
            [self refreshLikeAndCommentLabel];
            
            if (likeCommentShareCardView) {
                [likeCommentShareCardView removeFromSuperview];
            }
            likeCommentShareCardView = [[UILikeCommentShareView alloc] initInSuperView:innerView atStartX:0 startY:ratingScoreControl.endPointY+55 commentCount:[restaurant.commentCardCount intValue]  commentArray:restaurant.commentCardList liked:restaurant.isLikeCard withFooter:NO needCommentListView:NO];
            likeCommentShareCardView.likeCommentShareDelegate = self;
            
        }
        default:
            break;
    }
}


- (void)likeCommentShareView:(UIView *)likeCommentShareView shareButtonTapped:(UIButton *)button{
    switch (imgType) {
        case TypeForIMGReview:
        {
            NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/review/%@",QRAVED_WEB_SERVER_OLD,review.reviewId]];
            
            NSString *shareContentString = [NSString stringWithFormat:@"CHECK IT OUT NOW! A Restaurant Review by %@ on Qraved",review.fullName];
            ReviewActivityItemProvider *provider=[[ReviewActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
            provider.username = review.fullName;
            provider.restaurantTitle = review.restaurantTitle;
            provider.url=url;
            provider.title=shareContentString;
            TwitterActivityItemProvider *twitterProvider=[[TwitterActivityItemProvider alloc] initWithPlaceholderItem:@""];
            twitterProvider.title=[NSString stringWithFormat:L(@"Check out %@ review at %@ on #Qraved!"),review.fullName,review.restaurantTitle];
            
            
            UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterProvider] applicationActivities:nil];
            activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
            [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK IT OUT NOW!A Restaurant Review By %@ on Qraved!"),review.fullName] forKey:@"subject"];
            
            activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
                if (completed) {
                    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                    [eventProperties setValue:provider.activityType forKey:@"Channel"];
                    [eventProperties setValue:@"Card Detail Page" forKey:@"Location"];
                    [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
                    [eventProperties setValue:review.fullName forKey:@"ReviewerName"];
                    [eventProperties setValue:review.restaurantId forKey:@"Restaurant_ID"];

                    [[Amplitude instance] logEvent:@"SH - Share Review" withEventProperties:eventProperties];
                }else{
                }
            };
            if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
                // iOS8
                activityCtl.popoverPresentationController.sourceView =
                button;
            }

            [self presentViewController:activityCtl animated:YES completion:^{
                
            }];
            
        }
            break;
            case TypeForIMGRestaurantEvent:
        {
            NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/multipromo/%@",QRAVED_WEB_SERVER_OLD,restaurant.homeTimeLineId]];
            NSString *shareContentString = [NSString stringWithFormat:@"%@ on Qraved. %@",restaurant.title,url];
            RestaurantEventActivityItemProvider *provider=[[RestaurantEventActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
            provider.url=url;
            provider.title=shareContentString;
            provider.restaurantTitle=restaurant.title;
            
            TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
            twitterActivityItemProvider.title = [NSString stringWithFormat:@"Check out this event at %@ on Qraved",restaurant.title];
            
            UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
            
            activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
            [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK %@'s event on Qraved!"),restaurant.title] forKey:@"subject"];
            activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
                if (completed) {
                    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
                    [eventProperties setValue:restaurant.title forKey:@"RestaurantTitle"];
                    [eventProperties setValue:@"Restaurant event card detail page" forKey:@"Location"];
                    [eventProperties setValue:amplitudeRestaurantEvent.eventId forKey:@"RestrantEvent_ID"];
                    [eventProperties setValue:provider.activityType forKey:@"Channel"];
                    [[Amplitude instance] logEvent:@"SH - Share Restaurant Event" withEventProperties:eventProperties];
                }else{
                }
            };
            if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
                // iOS8
                activityCtl.popoverPresentationController.sourceView =
                button;
            }

            [self presentViewController:activityCtl animated:YES completion:^{
                
            }];

        
        }break;
            case TypeForIMGDish:
        {
            IMGDish *_dish = dishArr.firstObject;
            NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/multiphoto/%@",QRAVED_WEB_SERVER_OLD,user.moderateReviewId]];
            NSString *shareContentString = [NSString stringWithFormat:@"Check out this photos at %@ on Qraved. %@",restaurant.title,url];
            RestaurantMutiPhotosActivityItemProvider *provider=[[RestaurantMutiPhotosActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
            provider.url=url;
            provider.title=shareContentString;
            provider.restaurantTitle=restaurant.title;
            provider.cuisineAreaPrice = user.userName;
            
            TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
            twitterActivityItemProvider.title = [NSString stringWithFormat:@"Check out this photos at %@ on Qraved",restaurant.title];
            
            
            UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
            activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
            [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK IT OUT NOW! A Restaurant Photos by %@ on Qraved!"),user.userName] forKey:@"subject"];
            activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
                if (completed) {
                    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                    [eventProperties setValue:@"Photo card detail page" forKey:@"Locaton"];
                    [eventProperties setValue:provider.activityType forKey:@"Channel"];
                    [eventProperties setValue:_dish.restaurantId forKey:@"Restaurant_ID"];
                    [eventProperties setValue:_dish.dishId forKey:@"Photo_ID"];
                    [[Amplitude instance] logEvent:@"SH - Share Photo" withEventProperties:eventProperties];
                }else{
                }
            };
            if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
                // iOS8
                activityCtl.popoverPresentationController.sourceView =
                button;
            }

            [self presentViewController:activityCtl animated:YES completion:^{
                
            }];

        
        }
            
        default:
            break;
    }
    
}

-(void)commentCardLabelTapped{

    isHeadLikeCommentShare = YES;
    if (imgType==TypeForIMGReview) {
        if(popReviewView){
            [popReviewView removeFromSuperview];
            popReviewView = nil;
        }
        popReviewView = [[PopReviewView alloc]initInSuperView:self.view commentCount:[review.commentCount intValue] commentArray:review.commentList];
        popReviewView.delegate = self;
        
        if([review.commentCount intValue]>0&&(!review.commentList||review.commentList.count == 0)){
            [self popReviewView:popReviewView viewPreviousCommentsTapped:nil lastComment:[[IMGRestaurantEventComment alloc]init]];
            [popReviewView showPopReviewView:YES];
        }else{
            [popReviewView showPopReviewView:NO];
        }
        
    }else{
        if(popReviewView){
            [popReviewView removeFromSuperview];
            popReviewView = nil;
        }
        popReviewView = [[PopReviewView alloc]initInSuperView:self.view commentCount:[restaurant.commentCardCount intValue] commentArray:restaurant.commentCardList];
        popReviewView.delegate = self;
        
        if(restaurant.commentCardList.count>10){
            [self popReviewView:popReviewView viewPreviousCommentsTapped:nil lastComment:[[IMGDishComment alloc] init] ];
            [popReviewView showPopReviewView:YES];
        }else{
            [popReviewView showPopReviewView:NO];
        }
        
    }
}
- (void)likeCommentShareView:(UIView *)likeCommentShareView commentButtonTapped:(UIButton *)button{
    isHeadLikeCommentShare = YES;
    if (imgType==TypeForIMGReview) {
        if(popReviewView){
            [popReviewView removeFromSuperview];
            popReviewView = nil;
        }
        popReviewView = [[PopReviewView alloc]initInSuperView:self.view commentCount:[review.commentCount intValue] commentArray:review.commentList];
        popReviewView.delegate = self;
        
        if([review.commentCount intValue]>0&&(!review.commentList||review.commentList.count == 0)){
            [self popReviewView:popReviewView viewPreviousCommentsTapped:nil lastComment:[[IMGRestaurantEventComment alloc]init]];
            [popReviewView showPopReviewView:YES];
        }else{
            [popReviewView showPopReviewView:NO];
        }
        
    }else{
        if(popReviewView){
            [popReviewView removeFromSuperview];
            popReviewView = nil;
        }
        popReviewView = [[PopReviewView alloc]initInSuperView:self.view commentCount:[restaurant.commentCardCount intValue] commentArray:restaurant.commentCardList];
        popReviewView.delegate = self;
        
        if(restaurant.commentCardList.count>10){
            [self popReviewView:popReviewView viewPreviousCommentsTapped:nil lastComment:[[IMGDishComment alloc] init] ];
            [popReviewView showPopReviewView:YES];
        }else{
            [popReviewView showPopReviewView:NO];
        }
        
    }
    
}
-(void)refreshLikeAndCommentLabel{
    float start = LEFTLEFTSET;
    float top = reviewSummarizeLabel.endPointY;
    if (!reviewSummarizeLabel) {
        top = timeLabel.endPointY;
    }
    if (imgType ==TypeForIMGReview) {
        if ([review.likeCount intValue]==0) {
            likeCardLabel.hidden = YES;
            currentPoint = timeLabel.endPointY;
        }
        if ([review.commentCount intValue]==0) {
            commentCardLabel.hidden = YES;
            currentPoint = timeLabel.endPointY;
        }
        if (reviewSummarizeLabel) {
            currentPoint = reviewSummarizeLabel.endPointY;
        }
        if([review.likeCount intValue]>0){
            NSString *likeString=[NSString stringWithFormat:@"%@ %@",review.likeCount,[review.likeCount intValue]>1?L(@"Likes"):L(@"Like")];
            [likeCardLabel setText:likeString];
            likeCardLabel.frame=CGRectMake(start, top, likeCardLabel.expectedWidth,20);
            start+=likeCardLabel.expectedWidth+15;
            currentPoint = likeCardLabel.endPointY;
            [likeCardLabel setHidden:NO];
        }
        if([review.commentCount intValue]>0){
            NSString *commentString=[NSString stringWithFormat:@"%@ %@",review.commentCount,[review.commentCount intValue]>1?L(@"Comments"):L(@"Comment")];
            [commentCardLabel setText:commentString];
            commentCardLabel.frame=CGRectMake(start, top, commentCardLabel.expectedWidth,20);
            start+=commentCardLabel.expectedWidth+15;
            currentPoint = commentCardLabel.endPointY;
            
            [commentCardLabel setHidden:NO];
        }
        
    }else{
        if ([restaurant.likeCardCount intValue]==0) {
            likeCardLabel.hidden = YES;
            currentPoint = timeLabel.endPointY;
        }
        if ([restaurant.commentCardCount intValue]==0) {
            commentCardLabel.hidden = YES;
            currentPoint = timeLabel.endPointY;
        }
        if (reviewSummarizeLabel) {
            currentPoint = reviewSummarizeLabel.endPointY;
        }
        if([restaurant.likeCardCount intValue]>0){
            NSString *likeString=[NSString stringWithFormat:@"%@ %@",restaurant.likeCardCount,[restaurant.likeCardCount intValue]>1?L(@"Likes"):L(@"Like")];
            [likeCardLabel setText:likeString];
            likeCardLabel.frame=CGRectMake(start, top, likeCardLabel.expectedWidth,20);
            start+=likeCardLabel.expectedWidth+15;
            currentPoint = likeCardLabel.endPointY;
            [likeCardLabel setHidden:NO];
        }
        if([restaurant.commentCardCount intValue]>0){
            NSString *commentString=[NSString stringWithFormat:@"%@ %@",restaurant.commentCardCount,[restaurant.commentCardCount intValue]>1?L(@"Comments"):L(@"Comment")];
            [commentCardLabel setText:commentString];
            commentCardLabel.frame=CGRectMake(start, top, commentCardLabel.expectedWidth,20);
            start+=commentCardLabel.expectedWidth+15;
            currentPoint = commentCardLabel.endPointY;
            
            [commentCardLabel setHidden:NO];
        }
        
        
    }
}
- (void)gotoRestaurantWithRestaurantId:(NSNumber *)restaurantId
{
    DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:restaurantId];
    dvc.amplitudeType = @"Card detail page";
    
    [self.navigationController pushViewController:dvc animated:YES];
}
- (void)attributedLabel:(TYAttributedLabel *)attributedLabel textStorageClicked:(id<TYTextStorageProtocol>)TextRun atPoint:(CGPoint)point{
    
    
    if ([TextRun isKindOfClass:[TYLinkTextStorage class]]) {
        
        NSString *linkStr = ((TYLinkTextStorage*)TextRun).linkData;
        
        if ([linkStr hasPrefix:@"user"]) {
            switch (imgType) {
                case TypeForIMGReview:
                {
                    IMGUser *user_ = [[IMGUser alloc] init];
                    user_.userId = [NSNumber numberWithInt:[review.userId intValue] ];
                    user_.userName = review.fullName;
                    user_.token = @"";
                    user_.avatar = review.avatar;
                    [self gotoOtherProfileWithUser:user_];
                }
                    break;
                 case TypeForIMGDish:
                {
                    IMGUser *user_ = [[IMGUser alloc] init];
                    user_.userId = user.userId;
                    user_.userName = user.userName;
                    user_.token = user.token;
                    user_.avatar = user.avatar;
                    [self gotoOtherProfileWithUser:user_];
                }
                    
                default:
                    break;
            }

            
        }else {
            [self gotoRestaurantWithRestaurantId:restaurant.restaurantId];
        }
    }
    
}
- (void)gotoOtherProfileWithUser:(IMGUser *)user_
{
    IMGUser *currentUser = [IMGUser currentUser];
    
    OtherProfileViewController *opvc = [[OtherProfileViewController alloc] init];
    opvc.amplitude = @"Card Detail Page";
    if ([currentUser.userId intValue]==[user_.userId intValue])
    {
        opvc.isOtherUser = NO;
    }
    else
    {
        opvc.isOtherUser = YES;
    }
    opvc.otherUser = user_;
    [self.navigationController pushViewController:opvc animated:YES];
}

@end
