//
//  JournalSearchViewController.h
//  Qraved
//
//  Created by Lucky on 15/7/30.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseChildViewController.h"
#import "IMGJournalCategory.h"
#import "PPLabel.h"
#import "JournalListTableViewCell.h"

@interface JournalSearchViewController : BaseChildViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,PPLabelDelegate,JournalListTableViewCellDelegate>

@property (nonatomic)IMGJournalCategory *category;
@property (nonatomic)       NSMutableArray *categoryDataArrM;


@end
