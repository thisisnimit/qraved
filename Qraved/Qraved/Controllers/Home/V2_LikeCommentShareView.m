//
//  V2_LikeCommentShareView.m
//  Qraved
//
//  Created by harry on 2017/6/27.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_LikeCommentShareView.h"
#import "UILikeButton.h"
#import "UICommentButton.h"
#import "UIShareButton.h"
@implementation V2_LikeCommentShareView
{

    UIView *likeCommentShareButtonView;
    
    UILikeButton *likeButton;
    UICommentButton *commentButton;
    UIShareButton *shareButton;
    BOOL liked;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        likeCommentShareButtonView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 50)];
        [self addSubview:likeCommentShareButtonView];
        
    }
    return self;
}

- (void)setCommentCount:(int)commentCount likeCount:(int)likeCount liked:(BOOL)isLike{
    liked=isLike;
    if (likeButton) {
        [likeButton removeFromSuperview];
    }
    likeButton = [[UILikeButton alloc] initInSuperView:likeCommentShareButtonView liked:isLike likeCount:likeCount isFromPhotoViewer:self.isFromPhotoViewer];
    likeButton.backgroundColor = [UIColor clearColor];
    likeButton.communityDelegate = self;
    
    if (commentButton) {
        [commentButton removeFromSuperview];
    }
    commentButton = [[UICommentButton alloc] initInSuperView:likeCommentShareButtonView commentCount:commentCount isFromPhotoViewer:self.isFromPhotoViewer];
    commentButton.backgroundColor = [UIColor clearColor];
    commentButton.communityDelegate = self;
    
    if (shareButton) {
        [shareButton removeFromSuperview];
    }
    shareButton = [[UIShareButton alloc] initInSuperView:likeCommentShareButtonView isFromPhotoViewer:self.isFromPhotoViewer];
    shareButton.backgroundColor = [UIColor clearColor];
    shareButton.communityDelegate = self;
    
    [self setupAutoHeightWithBottomView:likeCommentShareButtonView bottomMargin:0];
    
}
- (void)communityButtonTapped:(UIButton *)button{
    if(button.tag==0){
        IMGUser *currentUser = [IMGUser currentUser];
        if (currentUser.userId == nil || currentUser.token == nil){
            [[AppDelegate ShareApp] goToLoginControllerByDelegate:self dictionary:@{@"likeCommentShareButton":button}];
            return;
        }
        [self.delegate likeCommentShareView:self likeButtonTapped:button];
    }else if(button.tag==1){
        
        [self.delegate likeCommentShareView:self commentButtonTapped:button];
    }else if(button.tag==2){
        [self.delegate likeCommentShareView:self shareButtonTapped:button];
    }
}

-(void)updateLikedStatus{
    liked=!liked;
    [likeButton updateLikedStatus];
}


@end
