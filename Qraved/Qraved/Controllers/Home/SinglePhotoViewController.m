//
//  SinglePhotoViewController.m
//  Qraved
//
//  Created by System Administrator on 3/9/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "SinglePhotoViewController.h"
#import "SinglePhotoView.h"
#import "IMGDish.h"

@interface SinglePhotoViewController ()

@end

@implementation SinglePhotoViewController

-(instancetype)initWithDish:(IMGDish*)dish{
	if(self=[super init]){
		self.view.backgroundColor=[UIColor whiteColor];
		self.navigationItem.hidesBackButton=YES;
		self.navigationController.view.backgroundColor=[UIColor blackColor];
	    SinglePhotoView *mainView=[[SinglePhotoView alloc] initWithFrame:CGRectMake(0,0,DeviceWidth,DeviceHeight-44) dish:dish];
	    [self.view addSubview:mainView];
	}
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}


@end
