//
//  V2_JournalListViewController.m
//  Qraved
//
//  Created by harry on 2017/7/5.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_JournalListViewController.h"
#import "JournalCategoryViewController.h"
#import "JournalDetailViewController.h"
#import "WebViewController.h"
#import "JournalHandler.h"
#import "SearchUtil.h"
#import "V2_JournalCell.h"
#import "UIScrollView+Helper.h"
#import "HomeService.h"
#import "V2_FoodTagSuggestionView.h"

@interface V2_JournalListViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    int trendingOffset;
    int latestOffset;
    BOOL isTrending;
    int trendingMax;
    int latestMax;
    NSMutableArray *categoryArray;

    NSMutableArray *trendingArray;
    NSMutableArray *latestArray;
    NSMutableArray *journalArray;
    NSMutableArray *foodTagArray;
    
    UICollectionView *journalCollectionView;
    UICollectionView *latestCollectionView;
    NSNumber *trendingCategoryId;
    NSNumber *latestCategoryId;
    IMGMediaComment *trendingJournal;
    IMGMediaComment *latestJournal;
    UISegmentedControl *segControl;
    UIButton *btnFilter;
    
    V2_FoodTagSuggestionView *foodTagView;
    
    DiscoverCategoriesModel *foodModel;
    BOOL isUpdateFoodTag;
    IMGShimmerView *shimmerView;
}
@end

@implementation V2_JournalListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadNavButton];
    [self loadData];
    [self requestData];
    [self loadMainUI];
    
    [IMGAmplitudeUtil trackJournalListWithName:@"RC - View Journal list" andSorting:@"Trending" andFilter:nil andOrigin:@"Homepage"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCity) name:CITY_SELECT_NOTIFICATION object:nil];
}

- (void)refreshCity{
    if (isTrending) {
        trendingOffset = 0;
        trendingMax = 11;
       // [trendingArray removeAllObjects];
    }else{
        latestOffset = 0;
        latestMax = 11;
        //[latestArray removeAllObjects];
    }
    
    [self getJournalList];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    btnFilter.sd_layout
    .bottomSpaceToView(self.view,self.isHomeSlide?5:5+IMG_TabbarHeight)
    .widthIs(104)
    .centerXEqualToView(self.view)
    .heightIs(40);
    
    //[[[UIApplication sharedApplication] keyWindow] addSubview:btnFilter];
    [AppDelegate ShareApp].isSlideJournalHidden = NO;
    [[self rdv_tabBarController] setTabBarHidden:NO animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    //[btnFilter removeFromSuperview];
    [AppDelegate ShareApp].isSlideJournalHidden = YES;
    if ([AppDelegate ShareApp].isSlideHomeHidden
        &&[AppDelegate ShareApp].isSlideJournalHidden
        &&[AppDelegate ShareApp].isSlideDiningGuideHidden) {
        [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    }
}



- (void)loadNavButton{
    [self addBackBtn];
    NSArray *arr = @[@"Trending",@"Latest"];
    segControl = [[UISegmentedControl alloc] initWithItems:arr];
    segControl.tintColor = [UIColor colorWithHexString:@"09BFD3"];
    segControl.selectedSegmentIndex = 0;
    [segControl addTarget:self action:@selector(changeSeg:) forControlEvents:UIControlEventValueChanged];
    [segControl setWidth:95 forSegmentAtIndex:0];
    [segControl setWidth:95 forSegmentAtIndex:1];
    self.navigationItem.titleView = segControl;
}

- (void)changeSeg:(UISegmentedControl *)segment{
    if (segment.selectedSegmentIndex ==0) {
        isTrending = YES;
        [latestCollectionView.mj_footer endRefreshing];
        journalCollectionView.hidden = NO;
        latestCollectionView.hidden = YES;
        if (trendingArray.count)
        {
            [journalCollectionView reloadData];
        }
        else{
            [self getJournalList];
        }
        
    }else{
        isTrending = NO;
        [journalCollectionView.mj_footer endRefreshing];
        latestCollectionView.hidden = NO;
        journalCollectionView.hidden = YES;
        if (latestArray.count) {
            [latestCollectionView reloadData];
        }else{
            
            [self getJournalList];
        }
    }
}

- (void)loadData{
    isUpdateFoodTag = YES;
    trendingOffset = 0;
    latestOffset = 0;
    trendingMax = 11;
    latestMax = 11;
    isTrending = YES;
    trendingArray = [NSMutableArray array];
    latestArray = [NSMutableArray array];
    journalArray = [NSMutableArray array];
    categoryArray = [NSMutableArray array];
    foodTagArray = [NSMutableArray array];
}

- (void)requestData{
    [self getFoodTag];
    [self getCategory];
    [self getJournalList];
}

- (void)getFoodTag{
    [HomeService getFoodTag:nil andBlock:^(id locationArr) {
        [foodTagArray addObjectsFromArray:locationArr];
        
        [journalCollectionView reloadData];
    } andError:^(NSError *error) {
        
    }];
}

- (void)getCategory{
    [JournalHandler getJournalCategoryWithBlock:^(NSArray *dataArray) {
        [categoryArray addObjectsFromArray:dataArray];
    }];

}
- (void)getTrendingJournal{
     NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValuesForKeysWithDictionary:@{@"sort":self.isHomeSlide?@"createTime":@"viewCount",@"order":@"desc",@"offset":[NSNumber numberWithInt:trendingOffset],@"max":[NSNumber numberWithInt:trendingMax]}];
    
    if (self.categoryId != nil) {
        [params setValue:self.categoryId forKey:@"category"];
    }
    
    if ([trendingCategoryId intValue]) {
        [params setObject:trendingCategoryId forKey:@"category"];
    }
    [params setValue:[NSNumber numberWithInt:1] forKey:@"needlog"];
    
    if (foodModel != nil) {
        [params setValue:foodModel.myID forKey:@"foodtagId"];
    }
    
    [JournalHandler getJournalListWithParams:params andBlock:^(NSArray *dataArray, int totalCount) {
        
        if (trendingOffset==0) {
            [trendingArray removeAllObjects];
        }
        [trendingArray addObjectsFromArray:dataArray];
        if (trendingArray.count < totalCount) {
            journalCollectionView.mj_footer.hidden = NO;
        }else{
            journalCollectionView.mj_footer.hidden = YES;
        }
        if (trendingMax==11 && dataArray.count>0) {
            btnFilter.hidden = NO;
            [shimmerView removeFromSuperview];
            trendingJournal = [trendingArray firstObject];
            [trendingArray removeObjectAtIndex:0];
        }
        if (isTrending) {
            [journalCollectionView reloadData];
        }
        
        [journalCollectionView.mj_footer endRefreshing];
        
    } failure:^(NSError *error) {
        [journalCollectionView.mj_header endRefreshing];
        [journalCollectionView.mj_footer endRefreshing];
    }];
    
}

- (void)getLatestJournal{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValuesForKeysWithDictionary:@{@"sort":@"createTime",@"order":@"desc",@"offset":[NSNumber numberWithInt:latestOffset],@"max":[NSNumber numberWithInt:latestMax]}];
    if (self.categoryId != nil) {
        [params setValue:self.categoryId forKey:@"category"];
    }
    
    if ([latestCategoryId intValue]) {
        [params setObject:latestCategoryId forKey:@"category"];
    }
    [params setValue:[NSNumber numberWithInt:1] forKey:@"needlog"];
    
    if (foodModel != nil) {
        [params setValue:foodModel.myID forKey:@"foodtagId"];
    }
    
    [JournalHandler getJournalListWithParams:params andBlock:^(NSArray *dataArray, int totalCount) {
        
        if (latestOffset==0) {
            [latestArray removeAllObjects];
        }
        [latestArray addObjectsFromArray:dataArray];
        if (latestArray.count < totalCount) {
            latestCollectionView.mj_footer.hidden = NO;
        }else{
            latestCollectionView.mj_footer.hidden = YES;
        }
        if (latestMax==11 && dataArray.count>0) {
            latestJournal = [latestArray firstObject];
            [latestArray removeObjectAtIndex:0];
        }
        if (!isTrending) {
            [latestCollectionView reloadData];
        }
        
        
        [latestCollectionView.mj_footer endRefreshing];
        
    } failure:^(NSError *error) {
        [latestCollectionView.mj_header endRefreshing];
        [latestCollectionView.mj_footer endRefreshing];
    }];
}

- (void)getJournalList{
    [self getTrendingJournal];
    [self getLatestJournal];
}

- (void)loadMainUI{
    self.view.backgroundColor = [UIColor whiteColor];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 0);
    layout.itemSize = CGSizeMake((DeviceWidth-15)/2, 190);
    
    journalCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+20-IMG_StatusBarAndNavigationBarHeight-IMG_TabbarHeight) collectionViewLayout:layout];
    journalCollectionView.backgroundColor = [UIColor whiteColor];
    journalCollectionView.delegate = self;
    journalCollectionView.dataSource = self;
    [self.view addSubview:journalCollectionView];
    
    latestCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+20-IMG_StatusBarAndNavigationBarHeight-IMG_TabbarHeight) collectionViewLayout:layout];
    latestCollectionView.backgroundColor = [UIColor whiteColor];
    latestCollectionView.delegate = self;
    latestCollectionView.dataSource = self;
    latestCollectionView.hidden = YES;
    [self.view addSubview:latestCollectionView];
    
    
    latestCollectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadLatestMore)];
    latestCollectionView.mj_footer.hidden = YES;
    //latestCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshJournalList)];
    [latestCollectionView registerClass:[V2_JournalCell class] forCellWithReuseIdentifier:@"journalCell"];
    [latestCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"journalHeader"];
    
    
    journalCollectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
    journalCollectionView.mj_footer.hidden = YES;
    
   // journalCollectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshJournalList)];
    
    [journalCollectionView registerClass:[V2_JournalCell class] forCellWithReuseIdentifier:@"journalCell"];
    [journalCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"journalHeader"];
    
    btnFilter = [UIButton buttonWithType:UIButtonTypeCustom];
//    btnFilter.frame = CGRectMake((DeviceWidth-104)/2, DeviceHeight+20-49-45-64-50, 104, 40);
    btnFilter.hidden = YES;
    [btnFilter setImage:[UIImage imageNamed:@"ic_filter"] forState:UIControlStateNormal];
    [btnFilter addTarget:self action:@selector(filterSort:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnFilter];
    
    btnFilter.sd_layout
    .bottomSpaceToView(self.view,self.isHomeSlide?5:5+IMG_TabbarHeight)
    .widthIs(104)
    .centerXEqualToView(self.view)
    .heightIs(40);
    
    shimmerView = [[IMGShimmerView alloc] initWithFrame:self.view.bounds];
    [shimmerView createJournalListShimmerView];
    [self.view addSubview:shimmerView];
}

- (void)refreshJournalList{
    if (isTrending) {
        trendingOffset = 0;
        trendingMax = 11;
        //[trendingArray removeAllObjects];
    }else{
        latestOffset = 0;
        latestMax = 11;
       // [latestArray removeAllObjects];
    }
    
    [self getJournalList];
}

- (void)loadMore{
    if (isTrending) {
        trendingOffset+=trendingMax;
        trendingMax = 10;
        [self getJournalList];
    }
}

-(void)loadLatestMore{
    latestOffset+=latestMax;
    latestMax = 10;
    [self getJournalList];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (isTrending) {
        return trendingArray.count;
    }else{
        return latestArray.count;
    }
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    V2_JournalCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"journalCell" forIndexPath:indexPath];
    IMGMediaComment *mediaComment;
    if (isTrending) {
        if (trendingArray.count>0) {
            mediaComment = [trendingArray objectAtIndex:indexPath.row];
        }
        
    }else{
        if (latestArray.count>0) {
            mediaComment = [latestArray objectAtIndex:indexPath.row];
        }
        
    }
    
    cell.mediaComment = mediaComment;
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    UICollectionReusableView *reusableView = nil;
    if (kind == UICollectionElementKindSectionHeader){
        UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"journalHeader" forIndexPath:indexPath];
        for (UIView *view in header.subviews) {
            [view removeFromSuperview];
        }
        IMGMediaComment *headerJournal;
        if (isTrending) {
            headerJournal = trendingJournal;
        }else{
            headerJournal = latestJournal;
        }
        if (headerJournal ==nil) {
            return nil;
        }
        if (isUpdateFoodTag) {
            foodTagView = [[V2_FoodTagSuggestionView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 0)];
            if (foodTagArray.count>0) {
                foodTagView.frame = CGRectMake(0, 0, DeviceWidth, 48);
            }else{
                foodTagView.frame = CGRectMake(0, 5, DeviceWidth, 0);
            }
            foodTagView.foodTagArr = foodTagArray;
            foodTagView.selectModel = foodModel;

            
            __weak typeof(self) weakSelf = self;
            foodTagView.selectFoodTag = ^(DiscoverCategoriesModel *model, NSInteger index) {
                [weakSelf refreshJournalWithFoodTag:model];
                
            };
            [header addSubview:foodTagView];
        }else{
            [header addSubview:foodTagView];
        }
        
        
        UIImageView *headImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, foodTagView.endPointY+10, DeviceWidth-30, DeviceWidth*.35)];
        headImageView.contentMode = UIViewContentModeScaleAspectFill;
        headImageView.clipsToBounds = YES;
        [header addSubview:headImageView];
        if ([headerJournal.journalImageUrl hasPrefix:@"http://"]||[headerJournal.journalImageUrl hasPrefix:@"https://"])
        {
            UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-30, (DeviceWidth-30)*.35)];
            [headImageView sd_setImageWithURL:[NSURL URLWithString:headerJournal.journalImageUrl] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            }];
        }
        else
        {
            NSString *urlStr = [headerJournal.journalImageUrl returnFullImageUrlWithWidth:DeviceWidth-30];
            UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-30, (DeviceWidth-30)*.35)];
            
            [headImageView sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            }];
        }
        
        UIImageView *videoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(headImageView.bounds.size.width-42, 15, 27, 19)];
        videoImageView.image = [UIImage imageNamed:@"ic_journal_video.png"];
        [headImageView addSubview:videoImageView];
        if ([headerJournal.hasVideo isEqual:@1]) {
            videoImageView.hidden = NO;
        }else{
            videoImageView.hidden = YES;
        }

        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, headImageView.endPointY+5, DeviceWidth-30, 50)];
        lblTitle.font = [UIFont systemFontOfSize:17];
        lblTitle.textColor = [UIColor color333333];
        lblTitle.text = headerJournal.journalTitle;
        lblTitle.numberOfLines = 3;
        [header addSubview:lblTitle];
        
        CGSize size = [headerJournal.journalTitle sizeWithFont:[UIFont systemFontOfSize:17] constrainedToSize:CGSizeMake(DeviceWidth-30, 100) lineBreakMode:NSLineBreakByWordWrapping];
        lblTitle.frame = CGRectMake(15, headImageView.endPointY+5, DeviceWidth-30, size.height);
       
        header.backgroundColor = [UIColor whiteColor];
        
        UITapGestureRecognizer *headerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(journalHeaderTapped:)];
        [header addGestureRecognizer:headerTap];
        
        reusableView = header;
    }
    return reusableView;
}

- (void)refreshJournalWithFoodTag:(DiscoverCategoriesModel *)model{
    if ([foodModel.myID isEqual:model.myID] && [foodModel.name isEqual:model.name]) {
        foodModel = nil;
        
    }else{
        foodModel = model;
        foodTagView.selectModel = model;
    }
    isUpdateFoodTag = NO;
    if (isTrending) {
        trendingOffset = 0;
        trendingMax = 11;
    }else{
        latestOffset = 0;
        latestMax = 11;
    }
    
    [self getJournalList];
}

- (void)journalHeaderTapped:(UITapGestureRecognizer *)tap{
    
    IMGMediaComment *journal;
    if (isTrending) {
        journal = trendingJournal;
    }else{
        journal = latestJournal;
    }
    
    [IMGAmplitudeUtil trackJDPWithName:@"RC - View Journal detail" andJournalId:journal.mediaCommentId andOrigin:@"JLP" andChannel:nil andLocation:nil];
    
    if ([journal.imported intValue]== 1)
    {
        tap.enabled =NO;
        [JournalHandler getJournalDetailWithJournalArticleId:journal.mediaCommentId andBlock:^(NSString *content,NSString *webSite) {
            journal.contents = content;
            WebViewController *wvc = [[WebViewController alloc] init];
            wvc.content = journal.contents;
            wvc.journal = journal;
            wvc.webSite = webSite;
            wvc.title = journal.title;
            wvc.fromDetail = NO;
            tap.enabled =YES;
            [self.navigationController pushViewController:wvc animated:YES];
        }];
        
    }
    else
    {
        
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        [param setValue:[self getPageIdByAmplitudeType] forKey:@"Location"];
        [[Amplitude instance] logEvent:@"CL - Journal Category" withEventProperties:param];
        
        JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
        jdvc.amplitudeType = @"Article card on Article list";
        jdvc.journal = journal;
        jdvc.categoryDataArrM = [NSMutableArray arrayWithArray:categoryArray];
        [self.navigationController pushViewController:jdvc animated:YES];
    }

}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    IMGMediaComment *journal;
    if (isTrending) {
        journal = trendingJournal;
    }else{
        journal = latestJournal;
    }
    CGSize size = [journal.journalTitle sizeWithFont:[UIFont systemFontOfSize:17] constrainedToSize:CGSizeMake(DeviceWidth-30, 100) lineBreakMode:NSLineBreakByWordWrapping];

    if (journal==nil) {
        return CGSizeMake(DeviceWidth, 0);
    }
    if (foodTagArray.count>0) {
        return CGSizeMake(DeviceWidth, DeviceWidth*.35+size.height + 70);
    }else{
        return CGSizeMake(DeviceWidth, DeviceWidth*.35+size.height + 35);
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    IMGMediaComment *journal;
    if (isTrending) {
        journal = [trendingArray objectAtIndex:indexPath.row];
    }else{
        journal = [latestArray objectAtIndex:indexPath.row];
    }
    
    [IMGAmplitudeUtil trackJDPWithName:@"RC - View Journal detail" andJournalId:journal.mediaCommentId andOrigin:@"JLP" andChannel:nil andLocation:nil];
    
    if ([journal.imported intValue]== 1)
    {
        cell.userInteractionEnabled=NO;
        [JournalHandler getJournalDetailWithJournalArticleId:journal.mediaCommentId andBlock:^(NSString *content,NSString *webSite) {
            journal.contents = content;
            WebViewController *wvc = [[WebViewController alloc] init];
            wvc.content = journal.contents;
            wvc.journal = journal;
            wvc.webSite = webSite;
            wvc.title = journal.title;
            wvc.fromDetail = NO;
            cell.userInteractionEnabled=YES;
            [self.navigationController pushViewController:wvc animated:YES];
        }];
        
    }
    else
    {
        
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        [param setValue:[self getPageIdByAmplitudeType] forKey:@"Location"];
        [[Amplitude instance] logEvent:@"CL - Journal Category" withEventProperties:param];
        
        JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
        jdvc.amplitudeType = @"Article card on Article list";
        jdvc.journal = journal;
        jdvc.categoryDataArrM = [NSMutableArray arrayWithArray:categoryArray];
        [self.navigationController pushViewController:jdvc animated:YES];
    }

}

-(NSString*)getPageIdByAmplitudeType{
    NSString *pageId = @"";
    if ([@"Search page" isEqualToString:self.amplitudeType]) {
        pageId = @",Search restaurant result page";
    }
    return [NSString stringWithFormat:@"%@%@",self.amplitudeType,pageId];
}

- (void)filterSort:(UIButton *)btn{
    JournalCategoryViewController *jcvc = [[JournalCategoryViewController alloc] init];
    jcvc.categoryDataArrM = [NSMutableArray arrayWithArray:categoryArray];
    [jcvc setRefreshJournal:^(NSNumber *categoryId) {
        if (isTrending) {
            trendingOffset = 0;
            trendingMax = 11;
            //[trendingArray removeAllObjects];
            trendingCategoryId = categoryId;
        }else{
            latestOffset = 0;
            latestMax = 11;
            //[latestArray removeAllObjects];
            latestCategoryId = categoryId;
        }
        
        [IMGAmplitudeUtil trackJournalListWithName:@"RC - View Journal list" andSorting:isTrending?@"Trending":@"Latest" andFilter:categoryId andOrigin:@"Homepage"];
        
        [self getJournalList];
    }];
    [self.navigationController pushViewController:jcvc animated:YES];
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.isHomeSlide && ![journalCollectionView.mj_footer isRefreshing]) {
        [scrollView scrollNavigationController:[AppDelegate ShareApp].homeNavigationController];
        journalCollectionView.bounces = YES;

        if (scrollView.contentOffset.y>=44) {
            btnFilter.sd_layout
            .bottomSpaceToView(self.view,IMG_TabbarHeight+5)
            .widthIs(104)
            .centerXEqualToView(self.view)
            .heightIs(40);
        }

        
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CITY_SELECT_NOTIFICATION object:nil];
}

@end
