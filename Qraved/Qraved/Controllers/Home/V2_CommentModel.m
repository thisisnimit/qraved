//
//  V2_CommentModel.m
//  Qraved
//
//  Created by harry on 2017/6/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_CommentModel.h"

@implementation V2_CommentModel

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"id"]) {
        self.commentId = value;
    }else if ([key isEqualToString:@"comment"]){
        self.content = value;
    }else if ([key isEqualToString:@"avatar"]){
        self.userImageUrl = value;
    }else if ([key isEqualToString:@"timeCreated"]){
        self.createTime = value;
    }
}

@end
