//
//  HomeSlideViewController.m
//  Qraved
//
//  Created by apple on 16/10/19.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "HomeSlideViewController.h"
#import "CarbonKit.h"
#import "HomeViewController.h"
#import "JournalListViewController.h"
#import "DiningGuideListViewController.h"
#import "MLNavigationController.h"
#import "UIColor+Helper.h"
#import "AppDelegate.h"
#import "SelectCityController.h"
#import "IMGCity.h"
#import "CityHandler.h"
#import "V2_HomeViewController.h"
#import "V2_DiningGuideListViewController.h"
#import "V2_JournalListViewController.h"
#import "V2_CItyViewController.h"
#import "V2_DiscoverListViewController.h"
#import "V2_PreferenceViewController.h"
#import "GoFoodSplashView.h"
#import "BrandViewController.h"
#import "WebServiceV2.h"
#import "DeliveryViewController.h"

@interface HomeSlideViewController ()<CarbonTabSwipeNavigationDelegate,CLLocationManagerDelegate>
{
    locationHandler _locationHandler;
    BOOL isFirstAlertView;
//    BOOL isFirstUpdateLocation;

}
@end

@implementation HomeSlideViewController{
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
    UIView *cityView;
    UILabel *selectedCity;
    UIButton *downBtn;
    UIImage *select;
    UIView* statusBar;
    NSMutableArray *_citysArrM;
    UIView *navView;
    UIButton *btnSearch;
    UIButton *cityButton;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _citysArrM=[[NSMutableArray alloc]init];
    isFirstAlertView=YES;
    
    [AppDelegate ShareApp].locationManager = [CLLocationManager new];
    [AppDelegate ShareApp].locationManager.delegate = self;
    [AppDelegate ShareApp].locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"]||![[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]) {
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:0] forKey:@"latitude"];
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:0] forKey:@"longitude"];
    }

    [[DBManager manager]selectWithSql:@"SELECT * FROM IMGCity;" successBlock:^(FMResultSet *resultSet) {
        while ([resultSet next]) {
            IMGCity  *city=[[IMGCity alloc]init];
            [city setValueWithResultSet:resultSet];
            [_citysArrM addObject:city];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"initRestaurantOfferFromLocal error when get datas:%@",error.description);
    }];

    [self getCurrentLoaction];

    [AppDelegate ShareApp].timer =  [NSTimer scheduledTimerWithTimeInterval:600 target:self selector:@selector(getCurrentLoaction) userInfo:nil repeats:YES];

    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT] == nil || [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] == nil) {
        [[NSUserDefaults standardUserDefaults] setObject:@"Jakarta" forKey:CITY_SELECT];
        [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:CITY_SELECT_ID];
    }

    [self loadNavButton];

    [AppDelegate ShareApp].isSlideHomeHidden = NO;
    [AppDelegate ShareApp].isSlideJournalHidden = YES;
    [AppDelegate ShareApp].isSlideDiningGuideHidden = YES;

    // Do any additional setup after loading the view.
    NSArray *items = @[@"Discover",@"Journal",@"Guide"];
    carbonTabSwipeNavigation=[[CarbonTabSwipeNavigation alloc]initWithItems:items delegate:self];
    carbonTabSwipeNavigation.toolbar.backgroundColor = [UIColor whiteColor];
    [carbonTabSwipeNavigation insertIntoRootViewController:self];
    //[carbonTabSwipeNavigation setTabExtraWidth:-100];
    [carbonTabSwipeNavigation setTabBarHeight:55];
    [carbonTabSwipeNavigation setNormalColor:[UIColor color333333] font:[UIFont boldSystemFontOfSize:14]];
    [carbonTabSwipeNavigation setSelectedColor:[UIColor colorWithHexString:@"DE2029"] font:[UIFont boldSystemFontOfSize:14]];
    [carbonTabSwipeNavigation setIndicatorColor:[UIColor colorWithHexString:@"DE2029"]];
    
    if (![[NSUserDefaults standardUserDefaults] valueForKey:@"checkNearBy"] && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
        [self nearbyCheck];
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"deepLinkUrl"] != nil) {
        [[AppDelegate ShareApp] referrerDeeplink];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"deepLinkUrl"];
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"isOpenInApp"] != nil) {
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"isOpenInApp"];
    }
    
    [WebServiceV2 getGoFoodBanner:nil andBlock:^(NSArray *bannerArr) {
        
        if (bannerArr.count > 0) {
            [[NSUserDefaults standardUserDefaults] setObject:@"delivery" forKey:@"ShowDelivery"];
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"goFoodSplash"] == nil) {
                [self createGoFoodSplashView];
            }
        }else{
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ShowDelivery"];
        }
        
    } andError:^(NSError *error) {
        
    }];
}

- (void)createGoFoodSplashView{
    
    [IMGAmplitudeUtil trackDeliveryWithName:@"RC - View Splash Screen" andJournalId:nil andFoodTagId:nil andLocation:@"Homepage" andOrigin:nil];
    
    GoFoodSplashView *splashView = [[GoFoodSplashView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHEIGHT)];
    splashView.layer.zPosition = 10000;
    __weak typeof(splashView) weakSplashView = splashView;
    splashView.dismissSplash = ^{
        
        [IMGAmplitudeUtil trackDeliveryWithName:@"CL - Close Splash Screen" andJournalId:nil andFoodTagId:nil andLocation:@"Homepage" andOrigin:nil];
        
//        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"goFoodSplash"];
        [weakSplashView removeFromSuperview];
    };
    
    splashView.reminderTapped = ^{
        
        [IMGAmplitudeUtil trackDeliveryWithName:@"CL - Hide Splash Screen" andJournalId:nil andFoodTagId:nil andLocation:@"Homepage" andOrigin:nil];
        
        [weakSplashView removeFromSuperview];
        NSDate *date = [NSDate date];
        [[NSUserDefaults standardUserDefaults] setObject:date forKey:@"goFoodSplash"];
    };
    __weak typeof(self) weakSelf = self;
    splashView.turntoDelivery = ^{

        [IMGAmplitudeUtil trackGoFoodWithName:@"CL - Splash Screen Area" andJournalId:nil andRestaurantId:nil andPhotoId:nil andLocation:@"Homepage" andOrigin:nil andOrder:nil];
        
        [IMGAmplitudeUtil trackDeliveryWithName:@"RC - View Delivery Page" andJournalId:nil andFoodTagId:nil andLocation:nil andOrigin:@"Splash Screen"];
        
        [weakSplashView removeFromSuperview];
        DeliveryViewController *deliveryViewController = [[DeliveryViewController alloc] init];
        [weakSelf.navigationController pushViewController:deliveryViewController animated:YES];
    };
    
    [[UIApplication sharedApplication].keyWindow addSubview:splashView];
}

- (void)loadNavButton{
    
//    navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 64)];
//    navView.backgroundColor = [UIColor clearColor];
//    //    navView.alpha = 0;
//    [self.view addSubview:navView];
    
//    lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 63.5, DeviceWidth, 0.5)];
//    lineView.backgroundColor = [UIColor clearColor];
//    [navView addSubview:lineView];
    
    cityButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT] == nil || [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] == nil) {
    }else{
        [cityButton setTitle:[[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT] capitalizedString] forState:UIControlStateNormal];
    }

    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"currentCityId"] != nil && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied){
        [cityButton setTitle:@"Nearby" forState:UIControlStateNormal];
    }

    [cityButton setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    [cityButton setImage:[UIImage imageNamed:@"ic_city_down"] forState:UIControlStateNormal];
    cityButton.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    [cityButton addTarget:self action:@selector(doSelectCity:) forControlEvents:UIControlEventTouchUpInside];
    cityButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 15);
    [cityButton.layer setAffineTransform:CGAffineTransformMakeScale(-1, 1)];
    [cityButton.titleLabel.layer setAffineTransform:CGAffineTransformMakeScale(-1, 1)];
    
    CGSize titleSize = [cityButton.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17]}];
    
    cityButton.height = 35;
    cityButton.width= titleSize.width + 50;
    
    cityButton.layer.masksToBounds = YES;
    cityButton.layer.cornerRadius = 18;
    cityButton.backgroundColor = [UIColor colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1];;
    
    cityButton.x = (self.view.width - cityButton.width)/2;
    cityButton.y = 4;
    
//    cityButton.frame = CGRectMake((self.view.width - titleSize.width) / 2, 27, titleSize.width, titleSize.height);
    


   // [navView addSubview:cityButton];
    
   // self.navigationItem.titleView = cityButton;
    
    btnSearch = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSearch.frame = CGRectMake(DeviceWidth-65, 20, 50, 44);
    btnSearch.imageEdgeInsets = UIEdgeInsetsMake(0, 30, 0, 0);
    [btnSearch setImage:[UIImage imageNamed:@"ic_home_search_black"] forState:UIControlStateNormal];
    [btnSearch addTarget:self action:@selector(searchClick) forControlEvents:UIControlEventTouchUpInside];
    //[navView addSubview:btnSearch];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnSearch];
    
    UIButton *btnQraved = [UIButton buttonWithType:UIButtonTypeCustom];
    btnQraved.frame = CGRectMake(0, 0, 60, 44);
    [btnQraved setImage:[UIImage imageNamed:@"ic_home_qraved_logo"] forState:UIControlStateNormal];
    btnQraved.imageEdgeInsets = UIEdgeInsetsMake(7, 0, 0, 30);
    [btnQraved addTarget:self action:@selector(qravedClick) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnQraved];
    
}

- (void)searchClick{
    V2_DiscoverListViewController *DiscoverListV = [[V2_DiscoverListViewController alloc] init];
    DiscoverListV.cityName = cityButton.titleLabel.text;
    [self.navigationController pushViewController:DiscoverListV animated:YES];
}

- (void)qravedClick{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setValue:@"Homepage" forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL - Q Icon" withEventProperties:param];
    V2_PreferenceViewController *prefeVC = [[V2_PreferenceViewController alloc] init];
    prefeVC.isFromProfile = YES;
    prefeVC.isFromHome = YES;
    [self.navigationController pushViewController:prefeVC animated:YES];
}

- (void)doSelectCity:(UIButton *)btn{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setValue:cityButton.titleLabel.text forKey:@"City Selection"];
    [param setValue:@"Homepage Header" forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL – City Selection" withEventProperties:param];
    
    V2_CItyViewController *cityViewController = [[V2_CItyViewController alloc] init];
    
    cityViewController.citySelected = ^(IMGCity *city) {
        
        if ([IMGUser currentUser].userId) {
            [CityHandler changeCity:city.cityId withUser:[IMGUser currentUser].userId block:nil];
        }
        [[NSUserDefaults standardUserDefaults] setObject:city.cityId forKey:CITY_SELECT_ID];
        [[NSUserDefaults standardUserDefaults] setObject:city.name forKey:CITY_SELECT];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"currentCityId"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"currentCityId"];
        [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"checkNearBy"];
        
        [cityButton setTitle:[city.name capitalizedString] forState:UIControlStateNormal];
        CGSize titleSize = [btn.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17]}];
        
        cityButton.height = 35;
        cityButton.width= titleSize.width + 50;
        
        cityButton.x = (self.view.width - cityButton.width)/2;
        
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        [param setValue:city.name forKey:@"City Selection"];
        [param setValue:@"Homepage Header" forKey:@"Location"];
        [[Amplitude instance] logEvent:@"CL – City Selection" withEventProperties:param];
        //cityButton.y = 7;
        [[NSNotificationCenter defaultCenter] postNotificationName:CITY_SELECT_NOTIFICATION object:nil userInfo:nil];
        
//        [self freshData];
    };
    
    cityViewController.nearbySelected = ^{
//
//        isFirstUpdateLocation = YES;
//        [self getCurrentLoaction];
//        [cityButton setTitle:@"Nearby" forState:UIControlStateNormal];
//        CGSize titleSize = [btn.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17]}];
//
//        cityButton.height = 35;
//        cityButton.width= titleSize.width + 50;
//
//        cityButton.x = (self.view.width - cityButton.width)/2;
        [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"checkNearBy"];
        [self nearbyCheck];
        
        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
        [param setValue:@"Nearby" forKey:@"City Selection"];
        [param setValue:@"Homepage Header" forKey:@"Location"];
        [[Amplitude instance] logEvent:@"CL – City Selection" withEventProperties:param];
        
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        
        if ([userDefault objectForKey:@"currentCityId"] == nil) {
            [[NSUserDefaults standardUserDefaults] setObject:@"Jakarta" forKey:CITY_SELECT];
            [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:CITY_SELECT_ID];
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:CITY_SELECT_NOTIFICATION object:nil userInfo:nil];
        
    };
    
    cityViewController.cityArray = _citysArrM;
    [self.navigationController presentViewController:cityViewController animated:YES completion:nil];
}


-(void)getCurrentLoaction{
    
    NSLog(@"Get current location");
    [self getCurrentLocationWithCompletionHandler:^(CLLocation *currentLocation) {
        NSNumber *latitude= @(currentLocation.coordinate.latitude);
        NSNumber *longitude= @(currentLocation.coordinate.longitude);
        [[NSUserDefaults standardUserDefaults] setObject:latitude forKey:@"latitude"];
        [[NSUserDefaults standardUserDefaults] setObject:longitude forKey:@"longitude"];
    }];
    
    
}
-(void)setIsBocomeActive:(BOOL)isBocomeActive
{
    
    [[AppDelegate ShareApp].timer setFireDate:[NSDate distantPast]];
    [self getCurrentLoaction];
    
    
}

- (void)getCurrentLocationWithCompletionHandler:(locationHandler)handler {
    _locationHandler = handler;
    
    if ([[AppDelegate ShareApp].locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [[AppDelegate ShareApp].locationManager requestWhenInUseAuthorization];
    }
    
    [[AppDelegate ShareApp].locationManager startUpdatingLocation];
}

-(void)setCurrentTabIndex:(NSUInteger)index{
    [carbonTabSwipeNavigation setCurrentTabIndex:index];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //cityView.hidden=NO;
    isFirstAlertView=YES;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"deepLinkDic"] != nil) {
        [[AppDelegate ShareApp] deepLinkhandle];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"deepLinkDic"];
    }
    
    [self.navigationController.navigationBar addSubview:cityButton];
    [self.navigationController.navigationBar layoutIfNeeded];
//    selectedCity.text=[[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT] capitalizedString];
//    selectedCity.frame=CGRectMake(0,0,selectedCity.expectedWidth,20);
//    downBtn.frame = CGRectMake(selectedCity.endPointX+2,selectedCity.startPointY+8, select.size.width, select.size.height);
//    cityView.frame=CGRectMake(DeviceWidth/2-selectedCity.frame.size.width/2-1-downBtn.frame.size.width/2,16,selectedCity.frame.size.width+2+downBtn.frame.size.width,22);
    
    
    statusBar =[[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, IMG_StatusBarHeight)];
    statusBar.backgroundColor=[UIColor whiteColor];
    statusBar.layer.zPosition = 1000;
    [[AppDelegate ShareApp].window addSubview:statusBar];
    
    if ([AppDelegate ShareApp].homeNavigationController.currFrameY != 0) {
        CGRect frame = [AppDelegate ShareApp].homeNavigationController.view.frame;
        frame.origin.y = [AppDelegate ShareApp].homeNavigationController.currFrameY;
        frame.size.height = [AppDelegate ShareApp].homeNavigationController.currFrameHeight;
        [AppDelegate ShareApp].homeNavigationController.view.frame = frame;
    }
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
   
}
-(void)viewWillDisappear:(BOOL)animated{

    [super viewWillDisappear:animated];
   // cityView.hidden=YES;
    [cityButton removeFromSuperview];
    [statusBar removeFromSuperview];
    statusBar = nil;
    if ([AppDelegate ShareApp].homeNavigationController.currFrameY != 0) {
        CGRect frame = [AppDelegate ShareApp].homeNavigationController.view.frame;
        frame.origin.y = [[AppDelegate ShareApp].homeNavigationController getNavigationControllerInitY];
        frame.size.height = [[AppDelegate ShareApp].homeNavigationController getNavigationControllerInitHeight];
        [AppDelegate ShareApp].homeNavigationController.view.frame = frame;
    }
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:nil];
    
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
//-(void)selectButtonTapped:(id)sender {
//    
//    SelectCityController *selectCityController = [[SelectCityController alloc] initWithSelectedIndex:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]];
//    selectCityController.isNotHiddenTitle = YES;
//    selectCityController.amplitudeType = @"Homepage";
//    [self.navigationController pushViewController:selectCityController animated:YES];
//}

- (UIViewController *)carbonTabSwipeNavigation:(CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                         viewControllerAtIndex:(NSUInteger)index {
    
    if (index == 0) {
        
        V2_HomeViewController *homeVC = [[V2_HomeViewController alloc] init];
//        homeVC.refreshLocation = ^{
//            if ([CLLocationManager authorizationStatus] !=kCLAuthorizationStatusDenied){
//                    [self getCurrentLoaction];
//            }else{
//                [cityButton setTitle:[[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT] capitalizedString] forState:UIControlStateNormal];
//            }
//
//        };
        return homeVC;
    }else if(index == 1) {
        V2_JournalListViewController *journalVC = [[V2_JournalListViewController alloc] init];
        journalVC.isHomeSlide = YES;
        return journalVC;

    }else if (index == 2){
        V2_DiningGuideListViewController *guideVC = [[V2_DiningGuideListViewController alloc] init];
        guideVC.isHomeSlide = YES;
        return guideVC;
    }
    else{
        V2_HomeViewController *homeVC = [[V2_HomeViewController alloc] init];
//        homeVC.refreshLocation = ^{
//            if ([CLLocationManager authorizationStatus] !=kCLAuthorizationStatusDenied){
//                [self getCurrentLoaction];
//            }else{
//                [cityButton setTitle:[[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT] capitalizedString] forState:UIControlStateNormal];
//            }
//        };
        return homeVC;
    }
    
}

- (void)nearbyCheck{
    for (IMGCity *city in _citysArrM) {
        if (city.maxLongitude==nil||[city.maxLongitude isEqual:[NSNull null]]) {
            continue;
        }
        CGFloat maxLatitue=[city.maxLatitude floatValue];
        CGFloat maxLongtitude=[city.maxLongitude floatValue];
        CGFloat minLatitue=[city.minLatitude floatValue];
        CGFloat minLongtitude=[city.minLongitude floatValue];
        //@(-6.1751100000)
        NSNumber *currentLatitude=[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"];
        //@(106.8650395000)
        NSNumber *currentLongtitue=[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"];
        
        if (([currentLatitude floatValue]>=minLatitue&&[currentLatitude floatValue]<=maxLatitue)&&([currentLongtitue floatValue]>=minLongtitude&&[currentLongtitue floatValue]<=maxLongtitude)) {
            
//            isFirstUpdateLocation = NO;
            [cityButton setTitle:@"Nearby" forState:UIControlStateNormal];
            CGSize titleSize = [cityButton.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17]}];
            
            cityButton.height = 35;
            cityButton.width= titleSize.width + 50;
            
            cityButton.x = (self.view.width - cityButton.width)/2;
            [[NSUserDefaults standardUserDefaults] setObject:city.cityId forKey:@"currentCityId"];
            [[NSUserDefaults standardUserDefaults] setObject:city.name forKey:@"currentCityName"];
            [[NSUserDefaults standardUserDefaults] setObject:city.name forKey:CITY_SELECT];
            [[NSUserDefaults standardUserDefaults] setObject:city.cityId forKey:CITY_SELECT_ID];
            if ([IMGUser currentUser].userId) {
                [CityHandler changeCity:city.cityId withUser:[IMGUser currentUser].userId block:nil];
            }
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    [[AppDelegate ShareApp].locationManager stopUpdatingLocation];
    //    CLLocation *current=[[CLLocation alloc] initWithLatitude:-6.2293867 longitude:106.6894296];
    CLLocation *current=locations.lastObject;
    //第二个坐标
    //    CLLocation *before=[[CLLocation alloc] initWithLatitude:-8.4543385 longitude:114.511032];
    NSNumber* latitude=[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"];
    NSNumber* longitude=[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"];
    
    CLLocation *before=[[CLLocation alloc] initWithLatitude:[latitude doubleValue] longitude:[longitude doubleValue]];
    
    // 计算距离
    CLLocationDistance DistanceMeters=[current distanceFromLocation:before];
    if ([latitude floatValue]==0&&[longitude floatValue]==0) {
        DistanceMeters=0;
    }
    
    BOOL isOurCity=YES;
    for (IMGCity *city in _citysArrM) {
        if (city.maxLongitude==nil||[city.maxLongitude isEqual:[NSNull null]]) {
            continue;
        }
        CGFloat maxLatitue=[city.maxLatitude floatValue];
        CGFloat maxLongtitude=[city.maxLongitude floatValue];
        CGFloat minLatitue=[city.minLatitude floatValue];
        CGFloat minLongtitude=[city.minLongitude floatValue];
        
        NSNumber *currentLatitude=@(current.coordinate.latitude);
        NSNumber *currentLongtitue=@(current.coordinate.longitude);
        
//        if (([currentLatitude floatValue]>=minLatitue&&[currentLatitude floatValue]<=maxLatitue)&&([currentLongtitue floatValue]>=minLongtitude&&[currentLongtitue floatValue]<=maxLongtitude)&&isFirstUpdateLocation) {
//
////            isFirstUpdateLocation = NO;
//            [cityButton setTitle:@"Nearby" forState:UIControlStateNormal];
//            CGSize titleSize = [cityButton.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17]}];
//
//            cityButton.height = 35;
//            cityButton.width= titleSize.width + 50;
//
//            cityButton.x = (self.view.width - cityButton.width)/2;
//            if (![city.cityId isEqual:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]]){
//                [[NSUserDefaults standardUserDefaults] setObject:city.cityId forKey:@"currentCityId"];
//                [[NSUserDefaults standardUserDefaults] setObject:city.name forKey:@"currentCityName"];
//                [[NSUserDefaults standardUserDefaults] setObject:city.name forKey:CITY_SELECT];
//                [[NSUserDefaults standardUserDefaults] setObject:city.cityId forKey:CITY_SELECT_ID];
//                //[[NSNotificationCenter defaultCenter] postNotificationName:CITY_SELECT_NOTIFICATION object:nil userInfo:nil];
//            }
//
//        }
        
        if (([currentLatitude floatValue]>=minLatitue&&[currentLatitude floatValue]<=maxLatitue)&&([currentLongtitue floatValue]>=minLongtitude&&[currentLongtitue floatValue]<=maxLongtitude)&&DistanceMeters>50000) {
            NSString *cityName=city.name;
            if ([cityName isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT]]) {
                isOurCity=YES;
            }else{
                isOurCity=NO;
            }
            break;
        }else if (DistanceMeters>50000){
            
            isOurCity=NO;
        }
        
    }
    if (!isOurCity&&isFirstAlertView) {
        UIAlertController *alterView=[UIAlertController alertControllerWithTitle:@"Hi, out of town?" message:@"Please select the city" preferredStyle:UIAlertControllerStyleAlert];
        for (IMGCity *city in _citysArrM) {
            UIAlertAction *action=[UIAlertAction actionWithTitle:[city.name capitalizedString] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                IMGUser *user=[IMGUser currentUser];
                if(user.userId){
                    [CityHandler changeCity:city.cityId withUser:user.userId block:nil];
                }
                
                NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
                [param setValue:city.name forKey:@"City Selection"];
                [param setValue:@"Pop Up Message" forKey:@"Location"];
                [[Amplitude instance] logEvent:@"CL – City Selection" withEventProperties:param];
                [[NSUserDefaults standardUserDefaults] setObject:city.cityId forKey:CITY_SELECT_ID];
                [[NSUserDefaults standardUserDefaults] setObject:city.name forKey:CITY_SELECT];
                [[NSNotificationCenter defaultCenter] postNotificationName:CITY_SELECT_NOTIFICATION object:nil userInfo:nil];
                //citySelectIndex = city.cityId;
                [cityButton setTitle:[city.name capitalizedString] forState:UIControlStateNormal];
                CGSize titleSize = [cityButton.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17]}];
                
                cityButton.height = 35;
                cityButton.width= titleSize.width + 50;
                
                cityButton.x = (self.view.width - cityButton.width)/2;

                
            }];
            [alterView addAction:action];
        }
        isFirstAlertView=NO;
        
        [self presentViewController:alterView animated:YES completion:nil];
        
    }

    _locationHandler(locations.lastObject);
    [AppDelegate ShareApp].lastLocationManager = [AppDelegate ShareApp].locationManager;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"Location manager failed with error: %@", error.localizedDescription);
}


 /*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
