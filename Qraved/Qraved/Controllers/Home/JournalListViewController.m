//
//  JournalListViewController.m
//  Qraved
//
//  Created by Lucky on 15/6/29.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "JournalListViewController.h"
#import "GDataXMLNode.h"
#import "IMGMediaComment.h"
#import "MoreArticleViewController.h"
#import "UIViewController+Helper.h"
#import "IMGMediaComment.h"
#import "UIConstants.h"
#import "UIImage+Resize.h"
#import "UIImageView+WebCache.h"
#import "Label.h"
#import "UIView+Helper.h"
#import <CoreText/CoreText.h>
#import "UIColor+Helper.h"
#import "WebViewController.h"
#import "NSString+Helper.h"
#import "LoadingView.h"
#import "DBManager.h"
#import "UIConstants.h"
#import "JournalListTableViewCell.h"
#import "JournalHandler.h"
#import "JournalDetailViewController.h"
#import "UILabel+Helper.h"
#import "IMGUser.h"
#import "AppDelegate.h"
#import "IMGJournalCategory.h"
#import "JournalSearchViewController.h"
#import "PPLabel.h"
#import "DiscoverListViewController.h"
#import "JournalCategoryViewController.h"
#import "SearchUtil.h"
#import "JournalSuggestViewController.h"
#import "PullTableView.h"
#import "UIScrollView+Helper.h"

#define REFRESH_HEADER_HEIGHT 52.0f

#define REFRESH_LIST_COUNT 10

@interface JournalListViewController ()<PPLabelDelegate,UITextFieldDelegate,PullTableViewDelegate>
{
    UIScrollView *_scrollView;
    BOOL _isLoad;
    
    UIButton *hottextBtn;
    UIButton *newestBtn;
    UIButton *userBtn;
    UIView *headerView;
    //    UITableView *_hottextTabView;
    //    UITableView *_newestTabView;
    
    PullTableView *_tableView;
    NSMutableArray *_dataArrM;
    NSUInteger TrendingPage;
    NSUInteger FreshPage;
    
    NSMutableDictionary *TrendingParams;
    NSMutableDictionary *FreshParams;
    NSMutableDictionary *DefaultParams;
    
    UIImageView *_isBtnImageView;
    
    NSMutableArray *_trendingArrM;
    NSMutableArray *_freshArrM;
    
    UIView *_backgroundView;
    UIScrollView *_searchView;
    NSMutableArray *_categoryDataArrM;
    NSMutableArray *_searchResultArray;
    UITableView *_categoryTableView;
    UILabel *categoryValueLabel;
    UITableView *optionTableView;
    
    NSArray *journalArr;
    
    BOOL _isTrending;
    
    int trendingOffset;
    int freshingOffset;
    int max;
    
    BOOL isHadNextDataTrending;
    BOOL isHadNextDataFresh;
    
    BOOL isStartRefreshTrending;
    BOOL isStartRefreshFresh;
    
    NSTimeInterval beginLoadingData;
    NSTimeInterval finishUI;
    BOOL isViewDidLoad;
    UIView *detailNavgation;
    UIView *topSegment;
    
    UIImageView *lineImage3;
    UIButton *searchBtn;
    UIImageView *arrow1ImageView;
    UIButton *categoryBtn;
    UIImageView *arrow2ImageView;
    BOOL isFreshing;
}
@end

@implementation JournalListViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //[self.navigationItem setTitle:@"Qraved Journal"];
    
//    self.navigationController.navigationBar.hidden = YES;
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token && userBtn)
    {
        [userBtn removeFromSuperview];
    }
    [[Amplitude instance] logEvent:@"RC - View Journal List" withEventProperties:@{}];
    
    [AppDelegate ShareApp].isSlideJournalHidden = NO;
    [[self rdv_tabBarController] setTabBarHidden:NO animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [MixpanelHelper trackOpenJournal:@"Open Journal"];
    self.screenName = @"Journal list page";
    isViewDidLoad = YES;
    if (self.isFromSearch) {
        self.navigationItem.title = @"Journal";
    }
    beginLoadingData = [[NSDate date]timeIntervalSince1970];
    _isTrending = self.isTrending;
    trendingOffset = 0;
    freshingOffset = 0;
    max = 10;
    
    _trendingArrM = [[NSMutableArray alloc] init];
    _freshArrM = [[NSMutableArray alloc] init];
    _categoryDataArrM = [[NSMutableArray alloc] init];
    _searchResultArray = [[NSMutableArray alloc] init];
    
    [JournalHandler getJournalCategoryWithBlock:^(NSArray *dataArray) {
        _categoryDataArrM = [NSMutableArray arrayWithArray:dataArray];
        _searchResultArray = [NSMutableArray arrayWithArray:dataArray];
    }];
    
    TrendingParams=[[NSMutableDictionary alloc] initWithDictionary:@{@"sort":@"viewCount",@"order":@"desc"}];
    FreshParams=[[NSMutableDictionary alloc] initWithDictionary:@{@"sort":@"createTime",@"order":@"desc"}];
    
    DefaultParams=FreshParams;
    
    
    self.navigationController.navigationBarHidden = NO;
    
    [self loadRevealController];
    
    self.view.backgroundColor = [UIColor whiteColor];
    //self.navigationController.title = self.title;
    
    [[LoadingView sharedLoadingView] startLoading];
//    [self setNavigation];

    [self addMainView];
    [self initData];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(clickTabMenuRefreshPage) name:NOTIFICATION_REFRESH_TAB_MENU object:nil];
}
-(void)clickTabMenuRefreshPage{
    if (isFreshing) {
        return;
    }
    _tableView.contentOffset = CGPointMake(_tableView.contentOffset.x, 0);
    [self refreshTable];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_REFRESH_TAB_MENU object:nil];
}
-(void)setNavigation{
    detailNavgation=[[UIView alloc]initWithFrame:CGRectMake(0,0, DeviceWidth, 64)];
    detailNavgation.backgroundColor=[UIColor colorC2060A];
    [self.view addSubview:detailNavgation];
    topSegment=[[UIView alloc]initWithFrame:CGRectMake(0, 64, DeviceWidth, 40)];
    topSegment.backgroundColor=[UIColor colorWithRed:242/255.0 green:242/255.0 blue:248/255.0 alpha:1];
    [self.view addSubview:topSegment];
    [self addTopSegmentview];
}
-(void)addTopSegmentview
{
    NSArray *buttonsArray = @[@"FEED",@"JOURNAL",@"DINING GUIDE"];
    CGFloat hotkeyWidth=(DeviceWidth)/buttonsArray.count;
    for (int i = 0; i<buttonsArray.count; i++)
    {
        UILabel* titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(hotkeyWidth*i, 5, hotkeyWidth, 20)];
        titleLabel.tag = 500+i;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.font=[UIFont systemFontOfSize:15];
        //        [button addSubview:titleLabel];
        titleLabel.numberOfLines = 0;
        titleLabel.text = [buttonsArray objectAtIndex:i];
        titleLabel.textColor=[UIColor colorWithRed:78/255.0 green:107/255.0 blue:122/255.0 alpha:1];
        
        if (i<2) {
            UIView* line=[[UIView alloc]initWithFrame:CGRectMake(DeviceWidth/3*(i+1), 10, 1, titleLabel.frame.size.height)];
            line.backgroundColor=[UIColor colorWithRed:78/255.0 green:107/255.0 blue:122/255.0 alpha:0.6];
            ;
            [topSegment addSubview:line];
        }
        
        
        
        UITapGestureRecognizer *tapGeature = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(bottomButtonLabelTapped:)];
        [titleLabel addGestureRecognizer:tapGeature];
        [topSegment addSubview:titleLabel];
    }
    
    
}
- (void)pullTableViewDidTriggerRefresh:(PullTableView *)pullTableView
{
    [self performSelector:@selector(refreshTable) withObject:nil afterDelay:1.0f];
}
- (void)refreshTable
{
    if (_isTrending)
    {
        trendingOffset = 0;
        [_trendingArrM removeAllObjects];
        isHadNextDataTrending = NO;
        isStartRefreshTrending = NO;
        [self initData];
    }
    else
    {
        freshingOffset = 0;
        [_freshArrM removeAllObjects];
        isHadNextDataFresh = NO;
        isStartRefreshFresh = NO;
        [self initData];
    }
    
}

- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    //    [[LoadingView sharedLoadingView] startLoading];
    //    [self performSelector:@selector(loadData) withObject:nil afterDelay:1.0f];
}

- (void)loadData{
    
    if (_isTrending)
    {
        if(trendingOffset<_trendingArrM.count)
        {
            
            if (isHadNextDataTrending)
            {
                isHadNextDataTrending = NO;
                
                [_dataArrM removeAllObjects];
                _dataArrM = [NSMutableArray arrayWithArray:_trendingArrM];
                [_tableView reloadData];
                
                trendingOffset+=max;
                [self getNextPageData];
            }
            else
                isStartRefreshTrending = YES;
            
            //            [self getNextPageData];
        }else{
            _tableView.pullTableIsLoadingMore = NO;
        }
    }
    else
    {
        if(freshingOffset<_freshArrM.count)
        {
            if (isHadNextDataFresh)
            {
                isHadNextDataFresh = NO;
                
                [_dataArrM removeAllObjects];
                _dataArrM = [NSMutableArray arrayWithArray:_freshArrM];
                [_tableView reloadData];
                
                freshingOffset+=max;
                [self getNextPageData];
            }
            else
                isStartRefreshTrending = YES;        }else{
                    _tableView.pullTableIsLoadingMore = NO;
                }
    }
    
}

- (void)initData
{
    isFreshing = YES;
    if ([self.currentCategoryId intValue] || self.searchText.length)
    {
        if (_isTrending)
        {
            NSMutableDictionary *trendingDic = [[NSMutableDictionary alloc] initWithDictionary:@{@"sort":@"viewCount",@"order":@"desc",@"offset":[NSNumber numberWithInt:trendingOffset],@"max":[NSNumber numberWithInt:10]}];
            
            if (self.searchText.length)
            {
                [trendingDic setValue:self.searchText forKey:@"content"];
            }
            if ([self.currentCategoryId intValue])
            {
                [trendingDic setValue:self.currentCategoryId forKey:@"category"];
            }
            if (self.needLog)
            {
                [trendingDic setValue:[NSNumber numberWithInt:1] forKey:@"needlog"];
            }
            
            IMGUser *user = [IMGUser currentUser];
            if (user.userId)
            {
                [trendingDic setObject:user.userId forKey:@"userId"];
            }
            
            if ([[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID])
            {
                [trendingDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
                
            }
            
            [trendingDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
            [trendingDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
            
            
            [SearchUtil journalSearch:trendingDic andBlock:^(NSArray *journalDataArray) {
                
                self.needLog = NO;
                
                [_trendingArrM addObjectsFromArray:journalDataArray];
                
                _dataArrM = [NSMutableArray arrayWithArray:_trendingArrM];
                [_tableView reloadData];
                if (trendingOffset == 0)
                {
                    _tableView.pullLastRefreshDate = [NSDate date];
                    _tableView.pullTableIsRefreshing = NO;
                }
                else
                    _tableView.pullTableIsLoadingMore = NO;
                [[LoadingView sharedLoadingView] stopLoading];
                if (!journalDataArray.count&&!_dataArrM.count)
                {
                    [self loadEmptyTableVIewHeadView];
                }
                else
                {
                    if (headerView)
                    {
                        [headerView removeFromSuperview];
                    }
                    trendingOffset+=max;
                    [self getNextPageData];
                }
                isFreshing = NO;
            }];
        }
        else
        {
            NSMutableDictionary *freshDic = [[NSMutableDictionary alloc] initWithDictionary:@{@"sort":@"createTime",@"order":@"desc",@"offset":[NSNumber numberWithInt:freshingOffset],@"max":[NSNumber numberWithInt:10]}];
            
            if (self.searchText.length)
            {
                [freshDic setValue:self.searchText forKey:@"content"];
            }
            if ([self.currentCategoryId intValue])
            {
                [freshDic setValue:self.currentCategoryId forKey:@"category"];
            }
            
            if (self.needLog)
            {
                [freshDic setValue:[NSNumber numberWithInt:1] forKey:@"needlog"];
            }
            
            IMGUser *user = [IMGUser currentUser];
            if (user.userId)
            {
                [freshDic setObject:user.userId forKey:@"userId"];
            }
            
            if ([[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID])
            {
                [freshDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
                
            }
            
            [freshDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
            [freshDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
            
            [SearchUtil journalSearch:freshDic andBlock:^(NSArray *journalDataArray) {
                
                self.needLog = NO;
                [_freshArrM addObjectsFromArray:journalDataArray];
                
                _dataArrM = [NSMutableArray arrayWithArray:_freshArrM];
                [_tableView reloadData];
                if (freshingOffset == 0)
                {
                    _tableView.pullLastRefreshDate = [NSDate date];
                    _tableView.pullTableIsRefreshing = NO;
                }
                else
                    _tableView.pullTableIsLoadingMore = NO;
                
                [[LoadingView sharedLoadingView] stopLoading];
                if (!journalDataArray.count&&!_dataArrM.count)
                {
                    [self loadEmptyTableVIewHeadView];
                }
                else
                {
                    if (headerView)
                    {
                        [headerView removeFromSuperview];
                    }
                    
                    freshingOffset+=max;
                    [self getNextPageData];
                }
                isFreshing = NO;
            }];
        }
    }
    else
    {
        if (_isTrending)
        {
            [TrendingParams setObject:[NSNumber numberWithInt:trendingOffset] forKey:@"offset"];
            [TrendingParams setObject:[NSNumber numberWithInt:max] forKey:@"max"];
            [JournalHandler getJournalListWithParams:TrendingParams andBlock:^(NSArray *dataArray,int totalCount) {
//                [MixpanelHelper trackOpenJournal:@"Trending Journal"];
                [_trendingArrM addObjectsFromArray:dataArray];
                _dataArrM = [NSMutableArray arrayWithArray:_trendingArrM];
                [_tableView reloadData];
                if (trendingOffset == 0)
                {
                    _tableView.pullLastRefreshDate = [NSDate date];
                    _tableView.pullTableIsRefreshing = NO;
                }
                else
                    _tableView.pullTableIsLoadingMore = NO;
                [[LoadingView sharedLoadingView] stopLoading];
                
                
                trendingOffset+=max;
                [self getNextPageData];
                
            } failure:^(NSError *error) {
                
            }];
        }
        else
        {
            [FreshParams setObject:[NSNumber numberWithInt:freshingOffset] forKey:@"offset"];
            [FreshParams setObject:[NSNumber numberWithInt:max] forKey:@"max"];
            [JournalHandler getJournalListWithParams:FreshParams andBlock:^(NSArray *dataArray,int totalCount) {
//                [MixpanelHelper trackOpenJournal:@"Fresh Journal"];
                [_freshArrM addObjectsFromArray:dataArray];
                _dataArrM = [NSMutableArray arrayWithArray:_freshArrM];
                [_tableView reloadData];
                if (freshingOffset == 0)
                {
                    _tableView.pullLastRefreshDate = [NSDate date];
                    _tableView.pullTableIsRefreshing = NO;
                }
                else
                    _tableView.pullTableIsLoadingMore = NO;
                [[LoadingView sharedLoadingView] stopLoading];
                
                freshingOffset+=max;
                [self getNextPageData];
            } failure:^(NSError *error) {
                
            }];
        }
    }
    
}


- (void)getNextPageData
{
    if (isViewDidLoad)
    {
        isViewDidLoad = NO;
        finishUI = [[NSDate date]timeIntervalSince1970];
        float duringTime = finishUI - beginLoadingData;
        duringTime = duringTime * 1000;
        int duringtime = duringTime;
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createTimingWithCategory:@"Qraved journal lists"  interval:[NSNumber numberWithInt:duringtime] name:@"" label:@""] build]];
        
    }
    if ([self.currentCategoryId intValue] || self.searchText.length)
    {
        if (_isTrending && !isHadNextDataTrending)
        {
            NSMutableDictionary *trendingDic = [[NSMutableDictionary alloc] initWithDictionary:@{@"sort":@"viewCount",@"order":@"desc",@"offset":[NSNumber numberWithInt:trendingOffset],@"max":[NSNumber numberWithInt:10]}];
            
            if (self.searchText.length)
            {
                [trendingDic setValue:self.searchText forKey:@"content"];
            }
            if ([self.currentCategoryId intValue])
            {
                [trendingDic setValue:self.currentCategoryId forKey:@"category"];
            }
            if (self.needLog)
            {
                [trendingDic setValue:[NSNumber numberWithInt:1] forKey:@"needlog"];
            }
            
            IMGUser *user = [IMGUser currentUser];
            if (user.userId)
            {
                [trendingDic setObject:user.userId forKey:@"userId"];
            }
            
            if ([[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID])
            {
                [trendingDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
                
            }
            
            [trendingDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
            [trendingDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
            
            
            [SearchUtil journalSearch:trendingDic andBlock:^(NSArray *journalDataArray) {
                
                self.needLog = NO;
                
                [_trendingArrM addObjectsFromArray:journalDataArray];
                
                isHadNextDataTrending = YES;
                
                if (isStartRefreshTrending)
                {
                    isStartRefreshTrending = NO;
                    _dataArrM = [NSMutableArray arrayWithArray:_trendingArrM];
                    [_tableView reloadData];
                }
                isFreshing = NO;
            }];
        }
        else if (!_isTrending && !isHadNextDataFresh)
        {
            NSMutableDictionary *freshDic = [[NSMutableDictionary alloc] initWithDictionary:@{@"sort":@"createTime",@"order":@"desc",@"offset":[NSNumber numberWithInt:freshingOffset],@"max":[NSNumber numberWithInt:10]}];
            
            if (self.searchText.length)
            {
                [freshDic setValue:self.searchText forKey:@"content"];
            }
            if ([self.currentCategoryId intValue])
            {
                [freshDic setValue:self.currentCategoryId forKey:@"category"];
            }
            
            if (self.needLog)
            {
                [freshDic setValue:[NSNumber numberWithInt:1] forKey:@"needlog"];
            }
            
            IMGUser *user = [IMGUser currentUser];
            if (user.userId)
            {
                [freshDic setObject:user.userId forKey:@"userId"];
            }
            
            if ([[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID])
            {
                [freshDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
                
            }
            
            [freshDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
            [freshDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
            
            [SearchUtil journalSearch:freshDic andBlock:^(NSArray *journalDataArray) {
                
                self.needLog = NO;
                [_freshArrM addObjectsFromArray:journalDataArray];
                
                
                isHadNextDataFresh = YES;
                
                if (isStartRefreshFresh)
                {
                    isStartRefreshFresh = NO;
                    _dataArrM = [NSMutableArray arrayWithArray:_freshArrM];
                    [_tableView reloadData];
                }
                isFreshing = NO;
            }];
        }else{
            isFreshing = NO;
        }
    }
    else
    {
        if (_isTrending && !isHadNextDataTrending)
        {
            [TrendingParams setObject:[NSNumber numberWithInt:trendingOffset] forKey:@"offset"];
            [TrendingParams setObject:[NSNumber numberWithInt:max] forKey:@"max"];
            [JournalHandler getJournalListWithParams:TrendingParams andBlock:^(NSArray *dataArray,int totalCount) {
//                [MixpanelHelper trackOpenJournal:@"Trending Journal"];
                [_trendingArrM addObjectsFromArray:dataArray];
                
                isHadNextDataTrending = YES;
                
                if (isStartRefreshTrending)
                {
                    isStartRefreshTrending = NO;
                    _dataArrM = [NSMutableArray arrayWithArray:_trendingArrM];
                    [_tableView reloadData];
                }
                isFreshing = NO;
            } failure:^(NSError *error) {
                
            }];
        }
        else if (!_isTrending && !isHadNextDataFresh)
        {
            [FreshParams setObject:[NSNumber numberWithInt:freshingOffset] forKey:@"offset"];
            [FreshParams setObject:[NSNumber numberWithInt:max] forKey:@"max"];
            [JournalHandler getJournalListWithParams:FreshParams andBlock:^(NSArray *dataArray,int totalCount) {
//                [MixpanelHelper trackOpenJournal:@"Fresh Journal"];
                [_freshArrM addObjectsFromArray:dataArray];
                isHadNextDataFresh = YES;
                
                if (isStartRefreshFresh)
                {
                    isStartRefreshFresh = NO;
                    _dataArrM = [NSMutableArray arrayWithArray:_freshArrM];
                    [_tableView reloadData];
                }
                isFreshing = NO;
            } failure:^(NSError *error) {
                
            }];
        }else{
            isFreshing = NO;
        }
    }
    
}

- (void)searchButtonClick
{
    NSLog(@"searchButtonTapped");
    DiscoverListViewController *dgvc = [[DiscoverListViewController alloc] init];
    dgvc.isSearchJournal = YES;
    dgvc.isTrending = _isTrending;
    dgvc.searchText = self.searchText;
    [self.navigationController pushViewController:dgvc animated:YES];
    
}
- (void)searchButtonTapped:(id)sender
{
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:@"Journal List" forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL - Search CTA" withEventProperties:eventProperties];
    
    if (self.isPop) {

        [self.navigationController popViewControllerAnimated:YES];

    }else{
        DiscoverListViewController *dgvc = [[DiscoverListViewController alloc] init];
        dgvc.isSearchJournal = YES;
        dgvc.isTrending = _isTrending;
        dgvc.searchText = self.searchText;
        [self.navigationController pushViewController:dgvc animated:YES];
    }
}
- (void)addMainView
{
    
    newestBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [newestBtn setTitle:L(@"FRESH") forState:UIControlStateNormal];
    newestBtn.frame = CGRectMake(LEFTLEFTSET,5, newestBtn.titleLabel.expectedWidth, 32);
    newestBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:newestBtn];
    newestBtn.backgroundColor = [UIColor clearColor];
    [newestBtn setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    newestBtn.tag = 101;
    [newestBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    hottextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [hottextBtn setTitle:L(@"TRENDING") forState:UIControlStateNormal];
    hottextBtn.frame = CGRectMake(newestBtn.endPointX + 25, newestBtn.frame.origin.y, hottextBtn.titleLabel.expectedWidth, 32);
    hottextBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:hottextBtn];
    //    hottextBtn.titleLabel.font.lineHeight=30;
    hottextBtn.backgroundColor = [UIColor clearColor];
    [hottextBtn setTitleColor:[UIColor colorAAAAAA] forState:UIControlStateNormal];
    hottextBtn.tag = 100;
    [hottextBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    _isBtnImageView = [[UIImageView alloc] init];
    _isBtnImageView.frame = CGRectMake(newestBtn.frame.origin.x, newestBtn.endPointY+10, newestBtn.titleLabel.expectedWidth, 1);
    _isBtnImageView.backgroundColor = [UIColor colorC2060A];
    [self.view addSubview:_isBtnImageView];
    
    
    if (_isTrending)
    {
        _isBtnImageView.frame = CGRectMake(hottextBtn.frame.origin.x, _isBtnImageView.frame.origin.y, hottextBtn.titleLabel.expectedWidth, 2);
        [hottextBtn setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
        [newestBtn setTitleColor:[UIColor colorAAAAAA] forState:UIControlStateNormal];
        
    }
    
    
    
    lineImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, _isBtnImageView.endPointY, DeviceWidth, 1)];
    lineImage3.image = [UIImage imageNamed:@"CutOffRule"];
    [self.view addSubview:lineImage3];
    
//    float leftMargin = 16;
//    UIFont *labelFont=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:10];
//    UIFont *valueLabelFont=[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13];
//    float valueLabelWidth = DeviceWidth/2 - 40;
    
    
//    searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    searchBtn.frame = CGRectMake(0, _isBtnImageView.endPointY, DeviceWidth/2, 44);
//    searchBtn.backgroundColor = [UIColor clearColor];
//    [searchBtn addTarget:self action:@selector(gotoSearch) forControlEvents:UIControlEventTouchUpInside];
    
//    UILabel *searchLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, 3, 30, 17)];
//    [searchLabel setText:@"Search"];
//    [searchLabel setFont:labelFont];
//    [searchLabel setTextColor:[UIColor color111111]];
//    searchLabel.frame = CGRectMake(leftMargin, 3, searchLabel.expectedWidth, searchLabel.font.lineHeight);
//    [searchLabel setBackgroundColor:[UIColor clearColor]];
//    [searchBtn addSubview:searchLabel];
//    
//    UILabel *searchValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, 19, valueLabelWidth, 20)];
//    
//    if (self.searchText.length)
//    {
//        searchValueLabel.text = self.searchText;
//    }
//    [searchValueLabel setFont:valueLabelFont];
//    [searchValueLabel  setTextColor:[UIColor color111111]];
//    [searchValueLabel setBackgroundColor:[UIColor clearColor]];
////    [searchBtn addSubview:searchValueLabel];
//    
//    UIImage* arrowImage = [UIImage imageNamed:@"arrowDown1"];
//    arrow1ImageView = [[UIImageView alloc] initWithImage:arrowImage];
//    arrow1ImageView.frame = CGRectMake(searchBtn.endPointX - 20, 20, 10, 5);
//    [searchBtn addSubview:arrow1ImageView];
//    [searchBtn setAlpha:0.8];
//    
////    [self.view addSubview:searchBtn];
//    
//    
//    categoryBtn = [[UIButton alloc] initWithFrame:CGRectMake(searchBtn.endPointX, searchBtn.frame.origin.y, DeviceWidth/2, 44)];
//    categoryBtn.backgroundColor = [UIColor clearColor];
//    [categoryBtn addTarget:self action:@selector(goSort) forControlEvents:UIControlEventTouchUpInside];
//    
//    UILabel *categoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, 3, 50, 17)];
//    [categoryLabel setText:L(@"category")];
//    [categoryLabel setFont:labelFont];
//    [categoryLabel setTextColor:[UIColor color111111]];
//    [categoryLabel setBackgroundColor:[UIColor clearColor]];
//    [categoryBtn addSubview:categoryLabel];
//    
//    categoryValueLabel= [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, 19, valueLabelWidth, 20)];
//    
//    if (self.currentCategoryName.length)
//    {
//        categoryValueLabel.text = self.currentCategoryName;
//    }
//    [categoryValueLabel setFont:valueLabelFont];
//    [categoryValueLabel  setTextColor:[UIColor color111111]];
//    [categoryValueLabel setBackgroundColor:[UIColor clearColor]];
////    [categoryBtn addSubview:categoryValueLabel];
//    
//    arrow2ImageView = [[UIImageView alloc] initWithImage:arrowImage];
//    arrow2ImageView.frame = CGRectMake(searchBtn.endPointX - 20, 20, 10, 5);
//    [categoryBtn addSubview:arrow2ImageView];
//    [categoryBtn setAlpha:0.8];
    
//    [self.view addSubview:categoryBtn];
    
    
    //    UIEdgeInsets ed = {1.0f, 0.0f, 0.0f, 0.0f};
    //
    //    _isBtnImageView.image = [[UIImage imageNamed:@"tab"] resizableImageWithCapInsets:ed];
    
    //    [ImageView setImage:[[UIImage imageNamed:@"xxx.png"]resizableImageWithCapInsets:ed]];
    
    
    categoryBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    categoryBtn.frame = CGRectMake(DeviceWidth - LEFTLEFTSET - 40,topSegment.endPointY+10,30,30);
    [categoryBtn setImage:[UIImage imageNamed:@"journalCategory"] forState:UIControlStateNormal];
    [categoryBtn addTarget:self action:@selector(goSort) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:categoryBtn];
    
    
    //    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [searchBtn setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
    //    searchBtn.frame = CGRectMake(DeviceWidth - LEFTLEFTSET - 30, 5, 40, 40);
    //    [self.view addSubview:searchBtn];
    //    [searchBtn addTarget:self action:@selector(searchClick) forControlEvents:UIControlEventTouchUpInside];
    CGFloat scrollViewStartY;
    if (self.isFromSearch&&self.searchText.length>0&&![self isEmpty:self.searchText]) {
        UILabel *searchTextLabel = [[UILabel alloc] init];
        searchTextLabel.text = [NSString stringWithFormat:@"Showing Results of \"%@\"",self.searchText];
        searchTextLabel.font = [UIFont boldSystemFontOfSize:16];
        searchTextLabel.backgroundColor = [UIColor whiteColor];
        [searchTextLabel setTextColor:[UIColor color333333]];
        searchTextLabel.numberOfLines = 0;
        CGSize size  = [searchTextLabel.text sizeWithFont:searchTextLabel.font constrainedToSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, MAXFLOAT)];
        searchTextLabel.frame = CGRectMake(LEFTLEFTSET, lineImage3.endPointY+5, DeviceWidth-2*LEFTLEFTSET, size.height);
        
        scrollViewStartY = searchTextLabel.endPointY+10;
        [self.view addSubview:searchTextLabel];
    }else{
        scrollViewStartY = lineImage3.endPointY+10;

    }
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0,scrollViewStartY, DeviceWidth*2, DeviceHeight - 44 - _isBtnImageView.endPointY - 46-20)];
    _searchView.backgroundColor = [UIColor clearColor];
    _scrollView.scrollsToTop = NO;
    [self.view addSubview:_scrollView];
    
    //    _hottextTabView = [[UITableView alloc] init];
    //    _hottextTabView.frame = CGRectMake(0, 0, DeviceWidth, _scrollView.frame.size.height);
    //    _hottextTabView.delegate = self;
    //    _hottextTabView.dataSource = self;
    //    _hottextTabView.tag = 1000;
    //    [_scrollView addSubview:_hottextTabView];
    //
    //    _newestTabView = [[UITableView alloc] init];
    //    _newestTabView.frame = CGRectMake(DeviceWidth, 0, DeviceWidth, _scrollView.frame.size.height);
    //    _newestTabView.delegate = self;
    //    _newestTabView.dataSource = self;
    //    _newestTabView.tag = 1001;
    //    [_scrollView addSubview:_newestTabView];
    
    
    
    _tableView = [[PullTableView alloc] init];
    _tableView.frame = CGRectMake(0, 0, DeviceWidth, _scrollView.frame.size.height+[[AppDelegate ShareApp].homeNavigationController getNavigationBarHeight]);
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.pullDelegate = self;
    _tableView.tag = 100;
    _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    //    [_tableView setLoadMoreViewWithBackGroundColor:[UIColor clearColor]];
    [_tableView setRefreshView];
    [_scrollView addSubview:_tableView];
    
    _scrollView.contentSize = CGSizeMake(DeviceWidth*2, 0);
    
}
- (void)loginClick
{
    LoginViewController *lvc = [[LoginViewController alloc] init];
    lvc.isSignUpSplash = YES;
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:lvc];
    [self.navigationController presentViewController:nav animated:NO completion:^{
  
    }];
}
- (void)goSort
{
    JournalCategoryViewController *jcvc = [[JournalCategoryViewController alloc] init];
    jcvc.categoryDataArrM = [NSMutableArray arrayWithArray:_categoryDataArrM];
    jcvc.listViewCategoryId = self.currentCategoryId;
    //jcvc.jcvcDelegate = self;
   [self.navigationController pushViewController:jcvc animated:YES];
}
- (void)sendValueWithCategoryId:(NSNumber *)categoryId andCategoryName:(NSString *)categoryName
{
    
    self.currentCategoryId = categoryId;
    self.currentCategoryName = categoryName;
    categoryValueLabel.text = self.currentCategoryName;
    
    freshingOffset = 0;
    trendingOffset = 0;
    
    [_trendingArrM removeAllObjects];
    [_freshArrM removeAllObjects];
    
    isHadNextDataFresh = NO;
    isStartRefreshFresh = NO;
    isHadNextDataTrending = NO;
    isStartRefreshTrending = NO;
    
    
    [self initData];
    
}
//- (void)gotoSearch
//{
//    DiscoverListViewController *dgvc = [[DiscoverListViewController alloc] init];
//    dgvc.isSearchJournal = YES;
//    dgvc.isTrending = _isTrending;
//    dgvc.searchText = self.searchText;
//    [self.navigationController pushViewController:dgvc animated:YES];
//}
//- (void)searchClick
//{
//    [self addSearchView];
//    
//}
//- (void)addSearchView
//{
//    _backgroundView = [[UIView alloc] init];
//    _backgroundView.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight);
//    [self.view addSubview:_backgroundView];
//    _backgroundView.alpha = 0.5f;
//    _backgroundView.backgroundColor = [UIColor blackColor];
//    _backgroundView.userInteractionEnabled = YES;
//    
//    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundViewClick)];
//    tapGesture.numberOfTouchesRequired = 1;
//    tapGesture.numberOfTapsRequired = 1;
//    [_backgroundView addGestureRecognizer:tapGesture];
//    
//    
//    _searchView = [[UIScrollView alloc] init];
//    _searchView.frame = CGRectMake(0, 0, DeviceWidth, 244+44);
//    _searchView.backgroundColor = [UIColor whiteColor];
//    _searchView.contentSize = CGSizeMake(DeviceWidth+1, 0);
//    _searchView.pagingEnabled = YES;
//    [self.view addSubview:_searchView];
//    
//    UITextField *searchTextField = [[UITextField alloc] init];
//    searchTextField.placeholder = @"Looking for somethings";;
//    searchTextField.frame = CGRectMake(LEFTLEFTSET, 12, DeviceWidth/2+80, 20);
//    searchTextField.textColor = [UIColor grayColor];
//    searchTextField.font = [UIFont systemFontOfSize:13];
//    searchTextField.delegate = self;
//    searchTextField.alpha = 0.7f;
//    [_searchView addSubview:searchTextField];
//    [self addLineImage:searchTextField.endPointY+11 withCurrentView:_searchView];
//    
//    searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [searchBtn setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
//    searchBtn.frame = CGRectMake(DeviceWidth - LEFTLEFTSET - 30, 5, 40, 40);
//    [_searchView addSubview:searchBtn];
//    
//    
//    _categoryTableView = [[UITableView alloc] init];
//    _categoryTableView.tag = 101;
//    _categoryTableView.delegate = self;
//    _categoryTableView.dataSource = self;
//    _categoryTableView.frame = CGRectMake(0, 44, DeviceWidth, 200+44);
//    [_searchView addSubview:_categoryTableView];
//    
//}
- (void)backgroundViewClick
{
    [_backgroundView removeFromSuperview];
    //[_searchView removeFromSuperview];
}
- (void)addLineImage:(CGFloat)currentPointY withCurrentView:(UIView *)currentView
{
    lineImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, currentPointY-1, DeviceWidth, 1)];
    lineImage3.image = [UIImage imageNamed:@"CutOffRule"];
    currentPointY = lineImage3.endPointY;
    [currentView addSubview:lineImage3];
}
- (void)btnClick:(UIButton *)btn
{
    
    if (btn.tag == 100)
    {
        _isTrending = YES;
        _isBtnImageView.frame = CGRectMake(hottextBtn.frame.origin.x, _isBtnImageView.frame.origin.y, hottextBtn.titleLabel.expectedWidth, 2);
        [hottextBtn setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
        [newestBtn setTitleColor:[UIColor colorAAAAAA] forState:UIControlStateNormal];
        
        if (_trendingArrM.count)
        {
            [_dataArrM removeAllObjects];
            _dataArrM = [NSMutableArray arrayWithArray:_trendingArrM];
            [_tableView reloadData];
        }
        else
            [self initData];
        
    }
    else
    {
        _isTrending = NO;
        _isBtnImageView.frame = CGRectMake(newestBtn.frame.origin.x, _isBtnImageView.frame.origin.y, newestBtn.titleLabel.expectedWidth, 2);
        [newestBtn setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
        [hottextBtn setTitleColor:[UIColor colorAAAAAA] forState:UIControlStateNormal];
        
        if (_freshArrM.count)
        {
            [_dataArrM removeAllObjects];
            _dataArrM = [NSMutableArray arrayWithArray:_freshArrM];
            [_tableView reloadData];
        }
        else
            [self initData];
        
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 100)
    {
        if (indexPath.row >= _dataArrM.count)
        {
            return [self loadingCell];
        }
        
        static NSString *identifier =@"cell";
        JournalListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[JournalListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cell.delegate = self;
        [cell setJournal:[_dataArrM objectAtIndex:indexPath.row]];
        return cell;
    }
    else
    {
        static NSString *cellName = @"cellName";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellName];
        }
        IMGJournalCategory *category = [_searchResultArray objectAtIndex:indexPath.row];
        cell.textLabel.text = category.categoryName;
        return cell;
        
    }
}
//- (void)categoryNameLabelClick:(NSString *)categoryName
//{
//    for (IMGJournalCategory *category in _categoryDataArrM)
//    {
//        if ([category.categoryName isEqualToString:categoryName])
//        {
//            [self backgroundViewClick];
//            JournalSearchViewController *jsvc = [[JournalSearchViewController alloc] init];
//            jsvc.category = category;
//            jsvc.categoryDataArrM = [NSMutableArray arrayWithArray:_categoryDataArrM];
//            [self.navigationController pushViewController:jsvc animated:YES];
//            break;
//        }
//    }
//}

- (void)saveBtnClickedWithSelect:(BOOL)isSaved andJournalArticleId:(NSNumber *)articleId
{
    IMGUser *user = [IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil)
    {
        LoginViewController *loginVC=[[LoginViewController alloc]initWithFromWhere:[NSNumber numberWithInt:1]];
        UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
        [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
        
        return;
    }
        
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 100)
    {
        
        if (_isTrending)
        {
            if (_trendingArrM.count%10==0 && _trendingArrM.count>0)
            {
                return _dataArrM.count + 1;
            }
            else
                return _dataArrM.count;
        }
        else
        {
            if (_freshArrM.count%10==0 && _freshArrM.count>0)
            {
                return _dataArrM.count + 1;
            }
            else
                return _dataArrM.count;
            
        }
        
        //        if (_dataArrM.count%10==0 && _dataArrM.count>0)
        //        {
        //            return _dataArrM.count + 1;
        //        }
        //        else
        //            return  _dataArrM.count;
    }
    else
    {
        return _searchResultArray.count;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 100)
    {
        
        if (indexPath.row < _dataArrM.count)
        {
            return 230;
        }
        else
        {
            return 60;
        }
    }
    else
    {
        return 40;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell=[tableView cellForRowAtIndexPath:indexPath];
    if (tableView.tag == 100)
    {
        IMGMediaComment *journal = [_dataArrM objectAtIndex:indexPath.row];
        if ([journal.imported intValue]== 1)
        {
            cell.userInteractionEnabled=NO;
            [JournalHandler getJournalDetailWithJournalArticleId:journal.mediaCommentId andBlock:^(NSString *content,NSString *webSite) {
                journal.contents = content;
                WebViewController *wvc = [[WebViewController alloc] init];
                wvc.content = journal.contents;
                wvc.journal = journal;
                wvc.webSite = webSite;
                wvc.title = journal.title;
                wvc.fromDetail = NO;
                cell.userInteractionEnabled=YES;
                [self.navigationController pushViewController:wvc animated:YES];
            }];
            
        }
        else
        {
            
            NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
            [param setValue:[self getPageIdByAmplitudeType] forKey:@"Location"];
            [[Amplitude instance] logEvent:@"CL - Journal Category" withEventProperties:param];
            
            JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
            jdvc.amplitudeType = @"Article card on Article list";
            jdvc.journal = journal;
            jdvc.categoryDataArrM = [NSMutableArray arrayWithArray:_categoryDataArrM];
            [self.navigationController pushViewController:jdvc animated:YES];
        }
    }
    else if (tableView.tag == 10000)
    {
        IMGJournalCategory *category = [_categoryDataArrM objectAtIndex:indexPath.row];
        self.currentCategoryId = category.categoryOrder;
        self.currentCategoryName = category.categoryName;
        categoryValueLabel.text = self.currentCategoryName;
        
        
        NSDictionary *freshDic = @{@"sort":@"createTime",@"order":@"desc",@"category":self.currentCategoryId,@"offset":[NSNumber numberWithInt:0],@"max":[NSNumber numberWithInt:10]};
        
        
        
        [SearchUtil journalSearch:freshDic andBlock:^(NSArray *journalDataArray) {
            
            _dataArrM = [NSMutableArray arrayWithArray:journalDataArray];
            [_tableView reloadData];
            [[LoadingView sharedLoadingView] stopLoading];
            
            if (!_dataArrM.count)
            {
                [self loadEmptyTableVIewHeadView];
            }
            else
            {
                [headerView removeFromSuperview];
            }
            
        }];
    }
    else
    {
        [self backgroundViewClick];
        JournalSearchViewController *jsvc = [[JournalSearchViewController alloc] init];
        jsvc.category = [_categoryDataArrM objectAtIndex:indexPath.row];
        jsvc.categoryDataArrM = [NSMutableArray arrayWithArray:_categoryDataArrM];
        [self.navigationController pushViewController:jsvc animated:YES];
    }
    
}
- (UITableViewCell *)loadingCell {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:nil] ;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]
                                                  initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.frame = CGRectMake(DeviceWidth/2 - 10, floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, REFRESH_HEADER_HEIGHT)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:12.0];
    label.textAlignment = NSTextAlignmentCenter;
    [label setText:@"loading"];
    [cell addSubview:activityIndicator];
    //    [cell addSubview:label];
    //    [cell bringSubviewToFront:label];
    
    [activityIndicator startAnimating];
    
    //    cell.tag = LOADING_CELL_TAG;
    
    return cell;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[LoadingView sharedLoadingView] stopLoading];
   
    [AppDelegate ShareApp].isSlideJournalHidden = YES;
    if ([AppDelegate ShareApp].isSlideHomeHidden
        &&[AppDelegate ShareApp].isSlideJournalHidden
        &&[AppDelegate ShareApp].isSlideDiningGuideHidden) {
        [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    }
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (scrollView.tag != 100)
    {
        return;
    }

}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

{  //string就是此时输入的那个字符textField就是此时正在输入的那个输入框返回YES就是可以改变输入框的值NO相反
    
    
    
    if ([string isEqualToString:@"\n"])  //按会车可以改变
        
    {
        
        return YES;
        
    }
    
    
    
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    
    
    
    if ([toBeString length] > 65) { //如果输入框内容大于20则弹出警告
        
        textField.text = [toBeString substringToIndex:65];
        
        return NO;
        
    }
    
    
    [_searchResultArray removeAllObjects];
    for(IMGJournalCategory *category in _categoryDataArrM)
    {
        NSRange categoryRange = [category.categoryName rangeOfString:toBeString options:NSCaseInsensitiveSearch];
        if(categoryRange.location != NSNotFound)
        {
            [_searchResultArray addObject:category];
        }
    }
    
    if (![toBeString length])
    {
        _searchResultArray = [NSMutableArray arrayWithArray:_categoryDataArrM];
    }
    
    [_categoryTableView reloadData];
    
    
    
    return YES;
    
}

-(void)loadEmptyTableVIewHeadView{
    //    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-restaurantSearchBar.endPointY)];
    //    UIImage *image = [UIImage imageNamed:@"noSearch"];
    //    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth/2-image.size.width/2, 40, image.size.width, image.size.height)];
    //    imageView.image = image;
    //    [headerView addSubview:imageView];
    //    NSString *noRestaurantStr = @"We can't find any results for this search. You can try searching for something more general.";
    //    CGSize expectSize = [noRestaurantStr sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16] constrainedToSize:CGSizeMake(DeviceWidth-LEFTLEFTSET*4, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
    //    Label *noRestaurantLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET*2, imageView.endPointY+15, DeviceWidth-4*LEFTLEFTSET, expectSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16] andTextColor:[UIColor color222222] andTextLines:0];
    //    noRestaurantLabel.lineBreakMode = NSLineBreakByWordWrapping;
    //    noRestaurantLabel.textAlignment = NSTextAlignmentCenter;
    //    noRestaurantLabel.text = noRestaurantStr;
    //    [headerView addSubview:noRestaurantLabel];
    //
    //    UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 1)];
    //    lineImage.backgroundColor = [UIColor colorEEEEEE];
    //    [headerView addSubview:lineImage];
    //
    //    [searchResultsTableView setTableHeaderView:headerView];
    
    headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, _scrollView.frame.size.height)];
    
    UIView *suggestView = [[UIView alloc] init];
    suggestView.backgroundColor = [UIColor colorWithRed:234/255.0 green:234/255.0 blue:234/255.0 alpha:1];
    suggestView.frame = CGRectMake(0, 0, DeviceWidth, 100);
    [headerView addSubview:suggestView];
    
    UIImageView *blackMapImageView = [[UIImageView alloc] init];
    blackMapImageView.frame = CGRectMake(30, 30, 24, 32);
    blackMapImageView.image = [UIImage imageNamed:@"map_black"];
    [suggestView addSubview:blackMapImageView];
    
    UILabel *suggestLabel = [[UILabel alloc] init];
    
    suggestLabel.text = @"Still haven’t found what you want? \nSuggest a your story topic. We'll find it for you.";
    suggestLabel.numberOfLines = 0;
    suggestLabel.frame = CGRectMake(80, 30, DeviceWidth - LEFTLEFTSET*2 - 90, 100);
    suggestLabel.font = [UIFont systemFontOfSize:12];
    CGSize size = [suggestLabel.text sizeWithFont:[UIFont boldSystemFontOfSize:12] constrainedToSize:CGSizeMake(suggestLabel.frame.size.width, 10000) lineBreakMode:NSLineBreakByWordWrapping];
    suggestLabel.frame = CGRectMake(suggestLabel.frame.origin.x, suggestLabel.frame.origin.y, size.width, size.height);
    [suggestView addSubview:suggestLabel];
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:suggestLabel.text];
    [attrString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:12] range:[suggestLabel.text rangeOfString:@"Suggest a your story topic. We'll find it for you."]];
    suggestLabel.attributedText = attrString;
    
    UIImageView *arrowView = [[UIImageView alloc] init];
    arrowView.image = [UIImage imageNamed:@"arrow.png"];
    arrowView.frame = CGRectMake(suggestView.frame.size.width - (9.5+8), (suggestView.frame.size.height-14)/2.0, 8, 14);
    [suggestView addSubview:arrowView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoSuggest)];
    [suggestView addGestureRecognizer:tap];
    
    [_scrollView addSubview:headerView];
    
    UILabel *optionLable = [[UILabel alloc] init];
    optionLable.text = @"Why not try some of these options";
    optionLable.frame  = CGRectMake(LEFTLEFTSET, suggestView.endPointY, DeviceWidth-LEFTLEFTSET, 44);
    optionLable.font = [UIFont boldSystemFontOfSize:13];
    [headerView addSubview:optionLable];
    
    optionTableView= [[UITableView alloc] init];
    optionTableView.delegate = self;
    optionTableView.dataSource = self;
    optionTableView.tag = 10000;
    optionTableView.frame = CGRectMake(0, optionLable.endPointY, DeviceWidth, headerView.frame.size.height - optionLable.endPointY);
    [headerView addSubview:optionTableView];
    
    
}
- (void)gotoSuggest
{
    JournalSuggestViewController *jsvc = [[JournalSuggestViewController alloc] init];
    [self.navigationController pushViewController:jsvc animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ( (!_tableView.pullTableIsRefreshing) ||  (_tableView.pullTableIsRefreshing && scrollView.contentOffset.y > 0)) {
        if ((_tableView.pullTableIsRefreshing && scrollView.contentOffset.y > 0)) {
            
        }
        [scrollView scrollNavigationController:[AppDelegate ShareApp].homeNavigationController];
    }
    scrollView.qraved_navigationTitleScrollLastOffsetY = [NSNumber numberWithFloat:scrollView.contentOffset.y];
    if (scrollView.tag != 100 )
    {
        return;
    }
    if (scrollView.contentOffset.y<0) {
        return;
    }

    int offset = 0;
    if (_isTrending)
    {
        offset = trendingOffset;
    }
    else
        offset = freshingOffset;
    
    if (scrollView.contentSize.height - scrollView.contentOffset.y - scrollView.bounds.size.height < 300*(offset/10))
    {
        [self loadData];
    }
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (BOOL) isEmpty:(NSString *) str {
    
    if (!str) {
        return true;
    } else {
        //A character set containing only the whitespace characters space (U+0020) and tab (U+0009) and the newline and nextline characters (U+000A–U+000D, U+0085).
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        
        //Returns a new string made by removing from both ends of the receiver characters contained in a given character set.
        NSString *trimedString = [str stringByTrimmingCharactersInSet:set];
        
        if ([trimedString length] == 0) {
            return true;
        } else {
            return false;
        }
    }
}

-(NSString*)getPageIdByAmplitudeType{
    NSString *pageId = @"";
    if ([@"Search page" isEqualToString:self.amplitudeType]) {
        pageId = @",Search restaurant result page";
    }
    return [NSString stringWithFormat:@"%@%@",self.amplitudeType,pageId];
}
@end
