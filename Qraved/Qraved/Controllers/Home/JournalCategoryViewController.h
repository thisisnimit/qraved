//
//  JournalCategoryViewController.h
//  Qraved
//
//  Created by Lucky on 15/10/8.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"

@protocol JournalCategoryViewControllerDelegate <NSObject>

- (void)sendValueWithCategoryId:(NSNumber *)categoryId andCategoryName:(NSString *)categoryName;

@end

@interface JournalCategoryViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic) NSMutableArray *categoryDataArrM;
@property (nonatomic) NSNumber *listViewCategoryId;

@property (nonatomic, copy) void (^refreshJournal)(NSNumber *categoryId);

@property (nonatomic,assign) id<JournalCategoryViewControllerDelegate>jcvcDelegate;

@end
