//
//  SelectCityController.h
//  Qraved
//
//  Created by root on 9/8/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol SelectCityDelegate <NSObject>
-(void)successSelectCity;

@end

@interface SelectCityController : BaseViewController<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong) UITableView *tableView;
@property (nonatomic,assign) BOOL isAutoPost;
@property (nonatomic,assign) BOOL isNotHiddenTitle;
@property(nonatomic,retain) NSString *amplitudeType;
@property(nonatomic) BOOL isFromBooking;
@property (nonatomic,assign)  BOOL isShowPersonalization;
@property (nonatomic,assign)  BOOL isFromList;

-(instancetype)initWithSelectedIndex:(NSNumber*)index;
@property(nonatomic,weak)id<SelectCityDelegate> selectCityDelegate;
@end
