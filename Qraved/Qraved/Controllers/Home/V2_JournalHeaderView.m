//
//  V2_JournalHeaderView.m
//  Qraved
//
//  Created by harry on 2017/7/6.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_JournalHeaderView.h"
#import "Date.h"
@implementation V2_JournalHeaderView
{
    UILabel *lblTitle;
    UILabel *lblPostTime;
    UILabel *lblViewed;
    UILabel *lblShare;
    UIImageView *authorImageView;
    UILabel *lblAuthorName;
    UILabel *lblAuthorInfo;
    UIImageView *journalImageView;
    UILabel *lblJournalImageType;
    UILabel *lblJournalContent;
    UIImageView *viewedImageView;
    UIImageView *shareImageView;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, DeviceWidth-30, 26)];
    lblTitle.font = [UIFont systemFontOfSize:24];
    lblTitle.textColor = [UIColor color333333];
    lblTitle.numberOfLines = 0;
    
    lblPostTime = [[UILabel alloc] initWithFrame:CGRectMake(15, lblTitle.endPointY+7, 120, 14)];
    lblPostTime.font = [UIFont systemFontOfSize:12];
    lblPostTime.textColor = [UIColor color999999];
    
    viewedImageView = [[UIImageView alloc] initWithFrame:CGRectMake(lblPostTime.endPointX+5, lblTitle.endPointY+10, 12, 8)];
    viewedImageView.image = [UIImage imageNamed:@"ic_eye_small"];
    
    lblViewed = [[UILabel alloc] initWithFrame:CGRectMake(viewedImageView.endPointX+5, lblTitle.endPointY+7, 60, 14)];
    lblViewed.font = [UIFont systemFontOfSize:12];
    lblViewed.textColor = [UIColor color999999];
    
    shareImageView = [[UIImageView alloc] initWithFrame:CGRectMake(lblViewed.endPointX+5, lblTitle.endPointY+10, 12, 8)];
    shareImageView.image = [UIImage imageNamed:@"ic_share_small"];
    
    lblShare = [[UILabel alloc] initWithFrame:CGRectMake(shareImageView.endPointX+5, lblTitle.endPointY+7, 50, 14)];
    lblShare.font = [UIFont systemFontOfSize:12];
    lblShare.textColor = [UIColor color999999];
    
    authorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, lblPostTime.endPointY+20, 32, 32)];
    authorImageView.layer.masksToBounds = YES;
    authorImageView.layer.cornerRadius = 16;
    authorImageView.contentMode = UIViewContentModeScaleAspectFill;
    authorImageView.clipsToBounds = YES;
    
    lblAuthorName = [[UILabel alloc] initWithFrame:CGRectMake(authorImageView.endPointX+10, lblPostTime.endPointY+20, DeviceWidth-72, 16)];
    lblAuthorName.font = [UIFont systemFontOfSize:14];
    lblAuthorName.textColor = [UIColor color333333];
    
    lblAuthorInfo = [[UILabel alloc] initWithFrame:CGRectMake(authorImageView.endPointX+10, lblAuthorName.endPointY+3, DeviceWidth-72, 14)];
    lblAuthorInfo.font = [UIFont systemFontOfSize:12];
    lblAuthorInfo.textColor = [UIColor color999999];
    
    journalImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, authorImageView.endPointY+20, DeviceWidth, 10)];
    journalImageView.contentMode= UIViewContentModeScaleAspectFill;
    journalImageView.clipsToBounds = YES;
    journalImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *dbTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(JournalRestaurantImageClick:)];
    dbTap.numberOfTapsRequired=1;
    dbTap.numberOfTouchesRequired=1;
    [journalImageView addGestureRecognizer:dbTap];
    
    lblJournalImageType = [[UILabel alloc] initWithFrame:CGRectMake(15, journalImageView.endPointY+5, DeviceWidth-30, 14)];
    lblJournalImageType.font = [UIFont systemFontOfSize:12];
    lblJournalImageType.textColor = [UIColor color999999];
    lblJournalImageType.textAlignment = NSTextAlignmentCenter;
    lblJournalImageType.numberOfLines = 2;
    
    lblJournalContent = [[UILabel alloc] initWithFrame:CGRectMake(15, lblJournalImageType.endPointY + 15, DeviceWidth-30, 0)];
    lblJournalContent.font = [UIFont systemFontOfSize:14];
    lblJournalContent.textColor = [UIColor color333333];
    lblJournalContent.numberOfLines = 0;
    
    //    [self sd_addSubviews:@[lblTitle,lblPostTime,viewedImageView,lblViewed,shareImageView,lblShare,authorImageView,lblAuthorName,lblAuthorInfo,journalImageView,lblJournalImageType,lblJournalContent]];
    
    [self addSubview:lblTitle];
    [self addSubview:lblPostTime];
    [self addSubview:viewedImageView];
    [self addSubview:lblViewed];
    [self addSubview:shareImageView];
    [self addSubview:lblShare];
    [self addSubview:authorImageView];
    [self addSubview:lblAuthorName];
    [self addSubview:lblAuthorInfo];
    [self addSubview:journalImageView];
    [self addSubview:lblJournalImageType];
    [self addSubview:lblJournalContent];
    
    //    lblTitle.sd_layout
    //    .topSpaceToView(self,5)
    //    .leftSpaceToView(self,15)
    //    .rightSpaceToView(self,15)
    //    .autoHeightRatio(0);
    //
    //    [lblTitle setMaxNumberOfLinesToShow:2];
    //
    //    lblPostTime.sd_layout
    //    .topSpaceToView(lblTitle,7)
    //    .leftEqualToView(lblTitle)
    //    .heightIs(14);
    //
    //    [lblPostTime setSingleLineAutoResizeWithMaxWidth:150];
    //
    //    viewedImageView.sd_layout
    //    .widthIs(12)
    //    .heightIs(8)
    //    .leftSpaceToView(lblPostTime,5)
    //    .topSpaceToView(lblTitle,10);
    //
    //    lblViewed.sd_layout
    //    .topEqualToView(lblPostTime)
    //    .leftSpaceToView(viewedImageView,5)
    //    .heightIs(14);
    //
    //    [lblViewed setSingleLineAutoResizeWithMaxWidth:100];
    //
    //    shareImageView.sd_layout
    //    .leftSpaceToView(lblViewed,5)
    //    .topEqualToView(viewedImageView)
    //    .widthIs(12)
    //    .heightIs(8);
    //
    //    lblShare.sd_layout
    //    .topEqualToView(lblViewed)
    //    .leftSpaceToView(shareImageView,5)
    //    .heightIs(14);
    //
    //    [lblShare setSingleLineAutoResizeWithMaxWidth:100];
    //
    //    authorImageView.sd_layout
    //    .leftEqualToView(lblPostTime)
    //    .topSpaceToView(lblPostTime,20)
    //    .widthIs(32)
    //    .heightIs(32);
    //
    //    authorImageView.layer.masksToBounds = YES;
    //    authorImageView.layer.cornerRadius = 16;
    //
    //    lblAuthorName.sd_layout
    //    .topEqualToView(authorImageView)
    //    .leftSpaceToView(authorImageView,10)
    //    .heightIs(16);
    //
    //    [lblAuthorName setSingleLineAutoResizeWithMaxWidth:DeviceWidth-72];
    //
    //    lblAuthorInfo.sd_layout
    //    .topSpaceToView(lblAuthorName,3)
    //    .leftEqualToView(lblAuthorName)
    //    .heightIs(14);
    //
    //    [lblAuthorInfo setSingleLineAutoResizeWithMaxWidth:DeviceWidth-72];
    //
    //
    //    journalImageView.sd_layout
    //    .topSpaceToView(authorImageView,20)
    //    .leftSpaceToView(self,0)
    //    .rightSpaceToView(self,0)
    //    .heightIs(180);
    //
    //    lblJournalImageType.sd_layout
    //    .topSpaceToView(journalImageView,5)
    //    .leftSpaceToView(self,15)
    //    .rightSpaceToView(self,15)
    //    .autoHeightRatio(0);
    //
    //    [lblJournalImageType setMaxNumberOfLinesToShow:2];
    //
    //    lblJournalContent.sd_layout
    //    .topSpaceToView(lblJournalImageType,15)
    //    .leftEqualToView(lblJournalImageType)
    //    .rightEqualToView(lblJournalImageType)
    //    .autoHeightRatio(0);
    //
    //    [lblJournalContent setMaxNumberOfLinesToShow:0];
    
}

- (void)setJournal:(IMGMediaComment *)journal{
    _journal = journal;
    
    lblTitle.text = journal.title;
    UIFont *titleFont = [UIFont systemFontOfSize:24];
    CGSize titleSize = [journal.title sizeWithFont:titleFont constrainedToSize:CGSizeMake(DeviceWidth-30, 200) lineBreakMode:NSLineBreakByWordWrapping];
    lblTitle.frame = CGRectMake(15, 5, DeviceWidth-30, titleSize.height);
    
    lblPostTime.text = [Date getJournalTime:[journal.createTimeUnixTimestamp longLongValue]/1000];
    CGSize timeSize = [lblPostTime.text sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(DeviceWidth-30,15) lineBreakMode:NSLineBreakByWordWrapping];
    lblPostTime.frame = CGRectMake(15, lblTitle.endPointY+7, timeSize.width, 14);
    viewedImageView.frame = CGRectMake(lblPostTime.endPointX+10, lblTitle.endPointY+10, 12, 8);
    
    if ([journal.viewCount intValue] > 1) {
        viewedImageView.hidden = NO;
        lblViewed.text = [NSString stringWithFormat:@"%@ Views",journal.viewCount];
    }else if([journal.viewCount intValue] == 1){
        viewedImageView.hidden = NO;
        lblViewed.text = [NSString stringWithFormat:@"%@ View",journal.viewCount];
    }else{
        viewedImageView.hidden = YES;
        lblViewed.text = @"";
    }
    CGSize viewedSize = [lblViewed.text sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(DeviceWidth-30,15) lineBreakMode:NSLineBreakByWordWrapping];
    lblViewed.frame = CGRectMake(viewedImageView.endPointX+5, lblTitle.endPointY+7, viewedSize.width, 14);
    shareImageView.frame = CGRectMake(lblViewed.endPointX+10, lblTitle.endPointY+10, 12, 8);
    
    if ([journal.shareCount intValue] > 1) {
        shareImageView.hidden = NO;
        lblShare.text = [NSString stringWithFormat:@"%@ Shares",journal.shareCount];
    }else if([journal.shareCount intValue] == 1)
    {
        shareImageView.hidden = NO;
        lblShare.text = [NSString stringWithFormat:@"%@ Share",journal.shareCount];
    }else{
        shareImageView.hidden = YES;
        lblShare.text = @"";
    }
    CGSize shareSize = [lblShare.text sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(DeviceWidth-30,15) lineBreakMode:NSLineBreakByWordWrapping];
    lblShare.frame = CGRectMake(shareImageView.endPointX+5, lblTitle.endPointY+7, shareSize.width, 14);
    
    
    authorImageView.frame = CGRectMake(15, lblPostTime.endPointY+20, 32, 32);
    lblAuthorName.frame = CGRectMake(authorImageView.endPointX+10, lblPostTime.endPointY+20, DeviceWidth-72, 16);
    lblAuthorInfo.frame = CGRectMake(authorImageView.endPointX+10, lblAuthorName.endPointY+3, DeviceWidth-72, 14);
    
    NSString *authorStr = [journal.authorPhoto stringByReplacingPercentEscapesUsingEncoding:
                           NSUTF8StringEncoding];
    NSArray *strarray = [authorStr componentsSeparatedByString:@"?"];
    if (strarray.count>0) {
        authorStr = strarray[0];
    }
    
    NSURL *authorUrl;
    if ([authorStr hasPrefix:@"http://"]||[authorStr hasPrefix:@"https://"])
    {
        authorUrl = [NSURL URLWithString:_journal.authorPhoto];
    }
    else
        authorUrl = [NSURL URLWithString:[_journal.authorPhoto returnFullImageUrl]];
    
    [authorImageView sd_setImageWithURL:authorUrl placeholderImage:[UIImage imageNamed:@"headDefault.jpg"]];
    
    lblAuthorName.text = journal.authorName;
    lblAuthorInfo.text = journal.authorDescritpion;
    
    
    NSString *journalImageStr = [journal.journalImageUrl stringByReplacingPercentEscapesUsingEncoding:
                                 NSUTF8StringEncoding];
    
    if (journal.JournalGifH/2) {
        journal.JournalGifH =DeviceWidth*journal.JournalGifH/journal.JournalGifW;
        
        journalImageView.frame = CGRectMake(0, authorImageView.endPointY+20, DeviceWidth, journal.JournalGifH);
    }
    else{
        journalImageView.frame = CGRectMake(0, authorImageView.endPointY+20, DeviceWidth, DeviceWidth*0.67);
    }
    
    if ([journalImageStr hasPrefix:@"http://"]||[journalImageStr hasPrefix:@"https://"])
    {
        [journalImageView sd_setImageWithURL:[NSURL URLWithString:journalImageStr] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        }];
    }
    else
    {
        NSString *urlStr = [journalImageStr returnFullImageUrlWithWidth:DeviceWidth];
        
        float height= 0.0;
        if (_journal.JournalGifH > 0 && _journal.JournalGifW>0) {
            height = DeviceWidth*_journal.JournalGifH/_journal.JournalGifW;
            
        }else{
            height = 0.0;
        }
        
        if ([journalImageStr rangeOfString:@".gif"].location!=NSNotFound) {
            @try {
                [journalImageView sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    [UIView animateWithDuration:1.0 animations:^{
                        [journalImageView setAlpha:1.0];
                    }];
                }];
            } @catch (NSException *exception) {
                
            }
            
            
            
            UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, authorImageView.endPointY+20, DeviceWidth, height)];
            webView.backgroundColor=[UIColor clearColor];
            [webView setScalesPageToFit:YES];
            webView.scrollView.bounces = NO;
            webView.userInteractionEnabled = YES;
            webView.scrollView.showsHorizontalScrollIndicator = NO;
            webView.scrollView.showsVerticalScrollIndicator = NO;
            [self addSubview:webView];
            //[self bringSubviewToFront:journalImageView];
            webView.frame=journalImageView.frame;
            UIView * coverView=[[UIView alloc]initWithFrame:webView.bounds];
            coverView.backgroundColor=[UIColor clearColor];
            UITapGestureRecognizer * coverTap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(JournalRestaurantImageClick:)];
            coverTap.numberOfTapsRequired=1;
            coverTap.numberOfTouchesRequired=1;
            
            [coverView addGestureRecognizer:coverTap];
            
            [webView addSubview:coverView];
            
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_async(queue, ^{
                NSString *filePath =[NSString stringWithFormat:@"%@/%@.gif",qravedGIFCachePath,[_journal.imageUrl MD5String]];
                
                NSFileManager *fileManager = [NSFileManager defaultManager];
                NSData* gifData;
                if ([fileManager fileExistsAtPath:filePath]) {
                    gifData = [NSData dataWithContentsOfFile:filePath];
                }else{
                    gifData =[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",QRAVED_WEB_IMAGE_SERVER,_journal.imageUrl]]];
                    [gifData writeToFile:filePath  atomically:NO];
                }
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [webView loadData:gifData MIMEType:@"image/gif" textEncodingName:@"" baseURL:[NSURL URLWithString:@""]];
                    
                });
            });
        }else{
            
            UIImage *placeHoderImage =[UIImage imageNamed:DEFAULT_IMAGE_STRING];
            
            [journalImageView sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
            }];
        }
    }
    if (journal.photoCreditDic != nil) {
        lblJournalImageType.text = [NSString stringWithFormat:@"Photo source: %@",journal.photoCreditDic];
    }else{
        lblJournalImageType.text = @"";
    }
    
    lblJournalContent.text = journal.contents;
    
    CGSize journalTypeSize = [journal.photoCreditTypeDic sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(DeviceWidth-30, 100) lineBreakMode:NSLineBreakByWordWrapping];
    lblJournalImageType.frame = CGRectMake(15, journalImageView.endPointY+5, DeviceWidth-30, journalTypeSize.height);
    
    CGSize journalContentSize = [journal.contents sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(DeviceWidth-30, 1000) lineBreakMode:NSLineBreakByWordWrapping];
    lblJournalContent.frame = CGRectMake(15, lblJournalImageType.endPointY + 15, DeviceWidth-30, journalContentSize.height);
    if (journal.contents == nil || [journal.contents isEqualToString:@""]) {
        self.journalHeight = lblJournalImageType.endPointY;
    }else{
        self.journalHeight = lblJournalContent.endPointY;
    }
    
    //    [self setupAutoHeightWithBottomView:lblJournalContent bottomMargin:10];
    
    
}

- (void)JournalRestaurantImageClick:(UITapGestureRecognizer *)tap{
    if (self.imageClick) {
        self.imageClick();
    }
}

@end

