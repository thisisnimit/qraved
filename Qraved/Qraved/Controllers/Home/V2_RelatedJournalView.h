//
//  V2_RelatedJournalView.h
//  Qraved
//
//  Created by harry on 2017/7/7.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGMediaComment.h"
@interface V2_RelatedJournalView : UIView

@property (nonatomic, strong) NSArray *relatedArray;
@property (nonatomic, assign) CGFloat relatedHeight;

@property (nonatomic, copy) void (^gotoJournalDetail)(IMGMediaComment *currentJournal);

@end
