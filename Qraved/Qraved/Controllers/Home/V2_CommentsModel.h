//
//  V2_CommentsModel.h
//  Qraved
//
//  Created by harry on 2017/7/3.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGRestaurant.h"
@interface V2_CommentsModel : NSObject


@property (nonatomic,strong)NSNumber *isVendor;

@property (nonatomic,strong)NSArray *commentList;
@property (nonatomic,strong)IMGRestaurant *restaurant;

@end
