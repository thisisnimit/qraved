//
//  V2_CommentTableViewCell.h
//  Qraved
//
//  Created by harry on 2017/6/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_CommentModel.h"
@interface V2_CommentTableViewCell : UITableViewCell

@property (nonatomic, strong)V2_CommentModel *commentModel;



@end
