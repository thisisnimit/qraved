//
//  V2_CommentModel.h
//  Qraved
//
//  Created by harry on 2017/6/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface V2_CommentModel : NSObject


@property (nonatomic,strong)NSNumber *commentId;
@property (nonatomic,strong)NSString *content;
@property (nonatomic,strong)NSString *userImageUrl;
@property (nonatomic,strong)NSString *fullName;
@property (nonatomic,strong)NSNumber *createTime;

@end
