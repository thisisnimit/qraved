//
//  HomeViewController.h
//  Qraved
//
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "EScrollerView.h"
#import "HomeTableViewCell.h"
#import <CoreLocation/CoreLocation.h>
#import "CDPDelegate.h"
#import <ZDCChat/ZDCChat.h>

@interface HomeViewController : BaseViewController<UIScrollViewDelegate,UISearchBarDelegate,EScrollerViewDelegate,CLLocationManagerDelegate,UITableViewDelegate,UITableViewDataSource,HomeTableViewCellDelegate,CDPDelegate>

@end
