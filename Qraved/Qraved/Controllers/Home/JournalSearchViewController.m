//
//  JournalSearchViewController.m
//  Qraved
//
//  Created by Lucky on 15/7/30.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "JournalSearchViewController.h"
#import "GDataXMLNode.h"
#import "IMGMediaComment.h"
#import "MoreArticleViewController.h"

#import "UIViewController+Helper.h"
#import "IMGMediaComment.h"
#import "UIConstants.h"
#import "UIImage+Resize.h"
#import "UIImageView+WebCache.h"
#import "Label.h"
#import "UIView+Helper.h"
#import <CoreText/CoreText.h>
#import "UIColor+Helper.h"
#import "WebViewController.h"
#import "NSString+Helper.h"
#import "LoadingView.h"
#import "DBManager.h"
#import "UIConstants.h"
#import "JournalListTableViewCell.h"
#import "JournalHandler.h"
#import "JournalDetailViewController.h"
#import "UILabel+Helper.h"
#import "IMGUser.h"
#import "AppDelegate.h"
#import "IMGJournalCategory.h"

#define REFRESH_HEADER_HEIGHT 52.0f

#define REFRESH_LIST_COUNT 10

@interface JournalSearchViewController ()
{
    UIScrollView *_scrollView;
    BOOL _isLoad;
    
    UIButton *userBtn;
    
    UITableView *_tableView;
    NSMutableArray *_dataArrM;
    NSUInteger page;
    BOOL _hasData;
    
    NSString *currentTab;
    
    UIImageView *_isBtnImageView;
    
    UIScrollView *_searchView;

    UITableView *_categoryTableView;
    UIView *_backgroundView;
    UILabel *_currentCategoryNameLabel;
}
@end

@implementation JournalSearchViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token && userBtn)
    {
        [userBtn removeFromSuperview];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    page=1;
    _hasData=1;
    self.navigationController.navigationBarHidden = NO;
    
    UILabel *navTitleLabel = [[UILabel alloc] init];
    navTitleLabel.textColor = [UIColor whiteColor];
    navTitleLabel.text = L(@"Qraved Journal");
    navTitleLabel.font = [UIFont boldSystemFontOfSize:20];
    navTitleLabel.frame = CGRectMake(DeviceWidth/2-navTitleLabel.expectedWidth/2, 2, navTitleLabel.expectedWidth, 38);
    self.navigationItem.titleView = navTitleLabel;
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    //self.navigationController.title = self.title;
    [self setBackBarButtonOffset30];
    
    [[LoadingView sharedLoadingView] startLoading];
    [self addMainView];
    if (self.category)
    {
        [[LoadingView sharedLoadingView] startLoading];
        NSDictionary *paramsDic = [[NSDictionary alloc] init];
        paramsDic = @{@"category":self.category.categoryType};
        [JournalHandler getJournalListWithParams:paramsDic andBlock:^(NSArray *dataArray,int totalCount) {

            [_dataArrM removeAllObjects];
            _dataArrM = [NSMutableArray arrayWithArray:dataArray];
            [_tableView reloadData];
            [[LoadingView sharedLoadingView] stopLoading];
            if (_dataArrM.count < totalCount) {
                _hasData=YES;
            }else{
                _hasData=NO;
            }
            
        } failure:^(NSError *error) {
            
        }];
 
    }
}
- (void)addMainView
{
    
    
    //    UIEdgeInsets ed = {1.0f, 0.0f, 0.0f, 0.0f};
    //
    //    _isBtnImageView.image = [[UIImage imageNamed:@"tab"] resizableImageWithCapInsets:ed];
    
    //    [ImageView setImage:[[UIImage imageNamed:@"xxx.png"]resizableImageWithCapInsets:ed]];
    
    
    IMGUser * user = [IMGUser currentUser];
    if(user.userId==nil || user.token == nil)
    {
        userBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        userBtn.frame = CGRectMake(DeviceWidth - LEFTLEFTSET - 70, 5, 40, 40);
        [userBtn setImage:[UIImage imageNamed:@"account"] forState:UIControlStateNormal];
        [self.view addSubview:userBtn];
        userBtn.tag = 102;
        [userBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    _currentCategoryNameLabel = [[UILabel alloc] init];
    _currentCategoryNameLabel.text = self.category.categoryName;
    _currentCategoryNameLabel.frame = CGRectMake(LEFTLEFTSET, 15, DeviceWidth/2+80, 20);
    _currentCategoryNameLabel.textColor = [UIColor grayColor];
    _currentCategoryNameLabel.font = [UIFont systemFontOfSize:13];
    _currentCategoryNameLabel.alpha = 0.7f;
    [self.view addSubview:_currentCategoryNameLabel];
    [self addLineImage:_currentCategoryNameLabel.endPointY+11 withCurrentView:self.view];
    
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchBtn setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
    searchBtn.frame = CGRectMake(DeviceWidth - LEFTLEFTSET - 30, 5, 40, 40);
    [self.view addSubview:searchBtn];
    [searchBtn addTarget:self action:@selector(searchClick) forControlEvents:UIControlEventTouchUpInside];
    
    _tableView = [[UITableView alloc] init];
    _tableView.frame = CGRectMake(0, _currentCategoryNameLabel.endPointY+11, DeviceWidth, DeviceHeight - _tableView.frame.origin.y - 44 - 44);
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tag = 100;
    _tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    [self.view addSubview:_tableView];
}

- (void)searchClick
{
    [JournalHandler getJournalCategoryWithBlock:^(NSArray *dataArray) {
        _categoryDataArrM = [NSMutableArray arrayWithArray:dataArray];
        [self addSearchView];
    }];
}
- (void)addSearchView
{
    
    _backgroundView = [[UIView alloc] init];
    _backgroundView.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight);
    [self.view addSubview:_backgroundView];
    _backgroundView.alpha = 0.5f;
    _backgroundView.backgroundColor = [UIColor blackColor];
    _backgroundView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundViewClick)];
    tapGesture.numberOfTouchesRequired = 1;
    tapGesture.numberOfTapsRequired = 1;
    [_backgroundView addGestureRecognizer:tapGesture];
    
    
    _searchView = [[UIScrollView alloc] init];
    _searchView.frame = CGRectMake(0, 0, DeviceWidth, 244+44);
    _searchView.backgroundColor = [UIColor whiteColor];
    _searchView.contentSize = CGSizeMake(DeviceWidth+1, 0);
    _searchView.pagingEnabled = YES;
    [self.view addSubview:_searchView];
    
    UILabel *label = [[UILabel alloc] init];
    label.text = L(@"Looking for somethings");;
    label.frame = CGRectMake(LEFTLEFTSET, 12, DeviceWidth/2+80, 20);
    label.textColor = [UIColor grayColor];
    label.font = [UIFont systemFontOfSize:13];
    label.alpha = 0.7f;
    [_searchView addSubview:label];
    [self addLineImage:label.endPointY+11 withCurrentView:_searchView];
    
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchBtn setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
//    searchBtn.frame = CGRectMake(DeviceWidth - LEFTLEFTSET - 20, 12, 20, 20);
    searchBtn.frame = CGRectMake(DeviceWidth - LEFTLEFTSET - 30, 5, 40, 40);
    [_searchView addSubview:searchBtn];
    
    
    _categoryTableView = [[UITableView alloc] init];
    _categoryTableView.tag = 101;
    _categoryTableView.delegate = self;
    _categoryTableView.dataSource = self;
    _categoryTableView.frame = CGRectMake(0, 44, DeviceWidth, 200+44);
    [_searchView addSubview:_categoryTableView];
    
}
- (void)backgroundViewClick
{
    [_backgroundView removeFromSuperview];
    [_searchView removeFromSuperview];
}
- (void)btnClick:(UIButton *)btn
{
    switch (btn.tag)
    {
        case 102:
        {
            LoginViewController *lvc = [[LoginViewController alloc] init];
            lvc.isSignUpSplash = YES;
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:lvc];
            [self.navigationController presentViewController:nav animated:NO completion:^{
            
            }];
        }
            break;
            
        default:
            break;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 100)
    {
        static NSString *identifier =@"cell";
        JournalListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[JournalListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cell.delegate = self;
        [cell setJournal:[_dataArrM objectAtIndex:indexPath.row]];
        return cell;
    }
    else
    {
        static NSString *cellName = @"cellName";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
        if(cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellName];
        }
        IMGJournalCategory *category = [_categoryDataArrM objectAtIndex:indexPath.row];
        cell.textLabel.text = category.categoryName;
        return cell;
        
    }
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView.tag == 100)
    {
        return  _dataArrM.count;
    }
    else
    {
        return _categoryDataArrM.count;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 100)
    {
        return 220;
    }
    else
    {
        return 40;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 100)
    {
        IMGMediaComment *journal = [_dataArrM objectAtIndex:indexPath.row];
        if ([journal.imported intValue]== 1)
        {
            [JournalHandler getJournalDetailWithJournalArticleId:journal.mediaCommentId andBlock:^(NSString *content,NSString *webSite) {
                journal.contents = content;
                WebViewController *wvc = [[WebViewController alloc] init];
                wvc.content = journal.contents;
                wvc.journal = journal;
                wvc.webSite = webSite;
                wvc.title = journal.title;
                wvc.fromDetail = NO;
                [self.navigationController pushViewController:wvc animated:YES];
            }];
            
        }
        else
        {
            JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
            jdvc.journal = journal;
            jdvc.categoryDataArrM = [NSMutableArray arrayWithArray:_categoryDataArrM];
            [self.navigationController pushViewController:jdvc animated:YES];
        }
    }
    else
    {
        [self backgroundViewClick];
        self.category = [_categoryDataArrM objectAtIndex:indexPath.row];
        _currentCategoryNameLabel.text = self.category.categoryName;
        [[LoadingView sharedLoadingView] stopLoading];
        NSDictionary *paramsDic = [[NSDictionary alloc] init];
        paramsDic = @{@"category":self.category.categoryType};
        [JournalHandler getJournalListWithParams:paramsDic andBlock:^(NSArray *dataArray,int totalCount) {
            
            [_dataArrM removeAllObjects];
            _dataArrM = [NSMutableArray arrayWithArray:dataArray];
            [_tableView reloadData];
            [[LoadingView sharedLoadingView] stopLoading];
            if (_dataArrM.count < totalCount) {
                _hasData=YES;
            }else{
                _hasData=NO;
            }
        } failure:^(NSError *error) {
            
        }];
    }
    
}
- (void)categoryNameLabelClick:(NSString *)categoryName
{
    for (IMGJournalCategory *category in _categoryDataArrM)
    {
        if ([category.categoryName isEqualToString:categoryName])
        {

            self.category = category;
            _currentCategoryNameLabel.text = self.category.categoryName;
            [[LoadingView sharedLoadingView] startLoading];
            NSDictionary *paramsDic = [[NSDictionary alloc] init];
            paramsDic = @{@"category":self.category.categoryType};
            [JournalHandler getJournalListWithParams:paramsDic andBlock:^(NSArray *dataArray,int totalCount) {
                
                [_dataArrM removeAllObjects];
                _dataArrM = [NSMutableArray arrayWithArray:dataArray];
                [_tableView reloadData];
                [[LoadingView sharedLoadingView] stopLoading];
                if (_dataArrM.count < totalCount) {
                    _hasData=YES;
                }else{
                    _hasData=NO;
                }
            } failure:^(NSError *error) {
                
            }];
            break;
        }
    }
}
- (void)addLineImage:(CGFloat)currentPointY withCurrentView:(UIView *)currentView
{
    UIImageView *lineImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, currentPointY-1, DeviceWidth, 1)];
    lineImage3.image = [UIImage imageNamed:@"CutOffRule"];
    currentPointY = lineImage3.endPointY;
    [currentView addSubview:lineImage3];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
