//
//  ImageListViewController.h
//  Qraved
//
//  Created by DeliveLee on 16/3/9.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDPDelegate.h"
#import "TYAttributedLabel.h"
@class IMGRestaurant;
@class IMGUser;
@class IMGReview;


@protocol ImageListViewControllerDelegate
@optional
- (void)gotoRestaurantWithRestaurantId:(NSNumber *)restaurantId;
- (void)gotoOtherProfileWithUser:(IMGUser *)user;

@end


@interface ImageListViewController : UIViewController<CDPDelegate,TYAttributedLabelDelegate>

@property (nonatomic,assign) id<ImageListViewControllerDelegate>delegate;
@property (nonatomic,assign) id<CDPDelegate>cdpDelegate;

-(instancetype)initWithRestaurant:(IMGRestaurant *)_restaurant andDishArr:(NSArray *)dishArr andIndex:(int)index andFromCellIndexPath:(NSIndexPath*)fromCellIndexPath;
-(instancetype)initWithRestaurant:(IMGRestaurant*)_restaurant andUser:(IMGUser *)_user andDishArr:(NSArray *)_dishArr andIndex:(int)index andFromCellIndexPath:(NSIndexPath*)fromCellIndexPath;
-(instancetype)initWithRestaurant:(IMGRestaurant*)_restaurant andReview:(IMGReview *)review andDishArr:(NSArray *)_dishArr andIndex:(int)index andFromCellIndexPath:(NSIndexPath*)fromCellIndexPath;

@property (nonatomic,strong) NSIndexPath *fromCellIndexPath;
@property (nonatomic,assign) BOOL isfromNotification;
@property (nonatomic,retain) NSNumber* moderateId;



@end
