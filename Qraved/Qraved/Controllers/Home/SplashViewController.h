//
//  SplashViewController.h
//  Qraved
//
//  Created by Lucky on 15/11/4.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "IMGSplash.h"

@protocol SplashViewControllerDelegate <NSObject>

- (void)btnClickWithSplash:(IMGSplash *)splash;

@end

@interface SplashViewController : UIViewController

- (id)initWithSplash:(IMGSplash *)splash;

@property (nonatomic,assign) id<SplashViewControllerDelegate>svcDelegate;
@property (nonatomic)        NSNumber *splashIdFromNotification;
@property (nonatomic,assign) BOOL isFromHome;
@end
