//
//  V2_JournalCell.m
//  Qraved
//
//  Created by harry on 2017/7/5.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_JournalCell.h"

@implementation V2_JournalCell
{
    UIImageView *journalImageView;
    UILabel *lblTitle;
    UIImageView *videoImageView;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    
    self.contentView.backgroundColor = [UIColor whiteColor];
    journalImageView = [UIImageView new];
    journalImageView.contentMode = UIViewContentModeScaleAspectFill;
    journalImageView.clipsToBounds = YES;
    
    lblTitle = [UILabel new];
    lblTitle.font = [UIFont systemFontOfSize:14];
    lblTitle.textColor = [UIColor color333333];
    
    [self.contentView sd_addSubviews:@[journalImageView,lblTitle]];
    
    videoImageView = [UIImageView new];
    videoImageView.image = [UIImage imageNamed:@"ic_journal_video.png"];
    [journalImageView addSubview:videoImageView];
    videoImageView.hidden = YES;
    
    journalImageView.sd_layout
    .topSpaceToView(self.contentView,0)
    .leftSpaceToView(self.contentView,0)
    .widthIs((DeviceWidth-45)/2)
    .heightIs(((DeviceWidth-45)/2) * .67);
    
    lblTitle.sd_layout
    .topSpaceToView(journalImageView,5)
    .leftEqualToView(journalImageView)
    .rightEqualToView(journalImageView)
    .autoHeightRatio(0);
    
    [lblTitle setMaxNumberOfLinesToShow:2];
    
    videoImageView.sd_layout
    .topSpaceToView(journalImageView, 12)
    .rightSpaceToView(journalImageView, 12)
    .widthIs(27)
    .heightIs(19);
}

- (void)setMediaComment:(IMGMediaComment *)mediaComment{
    _mediaComment = mediaComment;
    
    if ([mediaComment.journalImageUrl hasPrefix:@"http://"]||[mediaComment.journalImageUrl hasPrefix:@"https://"])
    {
        UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 160)];
        [journalImageView sd_setImageWithURL:[NSURL URLWithString:mediaComment.journalImageUrl] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        }];
    }
    else
    {
        NSString *urlStr = [mediaComment.journalImageUrl returnFullImageUrlWithWidth:(DeviceWidth-45)/2];
        UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake((DeviceWidth-45)/2, ((DeviceWidth-45)/2) * .67)];
        
        [journalImageView sd_setImageWithURL:[NSURL URLWithString:urlStr] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        }];
        
    }
    
    lblTitle.text = mediaComment.journalTitle;
    
    if ([mediaComment.hasVideo isEqual:@1]) {
        videoImageView.hidden = NO;
    }else{
        videoImageView.hidden = YES;
    }
}


@end
