//
//  ViewController.h
//  QuickStart
//
//  Created by Abir Majumdar on 12/3/14.
//  Copyright (c) 2014 Layer, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <LayerKit/LayerKit.h>
#import "UIConstants.h"

@interface LayerViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,UITextFieldDelegate>

@property (strong, nonatomic) UIButton *sendBtn;
@property (nonatomic, retain) UITextView *inputTextView;

@property (nonatomic, retain) LYRClient *layerClient;
@property (nonatomic) LYRConversation *conversation;
@property (retain, nonatomic) UITableView *tableView;

@property (nonatomic, retain) UILabel *typingIndicatorLabel;

@property (nonatomic, retain) LYRQueryController *queryController;

- (void)logMessage:(NSString*) messageText;

@end

