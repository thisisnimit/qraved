//
//  V2_CommentTableViewCell.m
//  Qraved
//
//  Created by harry on 2017/6/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_CommentTableViewCell.h"
#import "Date.h"
@implementation V2_CommentTableViewCell
{
    UIImageView *userImageView;
    UILabel *lblContent;
    UILabel *lblTime;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
    if(self=[super initWithStyle:style reuseIdentifier:identifier]){
        
        
        [self createUI];
    }
    return self;
}

- (void)createUI{
    userImageView = [UIImageView new];
    
    lblContent = [UILabel new];
    lblContent.font = [UIFont systemFontOfSize:14];
    lblContent.textColor = [UIColor color333333];
    
    lblTime = [UILabel new];
    lblTime.font = [UIFont systemFontOfSize:12];
    lblTime.textColor = [UIColor color999999];
    
    UIView *bottomLine = [UIView new];
    bottomLine.backgroundColor = [UIColor colorEBEBEB];
    
    [self.contentView sd_addSubviews:@[userImageView,lblContent,lblTime,bottomLine]];
    
    userImageView.sd_layout
    .topSpaceToView(self.contentView, 15)
    .leftSpaceToView(self.contentView, 15)
    .widthIs(48)
    .heightIs(48);
    userImageView.layer.masksToBounds = YES;
    userImageView.layer.cornerRadius = 24;
    
    lblContent.sd_layout
    .topEqualToView(userImageView)
    .leftSpaceToView(userImageView, 10)
    .widthIs(DeviceWidth-88)
    .autoHeightRatio(0);
    
    //[lblContent setMaxNumberOfLinesToShow:0];
    
    lblTime.sd_layout
    .topSpaceToView(lblContent, 5)
    .leftEqualToView(lblContent)
    .widthIs(DeviceWidth-88)
    .heightIs(14);
    
    bottomLine.sd_layout
    .topSpaceToView(lblTime, 15)
    .leftEqualToView(lblTime)
    .heightIs(1)
    .widthIs(DeviceWidth-25+48);
}

- (void)setCommentModel:(V2_CommentModel *)commentModel
{
    _commentModel = commentModel;
    
    UIImage *placeHolder = [UIImage imageNamed:@"headDefault.jpg"];
    [userImageView sd_setImageWithURL:[NSURL URLWithString:[commentModel.userImageUrl returnFullImageUrl]] placeholderImage:placeHolder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
    lblContent.text = [NSString stringWithFormat:@"%@ %@",commentModel.fullName,commentModel.content];
    
    lblTime.text = [Date getTimeInteval_v5:[commentModel.createTime longLongValue]/1000];
    
    [self setupAutoHeightWithBottomView:lblTime bottomMargin:16];
}

@end
