//
//  HomeJournalView.h
//  Qraved
//
//  Created by Lucky on 15/9/8.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGMediaComment.h"
#import "PPLabel.h"

@interface HomeJournalView : UIView

@property (nonatomic,retain,readonly) UIImageView *photoImageView;

@property (nonatomic,retain,readonly) UILabel *titleLabel;

@property (nonatomic,retain,readonly) PPLabel *categoryNameLabel;

@property(nonatomic) NSRange highlightedRange;


- (void)setJournal:(IMGMediaComment *)journal;


@end
