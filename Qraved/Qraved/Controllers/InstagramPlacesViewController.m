//
//  InstagramPlacesViewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/8/16.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "InstagramPlacesViewController.h"
#import "LoadingView.h"
@interface InstagramPlacesViewController ()<UIWebViewDelegate>

@end

@implementation InstagramPlacesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Instagram Places";
    [self addBackBtn];
    self.view.backgroundColor = [UIColor whiteColor];
    [self initUI];
    
   
    [[Amplitude instance] logEvent:@"RC - Instagram Places List" withEventProperties:@{}];
}

- (void)initUI{
    
    
    
    UIWebView *webv = [[UIWebView alloc] initWithFrame:CGRectMake(15, 0, DeviceWidth-30, DeviceHeight-64)];
    webv.backgroundColor = [UIColor whiteColor];
    webv.delegate = self;
    NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://www.instagram.com/explore/locations/%@",self.instagramLocationId]]];
    [webv loadRequest:request];
    [self.view addSubview:webv];
    
    
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
     [[LoadingView sharedLoadingView] startLoading];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
   [[LoadingView sharedLoadingView] stopLoading];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
}

@end
