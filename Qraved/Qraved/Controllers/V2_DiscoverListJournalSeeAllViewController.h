//
//  V2_DiscoverListJournalSeeAllViewController.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/24.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "DiscoverCategories.h"

@interface V2_DiscoverListJournalSeeAllViewController : BaseChildViewController

@property (nonatomic, strong)  NSString  *keyword;

@property (nonatomic, strong) NSArray *foodTagArr;
@property (nonatomic, assign) DiscoverCategoriesModel *foodModel;
@property (nonatomic, assign) NSNumber *quicklySearchFoodTagId;

@end
