//
//  EmailLoginViewController.h
//  PocketPlan
//
//  Created by Libo Liu on 13-11-12.
//  Copyright (c) 2013年 com.imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
@interface EmailLoginViewController : BaseChildViewController<UITextFieldDelegate>


//@property (nonatomic, retain) UIButton *forgetPasswordBtn;
@property(nonatomic,strong)LoginViewController *loginViewController;
@property(nonatomic,assign)BOOL isfromBookingInfo;

@end
