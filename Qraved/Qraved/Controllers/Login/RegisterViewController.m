//
//  RegisterViewController.m
//  Qraved
//
//  Created by Lucky on 15/5/27.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "RegisterViewController.h"
#import "AppDelegate.h"
#import "PushHelper.h"
#import <GoogleAnalytics/GAIFields.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import <CoreText/CoreText.h>
#import "IMGExceptionAlertView.h"
#import "UIConstants.h"

#import "EmailloginViewController.h"
#import "ResetPasswordViewController.h"
#import "EmailRegisterViewController.h"

#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "UIViewController+Helper.h"
#import "UIDevice+Util.h"
#import "IMGUser.h"
#import "LoadingView.h"

#import "UIAlertView+BlocksKit.h"
#import "loginParamter.h"
#import "DBManager.h"
#import "FBSignUpViewController.h"
#import "UpdateUserInfoViewController.h"
#import "SelectCityController.h"
#import "CityUtils.h"
#import "IMGCity.h"

@interface RegisterViewController ()<SighUpDelegate,SHKOAuthViewDelegate,UITextFieldDelegate>
{
    UIImageView *bgImageView;
    NSNumber *fromWhere;
    UIControl *control;
    CGRect oriFrame;
    UIImageView *emailImageView ;
    UIImageView *passwordImageView;
    CLLocationManager *clmanager;
}

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"Sign Up form page";
    [self.navigationController.navigationBar setHidden:YES];
    
    bgImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+20)];
    bgImageView.userInteractionEnabled = YES;
    [self.view addSubview:bgImageView];
    if([UIDevice isLaterThanIphone5]){
        bgImageView.image = [UIImage imageNamed:@"LoginBg5.jpg"];
    }else{
        bgImageView.image = [UIImage imageNamed:@"LoginBg4.jpg"];
    }
    [self loadMainView];
    [self loadEmailMainView];
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(DeviceWidth-45, 20, 45, 45)];
    [closeButton setImage:[UIImage imageNamed:@"closeWhite" ] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeButton];
    [self.view bringSubviewToFront:closeButton];
}

-(void)loadMainView
{
#pragma mark logo+title logoImage logoLabel
    UIImageView *logoImage = [[UIImageView alloc]initWithFrame:CGRectMake((DeviceWidth-198.67)/2, 16, 198.67, 108)];
    logoImage.image = [UIImage imageNamed:@"qraved_new_logo"];
    [bgImageView addSubview:logoImage];
    
    
    
#pragma mark 登录按钮 fblogupButton twlogupButton emaillogupButton
    self.fblogupButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.fblogupButton.frame = CGRectMake(36, logoImage.endPointY + 15, DeviceWidth-72, 46*[AppDelegate ShareApp].autoSizeScaleY);
    [self.fblogupButton setBackgroundImage:[UIImage imageNamed:@"BookFlowFacebookBtn"] forState:UIControlStateNormal];

    [self.fblogupButton addTarget:self action:@selector(facebooklogup:) forControlEvents:UIControlEventTouchUpInside];
    
        UILabel *fbLoginLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, (self.fblogupButton.frame.size.height - 25)/2.0, DeviceWidth-72, 25)];
        fbLoginLabel.text = L(@"Sign up with Facebook");
        fbLoginLabel.backgroundColor = [UIColor clearColor];
        fbLoginLabel.font =[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:14*[AppDelegate ShareApp].autoSizeScaleY];
        fbLoginLabel.textColor = [UIColor whiteColor];
        fbLoginLabel.textAlignment = NSTextAlignmentCenter;
        [self.fblogupButton addSubview:fbLoginLabel];
   

    [bgImageView addSubview:self.fblogupButton];
    
    
    self.twlogupButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.twlogupButton.frame = CGRectMake(36, self.fblogupButton.endPointY+10, DeviceWidth-72, 46*[AppDelegate ShareApp].autoSizeScaleY);
    [self.twlogupButton setBackgroundImage:[UIImage imageNamed:@"BookFlowTwitterBtn"] forState:UIControlStateNormal];
    [self.twlogupButton addTarget:self action:@selector(twitterlogup:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *twLoginLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, (self.twlogupButton.frame.size.height - 25)/2.0, DeviceWidth-72, 25)];
    twLoginLabel.text = L(@"Sign up with Twitter");
    twLoginLabel.backgroundColor = [UIColor clearColor];
    twLoginLabel.font =[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:14*[AppDelegate ShareApp].autoSizeScaleY];
    twLoginLabel.textColor = [UIColor whiteColor];
    twLoginLabel.textAlignment = NSTextAlignmentCenter;
    [self.twlogupButton addSubview:twLoginLabel];

    [bgImageView addSubview:self.twlogupButton];
    
//    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(touchBg:)];
//    [bgImageView addGestureRecognizer:tapGesture];
    
    
    UILabel *textLabel = [[UILabel alloc] init];
    textLabel.text = L(@"We'll never post without your permission");
    textLabel.frame = CGRectMake(self.twlogupButton.frame.origin.x, self.twlogupButton.endPointY + 17, self.twlogupButton.frame.size.width, 12);
    [bgImageView addSubview:textLabel];
    textLabel.font = [UIFont systemFontOfSize:10];
    textLabel.textColor = [UIColor whiteColor];
    textLabel.alpha = 0.7f;
    
    UILabel *lineLabel_1 = [[UILabel alloc] init];
    lineLabel_1.frame = CGRectMake(self.twlogupButton.frame.origin.x, self.twlogupButton.endPointY+40, self.twlogupButton.frame.size.width/2-12, 1);
    lineLabel_1.alpha = 0.7f;
    lineLabel_1.backgroundColor = [UIColor whiteColor];
    [bgImageView addSubview:lineLabel_1];
    
    UILabel *orLabel = [[UILabel alloc] init];
    orLabel.text = L(@"Or");
    orLabel.font = [UIFont systemFontOfSize:12];
    orLabel.textColor = [UIColor whiteColor];
    orLabel.alpha = 0.7f;
    orLabel.frame = CGRectMake(lineLabel_1.endPointX, self.twlogupButton.endPointY+33, 24, 12);
    [bgImageView addSubview:orLabel];
    orLabel.textAlignment = NSTextAlignmentCenter;
    
    UILabel *lineLabel_2 = [[UILabel alloc] init];
    lineLabel_2.frame = CGRectMake(orLabel.endPointX, lineLabel_1.frame.origin.y, self.twlogupButton.frame.size.width/2-12, 1);
    lineLabel_2.alpha = 0.7f;
    lineLabel_2.backgroundColor = [UIColor whiteColor];
    [bgImageView addSubview:lineLabel_2];
    
    
    
     self.emaillogupButton = [UIButton buttonWithType:UIButtonTypeCustom];
     self.emaillogupButton.frame = CGRectMake(36, lineLabel_2.endPointY+17, DeviceWidth-72, 46*[AppDelegate ShareApp].autoSizeScaleY);
     [self.emaillogupButton setBackgroundImage:[UIImage imageNamed:@"EmailLogupBtnNew"] forState:UIControlStateNormal];
     [self.emaillogupButton addTarget:self action:@selector(emaillogup:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *emailLoginLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, (self.emaillogupButton.frame.size.height - 25)/2.0, DeviceWidth-72, 25)];
    emailLoginLabel.text = L(@"Sign up with email");
    emailLoginLabel.backgroundColor = [UIColor clearColor];
    emailLoginLabel.font =[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:14*[AppDelegate ShareApp].autoSizeScaleY];
    emailLoginLabel.textColor = [UIColor whiteColor];
    emailLoginLabel.textAlignment = NSTextAlignmentCenter;
    [self.emaillogupButton addSubview:emailLoginLabel];
     [bgImageView addSubview:self.emaillogupButton];
     
     
    
     #pragma mark 注册按钮 registerBtn
     NSString *registerStr = L(@"Already a Qraved member? ");
     CGSize expectSize = [registerStr sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13]];
    CGSize stringSize = [@"Already a Qraved member? Sign in" sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13]];
     Label *registLabel = [[Label alloc]initWithFrame:CGRectMake((DeviceWidth-stringSize.width)/2, self.emaillogupButton.endPointY+15, expectSize.width, expectSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor whiteColor] andTextLines:1];
     registLabel.text = registerStr;
     [bgImageView addSubview:registLabel];
     self.registerBtn = [[Label alloc]initWithFrame:CGRectMake(registLabel.endPointX+1, self.emaillogupButton.endPointY+15, DeviceWidth-registLabel.endPointX, expectSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor colorFFC000] andTextLines:1];
     self.registerBtn.text = L(@"Sign in");
     [bgImageView addSubview:self.registerBtn];
     
     UITapGestureRecognizer *emailTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(jumpSignInViewController)];
     [self.registerBtn addGestureRecognizer:emailTapGesture];
    
     
}

- (void)jumpSignInViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)loadEmailMainView
{
    
}

- (void)twitterlogup:(UIButton*)sender{
    sender.userInteractionEnabled=NO;
    [[Amplitude instance] logEvent:@"SU - Sign Up Initiate" withEventProperties:@{@"Sing Up Type":@"Twitter"}];
    [[NSUserDefaults standardUserDefaults]setObject:@"login" forKey:TWITTERNOTIFICATIONTYPE];
    [SHK setRootViewController:self];
    SHKTwitter *twitter = [[SHKTwitter alloc] initWithNextDelegate:self];
    [twitter authorize];
    [self performSelector:@selector(senderEnabel:) withObject:sender afterDelay:5.0f];

}
-(void)senderEnabel:(UIButton*)sender{
    sender.userInteractionEnabled = YES;
    
}


-(void)logupWithTwitterInQraved:(NSNotification *)notification{
    [[LoadingView sharedLoadingView] startLoading];
    NSDictionary *twitterResult = [notification.userInfo objectForKey:@"twitterResult"];
    NSError *error = (NSError *)[notification.userInfo objectForKey:@"error"];
    [[LoadingView sharedLoadingView]startLoading];
    UserDataHandler *userDataController = [[UserDataHandler alloc] init];
    [userDataController twitterLogin:twitterResult withError:error withDelegateBlock: ^(){
         
        NSDictionary *dataDic = [[NSDictionary alloc] initWithDictionary:[userDataController getParameters]];
        if ([userDataController isSignUp])
        {
            if(self.loginViewController!=nil){
                [self.loginViewController loginSuccessful];
            }else{
                [AppDelegate ShareApp].userLogined=YES;
                [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
                BOOL locationEnabled=[CLLocationManager locationServicesEnabled];
                if(locationEnabled){
                    clmanager=[[CLLocationManager alloc] init];
                    clmanager.delegate=self;
                    clmanager.distanceFilter=1000;
                    clmanager.desiredAccuracy=kCLLocationAccuracyKilometer;
                    CLAuthorizationStatus authStatus=[CLLocationManager authorizationStatus];
                    if(authStatus==kCLAuthorizationStatusAuthorizedAlways || authStatus==kCLAuthorizationStatusAuthorizedAlways || authStatus==kCLAuthorizationStatusAuthorizedWhenInUse){
                        [clmanager startUpdatingLocation];
                    }else{
                        [self selectCity];
                    }
                }else{
                    [self selectCity];
                }
            }
        }
        else
        {
            FBSignUpViewController *fbsuvc = [[FBSignUpViewController alloc] init];
            fbsuvc.dataDic = dataDic;
            fbsuvc.amplitudeType = @"Twitter";
            [self.navigationController pushViewController:fbsuvc animated:YES];
        }
    }failedBlock:^{
        [[LoadingView sharedLoadingView] stopLoading];
        [self enterEmail];
    }];
}

#pragma mark Facebook登录
- (void)facebooklogup:(id)sender{
    NSArray *permissions = [[NSArray alloc] initWithObjects:@"email",nil];
    [[Amplitude instance] logEvent:@"SU - Sign Up Initiate" withEventProperties:@{@"Sign Up Type":@"Facebook"}];
//    [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
//        [[Amplitude instance] logEvent:@"SU - Sign Up Succeed" withEventProperties:@{@"Sign Up Type":@"Facebook",@"Reason":@"Authorized FB"}];
//        [[AppsFlyerTracker sharedTracker] trackEvent:@"SU - Sign Up Succeed" withValues:@{@"Sign Up Type":@"Facebook",@"Reason":@"Authorized FB"}];
//
//        UserDataHandler *userDataHandler = [[UserDataHandler alloc] init];
//        [userDataHandler faceBookLogin:session state:state error:error delegateWithBlock:^{
//            [[LoadingView sharedLoadingView] stopLoading];
//            NSDictionary *dataDic = [[NSDictionary alloc] initWithDictionary:[userDataHandler getParameters]];
//            if ([userDataHandler isSignUp])
//            {
//                if(self.loginViewController!=nil){
//                    [self.loginViewController loginSuccessful];
//                }else{
//                    [AppDelegate ShareApp].userLogined=YES;
//                    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
//                    [[AppDelegate ShareApp] requestToShowNotificationCount];
//                    [self dismissViewControllerAnimated:YES completion:nil];
//                }
//            }
//            else
//            {
//                FBSignUpViewController *fbsuvc = [[FBSignUpViewController alloc] init];
//                fbsuvc.dataDic = dataDic;
//                fbsuvc.isFromBooking = NO;
//                [self.navigationController pushViewController:fbsuvc animated:YES];
//            }
//            
//        }  failedBlock:^{
//            [[Amplitude instance] logEvent:@"SI - Sign In Failed" withEventProperties:@{@"Reason":@"Authorized FB",@"Sign In Type":@"Facebook"}];
//
//            [self enterEmail];
//        }];
//    }];
//    [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:YES fromViewController:self completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
//        [[Amplitude instance] logEvent:@"SU - Sign Up Succeed" withEventProperties:@{@"Sign Up Type":@"Facebook",@"Reason":@"Authorized FB"}];
//                [[AppsFlyerTracker sharedTracker] trackEvent:@"SU - Sign Up Succeed" withValues:@{@"Sign Up Type":@"Facebook",@"Reason":@"Authorized FB"}];
//        
//                UserDataHandler *userDataHandler = [[UserDataHandler alloc] init];
//                [userDataHandler faceBookLogin:session state:status error:error delegateWithBlock:^{
//                    [[LoadingView sharedLoadingView] stopLoading];
//                    NSDictionary *dataDic = [[NSDictionary alloc] initWithDictionary:[userDataHandler getParameters]];
//                    if ([userDataHandler isSignUp])
//                    {
//                        if(self.loginViewController!=nil){
//                            [self.loginViewController loginSuccessful];
//                        }else{
//                            [AppDelegate ShareApp].userLogined=YES;
//                            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
//                            [[AppDelegate ShareApp] requestToShowNotificationCount];
//                            [self dismissViewControllerAnimated:YES completion:nil];
//                            [[NSNotificationCenter defaultCenter] postNotificationName:@"loginNotification" object:nil];
//
//                        }
//                    }
//                    else
//                    {
//                        FBSignUpViewController *fbsuvc = [[FBSignUpViewController alloc] init];
//                        fbsuvc.dataDic = dataDic;
//                        fbsuvc.isFromBooking = NO;
//                        [self.navigationController pushViewController:fbsuvc animated:YES];
//                    }
//        
//                }  failedBlock:^{
//                    [[Amplitude instance] logEvent:@"SI - Sign In Failed" withEventProperties:@{@"Reason":@"Authorized FB",@"Sign In Type":@"Facebook"}];
//        
//                    [self enterEmail];
//                }];
//
//        
//    }];
}
-(void)tokenAuthorizeCancelledView:(SHKOAuthView *)authView{
    
}
-(void)tokenAuthorizeView:(SHKOAuthView *)authView didFinishWithSuccess:(BOOL)success queryParams:(NSMutableDictionary *)queryParams error:(NSError *)error{
    
}

#pragma mark Email登录
- (void)emaillogup:(id)sender {
    
//    EmailLoginViewController *emailLoginViewController = [[EmailLoginViewController alloc]init];
//    emailLoginViewController.loginViewController=self.loginViewController;
//    [self.navigationController pushViewController:emailLoginViewController animated:YES];
    
    [[Amplitude instance] logEvent:@"SU - Sign Up Initiate" withEventProperties:@{@"Sign Up Type":@"Email"}];
    
    EmailRegisterViewController *emailRegisterVC = [[EmailRegisterViewController alloc]init];
    emailRegisterVC.reviewPublishViewDelegate = self.reviewPublishViewDelegate;
    emailRegisterVC.autoPostDelegate = self.autoPostDelegate;
    emailRegisterVC.autoPostDictionary = self.autoPostDictionary;
    [self.navigationController pushViewController:emailRegisterVC animated:YES];
}
#pragma mark Email注册
-(void)emailRegister
{
    EmailRegisterViewController *emailRegisterViewController=[[EmailRegisterViewController alloc]init];
    [[Amplitude instance] logEvent:@"SU - Sign UP Initiate" withEventProperties:@{@"Sign Up Type":@"Email"}];

    [self.navigationController pushViewController:emailRegisterViewController animated:YES];
}
-(void)enterEmail
{
    EnterEmailViewController *enterEmailViewController=[[EnterEmailViewController alloc]init];
    enterEmailViewController.delegate=self;
    [self.navigationController pushViewController:enterEmailViewController animated:YES];
}

#pragma mark 关闭closeButton
- (void)close:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
    [[LoadingView sharedLoadingView] stopLoading];
    
    
}
#pragma mark
-(void)signUpSuccess
{
    
}
-(void)registerSuccess
{
    
}
#pragma mark 忘记密码
-(void)forgotPasswordButtonClick:(id)sender{
    [self.navigationController pushViewController:[[ResetPasswordViewController alloc]init] animated:YES];
}


#pragma mark 错误处理
-(void)errorViewShowWithTextField:(UITextField*)textField andImageView:(UIImageView*)imageView
{
    if (control == nil) {
        control = [[UIControl alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
        [control setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Mask@iphone5"]]];
        [self.view addSubview:control];
    }
    [imageView removeFromSuperview];
    [control addSubview:imageView];
    imageView.image = [UIImage imageNamed:@"input-box-selected"];
    oriFrame = imageView.frame;
    
    
    UIImageView *promptView = [[UIImageView alloc]initWithFrame:CGRectMake(0, oriFrame.origin.y-43, DeviceWidth, 39)];
    promptView.image = [UIImage imageNamed:@"Tooltip"];
    [control addSubview:promptView];
    Label *promptLabel = [[Label alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 30) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor whiteColor] andTextLines:1];
    promptLabel.textAlignment = NSTextAlignmentCenter;
    promptLabel.text = L(@"This field cannot be empty");
    [promptView addSubview:promptLabel];
    
    
    
}
#pragma mark 注册
-(void)registerBtnClick
{
    EmailRegisterViewController *emailRegisterVC = [[EmailRegisterViewController alloc]init];
    [self.navigationController pushViewController:emailRegisterVC animated:YES];
}

 

-(void)locationManager:(CLLocationManager*)manager didUpdateLocations:(NSArray*)locations{
    [manager stopUpdatingLocation];
    CLGeocoder *geocoder=[[CLGeocoder alloc] init];
    if(locations.count){
        CLLocation *location=[locations lastObject];
        // CLLocation *location=[[CLLocation alloc] initWithLatitude:45.04 longitude:7.42];
        if(location.horizontalAccuracy>0){
            [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks,NSError *error){
                if(error==nil && placemarks.count){
                    __block CLPlacemark *place=[placemarks objectAtIndex:0];
                    [CityUtils getCitiesWithCountryId:0 andBlock:^(NSArray *cities){
                        for(IMGCity *city in cities){
                            if([city.name.lowercaseString isEqualToString:place.locality.lowercaseString]){
                                NSLog(@"city name:%@",city.name);
                                [[NSUserDefaults standardUserDefaults] setObject:city.cityId forKey:CITY_SELECT_ID];
                                [[NSUserDefaults standardUserDefaults] setObject:city.name forKey:CITY_SELECT];
                                [[NSNotificationCenter defaultCenter] postNotificationName:CITY_SELECT_NOTIFICATION object:nil];
                                [self selectCity];
                                return;
                            }
                        }
                        [self selectCity];
                    }];
                }else{
                    [self selectCity];
                }
            }];
        }else{
            [self selectCity];
        }
    }else{
        [self selectCity];    
    }
}

-(void)locationManager:(CLLocationManager*)manager didFailWithError:(NSError*)error{
    [clmanager stopUpdatingLocation];
    [self selectCity];
}   

-(void)selectCity{
    [[LoadingView sharedLoadingView] stopLoading];
    SelectCityController *selectCityController = [[SelectCityController alloc] initWithSelectedIndex:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]];
    NeedExpandMenu=NO;
    BOOL registPushSelCity=[[NSUserDefaults standardUserDefaults] boolForKey:@"registPushSelCity"];
    if (registPushSelCity) {
        [[AppDelegate ShareApp].selectedNavigationController pushViewController:selectCityController animated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
//        ispushSelCity=NO;
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"registPushSelCity"];
    }
    
    if([AppDelegate ShareApp].userLogined){
        [[AppDelegate ShareApp] requestToShowNotificationCount];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
