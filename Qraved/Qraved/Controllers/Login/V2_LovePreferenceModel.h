//
//  V2_LovePreferenceModel.h
//  Qraved
//
//  Created by harry on 2017/6/11.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface V2_LovePreferenceModel : NSObject

@property (nonatomic, copy) NSArray *foodtypeList;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *objectId;
@property (nonatomic, copy) NSNumber *objectType;


@end



@interface V2_CuisineModel : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *objectId;
@property (nonatomic, copy) NSNumber *objectType;
@property (nonatomic, copy) NSString *state;

@end
