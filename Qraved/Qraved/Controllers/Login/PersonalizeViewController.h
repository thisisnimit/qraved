//
//  PersonalizeViewController.h
//  Qraved
//
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PersonalizeButton.h"

@interface PersonalizeViewController : UIViewController<UIScrollViewDelegate,PersonalizeButtonDelegate>
-(void)loadData;
@end
