//
//  V2_EatPreferenceViewController.m
//  Qraved
//
//  Created by harry on 2017/6/8.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_EatPreferenceViewController.h"
#import "V2_EatModel.h"
@interface V2_EatPreferenceViewController ()

@end

@implementation V2_EatPreferenceViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self loadMainView];
}

- (void)loadMainView{
    self.view.backgroundColor = [UIColor whiteColor];
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, DeviceWidth, 20)];
    lblTitle.text = @"Tell us your eating preferences";
    lblTitle.font = [UIFont systemFontOfSize:17];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.textColor = [UIColor colorWithHexString:@"#333333"];
    
    [self.view addSubview:lblTitle];
    UIView *tempView = [[UIView alloc] initWithFrame:CGRectMake((DeviceWidth-275)/2, 84, 275, 275)];
    [self.view addSubview:tempView];
    
    NSArray *normalArray = @[@"ic_eating_everything",@"ic_eating_veggles",
                             @"ic_eating_no_pork",@"ic_eating_no_alcohol"];
    NSMutableArray *temp = [NSMutableArray array];
    for (int i = 0; i < normalArray.count; i ++) {
        UIButton *btnEat = [UIButton new];
        [tempView addSubview:btnEat];
        btnEat.tag = i;
//        [btnEat setTitle:idArray[i] forState:UIControlStateNormal];
        NSString *normalImage = [normalArray objectAtIndex:i];
        NSString *selectImage = [NSString stringWithFormat:@"%@_selected",[normalArray objectAtIndex:i]];
        [btnEat setImage:[UIImage imageNamed:normalImage] forState:UIControlStateNormal];
        [btnEat setImage:[UIImage imageNamed:selectImage] forState:UIControlStateSelected];
        
        btnEat.layer.cornerRadius = 5;
        btnEat.layer.masksToBounds = YES;
        
        btnEat.sd_layout
        .heightIs(130);
        
        [btnEat addTarget:self action:@selector(chooseEat:) forControlEvents:UIControlEventTouchUpInside];
        
        [temp addObject:btnEat];
    }
    
    [tempView setupAutoMarginFlowItems:temp withPerRowItemsCount:2 itemWidth:130 verticalMargin:15 verticalEdgeInset:15 horizontalEdgeInset:0];
    
    
    UIImageView *bottomImageView = [UIImageView new];
    bottomImageView.image = [UIImage imageNamed:@"ic_bottom_eat"];
    [self.view addSubview:bottomImageView];
    
    bottomImageView.sd_layout
    .leftSpaceToView(self.view,0)
    .bottomSpaceToView(self.view,0)
    .widthIs(DeviceWidth)
    .heightIs(DeviceWidth/4.7);
    
}
- (void)setEatingArray:(NSArray *)eatingArray{
    _eatingArray = eatingArray;

}

- (void)chooseEat:(UIButton *)btn{
    btn.selected = !btn.selected;
    V2_EatModel *model = [self.eatingArray objectAtIndex:btn.tag];
    if (btn.selected) {
        
        model.state = @"1";
    }else{
        model.state = @"0";
    }
    
}

@end
