//
//  V2_LovePreferenceModel.m
//  Qraved
//
//  Created by harry on 2017/6/11.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_LovePreferenceModel.h"

@implementation V2_LovePreferenceModel


- (void)setValue:(id)value forKey:(NSString *)key{
    if ([key isEqualToString:@"foodtypeList"]) {
        NSMutableArray *arr = [NSMutableArray array];
        for (NSDictionary *cuisineDic in value) {
            V2_CuisineModel *model = [[V2_CuisineModel alloc] init];
            [model setValuesForKeysWithDictionary:cuisineDic];
            model.state = @"0";
            [arr addObject:model];
        }
        self.foodtypeList = arr;
    }
    
    if ([key isEqualToString:@"name"]) {
        self.name = value;
    }
    if ([key isEqualToString:@"objectType"]) {
        self.objectType = value;
    }
    if ([key isEqualToString:@"objectId"]) {
        self.objectId = value;
    }
}


@end

@implementation V2_CuisineModel

@end
