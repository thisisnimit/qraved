//
//  V2_PreferenceViewController.h
//  Qraved
//
//  Created by harry on 2017/6/8.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "GAITrackedViewController.h"

@interface V2_PreferenceViewController : GAITrackedViewController
@property (nonatomic, assign) BOOL isFromProfile;
@property (nonatomic, assign) BOOL isHaveOnBoarding;
@property (nonatomic, assign) BOOL isFromHome;

@property (nonatomic, strong)  NSArray  *awaysSelecteArray;
@end

