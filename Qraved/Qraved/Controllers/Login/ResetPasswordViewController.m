//
//  ResertPasswordViewController.m
//  PocketPlan
//
//  Created by Libo Liu on 13-11-22.
//  Copyright (c) 2013年 com.imaginato. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "AppDelegate.h"
#import "PushHelper.h"
#import <CoreText/CoreText.h>

#import "UIConstants.h"

#import "UILabel+Helper.h"
#import "UIView+Helper.h"
#import "UIAlertView+BlocksKit.h"
#import "UILabel+Helper.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "UIViewController+Helper.h"

#import "SVProgressHUD.h"
#import "NavigationBarButtonItem.h"
#import "SendCall.h"
#import "EmailRegisterViewController.h"
#import "PostDataHandler.h"
@interface ResetPasswordViewController ()<TYAttributedLabelDelegate,UITextFieldDelegate>
{
   // UIImageView *bgImageView;
}
@property (retain, nonatomic) UITextField *emailTextField;
@property (retain, nonatomic) UIButton *sendButton;

@end

@implementation ResetPasswordViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
 
- (void)viewDidLoad
{
    [super viewDidLoad];
#pragma mark 隐藏navigationBar
    self.navigationController.navigationBarHidden = YES;
    self.screenName=@"Forgot Password form page";
    [self.navigationItem setTitle:NSLocalizedStringFromTable(L(@"Forgot Password"), nil, nil)];
    
    [self loadMainView];
    [self createNavbutton];
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
}
- (void)createNavbutton{
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 20, 45, 45)];
    [closeButton setImage:[UIImage imageNamed:@"leftback@2x" ] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeButton];
    [self.view bringSubviewToFront:closeButton];
}
-(void)back
{
    [self.navigationController popViewControllerAnimated:YES];
    [[Amplitude instance] logEvent:@"SI - Forgot Password Cancel" withEventProperties:@{}];
}
-(void)loadMainView
{
    self.view.backgroundColor = [UIColor whiteColor];
//    bgImageView = [[UIImageView alloc]initWithFrame:self.view.bounds];
//    bgImageView.userInteractionEnabled = YES;
//    [self.view addSubview:bgImageView];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundTap:)];
    [self.view addGestureRecognizer:tapGesture];
//    bgImageView.image = [UIImage imageNamed:@"ic_forgot_pwd.png"];

    UIImageView *logoImageView= [UIImageView new];
    logoImageView.image = [UIImage imageNamed:@"Qraved Logo Red"];
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.text = @"Trouble Logging In?";
    lblTitle.textColor = [UIColor color333333];
    lblTitle.font = [UIFont systemFontOfSize:18];
    
    UILabel *lblSub = [UILabel new];
    lblSub.textAlignment = NSTextAlignmentCenter;
    lblSub.textColor = [UIColor color999999];
    lblSub.font = [UIFont systemFontOfSize:14];
    lblSub.text = @"Enter your email address and we’ll \nsend you a link to reset your password.";
    

    UIView *tfView = [UIView new];
    tfView.backgroundColor = [UIColor whiteColor];
    tfView.layer.borderWidth = 1;
    tfView.layer.borderColor = [UIColor colorWithHexString:@"#EAEAEA"].CGColor;
    tfView.layer.cornerRadius = 3;
    tfView.layer.masksToBounds = YES;
    
    self.emailTextField = [UITextField new];
    self.emailTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    self.emailTextField.font = [UIFont systemFontOfSize:14];
    self.emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    self.emailTextField.returnKeyType = UIReturnKeyDone;
    self.emailTextField.textColor = [UIColor colorWithHexString:@"#999999"];
    self.emailTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:L(@"Email address") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#999999"]}];
    self.emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.emailTextField.delegate = self;
    [tfView addSubview:self.emailTextField];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidChange:)
                                                 name:UITextFieldTextDidChangeNotification object:self.emailTextField];
#pragma mark send
    self.sendButton = [UIButton buttonWithType:UIButtonTypeCustom];

    [self.sendButton setTitle:L(@"SEND LOGIN LINK") forState:UIControlStateNormal];
    self.sendButton.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    [self.sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.sendButton addTarget:self action:@selector(sendButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    self.sendButton.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    self.sendButton.clipsToBounds = YES;
    self.sendButton.layer.cornerRadius = 3;
    
    [self.view sd_addSubviews:@[logoImageView,lblTitle, lblSub,tfView,self.sendButton]];
    
    
    logoImageView.sd_layout
    .centerXEqualToView(self.view)
    .topSpaceToView(self.view,84)
    .heightIs(61)
    .widthIs(163);
    
    lblTitle.sd_layout
    .leftSpaceToView(self.view,42)
    .rightSpaceToView(self.view,42)
    .topSpaceToView(logoImageView,30)
    .heightIs(20);
    
    lblSub.sd_layout
    .topSpaceToView(lblTitle, 8)
    .leftSpaceToView(self.view, 20)
    .rightSpaceToView(self.view, 20)
    .heightIs(36);
    lblSub.numberOfLines = 2;
    
    tfView.sd_layout
    .topSpaceToView(lblSub,30)
    .leftSpaceToView(self.view,42)
    .rightSpaceToView(self.view,42)
    .heightIs(50);
    
    self.emailTextField.sd_layout
    .topSpaceToView(tfView,0)
    .leftSpaceToView(tfView,15)
    .rightSpaceToView(tfView,15)
    .heightIs(50);
    
    self.sendButton.sd_layout
    .topSpaceToView(tfView,21)
    .leftSpaceToView(self.view,42)
    .rightSpaceToView(self.view,42)
    .heightIs(50);
    [self.emailTextField becomeFirstResponder];
}

- (void)textFieldTextDidChange:(NSNotification *)notification
{
    if (self.emailTextField.text.length > 0) {
        self.sendButton.backgroundColor = [UIColor colorWithHexString:@"#D20000"];
        self.sendButton.enabled = YES;
    }else{
        self.sendButton.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
        self.sendButton.enabled = NO;
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)attributedLabel:(TYAttributedLabel *)attributedLabel textStorageClicked:(id<TYTextStorageProtocol>)TextRun atPoint:(CGPoint)point{
    
    
    if ([TextRun isKindOfClass:[TYLinkTextStorage class]]) {
        EmailRegisterViewController * emaiRegisterVC = [[EmailRegisterViewController alloc] init];
        [self.navigationController pushViewController:emaiRegisterVC animated:YES];
    }
    
}


-(void)backgroundTap:(id)sender
{
    [self.emailTextField resignFirstResponder];
}

#pragma mark textField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.emailTextField resignFirstResponder];
    return YES;
}
#pragma mark login
-(void)loginButtonClick
{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark send Email
- (void)sendButtonClick:(id)sender {
    if (self.emailTextField.text.length<1) {
        UIAlertView *alert=[UIAlertView bk_alertViewWithTitle:ALERT_TITLE message:@"Enter your email address."];
        [alert bk_addButtonWithTitle:L(@"OK") handler:^(){
            
        }];
        [alert show];
        return;
    }
    if (![self.emailTextField.text isValidateEmail]) {
        UIAlertView *alert=[UIAlertView bk_alertViewWithTitle:ALERT_TITLE message:(@"The format of the email address is invalid.")];
        [alert bk_addButtonWithTitle:L(@"OK") handler:^(){
            
        }];
        [alert show];
        return;
    }
    if (self.emailTextField.text.length>0) {
        
        [SVProgressHUD show];
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc]initWithCapacity:0];
        [parameters setObject:self.emailTextField.text forKey:@"email"];
        [parameters setObject:@"ios" forKey:@"app"];
        [[IMGNetWork sharedManager]POST:@"user/resetPassword" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
                [SVProgressHUD dismiss];
    
                if ([responseObject objectForKey:@"exceptionmsg"]) {
                    UIAlertView *alert=[UIAlertView bk_alertViewWithTitle:ALERT_TITLE message:L(@"Email error")];
                    [alert bk_addButtonWithTitle:L(@"OK") handler:^(){
    
                    }];
                    [alert show];
                    [[Amplitude instance] logEvent:@"SI - Forgot Password Failed" withEventProperties:@{@"Reason":@"Email not registered"}];

                }else{
                    [[Amplitude instance] logEvent:@"SI - Forgot Password Succeed" withEventProperties:@{}];
                    UIAlertView *alert=[UIAlertView bk_alertViewWithTitle:ALERT_TITLE message:L(@"Email sent!")];
                    [alert bk_addButtonWithTitle:L(@"OK") handler:^(){
    
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                    [alert show];
                }
            

        } failure:^(NSURLSessionDataTask *operation, NSError *error) {
                [SVProgressHUD dismiss];
                UIAlertView *alert=[UIAlertView bk_alertViewWithTitle:ALERT_TITLE message:NSLocalizedStringFromTable(L(@"Please check your internet connection!"),nil,nil)];
                [alert bk_addButtonWithTitle:L(@"OK") handler:^(){
    
                }];
                [alert show];
            }];

    }

}
@end
