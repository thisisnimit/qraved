//
//  LoginVC.h
//  quaved_ios
//
//  Created by Shine Wang on 5/28/13.
//  Copyright (c) 2013 Shine Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SHKTwitter.h"
#import "EnterEmailViewController.h"
#import "Label.h"
#import "AlbumActionButtonsView.h"
#import "UserDataHandler.h"

typedef NS_ENUM(NSInteger, BtnClicked) {
    FacebookBtnClick            = 0,
    GoogleBtnClick            = 1,
    TwitterBtnClick = 2,
};

@protocol LoginDelegate <NSObject>
@required
-(void)successfulFunction;

@end

@protocol ReviewPublishViewDelegate <NSObject>
@required
-(void)postPhoto;

@end

@protocol AutoPostDelegate <NSObject>
@optional
- (void)communityButtonTapped:(UIButton *)button;
-(void)albumActionButtonsClick:(AlbumActionButtonsType)btnType;
-(void)postComment:(id)textInput;
-(void)saveBtnClick;
-(void)publishReview;
@end

@interface LoginViewController : GAITrackedViewController<registerDelegate>
@property (nonatomic,assign)  BOOL isSignUpSplash;
@property (nonatomic,assign)  BOOL isFromonboard;
@property (nonatomic,assign) BOOL isFromNotification;
@property (nonatomic,assign) BOOL isFromProfile;
@property (nonatomic, retain)NSDictionary *autoPostDictionary;
@property (nonatomic, retain) NSDictionary *deeplinkDic;
//- (void)close:(id)sender;

//- (void)facebookLogin:(id)sender;
//- (void)twitterLogin:(id)sender;
//
//- (void)emailLogin:(id)sender;
- (void)loginSuccessful;

-(id)initWithFromWhere:(NSNumber *)_fromWhere;

@property(nonatomic,weak)id<LoginDelegate>delegate;
@property(nonatomic,weak)id<ReviewPublishViewDelegate> reviewPublishViewDelegate;
@property(nonatomic,weak)id<AutoPostDelegate> autoPostDelegate;
@end




