//
//  PersonalizeViewController.m
//  Qraved
//
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "PersonalizeViewController.h"
#import "UserDataHandler.h"
#import "LoadingView.h"
#import "AppDelegate.h"
#import "IMGDiscoverEntity.h"
#import "IMGDiscover.h"
#import "RestaurantsViewController.h"
#import "UIAlertView+BlocksKit.h"

@interface PersonalizeViewController (){
    UIScrollView *mainScrollView;
    CGFloat currentY;
    NSDictionary *sourceData;
    
    UILabel *navigationLabel;
    
    NSMutableArray *cuisineIDArray;
    NSMutableArray *priceLevelIDArray;
    NSMutableArray *districtIDArray;
    NSMutableArray *tagIDArray;
}

@end

@implementation PersonalizeViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    cuisineIDArray = [[NSMutableArray alloc] init];
    priceLevelIDArray = [[NSMutableArray alloc] init];
    districtIDArray = [[NSMutableArray alloc] init];
    tagIDArray = [[NSMutableArray alloc] init];
    
    [self loadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self loadNavigationView];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (navigationLabel!=nil) {
        [navigationLabel removeFromSuperview];
        navigationLabel = nil;
    }
}

-(void)loadNavigationView{
    navigationLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, DeviceWidth, 90)];
    navigationLabel.backgroundColor = [UIColor colorWithRed:246/255.0 green:247/255.0 blue:249/255.0 alpha:1];
    navigationLabel.textAlignment = NSTextAlignmentCenter;
    navigationLabel.numberOfLines = 0;
    navigationLabel.text = @"Hello! Welcome to Qraved\n\nLet's search for the best dining experience for you!";
    
    NSMutableAttributedString *navigationViewAttributeString = [[NSMutableAttributedString alloc] initWithString:navigationLabel.text];
    [navigationViewAttributeString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:18]} range:NSMakeRange(0, 24)];
    NSRange rang=[navigationLabel.text rangeOfString:@"Let's search for the best dining experience for you!"];
    [navigationViewAttributeString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:DEFAULT_FONT_NAME size:16]} range:rang];
    
    [navigationViewAttributeString addAttribute:NSForegroundColorAttributeName value:[UIColor color333333] range:NSMakeRange(0, 24)];
    [navigationViewAttributeString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:rang];
    
    [navigationLabel setAttributedText:navigationViewAttributeString];
    
    [self.navigationController.view addSubview:navigationLabel];
}


-(void)loadData{
    [[LoadingView sharedLoadingView] startLoading];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
    [UserDataHandler personalizationOptionFetch:param andSuccessBlock:^(id responseObjectLocal) {
        sourceData = responseObjectLocal;
        [self loadMainView];
        [[LoadingView sharedLoadingView] stopLoading];
    } andFailedBlock:^(NSString *errorMsg) {
        [[LoadingView sharedLoadingView] stopLoading];
        
    }];
}

-(void)loadMainView{
    if (mainScrollView) {
        [mainScrollView removeFromSuperview];
    }
    mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 46, self.view.frame.size.width, self.view.frame.size.height-46)];
    [mainScrollView setShowsVerticalScrollIndicator:NO];
    mainScrollView.delegate = self;
    [self.view addSubview:mainScrollView];
    
    [self loadDistrictConfigListView];
    [self loadCuisineConfigListView];
    [self loadPriceLevelConfigListView];
    [self loadTagConfigListView];
    
    [self showMeMyResult];
}

-(void)loadDistrictConfigListView{
    currentY = 0;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, currentY, DeviceWidth, 50)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = @"Where do you usually eat out?";
    titleLabel.numberOfLines = 0;
    titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:16];
    titleLabel.textColor = [UIColor color333333];
    [mainScrollView addSubview:titleLabel];
    
    UIView *btnBackView = [self loadBtnsView:PersonalizeButtonDistrict];
    btnBackView.frame = CGRectMake(0, titleLabel.endPointY, btnBackView.frame.size.width, btnBackView.frame.size.height);
    [mainScrollView addSubview:btnBackView];

    currentY = btnBackView.endPointY;
    mainScrollView.contentSize = CGSizeMake(DeviceWidth, currentY);
}

-(void)loadCuisineConfigListView{
    currentY+=30;
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, currentY, DeviceWidth, 50)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = @"What are some of your favorite food?\nYumm...!";
    titleLabel.numberOfLines = 0;
    titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:16];
    titleLabel.textColor = [UIColor color333333];
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:titleLabel.text];
    [attributeString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:DEFAULT_FONT_NAME size:16]} range:NSMakeRange(36,9)];
    [attributeString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(36,9)];
    [titleLabel setAttributedText:attributeString];
    
    [mainScrollView addSubview:titleLabel];
    
    UIView *btnBackView = [self loadBtnsView:PersonalizeButtonCuisine];
    btnBackView.frame = CGRectMake(0, titleLabel.endPointY, btnBackView.frame.size.width, btnBackView.frame.size.height);
    [mainScrollView addSubview:btnBackView];
    
    currentY = btnBackView.endPointY;
    mainScrollView.contentSize = CGSizeMake(DeviceWidth, currentY);
}

-(void)loadPriceLevelConfigListView{
    currentY+=30;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, currentY, DeviceWidth, 50)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = @"Which of these interest you most?";
    titleLabel.numberOfLines = 0;
    titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:16];
    titleLabel.textColor = [UIColor grayColor];
    [mainScrollView addSubview:titleLabel];
    
    UIView *btnBackView = [self loadBtnsView:PersonalizeButtonPriceLevel];
    btnBackView.frame = CGRectMake(0, titleLabel.endPointY, btnBackView.frame.size.width, btnBackView.frame.size.height);
    [mainScrollView addSubview:btnBackView];
    
    currentY = btnBackView.endPointY;
    mainScrollView.contentSize = CGSizeMake(DeviceWidth, currentY);
}

-(void)loadTagConfigListView{
    currentY+=30;
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, currentY, DeviceWidth, 70)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = @"Getting hungry now...\nLastly,anything specific you're\nlooking for from the venue?";
    titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:16];
    titleLabel.textColor = [UIColor color333333];
    titleLabel.numberOfLines = 0;
    
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:titleLabel.text];
    [attributeString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:DEFAULT_FONT_NAME size:16]} range:NSMakeRange(21,60)];
    [attributeString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(21,60)];
    [titleLabel setAttributedText:attributeString];
    
    [mainScrollView addSubview:titleLabel];
    
    UIView *btnBackView = [self loadBtnsView:PersonalizeButtonTag];
    btnBackView.frame = CGRectMake(0, titleLabel.endPointY, btnBackView.frame.size.width, btnBackView.frame.size.height);
    [mainScrollView addSubview:btnBackView];
    
    currentY = btnBackView.endPointY;
    mainScrollView.contentSize = CGSizeMake(DeviceWidth, currentY);
}

-(void)showMeMyResult{
//    currentY += 290;
//    mainScrollView.contentSize = CGSizeMake(DeviceWidth, currentY);
    UILabel *btnLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, currentY+20, DeviceWidth, 40)];
    btnLabel.textAlignment = NSTextAlignmentCenter;
    btnLabel.text = @"Let's find you some yummy\nrestaurants to go to!";
    btnLabel.numberOfLines = 0;
    btnLabel.font = [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:16];
    btnLabel.textColor = [UIColor grayColor];
    [mainScrollView addSubview:btnLabel];

    UIButton *showMeMyResultBtn = [[UIButton alloc] initWithFrame:CGRectMake(20, btnLabel.endPointY+10, (self.view.frame.size.width - 40), 60)];
    [showMeMyResultBtn setTitle:@"show me my result!" forState:UIControlStateNormal];
    showMeMyResultBtn.backgroundColor = [UIColor colorWithRed:39.0/255.0 green:170.0/255.0 blue:54.0/255.0 alpha:1];
    [showMeMyResultBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [showMeMyResultBtn addTarget:self action:@selector(showMeMyResultBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [mainScrollView addSubview:showMeMyResultBtn];
    mainScrollView.contentSize = CGSizeMake(DeviceWidth, showMeMyResultBtn.endPointY+20);
    

}

-(UIView *)loadBtnsView:(PersonalizeButtonType)type{
    NSArray *configArray;
    CGFloat btnViewW = 0.0;
    CGFloat btnViewH = 0.0;
    int colCount = 0;
    if (type == PersonalizeButtonDistrict) {
        configArray = [sourceData objectForKey:@"districtConfigList"];
        btnViewW = 0.43 * self.view.frame.size.width;
        btnViewH = 45.0;
        colCount = 2;
    }else if(type == PersonalizeButtonPriceLevel){
        configArray = [sourceData objectForKey:@"priceLevelConfigList"];
        btnViewW = 0.43 * self.view.frame.size.width;
        btnViewH = 45.0;
        colCount = 2;
    }else if(type == PersonalizeButtonCuisine) {
        configArray = [sourceData objectForKey:@"cuisineConfigList"];
        btnViewW = 100.0;
        btnViewH = 100.0;
        colCount = 3;
    }else if (type == PersonalizeButtonTag ){
        configArray = [sourceData objectForKey:@"tagConfigList"];
        btnViewW = 100.0;
        btnViewH = 100.0;
        colCount = 3;
    }
    
    CGFloat marginX = (self.view.frame.size.width - colCount*btnViewW)/(colCount+1);
    CGFloat marginY = 15.0;
    
    UIView *btnBackView = [[UIView alloc] init];
    for (int i = 0 ; i < configArray.count ;i++) {
        NSDictionary *param = [configArray objectAtIndex:i];
        
        int row = i/colCount;
        int col = i%colCount;
        
        CGFloat x = marginX + col*(btnViewW+marginX);
        CGFloat y = marginY + row*(btnViewH+marginY);
        
        PersonalizeButton *personalizeButton = [[PersonalizeButton alloc] initWithType:type andFrame:CGRectMake( x, y, btnViewW, btnViewH) andParam:param];
        personalizeButton.delegate = self;
        [btnBackView addSubview:personalizeButton];
        if (i == (configArray.count - 1) ) {
            btnBackView.frame = CGRectMake(0, 0, self.view.frame.size.width, personalizeButton.endPointY);
        }
    }
    return btnBackView;
}

-(void)personalizeButtonAddWithType:(PersonalizeButtonType)type andID:(NSNumber*)ID{
    if (type == PersonalizeButtonCuisine) {
        [cuisineIDArray addObject:[NSString stringWithFormat:@"%@",ID]];
    }else if(type == PersonalizeButtonPriceLevel){
        [priceLevelIDArray addObject:[NSString stringWithFormat:@"%@",ID]];
    }else if(type == PersonalizeButtonDistrict){
        [districtIDArray addObject:[NSString stringWithFormat:@"%@",ID]];
    }else if(type == PersonalizeButtonTag){
        [tagIDArray addObject:[NSString stringWithFormat:@"%@",ID]];
    }
}

-(void)personalizeButtonMinusWithType:(PersonalizeButtonType)type andID:(NSNumber*)ID{
    if (type == PersonalizeButtonCuisine) {
        [cuisineIDArray removeObject:[NSString stringWithFormat:@"%@",ID]];
    }else if(type == PersonalizeButtonPriceLevel){
        [priceLevelIDArray removeObject:[NSString stringWithFormat:@"%@",ID]];
    }else if(type == PersonalizeButtonDistrict){
        [districtIDArray removeObject:[NSString stringWithFormat:@"%@",ID]];
    }else if(type == PersonalizeButtonTag){
        [tagIDArray removeObject:[NSString stringWithFormat:@"%@",ID]];
    }
}
-(void)showErrorMsg:(NSString*)errorMsg{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:ALERT_TITLE message:errorMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [alert show];

}


-(void)showMeMyResultBtnClick{
    if ( (cuisineIDArray.count == 0)
        ||(priceLevelIDArray.count == 0)
        || (districtIDArray.count == 0)
        || (tagIDArray.count == 0)) {
//        [[AppDelegate ShareApp].window.rootViewController dismissViewControllerAnimated:NO completion:nil];
        
        [self showErrorMsg:L(@"Please select at least one each")];

        
        
        return;
    }
    
    [[LoadingView sharedLoadingView] startLoading];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:[IMGUser currentUser].userId forKey:@"userId"];
    [param setObject:[IMGUser currentUser].token forKey:@"t"];
    if ( (cuisineIDArray !=nil) && (cuisineIDArray.count !=0) ) {
        [param setObject:[cuisineIDArray componentsJoinedByString:@","]  forKey:@"cuisineIds"];
    }
    if ( (priceLevelIDArray !=nil) &&(priceLevelIDArray.count!=0) ) {
        [param setObject:[priceLevelIDArray componentsJoinedByString:@","]  forKey:@"priceLevels"];
    }
    if ( (districtIDArray !=nil) &&(districtIDArray.count!=0) ) {
        [param setObject:[districtIDArray componentsJoinedByString:@","]  forKey:@"districtIds"];
    }
    if ((tagIDArray !=nil) && (tagIDArray.count !=0 ) ) {
        [param setObject:[tagIDArray componentsJoinedByString:@","]  forKey:@"tagIds"];
    }
    [UserDataHandler userPersonalizationAdd:param andSuccessBlock:^(id responseObjectLocal) {
        [[LoadingView sharedLoadingView] stopLoading];
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        if ([param objectForKey:@"cuisineIds"]!=nil) {
            [dic setObject:[param objectForKey:@"cuisineIds"]  forKey:@"cuisineIDs"];
        }
        
        priceLevelIDArray=[NSMutableArray arrayWithArray:[priceLevelIDArray sortedArrayUsingComparator:^(NSString *objc1,NSString *objc2){
            if([objc1 longLongValue]>[objc2 longLongValue]){
                return (NSComparisonResult)NSOrderedDescending;
            }
            if([objc1 longLongValue]<[objc2 longLongValue]){
                return (NSComparisonResult)NSOrderedAscending;
            }
            return (NSComparisonResult)NSOrderedSame;
        }]];
        
        if (priceLevelIDArray!=nil && (priceLevelIDArray.count!=0) ) {
            [dic setObject:[priceLevelIDArray objectAtIndex:(priceLevelIDArray.count-1)] forKey:@"endPrice"];
            [dic setObject:[priceLevelIDArray objectAtIndex:0]  forKey:@"startPrice"];
        }
        
        if ([param objectForKey:@"districtIds"]!=nil) {
            [dic setObject:[param objectForKey:@"districtIds"]  forKey:@"districtIDs"];
        }
        if ([param objectForKey:@"tagIds"]!=nil) {
            [dic setObject:[param objectForKey:@"tagIds"]  forKey:@"tagIDs"];
        }
        
        IMGDiscoverEntity *discoverEntity = [[IMGDiscoverEntity alloc] init];
        [discoverEntity initFromOnBoarding:dic];
        
        IMGDiscover *discover=[[IMGDiscover alloc ]init];
        [discover initFromDiscoverEntity:discoverEntity];
        discover.searchDistrictArray = discover.districtArray;
        discover.needLog = YES;
        
        RestaurantsViewController *restaurantsViewController=[[RestaurantsViewController alloc]initWithMap];
        restaurantsViewController.isFromPersonnazation=YES;
        restaurantsViewController.amplitudeKind = @"Onboarding SRP";
        [restaurantsViewController initDiscover:discover];
        
        [[AppDelegate ShareApp].window.rootViewController dismissViewControllerAnimated:NO completion:nil];
        [[AppDelegate ShareApp] goToPage:12];
        [[AppDelegate ShareApp].selectedNavigationController pushViewController:restaurantsViewController animated:NO];
        
    } andFailedBlock:^(NSString *errorMsg) {
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

@end
