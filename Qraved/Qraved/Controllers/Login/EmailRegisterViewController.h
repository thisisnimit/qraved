//
//  EmailRegisterViewController.h
//  PocketPlan
//
//  Created by Libo Liu on 13-11-12.
//  Copyright (c) 2013年 com.imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "Label.h"
#import "LoginViewController.h"
#import <CoreLocation/CoreLocation.h>
@protocol SighUpDelegate <NSObject>
-(void)signUpSuccess;
@end

@interface EmailRegisterViewController : BaseChildViewController<UITextFieldDelegate,CLLocationManagerDelegate>
//@property(nonatomic,retain) UITextField *birthdayTextField;
//@property(nonatomic,retain) UITextField *yearTextField;
//@property(nonatomic,retain) UITextField *monthTextField;
//@property(nonatomic,retain) UITextField *dayTextField;
//@property(nonatomic,retain) UITextField *codeTextField;
//@property(nonatomic,retain) UITextField *phoneNumField;
//@property(nonatomic,retain) UIButton *signUpButton;
//@property(nonatomic,retain) Label *loginButton;
@property (nonatomic, retain)NSDictionary *autoPostDictionary;
- (void)regButtonClick:(id)sender;
@property(nonatomic,weak)id<ReviewPublishViewDelegate> reviewPublishViewDelegate;
@property(nonatomic,retain) id<SighUpDelegate> delegate;
//@property(nonatomic,retain)UIControl *mainView;
@property(nonatomic,weak)id<AutoPostDelegate> autoPostDelegate;
@property(nonatomic,assign) BOOL isFromSignUpSplash;
@property(nonatomic,assign)BOOL isfromBookingInfo;

@end
