//
//  V2_LovePreferenceViewController.m
//  Qraved
//
//  Created by harry on 2017/6/8.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_LovePreferenceViewController.h"
#import "V2_LovePreferenceCell.h"
#import "V2_LovePreferenceLayout.h"
#import "V2_LovePreferenceModel.h"
#import "V2_EatModel.h"
#import "V2_LovePreferenceTableViewCell.h"
#import "V2_LovePreferenceCuisineTableViewCell.h"
@interface V2_LovePreferenceViewController ()<UITableViewDelegate, UITableViewDataSource, TTGTextTagCollectionViewDelegate, V2_LovePreferenceTableViewCellDelegate,V2_LovePreferenceCuisineTableViewCellDelegate>//<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    
    NSArray *normalArray;
    NSMutableArray *tagTextArray;
    V2_LovePreferenceTableViewCell * currentCell;
    
}
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong)  UITableView  *tableView;
@property (nonatomic, strong)  NSMutableArray  *cuisineArr;
@property (nonatomic, strong)  NSMutableArray  *objectIdArr;

@property (nonatomic, strong)  NSMutableArray  *selectedObjectIdArr;


@end

@implementation V2_LovePreferenceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [self loadMainView];
    
    tagTextArray = [NSMutableArray array];
    self.cuisineArr = [NSMutableArray array];
    self.objectIdArr = [NSMutableArray array];
    self.selectedObjectIdArr = [NSMutableArray array];
    self.awaysSelecteArray = [NSMutableArray array];

}
- (void)loadMainView{
    self.view.backgroundColor = [UIColor whiteColor];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight +20 - 64) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return self.cuisineArray.count + 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    if (section == 0) {
        return 90;
    }else{
        
     return 40;
    }
   
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{

    return 0.0001;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    if (section == 0) {
        UIView *headerview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 90)];
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 14, DeviceWidth, 20)];
        lblTitle.text = @"Tap a few things you like";
        lblTitle.font = [UIFont boldSystemFontOfSize:17];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.textColor = [UIColor colorWithHexString:@"#333333"];
        [headerview addSubview:lblTitle];
        
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, lblTitle.bottom +25, DeviceWidth-30, 16)];
        label.font = [UIFont boldSystemFontOfSize:14];
        label.text = [@"INGREDIENTS" capitalizedString];
        label.textColor = [UIColor colorWithHexString:@"#333333"];
        [headerview addSubview:label];
        return headerview;
    }else{
    
        UIView *headerview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, DeviceWidth-30, 16)];
        label.font = [UIFont boldSystemFontOfSize:14];
        V2_LovePreferenceModel *model = [self.cuisineArray objectAtIndex:section -1];
        label.text = [model.name capitalizedString];
        label.textColor = [UIColor colorWithHexString:@"#333333"];
        [headerview addSubview:label];
        return headerview;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section == 0) {
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        V2_LovePreferenceTableViewCell * cell = [[V2_LovePreferenceTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell createUI:self.eatingArray];
        return cell.cellHeight;
        
    }else{
    
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        V2_LovePreferenceCuisineTableViewCell *cell = [[V2_LovePreferenceCuisineTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        V2_LovePreferenceModel *model = [self.cuisineArray objectAtIndex:indexPath.section - 1];
        [cell createUI:model.foodtypeList];
        
        return cell.pointY;
    }
}




-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section == 0) {
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        V2_LovePreferenceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[V2_LovePreferenceTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
        cell.Delegate =self;
        [cell createUI:self.eatingArray];
        currentCell = cell;
        return cell;

       
    }else{
        
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        V2_LovePreferenceCuisineTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[V2_LovePreferenceCuisineTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.backgroundColor = [UIColor whiteColor];
            V2_LovePreferenceModel *model = [self.cuisineArray objectAtIndex:indexPath.section - 1];
//                V2_CuisineModel *cuisineModel = [model.foodtypeList objectAtIndex:indexPath.row];
            for ( V2_CuisineModel *cuisineModel in model.foodtypeList) {
                [self.cuisineArr addObject:cuisineModel.name];
                
            }
            cell.section = indexPath.section - 1;
            cell.Delegate = self;
            [cell createUI:model.foodtypeList];
            
        }
        return cell;

    }
    
    
}
- (void)textTagCollectionView:(NSInteger)textTagCollectionViewTag didTapTag:(NSString *)tagText atIndex:(NSUInteger)index selected:(BOOL)selected{

    if (textTagCollectionViewTag == 0) {
        V2_LovePreferenceModel *model = [self.cuisineArray objectAtIndex:0];
        V2_CuisineModel *cuisineModel = [model.foodtypeList objectAtIndex:index];
        NSLog(@"11====%@",cuisineModel.objectId);
        if (selected == 0) {
            cuisineModel.state = @"0";
            [self.selectedObjectIdArr removeObject:cuisineModel.name];
        }else{
            
            cuisineModel.state = @"1";
            [self.selectedObjectIdArr addObject:cuisineModel.name];
        }
        
        
    }else if (textTagCollectionViewTag == 1){
        V2_LovePreferenceModel *model = [self.cuisineArray objectAtIndex:1];
        V2_CuisineModel *cuisineModel = [model.foodtypeList objectAtIndex:index];
        NSLog(@"22====%@",cuisineModel.objectId);
        if (selected == 0) {
            cuisineModel.state = @"0";
            [self.selectedObjectIdArr removeObject:cuisineModel.name];
        }else{
            
            cuisineModel.state = @"1";
            [self.selectedObjectIdArr addObject:cuisineModel.name];
        }
    }else if (textTagCollectionViewTag == 2){
        V2_LovePreferenceModel *model = [self.cuisineArray objectAtIndex:2];
        V2_CuisineModel *cuisineModel = [model.foodtypeList objectAtIndex:index];
        NSLog(@"33====%@",cuisineModel.objectId);
        if (selected == 0) {
            cuisineModel.state = @"0";
            [self.selectedObjectIdArr removeObject:cuisineModel.name];
        }else{
            
            cuisineModel.state = @"1";
            [self.selectedObjectIdArr addObject:cuisineModel.name];
        }
        
    }
//    NSLog(@"%@",self.selectedObjectIdArr);
//    NSLog(@"%lu",(unsigned long)self.selectedObjectIdArr.count);
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"arrayCount" object:self.selectedObjectIdArr];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"eatArrayCount" object:nil];

}

- (void)setCuisineArray:(NSArray *)cuisineArray{
    _cuisineArray = cuisineArray;

    for (V2_LovePreferenceModel *model in cuisineArray) {
        for (int i = 0; i < model.foodtypeList.count; i ++) {
            V2_CuisineModel *cuisineModel = [model.foodtypeList objectAtIndex:i];
//            if (i == 0) {
//                cuisineModel.state = @"1";
//            }
        }
    }
    [self.tableView reloadData];
}
- (void)setEatingArray:(NSArray *)eatingArray{

    _eatingArray = eatingArray;
    NSLog(@"%@",self.eatingArray);
    [self loadMainView];
    [self.tableView reloadData];
}
#pragma amrk - V2_LovePreferenceTableViewCellDelegate
- (void)delegateSelecteArray:(NSMutableArray *)array{

//    for (V2_EatModel *eatModel in array) {
//        if ([eatModel.state isEqualToString:@"1"]) {
//            [self.selectedObjectIdArr addObject:eatModel.name];
//        }
//        
//        
//    }
//    
//    for (NSString *str in self.selectedObjectIdArr) {
//        if (str isEqualToString:<#(nonnull NSString *)#>) {
//            <#statements#>
//        }
//    }
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"eatArrayCount" object:nil];
}
- (void)setAarayArray:(NSArray *)aarayArray{
    
    _aarayArray = aarayArray;
    for (NSDictionary *dic in aarayArray) {
        for (V2_LovePreferenceModel *model in _cuisineArray) {
            for (V2_CuisineModel *cuisineModel in model.foodtypeList) {
                if ([[dic objectForKey:@"objectType"] isEqual:cuisineModel.objectType] && [[dic objectForKey:@"objectId"] isEqual:cuisineModel.objectId]) {
                    cuisineModel.state = @"1";
                    [self.awaysSelecteArray addObject:cuisineModel.state];
                    [self.selectedObjectIdArr addObject:cuisineModel.name];
                }
                
            }
            
        }
        
    }
//    NSLog(@"-=-=-=-=-=-=-=-=--==--=-=-=--=-=-=-%@",self.awaysSelecteArray);
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"backarrayCount" object:self.awaysSelecteArray];
    
}


@end
