//
//  EmailRegisterViewController.m
//  PocketPlan
//
//  Created by Libo Liu on 13-11-12.
//  Copyright (c) 2013年 com.imaginato. All rights reserved.
//

#import "EmailRegisterViewController.h"
#import "AppDelegate.h"
#import <CoreText/CoreText.h>

#import "UIConstants.h"
#import "EmailLoginViewController.h"

#import "UIView+Helper.h"
#import "UIAlertView+BlocksKit.h"
#import "UILabel+Helper.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "UITextField+Helper.h"
#import "UIViewController+Helper.h"

#import "NavigationBarButtonItem.h"
#import "SendCall.h"

#import "PostDataHandler.h"
#import "LoginParamter.h"
#import "IMGCountryCode.h"
#import "CityUtils.h"
#import "IMGCity.h"
#import "SelectCityController.h"
#import "HomeViewController.h"
#import "LoadingView.h"
#import "V2_PreferenceViewController.h"
extern BOOL NeedExpandMenu;

@interface EmailRegisterViewController ()<UIPickerViewDataSource,UIPickerViewDelegate,SelectCityDelegate,TYAttributedLabelDelegate>
{
    BOOL isViewUp;
    UIScrollView *bgImageView;

    UITextField *emailTextField;
    UITextField *passwordTextField;
    UITextField *firstNameTextField;
    UITextField *lastNameTextField;
    UIButton *signUpButton;
    Label *loginButton;

    
    UIButton *mrButton;
    UIButton *msButton;
    UIButton *mrsButton;
    
    
    NSInteger sexSelect;
    
    CLLocationManager *clmanager;
    UILabel *errorLabel;
    UIButton *boarderControl;
    UIView *tfView;
}
@end

@implementation EmailRegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    signUpButton.enabled = YES;
    self.screenName=@"Register";
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden:YES];
    isViewUp=NO;
    [self loadMainView];
    [self createNavButton];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}
- (void)keyboardWillShow:(NSNotification *)notification
{
    NSArray *textFields = @[emailTextField, passwordTextField];
    UIView *focusView = nil;
    for (UITextField *view in textFields) {
        if ([view isFirstResponder]) {
            focusView = view;
            break;
        }
    }
    if (focusView) {
        double duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
        CGFloat keyboardY = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].origin.y;
        CGRect rect = [focusView convertRect:focusView.bounds toView:[[[UIApplication sharedApplication] delegate] window]];
        CGPoint tmp = rect.origin;
        CGFloat inputBoxY = tmp.y + focusView.frame.size.height;
        CGFloat ty = keyboardY - inputBoxY;
        [UIView animateWithDuration:duration animations:^{
            if (ty < 0) {
                [bgImageView setContentOffset:CGPointMake(0, -ty)];
            }
        }];
    }
}
- (void)keyboardWillHide:(NSNotification *)notification
{
    double duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:duration animations:^{
        [bgImageView setContentOffset:CGPointMake(0, 0)];
    }];
}

- (void)createNavButton{
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, IMG_StatusBarHeight, 45, 45)];
    [closeButton setImage:[UIImage imageNamed:@"leftback" ] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeButton];
    [self.view bringSubviewToFront:closeButton];
}
-(void)back
{
    [[Amplitude instance] logEvent:@"SU - Sign Up Cancel" withEventProperties:@{@"Sign Up Type":@"Email"}];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)loadMainView
{
    //set background view
//    UIImageView *imageView = [[UIImageView alloc]initWithFrame:self.view.bounds];
//    imageView.image = [UIImage imageNamed:@"ic_registe_bg"];
//    imageView.userInteractionEnabled = YES;
//    [self.view addSubview:imageView];
    self.view.backgroundColor = [UIColor whiteColor];
    
    bgImageView = [[UIScrollView alloc]initWithFrame:CGRectMake(0,0, DeviceWidth, DeviceHEIGHT)];
    bgImageView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bgImageView];
    if (@available(iOS 11.0, *)) {
        bgImageView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundTap:)];
    [bgImageView addGestureRecognizer:tapGesture];
    
    //set logo and title
    UIImageView *logoImageView= [UIImageView new];
    logoImageView.image = [UIImage imageNamed:@"Qraved Logo Red"];
    
    UILabel *lblJoin = [UILabel new];
    lblJoin.text = @"Join the Community";
    lblJoin.textColor = [UIColor color333333];
    lblJoin.font = [UIFont systemFontOfSize:18];
    lblJoin.textAlignment = NSTextAlignmentCenter;
    
    //set sex view
    UILabel * lblSex = [UILabel new];
    lblSex.text = @"I’m a";
    lblSex.textColor = [UIColor color333333];
    lblSex.font = [UIFont boldSystemFontOfSize:12];
    
    NSArray *sexArray = @[[UIImage imageNamed:@"ic_male"],[UIImage imageNamed:@"ic_female"]];
    mrButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mrButton addTarget:self action:@selector(sexSelect:) forControlEvents:UIControlEventTouchUpInside];
    [mrButton setImage:[sexArray objectAtIndex:0] forState:UIControlStateNormal];
    [mrButton setImage:[UIImage imageNamed:@"ic_male_select"] forState:UIControlStateSelected];
    sexSelect = 0;
    
    UILabel *lblMale = [UILabel new];
    lblMale.text = @"Male";
    lblMale.textColor = [UIColor color333333];
    lblMale.font = [UIFont systemFontOfSize:12];
    
    msButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [msButton addTarget:self action:@selector(sexSelect:) forControlEvents:UIControlEventTouchUpInside];
    [msButton setImage:[sexArray objectAtIndex:1] forState:UIControlStateNormal];
    [msButton setImage:[UIImage imageNamed:@"ic_female_select"] forState:UIControlStateSelected];
    
    UILabel *lblFemale = [UILabel new];
    lblFemale.text = @"Female";
    lblFemale.textColor = [UIColor color333333];
    lblFemale.font = [UIFont systemFontOfSize:12];
    
    tfView = [UIView new];
    tfView.backgroundColor = [UIColor whiteColor];
    tfView.layer.borderWidth = 1;
    tfView.layer.borderColor = [UIColor colorWithHexString:@"#EAEAEA"].CGColor;
    tfView.layer.cornerRadius = 3;
    tfView.layer.masksToBounds = YES;
    
    
    firstNameTextField = [[UITextField alloc]init];
    firstNameTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    firstNameTextField.font = [UIFont systemFontOfSize:14];
    firstNameTextField. attributedPlaceholder= [[NSAttributedString alloc] initWithString:L(@"First name") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#999999"]}];
    
    lastNameTextField = [[UITextField alloc]initWithFrame:CGRectMake(10, 0, DeviceWidth-68-25-10, 35)];
    lastNameTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    lastNameTextField.font = [UIFont systemFontOfSize:14];
    lastNameTextField. attributedPlaceholder= [[NSAttributedString alloc] initWithString:L(@"Last name") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#999999"]}];
    
    UIView *segName = [UIView new];
    segName.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    
    UIView *segEmail = [UIView new];
    segEmail.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    
    
    emailTextField = [[UITextField alloc]initWithFrame:CGRectMake(10, 0, DeviceWidth-68-25-10, 35)];
    emailTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    emailTextField.font = [UIFont systemFontOfSize:14];
    emailTextField. attributedPlaceholder= [[NSAttributedString alloc] initWithString:L(@"Email address") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#999999"]}];
    //emailTextField.textColor = [UIColor lightGrayColor];
    
    UIView *segPwd = [UIView new];
    segPwd.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    
    passwordTextField = [[UITextField alloc]initWithFrame:CGRectMake(10, 0, DeviceWidth-68-25-10, 35)];
    passwordTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    passwordTextField.font = [UIFont systemFontOfSize:14];
    passwordTextField. attributedPlaceholder= [[NSAttributedString alloc] initWithString:L(@"Password") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#999999"]}];
    
    firstNameTextField.returnKeyType = UIReturnKeyDone;
    firstNameTextField.delegate = self;
    
    lastNameTextField.returnKeyType = UIReturnKeyDone;
    lastNameTextField.delegate = self;
    
    emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    emailTextField.returnKeyType = UIReturnKeyDone;
    emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    emailTextField.delegate = self;
    
    passwordTextField.secureTextEntry = YES;
    passwordTextField.returnKeyType = UIReturnKeyDone;
    passwordTextField.delegate = self;
    
    [self addObserverWithTextField];
    
    [tfView sd_addSubviews:@[firstNameTextField,lastNameTextField,emailTextField,passwordTextField,segName,segEmail,segPwd]];
    
    UILabel *Password = [UILabel new];
    Password.textColor = [UIColor color999999];
    Password.text = @"Set password at least 8 characters.";
    Password.font = [UIFont systemFontOfSize:12];
    
    signUpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [signUpButton setBackgroundColor:[UIColor colorWithHexString:@"#EAEAEA"]];
    [signUpButton setTitle:L(@"CREATE ACCOUNT") forState:UIControlStateNormal];
    signUpButton.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    [signUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [signUpButton addTarget:self action:@selector(regButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgImageView addSubview:signUpButton];
    signUpButton.layer.masksToBounds = YES;
    signUpButton.layer.cornerRadius = 3;
    signUpButton.enabled = NO;
    
    [bgImageView sd_addSubviews:@[lblJoin,logoImageView,lblSex, msButton, mrButton, lblMale, lblFemale, tfView, Password,signUpButton]];
    
//    UIView *bottomView = [UIView new];
//    bottomView.backgroundColor = [UIColor whiteColor];
//
//    UIView *bottomLine = [UIView new];
//    bottomLine.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
//
//    TYAttributedLabel *bottomLabel = [[TYAttributedLabel alloc] init];
//    bottomLabel.delegate = self;
//    bottomLabel.backgroundColor = [UIColor whiteColor];
//    bottomLabel.text = @"Already have an account? Sign in";
//    bottomLabel.textAlignment = kCTTextAlignmentCenter;
//    [bottomLabel setTextColor:[UIColor color333333]];
//    bottomLabel.characterSpacing = 0;
//
//    bottomLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:14];
//    TYLinkTextStorage *linkTextStorage = [[TYLinkTextStorage alloc]init];
//    linkTextStorage.range = NSMakeRange(24,8);
//    linkTextStorage.textColor = [UIColor color333333];
//    linkTextStorage.linkData = @" Sign in";
//    linkTextStorage.font = [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:14];
//    linkTextStorage.underLineStyle = kCTUnderlineStyleNone;
//    [bottomLabel addTextStorage:linkTextStorage];
//
//
//    [bottomView sd_addSubviews:@[bottomLine,bottomLabel]];
    
//    [self.view addSubview:bottomView];
    
    
    logoImageView.sd_layout
    .centerXEqualToView(bgImageView)
    .topSpaceToView(bgImageView,84)
    .heightIs(61)
    .widthIs(163);
    
    lblJoin.sd_layout
    .topSpaceToView(logoImageView,28)
    .centerXEqualToView(bgImageView)
    .heightIs(20)
    .widthIs(210);
    
    lblSex.sd_layout
    .topSpaceToView(lblJoin,IS_IPHONE_5?43:53)
    .leftSpaceToView(bgImageView, 65)
    .widthIs(30)
    .heightIs(14);
    
    mrButton.sd_layout
    .topSpaceToView(lblJoin,18)
    .leftSpaceToView(lblSex,IS_IPHONE_5?20:34)
    .heightIs(IS_IPHONE_5?61:81)
    .widthIs(IS_IPHONE_5?61:81);
    
    lblMale.sd_layout
    .topSpaceToView(mrButton,4)
    .widthIs(27)
    .heightIs(12)
    .centerXEqualToView(mrButton);
   
    msButton.sd_layout
    .topEqualToView(mrButton)
    .leftSpaceToView(mrButton, 24)
    .heightIs(IS_IPHONE_5?61:81)
    .widthIs(IS_IPHONE_5?61:81);
    
    lblFemale.sd_layout
    .topSpaceToView(msButton,4)
    .centerXEqualToView(msButton)
    .widthIs(41)
    .heightIs(12);
    
    tfView.sd_layout
    .leftSpaceToView(bgImageView,42)
    .rightSpaceToView(bgImageView,42)
    .topSpaceToView(lblFemale,20)
    .heightIs(152);
    
    firstNameTextField.sd_layout
    .topSpaceToView(tfView,0)
    .leftSpaceToView(tfView, 15)
    .heightIs(50)
    .widthIs((DeviceWidth - 84)/2-20);
    
    lastNameTextField.sd_layout
    .topEqualToView(firstNameTextField)
    .leftSpaceToView(firstNameTextField, 20)
    .heightIs(50)
    .widthIs((DeviceWidth - 84)/2-20);
    
    emailTextField.sd_layout
    .topSpaceToView(firstNameTextField, 1)
    .leftEqualToView(firstNameTextField)
    .heightIs(50)
    .widthIs(DeviceWidth - 84-20);
    
    passwordTextField.sd_layout
    .topSpaceToView(emailTextField, 1)
    .leftEqualToView(firstNameTextField)
    .heightIs(50)
    .widthIs(DeviceWidth - 84-20);
    
    segName.sd_layout
    .topSpaceToView(tfView,0)
    .heightIs(50)
    .widthIs(1)
    .centerXEqualToView(tfView);
    
    segEmail.sd_layout
    .topSpaceToView(firstNameTextField,0)
    .leftSpaceToView(tfView,0)
    .rightSpaceToView(tfView,0)
    .heightIs(1);
    
    segPwd.sd_layout
    .topSpaceToView(emailTextField,0)
    .leftEqualToView(segEmail)
    .rightEqualToView(segEmail)
    .heightIs(1);
    
    Password.sd_layout
    .topSpaceToView(tfView, 8)
    .leftSpaceToView(bgImageView,45)
    .heightIs(14)
    .widthIs(DeviceWidth-90);
    
    signUpButton.sd_layout
    .topSpaceToView(Password, 21)
    .leftSpaceToView(bgImageView,42)
    .rightSpaceToView(bgImageView,42)
    .heightIs(50);
   
//    bottomView.sd_layout
//    .bottomSpaceToView(self.view, 0)
//    .leftSpaceToView(self.view, 0)
//    .rightSpaceToView(self.view, 0)
//    .heightIs(52);
//
//    bottomLine.sd_layout
//    .topSpaceToView(bottomView, 0)
//    .leftSpaceToView(bottomView, 0)
//    .rightSpaceToView(bottomView, 0)
//    .heightIs(1);
//
//    bottomLabel.sd_layout
//    .topSpaceToView(bottomLine, 17)
//    .leftSpaceToView(bottomView, 0)
//    .widthIs(DeviceWidth)
//    .heightIs(18);
    
    
    [bgImageView setupAutoContentSizeWithBottomView:signUpButton bottomMargin:80];
}

- (void)addObserverWithTextField{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidChange:)
                                                 name:UITextFieldTextDidChangeNotification object:firstNameTextField];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidChange:)
                                                 name:UITextFieldTextDidChangeNotification object:lastNameTextField];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidChange:)
                                                 name:UITextFieldTextDidChangeNotification object:emailTextField];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidChange:)
                                                 name:UITextFieldTextDidChangeNotification object:passwordTextField];
}

- (void)textFieldTextDidChange:(NSNotification *)notification
{
    [self validateUseInformation];
}

- (void)attributedLabel:(TYAttributedLabel *)attributedLabel textStorageClicked:(id<TYTextStorageProtocol>)TextRun atPoint:(CGPoint)point{
    if ([TextRun isKindOfClass:[TYLinkTextStorage class]]) {
        LoginViewController * loginVC = [[LoginViewController alloc] init];
        [self.navigationController pushViewController:loginVC animated:YES];
    }
    
}


#pragma mark textField Delegae
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (errorLabel) {
        [self removeErrorView];
        return;
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{

    [textField resignFirstResponder];
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (errorLabel) {
        [self removeErrorView];
        return YES;
    }

    return YES;
}
- (void)validateUseInformation{
    if (firstNameTextField.text.length > 0 && lastNameTextField.text.length >0 && emailTextField.text.length > 0 && passwordTextField.text.length > 0 && sexSelect != 0) {
        signUpButton.backgroundColor = [UIColor colorWithHexString:@"#D20000"];
        signUpButton.enabled = YES;
    }else{
        signUpButton.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
        signUpButton.enabled = NO;
    }
}

-(void)backgroundTap:(id)sender
{
    [self.view endEditing:YES];
}

 
-(void)sexSelect:(UIButton *)button{
    if (errorLabel) {
        [self removeErrorView];
        return;
    }
    button.selected = YES;

    if (button == mrButton) {
        sexSelect = 1;
        mrButton.backgroundColor = [UIColor colorWithHexString:@"#D20000"];
        mrButton.clipsToBounds = YES;
        mrButton.layer.cornerRadius = mrButton.height/2;
        msButton.backgroundColor = [UIColor whiteColor];
        msButton.selected = NO;
    }else if (button == msButton){
        sexSelect = 2;
        mrButton.selected = NO;
        msButton.backgroundColor = [UIColor colorWithHexString:@"#D20000"];
        msButton.clipsToBounds = YES;
        msButton.layer.cornerRadius = msButton.height/2;
        mrButton.backgroundColor = [UIColor whiteColor];

    }
    [self validateUseInformation];
}
-(void)regButtonClick:(id)sender{
  
    [[LoadingView sharedLoadingView] startLoading];
    NSString *trimmedString1 = [firstNameTextField.text stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceCharacterSet]];
    NSString *trimmedString2 = [lastNameTextField.text stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceCharacterSet]];
    
    if (sexSelect == 0) {
        
        [self errorViewShowWithMessage:L(@"Please select your gender.") andTextField:nil];
        return;
    }
    if(firstNameTextField.text.length==0 || trimmedString1.length==0)
    {
        [self errorViewShowWithMessage:L(@"Please enter your first name.") andTextField:firstNameTextField];
        return ;
    }
    if( (firstNameTextField.text.length>20) || (trimmedString1.length>20) )
    {
        [self errorViewShowWithMessage:L(@"First name should be at most 20 characters.") andTextField:firstNameTextField];
        
        return ;
    }
    if(lastNameTextField.text.length==0 || trimmedString2.length==0)
    {
        [self errorViewShowWithMessage:L(@"Please enter your last name.") andTextField:lastNameTextField];
        
        return ;
    }
    if( (lastNameTextField.text.length>20) || (trimmedString2.length>20) )
    {
        [self errorViewShowWithMessage:L(@"Last name should be at most 20 characters.") andTextField:lastNameTextField];
        
        return ;
    }
    
    if(emailTextField.text.length==0)
    {
        [self errorViewShowWithMessage:L(@"Please enter your email address.") andTextField:emailTextField];
        
        return;
    }
    
    if (![emailTextField.text isValidateEmail]) {
        [self errorViewShowWithMessage:@"The format of the email address is invalid." andTextField:emailTextField];
        return;
    }
    if(passwordTextField.text.length<6)
    {
        if (passwordTextField.text.length == 0) {
            [self errorViewShowWithMessage:L(@"Please enter your password.") andTextField:passwordTextField];
            
        }
        else
        {
            [self errorViewShowWithMessage:L(@"Password should be between 6~18 characters.") andTextField:passwordTextField];
        }
        
        return;
    }
    else if(passwordTextField.text.length>18)
    {
        [self errorViewShowWithMessage:L(@"Password should be between 6~18 characters.") andTextField:passwordTextField];
        return;
    }
    
    signUpButton.enabled = NO;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:[NSString stringWithFormat:@"%ld",(long)sexSelect] forKey:@"gender"];
    [parameters setObject:firstNameTextField.text forKey:@"first_name"];
    [parameters setObject:lastNameTextField.text forKey:@"last_name"];
    [parameters setObject:@"01/01/1937" forKey:@"birthday"];
    [parameters setObject:emailTextField.text forKey:@"email"];
    [parameters setObject:[passwordTextField.text MD5String] forKey:@"password"];
    [parameters setObject:@"signupwithself" forKey:@"loginwith"];
    [parameters setObject:@"4" forKey:@"productSource"];
    [parameters setObject:@"1" forKey:@"sourceType"];
    [LoginParamter sharedParameter].loginwith=USER_LOGINTYPE_WITHSELF;
    
    UserDataHandler *userDataHandler=[[UserDataHandler alloc]init];
    [userDataHandler requestLogin:parameters delegateWithBlock:^{
        [IMGAmplitudeUtil trackSignInWithName:@"SU - Sign Up Succeed" andType:@"Email" andLocation:nil andReason:nil];
        [[AppsFlyerTracker sharedTracker] trackEvent:@"SU - Sign Up Succeed" withValues:@{@"Sign Up Type":@"Email",@"Reason":@"Email valid"}];
        //[[LoadingView sharedLoadingView] startLoading];
        signUpButton.enabled = YES;
        //        [MixpanelHelper trackSignUpWithTab:@"Email"];
        [self.delegate signUpSuccess];
        [AppDelegate ShareApp].userLogined=YES;
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
        [self selectCity];
  
        UIButton *likeCommentShareButton = [self.autoPostDictionary objectForKey:@"likeCommentShareButton"];
        if ( self.autoPostDictionary &&  (likeCommentShareButton != nil) && self.autoPostDelegate && ([self.autoPostDelegate respondsToSelector:@selector(communityButtonTapped:)]) ) {
            [self.autoPostDelegate communityButtonTapped:likeCommentShareButton];
            self.autoPostDelegate = nil;
        }
        
        if ( self.autoPostDictionary && ([self.autoPostDictionary objectForKey:@"AlbumActionButtonsType"]!=nil) &&  self.autoPostDelegate && ([self.autoPostDelegate respondsToSelector:@selector(albumActionButtonsClick:)]) ) {
            [self.autoPostDelegate albumActionButtonsClick:[[self.autoPostDictionary objectForKey:@"AlbumActionButtonsType"] integerValue]];
            self.autoPostDelegate = nil;
        }
        
        id postComment = [self.autoPostDictionary objectForKey:@"postComment"];
        if ( self.autoPostDictionary &&  (postComment != nil) && self.autoPostDelegate && ([self.autoPostDelegate respondsToSelector:@selector(postComment:)]) ) {
            [self.autoPostDelegate postComment:postComment];
            self.autoPostDelegate = nil;
        }
        
        if (self.autoPostDelegate && ([self.autoPostDelegate respondsToSelector:@selector(publishReview)]) ) {
            [self.autoPostDelegate publishReview];
            self.autoPostDelegate = nil;
        }
        if (self.isfromBookingInfo) {
            [self successSignUp];
        }
        
    } failedBlock:^(NSString *errorMsg){
        signUpButton.enabled = YES;
        [self createErrorAlert:errorMsg];
    }];
}


- (void)createErrorAlert:(NSString *)errorMsg{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Email already registered" message:@"The email address you have entered is already registered. Please log in or use different email address." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionLog = [UIAlertAction actionWithTitle:@"Log In" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [IMGAmplitudeUtil trackSignInWithName:@"SI - Sign In Initiate" andType:@"Email" andLocation:@"Onboarding Page" andReason:nil];
        EmailLoginViewController *emailLoginViewController = [[EmailLoginViewController alloc]init];
        [self.navigationController pushViewController:emailLoginViewController animated:YES];
    }];
    
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:actionLog];
    [alertController addAction:actionCancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

-(void)errorViewShowWithMessage:(NSString*)message andTextField:(UITextField*)textField{
    [[LoadingView sharedLoadingView] stopLoading];

    if (errorLabel) {
        [self removeErrorView];
    }

    errorLabel = [UILabel new];
    errorLabel.text = message;
    errorLabel.font = [UIFont systemFontOfSize:15];
    errorLabel.backgroundColor = [UIColor colorC2060A];
    [errorLabel setTextColor:[UIColor whiteColor]];
    errorLabel.textAlignment = NSTextAlignmentCenter;
    [bgImageView addSubview:errorLabel];
    
    errorLabel.sd_layout
    .bottomSpaceToView(signUpButton, 5)
    .leftSpaceToView(bgImageView, 0)
    .widthIs(DeviceWidth)
    .heightIs(40);
}

-(void)removeErrorView{
    [errorLabel removeFromSuperview];
    //[boarderControl removeFromSuperview];
    errorLabel = nil;
   // boarderControl = nil;
}
//-(void)locationManager:(CLLocationManager*)manager didUpdateLocations:(NSArray*)locations{
//    [manager stopUpdatingLocation];
//    CLGeocoder *geocoder=[[CLGeocoder alloc] init];
//    if(locations.count){
//        CLLocation *location=[locations lastObject];
//        // CLLocation *location=[[CLLocation alloc] initWithLatitude:45.04 longitude:7.42];
//        if(location.horizontalAccuracy>0){
//            [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks,NSError *error){
//                if(error==nil && placemarks.count){
//                    __block CLPlacemark *place=[placemarks objectAtIndex:0];
//                    [CityUtils getCitiesWithCountryId:0 andBlock:^(NSArray *cities){
//                        for(IMGCity *city in cities){
//                            if([city.name.lowercaseString isEqualToString:place.locality.lowercaseString]){
//                                NSLog(@"city name:%@",city.name);
//                                [[NSUserDefaults standardUserDefaults] setObject:city.cityId forKey:CITY_SELECT_ID];
//                                [[NSUserDefaults standardUserDefaults] setObject:city.name forKey:CITY_SELECT];
//                                [[NSNotificationCenter defaultCenter] postNotificationName:CITY_SELECT_NOTIFICATION object:nil];
//                                [self selectCity];
//                                return;
//                            }
//                        }
//                        [self selectCity];
//                    }];
//                }else{
//                    [self selectCity];
//                }
//            }];
//        }else{
//            [self selectCity];
//        }
//    }else{
//        [self selectCity];
//    }
//}
//
//-(void)locationManager:(CLLocationManager*)manager didFailWithError:(NSError*)error{
//    [clmanager stopUpdatingLocation];
//    [self selectCity];
//}

-(void)selectCity{
    [[LoadingView sharedLoadingView] stopLoading];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"isFromWebLink"] == nil) {
        [self requestDeeplink];
        [[NSUserDefaults standardUserDefaults] setObject:@"webLink" forKey:@"isFromWebLink"];
        return ;
    }
         
    [self signUpSuc];
}

- (void)signUpSuc{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"isOpenInApp"] != nil){
        [AppDelegate ShareApp].userLogined = YES;
        [[AppDelegate ShareApp] gotoHomePage];
    }else{
        
        V2_PreferenceViewController *V2_PreferenceVC = [[V2_PreferenceViewController alloc] init];
        //    V2_PreferenceVC.isFromProfile = YES;
        //    V2_PreferenceVC.isHaveOnBoarding = YES;
        [self.navigationController pushViewController:V2_PreferenceVC animated:YES];
    }
}

- (void)requestDeeplink{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"installDeeplink"] == nil) {
        UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectZero];
        NSString *userAgent = [webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
        NSDictionary *dic = @{@"userAgent":userAgent};
        
        [UserDataHandler deeplink:dic andSuccessBlock:^(id responseObject) {
            if (responseObject==nil) {
                [self signUpSuc];
            }else{
                [AppDelegate ShareApp].userLogined = YES;
                [[AppDelegate ShareApp] gotoHomePage];
            }
            
        } andFailedBlock:^{
            [self signUpSuc];
        }];
    }
}
         
-(void)successSelectCity{
    
    if([AppDelegate ShareApp].userLogined){
        [[AppDelegate ShareApp] requestToShowNotificationCount];
    }
    if (self.reviewPublishViewDelegate && ([self.reviewPublishViewDelegate respondsToSelector:@selector(postPhoto)]) ) {
        [self.reviewPublishViewDelegate postPhoto];
        self.reviewPublishViewDelegate = nil;
    }
}

-(void)loginBurronClick
{
    for(UIViewController *item in self.navigationController.viewControllers) {
        if ([item isKindOfClass:[LoginViewController class]] || [item isKindOfClass:[EmailLoginViewController class]]) {
            [self.navigationController popToViewController:item animated:YES];
            return;
        }
    }
    if (self.isFromSignUpSplash) {
        [self dismissViewControllerAnimated:NO completion:nil];
        [[AppDelegate ShareApp] goToLoginController];
    }
}

-(void)successSignUp{
    [AppDelegate ShareApp].userLogined=YES;
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
    [[AppDelegate ShareApp] requestToShowNotificationCount];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loginNotification" object:nil];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
