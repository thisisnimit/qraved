//
//  EnterEmailViewController.h
//  Qraved
//
//  Created by Shine Wang on 11/18/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseChildViewController.h"


@protocol registerDelegate <NSObject>

-(void)registerSuccess;

@end

@interface EnterEmailViewController : BaseChildViewController

@property(nonatomic,assign) id<registerDelegate>delegate;
@end
