//
//  MoreOptionsViewController.m
//  Qraved
//
//  Created by harry on 2018/3/6.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "MoreOptionsViewController.h"
#import "EmailRegisterViewController.h"
#import "EmailLoginViewController.h"
#import "V2_PreferenceViewController.h"
#import "FBSignUpViewController.h"
#import "LoginParamter.h"
@interface MoreOptionsViewController ()<GIDSignInDelegate,GIDSignInUIDelegate>
{
    NSString *ampitudeType;
}
@end

@implementation MoreOptionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createNavButton];
    [self loadMainUI];
    
    [GIDSignIn sharedInstance].uiDelegate=self;
    [GIDSignIn sharedInstance].delegate = self;
}

- (void)loadMainUI{
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIImageView *logoImg = [UIImageView new];
    logoImg.image = [UIImage imageNamed:@"Qraved Logo Red"];
    
    FunctionButton *btnFaceBook = [FunctionButton signButtonWithType:UIButtonTypeCustom andTitle:@"FACEBOOK" andBGColor:[UIColor colorWithHexString:@"#39579B"] andTitleColor:[UIColor whiteColor]];
    [btnFaceBook addTarget:self action:@selector(facebookLogin) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imgFaceBook = [UIImageView new];
    imgFaceBook.image = [UIImage imageNamed:@"ic_fb_login"];
    [btnFaceBook addSubview:imgFaceBook];
    
    FunctionButton *btnGoogle = [FunctionButton signButtonWithType:UIButtonTypeCustom andTitle:@"GOOGLE" andBGColor:[UIColor whiteColor] andTitleColor:[UIColor color333333]];
    [btnGoogle addTarget:self action:@selector(googleLogin) forControlEvents:UIControlEventTouchUpInside];
    btnGoogle.layer.borderWidth = 1;
    btnGoogle.layer.borderColor = [UIColor colorWithHexString:@"#EAEAEA"].CGColor;
    
    UIImageView *imgGoogle = [UIImageView new];
    imgGoogle.image = [UIImage imageNamed:@"google"];
    [btnGoogle addSubview:imgGoogle];
    
    FunctionButton *btnEmail = [FunctionButton signButtonWithType:UIButtonTypeCustom andTitle:@"EMAIL" andBGColor:[UIColor whiteColor] andTitleColor:[UIColor color333333]];
    [btnEmail addTarget:self action:@selector(emailLogin) forControlEvents:UIControlEventTouchUpInside];
    btnEmail.layer.borderWidth = 1;
    btnEmail.layer.borderColor = [UIColor colorWithHexString:@"#EAEAEA"].CGColor;
    
    FunctionButton *btnCreate = [FunctionButton signButtonWithType:UIButtonTypeCustom andTitle:@"CREATE ACCOUNT" andBGColor:[UIColor whiteColor] andTitleColor:[UIColor color333333]];
    [btnCreate addTarget:self action:@selector(createAccount) forControlEvents:UIControlEventTouchUpInside];
    btnCreate.layer.borderWidth = 1;
    btnCreate.layer.borderColor = [UIColor colorWithHexString:@"#EAEAEA"].CGColor;
    [self.view sd_addSubviews:@[logoImg, btnFaceBook, btnGoogle, btnEmail, btnCreate]];
    
    logoImg.sd_layout
    .topSpaceToView(self.view, 84)
    .centerXEqualToView(self.view)
    .widthIs(163)
    .heightIs(61);
    
    btnFaceBook.sd_layout
    .topSpaceToView(logoImg, 80)
    .leftSpaceToView(self.view, 42)
    .rightSpaceToView(self.view, 42)
    .heightIs(50);
    
    imgFaceBook.sd_layout
    .leftSpaceToView(btnFaceBook, 13)
    .widthIs(16)
    .heightIs(16)
    .centerYEqualToView(btnFaceBook);
    
    btnGoogle.sd_layout
    .topSpaceToView(btnFaceBook, 15)
    .leftEqualToView(btnFaceBook)
    .rightEqualToView(btnFaceBook)
    .heightIs(50);
    
    imgGoogle.sd_layout
    .leftSpaceToView(btnGoogle, 13)
    .widthIs(16)
    .heightEqualToWidth()
    .centerYEqualToView(btnGoogle);
    
    btnEmail.sd_layout
    .topSpaceToView(btnGoogle, 15)
    .leftEqualToView(btnGoogle)
    .rightEqualToView(btnGoogle)
    .heightIs(50);
    
    btnCreate.sd_layout
    .topSpaceToView(btnEmail, 15)
    .leftEqualToView(btnEmail)
    .rightEqualToView(btnEmail)
    .heightIs(50);
}

- (void)createNavButton{
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"icon_black_wrong"] forState:UIControlStateNormal];
    backButton.frame = CGRectMake(LEFTLEFTSET, IMG_StatusBarHeight, 60,44);
    [backButton addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    backButton.imageEdgeInsets = UIEdgeInsetsMake(12, 0, 12, 40);
    [self.view addSubview:backButton];
    [self.view bringSubviewToFront:backButton];
}

- (void)facebookLogin{
    ampitudeType = @"Facebook";
    [IMGAmplitudeUtil trackSignInWithName:@"SI - Sign In Initiate" andType:@"Facebook" andLocation:@"Onboarding Page" andReason:nil];
    
    NSArray *permissions = @[@"public_profile", @"email"];
    UserDataHandler *userDataHandler = [[UserDataHandler alloc] init];
    [userDataHandler faceBookLogin:permissions delegateWithBlock:^{
        [[LoadingView sharedLoadingView] stopLoading];
        NSDictionary *dataDic = [[NSDictionary alloc] initWithDictionary:[userDataHandler getParameters]];
        if ([userDataHandler isSignUp])
        {
            [self loginSuccessful];
        }else
        {
            [self loginWithFacebook:dataDic];
        }
    } failedBlock:^{
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

- (void)googleLogin{
    [[LoadingView sharedLoadingView] startLoading];
    [IMGAmplitudeUtil trackSignInWithName:@"SI - Sign In Initiate" andType:@"Google" andLocation:@"Onboarding Page" andReason:nil];
    ampitudeType = @"Google";
    [[GIDSignIn sharedInstance]signIn];
}

- (void)emailLogin{
    ampitudeType = @"Email";
    [IMGAmplitudeUtil trackSignInWithName:@"SI - Sign In Initiate" andType:@"Email" andLocation:@"Onboarding Page" andReason:nil];
    EmailLoginViewController *emailLoginViewController = [[EmailLoginViewController alloc]init];
    [self.navigationController pushViewController:emailLoginViewController animated:YES];
}

- (void)createAccount{
    [IMGAmplitudeUtil trackSignInWithName:@"SU - Sign Up Initiate" andType:@"Email" andLocation:@"Onboarding Page" andReason:nil];
    EmailRegisterViewController *emailRegisterViewController=[[EmailRegisterViewController alloc]init];
    [self.navigationController pushViewController:emailRegisterViewController animated:YES];
}

-(void)close{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBarHidden = YES;
    [[LoadingView sharedLoadingView] stopLoading];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [[LoadingView sharedLoadingView] stopLoading];
}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *name = user.profile.name;
    NSString *email = user.profile.email;
    NSLog(@"Customer details: %@ %@ %@ %@", userId, idToken, name, email);
    [[LoadingView sharedLoadingView] stopLoading];
    if (![user.userID intValue]) {
        //        [[Amplitude instance] logEvent: @"SU - Sign up Cancel" withEventProperties:@{@"Sign Up Type":@"Google"}];
        [IMGAmplitudeUtil trackSignInWithName:@"SI - Sign In Cancel" andType:@"Google" andLocation:nil andReason:nil];
    }
    UserDataHandler *userDataHandler = [[UserDataHandler alloc] init];
    [userDataHandler googleLogin:user error:error delegateWithBlock:^{
        
        NSDictionary *dataDic = [[NSDictionary alloc] initWithDictionary:[userDataHandler getParameters]];
        if ([userDataHandler isSignUp])
        {
            [self loginSuccessful];
        }
        else
        {
            [self googleLoginWithDic:dataDic];
        }
    } failedBlock:^{
        
        //[self enterEmail];
        
    }];
    
}

- (void)googleLoginWithDic:(NSDictionary *)dic{
    
    NSString *firstName = [[dic objectForKey:@"first_name"] stringByTrimmingCharactersInSet:
                           [NSCharacterSet whitespaceCharacterSet]];
    NSString *lastName = [[dic objectForKey:@"last_name"] stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceCharacterSet]];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:[NSString stringWithFormat:@"%@",@0] forKey:@"gender"];
    [parameters setObject:firstName forKey:@"firstName"];
    [parameters setObject:lastName forKey:@"lastName"];
    [parameters setObject:@"01/01/1937" forKey:@"birthday"];
    
    if ([[dic allKeys] containsObject:@"email"]) {
        [parameters setObject:[dic objectForKey:@"email"] forKey:@"email"];
        
    }else{
        FBSignUpViewController *fbsuvc = [[FBSignUpViewController alloc] init];
        fbsuvc.dataDic = dic;
        fbsuvc.amplitudeType = @"Google";
        [self.navigationController pushViewController:fbsuvc animated:YES];
        return;
        
    }
    [parameters setObject:@"signupwithself" forKey:@"loginwith"];
    [parameters setObject:@"4" forKey:@"productSource"];
    [parameters setObject:@"1" forKey:@"sourceType"];
    [parameters setObject:[dic objectForKey:@"thirdPartyType"] forKey:@"thirdPartyType"];
    [parameters setObject:[dic objectForKey:@"thirdPartyId"] forKey:@"thirdPartyId"];
    if ([dic objectForKey:@"picture"]) {
        [parameters setObject:[dic objectForKey:@"picture"] forKey:@"picture"];
        
    }
    
    [LoginParamter sharedParameter].loginwith=USER_LOGINTYPE_WITHSELF;
    
    UserDataHandler *userDataHandler=[[UserDataHandler alloc]init];
    [userDataHandler faceBookSignUp:parameters withBlock:^() {
        [[LoadingView sharedLoadingView] startLoading];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFacebookSignUp"];
        
        [AppDelegate ShareApp].userLogined=YES;
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
        
        [self loginSuccessful];
        
    } failedBlock:^(NSString *errorMsg) {
        
        //[self enterEmail];
    }];
}


-(void)loginWithFacebook:(NSDictionary*)dataDict{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:[dataDict objectForKey:@"first_name"] forKey:@"firstName"];
    [parameters setObject:[dataDict objectForKey:@"last_name"] forKey:@"lastName"];
    
    if ([[dataDict allKeys] containsObject:@"email"]) {
        [parameters setObject:[dataDict objectForKey:@"email"] forKey:@"email"];
        
    }
    else{
        FBSignUpViewController *fbsuvc = [[FBSignUpViewController alloc] init];
        fbsuvc.dataDic = dataDict;
        fbsuvc.amplitudeType = @"Facebook";
        [self.navigationController pushViewController:fbsuvc animated:YES];
        return;
        
    }
    [parameters setObject:@"signupwithself" forKey:@"loginwith"];
    [parameters setObject:@"1" forKey:@"sourceType"];
    [parameters setObject:@"4" forKey:@"productSource"];
    [parameters setObject:[dataDict objectForKey:@"thirdPartyType"] forKey:@"thirdPartyType"];
    [parameters setObject:[dataDict objectForKey:@"thirdPartyId"] forKey:@"thirdPartyId"];
    [parameters setObject:[dataDict objectForKey:@"picture"] forKey:@"picture"];
    
    [LoginParamter sharedParameter].loginwith=USER_LOGINTYPE_WITHSELF;
    
    UserDataHandler *userDataHandler=[[UserDataHandler alloc]init];
    [userDataHandler faceBookSignUp:parameters withBlock:^() {
        [[LoadingView sharedLoadingView] stopLoading];
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"loginSuccessful" object:nil];
        [AppDelegate ShareApp].userLogined=YES;
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
        [self loginSuccessful];
    } failedBlock:^(NSString *errorMsg) {
        
        [[Amplitude instance]
         logEvent:@"SU - Sign Up Failed" withEventProperties:@{@"Reason":errorMsg,@"Sign in type":@"Facebook"}];
    }];
    
}

- (void)loginSuccessful {
    if (ampitudeType.length>0) {
        [IMGAmplitudeUtil trackSignInWithName:@"SI - Sign In Succeed" andType:ampitudeType andLocation:nil andReason:nil];
        [[NSUserDefaults standardUserDefaults] setValue:ampitudeType forKey:@"loginType"];
    }
    [PushHelper postUserDeviceToken];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"isFromWebLink"] == nil) {
        [self requestDeeplink];
        [[NSUserDefaults standardUserDefaults] setObject:@"webLink" forKey:@"isFromWebLink"];
        return ;
    }
    
    [self loginSuc];
}

- (void)loginSuc{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"isOpenInApp"] != nil){
        [AppDelegate ShareApp].userLogined = YES;
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[AppDelegate ShareApp] gotoHomePage];
    }else{
        NSString *onboardingString = [[NSUserDefaults standardUserDefaults] objectForKey:ONBOARDING];
        if (onboardingString) {
            [AppDelegate ShareApp].userLogined = YES;
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[AppDelegate ShareApp] gotoHomePage];
            
        }else{
            
            V2_PreferenceViewController *V2_PreferenceVC = [[V2_PreferenceViewController alloc] init];
            V2_PreferenceVC.isFromProfile = YES;
            V2_PreferenceVC.isHaveOnBoarding = YES;
            [self.navigationController pushViewController:V2_PreferenceVC animated:YES];
        }
        
    }
}

- (void)requestDeeplink{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"installDeeplink"] == nil) {
        UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectZero];
        NSString *userAgent = [webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
        NSDictionary *dic = @{@"userAgent":userAgent};
        
        [UserDataHandler deeplink:dic andSuccessBlock:^(id responseObject) {
            if (responseObject == nil) {
                [self loginSuc];
            }else{
                [AppDelegate ShareApp].userLogined = YES;
                [[AppDelegate ShareApp] gotoHomePage];
            }
            
        } andFailedBlock:^{
            [self loginSuc];
        }];
    }
}

@end
