//
//  V2_LovePreferenceLayout.m
//  Qraved
//
//  Created by harry on 2017/6/11.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_LovePreferenceLayout.h"

#define WIDTH [UIScreen mainScreen].bounds.size.width

@interface V2_LovePreferenceLayout ()
{
    CGFloat _sumWidth ;
}
@property (nonatomic,assign)CGFloat betweenOfCell;

@end

@implementation V2_LovePreferenceLayout
{
    NSMutableArray * attributeAttay;
}

-(void)prepareLayout{
    [super prepareLayout];
  
    self.scrollDirection = UICollectionViewScrollDirectionVertical;
    self.sectionInset = UIEdgeInsetsMake(15, 15, 5, 0);
    self.betweenOfCell = 5;
    self.minimumLineSpacing = 5.0;
    
}

// 返回每一个item的attribute
- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    NSArray * layoutAttributes_t = [super layoutAttributesForElementsInRect:rect];
    NSArray * layoutAttributes = [[NSArray alloc]initWithArray:layoutAttributes_t copyItems:YES];
    //用来临时存放一行的Cell数组
    NSMutableArray * layoutAttributesTemp = [[NSMutableArray alloc]init];
    for (NSUInteger index = 0; index < layoutAttributes.count ; index++) {
        
        UICollectionViewLayoutAttributes *currentAttr = layoutAttributes[index]; // 当前cell的位置信息
        UICollectionViewLayoutAttributes *previousAttr = index == 0 ? nil : layoutAttributes[index-1];
        UICollectionViewLayoutAttributes *nextAttr = index + 1 == layoutAttributes.count ?
        nil : layoutAttributes[index+1];
        
        //加入临时数组
        [layoutAttributesTemp addObject:currentAttr];
        _sumWidth += currentAttr.frame.size.width;
        
        CGFloat previousY = previousAttr == nil ? 0 : CGRectGetMaxY(previousAttr.frame);
        CGFloat currentY = CGRectGetMaxY(currentAttr.frame);
        CGFloat nextY = nextAttr == nil ? 0 : CGRectGetMaxY(nextAttr.frame);
        //如果当前cell是单独一行
        if (currentY != previousY && currentY != nextY){
            if ([currentAttr.representedElementKind isEqualToString:UICollectionElementKindSectionHeader]) {
                [layoutAttributesTemp removeAllObjects];
                _sumWidth = 0.0;
            }else if ([currentAttr.representedElementKind isEqualToString:UICollectionElementKindSectionFooter]){
                [layoutAttributesTemp removeAllObjects];
                _sumWidth = 0.0;
            }else{
                [self setCellFrameWith:layoutAttributesTemp];
            }
        }
        //如果下一个cell在本行，这开始调整Frame位置
        else if( currentY != nextY) {
            [self setCellFrameWith:layoutAttributesTemp];
        }
    }
    return layoutAttributes;

}
-(void)setCellFrameWith:(NSMutableArray*)layoutAttributes{
    CGFloat nowWidth = 0.0;
//    switch (_cellType) {
//        case AlignWithLeft:
            nowWidth = self.sectionInset.left;
            for (UICollectionViewLayoutAttributes * attributes in layoutAttributes) {
                CGRect nowFrame = attributes.frame;
                nowFrame.origin.x = nowWidth;
                attributes.frame = nowFrame;
                nowWidth += nowFrame.size.width + self.betweenOfCell;
            }
            _sumWidth = 0.0;
            [layoutAttributes removeAllObjects];
//            break;
//        case AlignWithCenter:
//            nowWidth = (self.collectionView.frame.size.width - _sumWidth - (layoutAttributes.count * _betweenOfCell)) / 2;
//            for (UICollectionViewLayoutAttributes * attributes in layoutAttributes) {
//                CGRect nowFrame = attributes.frame;
//                nowFrame.origin.x = nowWidth;
//                attributes.frame = nowFrame;
//                nowWidth += nowFrame.size.width + self.betweenOfCell;
//            }
//            _sumWidth = 0.0;
//            [layoutAttributes removeAllObjects];
//            break;
//            
//        case AlignWithRight:
//            nowWidth = self.collectionView.frame.size.width - self.sectionInset.right;
//            for (NSInteger index = layoutAttributes.count - 1 ; index >= 0 ; index-- ) {
//                UICollectionViewLayoutAttributes * attributes = layoutAttributes[index];
//                CGRect nowFrame = attributes.frame;
//                nowFrame.origin.x = nowWidth - nowFrame.size.width;
//                attributes.frame = nowFrame;
//                nowWidth = nowWidth - nowFrame.size.width - _betweenOfCell;
//            }
//            _sumWidth = 0.0;
//            [layoutAttributes removeAllObjects];
//            break;
//            
//    }
}

@end
