//
//  V2_LovePreferenceViewController.h
//  Qraved
//
//  Created by harry on 2017/6/8.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "GAITrackedViewController.h"

@interface V2_LovePreferenceViewController : GAITrackedViewController

@property (nonatomic, strong) NSArray *cuisineArray;
@property (nonatomic, strong) NSArray *eatingArray;

@property (nonatomic, strong)  NSArray  *aarayArray;
@property (nonatomic, strong)  NSMutableArray  *awaysSelecteArray;
@end
