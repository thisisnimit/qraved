//
//  EnterEmailViewController.m
//  Qraved
//
//  Created by Shine Wang on 11/18/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "EnterEmailViewController.h"
#import "AppDelegate.h"
#import "UserDataHandler.h"
#import "LoginParamter.h"
#import "TextValidator.h"
#import "UIAlertView+BlocksKit.h"
#import "UIColor+Helper.h"


@interface EnterEmailViewController ()
@property (nonatomic,retain) UITextField *emailTextField;
@property (nonatomic,retain)  UILabel *firstNameLabel;

@end

@implementation EnterEmailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [self.navigationItem setTitle:L(@"Enter Email")];
    
    UILabel *welcomeLabel=[[UILabel alloc]initWithFrame:CGRectMake(28, 34, 177, 29)];
    UIFont *font=[UIFont fontWithName:@"HelveticaNeue-Bold" size:18];
    [welcomeLabel setText:L(@"Welcome to Qraved,")];
    [welcomeLabel setFont:font];
    [self.view addSubview:welcomeLabel];
    
    UILabel *descriptionLabel=[[UILabel alloc]initWithFrame:CGRectMake(28, 71, 266, 84)];
    UIFont *font1=[UIFont systemFontOfSize:15];
    [descriptionLabel setText:L(@"To complete your Qraved registration, please enter your email address, don't worry, we will never share your private information with others!")];
    [descriptionLabel setFont:font1];
    [descriptionLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [descriptionLabel setNumberOfLines:10];
    [self.view addSubview:descriptionLabel];

    self.firstNameLabel=[[UILabel alloc]initWithFrame:CGRectMake(206, 34, 93, 29)];
    NSString *firstName=[[LoginParamter sharedParameter].paramters objectForKey:@"first_name"];
    if(firstName){
        self.firstNameLabel.text=firstName;
    }
    [self.view addSubview:self.firstNameLabel];
    
    self.emailTextField=[[UITextField alloc]initWithFrame:CGRectMake(28, 171, 266, 30)];
    [self.emailTextField setFont:font1];
    self.emailTextField.layer.borderColor=[UIColor grayColor].CGColor;
    self.emailTextField.layer.borderWidth=0.3;
    self.emailTextField.layer.cornerRadius=8;
    [self.emailTextField setPlaceholder:L(@"Enter your email")];
    [self.view addSubview:self.emailTextField];

    UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake(103, 216, 114, 30)];
    [button addTarget:self action:@selector(submitClicked:) forControlEvents:UIControlEventTouchUpInside];
    [button setBackgroundColor:[UIColor colorC2060A]];
    [button setTitle:L(@"Submit") forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont systemFontOfSize:15]];
    [self.view addSubview:button];
}


-(BOOL)checkEmail{
    return [TextValidator isValidEmail:self.emailTextField.text];
}


- (void)submitClicked:(id)sender {
    if([self checkEmail])
    {

        UserDataHandler *userdataHandler=[[UserDataHandler alloc]init];
        NSMutableDictionary *parameters=[LoginParamter sharedParameter].paramters;
        [parameters setObject:@"0" forKey:@"manual"];
        [parameters setObject:self.emailTextField.text forKey:@"email"];
        [userdataHandler requestLogin:[LoginParamter sharedParameter].paramters delegateWithBlock:^{
            [self successFul];
        } failedBlock:^(NSString *errorMsg){
            
        }];
    }else{
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"" message:L(@"Invalid email address.") delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

-(void)successFul
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    
    [self.delegate registerSuccess];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.emailTextField resignFirstResponder];
}

@end
