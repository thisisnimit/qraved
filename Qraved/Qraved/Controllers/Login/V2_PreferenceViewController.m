//
//  V2_PreferenceViewController.m
//  Qraved
//
//  Created by harry on 2017/6/8.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_PreferenceViewController.h"
#import <BlocksKit/BlocksKit+UIKit.h>
#import "V2_EatPreferenceViewController.h"
#import "V2_LovePreferenceViewController.h"
#import "V2_BrandPreferenceViewController.h"
#import "PersonalizationHandler.h"
#import "V2_BrandModelSuper.h"
#import "V2_BrandModel.h"
#import "LoadingView.h"
@interface V2_PreferenceViewController ()<UIScrollViewDelegate>
{
    UIButton *btnNext;
//    UIPageControl *pageControl;
    UIScrollView *scrollPreference;
    int currentIndex;
    UIButton *closeButton;
    V2_EatPreferenceViewController *eatViewController;
    V2_LovePreferenceViewController *cuisineViewController;
    V2_BrandPreferenceViewController *brandViewController;
    NSMutableArray *arrayCount;
    UILabel *lblnum;
    UILabel *lbltitle;
    int selectCount;
}
@end

@implementation V2_PreferenceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    arrayCount = [NSMutableArray array];
    currentIndex = 0;
    selectCount = 0;
    
    [self requestData];
    if (!self.isFromProfile) {
        [self loadMainView];
//        [self createNav];
    }
    [self createNav];
    self.awaysSelecteArray = [NSArray array];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(arrraycount:) name:@"arrayCount" object:nil];
//     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backarrraycount:) name:@"backarrayCount" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eatArrayCount:) name:@"eatArrayCount" object:nil];
    
    if (self.isHaveOnBoarding) {
        [IMGAmplitudeUtil trackSignInWithName:@"ST - Change User Eating Preference Initiate" andType:nil andLocation:@"Onboarding" andReason:nil];
    }
}

- (void)dealloc{
 //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"arrayCount" object:nil];
 [[NSNotificationCenter defaultCenter] removeObserver:self name:@"eatArrayCount" object:nil];
    
}
- (void)eatArrayCount:(NSNotification *)noti{
    int count = 0;

    for (V2_EatModel *model in cuisineViewController.eatingArray) {
        if ([model.state isEqualToString:@"1"]) {
            count+=1;
        }
    }
    
    for (V2_LovePreferenceModel *model in cuisineViewController.cuisineArray) {
        for (V2_CuisineModel *cuisineModel in model.foodtypeList) {
            if ([cuisineModel.state isEqualToString:@"1"]) {
                count+=1;
            }
        }
    }
    lblnum.text = [NSString stringWithFormat:@"%d",count];
}

- (void)createNav{
    
    closeButton = [UIButton new];
    [closeButton setImage:[UIImage imageNamed:@"leftback@2x"] forState:UIControlStateNormal];
    
    [closeButton addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    
    lblnum = [UILabel new];
//    if (self.awaysSelecteArray.count > 0) {
    
    lblnum.text = [NSString stringWithFormat:@"%lu",(unsigned long)self.awaysSelecteArray.count +arrayCount.count];
    
    lblnum.textColor = [UIColor whiteColor];
    lblnum.font = [UIFont systemFontOfSize:14];
    lblnum.backgroundColor = [UIColor colorWithRed:211/255.0f green:31/255.0f blue:68/255.0f alpha:1.0f];
    lblnum.textAlignment = NSTextAlignmentCenter;
    
    lbltitle = [UILabel new];
    lbltitle.text = @"Food you love much...";
    lbltitle.textColor = [UIColor colorWithHexString:@"#333333"];
    lbltitle.font = [UIFont systemFontOfSize:14];
    
    btnNext = [UIButton new];
    [btnNext setTitle:@"Next" forState:UIControlStateNormal];
    [btnNext setTitleColor:[UIColor colorWithHexString:@"#09BFD3"] forState:UIControlStateNormal];
    btnNext.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    btnNext.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [btnNext addTarget:self action:@selector(nextButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view sd_addSubviews:@[closeButton,lblnum,lbltitle,btnNext]];
    
    closeButton.sd_layout
    .leftSpaceToView(self.view,0)
    .topSpaceToView(self.view,IMG_StatusBarHeight)
    .widthIs(45)
    .heightIs(45);
    
    if (self.isFromProfile) {
        if (self.isHaveOnBoarding) {
            closeButton.hidden = YES;
            closeButton.sd_layout
            .widthIs(15);
        }else{
            closeButton.hidden = NO;
            closeButton.sd_layout
            .widthIs(45);
        }
    }else{
        closeButton.hidden = YES;
        closeButton.sd_layout
        .widthIs(15);
    }
    
    lblnum.sd_layout
    .widthIs(24)
    .heightIs(24)
    .centerYEqualToView(closeButton)
    .leftSpaceToView(closeButton, 0);
    lblnum.layer.cornerRadius = 24/2;
    lblnum.layer.masksToBounds = YES;
    
    lbltitle.sd_layout
    .heightIs(22)
    .centerYEqualToView(closeButton)
    .leftSpaceToView(lblnum, 5);
    [lbltitle setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
    btnNext.sd_layout
    .rightSpaceToView(self.view,15)
    .heightIs(45)
    .centerYEqualToView(closeButton)
    .widthIs(80);
    
}

- (void)loadMainView{
    
    scrollPreference = [UIScrollView new];
    scrollPreference.pagingEnabled = YES;
    scrollPreference.showsHorizontalScrollIndicator = NO;
    scrollPreference.delegate = self;
    
    
    [self.view sd_addSubviews:@[scrollPreference]];//pageControl,

    
    scrollPreference.sd_layout
    .topSpaceToView(self.view,IMG_StatusBarAndNavigationBarHeight)
    .leftSpaceToView(self.view,0)
    .rightSpaceToView(self.view,0)
    .bottomSpaceToView(self.view,0);
    
    cuisineViewController = [[V2_LovePreferenceViewController alloc] init];
    [self addChildViewController:cuisineViewController];
    cuisineViewController.view.frame = CGRectMake(0, 0, DeviceWidth, scrollPreference.bounds.size.height);//CGRectMake(DeviceWidth, 0, DeviceWidth, scrollPreference.bounds.size.height);
    
    brandViewController = [[V2_BrandPreferenceViewController alloc] init];
    [self addChildViewController:brandViewController];
    brandViewController.view.frame = CGRectMake(DeviceWidth, 0, DeviceWidth, scrollPreference.bounds.size.height);//CGRectMake(DeviceWidth*2, 0, DeviceWidth, scrollPreference.bounds.size.height);

    [scrollPreference sd_addSubviews:@[cuisineViewController.view,brandViewController.view]];//eatViewController.view,
    scrollPreference.contentSize = CGSizeMake(DeviceWidth*2, 0);
    
}

- (void)requestData{
    
    
    //只传了个 userid
    if (self.isFromProfile) {
    
        NSMutableDictionary *param = [NSMutableDictionary dictionary];
        [param setValue:[IMGUser currentUser].userId forKey:@"userId"];
        [param setValue:[IMGUser currentUser].token forKey:@"t"];
        
        NSData *data = [NSJSONSerialization dataWithJSONObject:param options:NSJSONWritingPrettyPrinted error:nil];
        
        [[IMGNetWork sharedManager] requestWithPath:@"user/personalization/viewed/v2" withBody:data withSuccessBlock:^(id responseObject) {
            NSLog(@"%@",responseObject);
            
        } withFailurBlock:^(NSError *error) {
            
            
        }];
        
        [[LoadingView sharedLoadingView] startLoading];
        [PersonalizationHandler getUserPersonalizationWithPrama:param andSuccessBlock:^(id arayList, id brandList, id cuisineList, id eatingList) {
            
//            eatViewController.eatingArray = eatingList;
            
            
            [PersonalizationHandler getProfileWithPrama:param andSuccessBlock:^(id aarayList) {
                NSLog(@"%@",arayList);
                [self loadMainView];
                if ([aarayList isKindOfClass:[NSArray class]] && [aarayList count]>0) {
                    for (id dic in aarayList) {
                        for (V2_LovePreferenceModel *model in cuisineList) {
                            for (V2_CuisineModel *cuisineModel in model.foodtypeList) {
                                if ([dic isKindOfClass:[NSDictionary class]]) {
                                    if ([[dic objectForKey:@"objectType"] isEqual:cuisineModel.objectType] && [[dic objectForKey:@"objectId"] isEqual:cuisineModel.objectId]) {
                                        cuisineModel.state = @"1";
                                        selectCount+=1;
                                        //                                [self.awaysSelecteArray addObject:cuisineModel.state];
                                        
                                    }
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                    
                    for (id dic1 in aarayList) {
                        for (V2_EatModel *model in eatingList) {
                            if ([dic1 isKindOfClass:[NSDictionary class]]) {
                                if ([[dic1 objectForKey:@"objectType"] isEqual:model.objectType] && [[dic1 objectForKey:@"objectId"] isEqual:model.objectId]) {
                                    model.state = @"1";
                                    selectCount+=1;
                                }
                            }
                            
                        }
                    }
                    
                    for (id dic2 in aarayList) {
                        for (V2_BrandModelSuper *superModel in arayList) {
                            for (V2_BrandModel *model in superModel.brandList) {
                                if ([dic2 isKindOfClass:[NSDictionary class]]) {
                                    if ([[dic2 objectForKey:@"objectType"] isEqual:model.objectType] && [[dic2 objectForKey:@"objectId"] isEqual:model.objectId]) {
                                        model.state = @"1";
                                        
                                    }
                                }
                                
                            }
                        }
                    }
                }

                lblnum.text = [NSString stringWithFormat:@"%d",selectCount];
                NSLog(@"%@",cuisineList);
                NSLog(@"%@",eatingList);
                NSLog(@"%@",arayList);
                
                cuisineViewController.eatingArray = eatingList;
                cuisineViewController.cuisineArray = cuisineList;
                brandViewController.brandArray = brandList;
                brandViewController.brandTitleArray = arayList;
                if ([aarayList isKindOfClass:[NSArray class]] && [aarayList count]>0){
                    cuisineViewController.aarayArray = aarayList;
                }
                
                [[LoadingView sharedLoadingView] stopLoading];
            } anderrorBlock:^{
                
                [[LoadingView sharedLoadingView] stopLoading];
            }];
            
        } anderrorBlock:^{
            [[LoadingView sharedLoadingView] stopLoading];
        }];

    }else{
        NSDictionary *params = @{@"userId":[IMGUser currentUser].userId,@"t":[IMGUser currentUser].token};
        [[LoadingView sharedLoadingView] startLoading];
        [PersonalizationHandler getUserPersonalizationWithPrama:params andSuccessBlock:^(id arayList ,id brandList, id cuisineList, id eatingList) {
//            eatViewController.eatingArray = eatingList;
            cuisineViewController.eatingArray = eatingList;
            cuisineViewController.cuisineArray = cuisineList;
            brandViewController.brandArray = brandList;
            brandViewController.brandTitleArray = arayList;
            [[LoadingView sharedLoadingView] stopLoading];
            
        } anderrorBlock:^{
            [[LoadingView sharedLoadingView] stopLoading];
        }];
    }
}
//closeButton,lblnum,lbltitle,btnNext
- (void)nextButtonClick:(UIButton *)btn{
    if (currentIndex==0) {
        scrollPreference.contentOffset = CGPointMake(DeviceWidth, 0);
        currentIndex=1;
        lblnum.hidden = YES;
        lbltitle.hidden = YES;
    }
//    else if (currentIndex==1){
//        scrollPreference.contentOffset = CGPointMake(DeviceWidth*2, 0);
//        currentIndex=2;
//    }
    else{
//        if (self.isFromProfile) {
            [self chooseCuisineRequest];
//            [self.navigationController popViewControllerAnimated:YES];
//        }else{
//            [self chooseCuisineRequest];
//        }
        
                //[self dismissViewControllerAnimated:YES completion:nil];
    }
}
- (void)chooseCuisineRequest{
    
//    NSArray *eatArr = eatViewController.eatingArray;
    NSArray *eatArr = cuisineViewController.eatingArray;
    NSArray *cuisineArr = cuisineViewController.cuisineArray;
    NSArray *brandArr = brandViewController.brandTitleArray;

    NSMutableArray *chooseArr = [NSMutableArray array];
    NSMutableArray *eatTitleArr = [NSMutableArray array];
    NSMutableArray *preferenceArr = [NSMutableArray array];
    NSMutableArray *cuisineNameArr = [NSMutableArray array];
    NSMutableArray *brandNameArr = [NSMutableArray array];
    
    for (V2_EatModel *eatModel in eatArr) {
        if ([eatModel.state isEqualToString:@"1"]) {
            [eatTitleArr addObject:eatModel.name];
            [chooseArr addObject:@{@"objectId":eatModel.objectId,@"objectType":eatModel.objectType}];
        }
    }
    
    for (V2_LovePreferenceModel *model in cuisineArr) {
        for (V2_CuisineModel *cuisineModel in model.foodtypeList) {
            if ([cuisineModel.state isEqualToString:@"1"]) {
                [preferenceArr addObject:model.name];
                [chooseArr addObject:@{@"objectId":model.objectId,@"objectType":model.objectType}];
                break;
            }
        }
    }
    
    for (V2_LovePreferenceModel *model in cuisineArr) {
        for (V2_CuisineModel *cuisineModel in model.foodtypeList) {
            if ([cuisineModel.state isEqualToString:@"1"]) {
                [cuisineNameArr addObject:cuisineModel.name];
                [chooseArr addObject:@{@"objectId":cuisineModel.objectId,@"objectType":cuisineModel.objectType}];
            }
        }
    }
    
    for (V2_BrandModelSuper *model in brandArr) {
        for (V2_BrandModel *brandModel in model.brandList) {
            if ([brandModel.state isEqualToString:@"1"]) {
                [brandNameArr addObject:brandModel.name];
                [chooseArr addObject:@{@"objectId":brandModel.objectId,@"objectType":brandModel.objectType}];
            }
        }
    }
//    for (V2_BrandModel *brandModel in brandArr) {
//        if ([brandModel.state isEqualToString:@"1"]) {
//            [chooseArr addObject:@{@"objectId":brandModel.objectId,@"objectType":brandModel.objectType}];
//        }
//    }
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:[IMGUser currentUser].userId forKey:@"userId"];
    [param setValue:[IMGUser currentUser].token forKey:@"t"];
    [param setValue:chooseArr forKey:@"personalization"];
    
   [[LoadingView sharedLoadingView] startLoading];
    [PersonalizationHandler chooseCuisineWithPrama:param andSuccessBlock:^{
        [[LoadingView sharedLoadingView] stopLoading];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"onboarding" forKey:ONBOARDING];
        
        if (self.isHaveOnBoarding) {
            [IMGAmplitudeUtil trackPreferenceWithName:@"ST - Change User Eating Preference Succeed" andEatPreference:[eatTitleArr componentsJoinedByString:@","] andCuisinePreference:[preferenceArr componentsJoinedByString:@","] andCuisineName:[cuisineNameArr componentsJoinedByString:@","] andBrandName:nil andOrigin:@"Onboarding"];
            
            [IMGAmplitudeUtil trackPreferenceWithName:@"ST - Change Brand Preference Succeed" andEatPreference:nil andCuisinePreference:nil andCuisineName:nil andBrandName:[brandNameArr componentsJoinedByString:@","] andOrigin:@"Onboarding"];
        }
        
        
        if (self.isFromHome) {
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_HOME object:nil];
        }
        
        if (self.isFromProfile) {
            if (self.isHaveOnBoarding) {
                [[AppDelegate ShareApp] gotoHomePage];
                 [AppDelegate ShareApp].userLogined = YES;
            }
        [self.navigationController popToRootViewControllerAnimated:YES];
        }else{
            
            [[AppDelegate ShareApp] gotoHomePage];
        }
        
        
    } anderrorBlock:^{
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

- (void)close{
//    if (currentIndex==2) {
//        scrollPreference.contentOffset = CGPointMake(DeviceWidth, 0);
//        currentIndex=1;
//    }else
        if (currentIndex==1){
        scrollPreference.contentOffset = CGPointMake(0, 0);
        currentIndex=0;
            lblnum.hidden = NO;
            lbltitle.hidden = NO;
    }else{
        if (self.isFromProfile) {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    int index = scrollPreference.contentOffset.x/DeviceWidth;
    if (index==1) {
        if (self.isHaveOnBoarding) {
            [IMGAmplitudeUtil trackSignInWithName:@"ST - Change Brand Preference Initiate" andType:nil andLocation:@"Onboarding" andReason:nil];
        }
        
        lblnum.hidden = YES;
        lbltitle.hidden = YES;
        
        [btnNext setTitle:@"Finish" forState:UIControlStateNormal];
    }else{
        lblnum.hidden = NO;
        lbltitle.hidden = NO;
        [btnNext setTitle:@"Next" forState:UIControlStateNormal];
    }
    
    if (index==0) {
        if (self.isFromProfile) {
            if (self.isHaveOnBoarding) {
                closeButton.hidden = YES;
                closeButton.sd_layout
                .widthIs(15);
            }else{
                closeButton.hidden = NO;
                closeButton.sd_layout
                .widthIs(45);
            }
        }else{
            closeButton.hidden = YES;
            closeButton.sd_layout
            .widthIs(15);
        }
    }else{
        closeButton.hidden = NO;
        closeButton.sd_layout
        .widthIs(45);
    }
    currentIndex = index;
//    pageControl.currentPage = index;
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
    }
}

@end
