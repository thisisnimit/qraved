//
//  LoginVC.m
//  quaved_ios
//
//  Created by Shine Wang on 5/28/13.
//  Copyright (c) 2013 Shine Wang. All rights reserved.
//

#import "LoginViewController.h"
#import "AppDelegate.h"
#import "PushHelper.h"
//#import "GAIFields.h"
//#import "GAIDictionaryBuilder.h"
#import <GoogleAnalytics/GAIFields.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import <CoreText/CoreText.h>
#import "IMGExceptionAlertView.h"
#import "UIConstants.h"
#import "SelectCityController.h"
#import "EmailLoginViewController.h"
#import "ResetPasswordViewController.h"
#import "EmailRegisterViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "UIViewController+Helper.h"
#import "UIDevice+Util.h"
#import "IMGUser.h"
#import "LoadingView.h"
#import "IMGCity.h"
#import "CityUtils.h"
#import "CityHandler.h"

#import "UIAlertView+BlocksKit.h"
#import "LoginParamter.h"
#import "DBManager.h"
#import "FBSignUpViewController.h"
#import "UpdateUserInfoViewController.h"
#import "RegisterViewController.h"

#import "Amplitude.h"
#import "TYAttributedLabel.h"

#import <Google/SignIn.h>
#import "MoreOptionsViewController.h"
#import "V2_PreferenceViewController.h"
@interface LoginViewController()<SighUpDelegate,SHKOAuthViewDelegate,UITextFieldDelegate,TYAttributedLabelDelegate,CLLocationManagerDelegate,SelectCityDelegate>
{
    //UIScrollView *bgImageView;
    NSNumber *fromWhere;
    UIControl *control;
    CGRect oriFrame;
    UIImageView *emailImageView ;
    UIImageView *passwordImageView;
    BOOL isViewUp;
    NSDictionary* cityCoornitionDic;
    NSMutableArray* _cities;
    NSString *ampitudeType;
    UIImageView *twitterImageView;
    UIImageView *googleImageView;
    CLLocationManager *clmanager;
    FunctionButton *btnCreate;
    FunctionButton *btnOptions;
    UIButton *btnSignIn;
}

@end

@implementation LoginViewController
//@synthesize emailTextField,passwordTextField;

-(id)initWithFromWhere:(NSNumber *)_fromWhere{
    self=[super init];
    if(self!=nil){
        fromWhere=_fromWhere;
    }
    return self;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    self.screenName=@"Login";
    self.navigationController.navigationBarHidden = YES;
}



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
  
    btnSignIn.enabled = YES;
    btnOptions.enabled = YES;
    btnCreate.enabled = YES;
    self.navigationController.navigationBarHidden = YES;
    [AppDelegate ShareApp].isAlreadyPopLogin = YES;
    [[LoadingView sharedLoadingView] stopLoading];
    [AppDelegate ShareApp].isShowPersonalization = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
    //    [self.emailTextField resignFirstResponder];
    //    [self.passwordTextField resignFirstResponder];
    [[LoadingView sharedLoadingView] stopLoading];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TwitterLoginSelfPartDone object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"loginSuccessful" object:nil];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.screenName = @"Sign In form page";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginWithTwitterInQraved:) name:TwitterLoginSelfPartDone object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccessful) name:@"loginSuccessful" object:nil];
    [self.navigationController.navigationBar setHidden:YES];
    
   
    CLLocation* BaliCoornition=[[CLLocation alloc]initWithLatitude:-8.3514642891 longitude:115.0886535645];
    CLLocation* JakartaCoornition=[[CLLocation alloc]initWithLatitude:-6.2088209771 longitude:106.8456459045];
    CLLocation* BandungCoornition=[[CLLocation alloc]initWithLatitude:-6.9206329204 longitude:107.6053333282];
    cityCoornitionDic=@{@"BaliCoornition":BaliCoornition,@"JakartaCoornition":JakartaCoornition,@"BandungCoornition":BandungCoornition};
    [CityUtils getCitiesWithCountryId:0 andBlock:^(NSArray *cityArray){
        _cities=[cityArray mutableCopy];
        
    }];

    
    [self addLoginButton];
    [[Amplitude instance] logEvent:@"RC - View Sign Up Page"];
}


-(void)addLoginButton{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    btnSignIn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnSignIn setTitle:@"Log In" forState:UIControlStateNormal];
    btnSignIn.titleLabel.font = [UIFont systemFontOfSize:17];
    [btnSignIn setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    [btnSignIn addTarget:self action:@selector(emailLogin) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *logoImg = [UIImageView new];
    logoImg.image = [UIImage imageNamed:@"Qraved Logo Red"];
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.text = @"Discover Great Food";
    lblTitle.textColor = [UIColor color333333];
    lblTitle.font = [UIFont systemFontOfSize:18];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    
    UILabel *lblSubTitle = [UILabel new];
    lblSubTitle.text = @"To get personalized recommendations,\nplease connect your social account.";
    lblSubTitle.textColor = [UIColor color999999];
    lblSubTitle.font = [UIFont systemFontOfSize:14];
    lblSubTitle.textAlignment = NSTextAlignmentCenter;
    
    FunctionButton *btnFaceBook = [FunctionButton signButtonWithType:UIButtonTypeCustom andTitle:@"CONTINUE WITH FACEBOOK" andBGColor:[UIColor colorWithHexString:@"#39579B"] andTitleColor:[UIColor whiteColor]];
    [btnFaceBook addTarget:self action:@selector(facebookLogin:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imgFaceBook = [UIImageView new];
    imgFaceBook.image = [UIImage imageNamed:@"ic_fb_login"];
    [btnFaceBook addSubview:imgFaceBook];
    
    btnCreate = [FunctionButton signButtonWithType:UIButtonTypeCustom andTitle:@"CREATE ACCOUNT" andBGColor:[UIColor whiteColor] andTitleColor:[UIColor color333333]];
    btnCreate.layer.borderWidth = 1;
    btnCreate.layer.borderColor = [UIColor colorWithHexString:@"#EAEAEA"].CGColor;
    [btnCreate addTarget:self action:@selector(signUp) forControlEvents:UIControlEventTouchUpInside];
    
    btnOptions = [FunctionButton signButtonWithType:UIButtonTypeCustom andTitle:@"MORE OPTIONS" andBGColor:[UIColor whiteColor] andTitleColor:[UIColor color333333]];
    btnOptions.layer.borderWidth = 1;
    btnOptions.layer.borderColor = [UIColor colorWithHexString:@"#EAEAEA"].CGColor;
    [btnOptions addTarget:self action:@selector(moreOptionTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view sd_addSubviews:@[btnSignIn, logoImg, lblTitle, lblSubTitle, btnFaceBook, btnCreate, btnOptions]];
    
    btnSignIn.sd_layout
    .rightSpaceToView(self.view, 15)
    .topSpaceToView(self.view, IMG_StatusBarHeight + 10)
    .widthIs(50)
    .heightIs(22);
    
    logoImg.sd_layout
    .topSpaceToView(self.view, 84)
    .centerXEqualToView(self.view)
    .heightIs(61)
    .widthIs(163);
    
    lblTitle.sd_layout
    .topSpaceToView(logoImg, 30)
    .leftSpaceToView(self.view, 42)
    .rightSpaceToView(self.view, 42)
    .heightIs(18);
    
    lblSubTitle.sd_layout
    .topSpaceToView(lblTitle, 8)
    .leftSpaceToView(self.view, 20)
    .rightSpaceToView(self.view, 20)
    .heightIs(36);
    lblSubTitle.numberOfLines = 0;
    
    btnFaceBook.sd_layout
    .topSpaceToView(lblSubTitle, 40)
    .leftSpaceToView(self.view, 42)
    .rightSpaceToView(self.view, 42)
    .heightIs(50);
    
    imgFaceBook.sd_layout
    .leftSpaceToView(btnFaceBook, 13)
    .widthIs(16)
    .heightIs(16)
    .centerYEqualToView(btnFaceBook);
    
    btnCreate.sd_layout
    .topSpaceToView(btnFaceBook, 15)
    .leftEqualToView(btnFaceBook)
    .rightEqualToView(btnFaceBook)
    .heightIs(50);
    
    btnOptions.sd_layout
    .topSpaceToView(btnCreate, 15)
    .leftEqualToView(btnCreate)
    .rightEqualToView(btnCreate)
    .heightIs(50);
}

- (void)signUp{
    [self emailRegister];
}

- (void)moreOptionTapped{
    btnOptions.enabled = NO;
    MoreOptionsViewController *moreOptionsVC = [[MoreOptionsViewController alloc] init];
    [self.navigationController pushViewController:moreOptionsVC animated:YES];
}

- (void)attributedLabel:(TYAttributedLabel *)attributedLabel textStorageClicked:(id<TYTextStorageProtocol>)TextRun atPoint:(CGPoint)point{
    
    
    if ([TextRun isKindOfClass:[TYLinkTextStorage class]]) {
        
        //        [self emailLogin:nil];
        [self emailRegister];
    }
    
}
-(void)googleLogin:(id)sender{
    [[LoadingView sharedLoadingView] startLoading];
    googleImageView.userInteractionEnabled = NO;
    [IMGAmplitudeUtil trackSignInWithName:@"SI - Sign In Initiate" andType:@"Google" andLocation:@"Onboarding Page" andReason:nil];
    ampitudeType = @"Google";
    [[GIDSignIn sharedInstance]signIn];
    [self performSelector:@selector(senderEnabel) withObject:nil afterDelay:5.0f];
    
}

#pragma mark Facebook登录
- (void)facebookLogin:(id)sender{
    
    ampitudeType = @"Facebook";
    [IMGAmplitudeUtil trackSignInWithName:@"SI - Sign In Initiate" andType:@"Facebook" andLocation:@"Onboarding Page" andReason:nil];
    
    NSArray *permissions = @[@"public_profile", @"email"];
    UserDataHandler *userDataHandler = [[UserDataHandler alloc] init];
    [userDataHandler faceBookLogin:permissions delegateWithBlock:^{
        [[LoadingView sharedLoadingView] stopLoading];
        NSDictionary *dataDic = [[NSDictionary alloc] initWithDictionary:[userDataHandler getParameters]];
        if ([userDataHandler isSignUp])
        {
            [self loginSuccessful];
        }
        else
        {
            [self loginWithFacebook:dataDic];
        }
    } failedBlock:^{

         [[LoadingView sharedLoadingView] stopLoading];
    }];
}

-(void)loginWithFacebook:(NSDictionary*)dataDict{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:[dataDict objectForKey:@"first_name"] forKey:@"firstName"];
    [parameters setObject:[dataDict objectForKey:@"last_name"] forKey:@"lastName"];
    
    if ([[dataDict allKeys] containsObject:@"email"]) {
        [parameters setObject:[dataDict objectForKey:@"email"] forKey:@"email"];
        
    }
    else{
        FBSignUpViewController *fbsuvc = [[FBSignUpViewController alloc] init];
        fbsuvc.dataDic = dataDict;
        fbsuvc.amplitudeType = @"Facebook";
        [self.navigationController pushViewController:fbsuvc animated:YES];
        return;
        
    }
    [parameters setObject:@"signupwithself" forKey:@"loginwith"];
    [parameters setObject:@"1" forKey:@"sourceType"];
    [parameters setObject:@"4" forKey:@"productSource"];
    [parameters setObject:[dataDict objectForKey:@"thirdPartyType"] forKey:@"thirdPartyType"];
    [parameters setObject:[dataDict objectForKey:@"thirdPartyId"] forKey:@"thirdPartyId"];
    [parameters setObject:[dataDict objectForKey:@"picture"] forKey:@"picture"];
    
    [LoginParamter sharedParameter].loginwith=USER_LOGINTYPE_WITHSELF;
    
    UserDataHandler *userDataHandler=[[UserDataHandler alloc]init];
    [userDataHandler faceBookSignUp:parameters withBlock:^() {
        [[LoadingView sharedLoadingView] stopLoading];
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"loginSuccessful" object:nil];
        [AppDelegate ShareApp].userLogined=YES;
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
         [self loginSuccessful];
    } failedBlock:^(NSString *errorMsg) {
        [[Amplitude instance]
         logEvent:@"SU - Sign Up Failed" withEventProperties:@{@"Reason":errorMsg,@"Sign in type":@"Facebook"}];
    }];
    
}

-(void)tokenAuthorizeCancelledView:(SHKOAuthView *)authView{
    
}
-(void)tokenAuthorizeView:(SHKOAuthView *)authView didFinishWithSuccess:(BOOL)success queryParams:(NSMutableDictionary *)queryParams error:(NSError *)error{
    
}

#pragma mark Email登录
- (void)emailLogin {
    btnSignIn.enabled = NO;
    ampitudeType = @"Email";
    [IMGAmplitudeUtil trackSignInWithName:@"SI - Sign In Initiate" andType:@"Email" andLocation:@"Onboarding Page" andReason:nil];
    EmailLoginViewController *emailLoginViewController = [[EmailLoginViewController alloc]init];
    emailLoginViewController.loginViewController=self;
    [self.navigationController pushViewController:emailLoginViewController animated:YES];
}
#pragma mark Email注册
-(void)emailRegister
{
    btnCreate.enabled = NO;
    [IMGAmplitudeUtil trackSignInWithName:@"SU - Sign Up Initiate" andType:@"Email" andLocation:@"Onboarding Page" andReason:nil];
    EmailRegisterViewController *emailRegisterViewController=[[EmailRegisterViewController alloc]init];
    [self.navigationController pushViewController:emailRegisterViewController animated:YES];
}
//-(void)enterEmail
//{
//    EnterEmailViewController *enterEmailViewController=[[EnterEmailViewController alloc]init];
//    enterEmailViewController.delegate=self;
//    [self.navigationController pushViewController:enterEmailViewController animated:YES];
//}

- (void)loginSuc{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"isOpenInApp"] != nil){
        [AppDelegate ShareApp].userLogined = YES;
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[AppDelegate ShareApp] gotoHomePage];
    }else{
        NSString *onboardingString = [[NSUserDefaults standardUserDefaults] objectForKey:ONBOARDING];
        if (onboardingString) {
            [AppDelegate ShareApp].userLogined = YES;
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[AppDelegate ShareApp] gotoHomePage];

        }else{
            
            V2_PreferenceViewController *V2_PreferenceVC = [[V2_PreferenceViewController alloc] init];
            V2_PreferenceVC.isFromProfile = YES;
            V2_PreferenceVC.isHaveOnBoarding = YES;
            [self.navigationController pushViewController:V2_PreferenceVC animated:YES];
        }
        
    }
}

- (void)requestDeeplink{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"installDeeplink"] == nil) {
        UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectZero];
        NSString *userAgent = [webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
        NSDictionary *dic = @{@"userAgent":userAgent};
        
        [UserDataHandler deeplink:dic andSuccessBlock:^(id responseObject) {
            if (responseObject == nil) {
                [self loginSuc];
            }else{
                [AppDelegate ShareApp].userLogined = YES;
                [[AppDelegate ShareApp] gotoHomePage];
            }
            
        } andFailedBlock:^{
            [self loginSuc];
        }];
    }
}

#pragma mark - 登录成功之后 需要处理的事情
- (void)loginSuccessful {
    if (ampitudeType.length>0) {
        [IMGAmplitudeUtil trackSignInWithName:@"SI - Sign In Succeed" andType:ampitudeType andLocation:nil andReason:nil];
        [[NSUserDefaults standardUserDefaults] setValue:ampitudeType forKey:@"loginType"];
    }
    [PushHelper postUserDeviceToken];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"isFromWebLink"] == nil) {
        [self requestDeeplink];
        [[NSUserDefaults standardUserDefaults] setObject:@"webLink" forKey:@"isFromWebLink"];
        return ;
    }
    
    [self loginSuc];
}
@end
