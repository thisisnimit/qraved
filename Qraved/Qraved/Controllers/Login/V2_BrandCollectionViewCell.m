//
//  V2_BrandCollectionViewCell.m
//  Qraved
//
//  Created by harry on 2017/6/9.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_BrandCollectionViewCell.h"

@interface V2_BrandCollectionViewCell ()
{
    UIImageView *imageView;
}

@end

@implementation V2_BrandCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    for (UIView *view in self.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    imageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
    imageView.layer.cornerRadius = 5;
    imageView.layer.masksToBounds = YES;
    [self.contentView addSubview:imageView];
    
    self.selectView = [[UIView alloc] initWithFrame:self.contentView.bounds];
    self.selectView.backgroundColor = [UIColor blackColor];
    self.selectView.layer.cornerRadius = 5;
    self.selectView.layer.masksToBounds = YES;
    self.selectView.alpha = 0.5;
    self.selectView.hidden = YES;
    [self.contentView addSubview:self.selectView];
    
    self.imvCheckmark = [[UIImageView alloc] init];
    self.imvCheckmark.bounds = CGRectMake(0, 0, 28, 23);
    self.imvCheckmark.center = self.selectView.center;
    self.imvCheckmark.image = [UIImage imageNamed:@"ic_checkmark"];
    self.imvCheckmark.hidden = YES;
    [self.contentView addSubview:self.imvCheckmark];
}

- (void)setBrandModel:(V2_BrandModel *)brandModel{
    _brandModel = brandModel;
     UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(self.contentView.bounds.size.width,self.contentView.bounds.size.height)];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[brandModel.imageUrl returnFullImageUrlWithWidth:self.contentView.bounds.size.width andHeight:self.contentView.bounds.size.height]] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
    
}
//- (void)setImageStr:(NSString *)imageStr{
//    _imageStr = imageStr;
//    imageView.image = [UIImage imageNamed:imageStr];
//}

@end
