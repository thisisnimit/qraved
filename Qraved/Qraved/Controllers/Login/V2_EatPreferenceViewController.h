//
//  V2_EatPreferenceViewController.h
//  Qraved
//
//  Created by harry on 2017/6/8.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "GAITrackedViewController.h"

@interface V2_EatPreferenceViewController : GAITrackedViewController

@property (nonatomic, strong) NSArray *eatingArray;
@property (nonatomic, strong) UIButton *btn;
@property (nonatomic, strong) NSMutableArray *chooseEatArr;

@end
