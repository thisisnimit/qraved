//
//  V2_LovePreferenceCell.m
//  Qraved
//
//  Created by harry on 2017/6/9.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_LovePreferenceCell.h"

@implementation V2_LovePreferenceCell

- (instancetype)initWithFrame:(CGRect)frame{
   self =  [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    for (UIView *view in self.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    self.lblTitle = [[UILabel alloc] initWithFrame:self.contentView.frame];

     self.lblTitle.font = [UIFont systemFontOfSize:12];
    self.lblTitle.textAlignment = NSTextAlignmentCenter;
     self.lblTitle.layer.cornerRadius =  self.lblTitle.bounds.size.height/2;
     self.lblTitle.layer.masksToBounds = YES;
     self.lblTitle.layer.borderWidth  = 1;
    self.lblTitle.textColor = [UIColor colorWithHexString:@"#999999"];
    self.lblTitle.layer.borderColor = [UIColor colorWithHexString:@"#999999"].CGColor;
    self.lblTitle.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.lblTitle];
}
- (void)setModel:(V2_CuisineModel *)model{
    _model = model;
    self.lblTitle.text = model.name;

}


@end
