//
//  EmailLoginViewController.m
//  PocketPlan
//
//  Created by Libo Liu on 13-11-12.
//  Copyright (c) 2013年 com.imaginato. All rights reserved.
//

#import "EmailLoginViewController.h"
#import "AppDelegate.h"
#import "PushHelper.h"
#import <CoreText/CoreText.h>
#import "UIConstants.h"

#import "ResetPasswordViewController.h"
#import "EmailRegisterViewController.h"

#import "UILabel+Helper.h"
#import "UIView+Helper.h"
#import "UIAlertView+BlocksKit.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "UIViewController+Helper.h"
#import "LoginParamter.h"
#import "NavigationBarButtonItem.h"
#import "SendCall.h"

#import "PostDataHandler.h"
#import "TYAttributedLabel.h"

#import "NSString+Helper.h"
#import "V2_PreferenceViewController.h"

@interface EmailLoginViewController ()<TYAttributedLabelDelegate>
{
    //UIImageView *bgImageView;
    
    UIImageView *errorImageView;
    UIImageView *emailImageView ;
    UIImageView *passwordImageView;
    UIView *tfView;
    UITextField *emailTextField;
    UITextField *passwordTextField;
    UIButton *loginBtn;
    UIButton *registerBtn;
    FunctionButton *btnSignIn;
    
}
@end

@implementation EmailLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    self.screenName=@"Login";
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden:YES];

    [self loadMainView];
    [self createNavButton];
    
}

-(void)close{
    [IMGAmplitudeUtil trackSignInWithName:@"SI - Sign In Cancel" andType:@"Email" andLocation:nil andReason:nil];

    [self.navigationController popViewControllerAnimated:YES];
}
- (void)createNavButton{
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(0, IMG_StatusBarHeight, 45, 45)];
    [closeButton setImage:[UIImage imageNamed:@"leftback@2x"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeButton];
    [self.view bringSubviewToFront:closeButton];
}
-(void)loadMainView
{
    
    self.view.backgroundColor = [UIColor whiteColor];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(touchBg:)];
    [self.view addGestureRecognizer:tapGesture];
    
    UIButton *btnForgot = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnForgot setTitle:@"Forgot Password" forState:UIControlStateNormal];
    btnForgot.titleLabel.font = [UIFont systemFontOfSize:17];
    [btnForgot setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    [btnForgot addTarget:self action:@selector(forgotPasswordButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *logoImage = [[UIImageView alloc]init];
    logoImage.image = [UIImage imageNamed:@"Qraved Logo Red"];
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.textColor = [UIColor color333333];
    lblTitle.text = @"Welcome Back";
    lblTitle.font = [UIFont systemFontOfSize:18];
    lblTitle.textAlignment = NSTextAlignmentCenter;

    tfView = [UIView new];
    tfView.backgroundColor = [UIColor whiteColor];
    tfView.layer.borderWidth = 1;
    tfView.layer.borderColor = [UIColor colorWithHexString:@"#EAEAEA"].CGColor;
    tfView.layer.cornerRadius = 3;
    tfView.layer.masksToBounds = YES;
    
    
    emailTextField = [[UITextField alloc]init];
    emailTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    emailTextField.font = [UIFont systemFontOfSize:14];
    emailTextField. attributedPlaceholder= [[NSAttributedString alloc] initWithString:L(@"Email address") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#999999"]}];
    emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    emailTextField.returnKeyType = UIReturnKeyDone;
    emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    passwordTextField = [[UITextField alloc]init];
    passwordTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    passwordTextField.font = [UIFont systemFontOfSize:14];
    passwordTextField. attributedPlaceholder= [[NSAttributedString alloc] initWithString:L(@"Password") attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHexString:@"#999999"]}];
    passwordTextField.returnKeyType = UIReturnKeyDone;
    
    emailTextField.delegate=self;
    passwordTextField.delegate=self;
    passwordTextField.secureTextEntry = YES;
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    
    [tfView sd_addSubviews:@[emailTextField, lineView, passwordTextField]];

    btnSignIn = [FunctionButton signButtonWithType:UIButtonTypeCustom andTitle:@"LOG IN" andBGColor:[UIColor colorWithHexString:@"#EAEAEA"] andTitleColor:[UIColor whiteColor]];
    [btnSignIn addTarget:self action:@selector(loginButtonClick) forControlEvents:UIControlEventTouchUpInside];
    btnSignIn.enabled = NO;
    [self.view sd_addSubviews:@[btnForgot, logoImage, lblTitle,tfView, btnSignIn]];
    
    btnForgot.sd_layout
    .topSpaceToView(self.view, IMG_StatusBarHeight + 10)
    .rightSpaceToView(self.view, 15)
    .widthIs(132)
    .heightIs(22);
    
    logoImage.sd_layout
    .topSpaceToView(self.view,84)
    .centerXEqualToView(self.view)
    .heightIs(61)
    .widthIs(163);
    
    lblTitle.sd_layout
    .centerXEqualToView(self.view)
    .heightIs(18)
    .widthIs(DeviceWidth-84)
    .topSpaceToView(logoImage,30);
    
    tfView.sd_layout
    .topSpaceToView(lblTitle,21)
    .leftSpaceToView(self.view,42)
    .rightSpaceToView(self.view,42)
    .heightIs(103);
    
    emailTextField.sd_layout
    .topSpaceToView(tfView,0)
    .leftSpaceToView(tfView,15)
    .widthIs(DeviceWidth - 114)
    .heightIs(51);
    
    lineView.sd_layout
    .topSpaceToView(emailTextField,0)
    .leftSpaceToView(tfView,0)
    .rightSpaceToView(tfView,0)
    .heightIs(1);
    
    passwordTextField.sd_layout
    .topSpaceToView(emailTextField,1)
    .leftSpaceToView(tfView,15)
    .widthIs(DeviceWidth - 114)
    .heightIs(51);
    
    btnSignIn.sd_layout
    .topSpaceToView(tfView,21)
    .leftSpaceToView(self.view, 42)
    .rightSpaceToView(self.view, 42)
    .heightIs(50);
    
    [emailTextField becomeFirstResponder];
    [self addObserverWithTextField];
}

- (void)addObserverWithTextField{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidChange:)
                                                 name:UITextFieldTextDidChangeNotification object:emailTextField];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textFieldTextDidChange:)
                                                 name:UITextFieldTextDidChangeNotification object:passwordTextField];
}

- (void)textFieldTextDidChange:(NSNotification *)notification
{
    [self validateUseInformation];
}

- (void)validateUseInformation{
    if (emailTextField.text.length > 0 && passwordTextField.text.length > 0 ) {
        btnSignIn.backgroundColor = [UIColor colorWithHexString:@"#D20000"];
        btnSignIn.enabled = YES;
    }else{
        btnSignIn.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
        btnSignIn.enabled = NO;
    }
}

- (void)forgotButtonTapped{
    EmailRegisterViewController * emaiRegisterVC = [[EmailRegisterViewController alloc] init];
    [self.navigationController pushViewController:emaiRegisterVC animated:YES];
}

- (void)attributedLabel:(TYAttributedLabel *)attributedLabel textStorageClicked:(id<TYTextStorageProtocol>)TextRun atPoint:(CGPoint)point{
    
    
    if ([TextRun isKindOfClass:[TYLinkTextStorage class]]) {
        EmailRegisterViewController * emaiRegisterVC = [[EmailRegisterViewController alloc] init];
        [self.navigationController pushViewController:emaiRegisterVC animated:YES];
    }
    
}

#pragma mark textField Delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (errorImageView != nil) {
        [textField becomeFirstResponder];
        [errorImageView removeFromSuperview];
    }
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}



- (void)touchBg:(id)sender {
    
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
}

#pragma mark 忘记密码
-(void)forgotPasswordButtonClick{
    [self.navigationController pushViewController:[[ResetPasswordViewController alloc]init] animated:YES];
}
#pragma mark 登录
-(void)loginButtonClick{

    [self.view endEditing:YES];
    if (emailTextField.text.length == 0) {
        [self errorViewShowWithTextField:emailTextField];
        return;
    }

    if (![emailTextField.text isValidateEmail]) {
        [self createErrorAlert:@""];
        return;
    }

    if(passwordTextField.text.length<6)
    {
        [self errorViewShowWithTextField:passwordTextField];
        return;
    }

    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]initWithCapacity:0];
    [parameters setObject:emailTextField.text forKey:@"email"];
    [parameters setObject:[passwordTextField.text MD5String] forKey:@"password"];
    [parameters setObject:@"self" forKey:@"loginwith"];
    [LoginParamter sharedParameter].loginwith=USER_LOGINTYPE_WITHSELF;
    UserDataHandler *userDataHandler = [[UserDataHandler alloc] init];
    
    
    [userDataHandler emailLogin:parameters withError:nil withDelegateBlock:^{
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"isFromWebLink"] == nil) {
            [self requestDeeplink];
            [[NSUserDefaults standardUserDefaults] setObject:@"webLink" forKey:@"isFromWebLink"];
            return ;
        }
        
        
        [self loginSuc];
        

        
    } failBlock:^(NSString *errorMsg) {
        [self createErrorAlert:errorMsg];
    }];
}

- (void)createErrorAlert:(NSString *)errorMsg{
    if ([errorMsg isEqualToString:@"Invalid email"]) {
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Email not registered" message:@"This email address is not registered. Please create an account first." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionCreate = [UIAlertAction actionWithTitle:@"Create Account" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [IMGAmplitudeUtil trackSignInWithName:@"SU - Sign Up Initiate" andType:@"Email" andLocation:@"Onboarding Page" andReason:nil];
            EmailRegisterViewController *emailRegisterViewController=[[EmailRegisterViewController alloc]init];
            [self.navigationController pushViewController:emailRegisterViewController animated:YES];
        }];
        
        UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];
        [alertController addAction:actionCreate];
        [alertController addAction:actionCancel];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }else if ([errorMsg isEqualToString:@"Invalid password"]){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Incorrect passsword" message:@"The password you entered is incorrect. Please try again." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *tryCreate = [UIAlertAction actionWithTitle:@"Try Again" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];
        
        UIAlertAction *forgotCancel = [UIAlertAction actionWithTitle:@"Forgot Password?" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController pushViewController:[[ResetPasswordViewController alloc]init] animated:YES];
        }];
        [alertController addAction:tryCreate];
        [alertController addAction:forgotCancel];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }else{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Invalid Email" message:@"Please check if your email has been entered correctly." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *tryCreate = [UIAlertAction actionWithTitle:@"Try Again" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];
        
        UIAlertAction *forgotCancel = [UIAlertAction actionWithTitle:@"Create Account" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [IMGAmplitudeUtil trackSignInWithName:@"SU - Sign Up Initiate" andType:@"Email" andLocation:@"Onboarding Page" andReason:nil];
            EmailRegisterViewController *emailRegisterViewController=[[EmailRegisterViewController alloc]init];
            [self.navigationController pushViewController:emailRegisterViewController animated:YES];
        }];
        [alertController addAction:tryCreate];
        [alertController addAction:forgotCancel];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)loginSuccessful {
//    if (ampitudeType.length>0) {
//        [IMGAmplitudeUtil trackSignInWithName:@"SI - Sign In Succeed" andType:ampitudeType andLocation:nil andReason:nil];
//        [[NSUserDefaults standardUserDefaults] setValue:ampitudeType forKey:@"loginType"];
//    }
    [PushHelper postUserDeviceToken];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"isFromWebLink"] == nil) {
        [self requestDeeplink];
        [[NSUserDefaults standardUserDefaults] setObject:@"webLink" forKey:@"isFromWebLink"];
        return ;
    }
    
    [self loginSuc];
}

- (void)loginSuc{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"isOpenInApp"] != nil){
        [AppDelegate ShareApp].userLogined = YES;
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[AppDelegate ShareApp] gotoHomePage];
    }else{
        NSString *onboardingString = [[NSUserDefaults standardUserDefaults] objectForKey:ONBOARDING];
        if (onboardingString) {
            [AppDelegate ShareApp].userLogined = YES;
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[AppDelegate ShareApp] gotoHomePage];
            
        }else{
            
            V2_PreferenceViewController *V2_PreferenceVC = [[V2_PreferenceViewController alloc] init];
            V2_PreferenceVC.isFromProfile = YES;
            V2_PreferenceVC.isHaveOnBoarding = YES;
            [self.navigationController pushViewController:V2_PreferenceVC animated:YES];
        }
        
    }
}

- (void)requestDeeplink{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"installDeeplink"] == nil) {
        UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectZero];
        NSString *userAgent = [webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
        NSDictionary *dic = @{@"userAgent":userAgent};
        
        [UserDataHandler deeplink:dic andSuccessBlock:^(id responseObject) {
            if (responseObject == nil) {
                [self loginSuc];
            }else{
                [AppDelegate ShareApp].userLogined = YES;
                [[AppDelegate ShareApp] gotoHomePage];
            }
            
        } andFailedBlock:^{
            [self loginSuc];
        }];
    }
}


#pragma mark 错误处理
-(void)errorViewShowWithTextField:(UITextField*)textField{
    errorImageView = [UIImageView new];
    errorImageView.image = [UIImage imageNamed:@"Tooltip"];
    [self.view addSubview:errorImageView];
    
    Label *promptLabel = [[Label alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 30) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor whiteColor] andTextLines:1];
    promptLabel.textAlignment = NSTextAlignmentCenter;
    if (textField == emailTextField) {
        promptLabel.text = @"Please enter your email address.";
    }else{
        promptLabel.text = @"Password should be btween 6 ~ 18 characters.";

    }
    [errorImageView addSubview:promptLabel];
    
    if (textField == emailTextField) {
        errorImageView.sd_layout
        .bottomSpaceToView(tfView,0)
        .leftSpaceToView(self.view,0)
        .rightSpaceToView(self.view,0)
        .heightIs(39);
    }else{
        errorImageView.sd_layout
        .bottomSpaceToView(tfView,-50)
        .leftSpaceToView(self.view,0)
        .rightSpaceToView(self.view,0)
        .heightIs(39);
    }
    

}



#pragma mark 注册
-(void)registerBtnClick
{
    EmailRegisterViewController *emailRegisterVC = [[EmailRegisterViewController alloc]init];
    emailRegisterVC.isfromBookingInfo = self.isfromBookingInfo;
    [self.navigationController pushViewController:emailRegisterVC animated:YES];
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
