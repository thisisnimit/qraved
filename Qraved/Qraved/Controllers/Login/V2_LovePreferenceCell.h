//
//  V2_LovePreferenceCell.h
//  Qraved
//
//  Created by harry on 2017/6/9.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_LovePreferenceModel.h"
@interface V2_LovePreferenceCell : UICollectionViewCell

@property (nonatomic, strong) UILabel *lblTitle;
@property (nonatomic, strong) V2_CuisineModel *model;
@end
