//
//  PhotosDetailController.h
//  Qraved
//
//  Created by Shine Wang on 7/18/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostDataHandler.h"
#import "BaseViewController.h"

@interface FavouriteCuisineController : BaseViewController<UIScrollViewDelegate> {
    UIScrollView *scrView;
}
@property (nonatomic ,strong) NSMutableArray *cuisineList;
@property (nonatomic ,strong) NSMutableArray *imgUrlArray;
@end
