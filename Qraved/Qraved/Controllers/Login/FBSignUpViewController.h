//
//  FBSignUpViewController.h
//  Qraved
//
//  Created by Lucky on 15/5/25.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Label.h"
#import "BaseChildViewController.h"
#import "LoginViewController.h"
#import "FBUser.h"
#import "TWitterUser.h"
#import "EnterEmailViewController.h"
#import "updateUserInfoViewController.h"


//@protocol SighUpDelegate <NSObject>
//@optional
//-(void)signUpSuccess;
//@end

@interface FBSignUpViewController : BaseChildViewController<UITextFieldDelegate,registerDelegate,CLLocationManagerDelegate,SighUpDelegate>

@property(nonatomic,retain) UITextField *emailTextField;
@property(nonatomic,retain) UITextField *firstNameTextField;
@property(nonatomic,retain) UITextField *lastNameTextField;
@property(nonatomic,retain) UITextField *birthdayTextField;
@property(nonatomic,retain) UIButton *signUpButton;
@property(nonatomic,retain) Label *loginButton;
@property(nonatomic,strong) LoginViewController *loginViewController;
//@property(nonatomic)        FBSession *session;
//@property(nonatomic)        FBSessionState state;
@property(nonatomic)        NSDictionary *dataDic;
@property(nonatomic)        BOOL isFromBooking;
@property (nonatomic,copy) NSString *amplitudeType;
- (void)regButtonClick:(id)sender;

@property(nonatomic,retain) id<SighUpDelegate> delegate;
@property(nonatomic,retain)UIControl *mainView;

@end
