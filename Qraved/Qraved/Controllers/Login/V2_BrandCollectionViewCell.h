//
//  V2_BrandCollectionViewCell.h
//  Qraved
//
//  Created by harry on 2017/6/9.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_BrandModel.h"
@interface V2_BrandCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIView *selectView;
@property (nonatomic, strong) UIImageView *imvCheckmark;
@property (nonatomic, strong) V2_BrandModel *brandModel;
@end
