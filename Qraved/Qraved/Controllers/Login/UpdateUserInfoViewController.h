//
//  updateUserInfoViewController.h
//  Qraved
//
//  Created by Lucky on 15/5/27.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseChildViewController.h"
#import "Label.h"

@protocol SighUpDelegate <NSObject>
@optional
-(void)signUpSuccess;
@end

@interface UpdateUserInfoViewController : BaseChildViewController<UITextFieldDelegate>

@property(nonatomic,retain) UITextField *emailTextField;
@property(nonatomic,retain) UITextField *firstNameTextField;
@property(nonatomic,retain) UITextField *lastNameTextField;
@property(nonatomic,retain) UITextField *birthdayTextField;
@property(nonatomic,retain) UITextField *codeTextField;
@property(nonatomic,retain) UITextField *phoneNumField;
@property(nonatomic,retain) UIButton *signUpButton;
@property(nonatomic,retain) Label *loginButton;
@property(nonatomic)        NSDictionary *dataDic;
- (void)regButtonClick:(id)sender;

@property(nonatomic,retain) id<SighUpDelegate> delegate;
@property(nonatomic,retain)UIControl *mainView;


@end
