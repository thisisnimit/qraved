//
//  FBSignUpViewController.m
//  Qraved
//
//  Created by Lucky on 15/5/25.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "FBSignUpViewController.h"
#import "AppDelegate.h"
#import <CoreText/CoreText.h>

#import "UIConstants.h"
//#import "MixpanelHelper.h"
#import "EmailLoginViewController.h"

#import "UIView+Helper.h"
#import "UIAlertView+BlocksKit.h"
#import "UILabel+Helper.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "UITextField+Helper.h"
#import "UIViewController+Helper.h"

#import "NavigationBarButtonItem.h"
#import "SendCall.h"

#import "PostDataHandler.h"
#import "LoginParamter.h"
#import "IMGCountryCode.h"
#import "LoadingView.h"
#import "SelectCityController.h"
#import "CityUtils.h"
#import "IMGCity.h"
#import "UpdateUserInfoViewController.h"
#import "V2_PreferenceViewController.h"

extern BOOL NeedExpandMenu;

@interface FBSignUpViewController ()<UIPickerViewDataSource,UIPickerViewDelegate,SelectCityDelegate>
{
    BOOL isViewUp;
    UIScrollView *bgImageView;
    UIControl *control;
    CGRect oriFrame;
    
    UIImageView *firstNameImageView;
    UIImageView *lastNameImageView;
    UIImageView *birthdayImageView;
    UIImageView *emailImageView;
    UIImageView *passwordImageView;
    UIImageView *codeImageView;
    UIImageView *phoneImageView;
    
    
    UIButton *mrButton;
    UIButton *msButton;
    UIButton *mrsButton;
    
    NSInteger sexSelect;
    
    UIView *dateView;
    UIDatePicker *datePicker;
    UIButton *doneButton;
    
    UIView *codeNumView;
    UIPickerView *codeNumPicker;
    UIButton *doneButton2;
    
    NSMutableArray *codeArray;
    NSString *selectCode;
    NSInteger selectCodeIndex;
    CLLocationManager *clmanager;
    UIButton *boarderControl;
    UILabel *errorLabel;
}
@end

@implementation FBSignUpViewController
@synthesize emailTextField,firstNameTextField,lastNameTextField,birthdayTextField,signUpButton,loginButton;

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    signUpButton.enabled = YES;
    self.screenName=@"Register";
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
#pragma mark 隐藏navigationBar
    [self.navigationController.navigationBar setHidden:YES];
    [self.navigationItem setTitle:L(@"Sign Up")];
    
#pragma mark 背景 bgImageView
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:self.view.bounds];
    imageView.image = [UIImage imageNamed:@"EmailLogin1.jpg"];
    imageView.userInteractionEnabled = YES;
    [self.view addSubview:imageView];
    
    bgImageView = [[UIScrollView alloc]initWithFrame:imageView.bounds];
    [imageView addSubview:bgImageView];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundTap:)];
    [bgImageView addGestureRecognizer:tapGesture];
    
    
#pragma mark 键盘 isViewUp
    isViewUp=NO;
    
    [self loadMainView];
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(DeviceWidth-45, 20, 45, 45)];
    [closeButton setImage:[UIImage imageNamed:@"closeWhite" ] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeButton];
    [self.view bringSubviewToFront:closeButton];
    
    codeArray = [[NSMutableArray alloc]init];
    [[DBManager manager]selectWithSql:@"select *from IMGCountryCode order by CAST(code AS INTeger)" successBlock:^(FMResultSet *resultSet) {
        while ([resultSet next]) {
            IMGCountryCode *countryCode = [[IMGCountryCode alloc]init];
            [countryCode setValueWithResultSet:resultSet];
            [codeArray addObject:countryCode];
        }
        [resultSet close];
        for (int i =0;i<codeArray.count;i++) {
            IMGCountryCode *code = [codeArray objectAtIndex:i];
            if ([code.code intValue] == 62) {
                selectCode = [NSString stringWithFormat:@"%@",code.code];
                selectCodeIndex = i;
                break;
            }
        }
    } failureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    

}

-(void)back
{
    if (self.amplitudeType.length>0) {
        [[Amplitude instance] logEvent:@"SU - Sign Up Cancel" withEventProperties:@{@"Sign Up Type":self.amplitudeType}];
    }
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)loadMainView
{
    FBUser *fbUser=[[FBUser alloc]init];
#pragma mark logo+title logoImage logoLabel
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 120, DeviceWidth, 60)];
    titleLabel.text=@"Almost there!\nIs this info correct?";
    titleLabel.numberOfLines = 2;
    titleLabel.font = [UIFont systemFontOfSize:13];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    [bgImageView addSubview:titleLabel];
    

    NSArray *sexArray = @[L(@"Mr."),L(@"Ms."),L(@"Mrs.")];
    
    mrButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mrButton addTarget:self action:@selector(sexSelect:) forControlEvents:UIControlEventTouchUpInside];
    mrButton.frame = CGRectMake(34, 180, (DeviceWidth-34*2-20)/3, 35);
    if (![UIDevice isRunningOniPhone]) {
        mrButton.frame = CGRectMake(34, 220, (DeviceWidth-34*2-20)/3, 35);
        
    }
    if ([UIDevice isIphone4Now]) {
        mrButton.frame = CGRectMake(34, 100, (DeviceWidth-34*2-20)/3, 35);
    }
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *effectview = [[UIVisualEffectView alloc] initWithEffect:blur];
    effectview.frame =mrButton.frame;
    effectview.alpha = 0.5;
    effectview.clipsToBounds = YES;
    effectview.layer.cornerRadius = 10;
    [bgImageView addSubview:effectview];
    [bgImageView addSubview:mrButton];
    
    [mrButton setTitle:[sexArray objectAtIndex:0] forState:UIControlStateNormal];
    [mrButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    mrButton.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    sexSelect = 1;
    
    
    msButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [msButton addTarget:self action:@selector(sexSelect:) forControlEvents:UIControlEventTouchUpInside];
    msButton.frame = CGRectMake(34+((DeviceWidth-34*2-20)/3+10)*1, 180, (DeviceWidth-34*2-20)/3, 35);
    if (![UIDevice isRunningOniPhone]) {
        msButton.frame = CGRectMake(34+((DeviceWidth-34*2-20)/3+10)*1, 220, (DeviceWidth-34*2-20)/3, 35);
    }
    if ([UIDevice isIphone4Now]) {
        msButton.frame = CGRectMake(34+((DeviceWidth-34*2-20)/3+10)*1, 100, (DeviceWidth-34*2-20)/3, 35);
        
    }
    UIBlurEffect *blurMs = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *effectviewMs = [[UIVisualEffectView alloc] initWithEffect:blurMs];
    effectviewMs.frame =msButton.frame;
    effectviewMs.alpha = 0.5;
    effectviewMs.clipsToBounds = YES;
    effectviewMs.layer.cornerRadius = 10;
    [bgImageView addSubview:effectviewMs];
    [bgImageView addSubview:msButton];
    [msButton setTitle:[sexArray objectAtIndex:1] forState:UIControlStateNormal];
    [msButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    msButton.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    
    
    mrsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mrsButton addTarget:self action:@selector(sexSelect:) forControlEvents:UIControlEventTouchUpInside];
    mrsButton.frame = CGRectMake(34+((DeviceWidth-34*2-20)/3+10)*2, 180, (DeviceWidth-34*2-20)/3, 35);
    if (![UIDevice isRunningOniPhone]) {
        mrsButton.frame = CGRectMake(34+((DeviceWidth-34*2-20)/3+10)*2, 220, (DeviceWidth-34*2-20)/3, 35);
    }
    if ([UIDevice isIphone4Now]) {
        mrsButton.frame = CGRectMake(34+((DeviceWidth-34*2-20)/3+10)*2, 100, (DeviceWidth-34*2-20)/3, 35);
        
    }
    UIBlurEffect *blurMrs = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *effectviewMrs = [[UIVisualEffectView alloc] initWithEffect:blurMrs];
    effectviewMrs.frame =mrsButton.frame;
    effectviewMrs.alpha = 0.5;
    effectviewMrs.clipsToBounds = YES;
    effectviewMrs.layer.cornerRadius = 10;
    [bgImageView addSubview:effectviewMrs];
    [bgImageView addSubview:mrsButton];
    [mrsButton setTitle:[sexArray objectAtIndex:2] forState:UIControlStateNormal];
    [mrsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    mrsButton.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    
    //
    
#pragma mark name
    firstNameImageView = [[UIImageView alloc]initWithFrame:CGRectMake(34, 180+35+12, DeviceWidth-68, 35)];
    if (![UIDevice isRunningOniPhone]) {
        firstNameImageView.frame = CGRectMake(34, mrButton.endPointY+12, DeviceWidth-68, 35);
    }
    if ([UIDevice isIphone4Now]) {
        firstNameImageView.frame = CGRectMake(34, mrButton.endPointX+12, DeviceWidth-34*2, 35);
        
    }
    
    firstNameImageView.userInteractionEnabled = YES;
    UIBlurEffect *blurFirst = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *effectviewFirst = [[UIVisualEffectView alloc] initWithEffect:blurFirst];
    effectviewFirst.frame =firstNameImageView.frame;
    effectviewFirst.alpha = 0.5;
    effectviewFirst.clipsToBounds = YES;
    effectviewFirst.layer.cornerRadius = 10;
    [bgImageView addSubview:effectviewFirst];
    [bgImageView addSubview:firstNameImageView];
    
    UIImageView *nameLogoImage = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth-68-23, 9, 12, 16)];
    nameLogoImage.image = [UIImage imageNamed:@"name"];
    [firstNameImageView addSubview:nameLogoImage];
    
    firstNameTextField = [[UITextField alloc]initWithFrame:CGRectMake(10, 0, DeviceWidth-68-25-10, 35)];
    firstNameTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    firstNameTextField.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    firstNameTextField.text = [_dataDic objectForKey:fbUser.firstName];
    //    firstNameTextField.placeholder = L(@"First Name");
    firstNameTextField. attributedPlaceholder= [[NSAttributedString alloc] initWithString:L(@"First Name") attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    firstNameTextField.textColor = [UIColor whiteColor];
    [firstNameImageView addSubview:firstNameTextField];
    
    
    lastNameImageView = [[UIImageView alloc]initWithFrame:CGRectMake(34, firstNameImageView.endPointY+12, DeviceWidth-68, 35)];
    UIBlurEffect *blurLast = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *effectviewLast = [[UIVisualEffectView alloc] initWithEffect:blurLast];
    effectviewLast.frame =lastNameImageView.frame;
    effectviewLast.alpha = 0.5;
    effectviewLast.clipsToBounds = YES;
    effectviewLast.layer.cornerRadius = 10;
    [bgImageView addSubview:effectviewLast];
    lastNameImageView.userInteractionEnabled = YES;
    [bgImageView addSubview:lastNameImageView];
    
    UIImageView *lastNameLogoImage = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth-68-23, 9, 12, 16)];
    lastNameLogoImage.image = [UIImage imageNamed:@"name"];
    [lastNameImageView addSubview:lastNameLogoImage];
    
    lastNameTextField = [[UITextField alloc]initWithFrame:CGRectMake(10, 0, DeviceWidth-68-25-10, 35)];
    lastNameTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    lastNameTextField.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    lastNameTextField.text = [_dataDic objectForKey:fbUser.lastName];
    //    lastNameTextField.placeholder = L(@"Last Name");
    lastNameTextField. attributedPlaceholder= [[NSAttributedString alloc] initWithString:L(@"Last Name") attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    lastNameTextField.textColor = [UIColor whiteColor];
    [lastNameImageView addSubview:lastNameTextField];
    
#pragma mark birthday
//    birthdayImageView = [[UIImageView alloc]initWithFrame:CGRectMake(34, lastNameImageView.endPointY+12, DeviceWidth-68, 35)];
//    birthdayImageView.userInteractionEnabled = YES;
//    UIBlurEffect *blurBirthday = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
//    UIVisualEffectView *effectviewBirthday = [[UIVisualEffectView alloc] initWithEffect:blurBirthday];
//    effectviewBirthday.frame =birthdayImageView.frame;
//    effectviewBirthday.alpha = 0.5;
//    effectviewBirthday.clipsToBounds = YES;
//    effectviewBirthday.layer.cornerRadius = 10;
//    [bgImageView addSubview:effectviewBirthday];
//    
//    [bgImageView addSubview:birthdayImageView];
//    
//    birthdayTextField = [[UITextField alloc]initWithFrame:CGRectMake(10, 0, DeviceWidth-68-25-10, 35)];
//    birthdayTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
//    birthdayTextField.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
//    birthdayTextField.text = [_dataDic objectForKey:fbUser.birthday];
//    //    birthdayTextField.placeholder = L(@"Birthday");
//    birthdayTextField. attributedPlaceholder= [[NSAttributedString alloc] initWithString:L(@"Birthday") attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
//    birthdayTextField.textColor = [UIColor whiteColor];
//    birthdayTextField.enabled = NO;
//    [birthdayImageView addSubview:birthdayTextField];
//    
//    UIImageView *birthdayLogoImage = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth-68-23, 9, 16, 16)];
//    birthdayLogoImage.image = [UIImage imageNamed:@"birthday"];
//    [birthdayImageView addSubview:birthdayLogoImage];
//    
//    UITapGestureRecognizer *tapgesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addDatePicker)];
//    [birthdayImageView addGestureRecognizer:tapgesture];
//    
#pragma mark email
    
    emailImageView = [[UIImageView alloc]initWithFrame:CGRectMake(34, lastNameImageView.endPointY+12, DeviceWidth-68, 35)];
    UIBlurEffect *blurEmail = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *effectviewEmail = [[UIVisualEffectView alloc] initWithEffect:blurEmail];
    effectviewEmail.frame =emailImageView.frame;
    effectviewEmail.alpha = 0.5;
    effectviewEmail.clipsToBounds = YES;
    effectviewEmail.layer.cornerRadius = 10;
    [bgImageView addSubview:effectviewEmail];
    emailImageView.userInteractionEnabled = YES;
    [bgImageView addSubview:emailImageView];
    
    UIImageView *emailLogoImage = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth-68-25, 12, 16, 12)];
    emailLogoImage.image = [UIImage imageNamed:@"LoginEmail"];
    [emailImageView addSubview:emailLogoImage];
    
    emailTextField = [[UITextField alloc]initWithFrame:CGRectMake(10, 0, DeviceWidth-68-25-10, 35)];
    emailTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    emailTextField.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    //    emailTextField.placeholder = L(@"Email");
    emailTextField. attributedPlaceholder= [[NSAttributedString alloc] initWithString:L(@"Email") attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    NSString *emailStr = [_dataDic objectForKey:fbUser.email];
    NSRange rang = [emailStr rangeOfString:@"@"];
    if (rang.location != NSNotFound)
    {
        emailTextField.text = [_dataDic objectForKey:fbUser.email];
    }
    emailTextField.textColor = [UIColor whiteColor];
    [emailImageView addSubview:emailTextField];
    
    
    
#pragma mark phone num
    
    firstNameTextField.returnKeyType = UIReturnKeyDone;
    firstNameTextField.delegate = self;
    
    lastNameTextField.returnKeyType = UIReturnKeyDone;
    lastNameTextField.delegate = self;
    
    emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    emailTextField.returnKeyType = UIReturnKeyDone;
    emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    emailTextField.delegate = self;

    
#pragma mark sign up
    signUpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    if ([UIDevice isIphone5]) {
    //        signUpButton.frame = CGRectMake(34, passwordImageView.endPointY+27.5, DeviceWidth-68, 44);
    //    }
    //    else
    //    {
    //        signUpButton.frame = CGRectMake(34, phoneImageView.endPointY+7.5, DeviceWidth-68, 44);
    //    }
    signUpButton.frame = CGRectMake(34, emailImageView.endPointY+27.5, DeviceWidth-68, 44);
    signUpButton.clipsToBounds = YES;
    signUpButton.layer.cornerRadius = 10;
//    [signUpButton setBackgroundImage:[UIImage imageNamed:@"signMeUpBtn"] forState:UIControlStateNormal];
    [signUpButton setTitle:L(@"Confirm") forState:UIControlStateNormal];
    signUpButton.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:15];
    [signUpButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    signUpButton.backgroundColor = [UIColor colorC2060A];
    [signUpButton addTarget:self action:@selector(regButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgImageView addSubview:signUpButton];
    
    bgImageView.contentSize = CGSizeMake(DeviceWidth, signUpButton.endPointY+30);
    
}
#pragma mark textField Delegae
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (errorLabel) {
        [self removeErrorView];
        return;
    }

    CGFloat viewUpHeight;
    if (textField == firstNameTextField){
        viewUpHeight = [UIDevice isLaterThanIphone5]?0:0;
    }else if (textField == lastNameTextField){
        viewUpHeight = [UIDevice isLaterThanIphone5]?0:lastNameImageView.startPointY-130;
    }else if (textField == birthdayTextField){
        viewUpHeight = [UIDevice isLaterThanIphone5]?birthdayImageView.startPointY-130:birthdayImageView.startPointY-130;
    }else if (textField == emailTextField){
        viewUpHeight = [UIDevice isLaterThanIphone5]?emailImageView.startPointY-200:emailImageView.startPointY-130;
    }
    if(!isViewUp)
    {
        [UIView animateWithDuration:0.3 animations:^{
            bgImageView.contentSize = CGSizeMake(DeviceWidth, loginButton.endPointY+130);
            
            //            self.view.frame = CGRectMake(0, -viewUpHeight, self.view.frame.size.width, self.view.frame.size.height);
            bgImageView.contentOffset = CGPointMake(0, viewUpHeight);
            if (control) {
                control.frame = CGRectMake(0, -viewUpHeight, DeviceWidth, DeviceHeight+viewUpHeight);
            }
        }];
        isViewUp=YES;
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (errorLabel) {
        [self removeErrorView];
        return YES;
    }

    if (control != nil) {
        control.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight);
        
        if (textField == firstNameTextField) {
            [firstNameImageView removeFromSuperview];
            firstNameImageView.image = [UIImage imageNamed:@"input-box"];
            [bgImageView addSubview:firstNameImageView];
        }
        if (textField == lastNameTextField) {
            [lastNameImageView removeFromSuperview];
            lastNameImageView.image = [UIImage imageNamed:@"input-box"];
            [bgImageView addSubview:lastNameImageView];
        }
        if (textField == birthdayTextField) {
            [birthdayImageView removeFromSuperview];
            birthdayImageView.image = [UIImage imageNamed:@"input-box"];
            [bgImageView addSubview:birthdayImageView];
        }
        if (textField == emailTextField) {
            [emailImageView removeFromSuperview];
            emailImageView.image = [UIImage imageNamed:@"input-box"];
            [bgImageView addSubview:emailImageView];
        }
        [textField resignFirstResponder];
        [control removeFromSuperview];
        control = nil;
    }
    [firstNameTextField resignFirstResponder];
    [lastNameTextField resignFirstResponder];
    [emailTextField resignFirstResponder];
   
    if(isViewUp)
    {
        [UIView animateWithDuration:0.3 animations:^{
            bgImageView.contentSize = CGSizeMake(DeviceWidth, loginButton.endPointY+30);
            //            self.view.frame = CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height);
            bgImageView.contentOffset = CGPointMake(0, 0);
        }];
        isViewUp=NO;
    }
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (errorLabel) {
        [self removeErrorView];
        return YES;
    }

    if (control != nil) {
        control.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight);
        
        if (textField == firstNameTextField) {
            [firstNameImageView removeFromSuperview];
            firstNameImageView.image = [UIImage imageNamed:@"input-box"];
            [bgImageView addSubview:firstNameImageView];
        }
        if (textField == lastNameTextField) {
            [lastNameImageView removeFromSuperview];
            lastNameImageView.image = [UIImage imageNamed:@"input-box"];
            [bgImageView addSubview:lastNameImageView];
        }
        if (textField == birthdayTextField) {
            [birthdayImageView removeFromSuperview];
            birthdayImageView.image = [UIImage imageNamed:@"input-box"];
            [bgImageView addSubview:birthdayImageView];
        }
        if (textField == emailTextField) {
            [emailImageView removeFromSuperview];
            emailImageView.image = [UIImage imageNamed:@"input-box"];
            [bgImageView addSubview:emailImageView];
        }
                [textField becomeFirstResponder];
        [control removeFromSuperview];
        control = nil;
    }
    
    return YES;
}
#pragma mark 错误处理
-(void)errorViewShowWithMessage:(NSString*)message andErrorView:(UIView*)errorView{
    [[LoadingView sharedLoadingView] stopLoading];
    if (boarderControl) {
        [self removeErrorView];
    }
    if (errorLabel) {
        [self removeErrorView];
    }
    boarderControl = [[UIButton alloc] initWithFrame:CGRectMake(errorView.startPointX-2, errorView.startPointY-2, errorView.frame.size.width+4, errorView.frame.size.height+4)];
    boarderControl.clipsToBounds = YES;
    boarderControl.layer.cornerRadius = 10;
    boarderControl.layer.borderWidth = 2;
    boarderControl.layer.borderColor = [UIColor colorC2060A].CGColor;
    [boarderControl addTarget:self action:@selector(removeErrorView) forControlEvents:UIControlEventTouchUpInside];
    [errorView.superview addSubview:boarderControl];
    
    errorLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, emailImageView.startPointY+20, DeviceWidth, 40)];
    errorLabel.text = message;
    errorLabel.font = [UIFont systemFontOfSize:15];
    errorLabel.backgroundColor = [UIColor colorC2060A];
    [errorLabel setTextColor:[UIColor whiteColor]];
    errorLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:errorLabel];
    
}
-(void)removeErrorView{
    [errorLabel removeFromSuperview];
    [boarderControl removeFromSuperview];
    errorLabel = nil;
    boarderControl = nil;
}
//-(void)errorViewShowWithTextField:(UITextField*)textField andImageView:(UIImageView*)imageView withErrorMessage:(NSString *)message
//{
//    [[LoadingView sharedLoadingView] stopLoading];
//    bgImageView.contentOffset = CGPointMake(0, 0);
//    [self backgroundTap:nil];
//    
//    if (control == nil) {
//        control = [[UIControl alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
//        [control setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Mask@iphone5"]]];
//        [self.view addSubview:control];
//    }
//    control.frame = CGRectMake(0, -bgImageView.contentOffset.y, DeviceWidth, DeviceHeight+bgImageView.contentOffset.y);
//    
//    
//    [imageView removeFromSuperview];
//    [control addSubview:imageView];
//    imageView.image = [UIImage imageNamed:@"input-box-selected"];
//    oriFrame = imageView.frame;
//    
//    
//    UIImageView *promptView = [[UIImageView alloc]initWithFrame:CGRectMake(0, oriFrame.origin.y-43, DeviceWidth, 39)];
//    promptView.image = [UIImage imageNamed:@"Tooltip"];
//    [control addSubview:promptView];
//    Label *promptLabel = [[Label alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 30) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor whiteColor] andTextLines:1];
//    promptLabel.textAlignment = NSTextAlignmentCenter;
//    promptLabel.text = message;
//    [promptView addSubview:promptLabel];
//    
//    
//    
//}
//
-(void)backgroundTap:(id)sender
{
    [firstNameTextField resignFirstResponder];
    [lastNameTextField resignFirstResponder];
    [emailTextField resignFirstResponder];
    [self hideDatePicker];
    [self hideCodeNumPicker];
    
    if(isViewUp)
    {
        [UIView animateWithDuration:0.3 animations:^{
            bgImageView.contentSize = CGSizeMake(DeviceWidth, loginButton.endPointY+30);
            //            self.view.frame = CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height);
            bgImageView.contentOffset = CGPointMake(0, 0);
        }];
        isViewUp=NO;
    }
}

 
-(void)sexSelect:(UIButton *)button{
    if (errorLabel) {
        [self removeErrorView];
        return;
    }
    button.selected = YES;
    
    if (button == mrButton) {
        sexSelect = 1;
        mrButton.clipsToBounds = YES;
        mrButton.layer.cornerRadius = 10;
        mrButton.layer.borderWidth = 3;
        mrButton.layer.borderColor = [UIColor colorC2060A].CGColor;
        msButton.layer.borderColor = [UIColor clearColor].CGColor;
        mrsButton.layer.borderColor = [UIColor clearColor].CGColor;
        
        msButton.selected = NO;
        mrsButton.selected = NO;
    }else if (button == msButton){
        sexSelect = 2;
        mrButton.selected = NO;
        mrsButton.selected = NO;
        msButton.clipsToBounds = YES;
        msButton.layer.cornerRadius = 10;
        
        msButton.layer.borderWidth = 3;
        msButton.layer.borderColor = [UIColor colorC2060A].CGColor;
        mrButton.layer.borderColor = [UIColor clearColor].CGColor;
        mrsButton.layer.borderColor = [UIColor clearColor].CGColor;
        
    }else{
        sexSelect = 3;
        mrButton.selected = NO;
        msButton.selected = NO;
        mrsButton.clipsToBounds = YES;
        mrsButton.layer.cornerRadius = 10;
        mrsButton.layer.borderWidth = 3;
        mrsButton.layer.borderColor = [UIColor colorC2060A].CGColor;
        mrButton.layer.borderColor = [UIColor clearColor].CGColor;
        msButton.layer.borderColor = [UIColor clearColor].CGColor;
        
    }
    
}
-(void)regButtonClick:(id)sender{
    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(starButtonClicked:) object:sender];
    [self performSelector:@selector(starButtonClicked:) withObject:sender afterDelay:1.0f];
    
}


-(void)hideCodeNumPicker{
    [codeNumPicker removeFromSuperview];
    [codeNumView removeFromSuperview];
}
#pragma mark picker view delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component == 0) {
        return;
    }
    IMGCountryCode *code = [codeArray objectAtIndex:row];
    selectCode = [NSString stringWithFormat:@"%@",code.code];
    selectCodeIndex = row;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0) {
        return 1;
    }
    return codeArray.count;
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}
-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    if (component == 0) {
        return 40.0f;
    }
    return 100.0f;
}
#pragma mark picker view dataSource
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (component == 0) {
        return @"+";
    }
    IMGCountryCode *code = [codeArray objectAtIndex:row];
    return [NSString stringWithFormat:@"%@",code.code];
}
-(NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (component == 0) {
        return nil;
    }
    IMGCountryCode *code = [codeArray objectAtIndex:row];
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",code.code]];
    [attriString addAttribute:(NSString *)kCTFontAttributeName
                        value:(id)CFBridgingRelease(CTFontCreateWithName((CFStringRef)[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13].fontName,13,NULL))range:NSMakeRange(0, [NSString stringWithFormat:@"%@",code.code].length-1)];
    return attriString;
}

#pragma mark add date picker
//-(void)addDatePicker{
//    [self backgroundTap:nil];
//    if (errorLabel) {
//        [self removeErrorView];
//        return;
//    }
//
//    if ([[birthdayImageView superview] isKindOfClass:[UIControl class]]){
//        [birthdayImageView removeFromSuperview];
//        birthdayImageView.image = [UIImage imageNamed:@"input-box"];
//        [bgImageView addSubview:birthdayImageView];
//        [control removeFromSuperview];
//        control = nil;
//    }
//    
//    if (dateView == nil) {
//        dateView = [[UIView alloc]initWithFrame:CGRectMake(0, DeviceHeight-290+20, DeviceWidth, 290)];
//    }
//    dateView.backgroundColor = [UIColor whiteColor];
//    if (datePicker == nil) {
//        datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 40, DeviceWidth, 250)];
//    }
//    datePicker.datePickerMode = UIDatePickerModeDate;
////    NSDate* maxdate=[NSDate dateWithTimeIntervalSinceNow:-365*24*60*60*5];
////    NSString *maxStr = @"2011-12-31";
//    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//    NSDate *now = [NSDate date];;
//    NSDateComponents *comps = [[NSDateComponents alloc] init];
//    NSInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit |
//    NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
//    comps = [calendar components:unitFlags fromDate:now];
//    NSInteger  year = [comps year];
//    NSString *maxStr =[NSString stringWithFormat:@"%ld-12-31",(long)year-5];
//    NSDateFormatter *maxDateFormatter = [[NSDateFormatter alloc]init];
//    [maxDateFormatter setDateFormat:@"yyyy-MM-dd"];
//    NSDate *maxDate = [maxDateFormatter dateFromString:maxStr];
//    NSString *minStr = [NSString stringWithFormat:@"%ld-01-01",(long)year-80];
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
//    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
//    NSDate *minDate = [dateFormatter dateFromString:minStr];
//    datePicker.minimumDate=minDate;
//    datePicker.maximumDate=maxDate;
//    [dateView addSubview:datePicker];
//    [self.view addSubview:dateView];
//    
//    doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    doneButton.frame = CGRectMake(DeviceWidth-60, 0, 50, 40);
//    [doneButton setTitleColor:[UIColor color222222] forState:UIControlStateNormal];
//    [doneButton setTitle:L(@"Done") forState:UIControlStateNormal];
//    [dateView addSubview:doneButton];
//    [doneButton addTarget:self action:@selector(datePickerDone) forControlEvents:UIControlEventTouchUpInside];
//}
//-(void)datePickerDone{
//    
//    [datePicker removeFromSuperview];
//    [dateView removeFromSuperview];
//    NSLog(@"%@",datePicker.date);
//    NSString *datestr = [datePicker.date get_MM_DD_YYYYFormatString];
//    NSLog(@"%@",datestr);
//    birthdayTextField.text = datestr;
//    
//}
-(void)hideDatePicker{
    
    [datePicker removeFromSuperview];
    [dateView removeFromSuperview];
    
}

-(void)starButtonClicked:(id)sender
{
    [[LoadingView sharedLoadingView] startLoading];
    // NSString *completePhone=[NSString stringWithFormat:@"%@-%@",codeTextField.text,phoneNumField.text];
    NSString *trimmedString1 = [firstNameTextField.text stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceCharacterSet]];
    NSString *trimmedString2 = [lastNameTextField.text stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceCharacterSet]];
    
    if (!msButton.selected&&!mrButton.selected&&!mrsButton.selected) {
        
        [self errorViewShowWithMessage:L(@"Please select your gender.") andErrorView:mrButton];
        return;
    }
    if(firstNameTextField.text.length==0 || trimmedString1.length==0)
    {
        //        [self errorViewShowWithTextField:firstNameTextField andImageView:firstNameImageView withErrorMessage:L(@"This field cannot be empty")];
        [self errorViewShowWithMessage:L(@"Please enter your first name.") andErrorView:firstNameImageView];
        return ;
    }
    if( (firstNameTextField.text.length>20) || (trimmedString1.length>20) )
    {
        //        [self errorViewShowWithTextField:firstNameTextField andImageView:firstNameImageView withErrorMessage:L(@"First name should be at most 20 characters.")];
        [self errorViewShowWithMessage:L(@"First name should be at most 20 characters.") andErrorView:firstNameImageView];
        
        return ;
    }
    if(lastNameTextField.text.length==0 || trimmedString2.length==0)
    {
        //        [self errorViewShowWithTextField:lastNameTextField andImageView:lastNameImageView withErrorMessage:L(@"This field cannot be empty")];
        [self errorViewShowWithMessage:L(@"Please enter your last name.") andErrorView:lastNameImageView];
        
        return ;
    }
    if( (lastNameTextField.text.length>20) || (trimmedString2.length>20) )
    {
        //        [self errorViewShowWithTextField:lastNameTextField andImageView:lastNameImageView withErrorMessage:L(@"Last name should be at most 20 characters.")];
        [self errorViewShowWithMessage:L(@"Last name should be at most 20 characters.") andErrorView:firstNameImageView];
        
        return ;
    }
//    if(birthdayTextField.text.length==0)
//    {
//        //        [self errorViewShowWithTextField:birthdayTextField andImageView:birthdayImageView withErrorMessage:L(@"This field cannot be empty")];
//        [self errorViewShowWithMessage:L(@"Please select your birthday") andErrorView:birthdayImageView];
//
//        return ;
//    }
    
    if(emailTextField.text.length==0)
    {
        //        [self errorViewShowWithTextField:emailTextField andImageView:emailImageView withErrorMessage:L(@"This field cannot be empty")];
        [self errorViewShowWithMessage:L(@"Please enter your email address.") andErrorView:emailImageView];
        
        return;
    }
    
    if (![emailTextField.text isValidateEmail]) {
        //        [self errorViewShowWithTextField:emailTextField andImageView:emailImageView withErrorMessage:L(@"The format of the email address is invalid.")];
        [self errorViewShowWithMessage:@"The format of the email address is invalid." andErrorView:emailImageView];
        return;
    }
    
    
    signUpButton.enabled = NO;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:[NSString stringWithFormat:@"%ld",(long)sexSelect] forKey:@"gender"];
    [parameters setObject:firstNameTextField.text forKey:@"firstName"];
    [parameters setObject:lastNameTextField.text forKey:@"lastName"];
    [parameters setObject:@"01/01/1937" forKey:@"birthday"];
    [parameters setObject:emailTextField.text forKey:@"email"];
    [parameters setObject:@"signupwithself" forKey:@"loginwith"];
    [parameters setObject:@"4" forKey:@"productSource"];
    [parameters setObject:@"1" forKey:@"sourceType"];
    [parameters setObject:[_dataDic objectForKey:@"thirdPartyType"] forKey:@"thirdPartyType"];
    [parameters setObject:[_dataDic objectForKey:@"thirdPartyId"] forKey:@"thirdPartyId"];
    if ([_dataDic objectForKey:@"picture"]) {
        [parameters setObject:[_dataDic objectForKey:@"picture"] forKey:@"picture"];

    }
    
    [LoginParamter sharedParameter].loginwith=USER_LOGINTYPE_WITHSELF;
    
    UserDataHandler *userDataHandler=[[UserDataHandler alloc]init];
    [userDataHandler faceBookSignUp:parameters withBlock:^() {
        [[LoadingView sharedLoadingView] startLoading];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFacebookSignUp"];
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"isFromWebLink"] == nil) {
            [self requestDeeplink];
            [[NSUserDefaults standardUserDefaults] setObject:@"webLink" forKey:@"isFromWebLink"];
            return ;
        }
        
        [self loginSuc];


    } failedBlock:^(NSString *errorMsg) {
        if (self.amplitudeType.length>0) {
            [[Amplitude instance]
             logEvent:@"SU - Sign Up Failed" withEventProperties:@{@"Reason":errorMsg,@"Sign in type":self.amplitudeType}];
        }

        [self enterEmail];
    }];
}

- (void)requestDeeplink{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"installDeeplink"] == nil) {
        UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectZero];
        NSString *userAgent = [webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
        NSDictionary *dic = @{@"userAgent":userAgent};
        
        [UserDataHandler deeplink:dic andSuccessBlock:^(id responseObject) {
            if (responseObject == nil) {
                [self loginSuc];
            }else{
                [AppDelegate ShareApp].userLogined = YES;
                [[AppDelegate ShareApp] gotoHomePage];
            }
            
        } andFailedBlock:^{
            [self loginSuc];
        }];
    }
}

- (void)loginSuc{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"isOpenInApp"] != nil){
        [AppDelegate ShareApp].userLogined = YES;
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[AppDelegate ShareApp] gotoHomePage];
    }else{
        NSString *onboardingString = [[NSUserDefaults standardUserDefaults] objectForKey:ONBOARDING];
        if (onboardingString) {
            [AppDelegate ShareApp].userLogined = YES;
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[AppDelegate ShareApp] gotoHomePage];
            
        }else{
            
            V2_PreferenceViewController *V2_PreferenceVC = [[V2_PreferenceViewController alloc] init];
            V2_PreferenceVC.isFromProfile = YES;
            V2_PreferenceVC.isHaveOnBoarding = YES;
            [self.navigationController pushViewController:V2_PreferenceVC animated:YES];
        }
        
    }
}

-(void)enterEmail
{
    EnterEmailViewController *enterEmailViewController=[[EnterEmailViewController alloc]init];
    enterEmailViewController.delegate=self;
    [self.navigationController pushViewController:enterEmailViewController animated:YES];
}
-(void)registerSuccess
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loginSuccessful" object:nil];
}
-(void)loginBurronClick
{
//    EmailLoginViewController *emailLoginVC = [[EmailLoginViewController alloc]init];
//    [self.navigationController pushViewController:emailLoginVC animated:YES];
    
    [self.navigationController popViewControllerAnimated:YES];
    if (_isFromBooking)
    {
        [self.navigationController.navigationBar setHidden:NO];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
