//
//  PhotosDetailController.m
//  Qraved
//
//  Created by Shine Wang on 7/18/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "FavouriteCuisineController.h"
#define cuisineWidth 152.5
#define cuisineHeight 152.5

@interface FavouriteCuisineController ()


-(void)getDataSuccessful:(id)data;

@property(nonatomic,retain)NSMutableArray *photos;
@end

@implementation FavouriteCuisineController
@synthesize photos;
@synthesize imgUrlArray = _imgUrlArray;
@synthesize cuisineList = _cuisineList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}


-(void)loadRightBarItem
{
    
    UIImage *nextImg=[UIImage imageNamed:@"front_btn.png"];
    UIButton *search_btn=[UIButton buttonWithType:UIButtonTypeCustom];
    search_btn.frame = CGRectMake(0, 0, nextImg.size.width, nextImg.size.height);
    [search_btn setImage:nextImg forState:UIControlStateNormal];
    [search_btn addTarget:self action:@selector(nextStep) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarItem=[[UIBarButtonItem alloc]initWithCustomView:search_btn];
    self.navigationItem.rightBarButtonItem=rightBarItem;
}

-(void)nextStep
{
   
}

-(void)loadView
{
    [super loadView];
    
    
}



-(void)requestData {
//    NSDictionary *parameters=@{@"cityID":@"2",@"userID":[User sharedUser].userID};
//    PostDataHandler *postDataHandler=[[PostDataHandler alloc]init];
    
//        [postDataHandler httpRequest:parameters andPath:@"cuisines" andSuccessBlock:^(id postArray){
//            [self getDataSuccessful:postArray];
//            
//        } andFailedBlock:^(NSError *error){
//            
//            NSLog(@"failed..");
//        }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self loadRightBarItem];
    
    CGRect rect = [UIScreen mainScreen].bounds;
    scrView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 320, rect.size.height - 60)];
    scrView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:scrView];
    
    UILabel *lab1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 30)];
    lab1.backgroundColor = [UIColor clearColor];
    lab1.textColor = [UIColor color333333];
    lab1.textAlignment = NSTextAlignmentCenter;
    lab1.text = L(@"Welcome to Qraved!");
    lab1.font = [UIFont systemFontOfSize:17];
    [scrView addSubview:lab1];
    
    UILabel *lab2 = [[UILabel alloc]initWithFrame:CGRectMake(0, 30, 320, 20)];
    lab2.backgroundColor = [UIColor clearColor];
    lab2.textColor = [UIColor grayColor];
    lab2.text = L(@"Select your favourite cuisines");
    lab2.textAlignment = NSTextAlignmentCenter;
    lab2.font = [UIFont systemFontOfSize:15];
    [scrView addSubview:lab2];
    

    [self requestData];

}

-(void)postData
{
//    NSDictionary *parameters=@{@"cityID": @"2",@"sort":@"ranking"};
//    PostDataHandler *postDataHandler=[[PostDataHandler alloc]init];
//        [postDataHandler httpRequest:parameters andPath:@"search" andSuccessBlock:^(id postArray){
//            [self getPhotoSuccess:postArray];
//            
//        } andFailedBlock:^(){
//            
//        }];
}
-(void)getDataSuccessful:(id)data
{
    
}
-(void)getPhotoSuccess:(id)postArray
{
    NSArray *restaurantList=[postArray objectForKey:@"restaurantList"];
    
    for(int i=0;i<[restaurantList count];i++)
    {
        
        NSString *photoUrl=[[restaurantList objectAtIndex:i] objectForKey:@"imageUrl"];
        [self.photos addObject:photoUrl];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^(){

    });

}

 



@end
