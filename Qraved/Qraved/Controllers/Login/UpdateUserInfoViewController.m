//
//  updateUserInfoViewController.m
//  Qraved
//
//  Created by Lucky on 15/5/27.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "updateUserInfoViewController.h"

#import "AppDelegate.h"
#import <CoreText/CoreText.h>

#import "UIConstants.h"
//#import "MixpanelHelper.h"
#import "EmailLoginViewController.h"

#import "UIView+Helper.h"
#import "UIAlertView+BlocksKit.h"
#import "UILabel+Helper.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "UITextField+Helper.h"
#import "UIViewController+Helper.h"

#import "NavigationBarButtonItem.h"
#import "SendCall.h"


#import "PostDataHandler.h"
#import "LoginParamter.h"
#import "IMGCountryCode.h"


@interface UpdateUserInfoViewController ()<UIPickerViewDataSource,UIPickerViewDelegate>
{
    BOOL isViewUp;
    UIScrollView *bgImageView;
    UIControl *control;
    CGRect oriFrame;
    
    UIImageView *firstNameImageView;
    UIImageView *lastNameImageView;
    UIImageView *birthdayImageView;
    UIImageView *emailImageView;
    UIImageView *passwordImageView;
    UIImageView *codeImageView;
    UIImageView *phoneImageView;
    
    
    UIButton *mrButton;
    UIButton *msButton;
    UIButton *mrsButton;
    
    NSInteger sexSelect;
    
    UIView *dateView;
    UIDatePicker *datePicker;
    UIButton *doneButton;
    
    UIView *codeNumView;
    UIPickerView *codeNumPicker;
    UIButton *doneButton2;
    
    NSMutableArray *codeArray;
    NSString *selectCode;
    NSInteger selectCodeIndex;
    
}
@end

@implementation UpdateUserInfoViewController
@synthesize emailTextField,firstNameTextField,lastNameTextField,birthdayTextField,phoneNumField,signUpButton,loginButton,codeTextField;
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    signUpButton.enabled = YES;
    self.screenName=@"Register";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [super viewDidLoad];
#pragma mark 隐藏navigationBar
    [self.navigationController.navigationBar setHidden:YES];
    [self.navigationItem setTitle:L(@"Sign Up")];
    
#pragma mark 背景 bgImageView
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:self.view.bounds];
    imageView.image = [UIImage imageNamed:([UIDevice isIphone5])?@"LoginBg5.jpg":@"LoginBg4.jpg"];
    imageView.userInteractionEnabled = YES;
    [self.view addSubview:imageView];
    
    bgImageView = [[UIScrollView alloc]initWithFrame:imageView.bounds];
    [imageView addSubview:bgImageView];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundTap:)];
    [bgImageView addGestureRecognizer:tapGesture];
    
    
#pragma mark 键盘 isViewUp
    isViewUp=NO;
    
    [self loadMainView];
    
    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(DeviceWidth-45, 20, 45, 45)];
    [closeButton setImage:[UIImage imageNamed:@"closeWhite" ] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeButton];
    [self.view bringSubviewToFront:closeButton];
    
    codeArray = [[NSMutableArray alloc]init];
    [[DBManager manager]selectWithSql:@"select *from IMGCountryCode order by CAST(code AS INTeger)" successBlock:^(FMResultSet *resultSet) {
        while ([resultSet next]) {
            IMGCountryCode *countryCode = [[IMGCountryCode alloc]init];
            [countryCode setValueWithResultSet:resultSet];
            [codeArray addObject:countryCode];
        }
        [resultSet close];
        for (int i =0;i<codeArray.count;i++) {
            IMGCountryCode *code = [codeArray objectAtIndex:i];
            if ([code.code intValue] == 62) {
                selectCode = [NSString stringWithFormat:@"%@",code.code];
                selectCodeIndex = i;
                break;
            }
        }
    } failureBlock:^(NSError *error) {
    }];

}
-(void)back
{
    [self dismissViewControllerAnimated:YES completion:nil];
    //[self.navigationController popViewControllerAnimated:YES];
}
-(void)loadMainView
{
#pragma mark logo+title logoImage logoLabel
    UIImageView *logoImage = [[UIImageView alloc]initWithFrame:CGRectMake(80, 40, 160, 40)];
    //logoImage.image = [UIImage imageNamed:@"qraved_logo"];
    //[bgImageView addSubview:logoImage];
    IMGUser *user = [IMGUser currentUser];
    NSString *str = @"Please update your information.This will help us keep you FOOD-DATED!";
    CGSize expecSize = [str sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:10]];
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc]initWithString:str];
    NSRange range1 = [str rangeOfString:str];
    //    NSRange range2 = [str rangeOfString:@"FOR LESS"];
    CTFontRef font1 = CTFontCreateWithName((CFStringRef)[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:10].fontName, 10, NULL);
    //    CTFontRef font2 = CTFontCreateWithName((CFStringRef)[UIFont italicSystemFontOfSize:10].fontName, 10, NULL);
    
    [attStr addAttribute:(id)kCTFontAttributeName value:(id)CFBridgingRelease(font1) range:range1];
    [attStr addAttribute:(id)kCTForegroundColorAttributeName value:(id)[UIColor whiteColor].CGColor range:range1];
    //    [attStr addAttribute:(id)kCTFontAttributeName value:(id)CFBridgingRelease(font2) range:range2];
    //    [attStr addAttribute:(id)kCTForegroundColorAttributeName value:(id)[UIColor colorEEEEEE].CGColor range:range2];
    
    
    TYAttributedLabel *logoLabel = [[TYAttributedLabel alloc]initWithFrame:CGRectMake(DeviceWidth/2-expecSize.width/2, logoImage.endPointY+15, expecSize.width+10, expecSize.height)];
    logoLabel.attributedText = attStr;
    logoLabel.backgroundColor = [UIColor clearColor];
//    [logoLabel setAttributedString:attStr];
//    logoLabel.numberOfLines = 0;
    //[bgImageView addSubview:logoLabel];
    
    UILabel *mainLabel = [[UILabel alloc] init];
    mainLabel.text = str;
    mainLabel.textAlignment = NSTextAlignmentCenter;
    mainLabel.textColor = [UIColor whiteColor];
    mainLabel.font = [UIFont systemFontOfSize:11];
    CGSize size = [mainLabel.text sizeWithFont:mainLabel.font constrainedToSize:CGSizeMake(DeviceWidth-68, 10000) lineBreakMode:NSLineBreakByCharWrapping];
    mainLabel.numberOfLines = 0;
    mainLabel.frame = CGRectMake(34, 70, size.width, size.height);
    [bgImageView addSubview:mainLabel];
    
    
#pragma mark sex select
    NSArray *sexArray = @[L(@"Mr."),L(@"Ms."),L(@"Mrs.")];
    mrButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mrButton setBackgroundImage:[UIImage imageNamed:@"input-box"] forState:UIControlStateNormal];
    [mrButton setBackgroundImage:[UIImage imageNamed:@"signMeUpBtn"] forState:UIControlStateSelected];
    [bgImageView addSubview:mrButton];
    [mrButton addTarget:self action:@selector(sexSelect:) forControlEvents:UIControlEventTouchUpInside];
    mrButton.frame = CGRectMake(34, 130, (DeviceWidth-34*2-20)/3, 35);
    [mrButton setTitle:[sexArray objectAtIndex:0] forState:UIControlStateNormal];
    [mrButton setTitleColor:[UIColor color434343] forState:UIControlStateNormal];
    mrButton.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    //mrButton.selected = YES;
    sexSelect = [user.gender intValue];
    
    
    msButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [msButton setBackgroundImage:[UIImage imageNamed:@"input-box"] forState:UIControlStateNormal];
    [msButton setBackgroundImage:[UIImage imageNamed:@"signMeUpBtn"] forState:UIControlStateSelected];
    [bgImageView addSubview:msButton];
    [msButton addTarget:self action:@selector(sexSelect:) forControlEvents:UIControlEventTouchUpInside];
    msButton.frame = CGRectMake(34+((DeviceWidth-34*2-20)/3+10)*1, 130, (DeviceWidth-34*2-20)/3, 35);
    [msButton setTitle:[sexArray objectAtIndex:1] forState:UIControlStateNormal];
    [msButton setTitleColor:[UIColor color434343] forState:UIControlStateNormal];
    msButton.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    
    
    mrsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mrsButton setBackgroundImage:[UIImage imageNamed:@"input-box"] forState:UIControlStateNormal];
    [mrsButton setBackgroundImage:[UIImage imageNamed:@"signMeUpBtn"] forState:UIControlStateSelected];
    [bgImageView addSubview:mrsButton];
    [mrsButton addTarget:self action:@selector(sexSelect:) forControlEvents:UIControlEventTouchUpInside];
    mrsButton.frame = CGRectMake(34+((DeviceWidth-34*2-20)/3+10)*2, 130, (DeviceWidth-34*2-20)/3, 35);
    [mrsButton setTitle:[sexArray objectAtIndex:2] forState:UIControlStateNormal];
    [mrsButton setTitleColor:[UIColor color434343] forState:UIControlStateNormal];
    mrsButton.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    switch (sexSelect)
    {
        case 1:
        {
            mrButton.selected = YES;
        }
            break;
        case 2:
        {
            msButton.selected = YES;
        }
            break;
        case 3:
        {
            mrsButton.selected = YES;
        }
            break;
            
        default:
            break;
    }
    //
    
#pragma mark name
    firstNameImageView = [[UIImageView alloc]initWithFrame:CGRectMake(34, 130+35+12, DeviceWidth-68, 35)];
    firstNameImageView.userInteractionEnabled = YES;
    firstNameImageView.image = [UIImage imageNamed:@"input-box"];
    [bgImageView addSubview:firstNameImageView];
    
    UIImageView *nameLogoImage = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth-68-23, 9, 12, 16)];
    nameLogoImage.image = [UIImage imageNamed:@"name"];
    [firstNameImageView addSubview:nameLogoImage];
    
    firstNameTextField = [[UITextField alloc]initWithFrame:CGRectMake(10, 0, DeviceWidth-68-25-10, 35)];
    firstNameTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    firstNameTextField.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    firstNameTextField.placeholder = L(@"First Name");
    [firstNameImageView addSubview:firstNameTextField];
    firstNameTextField.text = user.firstName;
    
    lastNameImageView = [[UIImageView alloc]initWithFrame:CGRectMake(34, firstNameImageView.endPointY+12, DeviceWidth-68, 35)];
    lastNameImageView.userInteractionEnabled = YES;
    lastNameImageView.image = [UIImage imageNamed:@"input-box"];
    [bgImageView addSubview:lastNameImageView];
    
    //    UIImageView *nameLogoImage = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth-68-23, 9, 12, 16)];
    //    nameLogoImage.image = [UIImage imageNamed:@"name"];
    [lastNameImageView addSubview:nameLogoImage];
    
    lastNameTextField = [[UITextField alloc]initWithFrame:CGRectMake(10, 0, DeviceWidth-68-25-10, 35)];
    lastNameTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    lastNameTextField.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    lastNameTextField.placeholder = L(@"Last Name");
    [lastNameImageView addSubview:lastNameTextField];
    lastNameTextField.text = user.lastName;
    
#pragma mark birthday
    birthdayImageView = [[UIImageView alloc]initWithFrame:CGRectMake(34, lastNameImageView.endPointY+12, DeviceWidth-68, 35)];
    birthdayImageView.userInteractionEnabled = YES;
    birthdayImageView.image = [UIImage imageNamed:@"input-box"];
    [bgImageView addSubview:birthdayImageView];
    
    birthdayTextField = [[UITextField alloc]initWithFrame:CGRectMake(10, 0, DeviceWidth-68-25-10, 35)];
    birthdayTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    birthdayTextField.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    birthdayTextField.placeholder = L(@"Birthday");
    birthdayTextField.enabled = NO;
    [birthdayImageView addSubview:birthdayTextField];
    birthdayTextField.text = user.birthday;
    
    UITapGestureRecognizer *tapgesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addDatePicker)];
    [birthdayImageView addGestureRecognizer:tapgesture];
    
#pragma mark email
    
    emailImageView = [[UIImageView alloc]initWithFrame:CGRectMake(34, birthdayImageView.endPointY+12, DeviceWidth-68, 35)];
    emailImageView.userInteractionEnabled = YES;
    emailImageView.image = [UIImage imageNamed:@"input-box"];
    [bgImageView addSubview:emailImageView];
    
    UIImageView *emailLogoImage = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth-68-25, 12, 16, 12)];
    emailLogoImage.image = [UIImage imageNamed:@"LoginEmail"];
    [emailImageView addSubview:emailLogoImage];
    
    emailTextField = [[UITextField alloc]initWithFrame:CGRectMake(10, 0, DeviceWidth-68-25-10, 35)];
    emailTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    emailTextField.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    emailTextField.placeholder = L(@"Email");
    [emailImageView addSubview:emailTextField];
    emailTextField.text = user.email;
    
    
    
#pragma mark phone num
    
    
    codeImageView = [[UIImageView alloc]initWithFrame:CGRectMake(34, emailImageView.endPointY+12, 60, 35)];
    codeImageView.userInteractionEnabled = YES;
    codeImageView.image = [UIImage imageNamed:@"input-box"];
    [bgImageView addSubview:codeImageView];
    
    
    codeTextField = [[UITextField alloc]initWithFrame:CGRectMake(10, 0, 50, 35)];
    codeTextField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    codeTextField.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    codeTextField.placeholder = L(@"Code");
    codeTextField.enabled = NO;
    [codeImageView addSubview:codeTextField];
    codeTextField.text = @"+62";

    
    UITapGestureRecognizer *tapgesture2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(addCodeNumberPicker)];
    [codeImageView addGestureRecognizer:tapgesture2];
    
    
    phoneImageView = [[UIImageView alloc]initWithFrame:CGRectMake(codeImageView.endPointX+10, emailImageView.endPointY+12, DeviceWidth-(codeImageView.endPointX+10+34), 35)];
    phoneImageView.userInteractionEnabled = YES;
    phoneImageView.image = [UIImage imageNamed:@"input-box"];
    [bgImageView addSubview:phoneImageView];
    
    UIImageView *phoneLogoImage = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth-(codeImageView.endPointX+10+34)-25, 10, 16, 16)];
    phoneLogoImage.image = [UIImage imageNamed:@"phone"];
    [phoneImageView addSubview:phoneLogoImage];
    
    phoneNumField = [[UITextField alloc]initWithFrame:CGRectMake(10, 0, DeviceWidth-(codeImageView.endPointX+10-34)-25-10, 35)];
    phoneNumField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    phoneNumField.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    phoneNumField.placeholder = L(@"Phone number");
    [phoneImageView addSubview:phoneNumField];
    
    
    
    
    firstNameTextField.returnKeyType = UIReturnKeyDone;
    firstNameTextField.delegate = self;
    
    lastNameTextField.returnKeyType = UIReturnKeyDone;
    lastNameTextField.delegate = self;
    
    emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    emailTextField.returnKeyType = UIReturnKeyDone;
    emailTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    emailTextField.delegate = self;
    
    
    [phoneNumField limitTextLength:13];
    phoneNumField.keyboardType = UIKeyboardTypePhonePad;
    phoneNumField.returnKeyType = UIReturnKeyDone;
    phoneNumField.delegate = self;
    
#pragma mark sign up
    signUpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    if ([UIDevice isIphone5]) {
    //        signUpButton.frame = CGRectMake(34, passwordImageView.endPointY+27.5, DeviceWidth-68, 44);
    //    }
    //    else
    //    {
    //        signUpButton.frame = CGRectMake(34, phoneImageView.endPointY+7.5, DeviceWidth-68, 44);
    //    }
    signUpButton.frame = CGRectMake(34, phoneImageView.endPointY+27.5, DeviceWidth-68, 44);
    
    [signUpButton setBackgroundImage:[UIImage imageNamed:@"signMeUpBtn"] forState:UIControlStateNormal];
    [signUpButton setTitle:L(@"Update") forState:UIControlStateNormal];
    signUpButton.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:15];
    [signUpButton setTitleColor:[UIColor color222222] forState:UIControlStateNormal];
    [signUpButton addTarget:self action:@selector(regButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgImageView addSubview:signUpButton];
    
    bgImageView.contentSize = CGSizeMake(DeviceWidth, signUpButton.endPointY+30);
    
}
#pragma mark textField Delegae
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    CGFloat viewUpHeight;
    if (textField == firstNameTextField){
        viewUpHeight = [UIDevice isLaterThanIphone5]?0:0;
    }else if (textField == lastNameTextField){
        viewUpHeight = [UIDevice isLaterThanIphone5]?0:lastNameImageView.startPointY-130;
    }else if (textField == birthdayTextField){
        viewUpHeight = [UIDevice isLaterThanIphone5]?birthdayImageView.startPointY-130:birthdayImageView.startPointY-130;
    }else if (textField == emailTextField){
        viewUpHeight = [UIDevice isLaterThanIphone5]?emailImageView.startPointY-200:emailImageView.startPointY-130;
    }else if (textField == phoneNumField){
        viewUpHeight = [UIDevice isLaterThanIphone5]?emailImageView.startPointY-200:emailImageView.startPointY-130;
    }
    if(!isViewUp)
    {
        [UIView animateWithDuration:0.3 animations:^{
            bgImageView.contentSize = CGSizeMake(DeviceWidth, loginButton.endPointY+130);
            
            //            self.view.frame = CGRectMake(0, -viewUpHeight, self.view.frame.size.width, self.view.frame.size.height);
            bgImageView.contentOffset = CGPointMake(0, viewUpHeight);
            if (control) {
                control.frame = CGRectMake(0, -viewUpHeight, DeviceWidth, DeviceHeight+viewUpHeight);
            }
        }];
        isViewUp=YES;
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (control != nil) {
        control.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight);
        
        if (textField == firstNameTextField) {
            [firstNameImageView removeFromSuperview];
            firstNameImageView.image = [UIImage imageNamed:@"input-box"];
            [bgImageView addSubview:firstNameImageView];
        }
        if (textField == lastNameTextField) {
            [lastNameImageView removeFromSuperview];
            lastNameImageView.image = [UIImage imageNamed:@"input-box"];
            [bgImageView addSubview:lastNameImageView];
        }
        if (textField == birthdayTextField) {
            [birthdayImageView removeFromSuperview];
            birthdayImageView.image = [UIImage imageNamed:@"input-box"];
            [bgImageView addSubview:birthdayImageView];
        }
        if (textField == emailTextField) {
            [emailImageView removeFromSuperview];
            emailImageView.image = [UIImage imageNamed:@"input-box"];
            [bgImageView addSubview:emailImageView];
        }
        if (textField == phoneNumField) {
            [phoneImageView removeFromSuperview];
            phoneImageView.image = [UIImage imageNamed:@"input-box"];
            [bgImageView addSubview:phoneImageView];
        }
        [textField resignFirstResponder];
        [control removeFromSuperview];
        control = nil;
    }
    [firstNameTextField resignFirstResponder];
    [lastNameTextField resignFirstResponder];
    [emailTextField resignFirstResponder];
    [phoneNumField resignFirstResponder];
    if(isViewUp)
    {
        [UIView animateWithDuration:0.3 animations:^{
            bgImageView.contentSize = CGSizeMake(DeviceWidth, loginButton.endPointY+30);
            //            self.view.frame = CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height);
            bgImageView.contentOffset = CGPointMake(0, 0);
        }];
        isViewUp=NO;
    }
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (control != nil) {
        control.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight);
        
        if (textField == firstNameTextField) {
            [firstNameImageView removeFromSuperview];
            firstNameImageView.image = [UIImage imageNamed:@"input-box"];
            [bgImageView addSubview:firstNameImageView];
        }
        if (textField == lastNameTextField) {
            [lastNameImageView removeFromSuperview];
            lastNameImageView.image = [UIImage imageNamed:@"input-box"];
            [bgImageView addSubview:lastNameImageView];
        }
        if (textField == birthdayTextField) {
            [birthdayImageView removeFromSuperview];
            birthdayImageView.image = [UIImage imageNamed:@"input-box"];
            [bgImageView addSubview:birthdayImageView];
        }
        if (textField == emailTextField) {
            [emailImageView removeFromSuperview];
            emailImageView.image = [UIImage imageNamed:@"input-box"];
            [bgImageView addSubview:emailImageView];
        }
        if (textField == phoneNumField) {
            [phoneImageView removeFromSuperview];
            phoneImageView.image = [UIImage imageNamed:@"input-box"];
            [bgImageView addSubview:phoneImageView];
        }
        [textField becomeFirstResponder];
        [control removeFromSuperview];
        control = nil;
    }
    
    return YES;
}
#pragma mark 错误处理
-(void)errorViewShowWithTextField:(UITextField*)textField andImageView:(UIImageView*)imageView withErrorMessage:(NSString *)message
{
    bgImageView.contentOffset = CGPointMake(0, 0);
    [self backgroundTap:nil];
    
    if (control == nil) {
        control = [[UIControl alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
        [control setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Mask@iphone5"]]];
        [self.view addSubview:control];
    }
    control.frame = CGRectMake(0, -bgImageView.contentOffset.y, DeviceWidth, DeviceHeight+bgImageView.contentOffset.y);
    
    
    [imageView removeFromSuperview];
    [control addSubview:imageView];
    imageView.image = [UIImage imageNamed:@"input-box-selected"];
    oriFrame = imageView.frame;
    
    
    UIImageView *promptView = [[UIImageView alloc]initWithFrame:CGRectMake(0, oriFrame.origin.y-43, DeviceWidth, 39)];
    promptView.image = [UIImage imageNamed:@"Tooltip"];
    [control addSubview:promptView];
    Label *promptLabel = [[Label alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 30) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor whiteColor] andTextLines:1];
    promptLabel.textAlignment = NSTextAlignmentCenter;
    promptLabel.text = message;
    [promptView addSubview:promptLabel];
    
    
    
}

-(void)backgroundTap:(id)sender
{
    [firstNameTextField resignFirstResponder];
    [lastNameTextField resignFirstResponder];
    [emailTextField resignFirstResponder];
    [phoneNumField resignFirstResponder];
    [self hideDatePicker];
    [self hideCodeNumPicker];
    
    if(isViewUp)
    {
        [UIView animateWithDuration:0.3 animations:^{
            bgImageView.contentSize = CGSizeMake(DeviceWidth, loginButton.endPointY+30);
            //            self.view.frame = CGRectMake(0,0, self.view.frame.size.width, self.view.frame.size.height);
            bgImageView.contentOffset = CGPointMake(0, 0);
        }];
        isViewUp=NO;
    }
}

 
-(void)sexSelect:(UIButton *)button{
    button.selected = YES;
    if (button == mrButton) {
        sexSelect = 1;
        msButton.selected = NO;
        mrsButton.selected = NO;
    }else if (button == msButton){
        sexSelect = 2;
        mrButton.selected = NO;
        mrsButton.selected = NO;
    }else{
        sexSelect = 3;
        mrButton.selected = NO;
        msButton.selected = NO;
    }
    
}
-(void)regButtonClick:(id)sender{
    [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(starButtonClicked:) object:sender];
    [self performSelector:@selector(starButtonClicked:) withObject:sender afterDelay:1.0f];
    
}

#pragma mark add picker view
-(void)addCodeNumberPicker{
    [self backgroundTap:nil];
    
    if ([[codeImageView superview] isKindOfClass:[UIControl class]]){
        [codeImageView removeFromSuperview];
        codeImageView.image = [UIImage imageNamed:@"input-box"];
        [bgImageView addSubview:codeImageView];
        [control removeFromSuperview];
        control = nil;
    }
    
    if (codeNumView == nil) {
        codeNumView = [[UIView alloc]initWithFrame:CGRectMake(0, DeviceHeight-290+20, DeviceWidth, 290)];
    }
    codeNumView.backgroundColor = [UIColor whiteColor];
    if (codeNumPicker == nil) {
        codeNumPicker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 40, DeviceWidth, 250)];
    }
    codeNumPicker.delegate = self;
    codeNumPicker.dataSource = self;
    [codeNumPicker selectRow:selectCodeIndex inComponent:1 animated:YES];
    [codeNumView addSubview:codeNumPicker];
    [self.view addSubview:codeNumView];
    
    doneButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
    doneButton2.frame = CGRectMake(DeviceWidth-60, 0, 50, 40);
    [doneButton2 setTitleColor:[UIColor color222222] forState:UIControlStateNormal];
    [doneButton2 setTitle:L(@"Done") forState:UIControlStateNormal];
    [codeNumView addSubview:doneButton2];
    [doneButton2 addTarget:self action:@selector(codeNumDone) forControlEvents:UIControlEventTouchUpInside];
}
-(void)codeNumDone{
    [codeNumPicker removeFromSuperview];
    [codeNumView removeFromSuperview];
    if (selectCode) {
        codeTextField.text = [NSString stringWithFormat:@"+%@",selectCode];
    }else{
        IMGCountryCode *code = [codeArray firstObject];
        codeTextField.text = [NSString stringWithFormat:@"+%@",code.code];
    }
}
-(void)hideCodeNumPicker{
    [codeNumPicker removeFromSuperview];
    [codeNumView removeFromSuperview];
}
#pragma mark picker view delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component == 0) {
        return;
    }
    IMGCountryCode *code = [codeArray objectAtIndex:row];
    selectCode = [NSString stringWithFormat:@"%@",code.code];
    selectCodeIndex = row;
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0) {
        return 1;
    }
    return codeArray.count;
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}
-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    if (component == 0) {
        return 40.0f;
    }
    return 100.0f;
}
#pragma mark picker view dataSource
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (component == 0) {
        return @"+";
    }
    IMGCountryCode *code = [codeArray objectAtIndex:row];
    return [NSString stringWithFormat:@"%@",code.code];
}
-(NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (component == 0) {
        return nil;
    }
    IMGCountryCode *code = [codeArray objectAtIndex:row];
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",code.code]];
    [attriString addAttribute:(NSString *)kCTFontAttributeName
                        value:(id)CFBridgingRelease(CTFontCreateWithName((CFStringRef)[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13].fontName,13,NULL))range:NSMakeRange(0, [NSString stringWithFormat:@"%@",code.code].length-1)];
    return attriString;
}

#pragma mark add date picker
-(void)addDatePicker{
    [self backgroundTap:nil];
    
    if ([[birthdayImageView superview] isKindOfClass:[UIControl class]]){
        [birthdayImageView removeFromSuperview];
        birthdayImageView.image = [UIImage imageNamed:@"input-box"];
        [bgImageView addSubview:birthdayImageView];
        [control removeFromSuperview];
        control = nil;
    }
    
    if (dateView == nil) {
        dateView = [[UIView alloc]initWithFrame:CGRectMake(0, DeviceHeight-290+20, DeviceWidth, 290)];
    }
    dateView.backgroundColor = [UIColor whiteColor];
    if (datePicker == nil) {
        datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 40, DeviceWidth, 250)];
    }
    datePicker.datePickerMode = UIDatePickerModeDate;
    [dateView addSubview:datePicker];
    [self.view addSubview:dateView];
    
    doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    doneButton.frame = CGRectMake(DeviceWidth-60, 0, 50, 40);
    [doneButton setTitleColor:[UIColor color222222] forState:UIControlStateNormal];
    [doneButton setTitle:L(@"Done") forState:UIControlStateNormal];
    [dateView addSubview:doneButton];
    [doneButton addTarget:self action:@selector(datePickerDone) forControlEvents:UIControlEventTouchUpInside];
}
-(void)datePickerDone{
    
    [datePicker removeFromSuperview];
    [dateView removeFromSuperview];
    NSLog(@"%@",datePicker.date);
    NSString *datestr = [datePicker.date get_MM_DD_YYYYFormatString];
    NSLog(@"%@",datestr);
    birthdayTextField.text = datestr;
    
}
-(void)hideDatePicker{
    
    [datePicker removeFromSuperview];
    [dateView removeFromSuperview];
    
}

-(void)starButtonClicked:(id)sender
{
    NSString *completePhone=[NSString stringWithFormat:@"%@-%@",codeTextField.text,phoneNumField.text];
    NSString *trimmedString1 = [firstNameTextField.text stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceCharacterSet]];
    NSString *trimmedString2 = [lastNameTextField.text stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceCharacterSet]];
    
    if(firstNameTextField.text.length==0 || trimmedString1.length==0)
    {
        [self errorViewShowWithTextField:firstNameTextField andImageView:firstNameImageView withErrorMessage:L(@"This field cannot be empty")];
        return ;
    }
    if(lastNameTextField.text.length==0 || trimmedString2.length==0)
    {
        [self errorViewShowWithTextField:lastNameTextField andImageView:lastNameImageView withErrorMessage:L(@"This field cannot be empty")];
        return ;
    }
    
    if(birthdayTextField.text.length==0)
    {
        [self errorViewShowWithTextField:birthdayTextField andImageView:birthdayImageView withErrorMessage:L(@"This field cannot be empty")];
        return ;
    }
    
    if(emailTextField.text.length==0)
    {
        [self errorViewShowWithTextField:emailTextField andImageView:emailImageView withErrorMessage:L(@"This field cannot be empty")];
        return;
    }
    
    if (![emailTextField.text isValidateEmail]) {
        [self errorViewShowWithTextField:emailTextField andImageView:emailImageView withErrorMessage:L(@"The format of the email address is invalid.")];
        return;
    }
    if (phoneNumField.text.length == 0)
    {
        //[self errorViewShowWithTextField:phoneNumField andImageView:phoneImageView withErrorMessage:L(@"This field cannot be empty")];
        //return;
    }
    if (![completePhone isValidateInternationPhone]) {
        //[self errorViewShowWithTextField:phoneNumField andImageView:phoneImageView withErrorMessage:@"Incorrect phone number format."];
        //return ;
    }
    IMGUser *user = [IMGUser currentUser];
    signUpButton.enabled = NO;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setObject:[NSString stringWithFormat:@"%ld",(long)sexSelect] forKey:@"gender"];
    [parameters setObject:firstNameTextField.text forKey:@"first_name"];
    [parameters setObject:lastNameTextField.text forKey:@"last_name"];
    [parameters setObject:birthdayTextField.text forKey:@"birthday"];
    [parameters setObject:emailTextField.text forKey:@"email"];
    [parameters setObject:completePhone forKey:@"mobile_number"];
    [parameters setObject:@"signupwithself" forKey:@"loginwith"];
    [parameters setObject:@"4" forKey:@"productSource"];
    [parameters setObject:@"1" forKey:@"sourceType"];
    [parameters setObject:user.userId forKey:@"userID"];
    [parameters setObject:user.token forKey:@"t"];
    [parameters setObject:birthdayTextField.text forKey:@"birthDate"];
    [parameters setObject:@"www.qraved.com" forKey:@"website"];
    [parameters setObject:@"99" forKey:@"countryId"];
    [parameters setObject:@"1" forKey:@"cityId"];
    [parameters setObject:@"201" forKey:@"districtId"];
    [LoginParamter sharedParameter].loginwith=USER_LOGINTYPE_WITHSELF;
    
    
    UserDataHandler *userDataHandler=[[UserDataHandler alloc]init];
    [userDataHandler updateUserInfo:parameters withBlock:^{
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"loginSuccessful" object:nil];
    } failedBlock:^{
        
    }];
//    [userDataHandler requestLogin:parameters delegateWithBlock:^{
//        signUpButton.enabled = YES;
//        [MixpanelHelper trackSignUpWithTab:@"Email"];
//        [self.delegate signUpSuccess];
//        
//        //跳转到首页
//        [AppDelegate ShareApp].userLogined=YES;
//        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
//        [[AppDelegate ShareApp] requestToShowNotificationCount];
//        
//        [self.navigationController popToRootViewControllerAnimated:YES];
//        [self dismissViewControllerAnimated:YES completion:nil];
//        
//        
//    } failedBlock:^{
//        signUpButton.enabled = YES;
//    }];
    
    
    
}

-(void)loginBurronClick
{
    EmailLoginViewController *emailLoginVC = [[EmailLoginViewController alloc]init];
    [self.navigationController pushViewController:emailLoginVC animated:YES];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
