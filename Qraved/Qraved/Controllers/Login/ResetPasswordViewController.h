//
//  ResertPasswordViewController.h
//  PocketPlan
//
//  Created by Libo Liu on 13-11-22.
//  Copyright (c) 2013年 com.imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "Label.h"
@interface ResetPasswordViewController : BaseChildViewController<UITextFieldDelegate>

- (void)sendButtonClick:(id)sender;
@end
