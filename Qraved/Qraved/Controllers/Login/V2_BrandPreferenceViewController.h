//
//  V2_BrandPreferenceViewController.h
//  Qraved
//
//  Created by harry on 2017/6/9.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "GAITrackedViewController.h"

@interface V2_BrandPreferenceViewController : GAITrackedViewController

@property (nonatomic, strong) NSArray *brandArray;

@property (nonatomic, strong)  NSArray  *brandTitleArray;
@end
