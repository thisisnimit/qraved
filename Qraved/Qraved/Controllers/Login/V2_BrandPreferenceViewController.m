//
//  V2_BrandPreferenceViewController.m
//  Qraved
//
//  Created by harry on 2017/6/9.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_BrandPreferenceViewController.h"
#import "V2_BrandCollectionViewCell.h"
#import "V2_BrandModelSuper.h"
#import "V2_BrandPreferenceTableViewCell.h"
@interface V2_BrandPreferenceViewController ()<UITableViewDelegate, UITableViewDataSource>//<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
     V2_BrandPreferenceTableViewCell *currentCell;
}
@property (nonatomic, strong)  UITableView  *tableView;
@end

@implementation V2_BrandPreferenceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadMainView];

}

- (void)loadMainView{
    self.view.backgroundColor = [UIColor whiteColor];

    
//    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
//    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
//    layout.minimumLineSpacing = 15;
//    layout.minimumInteritemSpacing = 15;
//    
//    brandCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 65, DeviceWidth, self.view.bounds.size.height-65-80) collectionViewLayout:layout];
//    brandCollectionView.backgroundColor = [UIColor whiteColor];
//    brandCollectionView.delegate = self;
//    brandCollectionView.dataSource = self;
//    brandCollectionView.allowsMultipleSelection = YES;
//    
//    [self.view addSubview:brandCollectionView];
    
    
}
- (void)initUI{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight +20 - 64) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource =self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//UIScrollView也适用
    }else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 50)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, DeviceWidth, 20)];
    lblTitle.text = @"And brands that you love..?";
    lblTitle.font = [UIFont boldSystemFontOfSize:17];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.textColor = [UIColor colorWithHexString:@"#333333"];
    [headerView addSubview:lblTitle];
    
    self.tableView.tableHeaderView = headerView;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.brandTitleArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
 V2_BrandModelSuper *superModel = [self.brandTitleArray objectAtIndex:section];
    if (superModel.brandList.count >0) {
        return 1;
    }else{
        return 0;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    V2_BrandModelSuper *superModel = [self.brandTitleArray objectAtIndex:indexPath.section];
    if (superModel.brandList.count >0) {
        return 115;
    }else{
        return 0;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
V2_BrandModelSuper *superModel = [self.brandTitleArray objectAtIndex:section];
    if (superModel.brandList.count > 0) {
        return 30;
    }else{
        return 0.0001;
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    V2_BrandModelSuper *superModel = [self.brandTitleArray objectAtIndex:section];
    if (superModel.brandList.count > 0) {
        UIView *headerview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 30)];
        headerview.backgroundColor = [UIColor whiteColor];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 5, DeviceWidth-30, 20)];
        label.font = [UIFont boldSystemFontOfSize:14];
        V2_BrandModelSuper *model = [self.brandTitleArray objectAtIndex:section];
        label.text = [model.brandType capitalizedString];
        label.textColor = [UIColor colorWithHexString:@"#333333"];
        [headerview addSubview:label];
        return headerview;

    }else{
        return [[UIView alloc] init];
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0001;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [[UIView alloc] init];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
 
    V2_BrandModelSuper *superModel = [self.brandTitleArray objectAtIndex:indexPath.section];
    if (superModel.brandList.count > 0) {
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        V2_BrandPreferenceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            
            cell = [[V2_BrandPreferenceTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            [cell createUI:superModel.brandList brandTitleArray:self.brandTitleArray];
        }
        NSString *CellIdd = [NSString stringWithFormat:@"%ld", (long)[indexPath section]];
        cell.section = CellIdd;//[NSString stringWithFormat:@"%ld",(long)indexPath.section];
        currentCell = cell;
        return cell;
    }else{
        return nil;
    }
    
}

- (void)setBrandArray:(NSArray *)brandArray{
    _brandArray = brandArray;
    NSLog(@"%@",brandArray);
    [self.tableView reloadData];
}
- (void)setBrandTitleArray:(NSArray *)brandTitleArray{

    _brandTitleArray = brandTitleArray;
    
//    for (V2_BrandModelSuper *model in brandTitleArray) {
//        for (int i = 0; i < model.brandList.count; i ++) {
//            V2_BrandModel *BrandModel = [model.brandList objectAtIndex:i];
//            if (i == 0) {
//                BrandModel.state = @"1";
//            }
//        }
//    }

//    NSLog(@"%@",_brandTitleArray);
    [self initUI];
    [self.tableView reloadData];
}


@end
