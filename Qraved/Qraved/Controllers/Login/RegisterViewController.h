//
//  RegisterViewController.h
//  Qraved
//
//  Created by Lucky on 15/5/27.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
//#import <FacebookSDK/FacebookSDK.h>
#import "SHKTwitter.h"
#import "EnterEmailViewController.h"
#import "Label.h"

#import "UserDataHandler.h"
#import "LoginViewController.h"

extern BOOL NeedExpandMenu;

@protocol loginDelegate <NSObject>
@optional
-(void)successfulFunction;

@end

@interface RegisterViewController : GAITrackedViewController<registerDelegate,CLLocationManagerDelegate>
@property(nonatomic,retain) UIButton *fblogupButton;
@property (retain, nonatomic) UIButton *twlogupButton;
@property (retain, nonatomic) UIButton *emaillogupButton;
@property (retain, nonatomic) Label *registerLabel;
@property (retain, nonatomic) UITextField *emailTextField;
@property (retain, nonatomic) UITextField *passwordTextField;
@property (retain, nonatomic) UIButton *logupBtn;
@property (retain, nonatomic) Label *registerBtn;
@property (nonatomic, retain) UIButton *forgetPasswordBtn;
@property (nonatomic,strong)  LoginViewController *loginViewController;
@property (nonatomic, retain)NSDictionary *autoPostDictionary;
- (void)close:(id)sender;

- (void)facebooklogup:(id)sender;
- (void)twitterlogup:(id)sender;

- (void)emaillogup:(id)sender;
//- (void)logupSuccessful;

//-(id)initWithFromWhere:(NSNumber *)_fromWhere;

@property(nonatomic,weak)id<loginDelegate>delegate;
@property(nonatomic,weak)id<ReviewPublishViewDelegate> reviewPublishViewDelegate;
@property(nonatomic,weak)id<AutoPostDelegate> autoPostDelegate;
@end
