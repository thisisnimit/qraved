//
//  V2_DiscoverListResultViewController.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "IMGDiscover.h"
#import "V2_DiscoverListResultFiltersViewController.h"

@interface V2_DiscoverListResultViewController : BaseChildViewController<FiltersViewControllerDelegate>

@property (nonatomic, strong)  NSString  *cityName;
@property (nonatomic, strong)  NSString  *keywordString;
@property (nonatomic, strong)  NSNumber  *componentId;

@property (nonatomic, strong)  NSNumber  *homeSectionComponentId;
@property (nonatomic, assign)  BOOL isHomeNearBy;

@property (nonatomic, strong)  NSString  *placeholderString;

@property (nonatomic, assign)  BOOL isFromHome;

@property (nonatomic, assign) BOOL isLocation;
@property (nonatomic, strong) NSArray *locationArr;

//location
@property (nonatomic, strong)  NSString  *locationID;
@property (nonatomic, strong)  NSString  *isFrom;//1 location  2.cuisines 3 foods 4 restaurants 5 journals
@property (nonatomic, strong)  NSString  *typeString; // landmark  or district
// cuisine
@property (nonatomic, strong)  NSString  *cuisineID;

//food
@property (nonatomic, strong)  NSString  *foodID;

-(void)initDiscover:(IMGDiscover *)tmpDiscover;
-(IMGDiscover *)getDiscover;

@end
