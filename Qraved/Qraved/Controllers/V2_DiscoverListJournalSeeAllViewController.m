//
//  V2_DiscoverListJournalSeeAllViewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/24.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverListJournalSeeAllViewController.h"
#import "V2_DiscoverListHandler.h"
#import "V2_DiscoverListJournalSeeAllTableViewCell.h"
#import "V2_DiscoverListResultRestaurantTableViewCellSubModel.h"
#import "LoadingView.h"
#import "V2_DiscoverListResultFiltersSeeAllListlViewController.h"
#import "JournalHandler.h"
#import "JournalCategoryViewController.h"
#import "V2_DiscoverListResultViewController.h"
#import "LocationListViewController.h"
#import "JournalDetailViewController.h"
#import "DetailViewController.h"
#import "V2_FoodTagSuggestionView.h"
#import "HomeService.h"

@interface V2_DiscoverListJournalSeeAllViewController ()<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate,UISearchDisplayDelegate,JournalCategoryViewControllerDelegate>
{
    NSMutableArray *_categoryDataArrM;
    NSMutableArray *foodTagArray;
    //UIButton *btnFilters;
    V2_FoodTagSuggestionView *foodTagView;
    
    BOOL isUpdateFoodTag;
}

@property (nonatomic, strong)   UISearchBar *locationSearchBar;
@property (nonatomic, strong)  UITableView  *tableView;
@property (nonatomic, strong)  NSMutableArray  *dataArray;
@property (nonatomic, strong)  UITableView  *searchTableView;
@property (nonatomic, weak) UIImageView *imgV;
@end

@implementation V2_DiscoverListJournalSeeAllViewController

- (void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    // [[AppDelegate ShareApp].window addSubview:btnFilters];
    for (UIImageView *imgV in _locationSearchBar.subviews.firstObject.subviews.lastObject.subviews) {
        if ([imgV isMemberOfClass:[UIImageView class]]) {
            imgV.frame = CGRectMake(8, 7.5, 13, 13);
            _imgV = imgV;
            [_imgV addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];
        }
    }

//    [self updateSeachBar];
}
- (void)viewWillDisappear:(BOOL)animated{

    [super viewWillDisappear:animated];
    
    //[btnFilters removeFromSuperview];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addBackBtn];
    self.title = @"Journals";
    UIFont *font = [UIFont boldSystemFontOfSize:16];
    NSDictionary *dic = @{NSFontAttributeName:font};
    self.navigationController.navigationBar.titleTextAttributes =dic;
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self loadData];
    [self initUI];
//    [self addsearchTableView];
    [self reuquestFoodTagSuggestion];
    [self addSearchbar];
    [self initData];
    
    _categoryDataArrM = [[NSMutableArray alloc] init];
    [JournalHandler getJournalCategoryWithBlock:^(NSArray *dataArray) {
        _categoryDataArrM = [NSMutableArray arrayWithArray:dataArray];
    }];
}

- (void)loadData{
    isUpdateFoodTag = YES;
    self.dataArray = [NSMutableArray array];
    foodTagArray = [NSMutableArray array];
}

- (void)reuquestFoodTagSuggestion{
    if (!self.foodTagArr) {
        [HomeService getFoodTag:nil andBlock:^(id locationArr) {
            [foodTagArray addObjectsFromArray:locationArr];
            
            [self.tableView reloadData];
            
        } andError:^(NSError *error) {
            
        }];
    }else{
        [foodTagArray addObjectsFromArray:self.foodTagArr];
        [self.tableView reloadData];
    }

}

- (void)initData{

    NSMutableDictionary *pamars = [[NSMutableDictionary alloc] init];
    
    if (self.quicklySearchFoodTagId != nil) {
        [pamars setValue:self.quicklySearchFoodTagId forKey:@"foodtagId"];
    }
    
    if (self.foodModel != nil) {
        [pamars setValue:self.foodModel.myID forKey:@"foodtagId"];
    }
    if (self.keyword != nil) {
        [pamars setValue:self.keyword forKey:@"keyword"];
    }
     [[LoadingView sharedLoadingView] startLoading];
    [V2_DiscoverListHandler getgetTrendingJournalSeeAll:pamars andSuccessBlock:^(NSMutableArray *array) {
        [[LoadingView sharedLoadingView] stopLoading];
        [self.dataArray removeAllObjects];
        if (array) {
            [self.dataArray addObjectsFromArray:array];
        }
        [self.tableView reloadData];
        
    } anderrorBlock:^{
        
        [[LoadingView sharedLoadingView] stopLoading];
    }];

}

-(void)initUI{

    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, DeviceWidth, DeviceHeight - 50 - 64 +20) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor whiteColor];
}
- (void)addSearchbar{
    
    UIView *headerview= [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 50)];
    headerview.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:headerview];
    [self.view bringSubviewToFront:headerview];
    
    UIButton *button =[UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(15, 5, DeviceWidth - 30 , 40);
    button.backgroundColor = [UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1];
    button.layer.cornerRadius = 3;
    button.layer.masksToBounds = YES;
    [button addTarget:self action:@selector(butonBack:) forControlEvents:UIControlEventTouchUpInside];
    [headerview addSubview:button];
    
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(5, 25/2, 15, 15)];
        imageV.image = [UIImage imageNamed:@"ic_search_medium"];
//    imageV.backgroundColor = [UIColor  greenColor];
    [button addSubview:imageV];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(imageV.right +5, 0, button.width - 25, 40)];
    lblTitle.text = self.keyword;
    lblTitle.textColor = [UIColor color333333];
    lblTitle.font = [UIFont systemFontOfSize:14];
    [button addSubview:lblTitle];

    
    
//    btnFilters = [UIButton buttonWithType:UIButtonTypeCustom];
//    [btnFilters setTitle:@"FILTERS" forState:UIControlStateNormal];
//    [btnFilters setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
//    [btnFilters setImage:[UIImage imageNamed:@"ic_tune_small"] forState:UIControlStateNormal];
//    btnFilters.titleLabel.font = [UIFont boldSystemFontOfSize:14];
//    [btnFilters addTarget:self action:@selector(btnFiltersClick:) forControlEvents:UIControlEventTouchUpInside];
//    btnFilters.backgroundColor = [UIColor whiteColor];
//
////    [self.view addSubview:btnFilters];
////    [self.view bringSubviewToFront:btnFilters];
//    [[AppDelegate ShareApp].window addSubview:btnFilters];
//
//    btnFilters.sd_layout
//    .bottomSpaceToView([AppDelegate ShareApp].window, 20)
//    .centerXEqualToView([AppDelegate ShareApp].window)
//    .heightIs(36)
//    .widthIs(108);
//    btnFilters.layer.borderColor = [UIColor colorLineGray].CGColor;
//    btnFilters.layer.borderWidth = 1;
//    btnFilters.layer.cornerRadius = 18;
//    btnFilters.layer.masksToBounds = YES;
//    [btnFilters setImageEdgeInsets:UIEdgeInsetsMake(0, 75, 0, 0)];
//    [btnFilters setTitleEdgeInsets:UIEdgeInsetsMake(0, -10, 0, 30)];
}
- (void)butonBack:(UIButton *)button{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)btnFiltersClick:(UIButton *)btnFilters{
    
    NSLog(@"FILTERS");
    JournalCategoryViewController *jcvc = [[JournalCategoryViewController alloc] init];
    jcvc.categoryDataArrM = [NSMutableArray arrayWithArray:_categoryDataArrM];
//    jcvc.listViewCategoryId = self.currentCategoryId;
    jcvc.jcvcDelegate = self;
    [self.navigationController pushViewController:jcvc animated:YES];
}

#pragma mark - tableview delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (foodTagArray.count>0) {
        return 48;
    }else{
        return 0.0001;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{

    return 0.0001;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc] init];
    if (isUpdateFoodTag) {
        foodTagView = [[V2_FoodTagSuggestionView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 0)];
        if (foodTagArray.count>0) {
            view.frame = CGRectMake(0, 0, DeviceWidth, 48);
            foodTagView.frame = CGRectMake(0, 0, DeviceWidth, 48);
        }else{
            foodTagView.frame = CGRectMake(0, 0, DeviceWidth, 0);
        }
        foodTagView.foodTagArr = foodTagArray;
        foodTagView.selectModel = self.foodModel;
        
        
        __weak typeof(self) weakSelf = self;
        foodTagView.selectFoodTag = ^(DiscoverCategoriesModel *model, NSInteger index) {
            [weakSelf refreshJournalWithFoodTag:model];
            
        };
        [view addSubview:foodTagView];
    }else{
        [view addSubview:foodTagView];
    }
    
    return view;
}

- (void)refreshJournalWithFoodTag:(DiscoverCategoriesModel *)model{
    if ([self.foodModel.myID isEqual:model.myID] && [self.foodModel.name isEqual:model.name]) {
        self.foodModel = nil;
        
    }else{
        self.foodModel = model;
        foodTagView.selectModel = model;
    }
    isUpdateFoodTag = NO;
    [self initData];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

//    if (tableView == self.tableView) {
        V2_DiscoverListResultRestaurantTableViewCellSubModel *model = self.dataArray[indexPath.row];
        
        return [self.tableView cellHeightForIndexPath:indexPath model:model keyPath:@"model" cellClass:[V2_DiscoverListJournalSeeAllTableViewCell class] contentViewWidth:DeviceWidth];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    //    if (tableView == self.tableView) {
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
    V2_DiscoverListJournalSeeAllTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[V2_DiscoverListJournalSeeAllTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.model = self.dataArray[indexPath.row];
    return cell;

}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    V2_DiscoverListResultRestaurantTableViewCellSubModel * model = self.dataArray[indexPath.row];
    [IMGAmplitudeUtil trackJDPWithName:@"RC - View Journal detail" andJournalId:model.myID andOrigin:@"JLP" andChannel:nil andLocation:nil];
    
    IMGMediaComment *journal = [[IMGMediaComment alloc] init];
    journal.mediaCommentId = model.myID;
    JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
    jdvc.journal = journal;
    [self.navigationController pushViewController:jdvc animated:YES];
}

#pragma mark - near me
- (void)nearMeClick:(UIButton *)nearMe{
    
    NSLog(@"Nearby");
    LocationListViewController *locationListViewController = [[LocationListViewController alloc]initWithoutHead];
    locationListViewController.isFromSearch = YES;
    [self.navigationController pushViewController:locationListViewController animated:YES];
}


#pragma mark -  RecentSearchesTableViewCellDelegate

- (void)delegateSelecteIndexpath:(NSString *)title{
    
    NSLog(@"被点击了%@",title);
    V2_DiscoverListResultViewController *V2_DiscoverListResultVC = [[V2_DiscoverListResultViewController alloc] init];
    V2_DiscoverListResultVC.keywordString = title;
    [self.navigationController pushViewController:V2_DiscoverListResultVC animated:YES];
    
}


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    
    V2_DiscoverListResultViewController *V2_DiscoverListResultVC = [[V2_DiscoverListResultViewController alloc] init];
    V2_DiscoverListResultVC.keywordString = _locationSearchBar.text;
    [self.navigationController pushViewController:V2_DiscoverListResultVC animated:YES];
    
}


//- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
//    
//}


- (BOOL) isEmpty:(NSString *) str {
    
    if (!str) {
        return true;
    } else {
        //A character set containing only the whitespace characters space (U+0020) and tab (U+0009) and the newline and nextline characters (U+000A–U+000D, U+0085).
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        
        //Returns a new string made by removing from both ends of the receiver characters contained in a given character set.
        NSString *trimedString = [str stringByTrimmingCharactersInSet:set];
        
        if ([trimedString length] == 0) {
            return true;
        } else {
            return false;
        }
    }
}

#pragma  makr - 存储搜索内容
- (void)saveData:(NSDictionary *)dict{
    IMGUser * user = [IMGUser currentUser];
    [V2_DiscoverListHandler setRecentKeywords:dict userID:[NSString stringWithFormat:@"%@",user.userId] andSuccessBlock:^(NSMutableArray *array) {
        
        
    } anderrorBlock:^{
        
        
    }];
}


- (void)sendValueWithCategoryId:(NSNumber *)categoryId andCategoryName:(NSString *)categoryName
{

    NSDictionary *dict = @{@"keyword":categoryName};
    [[LoadingView sharedLoadingView] startLoading];
    [V2_DiscoverListHandler getgetTrendingJournalSeeAll:dict andSuccessBlock:^(NSMutableArray *array) {
        [[LoadingView sharedLoadingView] stopLoading];
        [self.dataArray removeAllObjects];
        if (array) {
            [self.dataArray addObjectsFromArray:array];
        }
        [self.tableView reloadData];
        
    } anderrorBlock:^{
        
        [[LoadingView sharedLoadingView] stopLoading];
    }];
    
}



@end
