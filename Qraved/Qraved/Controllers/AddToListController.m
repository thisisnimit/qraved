//
//  AddToListController.m
//  Qraved
//
//  Created by System Administrator on 9/25/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "AddToListController.h"
#import "IMGRestaurant.h"
#import "IMGUser.h"
#import "IMGMyList.h"
#import "AddToListView.h"
#import "ListHandler.h"
#import "UIViewController+Helper.h"
#import "LoadingView.h"
#import "UIConstants.h"
#import "Amplitude.h"
#import "AppDelegate.h"
@implementation AddToListController{
	IMGUser *user;
	NSInteger offset;
	NSInteger max;
	NSMutableArray *newMyList;
}

-(instancetype)initWithRestaurant:(IMGRestaurant*)restaurant{
	if(self=[super init]){
		self.restaurant=restaurant;
		user=[IMGUser currentUser];
		self.addToListBlock=nil;
		self.removeFromListBlock=nil;
		newMyList=[[NSMutableArray alloc] initWithCapacity:1];
	}
	return self;
}

-(void)viewDidLoad{
	offset=0;
    max=10;
    if ([UIDevice isIphone6]) {
        max=12;
    }
    if ([UIDevice isIphone6Plus]) {
        max=13;
    }
	[super viewDidLoad];
	[self setNavigation];
	[self loadData];
    
    // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newListNameChanged:) name:UITextFieldTextDidChangeNotification object:nil];
}


-(void)loadData{
	[[LoadingView sharedLoadingView] startLoading];
	[self loadListData:^(BOOL hasData){
		[self.listView resetRrefreshViewFrame];
		[[LoadingView sharedLoadingView] stopLoading];
	}];
}

-(void)loadListData:(void(^)(BOOL hasData))block{
  self.restaurant.cityId =  [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];

    [ListHandler getListWithUser:user restaurantId:self.restaurant.restaurantId cityId:self.restaurant.cityId offset:offset max:max andBlock:^(NSArray *lists,BOOL hasData){
		offset+=max;
		if(self.listView==nil){
			self.listView=[[AddToListView alloc] initWithFrame:CGRectMake(LEFTLEFTSET,0,DeviceWidth-LEFTLEFTSET*2,DeviceHeight-44) andRestaurant:self.restaurant list:lists];
            self.listView.amplitudeType=self.amplitudeType;
			self.listView.delegate=self;
			self.listView.delegate=self;
            self.listView.startHasData=hasData;
			[self.view addSubview:self.listView];
		}else{
			NSMutableArray *mylists=[[NSMutableArray alloc] initWithArray:self.listView.mylists];
			[mylists addObjectsFromArray:lists];
			self.listView.mylists=mylists;
		}
		if(self.listView.refreshView==nil && hasData){
			[self.listView addRefreshView];
		}
		block(hasData);
	}];
}

-(void)reloadData{
	NSLog(@"%@",@"reloadData");
	self.listView.textField.text=@"";
	for(IMGMyList *mylist in newMyList){
		[self.listView.mylists insertObject:mylist atIndex:0];
	}
	[self.listView.tableView reloadData];
	[self.listView resetRrefreshViewFrame];
}

-(void)setNavigation{

	self.view.backgroundColor=[UIColor whiteColor];
	self.navigationController.navigationBarHidden=NO;
	[self setBackBarButtonOffset30];

    self.navigationItem.title=L(@"Add to List");

    UIBarButtonItem *saveBtn=[[UIBarButtonItem alloc] initWithTitle:L(@"Done") style:UIBarButtonItemStylePlain target:self action:@selector(doneClick)];
    saveBtn.tintColor=[UIColor colorWithRed:17/255.0 green:183/255.0 blue:206/255.0 alpha:1];
    self.navigationItem.rightBarButtonItem=saveBtn;
}

-(void)viewDidDisappear:(BOOL)animated{
	[super viewDidDisappear:animated];
    [[LoadingView sharedLoadingView]stopLoading];
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc]init];
    [param setValue:@"Custom list" forKey:@"Type"];
    [param setValue:self.amplitudeType forKey:@"Location"];
    [param setValue:_restaurant.restaurantId forKey:@"Restaurant_ID"];
    [[Amplitude instance] logEvent:@"BM - Save Restaurant Cancel" withEventProperties:param];

}

-(void)doneClick{
    NSMutableArray *listsArr = [NSMutableArray array];
    for (IMGMyList *list in _listView.mylists) {
        if ([list.inList intValue]==3) {
            [listsArr addObject:list.listId];
        }
    }
    if (listsArr.count>0) {
        [ListHandler listAddRestaurantsWithUser:[IMGUser currentUser] andListIds:listsArr andRestaurantIds:@[_restaurant.restaurantId] andBlock:^(BOOL succeed, id status) {
            [[LoadingView sharedLoadingView] stopLoading];
            
            [self.navigationController popViewControllerAnimated:YES];

            
        }]  ;

    }else{
        
        [self.navigationController popViewControllerAnimated:YES];

    }

    
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event{
	[super touchesBegan:touches withEvent:event];
	for(UITouch *touch in touches){
		if(!self.listView.textField.hidden && ![touch.view isEqual:self.listView.textField]){
			[self.listView.textField resignFirstResponder];
		}
	}
}

-(BOOL)isValidateText:(NSString *)text {
    NSString *regex = @"[a-zA-Z0-9\\s]*";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [test evaluateWithObject:text];
}

-(void)createNewList:(NSString*)listname{
    if (listname.length > 100) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:L(@"Message") message:L(@"Maximum 100 characters for list name.") delegate:nil cancelButtonTitle:L(@"OK") otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
	if(![self isValidateText:listname]){
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:L(@"Message") message:L(@"Sorry, we are not able to process your list") delegate:nil cancelButtonTitle:L(@"OK") otherButtonTitles:nil, nil];
        [alertView show];
        return;
	}
	[[LoadingView sharedLoadingView] startLoading];
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    if ([user.userId isEqualToNumber:(currentUser.userId)]) {
        user.token = currentUser.token;
    }
	[ListHandler addListWithUser:user andListName:listname andCityId:self.restaurant.cityId andBlock:^(NSDictionary *dic){
		NSNumber *mylistId=[dic objectForKey:@"id"];
		if(mylistId){
			IMGMyList *mylist=[[IMGMyList alloc] init];
			mylist.listId=mylistId;
			mylist.listCount=[NSNumber numberWithInt:1];
			mylist.listType=[NSNumber numberWithInt:99];
			mylist.listName=listname;
			mylist.listUserId=user.userId;
			mylist.inList=[NSNumber numberWithBool:YES];
			[self myList:mylistId addRestaurant:self.restaurant.restaurantId block:^(BOOL status,id msg){
                self.restaurant.savedCount = [NSNumber numberWithInt:(self.restaurant.savedCount.intValue+1)];
                self.restaurant.saved = [NSNumber numberWithInt:1];
				if(self.addToListBlock!=nil){
					self.addToListBlock(status);
				}
                if (self.toListBlock!=nil) {
                    self.toListBlock(YES);
                }

				// if(self.listView.refreshView==nil){
				// 	[self.listView addRefreshView];
				// }
				// self.listView.refreshView.hidden=NO;
				// self.listView.hasData=YES;
				
				[newMyList removeAllObjects];
				[newMyList addObject:mylist];
				[self reloadData];
				[[LoadingView sharedLoadingView] stopLoading];

                NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
                [param setObject:self.restaurant.restaurantId forKey:@"Restaurant_ID"];
                [param setValue:self.amplitudeType forKey:@"Location"];
                [[Amplitude instance] logEvent:@"CL - Create New List CTA" withEventProperties:param];
                
			}];
		}
    }failure:^(NSString *exceptionMsg){
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
    }];
}

// -(void)newListNameChanged:(NSNotification*)notification{
// 	UITextField *field=(UITextField*)notification.object;
// 	if(field.text.length){
// 		UIBarButtonItem *saveBtn=[[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(createNewList:)];
// 	    saveBtn.tintColor=[UIColor whiteColor];
// 	    self.navigationItem.rightBarButtonItem=saveBtn;
// 	}else{
// 		self.navigationItem.rightBarButtonItem=nil;
// 	}
// }

-(void)addToList:(NSNumber*)listId{
	[ListHandler listAddRestaurantWithUser:user andListId:listId andRestaurantId:self.restaurant.restaurantId andBlock:^(BOOL succeed,id status){
		if(succeed){
			
		}
	}];
}

-(void)myList:(NSNumber*)listId addRestaurant:(NSNumber*)restaurantId block:(void(^)(BOOL,id))block{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil){
        [[LoadingView sharedLoadingView] stopLoading];
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    if ([user.userId isEqualToNumber:(currentUser.userId)]) {
        user.token = currentUser.token;
    }
	[ListHandler listAddRestaurantWithUser:user andListId:listId andRestaurantId:restaurantId andBlock:^(BOOL succeed,id status){
        if (!succeed) {
            if([status isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
                [[AppDelegate ShareApp] goToLoginController];
                return;
            }
        }
		block(succeed,status);
		if(self.addToListBlock!=nil){
			self.addToListBlock((BOOL)status);

		}
        if (self.toListBlock!=nil) {
            self.toListBlock(YES);
        }

	}];
}

-(void)myList:(NSNumber*)listId removeRestaurant:(NSNumber*)restaurantId block:(void(^)(BOOL))block failure:(void(^)(NSString *exceptionMsg))failureBlock{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil){
        [[LoadingView sharedLoadingView] stopLoading];
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    if ([user.userId isEqualToNumber:(currentUser.userId)]) {
        user.token = currentUser.token;
    }
	[ListHandler listRemoveWithUser:user andListId:listId andRestaurantId:restaurantId andBlock:^{
		block(YES);
		if(self.removeFromListBlock!=nil){
			self.removeFromListBlock(YES);
		}
        if (self.toListBlock!=nil) {
            self.toListBlock(NO);
        }
    } failure:^(NSString *exceptionMsg){
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
        failureBlock(exceptionMsg);
    }];
}

-(void)dealloc{
//	[[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
}

@end
