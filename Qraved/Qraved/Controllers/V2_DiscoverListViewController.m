//
//  V2_DiscoverListViewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/18.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverListViewController.h"
#import "RecentSearchesTableViewCell.h"
#import "V2_DiscoverListHandler.h"
#import "LoadingView.h"
#import "RecentSearchesTableViewCellModel.h"
#import "RecentSearchesSuperModel.h"
#import "V2_DiscoverListResultViewController.h"
#import "V2_DiscoverListResultViewController.h"
#import "LocationListViewController.h"
#import "DetailViewController.h"
#import "JournalDetailViewController.h"
#import "V2_SearchBar.h"
#import "SavedDataHandler.h"
#import "HomeService.h"
#import "DiscoverCategories.h"
#import "v2_DiscoveryCategoriesTableViewCell.h"
#import "V2_DiscoverResultViewController.h"
#import "FoodTagHeaderView.h"
#import "SearchRecentlyViewedCell.h"

#define SEARCHBAR_WIDTH 273
#define SEARCHBAR_HEIGHT 40

@interface V2_DiscoverListViewController ()<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate,UISearchDisplayDelegate,RecentSearchesTableViewCellDelegate>
{
    NSMutableArray * CuisineArray;
    NSMutableArray * FoodtypeArray;
    NSMutableArray * LandmarkAnddistrictArray;
    NSMutableArray * RestaurantArray;
    NSMutableArray * JournalArray;
    
    NSMutableArray *recentlyViewedArray;
    NSMutableArray *discoveryCategoriesArray;
    NSMutableArray *quiklySearchArr;
    
    NSString * searchTextString;
    int offset;
    UIView *navgationView;
}
@property (nonatomic, strong)  UITableView  *tableView;
@property (nonatomic, strong)  UITableView  *searchTableView;
@property (nonatomic, strong)   UISearchBar *locationSearchBar;
//@property (nonatomic, strong)  NSMutableArray  *tredingNowArray;

@property (nonatomic, strong)  UIImageView  *iconImageView;
@property (nonatomic, strong)  UILabel  *lblTitle;
@property (nonatomic, strong)  RecentSearchesSuperModel  *RecentSearchesSupermodel;
@property (nonatomic, strong)  NSMutableArray  *RecentSearchedsArray;
@property (nonatomic, strong)  UISearchBar  *findSearchBar;
@property (nonatomic, strong)  NSMutableArray  *restaurantD;
@property (nonatomic, weak) UIImageView *imgV;


@end

@implementation V2_DiscoverListViewController

- (void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBarHidden = YES;
    
    if (quiklySearchArr.count>0) {
        [self.tableView reloadData];
    }

    [self updateSeachBar];
}
- (void)viewWillDisappear:(BOOL)animated{

    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
   
    [self loadData];
    [self requestData];
    [self createNav];
    [self loadMainUI];
    
    [[Amplitude instance] logEvent:@"SC – Open Search Idle Page" withEventProperties:@{}];
}

- (void)loadData{
    recentlyViewedArray = [NSMutableArray array];
    discoveryCategoriesArray = [NSMutableArray array];
    quiklySearchArr = [NSMutableArray array];
    self.RecentSearchedsArray = [NSMutableArray array];
    CuisineArray = [NSMutableArray array];
    FoodtypeArray = [NSMutableArray array];
    LandmarkAnddistrictArray = [NSMutableArray array];
    RestaurantArray = [NSMutableArray array];
    JournalArray = [NSMutableArray array];
    self.restaurantD = [NSMutableArray array];
    offset = 0;
}

- (void)createNav{
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 0, 0.5, 40);
    leftBtn.backgroundColor = [UIColor clearColor];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem =item;
    
    navgationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, IMG_StatusBarAndNavigationBarHeight+10)];
    navgationView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:navgationView];
    
    UIButton *cancelBtn =[UIButton buttonWithType: UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(DeviceWidth - 80, IMG_StatusBarHeight+3, 70, IMG_NavigationBarHeight+10);
    [cancelBtn setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:[UIColor colortextBlue] forState:UIControlStateNormal];
    cancelBtn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    [cancelBtn addTarget:self action:@selector(cancelBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [navgationView addSubview:cancelBtn];
    [self loadSearchBar];
}

- (void)loadMainUI{
    [self addTableview];
    [self addsearchTableView];
}

- (void)requestData{
    [self getRecentSearchedsData];
    [self getRecentlyViewedData];
    [self getQuicklySearchFoodTag];
    [self getFoodTagList];
}

#pragma mark - request data
- (void)getRecentSearchedsData{

    IMGUser * user = [IMGUser currentUser];
    if (user.userId!=nil) {
        [[LoadingView sharedLoadingView] startLoading];
        [V2_DiscoverListHandler getRecentSearcheds:@{@"userId":user.userId} andSuccessBlock:^(RecentSearchesSuperModel * model ,NSArray *restaurantD) {
            [[LoadingView sharedLoadingView] stopLoading];
            if (model.history.count > 0) {
                
                _RecentSearchesSupermodel = model;
                [self.restaurantD addObjectsFromArray:model.history];
            }
            [self.tableView reloadData];
        } anderrorBlock:^{
            [[LoadingView sharedLoadingView] stopLoading];
            NSLog(@"ssss");
            
        }];
    }
}

- (void)getRecentlyViewedData{
    [HomeService getRecentlyViewedRestaurant:nil andBlock:^(id restaurantArr) {
        
        [recentlyViewedArray addObjectsFromArray:restaurantArr];
        [self.tableView reloadData];
        
    } andError:^(NSError *error) {
        
    }];
}

- (void)getQuicklySearchFoodTag{
    [HomeService getQuicklySearchFoodTag:nil andBlock:^(id foodtagArr) {
        [quiklySearchArr addObjectsFromArray:foodtagArr];
        [self.tableView reloadData];
    } andError:^(NSError *error) {
        
    }];
}

- (void)getFoodTagList{
    [HomeService getCategories:nil andBlock:^(id locationArr) {
        
        NSLog(@"%@",locationArr);
        [discoveryCategoriesArray addObjectsFromArray:locationArr];
        [self.tableView reloadData];
    } andError:^(NSError *error) {
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

#pragma mark  -loadSearchBar
- (void)loadSearchBar{
    _locationSearchBar = [[V2_SearchBar alloc] initWithFrame:CGRectMake(10, IMG_StatusBarHeight, DeviceWidth - 90, 55)];
    _locationSearchBar.delegate = self;
    _locationSearchBar.enablesReturnKeyAutomatically=NO;
    _locationSearchBar.returnKeyType=UIReturnKeySearch;
//    self.navigationItem.titleView = _locationSearchBar;
    [navgationView addSubview:_locationSearchBar];
    _locationSearchBar.showsCancelButton =NO;
    _locationSearchBar.placeholder = L(@"Search restaurants and articles");
    
    [[[_locationSearchBar.subviews objectAtIndex:0].subviews objectAtIndex:1] setTintColor:[UIColor lightGrayColor]];
    UIView *searchTextField = [[[_locationSearchBar.subviews firstObject]subviews]lastObject];
    searchTextField.backgroundColor =[UIColor colorWithRed:229.0/255.0 green:229.0/255.0 blue:229.0/255.0 alpha:1];
    for (UIView *subview in _locationSearchBar.subviews) {
        for(UIView* grandSonView in subview.subviews){
            if ([grandSonView isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                grandSonView.alpha = 0.0f;
            }else if([grandSonView isKindOfClass:NSClassFromString(@"UISearchBarTextField")] ){
            }else{
                grandSonView.alpha = 0.0f;
            }
        }
       
    }
    
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    [_imgV removeObserver:self forKeyPath:@"frame"];
    _imgV.frame = CGRectMake(8, 7.5, 13, 13);
    [_imgV addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];
}
#pragma mark -UISearchBarDelegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    if ([searchBar.text isEqualToString:searchBar.placeholder]) {
        searchBar.text = @"";
    }
    UITextField *searchField = [searchBar valueForKey:@"_searchField"];
    searchField.textColor = [UIColor color333333];
    searchField.clearButtonMode = UITextFieldViewModeWhileEditing;
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self updateSeachBar];
}
#pragma mark -clearButtonMode
- (void)updateSeachBar
{
    if ([_locationSearchBar.text isEqualToString:@""]) {
        UITextField *searchField = [_locationSearchBar valueForKey:@"_searchField"];
        searchField.textColor = [searchField valueForKeyPath:@"_placeholderLabel.textColor"];
        searchField.text = searchField.placeholder;
        searchField.clearButtonMode = UITextFieldViewModeNever;
    }
}
- (void)dealloc
{
    [_imgV removeObserver:self forKeyPath:@"frame"];
}

-(void)cancelClick{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - tableview
- (void)addTableview{

    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, navgationView.bottom, DeviceWidth, DeviceHeight +20 - navgationView.bottom) style:UITableViewStyleGrouped];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.separatorStyle =UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
}

#pragma mark - cancel
- (void)cancelBtnClick:(UIButton *)cancel{

    if (self.isFromHome == YES) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}
#pragma mark - 
- (void)addsearchTableView{

    self.searchTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, navgationView.bottom, DeviceWidth, DeviceHeight +20 - navgationView.bottom) style:UITableViewStylePlain];
    self.searchTableView.delegate = self;
    self.searchTableView.dataSource = self;
    [self.view addSubview:self.searchTableView];
    
    self.searchTableView.hidden = YES;

}
#pragma mark - nearby
- (void)nearMeClick:(UIButton *)nearMe{

    NSLog(@"Nearby");
    LocationListViewController *locationListViewController = [[LocationListViewController alloc]initWithoutHead];
    locationListViewController.isFromSearch = YES;
    [self.navigationController pushViewController:locationListViewController animated:YES];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == self.tableView) {
        
        return discoveryCategoriesArray.count+3;
    }else if (tableView == self.searchTableView){
    
        return 5;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (tableView == self.tableView) {
        return 1;
    }else if (tableView == self.searchTableView){
        if (section == 0) {
            return LandmarkAnddistrictArray.count;
        }else if (section == 1){
            return CuisineArray.count;
        }else if (section == 2){
            return FoodtypeArray.count;
        }else if (section == 3){
            return RestaurantArray.count;
        }else if (section == 4){
            return JournalArray.count;
        }
    }
    return 0;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    if (tableView == self.tableView) {
        if (section == 0) {
            if (self.restaurantD.count >0) {
                return 50;
            }else{
                return 0.0001;
            }
        }else if(section == 1){
            
            return recentlyViewedArray.count?50:0.001;
        }else if (section == 2){
            return 0.001;
        }else{
            return 36;
        }
    }else if (tableView == self.searchTableView){
        if (section == 0) {
            if (LandmarkAnddistrictArray.count >0) {
                return 40;
            }else{
                 return 0.0001;
            }
        }else if (section == 1){
        
            if (CuisineArray.count >0) {
                return 40;
            }else{
                 return 0.0001;
            }
        }else if (section == 2){
            
            if (FoodtypeArray.count >0) {
                return 40;
            }else{
                 return 0.0001;
            }
        }else if (section == 3){
            
            if (RestaurantArray.count >0) {
                return 40;
            }else{
                 return 0.0001;
            }
        }else if (section == 4){
            
            if (JournalArray.count >0) {
                return 40;
            }else{
                 return 0.0001;
            }
        }
    }
    return 0.0001;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.0001;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView == self.tableView) {
        if (section == 0) {
            if (self.restaurantD.count > 0) {
                UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 50)];
                
                UILabel *lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, 7.5, DeviceWidth, 42.5)];
                lblHeader.text = @"Recent Searches";
                lblHeader.textColor = [UIColor color333333];
                lblHeader.font = [UIFont boldSystemFontOfSize:14];
                [headerView addSubview:lblHeader];
                
                return headerView;
            }else{
                return nil;
            }
            
        }else if(section==1){
            if (recentlyViewedArray.count>0) {
                UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 50)];
                
                UILabel *lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, 7.5, DeviceWidth, 42.5)];
                lblHeader.text = @"Recently viewed";
                lblHeader.textColor = [UIColor color333333];
                lblHeader.font = [UIFont boldSystemFontOfSize:14];
                [headerView addSubview:lblHeader];
                
                return headerView;
            }else{
                return [UIView new];
            }
        }else if (section==2){
            return [UIView new];
        }else{
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 36)];
            view.backgroundColor = [UIColor whiteColor];
            UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, DeviceWidth-45, 16)];
            lblTitle.font = [UIFont boldSystemFontOfSize:14];
            lblTitle.textColor = [UIColor color333333];
            
            DiscoverCategories *model = [discoveryCategoriesArray objectAtIndex:section-3];
            lblTitle.text = model.name;
            [view addSubview:lblTitle];
            return view;
        }

    }else if (tableView == self.searchTableView){
    
        if (section == 0) {
            if (LandmarkAnddistrictArray.count >0) {
                UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
                headerView.backgroundColor = [UIColor whiteColor];
                
                UILabel *lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, LEFTLEFTSET, DeviceWidth, 25)];
                lblHeader.text = @"Locations";
                lblHeader.textColor = [UIColor color333333];
                lblHeader.font = [UIFont boldSystemFontOfSize:14];
                [headerView addSubview:lblHeader];
                return headerView;

            }else{
                return nil;
            }
        }
        else if (section == 1) {
            if (CuisineArray.count >0) {
                UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
                headerView.backgroundColor = [UIColor whiteColor];
                
                UILabel *lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, LEFTLEFTSET, DeviceWidth, 25)];
                lblHeader.text = @"Cuisines";
                lblHeader.textColor = [UIColor color333333];
                lblHeader.font = [UIFont boldSystemFontOfSize:14];;
                [headerView addSubview:lblHeader];
                return headerView;
                
            }else{
                return nil;
            }
        }
        else if (section == 2) {
            if (FoodtypeArray.count >0) {
                UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
                headerView.backgroundColor = [UIColor whiteColor];
                
                UILabel *lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, LEFTLEFTSET, DeviceWidth, 25)];
                lblHeader.text = @"Foods";
                lblHeader.textColor = [UIColor color333333];
                lblHeader.font = [UIFont boldSystemFontOfSize:14];
                [headerView addSubview:lblHeader];
                return headerView;
                
            }else{
                return nil;
            }
        }
        else if (section == 3) {
            if (RestaurantArray.count >0) {
                UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
                headerView.backgroundColor = [UIColor whiteColor];
                
                UILabel *lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, LEFTLEFTSET, DeviceWidth, 25)];
                lblHeader.text = @"Restaurants";
                lblHeader.textColor = [UIColor color333333];
                lblHeader.font = [UIFont boldSystemFontOfSize:14];
                [headerView addSubview:lblHeader];
                return headerView;
                
            }else{
                return nil;
            }
        }
        else if (section == 4) {
            if (JournalArray.count >0) {
                UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
                headerView.backgroundColor = [UIColor whiteColor];
                
                UILabel *lblHeader = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, LEFTLEFTSET, DeviceWidth, 25)];
                lblHeader.text = @"Journals";
                lblHeader.textColor = [UIColor color333333];
                lblHeader.font = [UIFont boldSystemFontOfSize:14];
                [headerView addSubview:lblHeader];
                return headerView;
                
            }else{
                return nil;
            }
        }
       
    }
        return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (tableView == self.tableView) {
        if (indexPath.section == 0) {
            if (self.restaurantD.count >0) {
                return 110;
            }else{
                return 0;
            }
        }else if(indexPath.section == 1){
            if (recentlyViewedArray.count>0) {
                return 220;
            }else{
                return 0;
            }
        }else if(indexPath.section == 2){
            int lines = (int)quiklySearchArr.count/4;
            if (quiklySearchArr.count%4 >0) {
                lines = (int)quiklySearchArr.count/4 +1;
            }
            return 100*lines+36;
        }else{
            v2_DiscoveryCategoriesTableViewCell *cell = [[v2_DiscoveryCategoriesTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"heightCell"];
            DiscoverCategories *model = [discoveryCategoriesArray objectAtIndex:indexPath.section-3];
            cell.model = model;
            return cell.pointY;
        }
    }else if (tableView == self.searchTableView){

        if (indexPath.section == 0) {
            if (LandmarkAnddistrictArray.count >0) {
                return 40;
            }else{
                return 0;
            }
        }
        else if (indexPath.section == 1){
            if (CuisineArray.count >0) {
                return 40;
            }else{
                return 0;
            }
        }
        else if (indexPath.section == 2){
            if (FoodtypeArray.count >0) {
                return 40;
            }else{
                return 0;
            }
        }
        else if (indexPath.section == 3){
            if (RestaurantArray.count >0) {
                return 70;
            }else{
                return 0;
            }
        }
        else if (indexPath.section == 4){
            if (JournalArray.count >0) {
                return 70;
            }else{
                return 0;
            }
        }

    }

    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (tableView == self.tableView) {
        if (indexPath.section == 0) {
            NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
            RecentSearchesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (!cell) {
                cell = [[RecentSearchesTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            cell.delegate = self;
            
            if (self.restaurantD.count >0) {
                [cell createUIWithModel:_RecentSearchesSupermodel];
            }
            
            return cell;
            
        }else if(indexPath.section == 1){
            NSString *idenStr = [NSString stringWithFormat:@"restaurantCell%ld%ld", (long)[indexPath section], (long)[indexPath row]];;
            SearchRecentlyViewedCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
            if (!cell) {
                cell = [[SearchRecentlyViewedCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
            }
            __weak typeof(self)weakSelf = self;
            cell.gotoRestaurantDetail = ^(IMGRestaurant *restaurant) {
                DetailViewController *detailViewController = [[DetailViewController alloc] initWithRestaurantId:restaurant.restaurantId];
                [weakSelf.navigationController pushViewController:detailViewController animated:YES];
            };
            __weak typeof(cell)weakCell = cell;
            cell.savedTapped = ^(IMGRestaurant *restaurant) {
                [weakSelf savedRestaurant:restaurant andCell:weakCell];
            };
            if (recentlyViewedArray.count>0) {
                cell.restaurantArray = recentlyViewedArray;
            }

            return cell;
        }else if (indexPath.section ==2){
            NSString *idenStr = @"foodTag";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
            if (!cell) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
            }
            for (UIView *view in cell.contentView.subviews) {
                [view removeFromSuperview];
            }
            if (quiklySearchArr.count>0) {
                FoodTagHeaderView *header = [[FoodTagHeaderView alloc] initWithFrame:CGRectMake(0, 10, DeviceWidth, 90)];
                header.foodTagArr = quiklySearchArr;
                __weak typeof(self) weakSelf = self;
                header.quicklySearchFoodTag = ^(DiscoverCategoriesModel *model) {
                    [weakSelf quicklySearchTapped:model.myID];
                };
                [cell.contentView addSubview:header];
            }
            return cell;
        }else{
            NSString *CellIdentifier = [NSString stringWithFormat:@"foodTagCell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
            v2_DiscoveryCategoriesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (!cell) {
                cell = [[v2_DiscoveryCategoriesTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            }
            DiscoverCategories *model = [discoveryCategoriesArray objectAtIndex:indexPath.section-3];
            if (model.childrens.count>0) {
                cell.model = model;
                __weak typeof(self) weakSelf = self;
                cell.gotoDiscoverResultPage = ^(DiscoverCategoriesModel *model) {
            
                    [IMGAmplitudeUtil trackDiscoverWithName:@"SC - Filter Food Succeed" andFoodName:model.name andLocationName:nil andLocation:@"New SRP"];
                    
                    V2_DiscoverResultViewController *discoverResultPage = [[V2_DiscoverResultViewController alloc] init];
                    discoverResultPage.foodModel = model;
                    [weakSelf.navigationController pushViewController:discoverResultPage animated:YES];
                };
            }
            return cell;

        }

    }
    else if (tableView == self.searchTableView){
    
        if (indexPath.section == 0) {
            
            if (LandmarkAnddistrictArray.count >0) {
                NSDictionary *dict = [LandmarkAnddistrictArray objectAtIndex:indexPath.row];
                NSString *sstring = dict[@"title"];
                NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (!cell) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                    
                }
                [cell removeAllSubviews];
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 12, 20, 20)];
                imageView.image = [UIImage imageNamed:@"ic_location_medium"];
                [cell addSubview:imageView];
                
                UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(imageView.right +15, 0, DeviceWidth - 59, 20)];
                lblTitle.centerY = imageView.centerY;
                lblTitle.text = sstring;
                lblTitle.textColor = [UIColor color333333];
                lblTitle.font = [UIFont systemFontOfSize:14];
                [cell addSubview:lblTitle];
                
                
                NSRange range;
                
                if (range.location != NSNotFound) {
                    
                    range = [sstring rangeOfString:searchTextString];
                    NSLog(@"found at location = %lu, length = %lu",(unsigned long)range.location,(unsigned long)range.length);
                    
                    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:sstring];
                    UIFont *boldFont = [UIFont boldSystemFontOfSize:15];
                    [attrString addAttribute:NSFontAttributeName value:boldFont range:range];
                    lblTitle.attributedText = attrString;
                }
                
                return cell;

            }else{
            
                return nil;
            }
        }
        else if (indexPath.section == 1){
        
            if (CuisineArray.count >0) {
                NSDictionary *dict = [CuisineArray objectAtIndex:indexPath.row];
                NSString *sstring = dict[@"title"];
                NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (!cell) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                   

                }
                [cell removeAllSubviews];
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 12, 20, 20)];
                imageView.image = [UIImage imageNamed:@"ic_fork_medium"];
                [cell addSubview:imageView];
                
                UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(imageView.right +15, 0, DeviceWidth - 59, 20)];
                lblTitle.centerY = imageView.centerY;
                lblTitle.text = sstring;
                lblTitle.textColor = [UIColor color333333];
                lblTitle.font = [UIFont systemFontOfSize:14];
                [cell addSubview:lblTitle];
                
                NSRange range;
                if (range.location != NSNotFound) {
                    range = [sstring rangeOfString:searchTextString];
                    NSLog(@"found at location = %lu, length = %lu",(unsigned long)range.location,(unsigned long)range.length);
                    
                    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:sstring];
                    UIFont *boldFont = [UIFont boldSystemFontOfSize:15];
                    [attrString addAttribute:NSFontAttributeName value:boldFont range:range];
                    lblTitle.attributedText = attrString;
                }
                
                
                return cell;
                
            }else{
                
                return nil;
            }
        }
        else if (indexPath.section == 2){
            
            if (FoodtypeArray.count >0) {
                NSDictionary *dict = [FoodtypeArray objectAtIndex:indexPath.row];
                NSString *sstring = dict[@"title"];
                NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (!cell) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                    
                }
                [cell removeAllSubviews];
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 12, 20, 20)];
                imageView.image = [UIImage imageNamed:@"ic_pan_medium"];
                [cell addSubview:imageView];
                
                UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(imageView.right +15, 0, DeviceWidth - 59, 20)];
                lblTitle.centerY = imageView.centerY;
                lblTitle.text = sstring;
                lblTitle.textColor = [UIColor color333333];
                lblTitle.font = [UIFont systemFontOfSize:14];
                [cell addSubview:lblTitle];
                
                NSRange range;
                if (range.location != NSNotFound) {
                    range = [sstring rangeOfString:searchTextString];
                    NSLog(@"found at location = %lu, length = %lu",(unsigned long)range.location,(unsigned long)range.length);
                    
                    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:sstring];
                    UIFont *boldFont = [UIFont boldSystemFontOfSize:15];
                    [attrString addAttribute:NSFontAttributeName value:boldFont range:range];
                    lblTitle.attributedText = attrString;
                }
                
                
                return cell;
                
            }else{
                
                return nil;
            }
            
        }
        else if (indexPath.section == 3){
            
            if (RestaurantArray.count >0) {
                
                NSDictionary *dict = [RestaurantArray objectAtIndex:indexPath.row];
                NSString *sstring = dict[@"title"];
                NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (!cell) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                }
                [cell removeAllSubviews];
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 50, 50)];
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                imageView.clipsToBounds = YES;
                [imageView sd_setImageWithURL:[NSURL URLWithString:[dict[@"img"] returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
                [cell addSubview:imageView];
                
                UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(imageView.right +15, 10, DeviceWidth - 95, 22)];
                lblTitle.text = sstring;
                lblTitle.textColor = [UIColor color333333];
                lblTitle.font = [UIFont systemFontOfSize:14];
                [cell addSubview:lblTitle];
                
                UILabel *lbldetail = [[UILabel alloc] initWithFrame:CGRectMake(imageView.right +15, lblTitle.bottom, DeviceWidth - 95, 22)];
                lbldetail.text = [NSString stringWithFormat:@"%@ • %@",dict[@"districtName"],dict[@"distance"]];
                lbldetail.textColor = [UIColor color999999];
                lbldetail.font = [UIFont systemFontOfSize:14];
                [cell addSubview:lbldetail];
                
                NSRange range;
                if (range.location != NSNotFound) {
                    range = [sstring rangeOfString:searchTextString];
                    NSLog(@"found at location = %lu, length = %lu",(unsigned long)range.location,(unsigned long)range.length);
                    
                    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:sstring];
                    UIFont *boldFont = [UIFont boldSystemFontOfSize:15];
                    [attrString addAttribute:NSFontAttributeName value:boldFont range:range];
                    lblTitle.attributedText = attrString;
                }
                
                
                return cell;
                
            }else{
                
                return nil;
            }
            
        }

        else if (indexPath.section == 4){
            
            if (JournalArray.count >0) {
                
                NSDictionary *dict = [JournalArray objectAtIndex:indexPath.row];
                NSString *sstring = dict[@"title"];
                NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                if (!cell) {
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                   

                }
                [cell removeAllSubviews];
                UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 50, 50)];
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                imageView.clipsToBounds = YES;
                [imageView sd_setImageWithURL:[NSURL URLWithString:[dict[@"img"] returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
                [cell addSubview:imageView];
                
                UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(imageView.right +15, 10, DeviceWidth - 95, 40)];
                lblTitle.text = sstring;
                lblTitle.textColor = [UIColor color333333];
                lblTitle.font = [UIFont systemFontOfSize:14];
                lblTitle.numberOfLines = 2;
                [cell addSubview:lblTitle];
                
//                UILabel *lbldetail = [[UILabel alloc] initWithFrame:CGRectMake(imageView.right +15, lblTitle.bottom, DeviceWidth - 95, 40)];
//                lbldetail.text = dict[@"districtName"];
//                lbldetail.textColor = [UIColor blackColor];
//                lbldetail.font = [UIFont systemFontOfSize:14];
//                [cell addSubview:lbldetail];
                
                NSRange range;
                if (range.location != NSNotFound) {
                    range = [sstring rangeOfString:searchTextString];
                    NSLog(@"found at location = %lu, length = %lu",(unsigned long)range.location,(unsigned long)range.length);
                    
                    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:sstring];
                    UIFont *boldFont = [UIFont boldSystemFontOfSize:15];
                    [attrString addAttribute:NSFontAttributeName value:boldFont range:range];
                    lblTitle.attributedText = attrString;
                }
                
                return cell;
                
            }else{
                
                return nil;
            }
            
        }


    }
        return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (tableView == self.tableView) {
        if (indexPath.section == 0) {
            NSLog(@"collectionviewcell");
        }else if (indexPath.section == 1){
//            IMGDiscover *discover=[[IMGDiscover alloc]init];
//
//         NSString *keywordstring = [self.tredingNowArray objectAtIndex:indexPath.row];
//            discover.name = keywordstring;
//
//            V2_DiscoverListResultViewController *V2_DiscoverListResultVC = [[V2_DiscoverListResultViewController alloc] init];
//            V2_DiscoverListResultVC.placeholderString = keywordstring;
//            V2_DiscoverListResultVC.cityName = self.cityName;
//            [V2_DiscoverListResultVC initDiscover:discover];
//            [self.navigationController pushViewController:V2_DiscoverListResultVC animated:YES];
//            IMGUser * user = [IMGUser currentUser];
//            NSDictionary *pram = @{@"type":@"keyword",@"title":keywordstring,@"id":@"",@"img":@"", @"userId":user.userId};
//
//             [self saveData:pram];
        }
    }
    else if (tableView == self.searchTableView){
    
        if (indexPath.section == 0) {
            IMGDiscover *discover=[[IMGDiscover alloc]init];
            if (LandmarkAnddistrictArray.count>0) {
                NSDictionary *dict = [LandmarkAnddistrictArray objectAtIndex:indexPath.row];
                NSString *keywordstring = dict[@"title"];
                NSString *typeString = dict[@"type"];
                NSNumber *landmarkID = dict[@"id"];
                discover.name = keywordstring;
                V2_DiscoverListResultViewController *V2_DiscoverListResultVC = [[V2_DiscoverListResultViewController alloc] init];
                //V2_DiscoverListResultVC.placeholderString = keywordstring;
                V2_DiscoverListResultVC.isFrom = @"1";
                V2_DiscoverListResultVC.typeString = typeString;
                V2_DiscoverListResultVC.locationID = [NSString stringWithFormat:@"%@",landmarkID];
                V2_DiscoverListResultVC.cityName = keywordstring;
                [V2_DiscoverListResultVC initDiscover:discover];
                [[Amplitude instance] logEvent:@"CL – Location Search Suggestion" withEventProperties:@{@"Keyword":_locationSearchBar.text,@"Location_ID":[NSString stringWithFormat:@"%@",landmarkID],@"Location_name":keywordstring}];
                [self.navigationController pushViewController:V2_DiscoverListResultVC animated:YES];
                
                NSDictionary *pram = @{@"type":dict[@"type"],@"title":dict[@"title"],@"id":dict[@"id"],@"img":dict[@"img"]};
                [self saveData:pram];
            }
        
        }
        else if (indexPath.section == 1){
            if (CuisineArray.count>0) {
                NSDictionary *dict = [CuisineArray objectAtIndex:indexPath.row];
                NSString *keywordstring = dict[@"title"];
                NSNumber *cuisineID = dict[@"id"];
                
                V2_DiscoverListResultViewController *V2_DiscoverListResultVC = [[V2_DiscoverListResultViewController alloc] init];
                //V2_DiscoverListResultVC.placeholderString = keywordstring;
                V2_DiscoverListResultVC.cityName = self.cityName;
                V2_DiscoverListResultVC.cuisineID = [NSString stringWithFormat:@"%@",cuisineID];
                V2_DiscoverListResultVC.isFrom = @"2";
                [[Amplitude instance] logEvent:@"CL – Cuisine Search Suggestion" withEventProperties:@{@"Keyword":_locationSearchBar.text,@"Cuisine_ID":[NSString stringWithFormat:@"%@",cuisineID],@"Cuisine_name":keywordstring}];
                [self.navigationController pushViewController:V2_DiscoverListResultVC animated:YES];
                
                NSDictionary *pram = @{@"type":dict[@"type"],@"title":dict[@"title"],@"id":dict[@"id"],@"img":dict[@"img"]};
                [self saveData:pram];
            }
            
        }
        else if (indexPath.section == 2){
            
            if (FoodtypeArray.count>0) {
                NSDictionary *dict = [FoodtypeArray objectAtIndex:indexPath.row];
                NSString *keywordstring = dict[@"title"];
                NSNumber *foodID = dict[@"id"];
                
                V2_DiscoverListResultViewController *V2_DiscoverListResultVC = [[V2_DiscoverListResultViewController alloc] init];
                //V2_DiscoverListResultVC.placeholderString = keywordstring;
                V2_DiscoverListResultVC.cityName = self.cityName;
                V2_DiscoverListResultVC.isFrom = @"3";
                V2_DiscoverListResultVC.foodID = [NSString stringWithFormat:@"%@",foodID];
                [self.navigationController pushViewController:V2_DiscoverListResultVC animated:YES];
                NSDictionary *pram = @{@"type":dict[@"type"],@"title":dict[@"title"],@"id":dict[@"id"],@"img":dict[@"img"]};
                [self saveData:pram];
            }
            
        }
        else if (indexPath.section == 3){
            if (RestaurantArray.count>0) {
                NSDictionary *dict = [RestaurantArray objectAtIndex:indexPath.row];
                NSString *keywordstring = dict[@"title"];
                NSString *typee = dict[@"type"];
                NSNumber *restaurantID = dict[@"id"];
                
                [[Amplitude instance] logEvent:@"CL – Restaurant Search Suggestion" withEventProperties:@{@"Keyword":_locationSearchBar.text,@"Restaurant_ID":[NSString stringWithFormat:@"%@",restaurantID],@"Restaurant_name":keywordstring}];
                if ([typee isEqualToString:@"restaurant"]){
                    
                    IMGRestaurant *model = [[IMGRestaurant alloc] init];
                    model.restaurantId = restaurantID;
                    
                    DetailViewController *DetailVC = [[DetailViewController alloc] initWithRestaurant:model];
                    [self.navigationController pushViewController:DetailVC animated:YES];
                    
                    NSDictionary *pram = @{@"type":dict[@"type"],@"title":dict[@"title"],@"id":dict[@"id"],@"img":dict[@"img"]};
                    [self saveData:pram];
                }else{
                    V2_DiscoverListResultViewController *V2_DiscoverListResultVC = [[V2_DiscoverListResultViewController alloc] init];
                    V2_DiscoverListResultVC.keywordString = keywordstring;
                    V2_DiscoverListResultVC.cityName = self.cityName;
                    [self.navigationController pushViewController:V2_DiscoverListResultVC animated:YES];
                }
            }
            
        }
        else if (indexPath.section == 4){
            if (JournalArray.count>0) {
                NSDictionary *dict = [JournalArray objectAtIndex:indexPath.row];
                NSString *keywordstring = dict[@"title"];
                NSString *typeString = dict[@"type"];
                NSNumber *journalID = dict[@"id"];
                
                [[Amplitude instance] logEvent:@"CL – Journal Search Suggestion" withEventProperties:@{@"Keyword":_locationSearchBar.text,@"Journal_ID":[NSString stringWithFormat:@"%@",journalID],@"Journal_title":keywordstring}];
                
                if ([typeString isEqualToString:@"journal"]) {
                    IMGMediaComment * comment = [[IMGMediaComment alloc] init];
                    comment.mediaCommentId = journalID;
                    
                    JournalDetailViewController *JournalDetailVC = [[JournalDetailViewController alloc] init];
                    JournalDetailVC.journal = comment;
                    [self.navigationController pushViewController:JournalDetailVC animated:YES];
                }else{
                    
                    
                    V2_DiscoverListResultViewController *V2_DiscoverListResultVC = [[V2_DiscoverListResultViewController alloc] init];
                    V2_DiscoverListResultVC.keywordString = keywordstring;
                    V2_DiscoverListResultVC.cityName = self.cityName;
                    [self.navigationController pushViewController:V2_DiscoverListResultVC animated:YES];
                }
            }
        }
    }
}

#pragma mark - search bar
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length>0) {
        searchTextString = searchText;
        self.tableView.hidden=YES;
        self.searchTableView.hidden = NO;
    }else{
        self.tableView.hidden=NO;
        self.searchTableView.hidden = YES;
        
    }
    
    [self searchWithSearchText:searchText];
}
- (void)searchWithSearchText:(NSString *)searchText
{
     NSDictionary *parameters = @{@"q":searchText,@"cityId":[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID],@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]};//,@"max":@"10",@"offset":[NSString stringWithFormat:@"%d",offset]
    
    
    [V2_DiscoverListHandler getSearchSuggestion:parameters andSuccessBlock:^(NSMutableArray *cuisineArray, NSMutableArray *foodtypeArray, NSMutableArray *landmarkAnddistrictArray, NSMutableArray *restaurantArray, NSMutableArray *journalArray) {
        [LandmarkAnddistrictArray removeAllObjects];
        [CuisineArray removeAllObjects];
        [FoodtypeArray removeAllObjects];
        [RestaurantArray removeAllObjects];
        [JournalArray removeAllObjects];
        
        if ([landmarkAnddistrictArray isKindOfClass:[NSMutableArray class]]) {
            [LandmarkAnddistrictArray addObjectsFromArray:landmarkAnddistrictArray];
            /* distance districtName id img isOnTop title type */
        }
        if ([cuisineArray isKindOfClass:[NSMutableArray class]]) {
            [CuisineArray addObjectsFromArray:cuisineArray];
        }
        if ([foodtypeArray isKindOfClass:[NSMutableArray class]]) {
            [FoodtypeArray addObjectsFromArray:foodtypeArray];
            /* distance districtName id img isOnTop title type */
        }
        
        if ([restaurantArray isKindOfClass:[NSMutableArray class]]) {
            [RestaurantArray addObjectsFromArray:restaurantArray];
            /* distance districtName id img isOnTop title type */
        }
        if ([journalArray isKindOfClass:[NSMutableArray class]]) {
            [JournalArray addObjectsFromArray:journalArray];
            /* distance districtName id img isOnTop title type */
        }
        
        [self.searchTableView reloadData];

    } anderrorBlock:^{

    }];
}

#pragma mark - restaurant saved
- (void)savedRestaurant:(IMGRestaurant *)restaurant andCell:(SearchRecentlyViewedCell *)cell{
    [CommonMethod doSaveWithRestaurant:restaurant andViewController:self andCallBackBlock:^(BOOL isSaved) {
        [cell freshSavedButton:isSaved];
    }];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{

    [_locationSearchBar resignFirstResponder];
}

#pragma mark -  RecentSearchesTableViewCellDelegate

- (void)recentSearchTapped:(RecentSearchesTableViewCellModel *)model{
    if ([model.type isEqualToString:@"restaurant"]) {
        IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
        restaurant.restaurantId = model.myID;
//        NSLog(@"%@",model.restaurantId);
        
        DetailViewController *detail = [[DetailViewController alloc] initWithRestaurant:restaurant];
        [self.navigationController pushViewController:detail animated:YES];
    }else if ([model.type isEqualToString:@"keyword"]){
        V2_DiscoverListResultViewController *searchResultVC = [[V2_DiscoverListResultViewController alloc] init];
        searchResultVC.placeholderString = model.title;
        searchResultVC.cityName = self.cityName;
        [self.navigationController pushViewController:searchResultVC animated:YES];
    }else if ([model.type isEqualToString:@"district"] || [model.type isEqualToString:@"landmark"]){
        V2_DiscoverListResultViewController *searchResultVC = [[V2_DiscoverListResultViewController alloc] init];
        searchResultVC.isFrom = @"1";
        searchResultVC.typeString = model.type;
        searchResultVC.locationID = [NSString stringWithFormat:@"%@",model.myID];
        searchResultVC.cityName = model.title;
        [self.navigationController pushViewController:searchResultVC animated:YES];
    }else if ([model.type isEqualToString:@"cuisine"]){
        V2_DiscoverListResultViewController *searchResultVC = [[V2_DiscoverListResultViewController alloc] init];
        searchResultVC.isFrom = @"2";
        searchResultVC.cuisineID = [NSString stringWithFormat:@"%@",model.myID];
        searchResultVC.cityName = self.cityName;
        [self.navigationController pushViewController:searchResultVC animated:YES];
    }else if ([model.type isEqualToString:@"foodtype"]){
        V2_DiscoverListResultViewController *searchResultVC = [[V2_DiscoverListResultViewController alloc] init];
        searchResultVC.isFrom = @"3";
        searchResultVC.foodID = [NSString stringWithFormat:@"%@",model.myID];
        searchResultVC.cityName = self.cityName;
        [self.navigationController pushViewController:searchResultVC animated:YES];
    }else{
        V2_DiscoverListResultViewController *searchResultVC = [[V2_DiscoverListResultViewController alloc] init];
        searchResultVC.cityName = self.cityName;
        [self.navigationController pushViewController:searchResultVC animated:YES];
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];

    V2_DiscoverListResultViewController *searchResultViewController = [[V2_DiscoverListResultViewController alloc] init];
    NSString *searchStr;
    if ([_locationSearchBar.text isEqualToString:@"Search restaurants and articles"]) {
         searchStr = @"";
    }else{
         searchStr = _locationSearchBar.text;
    }
    searchResultViewController.keywordString = searchStr;
   
    searchResultViewController.cityName = self.cityName;
    [self.navigationController pushViewController:searchResultViewController animated:YES];
//    IMGUser * user = [IMGUser currentUser];
    NSDictionary *pram = @{@"type":@"keyword",@"title":searchStr,@"id":@"",@"img":@""};
    [[Amplitude instance] logEvent:@"SC – Search Restaurant" withEventProperties:@{@"Keyword ":_locationSearchBar.text}];
    if (![searchStr isEqualToString:@""]) {
        [self saveData:pram];
    }
}


//- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
//   
//}


- (BOOL) isEmpty:(NSString *) str {
    
    if (!str) {
        return true;
    } else {
        //A character set containing only the whitespace characters space (U+0020) and tab (U+0009) and the newline and nextline characters (U+000A–U+000D, U+0085).
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        
        //Returns a new string made by removing from both ends of the receiver characters contained in a given character set.
        NSString *trimedString = [str stringByTrimmingCharactersInSet:set];
        
        if ([trimedString length] == 0) {
            return true;
        } else {
            return false;
        }
    }
}

#pragma  makr - 存储搜索内容
- (void)saveData:(NSDictionary *)dict{
    
    [V2_DiscoverListHandler setRecentKeywords:dict userID:@"" andSuccessBlock:^(NSMutableArray *array) {
        
        
    } anderrorBlock:^{
        
        
    }];
}

-(NSString *)convertToJsonData:(NSDictionary *)dict

{
    
    NSError *error;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:&error];
    
    NSString *jsonString;
    
    if (!jsonData) {
        
        NSLog(@"%@",error);
        
    }else{
        
        jsonString = [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
        
    }
    
    NSMutableString *mutStr = [NSMutableString stringWithString:jsonString];
    
    NSRange range = {0,jsonString.length};
    
    //去掉字符串中的空格
    
    [mutStr replaceOccurrencesOfString:@" " withString:@"" options:NSLiteralSearch range:range];
    
    NSRange range2 = {0,mutStr.length};
    
    //去掉字符串中的换行符
    
    [mutStr replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:range2];
    
    return mutStr;
    
}

- (void)quicklySearchTapped:(NSNumber *)foodTagId{
    V2_DiscoverResultViewController *discoverResultPage = [[V2_DiscoverResultViewController alloc] init];
    if (![foodTagId isEqual:@0]) {
        discoverResultPage.quicklySearchFoodTagId = foodTagId;
    }
    [self.navigationController pushViewController:discoverResultPage animated:YES];
}




@end
