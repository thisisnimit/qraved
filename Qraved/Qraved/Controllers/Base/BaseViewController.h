//
//  BaseViewController.h
//  quaved_ios
//
//  Created by Shine Wang on 5/30/13.
//  Copyright (c) 2013 Shine Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "GAITrackedViewController.h"
#import <GoogleAnalytics/GAITrackedViewController.h>
#import "Amplitude.h"

@interface BaseViewController : GAITrackedViewController

//-(void)setBackBar;
-(void)loadRevealController;
-(void)addBackBtn;
-(void)addSearchBtn;
-(void)searchButtonTapped:(id)sender;
-(void)sendManualScreenName:(NSString*)screenName;

@end
