//
//  BaseDetailController.h
//  Qraved
//
//  Created by Shine Wang on 8/27/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BaseAnimationController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    CGFloat contentOffsetY;
}

@property(nonatomic,retain)UITableView *tableView;

-(void)setNavBarShow;
-(void)setNavBarHidden;
@end
