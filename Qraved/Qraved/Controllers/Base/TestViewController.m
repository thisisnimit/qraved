//
//  TestViewController.m
//  Qraved
//
//  Created by apple on 16/10/19.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "TestViewController.h"
#import "CarbonKit.h"
#import "HomeViewController.h"
@interface TestViewController ()<CarbonTabSwipeNavigationDelegate>

@end

@implementation TestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSArray *items = @[[UIImage imageNamed:@"home"], [UIImage imageNamed:@"hourglass"],
                       [UIImage imageNamed:@"premium_badge"], @"Categories", @"Top Free",
                       @"Top New Free", @"Top Paid", @"Top New Paid"];
    
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation =
    [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self];
}
- (UIViewController *)carbonTabSwipeNavigation:(CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                         viewControllerAtIndex:(NSUInteger)index {
   
    return [[HomeViewController alloc] init];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
