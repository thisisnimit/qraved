//
//  BaseViewController.m
//  quaved_ios
//
//  Created by Shine Wang on 5/30/13.
//  Copyright (c) 2013 Shine Wang. All rights reserved.
//

#import "BaseViewController.h"
#import "AppDelegate.h"
#import "DiscoverListViewController.h"
#import "UIImage+Resize.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "UIDevice+Util.h"
#import "UILabel+Helper.h"
#import "NSString+Helper.h"
@interface BaseViewController ()

@end

@implementation BaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}
- (UIStatusBarStyle)preferredStatusBarStyle {

    return UIStatusBarStyleLightContent;
}
- (BOOL)prefersStatusBarHidden
{

    return NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if( floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        self.edgesForExtendedLayout= UIRectEdgeNone;
        [self setNeedsStatusBarAppearanceUpdate];
        self.navigationController.navigationBar.translucent = NO;
        [self.navigationController.navigationBar setTitleTextAttributes:
         
  @{NSFontAttributeName:[UIFont boldSystemFontOfSize:17],
    
    NSForegroundColorAttributeName:[UIColor color333333]}];
    }
    self.view.backgroundColor = [UIColor color333333];
    self.screenName = [NSString stringWithUTF8String:object_getClassName(self)];
    NSLog(@"self.class.name = %@",[NSString stringWithUTF8String:object_getClassName(self)]);
}

-(void)loadRevealController
{
    UIBarButtonItem *backBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:NavigationMenuImage] style:UIBarButtonItemStylePlain target:self action:@selector(presentLeftMenuViewController:)];
    backBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.leftBarButtonItem=backBtn;
}

-(void)addBackBtn{
    UIBarButtonItem *leftBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_back"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    leftBtn.tintColor=[UIColor blackColor];
    self.navigationItem.leftBarButtonItem=leftBtn;
}

-(void)addSearchBtn{
    UIBarButtonItem *searchBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search"] style:UIBarButtonItemStylePlain target:self action:@selector(searchButtonTapped:)];
    searchBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=searchBtn;
}

-(void)searchButtonTapped:(id)sender {
    DiscoverListViewController *discoverListViewController = [[DiscoverListViewController alloc] initWithCloseButton];
    [self.navigationController pushViewController:discoverListViewController animated:YES];
}

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
   
}

 

- (void)sendManualScreenName:(NSString*)screenName
{
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:screenName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}


@end
