//
//  IMGBasicViewController.h
//  Qraved
//
//  Created by Libo.liu on 13-9-25.
//  Copyright (c) 2013年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface IMGBasicViewController : GAITrackedViewController

@end
