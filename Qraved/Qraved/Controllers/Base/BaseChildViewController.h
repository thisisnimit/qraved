//
//  BaseChildViewController.h
//  Qraved
//
//  Created by Shine Wang on 8/23/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "GAITrackedViewController.h"
#import <GoogleAnalytics/GAITrackedViewController.h>


@interface BaseChildViewController : GAITrackedViewController
{
    @protected
        BOOL showCloseButton;
}
@property (nonatomic,assign) BOOL showCloseButton;
@property (nonatomic,assign) BOOL isJournalSearch;
- (id)initWithCloseButton;
-(void)loadRevealController;
-(void)addBackBtn;
-(void)addSearchBtn;
-(void)addShareBtn;

@end
