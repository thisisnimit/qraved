//
//  BaseDetailController.m
//  Qraved
//
//  Created by Shine Wang on 8/27/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "BaseAnimationController.h"
#import "UIViewController+Helper.h"
#define CONTENT_OFFSET 5
#define BANNER_HEIGHT 80
#define NAV_HEIGHT 35
@interface BaseAnimationController ()
@end

@implementation BaseAnimationController
@synthesize tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    if( floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
        self.edgesForExtendedLayout= UIRectEdgeNone;
        self.navigationController.navigationBar.translucent = NO;
    }
    tableView=[[UITableView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth,SUBVIEWHEIGHT)];
    tableView.dataSource=self;
    tableView.delegate=self;
    [self.view addSubview:tableView];
}

 

-(void)setNavBarHidden
{
    self.navigationController.navigationBarHidden=YES;
}

-(void)setNavBarShow
{
    self.navigationController.navigationBarHidden=NO;
}

#pragma mark scrollViewDelegate
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    contentOffsetY = scrollView.contentOffset.y;
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView {

}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
    
   
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}
@end
