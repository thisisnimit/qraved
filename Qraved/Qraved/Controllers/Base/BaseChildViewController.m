//
//  BaseChildViewController.m
//  Qraved
//
//  Created by Shine Wang on 8/23/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "AppDelegate.h"
#import "UIViewController+Helper.h"
#import "DiscoverListViewController.h"
#import "LoadingView.h"

@interface BaseChildViewController ()

@end

@implementation BaseChildViewController

@synthesize showCloseButton=showCloseButton;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (id)initWithCloseButton
{
    self = [super init];
    if (self) {
        showCloseButton = TRUE;
    }
    return self;

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];

    if(showCloseButton){
        [self setCloseBarButton];
        [self.navigationController.navigationItem setHidesBackButton:YES];
    }
}

-(void)loadRevealController
{
    UIBarButtonItem *leftBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:NavigationBackImage] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    leftBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.leftBarButtonItem=leftBtn;
    
    UIBarButtonItem *searchBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search"] style:UIBarButtonItemStylePlain target:self action:@selector(searchButtonTapped:)];
    searchBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=searchBtn;
}

-(void)addBackBtn{
    UIBarButtonItem *leftBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:NavigationBackImage] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    leftBtn.tintColor=[UIColor blackColor];
    self.navigationItem.leftBarButtonItem=leftBtn;
}

-(void)addSearchBtn{
    UIBarButtonItem *searchBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search"] style:UIBarButtonItemStylePlain target:self action:@selector(searchButtonTapped:)];
    searchBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=searchBtn;
}

-(void)addShareBtn{
    UIBarButtonItem *searchBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:NavigationShareImage] style:UIBarButtonItemStylePlain target:self action:@selector(shareButtonClick)];
    searchBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=searchBtn;
}

-(void)shareButtonClick{
    DiscoverListViewController *discoverListViewController = [[DiscoverListViewController alloc] initWithCloseButton];
    [self.navigationController pushViewController:discoverListViewController animated:YES];
}

-(void)searchButtonTapped:(id)sender{
    DiscoverListViewController *discoverListViewController = [[DiscoverListViewController alloc] initWithCloseButton];
    [self.navigationController pushViewController:discoverListViewController animated:YES];
}

-(void)back{
    [[LoadingView sharedLoadingView] stopLoading];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setIos7Layout];
    
}

 

@end
