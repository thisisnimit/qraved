//
//  V2_DiscoverListResultFiltersViewController.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "IMGDiscover.h"

typedef NS_ENUM(NSInteger, FilterType)
{
    PageTypeNone = 0,
    FilterTypePromo = 10,
    FilterTypeOpenNow = 11,
    FilterTypeSaved = 12
};


@protocol FiltersViewControllerDelegate <NSObject>

- (void)search:(NSNotification *)notification;

@end


@interface V2_DiscoverListResultFiltersViewController : BaseChildViewController

-(id)initWithDiscover:(IMGDiscover *)paramDiscover;

@property (nonatomic,assign) id<FiltersViewControllerDelegate>fvcDelegate;
@end
