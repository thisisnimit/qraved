//
//  MyListDelegate.h
//  Qraved
//
//  Created by root on 9/26/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "IMGRestaurant.h"

@protocol MyListDelegate

@optional

-(void)createNewList:(NSString*)listname;

-(void)addToList:(NSNumber*)listId;

-(void)goToAddRestaurant:(NSString *)title;

-(void)loadListData:(void(^)(BOOL hasData))block;

-(void)loadRestaurantData:(NSString*)searchText block:(void(^)(BOOL))block;

-(void)loadMyListRestaurants:(void(^)(BOOL))block;

-(void)moreBtnClick:(IMGRestaurant*)restaurant;

-(void)goToRestaurant:(IMGRestaurant*)restaurant;

-(void)myList:(NSNumber*)listId addRestaurant:(NSNumber*)restaurantId block:(void(^)(BOOL succeed,id status))block;
-(void)myList:(NSNumber*)listId removeRestaurant:(NSNumber*)restaurantId block:(void(^)(BOOL succeed))block failure:(void(^)(NSString *exceptionMsg))failureBlock;


-(void)writeNote:(NSString*)note withRestaurant:(IMGRestaurant*)restaurant atIndexPath:(NSIndexPath*)indexPath;

@end
