//
//  MyListRestaurantsViewController.h
//  Qraved
//
//  Created by System Administrator on 10/14/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "MyListDelegate.h"
#import "IMGUser.h"

@class IMGMyList;
@protocol MyListRestaurantsViewControllerDelegate <NSObject>

-(void)reloadForNewList;

@end

@interface MyListRestaurantsViewController : BaseChildViewController<MyListDelegate>

@property(nonatomic,strong) IMGMyList *mylist;
@property(nonatomic,assign) BOOL isOtherUser;
@property(nonatomic,retain) IMGUser *otherUser;
@property(nonatomic,assign) BOOL isFromMenu;
@property(nonatomic,assign) BOOL isFromAdd;
@property(nonatomic,assign) BOOL noRestaurant;
@property(nonatomic,weak)id<MyListRestaurantsViewControllerDelegate>delegate;
-(instancetype)initWithMyList:(IMGMyList*)mylist;

@end
