//
//  InstagramPlacesViewController.h
//  Qraved
//
//  Created by Adam.zhang on 2017/8/16.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"

@interface InstagramPlacesViewController : BaseViewController

@property (nonatomic, strong)  NSString  *instagramLocationId;
@end
