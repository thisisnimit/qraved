//
//  IMGReservationContentController.m
//  Qraved
//
//  Created by Libo Liu on 10/16/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "BookingDetailContentController.h"
#import <CoreText/CoreText.h>
#import "UIConstants.h"
#import "AppDelegate.h"
#import "MapViewController.h"
#import "UIViewController+Helper.h"
#import "NSDate+Helper.h"
#import "UILabel+Helper.h"
#import "NSString+Helper.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
//#import "MixpanelHelper.h"
#import "CustomIOS7AlertView.h"
#import "IMGUser.h"
#import "SendCall.h"
#import "LoadingView.h"
#import "IMGRestaurant.h"
#import "DBManager.h"
#import "BookUtil.h"
#import "ShareToPathView.h"
#import "NXOAuth2AccountStore.h"
#import "TrackHandler.h"


@interface BookingDetailContentController ()
{
    UILabel *statusLabel;
    UIButton *cancelButton;
    UIScrollView *bookInfoView;
    float navigationHeight ;
    BOOL isFromMenu;
}
@end

@implementation BookingDetailContentController
{
    IMGReservation *reservation;
    IMGRestaurant *_restaurant;
}
- (id)initWithFromSideMenu:(BOOL)isFromSlideMenu andReservation:(IMGReservation *)resv
{
    self = [super init];
    if (self) {
        isFromMenu = isFromSlideMenu;
        reservation=resv;
    }
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

-(id)initWithReservation:(IMGReservation *)tmpReservation{
    self = [super init];
    if(self){
        reservation = tmpReservation;
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = YES;
    self.screenName=@"Booking";
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[LoadingView sharedLoadingView] stopLoading];
}
-(void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
    if (self.isFromPushNotification) {
        self.navigationController.navigationBarHidden=YES;

    }
  

}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"My Reservation detail page";
    navigationHeight = 44+[UIDevice heightDifference];
    
    
    self.view.backgroundColor = [UIColor defaultColor];
    [self setIos7Layout];
    [self addMenuButton];

    if (self.isFromNotification)
    {
        IMGUser *user = [IMGUser currentUser];
        NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",reservation.reservationId,@"reservationId", nil];
        [BookUtil getReservationInfoWithparameters:dic andBlock:^(IMGReservation *reservationT) {
            reservation = reservationT;
            
            [self loadMainView];

        }];
    }
    else
    {
        
        [self loadMainView];

    }
}
-(void)addMenuButton
{
    UIView *navigationView = [[UIView alloc]init];
    
    [self.view addSubview:navigationView];
    navigationView.backgroundColor = [UIColor colorC2060A];
    if (isFromMenu) {
        navigationView.frame = CGRectMake(0, 0, DeviceWidth, navigationHeight);
    }else{
        navigationView.frame = CGRectMake(0, 0, DeviceWidth, navigationHeight);
    }
    
    UIButton * menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setImage:[UIImage imageNamed:NavigationBackImage] forState:UIControlStateNormal];
    menuButton.frame = CGRectMake(-50, navigationHeight-45, 140, 48);

    [menuButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [navigationView addSubview:menuButton];
    
    
    Label * naviNameLabel = [[Label alloc]initWithFrame:CGRectMake(menuButton.endPointX, navigationHeight-44, DeviceWidth-menuButton.endPointX*2, 44) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:18] andTextColor:[UIColor whiteColor] andTextLines:1];
    naviNameLabel.textAlignment = NSTextAlignmentCenter;
    naviNameLabel.text = @"Booking";
    [navigationView addSubview:naviNameLabel];
}
-(void)back
{
    if(self.backToLeft){
//        HomeViewController *homeView=[[HomeViewController alloc] init];
        [[AppDelegate ShareApp] goToPage:0];
        [self.sideMenuViewController presentLeftMenuViewController];
    }else{
        [self.navigationController popViewControllerAnimated:YES];    
    }
}
-(void)loadMainView
{
    
    
    bookInfoView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 44+[UIDevice heightDifference], DeviceWidth, DeviceHeight)];
    bookInfoView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bookInfoView];
    
    Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 0, DeviceWidth-2*LEFTLEFTSET-20, 43) andTextFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:20] andTextColor:[UIColor color222222] andTextLines:1];
    titleLabel.backgroundColor = [UIColor whiteColor];
    titleLabel.lineBreakMode=NSLineBreakByTruncatingTail;
    titleLabel.text = reservation.title;
    titleLabel.numberOfLines=2;
    CGSize size=[titleLabel.text sizeWithFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:20] constrainedToSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET-20, 1000) lineBreakMode:NSLineBreakByTruncatingTail];
    if (size.height>43.0) {
        titleLabel.frame=CGRectMake( LEFTLEFTSET, 0, DeviceWidth-2*LEFTLEFTSET-20, size.height);

    }
    [bookInfoView addSubview:titleLabel];
    UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, titleLabel.endPointY, DeviceWidth, 1)];
    lineImage.backgroundColor = [UIColor colorEDEDED];
    [bookInfoView addSubview:lineImage];
    
    NSString *partyString = [NSString stringWithFormat:L(@"%d persons"),[reservation.party intValue]];
    TYAttributedLabel *personLabel = [[TYAttributedLabel alloc] initWithFrame:CGRectMake(0, titleLabel.endPointY , DeviceWidth/3, 39)];
    personLabel.backgroundColor = [UIColor clearColor];
    personLabel.text = partyString;
    personLabel.textColor = [UIColor color333333];
    personLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13];
    [bookInfoView addSubview:personLabel];
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:partyString];
    NSRange range2 = [partyString rangeOfString:[NSString stringWithFormat:L(@"%d persons"),[reservation.party intValue]]];
    [AttributedStr addAttributes:@{NSForegroundColorAttributeName:[UIColor color333333],NSFontAttributeName:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13]} range:NSMakeRange(range2.location, range2.length)];
    personLabel.attributedText = AttributedStr;
    personLabel.verticalAlignment = TYVerticalAlignmentCenter;
    personLabel.textAlignment = kCTTextAlignmentCenter;

    
    NSString *bookDate = [BookUtil dateStringWithFormat:@"EEE, dd MMM" forDate:[BookUtil dateFromString:reservation.bookDate withFormat:@"yyyy-MM-dd"]];
    NSString *bookDate1 = [BookUtil dateStringWithFormat:@", dd MMM" forDate:[BookUtil dateFromString:reservation.bookDate withFormat:@"yyyy-MM-dd"]];
    TYAttributedLabel *dateLabel = [[TYAttributedLabel alloc] initWithFrame:CGRectMake(personLabel.right, titleLabel.endPointY, DeviceWidth/3, 39)];
    dateLabel.backgroundColor = [UIColor clearColor];
    dateLabel.text = bookDate;
    dateLabel.textColor =[UIColor color333333];
    dateLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13];
    [bookInfoView addSubview:dateLabel];
    NSMutableAttributedString *AttributedStr1 = [[NSMutableAttributedString alloc]initWithString:bookDate];
    NSRange range3 = [partyString rangeOfString:bookDate1];
    [AttributedStr1 addAttributes:@{NSForegroundColorAttributeName:[UIColor color333333],NSFontAttributeName:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13]} range:NSMakeRange(range3.location, range3.length)];
    dateLabel.attributedText = AttributedStr1;
    dateLabel.verticalAlignment = TYVerticalAlignmentCenter;
    dateLabel.textAlignment = kCTTextAlignmentCenter;

    
    
    
    Label *timeLabel = [[Label alloc]initWithFrame:CGRectMake(dateLabel.right, titleLabel.endPointY, DeviceWidth/3, 39) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13] andTextColor:[UIColor color222222] andTextLines:1];
    timeLabel.text = reservation.bookTime;
    timeLabel.textAlignment = NSTextAlignmentCenter;
    [bookInfoView addSubview:timeLabel];
    
    
    UIImageView *lineImage1 = [[UIImageView alloc]initWithFrame:CGRectMake((DeviceWidth-2)/3, titleLabel.endPointY, 1, 39)];
    lineImage1.backgroundColor = [UIColor colorEDEDED];
    [bookInfoView addSubview:lineImage1];
    
    UIImageView *lineImage2 = [[UIImageView alloc]initWithFrame:CGRectMake((DeviceWidth-2)/3*2, titleLabel.endPointY, 1, 39)];
    lineImage2.backgroundColor = [UIColor colorEDEDED];
    [bookInfoView addSubview:lineImage2];
    
    UIImageView *lineImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, titleLabel.endPointY+39, DeviceWidth, 1)];
    lineImage3.backgroundColor = [UIColor colorEDEDED];
    [bookInfoView addSubview:lineImage3];
    
    Label *discountLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, timeLabel.endPointY, DeviceWidth-2*LEFTLEFTSET, 38) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13] andTextColor:[UIColor colorC2060A] andTextLines:1];
    if(reservation.offerName!=nil){
        discountLabel.text = [reservation.offerName removeHTML];
    }
    discountLabel.textAlignment = NSTextAlignmentCenter;
    [bookInfoView addSubview:discountLabel];
    
    UIImageView *lineImage6 = [[UIImageView alloc]initWithFrame:CGRectMake(0, discountLabel.endPointY, DeviceWidth, 1)];
    lineImage6.backgroundColor = [UIColor colorEDEDED];
    [bookInfoView addSubview:lineImage6];
    
    Label *addressLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, discountLabel.endPointY, DeviceWidth-2*LEFTLEFTSET, 62) andTextFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:13] andTextColor:[UIColor color333333] andTextLines:2];
    addressLabel.text = [reservation.address removeHTML];
    addressLabel.textAlignment = NSTextAlignmentCenter;
    [bookInfoView addSubview:addressLabel];
    
    UIImageView *lineImage7 = [[UIImageView alloc]initWithFrame:CGRectMake(0, addressLabel.endPointY, DeviceWidth, 1)];
    lineImage7.backgroundColor = [UIColor colorEDEDED];
    [bookInfoView addSubview:lineImage7];
    
    
    NSArray *voucherCodeBarTitle = @[@"VOUCHER",@"BOOK CODE",@"STATUS"];
    NSString *statusString=@"Unknown";
    if([reservation.status isEqualToString: @"1"]){
        statusString= @"Pending";
    }else if([reservation.status isEqualToString: @"2"]||[reservation.status isEqualToString: @"11"]){
        statusString= @"Confirmed";
    }else if([reservation.status isEqualToString: @"4"]){
        statusString= @"Completed";
    }else if([reservation.status isEqualToString: @"5"]){
        statusString= @"No Show";
    }else{
        statusString= @"Cancelled";
    }
    double voucherCodeBarEndpointY = 0;
    NSArray *voucherCodeBarValue = @[reservation.voucherCode==nil?@"":reservation.voucherCode,reservation.code,statusString];
    for (int i=0; i<voucherCodeBarTitle.count; i++) {
        CGSize size1 = [[voucherCodeBarTitle objectAtIndex:i] sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:7]];
        UILabel * lable1=[[UILabel alloc]initWithFrame:CGRectMake(DeviceWidth/3*i, addressLabel.endPointY+20, DeviceWidth/3, size1.height)];
        lable1.textColor = [UIColor color999999];
        lable1.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:7];
        lable1.text = [voucherCodeBarTitle objectAtIndex:i];
        lable1.textAlignment = NSTextAlignmentCenter;
        lable1.backgroundColor = [UIColor clearColor];
        [bookInfoView addSubview:lable1];
        voucherCodeBarEndpointY = lable1.endPointY;
    }
    for (int i=0; i<voucherCodeBarValue.count; i++) {
        CGSize size2 = [[voucherCodeBarValue objectAtIndex:i] sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13]];
        UILabel *bookCodeLabel=[[UILabel alloc]initWithFrame:CGRectMake(DeviceWidth/3*i, voucherCodeBarEndpointY, DeviceWidth/3, size2.height)];
        bookCodeLabel.textColor = [UIColor color222222];
        bookCodeLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
        bookCodeLabel.text = [voucherCodeBarValue objectAtIndex:i];
        bookCodeLabel.textAlignment = NSTextAlignmentCenter;
        bookCodeLabel.backgroundColor = [UIColor clearColor];
        [bookInfoView addSubview:bookCodeLabel];
        if(i==2){
            statusLabel=bookCodeLabel;
        }
    }
    
    
    NSArray *imageArray = @[@"phone",@"locationBlack",@"calendar"];
    UIImageView *lineImage4 = [[UIImageView alloc]initWithFrame:CGRectMake(0, statusLabel.endPointY+20, DeviceWidth, 1)];
    lineImage4.backgroundColor = [UIColor colorEDEDED];
    [bookInfoView addSubview:lineImage4];
    
    for (int i=0; i<imageArray.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake((DeviceWidth/4)*i, statusLabel.endPointY+20, DeviceWidth/4-1, 52);

        UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET*2, LEFTLEFTSET, 80-4*LEFTLEFTSET, 52-2*LEFTLEFTSET)];
        iconImage.image = [UIImage imageNamed:[imageArray objectAtIndex:i]];
        [button addSubview:iconImage];
        button.tag = 1000+i;
        if(i==0){
            [button addTarget:self action:@selector(telephoneButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        }else if(i==1){
            [button addTarget:self action:@selector(gotoMap) forControlEvents:UIControlEventTouchUpInside];
        }else{
            [button addTarget:self action:@selector(addToCalendar) forControlEvents:UIControlEventTouchUpInside];
        }
        [bookInfoView addSubview:button];
        
        UIImageView *lineImage4 = [[UIImageView alloc]initWithFrame:CGRectMake((DeviceWidth/4)*(i+1), statusLabel.endPointY+20, 1, 52)];
        lineImage4.backgroundColor = [UIColor colorEDEDED];
        [bookInfoView addSubview:lineImage4];
    }
    
    cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.frame = CGRectMake((DeviceWidth/4)*3, statusLabel.endPointY+20, DeviceWidth/4-1, 52);
    [cancelButton setTitleColor:[UIColor colorC2060A] forState:UIControlStateNormal];
    cancelButton.tag = 1000+3;
    if([reservation.status isEqualToString:@"1"]||[reservation.status isEqualToString:@"11"]||[reservation.status isEqualToString:@"2"]){
        [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
        [cancelButton addTarget:self action:@selector(cancelBooking:) forControlEvents:UIControlEventTouchUpInside];
    }
    [bookInfoView addSubview:cancelButton];
    UIImageView *lineImage41 = [[UIImageView alloc]initWithFrame:CGRectMake((DeviceWidth/4)*(4+1), statusLabel.endPointY+20, 1, 52)];
    lineImage41.backgroundColor = [UIColor colorEDEDED];
    [bookInfoView addSubview:lineImage41];
    
    
    UIImageView *lineImage5 = [[UIImageView alloc]initWithFrame:CGRectMake(0, cancelButton.endPointY, DeviceWidth, 1)];
    lineImage5.backgroundColor = [UIColor colorEDEDED];
    [bookInfoView addSubview:lineImage5];
    
    
    UIView *inviteView = [[UIView alloc]initWithFrame:CGRectMake(0, cancelButton.endPointY, DeviceWidth, 156)];
    [bookInfoView addSubview:inviteView];
    
    Label *inviteLabel = [[Label alloc]initWithFrame:CGRectMake(0, 15, DeviceWidth, 20) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16] andTextColor:[UIColor color222222] andTextLines:1];
    inviteLabel.text = @"Invite friends";
    inviteLabel.textAlignment = NSTextAlignmentCenter;
    [inviteView addSubview:inviteLabel];
    
    NSArray *imagesArr = @[@"facebook",@"EmailGray",@"MessageGreen",@"path2"];
    for (int i=0; i<imagesArr.count; i++) {
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET+DeviceWidth/4*i, inviteLabel.endPointY+15, 50, 50)];
        imageView.image = [UIImage imageNamed:[imagesArr objectAtIndex:i]];
        imageView.tag = i;
        imageView.userInteractionEnabled = YES;
        [inviteView addSubview:imageView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(inviteButtonClick:)];
        [tap setNumberOfTapsRequired:1];
        [tap setNumberOfTouchesRequired:1];
        [imageView addGestureRecognizer:tap];
    }
    bookInfoView.contentSize = CGSizeMake(0, inviteView.endPointY);
    
    
    //标题下阴影
    UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:0 andHeight:4];
    [self.view addSubview:shadowImageView];
}
-(void)cancelBooking:(UIButton*)sender{
    [[Amplitude instance] logEvent:@"Restaurant Search Result Page Continue" withEventProperties:@{}];
    UIView *popUpView = [[UIView alloc]init];
    
    NSString *offerTitleStr = @"Are you sure want to cancel your booking?";
    
    CGSize maxSize = [offerTitleStr sizeWithFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
    Label *seeMenuTitle = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 64/2-(maxSize.height+4)/2, maxSize.width, maxSize.height+4) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] andTextColor:[UIColor color333333] andTextLines:0];
    seeMenuTitle.text = offerTitleStr;
    seeMenuTitle.textAlignment = NSTextAlignmentCenter;
    [popUpView addSubview:seeMenuTitle];
    
    NSArray *menusArr = @[@"We appreciate the gesture. This means other guests can get a seat if you can't attend."];
    float startY = seeMenuTitle.endPointY ;
    for (int i=0; i<menusArr.count; i++) {
        CGSize maxSize1 = [[menusArr objectAtIndex:i] sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
        
        Label *seeMenuLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, startY, DeviceWidth-4*LEFTLEFTSET, maxSize1.height+20) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15] andTextColor:[UIColor color333333] andTextLines:0];
        seeMenuLabel.textAlignment = NSTextAlignmentCenter;
        seeMenuLabel.text = [menusArr objectAtIndex:i];
        seeMenuLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [popUpView addSubview:seeMenuLabel];
        startY = seeMenuLabel.endPointY;
    }
    popUpView.frame = CGRectMake(0, 0, DeviceWidth-2*LEFTLEFTSET, startY);
    
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc]init];
    alertView.buttonTitles = @[@"No, keep it",@"Yes"];
    [alertView setContainerView:popUpView];
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, NSInteger buttonIndex) {
        if(buttonIndex==1){
            [[LoadingView sharedLoadingView] startLoading];
            IMGUser * user = [IMGUser currentUser];
            if(user.userId==nil){
                //lead to login
                return;
            }
            if(user.token==nil){
                //lead to login
                return;
            }
            NSString *newStatus=@"13";
            if([reservation.status isEqualToString:@"11"]){
                newStatus=@"14";
            }else if([reservation.status isEqualToString:@"2"]){
                newStatus=@"3";
            }
            NSDictionary * parameters = @{@"userID":user.userId,@"t":user.token,@"id":reservation.reservationId,@"status":newStatus};
            [[IMGNetWork sharedManager]POST:@"reseravtion/status/update" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
                
                NSString *exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
                if([exceptionMsg isLogout]){
                    [[LoadingView sharedLoadingView] stopLoading];
                    [[IMGUser currentUser]logOut];
                    [[AppDelegate ShareApp] goToLoginController];
                    return;
                }
                
                NSString *returnStatusString = [responseObject objectForKey:@"status"];
                if([returnStatusString isEqualToString:@"succeed"]){
                    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"select * from IMGRestaurant where restaurantId=%@ limit 1",reservation.restaurantId] successBlock:^(FMResultSet *resultSet) {
                        if ([resultSet next]) {
                            IMGRestaurant *restaurant = [[IMGRestaurant alloc]init];
                            [restaurant setValueWithResultSet:resultSet];
                            [[NSUserDefaults standardUserDefaults]setObject:[NSDate date] forKey:@"dateCancel"];
//                            [MixpanelHelper trackCancelBookingWithRestaurant:restaurant andReservation:reservation];
                        }
                    } failureBlock:^(NSError *error) {
                        
                    }];
                    [statusLabel setText:@"Cancelled"];
                    [cancelButton setHidden:YES];
                    if ([self.bookingDetailContentControllerDelegate respondsToSelector:@selector(cancelUpcomingBooking:)]) {
                        [self.bookingDetailContentControllerDelegate cancelUpcomingBooking:[reservation.restaurantId integerValue]];
                    }
                }
                [[LoadingView sharedLoadingView]stopLoading];
            } failure:^(NSURLSessionDataTask *operation, NSError *error) {
                NSLog(@"MainMenuViewController.request cancel book.error: %@",error.localizedDescription);
                [[LoadingView sharedLoadingView]stopLoading];
            }];
        }
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    [alertView show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==1){
        if(buttonIndex==0){
            [alertView dismissWithClickedButtonIndex:0 animated:NO];
        }else if (buttonIndex==1){
            
            [SendCall sendCall:alertView.message];
        }
    }
}
-(void)telephoneButtonTapped:(UIButton*)button{
    
    IMGUser *user = [IMGUser currentUser];
    NSNumber *userId = [[NSNumber alloc] init];
    if (user.userId != nil)
    {
        userId = user.userId;
    }
    else
        userId = [NSNumber numberWithInt:0];
    
    [TrackHandler trackWithUserId:userId andRestaurantId:reservation.restaurantId andContent:[NSDictionary dictionaryWithObject:@"0" forKey:@"type"]];
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"" message:reservation.restaurantPhone delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
    alertView.tag=1;
    [alertView show];
}
-(void)gotoMap{
    IMGReservation *currentReservation=reservation;
    NSString *sqlString1=[NSString stringWithFormat:@"select * from IMGReservation where reservationId=%@;",reservation.reservationId];
    FMResultSet * resultSet1 = [[DBManager manager] executeQuery:sqlString1];
    if([resultSet1 next]){
        [currentReservation setValueWithResultSet:resultSet1];
    }
    [resultSet1 close];

    IMGUser *user = [IMGUser currentUser];
    NSNumber *userId = [[NSNumber alloc] init];
    if (user.userId != nil)
    {
        userId = user.userId;
    }
    else{
        userId = [NSNumber numberWithInt:0];
    }
    IMGRestaurant *restaurant=[[IMGRestaurant alloc] init];
    restaurant.restaurantId=currentReservation.restaurantId;
    restaurant.latitude=currentReservation.latitude;
    restaurant.longitude=currentReservation.longitude;
    restaurant.title=currentReservation.restaurantName;
    restaurant.cuisineName=currentReservation.cuisineName;
    restaurant.priceLevel=currentReservation.priceName;
    restaurant.districtName=currentReservation.districtName;
    restaurant.ratingScore=currentReservation.ratingScore;
    [TrackHandler trackWithUserId:userId andRestaurantId:currentReservation.restaurantId andContent:[NSDictionary dictionaryWithObject:@"1" forKey:@"type"]];
    
    MapViewController *mapViewController=[[MapViewController alloc] initWithRestaurant:restaurant];
    [self.navigationController pushViewController:mapViewController animated:YES];
}
-(void)addToCalendar{
    IMGReservation *currentReservation=reservation;
    IMGRestaurant *currentRestaurant=[[IMGRestaurant alloc]init];
    NSString *sqlString=[NSString stringWithFormat:@"select * from IMGRestaurant where restaurantId=%@;",currentReservation.restaurantId];
    FMResultSet * resultSet2 = [[DBManager manager] executeQuery:sqlString];
    if([resultSet2 next]){
        [currentRestaurant setValueWithResultSet:resultSet2];
    }
    [resultSet2 close];
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    if ([eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)]){
        // the selector is available, so we must be on iOS 6 or newer
        [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (error){
                    // display error message here
                }else if(!granted){
                    // display access denied error message here
                }else{
                    // access granted
                    // ***** do the important stuff here *****
                    EKEvent *ekEvent  = [EKEvent eventWithEventStore:eventStore];
                    ekEvent.title     = reservation.restaurantName;
                    ekEvent.location = [currentRestaurant address];
                    
                    
                    NSDateFormatter *tempFormatter = [[NSDateFormatter alloc]init];
                    [tempFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
                    ekEvent.startDate=[tempFormatter dateFromString:[NSString stringWithFormat:@"%@ %@",currentReservation.bookDate,currentReservation.bookTime]];
                    ekEvent.endDate=[tempFormatter dateFromString:[NSString stringWithFormat:@"%@ %@",currentReservation.bookDate,currentReservation.bookTime]];
                    
                    [tempFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
                    ekEvent.allDay = NO;
                    
                    //添加提醒
                    [ekEvent addAlarm:[EKAlarm alarmWithRelativeOffset:60.0f * -60.0f * 24]];
                    [ekEvent addAlarm:[EKAlarm alarmWithRelativeOffset:60.0f * -15.0f]];
                    
                    [ekEvent setCalendar:[eventStore defaultCalendarForNewEvents]];
                    NSError *err;
                    [eventStore saveEvent:ekEvent span:EKSpanThisEvent error:&err];
                    
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Calendar" message:@"Reservation is added to calendar." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [alert show];
                    
                }
            });
        }];
    }
    
    
}


-(void)upcommingButtonClick:(UIButton*)button
{
    switch (button.tag - 1000) {
        case 0:
        {//phone
            
        }
            break;
            
        case 1:
        {//location
            
        }
            break;
            
        case 2:
        {//calendar
            
        }
            break;
            
        case 3:
        {//cancel
            
            
            
        }
            break;
            
        default:
            break;
    }
}

-(void)offerButtonClick
{
    UIView *popUpView = [[UIView alloc]init];
    
    NSString *offerTitleStr = @"Terms";
    
    CGSize maxSize = [offerTitleStr sizeWithFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
    Label *seeMenuTitle = [[Label alloc]initWithFrame:CGRectMake((DeviceWidth-4*LEFTLEFTSET)/2-maxSize.width/2, 54/2-(maxSize.height+4)/2, maxSize.width, maxSize.height+4) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] andTextColor:[UIColor color333333] andTextLines:0];
    seeMenuTitle.text = offerTitleStr;
    [popUpView addSubview:seeMenuTitle];
    
    NSArray *menusArr = @[@"Discount is valid only for food and non-alcoholic beverges",@"Please let us know if you are an Landmark Building Employee"];
    float startY = seeMenuTitle.endPointY ;
    for (int i=0; i<menusArr.count; i++) {
        CGSize maxSize1 = [[menusArr objectAtIndex:i] sizeWithFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:15] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
        
        Label *seeMenuLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, startY, DeviceWidth-4*LEFTLEFTSET, maxSize1.height+20) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15] andTextColor:[UIColor color222222] andTextLines:0];
        seeMenuLabel.text = [menusArr objectAtIndex:i];
        seeMenuLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [popUpView addSubview:seeMenuLabel];
        startY = seeMenuLabel.endPointY;
    }
    popUpView.frame = CGRectMake(0, 0, DeviceWidth-2*LEFTLEFTSET, startY);
    
    
    
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc]init];
    // Add some custom content to the alert view
    [alertView setContainerView:popUpView];
    
    //    [alertView setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, NSInteger buttonIndex) {
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
}
-(void)inviteButtonClick:(UITapGestureRecognizer*)gesture
{
    
    
    
    NSInteger tag = gesture.view.tag;
    if(tag==0){//facebook
        [self shareWithFacebook];
    }else if(tag==1){//email
        [self shareWithEmail];
    }else if(tag==2){//sms
        [self shareWithSMS];
    }else if(tag==3){//path
        [self shareToPath];
    }else{//whatsapp
        
    }
}
-(void)shareToPath{
    NSString *pathToken = [[NSUserDefaults standardUserDefaults] objectForKey:QRAVED_PATH_TOKEN_KEY];
    if(pathToken==nil){
        [[NXOAuth2AccountStore sharedStore] requestAccessToAccountWithType:@"Path"];
    }else{
        NSLog(@"get path token directly from NSUserDefaults");
    }
    [[Amplitude instance] logEvent:@"SH - Share Reservation" withEventProperties:@{@"Reservation_ID":reservation.reservationId,@"Channel":@"Path"}];
    

    IMGReservation *currentReservation=reservation;
    IMGRestaurant *currentRestaurant=[[IMGRestaurant alloc]init];
    NSString *sqlString=[NSString stringWithFormat:@"select * from IMGRestaurant where restaurantId=%@;",currentReservation.restaurantId];
    FMResultSet * resultSet2 = [[DBManager manager] executeQuery:sqlString];
    if([resultSet2 next]){
        [currentRestaurant setValueWithResultSet:resultSet2];
    }
    [resultSet2 close];
    NSString *shareContentString = [NSString stringWithFormat:@"I just made a booking at %@ on Qraved.com",reservation.restaurantName];
    
    NSString *imgURL;
//    FMResultSet *resultSet=[[DBManager manager]executeQuery:[NSString stringWithFormat:@"SELECT imageUrl FROM IMGRestaurantImage WHERE restaurantId = '%@' limit 1;",currentRestaurant.restaurantId]];
//    if ([resultSet next]) {
//        imgURL=[resultSet stringForColumn:@"imageUrl"];
//    }
//    [resultSet close];
    
    ShareToPathView *shareToPathView = [[ShareToPathView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+[UIDevice heightDifference]) title:shareContentString url:imgURL latitude:currentRestaurant.latitude longitude:currentRestaurant.longitude restaurantTitle:reservation.restaurantName phone:currentRestaurant.phoneNumber address:[currentRestaurant address]];
    [self.view addSubview:shareToPathView];
//    [MixpanelHelper trackShareToPathWithshareObject:reservation.restaurantName andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
}

-(void)shareWithFacebook{
    [[LoadingView sharedLoadingView]startLoading];
    [[Amplitude instance] logEvent:@"SH - Share Reservation" withEventProperties:@{@"Reservation_ID":reservation.reservationId,@"Channel":@"Facebook"}];

    NSString *shareContentString = [NSString stringWithFormat:@"I just made a booking at %@ on Qraved.com",reservation.restaurantName];
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@?shareType=booking",QRAVED_WEB_SERVER_OLD,[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],reservation.restaurantSeoKeywords]];
    SLComposeViewController *slComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [slComposerSheet setInitialText:shareContentString];
    [slComposerSheet addURL:url];
    if(slComposerSheet){
        [self presentViewController:slComposerSheet animated:YES completion:^{
            [[LoadingView sharedLoadingView]stopLoading];
        }];
        [slComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result){
            NSString *output;
            switch(result){
                case SLComposeViewControllerResultCancelled:
                    output = @"Action Cancelled";
                    break;
                case SLComposeViewControllerResultDone:{
                    output = @"Post Successful";
//                    [MixpanelHelper trackShareToFacebookWithshareObject:reservation.restaurantName andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
                }
                    break;
                default:
                    break;
            }
            if (result != SLComposeViewControllerResultCancelled){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook Message" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
        }];
    }else{
        [[LoadingView sharedLoadingView]stopLoading];
    }
}

-(void)shareWithEmail{
    _restaurant=[[IMGRestaurant alloc]init];
    NSString *sqlString=[NSString stringWithFormat:@"select * from IMGRestaurant where restaurantId=%@;",reservation.restaurantId];
    FMResultSet * resultSet2 = [[DBManager manager] executeQuery:sqlString];
    [[Amplitude instance] logEvent:@"SH - Share Reservation" withEventProperties:@{@"Reservation_ID":reservation.reservationId,@"Channel":@"Email"}];

    if([resultSet2 next]){
        [_restaurant setValueWithResultSet:resultSet2];
    }
    [resultSet2 close];
    if ([MFMailComposeViewController canSendMail]){
        NSString *shareContentString = [NSString stringWithFormat:@"I just made a booking for us at %@ on %@ %@. Click <a href='%@'>this</a> if you want to check out what the restaurant has got to offer.",reservation.restaurantName,reservation.bookDate,reservation.bookTime,[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],reservation.restaurantSeoKeywords]];
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:[NSString stringWithFormat:@"You're Invited To %@",reservation.restaurantName]];
        [mailer setMessageBody:shareContentString isHTML:YES];
        [self presentViewController:mailer animated:YES completion:nil];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Oops! This device does not support email."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
    }
}

-(void)shareWithSMS{
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Oops! This device does not support sms."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        return;
    }
    [[Amplitude instance] logEvent:@"SH - Share Reservation" withEventProperties:@{@"Reservation_ID":reservation.reservationId,@"Channel":@"SMS"}];
    

    _restaurant=[[IMGRestaurant alloc]init];
    NSString *sqlString=[NSString stringWithFormat:@"select * from IMGRestaurant where restaurantId=%@;",reservation.restaurantId];
    FMResultSet * resultSet2 = [[DBManager manager] executeQuery:sqlString];
    if([resultSet2 next]){
        [_restaurant setValueWithResultSet:resultSet2];
    }
    [resultSet2 close];
    NSString *shareContentString = [NSString stringWithFormat:@"I just made a booking for us at %@ on %@ %@. Visit %@ if you want to check out what the restaurant has got to offer.",reservation.restaurantName,reservation.bookDate,reservation.bookTime,[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],reservation.restaurantSeoKeywords]];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setBody:shareContentString];
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
        {
//            [MixpanelHelper trackShareToEmailWithshareObject:reservation.restaurantName andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
        }
            break;
        case MFMailComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Failed to send E-Mail!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
        }
            break;
            
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
//            [MixpanelHelper trackShareToSMSWithshareObject:reservation.restaurantName andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

 



@end
