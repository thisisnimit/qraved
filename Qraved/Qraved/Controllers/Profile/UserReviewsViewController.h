//
//  UserReviewsViewController.h
//  Qraved
//
//  Created by Lucky on 15/10/22.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "IMGUser.h"

@interface UserReviewsViewController : BaseViewController
@property (nonatomic,assign) BOOL isOtherUser;
@property(nonatomic,retain) IMGUser *otherUser;
@end
