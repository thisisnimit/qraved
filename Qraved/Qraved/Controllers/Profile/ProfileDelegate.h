//
//  ProfileDelegate.h
//  Qraved
//
//  Created by root on 10/20/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGRestaurant.h"

@protocol ProfileDelegate
@optional

-(void)loadMyList:(void(^)(BOOL))block;

-(void)menuClick:(NSInteger)index;

-(void)mylistClick:(NSInteger)index;
-(void)mylistMoreClick:(NSInteger)index;
-(void)upcomingClick:(NSInteger)index;

-(void)loadHistoryBookings:(void(^)(BOOL hasData))block;

-(void)goToBookingDetail:(NSIndexPath*)indexpath;

-(void)avatarClicked;
-(void)editClicked;
-(void)profileScrollNavigationController:(UIScrollView *)scrollView;
-(void)seeMoreDraftClicked;
-(void)reviewDraftClike:(IMGRestaurant*)restaurant;
-(void)uploadDraftCliked:(IMGRestaurant*)restanrant;
@end
