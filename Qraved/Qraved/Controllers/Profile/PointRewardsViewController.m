//
//  LoyaltyPointsViewController.m
//  Qraved
//
//  Created by Shine Wang on 1/24/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#define REWARD_HISTORY_MAX 4

#import "PointRewardsViewController.h"
#import <CoreText/CoreText.h>
#import "AppDelegate.h"
//#import "Mixpanel.h"

#import "UIConstants.h"

#import "UIViewController+Helper.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "UIColor+Helper.h"
#import "Label.h"
#import "LoadingView.h"
#import "TYAttributedLabel.h"
#import "IMGExceptionAlertView.h"
#import "CustomIOS7AlertView.h"

#import "IMGUser.h"

@interface PointRewardsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *mainTableView;
@end

@implementation PointRewardsViewController{
    UIButton *firstLevelRewardButton;
    UIButton *secondLevelRewardButton;
    UIButton *thirdLevelRewardButton;
    UILabel *firstRedeemLabel;
    UILabel *secondRedeemLabel;
    UILabel *thirdRedeemLabel;
    UIImageView *pointsProgressDefaultImage;
    UIImageView *pointsProgressImage;
    Label *pointCurrentLabel;
    Label *pointNextLevelLabel;
    TYAttributedLabel *pointNowLabel;
    TYAttributedLabel *nextRedeemLabel;
    Label *nextStepRewardLabel;
    Label *redeemsDescriptionLabel;
    Label *redeemsDescriptionL;
    UILabel *differenceLabel;
    
    UIView *redeemsView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.title = @"Rewards";
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"My Reward detail page";
    self.view.backgroundColor = [UIColor whiteColor];
    [self addBackBtn];
    [self loadMainView];
    [self updateRewardView];
}

-(void)loadMainView
{
    int pointCurrentValue=0;
    int pointNextLevelValue=1000;
    int pointsPerBooking=100;
    int leftBookings=(pointNextLevelValue-pointCurrentValue)/pointsPerBooking;
    
    NSNumber *nextStepReward=[NSNumber numberWithInt:100000];
    NSString *pointCurrentValueString = [NSString stringWithFormat:@"%d ",pointCurrentValue];
    NSString *pointNextLevelValueString = [NSString stringWithFormat:@"of %d ",pointNextLevelValue];
    
    NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setFormatterBehavior: NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
    NSString *nextStepRewardString = [numberFormatter stringFromNumber: nextStepReward];
    nextStepRewardString = [NSString stringWithFormat:@"You are %d bookings away from a IDR %@ voucher",leftBookings,nextStepRewardString];
    
    Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 36) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:16] andTextColor:[UIColor color222222] andTextLines:1];
    titleLabel.text = @"   YOUR POINTS";
    titleLabel.backgroundColor = [UIColor colorFBFBFB];
    [self.view addSubview:titleLabel];
    
    UIImageView *lineImage1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, titleLabel.endPointY, DeviceWidth, 1)];
    lineImage1.backgroundColor = [UIColor colorF4F4F4];
    [self.view addSubview:lineImage1];
    
    UIView *pointsView = [[UIView alloc]initWithFrame:CGRectMake(0, titleLabel.endPointY, DeviceWidth, 111)];
    
    [self.view addSubview:pointsView];
 
    //now ~point
    pointNowLabel = [[TYAttributedLabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, 10, 80, 40)];
    pointNowLabel.text = pointCurrentValueString;
    pointNowLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:28];
    pointNowLabel.textColor = [UIColor colorWithRed:241/255.0f green:15/255.0f blue:51/255.0f alpha:1.0f];//[UIColor color222222];
    [pointsView addSubview:pointNowLabel];
    
    NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:pointCurrentValueString];
 
    nextRedeemLabel = [[TYAttributedLabel alloc] initWithFrame:CGRectMake(DeviceWidth-LEFTLEFTSET-70, 30, 100, 20)];
    nextRedeemLabel.text = pointNextLevelValueString;
    nextRedeemLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14];
    nextRedeemLabel.textColor = [UIColor color333333];//[UIColor color222222];
    [pointsView addSubview:nextRedeemLabel];
   
    pointsProgressDefaultImage = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET, pointNowLabel.endPointY, DeviceWidth-2*LEFTLEFTSET, 10)];
    pointsProgressDefaultImage.image = [UIImage imageNamed:@"ProgressBarGray.png"];
    [pointsView addSubview:pointsProgressDefaultImage];
    
    pointsProgressImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, (DeviceWidth-2*LEFTLEFTSET)*(pointCurrentValue*1.0/pointNextLevelValue), 10)];
    pointsProgressImage.backgroundColor = [UIColor colorWithRed:241/255.0f green:15/255.0f blue:51/255.0f alpha:1.0f];
    pointsProgressImage.layer.masksToBounds = YES;
    pointsProgressImage.layer.cornerRadius = 5;
    [pointsProgressDefaultImage addSubview:pointsProgressImage];
    
    
    
    differenceLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, pointsProgressDefaultImage.endPointY +10, DeviceWidth - 30, 15)];
    differenceLabel.text = @"625 more points for next redemption";
    differenceLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12];
    differenceLabel.textColor = [UIColor lightGrayColor];//[UIColor color222222];
    differenceLabel.textAlignment = NSTextAlignmentCenter;
    [pointsView addSubview:differenceLabel];
    
    UIImageView *lineImage2 = [[UIImageView alloc]initWithFrame:CGRectMake(0, pointsView.endPointY, DeviceWidth, 1)];
    lineImage2.backgroundColor = [UIColor colorF4F4F4];
    [self.view addSubview:lineImage2];
    
    //You are 10 bookings away from a IDR 100,000 voucher
    nextStepRewardLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET*4, pointsView.endPointY, DeviceWidth-8*LEFTLEFTSET, 55) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor color333333] andTextLines:2];
    nextStepRewardLabel.text = nextStepRewardString;
    nextStepRewardLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:nextStepRewardLabel];
    
    UIImageView *lineImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, nextStepRewardLabel.endPointY, DeviceWidth, 1)];
    lineImage3.backgroundColor = [UIColor colorF4F4F4];
    [self.view addSubview:lineImage3];
    
    redeemsView = [[UIView alloc]initWithFrame:CGRectMake(0, nextStepRewardLabel.endPointY+1, DeviceWidth, 36)];
    [self.view addSubview:redeemsView];
    redeemsView.backgroundColor = [UIColor colorFBFBFB];
    
    Label *redeemsTitleLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 0, DeviceWidth-2*LEFTLEFTSET, 36) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:16] andTextColor:[UIColor color222222] andTextLines:1];
    redeemsTitleLabel.text = @"REDEEM";
    redeemsTitleLabel.backgroundColor = [UIColor colorFBFBFB];
    [redeemsView addSubview:redeemsTitleLabel];
    
    UIImageView *lineImage4 = [[UIImageView alloc]initWithFrame:CGRectMake(0, redeemsTitleLabel.endPointY, DeviceWidth, 1)];
    lineImage4.backgroundColor = [UIColor colorF4F4F4];
    [redeemsView addSubview:lineImage4];
    
    NSString *redeemsDescriptionString = [NSString stringWithFormat: @"Each reservation is honored with %d points.",pointsPerBooking];// %@ You can redeem your points to get cashback vouchers to be used on your next reservation.
    CGSize expectSize = [redeemsDescriptionString sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] constrainedToSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 120)];
    
    redeemsDescriptionLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, redeemsView.endPointY+16, DeviceWidth-2*LEFTLEFTSET, expectSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor darkGrayColor] andTextLines:0];
    redeemsDescriptionLabel.text = redeemsDescriptionString;
    redeemsDescriptionLabel.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:redeemsDescriptionLabel];
    
    NSString *redeemsDescriptionS = @"You can redeem your points to get cashback vouchers to be used on your next reservation.";
    CGSize expectSize1 = [redeemsDescriptionS sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] constrainedToSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 120)];
    
    
    redeemsDescriptionL = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, redeemsDescriptionLabel.bottom, DeviceWidth-2*LEFTLEFTSET, expectSize1.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor darkGrayColor] andTextLines:0];
    redeemsDescriptionL.text = redeemsDescriptionS;
    redeemsDescriptionL.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:redeemsDescriptionL];
    //标题下阴影
    UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:0 andHeight:4];
    [self.view addSubview:shadowImageView];
}

-(UIButton *)addRewardButton:(int)index reward:(NSString *)reward points:(int)points lastEndPointY:(CGFloat)lastEndPointY redeemblePoints:(int)redeemblePoints{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    //    button.frame = CGRectMake((DeviceWidth - 102*3-7*2)/2+103*index, 16+lastEndPointY, 102, 55);
    button.frame = CGRectMake(10, (16 *index)+lastEndPointY +60 *index, DeviceWidth - 20, 60);
    button.tag = points;
    
    Label *rewardLabel = [[Label alloc]initWithFrame:CGRectMake(0, 8, 102, 20) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:15] andTextColor:[UIColor whiteColor] andTextLines:1];
    rewardLabel.centerX = button.centerX;
    rewardLabel.tag = points;
    rewardLabel.textAlignment = NSTextAlignmentCenter;
    rewardLabel.text = reward;
    Label *pointsLabel = [[Label alloc]initWithFrame:CGRectMake(0, rewardLabel.endPointY, 102, 20) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:12] andTextColor:[UIColor whiteColor] andTextLines:1];
    pointsLabel.centerX = button.centerX;
    [button addSubview:pointsLabel];
    pointsLabel.tag = points;
    pointsLabel.textAlignment = NSTextAlignmentCenter;
    pointsLabel.text = [NSString stringWithFormat:@"%d points",points];
    CGFloat buttonEndPointY=button.endPointY;
    if (redeemblePoints>=points) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(redeemsButtonLabelTapped:)];
        [button addTarget:self action:@selector(redeemButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [button setBackgroundImage:[UIImage imageNamed:@"Rectangle"] forState:UIControlStateNormal];//RedeemNow
        
        if(index==0){
            //            firstRedeemLabel = [self addRedeemLabel:index addTapGestureRecognizer:tap lastEndPointY:buttonEndPointY];
        }else if(index==1){
            //            secondRedeemLabel = [self addRedeemLabel:index addTapGestureRecognizer:tap lastEndPointY:buttonEndPointY];
        }else if(index==2){
            //            thirdRedeemLabel = [self addRedeemLabel:index addTapGestureRecognizer:tap lastEndPointY:buttonEndPointY];
        }
        [pointsLabel addGestureRecognizer:tap];
    }else{
        [button setBackgroundImage:[UIImage imageNamed:@"Rectanglekong4@2x"] forState:UIControlStateNormal];
        rewardLabel.textColor = [UIColor colorWithRed:241/255.0f green:15/255.0f blue:51/255.0 alpha:1.0f];
        pointsLabel.textColor = [UIColor colorWithRed:241/255.0f green:15/255.0f blue:51/255.0 alpha:1.0f];
        if(index==0){
            //            firstRedeemLabel = [self addRedeemLabel:index addTapGestureRecognizer:nil lastEndPointY:buttonEndPointY];
        }else if(index==1){
            //            secondRedeemLabel = [self addRedeemLabel:index addTapGestureRecognizer:nil lastEndPointY:buttonEndPointY];
        }else if(index==2){
            //            thirdRedeemLabel = [self addRedeemLabel:index addTapGestureRecognizer:nil lastEndPointY:buttonEndPointY];
        }
    }
    [button addSubview:rewardLabel];
    [self.view addSubview:button];
    if (redeemblePoints >= points) {
        UIControl *control = [[UIControl alloc]initWithFrame:button.bounds];
        [button addSubview:control];
        [control addTarget:self action:@selector(redeemButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        control.tag =  button.tag;
    }
    
    return button;
}

//-(Label *)addRedeemLabel:(int)index addTapGestureRecognizer:(UITapGestureRecognizer *)gesture lastEndPointY:(CGFloat)lastEndPointY{
//    Label *redeemLabel;
//    if(gesture==nil){
//        redeemLabel=[[Label alloc]initWithFrame:CGRectMake((DeviceWidth - 102*3-7*2)/2+103*index, lastEndPointY+10, 102, 20) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:11] andTextColor:[UIColor color999999] andTextLines:1];
//        redeemLabel.text = @"Not enough points";
//        [self.view addSubview:redeemLabel];
//    }else{
//        redeemLabel=[[Label alloc]initWithFrame:CGRectMake((DeviceWidth - 102*3-7*2)/2+103*index, lastEndPointY+10, 102, 20) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:11] andTextColor:[UIColor colorC2060A] andTextLines:1];
//        redeemLabel.textAlignment = NSTextAlignmentCenter;
//        redeemLabel.text = @"Redeem now!";
//        [self.view addSubview:redeemLabel];
//        [redeemLabel addGestureRecognizer:gesture];
//    }
//    return redeemLabel;
//}

-(void)redeemsButtonLabelTapped:(UITapGestureRecognizer *)gesture
{
    
    UIView *popUpView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth-4*LEFTLEFTSET, 60)];
    
    NSString *offerTitleStr = @"Are you sure you want to redeem?";
    
    CGSize maxSize = [offerTitleStr sizeWithFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
    Label *seeMenuTitle = [[Label alloc]initWithFrame:CGRectMake((DeviceWidth-4*LEFTLEFTSET)/2-maxSize.width/2, 54/2-(maxSize.height+4)/2, maxSize.width, maxSize.height+4) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] andTextColor:[UIColor color222222] andTextLines:0];
    seeMenuTitle.text = offerTitleStr;
    [popUpView addSubview:seeMenuTitle];
    
    
    
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc]init];
    [alertView setContainerView:popUpView];
    alertView.buttonTitles = @[@"Yes",@"No"];
    
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 0) {
            NSInteger tag=gesture.view.tag;
            [self requestRedeem:tag];
        }
        else
        {
            
        }
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    
    [alertView show];
    
}

-(void)redeemButtonTapped:(UIButton*)button
{
    
    UIView *popUpView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth-4*LEFTLEFTSET, 60)];
    
    NSString *offerTitleStr = @"Are you sure you want to redeem?";
    
    CGSize maxSize = [offerTitleStr sizeWithFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
    Label *seeMenuTitle = [[Label alloc]initWithFrame:CGRectMake((DeviceWidth-4*LEFTLEFTSET)/2-maxSize.width/2, 54/2-(maxSize.height+4)/2, maxSize.width, maxSize.height+4) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] andTextColor:[UIColor color222222] andTextLines:0];
    seeMenuTitle.text = offerTitleStr;
    [popUpView addSubview:seeMenuTitle];
    
    
    
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc]init];
    [alertView setContainerView:popUpView];
    alertView.buttonTitles = @[@"Yes",@"No"];
    
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 0) {
            NSInteger tag=button.tag;
            [self requestRedeem:tag];
        }
        else
        {
            
        }
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    
    [alertView show];
    
    
}

-(void)requestRedeem:(NSInteger)pointToRedeem{
    [[LoadingView sharedLoadingView] startLoading];
    IMGUser * user = [IMGUser currentUser];
    if(user.userId==nil){
        //lead to login
        return;
    }
    if(user.token==nil){
        //lead to login
        return;
    }
    
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:[NSNumber numberWithLong:pointToRedeem] forKey:@"Point Redeemed"];
    [[Amplitude instance] logEvent:@"CL – Redeem User Rewards" withEventProperties:eventProperties];
    
    NSDictionary * parameters = @{@"userID":user.userId,@"t":user.token,@"pointToRedeem":[NSNumber numberWithLong:pointToRedeem]};
    
    [[IMGNetWork sharedManager]POST:@"user/point/redeem" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSString *returnStatusString = [responseObject objectForKey:@"status"];
        if([returnStatusString isEqualToString:@"succeed"]){
            
            if(firstLevelRewardButton!=nil){
                [firstLevelRewardButton removeFromSuperview];
                [firstRedeemLabel removeFromSuperview];
            }
            if(secondLevelRewardButton!=nil){
                [secondLevelRewardButton removeFromSuperview];
                [secondRedeemLabel removeFromSuperview];
            }
            if(thirdLevelRewardButton!=nil){
                [thirdLevelRewardButton removeFromSuperview];
                [thirdRedeemLabel removeFromSuperview];
            }
        }
        [[LoadingView sharedLoadingView]stopLoading];
        [self updateRewardView];
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"requestRedeem error: %@",error.localizedDescription);
        [[LoadingView sharedLoadingView]stopLoading];
    }];
    
}
-(void)updateRewardView{
    IMGUser *user=[IMGUser currentUser];
    NSString * userId = user.userId.description;
    NSString * token =user.token.description;
    NSDictionary * parameters = @{@"userID":userId,@"t":token};
    [[LoadingView sharedLoadingView] startLoading];
    [[IMGNetWork sharedManager]GET:@"app/points" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id pointDictionary) {
        NSString * exceptionMsg = [pointDictionary objectForKey:@"exceptionmsg"];
        if(exceptionMsg){
            [[IMGUser currentUser]logOut];
        }else{
            NSArray *rewardArray = [pointDictionary objectForKey:@"rewardTriggerList"];
            if(rewardArray!=nil){
                int pointNextLevelValue;
                NSNumber *redeemablePoints = [pointDictionary objectForKey:@"redeemablePoints"];
                int pointCurrentValue=[redeemablePoints intValue];
                int pointsPerBooking = [[pointDictionary objectForKey:@"rewardSetting"] intValue];
                int leftBookings=0;
                NSNumber *nextStepReward;
                NSString *pointCurrentValueString = [NSString stringWithFormat:@"%d ",pointCurrentValue];
                
                //                CGFloat lastEndPointY=redeemsDescriptionLabel.endPointY;
                CGFloat lastEndPointY=redeemsView.endPointY +16;
                
                
                NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
                [numberFormatter setFormatterBehavior: NSNumberFormatterBehavior10_4];
                [numberFormatter setNumberStyle: NSNumberFormatterDecimalStyle];
                for(NSDictionary *rewardDictionary in rewardArray){
                    NSNumber *points = [rewardDictionary objectForKey:@"points"];
                    nextStepReward = [rewardDictionary objectForKey:@"rewardValue"];
                    pointNextLevelValue=[points intValue];
                    if(pointCurrentValue<pointNextLevelValue){
                        break;
                    }
                }
                
                int i=0;
                for(NSDictionary *rewardDictionary in rewardArray){
                    NSNumber *points = [rewardDictionary objectForKey:@"points"];
                    NSNumber *reward = [rewardDictionary objectForKey:@"rewardValue"];
                    if(i==0){
                        firstLevelRewardButton = [self addRewardButton:0 reward:[NSString stringWithFormat:@"IDR %@",[numberFormatter stringFromNumber: reward]] points:[points intValue] lastEndPointY:lastEndPointY redeemblePoints:pointCurrentValue];
                    }else if(i==1){
                        secondLevelRewardButton = [self addRewardButton:1 reward:[NSString stringWithFormat:@"IDR %@",[numberFormatter stringFromNumber: reward] ] points:[points intValue] lastEndPointY:lastEndPointY redeemblePoints:pointCurrentValue];
                    }else if(i==2){
                        thirdLevelRewardButton = [self addRewardButton:2 reward:[NSString stringWithFormat:@"IDR %@",[numberFormatter stringFromNumber: reward] ] points:[points intValue] lastEndPointY:lastEndPointY redeemblePoints:pointCurrentValue];
                    }
                    i++;
                    
                    NSString *redeemsDescriptionString = [NSString stringWithFormat: @"Each reservation is honored with %d points. ",pointsPerBooking];//You can redeem your points to get cashback vouchers to be used on your next reservation.
                    redeemsDescriptionLabel.text = redeemsDescriptionString;
                    
                    
                    CGSize expectSize = [redeemsDescriptionString sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] constrainedToSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 120)];
                    redeemsDescriptionLabel.frame = CGRectMake(LEFTLEFTSET, (16 *i)+lastEndPointY +60 *i, DeviceWidth-2*LEFTLEFTSET, expectSize.height);
                    redeemsDescriptionLabel.text = redeemsDescriptionString;
                    
                    
                    NSString *redeemsDescriptionS = @"You can redeem your points to get cashback vouchers to be used on your next reservation.";
                    CGSize expectSize1 = [redeemsDescriptionS sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] constrainedToSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 120)];
                    redeemsDescriptionL.text = redeemsDescriptionS;
                    redeemsDescriptionL.frame = CGRectMake(LEFTLEFTSET, (16 *i)+lastEndPointY +60 *i +20, DeviceWidth-2*LEFTLEFTSET, expectSize1.height);
                    redeemsDescriptionL.text = redeemsDescriptionS;
                    
                    
                }
                
                leftBookings=(pointNextLevelValue-pointCurrentValue)/pointsPerBooking;
                if(leftBookings<0){
                    leftBookings=0;
                }
                NSString *pointNextLevelValueString = [NSString stringWithFormat:@"of %d ",pointNextLevelValue];
                
                NSString *nextStepRewardString = [numberFormatter stringFromNumber: nextStepReward];
                nextStepRewardString = [NSString stringWithFormat:@"You are %d bookings away from a IDR %@ voucher",leftBookings,nextStepRewardString];
                
                if(pointCurrentValue>0){
                    pointCurrentLabel.text = [NSString stringWithFormat:@"%d",pointCurrentValue];
                }else{
                    pointCurrentLabel.text = @"";
                }
                if(pointCurrentValue>pointNextLevelValue){
                    pointsProgressImage.frame = CGRectMake(0, 0, DeviceWidth-2*LEFTLEFTSET, 10);
                }else{
                    pointsProgressImage.frame = CGRectMake(0, 0, (DeviceWidth-2*LEFTLEFTSET)*(pointCurrentValue*1.0/pointNextLevelValue), 10);
                }
                
                pointCurrentLabel.frame = CGRectMake(pointsProgressImage.endPointX+LEFTLEFTSET, pointsProgressDefaultImage.endPointY+2, 30, 10);
                pointNextLevelLabel.text = [NSString stringWithFormat:@"%d",pointNextLevelValue];
                
                //                NSMutableAttributedString *AttributedStr = [[NSMutableAttributedString alloc]initWithString:pointCurrentValueString];
                //                NSRange range = [pointCurrentValueString rangeOfString:[NSString stringWithFormat:@"%d",pointCurrentValue]];
                //                [AttributedStr addAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:241.0f green:15.0f blue:51.0f alpha:1.0f],NSFontAttributeName:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:28]} range:NSMakeRange(range.location, range.length)];
                pointNowLabel.text  = pointCurrentValueString;
                //                pointNowLabel.attributedText = AttributedStr;
                
                
                //                NSMutableAttributedString *AttributedStrOne = [[NSMutableAttributedString alloc]initWithString:pointNextLevelValueString];
                //                NSRange rangeOne = [pointNextLevelValueString rangeOfString:[NSString stringWithFormat:@"%d",pointNextLevelValue]];
                //                [AttributedStrOne addAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14]} range:NSMakeRange(rangeOne.location, rangeOne.length)];
                //                nextRedeemLabel.attributedText = AttributedStrOne;
                nextStepRewardLabel.text = nextStepRewardString;
                
                //                NSString *redeemsDescriptionString = [NSString stringWithFormat: @"Each reservation is honored with %d points. You can redeem your points to get cashback vouchers to be used on your next reservation.",pointsPerBooking];
                //                redeemsDescriptionLabel.text = redeemsDescriptionString;
                nextRedeemLabel.text = [NSString stringWithFormat:@"of %d",pointNextLevelValue];
                
                
                NSString *redeemsDescriptionString =  [NSString stringWithFormat:@"%@ more points for next redemption",[self subV1:[NSString stringWithFormat:@"%d",pointNextLevelValue] v2:pointCurrentValueString]];
                CGSize expectSize = [redeemsDescriptionString sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12] constrainedToSize:CGSizeMake(DeviceWidth-30, 60)];
                
                differenceLabel.frame = CGRectMake(LEFTLEFTSET, pointsProgressDefaultImage.endPointY +10, DeviceWidth - 30, expectSize.height);
                differenceLabel.text = redeemsDescriptionString;// @"625 more points for next redemption";
                differenceLabel.textAlignment = NSTextAlignmentCenter;
                
                //                nextRedeemLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14];
                //                nextRedeemLabel.textColor = [UIColor colorWithRed:241.0f green:15.0f blue:51.0f alpha:1.0f];
            }
            [[LoadingView sharedLoadingView]stopLoading];
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        [[LoadingView sharedLoadingView]stopLoading];
        NSLog(@"updateRewardView error:%@",error.description);
    }];
}

- (NSString *)subV1:(NSString *)v1 v2:(NSString *)v2 {
    CGFloat result = [v1 floatValue] - [v2 floatValue];
    return [NSString stringWithFormat:@"%.2f", result];
}


@end
