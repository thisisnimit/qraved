//
//  OtherProfileViewController.m
//  Qraved
//
//  Created by lucky on 16/3/23.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "OtherProfileViewController.h"

#import "ProfileView.h"
#import "LoadingView.h"
#import "AccountView.h"
#import "UIConstants.h"
#import "UIColor+Helper.h"
#import "UIColor+Hex.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "ProfileUtil.h"
#import "IMGUser.h"
#import "IMGMyList.h"
#import "IMGReservation.h"
#import "ListHandler.h"
#import "Label.h"
#import "SettingViewController.h"
#import "PointRewardsViewController.h"
#import "NewListController.h"
#import "MyListRestaurantsViewController.h"
#import "ProfileBookingViewController.h"
#import "UserReviewsViewController.h"
#import "MyListEmptyController.h"
#import "AppDelegate.h"
#import "NotificationsViewController.h"
#import "NotificationHandler.h"
#import "ZYQAssetPickerController.h"
#import "PostDataHandler.h"
#import "EditProfileViewController.h"
#import "AvatarImageViewController.h"
#import "JourneyView.h"
#import "ReviewPublishViewController.h"
#import "RestaurantsSelectionViewController.h"
#import "DetailViewController.h"
#import "AlbumViewController.h"
#import "MapViewController.h"
#import "TrackHandler.h"
#import "IMGDish.h"
#import "SendFeedbackViewController.h"
#import "UIScrollView+Helper.h"

//new
#import "HJDefaultTabViewBar.h"
#import "HJTabViewControllerPlugin_TabViewBar.h"
#import "HJTabViewControllerPlugin_HeaderScroll.h"
#import "DLUserHeaderView.h"
#import "IMGProfileModel.h"
#import "ProfileHandler.h"
#import "PersonalLeftViewController.h"
#import "PersonalMiddleViewController.h"
#import "PersonalRightViewController.h"
#import "PersonalSettingviewController.h"

@interface OtherProfileViewController ()<HJTabViewControllerDataSource, HJTabViewControllerDelagate, HJDefaultTabViewBarDelegate, ZYQAssetPickerControllerDelegate, UINavigationControllerDelegate>
//<ProfileDelegate,UITextFieldDelegate,ZYQAssetPickerControllerDelegate,NewListDelegate,JourneyViewDelegate,UIScrollViewDelegate>
//{
//    HMSegmentedControl *tab;
//    ProfileView *profileView;
//    AccountView *accountView;
//    IMGMyList *currentMyList;
//    IMGUser *user;
//    NSInteger selected;
//    int offset;
//    int max;
//    NSInteger total;
//    BOOL hasData;
//    UIView *notificationView;
//    UILabel *notificationNumberLabel;
//    NSNumber *_notificationCount;
//    
//    UIAlertView *alert;
//    JourneyView* journeyView;
//    
//    UIButton *filterBtn;
//    BOOL checkedAll;
//    BOOL checkedBooking;
//    BOOL checkedReview;
//    BOOL checkedRate;
//    BOOL checkedPhoto;
//    BOOL isShowAccount;
//    UIView *statusBar;
//}
{
    IMGUser  *user;
    BOOL isChangeAvatar;
    BOOL ischangeBackimage;
    BOOL isSelecteImage;
    BOOL isfinishiUploadPhoto;
    BOOL isFinishUploadInfomation;

}
@property(nonatomic,strong)DLUserHeaderView *userHeaderView;

@property(nonatomic,strong)IMGProfileModel * profileModel;

@property (nonatomic, strong)  UIView *headerView;
@property (nonatomic, strong)  UIImageView  *backGroundImageView;
@property (nonatomic, strong)  UIButton  *btnSetting;
@property (nonatomic, strong)  UIButton  *btnBell;
@property (nonatomic, strong)  UIImageView  *iconImageView;
@property (nonatomic, strong)  UILabel  *nameLabel;
@property (nonatomic, strong)  UILabel  *compayLabel;

@end

@implementation OtherProfileViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self loadData];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.tabDataSource = self;
    self.tabDelegate = self;
    self.headerZoomIn = NO;
    HJDefaultTabViewBar *tabViewBar = [HJDefaultTabViewBar new];
    tabViewBar.delegate = self;
    HJTabViewControllerPlugin_TabViewBar *tabViewBarPlugin = [[HJTabViewControllerPlugin_TabViewBar alloc] initWithTabViewBar:tabViewBar delegate:nil];
    [self enablePlugin:tabViewBarPlugin];
    
    [self enablePlugin:[HJTabViewControllerPlugin_HeaderScroll new]];
    
    self.title = @"";
    
    user = [IMGUser currentUser];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToPhoto) name:@"goToPhotos" object:nil];
    
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:user.userId forKey:@"targetUserID"];
    [[Amplitude instance] logEvent:@"RC – View User Profile Summary Tab" withEventProperties:eventProperties];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:animated];

        [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];

}
- (void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];


        [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];

}

- (void)loadData{
    
    if (self.isOtherUser == NO) {
        user = [IMGUser currentUser];
        if (user.userId!=nil) {
            [[LoadingView sharedLoadingView] startLoading];
            [ProfileHandler getSelfProfile:@{@"userId":user.userId,@"t":user.token} andSuccessBlock:^(IMGProfileModel *model) {
                [[LoadingView sharedLoadingView] stopLoading];
                _profileModel = model;
                
                [self.backGroundImageView sd_setImageWithURL:[NSURL URLWithString:[_profileModel.coverImage returnFullImageUrl]] placeholderImage:[UIImage imageNamed:@"p_headerBackground"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    
                }];
                [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:[_profileModel.avatar returnFullImageUrl]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    
                }];
                self.nameLabel.text = _profileModel.userName;
                self.compayLabel.text = _profileModel.des;
                [self.userHeaderView.compayLabel sizeToFit];
                
            } anderrorBlock:^{
                [[LoadingView sharedLoadingView] stopLoading];
            }];
        }

    }else{
    
        if (_otherUser.userId != nil) {
            [[LoadingView sharedLoadingView] startLoading];
            [ProfileHandler getEditProfile:@{@"targetUserId":_otherUser.userId} andSuccessBlock:^(IMGProfileModel * model) {
//                NSLog(@"%@", twoDict);
                _profileModel = model;
                [self.backGroundImageView sd_setImageWithURL:[NSURL URLWithString:[_profileModel.coverImage returnFullImageUrl]] placeholderImage:[UIImage imageNamed:@"p_headerBackground"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    
                }];
                [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:[_profileModel.avatar returnFullImageUrl]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    
                }];
//                self.nameLabel.text = _profileModel.userName;
//                self.compayLabel.text = @"Graduated from the University of Newcastle as well as the blogging course in London (where he finally started to feel like her hobby is to blog about food).";
                self.nameLabel.text = [NSString stringWithFormat:@"%@-%@",_profileModel.firstName, _profileModel.lastName];//
                self.compayLabel.text = _profileModel.des;
                [self.userHeaderView.compayLabel sizeToFit];
                
            } anderrorBlock:^{
                
                
            }];
        }
    
    }
    
}

- (NSInteger)numberOfTabForTabViewBar:(HJDefaultTabViewBar *)tabViewBar {
    return [self numberOfViewControllerForTabViewController:self];
}

- (id)tabViewBar:(HJDefaultTabViewBar *)tabViewBar titleForIndex:(NSInteger)index {
    if (index == 0) {
        return @"Summary";
    }
    if (index == 1) {
        return @"Journey";
    }
    if (index == 2) {
        //        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:@"网易云 5"];
        //        [attString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(3, 2)];
        
        return @"Photo";
    }
    return nil;
}
-(void)goToPhoto{
    
    [self scrollToIndex:2 animated:NO];
    
}
- (void)tabViewBar:(HJDefaultTabViewBar *)tabViewBar didSelectIndex:(NSInteger)index {
    BOOL anim = labs(index - self.curIndex) > 1 ? NO: YES;
    [self scrollToIndex:index animated:anim];
}

- (NSInteger)numberOfViewControllerForTabViewController:(HJTabViewController *)tabViewController {
    return 3;
}

- (UIViewController *)tabViewController:(HJTabViewController *)tabViewController viewControllerForIndex:(NSInteger)index {
    if ( index == 0) {
        PersonalLeftViewController *PersonalLeftV = [PersonalLeftViewController new];
        //        vc.index = index;
        PersonalLeftV.isSelf = self.isOtherUser;
        PersonalLeftV.otherUser = self.otherUser;
        
        return PersonalLeftV;
    }else if (index == 1){
        
        PersonalMiddleViewController *PersonalMiddleV = [PersonalMiddleViewController new];
        PersonalMiddleV.isSelf = self.isOtherUser;
        PersonalMiddleV.otherUser = self.otherUser;
        
        return PersonalMiddleV;
    }else if (index == 2){
        
        PersonalRightViewController *PersonalRightV = [PersonalRightViewController new];
        PersonalRightV.isSelf = self.isOtherUser;
        PersonalRightV.otherUser = self.otherUser;
        return PersonalRightV;
    }
    return nil;
}

- (UIView *)tabHeaderViewForTabViewController:(HJTabViewController *)tabViewController {
    
    self.headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 300)];
    self.headerView.backgroundColor = [UIColor whiteColor];
    
    self.backGroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 125)];
    self.backGroundImageView.backgroundColor = [UIColor whiteColor];
    self.backGroundImageView.userInteractionEnabled = YES;
    
    [self.headerView addSubview:self.backGroundImageView];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(15, 30, 22, 22);
    [backBtn setImage:[UIImage imageNamed:@"ic_back_guide@2x"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.backGroundImageView addSubview:backBtn];
    
    if (_isOtherUser == NO) {
        self.btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btnSetting.frame = CGRectMake(DeviceWidth - 37, 30, 22, 22);
        [self.btnSetting setImage:[UIImage imageNamed:@"settingIcon"] forState:UIControlStateNormal];
        [self.btnSetting addTarget:self action:@selector(settingBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self.backGroundImageView addSubview:self.btnSetting];
        
        self.btnBell = [UIButton buttonWithType:UIButtonTypeCustom];
        self.btnBell.frame = CGRectMake(DeviceWidth - 98, 30, 22, 22);
        [self.btnBell setImage:[UIImage imageNamed:@"notifiIcon"] forState:UIControlStateNormal];
        [self.btnBell addTarget:self action:@selector(bellBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self.backGroundImageView addSubview:self.btnBell];
        
       
        
        
        UIButton *changePictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        changePictureBtn.frame = CGRectMake(DeviceWidth/2+30/2, self.iconImageView.bottom - 30, 30, 30);
        [changePictureBtn addTarget:self action:@selector(changePictureBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [changePictureBtn setImage:[UIImage imageNamed:@"ic_change_photo"] forState:UIControlStateNormal];
        
        changePictureBtn.clipsToBounds = YES;
        changePictureBtn.layer.cornerRadius = changePictureBtn.height/2;
        changePictureBtn.backgroundColor = [UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1];
        [changePictureBtn setTitleColor:[UIColor colorWithRed:149/255.0 green:149/255.0 blue:149/255.0 alpha:1] forState:UIControlStateNormal];
        [self.headerView addSubview:changePictureBtn];
        
        
        UIButton *btnChangeBackImage = [UIButton buttonWithType:UIButtonTypeCustom];
        btnChangeBackImage.frame = CGRectMake(DeviceWidth - 100, self.backGroundImageView.height - 20, 100, 20);
        [btnChangeBackImage setImage:[UIImage imageNamed:@"changeImageBtnIcon"] forState:UIControlStateNormal];
        [btnChangeBackImage addTarget:self action:@selector(btnChangeBackImageClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.backGroundImageView addSubview:btnChangeBackImage];

    }
    self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.backGroundImageView.height - 40, 90, 90)];
    self.iconImageView.centerX = self.backGroundImageView.centerX;
    //self.iconImageView.backgroundColor = [UIColor redColor];
    self.iconImageView.layer.cornerRadius = 45;
    self.iconImageView.layer.masksToBounds = YES;
    
    
    
    [self.backGroundImageView addSubview:self.iconImageView];
    
    self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.iconImageView.bottom +5, DeviceWidth, 20)];
    self.nameLabel.centerX = self.iconImageView.centerX;
    self.nameLabel.text  =@"test";
    self.nameLabel.font = [UIFont systemFontOfSize:20];
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    self.nameLabel.textColor = [UIColor color333333];
    [self.headerView addSubview:self.nameLabel];
    
    self.compayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.nameLabel.bottom +5, DeviceWidth - 30, 60)];
    self.compayLabel.centerX = self.iconImageView.centerX;
    self.compayLabel.text  =@"test";
    self.compayLabel.font = [UIFont systemFontOfSize:12];
    self.compayLabel.numberOfLines = 3;
    self.compayLabel.textColor = [UIColor colorLineGray];
    [self.headerView addSubview:self.compayLabel];
    
    return self.headerView;
}

- (CGFloat)tabHeaderBottomInsetForTabViewController:(HJTabViewController *)tabViewController {
    return HJTabViewBarDefaultHeight + CGRectGetMaxY(self.navigationController.navigationBar.frame);
}

- (UIEdgeInsets)containerInsetsForTabViewController:(HJTabViewController *)tabViewController {
    return UIEdgeInsetsMake(0, 0, CGRectGetHeight(self.tabBarController.tabBar.frame) , 0);
}
#pragma mark - DLUserHeaderViewDelegate
//NSLog(@"点击切换背景图片");
-(void)btnChangeBackImageClick:(UIButton *)headerView
{
    isSelecteImage = YES;
    NSLog(@"切换背景图");
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] initWithRestaurantTitle:restaurant.title andIsFromUploadPhoto:NO andIsUploadMenuPhoto:NO andCurrentRestaurantId:restaurant.restaurantId];
    picker.maximumNumberOfSelection = 1;
    picker.assetsFilter = [ALAssetsFilter allPhotos];
    picker.showEmptyGroups=NO;
    picker.delegate = self;
    picker.isFromUploadPhoto = NO;
    picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
            NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
            return duration >= 5;
        } else {
            return YES;
        }
    }];
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
    
}
//fanhui
-(void)backBtn:(UIButton *)backBtn{

    [self.navigationController popViewControllerAnimated:YES];
}
// touxiang
- (void)changePictureBtnClicked:(UIButton *)btn{
    
    isSelecteImage = NO;
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] initWithRestaurantTitle:restaurant.title andIsFromUploadPhoto:NO andIsUploadMenuPhoto:NO andCurrentRestaurantId:restaurant.restaurantId];
    picker.maximumNumberOfSelection = 1;
    picker.assetsFilter = [ALAssetsFilter allPhotos];
    picker.showEmptyGroups=NO;
    picker.delegate = self;
    picker.isFromUploadPhoto = NO;
    picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
            NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
            return duration >= 5;
        } else {
            return YES;
        }
    }];
    
    [self presentViewController:picker animated:YES completion:NULL];
}

//NSLog(@"设置");
- (void)settingBtn:(UIButton *)setting{
    PersonalSettingviewController *PersonalSettingv = [[PersonalSettingviewController alloc] init];
    [self.navigationController pushViewController:PersonalSettingv animated:YES];
    
}
//NSLog(@"通知");
- (void)bellBtn:(UIButton *)bell{
    
    NotificationsViewController *NotificationsV = [[NotificationsViewController alloc] init];
    [self.navigationController pushViewController:NotificationsV animated:YES];
    
}
-(void)wallPageDidChooseWallPager:(UIImage *)image
{
    self.userHeaderView.userImageView.image = image;
}
-(UIImage *) imageCompressForSize:(UIImage *)sourceImage targetSize:(CGSize)size{
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = size.width;
    CGFloat targetHeight = size.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0, 0.0);
    if(CGSizeEqualToSize(imageSize, size) == NO){
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        if(widthFactor > heightFactor){
            scaleFactor = widthFactor;
        }
        else{
            scaleFactor = heightFactor;
        }
        scaledWidth = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        if(widthFactor > heightFactor){
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }else if(widthFactor < heightFactor){
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    UIGraphicsBeginImageContext(size);
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    [sourceImage drawInRect:thumbnailRect];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    if(newImage == nil){
        NSLog(@"scale image fail");
    }
    
    UIGraphicsEndImageContext();
    
    return newImage;
    
}
#pragma mark - ZYQAssetPickerController Delegate
-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    
    if (isSelecteImage == YES) {
        ischangeBackimage = YES;
        [self requestSaveUser];
        if (assets.count>0) {
            ALAsset *asset=assets[0];
            [self.backGroundImageView setImage:[self imageCompressForSize:[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage] targetSize:self.backGroundImageView.frame.size]];
            [picker dismissViewControllerAnimated:YES completion:^{
                ischangeBackimage = YES;
            }];
        }
        
        
    }else{
        isChangeAvatar = YES;
        [self requestSaveUser];
        if (assets.count>0) {
            ALAsset *asset=assets[0];
            //            [avatarLinkButton setImage:[self imageCompressForSize:[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage] targetSize:avatarLinkButton.frame.size]forState:UIControlStateNormal];
            [self.iconImageView setImage:[self imageCompressForSize:[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage] targetSize:self.iconImageView.frame.size]];
            [picker dismissViewControllerAnimated:YES completion:^{
                isChangeAvatar = YES;
            }];
        }
    }
    
    
    
    
}
-(void)requestSaveUser{
    [[LoadingView sharedLoadingView] startLoading];
    user = [IMGUser currentUser];
    if (user.userId == nil || user.token == nil){
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    if ([user.userId isEqualToNumber:(user.userId)]) {
        user.token = user.token;
    }
    if (isChangeAvatar) {
        [[PostDataHandler sharedPostData] postImage:self.iconImageView.image andTitle:@"editPofileIcon" andDescription:@"edit my profile icon" andSuccessBlock:^(id postDic){
            if ([user.userId isEqualToNumber:([IMGUser currentUser].userId)]) {
                user.token = [IMGUser currentUser].token;
            }
            NSDictionary *parameters = @{@"t":user.token,@"userID":user.userId,@"imageUrl":[postDic objectForKey:@"imgPath"]};
            [[IMGNetWork sharedManager] POST:@"user/updateUserAvatar" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSString *exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
                if([exceptionMsg isLogout]){
                    [self dismissViewControllerAnimated:YES completion:^{
                        
                    }];
                    [[LoadingView sharedLoadingView] stopLoading];
                    [[IMGUser currentUser]logOut];
                    [[AppDelegate ShareApp] goToPage:0];
                    [[AppDelegate ShareApp] goToLoginController];
                    return;
                }
                
                if ([[responseObject objectForKey:@"status"] isEqualToString:@"success"]) {
                    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
                    [shared setObject:[postDic objectForKey:@"imgPath"] forKey:@"imgUrl"];
                    
                    [shared synchronize];
                }
                
                NSString *path_sandox = NSHomeDirectory();
                //设置一个图片的存储路径
                NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/avastar.png"];
                //把图片直接保存到指定的路径（同时应该把图片的路径imagePath存起来，下次就可以直接用来取）
                [UIImagePNGRepresentation(self.iconImageView.image) writeToFile:imagePath atomically:YES];
                [[LoadingView sharedLoadingView]stopLoading];
                
                isfinishiUploadPhoto = YES;
                [[LoadingView sharedLoadingView] stopLoading];
                if (isFinishUploadInfomation) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [[LoadingView sharedLoadingView] stopLoading];
                
            }];
        } andFailedBlock:^(NSError *error){
        }];
        
    }
    if (ischangeBackimage) {
        [[PostDataHandler sharedPostData] postbackImage:self.backGroundImageView.image andTitle:@"editPofilebackicon" andDescription:@"edit my profile back icon" andSuccessBlock:^(id postDic){
            if ([user.userId isEqualToNumber:([IMGUser currentUser].userId)]) {
                user.token = [IMGUser currentUser].token;
            }
            NSDictionary *parameters = @{@"t":user.token,@"userId":user.userId,@"imageUrl":[postDic objectForKey:@"imgPath"]};
            [[IMGNetWork sharedManager] POST:@"user/updateCoverImage" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSString *exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
                if([exceptionMsg isLogout]){
                    [self dismissViewControllerAnimated:YES completion:^{
                        
                    }];
                    [[LoadingView sharedLoadingView] stopLoading];
                    [[IMGUser currentUser]logOut];
                    [[AppDelegate ShareApp] goToPage:0];
                    [[AppDelegate ShareApp] goToLoginController];
                    return;
                }
                
                if ([[responseObject objectForKey:@"status"] isEqualToString:@"success"]) {
                    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
                    [shared setObject:[postDic objectForKey:@"imgPath"] forKey:@"imgUrl"];
                    
                    [shared synchronize];
                }
                
                NSString *path_sandox = NSHomeDirectory();
                //设置一个图片的存储路径
                NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/coverImage.png"];
                //把图片直接保存到指定的路径（同时应该把图片的路径imagePath存起来，下次就可以直接用来取）
                [UIImagePNGRepresentation(self.backGroundImageView.image) writeToFile:imagePath atomically:YES];
                [[LoadingView sharedLoadingView]stopLoading];
                
                isfinishiUploadPhoto = YES;
                [[LoadingView sharedLoadingView] stopLoading];
                if (isFinishUploadInfomation) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
            }failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [[LoadingView sharedLoadingView] stopLoading];
                
            }];
            
            
        } andFailedBlock:^{
            
            
        }];
        
    }
    
}



























@end
