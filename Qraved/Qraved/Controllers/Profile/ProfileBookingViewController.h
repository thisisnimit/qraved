//
//  ProfileBookingViewController.h
//  Qraved
//
//  Created by System Administrator on 10/21/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//


#import "BaseChildViewController.h"
#import "UIPopoverListView.h"
#import "ProfileDelegate.h"

@protocol ProfileBookingViewControllerDelegate <NSObject>

-(void)cancelUpcomingBooking:(NSInteger)reservationId;

@end
@interface ProfileBookingViewController : BaseChildViewController<UIPopoverListViewDataSource,UIPopoverListViewDelegate,ProfileDelegate>

@property(nonatomic,weak)id<ProfileBookingViewControllerDelegate>profileBookingViewControllerDelegate;

@end
