//
//  AvatarImageViewController.m
//  Qraved
//
//  Created by apple on 16/8/26.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "AvatarImageViewController.h"
 
#import "UIImageView+Helper.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Resize.h"

@interface AvatarImageViewController ()

@end

@implementation AvatarImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.avatarImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
    self.avatarImageView.userInteractionEnabled = YES;

    if (self.avatarImage) {
        self.avatarImageView.image = self.avatarImage;
    }else{
        self.avatarImageView.image = [UIImage imageNamed:@"headDefault.jpg"];
    }
    self.avatarImageView.contentMode = UIViewContentModeScaleAspectFit;
    

    UITapGestureRecognizer *tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(avatarImageViewClicked)];
    [self.avatarImageView addGestureRecognizer:tap];
    [self.view addSubview:self.avatarImageView];
}

-(void)avatarImageViewClicked{
    [self dismissViewControllerAnimated:NO completion:nil];
}
 

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
