//
//  UserViewController.h
//  Qraved
//
//  Created by System Administrator on 10/19/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "HJTabViewController.h"
#import "UIPopoverListView.h"
#import "NotificationsViewController.h"

@interface ProfileViewController : HJTabViewController

//@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,copy) NSString *amplitude;
//@property(nonatomic,strong) UIScrollView *contentScrollView;
@property (nonatomic,assign) BOOL isFromTabMenu;
@property (nonatomic,assign) BOOL isFromHotKey;
@property (nonatomic,assign) BOOL isSelf;
//-(void)setCurrentTabIndex:(NSUInteger)index;


@end

