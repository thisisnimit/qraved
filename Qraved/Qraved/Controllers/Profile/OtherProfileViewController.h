//
//  OtherProfileViewController.h
//  Qraved
//
//  Created by lucky on 16/3/23.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "UIPopoverListView.h"
#import "NotificationsViewController.h"
#import "IMGUser.h"

@interface OtherProfileViewController : HJTabViewController
//<UIPopoverListViewDataSource,UIPopoverListViewDelegate>

@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,retain) IMGUser *otherUser;
@property (nonatomic,copy) NSString *amplitude;
@property(nonatomic,strong) UIScrollView *contentScrollView;

//@property (nonatomic,assign) BOOL isFromTabMenu;
@property (nonatomic,assign) BOOL isOtherUser;  //判断跳转是自己还是其他人  no 是自己   yes 是别人
@end
