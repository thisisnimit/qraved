//
//  DraftViewController.m
//  Qraved
//
//  Created by apple on 16/12/26.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "DraftViewController.h"
#import "UIImageView+Helper.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "DLStarRatingControl.h"
#import "DetailDataHandler.h"
#import "ReviewPublishViewController.h"
#import "IMGRestaurant.h"

#define LEFTLEFTSET 15

@interface DraftViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation DraftViewController{
    NSMutableArray *draftArry;
    NSMutableArray *uploadPhottDraftArry;
    UITableView *tableview;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    draftArry = [[NSUserDefaults standardUserDefaults] objectForKey:@"reviewDrafts"];
    uploadPhottDraftArry = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"uploadPhotoDraft"]];

    tableview = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-44) style:UITableViewStylePlain];
    tableview.delegate = self;
    tableview.dataSource = self;
    tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tableview];
    [self addBackBtn];
    self.navigationItem.title=@"Draft";

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    draftArry = [[NSUserDefaults standardUserDefaults] objectForKey:@"reviewDrafts"];
    uploadPhottDraftArry = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"uploadPhotoDraft"]];
    [tableview reloadData];
}
 
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return draftArry.count+uploadPhottDraftArry.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"draftCell"];
    if (cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"draftCell"];
    }
    for (UIView*view in cell.subviews) {
        [view removeFromSuperview];
    }
    
    if (draftArry.count-1>=indexPath.row&&draftArry.count>0) {
        NSString *restaurantImageUrl = [draftArry[indexPath.row] objectForKey:@"restaurantImageUrl"];
        NSURL *imageUrl ;
        if ([restaurantImageUrl hasPrefix:@"http://"]||[restaurantImageUrl hasPrefix:@"https://"])
        {
            imageUrl =  [NSURL URLWithString:restaurantImageUrl];
        }
        else
        {
            imageUrl = [NSURL URLWithString:[restaurantImageUrl returnFullImageUrl]];
            
        }
        NSString *restaurantName = [draftArry[indexPath.row] objectForKey:@"restaurantTitle"];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(LEFTLEFTSET, LEFTLEFTSET, 80, 80)];
        [cell addSubview:imageView];
        [imageView sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
        UILabel *restaurantNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(imageView.endPointX+LEFTLEFTSET, imageView.startPointY, DeviceWidth-imageView.endPointX-2*LEFTLEFTSET, 20)];
        restaurantNameLabel.text = restaurantName;
        [restaurantNameLabel setTextColor:[UIColor colorRed]];
        [cell addSubview:restaurantNameLabel];
        UILabel *districLabel = [[UILabel alloc] initWithFrame:CGRectMake(restaurantNameLabel.startPointX, restaurantNameLabel.endPointY, restaurantNameLabel.frame.size.width-20, 20)];
        districLabel.font = [UIFont systemFontOfSize:15];
        districLabel.text = [draftArry[indexPath.row] objectForKey:@"restaurantArea"];
        [cell addSubview:districLabel];
        DLStarRatingControl *star = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(districLabel.startPointX, districLabel.endPointY, 80, 20) andStars:5 andStarWidth:15 isFractional:YES];
        int starCount = [[draftArry[indexPath.row] objectForKey:@"restaurantRatingCount"] intValue]/2.0;
        star.userInteractionEnabled = NO;
        [star setRating:starCount];
        [cell addSubview:star];
        UILabel *starLabel = [[UILabel alloc] initWithFrame:CGRectMake(star.endPointX-2, star.startPointY-3, 50, 20)];
        [starLabel setFont:[UIFont systemFontOfSize:12]];
        starLabel.text = [NSString stringWithFormat:@"(%d/5)",starCount];
        starLabel.textColor= [UIColor colorWithRed:253/255.0 green:181/255.0 blue:10/255.0 alpha:1];
        [cell addSubview:starLabel];
        UILabel *reviewDraftLabel = [[UILabel alloc] initWithFrame:CGRectMake(restaurantNameLabel.startPointX, starLabel.endPointY, restaurantNameLabel.frame.size.width, 20)];
        reviewDraftLabel.font = [UIFont systemFontOfSize:15];
        reviewDraftLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"reviewContent%@",[draftArry[indexPath.row] objectForKey:@"restaurantId"]]];
        [cell addSubview:reviewDraftLabel];
        UIImageView *arrowRight = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-20-LEFTLEFTSET, districLabel.startPointY+5, 20, 20)];
        arrowRight.image = [UIImage imageNamed:@"ArrowRight.png"];
        [cell addSubview:arrowRight];
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, imageView.endPointY+3, DeviceWidth, 1)];
        lineView.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:248/255.0 alpha:1];
        [cell addSubview:lineView];
    }else{
        if (uploadPhottDraftArry.count>0) {
            NSString *restaurantImageUrl = [uploadPhottDraftArry[indexPath.row-draftArry.count] objectForKey:@"restaurantImageUrl"];
            NSURL *imageUrl ;
            if ([restaurantImageUrl hasPrefix:@"http://"]||[restaurantImageUrl hasPrefix:@"https://"])
            {
                imageUrl =  [NSURL URLWithString:restaurantImageUrl];
            }
            else
            {
                imageUrl = [NSURL URLWithString:[restaurantImageUrl returnFullImageUrl]];
                
            }
            NSString *restaurantName = [uploadPhottDraftArry[indexPath.row-draftArry.count]  objectForKey:@"restaurantTitle"];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(LEFTLEFTSET, LEFTLEFTSET, 80, 80)];
            [cell addSubview:imageView];
            [imageView sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
            UILabel *restaurantNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(imageView.endPointX+LEFTLEFTSET, imageView.startPointY, DeviceWidth-imageView.endPointX-2*LEFTLEFTSET, 20)];
            restaurantNameLabel.text = restaurantName;
            [restaurantNameLabel setTextColor:[UIColor colorRed]];
            [cell addSubview:restaurantNameLabel];
            UILabel *districLabel = [[UILabel alloc] initWithFrame:CGRectMake(restaurantNameLabel.startPointX, restaurantNameLabel.endPointY, restaurantNameLabel.frame.size.width-20, 20)];
            districLabel.font = [UIFont systemFontOfSize:15];
            districLabel.text = [uploadPhottDraftArry[indexPath.row-draftArry.count] objectForKey:@"restaurantArea"];
            [cell addSubview:districLabel];
            DLStarRatingControl *star = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(districLabel.startPointX, districLabel.endPointY, 80, 20) andStars:5 andStarWidth:15 isFractional:YES];
            star.userInteractionEnabled = NO;
            
            int starCount = [[uploadPhottDraftArry[indexPath.row-draftArry.count] objectForKey:@"restaurantRatingCount"] intValue]/2;
            [star setRating:starCount];
            [cell addSubview:star];
            UILabel *starLabel = [[UILabel alloc] initWithFrame:CGRectMake(star.endPointX-2, star.startPointY-3, 50, 20)];
            [starLabel setFont:[UIFont systemFontOfSize:12]];
            starLabel.text = [NSString stringWithFormat:@"(%d/5)",starCount];
            starLabel.textColor= [UIColor colorWithRed:253/255.0 green:181/255.0 blue:10/255.0 alpha:1];
            [cell addSubview:starLabel];
            
            UIImageView *arrowRight = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-20-LEFTLEFTSET, districLabel.startPointY+5, 20, 20)];
            arrowRight.image = [UIImage imageNamed:@"ArrowRight.png"];
            [cell addSubview:arrowRight];
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, imageView.endPointY+3, DeviceWidth, 1)];
            lineView.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:248/255.0 alpha:1];
            [cell addSubview:lineView];

        }
       
    }
   
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if (draftArry.count-1>=indexPath.row) {
        IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
        restaurant.restaurantId = [draftArry[indexPath.row] objectForKey:@"restaurantId"];
        restaurant.title = [draftArry[indexPath.row] objectForKey:@"restaurantTitle"];
        restaurant.districtName = [draftArry[indexPath.row] objectForKey:@"restaurantArea"];
        ReviewPublishViewController *rpc = [[ReviewPublishViewController alloc] initWithRestaurant:restaurant andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
        rpc.isFromDraft = YES;
        rpc.isEdit=YES;
        [[Amplitude instance] logEvent:@"UC - Continue Draft" withEventProperties:@{@"Origin":@"User Profile"}];

        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:rpc] animated:YES completion:^{
        }];
    }else{
        IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
        restaurant.restaurantId = [uploadPhottDraftArry[indexPath.row-draftArry.count] objectForKey:@"restaurantId"];
        restaurant.title = [uploadPhottDraftArry[indexPath.row-draftArry.count]  objectForKey:@"restaurantTitle"];
        restaurant.districtName = [uploadPhottDraftArry[indexPath.row-draftArry.count] objectForKey:@"restaurantArea"];
        ReviewPublishViewController *rpc = [[ReviewPublishViewController alloc] initWithRestaurant:restaurant andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
        rpc.isUploadPhoto = YES;
        rpc.isFromDraft = YES;
        rpc.isEdit=YES;
        [[Amplitude instance] logEvent:@"UC - Continue Draft" withEventProperties:@{@"Origin":@"User Profile"}];

        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:rpc] animated:YES completion:^{
        }];
    }
}
-(UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 1)];
//    view.backgroundColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1];
    view.backgroundColor = [UIColor clearColor];
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}
@end
