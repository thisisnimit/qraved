//
//  UserViewController.m
//  Qraved
//
//  Created by System Administrator on 10/19/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "ProfileViewController.h"


#import "DLUserHeaderView.h"

#import "PersonalSettingviewController.h"
#import "NotificationsViewController.h"
#import "ProfileHandler.h"
#import "IMGProfileModel.h"
#import "LoadingView.h"

#import "PersonalLeftViewController.h"
#import "PersonalMiddleViewController.h"
#import "PersonalRightViewController.h"


#import "HJTabViewControllerPlugin_HeaderScroll.h"
#import "HJTabViewControllerPlugin_TabViewBar.h"
#import "HJDefaultTabViewBar.h"
#import "ZYQAssetPickerController.h"
#import "PostDataHandler.h"
#import "EditProfileViewController.h"

@interface ProfileViewController () <HJTabViewControllerDataSource, HJTabViewControllerDelagate, HJDefaultTabViewBarDelegate, ZYQAssetPickerControllerDelegate, UINavigationControllerDelegate>
{
    IMGUser  *user;
    BOOL isChangeAvatar;
    BOOL ischangeBackimage;
    BOOL isSelecteImage;
    BOOL isfinishiUploadPhoto;
    BOOL isFinishUploadInfomation;
}

@property(nonatomic,strong)DLUserHeaderView *userHeaderView;

@property(nonatomic,strong)IMGProfileModel * profileModel;

@property (nonatomic, strong)  UIView *headerView;
@property (nonatomic, strong)  UIImageView  *backGroundImageView;
@property (nonatomic, strong)  UIButton  *btnSetting;
@property (nonatomic, strong)  UIImageView  *iconImageView;
@property (nonatomic, strong)  UILabel  *nameLabel;
@property (nonatomic, strong)  UILabel  *compayLabel;



@end
@implementation ProfileViewController

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    //    [notificationView removeFromSuperview];
    //
    //    [[LoadingView sharedLoadingView] stopLoading];
    if (self.isFromTabMenu) {
        [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    }
    //    [statusBar removeFromSuperview];
    //    statusBar = nil;
    //    if ([AppDelegate ShareApp].ProfileNavigationController.currFrameY != 0) {
    //        CGRect frame = [AppDelegate ShareApp].ProfileNavigationController.view.frame;
    //        frame.origin.y = [[AppDelegate ShareApp].ProfileNavigationController getNavigationControllerInitY];
    //        frame.size.height = [[AppDelegate ShareApp].ProfileNavigationController getNavigationControllerInitHeight];
    //        [AppDelegate ShareApp].ProfileNavigationController.view.frame = frame;
    //    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [self loadData];
    if (self.isFromTabMenu) {
        [[self rdv_tabBarController] setTabBarHidden:NO animated:NO];
    }else{
        [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    }
    
}
//=========================================

- (void)viewDidLoad {
    [super viewDidLoad];
    //     [self loadData];
    //        [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    //        [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    //        [self.navigationController setNavigationBarHidden:NO animated:YES];
    //        self.navigationController.navigationBar.translucent=YES;
    [self initUI];
    self.view.backgroundColor = [UIColor whiteColor];
    self.isSelf = NO;
    self.title = @"";
    
    
    user = [IMGUser currentUser];
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(savePhoto:) name:@"savePhotoFromCameraNotification" object:nil];
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(savebackPhoto:) name:@"savebackPhotoFromCameraNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goToPhoto) name:@"goToPhotos" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeAvatarImage:) name:@"changeAvatar" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCoverImage:) name:@"changeCoverImage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logOutClcik) name:@"logoutNotification" object:nil];
}
- (void)logOutClcik{

    [self.backGroundImageView sd_setImageWithURL:nil placeholderImage:[UIImage imageNamed:@"p_headerBackground"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
    [self.iconImageView sd_setImageWithURL:nil placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    self.nameLabel.text = @"";
    self.compayLabel.text = @"";

}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logoutNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"changeAvatar" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"changeCoverImage" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"goToPhotos" object:nil];
}
- (void) initUI{
    
    self.tabDataSource = self;
    self.tabDelegate = self;
    self.headerZoomIn = NO;
    HJDefaultTabViewBar *tabViewBar = [HJDefaultTabViewBar new];
    tabViewBar.delegate = self;
    tabViewBar.normalColor = [UIColor color333333];
    HJTabViewControllerPlugin_TabViewBar *tabViewBarPlugin = [[HJTabViewControllerPlugin_TabViewBar alloc] initWithTabViewBar:tabViewBar delegate:nil];
    [self enablePlugin:tabViewBarPlugin];
    
    [self enablePlugin:[HJTabViewControllerPlugin_HeaderScroll new]];
}
- (void)loadData{
    
    user = [IMGUser currentUser];
    if (user.userId!=nil) {
        [[LoadingView sharedLoadingView] startLoading];
        [ProfileHandler getSelfProfile:@{@"userId":user.userId,@"t":user.token} andSuccessBlock:^(IMGProfileModel *model) {
            [[LoadingView sharedLoadingView] stopLoading];
            _profileModel = model;
            
            [self.backGroundImageView sd_setImageWithURL:[NSURL URLWithString:[_profileModel.coverImage returnFullImageUrl]] placeholderImage:[UIImage imageNamed:@"p_headerBackground"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
            }];
            
            [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:[_profileModel.avatar returnCurrentImageString]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
            }];
            self.nameLabel.text = [NSString stringWithFormat:@"%@ %@",_profileModel.firstName, _profileModel.lastName];//
            self.compayLabel.text = _profileModel.des;
            [self.userHeaderView.compayLabel sizeToFit];
            
        } anderrorBlock:^{
            [[LoadingView sharedLoadingView] stopLoading];
        }];
    }
    
}
- (NSInteger)numberOfTabForTabViewBar:(HJDefaultTabViewBar *)tabViewBar {
    return [self numberOfViewControllerForTabViewController:self];
}

- (id)tabViewBar:(HJDefaultTabViewBar *)tabViewBar titleForIndex:(NSInteger)index {
    if (index == 0) {
        return @"Summary";
    }
    if (index == 1) {
        return @"Journey";
    }
    if (index == 2) {
        //        NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:@"网易云 5"];
        //        [attString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:12] range:NSMakeRange(3, 2)];
        
        return @"Photo";
    }
    return nil;
}
-(void)goToPhoto{
    
    [self scrollToIndex:2 animated:NO];
    
}
- (void)tabViewBar:(HJDefaultTabViewBar *)tabViewBar didSelectIndex:(NSInteger)index {
    BOOL anim = labs(index - self.curIndex) > 1 ? NO: YES;
    [self scrollToIndex:index animated:anim];
}

#pragma mark -
//
//- (void)tabViewController:(HJTabViewController *)tabViewController scrollViewVerticalScroll:(CGFloat)contentPercentY {
//    self.navigationController.navigationBar.alpha = contentPercentY;
//}

- (NSInteger)numberOfViewControllerForTabViewController:(HJTabViewController *)tabViewController {
    return 3;
}

- (UIViewController *)tabViewController:(HJTabViewController *)tabViewController viewControllerForIndex:(NSInteger)index {
    if ( index == 0) {
        PersonalLeftViewController *PersonalLeftV = [PersonalLeftViewController new];
        //        vc.index = index;
        return PersonalLeftV;
    }else if (index == 1){
        
        PersonalMiddleViewController *PersonalMiddleV = [PersonalMiddleViewController new];
        
        return PersonalMiddleV;
    }else if (index == 2){
        
        PersonalRightViewController *PersonalRightV = [PersonalRightViewController new];
        
        return PersonalRightV;
    }
    return nil;
}

- (UIView *)tabHeaderViewForTabViewController:(HJTabViewController *)tabViewController {
    
    self.headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 300)];
    self.headerView.backgroundColor = [UIColor whiteColor];
    
    self.backGroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 125)];
    self.backGroundImageView.backgroundColor = [UIColor whiteColor];
    self.backGroundImageView.userInteractionEnabled = YES;
    self.backGroundImageView.contentMode = UIViewContentModeScaleAspectFill;
//    self.backGroundImageView.clipsToBounds = YES;
    
    [self.headerView addSubview:self.backGroundImageView];
    
    self.btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnSetting.frame = CGRectMake(DeviceWidth - 37, 30, 30, 30);
    [self.btnSetting setImage:[UIImage imageNamed:@"ic_profileSettings"] forState:UIControlStateNormal];
    [self.btnSetting addTarget:self action:@selector(settingBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.backGroundImageView addSubview:self.btnSetting];
    
//    self.btnBell = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.btnBell.frame = CGRectMake(DeviceWidth - 98, 30, 30, 30);
//    [self.btnBell setImage:[UIImage imageNamed:@"notifiIcon"] forState:UIControlStateNormal];
//    [self.btnBell addTarget:self action:@selector(bellBtn:) forControlEvents:UIControlEventTouchUpInside];
//    [self.backGroundImageView addSubview:self.btnBell];
    
    self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.backGroundImageView.height - 40, 90, 90)];
    self.iconImageView.centerX = self.backGroundImageView.centerX;
    self.iconImageView.contentMode = UIViewContentModeScaleAspectFill;
//    self.iconImageView.clipsToBounds = YES;
    //self.iconImageView.backgroundColor = [UIColor redColor];
    self.iconImageView.layer.cornerRadius = 45;
    self.iconImageView.layer.masksToBounds = YES;
    [self.backGroundImageView addSubview:self.iconImageView];
    
    
    UIButton *changePictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    changePictureBtn.frame = CGRectMake(DeviceWidth/2+30/2, self.iconImageView.bottom - 30, 30, 30);
    [changePictureBtn addTarget:self action:@selector(changePictureBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [changePictureBtn setImage:[UIImage imageNamed:@"ic_profileEdit"] forState:UIControlStateNormal];
    
    changePictureBtn.clipsToBounds = YES;
    changePictureBtn.layer.cornerRadius = changePictureBtn.height/2;
    changePictureBtn.layer.borderWidth = 1;
    changePictureBtn.layer.borderColor = [UIColor colorWithHexString:@"#EAEAEA"].CGColor;
    changePictureBtn.backgroundColor = [UIColor whiteColor];
    [self.headerView addSubview:changePictureBtn];
    
    
    UIButton *btnChangeBackImage = [UIButton buttonWithType:UIButtonTypeCustom];
    btnChangeBackImage.frame = CGRectMake(DeviceWidth - 100, self.backGroundImageView.height - 20, 100, 20);
    [btnChangeBackImage setImage:[UIImage imageNamed:@"changeImageBtnIcon"] forState:UIControlStateNormal];
    [btnChangeBackImage addTarget:self action:@selector(btnChangeBackImageClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.backGroundImageView addSubview:btnChangeBackImage];
    
    self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.iconImageView.bottom + 5, DeviceWidth, 20)];
    self.nameLabel.centerX = self.iconImageView.centerX;
    self.nameLabel.font = [UIFont systemFontOfSize:20];
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    self.nameLabel.textColor = [UIColor color333333];
    [self.headerView addSubview:self.nameLabel];
    
    self.compayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.nameLabel.bottom + 3, DeviceWidth - 30, 40)];
    self.compayLabel.centerX = self.iconImageView.centerX;
    self.compayLabel.textAlignment = NSTextAlignmentCenter;
    self.compayLabel.font = [UIFont systemFontOfSize:12];
    self.compayLabel.numberOfLines = 2;
    self.compayLabel.textColor = [UIColor colorLineGray];
    [self.headerView addSubview:self.compayLabel];
    
    return self.headerView;
}

- (CGFloat)tabHeaderBottomInsetForTabViewController:(HJTabViewController *)tabViewController {
    return HJTabViewBarDefaultHeight + CGRectGetMaxY(self.navigationController.navigationBar.frame);
}

- (UIEdgeInsets)containerInsetsForTabViewController:(HJTabViewController *)tabViewController {
    return UIEdgeInsetsMake(0, 0, CGRectGetHeight(self.tabBarController.tabBar.frame), 0);
}




#pragma mark - DLUserHeaderViewDelegate
//NSLog(@"click change the backgroundpicture");
-(void)btnChangeBackImageClick:(UIButton *)headerView
{
    isSelecteImage = YES;
    NSLog(@"change picture");
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] initWithRestaurantTitle:restaurant.title andIsFromUploadPhoto:NO andIsUploadMenuPhoto:NO andCurrentRestaurantId:restaurant.restaurantId];
    picker.maximumNumberOfSelection = 1;
    picker.assetsFilter = [ALAssetsFilter allPhotos];
    picker.showEmptyGroups=NO;
    picker.delegate = self;
    picker.isFromUploadPhoto = NO;
    picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
            NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
            return duration >= 5;
        } else {
            return YES;
        }
    }];
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
    
}
// touxiang
- (void)changePictureBtnClicked:(UIButton *)btn{
    
    isSelecteImage = NO;
    //    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    //    ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] initWithRestaurantTitle:restaurant.title andIsFromUploadPhoto:NO andIsUploadMenuPhoto:NO andCurrentRestaurantId:restaurant.restaurantId];
    //    picker.maximumNumberOfSelection = 1;
    //    picker.assetsFilter = [ALAssetsFilter allPhotos];
    //    picker.showEmptyGroups=NO;
    //    picker.delegate = self;
    //    picker.isFromUploadPhoto = NO;
    //    picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
    //        if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
    //            NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
    //            return duration >= 5;
    //        } else {
    //            return YES;
    //        }
    //    }];
    //
    //    [self presentViewController:picker animated:YES completion:NULL];
    [[Amplitude instance] logEvent:@"ST - Change User Profile Picture" withEventProperties:nil];
    EditProfileViewController *editProfileVC = [[EditProfileViewController alloc]init];
    editProfileVC.isFromProfile = YES;
    [self.navigationController pushViewController:editProfileVC animated:YES];
}

//NSLog(@"setting");
- (void)settingBtn:(UIButton *)setting{
    [[Amplitude instance] logEvent:@"ST - View User Setting Page" withEventProperties:nil];
    PersonalSettingviewController *PersonalSettingv = [[PersonalSettingviewController alloc] init];
    [self.navigationController pushViewController:PersonalSettingv animated:YES];
    
}
//NSLog(@"notification");
//- (void)bellBtn:(UIButton *)bell{
//    [[Amplitude instance] logEvent:@"RC - View User Notification Page" withEventProperties:nil];
//    NotificationsViewController *NotificationsV = [[NotificationsViewController alloc] init];
//    [self.navigationController pushViewController:NotificationsV animated:YES];
//
//}
-(void)wallPageDidChooseWallPager:(UIImage *)image
{
    self.userHeaderView.userImageView.image = image;
}
-(UIImage *) imageCompressForSize:(UIImage *)sourceImage targetSize:(CGSize)size{
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = size.width;
    CGFloat targetHeight = size.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0, 0.0);
    if(CGSizeEqualToSize(imageSize, size) == NO){
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        if(widthFactor > heightFactor){
            scaleFactor = widthFactor;
        }
        else{
            scaleFactor = heightFactor;
        }
        scaledWidth = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        if(widthFactor > heightFactor){
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }else if(widthFactor < heightFactor){
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    UIGraphicsBeginImageContext(size);
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    [sourceImage drawInRect:thumbnailRect];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    if(newImage == nil){
        NSLog(@"scale image fail");
    }
    
    UIGraphicsEndImageContext();
    
    return newImage;
    
}
#pragma mark - ZYQAssetPickerController Delegate
-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    
    if (isSelecteImage == YES) {
        if (assets.count>0) {
            ALAsset *asset=assets[0];
            [self.backGroundImageView setImage:[self imageCompressForSize:[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage] targetSize:self.backGroundImageView.frame.size]];
            ischangeBackimage = YES;
            isChangeAvatar = NO;
            [picker dismissViewControllerAnimated:YES completion:^{
                //ischangeBackimage = YES;
            }];
            [self requestSaveUser];
        }
        
        
    }
    else{
        
        if (assets.count>0) {
            ALAsset *asset=assets[0];
            //            [avatarLinkButton setImage:[self imageCompressForSize:[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage] targetSize:avatarLinkButton.frame.size]forState:UIControlStateNormal];
            [self.iconImageView setImage:[self imageCompressForSize:[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage] targetSize:self.iconImageView.frame.size]];
            isChangeAvatar = YES;
            ischangeBackimage = NO;
            [picker dismissViewControllerAnimated:YES completion:^{
                //isChangeAvatar = YES;
            }];
            [self requestSaveUser];
        }
    }
    
    
    
    
}
-(void)requestSaveUser{
    [[LoadingView sharedLoadingView] startLoading];
    user = [IMGUser currentUser];
    if (user.userId == nil || user.token == nil){
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    if ([user.userId isEqualToNumber:(user.userId)]) {
        user.token = user.token;
    }
    if (isChangeAvatar) {
        NSLog(@"%@",self.iconImageView.image);
        [[PostDataHandler sharedPostData] postImage:self.iconImageView.image andTitle:@"editPofileIcon" andDescription:@"edit my profile icon" andSuccessBlock:^(id postDic){
            if ([user.userId isEqualToNumber:([IMGUser currentUser].userId)]) {
                user.token = [IMGUser currentUser].token;
            }
            NSDictionary *parameters = @{@"t":user.token,@"userID":user.userId,@"imageUrl":[postDic objectForKey:@"imgPath"]};
            [[IMGNetWork sharedManager] POST:@"user/updateUserAvatar" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSString *exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
                if([exceptionMsg isLogout]){
                    [self dismissViewControllerAnimated:YES completion:^{
                        
                    }];
                    [[LoadingView sharedLoadingView] stopLoading];
                    [[IMGUser currentUser]logOut];
                    [[AppDelegate ShareApp] goToPage:0];
                    [[AppDelegate ShareApp] goToLoginController];
                    return;
                }
                
                if ([[responseObject objectForKey:@"status"] isEqualToString:@"success"]) {
                    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
                    [shared setObject:[postDic objectForKey:@"imgPath"] forKey:@"imgUrl"];
                    
                    [shared synchronize];
                }
                
                NSString *path_sandox = NSHomeDirectory();
                //设置一个图片的存储路径
                NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/avastar.png"];
                //把图片直接保存到指定的路径（同时应该把图片的路径imagePath存起来，下次就可以直接用来取）
                [UIImagePNGRepresentation(self.iconImageView.image) writeToFile:imagePath atomically:YES];
                [[LoadingView sharedLoadingView]stopLoading];
                
                isfinishiUploadPhoto = YES;
                [[LoadingView sharedLoadingView] stopLoading];
                //                if (isFinishUploadInfomation) {
                //                    [self.navigationController popViewControllerAnimated:YES];
                //                }
            }failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [[LoadingView sharedLoadingView] stopLoading];
                
            }];
        } andFailedBlock:^(NSError *error){
        }];
        
    }
    if (ischangeBackimage) {
        NSLog(@"%@",self.backGroundImageView.image);
        [[PostDataHandler sharedPostData] postbackImage:self.backGroundImageView.image andTitle:@"editPofilebackicon" andDescription:@"edit my profile back icon" andSuccessBlock:^(id postDic){
            if ([user.userId isEqualToNumber:([IMGUser currentUser].userId)]) {
                user.token = [IMGUser currentUser].token;
            }
            NSDictionary *parameters = @{@"t":user.token,@"userId":user.userId,@"imageUrl":[postDic objectForKey:@"imgPath"]};
            [[IMGNetWork sharedManager] POST:@"user/updateCoverImage" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSString *exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
                if([exceptionMsg isLogout]){
                    [self dismissViewControllerAnimated:YES completion:^{
                        
                    }];
                    [[LoadingView sharedLoadingView] stopLoading];
                    [[IMGUser currentUser]logOut];
                    [[AppDelegate ShareApp] goToPage:0];
                    [[AppDelegate ShareApp] goToLoginController];
                    return;
                }
                
                if ([[responseObject objectForKey:@"status"] isEqualToString:@"success"]) {
                    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
                    [shared setObject:[postDic objectForKey:@"imgPath"] forKey:@"imgUrl"];
                    
                    [shared synchronize];
                }
                
                NSString *path_sandox = NSHomeDirectory();
                //设置一个图片的存储路径
                NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/coverImage.png"];
                //把图片直接保存到指定的路径（同时应该把图片的路径imagePath存起来，下次就可以直接用来取）
                [UIImagePNGRepresentation(self.backGroundImageView.image) writeToFile:imagePath atomically:YES];
                [[LoadingView sharedLoadingView]stopLoading];
                
                isfinishiUploadPhoto = YES;
                [[LoadingView sharedLoadingView] stopLoading];
                //                if (isFinishUploadInfomation) {
                //                    [self.navigationController popViewControllerAnimated:YES];
                //                }
            }failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                [[LoadingView sharedLoadingView] stopLoading];
                
            }];
            
            
        } andFailedBlock:^{
            
            
        }];
        
    }
    
}

- (void)changeAvatarImage:(NSNotification *)noti{
    
    NSLog(@"%@",noti.object);
    
    self.iconImageView.image = noti.object;
    
    
}

- (void)changeCoverImage:(NSNotification *)noti{
    NSLog(@"%@",noti.object);
    self.backGroundImageView.image = noti.object;
    
}


@end
