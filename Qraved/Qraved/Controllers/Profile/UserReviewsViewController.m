//
//  UserReviewsViewController.m
//  Qraved
//
//  Created by Lucky on 15/10/22.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import "UserReviewsViewController.h"
#import "LoadingView.h"
#import "IMGUser.h"
#import "LoginViewController.h"
#import "IMGUserReview.h"
#import "UserReviewView.h"
#import "IMGDish.h"
#import "ReviewViewController.h"
#import "PhotosDetailViewController.h"
#import "DetailViewController.h"
#import "UIPopoverListView.h"
#import "ReviewPublishViewController.h"

@interface UserReviewsViewController ()<UIScrollViewDelegate,userReviewDelegate,UIPopoverListViewDataSource, UIPopoverListViewDelegate>
{
    NSMutableArray *_dataArrM;
    UIScrollView *_scrollView;
    int offset;
    int max;
    float currentPointY;
    IMGUserReview *userReviewTemp;

}
@end

@implementation UserReviewsViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"My Review list page";
    offset = 0;
    max = 20;
    self.view.backgroundColor = [UIColor whiteColor];
    [self addBackBtn];
    self.navigationItem.title=L(@"Reviews");
    [self loadMainView];
    [self initData];
}
- (void)loadMainView
{
    _scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:_scrollView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initData) name:@"ReloadUserReviewsView" object:nil];

}
- (void)back
{
    [self.navigationController popViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ReloadUserReviewsView" object:nil];
}

- (void)initData
{
    
    IMGUser *user;
    
    if (self.isOtherUser)
    {
        user = self.otherUser;
    }
    else
    {
        user=[IMGUser currentUser];
        if(user==nil || user.userId==nil || user.token==nil){
            LoginViewController *loginVC=[[LoginViewController alloc]initWithFromWhere:[NSNumber numberWithInt:1]];
            UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
            [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
            
            return;
        }
    }
    _dataArrM = [[NSMutableArray alloc] init];

    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] initWithDictionary:@{@"t":user.token,@"userID":user.userId,@"offset":[NSNumber numberWithInt:offset],@"max":[NSNumber numberWithInt:max]}];

    [[IMGNetWork sharedManager]GET:@"app/user/reviews/simple" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        
        NSArray *reviewList = [responseObject objectForKey:@"reviewList"];
        for (NSDictionary *dict in reviewList)
        {
            IMGUserReview *userReview = [[IMGUserReview alloc] init];
            [userReview setValuesForKeysWithDictionary:dict];
            
            for (NSDictionary *dishDic in [dict objectForKey:@"dishList"])
            {
                IMGDish *dish = [[IMGDish alloc] init];
                dish.dishId = [dishDic objectForKey:@"id"];
                dish.imageUrl = [dishDic objectForKey:@"imageUrl"];
                dish.title = [dishDic objectForKey:@"title"];
                dish.descriptionStr = [dishDic objectForKey:@"description"];
                dish.createTime = [dishDic objectForKey:@"createTime"];
                if (!userReview.dishListArrM)
                {
                    userReview.dishListArrM = [[NSMutableArray alloc] init];
                }
                [userReview.dishListArrM addObject:dish];
            }
            
            [_dataArrM addObject:userReview];
        }
        [self loadUserReviews];
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"userReviewsFromServer  --- error");
        
    }];

}
- (void)loadUserReviews
{
    currentPointY = 0;
    for (UIView *view in _scrollView.subviews)
    {
        [view removeFromSuperview];
    }
    
    for (IMGUserReview *userReview in _dataArrM)
    {
        UserReviewView *userReviewView = [[UserReviewView alloc] initWithReview:userReview andDish:nil andIsFromReviewViewController:NO andIsOtherUser:self.isOtherUser];
        userReviewView.frame = CGRectMake(0, currentPointY,DeviceWidth,userReviewView.reviewHeight);
        [_scrollView addSubview:userReviewView];
        userReviewView.delegate = self;
        currentPointY = currentPointY + userReviewView.reviewHeight;
    }
    _scrollView.contentSize = CGSizeMake(DeviceWidth, currentPointY + 64);
}

- (void)userReviewViewClickWithRestaurant:(IMGRestaurant *)restaurant
{
    
    ReviewViewController *reviewViewController=[[ReviewViewController alloc] initWithRestaurant:restaurant];
//    reviewViewController.rating = restaurant.ratingScore;
//    reviewViewController.amplitudeType=@"My Profile - review list";
    [self.navigationController pushViewController:reviewViewController animated:YES];
}
- (void)userReviewImagePressed:(UserReviewView *)review images:(NSArray *)images withTag:(int)tag andRestaurant:(IMGRestaurant *)restaurant
{
    
    PhotosDetailViewController *photosDetailViewController = [[PhotosDetailViewController alloc] initWithPhotosArray:images andPhotoTag:tag andRestaurant:restaurant andFromDetail:YES];
    photosDetailViewController.showMenu=NO;
    photosDetailViewController.showPage = NO;
    [self.navigationController pushViewController:photosDetailViewController animated:YES];
    

}
- (void)userReviewRestaurantClickWithRestaurant:(IMGRestaurant *)restaurant
{
    DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurant:restaurant];
    [self.navigationController pushViewController:dvc animated:YES];
}
- (void)userReviewMoreBtnClickWithIMGUserReview:(IMGUserReview *)review
{
    userReviewTemp = review;
    //CGFloat yHeight = 152.0f + 53.0f;
    CGFloat yHeight = 152.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(0, DeviceHeight-yHeight, DeviceWidth, yHeight)];
    poplistview.delegate = self;
    poplistview.tag = 99;
    poplistview.datasource = self;
    poplistview.listView.scrollEnabled = FALSE;
    [poplistview setTitle:nil];
    [poplistview show];

}
- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView cellForIndexPath:(NSIndexPath *)indexPath
{
    if (popoverListView.tag == 99)
    {
        static NSString *identifier = @"cell";
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET*2, 12, 30, 30)];
        [cell.contentView addSubview:iconImage];
        
        Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(70, 15, DeviceWidth-70, 23)
                                            andTextFont:[UIFont systemFontOfSize:15]
                                           andTextColor:[UIColor blackColor]
                                           andTextLines:0];
        
        [cell.contentView addSubview:titleLabel];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSInteger row = indexPath.row;
        if(row == 0){
            titleLabel.text = L(@"Edit Review");
            titleLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:15];
            titleLabel.textColor = [UIColor color222222];
            iconImage.image = [UIImage imageNamed:@"listEditGreen"];
        }
//        else if (row == 1)
//        {
//            titleLabel.text = L(@"Remove Review");
//            titleLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:15];
//            titleLabel.textColor = [UIColor color222222];
//            iconImage.image = [UIImage imageNamed:@"listDelete"];
//        }
        else
        {
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.textLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:18];
            cell.textLabel.textColor = [UIColor colorWithRed:23/255.0 green:127/255.0 blue:252/255.0 alpha:1];
            cell.textLabel.text = L(@"Cancel");
        }
        return cell;
    }
    return nil;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section
{
    
    return 2;
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 53.0f;
}
-(void)popoverListView:(UIPopoverListView *)popoverListView  didSelectIndexPath:(NSIndexPath *)indexPath{
    
    if (popoverListView.tag == 99)
    {
        if (indexPath.row == 0)
        {
            IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
            restaurant.restaurantId = userReviewTemp.restaurantId;
            restaurant.title = userReviewTemp.restaurantTitle;
            restaurant.districtName = userReviewTemp.restaurantDistrict;
            ReviewPublishViewController *rpvc = [[ReviewPublishViewController alloc] initWithRestaurant:restaurant andOverallRating:[userReviewTemp.reviewScore floatValue]/2 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
            //            rpvc.userReview = userReviewTemp;
            rpvc.isEdit = YES;
            rpvc.isUpdateReview = YES;
            rpvc.amplitudeType = @"My Profile";
            rpvc.isEditReview = YES;
            [self presentViewController:[[UINavigationController alloc] initWithRootViewController:rpvc] animated:YES completion:^{

            }];
        }

        [popoverListView dismiss];
    }
}

- (void)deleteReviewForServiceWithReviewId:(NSNumber *)reviewId
{
    IMGUser *user = [IMGUser currentUser];
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",reviewId,@"reviewId", nil];
    [[IMGNetWork sharedManager]GET:@"delreview" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"deleteReviewForServiceWithReviewId  --- error");
        
    }];
    

}

- (void)userReviewWriteReviewWithIMGUserReview:(IMGUserReview *)review
{
    userReviewTemp = review;
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    restaurant.restaurantId = userReviewTemp.restaurantId;
    restaurant.title = userReviewTemp.restaurantTitle;
    ReviewPublishViewController *rpvc = [[ReviewPublishViewController alloc] initWithRestaurant:restaurant andOverallRating:[userReviewTemp.reviewScore floatValue]/2 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
    rpvc.isEdit = YES;
    rpvc.userReview = userReviewTemp;
    rpvc.isUpdateReview = YES;
    rpvc.amplitudeType = @"My Profile";
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:rpvc] animated:YES completion:^{

    }];

}
 
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
