//
//  IMGReservationContentController.h
//  Qraved
//
//  Created by Libo Liu on 10/16/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseChildViewController.h"
#import "IMGReservation.h"
#import "IMGRestaurant.h"
#import <MessageUI/MessageUI.h>

@protocol BookingDetailContentControllerDelegate <NSObject>

-(void)cancelUpcomingBooking:(NSInteger)reservationId;

@end

@interface BookingDetailContentController : BaseChildViewController<UINavigationControllerDelegate,MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate>

@property(nonatomic,assign) BOOL backToLeft;
@property(nonatomic,assign) BOOL isFromNotification;
@property(nonatomic,assign) BOOL isFromPushNotification;
@property(nonatomic,weak)id<BookingDetailContentControllerDelegate>bookingDetailContentControllerDelegate;

-(id)initWithReservation:(IMGReservation *)reservation;
- (id)initWithFromSideMenu:(BOOL)isFromSlideMenu andReservation:(IMGReservation *)resv;
@end
