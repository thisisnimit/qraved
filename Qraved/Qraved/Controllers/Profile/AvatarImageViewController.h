//
//  AvatarImageViewController.h
//  Qraved
//
//  Created by apple on 16/8/26.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AvatarImageViewController : UIViewController

@property (nonatomic,strong)UIImageView *avatarImageView;
@property(nonatomic,strong) UIImage *avatarImage;
@end
