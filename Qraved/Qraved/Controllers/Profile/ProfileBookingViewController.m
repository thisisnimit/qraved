//
//  ProfileBookingViewController.m
//  Qraved
//
//  Created by System Administrator on 10/21/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "ProfileBookingViewController.h"
#import "ProfileUtil.h"
#import "BookingView.h"
#import "IMGUser.h"
#import "Label.h"
#import "UIConstants.h"
#import "IMGReservation.h"
#import "IMGMyList.h"
#import "BookingDetailContentController.h"
#import "CustomIOS7AlertView.h"
//#import "MixpanelHelper.h"
#import "TrackHandler.h"
#import "LoadingView.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "UIView+Helper.h"
#import "UIDevice+Util.h"
#import <EventKit/EventKit.h>
#import <Social/Social.h>
#import "SendCall.h"
#import "ListHandler.h"
#import "ShareToPathView.h"
#import "NXOAuth2AccountStore.h"
#import "MapViewController.h"
#import "DetailViewController.h"
#import "AppDelegate.h"

@interface ProfileBookingViewController ()<BookingDetailContentControllerDelegate>

@end
@implementation ProfileBookingViewController{
	BookingView *bookingView;
	int offset;
	int max;
	BOOL hasHistory;
	IMGUser *user;
    IMGReservation *currentReservation;
    IMGReservation *reservationOne;
}

-(void)viewDidLoad{
	[super viewDidLoad];
    self.screenName = @"My Reservation list page";
	offset=0;
	max=10;
	hasHistory=YES;
	user=[IMGUser currentUser];
	[self setNavigation];
	[self loadUpcomingAndHistoryBooking];
	[self loadBookingView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=NO;
}

-(void)setNavigation{
	UIBarButtonItem *leftBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:NavigationBackImage] style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
    leftBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.leftBarButtonItem=leftBtn;
    self.navigationItem.title=@"Bookings";
}

-(void)loadUpcomingAndHistoryBooking{
    [[LoadingView sharedLoadingView] startLoading];
	[ProfileUtil getUpcomingAndHistoryBookings:^(NSArray *upcomings,NSArray *histories,NSNumber* historyTotal){
        [[LoadingView sharedLoadingView] stopLoading];
		bookingView.upcomingBookings=[NSMutableArray arrayWithArray:upcomings];
		bookingView.pastBookings=[NSMutableArray arrayWithArray:histories];
		offset+=max;
		if(max>=[historyTotal intValue]){
			hasHistory=NO;
		}else{
			[bookingView setRefreshViewFrame];
		}
	}];
}

-(void)loadHistoryBookings:(void(^)(BOOL hasData))block{
	[ProfileUtil getHisotryBookings:@{@"offset":[NSNumber numberWithInt:offset],@"max":[NSNumber numberWithInt:max]} block:^(NSArray *histories,NSNumber *total){
		offset+=max;
		if(offset>=[total intValue]){
			hasHistory=NO;
		}
		NSMutableArray *past=[[NSMutableArray alloc] initWithArray:bookingView.pastBookings];
		[past addObjectsFromArray:histories];
		bookingView.pastBookings=past;
		block(hasHistory);
	}];
}

-(void)loadBookingView{
	bookingView=[[BookingView alloc] initWithFrame:CGRectMake(0,0,DeviceWidth,DeviceHeight-44)];
	bookingView.delegate=self;
	[self.view addSubview:bookingView];
}

-(void)backClick{
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)goToBookingDetail:(NSIndexPath*)indexpath{
    if(indexpath.section==0){
        IMGReservation *reservation = [bookingView.upcomingBookings objectAtIndex:indexpath.row];
        BookingDetailContentController *bookingDetailContentController=[[BookingDetailContentController alloc] initWithReservation:reservation];
        bookingDetailContentController.bookingDetailContentControllerDelegate = self;
        [self.navigationController pushViewController:bookingDetailContentController animated:YES];
    }else{
        IMGReservation *reservation = [bookingView.pastBookings objectAtIndex:indexpath.row];
        BookingDetailContentController *bookingDetailContentController=[[BookingDetailContentController alloc] initWithReservation:reservation];
        bookingDetailContentController.bookingDetailContentControllerDelegate = self;

        [self.navigationController pushViewController:bookingDetailContentController animated:YES];
    }
}

-(void)goToRestaurant_v1:(UITapGestureRecognizer*)tap{
    NSInteger index=[tap.view tag];
    IMGReservation *reservation = [bookingView.upcomingBookings objectAtIndex:index];
    DetailViewController *detial=[[DetailViewController alloc] initWithRestaurantId:reservation.restaurantId];
    [self.navigationController pushViewController:detial animated:YES];
}

-(void)goToRestaurant_v2:(UITapGestureRecognizer*)tap{
    NSInteger index=[tap.view tag];
    IMGReservation *reservation = [bookingView.pastBookings objectAtIndex:index];
    DetailViewController *detial=[[DetailViewController alloc] initWithRestaurantId:reservation.restaurantId];
    [self.navigationController pushViewController:detial animated:YES];
}

-(void)telephoneButtonTapped:(UIButton*)button{
    IMGReservation *reservation = [bookingView.upcomingBookings objectAtIndex:button.tag];
    [TrackHandler trackWithUserId:user.userId andRestaurantId:reservation.restaurantId andContent:[NSDictionary dictionaryWithObject:@"0" forKey:@"type"]];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:reservation.restaurantPhone delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
    alertView.tag=1;
    [alertView show];
}

-(void)shareButtonTapped:(UIButton *)button{
    currentReservation=[bookingView.upcomingBookings objectAtIndex:button.tag];
    CGFloat yHeight = 310.0f+52.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(0, DeviceHeight-yHeight, DeviceWidth, yHeight)];
    poplistview.tag=button.tag;
    poplistview.delegate = self;
    poplistview.datasource = self;
    poplistview.listView.scrollEnabled = FALSE;
    [poplistview setTitle:L(@"Share")];
    [poplistview show];
}

-(void)addToCalendar:(UIButton *)button{
    IMGReservation *reservation = [bookingView.upcomingBookings objectAtIndex:button.tag];
    IMGRestaurant *currentRestaurant=[[IMGRestaurant alloc]init];
    NSString *sqlString=[NSString stringWithFormat:@"select * from IMGRestaurant where restaurantId=%@;",reservation.restaurantId];
    FMResultSet * resultSet2 = [[DBManager manager] executeQuery:sqlString];
    if([resultSet2 next]){
        [currentRestaurant setValueWithResultSet:resultSet2];
    }
    [resultSet2 close];
    
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    if ([eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)])
    {
        // the selector is available, so we must be on iOS 6 or newer
        [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (error){
                    // display error message here
                }else if (!granted){
                    // display access denied error message here
                }else{
                    // access granted
                    // ***** do the important stuff here *****
                    EKEvent *ekEvent  = [EKEvent eventWithEventStore:eventStore];
                    ekEvent.title     = currentRestaurant.title;
                    ekEvent.location = [currentRestaurant address];
                    
                    NSDateFormatter *tempFormatter = [[NSDateFormatter alloc]init];
                    [tempFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
                    ekEvent.startDate=[tempFormatter dateFromString:[NSString stringWithFormat:@"%@ %@",reservation.bookDate,reservation.bookTime]];
                    ekEvent.endDate=[tempFormatter dateFromString:[NSString stringWithFormat:@"%@ %@",reservation.bookDate,reservation.bookTime]];
                    
                    [tempFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
                    ekEvent.allDay = NO;
                    
                    //添加提醒
                    [ekEvent addAlarm:[EKAlarm alarmWithRelativeOffset:60.0f * -60.0f * 24]];
                    [ekEvent addAlarm:[EKAlarm alarmWithRelativeOffset:60.0f * -15.0f]];
                    
                    [ekEvent setCalendar:[eventStore defaultCalendarForNewEvents]];
                    NSError *err;
                    [eventStore saveEvent:ekEvent span:EKSpanThisEvent error:&err];
                    
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Calendar" message:@"Reservation is added to calendar." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [alert show];
                    
                }
            });
        }];
    }
}

-(void)cancelBooking:(UIButton*)button{
    reservationOne = [bookingView.upcomingBookings objectAtIndex:button.tag];
    currentReservation = [bookingView.upcomingBookings objectAtIndex:button.tag];

    NSMutableDictionary* eventDic=[[NSMutableDictionary alloc]init];
    [eventDic setValue:reservationOne.restaurantId forKey:@"Restaurant_ID"];
    [eventDic setValue:reservationOne.reservationId forKey:@"Reservation_ID"];
    [eventDic setValue:@"My Reservation page" forKey:@"Location"];

    
    [[Amplitude instance] logEvent:@"TR - Reservation Cancel Initiate" withEventProperties:eventDic];
    
    UIView *popUpView = [[UIView alloc]init];
    NSString *offerTitleStr = L(@"Are you sure want to cancel your booking?");
    CGSize maxSize = [offerTitleStr sizeWithFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
    Label *seeMenuTitle = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 64/2-(maxSize.height+4)/2, maxSize.width, maxSize.height+4) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] andTextColor:[UIColor color222222] andTextLines:0];
    seeMenuTitle.text = offerTitleStr;
    seeMenuTitle.textAlignment = NSTextAlignmentCenter;
    [popUpView addSubview:seeMenuTitle];
    
    NSArray *menusArr = @[@"We appreciate the gesture. This means other guests can get a seat if you can't attend."];
    float startY = seeMenuTitle.endPointY ;
    for (int i=0; i<menusArr.count; i++) {
        CGSize maxSize1 = [[menusArr objectAtIndex:i] sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
        
        Label *seeMenuLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, startY, DeviceWidth-4*LEFTLEFTSET, maxSize1.height+20) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15] andTextColor:[UIColor color333333] andTextLines:0];
        seeMenuLabel.textAlignment = NSTextAlignmentCenter;
        seeMenuLabel.text = [menusArr objectAtIndex:i];
        seeMenuLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [popUpView addSubview:seeMenuLabel];
        startY = seeMenuLabel.endPointY;
    }
    popUpView.frame = CGRectMake(0, 0, DeviceWidth-2*LEFTLEFTSET, startY);
    
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc]init];
    alertView.buttonTitles = @[@"No, keep it",@"Yes"];
    [alertView setContainerView:popUpView];
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, NSInteger buttonIndex) {
        if(buttonIndex==1){
            [[LoadingView sharedLoadingView] startLoading];
            IMGUser * user_ = [IMGUser currentUser];
            if(user_.userId==nil){
                //lead to login
                return;
            }
            if(user_.token==nil){
                //lead to login
                return;
            }
            NSString *newStatus=@"13";
            if([reservationOne.status isEqualToString:@"11"]){
                newStatus=@"14";
            }else if([reservationOne.status isEqualToString:@"2"]){
                newStatus=@"3";
            }
            NSDictionary * parameters = @{@"userID":user_.userId,@"t":user_.token,@"id":reservationOne.reservationId,@"status":newStatus};
            [[IMGNetWork sharedManager]POST:@"reseravtion/status/update" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
                
                NSString *exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
                if([exceptionMsg isLogout]){
                    [[LoadingView sharedLoadingView] stopLoading];
                    [[IMGUser currentUser]logOut];
                    [[AppDelegate ShareApp] goToLoginController];
                    return;
                }
                
                NSString *returnStatusString = [responseObject objectForKey:@"status"];
                if([returnStatusString isEqualToString:@"succeed"]){
                   	reservationOne.status=newStatus;
                   	NSMutableArray *past=[[NSMutableArray alloc] initWithArray:bookingView.pastBookings];
                   	[bookingView.upcomingBookings removeObjectAtIndex:button.tag];
                   	[past insertObject:reservationOne atIndex:0];
                   	bookingView.pastBookings=past;
                    NSMutableDictionary* eventdictionary=[[NSMutableDictionary alloc]init];
                    [eventdictionary setValue:reservationOne.reservationId forKey:@"Reservation_ID"];
                    [eventdictionary setValue:reservationOne.restaurantId forKey:@"Restaurant_ID"];
                    [eventdictionary setValue:@"My Reservation page" forKey:@"Location"];
                    [[Amplitude instance] logEvent:@"TR - Reservation Cancel Succeed" withEventProperties:eventdictionary];
                    if ([self.profileBookingViewControllerDelegate respondsToSelector:@selector(cancelUpcomingBooking:)]) {
                        [self.profileBookingViewControllerDelegate cancelUpcomingBooking:[reservationOne.reservationId integerValue]];
                    }
                }else{
                
                    NSMutableDictionary* eventdictionary=[[NSMutableDictionary alloc]init];
                    [eventdictionary setValue:reservationOne.reservationId forKey:@"Reservation_ID"];
                    [eventdictionary setValue:reservationOne.restaurantId forKey:@"Restaurant_ID"];
                    [eventdictionary setValue:returnStatusString forKey:@"Reason"];
                    [eventdictionary setValue:@"My Reservation page" forKey:@"Location"];
                    [[Amplitude instance] logEvent:@"TR - Reservation Cancel Failed" withEventProperties:eventdictionary];
                
                }
                [[LoadingView sharedLoadingView]stopLoading];
            } failure:^(NSURLSessionDataTask *operation, NSError *error) {
                NSLog(@"MainMenuViewController.request cancel book.error: %@",error.localizedDescription);
                NSMutableDictionary* eventdictionary=[[NSMutableDictionary alloc]init];
                [eventdictionary setValue:reservationOne.reservationId forKey:@"Reservation_ID"];
                [eventdictionary setValue:reservationOne.restaurantId forKey:@"Restaurant_ID"];
                [eventdictionary setValue:error forKey:@"Reason"];
                [eventdictionary setValue:@"My Reservation page" forKey:@"Location"];
                [[Amplitude instance] logEvent:@"TR - Reservation Cancel Failed" withEventProperties:eventdictionary];
                [[LoadingView sharedLoadingView]stopLoading];
            }];
        }
        [alertView close];
        if (buttonIndex==0) {
            NSMutableDictionary* eventDic=[[NSMutableDictionary alloc]init];
            [eventDic setValue:reservationOne.reservationId forKey:@"Reservation_ID"];
            [eventDic setValue:reservationOne.restaurantId forKey:@"Restaurant_ID"];
            [[Amplitude instance] logEvent:@"TR - Reservation Cancel Cancel" withEventProperties:eventDic];
        }

    }];
    
    [alertView setUseMotionEffects:true];
    [alertView show];
}

-(void)gotoMap:(UIButton*)button{
    currentReservation = [bookingView.upcomingBookings objectAtIndex:button.tag];
    NSString *sqlString1=[NSString stringWithFormat:@"select * from IMGReservation where reservationId=%@;",currentReservation.reservationId];
    FMResultSet * resultSet1 = [[DBManager manager] executeQuery:sqlString1];
    if([resultSet1 next]){
        [currentReservation setValueWithResultSet:resultSet1];
    }
    [resultSet1 close];
    
    NSNumber *userId = [[NSNumber alloc] init];
    if (user.userId != nil)
    {
        userId = user.userId;
    }
    else{
        userId = [NSNumber numberWithInt:0];
    }
    IMGRestaurant *restaurant=[[IMGRestaurant alloc] init];
    if ([currentReservation.restaurantLatitude intValue]==0&&[currentReservation.restaurantLongitude intValue] ==0) {
        restaurant.latitude = currentReservation.latitude;
        restaurant.longitude = currentReservation.longitude;
    }else{
        restaurant.longitude=currentReservation.restaurantLongitude;
        restaurant.latitude=currentReservation.restaurantLatitude;
    }
    
    restaurant.restaurantId=currentReservation.restaurantId;
    restaurant.title=currentReservation.restaurantName;
    restaurant.cuisineName=currentReservation.cuisineName;
    restaurant.districtName=currentReservation.districtName;
    restaurant.ratingScore=currentReservation.ratingScore;
    restaurant.priceLevel=currentReservation.priceName;
    [TrackHandler trackWithUserId:userId andRestaurantId:currentReservation.restaurantId andContent:[NSDictionary dictionaryWithObject:@"1" forKey:@"type"]];
    
    MapViewController *mapViewController=[[MapViewController alloc] initWithRestaurant:restaurant];
    
    mapViewController.fromProfile=YES;
    
    [self.navigationController pushViewController:mapViewController animated:YES];
}

-(BOOL)isValidateText:(NSString *)text {
    NSString *regex = @"[a-zA-Z0-9\\s]*";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [test evaluateWithObject:text];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==1){
        if(buttonIndex==0){
            [alertView dismissWithClickedButtonIndex:0 animated:NO];
        }else if (buttonIndex==1){
            
            [SendCall sendCall:alertView.message];
        }
    }
}

@end
