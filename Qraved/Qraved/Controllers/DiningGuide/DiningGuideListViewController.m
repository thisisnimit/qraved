//
//  DiningGuideListViewController.m
//  Qraved
//
//  Created by Lucky on 15/9/9.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//
#define REFRESH_HEADER_HEIGHT 52.0f

#import "AppDelegate.h"
#import "DiningGuideListViewController.h"
#import "DiscoverListViewController.h"
#import "UIViewController+Helper.h"
#import "UIView+Helper.h"
#import "DiningGuideUtil.h"
#import "IMGDiningGuide.h"
#import "SelectCityController.h"
#import "NSString+Helper.h"
#import "UILabel+Helper.h"
#import "DiningGuideRestaurantsViewController.h"
#import "UIColor+Helper.h"
#import "UIDevice+Util.h"
#import "LoadingView.h"
//#import "GAIDictionaryBuilder.h"
//#import "GAIFields.h"
#import <GoogleAnalytics/GAIFields.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import "UIScrollView+Helper.h"

extern BOOL NeedExpandMenu;
extern BOOL ChangeCity;

#define navigationHeight  (44+[UIDevice heightDifference])

@interface DiningGuideListViewController ()
{
    UIScrollView *_scrollView;
    NSMutableArray *_dataArr;
    IMGDiningGuide *_diningGuide;
    UIButton *backButton;
    UIView *cityView;
    int max;
    int offset;
    int doubleNum;
    int viewPointY;
    EGORefreshTableFooterView *refreshTableFooterView;
    BOOL _hasData;
    NSTimeInterval beginLoadingData;
    NSTimeInterval finishUI;
    BOOL isFreshing;
}
@end

@implementation DiningGuideListViewController
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = YES;
    [[AppsFlyerTracker sharedTracker] trackEvent:@"RC - View Dining Guide list" withValues:@{}];
    
    [AppDelegate ShareApp].isSlideDiningGuideHidden = NO;
//    [[self rdv_tabBarController] setTabBarHidden:NO animated:NO];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [cityView removeFromSuperview];
    [[LoadingView sharedLoadingView] stopLoading];
    
//    [AppDelegate ShareApp].isSlideDiningGuideHidden = YES;
//    if ([AppDelegate ShareApp].isSlideHomeHidden
//        &&[AppDelegate ShareApp].isSlideJournalHidden
//        &&[AppDelegate ShareApp].isSlideDiningGuideHidden) {
//        [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
//    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"Dining Guide list page";
    

    offset = 0;
    max = 10;
    doubleNum = 0;
    viewPointY = 0;

    _scrollView = [[UIScrollView alloc] init];
    
    _scrollView.frame = CGRectMake(0,0, DeviceWidth, DeviceHeight-44-64-20+[[AppDelegate ShareApp].homeNavigationController getNavigationBarHeight]);
    _scrollView.backgroundColor = [UIColor whiteColor];
    _scrollView.delegate = self;
    _scrollView.scrollsToTop = YES;
    [self.view addSubview:_scrollView];
    [self setRefreshViewFrame];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCity) name:CITY_SELECT_NOTIFICATION object:nil];
    [[LoadingView sharedLoadingView] startLoading];
    beginLoadingData = [[NSDate date]timeIntervalSince1970];
    [self refreshData:^(BOOL hasData){
        finishUI = [[NSDate date]timeIntervalSince1970];
       
        float duringTime = finishUI - beginLoadingData;
        duringTime = duringTime * 1000;
        int duringtime = duringTime;
        [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createTimingWithCategory:@"Dining guide list"  interval:[NSNumber numberWithInt:duringtime] name:@"" label:@""] build]];
        [[LoadingView sharedLoadingView] stopLoading];
        
    }];

//    [self setNavigation];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(clickTabMenuRefreshPage) name:NOTIFICATION_REFRESH_TAB_MENU object:nil];
}

-(void)clickTabMenuRefreshPage{
    if (isFreshing) {
        return;
    }
    [self refreshCity];
}

- (void)dealloc{
   [[NSNotificationCenter defaultCenter] removeObserver:CITY_SELECT_NOTIFICATION];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_REFRESH_TAB_MENU object:nil];
}
-(void)refreshCity{
    for (UIView *view in _scrollView.subviews){
        [view removeFromSuperview];
    }
    offset = 0;
    max = 10;
    doubleNum = 0;
    viewPointY = 0;
    [_dataArr removeAllObjects];
    [self refreshData:NULL];
}
- (void)refreshData:(void(^)(BOOL))block{
    
//    self.navigationController.navigationBar.tintColor = [UIColor clearColor];
    isFreshing = YES;
    [DiningGuideUtil getDiningGuideListWithCityId:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] andMax:[NSNumber numberWithInt:max] andOffset:[NSNumber numberWithInt:offset] andBlock:^(NSArray *dataArray,BOOL hasData) {
        _hasData = hasData;
        if (_dataArr)
        {
            [_dataArr addObjectsFromArray:dataArray];
        }
        else
        {
            _dataArr = [[NSMutableArray alloc] initWithArray:dataArray];
        }
        [self loadMainView];
        [refreshTableFooterView egoRefreshScrollViewDataSourceDidFinishedLoading:_scrollView];
        [self setRefreshViewFrame];
        if(block!=NULL){
            block(hasData);
        }
        isFreshing = NO;
        _pullTableIsLoadingMore = NO;
    } andFailure:^(NSString *description){
        _pullTableIsLoadingMore = NO;
    }];
}

//-(void)setNavigation
//{
//    // NavigationBarButtonItem *navigationBarLeftButton = [[NavigationBarButtonItem alloc ]initWithImage:[UIImage imageNamed:NavigationMenuImage] andTitle:@"" target:self action:@selector(backClick) width:43.0f offset:0-[UIDevice heightDifference]];
//    // self.navigationItem.leftBarButtonItem=navigationBarLeftButton;
//    [self loadRevealController];
//    cityView=[[UIView alloc] init];
//    cityView.userInteractionEnabled=YES;
//    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectButtonTapped:)];
//    tap.numberOfTapsRequired=1;
//    tap.numberOfTouchesRequired=1;
//    [cityView addGestureRecognizer:tap];
//    UILabel *selectedCity=[[UILabel alloc] initWithFrame:CGRectMake(50,0,DeviceWidth-97,44)];
//    selectedCity.text=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT];
//    selectedCity.numberOfLines=1;
//    selectedCity.font=[UIFont systemFontOfSize:16];
//    selectedCity.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
//    selectedCity.textColor=[UIColor whiteColor];
//    selectedCity.frame=CGRectMake(0,0,selectedCity.expectedWidth,44);
//    [cityView addSubview:selectedCity];
//    // [self.navigationController.navigationBar addSubview:selectedCity];
//
//    UIButton *downBtn=[UIButton buttonWithType:UIButtonTypeCustom];
//    UIImage *select=[UIImage imageNamed:@"select"];
//    [downBtn setBackgroundImage:select forState:UIControlStateNormal];
//    downBtn.frame = CGRectMake(selectedCity.endPointX+2, (43-select.size.height)/2, select.size.width, select.size.height);
//    downBtn.userInteractionEnabled=NO;
//    [cityView addSubview:downBtn];
//    cityView.frame=CGRectMake(50,0,selectedCity.frame.size.width+2+downBtn.frame.size.width,44);
//    [self.navigationController.navigationBar addSubview:cityView];
//
//    [self addSearchBtn];
//}

- (void)loadMainView
{
    for (int i = offset; i<_dataArr.count; i++)
    {
        IMGDiningGuide *diningGuide = (IMGDiningGuide *)[_dataArr objectAtIndex:i];
        NSURL *urlString = [NSURL URLWithString:[diningGuide.headerImage returnFullImageUrlWithWidth:DeviceWidth]];
        HomeDiningGuideView *diningGuideView = [[HomeDiningGuideView alloc] initWithImageUrl:urlString andTitle:diningGuide.pageName andNum:[NSString stringWithFormat:@"%@ restaurant%@",diningGuide.restaurantCount,[diningGuide.restaurantCount intValue]>1?@"s":@""]];
        [_scrollView addSubview:diningGuideView];
        diningGuideView.frame = CGRectMake(doubleNum, viewPointY, DeviceWidth/2-1.5, DeviceWidth/2-1.5);
        if (doubleNum == 0)
        {
            doubleNum = diningGuideView.frame.size.width + 3;
        }
        else
        {
            doubleNum = 0;
            viewPointY = viewPointY + diningGuideView.frame.size.width + 4;
        }
        _scrollView.contentSize = CGSizeMake(DeviceWidth, diningGuideView.endPointY+50);
        
        diningGuideView.tag = i;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(diningGuideImageViewSelected:)];
        [diningGuideView addGestureRecognizer:tapGesture];
        
    }

}


-(void)searchButtonTapped:(id)sender {
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:@"Dining guide list page" forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL - Search CTA" withEventProperties:eventProperties];
    
    DiscoverListViewController *discoverListViewController = [[DiscoverListViewController alloc] initWithCloseButton];
    [self.navigationController pushViewController:discoverListViewController animated:YES];
}

-(void)selectButtonTapped:(id)sender {
    SelectCityController *selectCityController = [[SelectCityController alloc] initWithSelectedIndex:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]];
    selectCityController.amplitudeType = @"Dining guide list page";
    [self.navigationController pushViewController:selectCityController animated:YES];
}

-(void)backClick
{

    [self.navigationController popViewControllerAnimated:YES];
}

- (void)diningGuideImageViewSelected:(UITapGestureRecognizer *)tap
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:_diningGuide.diningGuideId forKey:@"DiningGuide_ID"];
    [param setValue:_diningGuide.pageName forKey:@"DiningGuide_name"];
    [param setValue:@"Dining Guide list" forKey:@"Origin"];
    [[Amplitude instance] logEvent:@"RC - View Dining Guide page" withEventProperties:param];
    [[AppsFlyerTracker sharedTracker] trackEvent:@"RC - View Dining Guide page" withValues:param];
    
    
    [[Amplitude instance] logEvent:@"CL - Dining Guide List" withEventProperties:@{@"Location":@"Dining guide list page"}];
    _diningGuide = (IMGDiningGuide *)[_dataArr objectAtIndex:tap.view.tag];;
    if(_diningGuide==nil){return;}
    [self performSelectorOnMainThread:@selector(createData) withObject:nil waitUntilDone:NO];
    
}
-(void)createData
{
    DiningGuideRestaurantsViewController *diningResList = [[DiningGuideRestaurantsViewController alloc] initWithDiningGuide:_diningGuide];
    [self.navigationController pushViewController:diningResList animated:YES];
}
 
-(void)setRefreshViewFrame{
    if(_hasData){
        if(refreshTableFooterView==nil){
            refreshTableFooterView=[[EGORefreshTableFooterView alloc] initWithFrame:CGRectMake(0,DeviceHeight,DeviceWidth,66)];
            refreshTableFooterView.backgroundColor=[UIColor clearColor];
            refreshTableFooterView.delegate=self;
            [_scrollView addSubview:refreshTableFooterView];
        }
        if(_scrollView.contentSize.height>_scrollView.frame.size.height){
            refreshTableFooterView.frame=CGRectMake(0,_scrollView.contentSize.height+10,DeviceWidth,66);
        }else{
            refreshTableFooterView.frame=CGRectMake(0,_scrollView.frame.size.height+10,DeviceWidth,66);
        }
    }else if(refreshTableFooterView){
        [refreshTableFooterView removeFromSuperview];
        refreshTableFooterView=nil;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [refreshTableFooterView egoRefreshScrollViewDidScroll:scrollView];
    if (!_pullTableIsLoadingMore) {
        [scrollView scrollNavigationController:[AppDelegate ShareApp].homeNavigationController];
    }
}
-(BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView{
    return YES;
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
        refreshTableFooterView.frame=CGRectMake(0,_scrollView.contentSize.height,DeviceWidth,66);
        [refreshTableFooterView egoTempRefreshScrollViewDidEndDragging:scrollView];
}
- (void)egoRefreshTableFooterDidTriggerRefresh:(EGORefreshTableFooterView*)view{
//    if (!_hasData)
//    {
//        return;
//    }
    _pullTableIsLoadingMore = YES;

    [self loadMoreDiningGuideTable];
}


- (void)loadMoreDiningGuideTable
{
    if(offset<_dataArr.count)
    {
        offset+=max;
        [self refreshData:NULL];
    }else{
        _pullTableIsLoadingMore = NO;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
@end
