//
//  DiningGuideRestaurantsViewController.m
//  Qraved
//
//  Created by System Administrator on 11/26/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import "DiningGuideRestaurantsViewController.h"
#import "DetailViewController.h"
#import "IMGUser.h"
#import "IMGDiningGuide.h"
#import "DiningGuideUtil.h"
#import "LoadingView.h"
#import "DiningGuideActivityItemProvider.h"
#import "TwitterActivityItemProvider.h"
#import "SelectPaxViewController.h"
// share part
#import <MessageUI/MessageUI.h>
//#import "MixpanelHelper.h"

#import "UIPopoverListView.h"
#import "UIColor+Helper.h"
#import "AppDelegate.h"
#import "IMGCity.h"
#import "SpecialOffersViewController.h"
#import "IMGRestaurantOffer.h"
#import "DBManager.h"
#import "RestaurantHandler.h"
#import "RestaurantsViewController.h"
#import "IMGDiscover.h"
#import "DiningGuideRestaurantsViewCell.h"
#import "MapViewController.h"
#import "MapButton.h"

@interface DiningGuideRestaurantsViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation DiningGuideRestaurantsViewController{
	int offset;
	int max;
	BOOL hasData;
    NSNumber *restaurantCount;
	IMGUser *user;
	DiningGuideRestaurantsView *restaurantView;
    NSMutableArray *regularTimeArray;
    NSMutableArray *restaurantOfferArray;
    // share part
    UIPopoverListView *poplistview;
    UITableView *restaurantTableView;
    UILabel *navTitleLabel;
    UILabel *titleLabel;
    UIImageView *introView;
    UIButton *backButton;
    UIButton *shareButton;
    NSMutableArray *restaurantArr;
    float titleHeight;
    UIView *navgationView;
    MapViewController *mapViewController;
    MKMapView *mapView;
    UIButton *navigateButton;
    MapButton *mapBtn;
    
    IMGRestaurant *currentRestaurant;
    NSIndexPath *currentIndexPath;
    BOOL isMapSaved;
    SavedMapListView *currentSavedMapView;
}

-(instancetype)initWithDiningGuide:(IMGDiningGuide*)diningGuide{
	if(self=[super init]){
		self.diningGuide=diningGuide;
	}
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self sendManualScreenName:@"Dining Guide detail page"];
    self.view.backgroundColor=[UIColor whiteColor];
    
    [self loadMainUI];
    [self loadData];
    [self requestData];
    [self loadNav];
    [self trackGuide];
}

- (void)loadNav{
    navgationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, IMG_StatusBarAndNavigationBarHeight)];
    navgationView.backgroundColor=[UIColor clearColor];
    navgationView.userInteractionEnabled=YES;
    navgationView.layer.zPosition=999999;
    
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"ic_back_guide"] forState:UIControlStateNormal];
    backButton.frame = CGRectMake(LEFTLEFTSET, IMG_StatusBarHeight, 60,44);
    [backButton addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    backButton.imageEdgeInsets = UIEdgeInsetsMake(12, 0, 12, 48);
    
    shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareButton setImage:[UIImage imageNamed:@"ic_share_guide"] forState:UIControlStateNormal];
    shareButton.frame = CGRectMake(DeviceWidth-LEFTLEFTSET-44, IMG_StatusBarHeight, 60, 44);
    [shareButton addTarget:self action:@selector(shareButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    navTitleLabel=[[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET+60, IMG_StatusBarHeight, DeviceWidth-(LEFTLEFTSET+60)*2, 44)];
    [navTitleLabel setFont:[UIFont boldSystemFontOfSize:17]];
    navTitleLabel.text=[self.diningGuide.pageName filterHtml];
    navTitleLabel.textColor=[UIColor color333333];
    navTitleLabel.textAlignment=NSTextAlignmentCenter;
    navTitleLabel.hidden=YES;
    
    [navgationView addSubview:backButton];
    [navgationView addSubview:shareButton];
    [navgationView addSubview:navTitleLabel];
    [self.view addSubview:navgationView];
}

- (void)loadData{
    offset=0;
    max=10;
    hasData=YES;
    titleHeight = [UIDevice heightDifference];
    user=[IMGUser currentUser];
    restaurantArr = [NSMutableArray array];
}

- (void)requestData{
    [[LoadingView sharedLoadingView] startLoading];
    restaurantView.isLoading=YES;
    NSNumber *latitude=[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"];
    NSNumber *longitude=[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"];
    if(latitude==nil) latitude=[NSNumber numberWithInteger:0];
    if(longitude==nil) longitude=[NSNumber numberWithInteger:0];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:[IMGUser currentUser].userId forKey:@"userId"];
    [param setValue:self.diningGuide.diningGuideId forKey:@"diningGuideId"];
    [param setValue:[NSNumber numberWithInteger:max] forKey:@"max"];
    [param setValue:[NSNumber numberWithInteger:offset] forKey:@"offset"];
    [param setValue:latitude forKey:@"latitude"];
    [param setValue:longitude forKey:@"longitude"];
    [DiningGuideUtil getDiningGuideRestaurants:param completion:^(NSNumber *total,NSArray *restaurants,IMGDiningGuide *guide){
        
        [[LoadingView sharedLoadingView] stopLoading];
        [restaurantArr addObjectsFromArray:restaurants];
        self.diningGuide = guide;
        [restaurantTableView.mj_footer endRefreshing];
        if ([total intValue] <= restaurantArr.count) {
            restaurantTableView.mj_footer.hidden = YES;
        }else{
            restaurantTableView.mj_footer.hidden = NO;
        }
        
        if (restaurantArr.count>0) {
            mapBtn.hidden = NO;
        }else{
            mapBtn.hidden= YES;
        }
        [mapViewController setMapDatas:restaurantArr offset:0];
        [restaurantTableView reloadData];
      
        
    } failure:^(NSString *description){
      [restaurantTableView.mj_footer endRefreshing];
        [[LoadingView sharedLoadingView] stopLoading];
    }];

}
- (void)loadMore{
    offset += max;
    [self requestData];
}
- (void)loadMainUI{
    restaurantTableView=[[UITableView alloc] initWithFrame:CGRectMake(0 ,0,DeviceWidth,DeviceHeight+20) style:UITableViewStyleGrouped];
    restaurantTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    restaurantTableView.delegate=self;
    restaurantTableView.dataSource=self;
    restaurantTableView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:restaurantTableView];
    
    if (@available(iOS 11.0, *)) {
        restaurantTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    restaurantTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
    restaurantTableView.mj_footer.hidden = YES;
    
    [self createMapbutton];
    [self createMap];
}

- (void)createMapbutton{
    if (mapBtn) {
        [mapBtn removeFromSuperview];
    }
    mapBtn = [MapButton buttonWithType:UIButtonTypeCustom];
    mapBtn.frame = CGRectMake(DeviceWidth/2-45, DeviceHeight-30, 90, 40);
    mapBtn.hidden = YES;
    mapBtn.selected = NO;
    [mapBtn addTarget:self action:@selector(mapButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:mapBtn];
    [self.view bringSubviewToFront:mapBtn];
}
#pragma mark - map button click
-(void)mapButtonTapped{
    mapBtn.selected = !mapBtn.selected;
    if (mapBtn.selected) {
        
        [IMGAmplitudeUtil trackGuideWithName:@"RC - View Guide Listed Restaurant on Map" andDiningGuideId:self.diningGuide.diningGuideId andDiningGuideName:self.diningGuide.pageName andOrigin:nil];
        
        mapBtn.frame = CGRectMake(DeviceWidth/2-45, DeviceHeight-196, 90, 40);
        restaurantTableView.hidden = YES;
        mapView.hidden = NO;
        [self blackNav];
    }else{
        mapBtn.frame = CGRectMake(DeviceWidth/2-45, DeviceHeight-30, 90, 40);
        restaurantTableView.hidden = NO;
        mapView.hidden = YES;
        
        if (restaurantTableView.contentOffset.y>titleHeight) {
            [self blackNav];
        }else{
            [self whiteNav];
        }
        
    }
}

#pragma mark - map view
- (void)createMap{
    mapViewController=[[MapViewController alloc] initWithFrame:CGRectMake(0, 64, DeviceWidth, DeviceHeight-44)];
    mapViewController.fromSearchPage=YES;
    mapViewController.fromSaved = YES;
    mapViewController.restaurantsViewController=self;
    
    mapViewController.savedClick = ^(IMGRestaurant *pressRestaurant, SavedMapListView *savedMapView){
        currentRestaurant = pressRestaurant;
        currentSavedMapView = savedMapView;
        isMapSaved = YES;
        for (int i = 0; i < restaurantArr.count; i ++) {
            IMGRestaurant *currentRes = [restaurantArr objectAtIndex:i];
            if ([pressRestaurant.restaurantId isEqual:currentRes.restaurantId]) {
                currentIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
                break;
            }
        }
       // currentIndexPath = pressIndexPath;
        [self savedButtonClick];
    };
    mapView = mapViewController.mapView;
    
    mapView.frame = CGRectMake(0,64, DeviceWidth, self.view.frame.size.height-64);
    mapView.hidden = YES;
    [self.view addSubview:mapViewController.mapView];
    
    [self.view bringSubviewToFront:mapBtn];
}


- (void) insertTransparentGradient {
    UIColor *colorOne = [UIColor colorWithRed:(0/255.0)  green:(0/255.0)  blue:(0/255.0)  alpha:0.8];
    UIColor *colorTwo = [UIColor colorWithRed:(0/255.0)  green:(0/255.0)  blue:(0/255.0)  alpha:0.0];
    NSArray *colors = [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, nil];
    NSNumber *stopOne = [NSNumber numberWithFloat:0.0];
    NSNumber *stopTwo = [NSNumber numberWithFloat:1.0];
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    
    //crate gradient layer
    CAGradientLayer *headerLayer = [CAGradientLayer layer];
    
    headerLayer.colors = colors;
    headerLayer.locations = locations;
    
    headerLayer.frame = CGRectMake(0, 0, DeviceWidth, 64);
    [introView.layer insertSublayer:headerLayer atIndex:0];
}


-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexpath{
    IMGRestaurant *restaurant=[restaurantArr objectAtIndex:indexpath.row];
    return [tableView cellHeightForIndexPath:indexpath model:restaurant keyPath:@"restaurant" cellClass:[DiningGuideRestaurantsViewCell class] contentViewWidth:DeviceWidth];
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section{
    return restaurantArr.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
    return 1;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexpath{
    DiningGuideRestaurantsViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"viewedcell"];
    if(cell==nil){
        cell=[[DiningGuideRestaurantsViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"viewedcell"];
    }
    
    IMGRestaurant *restaurant=[restaurantArr objectAtIndex:indexpath.row];
    cell.restaurant=restaurant;
    
    cell.indexPath = indexpath;
    
    cell.savedClick = ^(IMGRestaurant *pressRestaurant,NSIndexPath *pressIndexPath){
        currentRestaurant = pressRestaurant;
        currentIndexPath = pressIndexPath;
        isMapSaved = NO;
        [self savedButtonClick];
    };
    
    return cell;
}

- (void)savedButtonClick{
    [CommonMethod doSaveWithRestaurant:currentRestaurant andViewController:self andCallBackBlock:^(BOOL isSaved) {
        currentRestaurant.saved = [NSNumber numberWithBool:isSaved];
        if (currentSavedMapView) {
            [currentSavedMapView refreshSavedButtonStyle:isSaved];
        }
        [self refreshCell:currentIndexPath];
        if (!isMapSaved) {
            [mapViewController setMapDatas:restaurantArr offset:0];
        }
    }];
}

- (void)refreshCell:(NSIndexPath *)indexPath{
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            if ([[restaurantTableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                return;
            }
            if(restaurantArr.count == 0) {
                return;
            }
            [restaurantTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            
        }
        
        @catch ( NSException *e ) {
            return;
        }
    });
}

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexpath{
    IMGRestaurant *restaurant=[restaurantArr objectAtIndex:indexpath.row];
    DetailViewController *detailViewController = [[DetailViewController alloc] initWithRestaurant:restaurant];
    detailViewController.amplitudeType = @"Dining Guide";
    detailViewController.amplitudeTypePage = @"DiningGuide_ID";
    detailViewController.amplitudeTypePageId = self.diningGuide.diningGuideId;
    [self.navigationController pushViewController:detailViewController animated:YES];

   
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *headerView=[UIView new];
    headerView.backgroundColor = [UIColor whiteColor];
    NSURL *urlString = [NSURL URLWithString:[self.diningGuide.headerImage returnFullImageUrlWithWidth:DeviceWidth andHeight:DeviceWidth*245/375]];
    
    introView = [UIImageView new];
    
    UIImage *placeHolderImage = [UIImage imageNamed:DEFAULT_IMAGE_STRING2];  
    [introView sd_setImageWithURL:urlString placeholderImage:placeHolderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
    [self insertTransparentGradient];
    
    titleLabel=[UILabel new];
    titleLabel.font=[UIFont boldSystemFontOfSize:24];
    titleLabel.text=[self.diningGuide.pageName filterHtml];
    titleLabel.textColor=[UIColor color333333];
    
    
    NSString *content=[[self.diningGuide.pageContent removeHTML] stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    UILabel *contentLabel=[UILabel new];
    contentLabel.font = [UIFont systemFontOfSize:14];
    contentLabel.textColor=[UIColor color999999];
    
    [headerView sd_addSubviews:@[introView,titleLabel,contentLabel]];
    
    introView.sd_layout
    .topSpaceToView(headerView,0)
    .leftSpaceToView(headerView,0)
    .rightSpaceToView(headerView,0)
    .heightIs(DeviceWidth*245/375);
    
    titleLabel.sd_layout
    .topSpaceToView(introView,5)
    .leftSpaceToView(headerView,15)
    .rightSpaceToView(headerView,15)
    .heightIs(28);
    
    contentLabel.sd_layout
    .topSpaceToView(titleLabel,5)
    .leftEqualToView(titleLabel)
    .rightEqualToView(titleLabel)
    .autoHeightRatio(0);
    
    contentLabel.numberOfLines = 0;
    
    contentLabel.text=content;
    
    UILabel *lblTitle = [UILabel new];
    
    lblTitle.font = [UIFont boldSystemFontOfSize:14];
    lblTitle.textColor = [UIColor color333333];
    [headerView addSubview:lblTitle];
    
    if (restaurantArr.count>0) {
        lblTitle.text = @"Restaurants Selection";
        lblTitle.sd_layout
        .topSpaceToView(contentLabel, 0)
        .leftEqualToView(contentLabel)
        .rightEqualToView(contentLabel)
        .heightIs(17);
    }else{
        lblTitle.text = @"";
        lblTitle.sd_layout
        .topSpaceToView(contentLabel, 0)
        .leftEqualToView(contentLabel)
        .rightEqualToView(contentLabel)
        .heightIs(0);
    }
    
//    if (restaurantArr.count>0) {
//
//
//    }
    
//    [headerView setupAutoHeightWithBottomView:contentLabel bottomMargin:10];
    
//    restaurantTableView.tableHeaderView=headerView;
    
//    if (restaurantArr.count>0) {
//
//        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, contentLabel.endPointY, DeviceWidth, 27)];
//
//        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, DeviceWidth, 17)];
//        lblTitle.text = @"Restaurants Selection";
//        lblTitle.font = [UIFont boldSystemFontOfSize:14];
//        lblTitle.textColor = [UIColor color333333];
//        [headerView addSubview:lblTitle];
//
//
//        return headerView;
//    }
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
   
    NSString *content=[[self.diningGuide.pageContent removeHTML] stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    CGSize size = [content sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(DeviceWidth-30, 500) lineBreakMode:NSLineBreakByWordWrapping];
    if (restaurantArr.count>0) {
        return DeviceWidth*245/375 + 38 + size.height + 27;
    }else{
        return DeviceWidth*245/375 + 38 + size.height;
    }

}

-(void)scrollViewDidScroll:(UITableView*)tableView{
    
    if(restaurantTableView.contentOffset.y>titleHeight){
        [UIView animateWithDuration:0.3 animations:^{
            
            [self blackNav];
        }];
    }else{
        [UIView animateWithDuration:0.3 animations:^{
            [self whiteNav];
            
        }];
    }
}
- (void)whiteNav{
    navgationView.backgroundColor = [UIColor clearColor];
    [backButton setImage:[UIImage imageNamed:@"ic_back_guide"] forState:UIControlStateNormal];
    [shareButton setImage:[UIImage imageNamed:@"ic_share_guide"] forState:UIControlStateNormal];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    navTitleLabel.hidden=YES;
}
- (void)blackNav{
    navgationView.backgroundColor = [UIColor whiteColor];
    navTitleLabel.hidden=NO;
    [backButton setImage:[UIImage imageNamed:@"ic_back"] forState:UIControlStateNormal];
    [shareButton setImage:[UIImage imageNamed:@"ic_share_guide_black"] forState:UIControlStateNormal];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated]; 
    self.navigationController.navigationBarHidden=YES;
    if (restaurantTableView.contentOffset.y <= 0) {
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    }
   
}
-(void)viewDidAppear:(BOOL)animated
{
    if (self.isfromHome) {
       self.navigationController.navigationBarHidden=YES;
    }
    if (self.hiddenNavigationBarStatus!=nil) {
        if ([self.hiddenNavigationBarStatus intValue] == 0) {
            self.navigationController.navigationBarHidden = NO;
        }else{
            self.navigationController.navigationBarHidden = YES;
        }
    }
}

-(void)viewWillDisappear:(BOOL)animated{

    [super viewWillDisappear:animated];

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    self.navigationController.navigationBarHidden=NO;

}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[LoadingView sharedLoadingView] stopLoading];
}

//-(void)loadRestaurantData:(void(^)(BOOL))block{
//    // __block __weak DiningGuideRestaurantsViewController *weakSelf=self;
//    if(hasData){
//        [[LoadingView sharedLoadingView] startLoading];
//        restaurantView.isLoading=YES;
//        NSNumber *latitude=[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"];
//        NSNumber *longitude=[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"];
//        if(latitude==nil) latitude=[NSNumber numberWithInteger:0];
//        if(longitude==nil) longitude=[NSNumber numberWithInteger:0];
//        NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithDictionary:@{@"diningGuideId":self.diningGuide.diningGuideId,@"max":[NSNumber numberWithInteger:max],@"offset":[NSNumber numberWithInteger:offset],@"latitude":latitude,@"longitude":longitude}];
//        [DiningGuideUtil getDiningGuideRestaurants:params completion:^(NSNumber *total,NSArray *restaurants){
//            restaurantView.isLoading=NO;
//            [self callback:total restaurants:restaurants block:block];
//        } failure:^(NSString *description){
//            restaurantView.isLoading=NO;
//            [[LoadingView sharedLoadingView] stopLoading];
//        }];
//    }
//}

//-(void)callback:(NSNumber*)total restaurants:(NSArray*)restaurants block:(void(^)(BOOL))block{
//    offset+=max;
//    NSMutableArray *searchRestaurants=[[NSMutableArray alloc] initWithArray:restaurantView.restaurants];
//    [searchRestaurants addObjectsFromArray:restaurants];
//    restaurantView.restaurants=searchRestaurants;
//    restaurantCount=total;
//    hasData=offset>[total intValue]?NO:YES;
//    if(hasData){
//        //[restaurantView resetRefreshViewFrame];
//        restaurantView.refreshView.hidden=NO;
//    }
//    if(!hasData && restaurantView.refreshView){
//        restaurantView.refreshView.hidden=YES;
//        // [resturantView.refreshView removeFromSuperview];
//    }
//    [[LoadingView sharedLoadingView] stopLoading];
//    block(hasData);
//}


- (void)backClick{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)shareButtonClick:(UIButton*)btn{
    NSString *description=[self.diningGuide.pageContent removeHTML];
    NSURL *url=[NSURL URLWithString:[self diningGuideUrl]];
    NSString *shareContentString = [[NSString stringWithFormat:@"%@ on Qraved. %@. %@",self.diningGuide.pageName,description,url] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    // NSString *shareContentString=[NSString stringWithFormat:@"%@",url];
    // UIImage *image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[self.diningGuide.headerImage returnFullImageUrlWithWidth:DeviceWidth]]]];
    DiningGuideActivityItemProvider *provider=[[DiningGuideActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
    //    provider.image=image;
    provider.url=url;
    provider.title=self.diningGuide.pageName;
    provider.restaurantCount=[restaurantCount intValue];
    TwitterActivityItemProvider *twitterProvider=[[TwitterActivityItemProvider alloc] initWithPlaceholderItem:@""];
    twitterProvider.title=[NSString stringWithFormat:@"Want to go for %@? #Qraved!",self.diningGuide.pageName];
    UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterProvider] applicationActivities:nil];
    activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
    [activityCtl setValue:[NSString stringWithFormat:@"Awesome places to go for %@",self.diningGuide.pageName] forKey:@"subject"];
    activityCtl.completionWithItemsHandler =^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
        if (completed) {
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:self.diningGuide.diningGuideId forKey:@"DiningGuideID"];
            [eventProperties setValue:self.diningGuide.title forKey:@"DiningGuideTitle"];
            [eventProperties setValue:activityType forKey:@"Channel"];
            [eventProperties setValue:@"Dining guide detail page" forKey:@"Location"];
            [[Amplitude instance] logEvent:@"SH - Share Dining Guide" withEventProperties:eventProperties];
        }
        
    };
    if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
        // iOS8
        activityCtl.popoverPresentationController.sourceView =btn
        ;
    }
    
    
    [self presentViewController:activityCtl animated:YES completion:^{
        
    }];

}

//-(void)bookNow:(IMGRestaurant*)restaurant{
//    NSMutableDictionary *eventProperties = [[NSMutableDictionary alloc] init];
//    [eventProperties setObject:@"Dining guide detail page" forKey:@"Location"];
//    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
//    [[Amplitude instance] logEvent:@"CL - Book Now" withEventProperties:eventProperties];
//    
////    [self loadSpecialOffersViewController:(IMGRestaurant*)restaurant andBookDate:nil andBookTime:nil andPax:nil];
//    [RestaurantHandler geOfferFromServer:restaurant.restaurantId andBookDate:nil andBlock:^(NSMutableArray *restaurantOfferArray_) {
//        SelectPaxViewController *selectDinersViewController = [[SelectPaxViewController alloc]initWithRestaurant:restaurant andTimeArray:nil andOfferArray:restaurantOfferArray_ andOffer:nil];
//        [self.navigationController pushViewController:selectDinersViewController animated:YES];
//        
//    }];
//
//
//}
//
//-(void)loadSpecialOffersViewController:(IMGRestaurant*)restaurant andBookDate:(NSDate *)bookDate andBookTime:(NSString *)bookTime andPax:(NSNumber *)pax{
//    if(bookDate!=nil&&bookTime!=nil&&pax!=nil&&![bookTime isEqualToString:@""]){
//        
//    }else{
//        [self initRestaurantOfferFromLocal:nil withRestaurant:restaurant];
//    }
//    SpecialOffersViewController *specialOffersViewController = [[SpecialOffersViewController alloc] initWithRestaurant:restaurant andTimeArray:regularTimeArray andOfferArray:restaurantOfferArray];
//    if(bookDate!=nil&&bookTime!=nil&&pax!=nil&&![bookTime isEqualToString:@""]){
//        specialOffersViewController.pax = pax;
//        specialOffersViewController.bookDate = bookDate;
//        specialOffersViewController.bookTime = bookTime;
//    }
//    [self.navigationController pushViewController:specialOffersViewController animated:YES];
//}
//
//-(void)initRestaurantOfferFromLocal:(NSDate *)bookDate withRestaurant:(IMGRestaurant*)restaurant{
//    NSString *currentDateStr;
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"yyyy-MM-dd"];
//    if(bookDate==nil){
//        NSDate *date = [NSDate date];
//        currentDateStr = [formatter stringFromDate:date];
//        regularTimeArray = [[NSMutableArray alloc] initWithCapacity:0];
//        if(restaurantOfferArray!=nil){
//            [restaurantOfferArray removeAllObjects];
//        }else{
//            restaurantOfferArray=[[NSMutableArray alloc] initWithCapacity:0];
//        }
//        
//        [[DBManager manager] selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGRestaurantOffer WHERE restaurantId = '%@' and offerStartDate<='%@' and offerEndDate>='%@' and offerType=4 ORDER BY offerSlotMaxDisc desc;",restaurant.restaurantId,currentDateStr,currentDateStr] successBlock:^(FMResultSet *resultSet) {
//            while ([resultSet next]) {
//                IMGRestaurantOffer  *tmpRestaurantOffer=[[IMGRestaurantOffer alloc]init];
//                [tmpRestaurantOffer setValueWithResultSet:resultSet];
//                [restaurantOfferArray addObject:tmpRestaurantOffer];
//            }
//            [resultSet close];
//        }failureBlock:^(NSError *error) {
//            NSLog(@"initRestaurantOfferFromLocal error when get datas:%@",error.description);
//        }];
//        
//        [[DBManager manager] selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGRestaurantOffer WHERE restaurantId = '%@' and offerStartDate<='%@' and offerEndDate>='%@' and offerType!=4 ORDER BY offerType,offerSlotMaxDisc desc;",restaurant.restaurantId,currentDateStr,currentDateStr] successBlock:^(FMResultSet *resultSet) {
//            while ([resultSet next]) {
//                IMGRestaurantOffer  *tmpRestaurantOffer=[[IMGRestaurantOffer alloc]init];
//                [tmpRestaurantOffer setValueWithResultSet:resultSet];
//                [restaurantOfferArray addObject:tmpRestaurantOffer];
//            }
//            [resultSet close];
//        }failureBlock:^(NSError *error) {
//            NSLog(@"initRestaurantOfferFromLocal error when get datas:%@",error.description);
//        }];
//
//    }
//}
//

-(NSString *)diningGuideUrl{
    NSString *url;
    if(self.diningGuide.cityId !=nil){
        IMGCity *city=[IMGCity findById:self.diningGuide.cityId];
        url=[NSString stringWithFormat:@"%@%@/dining-guide/%@",QRAVED_WEB_SERVER_OLD,city.name,self.diningGuide.pageUrl];
    }
    if(url==nil){
       url=[NSString stringWithFormat:@"%@%@",QRAVED_WEB_SERVER_OLD,self.diningGuide.searchPageUrl];
    }
    return [url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

 -(void)mapButtonClick{
    IMGDiscover *discover=[[IMGDiscover alloc] init];
    discover.diningGuideId = self.diningGuide.diningGuideId;
    discover.searchDistrictArray = discover.districtArray;
    discover.sortby=SORTBY_POPULARITY;
    discover.needLog = YES;
    
    RestaurantsViewController *restaurantsViewController=[[RestaurantsViewController alloc]initWithMap];
    restaurantsViewController.isFromSearchDininguide = self.isFromSearchDininguide;
    restaurantsViewController.isBackJumpToHome = YES;
    restaurantsViewController.isPop = YES;
    restaurantsViewController.suggestionDiscovery = discover.name;
    restaurantsViewController.amplitudeType = @"Search result page - restaurant";
    restaurantsViewController.isFromDiscoverList = YES;
    restaurantsViewController.isTomap=YES;
    [restaurantsViewController initDiscover:discover];
    [[AppDelegate ShareApp].discoverListNavigationController pushViewController:restaurantsViewController animated:NO];
    [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 1;
    [AppDelegate ShareApp].selectedNavigationController =[AppDelegate ShareApp].discoverListNavigationController;
//    [self.navigationController pushViewController:restaurantsViewController animated:YES];
}

- (void)trackGuide{
    [DiningGuideUtil viewedGuide:self.diningGuide];
}

@end
