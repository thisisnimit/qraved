//
//  V2_DiningGuideListViewController.m
//  Qraved
//
//  Created by harry on 2017/6/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiningGuideListViewController.h"
#import "LoadingView.h"
#import "V2_GuideCell.h"
#import "DiningGuideUtil.h"
#import "DiningGuideRestaurantsViewController.h"
#import "MapViewController.h"
#import "UIScrollView+Helper.h"
#import <FBShimmeringView.h>
#import "IMGShimmerView.h"
@interface V2_DiningGuideListViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,AndroidRefreshDelegate>
{
    UICollectionView *diningGuideCollectionView;
    NSMutableArray *diningGuideArray;
    int max;
    int offset;
    BOOL _hasData;
    BOOL isFreshing;
    IMGShimmerView *shimmerView;
}

@end

@implementation V2_DiningGuideListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.screenName = @"Dining Guide list page";
    
    [self addBackBtn];
    [self loadData];
    [self loadMainUI];
    [self requestData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCity) name:CITY_SELECT_NOTIFICATION object:nil];
    
}

- (void)refreshCity{
    offset = 0;
    max = 10;
    //[diningGuideArray removeAllObjects];
    [self requestData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"Guides";
    [[AppsFlyerTracker sharedTracker] trackEvent:@"RC - View Dining Guide list" withValues:@{}];
    
    [AppDelegate ShareApp].isSlideDiningGuideHidden = NO;
    [[self rdv_tabBarController] setTabBarHidden:NO animated:NO];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [AppDelegate ShareApp].isSlideDiningGuideHidden = YES;
    if ([AppDelegate ShareApp].isSlideHomeHidden
        &&[AppDelegate ShareApp].isSlideJournalHidden
        &&[AppDelegate ShareApp].isSlideDiningGuideHidden) {
        [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    }

}
#pragma mark - data
- (void)loadData{
    offset = 0;
    max = 10;
    diningGuideArray = [NSMutableArray array];
    
    id json = [CommonMethod getJsonFromFile:GUIDELIST_CACHE];
    if (json) {
        [diningGuideArray addObjectsFromArray:[CommonMethod getGuideList:json]];
    }
}

#pragma mark - UI
- (void)loadMainUI{
    self.view.backgroundColor = [UIColor whiteColor];

    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 15;
    layout.minimumInteritemSpacing = 15;
    layout.sectionInset = UIEdgeInsetsMake(15, 15, 15, 15);
    CGFloat itemWidth = (DeviceWidth-45)/2;
    layout.itemSize = CGSizeMake(itemWidth, itemWidth);
    
    diningGuideCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+20-IMG_StatusBarAndNavigationBarHeight-IMG_TabbarHeight) collectionViewLayout:layout];
    diningGuideCollectionView.backgroundColor = [UIColor whiteColor];
    diningGuideCollectionView.delegate = self;
    diningGuideCollectionView.dataSource = self;
    [self.view addSubview:diningGuideCollectionView];

    if (self.isHomeSlide) {
        diningGuideCollectionView.contentInset = UIEdgeInsetsMake(0, 0, 44, 0);
    }
    
    
    diningGuideCollectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMore)];
    diningGuideCollectionView.mj_footer.hidden = YES;
    
    [diningGuideCollectionView registerNib:[UINib nibWithNibName:@"V2_GuideCell" bundle:nil] forCellWithReuseIdentifier:@"diningGuideCell"];
    
    id json = [CommonMethod getJsonFromFile:GUIDELIST_CACHE];
    if (!json) {
        shimmerView = [[IMGShimmerView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, self.view.bounds.size.height)];
        [shimmerView createGuideListShimmerView];
        [self.view addSubview:shimmerView];
    }
}

#pragma mark - request
- (void)requestData{
    isFreshing = YES;
    [DiningGuideUtil getDiningGuideListWithCityId:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] andMax:[NSNumber numberWithInt:max] andOffset:[NSNumber numberWithInt:offset] andBlock:^(NSArray *dataArray,BOOL hasData) {
        _hasData = hasData;
        if (offset==0) {
            [diningGuideArray removeAllObjects];
            [shimmerView removeFromSuperview];
        }
        
        [diningGuideArray addObjectsFromArray:dataArray];
        if (hasData) {
            diningGuideCollectionView.mj_footer.hidden = NO;
        }else{
            diningGuideCollectionView.mj_footer.hidden = YES;
        }

        [diningGuideCollectionView reloadData];
        
        isFreshing = NO;
        [diningGuideCollectionView.mj_footer endRefreshing];
       
    } andFailure:^(NSString *description){
        [diningGuideCollectionView.mj_footer endRefreshing];
    }];
}

- (void)loadMore{
    offset +=max;
    [self requestData];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (diningGuideArray.count > 0) {
        return diningGuideArray.count;
    }else{
        return 8;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    V2_GuideCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"diningGuideCell" forIndexPath:indexPath];
    if (diningGuideArray.count > 0) {
        IMGDiningGuide *diningGuide = [diningGuideArray objectAtIndex:indexPath.row];
        cell.diningGuideModel = diningGuide;
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    IMGDiningGuide *diningGuide = [diningGuideArray objectAtIndex:indexPath.row];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:diningGuide.diningGuideId forKey:@"DiningGuide_ID"];
    [param setValue:diningGuide.pageName forKey:@"DiningGuide_name"];
    [param setValue:@"Dining Guide list" forKey:@"Origin"];
    [[Amplitude instance] logEvent:@"RC - View Dining Guide page" withEventProperties:param];
    [[AppsFlyerTracker sharedTracker] trackEvent:@"RC - View Dining Guide page" withValues:param];
    
    
    [[Amplitude instance] logEvent:@"CL - Dining Guide List" withEventProperties:@{@"Location":@"Dining guide list page"}];

    
    DiningGuideRestaurantsViewController *diningResList = [[DiningGuideRestaurantsViewController alloc] initWithDiningGuide:diningGuide];
    [self.navigationController pushViewController:diningResList animated:YES];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    NSLog(@"%f-------%f",self.view.frame.origin.y,self.view.frame.size.height);
    
    if (self.isHomeSlide && ![diningGuideCollectionView.mj_footer isRefreshing] ) {
        [scrollView scrollNavigationController:[AppDelegate ShareApp].homeNavigationController];

        if (scrollView.contentOffset.y>0) {
            diningGuideCollectionView.bounces = YES;
        }
    }
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CITY_SELECT_NOTIFICATION object:nil];
}

@end
