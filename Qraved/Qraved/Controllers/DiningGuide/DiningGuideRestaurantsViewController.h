//
//  DiningGuideRestaurantsViewController.h
//  Qraved
//
//  Created by System Administrator on 11/26/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//
#import "DiningGuideRestaurantsView.h"
#import "BaseViewController.h"
@class IMGDiningGuide;

@interface DiningGuideRestaurantsViewController : BaseViewController

@property(nonatomic,strong) IMGDiningGuide *diningGuide;
@property(nonatomic,assign)BOOL isfromHome;
@property (nonatomic,strong)NSNumber *hiddenNavigationBarStatus;
@property (nonatomic,assign) BOOL isFromSearchDininguide;

-(instancetype)initWithDiningGuide:(IMGDiningGuide*)diningGuide;

@end
