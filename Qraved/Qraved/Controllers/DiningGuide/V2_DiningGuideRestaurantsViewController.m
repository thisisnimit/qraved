//
//  V2_DiningGuideRestaurantsViewController.m
//  Qraved
//
//  Created by harry on 2017/6/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiningGuideRestaurantsViewController.h"
#import "LoadingView.h"
@interface V2_DiningGuideRestaurantsViewController ()

@end

@implementation V2_DiningGuideRestaurantsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    // self.navigationController.navigationBar.backgroundColor=[UIColor clearColor];
    // self.navigationController.navigationBar.translucent=YES;
}


-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden=NO;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];

    
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[LoadingView sharedLoadingView] stopLoading];
}

@end
