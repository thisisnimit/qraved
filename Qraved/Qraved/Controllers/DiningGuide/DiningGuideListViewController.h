//
//  DiningGuideListViewController.h
//  Qraved
//
//  Created by Lucky on 15/9/9.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "HomeDiningGuideView.h"
#import "EGORefreshTableFooterView.h"
#import "DiningGuideUtil.h"


@interface DiningGuideListViewController : BaseViewController<EGORefreshTableFooterDelegate,UIScrollViewDelegate>
{

//    BOOL pullTableIsLoadingMore;

}

@property (nonatomic, assign) BOOL pullTableIsLoadingMore;

@end
