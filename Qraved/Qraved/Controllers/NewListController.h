//
//  NewListController.h
//  Qraved
//
//  Created by System Administrator on 9/24/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "LoginViewController.h"
#import "IMGMyList.h"

@protocol NewListDelegate <NSObject>
@optional
-(void)newListDelegateGoToSuccessPage:(IMGMyList *)mylist;
@end

@interface NewListController : BaseViewController<UITextFieldDelegate,AutoPostDelegate>
@property(nonatomic,weak) id<NewListDelegate> listDelegate;
@end
