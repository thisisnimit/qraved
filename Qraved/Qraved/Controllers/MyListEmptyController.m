//
//  MyListEmptyController.m
//  Qraved
//
//  Created by System Administrator on 9/24/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "MyListEmptyController.h"
#import "IMGMyList.h"
#import "ListHandler.h"
#import "IMGUser.h"
#import "UIViewController+Helper.h"
#import "LoadingImageView.h"
#import "EmptyListView.h"
#import "MyListAddRestaurantController.h"
#import "AppDelegate.h"
#import "UIViewController+RESideMenu.h"
#import "MyListActivityItemProvider.h"
#import "TwitterActivityItemProvider.h"

extern BOOL NeedExpandMenu;

@implementation MyListEmptyController{
	
	NSMutableArray *MyListArr;
	NSInteger offset;
	NSInteger max;
}

-(instancetype)initWithMyList:(IMGMyList*)mylist{
	if(self=[super init]){
		self.mylist=mylist;
	}
	return self;
}

-(void)viewDidLoad{
	[super viewDidLoad];
	offset=0;
	max=10;
	
    self.view.backgroundColor = [UIColor whiteColor];
	[self setNavigation];
    
    if (self.isOtherUser)
    {
        return;
    }

	EmptyListView *emptyListView=[[EmptyListView alloc] initWithFrame:CGRectMake(0,(DeviceHeight-88-280)/2,DeviceWidth,280) withType:ListDetailType];
	emptyListView.delegate=self;
	[self.view addSubview:emptyListView];

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.rdv_tabBarController.tabBarHidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
}


-(void)setNavigation{
	self.navigationController.navigationBarHidden=NO;

	UIBarButtonItem *backBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:NavigationBackImage] style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
	backBtn.tintColor=[UIColor color333333];

	self.navigationItem.leftBarButtonItems=@[backBtn];
    [self.navigationItem setTitle:self.mylist.listName];


    // self.navigationItem.title=self.mylist.listName;

    UIBarButtonItem *moreBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_more"] style:UIBarButtonItemStylePlain target:self action:@selector(moreBtnClick)];
    moreBtn.tintColor=[UIColor color333333];
    UIBarButtonItem *addBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"addBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(addBtnClick)];
    addBtn.tintColor=[UIColor color333333];
    if (self.isOtherUser)
    {
    }
    else
    {
        self.navigationItem.rightBarButtonItems=@[moreBtn,addBtn];
    }

}

-(void)backClick{
	if(self.expandSideMenu&&self.isFromMenu){
        [[AppDelegate ShareApp] goToPage:0];
        [self presentLeftMenuViewController:nil];

    }else if (self.expandSideMenu){
        NeedExpandMenu=YES;
        [self.navigationController popViewControllerAnimated:YES];
    }else if(self.canBack){
    	[self.navigationController popViewControllerAnimated:YES];
    }else if (self.isFromMenu){
        [[AppDelegate ShareApp] goToPage:0];
        [self presentLeftMenuViewController:nil];

    }else{
		NSMutableArray *controllers=[[NSMutableArray arrayWithArray:self.navigationController.viewControllers] mutableCopy];
		[controllers removeLastObject];
		[controllers removeLastObject];
		[self.navigationController setViewControllers:controllers animated:YES];
	}
}

-(void)moreBtnClick{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"Share List" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self shareButtonClick:nil];
        
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Rename List" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Rename Your List" message:@"Enter a new name for this list" preferredStyle:UIAlertControllerStyleAlert];
        [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.placeholder = @"Input";
        }];
        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Rename" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
              UITextField *nameTexfiled = alert.textFields.firstObject;

            [ListHandler editListWithUser:[IMGUser currentUser] andListName:nameTexfiled.text andListId:self.mylist.listId andBlock:^(BOOL status, NSString *exceptionmsg) {
                self.navigationItem.title = nameTexfiled.text;
            }];
            
        }]];
        [self presentViewController:alert animated:YES completion:nil];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Delete List" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"Are you sure you want to delete \"%@\" list?",self.mylist.listName] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:nil]];
        [alert addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [ListHandler delListWithUser:[IMGUser currentUser] andListId:self.mylist.listId andBlock:^(BOOL status, NSString *exceptionmsg) {
                [self.navigationController popViewControllerAnimated:YES];
            }];

        }]];
        [self presentViewController:alert animated:YES completion:nil];


    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [[AppDelegate ShareApp].window.rootViewController presentViewController:alert animated:YES completion:nil];
}

- (void)shareButtonClick:(UIBarButtonItem*)item {
    IMGUser *user = [IMGUser currentUser];
    NSString * urlString = [[NSString alloc] initWithFormat:@"%@user/%@-%@-%@/list/%@",QRAVED_WEB_SERVER_OLD,user.firstName,user.lastName,user.userId,self.mylist.listName];

    NSString *shareContentString = [NSString stringWithFormat:L(@"My List %@ on Qraved. %@"),self.mylist.listName,urlString];
    // NSString *shareContentString=urlString;
    NSURL *url=[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    MyListActivityItemProvider *provider=[[MyListActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
    provider.url=url;
    provider.title=shareContentString;
    provider.myListTitle=self.mylist.listName;
    provider.restaurantTitles=@"";

    
    TwitterActivityItemProvider *twitterProvider=[[TwitterActivityItemProvider alloc] initWithPlaceholderItem:@""];
    twitterProvider.title=[NSString stringWithFormat:L(@"Check out My %@ List on #Qraved!"),self.mylist.listName];
    
    UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterProvider] applicationActivities:nil];
    activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
    [activityCtl setValue:[NSString stringWithFormat:L(@"mylist_email_subject"),self.mylist.listName] forKey:@"subject"];
    activityCtl.completionWithItemsHandler = ^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
        if (completed) {
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:self.mylist.listId forKey:@"UserList_ID"];
            [eventProperties setValue:self.mylist.listName forKey:@"ListName"];
            [eventProperties setValue:@"My profile - list page" forKey:@"Location"];
            [eventProperties setValue:activityType forKey:@"Channel"];
            
            
            
            [[Amplitude instance] logEvent:@"SH - Share List" withEventProperties:eventProperties];
        }else{
        }
        
    };
    UIView* view=(UIView*)item;
    if ([activityCtl respondsToSelector:@selector(popoverPresentationController)]) {
        
        activityCtl.popoverPresentationController.sourceView = view;
        
    }
    
    [self presentViewController:activityCtl animated:YES completion:^{
        
    }];
}

-(void)goToAddRestaurant:(NSString *)title{
	[self addBtnClick];
}

-(void)addBtnClick{
	MyListAddRestaurantController *listController=[[MyListAddRestaurantController alloc] initWithMyList:self.mylist];
    listController.isfromNewList=self.isNewList;
	listController.redirectToListDetail=YES;
    listController.isfromSlideMenu=self.isFromMenu;
	[self.navigationController pushViewController:listController animated:YES];
}

@end
