//
//  V2_DiscoverListViewController.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/18.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"

@interface V2_DiscoverListViewController : BaseChildViewController


@property (nonatomic, strong)  NSString  *cityName;
@property (nonatomic, assign)  BOOL  isFromHome;

@end
