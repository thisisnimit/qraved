//
//  MyListRestaurantsViewController.m
//  Qraved
//
//  Created by System Administrator on 10/14/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "MyListRestaurantsViewController.h"
#import "MyListRestaurantsView.h"
#import "MyListAddRestaurantController.h"
#import "DetailViewController.h"
#import "ListHandler.h"
#import "IMGUser.h"
#import "IMGMyList.h"
#import "MyListActivityItemProvider.h"
#import "TwitterActivityItemProvider.h"
// #import "UIConstants.h"
#import "LoadingView.h"
#import "Consts.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import <Social/Social.h>
#import "UILabel+Helper.h"
#import "UIView+Helper.h"
#import "AppDelegate.h"
#import "RestaurantsViewController.h"
#import "IMGDiscover.h"
#import "EmptyListView.h"
#import "MapViewController.h"
#import <BlocksKit/BlocksKit+UIKit.h>
#import "MapButton.h"

#define CONDITION_BUTTON_HEIGHT 45
@interface MyListRestaurantsViewController ()<UIPopoverListViewDelegate,UIPopoverListViewDataSource,UITextFieldDelegate,MKMapViewDelegate,CLLocationManagerDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate>

@end

@implementation MyListRestaurantsViewController{
	IMGUser *user;
	MyListRestaurantsView *restaurantsView;
	IMGRestaurant *selectedRestaurant;
	NSString *urlString;
	int offset;
	int max;
	BOOL hasData;
    NSArray *tutorialStrArr;
    MapButton *mapBtn;
    NSMutableArray *restaurants;
    EmptyListView *emptyListView;
    MapViewController *mapViewController;
    MKMapView *mapView;
    UIButton *navigateButton;
    NSNumber *myRestaurantId;
    UIAlertController *alertController;
}


-(instancetype)initWithMyList:(IMGMyList*)mylist{
	if(self=[super init]){
		self.mylist=mylist;
	}
	return self;
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[LoadingView sharedLoadingView]stopLoading];

}
- (void)loadView{
    [super loadView];
    if ([self.mylist.restaurantListItemCount isEqual:@0]) {
        [self addBtnClick];
    }
}
-(void)viewDidLoad{
	[super viewDidLoad];
    
	offset=0;
	max=10;
	hasData=YES;
    if (self.isOtherUser)
    {
        user = self.otherUser;
    }
    else
    {
        user=[IMGUser currentUser];
    }
    restaurants = [NSMutableArray array];
	NSString *listName = [self.mylist.listName stringByReplacingOccurrencesOfString:@" " withString:@"-"];
    urlString = [[NSString alloc] initWithFormat:@"%@user/%@-%@-%@/list/%@",QRAVED_WEB_SERVER_OLD,user.firstName,user.lastName,user.userId,listName];
	[self setNavigation];
	restaurantsView=[[MyListRestaurantsView alloc] initWithFrame:CGRectMake(0,0,DeviceWidth,DeviceHeight) list:self.mylist];
	restaurantsView.delegate=self;
    __weak typeof(self) weakSelf = self;
    restaurantsView.refreshList= ^(NSNumber *restaurantId){
        myRestaurantId = restaurantId;
        [weakSelf createSelectViewWithTitle:@"Removed from list" andButtonTitle:@"Undo" andListId:weakSelf.mylist.listId];
        offset = 0;
        [weakSelf loadMyListRestaurants:^(BOOL _hasData){
            [mapViewController setMapDatas:restaurants offset:0];
        }];
    };
    restaurantsView.isOtherUser = self.isOtherUser;
	[self.view addSubview:restaurantsView];
    
    
    mapBtn = [MapButton buttonWithType:UIButtonTypeCustom];
    mapBtn.frame = CGRectMake(DeviceWidth/2-45, DeviceHeight-100, 90, 40);
    mapBtn.hidden = YES;
    mapBtn.selected = NO;
    [mapBtn addTarget:self action:@selector(mapButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:mapBtn];
    [self.view bringSubviewToFront:mapBtn];
    [self loadMyListRestaurants:^(BOOL _hasData){
    	hasData=_hasData;
        [self createMap];
    	restaurantsView.hasData=_hasData;
        [restaurantsView setRefreshViewFrame];
    }];
}

- (void)createSelectViewWithTitle:(NSString *)title andButtonTitle:(NSString *)buttonTitle andListId:(NSNumber *)listId{
    
    UIView *selectView = [[UIView alloc] initWithFrame:CGRectMake(0, DeviceHeight-44-45, DeviceWidth, 45)];
    
    selectView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:selectView];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 1)];
    lineView.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:248/255.0 alpha:1];
    [selectView addSubview:lineView];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, 12, 200, 20)];
    lblTitle.font = [UIFont systemFontOfSize:14];
    lblTitle.textColor = [UIColor grayColor];
    lblTitle.text = title;
    [selectView addSubview:lblTitle];
    
    UIButton *btnSelect = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSelect.frame = CGRectMake(DeviceWidth-LEFTLEFTSET-60, 0, 60, 45);
    btnSelect.titleLabel.font = [UIFont systemFontOfSize:15];
    [btnSelect setTitle:buttonTitle forState:UIControlStateNormal];
    [btnSelect setTitleColor:[UIColor barButtonTitleColor] forState:UIControlStateNormal];
    [btnSelect addTarget:self action:@selector(unDoButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [selectView addSubview:btnSelect];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(),^{
        [selectView removeFromSuperview];
    });
}
- (void)unDoButtonClick:(UIButton *)button{
    if ([button.titleLabel.text isEqualToString:@"Undo"]) {
        NSArray *listArr = @[self.mylist.listId];
        NSArray *restaurantArr = @[myRestaurantId];
        [ListHandler listAddRestaurantsWithUser:[IMGUser currentUser] andListIds:listArr andRestaurantIds:restaurantArr andBlock:^(BOOL succeed, id status) {
            offset = 0;
            [self loadMyListRestaurants:^(BOOL _hasData){
                [mapViewController setMapDatas:restaurants offset:0];
            }];
        }];
    }
}
- (void)createMap{
    mapViewController=[[MapViewController alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-44)];
    mapViewController.fromSearchPage=YES;
    mapViewController.fromSaved = YES;
    mapViewController.isSavedAmplitude = YES;
    mapViewController.restaurantsViewController=self;
    
    mapViewController.savedClick = ^(IMGRestaurant *pressRestaurant,SavedMapListView *savedMapView){
        [[LoadingView sharedLoadingView] startLoading];
        [ListHandler removeRestaurantFromListWithRestaurantId:pressRestaurant.restaurantId andListId:self.mylist.listId andBlock:^{
            [[LoadingView sharedLoadingView] stopLoading];
     
            myRestaurantId = pressRestaurant.restaurantId;
            [self createSelectViewWithTitle:@"Removed from list" andButtonTitle:@"Undo" andListId:self.mylist.listId];
            offset = 0;
            [self loadMyListRestaurants:^(BOOL _hasData){
                [mapViewController setMapDatas:restaurants offset:0];
            }];
            
        } failure:^(NSString *exceptionMsg) {
            
        }];

    };
    mapView = mapViewController.mapView;
    
    mapView.frame = CGRectMake(0,0, DeviceWidth, self.view.frame.size.height);
    mapView.hidden = YES;
    [self.view addSubview:mapViewController.mapView];
    
    [mapViewController setMapDatas:restaurants offset:0];
    
    navigateButton = [[UIButton alloc] initWithFrame:CGRectMake(DeviceWidth-50, 10, 35, 35)];
    
    navigateButton.backgroundColor = [UIColor clearColor];
    [navigateButton addTarget:self action:@selector(nearYouBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navigateButton setImage:[UIImage imageNamed:@"ic_located"] forState:UIControlStateNormal];
    
    [mapView addSubview:navigateButton];
    [self.view bringSubviewToFront:mapBtn];
}
-(void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	self.navigationController.navigationBarHidden=NO;
    self.title = self.mylist.listName;
    UIFont *font = [UIFont boldSystemFontOfSize:16];
    NSDictionary *dic = @{NSFontAttributeName:font, NSForegroundColorAttributeName:[UIColor color333333]};
    self.navigationController.navigationBar.titleTextAttributes =dic;
    //self.rdv_tabBarController.tabBarHidden = YES;
}

-(void)setNavigation{

	self.view.backgroundColor=[UIColor whiteColor];
    
    UIBarButtonItem *leftBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:NavigationBackImage] style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
    leftBtn.tintColor=[UIColor color333333];
    self.navigationItem.leftBarButtonItem=leftBtn;

    UIBarButtonItem *moreBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_more.png"] style:UIBarButtonItemStylePlain target:self action:@selector(moreBtnClick)];
    moreBtn.tintColor=[UIColor color333333];
    UIBarButtonItem *addBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"addBtn.png"] style:UIBarButtonItemStylePlain target:self action:@selector(addBtnClick)];
    addBtn.tintColor=[UIColor color333333];
    
//    UIBarButtonItem *mapBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"restaurants_map"] style:UIBarButtonItemStylePlain target:self action:@selector(mapButtonTapped)];
//    mapBtn.tintColor=[UIColor whiteColor];
    if (self.isOtherUser)
    {
//        self.navigationItem.rightBarButtonItem = shareBtn;
    }
    else
    {
        self.navigationItem.rightBarButtonItems=@[moreBtn,addBtn];
    }
}
-(void)mapButtonTapped{
    mapBtn.selected = !mapBtn.selected;
    if (mapBtn.selected) {
        
        [IMGAmplitudeUtil trackSavedWithName:@"RC - View Restaurant Map" andRestaurantId:nil andRestaurantTitle:nil andOrigin:@"List Detail Page" andListId:nil andType:nil andLocation:nil];
        [IMGAmplitudeUtil trackSavedWithName:@"RC - View Saved Restaurant on Map" andRestaurantId:nil andRestaurantTitle:nil andOrigin:@"List Detail Page" andListId:nil andType:nil andLocation:nil];
        
        mapBtn.frame = CGRectMake(DeviceWidth/2-45, DeviceHeight-260, 90, 40);
        restaurantsView.hidden = YES;
        mapView.hidden = NO;
    }else{
        mapBtn.frame = CGRectMake(DeviceWidth/2-45, DeviceHeight-100, 90, 40);
        restaurantsView.hidden = NO;
        mapView.hidden = YES;
    }
}
- (void)nearYouBtnClick
{
    CLLocationCoordinate2D center;
    
    center.latitude=[AppDelegate ShareApp].locationManager.location.coordinate.latitude;
    center.longitude = [AppDelegate ShareApp].locationManager.location.coordinate.longitude;
    CLLocationCoordinate2D theCoordinate =CLLocationCoordinate2DMake(center.latitude, center.longitude);
    MKCoordinateSpan span;
    span.latitudeDelta=0.03;
    span.longitudeDelta=0.005;
    MKCoordinateRegion theRegion;
    theRegion.center=theCoordinate;
    theRegion.span=span;
    [mapView setRegion:theRegion animated:NO];
    
    [mapView regionThatFits:theRegion];
    mapView.showsUserLocation = YES;
}
-(void)backClick{
    [mapBtn removeFromSuperview];
    if(self.isFromMenu) {
        [[AppDelegate ShareApp] goToPage:0];
        [self presentLeftMenuViewController:nil];
    }else if (self.isFromAdd){
    
        [[AppDelegate ShareApp] goToPage:0];
        [self presentLeftMenuViewController:nil];

    
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)addBtnClick{
    MyListAddRestaurantController *listController=[[MyListAddRestaurantController alloc] initWithMyList:self.mylist];
    listController.refreshRestaurantList = ^{
        offset = 0;
        [self loadMyListRestaurants:^(BOOL _hasData){
            [mapViewController setMapDatas:restaurants offset:0];
        }];
    };
    listController.isfromSlideMenu=self.isFromMenu;
    [self.navigationController pushViewController:listController animated:YES];
}


-(void)loadMyListRestaurants:(void(^)(BOOL))block{
	[[LoadingView sharedLoadingView] startLoading];
    if (emptyListView) {
        [emptyListView removeFromSuperview];
    }
	[ListHandler getListDetailWithUser:user andOffset:offset andMax:max andListId:self.mylist.listId andBlock:^(NSInteger total, NSArray *dataArray) {
        
        if (offset==0) {
            [restaurants removeAllObjects];
        }
        if (offset==0 && dataArray.count==0) {
            
            [IMGAmplitudeUtil trackSavedWithName:@"BM - Save Restaurant Initiate" andRestaurantId:nil andRestaurantTitle:nil andOrigin:nil andListId:self.mylist.listId andType:nil andLocation:nil];
            
            emptyListView=[[EmptyListView alloc] initWithFrame:CGRectMake(0,(DeviceHeight-88-280)/2-40,DeviceWidth,280) withType:ListDetailType];
            emptyListView.delegate=self;
            emptyListView.isOtherUser = self.isOtherUser;
            [self.view addSubview:emptyListView];
        }
		offset+=max;
		if(offset>=total){
			hasData=NO;
		}else{
			hasData=YES;
		}

       [restaurants addObjectsFromArray:dataArray];
       restaurantsView.restaurants=restaurants;
       
        // [_tableView reloadData];
        // _tableView.pullTableIsLoadingMore = NO;
        if (restaurants.count>0) {
            mapBtn.hidden = NO;
        }else{
            mapBtn.hidden = YES;
        }
        if (block) {
            block(hasData);
        }
        
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

-(void)writeNote:(NSString*)note withRestaurant:(IMGRestaurant*)restaurant atIndexPath:(NSIndexPath*)indexPath{
    [ListHandler mylist:self.mylist.listId user:user restaurant:restaurant.restaurantId note:note block:^(BOOL succeed){
        restaurant.note=note;
        [restaurantsView.restaurants setObject:restaurant atIndexedSubscript:indexPath.row];
    }];
}

-(void)goToRestaurant:(IMGRestaurant*)restaurant{
    
    [IMGAmplitudeUtil trackSavedWithName:@"RC - View Restaurant Page" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andOrigin:@"Saved - List Detail Page" andListId:self.mylist.listId andType:nil andLocation:nil];
    
	DetailViewController *detailViewController = [[DetailViewController alloc] initWithRestaurant:restaurant];
    [self.navigationController pushViewController:detailViewController animated:YES];
}
-(void)moreBtnClick{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alert addAction:[UIAlertAction actionWithTitle:@"Rename List" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        alertController = [UIAlertController alertControllerWithTitle:@"Rename Your List" message:@"Enter a new name for this list" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
            textField.placeholder = @"List Name";
        }];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Rename" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            UITextField *nameTexfiled=alertController.textFields.firstObject;
            NSString *str = nameTexfiled.text;
            if (str.length>0 && str.length <=100) {
                [ListHandler editListWithUser:[IMGUser currentUser] andListName:nameTexfiled.text andListId:_mylist.listId andBlock:^(BOOL status, NSString *exceptionmsg) {
                    
                    [IMGAmplitudeUtil trackSavedWithName:@"BM - Rename List" andRestaurantId:nil andRestaurantTitle:nil andOrigin:nil andListId:self.mylist.listId andType:nil andLocation:nil];
                    
                    self.title = nameTexfiled.text;
                    UIFont *font = [UIFont boldSystemFontOfSize:16];
                    NSDictionary *dic = @{NSFontAttributeName:font, NSForegroundColorAttributeName:[UIColor color333333]};
                    self.navigationController.navigationBar.titleTextAttributes =dic;
                    if ([self.delegate respondsToSelector:@selector(reloadForNewList)]) {
                        [self.delegate reloadForNewList];
                    }
                }];
                
                return ;
            }
            
            NSString *message;
            if (str.length==0) {
                message = @"List name cannot be empty";
            }else if (str.length > 100){
                message = L(@"Maximum 100 characters for list name.");
            }
           
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:L(@"OK") otherButtonTitles:nil, nil];
            [alertView show];
            
        }]];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Share" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self shareButtonClick:nil];

    }]];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Delete List" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"Are you sure you want to delete \"%@\" List?",self.mylist.listName] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [ListHandler delListWithUser:[IMGUser currentUser] andListId:self.mylist.listId andBlock:^(BOOL status, NSString *exceptionmsg) {
                
                [IMGAmplitudeUtil trackSavedWithName:@"BM - Delete List" andRestaurantId:nil andRestaurantTitle:nil andOrigin:nil andListId:self.mylist.listId andType:nil andLocation:nil];
                
                if ([self.delegate respondsToSelector:@selector(reloadForNewList)]) {
                    [self.delegate reloadForNewList];
                }
                [self.navigationController popViewControllerAnimated:YES];
            }];
            
        }]];
        [self presentViewController:alert animated:YES completion:nil];

        
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)moreBtnClick:(IMGRestaurant *)restaurant{
    selectedRestaurant=restaurant;
    CGFloat yHeight = 152.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(0, DeviceHeight-yHeight, DeviceWidth, yHeight)];
    poplistview.delegate = self;
    poplistview.tag = 99;
    poplistview.datasource = self;
    poplistview.listView.scrollEnabled = FALSE;
    [poplistview setTitle:nil];
    [poplistview show];
}
- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView cellForIndexPath:(NSIndexPath *)indexPath
{
    if (popoverListView.tag == 99)
    {
        static NSString *identifier = @"cell";
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET*2, 12, 30, 30)];
        [cell.contentView addSubview:iconImage];
        
        Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(70, 15, DeviceWidth-70, 23)
                                            andTextFont:[UIFont systemFontOfSize:15]
                                           andTextColor:[UIColor color333333]
                                           andTextLines:0];
        
        [cell.contentView addSubview:titleLabel];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSInteger row = indexPath.row;
        if(row == 0){
            titleLabel.text = L(@"Remove from list");
            titleLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:15];
            titleLabel.textColor = [UIColor color222222];
            iconImage.image = [UIImage imageNamed:@"listDelete"];
        }else
        {
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.textLabel.font = [UIFont fontWithName:FONT_GOTHAM_BOOK size:18];
            cell.textLabel.textColor = [UIColor colorWithRed:23/255.0 green:127/255.0 blue:252/255.0 alpha:1];
            cell.textLabel.text = L(@"Cancel");
        }
        return cell;
    }
    return nil;
}
- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section
{
    if (popoverListView.tag == 99)
    {
        return 2;
    }
    else
        return 3;
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 53.0f;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{  //string就是此时输入的那个字符textField就是此时正在输入的那个输入框返回YES就是可以改变输入框的值NO相反
    
    if ([string isEqualToString:@"\n"])  //按会车可以改变
    {
        return YES;
    }
    
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    
    if ([toBeString length] > 65) { //如果输入框内容大于20则弹出警告
        textField.text = [toBeString substringToIndex:65];
        return NO;
    }
    
    return YES;
}
-(void)popoverListView:(UIPopoverListView *)popoverListView     didSelectIndexPath:(NSIndexPath *)indexPath{

    if (popoverListView.tag == 99)
    {
        if (indexPath.row == 0)
        {
            // IMGRestaurant *tmpRtt = [_dataArr objectAtIndex:_currentTag];
            // 
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:L(@"Are you sure you want to delete this restaurant?") delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
            alertView.tag = 99;
            [alertView show];
        }
        [popoverListView dismiss];
    }
    else if(popoverListView.tag == 101)
    {
        if (indexPath.row == 0)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:L(@"Edit list") message:nil delegate:self cancelButtonTitle:@"Edit" otherButtonTitles:@"Cancel", nil];
            alertView.tag = 101;
            alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
            UITextField *textField = [alertView textFieldAtIndex:0];
            textField.delegate = self;
            textField.placeholder = self.mylist.listName;
            [alertView show];
        }
        else if (indexPath.row == 1)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:L(@"Are you sure you want to delete this restaurant?") delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
            alertView.tag = 99;
            [alertView show];
        }
        [popoverListView dismiss];
    }
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 99)
    {
        //NSLog(@"buttonIndex = %d",buttonIndex);
        if (buttonIndex == 0)
        {
            NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
            [param setObject:@"My profile" forKey:@"Location"];
            if ([self.mylist.listName isEqualToString:@"Favorites"]) {
                [param setObject:@"Favorite" forKey:@"Type"];
            }else{
                [param setObject:@"Custom list" forKey:@"Type"];
            }
            [param setObject:selectedRestaurant.restaurantId forKey:@"Restaurant_ID"];
            [param setObject:self.mylist.listId forKey:@"List_ID"];
            
            [[Amplitude instance] logEvent:@"BM - Unsave Restaurant Initiate" withEventProperties:param];
            
            [ListHandler listRemoveWithUser:user andListId:self.mylist.listId andRestaurantId:selectedRestaurant.restaurantId andBlock:^{
                [[Amplitude instance] logEvent:@"BM - Unsave Restaurant Succeed" withEventProperties:param];
                [restaurantsView.restaurants removeObject:selectedRestaurant];
                [restaurantsView.tableView reloadData];
                // [_dataArr removeObjectAtIndex:(int)_currentTag];
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"MyListCollectionViewReloadData" object:nil];
                // [_tableView reloadData];
            }failure:^(NSString *exceptionMsg){
                if([exceptionMsg isLogout]){
                    [[LoadingView sharedLoadingView] stopLoading];
                    [[IMGUser currentUser]logOut];
                    [[AppDelegate ShareApp] goToLoginController];
                }
                [param setObject:exceptionMsg forKey:@"Reason"];
                [[Amplitude instance] logEvent:@"BM - Unsave Restaurant Failed" withEventProperties:param];
            }];
        }
    }
    else if (alertView.tag == 101)
    {
        if (buttonIndex == 0)
        {
            UITextField *textFiedl = [alertView textFieldAtIndex:0];
            if (!textFiedl.text.length) {
                return;
            }
            if (![self isValidateText:textFiedl.text])
            {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:L(@"Message") message:L(@"Sorry, we are not able to process your list") delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alertView show];
                return;
            }
            [ListHandler editListWithUser:user andListName:textFiedl.text andListId:self.mylist.listId andBlock:^(BOOL status,NSString *exceptionmsg){
                // CGSize size = [textFiedl.text sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:18]];
                // naviNameLabel.frame = CGRectMake((size.width > DeviceWidth-88)? 44:DeviceWidth/2-size.width/2, navigationHeight-44, (size.width > DeviceWidth-88)?DeviceWidth-88:size.width, 44);
                // naviNameLabel.text = textFiedl.text;
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"MyListCollectionViewReloadData" object:nil];
            }];
        }
    }else{
        if (buttonIndex==0) {
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }
}

-(BOOL)isValidateText:(NSString *)text {
    NSString *regex = @"[a-zA-Z0-9\\s]*";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [test evaluateWithObject:text];
}

- (void)shareButtonClick:(UIBarButtonItem*)item {
    NSString *shareContentString = [NSString stringWithFormat:L(@"My List %@ on Qraved. %@"),self.mylist.listName,urlString];
    // NSString *shareContentString=urlString;
    NSURL *url=[NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    MyListActivityItemProvider *provider=[[MyListActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
    provider.url=url;
    provider.title=shareContentString;
    provider.myListTitle=self.mylist.listName;
    provider.restaurantTitles=@"";
    if(restaurantsView.restaurants.count>3){
        NSMutableArray *titleArr=[[NSMutableArray alloc] initWithCapacity:3];
        [titleArr addObject:[restaurantsView.restaurants[0] title]];
        [titleArr addObject:[restaurantsView.restaurants[1] title]];
        [titleArr addObject:[restaurantsView.restaurants[2] title]];
        [titleArr addObject:L(@"and many more")];
        provider.restaurantTitles=[titleArr componentsJoinedByString:@", "];
    }else{
        NSMutableArray *titleArr=[[NSMutableArray alloc] initWithCapacity:3];
        for(IMGRestaurant *restaurant in restaurantsView.restaurants){
            [titleArr addObject:restaurant.title];
        }
        provider.restaurantTitles=[titleArr componentsJoinedByString:@", "];
    }

    TwitterActivityItemProvider *twitterProvider=[[TwitterActivityItemProvider alloc] initWithPlaceholderItem:@""];
    twitterProvider.title=[NSString stringWithFormat:L(@"Check out My %@ List on #Qraved!"),self.mylist.listName];
    
    UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterProvider] applicationActivities:nil];
    activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
    [activityCtl setValue:[NSString stringWithFormat:L(@"mylist_email_subject"),self.mylist.listName] forKey:@"subject"];
    activityCtl.completionWithItemsHandler = ^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
        if (completed) {
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:self.mylist.listId forKey:@"List_ID"];
            [eventProperties setValue:activityType forKey:@"Channel"];
            [[Amplitude instance] logEvent:@"SH - Share List" withEventProperties:eventProperties];
        }else{
        }
    };
    UIView* view=(UIView*)item;
    if ([activityCtl respondsToSelector:@selector(popoverPresentationController)]) {
        
        activityCtl.popoverPresentationController.sourceView = view;
        
    }

    [self presentViewController:activityCtl animated:YES completion:^{
        
    }];
}

-(void)goToAddRestaurant:(NSString *)title{
    [self addBtnClick];
}



@end
