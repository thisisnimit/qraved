//
//  JournalSuggestViewController.m
//  Qraved
//
//  Created by Lucky on 15/10/9.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import "JournalSuggestViewController.h"
#import "UIViewController+Helper.h"
#import "NavigationBarButtonItem.h"
#import "SuggestHandler.h"
#import "UIView+Helper.h"
#import "UIDevice+Util.h"
#import "UIConstants.h"
#import "IMGUser.h"
#import "SVProgressHUD.h"

@interface JournalSuggestViewController ()
{
    UITextField *textField;
}
@end

@implementation JournalSuggestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self addDoneBtn];
    [self setBackBarButtonOffset30];
    [self loadMainView];
}
- (void)loadMainView
{
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.backgroundColor = [UIColor colorWithRed:241/255.0 green:243/255.0 blue:238/255.0 alpha:1];
    titleLabel.text = L(@"    What journal are you looking for?");
    titleLabel.frame = CGRectMake(0, 0, DeviceWidth, 44);
    titleLabel.font = [UIFont systemFontOfSize:13];
    [self.view addSubview:titleLabel];
    
    textField = [[UITextField alloc] init];
    textField.frame = CGRectMake(LEFTLEFTSET, titleLabel.endPointY, DeviceWidth-LEFTLEFTSET, 44);
    [self.view addSubview:textField];
    textField.placeholder = L(@"I need article about...");
}
- (void)addDoneBtn
{
    self.navigationItem.title=L(@"Journal Suggestion");
    NavigationBarButtonItem *navigationBarRightButton = [[NavigationBarButtonItem alloc ] initWithImage:nil andTitle:@"Done" target:self action:@selector(doneBtnClick) width:53.0f offset:10];
    self.navigationItem.rightBarButtonItem=navigationBarRightButton;
    
}
- (void)doneBtnClick
{
    if (!textField.text.length)
    {
        return;
    }
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
    IMGUser *user = [IMGUser currentUser];
    if (user.userId)
    {
        [dic setObject:user.userId forKey:@"userId"];
    }
    [dic setObject:textField.text forKey:@"suggestion"];
    [SuggestHandler suggestJournalWithDic:dic withSuccess:^(bool succes) {
        if (succes) {
            
            [[Amplitude instance] logEvent:@"UC - Suggest New Journal Succeed" withEventProperties:@{@"Origin":@"Journal search result page"}];
        }
        
        
        
    } withfail:^(bool fail, NSString *erroInfo) {
        
        if (fail) {
            
         [[Amplitude instance] logEvent:@"UC - Suggest New Journal Failed" withEventProperties:@{@"Reson":erroInfo,@"Origin":@"Journal search result page"}];
            
        }
        
        
    }];
    [self.navigationController popViewControllerAnimated:YES];
    [SVProgressHUD showImage:[UIImage imageNamed:@"success"] status:L(@"Thank you for your contribution, your suggestion has send to our team")];
    [SVProgressHUD dismissWithDelay:1.5];

}



 -(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[Amplitude instance] logEvent:@"UC - Suggest New Journal Cancel" withEventProperties:@{@"Origin":@"Journal search result page"}];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
