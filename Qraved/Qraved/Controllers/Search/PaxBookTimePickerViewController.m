//
//  PaxBookTimePickerViewController.m
//  Qraved
//
//  Created by Jeff on 8/26/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "PaxBookTimePickerViewController.h"

#import "UIConstants.h"

#import "RestaurantsViewController.h"
#import "DetailViewController.h"

#import "UIColor+Helper.h"
#import "UIViewController+Helper.h"
#import "UIView+Helper.h"
#import "NSDate+Helper.h"
#import "UIDevice+Util.h"
#import "BookUtil.h"
#import "SearchCellLabel.h"
#import "BottomButton.h"
#import "NAPickerView.h"
#import "NACustomCell.h"
#import "NALabelCell.h"
#import "RadioButton.h"
#import "IMGDiscover.h"
#import "NotificationConsts.h"
#import "DBManager.h"

#define PAX_MAX 30
#define PAX_MIN 2

#define PICKER_VIEW_HEIGHT 210

@interface PaxBookTimePickerViewController ()
@property (nonatomic,retain) NSMutableDictionary * tableInfo;
@end

@implementation PaxBookTimePickerViewController{
    UIImageView *pickersBackgroudImageView;

    NAPickerView *paxPickerView;
    NAPickerView *datePickerView;
    NAPickerView *timePickerView;

    PickerViewType pickerType;

    SearchCellLabel *titleLabel;

    NSArray *dateStringArray;
    NSMutableArray *dateArray;
    NSMutableArray *timeArray;
    NSMutableArray *paxArray;

    BottomButton *findButton;

    UIButton *closeButton;
    UILabel *spaceHolderLabel;
    
    UILabel *naviTitleLabel;

}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Find a table page";
    _tableInfo = [[NSMutableDictionary alloc]init];
    self.navigationController.navigationBarHidden = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    if (!showCloseButton) {
        [self loadRevealController];
    }
    [self initData];

    if (!self.navigationController) {
        [self loadTopBar];
    }
    [self loadTitleView];
    [self loadPaxTimePickerView];
    [self loadBottomButtons];

}

-(void)find:(id)sender{
//    if (self.navigationController) {
    if(self.discover==nil){
        self.discover = [[IMGDiscover alloc]init];
        self.discover.cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    }
    if([sender isKindOfClass:[BottomButton class]]){
        self.discover.pax = [NSNumber numberWithLong:self.selectedPax];
        NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEE dd MMM"];
        self.discover.bookDate = self.selectedDate;
        self.discover.bookTime = self.selectedTime;
    }else{
        self.discover.pax = nil;
        self.discover.bookDate = nil;
        self.discover.bookTime = nil;
    }
    

    if(self.fromRestaurantsViewController){
//        [[NSNotificationCenter defaultCenter] postNotificationName:SEARCH_RESTAURANT object:nil userInfo:@{@"discover":self.discover}];
        NSNotification *notification = [[NSNotification alloc] initWithName:SEARCH_RESTAURANT object:nil userInfo:@{@"discover":self.discover}];
        
        if([self.pbtpvcDelegate respondsToSelector:@selector(search:)])
        {
            [self.pbtpvcDelegate search:notification];
            [self dismissViewControllerAnimated:YES completion:nil];
        }

    }else{
        if(self.bestOffer){
            self.discover.sortby = SORTBY_OFFERS;
            IMGOfferType *offerType1=[[IMGOfferType alloc] init];
            offerType1.offerId = [NSNumber numberWithInt:1];
            offerType1.typeId = [NSNumber numberWithInt:0];
            offerType1.off = [NSNumber numberWithInt:50];
            offerType1.name = L(@"50% Qraved Discount");
            
            IMGOfferType *offerType2=[[IMGOfferType alloc] init];
            offerType2.offerId = [NSNumber numberWithInt:2];
            offerType2.typeId = [NSNumber numberWithInt:0];
            offerType2.off = [NSNumber numberWithInt:40];
            offerType2.name = L(@"40% Qraved Discount");
            
            IMGOfferType *offerType3=[[IMGOfferType alloc] init];
            offerType3.offerId = [NSNumber numberWithInt:3];
            offerType3.typeId = [NSNumber numberWithInt:0];
            offerType3.off = [NSNumber numberWithInt:30];
            offerType3.name = L(@"30% Qraved Discount");
            
            IMGOfferType *offerType4=[[IMGOfferType alloc] init];
            offerType4.offerId = [NSNumber numberWithInt:4];
            offerType4.typeId = [NSNumber numberWithInt:0];
            offerType4.off = [NSNumber numberWithInt:20];
            offerType4.name = L(@"20% Qraved Discount");
            
            IMGOfferType *offerType5=[[IMGOfferType alloc] init];
            offerType5.offerId = [NSNumber numberWithInt:5];
            offerType5.typeId = [NSNumber numberWithInt:0];
            offerType5.off = [NSNumber numberWithInt:10];
            offerType5.name = L(@"10% Qraved Discount");
            
            IMGOfferType *offerType6=[[IMGOfferType alloc] init];
            offerType6.offerId = [NSNumber numberWithInt:6];
            offerType6.typeId = [NSNumber numberWithInt:1];
            offerType6.off = [NSNumber numberWithInt:0];
            offerType6.name = L(@"Special Set Menu");
            
            IMGOfferType *offerType7=[[IMGOfferType alloc] init];
            offerType7.offerId = [NSNumber numberWithInt:7];
            offerType7.typeId = [NSNumber numberWithInt:2];
            offerType7.off = [NSNumber numberWithInt:0];
            offerType7.name = L(@"In-restaurant Promotion");
            
            IMGOfferType *offerType8=[[IMGOfferType alloc] init];
            offerType8.offerId = [NSNumber numberWithInt:8];
            offerType8.typeId = [NSNumber numberWithInt:3];
            offerType8.off = [NSNumber numberWithInt:0];
            offerType8.name = L(@"Featured Promo");
            
            [self.discover addOfferType:offerType1];
            [self.discover addOfferType:offerType2];
            [self.discover addOfferType:offerType3];
            [self.discover addOfferType:offerType4];
            [self.discover addOfferType:offerType5];
            [self.discover addOfferType:offerType6];
            [self.discover addOfferType:offerType7];
            [self.discover addOfferType:offerType8];
        }else if(self.mostPopular){
            self.discover.sortby = SORTBY_POPULARITY;
        }else if(self.mostBooked){
            self.discover.sortby = SORTBY_BOOKINGS;
        }
        RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc]initWithMap];
        [restaurantsViewController initDiscover:self.discover];
        [self.navigationController pushViewController:restaurantsViewController animated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    
    
}


- (void)loadTopBar{
    if([UIDevice isLaterThanIos6])
    {
        spaceHolderLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 20)];
        [spaceHolderLabel setBackgroundColor:[UIColor colorC2060A]];
        [self.view addSubview:spaceHolderLabel];
    }

    naviTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, spaceHolderLabel.frame.size.height, DeviceWidth, 45)];
    [naviTitleLabel setText:L(@"Find a table")];
    UIFont *titleLabelFont=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:18];
    [naviTitleLabel setFont:titleLabelFont];
    [naviTitleLabel setTextAlignment:NSTextAlignmentCenter];
    [naviTitleLabel setTextColor:[UIColor whiteColor]];
    [naviTitleLabel setBackgroundColor: [UIColor colorC2060A]];
    [self.view addSubview:naviTitleLabel];

    closeButton = [[UIButton alloc] initWithFrame:CGRectMake(DeviceWidth-45, spaceHolderLabel.frame.size.height, 45, 45)];
    [closeButton setImage:[UIImage imageNamed:@"closeWhite"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeButton];

}

-(void)close{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)initData{
    [self initDateArray];
    [self initTimeArray:[dateStringArray objectAtIndex:0]];
    [self initPaxArray];
    
    [_tableInfo setObject:@"" forKey:BookAttributeParty];
    [_tableInfo setObject:@"" forKey:BookAttributeDate];
    [_tableInfo setObject:@"" forKey:BookAttributeTime];
}

-(void)initDateArray{
    NSMutableArray *tmpDateArray = [NSMutableArray arrayWithCapacity:180];
    dateArray = [NSMutableArray arrayWithCapacity:180];
    int seconds = 60*60*24;
    NSDate *date = [NSDate date];
    int ii=0;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"HH"];
    NSString *bookHour = [formatter stringFromDate:date];
    [formatter setDateFormat:@"mm"];
    NSString *bookMinute = [formatter stringFromDate:date];
    if([bookHour intValue]==23 && [bookMinute intValue]>=30){
        ii=1;
    }
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE dd MMM"];

    for(int i=0;i<=180+ii;i++){
        NSString *dateString = [dateFormatter stringFromDate:date];
        if(i>=ii){
            [tmpDateArray addObject:dateString];
            [dateArray addObject:date];
        }

        date = [[NSDate alloc] initWithTimeInterval:seconds sinceDate:date];
    }
    dateStringArray = [NSArray arrayWithArray:tmpDateArray];
}

-(void)initTimeArray:(NSString *)month{
    timeArray = [[NSMutableArray alloc] initWithObjects:@"06:00",@"06:30",@"07:00",@"07:30",@"08:00",@"08:30",@"09:00",@"09:30",@"10:00",@"10:30",@"11:00",@"11:30",@"12:00",@"12:30",@"13:00",@"13:30",@"14:00",@"14:30",@"15:00",@"15:30",@"16:00",@"16:30",@"17:00",@"17:30",@"18:00",@"18:30",@"19:00",@"19:30",@"20:00",@"20:30",@"21:00",@"21:30",@"22:00",@"22:30",@"23:00",@"23:30",nil];
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE dd MMM"];
    NSDate *today = [NSDate date];
    NSString *todayString = [dateFormatter stringFromDate:today];
    if([todayString isEqualToString:month]){
        NSString *currentTime = [BookUtil currentTimeAt:today];
        int hour = [[[currentTime componentsSeparatedByString:@":"] objectAtIndex:0] intValue];
        int minute = [[[currentTime componentsSeparatedByString:@":"] objectAtIndex:1] intValue];
        
        for(NSInteger i=timeArray.count-1;i>=0;i--){
            NSString *timeString = [timeArray objectAtIndex:i];
            int hour1 = [[[timeString componentsSeparatedByString:@":"] objectAtIndex:0] intValue];
            int minute1 = [[[timeString componentsSeparatedByString:@":"] objectAtIndex:1] intValue];
            if(hour1<hour || (hour==hour1 && minute1<minute)){
                [timeArray removeObjectAtIndex:i];
            }
        }
    }
}

-(void)initPaxArray{
    paxArray = [[NSMutableArray alloc] initWithCapacity:PAX_MAX-PAX_MIN+1];
    for(int i=PAX_MIN;i<=PAX_MAX;i++){
        [paxArray addObject:[[NSString alloc] initWithFormat:@"%2d",i]];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


 


- (void)loadTitleView
{
    titleLabel =  [[SearchCellLabel alloc] initWith16BoldBlackColorAndFrame:CGRectMake(0, self.navigationController?0:(45+[UIDevice heightDifference]), DeviceWidth, CELL_HEIGHT_50)];
    [titleLabel setText:L(@"When would you like to dine?")];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:titleLabel];
}

- (void)loadPaxTimePickerView
{
    pickersBackgroudImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, titleLabel.endPointY, DeviceWidth, PICKER_VIEW_HEIGHT)];
    UIImage *pickersBackgroudImage = [UIImage imageNamed:@"picker_bg"];
    [pickersBackgroudImageView setImage:pickersBackgroudImage];
    [self.view addSubview:pickersBackgroudImageView];

    float i = 1.0;
    if ([UIDevice isIphone6Plus]) {
        i=1.6;
    }
    if ([UIDevice isIphone6]) {
        i=1.6;
    }

    
    paxPickerView = [[NAPickerView alloc] initWithFrame:CGRectMake(13*i, titleLabel.endPointY, 85, PICKER_VIEW_HEIGHT)
                                               andItems:paxArray andCellClassName:@"NACustomCell" andDelegate:self];
   
  NSUInteger paxIndex=  [paxArray indexOfObject:[NSString stringWithFormat:@"%2d",[self.discover.pax intValue]]];
    if (self.fromRestaurantsViewController&&self.discover.pax) {
        [paxPickerView setIndex:paxIndex];
    }else{
    
        [paxPickerView setIndex:0];

    }
    
    paxPickerView.backgroundColor = [UIColor clearColor];
    paxPickerView.showOverlay = NO;
    paxPickerView.configureBlock = ^(NACustomCell *cell, NSArray *item) {
        NSString *title =(NSString *)item;
        [cell.label setText:title];
        [cell.label setTextAlignment:NSTextAlignmentCenter];
        [cell.label setBackgroundColor:[UIColor clearColor]];
    };
    paxPickerView.highlightBlock = ^(NACustomCell *cell) {
        cell.label.textColor = [UIColor color222222];
        [cell.label setBackgroundColor:[UIColor clearColor]];
    };
    paxPickerView.unhighlightBlock = ^(NACustomCell *cell) {
        cell.label.textColor = [UIColor color6F6F6F];
        [cell.label setBackgroundColor:[UIColor clearColor]];
    };
    if (self.fromRestaurantsViewController&&[self.discover.pax intValue]) {
        self.selectedPax = [[paxArray objectAtIndex:paxIndex] intValue];

    }else{
    
        self.selectedPax = [[paxArray objectAtIndex:0] intValue];

    }
        [self.view addSubview:paxPickerView];


    datePickerView = [[NAPickerView alloc] initWithFrame:CGRectMake(paxPickerView.endPointX, titleLabel.endPointY, DeviceWidth-85-100, PICKER_VIEW_HEIGHT)
                                                andItems:dateStringArray andCellClassName:@"NACustomCell" andDelegate:self];
    
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE dd MMM"];
    NSString* date=  [dateFormatter stringFromDate:self.discover.bookDate  ];
    NSUInteger dateIndex=  [dateStringArray indexOfObject:date ];
    if (self.fromRestaurantsViewController&&self.discover.bookDate) {
        [datePickerView setIndex:dateIndex];

    }else{
    
        [datePickerView setIndex:0];

    }
    datePickerView.backgroundColor = [UIColor clearColor];
    datePickerView.showOverlay = NO;
    datePickerView.configureBlock = ^(NACustomCell *cell, NSArray *item) {
        NSString *title =(NSString *)item;
        [cell.label setText:title];
        [cell.label setTextAlignment:NSTextAlignmentCenter];
        [cell.label setBackgroundColor:[UIColor clearColor]];
    };
    datePickerView.highlightBlock = ^(NACustomCell *cell) {
        cell.label.textColor = [UIColor color222222];
        [cell.label setBackgroundColor:[UIColor clearColor]];
    };
    datePickerView.unhighlightBlock = ^(NACustomCell *cell) {
        cell.label.textColor = [UIColor color6F6F6F];
        [cell.label setBackgroundColor:[UIColor clearColor]];
    };
    if (self.fromRestaurantsViewController&&self.discover.bookDate) {
        self.selectedDate = [dateArray objectAtIndex:dateIndex];

    }else{
    
        self.selectedDate = [dateArray objectAtIndex:0];

    }
        [self.view addSubview:datePickerView];


    
    if (self.fromRestaurantsViewController&&self.discover.bookDate) {
        NSString * month = [dateStringArray objectAtIndex:dateIndex];
        [self initTimeArray:month];
        
    }
    timePickerView = [[NAPickerView alloc] initWithFrame:CGRectMake(datePickerView.endPointX, titleLabel.endPointY, 100.f, PICKER_VIEW_HEIGHT)
                                                andItems:timeArray andCellClassName:@"NACustomCell" andDelegate:self];
    
    NSUInteger timeindex;
    if (self.fromRestaurantsViewController&&self.discover.bookTime.length>0) {
        NSString * month = [dateStringArray objectAtIndex:dateIndex];
        [self initTimeArray:month];
        NSUInteger timeIndex=  [timeArray indexOfObject:(NSString*)self.discover.bookTime ];
        [timePickerView setIndex:timeIndex];
        timeindex=timeIndex;
    }else{
    
        [timePickerView setIndex:0];
    
    }

    timePickerView.backgroundColor = [UIColor clearColor];
    timePickerView.showOverlay = NO;
    timePickerView.configureBlock = ^(NACustomCell *cell, NSArray *item) {
        NSString *title =(NSString *)item;
        [cell.label setText:title];
        [cell.label setTextAlignment:NSTextAlignmentCenter];
        [cell.label setBackgroundColor:[UIColor clearColor]];
    };
    timePickerView.highlightBlock = ^(NACustomCell *cell) {
        cell.label.textColor = [UIColor color222222];
        [cell.label setBackgroundColor:[UIColor clearColor]];
    };
    timePickerView.unhighlightBlock = ^(NACustomCell *cell) {
        cell.label.textColor = [UIColor color6F6F6F];
        [cell.label setBackgroundColor:[UIColor clearColor]];
    };
    if (self.fromRestaurantsViewController&&self.discover.bookTime.length>0) {
        self.selectedTime = [timeArray objectAtIndex:timeindex];

    }else{
    
        self.selectedTime = [timeArray objectAtIndex:0];

    
    }
    [self.view addSubview:timePickerView];

}

- (void)loadBottomButtons
{
    
    float i=1.0;
    float a=1.0;
    if ([UIDevice isIphone6Plus]) {
        i=1.5;
        a=1.5;
    }
    if ([UIDevice isIphone6]) {
        i=1.3;
        a=1.5;

    }

    UIView *backgroudView = [[UIView alloc] initWithFrame:CGRectMake(0, paxPickerView.endPointY, DeviceWidth, 250*a)];
    [backgroudView setBackgroundColor:[UIColor colorEEEEEE]];
    [self.view addSubview:backgroudView];

    UIImageView *noDateImage = [[UIImageView alloc]initWithFrame:CGRectMake(62*i, self.navigationController?305:(350+[UIDevice heightDifference]), 15, 15)];
    noDateImage.image = [UIImage imageNamed:@"RadioChecked.png"];
    [self.view addSubview:noDateImage];
    
    SearchCellLabel *noDateTimeLabel = [[SearchCellLabel alloc] initWith13BoldBlackColorAndFrame:CGRectMake(90*i, self.navigationController?305:(350+[UIDevice heightDifference]), DeviceWidth-150, 15)];
    [noDateTimeLabel setText:L(@"I don't have a specific date")];
    [self.view addSubview:noDateTimeLabel];

    UIControl *control = [[UIControl alloc]initWithFrame:CGRectMake(62, self.navigationController?300:(345+[UIDevice heightDifference]), DeviceWidth-120, 25)];
    [control addTarget:self action:@selector(find:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:control];

    findButton=[[BottomButton alloc] initWithFrame: CGRectMake(4,self.navigationController?(DeviceHeight-94):(DeviceHeight-94+(45+[UIDevice heightDifference])) , DeviceWidth-8, CELL_HEIGHT_50) title:L(@"Find a table")];
    [findButton addTarget:self action:@selector(find:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:findButton];

}

#pragma mark pickerViewDelegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (void)didSelectedAtIndexDel:(NSInteger)index
{
    if(pickerType == BookPeopleType){
        if (index>paxArray.count-1) {
            return;
        }
        NSString * party = [paxArray objectAtIndex:index];
        [_tableInfo setObject:party forKey:BookAttributeParty];
        self.selectedPax = [party intValue];
        return ;
    }
    if(pickerType == BookMonthType){
        if (index>dateStringArray.count-1) {
            return;
        }
        NSString * month = [dateStringArray objectAtIndex:index];
        [_tableInfo setObject:month forKey:BookAttributeDate];

        self.selectedDate = [dateArray objectAtIndex:index];
        [self initTimeArray:month];
        [timePickerView setDataSource:timeArray];
        [timePickerView reloadData];
        [timePickerView setIndex:0];
        self.selectedTime = [timeArray objectAtIndex:0];
        return ;
    }
    if(pickerType == BookDiscountType){
        if (index>timeArray.count-1) {
            return;
        }
        NSString * time = [timeArray objectAtIndex:index];
        [_tableInfo setObject:time forKey:BookAttributeTime];
        self.selectedTime =time;
        return;
    }
}

-(void)refreshPickerView
{

}

-(NSInteger)getWeekDay:(NSDate *)date
{
    return [[date getDayOfWeekString] integerValue]-1;
}


-(void)beginScroll:(id)target
{
    if(target==paxPickerView)
    {
        pickerType=BookPeopleType;
    }
    else if(target==datePickerView)
    {
        pickerType=BookMonthType;
    }
    else if(target==timePickerView)
    {
        pickerType=BookDiscountType;
    }
}
@end
