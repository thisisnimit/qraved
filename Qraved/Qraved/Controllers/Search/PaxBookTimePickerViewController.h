//
//  PaxBookTimePickerViewController.h
//  Qraved
//
//  Created by Jeff on 8/26/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "IMGDiscover.h"

@protocol PaxBookTimePickerViewControllerDelegate <NSObject>

- (void)search:(NSNotification *)notification;

@end

typedef enum {
    BookPeopleType,
    BookMonthType,
    BookDiscountType
}
PickerViewType;

static NSString * const BookAttributeParty = @"bookAttributeParty";
static NSString * const BookAttributeDate = @"bookAttributeDate";
static NSString * const BookAttributeTime = @"bookAttributeTime";


@interface PaxBookTimePickerViewController : BaseChildViewController


@property(nonatomic,assign) BOOL bestOffer;
@property(nonatomic,assign) BOOL mostPopular;
@property(nonatomic,assign) BOOL mostBooked;
@property(nonatomic,assign) NSInteger selectedPax;

@property(nonatomic,copy) NSDate *selectedDate;
@property(nonatomic,copy) NSString *selectedTime;

@property(nonatomic,retain) IMGDiscover *discover;
@property(nonatomic,assign) BOOL fromRestaurantsViewController;

@property (nonatomic,assign) id<PaxBookTimePickerViewControllerDelegate>pbtpvcDelegate;


@end
