//
//  CuisineListViewController.m
//  Qraved
//
//  Created by Jeff on 8/22/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "CuisineListViewController.h"
#import "CuisineFilteredListController.h"

#import "UIViewController+Helper.h"
#import "UIColor+Helper.h"
#import "UIDevice+Util.h"
#import "CuisineCell.h"

#import "IMGCuisine.h"
#import "NotificationConsts.h"
#import "DBManager.h"
#import "RestaurantsViewController.h"
#import "NavigationBarButtonItem.h"
#import "DiscoverListViewController.h"
@implementation CuisineListViewController
{
    NSMutableArray *cuisineArray;
    NSMutableArray * searchResultArray;
    BOOL searchActive;
    IMGDiscover *discover;
}

#define DEFAULT_SEARCH_TEXT @"Find Cuisine"

-(id)initWithDiscover:(IMGDiscover *)paramDiscover{
    self = [super init];
    if(self){
        discover = paramDiscover;
        if(discover==nil){
            discover = [[IMGDiscover alloc]init];
        }
        if(discover.cuisineArray==nil){
            discover.cuisineArray = [[NSMutableArray alloc]initWithCapacity:0];
        }
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Cuisine";
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self initData];
    [self setBackBarButtonOffset30];
	[self loadSearchBar];
    
	self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, self.searchBar.frame.origin.y+self.searchBar.frame.size.height, DeviceWidth,DeviceHeight-90) style: UITableViewStylePlain];
    self.tableView.dataSource = self;
	self.tableView.delegate = self;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[self.view addSubview:self.tableView];
}

- (void)loadSearchBar
{
    self.searchStr = DEFAULT_SEARCH_TEXT;
	
    float height = 0;

    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, height, DeviceWidth, 45)];
    self.searchBar.delegate = self;
    self.searchBar.placeholder = DEFAULT_SEARCH_TEXT;
    [self.view addSubview:self.searchBar];
    if ([UIDevice isLaterThanIos6]) {
        self.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    }else{
        for(UIView *subview in self.searchBar.subviews){
            if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                [subview removeFromSuperview];
            }
            
        }
    }
    self.searchBar.layer.borderColor = [UIColor colorEEEEEE].CGColor;
    self.searchBar.layer.borderWidth = 0.3;
    [self.searchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"search_input-box" ]  forState:UIControlStateNormal];
}

- (void)initData
{
    cuisineArray = [[NSMutableArray alloc] initWithCapacity:0];
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGCuisine order by name;"] successBlock:^(FMResultSet *resultSet) {
        while([resultSet next]) {
            IMGCuisine *cuisine = [[IMGCuisine alloc]init];
            [cuisine setValueWithResultSet:resultSet];
            [cuisineArray addObject:cuisine];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"CuisineListViewController initData error when get datas:%@",error.description);
    }];
}




#pragma mark- SearchBar Delegate Methods
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if(searchResultArray==nil){
        searchResultArray = [[NSMutableArray alloc]init];
    }else{
        [searchResultArray removeAllObjects];
    }
    searchActive = searchText.length > 0 ? YES : NO;
    if(searchActive){
        for (IMGCuisine * cuisine in cuisineArray) {
            NSString * string = cuisine.name;
            NSRange range = [string rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if(range.location!=NSNotFound ){
                [searchResultArray addObject:cuisine];
            }
        }
    }

    [self.tableView reloadData];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
	searchBar.showsCancelButton = NO;
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {

}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
	searchBar.showsCancelButton = NO;

}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	self.searchStr = [self.searchBar text];
	[searchBar resignFirstResponder];
	[self.tableView reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
	[searchBar resignFirstResponder];
}
#pragma mark- Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return searchActive?searchResultArray.count:cuisineArray.count;
}

- (CuisineCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";

    CuisineCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[CuisineCell alloc] initWithStyle:UITableViewCellStyleDefault selectionStyle:UITableViewCellSelectionStyleNone reuseIdentifier:CellIdentifier] ;
    }
    IMGCuisine * cuisine = nil;

    if(searchActive){
        cuisine = [searchResultArray objectAtIndex:indexPath.row];
    }else{
        cuisine =  [cuisineArray objectAtIndex:indexPath.row];
    }
    [cell setText:cuisine.name];
    [cell setChecked:[discover hasCuisine:cuisine]];
    return cell;
}


#pragma mark- Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (_isfromSearchCategary) {
        IMGDiscover *searchDiscover=[[IMGDiscover alloc]init];
        IMGCuisine *searchCuisine=[cuisineArray objectAtIndex:indexPath.row];
        searchDiscover.cuisineArray=[NSMutableArray arrayWithObjects:searchCuisine, nil];
        searchDiscover.cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
        RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc] initWithMap];
        restaurantsViewController.isBackJumpToSearch=YES;
        [restaurantsViewController initDiscover:searchDiscover];
        [self.navigationController pushViewController:restaurantsViewController animated:NO];
        

    }
    NSArray  * selectArray = searchActive ? searchResultArray : cuisineArray;
    CuisineCell *cell = (CuisineCell *)[tableView  cellForRowAtIndexPath:indexPath];
    IMGCuisine * cuisine = [selectArray objectAtIndex:indexPath.row];
    [cell setChecked:![discover hasCuisine:cuisine]];
    if(![discover hasCuisine:cuisine]){
        [discover addCuisine:cuisine];
    }else{
        [discover removeCuisine:cuisine];
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:SEARCH_FILTER object:nil userInfo:@{@"discover":discover,@"tempArray":cuisine}];
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            if ([[tableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                return;
            }
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        
        @catch ( NSException *e ) {
            return;
        }
    });
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scroll {
    [self.searchBar resignFirstResponder];
}

@end
