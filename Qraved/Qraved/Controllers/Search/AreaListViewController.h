//
//  AreaListViewController.h
//  Qraved
//
//  Created by lucky on 16/9/21.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "IMGDiscover.h"

@interface AreaListViewController : BaseViewController<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>


@property (nonatomic, retain) UISearchBar *searchBar;
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) UIView   *headView;
@property (nonatomic, retain) UIButton *goButton;

@property (nonatomic, retain) NSMutableArray *resultArray;
@property (nonatomic, copy)   NSString *searchStr;



-(id)initWithDiscover:(IMGDiscover *)paramDiscover;


@end
