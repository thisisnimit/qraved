//
//  RestaurantListViewController.h
//  Qraved
//
//  Created by Jeff on 8/14/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//
#import "BaseChildViewController.h"
#import <MapKit/MapKit.h>
#import "IMGEvent.h"
#import "IMGDiscover.h"
#import <CoreLocation/CoreLocation.h>
#import "IMGRestaurant.h"
#import "FiltersViewController.h"
#import "SortTypeListViewController.h"
#import "PaxBookTimePickerViewController.h"
#import "EGORefreshTableHeaderView.h"
#import "SearchPageResturantCell.h"
#import "SearchEmptyResultView.h"
#import "TYAttributedLabel.h"

@interface RestaurantsViewController : BaseChildViewController<UISearchBarDelegate,UISearchDisplayDelegate,UITableViewDataSource,UITableViewDelegate,MKMapViewDelegate,CLLocationManagerDelegate,FiltersViewControllerDelegate,SortTypeListViewControllerDelegate,PaxBookTimePickerViewControllerDelegate,EGORefreshTableHeaderDelegate,searchPageCellDelegate,SearchEmptyResultViewDelegate,TYAttributedLabelDelegate>

@property(nonatomic,assign) NSInteger eventId;
@property(nonatomic,assign) BOOL backToPrevious;
@property(nonatomic,retain) NSString *amplitudeType;
@property(nonatomic,retain) NSString *amplitudeKind;
@property(nonatomic,retain) NSString *suggestionDiscovery;
@property(copy,nonatomic) NSString *amplitudeSortBy;
@property(nonatomic,assign)BOOL isFromEvent;
@property (nonatomic,assign) BOOL isPop;
@property (nonatomic,assign) BOOL isNearBy;
@property (nonatomic,assign) BOOL isBackJumpToHome;
@property (nonatomic,assign) BOOL isBackJumpToProfile;
@property (nonatomic,assign) BOOL isFromDiscoverList;
@property (nonatomic,assign) BOOL isFromTabMenu;
@property (nonatomic,assign) BOOL isFromPersonnazation;
@property (nonatomic,assign) BOOL isFromSearchDininguide;
@property(nonatomic,assign)BOOL isTomap;
@property (nonatomic,assign) BOOL isBackJumpToSearch;
@property (nonatomic,assign) BOOL isfromSearchNearby;
@property(nonatomic,assign)BOOL isPromo;
@property(nonatomic,retain) IMGDiscover *searchDiscover;



-(void)initDiscover:(IMGDiscover *)tmpDiscover;
-(IMGDiscover *)getDiscover;
- (id)initWithEventId:(NSUInteger)eventId;
- (id)initWithEvent:(IMGEvent *)event;
- (id)initWithMap;
-(void)removeNotifications;
-(void)gotoDetailViewControllerWithRestaurant:(IMGRestaurant *)restaurant;
- (void)searchButtonTapped:(id)sender;

@end
