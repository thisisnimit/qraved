//
//  FiltersViewController.m
//  Qraved
//
//  Created by Jeff on 8/19/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "FiltersViewController.h"
#import "UIConstants.h"
#import "NotificationConsts.h"

#import "OfferTypeListViewController.h"
#import "FeatureListViewController.h"
#import "CuisineListViewController.h"
#import "AreaListViewController.h"

#import "UIView+Helper.h"
#import "UIViewController+Helper.h"
#import "Helper.h"
#import "UIColor+Helper.h"
#import "UIImage+Helper.h"

#import "FilterItemOfferCellView.h"
#import "FilterItemPriceCellView.h"
#import "FilterItemFeatureCellView.h"
#import "FilterItemGreatforCellView.h"
#import "FilterItemCuisineCellView.h"
#import "SearchCellLabel.h"
#import "TagButtonGridView.h"
#import "BottomButton.h"
#import "DBManager.h"
#import "NMRangeSlider.h"

#import "IMGFeature.h"
#import "IMGCuisine.h"
#import "IMGGreatfor.h"
#import "IMGOfferType.h"
#import "IMGPriceLevel.h"
#import "IMGDiscover.h"
#import "LocationListViewController.h"
#import "FilterItemDistrictCellView.h"
#import "IMGTagUpdate.h"
#import "TagListViewController.h"
#import "UIImage+RTTint.h"

#define SECTION_HEADER_HEIGHT 35

#define SAVE_BUTTON_VIEW_HEIGHT 60

@interface FiltersViewController ()

@end

@implementation FiltersViewController{
    UILabel *spaceHolderLabel;
    UILabel *titleLabel;
    UIButton *closeButton;
    UIButton *resetButton;

    UITableView *filtersTableView;
    NSMutableDictionary *filtersDictionary;

    UIButton *saveButton;
    UIView *saveButtonBackgroudView;

    NSMutableArray *selectedGreatforArray;

    NSArray *priceLevelArray;

    TagButtonGridView *buttonGridView;
    float greatforCellHeight;
    
    IMGDiscover *discover;
    
    float priceLevelLower;
    float priceLevelUpper;
    NMRangeSlider *standardSlider;
    
    NSMutableArray *tagUpdateArrM;
    
    NSMutableArray* tempArray;
    UIView *topview;
    BOOL sortSelected;
    BOOL rateSelected;
    UIView *sortAllView;
    UIView* rateAllView;
    NSMutableArray *feartureArr;
    IMGTagUpdate *featureTagUpdate;
    NSArray *sortImageArr;
    NSArray *featureImageArr;
    UILabel *allRateLable;
    NSMutableArray *selectArr;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        greatforCellHeight = 130;
    }
    return self;
}
-(id)initWithDiscover:(IMGDiscover *)paramDiscover{
    if (self = [super init]) {
        discover = paramDiscover;
        if(discover==nil){
            discover = [[IMGDiscover alloc]init];
        }
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Restaurant list filter page";
    [self loadFeatureTag];
    [self addNotifications];
    [self initData];
    [self loadTopBar];
    [self loadButtonGridView];
    [self loadTableView];
    [self loadSaveButtonView];
   
}
-(void)loadFeatureTag{

    feartureArr=[[NSMutableArray alloc]init];
    NSString *sqlStringWifi = [NSString stringWithFormat:@"select * from IMGTag where typeShows='Features'and name='Free Wifi' and cityId=%@;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]];
    FMResultSet *resultSetWifi = [[DBManager manager] executeQuery:sqlStringWifi];
    while ([resultSetWifi next])
    {
        IMGTag *tag=[[IMGTag alloc]init];
        [tag setValueWithResultSet:resultSetWifi];
        [feartureArr addObject:tag];
    }
    
    NSString *sqlStringOpen = [NSString stringWithFormat:@"select * from IMGTag where typeShows='Features'and name='Open 24 hours' and cityId=%@;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]];
    FMResultSet *resultSetOpen = [[DBManager manager] executeQuery:sqlStringOpen];
    while ([resultSetOpen next])
    {
        IMGTag *tag=[[IMGTag alloc]init];
        [tag setValueWithResultSet:resultSetOpen];
        [feartureArr addObject:tag];
    }
    
    NSString *sqlStringSmoking = [NSString stringWithFormat:@"select * from IMGTag where typeShows='Features'and name='Smoking Area' and cityId=%@;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]];
    FMResultSet *resultSetSmoking = [[DBManager manager] executeQuery:sqlStringSmoking];
    while ([resultSetSmoking next])
    {
        IMGTag *tag=[[IMGTag alloc]init];
        [tag setValueWithResultSet:resultSetSmoking];
        [feartureArr addObject:tag];
    }
    
    NSString *sqlStringMusic = [NSString stringWithFormat:@"select * from IMGTag where typeShows='Features'and name='Live Music' and cityId=%@;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]];
    FMResultSet *resultSetMusic = [[DBManager manager] executeQuery:sqlStringMusic];
    while ([resultSetMusic next])
    {
        IMGTag *tag=[[IMGTag alloc]init];
        [tag setValueWithResultSet:resultSetMusic];
        [feartureArr addObject:tag];
    }
    
    NSString *sqlStringBook = [NSString stringWithFormat:@"select * from IMGTag where typeShows='Features'and name='Book Online' and cityId=%@;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]];
    FMResultSet *resultSetBook = [[DBManager manager] executeQuery:sqlStringBook];
    while ([resultSetBook next])
    {
        IMGTag *tag=[[IMGTag alloc]init];
        [tag setValueWithResultSet:resultSetBook];
        [feartureArr addObject:tag];
    }
    
    NSString *sqlStringDelivery = [NSString stringWithFormat:@"select * from IMGTag where typeShows='Features'and name='Delivery Service' and cityId=%@;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]];
    FMResultSet *resultSetDelivery = [[DBManager manager] executeQuery:sqlStringDelivery];
    while ([resultSetDelivery next])
    {
        IMGTag *tag=[[IMGTag alloc]init];
        [tag setValueWithResultSet:resultSetDelivery];
        [feartureArr addObject:tag];
    }

    NSString *sqlStringAway = [NSString stringWithFormat:@"select * from IMGTag where typeShows='Features'and name='Take Away' and cityId=%@;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]];
    FMResultSet *resultSetAway = [[DBManager manager] executeQuery:sqlStringAway];
    while ([resultSetAway next])
    {
        IMGTag *tag=[[IMGTag alloc]init];
        [tag setValueWithResultSet:resultSetAway];
        [feartureArr addObject:tag];
        
    }

   
}
- (void)initData{

    IMGPriceLevel *priceLevel1 = [[IMGPriceLevel alloc] init];
    priceLevel1.priceLevelId = [NSNumber numberWithInt:1];
    priceLevel1.title=@"100k-199k";
    priceLevel1.dollors=@"$";
    IMGPriceLevel *priceLevel2 = [[IMGPriceLevel alloc] init];
    priceLevel2.priceLevelId = [NSNumber numberWithInt:2];
    priceLevel2.title=@"200k-299k";
    priceLevel2.dollors=@"$$";
    IMGPriceLevel *priceLevel3 = [[IMGPriceLevel alloc] init];
    priceLevel3.priceLevelId = [NSNumber numberWithInt:3];
    priceLevel3.title=@"300k-399k";
    priceLevel3.dollors=@"$$$";
    IMGPriceLevel *priceLevel4 = [[IMGPriceLevel alloc] init];
    priceLevel4.priceLevelId = [NSNumber numberWithInt:4];
    priceLevel4.title=@">400k";
    priceLevel4.dollors=@"$$$$";
    priceLevelArray = [[NSArray alloc] initWithObjects:priceLevel1,priceLevel2,priceLevel3,priceLevel4, nil];
    
    
    
    NSArray *priceLevels = discover.priceLevelArray;
    priceLevelLower = 0.0;
    priceLevelUpper = 1.0;

    for (int i=0; i<priceLevels.count; i++) {
        IMGPriceLevel *price = [priceLevels objectAtIndex:i];
        if (i == 0) {
            priceLevelLower = ([price.priceLevelId intValue]-1) * 1/4.0;
        }
        if (i == [priceLevels count]-1) {
            priceLevelUpper = ([price.priceLevelId intValue]) * 1/4.0;
        }
    }
    if (priceLevels==nil || priceLevels.count == 0) {
        priceLevelLower= 0.0;
        priceLevelUpper = 1.0;
    }
    
    tagUpdateArrM = [[NSMutableArray alloc] init];
    NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM IMGTagUpdate WHERE tagTypeId = 0 AND typeId !=50  ORDER BY typeId;"];
    FMResultSet *resultSet = [[DBManager manager] executeQuery:sqlString];
    while ([resultSet next])
    {
        IMGTagUpdate *tagUpdate=[[IMGTagUpdate alloc]init];
        [tagUpdate setValueWithResultSet:resultSet];
        [tagUpdateArrM addObject:tagUpdate];
    }

    sqlString = [NSString stringWithFormat:@"SELECT * FROM IMGTagUpdate WHERE shows != \"\" and tagTypeId != 0 and visible = 1 ORDER BY rank;"];
    FMResultSet *resultSet2 = [[DBManager manager] executeQuery:sqlString];
    while ([resultSet2 next])
    {
        IMGTagUpdate *tagUpdate=[[IMGTagUpdate alloc]init];
        [tagUpdate setValueWithResultSet:resultSet2];

        sqlString = [NSString stringWithFormat:@"SELECT * FROM IMGTag where cityId=%@ and status=1 and tagTypeId='%@' order by ranking;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID],tagUpdate.tagTypeId];
        FMResultSet *resultSet3 = [[DBManager manager] executeQuery:sqlString];
        if ([resultSet3 next])
        {
            [tagUpdateArrM addObject:tagUpdate];
        }
    }
    
    tempArray = [[NSMutableArray alloc] init];
}


-(void)addNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(filterd:) name:SEARCH_FILTER object:nil];
}

-(void)filterd:(NSNotification*)notification
{
    NSDictionary * userInfo  = notification.userInfo;
    discover = [userInfo objectForKey:@"discover"];
    discover.districtArray = discover.searchDistrictArray;
    if ([userInfo objectForKey:@"tempArray"]!=nil) {
        [tempArray addObject:[userInfo objectForKey:@"tempArray"]];
    }
    if (topview) {
        [topview removeAllSubviews];
        [self loadTopView];
    }
    
    [filtersTableView reloadData];
}


- (void)loadTopBar{
    if([UIDevice isLaterThanIos6])
    {
        spaceHolderLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 20)];
        [spaceHolderLabel setBackgroundColor:[UIColor colorC2060A]];
        [self.view addSubview:spaceHolderLabel];
    }

    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, spaceHolderLabel.frame.size.height, DeviceWidth, 45)];
    [titleLabel setText:L(@"Filters")];
    UIFont *titleLabelFont=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:18];
    [titleLabel setFont:titleLabelFont];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setBackgroundColor: [UIColor colorC2060A]];
    [self.view addSubview:titleLabel];

    closeButton = [[UIButton alloc] initWithFrame:CGRectMake(DeviceWidth-45, spaceHolderLabel.frame.size.height, 45, 45)];
    [closeButton setImage:[UIImage imageNamed:@"closeWhite" ] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeButton];

    resetButton = [[UIButton alloc] initWithFrame:CGRectMake(0, spaceHolderLabel.frame.size.height, 60, 45)];
    [resetButton setBackgroundColor:[UIColor clearColor]];
    [resetButton addTarget:self action:@selector(reset) forControlEvents:UIControlEventTouchUpInside];
    UIFont *resetButtonFont=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:13];
    UILabel *resetLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, 0, resetButton.frame.size.width, resetButton.frame.size.height)];
    [resetLabel setText:L(@"Reset")];
    [resetLabel setFont:resetButtonFont];
    [resetLabel setTextColor:[UIColor whiteColor]];
    [resetLabel setBackgroundColor:[UIColor clearColor]];
    [resetButton addSubview:resetLabel];
    [self.view addSubview:resetButton];

}

-(void)reset{
    discover.offArray=[[NSMutableArray alloc]initWithCapacity:0];
    discover.discoverId=nil;
    discover.districtArray=[[NSMutableArray alloc]initWithCapacity:0];
    discover.offerTypeArray=[[NSMutableArray alloc]initWithCapacity:0];
    discover.eventArray = [[NSMutableArray alloc]initWithCapacity:0];
    discover.tagsArr=[[NSMutableArray alloc]initWithCapacity:0];
    discover.occasionArray=[[NSMutableArray alloc]initWithCapacity:0];
    discover.cuisineArray=[[NSMutableArray alloc]initWithCapacity:0];
    discover.priceLevelArray=[[NSMutableArray alloc]initWithCapacity:0];
    discover.areaArray=[[NSMutableArray alloc]initWithCapacity:0];
    discover.searchDistrictArray=[[NSMutableArray alloc]initWithCapacity:0];
    discover.ratingNumber=nil;
    discover.sortby=nil;
    NSNotification *notification = [[NSNotification alloc] initWithName:SEARCH_RESTAURANT object:nil userInfo:@{@"discover":discover}];
    
    if([self.fvcDelegate respondsToSelector:@selector(search:)])
    {
        [self.fvcDelegate search:notification];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
//
//    [[NSNotificationCenter defaultCenter] postNotificationName:SEARCH_RESTAURANT object:nil userInfo:@{@"discover":discover}];

}

- (void)loadButtonGridView{
    buttonGridView = [[TagButtonGridView alloc] initWithFrame:CGRectMake(10, 10, DeviceWidth-20, greatforCellHeight) andDiscover:discover];
    NSMutableArray *occasionArray = [[NSMutableArray alloc]init];
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGTag where cityId=%@ and status=1 and type='Occasion' order by ranking;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]] successBlock:^(FMResultSet *resultSet) {
        while([resultSet next]) {
            IMGTag *tag = [[IMGTag alloc] init];
            [tag setValueWithResultSet:resultSet];
            [occasionArray addObject:tag];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"FiltersViewController loadButtonGridView error when get datas:%@",error.description);
    }];
    [buttonGridView setButtonGrids:occasionArray];
    greatforCellHeight = [buttonGridView fittedSize].height+80;
    [buttonGridView setFrame:CGRectMake(10, 10, DeviceWidth-20, greatforCellHeight)];
}

- (void)loadTableView{
    float filtersTableViewHeight = DeviceHeight-(titleLabel.frame.size.height);

    filtersTableViewHeight += (20 - [UIDevice heightDifference]);

    topview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 560)];
    if ([UIDevice isIphone6]) {
        topview.frame=CGRectMake(0, 0, DeviceWidth, 560);
    }
    if ([UIDevice isIphone5]||[UIDevice isIphone4Now]) {
        topview.frame=CGRectMake(0, 0, DeviceWidth, 530);

    }
    if ([UIDevice isIphone6Plus]) {
        topview.frame=CGRectMake(0, 0, DeviceWidth, 580);
        
    }
    
    topview.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:topview];
    [self loadTopView];
    filtersTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, spaceHolderLabel.frame.size.height+titleLabel.frame.size.height, DeviceWidth, filtersTableViewHeight) style:UITableViewStylePlain];
    filtersTableView.backgroundColor = [UIColor whiteColor];
    [filtersTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    filtersTableView.showsVerticalScrollIndicator=NO;
    filtersTableView.dataSource = self;
    filtersTableView.delegate = self;
    filtersTableView.tableHeaderView=topview;
    UIView *footView = [[UIView alloc] init];
    footView.frame = CGRectMake(0, 0, DeviceWidth, 70);
    filtersTableView.tableFooterView = footView;

    [self.view addSubview:filtersTableView];
}
-(void)loadTopView{

    
    UILabel *sortTitleLale=[[UILabel alloc]initWithFrame:CGRectMake(15, 10, DeviceWidth/2, 20)];
    sortTitleLale.text=@"Sort by";
    sortTitleLale.textColor=[UIColor grayColor];
    sortTitleLale.textAlignment=NSTextAlignmentLeft;
    [topview addSubview:sortTitleLale];
    
    float weight=(DeviceWidth-30)/4;
    NSArray *sortarr=@[@"Popularity",@"Rating",@"Price",@"Distance"];
    NSArray *sortnameArr=@[@"Popularity",@"rating",@"price",@"distance"];
    sortImageArr=@[@"ic_popularity",@"ic_rating_star",@"ic_price",@"ic_distance"];
    NSUInteger sortindex=0;
    if (discover.sortby==nil) {
        sortindex=0;
    }else{
       sortindex=[sortnameArr indexOfObject:discover.sortby];
    
    }
    sortAllView=[[UIView alloc]initWithFrame:CGRectMake(15,  sortTitleLale.endPointY+10, DeviceWidth-30, 50)];
    [topview addSubview:sortAllView];
    for (int i=0; i<4; i++) {
        
        UIButton* sortView=[[UIButton alloc]initWithFrame:CGRectMake((weight+1)*i,0, weight+2, 50)];
        sortView.layer.borderColor=[UIColor colorRed].CGColor;
        sortView.layer.borderWidth=1;
        sortView.tag=200+i;
        [sortView addTarget:self action:@selector(sortByTap:) forControlEvents:UIControlEventTouchUpInside];
        [sortAllView addSubview:sortView];
        
        UIImageView *sortimagview=[[UIImageView alloc]initWithFrame:CGRectMake(weight/2-9, 6, 17, 20)];
        if (i==0||i==1) {
            sortimagview.frame=CGRectMake(weight/2-9, 6, 20, 20);
        }
        sortimagview.tag=100;
        sortimagview.image=[UIImage imageNamed:sortImageArr[i]];
        [sortView addSubview:sortimagview];
        
        UILabel* sortlable=[[UILabel alloc]initWithFrame:CGRectMake(0, sortimagview.endPointY+3, weight, 20)];
        sortlable.tag=101;
        sortlable.text=sortarr[i];
        sortlable.textColor=[UIColor colorRed];
        sortlable.textAlignment=NSTextAlignmentCenter;
        sortlable.font=[UIFont systemFontOfSize:15];
        if (sortindex==i){
            sortView.backgroundColor=[UIColor colorRed];
            sortlable.textColor=[UIColor whiteColor];
            sortimagview.image=[[UIImage imageNamed:sortImageArr[i]] rt_tintedImageWithColor:[UIColor whiteColor]];

        }
        [sortView addSubview:sortlable];
       
        
    }
    
    UIView* sortLine=[[UIView alloc]initWithFrame:CGRectMake(15, 100, DeviceWidth-15, 1)];
    sortLine.backgroundColor=[UIColor colorWithRed:220.0/255.0 green:225.0/255.0 blue:225.0/255.0 alpha:1];
    [topview addSubview:sortLine];
    
    UILabel *ratingLale=[[UILabel alloc]initWithFrame:CGRectMake(15, sortLine.endPointY+10, DeviceWidth/2, 20)];
    ratingLale.text=@"Rating";
    ratingLale.textColor=[UIColor grayColor];
    ratingLale.textAlignment=NSTextAlignmentLeft;
    [topview addSubview:ratingLale];
    NSArray *ratingarr=@[@"",@"3+",@"4+",@"5"];
    NSArray *ratingImageArr=@[@"",@"★★★",@"★★★★",@"★★★★★"];
    int rateIndex ;
    if ([discover.ratingNumber intValue]==3) {
        rateIndex=1;
    }else if ([discover.ratingNumber intValue]==4){
    
        rateIndex=2;
    }else if ([discover.ratingNumber intValue]==5){
        rateIndex=3;
    
    }else{
        rateIndex=10;
    }
   rateAllView=[[UIView alloc]initWithFrame:CGRectMake(15,  ratingLale.endPointY+10, DeviceWidth-30, 45)];
    [topview addSubview:rateAllView];
    
    
    for (int i=0; i<4; i++) {
        
        UIButton* ratingView=[[UIButton alloc]initWithFrame:CGRectMake((weight+1)*i, 0, weight+2, 45)];
        ratingView.layer.borderColor=[UIColor colorWithRed:253.0/255.0 green:168.0/255.0 blue:41.0/255.0 alpha:1].CGColor;
        ratingView.layer.borderWidth=1;
        ratingView.tag=2000+i;
        [ratingView addTarget:self action:@selector(ratingBtnAct:) forControlEvents:UIControlEventTouchUpInside];
        [rateAllView addSubview:ratingView];
        
        if (i==0) {
            allRateLable=[[UILabel alloc]initWithFrame:CGRectMake(0, 12, weight, 20)];
            allRateLable.textAlignment=NSTextAlignmentCenter;
            allRateLable.textColor=[UIColor whiteColor];
            allRateLable.tag=1;
            allRateLable.text=@"All";
            [ratingView addSubview:allRateLable];
            if (rateIndex==10) {
              ratingView.backgroundColor=[UIColor colorWithRed:253.0/255.0 green:168.0/255.0 blue:41.0/255.0 alpha:1];
            }

        }else{
            UILabel *ratingNumLable=[[UILabel alloc]initWithFrame:CGRectMake(weight/2-13, 6, 30, 20)];
            ratingNumLable.tag=200;
            ratingNumLable.textAlignment=NSTextAlignmentCenter;
            ratingNumLable.textColor=[UIColor colorWithRed:253.0/255.0 green:168.0/255.0 blue:41.0/255.0 alpha:1];
            if ([UIDevice isIphone4Now]||[UIDevice isIphone5]) {
                ratingNumLable.font=[UIFont systemFontOfSize:15];

            }else{
            
                ratingNumLable.font=[UIFont systemFontOfSize:16];

            }
            ratingNumLable.text=ratingarr[i];
            [ratingView addSubview:ratingNumLable];
            UILabel *rateImageLable=[[UILabel alloc]initWithFrame:CGRectMake(0,ratingNumLable.endPointY-2 , weight, 18)];
            rateImageLable.tag=201;
            rateImageLable.textAlignment=NSTextAlignmentCenter;
            rateImageLable.textColor=[UIColor colorWithRed:253.0/255.0 green:168.0/255.0 blue:41.0/255.0 alpha:1];
            rateImageLable.text=ratingImageArr[i];
            if ([UIDevice isIphone4Now]||[UIDevice isIphone5]) {
                rateImageLable.font=[UIFont systemFontOfSize:14];
 
            }else{
                rateImageLable.font=[UIFont systemFontOfSize:15];

            }
            if (i==rateIndex) {
                allRateLable.textColor=[UIColor colorWithRed:253.0/255.0 green:168.0/255.0 blue:41.0/255.0 alpha:1];
                ratingView.backgroundColor=[UIColor colorWithRed:253.0/255.0 green:168.0/255.0 blue:41.0/255.0 alpha:1];
                ratingNumLable.textColor=[UIColor whiteColor];
                rateImageLable.textColor=[UIColor whiteColor];
                
            }
            
            [ratingView addSubview:rateImageLable];

        
        }

    }
    
    UIView* ratingline=[[UIView alloc]initWithFrame:CGRectMake(15, 195, DeviceWidth-15, 1)];
    ratingline.backgroundColor=[UIColor colorWithRed:220.0/255.0 green:225.0/255.0 blue:225.0/255.0 alpha:1];
    [topview addSubview:ratingline];
    

    UILabel *PriceLale=[[UILabel alloc]initWithFrame:CGRectMake(15, ratingline.endPointY+10, DeviceWidth/2, 20)];
    PriceLale.text=@"Price";
    PriceLale.textColor=[UIColor grayColor];
    PriceLale.textAlignment=NSTextAlignmentLeft;
    [topview addSubview:PriceLale];

    UILabel *priceNumLable=[[UILabel alloc]initWithFrame:CGRectMake(DeviceWidth/2-80, PriceLale.endPointY+5, 160, 20)];
    priceNumLable.font=[UIFont systemFontOfSize:16];
    priceNumLable.textAlignment=NSTextAlignmentCenter;
    priceNumLable.text=@"0K - 300K+";
    [topview addSubview:priceNumLable];
    standardSlider = [[NMRangeSlider alloc]initWithFrame:CGRectMake(5, priceNumLable.endPointY+5, DeviceWidth-2*5, 40)];
    [topview addSubview:standardSlider];
    [self configureMetalThemeForSlider:standardSlider];
    standardSlider.stepValue = 1/4.0;
    standardSlider.stepValueContinuously = NO;
    standardSlider.lowerValue = priceLevelLower;
    standardSlider.upperValue = priceLevelUpper;
  __block  CGFloat lowerValue;
  __block  CGFloat upperValue;
  __block  NSString *lowstring=@"";
  __block  NSString *upperstring=@"";
    if (priceLevelLower==0) {
        lowstring=@"0K";
    }else if (priceLevelLower==0.25){
        lowstring=@"100K";
    }else if (priceLevelLower==0.50){
        lowstring=@"200K";
    }else if (priceLevelLower==0.75)
    {
        lowstring=@"300K";
    }else{
        lowstring=@"300K+";
    }
    
    if (priceLevelUpper==0) {
        upperstring=@"0K";
    }else if (priceLevelUpper==0.25){
        upperstring=@"100K";
    }else if (priceLevelUpper==0.50){
        upperstring=@"200K";
    }else if (priceLevelUpper==0.75){
        upperstring=@"300K";
        
    }else{
        upperstring=@"300K+";
    }
    
    priceNumLable.text=[NSString stringWithFormat:@"%@ - %@",lowstring,upperstring];

    [standardSlider returnLowerValue:^(float value) {
        lowerValue=value;
        if (lowerValue==0) {
            lowstring=@"0K";
        }else if (lowerValue==0.25){
            lowstring=@"100K";
        }else if (lowerValue==0.50){
            lowstring=@"200K";
        }else if (lowerValue==0.75)
        {
            lowstring=@"300K";
        }else{
            lowstring=@"300K+";

        
        }
        priceNumLable.text=[NSString stringWithFormat:@"%@ - %@",lowstring,upperstring];
        
    } anduplower:^(float value) {
        upperValue=value;
        if (upperValue==0) {
            upperstring=@"0K";
        }else if (upperValue==0.25){
            upperstring=@"100K";
        }else if (upperValue==0.50){
            upperstring=@"200K";
        }else if (upperValue==0.75){
            upperstring=@"300K";

        }else{
            upperstring=@"300K+";
        }

        priceNumLable.text=[NSString stringWithFormat:@"%@ - %@",lowstring,upperstring];
        
    }];
    UILabel *lowerLable=[[UILabel alloc]initWithFrame:CGRectMake(10, standardSlider.endPointY+2, 100, 20)];
    lowerLable.text=@"Under 100K";
    lowerLable.font=[UIFont systemFontOfSize:15];
    lowerLable.textColor=[UIColor grayColor];
    [topview addSubview:lowerLable];
    
    UILabel *upperLable=[[UILabel alloc]initWithFrame:CGRectMake(standardSlider.endPointX-90, standardSlider.endPointY+2, 100, 20)];
    upperLable.text=@"Above 300K";
    upperLable.font=[UIFont systemFontOfSize:15];
    upperLable.textColor=[UIColor grayColor];
    [topview addSubview:upperLable];

    UIView* priceline=[[UIView alloc]initWithFrame:CGRectMake(15, upperLable.endPointY+10, DeviceWidth-15, 1)];
    priceline.backgroundColor=[UIColor colorWithRed:220.0/255.0 green:225.0/255.0 blue:225.0/255.0 alpha:1];
    [topview addSubview:priceline];
    
    
    UILabel *featureLale=[[UILabel alloc]initWithFrame:CGRectMake(15, priceline.endPointY+10, DeviceWidth/2, 20)];
    featureLale.text=@"Features";
    featureLale.textColor=[UIColor grayColor];
    featureLale.textAlignment=NSTextAlignmentLeft;
    [topview addSubview:featureLale];

    CGFloat feartureWide=(DeviceWidth-75)/4;
    NSArray* featureNameArr=@[@"Wifi",@"Open 24hr",@"Smoking",@"Live Music",@"Book Online",@"Delivery Service",@"Take Away"];
    NSArray* featureTagArr=@[@"Free Wifi",@"Open 24 hours",@"Smoking Area",@"Live Music",@"Book Online",@"Delivery Service",@"Take Away"];
    featureImageArr=@[@"ic_wifi",@"ic_open",@"ic_smoking",@"ic_live",@"ic_reservation",@"ic_delivery",@"ic_take"];
     selectArr=[[NSMutableArray alloc]init];
    if (discover.tagsArr.count>0) {
        
        for (int a=0; a<discover.tagsArr.count; a++) {
            IMGTag *tag=[discover.tagsArr objectAtIndex:a];
            for (int i=0; i<7; i++) {
                
                if ([tag.name isEqualToString:featureTagArr[i]]) {
                    [selectArr addObject:[NSNumber numberWithInt:i]];
                }
            }

        }
       

    }
    
    for (int i=0; i<8; i++) {
        if (i<7) {
            UIButton * featureView=[[UIButton alloc]initWithFrame:CGRectMake(15+(feartureWide+15)*(i%4),(featureLale.endPointY+10)+(feartureWide+10)*((int)i/4) , feartureWide, feartureWide)];
            featureView.layer.borderWidth=1;
            featureView.tag=3000+i;
            [featureView addTarget:self action:@selector(featureTapGestureAct:) forControlEvents:UIControlEventTouchUpInside];
            featureView.layer.borderColor=[UIColor grayColor].CGColor;
            [topview addSubview:featureView];

            UIImageView *imageview=[[UIImageView alloc]initWithFrame:CGRectMake(feartureWide/2-(feartureWide/3+5)/2,feartureWide/5, feartureWide/3+5, feartureWide/3+1)];
            imageview.tag=100;
            imageview.image=[UIImage imageNamed:featureImageArr[i]];
            [featureView addSubview:imageview];
            
            UILabel* lable=[[UILabel alloc]initWithFrame:CGRectMake(2, imageview.endPointY, feartureWide-4, 32)];
            lable.numberOfLines=2;
            lable.tag=101;
            lable.textAlignment=NSTextAlignmentCenter;
            lable.textColor=[UIColor grayColor];
            lable.font=[UIFont systemFontOfSize:13];
            lable.text=featureNameArr[i];
            if ([UIDevice isIphone4Now]||[UIDevice isIphone5]) {
                lable.frame=CGRectMake(3, imageview.endPointY, feartureWide-6, 26);
                lable.adjustsFontSizeToFitWidth=YES;

            }
            [featureView addSubview:lable];
            
            for (int b=0; b<selectArr.count; b++) {
                int index=[selectArr[b] intValue];
                if (i==index) {
                    featureView.selected=YES;
                    IMGTag * object= [feartureArr objectAtIndex:i];
                    [discover addTag:object];
                    
                }
                
            }
            if (featureView.selected) {
                featureView.backgroundColor=[UIColor grayColor];
                imageview.image=[[UIImage imageNamed:featureImageArr[i]] rt_tintedImageWithColor:[UIColor whiteColor]];
                lable.textColor=[UIColor whiteColor];


            }
        }else{
            UIButton * featureView=[[UIButton alloc]initWithFrame:CGRectMake(15+(feartureWide+15)*(i%4),(featureLale.endPointY+10)+(feartureWide+10)*((int)i/4) , feartureWide, feartureWide)];
            featureView.layer.borderWidth=1;
            featureView.tag=3000+i;
            [featureView addTarget:self action:@selector(feartureAllTap:) forControlEvents:UIControlEventTouchUpInside];
            featureView.layer.borderColor=[UIColor grayColor].CGColor;
            [topview addSubview:featureView];

            UILabel* allLable=[[UILabel alloc]initWithFrame:CGRectMake(0, feartureWide/2-10, feartureWide, 20)];
            
            allLable.textAlignment=NSTextAlignmentCenter;
            allLable.textColor=[UIColor grayColor];
            allLable.font=[UIFont systemFontOfSize:16];
            allLable.text=@"See All";
            [featureView addSubview:allLable];
        
        }
        
    }
    
}
-(void)feartureAllTap:(UIButton*)btn{

    NSMutableArray *featureArrs=[[NSMutableArray alloc]init];
    NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM IMGTagUpdate where type='Features';"];
    FMResultSet *resultSet = [[DBManager manager] executeQuery:sqlString];
    while ([resultSet next])
    {
        IMGTagUpdate *tagUpdate=[[IMGTagUpdate alloc]init];
        [tagUpdate setValueWithResultSet:resultSet];
        [featureArrs addObject:tagUpdate];
    }
    if (featureArrs.count>0) {
         featureTagUpdate=[featureArrs objectAtIndex:0];
    }
    TagListViewController *tagListViewController = [[TagListViewController alloc]initWithDiscover:discover andTagUpdate:featureTagUpdate];
    [self.navigationController pushViewController:tagListViewController animated:YES];



}
-(void)featureTapGestureAct:(UIButton*)btn{
    btn.selected=!btn.selected;
    if (!btn.selected) {
        btn.backgroundColor=[UIColor whiteColor];
        for (UIView *view in btn.subviews) {
            if (view.tag==100) {
                UIImageView *imageview=(UIImageView*)view;
                imageview.image=[UIImage imageNamed:featureImageArr[btn.tag-3000]];
            }else if (view.tag==101){
                
                UILabel *lable=(UILabel*)view;
                lable.textColor=[UIColor grayColor];
            }
        }
    }else{

        btn.backgroundColor=[UIColor grayColor];
        for (UIView *view in btn.subviews) {
            if (view.tag==100) {
                UIImageView *imageview=(UIImageView*)view;
                imageview.image=[[UIImage imageNamed:featureImageArr[btn.tag-3000]] rt_tintedImageWithColor:[UIColor whiteColor]];
            }else if (view.tag==101){
                
                UILabel *lable=(UILabel*)view;
                lable.textColor=[UIColor whiteColor];
            }
        }
    }
    
    if (btn.tag<3008) {
     IMGTag * object= [feartureArr objectAtIndex:btn.tag-3000];
        if(![discover hasTag:object]){
            [discover addTag:object];
        }else{
            [discover removeTag:object];
        }

    }
    
    
    
    
    
}
-(void)ratingBtnAct:(UIButton*)btn{
    for (int i=0;i<4;i++) {
        if (i==0) {
            UIButton *btn=rateAllView.subviews[i];
            btn.backgroundColor=[UIColor whiteColor];
            for (UILabel *lable in btn.subviews) {
                if (lable.tag==1) {
                    lable.textColor=[UIColor colorWithRed:253.0/255.0 green:168.0/255.0 blue:41.0/255.0 alpha:1];
                }
            }
        }else{
            UIButton *btn=rateAllView.subviews[i];
            btn.selected=NO;
            btn.backgroundColor=[UIColor whiteColor];
            for (UIView *view in btn.subviews ) {
                if (view.tag==200) {
                    UILabel *imageV=(UILabel*)view;
                    imageV.textColor=[UIColor colorWithRed:253.0/255.0 green:168.0/255.0 blue:41.0/255.0 alpha:1];
                }else if (view.tag==201){
                    UILabel *lable=(UILabel*)view;
                    lable.textColor=[UIColor colorWithRed:253.0/255.0 green:168.0/255.0 blue:41.0/255.0 alpha:1];
                }
            }
        }
    }
    if (btn.tag==2000) {
        discover.ratingNumber=nil;
            btn.backgroundColor=[UIColor colorWithRed:253.0/255.0 green:168.0/255.0 blue:41.0/255.0 alpha:1];
            for (UILabel *lable in btn.subviews ) {
                if (lable.tag==1) {
                    lable.textColor=[UIColor whiteColor];
                }
            }
    }else{
        switch (btn.tag) {
            case 2001:
            {
                discover.ratingNumber=@3;
            }
                break;
            case 2002:
            {
                discover.ratingNumber=@4;

            }
                break;
            case 2003:
            {
                discover.ratingNumber=@5;
   
            }
                break;

                
            default:
                break;
        }
        btn.backgroundColor=[UIColor colorWithRed:253.0/255.0 green:168.0/255.0 blue:41.0/255.0 alpha:1];
        for (UILabel *lable in btn.subviews ) {
          if (lable.tag==200) {
              lable.textColor=[UIColor whiteColor];
            }else if (lable.tag==201){
                lable.textColor=[UIColor whiteColor];
            }
        }
    }
}
-(void)sortByTap:(UIButton*)btn{
    int i=0;
    for (UIButton*views in sortAllView.subviews) {
        
        views.selected=NO;
        views.backgroundColor=[UIColor whiteColor];
        for (UIView *view in views.subviews ) {
        
            if (view.tag==100) {
                UIImageView *imageV=(UIImageView*)view;
                imageV.image=[UIImage imageNamed:sortImageArr[i]];
            }else if (view.tag==101){
                UILabel *lable=(UILabel*)view;
                lable.textColor=[UIColor colorRed];
                
            }
            
        }
        i++;
    }
    
    btn.backgroundColor=[UIColor colorRed];
            
    for (UIView *view in btn.subviews ) {
                
        if (view.tag==100) {
                    
            UIImageView *imageV=(UIImageView*)view;
            imageV.image=[[UIImage imageNamed:sortImageArr[btn.tag-200]] rt_tintedImageWithColor:[UIColor whiteColor]];
                    
        }else if (view.tag==101){
            UILabel *lable=(UILabel*)view;
            lable.textColor=[UIColor whiteColor];
                    
        }
                
    }
    switch (btn.tag) {
        case 200:
        {
        discover.sortby=SORTBY_POPULARITY;
        }
            break;
        case 201:
        {
            discover.sortby=@"rating";
        }
            break;
        case 202:
        {
            discover.sortby=@"price";
        }
            break;
        case 203:
        {
            discover.sortby=@"distance";
        }
            break;

            
        default:
            break;
    }
}
- (void)loadSaveButtonView{
    float saveButtonBackgroudViewY = DeviceHeight-(50-[UIDevice heightDifference]);
    UIView *saveView=[[UIView alloc]initWithFrame:CGRectMake(0, saveButtonBackgroudViewY-5, DeviceWidth, CELL_HEIGHT_50+5)];
    saveView.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:saveView];
    saveButton=[[UIButton alloc] initWithFrame: CGRectMake(4, 2, DeviceWidth-8, CELL_HEIGHT_50)];
    saveButton.layer.cornerRadius=18;
    [saveButton setTitle:L(@"Apply") forState:UIControlStateNormal];
    saveButton.backgroundColor=[UIColor colorRed];
    [saveButton addTarget:self action:@selector(saveFilters) forControlEvents:UIControlEventTouchUpInside];
    [saveView addSubview:saveButton];

}

-(void)close{
    if ( (tempArray !=nil ) && (tempArray.count !=0 )) {
        for (id item in tempArray) {
            if ([item isKindOfClass:[IMGArea class]]) {
                [discover removeArea:((IMGArea*)item)];
            }if ([item isKindOfClass:[IMGCuisine class]]) {
                [discover removeCuisine:((IMGCuisine*)item)];
            }if ([item isKindOfClass:[IMGTag class]]) {
                [discover removeTag:((IMGTag*)item)];
            }if ([item isKindOfClass:[IMGOfferType class]]) {
                [discover removeOfferType:((IMGOfferType*)item)];
            }if ([item isKindOfClass:[IMGDistrict class]]) {
                [discover removeDistrict:((IMGDistrict*)item)];
            }
        }
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)saveFilters{
    IMGPriceLevel *priceLevel1 = [[IMGPriceLevel alloc] init];
    priceLevel1.priceLevelId = [NSNumber numberWithInt:1];
    priceLevel1.title=@"100k-199k";
    priceLevel1.dollors=@"$";
    IMGPriceLevel *priceLevel2 = [[IMGPriceLevel alloc] init];
    priceLevel2.priceLevelId = [NSNumber numberWithInt:2];
    priceLevel2.title=@"200k-299k";
    priceLevel2.dollors=@"$$";
    IMGPriceLevel *priceLevel3 = [[IMGPriceLevel alloc] init];
    priceLevel3.priceLevelId = [NSNumber numberWithInt:3];
    priceLevel3.title=@"300k-399k";
    priceLevel3.dollors=@"$$$";
    IMGPriceLevel *priceLevel4 = [[IMGPriceLevel alloc] init];
    priceLevel4.priceLevelId = [NSNumber numberWithInt:4];
    priceLevel4.title=@">400k";
    priceLevel4.dollors=@"$$$$";
    
    priceLevelLower = standardSlider.lowerValue;
    priceLevelUpper = standardSlider.upperValue;
    NSMutableArray *priceLavels = [[NSMutableArray alloc]init];
    if (priceLevelLower == 0) {
        if (priceLevelUpper == 0||priceLevelUpper ==0.25) {
            [priceLavels addObject:priceLevel1];
        }else if (priceLevelUpper ==0.50){
            [priceLavels addObject:priceLevel1];
            [priceLavels addObject:priceLevel2];
        }else if (priceLevelUpper ==0.75){
            [priceLavels addObject:priceLevel1];
            [priceLavels addObject:priceLevel2];
            [priceLavels addObject:priceLevel3];
        }else if (priceLevelUpper == 1){
            [priceLavels addObject:priceLevel1];
            [priceLavels addObject:priceLevel2];
            [priceLavels addObject:priceLevel3];
            [priceLavels addObject:priceLevel4];
        }
    }else if (priceLevelLower ==0.25){
        if (priceLevelUpper ==0.25){
            [priceLavels addObject:priceLevel2];
        }else if (priceLevelUpper ==0.50){
            [priceLavels addObject:priceLevel2];
        }else if (priceLevelUpper ==0.75){
            [priceLavels addObject:priceLevel2];
            [priceLavels addObject:priceLevel3];
        }else if (priceLevelUpper == 1){
            [priceLavels addObject:priceLevel2];
            [priceLavels addObject:priceLevel3];
            [priceLavels addObject:priceLevel4];
        }
    
    }else if (priceLevelLower== 0.50 ){
        if (priceLevelUpper ==0.50){
            [priceLavels addObject:priceLevel3];
        }else if (priceLevelUpper ==0.75 ){
            [priceLavels addObject:priceLevel3];
        }else if (priceLevelUpper == 1){
            [priceLavels addObject:priceLevel3];
            [priceLavels addObject:priceLevel4];
        }
    }else if (priceLevelLower ==0.75){
        if (priceLevelUpper ==0.75){
            [priceLavels addObject:priceLevel4];
        }else if (priceLevelUpper == 1){
            [priceLavels addObject:priceLevel4];
        }

    
    }else if (priceLevelLower == 1){
        if (priceLevelUpper == 1){
            [priceLavels addObject:priceLevel4];
        }
    }
    discover.priceLevelArray = priceLavels;
    [self performSelectorOnMainThread:@selector(createData) withObject:nil waitUntilDone:NO];
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)createData
{
//    [[NSNotificationCenter defaultCenter] postNotificationName:SEARCH_RESTAURANT object:nil userInfo:@{@"discover":discover}];
    
    NSNotification *notification = [[NSNotification alloc] initWithName:SEARCH_RESTAURANT object:nil userInfo:@{@"discover":discover}];
    
    if([self.fvcDelegate respondsToSelector:@selector(search:)])
    {
        [self.fvcDelegate search:notification];
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}


#pragma mark UITable datasource and delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return tagUpdateArrM.count + 1;

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
//    if (section == 0)
//    {
//        return 0;
//    }
//    else if (section == 1)
//    {
//        return SECTION_HEADER_HEIGHT;
//    }
//    else
//    {
//        return 10;
//    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    switch (section) {
        case 0:
            return nil;
//        case 1:
//        {
//            SearchCellLabel *sectionTitleLabel = [[SearchCellLabel alloc] initWithBlackColorAndFrame:CGRectMake(0, 0, DeviceWidth, SECTION_HEADER_HEIGHT)];
//            [sectionTitleLabel setText:L(@"   Price")];
//            [sectionTitleLabel setBackgroundColor: [UIColor colorEDEDED]];
//            return sectionTitleLabel;
//        }
        default:
        {
            SearchCellLabel *sectionTitleLabel = [[SearchCellLabel alloc] initWithBlackColorAndFrame:CGRectMake(0, 0, DeviceWidth, 10)];
            [sectionTitleLabel setBackgroundColor:[UIColor clearColor]];
            return sectionTitleLabel;
        }
            return nil;
    }

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"FilterCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];

    FilterItemOfferCellView *filterItemOfferCellView;
//    FilterItemPriceCellView *filterItemPriceCellView;
//    FilterItemCuisineCellView *filterItemCuisineCellView;
//    FilterItemFeatureCellView *filterItemFeatureCellView;
//    FilterItemGreatforCellView *filterItemGreatforCellView;
//    FilterItemDistrictCellView *locationCellView;
    FilterItemCellView *filterItemCellView;
    @autoreleasepool {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier ];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        switch (indexPath.section) {
            case 0:
            {
                filterItemOfferCellView = [[FilterItemOfferCellView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, CELL_HEIGHT_50) discover:discover title:L(@"Promo") andTagUpdate:nil];
                [cell.contentView addSubview:filterItemOfferCellView];
                UIImageView *shadowTopImage = [[UIImageView alloc]initWithFrame:CGRectMake(15,0, DeviceWidth, 1)];
                shadowTopImage.backgroundColor=[UIColor colorWithRed:220.0/255.0 green:225.0/255.0 blue:225.0/255.0 alpha:1];
                [filterItemOfferCellView addSubview:shadowTopImage];

                UIImageView *shadowImage = [[UIImageView alloc]initWithFrame:CGRectMake(15, filterItemOfferCellView.height, DeviceWidth, 1)];
                shadowImage.backgroundColor=[UIColor colorWithRed:220.0/255.0 green:225.0/255.0 blue:225.0/255.0 alpha:1];
                [filterItemOfferCellView addSubview:shadowImage];
            }
                break;
//            case 1:
//            {
//                UIFont *titleFont=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14];
//                standardSlider = [[NMRangeSlider alloc]initWithFrame:CGRectMake(5, -3, DeviceWidth-2*5, 40)];
//                [cell.contentView addSubview:standardSlider];
//                [self configureMetalThemeForSlider:standardSlider];
//                standardSlider.stepValue = 1/3.0;
//                standardSlider.stepValueContinuously = NO;
//                standardSlider.lowerValue = priceLevelLower;
//                standardSlider.upperValue = priceLevelUpper;
//                NSArray *array = @[@"$",@"$$",@"$$$",@"$$$$"];
//                for (int i=0; i<4; i++) {
//                    CGSize size = [[array objectAtIndex:i] sizeWithFont:titleFont];
//                    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(20+standardSlider.stepValue*i*(DeviceWidth-3*LEFTLEFTSET)-size.width/2, 32, size.width, 20)];
//                    label.font = titleFont;
//                    label.textColor = [UIColor color222222];
//                    label.textAlignment = NSTextAlignmentCenter;
//                    label.text = [array objectAtIndex:i];
//                    [cell.contentView addSubview:label];
//                }
//                
//            }
//                break;
            default:
            {
                NSArray * array =  cell.contentView.subviews ;
                for (UIView * view in array) {
                    [view removeFromSuperview];
                }
                IMGTagUpdate *tagUpdate = [tagUpdateArrM objectAtIndex:(int)indexPath.section - 1];
                filterItemCellView = [[FilterItemCellView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, CELL_HEIGHT_50) discover:discover  title:tagUpdate.shows andTagUpdate:tagUpdate];
                

                [cell.contentView addSubview:filterItemCellView];
                UIImageView *shadowImage = [[UIImageView alloc]initWithFrame:CGRectMake(15, filterItemCellView.height, DeviceWidth, 1)];
                shadowImage.backgroundColor=[UIColor colorWithRed:220.0/255.0 green:225.0/255.0 blue:225.0/255.0 alpha:1];
                [filterItemCellView addSubview:shadowImage];
            }
                break;
        }
    }
    return cell;
}
- (void) configureMetalThemeForSlider:(NMRangeSlider*) slider
{
    UIImage* image = nil;
    
    image = [UIImage imageWithColor:[UIColor grayColor] size:CGSizeMake(DeviceWidth-2*5, 20)];
//    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, 5.0, 0.0, 5.0)];
    slider.trackBackgroundImage = image;
    
    image = [UIImage imageWithColor:[UIColor colorRed] size:CGSizeMake(DeviceWidth-2*5, 20)];
//    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, 7.0, 0.0, 7.0)];
    slider.trackImage = image;
    
    
    
    image = [UIImage imageNamed:@"ic_knob"];
    slider.lowerHandleImageNormal = image;
    slider.upperHandleImageNormal = image;
    
//
//    image = [UIImage imageNamed:@"slider-metal-handle-highlighted"];
//    slider.lowerHandleImageHighlighted = image;
//    slider.upperHandleImageHighlighted = image;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==tagUpdateArrM.count+2){
        return greatforCellHeight;
    }
    return CELL_HEIGHT_50;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (indexPath.section == 0)
    {
        
        priceLevelLower = standardSlider.lowerValue;
        priceLevelUpper = standardSlider.upperValue;

        OfferTypeListViewController *offerTypeListViewController = [[OfferTypeListViewController alloc]initWithDiscover:discover];
        [self.navigationController pushViewController:offerTypeListViewController animated:YES];
        return;
    }
    IMGTagUpdate *tagUpdate = [tagUpdateArrM objectAtIndex:indexPath.section - 1];
    if ([tagUpdate.typeId intValue] == 1)
    {
        priceLevelLower = standardSlider.lowerValue;
        priceLevelUpper = standardSlider.upperValue;
        
        AreaListViewController *areaListViewController = [[AreaListViewController alloc]initWithDiscover:discover];
        [self.navigationController pushViewController:areaListViewController animated:YES];
    }
    else if ([tagUpdate.typeId intValue] == 3)
    {
        priceLevelLower = standardSlider.lowerValue;
        priceLevelUpper = standardSlider.upperValue;

        CuisineListViewController *cuisineListViewController = [[CuisineListViewController alloc]initWithDiscover:discover];
        [self.navigationController pushViewController:cuisineListViewController animated:YES];
    }
    else if ([tagUpdate.typeId intValue] == 2)
    {
        priceLevelLower = standardSlider.lowerValue;
        priceLevelUpper = standardSlider.upperValue;

        LocationListViewController *locationListViewController = [[LocationListViewController alloc]initWithHead:discover];
        [self.navigationController pushViewController:locationListViewController animated:YES];
    }
    else
    {
        priceLevelLower = standardSlider.lowerValue;
        priceLevelUpper = standardSlider.upperValue;

        TagListViewController *tagListViewController = [[TagListViewController alloc]initWithDiscover:discover andTagUpdate:tagUpdate];
        [self.navigationController pushViewController:tagListViewController animated:YES];
    }
}

 
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SEARCH_FILTER object:nil];
    
}











@end
