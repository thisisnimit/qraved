//
//  FiltersViewController.h
//  Qraved
//
//  Created by Jeff on 8/19/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "IMGDiscover.h"

@protocol FiltersViewControllerDelegate <NSObject>

- (void)search:(NSNotification *)notification;

@end


@interface FiltersViewController : BaseChildViewController<UITableViewDataSource,UITableViewDelegate>

-(id)initWithDiscover:(IMGDiscover *)paramDiscover;

@property (nonatomic,assign) id<FiltersViewControllerDelegate>fvcDelegate;


@end
