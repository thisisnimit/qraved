
//
//  RestaurantListViewController.m
//  Qraved
//
//  Created by Jeff on 8/14/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "RestaurantsViewController.h"
#import "EGORefreshTableHeaderView.h"
#import "UIConstants.h"
#import "NotificationConsts.h"
#import "AppDelegate.h"
#import "PaxBookTimePickerViewController.h"
#import "SortTypeListViewController.h"
#import "MapViewController.h"
#import "DetailViewController.h"
#import "FiltersViewController.h"
#import "Label.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "UIImage+Helper.h"
#import "UIImage+Resize.h"
#import "UIImageView+WebCache.h"
#import "UIColor+Helper.h"
#import "UIViewController+Helper.h"
#import "UIDevice+Util.h"
#import "RestaurantCell.h"
#import "RestaurantCellView.h"
#import "LoadingView.h"
#import "IMGRestaurant.h"
#import "IMGEvent.h"
#import "IMGSortType.h"
#import "IMGDistrict.h"
#import "DBManager.h"
#import "RestaurantHandler.h"
#import "BookUtil.h"
#import "MapUtil.h"
#import "IMGRestaurantEvent.h"
//#import "MixpanelHelper.h"
#import "SearchUtil.h"
#import "LoadingImageView.h"
#import "DiscoverListViewController.h"
#import "RestaurantSuggestViewController.h"
#import "DiscoverUtil.h"
#import "SearchPageResturantCell.h"
#import "ReviewViewController.h"
#import "SearchPhotosViewController.h"
#import "SpecialOffersViewController.h"
#import "MenuPhotoGalleryController.h"
#import "SimpleLoadingImageView.h"
#import "SelectPaxViewController.h"
#import "IMGDistrictAndLandmark.h"
#import "IMGCuisineAndFoodType.h"
#import "AppDelegate.h"
#import "UIImage+RTTint.h"


#define SEARCHBAR_WIDTH 300
#define SEARCHBAR_HEIGHT 34

#define SMALL_MAP_HEIGHT 161*[AppDelegate ShareApp].autoSizeScaleY
#define CONDITION_BUTTON_HEIGHT 45

#define SEARCH_BUTTON_WIDTH 100
#define SORT_BUTTON_WIDTH 106
#define FILTER_BUTTON_WIDTH 90
#define BOOK_DATE_TIME_BUTTON_WIDTH 106

#define REFRESH_HEADER_HEIGHT 52.0f

#define LOADING_CELL_TAG 1273

#define navigationHeight (44+[UIDevice heightDifference])
#define NEARBY_OFFSET 60

@interface RestaurantsViewController ()
@property (nonatomic,strong)  DiscoverListViewController *discoverListViewController;

@end

@implementation RestaurantsViewController{
    
    LoadingImageView *loadingImageView;
    UISearchBar *restaurantSearchBar;
//    UIControl *backGroundControl;
    BOOL showMap;
    BOOL showEvent;
    IMGEvent *event;
    IMGDiscover *discover;
    EGORefreshTableHeaderView * refreshTableHeaderView;
    UIImageView *eventImageView;
    MapViewController *mapViewController;
    UITableView  *searchResultsTableView;
    
    UIControl *coverControl;
    UIButton *mapSearchLineControl;
    
    //>>>>>>>>>>>>>>>>condition button area refresh about UIs>>>>>>>>>>>>>>>>
    UIView *restaurantListView;
    
    UIImage *sideButtonImage ,*middleButtonImage;
    
    UIButton *sortButton ,*filterButton ,*bookDateTimeButton;
    UIControl *bookDateTimeControl;
    
    UILabel *mapSearchLineLabel, *sortLabel, *sortValueLabel, *filterLabel, *filterValueLabel ,*bookDateTimeLabel;
    TYAttributedLabel *bookDateTimeValueLabel;

    UIImage *arrowImage;
    UIImageView *arrow1ImageView;
    UIImageView *arrow2ImageView;
    
    float restaurantListViewBottomY;
    float restaurantListViewTopY;
    //<<<<<<<<<<<<<<<<condition button area refresh about UIs<<<<<<<<<<<<<<<<

    //>>>>>>>>>>>>>>>>search refresh about UIs>>>>>>>>>>>>>>>>
    CGFloat contentOffsetY;
    CGFloat oldContentOffsetY;
    CGFloat newContentOffsetY;
    
    UIView *refreshHeaderView;
    UILabel *refreshLabel;
    UIImageView *refreshArrow;
    UIActivityIndicatorView *refreshSpinner;

    BOOL isLoading;
    BOOL isSearched;
    NSString *textPull;
    NSString *textRelease;
    NSString *textLoading;
    
    NSInteger offset;
    //<<<<<<<<<<<<<<<<search refresh about UIs<<<<<<<<<<<<<<<<

    NSString *keyword;
    UIButton *navigateButton;
    

    NSNumber *latitude;
    NSNumber *longitude;
    
    IMGRestaurant *selectedRestaurant;
    UIImageView *firstImageView;
    
    IMGRestaurantOffer *currentOffer;
    NSArray *discoverArr;
    
    UITableView *optionTableView;
    
    NSNumber *minLatitudeTemp;
    NSNumber *maxLatitudeTemp;
    NSNumber *minLongitudeTemp;
    NSNumber *maxLongitudeTemp;
    
    NSArray *tutorialStrArr;
    
    BOOL isHadNextData;
    BOOL isStartLoad;
    BOOL isExceedMax;
    NSInteger lastPageCount;
    NSInteger scrollviewSpacing;
    NSInteger currentPageNum;
    
    IMGDiscover *lastDiscover;
    BOOL isLastDismissBtn;
    NSTimeInterval beginLoadingData;
    NSTimeInterval finishUI;
    
    BOOL firstTapBookNow;
    UIView *navigationView;
    UIButton *backButton;
    UIButton *shareButton;
    UIView* successTutorialView;
    
    UIView *suggestHeaderView;
    UILabel *searchValueLabel;
    
    BOOL isHomeNearby;
    BOOL isMapSearch;
    
    MKMapView *mapView;
    UIBarButtonItem *searchBtnRight;
    BOOL offerBtnClicked;
    BOOL openNowBtnClicked;
    BOOL myListBtnClicked;
    UILabel* title;
    UIButton *mapBtnRight;
}

-(void)initDiscover:(IMGDiscover *)tmpDiscover{
    discover=tmpDiscover;
}

-(IMGDiscover *)getDiscover{
    return discover;
}

- (id)initWithEventId:(NSUInteger)paramEventId
{
    self = [super init];
    if (self) {
        showEvent = TRUE;
        showMap = FALSE;
        self.eventId = paramEventId;
        [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGEvent WHERE eventId = %ld;",(long)self.eventId] successBlock:^(FMResultSet *resultSet) {
            if ([resultSet next]) {
                event = [[IMGEvent alloc]init];
                [event setValueWithResultSet:resultSet];
            }
            [resultSet close];
        }failureBlock:^(NSError *error) {
            NSLog(@"eventsImagePressed error when get datas:%@",error.description);
        }];
        showCloseButton = TRUE;
//        if(event.name){
//            [MixpanelHelper trackViewEventWithEventName:event.name];
//        }
    }
    return self;
}

- (id)initWithEvent:(IMGEvent *)tmpEvent
{
    self = [super init];
    if (self) {
        showEvent = TRUE;
        showMap = FALSE;
        event = tmpEvent;
        showCloseButton = TRUE;
    }
    return self;
}

- (id)initWithMap
{
    self = [super init];
    if (self) {
        showEvent = FALSE;
        showMap = TRUE;
        showCloseButton = NO;
        
    }
    return self;
}

-(void)removeNotifications{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:SEARCH_RESTAURANT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GO_DETAIL_FROM_SEARCH object:nil];
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)goToBackViewController{
    [restaurantSearchBar resignFirstResponder];

    for(UIView *view in self.view.subviews){
        [view removeFromSuperview];
    }
    if(self.backToPrevious){
        [self.navigationController popViewControllerAnimated:YES];
    }else if(showEvent){
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

  
    title.text = @"Restaurants";
    [self addNotifications];
    firstTapBookNow = YES;
//    if (self.isFromTabMenu) {
        [[self rdv_tabBarController] setTabBarHidden:NO animated:NO];
    //}
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self removeNotifications];
    self.navigationController.navigationBarHidden = NO;

        [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];

    title.text = @"";

}

- (void)GAITrack
{
    switch ([discover.fromWhere intValue])
    {
        case 1:
        {
            self.screenName = @"Restaurant list nearby page";
        }
            break;
        case 4:
        {
            self.screenName = @"Event Detail Page";
        }
            break;
        case 9:
        {
            self.screenName = @"Restaurant Event detail page";
        }
            break;
            
        default:
        {
            self.screenName = @"Restaurant list search result page";
        }
            break;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self GAITrack];
    offerBtnClicked = YES;
    openNowBtnClicked = YES;
    myListBtnClicked = YES;
    minLatitudeTemp = 0;
    maxLatitudeTemp = 0;
    minLongitudeTemp = 0;
    maxLongitudeTemp = 0;
    [self.view setBackgroundColor:[UIColor whiteColor]];
    [NSUserDefaults standardUserDefaults];
    [self loadHeadView];
    [self loadDefaultSearchViewFactors];
    [self loadSearchBar];
//    [self loadRevealController];
    title=[[UILabel alloc]initWithFrame:CGRectMake(DeviceWidth/2-80, 10, 160, 20)];
    title.text=@"Restaurants";
    title.textAlignment=NSTextAlignmentCenter;
    title.textColor=[UIColor whiteColor];
    [self.navigationController.navigationBar addSubview:title];
    if(showMap){
        restaurantListViewTopY = 0;
        [self loadMapView];
        [self loadRestaurantListView];

    }else if(showEvent){
        restaurantListViewTopY = 160*[AppDelegate ShareApp].autoSizeScaleY+1;
        [self loadEventView];
        [self loadRestaurantListView];

    }else{
        restaurantListViewTopY = 0;
        [self loadRestaurantListView];

    }
    
    if (self.isNearBy) {
        isHomeNearby = YES;
        [self search:nil];
    }else{
        [self search:nil];
    }
//    [self addTutorialView];
    
    if (self.isFromPersonnazation) {
        [self loadSuccessTutorialView];
    }
    
    if (self.isTomap) {
        [self searchButtonTapped:self];
    }
    
}
-(void)loadHeadView{
    UIBarButtonItem *leftBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:NavigationBackImage] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
    leftBtn.tintColor=[UIColor blackColor];
    self.navigationItem.leftBarButtonItem=leftBtn;
   
    if (!_isTomap) {
        searchBtnRight=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"restaurants_map"] style:UIBarButtonItemStylePlain target:self action:@selector(searchButtonTapped:)];

    }else{
        searchBtnRight=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search_map"] style:UIBarButtonItemStylePlain target:self action:@selector(searchButtonTapped:)];

    }
    searchBtnRight.tintColor=[UIColor blackColor];
    self.navigationItem.rightBarButtonItem=searchBtnRight;
}
-(void)loadSuccessTutorialView{
    
    
    successTutorialView=[[UIView alloc]initWithFrame:CGRectMake(0, 20, DeviceWidth, DeviceHeight)];
    UITapGestureRecognizer* tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(successTutialViewTapAct:)];
    [successTutorialView addGestureRecognizer:tap];
    successTutorialView.backgroundColor=[UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.7];
    [[AppDelegate ShareApp].window addSubview:successTutorialView];
    
    UILabel* greatLable=[[UILabel alloc]initWithFrame:CGRectMake(0, 180, DeviceWidth, 30)];
    greatLable.textAlignment=NSTextAlignmentCenter;
    greatLable.textColor=[UIColor colorWithRed:249.0/255.0f green:214.0/255.0f blue:65.0/255.0f alpha:1];
    greatLable.text=@"Great Taste !";
    greatLable.font=[UIFont boldSystemFontOfSize:30];
    [successTutorialView addSubview:greatLable];
    UIImageView* imagview=[[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth/2-30, greatLable.endPointY+25, 64, 69)];
    imagview.image=[UIImage imageNamed:@"thumbsUp"];
    [successTutorialView addSubview:imagview];
    
    UILabel* middleLable=[[UILabel alloc]initWithFrame:CGRectMake(0, imagview.endPointY+18, DeviceWidth, 20)];
    middleLable.text=@"Here are you search result.";
    middleLable.font=[UIFont boldSystemFontOfSize:20];
    middleLable.textColor=[UIColor colorWithRed:249.0/255.0f green:214.0/255.0f blue:65.0/255.0f alpha:1];
    middleLable.textAlignment=NSTextAlignmentCenter;
    [successTutorialView addSubview:middleLable];
    
    UILabel* bootomLable=[[UILabel alloc]initWithFrame:CGRectMake(0, middleLable.endPointY+3, DeviceWidth, 80)];
    bootomLable.text=@"Now start discovering\nfood and restaurants\nthat you'll love !";
    bootomLable.numberOfLines=3;
    bootomLable.font=[UIFont boldSystemFontOfSize:20];
    bootomLable.textColor=[UIColor whiteColor];
    bootomLable.textAlignment=NSTextAlignmentCenter;
    [successTutorialView addSubview:bootomLable];
    
    
    
    
}
-(void)successTutialViewTapAct:(UIGestureRecognizer*)tap{
    
    [successTutorialView removeFromSuperview];
    
    
}

- (void)addTutorialView
{
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isFirstSearchTutorialPage"])
    {
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isFirstSearchTutorialPage"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        tutorialStrArr = [[NSArray alloc] initWithObjects:@"pilih waktu dan jumlah pemesanan",@"lihat rating restorannya", nil];
        [self addTutorialViewWithCurrentTag:0];
    }
}
- (void)addFirstView
{
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isFirstSearch"])
    {
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isFirstSearch"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    else
        return;
    
    firstImageView = [[UIImageView alloc] init];
    self.navigationController.navigationBarHidden = YES;

    if (![UIDevice isLaterThanIphone5])
    {
        firstImageView.frame = self.view.bounds;
        [self.view addSubview:firstImageView];
        
        firstImageView.image = [UIImage imageNamed:@"iPhone4_search"];
    }
    else
    {
        firstImageView.frame = self.view.bounds;
        [self.view addSubview:firstImageView];
        firstImageView.image = [UIImage imageNamed:@"help_search"];
        
    }
//    
//    else if ([UIDevice isIphone6])
//    {
//        firstImageView.frame = self.view.bounds;
//        [self.view addSubview:firstImageView];
//        firstImageView.image = [UIImage imageNamed:@"iPhone6_search"];
//    }
//    else
//    {
//        firstImageView.frame = self.view.bounds;
//        [self.view addSubview:firstImageView];
//        firstImageView.image = [UIImage imageNamed:@"iPhone6plus_search"];
//    }
    firstImageView.userInteractionEnabled = YES;
    [self.view bringSubviewToFront:firstImageView];
    //
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewClick:)];
    [firstImageView addGestureRecognizer:tap];
    
}
- (void)viewClick:(id)sender
{
    self.navigationController.navigationBarHidden = NO;
    [firstImageView removeFromSuperview];
}

- (void)searchButtonTapped:(id)sender
{
    if (showMap) {
        mapBtnRight.selected=!mapBtnRight.selected;
        if (restaurantListView.hidden) {
            restaurantListView.hidden = NO;
            mapView.hidden = YES;
            searchBtnRight.image = [UIImage imageNamed:@"restaurants_map"];
        }else{
            restaurantListView.hidden = YES;
            mapView.hidden = NO;
            searchBtnRight.image = [UIImage imageNamed:@"search_map"];

        }
        return;
    }
    
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:@"Search restaurant result page" forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL - Search CTA" withEventProperties:eventProperties];
    
    [self gotoSearch];
}

-(void)initMapRestaurants{
    offset=0;
    lastPageCount = 0;
    
    double deltaLatitude = mapViewController.mapView.region.span.latitudeDelta;
    double deltaLongitude = mapViewController.mapView.region.span.longitudeDelta;
    
    double centerLatitude = mapViewController.mapView.region.center.latitude;
    double centerLongitude = mapViewController.mapView.region.center.longitude;
    
    minLatitudeTemp = [NSNumber numberWithDouble:[MapUtil minLatitude:centerLatitude deltaLatitude:deltaLatitude]];
    maxLatitudeTemp = [NSNumber numberWithDouble:[MapUtil maxLatitude:centerLatitude deltaLatitude:deltaLatitude]];
    minLongitudeTemp = [NSNumber numberWithDouble:[MapUtil minLongitude:centerLongitude deltaLongitude:deltaLongitude]];
    maxLongitudeTemp = [NSNumber numberWithDouble:[MapUtil maxLongitude:centerLongitude deltaLongitude:deltaLongitude]];
    [[LoadingView sharedLoadingView]startLoading];
    [SearchUtil isfromEventsearch:discover offset:offset max:50 minLatitude:minLatitudeTemp maxLatitude:maxLatitudeTemp minLongitude:minLongitudeTemp maxLongitude:maxLongitudeTemp andBlock:^(NSArray *restaurantArray,NSDictionary *suggestDic) {
        [[LoadingView sharedLoadingView]stopLoading];
        isExceedMax = YES;
        if (loadingImageView) {
            [loadingImageView stopLoading];
            loadingImageView=nil;
            searchResultsTableView.scrollEnabled = YES;
            
        }
        if (restaurantArray==nil || restaurantArray.count==0) {
            [self loadEmptyTableVIewHeadView];
        }else{
            [self createSuggestHeaderView:suggestDic];
        }
        lastPageCount = [SearchUtil searchedRestaurantArray].count;

        if(searchResultsTableView!=nil){
            [searchResultsTableView reloadData];
        }
        if(mapViewController!=nil){
            [mapViewController setSearchMapDatas:[SearchUtil searchedRestaurantArray] offset:offset];
        }
        
    }];
    
    
}

-(void)addNotifications{

    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(search:) name:SEARCH_RESTAURANT object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoDetal:) name:GO_DETAIL_FROM_SEARCH object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardShow) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardClose) name:UIKeyboardWillHideNotification object:nil];

}


-(void)gotoDetal:(NSNotification*)notification
{
    NSNumber *restaurantId = [notification.userInfo objectForKey:@"restaurantId"];
    for(IMGRestaurant *tmpRestaurant in [SearchUtil searchedRestaurantArray]){
        if([tmpRestaurant.restaurantId intValue]==[restaurantId intValue]){
            [self gotoDetailViewControllerWithRestaurant:tmpRestaurant];
            break;
        }
    }

}
-(void)search:(NSNotification*)notification
{
    searchResultsTableView.contentSize = CGSizeMake(DeviceWidth, 0);
    loadingImageView = [[SimpleLoadingImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, searchResultsTableView.frame.size.height)];

    searchResultsTableView.scrollEnabled = NO;
    [searchResultsTableView addSubview:loadingImageView];
    [loadingImageView startLoading];
    
    if (notification != nil) {
        discover = [notification.userInfo objectForKey:@"discover"];
        isStartLoad = NO;
        isHadNextData = NO;
        lastPageCount = 0;
        
        NSString *isSortby = [notification.userInfo objectForKey:@"isSortby"];
        if( (isSortby != nil) && ([@"YES" isEqualToString:isSortby]) ) {
            isMapSearch = NO;
        }
    }

    if (discover.priceLevelArray) {
        NSMutableArray *priceLevelArray = [[NSMutableArray alloc]init];
        for (int i=0; i<discover.priceLevelArray.count; i++) {
            IMGPriceLevel *priceLevel = [discover.priceLevelArray objectAtIndex:i];
            [priceLevelArray addObject:priceLevel.dollors];
        }
//       [MixpanelHelper trackSerachRestaurantPriceLevelsWithArray:priceLevelArray];

    }
    
//    if ([discover.sortby isEqualToString:SORTBY_BOOKINGS]) {
//        [MixpanelHelper trackRestaurantTabFilterWithTab:@"Most Bookings"];
//    }else if ([discover.sortby isEqualToString:SORTBY_DISTANCE]){
//        [MixpanelHelper trackRestaurantTabFilterWithTab:@"Location"];
//    }else if ([discover.sortby isEqualToString:SORTBY_OFFERS]){
//        [MixpanelHelper trackRestaurantTabFilterWithTab:@"Best Offers"];
//    }else if ([discover.sortby isEqualToString:SORTBY_POPULARITY]){
//        [MixpanelHelper trackRestaurantTabFilterWithTab:@"Most Popular"];
//    }
//    if (discover.cuisineArray) {
//        [MixpanelHelper trackSerachRestaurantCuisine:discover.cuisineArray];
//    }
    if(discover!=nil && [discover hasFilters]){
//        [filterValueLabel setText:L(@"Filters")];
        
        [filterValueLabel setText:[discover ReturnFiltersName]];
    }else{
        [filterValueLabel setText:L(@"None")];
    }
    if(discover!=nil && discover.sortby!=nil){
        if ([discover.sortby isEqualToString:@"Offers"]) {
            [sortValueLabel setText:@"Latest Promo"];
        }else{
            [sortValueLabel setText:discover.sortby];
        }
        self.amplitudeSortBy = discover.sortby;
    }else{
        [sortValueLabel setText:SORTBY_POPULARITY];
    }
    if(discover!=nil && discover.pax!=nil){
        [bookDateTimeLabel setText:[NSString stringWithFormat:L(@"Table for %@"),discover.pax]];
    }else{
        [bookDateTimeLabel setText:@""];
    }
    NSString *bookDateTimeStr;
    if(discover!=nil && discover.bookDate!=nil && discover.bookTime!=nil&&![discover.bookTime isEqualToString:@""]){
        NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd MMM"];
        bookDateTimeStr = [NSString stringWithFormat: @"%@, %@",[dateFormatter stringFromDate:discover.bookDate ],discover.bookTime];
    }
    if(bookDateTimeStr!=nil){
        UIFont *valueLabelFont=[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13];
        NSMutableAttributedString *bookDateAndTimeAttributedString = [[NSMutableAttributedString alloc]initWithString:bookDateTimeStr];
        NSRange range = [bookDateTimeStr rangeOfString:bookDateTimeStr];
        [bookDateAndTimeAttributedString addAttribute:(id)kCTForegroundColorAttributeName value:(id)[UIColor colorC2060A].CGColor range:range];
        
        CTFontRef font1 = CTFontCreateWithName((CFStringRef)valueLabelFont.fontName, 13, NULL);
        [bookDateAndTimeAttributedString addAttribute:(id)kCTFontAttributeName value:(id)CFBridgingRelease(font1) range:range];
        NSString *redTimeStr = [bookDateTimeStr substringToIndex:bookDateTimeStr.length-5];
        NSRange range1 = [bookDateTimeStr rangeOfString:redTimeStr];
        [bookDateAndTimeAttributedString addAttribute:(id)kCTForegroundColorAttributeName value:(id)[UIColor color111111].CGColor range:range1];
        bookDateTimeValueLabel.attributedText = bookDateAndTimeAttributedString;
        bookDateTimeValueLabel.verticalAlignment = TYVerticalAlignmentCenter;
    }else{
        [bookDateTimeLabel setText:@"Find a"];
        NSMutableAttributedString *bookDateAndTimeAttributedString = [[NSMutableAttributedString alloc]initWithString:@"Table"];
        NSRange range = [@"Table" rangeOfString:@"Table"];
        UIFont *valueLabelFont=[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13];
        CTFontRef font1 = CTFontCreateWithName((CFStringRef)valueLabelFont.fontName, 13, NULL);
        [bookDateAndTimeAttributedString addAttribute:(id)kCTFontAttributeName value:(id)CFBridgingRelease(font1) range:range];
        [bookDateAndTimeAttributedString addAttribute:(id)kCTForegroundColorAttributeName value:(id)[UIColor color111111].CGColor range:range];
        bookDateTimeValueLabel.attributedText = bookDateAndTimeAttributedString;
    }
    if (isMapSearch) {
        [discover resetMapParam];
    }else{
        isMapSearch = YES;
    }
    offset=0;
    beginLoadingData = [[NSDate date]timeIntervalSince1970];
    if (isHomeNearby) {
        CLLocationCoordinate2D center;
        center.latitude=[AppDelegate ShareApp].locationManager.location.coordinate.latitude;
        center.longitude = [AppDelegate ShareApp].locationManager.location.coordinate.longitude;
        MKCoordinateSpan span;
        span.latitudeDelta=DELTA_MAP;
        span.longitudeDelta=DELTA_MAP;
        
        minLatitudeTemp = [NSNumber numberWithDouble:[MapUtil minLatitude:center.latitude deltaLatitude:span.latitudeDelta]];
        maxLatitudeTemp = [NSNumber numberWithDouble:[MapUtil maxLatitude:center.latitude deltaLatitude:span.latitudeDelta]];
        minLongitudeTemp = [NSNumber numberWithDouble:[MapUtil minLongitude:center.longitude deltaLongitude:span.longitudeDelta]];
        maxLongitudeTemp = [NSNumber numberWithDouble:[MapUtil maxLongitude:center.longitude deltaLongitude:span.longitudeDelta]];
    }
    isHomeNearby = NO;
    if ([discover.sortby isEqualToString:@"Latest Promo"]) {
        discover.sortby=@"bestOffer";
    }
    [SearchUtil  isfromEventsearch:discover offset:offset minLatitude:minLatitudeTemp maxLatitude:maxLatitudeTemp minLongitude:minLongitudeTemp maxLongitude:maxLongitudeTemp andBlock:^(NSArray *restaurantArray,NSDictionary *suggestDic) {
        
//        discover.needLog = NO;
        
        if(searchResultsTableView!=nil){
            if ([SearchUtil searchedRestaurantArray]==nil || [SearchUtil searchedRestaurantArray].count == 0) {
                [self loadEmptyTableVIewHeadView];
            }else{
                [self createSuggestHeaderView:suggestDic];
            }
            
            lastPageCount = [[SearchUtil searchedRestaurantArray] count];
            
            [searchResultsTableView reloadData];
        }
        
        if(mapViewController!=nil){
            [mapViewController setMapDatas:[SearchUtil searchedRestaurantArray] offset:offset];
        }
        [loadingImageView stopLoading];
        loadingImageView=nil;
        searchResultsTableView.scrollEnabled = YES;
        
        NSLog(@"%f,getSearchData 4.0",[[NSDate date]timeIntervalSince1970]);
        finishUI = [[NSDate date]timeIntervalSince1970];
        float duringTime = finishUI - beginLoadingData;
        duringTime = duringTime * 1000;
        int duringtime = duringTime;
        if (self.isNearBy==YES) {
            [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createTimingWithCategory:@"nearby"  interval:[NSNumber numberWithInt:duringtime] name:@"" label:@""] build]];

        }else
        {
            [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createTimingWithCategory:@"SRP"  interval:[NSNumber numberWithInt:duringtime] name:@"" label:@""] build]];

        }
        if (self.amplitudeSortBy.length==0) {
            self.amplitudeSortBy = @"";
        }
        if (self.amplitudeKind.length==0) {
            self.amplitudeKind = @"";
        }
        if (self.suggestionDiscovery.length==0) {
            self.suggestionDiscovery = @"";
        }
        NSString *isdistrict=@"false";
        NSString *isCusine=@"false";
        NSString *isLandMark=@"false";
        NSString *userLocated=@"true";


        if (discover.districtArray.count>0) {
            isdistrict=@"true";
        }
        if (discover.cuisineArray.count>0){
            isCusine=@"true";
        
        }
        if (discover.landmarkArray.count>0){
        
            isLandMark=@"true";
        }
        if (discover.tagsArr.count>0) {
            for (IMGTag* tag in discover.tagsArr) {
                if ([tag.typeShows isEqualToString:@"Landmark/ Mall"]) {
                    isLandMark=@"true";

                }
            }
        
        }
        NSNumber* havelatitude=[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"];
        if (havelatitude==nil||[havelatitude intValue]==0) {
            userLocated=@"false";
        }
        int i =0;
        for (IMGRestaurant *res in restaurantArray) {
            if (res.isAd) {
                i++;
            }
        }
        
        [[Amplitude instance] logEvent:@"SC - View Search Result Restaurant" withEventProperties:@{@"Type":self.amplitudeKind,@"":self.amplitudeSortBy,@"Suggestion Discovery":self.suggestionDiscovery,@"filterOnDistrict":isdistrict,@"filterOnCuisine":isCusine,@"filterOnLandmark":isLandMark,@"userLocated":userLocated,@"numberOfAds":[NSNumber numberWithInt:i]}];

        [self searchNextPageWithDiscover:discover andOffset:offset+=SEARCH_RESULT_PAGE_SIZE
                             minLatitude:minLatitudeTemp maxLatitude:maxLatitudeTemp minLongitude:minLongitudeTemp maxLongitude:maxLongitudeTemp andBlock:^(NSArray *restaurantArray) {
                                 
                                 
                                 
                             }];
    }];
}
-(void)createSuggestHeaderView:(NSDictionary *)suggestDic{
    [suggestHeaderView removeFromSuperview];
    suggestHeaderView = nil;
    [searchResultsTableView setTableHeaderView:nil];
    
    if ((!offerBtnClicked&&!openNowBtnClicked)||(!offerBtnClicked&&!myListBtnClicked)||(!openNowBtnClicked&&!myListBtnClicked)) {
        return;
    }
    
    NSString *numFound = [NSString stringWithFormat:@"%@",[suggestDic objectForKey:@"numFound"]];
    
    if ( (suggestDic !=nil)
        && (([suggestDic objectForKey:@"searchInstead"] !=nil) || ([suggestDic objectForKey:@"collationQuery"] !=nil)) ) {
        NSString *searchInstead = [suggestDic objectForKey:@"searchInstead"];
        NSString *collationQuery = [suggestDic objectForKey:@"collationQuery"];
        if (searchInstead == nil) {
            searchInstead = [NSString stringWithFormat:@"\"%@\"",discover.keyword];
        }
        suggestHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 60)];
        suggestHeaderView.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:248.0/255.0 alpha:1];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(18, 10, DeviceWidth - 36, 20)];
        label.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:248.0/255.0 alpha:1];
        label.numberOfLines = 0;
        label.textAlignment = NSTextAlignmentLeft;
        label.font = [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:15];
        
        NSMutableArray* cuisineArray = discover.cuisineArray;
        NSString *cuisineStr = @" ";
        if ( (cuisineArray != nil) && (cuisineArray.count ==1 )) {
            cuisineStr = [NSString stringWithFormat:@" %@ ",((IMGCuisine*)[cuisineArray objectAtIndex:0]).name];
        }
        
        NSString *str = [NSString stringWithFormat:@"Showing %@%@Restaurants of %@",numFound,cuisineStr,searchInstead];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
        [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:196/255.0 green:0 blue:0 alpha:1] range:[str rangeOfString:numFound]];
        label.attributedText = attributedString;
        [label sizeToFit];
        [suggestHeaderView addSubview:label];
        
        if (collationQuery!=nil) {
            TYAttributedLabel *tyAttributedLabel = [[TYAttributedLabel alloc] initWithFrame:CGRectMake(18, label.endPointY, DeviceWidth - 36, 20)];
            tyAttributedLabel.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:248.0/255.0 alpha:1];
            tyAttributedLabel.numberOfLines = 0;
            tyAttributedLabel.delegate = self;
            [tyAttributedLabel setLineBreakMode:kCTLineBreakByWordWrapping];
            tyAttributedLabel.font=[UIFont fontWithName:DEFAULT_FONT_NAME size:13];
            tyAttributedLabel.text = [NSString stringWithFormat:@"Search instead of %@",collationQuery];
            tyAttributedLabel.verticalAlignment = TYVerticalAlignmentCenter;
            [suggestHeaderView addSubview:tyAttributedLabel];
            
            //
            TYLinkTextStorage *linkTextStorage = [[TYLinkTextStorage alloc]init];
            if ([[collationQuery substringToIndex:1] isEqualToString:@"\""]
                &&[[collationQuery substringFromIndex:(collationQuery.length-1)] isEqualToString:@"\""]) {
                linkTextStorage.range = NSMakeRange(19, collationQuery.length-2);
            }else{
                linkTextStorage.range = NSMakeRange(18, collationQuery.length);
            }
            
            linkTextStorage.textColor=[UIColor colorWithRed:71/255.0 green:124/255.0 blue:230/255.0 alpha:1];
            linkTextStorage.linkData = @"collationQuery";
            linkTextStorage.underLineStyle = kCTUnderlineStyleNone;
            [tyAttributedLabel addTextStorage:linkTextStorage];
            if ( (tyAttributedLabel.endPointY+10) > suggestHeaderView.frame.size.height) {
                suggestHeaderView.frame = CGRectMake(0, 0, suggestHeaderView.frame.size.width,tyAttributedLabel.endPointY+10);
            }
        }else{
            if ( (label.endPointY+10) > suggestHeaderView.frame.size.height) {
                suggestHeaderView.frame = CGRectMake(0, 0, suggestHeaderView.frame.size.width,label.endPointY+10);
            }
            label.center = CGPointMake(label.center.x, suggestHeaderView.frame.size.height/2.0);
        }
        [searchResultsTableView setTableHeaderView:suggestHeaderView];
    }else{
        
        UIView *suggestHeaderViewLocal = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
        suggestHeaderViewLocal.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:248.0/255.0 alpha:1];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(18, 0, DeviceWidth - 36, 20)];
        label.backgroundColor = [UIColor colorWithRed:242.0/255.0 green:242.0/255.0 blue:248.0/255.0 alpha:1];
        label.numberOfLines = 0;
        label.textAlignment = NSTextAlignmentLeft;
        label.font = [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:15];
        [suggestHeaderViewLocal addSubview:label];
        BOOL isTableHeaderViewNil = YES;
        switch (discover.gotoWhere) {
           
            case IsNearby:
            {
                isTableHeaderViewNil = NO;
                NSString *message = [NSString stringWithFormat:@"%@ Nearby Restaurants",numFound];
                [self handleLabel:label showMessage:message numFound:numFound suggestHeaderView:suggestHeaderViewLocal];
            }
                break;

            case IsPopular:
            {
                isTableHeaderViewNil = NO;
                NSString *message = [NSString stringWithFormat:@"%@ Most Popular Restaurants",numFound];
                [self handleLabel:label showMessage:message numFound:numFound suggestHeaderView:suggestHeaderViewLocal];
            }
                break;
            case Ispromo:{
                isTableHeaderViewNil = NO;
                NSString *message = [NSString stringWithFormat:@"%@ Restaurants with Promos",numFound];
                [self handleLabel:label showMessage:message numFound:numFound suggestHeaderView:suggestHeaderViewLocal];
                        }
                break;
            case IsLunch:{
                isTableHeaderViewNil = NO;
                NSString *message = [NSString stringWithFormat:@"%@ Restaurants for Lunch",numFound];
                [self handleLabel:label showMessage:message numFound:numFound suggestHeaderView:suggestHeaderViewLocal];
            }
                break;
            case IsDinner:{
                isTableHeaderViewNil = NO;
                NSString *message = [NSString stringWithFormat:@"%@ Restaurants for Dinner",numFound];
                [self handleLabel:label showMessage:message numFound:numFound suggestHeaderView:suggestHeaderViewLocal];
            }
                break;
            case IsBooking:{
                isTableHeaderViewNil = NO;
                NSString *message = [NSString stringWithFormat:@"%@ Bookable Restaurants",numFound];
                [self handleLabel:label showMessage:message numFound:numFound suggestHeaderView:suggestHeaderViewLocal];
            }
                break;
            case IsDelivery:{
                isTableHeaderViewNil = NO;
                NSString *message = [NSString stringWithFormat:@"%@ Restaurants with Delivery",numFound];
                [self handleLabel:label showMessage:message numFound:numFound suggestHeaderView:suggestHeaderViewLocal];
            }
                break;

                
            default:
                break;
        }
        if ( (discover.keyword !=nil) && ![@"" isEqualToString:discover.keyword]&&![discover.keyword isBlankString]) {
            NSString *message = [NSString stringWithFormat:@"Showing %@ Restaurants of \"%@\"",numFound,discover.keyword];
            [self handleLabel:label showMessage:message numFound:numFound suggestHeaderView:suggestHeaderViewLocal];
        }else if( (discover.cuisineArray!=nil) && (discover.cuisineArray.count != 0) ){
            NSString *cuisineStr = [NSString stringWithFormat:@"%@",((IMGCuisine*)[discover.cuisineArray objectAtIndex:0]).name];
            NSString *cityStr = [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT];
            NSString *message = [NSString stringWithFormat:@"%@ %@ Restaurants in %@",numFound,cuisineStr,[cityStr capitalizedString]];
            [self handleLabel:label showMessage:message numFound:numFound suggestHeaderView:suggestHeaderViewLocal];
        }else if((discover.districtArray!=nil) && (discover.districtArray.count != 0)){
            NSString *districtAndLandmarkStr = [NSString stringWithFormat:@"%@",((IMGDistrictAndLandmark*)[discover.districtArray objectAtIndex:0]).name];
            NSString *message = [NSString stringWithFormat:@"%@ Restaurants in %@",numFound,districtAndLandmarkStr];
            [self handleLabel:label showMessage:message numFound:numFound suggestHeaderView:suggestHeaderViewLocal];
        }else if((discover.foodArray!=nil) && (discover.foodArray.count != 0)){
            NSString *foodStr = [NSString stringWithFormat:@"%@",((IMGCuisineAndFoodType*)[discover.foodArray objectAtIndex:0]).name];
            NSString *message = [NSString stringWithFormat:@"%@ %@ Restaurants",numFound,foodStr];
            [self handleLabel:label showMessage:message numFound:numFound suggestHeaderView:suggestHeaderViewLocal];
        }else{
            if (discover.landmarkArray!=nil&&(discover.landmarkArray.count!=0)) {
                NSString *landmarkStr = [NSString stringWithFormat:@"%@",((IMGTag*)[discover.landmarkArray objectAtIndex:0]).name];
                NSString *message = [NSString stringWithFormat:@"%@ Restaurants in %@",numFound,landmarkStr];
                [self handleLabel:label showMessage:message numFound:numFound suggestHeaderView:suggestHeaderViewLocal];
                isTableHeaderViewNil = NO;
            }
            for (IMGTag *tag in discover.tagsArr) {
                if ([@"Landmark/ Mall" isEqualToString:tag.type]) {
                    NSString *landmarkStr = [NSString stringWithFormat:@"%@",tag.name];
                    NSString *message = [NSString stringWithFormat:@"%@ Restaurants in %@",numFound,landmarkStr];
                    [self handleLabel:label showMessage:message numFound:numFound suggestHeaderView:suggestHeaderViewLocal];
                    isTableHeaderViewNil = NO;
                }
            }
            if (isTableHeaderViewNil) {
                [searchResultsTableView setTableHeaderView:nil];
            }
        }
        
        if (!offerBtnClicked) {
            isTableHeaderViewNil = NO;
            NSString *message = [NSString stringWithFormat:@"%@ Restaurants with Promos",numFound];
            [self handleLabel:label showMessage:message numFound:numFound suggestHeaderView:suggestHeaderViewLocal];
        }

        
        
    }
}
-(void)handleLabel:(UILabel *)label showMessage:(NSString*)message numFound:(NSString*)numFound suggestHeaderView:(UIView *)suggestHeaderViewLocal{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:message];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:196/255.0 green:0 blue:0 alpha:1] range:[message rangeOfString:numFound]];
    label.attributedText = attributedString;
    
    [label sizeToFit];
    
    if (label.frame.size.height > suggestHeaderViewLocal.frame.size.height) {
        suggestHeaderViewLocal.frame = CGRectMake(0, 0, suggestHeaderViewLocal.frame.size.width, label.frame.size.height);
    }
    label.center = CGPointMake(label.center.x, suggestHeaderViewLocal.frame.size.height/2.0);
    
    suggestHeaderView = suggestHeaderViewLocal;
    
    label.frame=CGRectMake(18, suggestHeaderViewLocal.frame.size.height/2.0-8, DeviceWidth - 36, 20);
    
    
    [searchResultsTableView setTableHeaderView:suggestHeaderView];
}
- (void)attributedLabel:(TYAttributedLabel *)attributedLabel textStorageClicked:(id<TYTextStorageProtocol>)TextRun atPoint:(CGPoint)point{
    if ([TextRun isKindOfClass:[TYLinkTextStorage class]]) {
        NSString *linkStr = ((TYLinkTextStorage*)TextRun).linkData;
        if ([linkStr hasPrefix:@"collationQuery"]) {
            discover.keyword = ((TYLinkTextStorage*)TextRun).text;
            if(discover!=nil && discover.keyword!=nil){
                [searchValueLabel setText:discover.keyword];
            }
            [self search:nil];
        }
    }
}
- (void)searchNextPageWithDiscover:(IMGDiscover *)discover_ andOffset:(NSInteger)offsetT minLatitude:(NSNumber *)minLatitude maxLatitude:(NSNumber *)maxLatitude minLongitude:(NSNumber *)minLongitude maxLongitude:(NSNumber *)maxLongitude andBlock:(void (^)(NSArray * restaurantArray))block
{
    if (isExceedMax)
    {
        offsetT = offsetT + 40;
        offset = offsetT;
        isExceedMax = NO;
    }
    [SearchUtil isfromEventsearch:discover offset:offsetT minLatitude:minLatitudeTemp maxLatitude:maxLatitudeTemp minLongitude:minLongitudeTemp maxLongitude:maxLongitudeTemp andBlock:^(NSArray *restaurantArray,NSDictionary *suggestDic) {
        
        isHadNextData = YES;
        
        if (isStartLoad)
        {
            isStartLoad = NO;
            
            if(searchResultsTableView!=nil){
                if ([SearchUtil searchedRestaurantArray]==nil || [SearchUtil searchedRestaurantArray].count == 0) {
                    [self loadEmptyTableVIewHeadView];
                }else{
                    [self createSuggestHeaderView:suggestDic];
                }
                lastPageCount = [SearchUtil searchedRestaurantArray].count;

                [searchResultsTableView reloadData];
            }
            
            if(mapViewController!=nil){
                [mapViewController setMapDatas:[SearchUtil searchedRestaurantArray] offset:offset];
            }
            [loadingImageView stopLoading];
            loadingImageView=nil;
            searchResultsTableView.scrollEnabled = YES;

        }
        
    }];
}

- (void)loadSearchBar {

    restaurantSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, SEARCHBAR_WIDTH, SEARCHBAR_HEIGHT)];
    restaurantSearchBar.hidden = YES;
    restaurantSearchBar.delegate = self;
    restaurantSearchBar.showsCancelButton = NO;
    self.navigationItem.titleView = restaurantSearchBar;
    if ([discover.sortby  isEqual: SORTBY_BOOKINGS]) {
        restaurantSearchBar.placeholder = L(@"Most Booked");
    }
    else if ([discover.fromWhere intValue]==3){
        restaurantSearchBar.placeholder = L(@"Discover");
    }else{
        restaurantSearchBar.placeholder = L(@"Search");
    }
    if ([UIDevice isLaterThanIos6]) {
        restaurantSearchBar.searchBarStyle = UISearchBarStyleMinimal;
    }
    for(UIView *subview in restaurantSearchBar.subviews){
        if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
            [subview removeFromSuperview];
        }
    }
    if (discover.keyword) {
        restaurantSearchBar.text = discover.keyword;
    }
}

- (void)loadEventView {
    eventImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 160*[AppDelegate ShareApp].autoSizeScaleY)];
//    UIImage *eventImage = [UIImage imageNamed:event.banner];
//    [eventImageView setImage:eventImage];
//    UIImage *placeHolderImage = [[UIImage imageNamed:DEFAULT_IMAGE_STRING ] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth, 160)];
    [eventImageView setImageWithURL:[NSURL URLWithString:[event.appBanner returnFullImageUrlWithWidth:DeviceWidth]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2]];
        
    [self.view addSubview:eventImageView];
}
-(void)initDiscovers{
    [DiscoverUtil refreshDiscoverAndHandle:^(NSArray *discoverArray) {
        discoverArr = [NSArray arrayWithArray:discoverArray];
        [optionTableView reloadData];
        
    }];
}
- (void)loadMapView {
    
    mapViewController=[[MapViewController alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    mapViewController.fromSearchPage=YES;
    mapViewController.restaurantsViewController=self;
    mapView = mapViewController.mapView;
    
    mapView.frame = CGRectMake(0,CONDITION_BUTTON_HEIGHT, DeviceWidth, self.view.frame.size.height-CONDITION_BUTTON_HEIGHT-([[self rdv_tabBarController] isTabBarHidden]?0:45));
    
    [self.view addSubview:mapViewController.mapView];
    
    coverControl = [[UIControl alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, restaurantListViewTopY)];
    [coverControl addTarget:self action:@selector(coverControlTapped) forControlEvents:UIControlEventTouchUpInside];
    coverControl.backgroundColor = [UIColor colorWithHue:0.0 saturation:0.0 brightness:0.0 alpha:0.0];
    UIPanGestureRecognizer *coverControlPanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panHandle:)];
    [coverControl addGestureRecognizer:coverControlPanGestureRecognizer];
    [self.view addSubview:coverControl];

    mapSearchLineControl = [[UIButton alloc] initWithFrame:CGRectMake((DeviceWidth - 175)/2,6+CONDITION_BUTTON_HEIGHT, 175, 35)];
    [mapSearchLineControl addTarget:self action:@selector(mapSearchLineTapped) forControlEvents:UIControlEventTouchUpInside];
    UIPanGestureRecognizer * mapSearchLinePanGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panHandle:)];
    [mapSearchLineControl addGestureRecognizer:mapSearchLinePanGestureRecognizer];
    mapSearchLineControl.layer.cornerRadius = 3;
    mapSearchLineControl.backgroundColor = [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1];
    mapSearchLineControl.layer.borderColor = [UIColor lightGrayColor].CGColor;
    mapSearchLineControl.layer.cornerRadius = 5;
    mapSearchLineControl.layer.borderWidth = 0.5;
    [self.view addSubview:mapSearchLineControl];
    
    mapSearchLineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, -2, mapSearchLineControl.frame.size.width, mapSearchLineControl.frame.size.height)];
    [mapSearchLineLabel setBackgroundColor:[UIColor clearColor]];
    [mapSearchLineLabel setFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12]];
    [mapSearchLineLabel setTextColor:[UIColor color333333]];
    [mapSearchLineLabel setText:L(@"Search this area")];
    [mapSearchLineLabel setTextAlignment:NSTextAlignmentCenter];
    [mapSearchLineControl addSubview:mapSearchLineLabel];
    
    navigateButton = [[UIButton alloc] initWithFrame:CGRectMake(9, mapSearchLineControl.frame.origin.y, 35, 35)];
//    [navigateButton setBackgroundColor:[UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1]];
    navigateButton.backgroundColor = [UIColor clearColor];
    [navigateButton addTarget:self action:@selector(nearYouBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navigateButton setImage:[UIImage imageNamed:@"ic_located"] forState:UIControlStateNormal];
//    navigateButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    navigateButton.layer.cornerRadius = 17.5;
//    navigateButton.layer.borderWidth = 0.5;
    [self.view addSubview:navigateButton];
    
    if (!showMap) {
        mapViewController.mapView.showsUserLocation = YES;
        navigateButton.hidden = YES;
        mapSearchLineControl.hidden = YES;
    }
    
    if (discover!=nil){
        if([discover.fromWhere intValue ]==1){
            [self gotoCurrentLocation];
        }else if([discover.fromWhere intValue ]==2){
            if (discover.districtArray&&discover.districtArray.count>0) {
                mapViewController.mapView.region=[SearchUtil regionOfDistrict:[discover.districtArray objectAtIndex:0]];
            }else{
                mapViewController.mapView.region=[SearchUtil regionOfCity];
            }
        }else if([discover.fromWhere intValue ]==2){
            if (discover.districtArray&&discover.districtArray.count>0) {
                mapViewController.mapView.region=[SearchUtil regionOfDistrict:[discover.districtArray objectAtIndex:0]];
            }else{
                mapViewController.mapView.region=[SearchUtil regionOfCity];
            }
        }else{
            mapViewController.mapView.region=[SearchUtil regionOfCity];
        }
    }
    
}
- (void)nearYouBtnClick
{
    CLLocationCoordinate2D center;
    
    center.latitude=[AppDelegate ShareApp].locationManager.location.coordinate.latitude;
    center.longitude = [AppDelegate ShareApp].locationManager.location.coordinate.longitude;
    
    MKCoordinateSpan span;
    span.latitudeDelta=0.02;
    span.longitudeDelta=0.02;
    MKCoordinateRegion region={center,span};
    [mapViewController.mapView setRegion:region animated:YES];
    //    mapViewController.mapView.centerCoordinate = center;
    
    offset = 0;
    minLatitudeTemp = [NSNumber numberWithDouble:[MapUtil minLatitude:center.latitude deltaLatitude:span.latitudeDelta]];
    maxLatitudeTemp = [NSNumber numberWithDouble:[MapUtil maxLatitude:center.latitude deltaLatitude:span.latitudeDelta]];
    minLongitudeTemp = [NSNumber numberWithDouble:[MapUtil minLongitude:center.longitude deltaLongitude:span.longitudeDelta]];
    maxLongitudeTemp = [NSNumber numberWithDouble:[MapUtil maxLongitude:center.longitude deltaLongitude:span.longitudeDelta]];

//    [[LoadingView sharedLoadingView]startLoading];
    [discover resetMapParam];
//    [SearchUtil isfromEvent:_isFromEvent search:discover offset:offset max:50 minLatitude:minLatitudeTemp maxLatitude:maxLatitudeTemp minLongitude:minLongitudeTemp maxLongitude:maxLongitudeTemp andBlock:^(NSArray *restaurantArray,NSDictionary *suggestDic) {
//        [[LoadingView sharedLoadingView]stopLoading];
//        isExceedMax = YES;
//        if (loadingImageView) {
//            [loadingImageView stopLoading];
//            loadingImageView=nil;
//            searchResultsTableView.scrollEnabled = YES;
//        }
//        if (restaurantArray==nil || restaurantArray.count==0) {
//            [self loadEmptyTableVIewHeadView];
//        }else{
//            [self createSuggestHeaderView:suggestDic];
//        }
//        lastPageCount = [SearchUtil searchedRestaurantArray].count;
//        
//        if(searchResultsTableView!=nil){
//            [searchResultsTableView reloadData];
//        }
//        if(mapViewController!=nil){
//            [mapViewController setMapDatas:[SearchUtil searchedRestaurantArray] offset:offset];
//        }
//        
//    }];

}
-(void)gotoCurrentLocation
{
    CLLocationCoordinate2D center;
    
    center.latitude=[AppDelegate ShareApp].locationManager.location.coordinate.latitude;
    center.longitude = [AppDelegate ShareApp].locationManager.location.coordinate.longitude;
    
    MKCoordinateSpan span;
    span.latitudeDelta=DELTA_MAP;
    span.longitudeDelta=DELTA_MAP;
    MKCoordinateRegion region={center,span};
    [mapViewController.mapView setRegion:region animated:YES];
//    mapViewController.mapView.centerCoordinate = center;

    
}
- (void)mapSearchLineTapped
{
    [restaurantSearchBar resignFirstResponder];
    
    [self initMapRestaurants];
}

- (void)coverControlTapped
{
    [restaurantSearchBar resignFirstResponder];
    float restaurantListViewY;
    float coverControlY;
    float mapViewHeight;
    if(restaurantListView.frame.origin.y!=restaurantListViewTopY){
//        [MixpanelHelper trackViewMethodforSearchListWithTab:@"List View"];
        restaurantListViewY = restaurantListViewTopY;
        coverControlY = 0.0;
        mapViewHeight = SMALL_MAP_HEIGHT;
        navigateButton.hidden = YES;
        mapSearchLineControl.hidden = YES;
    }else{
//        [MixpanelHelper trackViewMethodforSearchListWithTab:@"Map View"];
        
        restaurantListViewY = restaurantListViewBottomY;
        coverControlY = restaurantListViewBottomY;
        mapViewHeight = DeviceHeight;
        navigateButton.hidden = NO;
        mapSearchLineControl.hidden = NO;
    }
    [UIView animateWithDuration:0.5f animations:^{
         [self updateViewsFrame:restaurantListViewY coverControlY:coverControlY mapViewHeight:mapViewHeight];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)panHandle:(UIPanGestureRecognizer *)gesture
{
    [restaurantSearchBar resignFirstResponder];
    static float startPoint_Y;
    float endPoint_Y;
    static float viewPoint_Y;
    
    switch (gesture.state)
    {
        case UIGestureRecognizerStateBegan:
        {
            startPoint_Y = [gesture locationInView:self.view].y;
            viewPoint_Y  = restaurantListView.frame.origin.y;
        }
            break;
        case UIGestureRecognizerStateChanged:
        {
            endPoint_Y   = [gesture locationInView:self.view].y;
            float gPoint = viewPoint_Y + (endPoint_Y - startPoint_Y);
            if(gPoint>=restaurantListViewTopY && gPoint<=restaurantListViewBottomY){
                [restaurantListView setFrame:CGRectMake(0, gPoint, restaurantListView.frame.size.width, restaurantListView.frame.size.height)];
                
            }
            [gesture setTranslation:CGPointZero inView:self.view];
        }
            break;
        case UIGestureRecognizerStateEnded:
        {
            [self positioned];
        }
            break;
        case UIGestureRecognizerStateCancelled:
        {
            [self positioned];
        }
            break;
        default:
            break;
    }
    
}

- (void)positioned{
    float restaurantListViewY = restaurantListView.frame.origin.y;
    float coverControlY = 0;
    float mapViewHeight = SMALL_MAP_HEIGHT;
    float halfDeviceHeight = DeviceHeight;
    halfDeviceHeight = halfDeviceHeight/2;
    
    if (restaurantListViewY >= 0)
    {
        if (restaurantListViewY >= halfDeviceHeight)
        {
            restaurantListViewY = restaurantListViewBottomY;
            coverControlY = restaurantListViewBottomY;
            mapViewHeight = DeviceHeight;
        }
        else
        {
            restaurantListViewY = restaurantListViewTopY;
        }
    }
    else
    {
        restaurantListViewY = restaurantListViewTopY;
    }
    
    //简单的过度动画
    [UIView animateWithDuration:.3f animations:^{
        [self updateViewsFrame:restaurantListViewY coverControlY:coverControlY mapViewHeight:mapViewHeight];
    } completion:^(BOOL finished) {
        
    }];
}

- (void)updateViewsFrame:(float)restaurantListViewY coverControlY:(float)coverControlY mapViewHeight:(float)mapViewHeight {
    if (restaurantListViewY == restaurantListViewTopY) {
        navigateButton.hidden = YES;
        mapSearchLineControl.hidden = YES;
    }
    if (restaurantListViewY == restaurantListViewBottomY) {
        navigateButton.hidden = NO;
        mapSearchLineControl.hidden = NO;
    }
    [restaurantListView setFrame:CGRectMake(0, restaurantListViewY, restaurantListView.frame.size.width, restaurantListView.frame.size.height)];
    [coverControl setFrame:CGRectMake(0, coverControlY, coverControl.frame.size.width, coverControl.frame.size.height)];
    [self.view bringSubviewToFront:coverControl];
}

- (void)loadRestaurantListView{

    restaurantListView = [[UIView alloc] initWithFrame:CGRectMake(0, restaurantListViewTopY, DeviceWidth, DeviceHeight - restaurantListViewTopY - 45)];

    [restaurantListView setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.8]];
    [self.view addSubview:restaurantListView];
    if (showMap) {
        [self loadMapBar];
    }else{
        [self loadConditionBar];
    }

    [self loadSearchResultTableView];
    [self addPullToRefreshHeader];
}
-(void)loadMapBar{
    UIScrollView *btnsView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, CONDITION_BUTTON_HEIGHT)];
//    [btnsView.layer setBorderWidth:0.5];
    btnsView.showsHorizontalScrollIndicator =NO;
    btnsView.showsVerticalScrollIndicator = NO;
//    [btnsView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    btnsView.backgroundColor = [UIColor whiteColor];
    int buttonWidth = (DeviceWidth - 20 - 40)/3;
    UIButton *offerBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, (CONDITION_BUTTON_HEIGHT - 25)/2, buttonWidth, 30)];
    if ([UIDevice isIphone4Now]||[UIDevice isIphone5]) {
        offerBtn.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:13];

    }else{
        offerBtn.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:16];

    }
    [offerBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [offerBtn setTitle:@" Has Promo" forState:UIControlStateNormal];
    [offerBtn.layer setBorderWidth:1];
    [offerBtn.layer setBorderColor:[UIColor whiteColor].CGColor];
    [offerBtn.layer setCornerRadius:5.0];
    if (self.isPromo) {
        offerBtnClicked = NO;
        offerBtn.backgroundColor = [UIColor colorC2060A];
        [offerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [offerBtn setImage:[[UIImage imageNamed:@"search_offer_selected.png"] imageByScalingAndCroppingForSize:CGSizeMake(10, 10)] forState:UIControlStateNormal];

    }else{
        [offerBtn setImage:[[UIImage imageNamed:@"search_hasPromo.png"] imageByScalingAndCroppingForSize:CGSizeMake(10, 10)] forState:UIControlStateNormal];
    }

    [offerBtn addTarget:self action:@selector(offerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [btnsView addSubview:offerBtn];
    
    UIButton *openNowBtn = [[UIButton alloc] initWithFrame:CGRectMake(offerBtn.endPointX+5, (CONDITION_BUTTON_HEIGHT - 25)/2, buttonWidth, 30)];
    if ([UIDevice isIphone4Now]||[UIDevice isIphone5]) {
        openNowBtn.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:13];

    }else{
        openNowBtn.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:16];

    }
    [openNowBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [openNowBtn setTitle:@" Open Now" forState:UIControlStateNormal];
    [openNowBtn.layer setBorderWidth:1];
    [openNowBtn.layer setBorderColor:[UIColor whiteColor].CGColor];
    [openNowBtn.layer setCornerRadius:5.0];
    [openNowBtn setImage:[[UIImage imageNamed:@"search_openNow.png"] imageByScalingAndCroppingForSize:CGSizeMake(10, 10)] forState:UIControlStateNormal];

    [openNowBtn addTarget:self action:@selector(openNowBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [btnsView addSubview:openNowBtn];
    
    UIButton *myListBtn = [[UIButton alloc] initWithFrame:CGRectMake(openNowBtn.endPointX + 5, (CONDITION_BUTTON_HEIGHT - 25)/2, buttonWidth, 30)];
    if ([UIDevice isIphone4Now]||[UIDevice isIphone5]){
        myListBtn.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:13];
    }else{
        myListBtn.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:16];

    } 
    [myListBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [myListBtn setTitle:@" In My List" forState:UIControlStateNormal];
    [myListBtn.layer setBorderWidth:1];
    [myListBtn.layer setBorderColor:[UIColor whiteColor].CGColor];
    [myListBtn.layer setCornerRadius:5.0];
    [myListBtn setImage:[[UIImage imageNamed:@"search_myList.png"] imageByScalingAndCroppingForSize:CGSizeMake(10, 10)] forState:UIControlStateNormal];

    [myListBtn addTarget:self action:@selector(myListBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [btnsView addSubview:myListBtn];
    
    UIImage *filterImage = [UIImage imageNamed:@"journalCategory"];
    UIImageView *filterBtn = [[UIImageView alloc] initWithImage:filterImage];
    if ([UIDevice isIphone6Plus]) {
        filterBtn.frame = CGRectMake(DeviceWidth-filterImage.size.height-22, btnsView.startPointY+15, filterImage.size.width, filterImage.size.height);
        filterBtn.userInteractionEnabled = YES;
        btnsView.frame = CGRectMake(btnsView.startPointX, btnsView.startPointY, DeviceWidth-filterImage.size.width-22, CONDITION_BUTTON_HEIGHT);
        btnsView.contentSize = CGSizeMake(DeviceWidth-filterImage.size.width, 0);

    }else if ([UIDevice isIphone4Now]||[UIDevice isIphone5]){
        filterBtn.frame = CGRectMake(DeviceWidth-filterImage.size.height-LEFT_X, btnsView.startPointY+15, filterImage.size.width, filterImage.size.height);
        filterBtn.userInteractionEnabled = YES;
        btnsView.frame = CGRectMake(btnsView.startPointX, btnsView.startPointY, DeviceWidth-filterImage.size.width-24, CONDITION_BUTTON_HEIGHT);
        btnsView.contentSize = CGSizeMake(DeviceWidth-filterImage.size.width-2, 0);

    }else{
        filterBtn.frame = CGRectMake(DeviceWidth-filterImage.size.height-LEFT_X, btnsView.startPointY+15, filterImage.size.width, filterImage.size.height);
        filterBtn.userInteractionEnabled = YES;
        btnsView.frame = CGRectMake(btnsView.startPointX, btnsView.startPointY, DeviceWidth-filterImage.size.width-24, CONDITION_BUTTON_HEIGHT);
        btnsView.contentSize = CGSizeMake(DeviceWidth-filterImage.size.width-2, 0);
    }
    UIControl *control = [[UIControl alloc] initWithFrame:CGRectMake(filterBtn.startPointX-5, filterBtn.startPointY-5, filterImage.size.width+30, filterImage.size.height+30)];
    [control addTarget:self action:@selector(filterBtnClick) forControlEvents:UIControlEventTouchUpInside];
    UIView * backView=[[UIView alloc]initWithFrame:CGRectMake(btnsView.endPointX,0, 50, CONDITION_BUTTON_HEIGHT)];
    backView.backgroundColor=[UIColor whiteColor];

    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, btnsView.endPointY, DeviceWidth, 1)];
    lineView.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:lineView];
    [self.view addSubview:backView];
    [self.view addSubview:filterBtn];
    [self.view addSubview:control];
    [self.view addSubview:btnsView];
}
-(void)offerBtnClick:(UIButton*)sender{
    if (offerBtnClicked) {
        sender.backgroundColor = [UIColor colorC2060A];
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [sender setImage:[[UIImage imageNamed:@"search_offer_selected.png"] imageByScalingAndCroppingForSize:CGSizeMake(10, 10)] forState:UIControlStateNormal];
        offerBtnClicked=NO;
        NSMutableArray *arr=[[NSMutableArray alloc]init];
        [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"select * from IMGOfferType order by typeId asc,off desc ;"] successBlock:^(FMResultSet *resultSet) {
            while([resultSet next]) {
                IMGOfferType *tag = [[IMGOfferType alloc] init];
                [tag setValueWithResultSet:resultSet];
                [arr addObject:tag];
            }
            [resultSet close];
        }failureBlock:^(NSError *error) {
            NSLog(@"FiltersViewController loadButtonGridView error when get datas:%@",error.description);
        }];
        NSMutableArray *offArr=[[NSMutableArray alloc]init];
        NSMutableArray *offerTpyeArr=[[NSMutableArray alloc]init];
        
        if (arr!=nil&&arr.count>0) {
            for (IMGOfferType *tag in arr) {
                
                if ([tag.typeId intValue]==0) {
                    [offArr addObject:tag];
                }else{
                    
                    [offerTpyeArr addObject:tag];
                }
            }
            
        }
        
        discover.offArray=offArr;
        discover.offerTypeArray=offerTpyeArr;
        discover.gotoWhere=Ispromo;
        [self search:nil];
    }else{
        sender.backgroundColor = [UIColor whiteColor];
        [sender setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [sender setImage:[[UIImage imageNamed:@"search_hasPromo.png"] imageByScalingAndCroppingForSize:CGSizeMake(10, 10)]forState:UIControlStateNormal];
        discover.gotoWhere=Isnull;
        discover.offArray=nil;
        discover.offerTypeArray=nil;
        [self search:nil];

        offerBtnClicked = YES;
    }

}
-(void)openNowBtnClick:(UIButton*)sender{
    if (openNowBtnClicked) {
        sender.backgroundColor = [UIColor colorC2060A];
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [sender setImage:[[UIImage imageNamed:@"search_openNow_selected.png"] imageByScalingAndCroppingForSize:CGSizeMake(10, 10)] forState:UIControlStateNormal];
        openNowBtnClicked = NO;
        discover.openNowToggle = @(1);
        [self search:nil];
    }else{
        sender.backgroundColor = [UIColor whiteColor];
        [sender setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [sender setImage:[[UIImage imageNamed:@"search_openNow.png"] imageByScalingAndCroppingForSize:CGSizeMake(10, 10)] forState:UIControlStateNormal];
        openNowBtnClicked = YES;
        discover.openNowToggle = @(0);
        [self search:nil];

    }
   
}
-(void)myListBtnClick:(UIButton*)sender{
    IMGUser *user = [IMGUser currentUser];
    if (![user.userId intValue]) {
        LoginViewController *lvc = [[LoginViewController alloc] initWithFromWhere:[NSNumber numberWithInt:1]];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:lvc];
        [self.navigationController presentViewController:nav animated:NO completion:^{
        }];
        return;
    }
    if (myListBtnClicked) {
        sender.backgroundColor = [UIColor colorC2060A];
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [sender setImage:[[UIImage imageNamed:@"search_myList_selected.png"] imageByScalingAndCroppingForSize:CGSizeMake(10, 10)]forState:UIControlStateNormal];
        myListBtnClicked = NO;
        discover.myListToggle =@(1);
        [self search:nil];
    }else{
        sender.backgroundColor = [UIColor whiteColor];
        [sender setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [sender setImage:[[UIImage imageNamed:@"search_myList.png"] imageByScalingAndCroppingForSize:CGSizeMake(10, 10)] forState:UIControlStateNormal];
        myListBtnClicked = YES;
        discover.myListToggle =@(0);
        [self search:nil];

    }

}
-(void)filterBtnClick{
    [self goFilter:nil];
}
- (void)loadConditionBar {
    float leftMargin = 16;
    UIFont *labelFont=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:10];
    UIFont *valueLabelFont=[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13];
   
    float i=0;
    if ([UIDevice isIphone6]||[UIDevice isIphone6Plus]) {
        i=0.06;
    }
    
    UIScrollView *btnsScrollView = [[UIScrollView alloc] init];
    btnsScrollView.scrollsToTop = NO;
    [restaurantListView addSubview:btnsScrollView];
    btnsScrollView.frame = CGRectMake(0, 0, DeviceWidth, CONDITION_BUTTON_HEIGHT);
    btnsScrollView.backgroundColor = [UIColor clearColor];
    btnsScrollView.bounces = NO;
    btnsScrollView.showsHorizontalScrollIndicator = NO;
    btnsScrollView.showsVerticalScrollIndicator = NO;
    
    
    
    float a=1;
    float b=0;
    if ([UIDevice isiPadHiRes]) {
        a=2;
        b=80;
    }
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    searchBtn.frame = CGRectMake(0, 0, (SEARCH_BUTTON_WIDTH*(i+1))*a , CONDITION_BUTTON_HEIGHT);
    searchBtn.backgroundColor = [UIColor clearColor];
    sideButtonImage = [UIImage imageNamed:@"conditionButton2"];
    [searchBtn setBackgroundImage:sideButtonImage forState:UIControlStateNormal];
    [searchBtn addTarget:self action:@selector(gotoSearch) forControlEvents:UIControlEventTouchUpInside];

    UILabel *searchLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, 3, 30, 17)];
    [searchLabel setText:L(@"Search")];
    [searchLabel setFont:labelFont];
    [searchLabel setTextColor:[UIColor color111111]];
    searchLabel.frame = CGRectMake(leftMargin, 3, searchLabel.expectedWidth, searchLabel.font.lineHeight);
    [searchLabel setBackgroundColor:[UIColor clearColor]];
    [searchBtn addSubview:searchLabel];
    
    searchValueLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, 19, 70, 20)];
    if(discover!=nil && discover.keyword!=nil){
        [searchValueLabel setText:discover.keyword];
    }
    [searchValueLabel setFont:valueLabelFont];
    [searchValueLabel  setTextColor:[UIColor color111111]];
    [searchValueLabel setBackgroundColor:[UIColor clearColor]];
    [searchBtn addSubview:searchValueLabel];
    
    arrowImage = [UIImage imageNamed:@"arrowDown1"];
    UIImageView *searchArrow1ImageView = [[UIImageView alloc] initWithImage:arrowImage];
    searchArrow1ImageView.frame = CGRectMake(84+b, 20, 10, 5);
    [searchBtn addSubview:searchArrow1ImageView];
    [searchBtn setAlpha:0.8];
    
    [btnsScrollView addSubview:searchBtn];
    
    arrowImage = [UIImage imageNamed:@"arrowDown1"];
    arrow1ImageView = [[UIImageView alloc] initWithImage:arrowImage];
    arrow1ImageView.frame = CGRectMake(90+b, 20, 10, 5);
    
    middleButtonImage = [UIImage imageNamed:@"conditionButton2"];

    sortButton = [[UIButton alloc] initWithFrame:CGRectMake(searchBtn.endPointX, 0, (SORT_BUTTON_WIDTH*(i+1))*a, CONDITION_BUTTON_HEIGHT)];
    sortButton.backgroundColor = [UIColor clearColor];
    [sortButton setBackgroundImage:sideButtonImage forState:UIControlStateNormal];
    [sortButton addTarget:self action:@selector(goSort:) forControlEvents:UIControlEventTouchUpInside];
    [sortButton setBackgroundImage:middleButtonImage forState:UIControlStateNormal];
    
    sortLabel = [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, 3, 30, 17)];
    [sortLabel setText:L(@"Sort")];
    [sortLabel setFont:labelFont];
    [sortLabel setTextColor:[UIColor color111111]];
    [sortLabel setBackgroundColor:[UIColor clearColor]];
    [sortButton addSubview:sortLabel];
    
    sortValueLabel= [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, 19, 100, 20)];
    if(discover!=nil && discover.sortby!=nil){
        [sortValueLabel setText:discover.sortby];
        self.amplitudeSortBy = discover.sortby;
    }else{
        [sortValueLabel setText:SORTBY_POPULARITY];
    }
    [sortValueLabel setFont:valueLabelFont];
    [sortValueLabel  setTextColor:[UIColor color111111]];
    [sortValueLabel setBackgroundColor:[UIColor clearColor]];
    [sortButton addSubview:sortValueLabel];
    
    [sortButton addSubview:arrow1ImageView];
    [sortButton setAlpha:0.8];
    
    [btnsScrollView addSubview:sortButton];
    
    
    filterButton = [[UIButton alloc] initWithFrame:CGRectMake(sortButton.endPointX, 0, (FILTER_BUTTON_WIDTH*(i+1))*a, CONDITION_BUTTON_HEIGHT)];
    filterButton.backgroundColor = [UIColor clearColor];
    [filterButton setBackgroundImage:middleButtonImage forState:UIControlStateNormal];
    [filterButton addTarget:self action:@selector(goFilter:) forControlEvents:UIControlEventTouchUpInside];
    
    filterLabel= [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, 3, 30, 17)];
    [filterLabel setText:L(@"Filter")];
    [filterLabel setFont:labelFont];
    [filterLabel setTextColor:[UIColor color111111]];
    [filterLabel setBackgroundColor:[UIColor clearColor]];
    [filterButton addSubview:filterLabel];
    
    filterValueLabel= [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, 19, 100, 20)];
    if(discover!=nil && [discover hasFilters]){
        [filterValueLabel setText:L(@"Filters")];
    }else{
        [filterValueLabel setText:L(@"None")];
    }
    
    [filterValueLabel setFont:valueLabelFont];
    [filterValueLabel setTextColor:[UIColor color111111]];
    [filterValueLabel setBackgroundColor:[UIColor clearColor]];
    [filterButton addSubview:filterValueLabel];
    arrow2ImageView = [[UIImageView alloc] initWithImage:arrowImage];
    arrow2ImageView.frame = CGRectMake(74+b, 20, 10, 5);
    [filterButton addSubview:arrow2ImageView];
    [filterButton setAlpha:0.8];
    
    [btnsScrollView addSubview:filterButton];
    
    
    bookDateTimeButton = [[UIButton alloc] initWithFrame:CGRectMake(filterButton.endPointX, 0, (BOOK_DATE_TIME_BUTTON_WIDTH*(i+1))*a, CONDITION_BUTTON_HEIGHT)];
    bookDateTimeButton.backgroundColor = [UIColor clearColor];
    
    [bookDateTimeButton setBackgroundImage:sideButtonImage forState:UIControlStateNormal];
    [bookDateTimeButton addTarget:self action:@selector(goSelectTime:) forControlEvents:UIControlEventTouchUpInside];
    
    bookDateTimeLabel= [[UILabel alloc] initWithFrame:CGRectMake(leftMargin, 3, 100, 17)];
    if(discover!=nil && discover.pax!=nil){
        [bookDateTimeLabel setText:[NSString stringWithFormat:L(@"Table for %@"),discover.pax]];
    }
    [bookDateTimeLabel setFont:labelFont];
    [bookDateTimeLabel setTextColor:[UIColor color111111]];
    [bookDateTimeLabel setBackgroundColor:[UIColor clearColor]];
    [bookDateTimeButton addSubview:bookDateTimeLabel];
    
    NSString *bookDateTimeStr;
    if(discover!=nil && discover.bookDate!=nil && discover.bookTime!=nil&&![discover.bookTime isEqualToString:@""]){
        NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd MMM"];
        bookDateTimeStr = [NSString stringWithFormat: @"%@, %@",[dateFormatter stringFromDate:discover.bookDate ],discover.bookTime];
    }
    
    bookDateTimeValueLabel = [[TYAttributedLabel alloc]initWithFrame:CGRectMake(leftMargin, 21, 100, 20)];
    if(bookDateTimeStr!=nil){
        NSMutableAttributedString *bookDateAndTimeAttributedString = [[NSMutableAttributedString alloc]initWithString:bookDateTimeStr];
        NSRange range = [bookDateTimeStr rangeOfString:bookDateTimeStr];
        [bookDateAndTimeAttributedString addAttribute:(id)kCTForegroundColorAttributeName value:(id)[UIColor colorC2060A].CGColor range:range];
        
        CTFontRef font1 = CTFontCreateWithName((CFStringRef)valueLabelFont.fontName, 13, NULL);
        [bookDateAndTimeAttributedString addAttribute:(id)kCTFontAttributeName value:(id)CFBridgingRelease(font1) range:range];
        
        NSString *redTimeStr = [bookDateTimeStr substringToIndex:bookDateTimeStr.length-5];
        NSRange range1 = [bookDateTimeStr rangeOfString:redTimeStr];
        [bookDateAndTimeAttributedString addAttribute:(id)kCTForegroundColorAttributeName value:(id)[UIColor color111111].CGColor range:range1];
        bookDateTimeValueLabel.attributedText = bookDateAndTimeAttributedString;
        bookDateTimeValueLabel.verticalAlignment = TYVerticalAlignmentCenter;
    }
    else
    {
        [bookDateTimeLabel setText:@"Find a"];
        NSMutableAttributedString *bookDateAndTimeAttributedString = [[NSMutableAttributedString alloc]initWithString:@"Table"];
        NSRange range = [@"Table" rangeOfString:@"Table"];
        CTFontRef font1 = CTFontCreateWithName((CFStringRef)valueLabelFont.fontName, 13, NULL);
        [bookDateAndTimeAttributedString addAttribute:(id)kCTFontAttributeName value:(id)CFBridgingRelease(font1) range:range];
        [bookDateAndTimeAttributedString addAttribute:(id)kCTForegroundColorAttributeName value:(id)[UIColor color111111].CGColor range:range];
        bookDateTimeValueLabel.attributedText = bookDateAndTimeAttributedString;
        
    }
    [bookDateTimeValueLabel setBackgroundColor:[UIColor clearColor]];

    [bookDateTimeButton addSubview:bookDateTimeValueLabel];
    
    bookDateTimeControl = [[UIControl alloc] initWithFrame:CGRectMake(bookDateTimeValueLabel.frame.origin.x, bookDateTimeValueLabel.frame.origin.y, bookDateTimeValueLabel.frame.size.width, bookDateTimeValueLabel.frame.size.height)];
    [bookDateTimeControl setBackgroundColor:[UIColor clearColor]];
    [bookDateTimeControl addTarget:self action:@selector(goSelectTime:) forControlEvents:UIControlEventTouchUpInside];
    [bookDateTimeButton addSubview:bookDateTimeControl];
    
    UIImageView *bookArrowImageView = [[UIImageView alloc] initWithImage:arrowImage];
    bookArrowImageView.frame = CGRectMake(87+b, 20, 10, 5);
    [bookDateTimeButton addSubview:bookArrowImageView];
    [bookDateTimeButton setAlpha:0.8];
    
    
    [btnsScrollView addSubview:bookDateTimeButton];
    btnsScrollView.contentSize = CGSizeMake(bookDateTimeButton.endPointX, CONDITION_BUTTON_HEIGHT);

}
- (void)gotoSearch
{

    if (discover.name)
    {
        self.discoverListViewController.searchText = discover.name;
    }
    if (self.isBackJumpToProfile || self.isBackJumpToHome) {
        if (self.isFromDiscoverList) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"initDiscovers"];
        }
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else if (self.isPop) {
        if (self.isFromDiscoverList) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"initDiscovers"];
        }
        [self.navigationController popViewControllerAnimated:YES];

    }else{
        [self.navigationController pushViewController:self.discoverListViewController animated:YES];
    }
}
-(DiscoverListViewController *)discoverListViewController{
    if (!_discoverListViewController) {
        _discoverListViewController = [[DiscoverListViewController alloc] initWithCloseButton];
        _discoverListViewController.searchText = discover.keyword;

    }
    return _discoverListViewController;
}

#pragma  mark - 添加查询结果列表 tableView
- (void)loadSearchResultTableView {
    if(searchResultsTableView){
        return;
    }
    double searchResultsTableViewHeight = DeviceHeight-SMALL_MAP_HEIGHT-CONDITION_BUTTON_HEIGHT+2;
    if (showMap) {
        searchResultsTableViewHeight = restaurantListView.frame.size.height-CONDITION_BUTTON_HEIGHT-45;
    }
    searchResultsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CONDITION_BUTTON_HEIGHT, DeviceWidth, searchResultsTableViewHeight) style:UITableViewStylePlain];
    searchResultsTableView.dataSource = self;
    searchResultsTableView.delegate = self;
    searchResultsTableView.scrollsToTop = YES;
    searchResultsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    [restaurantListView addSubview:searchResultsTableView];
}

#pragma mark- 3 filter action
- (void)goSort:(id)sender {
    SortTypeListViewController *sortTypeListViewController = [[SortTypeListViewController alloc]initWithDiscover:discover];
    sortTypeListViewController.stlvcDelegate = self;
    [self.navigationController presentViewController:sortTypeListViewController animated:YES completion:nil];
}
- (void)goFilter:(id)sender {
    discover.gotoWhere=Isnull;
    FiltersViewController *filtersViewController = [[FiltersViewController alloc]initWithDiscover:discover];
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:filtersViewController];
    filtersViewController.fvcDelegate = self;
    [self presentViewController:navigationController animated:YES completion:nil];
}
- (void)goSelectTime:(id)sender {
    PaxBookTimePickerViewController *paxBookTimePickerViewController = [[PaxBookTimePickerViewController alloc]init];
    paxBookTimePickerViewController.pbtpvcDelegate = self;
    paxBookTimePickerViewController.showCloseButton = YES;
    paxBookTimePickerViewController.fromRestaurantsViewController = YES;
    paxBookTimePickerViewController.discover = discover;
    [self presentViewController:paxBookTimePickerViewController animated:YES completion:nil];
}


#pragma mark UITable datasource and delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView.tag == 10000)
    {
        return discoverArr.count;
    }
    else
    {
        if([SearchUtil searchedRestaurantArray]==nil || [SearchUtil searchedRestaurantArray].count==0){
            return 0;
        }else{
            if([SearchUtil searchedRestaurantCount]%SEARCH_RESULT_PAGE_SIZE==0 && [SearchUtil searchedRestaurantCount]>0){
                return lastPageCount+1;
            }else{
                return lastPageCount;
            }
        }

    }
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 10000)
    {
        static NSString *cellName = @"cellName";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellName];
        if (!cell)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
            cell.selectionStyle=UITableViewCellSelectionStyleGray;
        }

        IMGDiscover *discoverT=[discoverArr objectAtIndex:indexPath.row];
        cell.textLabel.text = discoverT.name;
        return cell;
    }
    else
    {
        if([SearchUtil searchedRestaurantArray].count==0){
            return [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"a"];
        }
        if (indexPath.row >= lastPageCount) {
            return [self loadingCell];
        }
        if (self.isFromEvent) {
            static NSString *identifier =@"cell";
            RestaurantCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (cell == nil) {
                cell = [[RestaurantCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                //        cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.selectionStyle=UITableViewCellSelectionStyleGray;
                //        UIColor* color=[[UIColor alloc]initWithRed:0.01 green:0.01 blue:0.01 alpha:0.1];
                //        cell.selectedBackgroundView=[[UIView alloc]initWithFrame:cell.frame];
                //        cell.selectedBackgroundView.backgroundColor=color;
            }
            //        NSLog(@"%f,cellForRowAtIndexPath = %ld 1.0",[[NSDate date]timeIntervalSince1970],indexPath.row);
            [cell setRestaurant:[[SearchUtil searchedRestaurantArray] objectAtIndex:indexPath.row] andBookDate:discover.bookDate andBookTime:discover.bookTime andPax:discover.pax andRestaurantOffer:[[SearchUtil searchedRestaurantOfferDic] objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]];
            return cell;

            
        }else{
        
            static NSString *identifier =@"SearhPageCell";
            SearchPageResturantCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (cell == nil) {
                cell = [[SearchPageResturantCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                cell.selectionStyle=UITableViewCellSelectionStyleNone;
                cell.searchPageCellDelegate=self;
                
            }
            [cell setRestaurant:[[SearchUtil searchedRestaurantArray] objectAtIndex:indexPath.row] andBookDate:discover.bookDate andBookTime:discover.bookTime andPax:discover.pax andRestaurantOffer:[[SearchUtil searchedRestaurantOfferDic] objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]] andRestaurantNumber:indexPath.row+1];
            cell.index=indexPath.row;
            return cell;
        }

    }
}
-(void)reviewTap:(IMGRestaurant *)restaurant
{

    ReviewViewController* reviewVC=[[ReviewViewController alloc]initWithRestaurant:restaurant];
//    reviewVC.ambianceScore = restaurant.ambianceScore;
//    reviewVC.serviceScore = restaurant.serviceScore;
//    reviewVC.foodScore = restaurant.foodScore;
//    reviewVC.rating = restaurant.ratingScore;
    [self.navigationController pushViewController:reviewVC animated:YES];



}
-(void)restaurantImageTap:(IMGRestaurant *)restaurant
{

//    SearchPhotosViewController* searchPhotoVC=[[SearchPhotosViewController alloc]initWithRestaurant:resturant];
//    searchPhotoVC.isFromSearch = YES;
//    [self.navigationController pushViewController:searchPhotoVC animated:YES];
    
    DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurant:restaurant];
    detailViewController.pax = discover.pax;
    detailViewController.bookDate = discover.bookDate;
    detailViewController.bookTime = discover.bookTime;

    detailViewController.amplitudeType = self.amplitudeType;
    [self.navigationController pushViewController:detailViewController animated:YES];


}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row < lastPageCount) {
        if (indexPath.row >= [[SearchUtil searchedRestaurantArray] count]) {
            return 222.0;
        }
        IMGRestaurant *restaurant=[[SearchUtil searchedRestaurantArray] objectAtIndex:indexPath.row];
       
        if (self.isFromEvent) {
           
            NSString *dollar=nil;
            switch(restaurant.priceLevel.intValue){
                case 1:dollar=@"Below 100K";break;
                case 2:dollar=@"100K - 200K";break;
                case 3:dollar=@"200K - 300K";break;
                case 4:dollar=@"Start from 300K";break;
            }
            UIFont *font=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
            NSString *cuisineAndDistrictText=[NSString stringWithFormat:@"%@ / %@ / %@",restaurant.cuisineName,restaurant.districtName,dollar];
            CGSize size=[cuisineAndDistrictText sizeWithFont:font constrainedToSize:CGSizeMake(DeviceWidth-18*2-70,font.lineHeight*2) lineBreakMode:NSLineBreakByTruncatingTail];
                return CELL_HEIGHT_120+size.height-font.lineHeight;
            

        }else{
               CGFloat height=[SearchPageResturantCell calculateHeightWithRestaurant:restaurant andBookDate:discover.bookDate andBookTime:discover.bookTime andPax:discover.pax andRestaurantOffer:[[SearchUtil searchedRestaurantOfferDic] objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]] andRestaurantNumber:indexPath.row+1];
                  return height;
        
            }
        
        
        
    } else {
        return CELL_HEIGHT_120/2;
    }
}


-(void)gotoDetailViewControllerWithRestaurant:(IMGRestaurant *)restaurant{
    
    DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurant:restaurant];
    detailViewController.pax = discover.pax;
    detailViewController.bookDate = discover.bookDate;
    detailViewController.bookTime = discover.bookTime;

    detailViewController.amplitudeType = self.amplitudeType;
    [self.navigationController pushViewController:detailViewController animated:YES];
    /*
    NSString *currentDateStr;
    if(discover.bookDate!=nil&&discover.bookTime!=nil&&discover.pax!=nil&&![discover.bookTime isEqualToString:@""]){
        NSDateFormatter *formatter = [[NSDateFormatter  alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        currentDateStr = [formatter stringFromDate:discover.bookDate];
    }
    if(currentDateStr!=nil){
        NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:currentDateStr,@"bookDate",discover.pax,@"peopleCount",restaurant.restaurantId,@"restaurantID", nil];
        [[IMGNetWork sharedManager] GET:@"day/times" parameters:parameters success:^(NSURLSessionDataTask *operation, id responseObject) {
            
            NSLog(@"Checking gotoDetailViewControllerWithRestaurant,%@",restaurant.restaurantId);
            
            NSString *exceptionMessage = [responseObject objectForKey:@"exceptionmsg"];
            if(exceptionMessage!=nil){
                NSLog(@"gotoDetailViewControllerWithRestaurant erors when get datas, exceptionmsg is:%@",exceptionMessage);
                return;
            }
            
            NSArray *responseArray = [responseObject objectForKey:@"return"];
            if(responseArray!=nil && [responseArray count]>0){
                NSDictionary *timeSlotDictionary = [responseArray objectAtIndex:0];
                NSMutableArray *tmpRestaurantOfferArray = [responseArray objectAtIndex:1];

                NSMutableArray *restaurantOfferArray = [[NSMutableArray alloc]initWithCapacity:0];
                NSMutableArray *regularTimeArray = [[NSMutableArray alloc]initWithArray:[timeSlotDictionary objectForKey:@"times"]];
                
                for(int i=0;i<tmpRestaurantOfferArray.count;i++){
                    NSDictionary *offerDictionary = [tmpRestaurantOfferArray objectAtIndex:i];
                    IMGRestaurantOffer *restaurantOffer = [[IMGRestaurantOffer alloc]init];
                    [restaurantOffer setValuesForKeysWithDictionary:offerDictionary];
                    restaurantOffer.restaurantId = restaurant.restaurantId;
                    if(restaurantOffer.offerSlotMaxDisc!=nil&&[BookUtil availableOfferSlotBy:restaurantOffer andPeopleCount:[discover.pax intValue] andDate:discover.bookDate andStartTime:discover.bookTime]){
                        [restaurantOfferArray addObject:restaurantOffer];
                    }
                }
                if (restaurantOfferArray.count)
                {
                    currentOffer = [restaurantOfferArray objectAtIndex:0];
                }
                
                DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurant:restaurant andRestaurantOfferArray:restaurantOfferArray andRegularTimeArray:regularTimeArray];
                detailViewController.pax = discover.pax;
                detailViewController.bookDate = discover.bookDate;
                detailViewController.bookTime = discover.bookTime;
                [self.navigationController pushViewController:detailViewController animated:YES];
                
            }else{
                DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurant:restaurant andRestaurantOfferArray:nil andRegularTimeArray:nil];
                detailViewController.pax = discover.pax;
                detailViewController.bookDate = discover.bookDate;
                detailViewController.bookTime = discover.bookTime;
                [self.navigationController pushViewController:detailViewController animated:YES];

            }
        }failure:^(NSURLSessionDataTask *operation, NSError *error) {
            NSLog(@"gotoDetailViewControllerWithRestaurant erors when get datas:%@",error.localizedDescription);
            DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurant:restaurant andRestaurantOfferArray:nil andRegularTimeArray:nil];
            detailViewController.pax = discover.pax;
            detailViewController.bookDate = discover.bookDate;
            detailViewController.bookTime = discover.bookTime;
            [self.navigationController pushViewController:detailViewController animated:YES];
        }];
        
        
    }else{
        DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurant:restaurant];
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
     */
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag == 10000)
    {
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:[self getPageIdByAmplitudeType] forKey:@"Location"];
        [[Amplitude instance] logEvent:@"CL - Restaurant List" withEventProperties:eventProperties];
        
        if (self.isFromEvent) {
            IMGDiscover *discoverT=[discoverArr objectAtIndex:indexPath.row];
            discoverT.fromWhere = @3;
            RestaurantsViewController *restaurantsViewController=[[RestaurantsViewController alloc]initWithMap];
            restaurantsViewController.isFromEvent=YES;
            [restaurantsViewController initDiscover:discoverT];
            [self.navigationController pushViewController:restaurantsViewController animated:YES];
            //        [MixpanelHelper trackViewDiscoverWithDiscoverName:discoverT.name];

        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }else{
        
            IMGDiscover *discoverT=[discoverArr objectAtIndex:indexPath.row];
            discoverT.fromWhere = @3;
            RestaurantsViewController *restaurantsViewController=[[RestaurantsViewController alloc]initWithMap];
            [restaurantsViewController initDiscover:discoverT];
            [self.navigationController pushViewController:restaurantsViewController animated:YES];

        
        
        }
       
    }
    else
    {
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        [restaurantSearchBar resignFirstResponder];
        selectedRestaurant = [[SearchUtil searchedRestaurantArray] objectAtIndex:indexPath.row];
        [self gotoDetailViewControllerWithRestaurant:selectedRestaurant];
    }
    
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (cell.tag == LOADING_CELL_TAG) {
        
        
        if (isHadNextData)
        {
            isHadNextData =NO;
            if(searchResultsTableView!=nil){
                if ([SearchUtil searchedRestaurantArray]==nil || [SearchUtil searchedRestaurantArray].count == 0) {
                    [self loadEmptyTableVIewHeadView];
                }else{
                    [searchResultsTableView setTableHeaderView:suggestHeaderView];
                }
                lastPageCount = [SearchUtil searchedRestaurantArray].count;
                [searchResultsTableView reloadData];
                
                
                [self searchNextPageWithDiscover:discover andOffset:offset+=SEARCH_RESULT_PAGE_SIZE
                                     minLatitude:minLatitudeTemp maxLatitude:maxLatitudeTemp minLongitude:minLongitudeTemp maxLongitude:maxLongitudeTemp andBlock:^(NSArray *restaurantArray) {
                                         
                                         
                                     }];

            }
            
            if(mapViewController!=nil){
                [mapViewController setMapDatas:[SearchUtil searchedRestaurantArray] offset:offset];
            }
        }
        else
            isStartLoad = YES;
        

    }
}
#pragma mark- searchBar delegate
-(void)gotoKeyboardClose
{
    [restaurantSearchBar resignFirstResponder];
}
-(void)keyBoardShow{
//    if (backGroundControl == nil) {
//        backGroundControl = [[UIControl alloc]initWithFrame:self.view.bounds];
//    }
//    
//    [backGroundControl addTarget:self action:@selector(gotoKeyboardClose) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:backGroundControl];
}
-(void)keyBoardClose
{
//    [backGroundControl removeFromSuperview];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if ([restaurantSearchBar.text  isEqual: @""]){
        keyword = restaurantSearchBar.text;
        discover.keyword = keyword;
        offset=0;
        minLatitudeTemp = nil;
        maxLatitudeTemp = nil;
        minLongitudeTemp = nil;
        maxLongitudeTemp = nil;
        [discover resetMapParam];
        [SearchUtil isfromEventsearch:discover offset:offset minLatitude:minLatitudeTemp maxLatitude:maxLatitudeTemp minLongitude:minLongitudeTemp maxLongitude:maxLongitudeTemp andBlock:^(NSArray *restaurantArray,NSDictionary *suggestDic) {
            if(searchResultsTableView!=nil){
                if ([SearchUtil searchedRestaurantArray]==nil || [SearchUtil searchedRestaurantArray].count == 0) {
                    [self loadEmptyTableVIewHeadView];
                }else{
                    [self createSuggestHeaderView:suggestDic];
                }
                [searchResultsTableView reloadData];
            }
            
            lastPageCount = [SearchUtil searchedRestaurantArray].count;

            if(mapViewController!=nil){
                [mapViewController setMapDatas:[SearchUtil searchedRestaurantArray] offset:offset];
            }
            [loadingImageView stopLoading];
            loadingImageView=nil;
            searchResultsTableView.scrollEnabled = YES;
        }];
        
    }
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
	for(id cc in [searchBar subviews])
    {
        if([cc isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton *)cc;
            [btn setTitle:L(@"Cancel")  forState:UIControlStateNormal];
        }
    }
	return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {

}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {


}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [loadingImageView startLoading];
    [searchBar resignFirstResponder];
    keyword=searchBar.text;
    discover.keyword=keyword;

    searchResultsTableView.contentSize = CGSizeMake(DeviceWidth, 0);
    searchResultsTableView.scrollEnabled = NO;
    loadingImageView = [[LoadingImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, searchResultsTableView.frame.size.height) andViewNum:1 andShadowWidth:0];
    [searchResultsTableView addSubview:loadingImageView];
    
//    [MixpanelHelper trackSerachRestaurantWithKeyWord:keyword];
    offset=0;
    minLatitudeTemp = nil;
    maxLatitudeTemp = nil;
    minLongitudeTemp = nil;
    maxLongitudeTemp = nil;
    [discover resetMapParam];
    [SearchUtil isfromEventsearch:discover offset:offset minLatitude:minLatitudeTemp maxLatitude:maxLatitudeTemp minLongitude:minLongitudeTemp maxLongitude:maxLongitudeTemp andBlock:^(NSArray *restaurantArray,NSDictionary *suggestDic) {
        if(searchResultsTableView!=nil){
            
            if ([SearchUtil searchedRestaurantArray]==nil || [SearchUtil searchedRestaurantArray].count == 0) {
                [self loadEmptyTableVIewHeadView];
            }else{
                [self createSuggestHeaderView:suggestDic];
            }
            lastPageCount = [SearchUtil searchedRestaurantArray].count;

            [searchResultsTableView reloadData];
        }
        
        if(mapViewController!=nil){
            [mapViewController setMapDatas:[SearchUtil searchedRestaurantArray] offset:offset];
        }
        [loadingImageView stopLoading];
        loadingImageView=nil;
        searchResultsTableView.scrollEnabled = YES;

    }];
    

}
-(void)loadEmptyTableVIewHeadView{
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    if ( (discover.keyword != nil) && (![@"" isEqualToString:discover.keyword] ) ) {
        [eventProperties setValue:discover.keyword forKey:@"keyword"];
    }
    [[Amplitude instance] logEvent:@"RC - View Empty Result on Search" withEventProperties:eventProperties];
    SearchEmptyResultView *headerView = [[SearchEmptyResultView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-restaurantSearchBar.endPointY)];
    headerView.delegate = self;
    [searchResultsTableView setTableHeaderView:headerView];
    [self initDiscovers];
    
}
- (void)gotoSuggest
{
    NSLog(@"gotoSuggest");
    RestaurantSuggestViewController *rsvc = [[RestaurantSuggestViewController alloc] init];
    rsvc.amplitudeType = @"Restaurant search result page";
    [self.navigationController pushViewController:rsvc animated:YES];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    
	[searchBar resignFirstResponder];
}

 

//==========search refresh about Events==============>

- (void)loadDefaultSearchViewFactors {
    restaurantListViewBottomY = DeviceHeight-CONDITION_BUTTON_HEIGHT*2-30;
    restaurantListViewTopY = SMALL_MAP_HEIGHT-CONDITION_BUTTON_HEIGHT  ;
    textPull = @"Pull down to refresh...";
    textRelease = @"Release to refresh...";
    textLoading = @"Loading...";

}
#pragma mark - ego refresh table view
-(void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view{
    [self startLoading];
}
- (void)addPullToRefreshHeader {
    refreshTableHeaderView =[[EGORefreshTableHeaderView alloc]initWithFrame:CGRectMake(0, - REFRESH_HEADER_HEIGHT, DeviceWidth, REFRESH_HEADER_HEIGHT)];
    __autoreleasing __typeof(self)weakSelf = self;
    refreshTableHeaderView.delegate=weakSelf;
    [refreshTableHeaderView setBackgroundColor:[UIColor whiteColor] textColor:nil arrowImage:[UIImage imageNamed:@"arrowDown.png"]];
    refreshTableHeaderView.statusLabel.font=[UIFont boldSystemFontOfSize:12.0];
    [searchResultsTableView addSubview:refreshTableHeaderView];
}

#pragma mark - scroll view delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {

    [refreshTableHeaderView egoRefreshScrollViewWillBeginDragging:scrollView];

    contentOffsetY = scrollView.contentOffset.y;
    if (isLoading) return;

}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    
    if (scrollView.tag == 10000)
    {
        return;
    }
    
    [refreshTableHeaderView egoRefreshScrollViewDidScroll:scrollView];

    [restaurantSearchBar resignFirstResponder];
//    [loadingImageView stopLoading];
//    searchResultsTableView.scrollEnabled = YES;

    CGFloat listViewY = restaurantListView.frame.origin.y;
    
    CGFloat tableViewOffsetY = scrollView.contentOffset.y;
    
    CGRect frame = restaurantListView.frame;
    if (!showMap) {
    if ( listViewY > 0) {
        //向下拉
        if (tableViewOffsetY < 0)
        {
            if (listViewY != restaurantListViewTopY) {
                if (frame.origin.y - scrollView.contentOffset.y > restaurantListViewTopY) {
                    
                    restaurantListView.frame = CGRectMake(0, restaurantListViewTopY, DeviceWidth, DeviceHeight-restaurantListViewTopY);
                }else{
                    restaurantListView.frame = CGRectMake(0, frame.origin.y - scrollView.contentOffset.y, DeviceWidth, frame.size.height + scrollView.contentOffset.y);
                }
                scrollView.contentOffset = CGPointMake(0, 0);
            }
        }
        //向上拉
        if (tableViewOffsetY > 0)
        {

            if (frame.origin.y- scrollView.contentOffset.y < 0) {
                restaurantListView.frame = CGRectMake(0, 0, DeviceWidth,DeviceHeight-sortButton.endPointY);
            }else{
                restaurantListView.frame = CGRectMake(0, frame.origin.y- scrollView.contentOffset.y, DeviceWidth, frame.size.height + scrollView.contentOffset.y);
            }
            scrollView.contentOffset = CGPointMake(0, 0);
        }
    }else if (listViewY == 0){
        if (tableViewOffsetY < 0) {
           // CGRect frame = restaurantListView.frame;
            restaurantListView.frame = CGRectMake(0, frame.origin.y - scrollView.contentOffset.y, DeviceWidth, frame.size.height + scrollView.contentOffset.y);
            scrollView.contentOffset = CGPointMake(0, 0);
        }
    }
    
    //重新设置 tableView的frame
    scrollView.frame = CGRectMake(0, sortButton.endPointY, DeviceWidth, restaurantListView.frame.size.height - sortButton.endPointY+1-44);
    }
    
    if (scrollView.contentSize.height - scrollView.contentOffset.y - scrollView.bounds.size.height < 300*(offset/10))
    {
//        NSLog(@"scrollView.contentOffset.y = %lf",scrollView.contentOffset.y);
        if (scrollView.contentOffset.y == 0)
        {
            return;
        }
        if (offset >= lastPageCount + SEARCH_RESULT_PAGE_SIZE)
        {
            return;
        }
        if (isHadNextData)
        {
            isHadNextData =NO;
            if(searchResultsTableView!=nil){
                if ([SearchUtil searchedRestaurantArray]==nil || [SearchUtil searchedRestaurantArray].count == 0) {
                    [self loadEmptyTableVIewHeadView];
                }else{
                    [searchResultsTableView setTableHeaderView:suggestHeaderView];
                }
                lastPageCount = [SearchUtil searchedRestaurantArray].count;
                [searchResultsTableView reloadData];
                
                
                [self searchNextPageWithDiscover:discover andOffset:offset+=SEARCH_RESULT_PAGE_SIZE
                                     minLatitude:minLatitudeTemp maxLatitude:maxLatitudeTemp minLongitude:minLongitudeTemp maxLongitude:maxLongitudeTemp andBlock:^(NSArray *restaurantArray) {
                                         
                                         
                                     }];
                
            }
            
            if(mapViewController!=nil){
                [mapViewController setMapDatas:[SearchUtil searchedRestaurantArray] offset:offset];
            }
        }
        else
            isStartLoad = YES;

    }

//    //调整 searchBar显示与否
//    if(/*mapSearchLineControl.frame.size.height > 0 && */restaurantListView.frame.origin.y - scrollView.contentOffset.y < 30){
//        [UIView animateWithDuration:0.3 animations:^{
//            mapSearchLineLabel.frame = CGRectMake(0, -2, mapSearchLineControl.frame.size.width, 0);
//            mapSearchLineControl.frame = CGRectMake(5, 6, DeviceWidth-10, 0);
//            restaurantListView.backgroundColor=[UIColor whiteColor];
//        }];
//    }
//    if(/*mapSearchLineControl.frame.size.height == 0 && */restaurantListView.frame.origin.y - scrollView.contentOffset.y > 30){
//        [UIView animateWithDuration:0.3 animations:^{
//            
//            mapSearchLineControl.frame = CGRectMake(5, 6, DeviceWidth-10, 25);
//            mapSearchLineLabel.frame = CGRectMake(0, -2, mapSearchLineControl.frame.size.width, mapSearchLineControl.frame.size.height);
//            restaurantListView.backgroundColor=[UIColor colorWithWhite:1.0 alpha:0.8];
//        }];
//    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [refreshTableHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

- (void)startLoading {
    isLoading = YES;
    [self performSelector:@selector(refreshSearch) withObject:nil afterDelay:0.1];
}

-(void)refreshSearch{
    offset=0;
    [discover resetMapParam];
      [SearchUtil isfromEventsearch:discover offset:offset minLatitude:minLatitudeTemp maxLatitude:maxLatitudeTemp minLongitude:minLongitudeTemp maxLongitude:maxLongitudeTemp andBlock:^(NSArray *restaurantArray,NSDictionary *suggestDic) {
        if(searchResultsTableView!=nil){
            lastPageCount = [SearchUtil searchedRestaurantArray].count;
            if ([SearchUtil searchedRestaurantArray]==nil || [SearchUtil searchedRestaurantArray].count == 0) {
                [self loadEmptyTableVIewHeadView];
            }else{
                [searchResultsTableView setTableHeaderView:suggestHeaderView];
            }
            lastPageCount = [SearchUtil searchedRestaurantArray].count;

            [searchResultsTableView reloadData];
            [self positioned];
        }
        
        if(mapViewController!=nil){
            [mapViewController setMapDatas:[SearchUtil searchedRestaurantArray] offset:offset];
        }
        [refreshTableHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:searchResultsTableView];
    }];
}

- (UITableViewCell *)loadingCell {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:nil] ;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]
                                                  initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.frame = CGRectMake(DeviceWidth/2 - 10, floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, REFRESH_HEADER_HEIGHT)];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:12.0];
    label.textAlignment = NSTextAlignmentCenter;
    [label setText:L(@"loading")];
    [cell addSubview:activityIndicator];
//    [cell addSubview:label];
//    [cell bringSubviewToFront:label];
    
    [activityIndicator startAnimating];
    
//    cell.tag = LOADING_CELL_TAG;
    
    return cell;
}

- (void)addTutorialViewWithCurrentTag:(NSInteger)currentTag
{
    
    UIControl *tutorialBackground = [[UIControl alloc] init];
    tutorialBackground.frame = self.view.bounds;
    tutorialBackground.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    [self.view addSubview:tutorialBackground];
    
    UIView *tutorialView = [[UIView alloc] initWithFrame:CGRectMake(100, 150, 150, 150) ];
    tutorialView.layer.masksToBounds = YES;
    tutorialView.layer.cornerRadius = 10.0;
    tutorialView.layer.borderWidth = 0;
    tutorialView.backgroundColor = [UIColor whiteColor];
    [tutorialBackground addSubview:tutorialView];
    
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = [tutorialStrArr objectAtIndex:currentTag];
    titleLabel.font = [UIFont fontWithName:@"montserratRegular" size:15];
    titleLabel.font = [UIFont systemFontOfSize:15];
    CGSize size = [titleLabel sizeWithWidth:230 - 10*2];
    titleLabel.frame = CGRectMake(10, 10, size.width, size.height);
    [tutorialView addSubview:titleLabel];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.numberOfLines = 0;
    titleLabel.alpha = 0.8f;
    tutorialView.frame = CGRectMake(tutorialView.frame.origin.x, tutorialView.frame.origin.y, titleLabel.endPointX + 10, titleLabel.endPointY + 10);
    
    
    UIButton *dismissBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [dismissBtn setTitle:L(@"Dismiss") forState:UIControlStateNormal];
    [dismissBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    dismissBtn.alpha = 0.6f;
    dismissBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [tutorialView addSubview:dismissBtn];
    dismissBtn.tag = 10000;
    [dismissBtn addTarget:self action:@selector(nextBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    tutorialView.userInteractionEnabled = YES;
    
    
    if (currentTag +1 == tutorialStrArr.count)
    {
        isLastDismissBtn = YES;
        dismissBtn.frame = CGRectMake(tutorialView.frame.size.width - 10 - dismissBtn.titleLabel.expectedWidth-5, titleLabel.endPointY + 15, dismissBtn.titleLabel.expectedWidth, 20);
        
    }
    else
    {
        UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [nextBtn setTitle:L(@"Next") forState:UIControlStateNormal];
        [nextBtn setTitleColor:[UIColor colorRed] forState:UIControlStateNormal];
        nextBtn.frame = CGRectMake(tutorialView.frame.size.width - 10 - nextBtn.titleLabel.expectedWidth-5, titleLabel.endPointY + 15, nextBtn.titleLabel.expectedWidth, 20);
        [tutorialView addSubview:nextBtn];
        nextBtn.tag = currentTag;
        [nextBtn addTarget:self action:@selector(nextBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        nextBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        
        tutorialView.userInteractionEnabled = YES;
        
        dismissBtn.frame = CGRectMake(tutorialView.frame.size.width -10 -nextBtn.frame.size.width - 10 - dismissBtn.titleLabel.expectedWidth -5, titleLabel.endPointY + 15, dismissBtn.titleLabel.expectedWidth, 20);
    }
    
    
    UIImageView *selectImageView = [[UIImageView alloc] init];
    selectImageView.image = [UIImage imageNamed:@"select"];
    [tutorialView addSubview:selectImageView];
    
    int eventHeight = 0;
    if (showEvent)
    {
        eventHeight = 45;
    }
    int iphone6Height=0;
    int diffrentHeight=0;
    int iphone6Width=0;
    int height=0;
    int rateheight=0;
    if ([UIDevice isIphone6Plus]) {
        iphone6Height = 41;
        diffrentHeight=6;
        iphone6Width=-20;
        
    }
    if ([UIDevice isIphone6]) {
        iphone6Height = 23;

    }
 


    float a=0;
    if ([UIDevice isiPadHiRes]) {
        a=360;
    }
    
       switch (currentTag) {
        case 0:
        {
            if ([UIDevice isIphone6Plus] && self.isFromEvent) {
                iphone6Height -= 16;
            }
            tutorialView.frame = CGRectMake(140*[AppDelegate ShareApp].autoSizeScaleX+iphone6Width+a, (18 + eventHeight)*[AppDelegate ShareApp].autoSizeScaleY+iphone6Height+height, titleLabel.endPointX + 10, dismissBtn.endPointY + 10);
            selectImageView.frame = CGRectMake(tutorialView.frame.size.width - 20, tutorialView.frame.size.height -1, 12, 9);
            
        }
            break;
        case 1:
        {
            if ([UIDevice isIphone6Plus] && self.isFromEvent) {
                diffrentHeight = -12;
            }
            tutorialView.frame = CGRectMake(12*[AppDelegate ShareApp].autoSizeScaleX, (138 + eventHeight)*[AppDelegate ShareApp].autoSizeScaleY+diffrentHeight+rateheight, titleLabel.endPointX + 10, dismissBtn.endPointY + 10);
            selectImageView.frame = CGRectMake(50, tutorialView.frame.size.height -1, 12, 9);
            
        }
            break;
            
        default:
            break;
    }
    
    
    tutorialView.alpha = 0.f;
    tutorialView.transform = CGAffineTransformMakeScale(0.1f, 0.1f);
    tutorialView.clipsToBounds = NO;
    
    
    [UIView animateWithDuration:0.15f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
        tutorialView.alpha = 1.f;
        tutorialView.transform = CGAffineTransformMakeScale(1.05f, 1.05f);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.08f delay:0.f options:UIViewAnimationOptionCurveEaseInOut animations:^{
            tutorialView.transform = CGAffineTransformIdentity;
        } completion:nil];
    }];
    
    
}

- (void)nextBtnClick:(UIButton *)btn
{
    [UIView animateWithDuration:0.3f animations:^{
        btn.superview.alpha = 0.1f;
        btn.superview.transform = CGAffineTransformMakeScale(0.1f, 0.1f);
    } completion:^(BOOL finished) {
        [btn.superview.superview removeFromSuperview];
        if (btn.tag != 10000)
        {
            [self addTutorialViewWithCurrentTag:btn.tag +1];
        }else{
            if (isLastDismissBtn) {
                [[Amplitude instance] logEvent:@"AT - Restaurant Search Result Page Continue" withEventProperties:@{}];
            }else{
                [[Amplitude instance] logEvent:@"AT - Restaurant Search Result Page Dismiss" withEventProperties:@{}];
            }
        }
    }];
}
- (void)bookNowTap:(IMGRestaurant *)restaurant
{
    NSMutableDictionary *eventProperties = [[NSMutableDictionary alloc] init];
    [eventProperties setObject:@"Restaurant Search Result Page" forKey:@"Location"];
    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
    [[Amplitude instance] logEvent:@"CL - Book Now" withEventProperties:eventProperties];
    NSMutableArray *restaurantOfferArray = [NSMutableArray array];
    if (firstTapBookNow) {
        firstTapBookNow=NO;
        [RestaurantHandler getOfferFromServer:restaurant.restaurantId andBookDate:nil andBlock:^{
            
            [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGRestaurantOffer WHERE restaurantId = '%@' ORDER BY offerType,offerSlotMaxDisc desc;",restaurant.restaurantId] successBlock:^(FMResultSet *resultSet) {
                while ([resultSet next]) {
                    IMGRestaurantOffer  *tmpRestaurantOffer=[[IMGRestaurantOffer alloc]init];
                    [tmpRestaurantOffer setValueWithResultSet:resultSet];
                    [restaurantOfferArray addObject:tmpRestaurantOffer];
                }
                [resultSet close];
            }failureBlock:^(NSError *error) {
                NSLog(@"initRestaurantOfferFromLocal error when get datas:%@",error.description);
            }];
            
            SelectPaxViewController *selectDinersViewController = [[SelectPaxViewController alloc]initWithRestaurant:restaurant andTimeArray:nil andOfferArray:restaurantOfferArray andOffer:nil];
            [self.navigationController pushViewController:selectDinersViewController animated:YES];
            
        }];

    }
}
- (void)menuBtnTap:(IMGRestaurant *)restaurant
{
    [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Menu list" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:nil andOrigin:@"Restaurant search result page" andType:nil];
    
    DetailViewController *detailVC=[[DetailViewController alloc]initWithRestaurant:restaurant];
    detailVC.isGoMenuSection=YES;
//    MenuPhotoGalleryController *menuPhotoGalleryController = [[MenuPhotoGalleryController alloc] initWithRestaurant:restaurant andIsSelfUpload:NO];
    [self.navigationController pushViewController:detailVC animated:YES];

}
- (void)searchDistrictTap:(IMGRestaurant *)restaurant
{
    NSLog(@"distrctid = %@",restaurant.districtId);
    IMGDistrict *district = [[IMGDistrict alloc] init];
    district.districtId = restaurant.districtId;
    district.name = restaurant.districtName;
    discover.searchDistrictArray = [NSMutableArray arrayWithObjects:district, nil];
    [self search:nil];
}
-(NSString*)getPageIdByAmplitudeType{
    NSString *pageId = @"";
    if ([@"Events and Dining Guides" isEqualToString:self.amplitudeType]) {
        pageId = @",Restaurant detail page";
    }
    return [NSString stringWithFormat:@"%@%@",self.amplitudeType,pageId];
}
-(void)addBackButton
{
    navigationView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, navigationHeight)];
    [self.view addSubview:navigationView];
    navigationView.backgroundColor = [UIColor colorC2060A];
    
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:NavigationBackImage] forState:UIControlStateNormal];
    backButton.frame = CGRectMake(-50, navigationHeight-45, 140,48);
    [backButton addTarget:self action:@selector(backClick) forControlEvents:UIControlEventTouchUpInside];
    [navigationView addSubview:backButton];
    
    UILabel *navigationLabel = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth/2-30, 20, 60, 40)];
    navigationLabel.text = @"Nearby";
    navigationLabel.textColor = [UIColor whiteColor];
    [navigationView addSubview:navigationLabel];
    if (self.isfromSearchNearby) {
        navigationLabel.text=@"Restaurants";
        navigationLabel.frame=CGRectMake(50, 20, DeviceWidth-100, 40);
        navigationLabel.textAlignment=NSTextAlignmentCenter;
    }
    
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [searchBtn setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
    searchBtn.frame = CGRectMake(DeviceWidth - 38 - 10, (navigationHeight-38)/2.0, 38,38);
    [searchBtn addTarget:self action:@selector(searchButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [navigationView addSubview:backButton];
    if ([UIDevice isIphone6Plus]) {
        mapBtnRight=[[UIButton alloc]initWithFrame:CGRectMake(DeviceWidth-44, 27, 28, 28)];

    }else if ([UIDevice isIphone4Now]||[UIDevice isIphone5]){
        mapBtnRight=[[UIButton alloc]initWithFrame:CGRectMake(DeviceWidth-40, 27, 28, 28)];

    }else{
        mapBtnRight=[[UIButton alloc]initWithFrame:CGRectMake(DeviceWidth-40, 27, 28, 28)];

    }
    [mapBtnRight setImage:[UIImage imageNamed:@"restaurants_map"] forState:UIControlStateNormal];
    [mapBtnRight setImage:[UIImage imageNamed:@"search_map"] forState:UIControlStateSelected];
    [mapBtnRight addTarget:self action:@selector(searchButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [navigationView addSubview:mapBtnRight];
    
    
    
}
-(void)backClick{
    
    if (self.isfromSearchNearby) {
      [self.navigationController popViewControllerAnimated:YES];
        
    }else if (self.isNearBy) {
        [self.navigationController popViewControllerAnimated:NO];
        [[AppDelegate ShareApp] goToPage:0];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
-(void)back{
    [[LoadingView sharedLoadingView] stopLoading];
    if(self.isFromSearchDininguide){
        [self.navigationController popViewControllerAnimated:YES];

    }else if (self.isBackJumpToHome) {
        [[AppDelegate ShareApp].discoverListNavigationController popViewControllerAnimated:NO];
        [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 0;
        [AppDelegate ShareApp].selectedNavigationController = [AppDelegate ShareApp].homeNavigationController;
    }else if(self.isBackJumpToProfile){
        [[AppDelegate ShareApp].discoverListNavigationController popViewControllerAnimated:NO];
        [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 4;
        [AppDelegate ShareApp].selectedNavigationController = [AppDelegate ShareApp].ProfileNavigationController;
    }else if (self.isBackJumpToSearch){
    
        [[AppDelegate ShareApp].discoverListNavigationController popToRootViewControllerAnimated:YES];
        [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 1;
        [AppDelegate ShareApp].selectedNavigationController = [AppDelegate ShareApp].discoverListNavigationController;

    
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}


@end
