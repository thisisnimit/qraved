//
//  LocationFilteredListViewController.m
//  Qraved
//
//  Created by Jeff on 8/5/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "LocationFilteredListViewController.h"

#import "LocationFilteredListController.h"
#import "PaxBookTimePickerViewController.h"


#import "Label.h"
#import "UIColor+Helper.h"
#import "UIViewController+Helper.h"

#import "DiscoverTitleLabel.h"
#import "LocationItemCell.h"

#import "IMGLocation.h"

#define HEAD_VIEW_HEIGHT 80
#define SEARCH_BAR_HEIGHT 30
#define DEFAULT_SEARCH_TEXT @"Find location"

@implementation LocationFilteredListViewController


-(void)viewWillAppear:(BOOL)animated
{

    [super viewWillAppear:YES];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = L(@"Discover");
    self.view.backgroundColor = [UIColor colorRed];
    [self setBackBarButtonOffset30];
    [self loadHeadView];
    [self loadSearchBar];
	[self loadTableView];
    
//	self.locationFilteredListController = [[LocationFilteredListController alloc] initWithStyle:UITableViewStylePlain];
//	[self.locationFilteredListController.view setFrame:CGRectMake(0, 110, 200, 0)];
//	self.locationFilteredListController.passValueDelegate = self;
//	[self.view addSubview:self.locationFilteredListController.view];
//    [self.view bringSubviewToFront:self.locationFilteredListController.view];

	[self loadButtonView];
    
}

- (void)loadHeadView
{
    self.headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, HEAD_VIEW_HEIGHT)];
    
    DiscoverTitleLabel *titleLabel = [[DiscoverTitleLabel alloc] initWithFrame:CGRectMake(25, 0, DeviceWidth-50, HEAD_VIEW_HEIGHT) title:L(@"Which locations do you prefer?")];
    [self.headView addSubview:titleLabel];
    [self.headView setBackgroundColor:[UIColor colorC2060A]];    
    [self.view addSubview:self.headView];
    
}

- (void)loadSearchBar
{
    self.searchStr = DEFAULT_SEARCH_TEXT;
	
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, self.headView.frame.size.height, DeviceWidth, SEARCH_BAR_HEIGHT)];
    self.searchBar.delegate = self;
    self.searchBar.placeholder = DEFAULT_SEARCH_TEXT;
    [self.view addSubview:self.searchBar];
}

- (void)loadTableView
{
    self.locationTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, HEAD_VIEW_HEIGHT+SEARCH_BAR_HEIGHT, DeviceWidth, DeviceHeight-200) style: UITableViewStylePlain];
    self.locationTableView.dataSource = self;
	self.locationTableView.delegate = self;
    [self.locationTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[self.view addSubview:self.locationTableView];
}

- (void)loadButtonView
{
    self.goButton=[[UIButton alloc] init ];
    [self.goButton setFrame:CGRectMake(0,DeviceHeight-90 , DeviceWidth, 46)];
    [self.goButton setTitle:L(@"Go") forState:UIControlStateNormal];
    [self.goButton.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
    [self.goButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.goButton setBackgroundColor:[UIColor colorRed]];
    [self.goButton addTarget:self action:@selector(go:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.goButton];
}

-(void)go:(id)sender
{
    PaxBookTimePickerViewController *paxBookTimePickerViewController = [[PaxBookTimePickerViewController alloc]initWithCloseButton];
    [self.navigationController pushViewController:paxBookTimePickerViewController animated:YES];
}

/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
    [super viewDidUnload];
}



- (void)setFilterListHidden:(BOOL)hidden {
//	NSInteger height = hidden ? 0 : 100;
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:.2];
//	[self.locationFilteredListController.view setFrame:CGRectMake(30, 110, 200, height)];
//    [self.view bringSubviewToFront:self.locationFilteredListController.view];
	[UIView commitAnimations];
}

#pragma mark -
#pragma mark PassValue protocol
- (void)passValue:(NSString *)value{
	if (value) {
		_searchBar.text = value;
		[self searchBarSearchButtonClicked:_searchBar];
	}
	else {
		
	}
}

#pragma mark -
#pragma mark SearchBar Delegate Methods
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	if ([searchText length] != 0) {
//		self.locationFilteredListController.searchText = searchText;
//		[self.locationFilteredListController updateData];
		[self setFilterListHidden:NO];
	}
	else {
		[self setFilterListHidden:YES];
	}
    
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
//	searchBar.showsCancelButton = YES;
    searchBar.placeholder = @"";
	for(id cc in [searchBar subviews])
    {
        if([cc isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton *)cc;
            [btn setTitle:L(@"Cancel")  forState:UIControlStateNormal];
        }
    }
	return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
	searchBar.text = @"";
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
//	searchBar.showsCancelButton = NO;
//	searchBar.text = @"";
    searchBar.placeholder = DEFAULT_SEARCH_TEXT;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	[self setFilterListHidden:YES];
	self.searchStr = [self.searchBar text];
	[searchBar resignFirstResponder];
	[self.locationTableView reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
	[self setFilterListHidden:YES];
	[searchBar resignFirstResponder];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 20;
}


// Customize the appearance of table view cells.
- (LocationItemCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";

	NSUInteger row = [indexPath row];
    
    LocationItemCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[LocationItemCell alloc] initWithStyle:UITableViewCellStyleDefault selectionStyle:UITableViewCellSelectionStyleNone reuseIdentifier:CellIdentifier] ;
    }

    IMGLocation *location = [[IMGLocation alloc] init];
    location.districtTitle = [NSString stringWithFormat:@"%@%d",self.searchStr,row];
    cell.object = location;
    [cell.textLabel setText:location.districtTitle];
    
    return cell;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    LocationItemCell *cell = (LocationItemCell *)[self.locationTableView  cellForRowAtIndexPath:indexPath];
    [cell click];
}

@end
