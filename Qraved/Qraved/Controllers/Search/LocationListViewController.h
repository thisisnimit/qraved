//
//  LocationFilterViewController.h
//  Qraved
//
//  Created by Jeff on 8/11/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "IMGDiscover.h"

@interface LocationListViewController : BaseChildViewController<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>

- (id)initWithoutHead;
- (id)initWithHead:(IMGDiscover *)discover;
@property(nonatomic,assign)BOOL isFromSearch;
@end
