//
//  LocationFilterViewController.m
//  Qraved
//
//  Created by Jeff on 8/11/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "LocationListViewController.h"

#import "Consts.h"
#import "UIConstants.h"
//#import "MixpanelHelper.h"

#import "DiscoverListViewController.h"
#import "IMGCity.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "UIViewController+Helper.h"
#import "UIImage+Helper.h"
#import "Helper.h"
#import "Label.h"
#import "RestaurantsViewController.h"
#import "SearchUtil.h"
#import "DiscoverTitleLabel.h"
#import "SearchCellLabel.h"
#import "DistrictHandler.h"
#import "IMGDistrict.h"
#import "DBManager.h"
#import "LocationTableViewCell.h"
#import "V2_DiscoverListResultViewController.h"

#define SEARCHBAR_HEIGHT 45

#define HEAD_VIEW_HEIGHT 80
#define TOP_BUTTON_HEIGHT 45


@interface LocationListViewController ()
@property(nonatomic,strong)UIView *headView;
@end

@implementation LocationListViewController{
    BOOL showHead;
    
    UIButton *nearbyButton;
    UIImage *nearbyButtonImage;
    
    UISearchBar *locationSearchBar;
    
    UITableView *locationTableView;
    
    NSArray *dictionaryArray;
    NSMutableArray *restlutsArray;
    IMGDiscover *_discover;
    NSMutableArray * searchResultArray;
    BOOL searchActive;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (id)initWithoutHead{
    self = [super init];
    if (self) {
        showHead = FALSE;
//        showCloseButton = TRUE;
    }
    return self;
}

- (id)initWithHead:(IMGDiscover *)discover{
    self = [super init];
    if (self) {
        showHead = TRUE;
        _discover=discover;
        
        if(_discover==nil){
            _discover = [[IMGDiscover alloc]init];
        }
        if(_discover.searchDistrictArray==nil){
            _discover.searchDistrictArray = [[NSMutableArray alloc]initWithCapacity:0];
        }

    }
    return self;
}

- (void)initData{
    //        dictionaryArray = NSArray arrayWithObjects:, nil
    IMGDistrict *allOfCityLocation = [[IMGDistrict alloc] init];
    allOfCityLocation.districtId = [[NSNumber alloc] initWithInt:0];
    IMGCity *city=[IMGCity findById:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]];
    allOfCityLocation.name = [NSString stringWithFormat:L(@"ALL of %@"),city.name];
//    NSArray *allOfCityArray = [[NSArray alloc] initWithObjects:allOfCityLocation, nil];
    
    NSMutableArray *popularLocationArray = [[NSMutableArray alloc]initWithCapacity:0];
    NSMutableArray *alphabeticalLocationArray = [[NSMutableArray alloc]initWithCapacity:0];
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGDistrict WHERE cityId = %@ ORDER BY name;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]] successBlock:^(FMResultSet *resultSet) {
        while ([resultSet next]) {
            IMGDistrict  *district=[[IMGDistrict alloc]init];
            [district setValueWithResultSet:resultSet];
            if([district.popular intValue]==1){
                [popularLocationArray addObject:district];
            }
            [alphabeticalLocationArray addObject:district];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"addMainView error when get datas:%@",error.description);
    }];
    
    dictionaryArray = [[NSArray alloc] initWithArray:alphabeticalLocationArray];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = @"Location";
    [self.view setBackgroundColor:[UIColor whiteColor]];
    restlutsArray = [[NSMutableArray alloc]init];
    [self setBackBarButtonOffset30];
    [self initData];
    
//    [self loadHeadView];
    [self loadSearchBar];
    [self loadTableView];
    
    
}

- (void)loadHeadView
{
    if(showHead){
        self.navigationItem.title = L(@"Discover");
        self.headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, HEAD_VIEW_HEIGHT+5)];
        
        DiscoverTitleLabel *titleLabel = [[DiscoverTitleLabel alloc] initWithFrame:CGRectMake(25, -5, DeviceWidth-50, self.headView.frame.size.height) title:@"Which locations do you prefer?"];
        [self.headView addSubview:titleLabel];
        [self.headView setBackgroundColor:[UIColor colorC2060A]];
        [self.view addSubview:self.headView];
#pragma mark 标题下阴影
        UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:self.headView.frame.size.height-6 andHeight:0];
        [self.headView addSubview:shadowImageView];
        shadowImageView.transform = CGAffineTransformMakeScale(1, -1);
    }
    
}


- (void)loadSearchBar{
    
    
    float height = 0;
    
    locationSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, height, DeviceWidth, 45)];
    locationSearchBar.delegate = self;
    locationSearchBar.placeholder = L(@"Find Location");
    [self.view addSubview:locationSearchBar];
    if ([UIDevice isLaterThanIos6]) {
        locationSearchBar.searchBarStyle = UISearchBarStyleMinimal;
    }else{
        for(UIView *subview in locationSearchBar.subviews){
            if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                [subview removeFromSuperview];
            }
            
        }
    }
    locationSearchBar.layer.borderColor = [UIColor colorEEEEEE].CGColor;
    locationSearchBar.layer.borderWidth = 0.3;
    [locationSearchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"search_input-box" ]  forState:UIControlStateNormal];

    
//    float searchBarY = 0;
//    if(showHead){
//        searchBarY = self.headView.endPointY;
//    }else{
//        searchBarY = nearbyButton.endPointY;
//    }
//    locationSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, searchBarY, DeviceWidth, SEARCHBAR_HEIGHT)];
//    locationSearchBar.delegate = self;
//    locationSearchBar.showsCancelButton = NO;
//    [self.view addSubview:locationSearchBar];
//    locationSearchBar.placeholder = L(@"Find Location");
//
//    if ([UIDevice isLaterThanIos6]) {
//        locationSearchBar.searchBarStyle = UISearchBarStyleMinimal;
//    }else{
//        for(UIView *subview in locationSearchBar.subviews){
//            if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
//                [subview removeFromSuperview];
//            }
//        }
//    }
//    [locationSearchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"search_input-box" ]  forState:UIControlStateNormal];

}

- (void)loadTableView
{
//    float locationTableViewHeight = 0;
//    if(showHead){
//        locationTableViewHeight = DeviceHeight-TOP_BUTTON_HEIGHT-self.headView.frame.size.height-SEARCHBAR_HEIGHT;
//    }else{
//        locationTableViewHeight = DeviceHeight-TOP_BUTTON_HEIGHT-nearbyButton.frame.size.height-SEARCHBAR_HEIGHT;
//    }
    locationTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, locationSearchBar.frame.origin.y+locationSearchBar.frame.size.height, DeviceWidth, DeviceHeight-90) style:UITableViewStylePlain];
    locationTableView.dataSource = self;
//    locationTableView.separatorColor = [UIColor colorEDEDED];
    locationTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    locationTableView.delegate = self;
    locationTableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:locationTableView];
}

//- (void)goNearby:(id)sender{
////    [MixpanelHelper trackRestaurantTabFilterWithTab:@"Nearby"];
//    IMGDiscover *discover=[[IMGDiscover alloc]init];
//    discover.sortby=SORTBY_DISTANCE;
//    discover.fromWhere = [NSNumber numberWithInt:1];
//    RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc]initWithMap];
//    [restaurantsViewController  initDiscover:discover];
//    [self.navigationController pushViewController:restaurantsViewController animated:YES];
//    
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *identifier = [[NSString alloc] initWithFormat:@"Cell%ld",(long)indexPath.row ];
    
    LocationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[LocationTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault selectionStyle:UITableViewCellSelectionStyleNone reuseIdentifier:identifier] ;
    }
    IMGDistrict *district;
    if(searchActive){
        district = [searchResultArray objectAtIndex:indexPath.row];
    }else{
        district = [dictionaryArray objectAtIndex:indexPath.row];
    }
    [cell setText:district.name];
    [cell setChecked:[_discover hasDistrict:district]];
    return cell;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.isFromSearch) {
        V2_DiscoverListResultViewController *V2_DiscoverListResultViewC = [[V2_DiscoverListResultViewController alloc] init];
        IMGDistrict *district = (IMGDistrict *)(dictionaryArray[indexPath.row]);
        _discover = [[IMGDiscover alloc] init];
        _discover.searchDistrictArray = [[NSMutableArray alloc] initWithObjects:district, nil];
        _discover.districtArray= [[NSMutableArray alloc] initWithObjects:district, nil];
        _discover.sortby = SORTBY_POPULARITY;
    V2_DiscoverListResultViewC.keywordString = district.name;
        _discover.fromWhere=[NSNumber numberWithInt:1];
        [V2_DiscoverListResultViewC initDiscover:_discover];
        [self.navigationController pushViewController:V2_DiscoverListResultViewC animated:YES];
//        RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc]initWithMap];
//        restaurantsViewController.isBackJumpToSearch=YES;
//        IMGDistrict *district = (IMGDistrict *)(dictionaryArray[indexPath.row]);
//        _discover = [[IMGDiscover alloc] init];
//        _discover.searchDistrictArray = [[NSMutableArray alloc] initWithObjects:district, nil];
//       _discover.districtArray= [[NSMutableArray alloc] initWithObjects:district, nil];
//        _discover.sortby = SORTBY_POPULARITY;
//        
//        _discover.fromWhere=[NSNumber numberWithInt:1];
//        [restaurantsViewController initDiscover:_discover];
//        [self.navigationController pushViewController:restaurantsViewController animated:YES];
    }else{
    LocationTableViewCell *cell = (LocationTableViewCell *)[tableView  cellForRowAtIndexPath:indexPath];
    NSArray  * selectArray = searchActive ? searchResultArray : dictionaryArray;

    IMGDistrict * district = [selectArray objectAtIndex:indexPath.row];
    [cell setChecked:![_discover hasDistrict:district]];
    if(![_discover hasDistrict:district]){
        [_discover addDistrict:district];
    }else{
        [_discover removeDistrict:district];
    }
//
    
        //这个通知没明白是什么意思
    [[NSNotificationCenter defaultCenter] postNotificationName:SEARCH_FILTER object:nil userInfo:@{@"discover":_discover,@"tempArray":district}];
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            if ([[tableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                return;
            }
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        
        @catch ( NSException *e ) {
            return;
        }
    });    
//    
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(searchActive){
        return searchResultArray.count;
    }
    return [dictionaryArray count];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 37)];
    [view setBackgroundColor:[UIColor colorFBFBFB]];
    if(section==1){
        SearchCellLabel *label = [[SearchCellLabel alloc] initWith16BoldBlackColorAndFrame:CGRectMake(10, 0, DeviceWidth, view.frame.size.height)];
        [label setText:L(@"Popular Locations")];
        [view addSubview:label];
    }else if(section==2){
        SearchCellLabel *label = [[SearchCellLabel alloc] initWith16BoldBlackColorAndFrame:CGRectMake(10, 0, DeviceWidth, view.frame.size.height)];
        [label setText:L(@"Alphabetical")];
        [view addSubview:label];
    }

    UIImageView *lineImage2 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 1)];
    lineImage2.backgroundColor = [UIColor colorEEEEEE];
    [view addSubview:lineImage2];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CELL_HEIGHT_45;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section==0){
        return 0;
    }else {
        return 37;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scroll {
    [locationSearchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if(searchResultArray==nil){
      searchResultArray = [[NSMutableArray alloc]init];
    }else{
        [searchResultArray removeAllObjects];
    }
    searchActive = searchText.length==0 ? NO:YES;
    if(searchActive){
        for (IMGTag * tag in dictionaryArray) {
            NSString * string = tag.name;
            
            NSRange range = [string rangeOfString:searchText options:1];
            
            if(range.location!=NSNotFound){
                [searchResultArray addObject:tag];
            }
        }
    }
    
    [locationTableView reloadData];

//    [restlutsArray removeAllObjects];
//    if(searchText!=nil && searchText.length>0){
//        if ([[DBManager manager]SQLNullWithClass:[IMGDistrict class]]) {
//            [[[DistrictHandler alloc]init]initFromServer];
//        }
//        IMGDistrict *allOfCityLocation = [[IMGDistrict alloc] init];
//        allOfCityLocation.districtId = [[NSNumber alloc] initWithInt:0];
//        
//        IMGCity *city=[IMGCity findById:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]];
//        allOfCityLocation.name = [NSString stringWithFormat:L(@"ALL of %@"),city.name];
//        
////        NSArray *allOfCityArray = [[NSArray alloc] initWithObjects:allOfCityLocation, nil];
//        NSMutableArray *popularLocationArray = [[NSMutableArray alloc]initWithCapacity:0];
//        NSMutableArray *alphabeticalLocationArray = [[NSMutableArray alloc]initWithCapacity:0];
//        
//        
//        [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGDistrict WHERE cityId=%@ and name like '%@' ORDER BY name;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID],[searchText stringByAppendingString:@"%"]] successBlock:^(FMResultSet *resultSet) {
//            while ([resultSet next]) {
//                IMGDistrict  *district=[[IMGDistrict alloc]init];
//                [district setValueWithResultSet:resultSet];
//                if([district.popular intValue]==1){
//                    [popularLocationArray addObject:district];
//                }
//                [alphabeticalLocationArray addObject:district];
//                [restlutsArray addObject:district];
//            }
//            [resultSet close];
//        }failureBlock:^(NSError *error) {
//            NSLog(@"addMainView error when get datas:%@",error.description);
//        }];
//        
//        dictionaryArray = [[NSArray alloc] initWithArray:alphabeticalLocationArray];
//    }
//    else
//    {
//        [self initData];
//        [locationTableView setTableHeaderView:nil];
//        [locationTableView reloadData];
//        return;
//    }
//    if (restlutsArray.count == 0) {
//        UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-searchBar.endPointY)];
//        UIImage *image = [UIImage imageNamed:@"noSearch"];
//        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth/2-image.size.width/2, 40, image.size.width, image.size.height)];
//        imageView.image = image;
//        [headerView addSubview:imageView];
//        NSString *noRestaurantStr = L(@"We can't find any results for this search. You can try searching for something more general.");
//        CGSize expectSize = [noRestaurantStr sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16] constrainedToSize:CGSizeMake(DeviceWidth-LEFTLEFTSET*4, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
//        Label *noRestaurantLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET*2, imageView.endPointY+15, DeviceWidth-4*LEFTLEFTSET, expectSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16] andTextColor:[UIColor color222222] andTextLines:0];
//        noRestaurantLabel.lineBreakMode = NSLineBreakByWordWrapping;
//        noRestaurantLabel.textAlignment = NSTextAlignmentCenter;
//        noRestaurantLabel.text = noRestaurantStr;
//        [headerView addSubview:noRestaurantLabel];
//        
//        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 1)];
//        lineImage.backgroundColor = [UIColor colorEEEEEE];
//        [headerView addSubview:lineImage];
//        
//        [locationTableView setTableHeaderView:headerView];
//        dictionaryArray =nil;
//        [locationTableView reloadData];
//        locationTableView.contentOffset = CGPointMake(0, 0);        
//    }
//    else
//    {
//        [locationTableView setTableHeaderView:nil];
//        [locationTableView reloadData];
//    }
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
//	searchBar.showsCancelButton = YES;
	for(id cc in [searchBar subviews])
    {
        if([cc isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton *)cc;
            [btn setTitle:L(@"Cancel")  forState:UIControlStateNormal];
        }
    }
	return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
	searchBar.text = @"";
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
//	searchBar.showsCancelButton = NO;
    //	searchBar.text = @"";
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
	[searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
	[searchBar resignFirstResponder];
}

 


@end
