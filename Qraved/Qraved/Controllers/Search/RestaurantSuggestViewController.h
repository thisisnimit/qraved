//
//  RestaurantSuggestViewController.h
//  Qraved
//
//  Created by Lucky on 15/10/5.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZYQAssetPickerController.h"
#import "UIKeyboardCoView.h"
#import "CameraViewController.h"
#import <MapKit/MapKit.h>

@interface RestaurantSuggestViewController : BaseViewController<UITextViewDelegate,UITextFieldDelegate,ZYQAssetPickerControllerDelegate,UINavigationControllerDelegate,UIScrollViewDelegate,UIKeyboardCoViewDelegate,MKMapViewDelegate>
@property(nonatomic,retain) NSString *amplitudeType;
@end
