//
//  FeatureListViewController.h
//  Qraved
//
//  Created by Jeff on 8/22/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "IMGDiscover.h"

#import "FeatureFilteredListController.h"
@interface FeatureListViewController : BaseChildViewController<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, retain) UISearchBar *searchBar;
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) UIView   * headView;
@property (nonatomic, retain) UIButton * goButton;

@property (nonatomic, copy)   NSString	*searchStr;

-(id)initWithDiscover:(IMGDiscover *)paramDiscover;

@end
