//
//  FeatureListViewController.m
//  Qraved
//
//  Created by Jeff on 8/22/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "FeatureListViewController.h"

#import "UIConstants.h"
#import "Label.h"
#import "UIColor+Helper.h"
#import "UIViewController+Helper.h"
#import "Helper.h"
#import "DBManager.h"

#import "FeatueCell.h"
#import "NotificationConsts.h"

@implementation FeatureListViewController{
    NSMutableArray * featureArray;
    BOOL searchActive;
    NSMutableArray * searchResultArray;
    IMGDiscover *discover;
}

#define DEFAULT_SEARCH_TEXT @"Find Feature"
-(id)init{
    self= [super init];
    if(self){

    }
    return self;

}

-(id)initWithDiscover:(IMGDiscover *)paramDiscover{
    self = [super init];
    if(self){
        discover = paramDiscover;
        if(discover==nil){
            discover = [[IMGDiscover alloc]init];
        }
        if(discover.cuisineArray==nil){
            discover.cuisineArray = [[NSMutableArray alloc]initWithCapacity:0];
        }
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = NO;    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Feature";

    self.view.backgroundColor = [UIColor whiteColor];
    
    [self initData];
    [self setBackBarButtonOffset30];
    [self loadSearchBar];
    
    [self loadTableView];

    [_tableView reloadData];

	
}

- (void)loadSearchBar
{
    self.searchStr = DEFAULT_SEARCH_TEXT;
    float height = 0;

    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, height, DeviceWidth, 45)];
    self.searchBar.delegate = self;
    self.searchBar.placeholder = DEFAULT_SEARCH_TEXT;

    [self.view addSubview:self.searchBar];
    if ([UIDevice isLaterThanIos6]) {
        self.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    }else{
        for(UIView *subview in self.searchBar.subviews){
            if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                [subview removeFromSuperview];
            }
            
        }
    }
    self.searchBar.layer.borderColor = [UIColor colorEEEEEE].CGColor;
    self.searchBar.layer.borderWidth = 0.3;
    [self.searchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"search_input-box" ]  forState:UIControlStateNormal];
}

- (void)loadTableView
{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, self.searchBar.frame.origin.y+self.searchBar.frame.size.height, DeviceWidth, DeviceHeight-90) style: UITableViewStylePlain];
    self.tableView.dataSource = self;
	self.tableView.delegate = self;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
	[self.view addSubview:self.tableView];
}

- (void)initData
{
    featureArray = [[NSMutableArray alloc] initWithCapacity:0];
    NSString *sqlStr = [NSString stringWithFormat:@"SELECT * FROM IMGTag where cityId=%@ and status=1 and type='Features' order by ranking;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] ];
    FMResultSet *resultSet=[[DBManager manager] executeQuery:sqlStr];
    while([resultSet next]) {
        IMGTag *feature = [[IMGTag alloc] init];
        [feature setValueWithResultSet:resultSet];
        [featureArray addObject:feature];
    }
    [resultSet close];
    
}




#pragma mark- SearchBar Delegate Methods

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {

    if(searchResultArray==nil){
        searchResultArray = [[NSMutableArray alloc]init];
    }else{
        [searchResultArray removeAllObjects];
    }
    searchActive = searchText.length==0 ? NO:YES;
    if(searchActive){
        for (IMGTag * feature in featureArray) {
            NSString * string = feature.name;

            NSRange range = [string rangeOfString:searchText options:NSAnchoredSearch];

            if(range.location!=NSNotFound){
                [searchResultArray addObject:feature];
            }
        }
    }

    [self.tableView reloadData];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
	searchBar.showsCancelButton = NO;
	return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {

}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = NO;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    self.searchStr = [self.searchBar text];
    [searchBar resignFirstResponder];
	[self.tableView reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
	[searchBar resignFirstResponder];
}

#pragma mark -
#pragma mark Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(searchActive){
        return searchResultArray.count;
    }
    return featureArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CELL_HEIGHT_45;
}
- (FeatueCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";

    IMGTag *object =nil;
    if(searchActive){
        object = [searchResultArray objectAtIndex:indexPath.row];
    }else{
        object = [featureArray objectAtIndex:indexPath.row];
    }

    FeatueCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[FeatueCell alloc] initWithStyle:UITableViewCellStyleDefault selectionStyle:UITableViewCellSelectionStyleNone reuseIdentifier:CellIdentifier] ;
    }

   [cell setText:object.name];
   [cell setChecked: [discover hasTag:object]];

    return cell;
}


#pragma mark- Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray  * selectArray = searchActive ? searchResultArray : featureArray;

    IMGTag * object = [selectArray objectAtIndex:indexPath.row];
    FeatueCell *cell = (FeatueCell *)[tableView  cellForRowAtIndexPath:indexPath];
    
    [cell setChecked:![discover hasTag:object]];
    if(![discover hasTag:object]){
        [discover addTag:object];
    }else{
        [discover removeTag:object];
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            if ([[tableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                return;
            }
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        
        @catch ( NSException *e ) {
            return;
        }
    });
    [[NSNotificationCenter defaultCenter] postNotificationName:SEARCH_FILTER object:nil userInfo:@{@"discover":discover,@"tempArray":object}];
}

- (void)scrollViewDidScroll:(UIScrollView *)scroll {
    [self.searchBar resignFirstResponder];
}

- (void)goToBackViewController{
    [self.searchBar resignFirstResponder];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
