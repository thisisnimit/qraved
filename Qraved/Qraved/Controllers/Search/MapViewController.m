//
//  MapViewController.m
//  Qraved
//
//  Created by Jeff on 8/19/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "MapViewController.h"

#import "UIConstants.h"
#import "AppDelegate.h"
#import "Label.h"
#import "UIViewController+Helper.h"
#import "UIDevice+Util.h"
#import "UIImageView+Helper.h"
#import "UIImage+Resize.h"
#import "CalloutMapAnnotation.h"
#import "CallOutAnnotationView.h"
#import "CustomAnnotation.h"
#import "LoadingView.h"
#import "DetailViewController.h"
#import "IMGRestaurantOffer.h"
#import "BookUtil.h"
#import "NotificationConsts.h"
#import "IMGUser.h"
#import "TrackHandler.h"
#import "RideRequestView.h"
//#import <UberRides/UberRides-Swift.h>
#import "RestaurantMapCell.h"
#import "RollView.h"
#import "V2_DiscoverListResultViewController.h"


#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:@"6.0" options:NSNumericSearch] == NSOrderedAscending)
@interface MapViewController ()<CLLocationManagerDelegate,UIScrollViewDelegate,RollViewDelegate,UIActionSheetDelegate>


{
    MKCoordinateSpan  theSpan;
    CLLocationManager *_locationManager;
}

@property(nonatomic,retain)CalloutMapAnnotation *calloutAnnotation;
@end

@implementation MapViewController{
    BOOL fromMenu;
    float navigationHeight;
    IMGRestaurant *restaurant;
    NSMutableArray *restaurantArr;
    UICollectionView *restaurantCollectionView;
    long currentCellIndex;
    long lastCellIndex;
    MKAnnotationView *lastAnnotationView;
    RollView *rollView;

}

-(MapViewController *)initWithFrame:(CGRect )frame
{
    self=[super init];
    if(self)
    {
        [self.view setFrame:frame];
    }
    return self;
}

-(MapViewController *)initWithFrame:(CGRect )frame andSearchThisArea:(BOOL)searchThisArea
{
    self=[super init];
    if(self)
    {
        self.showSearchThisArea = searchThisArea;
        [self.view setFrame:frame];
    }
    return self;
}


-(MapViewController *)initWithRestaurant:(IMGRestaurant *)r{
    self=[super init];
    if(self)
    {
        restaurant=r;
        self.navigationItem.title = restaurant.title;
        self.restaurantArray=[[NSMutableArray alloc] initWithObjects:restaurant, nil];
//        theSpan.latitudeDelta=0.0010290;
//        theSpan.longitudeDelta=0.0010290;
        _locationManager = [CLLocationManager new];
        _locationManager.delegate = self;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    }
    return self;
}

-(MapViewController *)initWithRestaurant:(IMGRestaurant *)_restaurant fromMenu:(BOOL)from{
    self=[super init];
    if(self){
        self.navigationItem.title = _restaurant.title;
        self.restaurantArray=[[NSMutableArray alloc] initWithObjects:_restaurant, nil];
        fromMenu=from;
        //        theSpan.latitudeDelta=0.0010290;
        //        theSpan.longitudeDelta=0.0010290;
    }
    return self;
}


-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
    [super viewWillAppear:YES];
}

-(void)setData:(IMGRestaurant *)restaurantData
{
    [self.mapView removeAnnotations:self.mapView.annotations];
    
    
    theSpan.latitudeDelta=DELTA_MAP;
    theSpan.longitudeDelta=DELTA_MAP;
        
    [self addAnnotation:restaurantData];

}

-(MKCoordinateRegion) regionForAnnotations:(NSArray*) restaurantArray{
    double minLat=90.0f, maxLat=-90.0f;
    double minLon=180.0f, maxLon=-180.0f;
    
    for(int i=0;i<restaurantArray.count;i++){
        IMGRestaurant *restaurantData=(IMGRestaurant *)[restaurantArray objectAtIndex:i];
        if([restaurantData.latitude doubleValue]>90.0 || [restaurantData.latitude doubleValue]<-90.0){
            restaurantData.latitude=[NSNumber numberWithDouble:0.0];
        }
        if([restaurantData.longitude doubleValue]>180.0 || [restaurantData.longitude doubleValue]<-180.0){
            restaurantData.longitude=[NSNumber numberWithDouble:0.0];
        }
        if ( restaurantData.latitude.doubleValue  < minLat ) minLat = restaurantData.latitude.doubleValue;
        if ( restaurantData.latitude.doubleValue  > maxLat ) maxLat = restaurantData.latitude.doubleValue;
        if ( restaurantData.longitude.doubleValue < minLon ) minLon = restaurantData.longitude.doubleValue;
        if ( restaurantData.longitude.doubleValue > maxLon ) maxLon = restaurantData.longitude.doubleValue;
    }
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake((minLat+maxLat)/2.0, (minLon+maxLon)/2.0);
//    MKCoordinateSpan span = MKCoordinateSpanMake(maxLat-minLat, maxLon-minLon);
    MKCoordinateSpan span = MKCoordinateSpanMake(0.01, 0.01);
    MKCoordinateRegion region = MKCoordinateRegionMake (center, span);
    return region;
}

-(void)setDatas:(NSMutableArray *)restaurantArray
{

    [self.mapView removeAnnotations:self.mapView.annotations];
    
    theSpan.latitudeDelta=DELTA_MAP;
    theSpan.longitudeDelta=DELTA_MAP;
    
    for(int i=0;i<restaurantArray.count;i++){
        IMGRestaurant *restaurantData=(IMGRestaurant *)[restaurantArray objectAtIndex:i];
        
        [self addAnnotation:restaurantData];
    }
    if (restaurantArray!=nil && restaurantArray.count > 0) {
        self.mapView.region =[self regionForAnnotations:restaurantArray];
    }
    
}


-(void)setSearchMapDatas:(NSMutableArray *)restaurantArray offset:(NSInteger)offset{
    if(offset==0 || offset>500){
        [self.mapView removeAnnotations:self.mapView.annotations];
    }
    restaurantArr = restaurantArray;
    for(int i=0;i<restaurantArray.count;i++){
        IMGRestaurant *restaurantData=(IMGRestaurant *)[restaurantArray objectAtIndex:i];
        if(restaurantData.latitude!=nil && [restaurantData.latitude intValue]!=0 && restaurantData.longitude!=nil && [restaurantData.longitude intValue]!=0){
            [self addMapAnnotation:restaurantData];
        }
    }
    [self creatPicRollView];
//    if (restaurantArray!=nil && restaurantArray.count > 0) {
//        self.mapView.region =[self regionSearchForAnnotationsOffset:restaurantArray];
//    }
}




-(void)setMapDatas:(NSMutableArray *)restaurantArray offset:(NSInteger)offset{
    if(offset==0 || offset>500){
        [self.mapView removeAnnotations:self.mapView.annotations];
    }
    
    for(int i=0;i<restaurantArray.count;i++){
        IMGRestaurant *restaurantData=(IMGRestaurant *)[restaurantArray objectAtIndex:i];
        if(restaurantData.latitude!=nil && [restaurantData.latitude intValue]!=0 && restaurantData.longitude!=nil && [restaurantData.longitude intValue]!=0){
            [self addMapAnnotation:restaurantData];
        }
    }
    if (restaurantArray!=nil && restaurantArray.count > 0) {
        if (self.fromSearchPage) {
            restaurantArr = restaurantArray;
            
            [self creatPicRollView];

            currentCellIndex = 0;
            [self jumpToCurrentIndexCustomAnnotation];
        }
        self.mapView.region =[self regionForAnnotationsOffset:restaurantArray];
        
    }else{
        [restaurantCollectionView reloadData];
        [self creatPicRollView];

    }
}

static NSString * const carouselID = @"mapCollectionViewCellId";
-(void)creatPicRollView{
    
    if (rollView) {
        [rollView removeFromSuperview];
    }
    CGFloat distance = self.fromSaved?0:20.0f;
    CGFloat listHeight = self.fromSaved?147:110;
    CGFloat space = self.fromSaved?200:250;
    CGFloat width = self.fromSaved?DeviceWidth-40:self.view.frame.size.width;
    if (self.fromWantToGo) {
        distance =0;
        listHeight=147;
        space =252;
        width = DeviceWidth-40;
    }
    rollView = [[RollView alloc] initWithFrame:CGRectMake(0, DeviceHeight - space, width, listHeight) withDistanceForScroll:distance withGap:8.0f];
    rollView.isfromSaved = self.fromSaved;
    rollView.delegate = self;
    [self.mapView addSubview:rollView];
    NSArray *arr = restaurantArr;
    [rollView rollView:arr];
}

- (void)savedClick:(IMGRestaurant *)restaurant andSavedMapView:(SavedMapListView *)savedMapView{
    if (self.savedClick) {
        self.savedClick(restaurant,savedMapView);
    }
}

-(void)jumpToCurrentIndexCustomAnnotation{
    CustomAnnotation *pointAnnotation = [self getCurrentIndexCustomAnnotation];
    if (pointAnnotation != nil) {
        [self.mapView selectAnnotation:pointAnnotation animated:YES];
    }
}
-(CustomAnnotation *)getCurrentIndexCustomAnnotation{
    IMGRestaurant *currentRestaurant = [restaurantArr objectAtIndex:currentCellIndex];

    for (id pointAnnotation in self.mapView.annotations) {
        if ([pointAnnotation isKindOfClass:[CustomAnnotation class]]) {
            CustomAnnotation *customAnnotation = (CustomAnnotation *)pointAnnotation;
            if ([currentRestaurant.restaurantId isEqualToNumber:customAnnotation.restaurant.restaurantId]) {
                return pointAnnotation;
            }
        }
    }
    return nil;
}


-(MKCoordinateRegion) regionForAnnotationsOffset:(NSArray*) restaurantArray{
    double minLat=90.0f, maxLat=-90.0f;
    double minLon=180.0f, maxLon=-180.0f;
    
    for(int i=0;i<restaurantArray.count;i++){
        IMGRestaurant *restaurantData=(IMGRestaurant *)[restaurantArray objectAtIndex:i];
        if([restaurantData.latitude doubleValue]>90.0 || [restaurantData.latitude doubleValue]<-90.0){
            restaurantData.latitude=[NSNumber numberWithDouble:0.0];
        }
        if([restaurantData.longitude doubleValue]>180.0 || [restaurantData.longitude doubleValue]<-180.0){
            restaurantData.longitude=[NSNumber numberWithDouble:0.0];
        }
        if ( restaurantData.latitude.doubleValue  < minLat ) minLat = restaurantData.latitude.doubleValue;
        if ( restaurantData.latitude.doubleValue  > maxLat ) maxLat = restaurantData.latitude.doubleValue;
        if ( restaurantData.longitude.doubleValue < minLon ) minLon = restaurantData.longitude.doubleValue;
        if ( restaurantData.longitude.doubleValue > maxLon ) maxLon = restaurantData.longitude.doubleValue;
    }
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake((minLat+maxLat)/2.0, (minLon+maxLon)/2.0);
    MKCoordinateSpan span = MKCoordinateSpanMake(maxLat-minLat, maxLon-minLon);
    MKCoordinateRegion region = MKCoordinateRegionMake (center, span);
    return region;
}

-(void)addMapAnnotation:(id)restaurantData{
    CustomAnnotation *pointAnnotation=[[CustomAnnotation alloc]init];
    
    if([restaurantData isKindOfClass:[IMGRestaurant class]]){
        IMGRestaurant *convertRestaurant=(IMGRestaurant *)restaurantData;
        CLLocationCoordinate2D theCoordinate = CLLocationCoordinate2DMake(convertRestaurant.latitude.doubleValue, convertRestaurant.longitude.doubleValue);
        
        MKCoordinateRegion theRegion;
        theRegion.center=theCoordinate;
        theRegion.span=theSpan;
        
        if(CLLocationCoordinate2DIsValid(theCoordinate)){
//            [self.mapView setRegion:theRegion animated:YES];
            pointAnnotation.restaurant=convertRestaurant;
            pointAnnotation.coordinate=theCoordinate;
            [self.mapView addAnnotation:pointAnnotation];
        }
    }
}

-(void)addAnnotation:(id)restaurantData
{
    CustomAnnotation *pointAnnotation=[[CustomAnnotation alloc]init];
    
    if([restaurantData isKindOfClass:[IMGRestaurant class]])
    {
        IMGRestaurant *convertRestaurant=(IMGRestaurant *)restaurantData;
        CLLocationCoordinate2D theCoordinate = CLLocationCoordinate2DMake(convertRestaurant.latitude.doubleValue, convertRestaurant.longitude.doubleValue);
        
        MKCoordinateRegion theRegion;
        theRegion.center=theCoordinate;
        theRegion.span=theSpan;
        
        if(CLLocationCoordinate2DIsValid(theCoordinate))
        {
            [self.mapView setRegion:theRegion animated:YES];
            //Add annotation
            pointAnnotation.restaurant=convertRestaurant;
            pointAnnotation.coordinate=theCoordinate;
            //        NSLog(@"pointAnnotation.restaurant is %@",pointAnnotation.restaurant.title);
            [self.mapView addAnnotation:pointAnnotation];
        }
    }
    else
    {
        IMGRestaurant *convertRestaurantinfo=(IMGRestaurant *)restaurantData;
        
        CLLocationCoordinate2D theCoordinate =CLLocationCoordinate2DMake(convertRestaurantinfo.latitude.doubleValue, convertRestaurantinfo.longitude.doubleValue);
        
        //-6.189909~~~~~~~~106.822396
        
        MKCoordinateRegion theRegion;
        theRegion.center=theCoordinate;
        theRegion.span=theSpan;
        
        if(CLLocationCoordinate2DIsValid(theCoordinate))
        {
            [self.mapView setRegion:theRegion animated:YES];
            
            //Add annotation
            pointAnnotation.coordinate=theCoordinate;
            
            [self.mapView addAnnotation:pointAnnotation];
        }
        
    }
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GO_DETAIL object:nil];
    if (self.fromLeftMenu || self.fromProfile || self.fromHomeCard) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:GO_DETAIL_FROM_SEARCH object:nil];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.screenName = @"Restaurant Map page";
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoDetal:) name:GO_DETAIL object:nil];
    self.mapView=[[MKMapView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, self.view.frame.size.height)];
    [self.mapView setMapType:MKMapTypeStandard];
    self.mapView.delegate=self;
    [self.view addSubview:self.mapView];
    
    //searchBtn
    if (self.showSearchThisArea) {
        UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
        searchButton.frame = CGRectMake(0, 15, 120, 30);
        searchButton.centerX = self.mapView.centerX;
        [searchButton setTitle:@"Search this area" forState:UIControlStateNormal];
        [searchButton setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
        searchButton.titleLabel.font = [UIFont systemFontOfSize:14];
        searchButton.backgroundColor = [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:0.8f];
        searchButton.layer.cornerRadius = 15;
        searchButton.layer.masksToBounds = YES;
        searchButton.layer.borderWidth = 1;
        searchButton.layer.borderColor = [UIColor colorCCCCCC].CGColor;
        [searchButton addTarget:self action:@selector(searchBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.mapView addSubview:searchButton];

    }
    
   
    
    if(self.restaurantArray){
        [self setDatas:self.restaurantArray];
    }
    if (self.navigationController) {
        //标题下阴影
        UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:0 andHeight:4];
        [self.mapView addSubview:shadowImageView];
        
        self.navigationController.navigationBarHidden = NO;
        //返回
        [self setBackBarButtonOffset30];
        //

//        UIBarButtonItem *shareBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"RoundedRectangleMapTop"] style:UIBarButtonItemStylePlain target:self action:@selector(turnToMap:)];
//        shareBtn.tintColor=[UIColor blackColor];
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 105, 40)];
        [btn setTitle:@"Get Directions" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor colortextBlue] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:16];
        [btn addTarget:self action:@selector(turnToMap:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBarButtonItem *shareBtn = [[UIBarButtonItem alloc] initWithCustomView:btn];
        self.navigationItem.rightBarButtonItem=shareBtn;
        
        [self addLocationButton];
    }else{
        navigationHeight = 44+[UIDevice heightDifference];
        [self setIos7Layout];
        [self addMenuButton];
        
    }
    currentCellIndex = 0;
    if (self.fromLeftMenu ||  self.fromProfile || self.fromHomeCard) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotoRestaurantDetal:) name:GO_DETAIL_FROM_SEARCH object:nil];
    }
}
- (void)searchBtnClick:(UIButton *)searchBtn{

    if (self.searchThisArea) {
        self.searchThisArea();
    }
}
-(void)didSelectPicWithIndexPath:(NSInteger)index{
    
    IMGRestaurant *restaurantLocal = [restaurantArr objectAtIndex:index];
    
    if (self.isSavedAmplitude) {
        [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Page" andRestaurantId:restaurantLocal.restaurantId andRestaurantTitle:restaurantLocal.title andLocation:nil andOrigin:@"Saved Restaurant Map" andType:nil];
    }
    if (self.guideId) {
        [IMGAmplitudeUtil trackRestaurantInGuideWithName:@"RC - View Restaurant Page" andDiningGuideId:self.guideId andOrigin:@"Dining Guide" andRestaurantId:restaurantLocal.restaurantId andRestaurantTitle:restaurantLocal.title];
    }
    
    DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:restaurantLocal.restaurantId];
    [self.restaurantsViewController.navigationController pushViewController:dvc animated:YES];

}
-(void)didScrollToIndexpath:(NSInteger)index{
    currentCellIndex = index;
    [self jumpToCurrentIndexCustomAnnotation];
}
- (void)longPress:(UIGestureRecognizer*)gestureRecognizer{
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded){
        return;
    }
    
    //坐标转换
    CGPoint touchPoint = [gestureRecognizer locationInView:self.mapView];
    CLLocationCoordinate2D touchMapCoordinate =
    [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];

//    MKPointAnnotation*　pointAnnotation = nil;
//    pointAnnotation = [[MKPointAnnotation alloc] init];
//    pointAnnotation.coordinate = touchMapCoordinate;
//    pointAnnotation.title = @"名字";
    CalloutMapAnnotation *pointAnnotation = [[CalloutMapAnnotation alloc] initWithLatitude:touchMapCoordinate.latitude
                                      andLongitude:touchMapCoordinate.longitude                                     andRestaurant:restaurant];
    //CustomAnnotation *pointAnnotation = [CustomAnnotation al]
    [self.mapView addAnnotation:pointAnnotation];
    
}

-(void)addLocationButton
{
    UIButton *mapButton = [[UIButton alloc] initWithFrame:CGRectMake(DeviceWidth-50, 10, 35, 35)];
    [mapButton  setBackgroundColor:[UIColor clearColor]];
    [mapButton  addTarget:self action:@selector(gotoCurrentLocation) forControlEvents:UIControlEventTouchUpInside];
    [mapButton  setImage:[UIImage imageNamed:@"ic_located"] forState:UIControlStateNormal];
    [self.view addSubview:mapButton];
    self.mapView.showsUserLocation = YES;
    
}
-(void)gotoCurrentLocation
{
    CLLocationCoordinate2D center;
    center.latitude=[AppDelegate ShareApp].locationManager.location.coordinate.latitude;
    center.longitude = [AppDelegate ShareApp].locationManager.location.coordinate.longitude;
    MKCoordinateSpan span;
    span.latitudeDelta=0.03;
    span.longitudeDelta=0.005;
    MKCoordinateRegion region={center,span};
    [self.mapView setRegion:region];
    
}
-(void)addMenuButton
{
    UIView *navigationView = [[UIView alloc]init];
    
    [self.view addSubview:navigationView];
    navigationView.backgroundColor = [UIColor colorC2060A];

    navigationView.frame = CGRectMake(0, 0, DeviceWidth, navigationHeight);
    
    UIButton * menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setImage:[UIImage imageNamed:NavigationBackImage] forState:UIControlStateNormal];
    menuButton.frame = CGRectMake(-50, navigationHeight-45, 140, 48);
    
    [menuButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [navigationView addSubview:menuButton];
    
    
    Label * naviNameLabel = [[Label alloc]initWithFrame:CGRectMake(menuButton.endPointX, navigationHeight-44, DeviceWidth-menuButton.endPointX*2, 44) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:18] andTextColor:[UIColor whiteColor] andTextLines:1];
    naviNameLabel.textAlignment = NSTextAlignmentCenter;
    naviNameLabel.text = restaurant.title;
    [navigationView addSubview:naviNameLabel];
    
    
    
   
}
-(void)back
{
    if (self.fromLeftMenu) {
        [[AppDelegate ShareApp]goToPage:11];
    }else{
        [[AppDelegate ShareApp]goToPage:1];
    }
}

-(void)turnToMap:(UIBarButtonItem*)btn{
    
    NSMutableDictionary* eventDic=[[NSMutableDictionary alloc]init];
    [eventDic setValue:_restaurantId forKey:@"Restaurant_ID"];
    [eventDic setValue:self.amplitudeType forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL - Get Direction" withEventProperties:eventDic];
    [[AppsFlyerTracker sharedTracker] trackEvent:@"CL - Get Direction" withValues:eventDic];
    [self sendManualScreenName:@"Restaurant Map navigation page"];
    IMGUser *user = [IMGUser currentUser];
    NSNumber *userId = [[NSNumber alloc] init];
    if (user.userId != nil)
    {
        userId = user.userId;
    }
    else
        userId = [NSNumber numberWithInt:0];
    
    [TrackHandler trackWithUserId:userId andRestaurantId:restaurant.restaurantId andContent:[NSDictionary dictionaryWithObject:@"2" forKey:@"type"]];
    
    // (latitude = -6.2324570000000001, longitude = 106.811482)
//中国
//    CLLocationCoordinate2D _start = CLLocationCoordinate2DMake(30.691793, 104.088264);
//    CLLocationCoordinate2D _end = CLLocationCoordinate2DMake(30.691293, 104.088264);

//印尼
//    CLLocationCoordinate2D _start = CLLocationCoordinate2DMake(-6.232457, 106.811482);
//    CLLocationCoordinate2D _end = CLLocationCoordinate2DMake(-6.232157, 106.811482);

    // real data
    
    

    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]]) {
        NSLog(@"YES");

        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Open in Google Maps",@"Open in Apple Maps", nil];

        [sheet showInView:self.view];

    }else{
        [self openInAppleMap];
    }

}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
        NSString *appName = [infoDictionary objectForKey:@"CFBundleDisplayName"];
        
//        CLLocationCoordinate2D _start = CLLocationCoordinate2DMake(_locationManager.location.coordinate.latitude, _locationManager.location.coordinate.longitude);
        CLLocationCoordinate2D _end = CLLocationCoordinate2DMake([restaurant.latitude doubleValue], [restaurant.longitude doubleValue]);
        NSString *urlString = [[NSString stringWithFormat:@"comgooglemaps://?x-source=%@&x-success=%@&saddr=&daddr=%f,%f&directionsmode=driving",appName,@"comgooglemaps",_end.latitude,_end.longitude]stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSURL *aURL = [NSURL URLWithString:urlString];
        
        [[UIApplication sharedApplication] openURL:aURL];
        
    }else if (buttonIndex==1){
        [self openInAppleMap];
    }
}

- (void)openInAppleMap{
    
    CLLocationCoordinate2D _start = CLLocationCoordinate2DMake(_locationManager.location.coordinate.latitude, _locationManager.location.coordinate.longitude);
    CLLocationCoordinate2D _end = CLLocationCoordinate2DMake([restaurant.latitude doubleValue], [restaurant.longitude doubleValue]);
    
    MKMapItem *currentLocation = [[MKMapItem alloc]initWithPlacemark:[[MKPlacemark alloc]initWithCoordinate:_start addressDictionary:nil]];
    currentLocation.name =L(@"Current Location");
    MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:_end addressDictionary:nil] ];
    toLocation.name = restaurant.title;
    [MKMapItem openMapsWithItems:[NSArray arrayWithObjects:currentLocation, toLocation, nil]
                   launchOptions:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:MKLaunchOptionsDirectionsModeDriving, [NSNumber numberWithBool:YES], nil]
                                                             forKeys:[NSArray arrayWithObjects:MKLaunchOptionsDirectionsModeKey, MKLaunchOptionsShowsTrafficKey, nil]]];
}

-(void)openPin
{
    for (id<MKAnnotation> currentAnnotation in self.mapView.annotations) {
        [self.mapView selectAnnotation:currentAnnotation animated:FALSE];
    }
}

 

- (void)mapView:(MKMapView *)theMapView didSelectAnnotationView:(MKAnnotationView *)view {
    if (self.fromSearchPage) {
        if ([view.annotation isKindOfClass:[CustomAnnotation class]]) {
            [theMapView setCenterCoordinate:view.annotation.coordinate animated:YES];
            if (lastAnnotationView!=nil) {
                lastAnnotationView.image = [[UIImage imageNamed:@"ic_white_pin"] imageByScalingAndCroppingForSize:CGSizeMake(27, 33)];
                //            for (UILabel *label in lastAnnotationView.subviews) {
                //                label.textColor = [UIColor blackColor];
                //            }
                
            }
            
            lastAnnotationView = view;
            
            for (int i=0; i<restaurantArr.count; i++) {
                CustomAnnotation *pointAnnotation=(CustomAnnotation*)view.annotation;
                IMGRestaurant *resta=[restaurantArr objectAtIndex:i];
                
                if ([resta.restaurantId intValue]==[pointAnnotation.restaurant.restaurantId intValue]) {
                    //                CGFloat offsetx=(_mapView.frame.size.width)*i;
                    //                restaurantCollectionView.contentOffset=CGPointMake(offsetx, 0);
                    view.image = [[UIImage imageNamed:@"ic_red_pin.png"] imageByScalingAndCroppingForSize:CGSizeMake(28, 35)];
                    //                for (UILabel *label in view.subviews) {
                    //                    label.textColor = [UIColor whiteColor];
                    //                }
                    
                    [UIView animateWithDuration:0.3 animations:^{
                        CGFloat  scrollViewOffsetX =(i)*(rollView.scrollView.frame.size.width);
                        restaurantCollectionView.contentOffset=CGPointMake(scrollViewOffsetX, 0);
                        rollView.scrollView.contentOffset = CGPointMake(scrollViewOffsetX, 0);
                    }];
                    
                }
            }
        }
        
        
    }else if ([view.annotation isKindOfClass:[MKPointAnnotation class]]) {
        if ((self.calloutAnnotation !=nil) && (self.calloutAnnotation.coordinate.latitude == view.annotation.coordinate.latitude&&
            self.calloutAnnotation.coordinate.longitude == view.annotation.coordinate.longitude) ) {
            return;
        }
        if (self.calloutAnnotation) {
            [theMapView removeAnnotation:self.calloutAnnotation];
            self.calloutAnnotation = nil;
        }
        
        CustomAnnotation *tempAnnotation=(CustomAnnotation *)view.annotation;

        self.calloutAnnotation = [[CalloutMapAnnotation alloc] initWithLatitude:view.annotation.coordinate.latitude
                                                                   andLongitude:view.annotation.coordinate.longitude
                                                                  andRestaurant:tempAnnotation.restaurant];

        [theMapView addAnnotation:self.calloutAnnotation];
        [theMapView setCenterCoordinate:self.calloutAnnotation.coordinate animated:YES];
    }
}

- (void)mapView:(MKMapView *)theMapView didDeselectAnnotationView:(MKAnnotationView *)view {
    if (self.calloutAnnotation&& ![view isKindOfClass:[CallOutAnnotationView class]]) {
        if (self.calloutAnnotation.coordinate.latitude == view.annotation.coordinate.latitude&&
            self.calloutAnnotation.coordinate.longitude == view.annotation.coordinate.longitude) {
            [theMapView removeAnnotation:self.calloutAnnotation];
            self.calloutAnnotation = nil;
        }
    }
}



- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation{
    if ([annotation isKindOfClass:[CalloutMapAnnotation class]])
    {
//        CallOutAnnotationView *annotationView = [[CallOutAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"CalloutView" andPostNotification:!self.fromDetail];
        CallOutAnnotationView* annotationView=[[CallOutAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"CalloutView" andPostNotification:!self.fromDetail isFromSearchPage:self.fromSearchPage];
        
        return annotationView;
    }
    else if ([annotation isKindOfClass:[MKPointAnnotation class]]) {
        
        CustomAnnotation *pointAnnotation = (CustomAnnotation *)annotation;
        //IMGRestaurant *arestaurant = pointAnnotation.restaurant;
        MKAnnotationView *annotationView =[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomAnnotation"];
        if (!annotationView) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                           reuseIdentifier:@"CustomAnnotation"];
            annotationView.canShowCallout = NO;
//            if(arestaurant.disCount){
//                annotationView.image = [UIImage imageNamed:@"location.png"];
//            }else if(arestaurant.disCount){
//                annotationView.image = [UIImage imageNamed:@"location-yellow.png"];
//            }else {
//                annotationView.image = [UIImage imageNamed:@"location.png"];
//            }

        }
        annotationView.image = [[UIImage imageNamed:@"ic_white_pin.png"] imageByScalingAndCroppingForSize:CGSizeMake(27, 33)] ;
        if (self.fromDetail) {
            annotationView.image = [[UIImage imageNamed:@"ic_red_pin.png"] imageByScalingAndCroppingForSize:CGSizeMake(27, 33)] ;
        }
        if (_fromSearchPage) {
            
            for (UILabel *label in annotationView.subviews) {
                [label removeFromSuperview];
            }
//            UILabel *numberLabel = [[UILabel alloc] initWithFrame:annotationView.bounds];
//            numberLabel.font = [UIFont boldSystemFontOfSize:11];
//            numberLabel.textAlignment = NSTextAlignmentCenter;
//            [annotationView addSubview:numberLabel];
          
//            for (int i=0; i<restaurantArr.count; i++) {
//                IMGRestaurant *resta=[restaurantArr objectAtIndex:i];
//                
//                if ([resta.restaurantId intValue]==[pointAnnotation.restaurant.restaurantId intValue]) {
//                    //numberLabel.text = [NSString stringWithFormat:@"%d",i+1];
//
//                }
//
//            }
        }
        return annotationView;
    }
    
    return nil;
}

-(void)popBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)gotoDetal:(NSNotification*)notification
{
    if(self.fromDetail){
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)sendManualScreenName:(NSString*)screenName
{
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:screenName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)gotoRestaurantDetal:(NSNotification*)notification{
    NSNumber *restaurantId = [notification.userInfo objectForKey:@"restaurantId"];
    DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:restaurantId];
    if (self.fromLeftMenu) {
        dvc.amplitudeType = @"Main menu";
    }
    if (self.fromHomeCard) {
        dvc.amplitudeType = @"Homepage";
    }
    [self.navigationController pushViewController:dvc animated:YES];
}
- (void)goToBackViewController
{
    if (self.fromLeftMenu) {
        [[AppDelegate ShareApp]goToPage:11];
    }else{
        [self removeNotifications];
        [self.navigationController popViewControllerAnimated:YES];
    }
}
@end
