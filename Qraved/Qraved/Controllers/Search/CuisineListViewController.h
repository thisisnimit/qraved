//
//  CuisineListViewController.h
//  Qraved
//
//  Created by Jeff on 8/22/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "IMGDiscover.h"

@interface CuisineListViewController : BaseChildViewController<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, retain) UISearchBar *searchBar;
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) UIView   *headView;
@property (nonatomic, retain) UIButton *goButton;

@property (nonatomic, retain) NSMutableArray *resultArray;
@property (nonatomic, copy)   NSString *searchStr;
@property(nonatomic,assign)BOOL isfromSearchCategary;


-(id)initWithDiscover:(IMGDiscover *)paramDiscover;


@end
