//
//  SortTypeViewController.m
//  Qraved
//
//  Created by Jeff on 8/18/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "SortTypeListViewController.h"

#import "UIConstants.h"
#import "NotificationConsts.h"

#import "UIColor+Helper.h"
#import "UIViewController+Helper.h"
#import "Helper.h"

#import "SortTypeCell.h"

@interface SortTypeListViewController ()

@end

@implementation SortTypeListViewController{
    UILabel *spaceHolderLabel;
    UILabel *titleLabel;
    UIButton *closeButton;
    
    UITableView *objectTableView;
    NSArray *objectArray;
    IMGSortType *_sortType;
    IMGDiscover *discover;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
-(id)initWithDiscover:(IMGDiscover *)paramDiscover
{
    if (self = [super init]) {
        IMGSortType *sortType = [[IMGSortType alloc]init];
        discover=paramDiscover;
        if(discover!=nil && discover.sortby!=nil){
            sortType.name=discover.sortby;
        }else{
            if(discover==nil){
                discover = [[IMGDiscover alloc]init];
            }
            if(discover.sortby==nil){
                discover.sortby=sortType.name=SORTBY_POPULARITY;
            }
        }
        _sortType = sortType;
    }
    return self;
}
- (void)initSortTypeArray{
    if(objectArray==nil){
        IMGSortType *sortType1=[[IMGSortType alloc] init];
        sortType1.sortTypeId = [NSNumber numberWithInt:1];
        sortType1.name = L(@"Popularity");
        
        IMGSortType *sortType2=[[IMGSortType alloc] init];
        sortType2.sortTypeId = [NSNumber numberWithInt:2];
        sortType2.name = L(@"Latest Promo");
        
        IMGSortType *sortType3=[[IMGSortType alloc] init];
        sortType3.sortTypeId = [NSNumber numberWithInt: 3];
        sortType3.name = L(@"Bookings");
        
        IMGSortType *sortType4=[[IMGSortType alloc] init];
        sortType4.sortTypeId = [NSNumber numberWithInt: 4];
        sortType4.name = L(@"Rating");
        
        IMGSortType *sortType5=[[IMGSortType alloc] init];
        sortType5.sortTypeId = [NSNumber numberWithInt: 5];
        sortType5.name = L(@"Distance");
        
        IMGSortType *sortType6=[[IMGSortType alloc] init];
        sortType6.sortTypeId = [NSNumber numberWithInt: 6];
        sortType6.name = L(@"Relevance");
        
        objectArray = [[NSArray alloc] initWithObjects:sortType6,sortType1,sortType2,sortType3,sortType4,sortType5, nil];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];    
    [self initSortTypeArray];
    
    [self loadTopBar];
    [self loadTableView];
}

- (void)loadTopBar{
    if ([UIDevice isLaterThanIos6]) {
        spaceHolderLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 20)];
        [spaceHolderLabel setBackgroundColor:[UIColor colorC2060A]];
        [self.view addSubview:spaceHolderLabel];
    }
    
    UIFont *titleLabelFont=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:18];
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, spaceHolderLabel.frame.size.height, DeviceWidth, 45)];
    [titleLabel setFont:titleLabelFont];
    [titleLabel setText:L(@"Sort by")];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setBackgroundColor: [UIColor colorC2060A]];
    [self.view addSubview:titleLabel];
    
    closeButton = [[UIButton alloc] initWithFrame:CGRectMake(DeviceWidth-45, spaceHolderLabel.frame.size.height, 45, 45)];
    [closeButton setImage:[UIImage imageNamed:@"closeWhite" ] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeButton];
}

- (void)loadTableView{
    objectTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, spaceHolderLabel.frame.size.height+titleLabel.frame.size.height, DeviceWidth, DeviceHeight) style:UITableViewStylePlain];
    objectTableView.dataSource = self;
    objectTableView.delegate = self;
    [objectTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:objectTableView];
}

-(void)delayClose
{
    [self performSelector:@selector(close) withObject:nil afterDelay:0.3];
}

-(void)close
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

 


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(objectArray!=nil) {
        return [objectArray count];
    }else{
        return 0;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = [[NSString alloc] initWithFormat:@"SortTypeCell"];
    SortTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[SortTypeCell alloc] initWithStyle:UITableViewCellStyleDefault selectionStyle:UITableViewCellSelectionStyleNone reuseIdentifier:identifier];
    
        [cell setText:((IMGSortType *)objectArray[indexPath.row]).name];
        
    }
    IMGSortType *sortType = [objectArray objectAtIndex:indexPath.row];
    if ([sortType.name isEqualToString:@"Latest Promo"]) {
        sortType.name=@"Offers";
    }
    if([sortType.name isEqualToString:discover.sortby]){
        [cell check];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CELL_HEIGHT_45;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    SortTypeCell *cell = (SortTypeCell *)[tableView  cellForRowAtIndexPath:indexPath];
    [cell click];
    if([cell isChecked]){
        for(int i=0;i<[objectArray count];i++){
            if(i!=indexPath.row){
                NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i inSection:indexPath.section];
                SortTypeCell *tmpCell = (SortTypeCell *)[tableView  cellForRowAtIndexPath:tmpIndexPath];
                [tmpCell uncheck];
            }
        }
        _sortType = [objectArray objectAtIndex:indexPath.row];
        [self performSelectorOnMainThread:@selector(createData) withObject:nil waitUntilDone:NO];
        [self close];
    }
//    }else{
//        for(int i=0;i<[objectArray count];i++){
//            if(i!=indexPath.row){
//                NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i inSection:indexPath.section];
//                SortTypeCell *tmpCell = (SortTypeCell *)[tableView  cellForRowAtIndexPath:tmpIndexPath];
//                [tmpCell uncheck];
//            }
//        }
//        _sortType=nil;
//        [self performSelectorOnMainThread:@selector(createData) withObject:nil waitUntilDone:NO];

    
}
-(void)createData
{
    discover.sortby = _sortType.name;
    
    
    NSNotification *notification = [[NSNotification alloc] initWithName:SEARCH_RESTAURANT object:nil userInfo:@{@"discover":discover,@"isSortby":@"YES"}];
    
    if([self.stlvcDelegate respondsToSelector:@selector(search:)])
    {
        [self.stlvcDelegate search:notification];
//        [self dismissViewControllerAnimated:YES completion:nil];
    }

    
//    [[NSNotificationCenter defaultCenter] postNotificationName:SEARCH_RESTAURANT object:nil userInfo:@{@"discover":discover}];
    
}

@end
