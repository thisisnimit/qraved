//
//  BookFactorsSelectionViewController.m
//  Qraved
//
//  Created by Jeff on 8/7/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "BookFactorsSelectionViewController.h"
#import "Label.h"
#import "ValueSelectorView.h"
#import "UIColor+Helper.h"
#import "RadioButton.h"
#import "RestaurantsViewController.h"

#define PAX_MAX 30
#define PAX_MIN 2

#define TITLE_LABEL_HEIGHT 50
#define PAX_SELECTOR_VIEW_HEIGHT 40
#define PAX_ARROW_WIDTH 40
#define PICKER_VIEW_HEIGHT 220
#define PICKER_VIEW__ROW_HEIGHT 30
#define NO_DATE_TIME_BUTTON_HEIGHT 40

@interface BookFactorsSelectionViewController ()


@end

@implementation BookFactorsSelectionViewController {
    Label *paxTitleLabel;
    UIButton *paxArrowLeftButton;
    UIButton *paxArrowRightButton;
    ValueSelectorView *paxSelectorView;
    Label *dateTimeTitleLabel;
    ValueSelectorView *dateSelectorView;
    ValueSelectorView *timeSelectorView;
    NSInteger paxSelectorIndex;

    NSArray *dateArray;
    NSArray *timeArray;
    
    RadioButton *noDateTimeButton;
    
    UIButton *findButton;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = L(@"Find a table");
    self.view.backgroundColor = [UIColor whiteColor];
    
    paxTitleLabel = [[Label alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, TITLE_LABEL_HEIGHT)];
    [paxTitleLabel setText:@"How many people?"];
    [paxTitleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:paxTitleLabel];
    
    paxArrowLeftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, paxTitleLabel.frame.size.height, PAX_ARROW_WIDTH, PAX_SELECTOR_VIEW_HEIGHT)];
    UIImage *paxArrowLeftImage = [UIImage imageNamed:@"arrowleft.png"];
    [paxArrowLeftButton setImage:paxArrowLeftImage forState:UIControlStateNormal];
    [paxArrowLeftButton addTarget:self action:@selector(leftButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:paxArrowLeftButton];
    
    paxArrowRightButton = [[UIButton alloc] initWithFrame:CGRectMake(DeviceWidth-PAX_ARROW_WIDTH, paxTitleLabel.frame.size.height, PAX_ARROW_WIDTH, PAX_SELECTOR_VIEW_HEIGHT)];
    UIImage *paxArrowRightImage = [UIImage imageNamed:@"arrowRightGreen"];
    [paxArrowRightButton setImage:paxArrowRightImage forState:UIControlStateNormal];
    [paxArrowRightButton addTarget:self action:@selector(rightButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:paxArrowRightButton];
    
    paxSelectorView = [[ValueSelectorView alloc] initWithFrame:CGRectMake(PAX_ARROW_WIDTH, paxTitleLabel.frame.size.height, DeviceWidth-PAX_ARROW_WIDTH*2, PAX_SELECTOR_VIEW_HEIGHT)];
    paxSelectorView.shouldBeTransparent = YES;
    paxSelectorView.horizontalScrolling = YES;
    paxSelectorView.dataSource = self;
    paxSelectorView.delegate  =self;
    paxSelectorView.isCanScroll = YES;
    paxSelectorView.tag = 1;

    [self.view addSubview:paxSelectorView];
    
    [paxSelectorView selectRowAtIndex:0];
    
    paxSelectorIndex = 0;
    
    dateTimeTitleLabel = [[Label alloc] initWithFrame:CGRectMake(0, TITLE_LABEL_HEIGHT+PAX_SELECTOR_VIEW_HEIGHT, DeviceWidth, TITLE_LABEL_HEIGHT)];
    [dateTimeTitleLabel setText:@"When would you like to dine?"];
    [dateTimeTitleLabel setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:dateTimeTitleLabel];
    
    [self initDateArray];
    [self initTimeArray];
    
    dateSelectorView = [[ValueSelectorView alloc] initWithFrame:CGRectMake(0, TITLE_LABEL_HEIGHT+PAX_SELECTOR_VIEW_HEIGHT+TITLE_LABEL_HEIGHT, DeviceWidth/2, PICKER_VIEW_HEIGHT)];
    dateSelectorView.delegate=self;
    dateSelectorView.dataSource=self;
    [self.view addSubview:dateSelectorView];
    
    timeSelectorView = [[ValueSelectorView alloc] initWithFrame:CGRectMake(DeviceWidth/2, TITLE_LABEL_HEIGHT+PAX_SELECTOR_VIEW_HEIGHT+TITLE_LABEL_HEIGHT, DeviceWidth/2, PICKER_VIEW_HEIGHT)];
    timeSelectorView.delegate=self;
    timeSelectorView.dataSource=self;
    [self.view addSubview:timeSelectorView];
    
    
    noDateTimeButton = [[RadioButton alloc] initWithDelegate:self groupId:@"none"];
    noDateTimeButton.frame = CGRectMake(60, DeviceHeight-150, DeviceWidth-60, NO_DATE_TIME_BUTTON_HEIGHT);
    [noDateTimeButton setTitle:@"I don't have a specific date" forState:UIControlStateNormal];
    [noDateTimeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [noDateTimeButton.titleLabel setFont:[UIFont boldSystemFontOfSize:13.0f]];
    [self.view addSubview:noDateTimeButton];
    [noDateTimeButton setChecked:YES];

    
    findButton=[[UIButton alloc] init ];
    [findButton setFrame:CGRectMake(0,DeviceHeight-90 , DeviceWidth, 46)];
    [findButton setTitle:@"Find table" forState:UIControlStateNormal];
    [findButton.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
    [findButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [findButton setBackgroundColor:[UIColor colorRed]];
    [findButton addTarget:self action:@selector(find:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:findButton];
}

-(void)find:(id)sender{
    RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc] initWithMap];
    [self.navigationController pushViewController:restaurantsViewController animated:YES];
}

-(void)initDateArray{
    NSMutableArray *tmpDateArray = [NSMutableArray arrayWithCapacity:180] ;

    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE  dd  MMM"];
    
    int seconds = 60*60*24;
    
    NSDate *date = [NSDate date];
    for(int i=0;i<=180;i++){
        NSString *dateString = [dateFormatter stringFromDate:date];
        [tmpDateArray addObject:dateString];
        
        date = [[NSDate alloc] initWithTimeInterval:seconds sinceDate:date];
    }
    dateArray = [NSArray arrayWithArray:tmpDateArray];
}

-(void)initTimeArray{
    timeArray = [[NSArray alloc] initWithObjects:@"00:00",@"00:30",@"01:00",@"01:30",@"02:00",@"02:30",@"03:00",@"03:30",@"04:00",@"04:30",@"05:00",@"05:30",@"06:00",@"06:30",@"07:00",@"07:30",@"08:00",@"08:30",@"09:00",@"09:30",@"10:00",@"10:30",@"11:00",@"11:30",@"12:00",@"12:30",@"13:00",@"13:30",@"14:00",@"14:30",@"15:00",@"15:30",@"16:00",@"16:30",@"17:00",@"17:30",@"18:00",@"18:30",@"19:00",@"19:30",@"20:00",@"20:30",@"21:00",@"21:30",@"22:00",@"22:30",@"23:00",@"23:30",nil];
}

-(IBAction)leftButtonClick:(id)sender{
    if(paxSelectorIndex==0){
        return;
    }else{
        [paxSelectorView selectRowAtIndex:paxSelectorIndex-1];
    }
}
-(IBAction)rightButtonClick:(id)sender{
    if(paxSelectorIndex==PAX_MAX-2){
        return;
    }else{
        [paxSelectorView selectRowAtIndex:paxSelectorIndex+1];
    }
}

- (NSInteger)numberOfRowsInSelector:(ValueSelectorView *)valueSelector {
    return PAX_MAX-1;
}


//ONLY ONE OF THESE WILL GET CALLED (DEPENDING ON the horizontalScrolling property Value)
- (CGFloat)rowWidthInSelector:(ValueSelectorView *)valueSelector {
    if(valueSelector==paxSelectorView){
        return PAX_SELECTOR_VIEW_HEIGHT;
    }else {
        return PICKER_VIEW__ROW_HEIGHT;
    }
}

- (CGFloat)rowHeightInSelector:(ValueSelectorView *)valueSelector {
    if(valueSelector==paxSelectorView){
        return PAX_SELECTOR_VIEW_HEIGHT;
    }else{
        return PICKER_VIEW__ROW_HEIGHT;
    }
}

- (UIView *)selector:(ValueSelectorView *)valueSelector viewForRowAtIndex:(NSInteger)index
{
    return [self selector:valueSelector viewForRowAtIndex:index selected:NO];
}

- (UIView *)selector:(ValueSelectorView *)valueSelector viewForRowAtIndex:(NSInteger)index selected:(BOOL)selected
{
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectZero];
    if(valueSelector == paxSelectorView){
        label.text = [NSString stringWithFormat:@"%02d",(int)(index+2)];
    
        label.backgroundColor = [UIColor clearColor];
        if (selected) {
            label.frame = CGRectMake(0, 0,PAX_SELECTOR_VIEW_HEIGHT, PAX_SELECTOR_VIEW_HEIGHT);
            label.textColor = [UIColor redColor];
        
        } else {
            label.frame = CGRectMake(0, 1,PAX_SELECTOR_VIEW_HEIGHT, PAX_SELECTOR_VIEW_HEIGHT-1);
            label.textColor = [UIColor colorEDEDED];
        }
    }else if(valueSelector==dateSelectorView){
        label.text = dateArray[index];
        
        label.backgroundColor = [UIColor clearColor];
        if (selected) {
            label.frame = CGRectMake(0, 0,DeviceWidth/2, PICKER_VIEW__ROW_HEIGHT);
            label.textColor = [UIColor redColor];
            
        } else {
            label.frame = CGRectMake(0, 1,DeviceWidth/2, PICKER_VIEW__ROW_HEIGHT-1);
            label.textColor = [UIColor colorEDEDED];
        }
        
    }else {
        label.text = timeArray[index];
        
        label.backgroundColor = [UIColor clearColor];
        if (selected) {
            label.frame = CGRectMake(0, 0,DeviceWidth/2, PICKER_VIEW__ROW_HEIGHT);
            label.textColor = [UIColor redColor];
            
        } else {
            label.frame = CGRectMake(0, 1,DeviceWidth/2, PICKER_VIEW__ROW_HEIGHT-1);
            label.textColor = [UIColor colorEDEDED];
        }
        
    }
        label.textAlignment =  NSTextAlignmentCenter;
//    [label sizeToFit];
    return label;
}

- (CGRect)rectForSelectionInSelector:(ValueSelectorView *)valueSelector {
    if(valueSelector==paxSelectorView){
        return CGRectMake( valueSelector.frame.size.width/2 - PAX_SELECTOR_VIEW_HEIGHT/2,0, PAX_SELECTOR_VIEW_HEIGHT, PAX_SELECTOR_VIEW_HEIGHT);
    }else{
        return CGRectMake( 0,valueSelector.frame.size.height/2 - PICKER_VIEW__ROW_HEIGHT/2, DeviceWidth/2, PICKER_VIEW__ROW_HEIGHT);

    }
}

#pragma IZValueSelector delegate
- (void)selector:(ValueSelectorView *)valueSelector didSelectRowAtIndex:(NSInteger)index {
    paxSelectorIndex = index;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
