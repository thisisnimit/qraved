//
//  AreaListViewController.m
//  Qraved
//
//  Created by lucky on 16/9/21.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "AreaListViewController.h"

#import "UIViewController+Helper.h"
#import "UIColor+Helper.h"
#import "UIDevice+Util.h"
#import "AreaCell.h"

#import "IMGArea.h"
#import "NotificationConsts.h"
#import "DBManager.h"

@interface AreaListViewController ()
{
    NSMutableArray *areaArray;
    NSMutableArray * searchResultArray;
    BOOL searchActive;
    IMGDiscover *discover;
}
@end

#define DEFAULT_SEARCH_TEXT @"Find Area"


@implementation AreaListViewController

-(id)initWithDiscover:(IMGDiscover *)paramDiscover{
    self = [super init];
    if(self){
        discover = paramDiscover;
        if(discover==nil){
            discover = [[IMGDiscover alloc]init];
        }
        if(discover.areaArray==nil){
            discover.areaArray = [[NSMutableArray alloc]initWithCapacity:0];
        }
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Area";
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self initData];
    [self setBackBarButtonOffset30];
    [self loadSearchBar];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, self.searchBar.frame.origin.y+self.searchBar.frame.size.height, DeviceWidth,DeviceHeight-90) style: UITableViewStylePlain];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:self.tableView];
}

- (void)loadSearchBar
{
    self.searchStr = DEFAULT_SEARCH_TEXT;
    
    float height = 0;
    
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, height, DeviceWidth, 45)];
    self.searchBar.delegate = self;
    self.searchBar.placeholder = DEFAULT_SEARCH_TEXT;
    [self.view addSubview:self.searchBar];
    if ([UIDevice isLaterThanIos6]) {
        self.searchBar.searchBarStyle = UISearchBarStyleMinimal;
    }else{
        for(UIView *subview in self.searchBar.subviews){
            if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                [subview removeFromSuperview];
            }
            
        }
    }
    self.searchBar.layer.borderColor = [UIColor colorEEEEEE].CGColor;
    self.searchBar.layer.borderWidth = 0.3;
    [self.searchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"search_input-box" ]  forState:UIControlStateNormal];
}

- (void)initData
{
    areaArray = [[NSMutableArray alloc] initWithCapacity:0];
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGArea where cityId=%@ order by name;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]] successBlock:^(FMResultSet *resultSet) {
        while([resultSet next]) {
            IMGArea *area = [[IMGArea alloc]init];
            [area setValueWithResultSet:resultSet];
            [areaArray addObject:area];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"AreaListViewController initData error when get datas:%@",error.description);
    }];
}

#pragma mark- SearchBar Delegate Methods
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if(searchResultArray==nil){
        searchResultArray = [[NSMutableArray alloc]init];
    }else{
        [searchResultArray removeAllObjects];
    }
    searchActive = searchText.length > 0 ? YES : NO;
    if(searchActive){
        for (IMGArea * area in areaArray) {
            NSString * string = area.name;
            NSRange range = [string rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if(range.location!=NSNotFound){
                [searchResultArray addObject:area];
            }
        }
    }
    
    [self.tableView reloadData];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = NO;
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = NO;
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    self.searchStr = [self.searchBar text];
    [searchBar resignFirstResponder];
    [self.tableView reloadData];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}
#pragma mark- Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return searchActive?searchResultArray.count:areaArray.count;
}

- (AreaCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    AreaCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[AreaCell alloc] initWithStyle:UITableViewCellStyleDefault selectionStyle:UITableViewCellSelectionStyleNone reuseIdentifier:CellIdentifier] ;
    }
    IMGArea * area = nil;
    
    if(searchActive){
        area = [searchResultArray objectAtIndex:indexPath.row];
    }else{
        area =  [areaArray objectAtIndex:indexPath.row];
    }
    [cell setText:area.name];
    [cell setChecked:[discover hasArea:area]];
    return cell;
}


#pragma mark- Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray  * selectArray = searchActive ? searchResultArray : areaArray;
    AreaCell *cell = (AreaCell *)[tableView  cellForRowAtIndexPath:indexPath];
    IMGArea * area = [selectArray objectAtIndex:indexPath.row];
    [cell setChecked:![discover hasArea:area]];
    if(![discover hasArea:area]){
        [discover addArea:area];
    }else{
        [discover removeArea:area];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SEARCH_FILTER object:nil userInfo:@{@"discover":discover,@"tempArray":area}];
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            if ([[tableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                return;
            }
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        
        @catch ( NSException *e ) {
            return;
        }
    });
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scroll {
    [self.searchBar resignFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
