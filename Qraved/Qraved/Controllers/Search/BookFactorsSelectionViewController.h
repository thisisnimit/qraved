//
//  BookFactorsSelectionViewController.h
//  Qraved
//
//  Created by Jeff on 8/7/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseChildViewController.h"
#import "Label.h"
#import "ValueSelectorView.h"

@interface BookFactorsSelectionViewController : BaseChildViewController <ValueSelectorViewDataSource,ValueSelectorViewDelegate>

//@property (nonatomic, retain) Label *paxTitleLabel;
//@property (nonatomic, retain) UIButton *paxArrowLeftButton;
//@property (nonatomic, retain) UIButton *paxArrowRightButton;
//@property (nonatomic, retain) ValueSelectorView *paxSelectorView;
//@property (nonatomic, retain) Label *dateTimeTitleLabel;
//@property (nonatomic, retain) ValueSelectorView *dateTimeSelectorView;

@end
