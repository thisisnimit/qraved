//
//  RestaurantSuggestViewController.m
//  Qraved
//
//  Created by Lucky on 15/10/5.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "RestaurantSuggestViewController.h"
#import "IMGUploadPhoto.h"
#import "UIView+Helper.h"
#import "UILabel+Helper.h"
#import "UIDevice+Util.h"
#import "UIConstants.h"
#import "UIViewController+Helper.h"
#import "CalloutMapAnnotation.h"
#import "CallOutAnnotationView.h"
#import "AppDelegate.h"
#import "SuggestHandler.h"
#import "IMGUser.h"
#import "PostDataHandler.h"
#import "LoadingView.h"
#import "SVProgressHUD.h"

@interface RestaurantSuggestViewController ()<CLLocationManagerDelegate,UIScrollViewDelegate>
{
    NSMutableArray *_photosArrM;
    UIScrollView *photosScrollView;
    UIScrollView *scrollView;
    UIView *mapView;
    UITextField *nameTextField;
    UITextField *phoneTextField;
    UITextField *addressTextField;
    UITextField *cityTextField;
    UITextField *districtTextField;
    UITextField *ownerPhoneTextField;
    UIView * bottomLine;
    CGFloat currentY;
    
    MKMapView *map;
    MKPointAnnotation *pointAnnatation;
    UIView *mapGrayView;
    UIImageView *locationImageView;
    CLLocationCoordinate2D centerCoordinate;
    NSDictionary *infoDic;
    NSMutableString *urlsStr;
    NSMutableDictionary *suggestDicM;
    UIButton* selectView;
    UILabel* selectedLabel;
}
@end

@implementation RestaurantSuggestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"Suggest New Place form page";
    _photosArrM = [[NSMutableArray alloc] init];
    urlsStr = [[NSMutableString alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(savePhoto:) name:@"savePhotoFromCameraNotification" object:nil];
    self.view.backgroundColor = [UIColor whiteColor];
    [self addDoneBtn];
    [self setBackBarButtonOffset30];
    [self loadMainView];
    [self addMapView];

}
- (void)addDoneBtn
{
    self.navigationItem.title= L(@"New Place");
    UIBarButtonItem *shareBtn=[[UIBarButtonItem alloc] initWithTitle:L(@"Done") style:UIBarButtonItemStylePlain target:self action:@selector(doneBtnClick)];
    shareBtn.tintColor=[UIColor  colorWithRed:9/255.0f green:191/255.0f blue:211/255.0f alpha:1.0f];
    self.navigationItem.rightBarButtonItem=shareBtn;

}
- (void)doneBtnClick
{
    
    
    if (!nameTextField.text.length)
    {
        [SVProgressHUD showSuccessWithStatus:L(@"Please input restaurant name")];
        return;
    }
    else if (!addressTextField.text.length)
    {
        [SVProgressHUD showSuccessWithStatus:L(@"Please input restaurant address")];
        return;
    }
    
//    [[LoadingView sharedLoadingView] startLoading];
    suggestDicM = [[NSMutableDictionary alloc] init];
    [suggestDicM setObject:nameTextField.text forKey:@"restaurantName"];
    IMGUser *user = [IMGUser currentUser];
    if (user.userId)
    {
        [suggestDicM setObject:user.userId forKey:@"userId"];
    }
    if (phoneTextField.text.length)
    {
        [suggestDicM setObject:phoneTextField.text forKey:@"phoneNumber"];
    }
    if (addressTextField.text.length)
    {
        [suggestDicM setObject:addressTextField.text forKey:@"address"];
    }
    if (districtTextField.text.length)
    {
        [suggestDicM setObject:districtTextField.text forKey:@"districtName"];
    }
    if (cityTextField.text.length)
    {
        [suggestDicM setObject:cityTextField.text forKey:@"cityName"];
    }
    if (ownerPhoneTextField.text.length) {
        [suggestDicM setObject:ownerPhoneTextField.text forKey:@"ownerPhoneNumber"];

    }
    if (infoDic.count)
    {
        [suggestDicM setObject:[infoDic objectForKey:@"latitude"] forKey:@"latitude"];
        [suggestDicM setObject:[infoDic objectForKey:@"longitude"] forKey:@"longitude"];
    }
    if (_photosArrM.count)
    {
       
        [self postImageWithCurrentIndex:0];
    }
    else
    {
        [SuggestHandler suggestWithDic:suggestDicM withSuccess:^(bool success) {
            
            if (success) {
                 NSMutableDictionary* eventDic=[[NSMutableDictionary alloc]init];
                [eventDic setValue:[self getPageIdByAmplitudeType] forKey:@"Origin"];
                [[Amplitude instance] logEvent:@"UC - Suggest New Restaurant Succeed" withEventProperties:eventDic];
            }
            
            
        } withFail:^(bool fail, NSString *erroInfo) {
            if (fail) {
                NSMutableDictionary* eventDic=[[NSMutableDictionary alloc]init];
                [eventDic setValue:[self getPageIdByAmplitudeType] forKey:@"Origin"];
                [eventDic setValue:erroInfo forKey:@"Reason"];

                [[Amplitude instance] logEvent:@"UC - Suggest New Restaurant Failed" withEventProperties:eventDic];
            }
            
            
            
            
            
        }];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    [SVProgressHUD showImage:[UIImage imageNamed:@"success"] status:L(@"Thank you for your contribution, your suggestion has send to our team")];
    [SVProgressHUD dismissWithDelay:1.5];

}
- (void)postImageWithCurrentIndex:(NSInteger)currentIndex
{
    IMGUploadPhoto *photo=[_photosArrM objectAtIndex:currentIndex];
    PostDataHandler *postDataHandler = [[PostDataHandler alloc] init];
    [postDataHandler postImage:photo.image andTitle:@"image" andDescription:@"image" andSuccessBlock:^(id postArray){
        NSString *imageUrl=[postArray objectForKey:@"imgPath"];
        NSLog(@"reviewPublish imageUrl = %@",imageUrl);
        if (urlsStr.length)
        {
            urlsStr = [NSMutableString stringWithFormat:@"%@,%@",urlsStr,imageUrl];
        }
        else
        {
            urlsStr = [NSMutableString stringWithString:imageUrl];
        }
        
        if (currentIndex == _photosArrM.count - 1)
        {
            [suggestDicM setObject:urlsStr forKey:@"imageUrl"];
            [SuggestHandler suggestWithDic:suggestDicM withSuccess:^(bool success) {
                if (success) {
                    NSMutableDictionary* eventDic=[[NSMutableDictionary alloc]init];
                    [eventDic setValue:[self getPageIdByAmplitudeType]forKey:@"Origin"];
                    [[Amplitude instance] logEvent:@"UC - Suggest New Restaurant Succeed" withEventProperties:eventDic];
                }

            } withFail:^(bool fail, NSString *erroInfo) {
                
                if (fail) {
                    NSMutableDictionary* eventDic=[[NSMutableDictionary alloc]init];
                    [eventDic setValue:[self getPageIdByAmplitudeType]forKey:@"Origin"];
                    [eventDic setValue:erroInfo forKey:@"Reason"];

                    [[Amplitude instance] logEvent:@"UC - Suggest New Restaurant Failed" withEventProperties:eventDic];
                }
   
            }];
        }
        else
        {
            [self postImageWithCurrentIndex:currentIndex + 1];
        }
        
    }andFailedBlock:^(NSError *error){
        NSLog(@"upload photo failed, error:%@",error.description);
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}
- (void)loadMainView
{
    scrollView = [[UIScrollView alloc] init];
    scrollView.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight - 84);
    scrollView.delegate = self;
//    UIPanGestureRecognizer* panTap=[[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(panGestureTap:)];
//    [scrollView addGestureRecognizer:panTap];
//    UITapGestureRecognizer* Tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(panGestureTap:)];
//    [scrollView addGestureRecognizer:Tap];

    [self.view addSubview:scrollView];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.backgroundColor = [UIColor colorWithRed:241/255.0 green:243/255.0 blue:238/255.0 alpha:1];
    titleLabel.text = L(@"    Suggest a new place to us!");
    titleLabel.frame = CGRectMake(0, 0, DeviceWidth, 44);
    titleLabel.font = [UIFont systemFontOfSize:13];
    [scrollView addSubview:titleLabel];
    
    nameTextField = [[UITextField alloc] init];
    nameTextField.frame = CGRectMake(LEFTLEFTSET, titleLabel.endPointY, DeviceWidth - LEFTLEFTSET, 44);
    nameTextField.placeholder = L(@"Name");
    nameTextField.delegate = self;
    [scrollView addSubview:nameTextField];
    
    phoneTextField = [[UITextField alloc] init];
    phoneTextField.frame = CGRectMake(LEFTLEFTSET, nameTextField.endPointY, DeviceWidth - LEFTLEFTSET, 44);
    phoneTextField.placeholder = L(@"Phone number");
    phoneTextField.delegate = self;
    phoneTextField.keyboardType = UIKeyboardTypePhonePad;
    [scrollView addSubview:phoneTextField];
    
    addressTextField = [[UITextField alloc] init];
    addressTextField.frame = CGRectMake(LEFTLEFTSET, phoneTextField.endPointY, DeviceWidth - LEFTLEFTSET, 44);
    addressTextField.placeholder = L(@"Address");
    addressTextField.delegate = self;
    [scrollView addSubview:addressTextField];
    
    cityTextField = [[UITextField alloc] init];
    cityTextField.frame = CGRectMake(LEFTLEFTSET, addressTextField.endPointY, (DeviceWidth - LEFTLEFTSET*2)/2, 44);
    cityTextField.placeholder = L(@"City");
    cityTextField.delegate = self;
    [scrollView addSubview:cityTextField];
    
    districtTextField = [[UITextField alloc] init];
    districtTextField.frame = CGRectMake(cityTextField.endPointX, addressTextField.endPointY, cityTextField.frame.size.width, 44);
    districtTextField.placeholder = L(@"District");
    districtTextField.delegate = self;
    [scrollView addSubview:districtTextField];
    

    currentY = districtTextField.endPointY;
    
    photosScrollView = [[UIScrollView alloc] init];
    
    
    UIKeyboardCoView *btnsView = [[UIKeyboardCoView alloc] init];
    btnsView.delegate = self;
    btnsView.backgroundColor = [UIColor colorWithRed:241/255.0 green:241/255.0 blue:241/255.0 alpha:1];
    btnsView.frame = CGRectMake(0, DeviceHeight - 84, DeviceWidth, 44);
    [self.view addSubview:btnsView];
    
    UIButton *addPhotoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [addPhotoBtn setImage:[UIImage imageNamed:@"upload-photo"] forState:UIControlStateNormal];
    addPhotoBtn.frame = CGRectMake(22, btnsView.frame.size.height - 35, 22, 18);
    [btnsView addSubview:addPhotoBtn];
    [addPhotoBtn addTarget:self action:@selector(addPhotoClick) forControlEvents:UIControlEventTouchUpInside];
    
}
-(void)panGestureTap:(UIGestureRecognizer*)tap
{
    [self.view endEditing:YES];


}
- (void)reloadPhotos
{
    CGFloat dishImageWidth;
    CGFloat dishImageEndX;
    for (UIView *view in photosScrollView.subviews)
    {
        [view removeFromSuperview];
    }
    for (int i = 0 ; i<_photosArrM.count; i++)
    {
        UIImageView *dishImageView = [[UIImageView alloc]init];
        
        dishImageWidth = (DeviceWidth-LEFTLEFTSET*4)/2;
        dishImageView.frame = CGRectMake((dishImageWidth+LEFTLEFTSET*2)*i+LEFTLEFTSET, 0, dishImageWidth, dishImageWidth);
        dishImageView.tag = i;
        UITapGestureRecognizer *Tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagePressed:)];
        [Tap setNumberOfTapsRequired:1];
        [Tap setNumberOfTouchesRequired:1];
        dishImageView.userInteractionEnabled=YES;
//        [dishImageView addGestureRecognizer:Tap];
        IMGUploadPhoto *photo=[_photosArrM objectAtIndex:i];

        [photosScrollView addSubview:dishImageView];
        dishImageEndX = dishImageView.endPointX;
        dishImageView.image = photo.image;
        
    }
    
    photosScrollView.contentSize = CGSizeMake(dishImageEndX+LEFTLEFTSET, 0);
    photosScrollView.bounces = NO;
    photosScrollView.showsHorizontalScrollIndicator = NO;
    photosScrollView.frame = CGRectMake(0, cityTextField.endPointY + 8, DeviceWidth, dishImageWidth+10);
    [scrollView addSubview:photosScrollView];
    currentY = photosScrollView.endPointY;
    [self addMapView];
}
- (void)savePhoto:(NSNotification *)notification
{

    IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
    photo.image = [notification.userInfo objectForKey:@"finishImage"];
    [_photosArrM addObject:photo];
    [self dismissViewControllerAnimated:YES completion:nil];
    [self reloadPhotos];
}
- (void)addMapView
{
    
    if (![AppDelegate ShareApp].locationManager.location.coordinate.latitude)
    {
        return;
    }
    
    if (mapView)
    {
        [mapView setFrame:CGRectMake(0,currentY, DeviceWidth, mapView.frame.size.height)];
    }
    else
    {
        mapView = [[UIView alloc] init];
        [scrollView addSubview:mapView];
        
        UILabel *locationLabel = [[UILabel alloc] init];
        locationLabel.text = L(@"Adjust Location");
        locationLabel.frame = CGRectMake(LEFTLEFTSET, LEFTLEFTSET, DeviceWidth, 44);
        locationLabel.textAlignment = NSTextAlignmentLeft;
        [mapView addSubview:locationLabel];
        
        
        map=[[MKMapView alloc]initWithFrame:CGRectMake(0, locationLabel.endPointY, DeviceWidth, 280)];
        
        [map setMapType:MKMapTypeStandard];
        [map setDelegate:self];
        [mapView addSubview:map];
        CLLocationCoordinate2D coor;
        coor.latitude = [AppDelegate ShareApp].locationManager.location.coordinate.latitude;
        coor.longitude = [AppDelegate ShareApp].locationManager.location.coordinate.longitude;
        CLLocationCoordinate2D theCoordinate =CLLocationCoordinate2DMake(coor.latitude, coor.longitude);
        
        MKCoordinateSpan  theSpan;
        theSpan.latitudeDelta=0.01;
        theSpan.longitudeDelta=0.01;
        
        MKCoordinateRegion theRegion;
        theRegion.center=theCoordinate;
        theRegion.span=theSpan;
        
        
        pointAnnatation= [[MKPointAnnotation alloc] init];
        
        if(CLLocationCoordinate2DIsValid(theCoordinate))
        {
            [map setRegion:theRegion animated:NO];
            
            [map regionThatFits:theRegion];
            
            pointAnnatation.coordinate=theCoordinate;
            
            //[map addAnnotation:pointAnnatation];
        }
        
        
        [mapView setFrame:CGRectMake(0,currentY, DeviceWidth, map.endPointY)];
        
        //UITapGestureRecognizer *mTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPress:)];
        //[map addGestureRecognizer:mTap];
        
        [scrollView addSubview:mapView];
    }
    [self addlocationImageView];
    if (!selectView&&!selectedLabel) {
        selectView=[[UIButton alloc]initWithFrame:CGRectZero];
        [selectView setImage:[UIImage imageNamed:@"checkbox1"] forState:UIControlStateNormal];
        [selectView setImage:[UIImage imageNamed:@"checkbox2"] forState:UIControlStateSelected];
        [selectView addTarget:self action:@selector(seletedBtn:) forControlEvents:UIControlEventTouchUpInside];
        [scrollView addSubview:selectView];
        selectedLabel=[[UILabel alloc]initWithFrame:CGRectZero];
        selectedLabel.text=@"I'm the owner";
        selectedLabel.userInteractionEnabled=YES;
        UITapGestureRecognizer* gestureTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
        [selectedLabel addGestureRecognizer:gestureTap];
        
        [scrollView addSubview:selectedLabel];

    }
    selectView.frame=CGRectMake(15, mapView.endPointY+10, 20, 20);
    selectedLabel.frame=CGRectMake(selectView.endPointX+10, mapView.endPointY+10, 200, 20);
    ownerPhoneTextField.frame = CGRectMake(LEFTLEFTSET, selectView.endPointY, DeviceWidth - LEFTLEFTSET-30, 44);
    scrollView.contentSize = CGSizeMake(DeviceWidth, selectView.endPointY+50);
}
-(void)seletedBtn:(UIButton*)btn{

    if (!btn.selected) {
        if (ownerPhoneTextField) {
            [ownerPhoneTextField removeFromSuperview];
            [bottomLine removeFromSuperview];
        }
        ownerPhoneTextField = [[UITextField alloc] init];
        ownerPhoneTextField.frame = CGRectMake(LEFTLEFTSET, selectView.endPointY, DeviceWidth - LEFTLEFTSET-30, 44);
        ownerPhoneTextField.hidden=NO;
        ownerPhoneTextField.placeholder = L(@"Please leave you phone number");
        ownerPhoneTextField.delegate = self;
        ownerPhoneTextField.tag=100;
        [scrollView addSubview:ownerPhoneTextField];
        bottomLine=[[UIView alloc]initWithFrame:CGRectMake(0, 38, DeviceWidth - LEFTLEFTSET-60, 1)];
        bottomLine.backgroundColor=[UIColor grayColor];
        [ownerPhoneTextField addSubview:bottomLine];

    }else{
    
        ownerPhoneTextField.hidden=YES;
    
    }
 
    btn.selected=!btn.selected;

}
-(void)tap:(UIGestureRecognizer*)tap{

    [self seletedBtn:selectView];

}
- (void)addlocationImageView
{
    if (locationImageView)
    {
        locationImageView.frame = CGRectMake(map.center.x, 280/2, 28, 39);
    }
    else
    {
        locationImageView = [[UIImageView alloc] init];
        locationImageView.image = [UIImage imageNamed:@"location"];
        locationImageView.frame = CGRectMake(map.center.x, 280/2, 28, 39);
        [map addSubview:locationImageView];
    }
}
- (void)addPhotoClick
{
    ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] initWithRestaurantTitle:nil andIsFromUploadPhoto:NO andIsUploadMenuPhoto:NO andCurrentRestaurantId:[NSNumber numberWithInt:0]];
    picker.publishViewController = self;
//    if (!_photosArrM || _photosArrM.count == 0)
//    {
//        picker.maximumNumberOfSelection = 9;
//    }
//    else
//        picker.maximumNumberOfSelection = 9 - _photosArrM.count;
    
    picker.assetsFilter = [ALAssetsFilter allPhotos];
    picker.showEmptyGroups=NO;
    picker.delegate = self;
    picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
            NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
            return duration >= 5;
        } else {
            return YES;
        }
    }];
    //    [self.navigationController pushViewController:picker animated:YES];
    
    [self presentViewController:picker animated:YES completion:NULL];

}

#pragma mark - ZYQAssetPickerController Delegate
-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    for (int i=0; i<assets.count; i++) {
        ALAsset *asset=assets[i];
        UIImageView *imgview=[[UIImageView alloc] init];
        imgview.contentMode=UIViewContentModeScaleAspectFill;
        imgview.clipsToBounds=YES;
        UIImage *tempImg=[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage];
        // [imgview setImage:tempImg];
        IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
        photo.image = tempImg;
        [_photosArrM addObject:photo];
    }
    [self reloadPhotos];
    [picker dismissViewControllerAnimated:YES completion:nil];

}


#pragma mark - Rotation control
- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [[NSNotificationCenter defaultCenter] postNotificationName:UIKeyboardCoViewWillRotateNotification object:nil];
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [[NSNotificationCenter defaultCenter] postNotificationName:UIKeyboardCoViewDidRotateNotification object:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return YES;
}

#pragma mark - UI Keyboard Co View Delegate
- (void) keyboardCoViewWillAppear:(UIKeyboardCoView*)keyboardCoView{
    NSLog(@"Keyboard Co View Will Appear");
}

- (void) keyboardCoViewDidAppear:(UIKeyboardCoView*)keyboardCoView{
    NSLog(@"Keyboard Co View Did Appear");
}

- (void) keyboardCoViewWillDisappear:(UIKeyboardCoView*)keyboardCoView{
    NSLog(@"Keyboard Co View Will Disappear");
}


- (void) keyboardCoViewDidDisappear:(UIKeyboardCoView*)keyboardCoView{
    NSLog(@"Keyboard Co View Did Disappear");
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (textField.tag==100) {
        scrollView.transform=CGAffineTransformMakeTranslation(0, -300);
            }



}



- (void)mapView:(MKMapView *)mapView_ regionDidChangeAnimated:(BOOL)animated {
    MKCoordinateRegion region;
    centerCoordinate = mapView_.region.center;
    region.center= centerCoordinate;
    
    NSLog(@" regionDidChangeAnimated %f,%f",centerCoordinate.latitude, centerCoordinate.longitude);
    infoDic = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithFloat:centerCoordinate.latitude],@"latitude",[NSNumber numberWithFloat:centerCoordinate.longitude],@"longitude", nil];
}

- (void)scrollViewDidScroll:(UIScrollView *)scroll
{
    [nameTextField resignFirstResponder];
    [phoneTextField resignFirstResponder];
    [addressTextField resignFirstResponder];
    [cityTextField resignFirstResponder];
    [districtTextField resignFirstResponder];
    [ownerPhoneTextField resignFirstResponder];
    scrollView.transform=CGAffineTransformIdentity;
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:[self getPageIdByAmplitudeType] forKey:@"Origin"];
    [[Amplitude instance] logEvent:@"UC - Suggest New Restaurant Cancel" withEventProperties:eventProperties];
}
 
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(NSString*)getPageIdByAmplitudeType{
    NSString *pageId = @"";
    if ([@"Hot Key" isEqualToString:self.amplitudeType]) {
        pageId = @",Homepage";
    }
    return [NSString stringWithFormat:@"%@%@",self.amplitudeType,pageId];
}
@end
