//
//  LocationFilteredListViewController.h
//  Qraved
//
//  Created by Jeff on 8/5/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationFilteredListController.h"
#import "BaseChildViewController.h"

@interface LocationFilteredListViewController : BaseChildViewController <UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate, PassValueDelegate>

@property (nonatomic, retain) UISearchBar *searchBar;
@property (nonatomic, retain) UITableView *locationTableView;
@property (nonatomic, retain) UIView   *headView;
@property (nonatomic, retain) UIButton *goButton;
@property (nonatomic, retain) LocationFilteredListController				 *locationFilteredListController;
@property (nonatomic, retain) NSMutableArray		 *resultArray;
@property (nonatomic, copy) NSString			 *searchStr;

- (void)setFilterListHidden:(BOOL)hidden;

@end

