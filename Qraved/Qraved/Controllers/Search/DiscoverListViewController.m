//
//  SearchViewController.m
//  Qraved
//
//  Created by Jeff on 8/8/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "DiscoverListViewController.h"

#import "UIConstants.h"
#import "RestaurantHandler.h"
#import "PaxBookTimePickerViewController.h"
#import "RestaurantsViewController.h"
#import "LocationListViewController.h"

#import "UIColor+Helper.h"
#import "UIViewController+Helper.h"
#import "UIDevice+Util.h"
#import "Label.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"
#import "LoadingImageView.h"

#import "SearchCellLabel.h"
#import "SearchDiscoverCategoryCell.h"
#import "LocationAndRestaurantCellTableViewCell.h"
#import "DetailViewController.h"
#import "SearchScrolView.h"
//#import "MixpanelHelper.h"
#import "IMGDistrictAndLandmark.h"
#import "IMGRestaurant.h"
#import "DBManager.h"
#import "DistrictHandler.h"
#import "DiscoverUtil.h"
#import "SearchUtil.h"
#import "LoadingView.h"
#import "UILabel+Helper.h"
#import "JournalHandler.h"
#import "IMGJournalCategory.h"
#import "JournalListViewController.h"
#import "WebViewController.h"
#import "JournalDetailViewController.h"
#import "JournalSuggestViewController.h"
#import "RestaurantSuggestViewController.h"
#import "SearchJournalTableViewCell.h"
#import "TrackHandler.h"
#import "IMGUser.h"
#import "SimpleLoadingImageView.h"
#import "AppDelegate.h"
#import "SearchCategryHandler.h"
#import "RestaurantsViewController.h"
#import "DiningGuideRestaurantsViewController.h"
#import "DiningGuideListViewController.h"
#import "CuisineListViewController.h"
#import "IMGTagUpdate.h"
#import "TagListViewController.h"
#define SEARCHBAR_WIDTH 300
#define SEARCHBAR_HEIGHT 40


#define KEY_LOCATION @"location"
#define KEY_RESTAURANT @"restaurant"
#define KEY_CUISINE_TYPE @"cuisine"
#define KEY_FOOD_TYPE @"food"

@interface DiscoverListViewController ()<SearchScrolViewDelegate>

@end

@implementation DiscoverListViewController{
    UISearchBar *locationSearchBar;
    UIButton *nearbyButton;
    UIImage *nearbyButtonImage;
    
    UIButton *bestOffersButton;
    UIImage *bestOffersButtonImage;
    
    UIButton *mostPopularButton;
    UIImage *mostPopularButtonImage;
    
    UITableView *searchDiscoverCategoryTableView;
    
    UIView *discoverCategoryView;
    
    UITableView *searchResultsTableView;
    
    NSMutableDictionary *filterResultsDictionary;
    
    NSArray *lastDistrictArray;
    NSArray *lastRestaurantArray;
    NSArray *lastCuisineTypeArray;
    NSArray *lastFoodTypeArray;
    NSString *lastSearchBarText;
    BOOL isUseLastSearchBarText;
    
    NSArray *lastSearchJournalArr;
    NSString *lastSearchJournalBarText;
    BOOL isUseLastSearchJournalBarText;
    
    NSMutableArray *_discoverArray;
    NSMutableArray *_journalArray;
    
    SimpleLoadingImageView *loadingImageView;
    
    UIView *noResultHeaderView;
    UIView *resultHeaderView;
    UIView *noJournalResultHeaderView;
    
    UIButton *restaurantBtn;
    UIButton *journalBtn;
    
    NSArray *_tempDataArr;
    NSArray *_searchJournalArr;
    
    UIView *_isBtnImageView;
    
    NSTimeInterval beginLoadingData;
    NSTimeInterval finishUI;
    BOOL isViewDidLoad;
    BOOL isFreshing;
    NSString *searchBarText;
    UIScrollView *searchAllCategoryScrol;
    BOOL isInitDiscoverIng;
    NSArray *cusineArr;
    NSArray *landmarkArr;
    NSArray *districtArr;
    NSArray *diningguideArr;
    NSArray *topStrs;
    NSArray *topimages;
    UIView *line;
    SearchScrolView *cuisineScrol;
    SearchScrolView *diningGuidesScrol;
    SearchScrolView *mallsScrol;
    SearchScrolView *districtScrol;
    CGFloat mallScrolY;
    NSMutableArray *draftRestaurantArray;
    NSMutableArray *draftJournalArray;

}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    isViewDidLoad = YES;
    showCloseButton=NO;
    draftRestaurantArray = [NSMutableArray array];
    draftJournalArray = [NSMutableArray array];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"hasRecentRestaurant"])
    {
        draftRestaurantArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"searchRestaurant"]];
    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"hasRecentJournal"])
    {
        draftJournalArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"searchJournal"]];
    }

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadRefeshData) name:CITY_SELECT_NOTIFICATION object:nil];
    _discoverArray = [[NSMutableArray alloc]init];
    _journalArray = [[NSMutableArray alloc] init];
    _searchJournalArr = [[NSMutableArray alloc] init];
    _tempDataArr = [[NSArray alloc] init];
    filterResultsDictionary = [[NSMutableDictionary alloc] initWithCapacity:3];
    [self loadSearchBar];
    [self loadButtonView];
    [self loadDiscoverCategoryView];
    [self loadtopview];
    [self loadRefeshData];

    [self loadSearchResultsView];
    [searchResultsTableView setHidden:true];
    [self loadLoadingView];
    if (self.isSearchJournal)
    {
        self.screenName = @"Search journal form page";
        [self initJournals];
    }
    else
    {
        self.screenName = @"Search restaurant form page";
        [self initDiscovers];
    }
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(clickTabMenuRefreshPage) name:NOTIFICATION_REFRESH_TAB_SEARCH object:nil];
    
}
-(void)loadRefeshData{
      NSDictionary *pargram=@{@"cityId":@"1"};
    [[LoadingView sharedLoadingView] startLoading];
    [SearchCategryHandler getSearchAllCategryDataFromeService:pargram andSuccessBlock:^(NSArray *cusinearr, NSArray *landmark, NSArray *district, NSArray *dininguide) {
        [[LoadingView sharedLoadingView] stopLoading];
        
        cusineArr=[[NSArray alloc]initWithArray:cusinearr];
        landmarkArr=[[NSArray alloc]initWithArray:landmark];
        districtArr=[[NSArray alloc]initWithArray:district];
        diningguideArr=[[NSArray alloc]initWithArray:dininguide];
        [self loadSearchAllCategoryView];
    } anderrorBlock:^(NSString *str) {
        NSLog(@"search all categrate:%@",str);
    }];



}
-(void)loadtopview{

    topimages=@[@"nearby_circle",@"popular_circle",@"promo",@"choice",@"lunch",@"dinner",@"booking",@"ic_deliv"];
    topStrs=@[@"Nearby",@"Most Popular",@"Promo",@"Qraved's Choice",@"Lunch",@"Dinner",@"Bookings",@" Delivery"];
    searchAllCategoryScrol=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-80)];
    searchAllCategoryScrol.delegate=self;
    searchAllCategoryScrol.showsVerticalScrollIndicator=NO;
    searchAllCategoryScrol.backgroundColor=[UIColor whiteColor];
    [self.view addSubview:searchAllCategoryScrol];
    
    CGFloat wight=(DeviceWidth-30-60)/4;
    UIView *topCategoryView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, wight*2+10+50)];
    [searchAllCategoryScrol addSubview:topCategoryView];
    CGFloat currenty;
    CGFloat cury;
    
    for (int i=0; i<8; i++) {
        
        
        if (i<4) {
            UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(15+(wight+20)*i, 10, wight, wight)];
            imageView.tag=i+100;
            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(topTapGesture:)];
            imageView.userInteractionEnabled=YES;
            [imageView addGestureRecognizer:tap];
            imageView.layer.cornerRadius=wight/2;
            imageView.image=[UIImage imageNamed:topimages[i]];
            UILabel *lable=[[UILabel alloc]initWithFrame:CGRectMake(imageView.startPointX, imageView.endPointY+5, wight+10, 43)];
            lable.font=[UIFont systemFontOfSize:17];
            lable.textAlignment=NSTextAlignmentCenter;
            lable.textColor=[UIColor colorWithRed:117.0/255.0 green:117.0/255.0 blue:117.0/255.0 alpha:1];
            if ([UIDevice isIphone5]||[UIDevice isIphone4Now]) {
                lable.frame=CGRectMake(imageView.startPointX, imageView.endPointY+5, wight+10, 40);
                lable.font=[UIFont systemFontOfSize:15];
            }
            lable.text=topStrs[i];
            lable.numberOfLines=2;
            CGSize size =[lable.text sizeWithFont:lable.font constrainedToSize:CGSizeMake(wight+10, 100) lineBreakMode:NSLineBreakByWordWrapping];
            lable.frame=CGRectMake(imageView.startPointX-1, imageView.endPointY+5, wight+4, size.height);
//            [lable sizeToFit];
            currenty=lable.endPointY;
            [topCategoryView addSubview:imageView];
            [topCategoryView addSubview:lable];
            
        }else{
            
            UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(15+(wight+20)*(i%4), currenty+10, wight, wight)];
            imageView.layer.cornerRadius=wight/2;
            imageView.image=[UIImage imageNamed:topimages[i]];
            imageView.tag=i+100;
            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(topTapGesture:)];
            imageView.userInteractionEnabled=YES;
            [imageView addGestureRecognizer:tap];
            
            UILabel *lable=[[UILabel alloc]initWithFrame:CGRectMake(imageView.startPointX, imageView.endPointY+5, wight+10, 20)];
            lable.text=topStrs[i];
            lable.font=[UIFont systemFontOfSize:17];
            lable.textColor=[UIColor colorWithRed:117.0/255.0 green:117.0/255.0 blue:117.0/255.0 alpha:1];
            if ([UIDevice isIphone5]||[UIDevice isIphone4Now]) {
                lable.font=[UIFont systemFontOfSize:15];
            }

            lable.numberOfLines=2;
            CGSize size =[lable.text sizeWithFont:lable.font constrainedToSize:CGSizeMake(wight+10, 100) lineBreakMode:NSLineBreakByWordWrapping];
            lable.frame=CGRectMake(imageView.startPointX, imageView.endPointY+5, wight+6, size.height);
//            [lable sizeToFit];
            lable.textAlignment=NSTextAlignmentCenter;
            [topCategoryView addSubview:imageView];
            [topCategoryView addSubview:lable];
            cury=lable.endPointY;
        }
        
    }
    topCategoryView.frame=CGRectMake(0, 0, DeviceWidth, cury+10);
    line=[[UIView alloc]initWithFrame:CGRectMake(0, topCategoryView.endPointY+15, DeviceWidth, 1)];
    line.backgroundColor=[UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1];
    [searchAllCategoryScrol addSubview:line];


}
-(void)loadSearchAllCategoryView{
  //cuisineScrol
 
    if (cuisineScrol) {
        [cuisineScrol removeFromSuperview];
    }
    if (cusineArr.count>0) {
        cuisineScrol=[[SearchScrolView alloc]initWithFrame:CGRectMake(0, line.endPointY+10, DeviceWidth, 16) andtype:CuisineType withDataArr:cusineArr withImageHeight:110];
        cuisineScrol.deleage=self;
        [searchAllCategoryScrol addSubview:cuisineScrol];
        cuisineScrol.frame=CGRectMake(0, line.endPointY, DeviceWidth, cuisineScrol.scrolViewHeight+5);
        searchAllCategoryScrol.contentSize=CGSizeMake(DeviceWidth, cuisineScrol.endPointY+10);
 
    }
//diningGuidesScrol
    if (diningGuidesScrol) {
        [diningGuidesScrol removeFromSuperview];
        diningGuidesScrol=nil;
    }
    if (diningguideArr.count>0) {
        diningGuidesScrol=[[SearchScrolView alloc]initWithFrame:CGRectMake(0, cuisineScrol.endPointY+10, DeviceWidth, 16) andtype:DiningGuideType withDataArr:diningguideArr withImageHeight:210];
        diningGuidesScrol.deleage=self;
        [searchAllCategoryScrol addSubview:diningGuidesScrol];
        diningGuidesScrol.frame=CGRectMake(0, cuisineScrol.endPointY+3, DeviceWidth, diningGuidesScrol.scrolViewHeight+5);
        searchAllCategoryScrol.contentSize=CGSizeMake(DeviceWidth, diningGuidesScrol.endPointY+10);

    }
    
//mallsScrol
    if (mallsScrol) {
        [mallsScrol removeFromSuperview];
    }

    if (diningGuidesScrol==nil) {
        mallScrolY=cuisineScrol.endPointY;
    }else{
        mallScrolY= diningGuidesScrol.endPointY;
    
    }
    if (landmarkArr.count>0) {
        mallsScrol=[[SearchScrolView alloc] initWithFrame:CGRectMake(0,mallScrolY+10, DeviceWidth, 16) andtype:LandMarkType withDataArr:landmarkArr withImageHeight:110];
        mallsScrol.deleage=self;
        [searchAllCategoryScrol addSubview:mallsScrol];
        mallsScrol.frame=CGRectMake(0,mallScrolY+3, DeviceWidth, mallsScrol.scrolViewHeight+5);
        searchAllCategoryScrol.contentSize=CGSizeMake(DeviceWidth, mallsScrol.endPointY+10);
 
    }
  
//districtScrol
    if (districtScrol) {
        [districtScrol removeFromSuperview];
    }
    if (districtArr.count>0) {
        districtScrol=[[SearchScrolView alloc]initWithFrame:CGRectMake(0, mallsScrol.endPointY+10, DeviceWidth, 16) andtype:DistrictType withDataArr:districtArr withImageHeight:110];
        districtScrol.deleage=self;
        [searchAllCategoryScrol addSubview:districtScrol];
        districtScrol.frame=CGRectMake(0, mallsScrol.endPointY+3, DeviceWidth, districtScrol.scrolViewHeight+5);
        searchAllCategoryScrol.contentSize=CGSizeMake(DeviceWidth, districtScrol.endPointY+20);
  
    }
}
-(void)gotoShowAll:(SearchType)type{


    switch (type) {
        case CuisineType:
        {
            CuisineListViewController *cuisineListViewController = [[CuisineListViewController alloc]initWithDiscover:nil];
            cuisineListViewController.isfromSearchCategary=YES;
            [self.navigationController pushViewController:cuisineListViewController animated:YES];

        }
            break;
        case LandMarkType:
        {
            NSMutableArray *arr=[[NSMutableArray alloc]init];
            [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"select * from IMGTagUpdate where type='Landmark/ Mall';"] successBlock:^(FMResultSet *resultSet) {
                while([resultSet next]) {
                    IMGTagUpdate *tag = [[IMGTagUpdate alloc] init];
                    [tag setValueWithResultSet:resultSet];
                    [arr addObject:tag];
                }
                [resultSet close];
            }failureBlock:^(NSError *error) {
                NSLog(@"FiltersViewController loadButtonGridView error when get datas:%@",error.description);
            }];
            if (arr!=nil&&arr.count>0) {
                IMGTagUpdate *tagUpdate=[arr objectAtIndex:0];
                TagListViewController *tagListViewController = [[TagListViewController alloc]initWithDiscover:nil andTagUpdate:tagUpdate];
                tagListViewController.isfromSearchCategary=YES;
                [self.navigationController pushViewController:tagListViewController animated:YES];

            }
            
            
            
        }
            break;
        case DistrictType:
        {
            LocationListViewController *locationListViewController = [[LocationListViewController alloc]init];
            locationListViewController.isFromSearch=YES;
            [self.navigationController pushViewController:locationListViewController animated:YES];

        }
            break;

            
        default:
            break;
    }




}
-(void)gotoPage:(SearchType)type andIndex:(NSInteger)index{


    switch (type) {
        case CuisineType:{
            NSLog(@"click cuisine %ld",(long)index);
            
            IMGDiscover *discover=[[IMGDiscover alloc]init];
            IMGCuisine *cuisine=[cusineArr objectAtIndex:index];
            discover.cuisineArray=[NSMutableArray arrayWithObjects:cuisine, nil];
            discover.cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
            RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc] initWithMap];
            [restaurantsViewController initDiscover:discover];
//            [self.navigationController pushViewController:restaurantsViewController animated:NO];
            [[AppDelegate ShareApp].discoverListNavigationController pushViewController:restaurantsViewController animated:NO];
            [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 1;
            [AppDelegate ShareApp].selectedNavigationController =[AppDelegate ShareApp].discoverListNavigationController;

        }
            break;
        case DiningGuideType:{
            
            IMGDiningGuide *diningGuide=[diningguideArr objectAtIndex:index];
            DiningGuideRestaurantsViewController *diningResList =[[DiningGuideRestaurantsViewController alloc]initWithDiningGuide:diningGuide];
            diningResList.isFromSearchDininguide=YES;
            [self.navigationController pushViewController:diningResList animated:YES];

            
            
        }
            break;
        case LandMarkType:{
            
            IMGDiscover *discover=[[IMGDiscover alloc]init];
            IMGTag *tag=[landmarkArr objectAtIndex:index];
            discover.landmarkArray=[NSMutableArray arrayWithObjects:tag, nil];
            discover.cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
            RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc] initWithMap];
            [restaurantsViewController initDiscover:discover];
//            [self.navigationController pushViewController:restaurantsViewController animated:NO];
            [[AppDelegate ShareApp].discoverListNavigationController pushViewController:restaurantsViewController animated:NO];
            [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 1;
            [AppDelegate ShareApp].selectedNavigationController =[AppDelegate ShareApp].discoverListNavigationController;

        }
            break;
        case DistrictType:{
            IMGDiscover *discover=[[IMGDiscover alloc]init];
            IMGDistrict *district=[districtArr objectAtIndex:index];
            discover.searchDistrictArray=[NSMutableArray arrayWithObjects:district, nil];
            discover.districtArray=[NSMutableArray arrayWithObjects:district, nil];
            discover.cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
            RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc] initWithMap];
            [restaurantsViewController initDiscover:discover];
//            [self.navigationController pushViewController:restaurantsViewController animated:NO];
            [[AppDelegate ShareApp].discoverListNavigationController pushViewController:restaurantsViewController animated:NO];
            [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 1;
            [AppDelegate ShareApp].selectedNavigationController =[AppDelegate ShareApp].discoverListNavigationController;

            
        }
            break;

            
        default:
            break;
    }


}
-(void)topTapGesture:(UITapGestureRecognizer*)tap{
    UIView *view=tap.view;
    switch (view.tag) {
        case 100:
        {
            [self goToNearby];
        }
            break;
        case 101:
        {
            [self goToPopular];
        }
            break;
        case 102:
        {
            [self gotoPromo];
        }
            break;
        case 103:
        {
            [self gotoDiningGuide];
        }
            break;
        case 104:
        {
            [self gotoLunch];
        }
            break;
        case 105:
        {
            [self gotoDinner];
        }
            break;
        case 106:
        {
            [self gotoBooking];
        }
            break;
        case 107:
        {
            [self gotoDelivery];
        }
            break;
        default:
            break;
    }
}
-(void)gotoPromo{

    
    IMGDiscover *discover=[[IMGDiscover alloc]init];

    NSMutableArray *arr=[[NSMutableArray alloc]init];
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"select * from IMGOfferType order by typeId asc,off desc ;"] successBlock:^(FMResultSet *resultSet) {
        while([resultSet next]) {
            IMGOfferType *tag = [[IMGOfferType alloc] init];
            [tag setValueWithResultSet:resultSet];
            [arr addObject:tag];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"FiltersViewController loadButtonGridView error when get datas:%@",error.description);
    }];
    NSMutableArray *offArr=[[NSMutableArray alloc]init];
    NSMutableArray *offerTpyeArr=[[NSMutableArray alloc]init];

    if (arr!=nil&&arr.count>0) {
        for (IMGOfferType *tag in arr) {
            
            if ([tag.typeId intValue]==0) {
                [offArr addObject:tag];
            }else{
                
                [offerTpyeArr addObject:tag];
            }
        }
        
    }
    
    discover.gotoWhere=Ispromo;
    discover.offArray=offArr;
    discover.offerTypeArray=offerTpyeArr;
    discover.cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc] initWithMap];
    restaurantsViewController.isPromo = YES;
    [restaurantsViewController initDiscover:discover];
//    [self.navigationController pushViewController:restaurantsViewController animated:NO];
    [[AppDelegate ShareApp].discoverListNavigationController pushViewController:restaurantsViewController animated:NO];
    [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 1;
    [AppDelegate ShareApp].selectedNavigationController =[AppDelegate ShareApp].discoverListNavigationController;
}
-(void)gotoDelivery{

    IMGDiscover *discover=[[IMGDiscover alloc]init];
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"select * from IMGTag where type='Features' and name='Delivery Service' and cityId=%@;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]] successBlock:^(FMResultSet *resultSet) {
        while([resultSet next]) {
            IMGTag *tag = [[IMGTag alloc] init];
            [tag setValueWithResultSet:resultSet];
            [arr addObject:tag];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"FiltersViewController loadButtonGridView error when get datas:%@",error.description);
    }];
    discover.tagsArr=arr;
    discover.gotoWhere=IsDelivery;
    discover.cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc] initWithMap];
    [restaurantsViewController initDiscover:discover];
//    [self.navigationController pushViewController:restaurantsViewController animated:NO];
    [[AppDelegate ShareApp].discoverListNavigationController pushViewController:restaurantsViewController animated:NO];
    [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 1;
    [AppDelegate ShareApp].selectedNavigationController =[AppDelegate ShareApp].discoverListNavigationController;

}
-(void)goToNearby{
    IMGDiscover *discover=[[IMGDiscover alloc]init];
    discover.sortby=SORTBY_POPULARITY;
    discover.gotoWhere=IsNearby;
    discover.fromWhere=[NSNumber numberWithInt:1];
    discover.cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc] initWithMap];
    restaurantsViewController.isNearBy = YES;
    restaurantsViewController.isfromSearchNearby=YES;
    [restaurantsViewController initDiscover:discover];
//    [self.navigationController pushViewController:restaurantsViewController animated:NO];
    [[AppDelegate ShareApp].discoverListNavigationController pushViewController:restaurantsViewController animated:NO];
    [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 1;
    [AppDelegate ShareApp].selectedNavigationController =[AppDelegate ShareApp].discoverListNavigationController;

}
-(void)gotoLunch{

    IMGDiscover *discover=[[IMGDiscover alloc]init];
    NSMutableArray *arr=[[NSMutableArray alloc]init];
     [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGTagUpdate where type='Landmark/ Mall';"] successBlock:^(FMResultSet *resultSet) {
      while([resultSet next]) {
          IMGTagUpdate *tag = [[IMGTagUpdate alloc] init];
          [tag setValueWithResultSet:resultSet];
          [arr addObject:tag];
      }
         [resultSet close];
     }failureBlock:^(NSError *error) {
         NSLog(@"FiltersViewController loadButtonGridView error when get datas:%@",error.description);
     }];
    IMGPriceLevel *priceLevel1 = [[IMGPriceLevel alloc] init];
    priceLevel1.priceLevelId = [NSNumber numberWithInt:1];
    priceLevel1.title=@"100k-199k";
    priceLevel1.dollors=@"$";

    NSMutableArray *priceArr=[[NSMutableArray alloc]init];
    [priceArr addObject:priceLevel1];
    discover.priceLevelArray=priceArr;
    discover.tagUpdateArr=arr;
    discover.gotoWhere=IsLunch;
    discover.cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc] initWithMap];
    [restaurantsViewController initDiscover:discover];
//    [self.navigationController pushViewController:restaurantsViewController animated:NO];
    [[AppDelegate ShareApp].discoverListNavigationController pushViewController:restaurantsViewController animated:NO];
    [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 1;
    [AppDelegate ShareApp].selectedNavigationController =[AppDelegate ShareApp].discoverListNavigationController;
}
-(void)gotoBooking{
    IMGDiscover *discover=[[IMGDiscover alloc]init];
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"select * from IMGTag where type='Features' and name='Book Online' and cityId=%@;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]] successBlock:^(FMResultSet *resultSet) {
        while([resultSet next]) {
            IMGTag *tag = [[IMGTag alloc] init];
            [tag setValueWithResultSet:resultSet];
            [arr addObject:tag];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"FiltersViewController loadButtonGridView error when get datas:%@",error.description);
    }];
    discover.gotoWhere=IsBooking;
    discover.tagsArr=arr;
    discover.cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc] initWithMap];
    [restaurantsViewController initDiscover:discover];
//    [self.navigationController pushViewController:restaurantsViewController animated:NO];
    [[AppDelegate ShareApp].discoverListNavigationController pushViewController:restaurantsViewController animated:NO];
    [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 1;
    [AppDelegate ShareApp].selectedNavigationController =[AppDelegate ShareApp].discoverListNavigationController;
}
-(void)gotoDinner{

    IMGDiscover *discover=[[IMGDiscover alloc]init];
    
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"select * from IMGTag where ((type='Food Type' and name='Steak' or name='Ribs') or(type='Features' and name='Live Music'))and cityId=%@;",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]] successBlock:^(FMResultSet *resultSet) {
        while([resultSet next]) {
            IMGTag *tag = [[IMGTag alloc] init];
            [tag setValueWithResultSet:resultSet];
            [arr addObject:tag];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"FiltersViewController loadButtonGridView error when get datas:%@",error.description);
    }];
    
    discover.tagsArr=arr;
    discover.gotoWhere=IsDinner;
    discover.cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc] initWithMap];
    [restaurantsViewController initDiscover:discover];
//    [self.navigationController pushViewController:restaurantsViewController animated:NO];
    [[AppDelegate ShareApp].discoverListNavigationController pushViewController:restaurantsViewController animated:NO];
    [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 1;
    [AppDelegate ShareApp].selectedNavigationController =[AppDelegate ShareApp].discoverListNavigationController;

}
-(void)goToPopular{
    IMGDiscover *discover=[[IMGDiscover alloc]init];
    discover.sortby =SORTBY_POPULARITY;
    discover.gotoWhere=IsPopular;
    RestaurantsViewController *restaurantsVC=[[RestaurantsViewController alloc]initWithMap];
    [restaurantsVC initDiscover:discover];
//    [self.navigationController pushViewController:restaurantsVC animated:YES];
    [[AppDelegate ShareApp].discoverListNavigationController pushViewController:restaurantsVC animated:NO];
    [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 1;
    [AppDelegate ShareApp].selectedNavigationController =[AppDelegate ShareApp].discoverListNavigationController;
    NSNotification *notification = [[NSNotification alloc] initWithName:SEARCH_RESTAURANT object:nil userInfo:@{@"discover":discover,@"isSortby":@"YES"}];
    
    if([self.delegate respondsToSelector:@selector(search:)])
    {
        [self.delegate search:notification];
        
        //        [self dismissViewControllerAnimated:YES completion:nil];
    }



}
-(void)gotoDiningGuide{
    [[AppDelegate ShareApp].tabMenuViewController setSelectedIndex:0];
    [[AppDelegate ShareApp].homeSlideViewController setCurrentTabIndex:2];


}
-(void)clickTabMenuRefreshPage{
    if (isFreshing) {
        return;
    }
    locationSearchBar.text = @"";
    [self searchWithSearchText:locationSearchBar.text];
    [self.navigationController popToRootViewControllerAnimated:NO];
    if (self.isSearchJournal)
    {
        [self initJournals];
    }
    else
    {
        [self initDiscovers];
    }
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:CITY_SELECT_NOTIFICATION];

//    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_REFRESH_TAB_SEARCH object:nil];
}
- (void)loadButtonView
{
    restaurantBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [restaurantBtn setTitle:L(@"RESTAURANT") forState:UIControlStateNormal];
    [restaurantBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    restaurantBtn.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [restaurantBtn setTitleColor:[UIColor colorC2060A] forState:UIControlStateSelected];
    restaurantBtn.frame = CGRectMake(0, 0, DeviceWidth/2, 35);
    restaurantBtn.tag = 1000;
    [restaurantBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:restaurantBtn];
    
    journalBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [journalBtn setTitle:L(@"JOURNAL") forState:UIControlStateNormal];
    [journalBtn setTitleColor:restaurantBtn.titleLabel.textColor forState:UIControlStateNormal];
    journalBtn.titleLabel.font = restaurantBtn.titleLabel.font;
    [journalBtn setTitleColor:[UIColor colorC2060A] forState:UIControlStateSelected];
    journalBtn.frame = CGRectMake(restaurantBtn.endPointX, restaurantBtn.frame.origin.y, DeviceWidth/2, 35);
    journalBtn.tag = 1001;
    [journalBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:journalBtn];
    
   
    _isBtnImageView = [[UIImageView alloc] init];
    _isBtnImageView.backgroundColor = [UIColor colorC2060A];
    [self.view addSubview:_isBtnImageView];

    
    if (self.isSearchJournal)
    {
        journalBtn.selected = YES;
        _isBtnImageView.frame = CGRectMake(journalBtn.frame.origin.x, journalBtn.endPointY+2, DeviceWidth/2, 2);
    }
    else
    {
        restaurantBtn.selected = YES;
        _isBtnImageView.frame = CGRectMake(restaurantBtn.frame.origin.x, restaurantBtn.endPointY+2, DeviceWidth/2, 2);
    }
    
//    UIImageView *lineImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, _isBtnImageView.endPointY, DeviceWidth, 1)];
//    lineImage3.image = [UIImage imageNamed:@"CutOffRule"];
//    [self.view addSubview:lineImage3];
}
- (void)btnClick:(UIButton *)btn
{
   
    journalBtn.selected = NO;
    restaurantBtn.selected = NO;
    btn.selected = YES;
    if (btn.tag == 1000)
    {

        self.isSearchJournal = NO;
        _isBtnImageView.frame = CGRectMake(restaurantBtn.frame.origin.x, _isBtnImageView.frame.origin.y, DeviceWidth/2, 2);
        if (_discoverArray.count)
        {
            _tempDataArr = [NSArray arrayWithArray:_discoverArray];
            [searchDiscoverCategoryTableView reloadData];
        }
        else
        {
            [loadingImageView startLoading];
            [self initDiscovers];
        }
    }
    else
    {

        self.isSearchJournal = YES;
        _isBtnImageView.frame = CGRectMake(journalBtn.frame.origin.x, _isBtnImageView.frame.origin.y, DeviceWidth/2, 2);
        if (_journalArray.count)
        {
            _tempDataArr = [NSArray arrayWithArray:_journalArray];
            [searchDiscoverCategoryTableView reloadData];
        }
        else
        {
            [loadingImageView startLoading];
            [self initJournals];
        }
    }
    
    if (locationSearchBar.text.length)
    {
        [self searchWithSearchText:locationSearchBar.text];
    }
}
- (void)loadNearbyButton
{
    self.navigationItem.title = L(@"Locations");
    nearbyButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, CELL_HEIGHT_45)];
    nearbyButtonImage = [UIImage imageNamed:@"nearby_bg.png"];
    [nearbyButton setImage:nearbyButtonImage forState:UIControlStateNormal];
    [nearbyButton addTarget:self action:@selector(goNearby:) forControlEvents:UIControlEventTouchUpInside];
    [nearbyButton setBackgroundColor:[UIColor clearColor]];
    float i=0;
    if ([UIDevice isiPadHiRes]) {
        i=60;
    }

    SearchCellLabel *label = [[SearchCellLabel alloc] initWith16BoldBlackColorAndFrame:CGRectMake(55+i, 0, DeviceWidth, CELL_HEIGHT_45)];
    [label setText:L(@"Nearby")];
    [label setBackgroundColor:[UIColor clearColor]];
    [nearbyButton addSubview:label];
}
- (void)loadDiscoverCategoryView{
    discoverCategoryView = [[UIView alloc] initWithFrame:CGRectMake(0, _isBtnImageView.endPointY, DeviceWidth, DeviceHeight)];
    [self.view addSubview:discoverCategoryView];
    discoverCategoryView.hidden=YES;
    UIButton* _nearbyButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, CELL_HEIGHT_45)];
    nearbyButtonImage = [UIImage imageNamed:@"nearby_bg.png"];
    [_nearbyButton setImage:nearbyButtonImage forState:UIControlStateNormal];
    [_nearbyButton addTarget:self action:@selector(goNearby:) forControlEvents:UIControlEventTouchUpInside];
    [_nearbyButton setBackgroundColor:[UIColor clearColor]];
    
    SearchCellLabel *label = [[SearchCellLabel alloc] initWith16BoldBlackColorAndFrame:CGRectMake(55, 0, DeviceWidth, CELL_HEIGHT_45)];
    [label setText:L(@"Nearby")];
    [label setBackgroundColor:[UIColor clearColor]];
    [_nearbyButton addSubview:label];
    
    bestOffersButton = [[UIButton alloc] initWithFrame:CGRectMake(0, _nearbyButton.endPointY, DeviceWidth, CELL_HEIGHT_45)];
    bestOffersButtonImage = [UIImage imageNamed:@"best-offers.png"];
    [bestOffersButton setImage:bestOffersButtonImage forState:UIControlStateNormal];
    [bestOffersButton addTarget:self action:@selector(goBestOffers:) forControlEvents:UIControlEventTouchUpInside];
    
    mostPopularButton = [[UIButton alloc] initWithFrame:CGRectMake(0, bestOffersButton.endPointY, DeviceWidth, CELL_HEIGHT_45)];
    mostPopularButtonImage = [UIImage imageNamed:@"most-popular.png"];
    [mostPopularButton setImage:mostPopularButtonImage forState:UIControlStateNormal];
    [mostPopularButton addTarget:self action:@selector(goMostPopular:) forControlEvents:UIControlEventTouchUpInside];
    
    
    searchDiscoverCategoryTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-CELL_HEIGHT_45-discoverCategoryView.frame.origin.y+2-tabMenuBarHeight) style:UITableViewStylePlain];
    searchDiscoverCategoryTableView.dataSource = self;
    //    searchDiscoverCategoryTableView.separatorColor = [UIColor colorEDEDED];
    searchDiscoverCategoryTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    searchDiscoverCategoryTableView.delegate = self;
    searchDiscoverCategoryTableView.backgroundColor = [UIColor whiteColor];
    [discoverCategoryView addSubview:searchDiscoverCategoryTableView];
    
//    UIView *tableViewHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, mostPopularButton.endPointY)];
//    [tableViewHeaderView addSubview:_nearbyButton];
//    [tableViewHeaderView addSubview:bestOffersButton];
//    [tableViewHeaderView addSubview:mostPopularButton];
//    [searchDiscoverCategoryTableView setTableHeaderView:tableViewHeaderView];
    
}
-(void)initDiscovers{
    isFreshing = YES;
    if (isInitDiscoverIng) {
        return;
    }else{
        isInitDiscoverIng = YES;
    }
    [DiscoverUtil refreshDiscoverAndHandle:^(NSArray *discoverArray) {
        isInitDiscoverIng = NO;
        [_discoverArray removeAllObjects];
        [_discoverArray addObjectsFromArray:discoverArray];
        _tempDataArr = [NSArray arrayWithArray:_discoverArray];
        if (!self.isSearchJournal)
        {
            [searchDiscoverCategoryTableView reloadData];
        }
        [loadingImageView stopLoading];
        isFreshing = NO;
    }];
}
- (void)initJournals
{
    beginLoadingData = [[NSDate date]timeIntervalSince1970];
    isFreshing = YES;
    [JournalHandler getJournalCategoryWithBlock:^(NSArray *dataArray) {
        _journalArray = [NSMutableArray arrayWithArray:dataArray];
        _tempDataArr = [NSArray arrayWithArray:_journalArray];
        [searchDiscoverCategoryTableView reloadData];
        [loadingImageView stopLoading];
        finishUI = [[NSDate date]timeIntervalSince1970];
        if (isViewDidLoad) {
            float duringTime = finishUI - beginLoadingData;
            duringTime = duringTime * 1000;
            int duringtime = duringTime;
            [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createTimingWithCategory:@"Search result journal"  interval:[NSNumber numberWithInt:duringtime] name:@"" label:@""] build]];
            isViewDidLoad = NO;
            
        }

        isFreshing = NO;
    }];
}
-(void)loadLoadingView{
    loadingImageView = [[SimpleLoadingImageView alloc]initWithFrame:CGRectMake(0, -50, DeviceWidth, searchDiscoverCategoryTableView.frame.size.height+100)];
    [searchDiscoverCategoryTableView addSubview:loadingImageView];
    [loadingImageView startLoading];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    [loadingImageView stopLoading];
    if (self.isJournalSearch)
    {
    }
    else
    {
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"initDiscovers"]){
            [self initDiscovers];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"initDiscovers"];
        }
        
    };
    if (self.isFromTabMenu) {
        [[self rdv_tabBarController] setTabBarHidden:NO animated:NO];
    }
    if (self.isSearchJournal)
    {
        self.screenName = @"Search journal form page";
        loadingImageView.frame = CGRectMake(0, -50, DeviceWidth, searchDiscoverCategoryTableView.frame.size.height+100);
        [searchDiscoverCategoryTableView addSubview:loadingImageView];

        [loadingImageView startLoading];
        [self initJournals];
    }
    else
    {
        self.screenName = @"Search restaurant form page";
        loadingImageView.frame = CGRectMake(0, -50, DeviceWidth, searchDiscoverCategoryTableView.frame.size.height+100);
        [searchDiscoverCategoryTableView addSubview:loadingImageView];

        [loadingImageView startLoading];

        [self initDiscovers];
    }
    if (locationSearchBar.text.length>0) {
        searchAllCategoryScrol.hidden=YES;
        
    }else{
        
        searchResultsTableView.hidden=YES;
        searchAllCategoryScrol.hidden=NO;
        discoverCategoryView.hidden=YES;
        self.navigationItem.leftBarButtonItem=nil;
        [locationSearchBar resignFirstResponder];
        
    }

}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[LoadingView sharedLoadingView] stopLoading];
    if (self.isFromTabMenu) {
        [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
    }
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (self.searchText.length)
    {
        [self searchWithSearchText:self.searchText];
    }
//    [locationSearchBar becomeFirstResponder];
}
- (void)loadSearchBar
{
    if (!self.isFromTabMenu) {
        UIBarButtonItem *backBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:NavigationBackImage] style:UIBarButtonItemStylePlain target:self action:@selector(backClick)];
        backBtn.tintColor=[UIColor whiteColor];
        self.navigationItem.leftBarButtonItem=backBtn;
    }
    locationSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(30, 0, SEARCHBAR_WIDTH, SEARCHBAR_HEIGHT)];
    locationSearchBar.delegate = self;
    locationSearchBar.enablesReturnKeyAutomatically=NO;
    locationSearchBar.returnKeyType=UIReturnKeySearch;
    self.navigationItem.titleView = locationSearchBar;
    locationSearchBar.showsCancelButton =NO;
    locationSearchBar.placeholder = L(@"Search ");
    if (self.searchText.length)
    {
        locationSearchBar.text = self.searchText;
    }
//    if ([UIDevice isLaterThanIos6]) {
//        locationSearchBar.searchBarStyle = UISearchBarStyleMinimal;
//    }
    [[[locationSearchBar.subviews objectAtIndex:0].subviews objectAtIndex:1] setTintColor:[UIColor lightGrayColor]];
    UIView *searchTextField = [[[locationSearchBar.subviews firstObject]subviews]lastObject];
    searchTextField.backgroundColor =[UIColor colorWithRed:242.0/255.0 green:225.0/255.0 blue:226.0/255.0 alpha:1];
    for (UIView *subview in locationSearchBar.subviews) {
        for(UIView* grandSonView in subview.subviews){
            if ([grandSonView isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                grandSonView.alpha = 0.0f;
            }else if([grandSonView isKindOfClass:NSClassFromString(@"UISearchBarTextField")] ){
            }else{
                grandSonView.alpha = 0.0f;
            }
        }
    }
    
}

-(void)backClick{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)goToBackViewController{
    [locationSearchBar resignFirstResponder];
    if (self.isFromAppDelegate)
    {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
    else
        [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)loadSearchResultsView
{
    
    searchResultsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, _isBtnImageView.endPointY, DeviceWidth, DeviceHeight-CELL_HEIGHT_45-discoverCategoryView.frame.origin.y-tabMenuBarHeight) style:UITableViewStylePlain];
    searchResultsTableView.dataSource = self;
    searchResultsTableView.delegate = self;
    //    searchResultsTableView.separatorColor = [UIColor colorEDEDED];
    searchResultsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:searchResultsTableView];
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, CELL_HEIGHT_45)];
    [headerView addSubview:nearbyButton];
    [searchResultsTableView setTableHeaderView:headerView];
    
}
- (void)goNearby:(id)sender
{
    [locationSearchBar resignFirstResponder];
    LocationListViewController *locationListViewController = [[LocationListViewController alloc]initWithoutHead];
    locationListViewController.isFromSearch = YES;
    [self.navigationController pushViewController:locationListViewController animated:YES];
}

- (void)goBestOffers:(id)sender
{
    [self goSelectPaxAndTimeWithBestOffer];
}

- (void)goMostPopular:(id)sender
{
    [self goRestaurantsViewControllerWithMostPopular];
}


-(int)numberOfSectionsForFilter{
    int i=0;
    if([filterResultsDictionary objectForKey:KEY_LOCATION]!=nil&&[[filterResultsDictionary objectForKey:KEY_LOCATION] count]>0){
        i++;
    }
    if([filterResultsDictionary objectForKey:KEY_CUISINE_TYPE]!=nil&&[[filterResultsDictionary objectForKey:KEY_CUISINE_TYPE] count]>0){
        i++;
    }
    if([filterResultsDictionary objectForKey:KEY_FOOD_TYPE]!=nil&&[[filterResultsDictionary objectForKey:KEY_FOOD_TYPE] count]>0){
        i++;
    }
    if([filterResultsDictionary objectForKey:KEY_RESTAURANT]!=nil&&[[filterResultsDictionary objectForKey:KEY_RESTAURANT] count]>0){
        i++;
    }
    return i;
}

-(NSArray *)titleOfSectionForFilter{
    NSMutableArray *titleArray = [[NSMutableArray alloc]initWithCapacity:0];
    if([filterResultsDictionary objectForKey:KEY_LOCATION]!=nil&&[[filterResultsDictionary objectForKey:KEY_LOCATION] count]>0){
        [titleArray addObject:@"Locations"];
    }
    if([filterResultsDictionary objectForKey:KEY_CUISINE_TYPE]!=nil&&[[filterResultsDictionary objectForKey:KEY_CUISINE_TYPE] count]>0){
        [titleArray addObject:@"Cuisine"];
    }
    if([filterResultsDictionary objectForKey:KEY_FOOD_TYPE]!=nil&&[[filterResultsDictionary objectForKey:KEY_FOOD_TYPE] count]>0){
        [titleArray addObject:@"Food"];
    }
    if([filterResultsDictionary objectForKey:KEY_RESTAURANT]!=nil&&[[filterResultsDictionary objectForKey:KEY_RESTAURANT] count]>0){
        [titleArray addObject:@"Restaurants"];
    }
    return titleArray;
}

-(NSArray *)objectOfSectionForFilter{
    NSMutableArray *objectArray = [[NSMutableArray alloc]initWithCapacity:0];
    if([filterResultsDictionary objectForKey:KEY_LOCATION]!=nil&&[[filterResultsDictionary objectForKey:KEY_LOCATION] count]>0){
        NSArray *locationArray = [filterResultsDictionary objectForKey:KEY_LOCATION];
        [objectArray addObject:locationArray];
    }
    if([filterResultsDictionary objectForKey:KEY_CUISINE_TYPE]!=nil&&[[filterResultsDictionary objectForKey:KEY_CUISINE_TYPE] count]>0){
        NSArray *cuisineArray = [filterResultsDictionary objectForKey:KEY_CUISINE_TYPE];
        [objectArray addObject:cuisineArray];
    }
    if([filterResultsDictionary objectForKey:KEY_FOOD_TYPE]!=nil&&[[filterResultsDictionary objectForKey:KEY_FOOD_TYPE] count]>0){
        NSArray *foodArray = [filterResultsDictionary objectForKey:KEY_FOOD_TYPE];
        [objectArray addObject:foodArray];
    }
    if([filterResultsDictionary objectForKey:KEY_RESTAURANT]!=nil&&[[filterResultsDictionary objectForKey:KEY_RESTAURANT] count]>0){
        NSArray *restaurantArray = [filterResultsDictionary objectForKey:KEY_RESTAURANT];
        [objectArray addObject:restaurantArray];
    }
    return objectArray;
}

-(NSString *)typeOfSectionForFilter:(NSInteger)section{
    NSMutableArray *typeArray = [[NSMutableArray alloc]initWithCapacity:0];
    if([filterResultsDictionary objectForKey:KEY_LOCATION]!=nil&&[[filterResultsDictionary objectForKey:KEY_LOCATION] count]>0){
        [typeArray addObject:KEY_LOCATION];
    }
    if([filterResultsDictionary objectForKey:KEY_CUISINE_TYPE]!=nil&&[[filterResultsDictionary objectForKey:KEY_CUISINE_TYPE] count]>0){
        [typeArray addObject:KEY_CUISINE_TYPE];
    }
    if([filterResultsDictionary objectForKey:KEY_FOOD_TYPE]!=nil&&[[filterResultsDictionary objectForKey:KEY_FOOD_TYPE] count]>0){
        [typeArray addObject:KEY_FOOD_TYPE];
    }
    if([filterResultsDictionary objectForKey:KEY_RESTAURANT]!=nil&&[[filterResultsDictionary objectForKey:KEY_RESTAURANT] count]>0){
        [typeArray addObject:KEY_RESTAURANT];
    }
    return [typeArray objectAtIndex:section];
}

#pragma mark UITable datasource and delegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(tableView==searchResultsTableView){
        if (self.isSearchJournal)
        {
            return 1;
        }
        else
            return [self numberOfSectionsForFilter];
    }
    else{
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(tableView == searchResultsTableView && [tableView numberOfSections]>0){
        if (self.isSearchJournal)
        {
            return 0;
        }
        else
            return 37;
    }else{
        return 37;
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(tableView == searchResultsTableView){
        if (self.isSearchJournal)
        {
            
        }
        else
        {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 37)];
            [view setBackgroundColor:[UIColor colorFBFBFB]];
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, view.frame.size.width-20, view.frame.size.height-1)];
            [label setBackgroundColor:[UIColor clearColor]];
            UIFont *font=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:16];
            [label setFont:font];
            [label setTextColor:[UIColor color222222]];
            [label setTextAlignment:NSTextAlignmentLeft];
            [view addSubview:label];
            UIImageView *celllineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 36, DeviceWidth, 1)];
            [celllineImageView setImage:[UIImage imageNamed:@"search_line"] ];
            [view addSubview:celllineImageView];
            NSArray *titleArray = [self titleOfSectionForFilter];
            [label setText:[titleArray objectAtIndex:section]];
            return view;
        }
    }else{
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 37)];
            [view setBackgroundColor:[UIColor colorFBFBFB]];
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, view.frame.size.width-20, view.frame.size.height-1)];
            [label setBackgroundColor:[UIColor clearColor]];
            UIFont *font=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:16];
            [label setFont:font];
            [label setTextColor:[UIColor color222222]];
            [label setTextAlignment:NSTextAlignmentLeft];
            [label setText:@"Recent Searches"];
            [view addSubview:label];
            UIImageView *celllineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 36, DeviceWidth, 1)];
            [celllineImageView setImage:[UIImage imageNamed:@"search_line"] ];
            [view addSubview:celllineImageView];
            return view;

    
        

    }
    return nil;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger numberOfRows = 0;
    if(tableView==searchDiscoverCategoryTableView||tableView.tag == 10000){
        if (self.isSearchJournal) {
            numberOfRows=draftJournalArray.count;
        }else{
            numberOfRows = draftRestaurantArray.count;
        }
    }else if(tableView == searchResultsTableView){
        if (self.isSearchJournal)
        {
            numberOfRows = _searchJournalArr.count;
        }
        else
        {
            NSArray *objectArray = [self objectOfSectionForFilter];
            if(objectArray!=nil && [objectArray objectAtIndex:section]!=nil){
                numberOfRows=[[objectArray objectAtIndex:section] count];
            }
            else
                numberOfRows = 0;
        }
    }else{
        numberOfRows = 0;
        
    }
    return numberOfRows;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier ;
    
    if(tableView==searchDiscoverCategoryTableView || tableView.tag == 10000)
    {
        if (self.isSearchJournal)
        {
            identifier = [[NSString alloc] initWithFormat:@"JournalCell"];
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if(cell == nil){
                
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                UIImageView *celllineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, cell.endPointY-1, DeviceWidth, 1)];
                [celllineImageView setImage:[UIImage imageNamed:@"search_line"] ];
                [cell addSubview:celllineImageView];
            }
//            IMGJournalCategory *journalCategory = [_tempDataArr objectAtIndex:indexPath.row];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            if (draftJournalArray.count>0) {
                [cell.textLabel setText:draftJournalArray[indexPath.row]];

            }
            cell.selectionStyle = UITableViewCellSelectionStyleGray;

            return cell;
        }
        else
        {
            UITableViewCell *cell;
                identifier = [[NSString alloc] initWithFormat:@"draftCell"];
                cell = [tableView dequeueReusableCellWithIdentifier:identifier];
                if(cell == nil){
                    
                    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
                    UIImageView *celllineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, cell.endPointY-1, DeviceWidth, 1)];
                    [celllineImageView setImage:[UIImage imageNamed:@"search_line"] ];
                    [cell addSubview:celllineImageView];

                }
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

                [cell.textLabel setText:draftRestaurantArray[indexPath.row]];

                cell.selectionStyle = UITableViewCellSelectionStyleGray;
            return cell;

        }
    }
    else if(tableView == searchResultsTableView){
        if (self.isSearchJournal)
        {
            NSString *searchJournalCellName = [[NSString alloc] initWithFormat:@"SearchJournalCell"];
            SearchJournalTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:searchJournalCellName];
            if(cell == nil){
                
                cell = [[SearchJournalTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:searchJournalCellName];
            }
            IMGMediaComment *journal = [_searchJournalArr objectAtIndex:indexPath.row];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

            NSString *matchStr = nil;
            if (searchBarText != nil) {
                matchStr = searchBarText;
            }
            if (isUseLastSearchJournalBarText && (lastSearchJournalBarText !=nil) ) {
                matchStr = lastSearchJournalBarText;
            }
            matchStr = [matchStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if (matchStr != nil) {
                NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:journal.title];
                [self string:journal.title attributedString:attStr matchStr:matchStr];
                
                NSArray *matchStrItemArr = [matchStr componentsSeparatedByString:@" "];
                if ( (matchStrItemArr != nil) && (matchStrItemArr.count != 0) && (matchStrItemArr.count != 1)) {
                    for (NSString *item in matchStrItemArr) {
                        if (![@"" isEqualToString:item]) {
                            [self string:journal.title attributedString:attStr matchStr:[item stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
                        }
                    }
                }
                
                [cell.textLabel setAttributedText:attStr];
            }else{
                [cell.textLabel setText:journal.title];
            }

            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            return cell;
        }
        else
        {
            identifier = [[NSString alloc] initWithFormat:@"SearchCell"];
            LocationAndRestaurantCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if(cell == nil)
            {
                cell = [[LocationAndRestaurantCellTableViewCell alloc] init];
            }
            NSString *cellTitle=@"";
            NSString *sectionType=[self typeOfSectionForFilter:indexPath.section];
            NSArray *objectArray = [[self objectOfSectionForFilter] objectAtIndex:indexPath.section];
            if([sectionType isEqualToString:KEY_LOCATION]){
                id obj = objectArray[indexPath.row];
                IMGDistrictAndLandmark *district;
                if ([obj isKindOfClass:[IMGDistrictAndLandmark class]]) {
                    district = (IMGDistrictAndLandmark *)obj;
                }else{
                    IMGTag *tag = (IMGTag*)obj;
                    district = [[IMGDistrictAndLandmark alloc]init];
                    district.districtId = tag.tagId;
                    district.name = [tag.name filterHtml];
                    district.locationType=[NSNumber numberWithInt:1];
                }
                cellTitle = district.name;
                cell.district = district;
            }else if([sectionType isEqualToString:KEY_CUISINE_TYPE]){
                IMGCuisineAndFoodType *food = (IMGCuisineAndFoodType *)(objectArray[indexPath.row]);
                cellTitle = food.name;
                cell.food = food;
            }else if([sectionType isEqualToString:KEY_FOOD_TYPE]){
                IMGCuisineAndFoodType *food = (IMGCuisineAndFoodType *)(objectArray[indexPath.row]);
                cellTitle = food.name;
                cell.food = food;
            }else if([sectionType isEqualToString:KEY_RESTAURANT]){
                IMGRestaurant *restaurant = (IMGRestaurant *)objectArray[indexPath.row];
                
                NSString *locationName=restaurant.districtName;
                
                //            FMResultSet *resultSet = [[DBManager manager] executeQuery:[NSString stringWithFormat:@"select tagId from IMGRestaurantTag where restaurantId=%@",restaurant.restaurantId]];
                //            while([resultSet next]){
                //                IMGTag *tag=[IMGTag findById:[resultSet objectForColumnName:@"tagId"]];
                //                if(tag!=nil&&[tag.type isEqual:TAG_TYPE_LANDMARK]){
                //                    locationName=tag.name;
                //                    break;
                //                }
                //            }
                //            [resultSet close];
                
                if(locationName==nil || [locationName isKindOfClass:[NSNull class]]){
                    locationName=@"";
                }
                if (locationName.length)
                {
                    cellTitle = [NSString stringWithFormat:@"%@, %@",restaurant.title,locationName];
                }
                else
                {
                    cellTitle = [NSString stringWithFormat:@"%@",restaurant.title];
                }
                cell.restaurant = restaurant;
            }
            else
            {
                return nil;
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            
            SearchCellLabel *searchCellLabel = [[SearchCellLabel alloc] initWithBlackColorAndFrame:CGRectMake(20, 0, DeviceWidth-30, CELL_HEIGHT_45)];
            
            NSString *matchStr = nil;
            if (searchBarText != nil) {
                matchStr = searchBarText;
            }
            if (isUseLastSearchBarText && (lastSearchBarText !=nil) ) {
                matchStr = lastSearchBarText;
            }
            matchStr = [matchStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if (matchStr != nil) {
                NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:cellTitle];
                [self string:cellTitle attributedString:attStr matchStr:matchStr];
                
                NSArray *matchStrItemArr = [matchStr componentsSeparatedByString:@" "];
                if ( (matchStrItemArr != nil) && (matchStrItemArr.count != 0) && (matchStrItemArr.count != 1)) {
                    for (NSString *item in matchStrItemArr) {
                        if (![@"" isEqualToString:item]) {
                            [self string:cellTitle attributedString:attStr matchStr:[item stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
                        }
                    }
                }
                
                [searchCellLabel setAttributedText:attStr];
            }else{
                [searchCellLabel setText:cellTitle];
            }
            [cell addSubview:searchCellLabel];
            
            UIImageView *celllineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, cell.frame.origin.y+cell.frame.size.height-1, DeviceWidth, 1)];
            [celllineImageView setImage:[UIImage imageNamed:@"search_line"] ];
            [cell addSubview:celllineImageView];
            return cell;
        }
    }
    else {
        return nil;
    }
}

-(void)string:(NSString*)str attributedString:(NSMutableAttributedString*)attStr matchStr:(NSString*)matchStr {
    NSRange range = [str rangeOfString:matchStr options:NSCaseInsensitiveSearch];
    if ( (range.location != NSNotFound) && (range.length != 0) ) {
        NSMutableArray *rangeArr = [self getRangeStr:str findText:matchStr];
        for (NSNumber *item in rangeArr) {
            [attStr setAttributes:@{NSFontAttributeName:[UIFont fontWithName:FONT_OPEN_SANS_BOLD size:16]} range:NSMakeRange([item intValue], matchStr.length)];
        }
    }
}

- (NSMutableArray *)getRangeStr:(NSString *)text findText:(NSString *)findText{
    NSString *regex = findText;
    NSError *error;
    NSMutableArray *arrayRanges = [[NSMutableArray alloc] init];
    NSRegularExpression *regular = [NSRegularExpression regularExpressionWithPattern:regex
                                                                             options:NSRegularExpressionCaseInsensitive
                                                                               error:&error];
    NSArray *matches = [regular matchesInString:text
                                        options:0
                                          range:NSMakeRange(0, text.length)];
    for (NSTextCheckingResult *match in matches) {
        NSRange range = [match range];
        [arrayRanges addObject:[NSNumber numberWithLong:range.location]];
    }
    return arrayRanges;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CELL_HEIGHT_45;
}


- (void)scrollViewDidScroll:(UIScrollView *)scroll {
    [locationSearchBar resignFirstResponder];
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [locationSearchBar resignFirstResponder];
    if(tableView == searchResultsTableView){
        if (self.isSearchJournal)
        {
            [[Amplitude instance] logEvent:@"CL - Journal Search Suggestion" withEventProperties:@{}];
            
            IMGMediaComment *journal = [_searchJournalArr objectAtIndex:indexPath.row];
            if ([journal.imported intValue]== 1)
            {
                [JournalHandler getJournalDetailWithJournalArticleId:journal.mediaCommentId andBlock:^(NSString *content,NSString *webSite) {
                    journal.contents = content;
                    WebViewController *wvc = [[WebViewController alloc] init];
                    wvc.content = journal.contents;
                    wvc.journal = journal;
                    wvc.webSite = webSite;
                    wvc.title = journal.title;
                    wvc.fromDetail = NO;
                    [self.navigationController pushViewController:wvc animated:YES];
                }];
                
            }
            else
            {
                JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
                jdvc.journal = journal;
                [TrackHandler trackForSearchWithProducttype:[NSNumber numberWithInt:1] andSearchresulttype:[NSNumber numberWithInt:4] andContent:locationSearchBar.text andResultObjectId:journal.mediaCommentId];

                //                jdvc.categoryDataArrM = [NSMutableArray arrayWithArray:_categoryDataArrM];
                
                [self.navigationController pushViewController:jdvc animated:YES];
                
                //Event Tracking
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
//                [eventProperties setValue:locationSearchBar.text forKey:@"Article Key Word"];
//                [eventProperties setValue:journal.name forKey:@"ArticlelName"];
                [[Amplitude instance] logEvent:@"SC - Search Article" withEventProperties:eventProperties];
                
                
            }
        }
        else
        {
            NSString *sectionType=[self typeOfSectionForFilter:indexPath.section];
            NSArray *objectArray = [[self objectOfSectionForFilter] objectAtIndex:indexPath.section];
            
            NSMutableDictionary *properties = [NSMutableDictionary dictionary];
            
            if([sectionType isEqualToString:KEY_LOCATION]){
                id district = objectArray[indexPath.row];
                [self goToRestaurantsViewControllerByDistrict:district];
                
                //Event Tracking
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
//                [eventProperties setValue:locationSearchBar.text forKey:@"Location Key Word"];
//                [eventProperties setValue:district.name forKey:@"LocationName"];
                [eventProperties setValue:@"Search Page" forKey:@"Location"];
                [[Amplitude instance] logEvent:@"SC - Search Location" withEventProperties:eventProperties];
                
                [properties setValue:@"District" forKey:@"Type"];
                
            }else if([sectionType isEqualToString:KEY_CUISINE_TYPE]){
                IMGCuisineAndFoodType *food = (IMGCuisineAndFoodType *)(objectArray[indexPath.row]);
                [self goToRestaurantsViewControllerByFood:food];
                
                //Event Tracking
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
//                [eventProperties setValue:locationSearchBar.text forKey:@"Cusine Key Word"];
//                [eventProperties setValue:food.cuisineId forKey:@"CuisineId"];
                [eventProperties setValue:@"Search Page" forKey:@"Location"];
                [[Amplitude instance] logEvent:@"SC - Search Cuisine" withEventProperties:eventProperties];
                
                [properties setValue:@"Cuisine" forKey:@"Type"];
                
            }else if([sectionType isEqualToString:KEY_FOOD_TYPE]){
                IMGCuisineAndFoodType *food = (IMGCuisineAndFoodType *)(objectArray[indexPath.row]);
                [self goToRestaurantsViewControllerByFood:food];
                
                //Event Tracking
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:locationSearchBar.text forKey:@"Food Key Word"];
                [eventProperties setValue:food.name forKey:@"Food Name"];
                [eventProperties setValue:@"Search Page" forKey:@"Loocation"];
                [[Amplitude instance] logEvent:@"SC - Search Food" withEventProperties:eventProperties];
                
                [properties setValue:@"Food Type" forKey:@"Type"];
                
            }else if([sectionType isEqualToString:KEY_RESTAURANT]){
                IMGRestaurant *restaurant = (IMGRestaurant *)objectArray[indexPath.row];

                [self goToRestaurantDetailViewController:restaurant];
                
                //Event Tracking
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:locationSearchBar.text forKey:@"Restaurant Key Word"];
                [eventProperties setValue:restaurant.restaurantId forKey:@"restaurantId"];
                [eventProperties setValue:restaurant.title forKey:@"restaurantTitle"];
                [eventProperties setValue:@"Search page" forKey:@"Location"];
                [[Amplitude instance] logEvent:@"SC - Search Restaurant" withEventProperties:eventProperties];
                
                [properties setValue:@"Restaurant Name" forKey:@"Type"];
                
            }
            [[Amplitude instance] logEvent:@"CL - Restaurant Search Suggestion" withEventProperties:properties];
        }
    }else if(tableView==searchDiscoverCategoryTableView||tableView.tag == 10000){
        if (self.isSearchJournal)
        {
            IMGJournalCategory *journalCategory = [_journalArray objectAtIndex:indexPath.row];
            JournalListViewController *jlvc = [[JournalListViewController alloc] init];
            jlvc.amplitudeType = @"Search page";
            jlvc.isPop = YES;
            jlvc.searchText = draftJournalArray[indexPath.row];
            jlvc.isFromSearch = YES;
            jlvc.needLog = YES;
            jlvc.isTrending = self.isTrending;
            jlvc.currentCategoryName = journalCategory.categoryName;
            jlvc.currentCategoryId = journalCategory.categoryType;
            [[Amplitude instance] logEvent:@"SC - View Search Result Journal" withEventProperties:@{@"Suggestion Discovery":journalCategory.categoryName}];

            [self.navigationController pushViewController:jlvc animated:YES];
            
            //Event Tracking
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:locationSearchBar.text forKey:@" Keyword is none"];
            [eventProperties setValue:journalCategory.categoryName forKey:@"JournalCategoryName"];
            [[Amplitude instance] logEvent:@"CL - See All Articles	" withEventProperties:eventProperties];
            
        }
        else
        {
            IMGDiscover *discover=[[IMGDiscover alloc]init];
            discover.sortby=SORTBY_RELEVANCE;
            discover.keyword = draftRestaurantArray[indexPath.row];
            discover.fromWhere=@0;
            discover.needLog = YES;
            
            RestaurantsViewController *restaurantsViewController=[[RestaurantsViewController alloc]initWithMap];
            restaurantsViewController.isFromTabMenu = self.isFromTabMenu;
            restaurantsViewController.isPop = YES;
            restaurantsViewController.suggestionDiscovery = draftRestaurantArray[indexPath.row];
            restaurantsViewController.amplitudeType = @"Search result page - restaurant";
            restaurantsViewController.isFromDiscoverList = YES;
            [restaurantsViewController initDiscover:discover];

//            [self.navigationController pushViewController:restaurantsViewController animated:YES];
            [[AppDelegate ShareApp].discoverListNavigationController pushViewController:restaurantsViewController animated:NO];
            [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 1;
            [AppDelegate ShareApp].selectedNavigationController =[AppDelegate ShareApp].discoverListNavigationController;

//            [MixpanelHelper trackViewDiscoverWithDiscoverName:discover.name];
            
            //Event Tracking
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:locationSearchBar.text forKey:@" Keyword is none"];
            
            [eventProperties setValue:discover.name forKey:@"RestaurantType"];
            
            [[Amplitude instance] logEvent:@"Click - Restaurant list" withEventProperties:eventProperties];
            
            
        }
    }
    
}

-(void)goToRestaurantsViewControllerByDistrict:(id)districtLocal{
    [locationSearchBar resignFirstResponder];
    IMGDiscover *discover=[[IMGDiscover alloc]init];
    discover.sortby=SORTBY_POPULARITY;
    if([districtLocal isKindOfClass:[IMGDistrictAndLandmark class]]){
        IMGDistrictAndLandmark *district = (IMGDistrictAndLandmark*)districtLocal;
        if ([district.locationType intValue]==0) {
            discover.districtArray=[[NSMutableArray alloc]initWithCapacity:0];
            [discover.districtArray addObject:district];
            discover.searchDistrictArray = discover.districtArray;
            discover.name = district.name;
            [TrackHandler trackForSearchWithProducttype:[NSNumber numberWithInt:0] andSearchresulttype:[NSNumber numberWithInt:1] andContent:locationSearchBar.text andResultObjectId:district.districtId];
        }
    }else if([districtLocal isKindOfClass:[IMGTag class]]){
        IMGTag *tag = (IMGTag*)districtLocal;
        discover.name = tag.name;
        discover.tagsArr=[[NSMutableArray alloc]initWithCapacity:0];
        [discover.tagsArr addObject:tag];
        
        [TrackHandler trackForSearchWithProducttype:[NSNumber numberWithInt:0] andSearchresulttype:[NSNumber numberWithInt:2] andContent:locationSearchBar.text andResultObjectId:tag.tagId];

    }
    

    
    RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc]initWithMap];
    restaurantsViewController.amplitudeType = @"Search restaurant page";
    restaurantsViewController.isPop = YES;
    restaurantsViewController.suggestionDiscovery = discover.name;
    discover.fromWhere = [NSNumber numberWithInt:2];
    [restaurantsViewController initDiscover:discover];
//    [self.navigationController pushViewController:restaurantsViewController animated:YES];
    [[AppDelegate ShareApp].discoverListNavigationController pushViewController:restaurantsViewController animated:NO];
    [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 1;
    [AppDelegate ShareApp].selectedNavigationController =[AppDelegate ShareApp].discoverListNavigationController;

}

-(void)goToRestaurantsViewControllerByFood:(IMGCuisineAndFoodType *)food{
    [locationSearchBar resignFirstResponder];
    IMGDiscover *discover=[[IMGDiscover alloc]init];
    discover.sortby=SORTBY_POPULARITY;
    if([food.entityType intValue]==0){
        discover.cuisineArray=[[NSMutableArray alloc]initWithCapacity:0];
        [discover.cuisineArray addObject:(IMGCuisine *)food];
        [TrackHandler trackForSearchWithProducttype:[NSNumber numberWithInt:0] andSearchresulttype:[NSNumber numberWithInt:6] andContent:locationSearchBar.text andResultObjectId:food.cuisineId];
    }else{

        discover.foodTypeArray=[[NSMutableArray alloc]initWithCapacity:0];
        discover.foodArray = [NSMutableArray arrayWithObjects:food, nil];
        [discover.foodTypeArray addObject:food.cuisineId];
        [TrackHandler trackForSearchWithProducttype:[NSNumber numberWithInt:0] andSearchresulttype:[NSNumber numberWithInt:5] andContent:locationSearchBar.text andResultObjectId:food.cuisineId];
    }
    
    
    RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc]initWithMap];
    restaurantsViewController.amplitudeType = @"Search restaurant page";
    restaurantsViewController.isPop = YES;
    discover.fromWhere = [NSNumber numberWithInt:5];
    discover.name = food.name;
    [restaurantsViewController initDiscover:discover];
//    [self.navigationController pushViewController:restaurantsViewController animated:YES];
    [[AppDelegate ShareApp].discoverListNavigationController pushViewController:restaurantsViewController animated:NO];
    [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 1;
    [AppDelegate ShareApp].selectedNavigationController =[AppDelegate ShareApp].discoverListNavigationController;

}

-(void)goToRestaurantDetailViewController:(IMGRestaurant *)tmpRestaurant{
    [locationSearchBar resignFirstResponder];
    [TrackHandler trackForSearchWithProducttype:[NSNumber numberWithInt:0] andSearchresulttype:[NSNumber numberWithInt:0] andContent:locationSearchBar.text andResultObjectId:tmpRestaurant.restaurantId];

    IMGRestaurant *restaurant = [IMGRestaurant findById:tmpRestaurant.restaurantId];
    if (!restaurant)
    {
        [[LoadingView sharedLoadingView] startLoading];
        NSDictionary * params = @{@"ids":tmpRestaurant.restaurantId,@"downloadFromApp":@"1"};
        [[IMGNetWork sharedManager] GET:@"app/restaurant/list" parameters:params progress:nil  success:^(NSURLSessionDataTask *operation, id responseObject)
         {
             
             NSArray *returnRestaurantArray=(NSArray *)responseObject;
             for(NSDictionary *restaurantDictionary in returnRestaurantArray){
                 IMGRestaurant *tmpRestaurant = [[IMGRestaurant alloc] init];
                 tmpRestaurant.needUpdate = @"";
                 tmpRestaurant =  [RestaurantHandler parseJson2Entity:(NSDictionary *)restaurantDictionary andBlock:^(IMGRestaurant *restaurantT) {
                      
                 }];
                 [[LoadingView sharedLoadingView] stopLoading];
                 DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurant:tmpRestaurant];
                 detailViewController.amplitudeType = @"Search form page - suggested restaurant";
                 [self.navigationController pushViewController:detailViewController animated:YES];

             }
             
         }failure:^(NSURLSessionDataTask *operation, NSError *error) {
             NSLog(@"AFHTTPRequestOperation erors when get search:%@",error.localizedDescription);
         }];
    }
    else
    {
        DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurant:tmpRestaurant];
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
}


-(void)goSelectPaxAndTimeWithBestOffer{
//    [MixpanelHelper trackRestaurantTabFilterWithTab:@"Best Offers"];
    
    [locationSearchBar resignFirstResponder];
    PaxBookTimePickerViewController *paxBookTimePickerViewController = [[PaxBookTimePickerViewController alloc]initWithCloseButton];
    paxBookTimePickerViewController.bestOffer = YES;
    [self.navigationController pushViewController:paxBookTimePickerViewController animated:YES];
}

-(void)goRestaurantsViewControllerWithMostPopular
{
    [locationSearchBar resignFirstResponder];
    
    IMGDiscover *discover=[[IMGDiscover alloc]init];
    discover.sortby=SORTBY_POPULARITY;
    discover.fromWhere=@6;
    discover.cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc]initWithMap];
    restaurantsViewController.amplitudeType = @"Search restaurant page";
    restaurantsViewController.isPop = YES;
    [restaurantsViewController initDiscover:discover];
//    [self.navigationController pushViewController:restaurantsViewController animated:YES];
    [[AppDelegate ShareApp].discoverListNavigationController pushViewController:restaurantsViewController animated:NO];
    [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 1;
    [AppDelegate ShareApp].selectedNavigationController =[AppDelegate ShareApp].discoverListNavigationController;

}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length>0) {
        searchAllCategoryScrol.hidden=YES;
 
    }else{
        
//        searchResultsTableView.hidden=YES;
//        searchAllCategoryScrol.hidden=NO;
//        discoverCategoryView.hidden=YES;
//        self.navigationItem.leftBarButtonItem=nil;
//        [locationSearchBar resignFirstResponder];
    
    }
    
    if ((searchBarText == nil) || ([@"" isEqualToString:searchBarText]) ) {
        [[Amplitude instance] logEvent:@"SC - Type Search Keyword" withEventProperties:@{}];
    }
    searchBarText = searchText;
    [self searchWithSearchText:searchText];
}

//搜索引擎
- (void)searchWithSearchText:(NSString *)searchText
{

    loadingImageView = [[SimpleLoadingImageView alloc]initWithFrame:CGRectMake( 0, 0, DeviceWidth, searchResultsTableView.frame.size.height)];
    loadingImageView.backgroundColor = [UIColor color333333];
    [searchResultsTableView addSubview:loadingImageView];
    [loadingImageView startLoading];
    isUseLastSearchBarText = NO;
    isUseLastSearchJournalBarText = NO;
    
    if (self.isSearchJournal)
    {
        if(searchText!=nil && searchText.length>0){
            [discoverCategoryView setHidden:true];
            [searchResultsTableView setHidden:false];

            
            [SearchUtil journalFilterFromServerWithSearchText:searchText andBlock:^(NSArray *journalDataArray) {
                _searchJournalArr = [NSArray arrayWithArray:journalDataArray];
                if (_searchJournalArr.count)
                {
                    lastSearchJournalArr = [NSArray arrayWithArray:journalDataArray];
                    lastSearchJournalBarText = searchBarText;
                    
                    [searchResultsTableView setTableHeaderView:nil];
                    [searchResultsTableView reloadData];
                }
                else
                {
                    if (noJournalResultHeaderView == nil)
                    {
                        noJournalResultHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-nearbyButton.endPointY)];
                        UIView *suggestView = [[UIView alloc] init];
                        suggestView.backgroundColor = [UIColor colorWithRed:234/255.0 green:234/255.0 blue:234/255.0 alpha:1];
                        suggestView.frame = CGRectMake(0, 0, DeviceWidth, 100);
                        [noJournalResultHeaderView addSubview:suggestView];
                        
                        UIImageView *blackMapImageView = [[UIImageView alloc] init];
                        blackMapImageView.frame = CGRectMake(30, 30, 24, 32);
                        blackMapImageView.image = [UIImage imageNamed:@"map_black"];
                        [suggestView addSubview:blackMapImageView];
                        
                        UILabel *suggestLabel = [[UILabel alloc] init];
                        
                        suggestLabel.text = L(@"Still haven’t found what you want? \nSuggest a your story topic. We'll find it for you.");
                        suggestLabel.numberOfLines = 0;
                        suggestLabel.frame = CGRectMake(80, 30, DeviceWidth - LEFTLEFTSET*2 - 90, 100);
                        suggestLabel.font = [UIFont systemFontOfSize:12];
                        CGSize size = [suggestLabel.text sizeWithFont:[UIFont boldSystemFontOfSize:12] constrainedToSize:CGSizeMake(suggestLabel.frame.size.width, 10000) lineBreakMode:NSLineBreakByWordWrapping];
                        suggestLabel.frame = CGRectMake(suggestLabel.frame.origin.x, suggestLabel.frame.origin.y, size.width, size.height);
                        [suggestView addSubview:suggestLabel];
                        
                        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:suggestLabel.text];
                        [attrString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:12] range:[suggestLabel.text rangeOfString:@"Suggest a your story topic. We'll find it for you."]];
                        suggestLabel.attributedText = attrString;
                        
                        UIImageView *arrowView = [[UIImageView alloc] init];
                        arrowView.image = [UIImage imageNamed:@"arrow.png"];
                        arrowView.frame = CGRectMake(suggestView.frame.size.width - (9.5+8), (suggestView.frame.size.height-14)/2.0, 8, 14);
                        [suggestView addSubview:arrowView];
                        
                        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoSuggest)];
                        [suggestView addGestureRecognizer:tap];
                        
                        
                        UILabel *optionLable = [[UILabel alloc] init];
                        optionLable.text = L(@"Why not try some of these options");
                        optionLable.frame  = CGRectMake(LEFTLEFTSET, suggestView.endPointY, DeviceWidth-LEFTLEFTSET, 44);
                        optionLable.font = [UIFont boldSystemFontOfSize:13];
                        [noJournalResultHeaderView addSubview:optionLable];
                        
                        UITableView *optionTableView= [[UITableView alloc] init];
                        optionTableView.delegate = self;
                        optionTableView.dataSource = self;
                        optionTableView.tag = 10000;
                        optionTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
                
                        optionTableView.frame = CGRectMake(0, optionLable.endPointY, DeviceWidth, noJournalResultHeaderView.frame.size.height - optionLable.endPointY);
                        [noJournalResultHeaderView addSubview:optionTableView];
                        
                    }
                    
                    [searchResultsTableView setTableHeaderView:noJournalResultHeaderView];
                    
                    if (lastSearchJournalArr == nil) {
                        [searchResultsTableView setTableHeaderView:noJournalResultHeaderView];
                    }else{
                        [searchResultsTableView setTableHeaderView:nil];
                        _searchJournalArr = lastSearchJournalArr;
                        isUseLastSearchJournalBarText = YES;
                    }
                    
                    
                    [searchResultsTableView reloadData];
                }
            }];

        }
        else
        {
            [discoverCategoryView setHidden:false];
            [searchResultsTableView setHidden:true];
            
        }
        [loadingImageView stopLoading];

    }
    else
    {
        if(searchText!=nil && searchText.length>0){
            
            [discoverCategoryView setHidden:true];
            [searchResultsTableView setHidden:false];
            
            if ([[DBManager manager]SQLNullWithClass:[IMGDistrict class]]) {
                [[[DistrictHandler alloc]init]initFromServer];
            }
            [SearchUtil filterFromServerWithSearchText:searchText andBlock:^(NSArray *districtArray, NSArray *restaurantArray,NSArray *cuisineTypeArray,NSArray *foodTypeArray) {
                [filterResultsDictionary setObject:districtArray forKey:KEY_LOCATION];
                [filterResultsDictionary setObject:restaurantArray forKey:KEY_RESTAURANT];
                [filterResultsDictionary setObject:cuisineTypeArray forKey:KEY_CUISINE_TYPE];
                [filterResultsDictionary setObject:foodTypeArray forKey:KEY_FOOD_TYPE];
                
                if ((districtArray.count == 0 || [filterResultsDictionary objectForKey:KEY_LOCATION] == nil) && (restaurantArray.count == 0 || [filterResultsDictionary objectForKey:KEY_RESTAURANT] == nil)&&(cuisineTypeArray.count==0||[filterResultsDictionary objectForKey:KEY_CUISINE_TYPE ]==nil)&&(foodTypeArray.count==0||[filterResultsDictionary objectForKey:KEY_FOOD_TYPE ]==nil)) {
                    
                    if (self.isSearchJournal)
                    {
                        
                    }
                    else
                    {
                        if (noResultHeaderView == nil) {
                            noResultHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-nearbyButton.endPointY)];
                            UIButton* _nearbyButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, CELL_HEIGHT_45)];
                            nearbyButtonImage = [UIImage imageNamed:@"nearby_bg.png"];
                            [_nearbyButton setBackgroundImage:nearbyButtonImage forState:UIControlStateNormal];
                            [_nearbyButton addTarget:self action:@selector(goNearby:) forControlEvents:UIControlEventTouchUpInside];
                            [_nearbyButton setBackgroundColor:[UIColor clearColor]];
                            float i=0;
                            if ([UIDevice isiPadHiRes]) {
                                i=60;
                            }
                            SearchCellLabel *label = [[SearchCellLabel alloc] initWith16BoldBlackColorAndFrame:CGRectMake(55+i, 0, DeviceWidth, CELL_HEIGHT_45)];
                            [label setText:L(@"Nearby")];
                            [label setBackgroundColor:[UIColor clearColor]];
                            [_nearbyButton addSubview:label];
                            [noResultHeaderView addSubview:_nearbyButton];
                            

                            UIView *suggestView = [[UIView alloc] init];
                            suggestView.backgroundColor = [UIColor colorWithRed:234/255.0 green:234/255.0 blue:234/255.0 alpha:1];
                            suggestView.frame = CGRectMake(0, _nearbyButton.endPointY, DeviceWidth, 100);
                            [noResultHeaderView addSubview:suggestView];
                            
                            UIImageView *blackMapImageView = [[UIImageView alloc] init];
                            blackMapImageView.frame = CGRectMake(30, 30, 24, 32);
                            blackMapImageView.image = [UIImage imageNamed:@"map_black"];
                            [suggestView addSubview:blackMapImageView];
                            
                            UILabel *suggestLabel = [[UILabel alloc] init];
                            
                            suggestLabel.text = L(@"Still haven’t found what you want? \nSuggest a new place.");
                            suggestLabel.numberOfLines = 0;
                            suggestLabel.frame = CGRectMake(80, 30, DeviceWidth - LEFTLEFTSET*2 - 90, 100);
                            suggestLabel.font = [UIFont systemFontOfSize:12];
                            CGSize size = [suggestLabel.text sizeWithFont:[UIFont boldSystemFontOfSize:12] constrainedToSize:CGSizeMake(suggestLabel.frame.size.width, 10000) lineBreakMode:NSLineBreakByWordWrapping];
                            suggestLabel.frame = CGRectMake(suggestLabel.frame.origin.x, suggestLabel.frame.origin.y, size.width, size.height);
                            [suggestView addSubview:suggestLabel];
                            
                            NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:suggestLabel.text];
                            [attrString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:12] range:[suggestLabel.text rangeOfString:@"Suggest a new place."]];
                            suggestLabel.attributedText = attrString;
                            
                            UIImageView *arrowView = [[UIImageView alloc] init];
                            arrowView.image = [UIImage imageNamed:@"arrow.png"];
                            arrowView.frame = CGRectMake(suggestView.frame.size.width - (9.5+8), (suggestView.frame.size.height-14)/2.0, 8, 14);
                            [suggestView addSubview:arrowView];
                            
                            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoSuggest)];
                            [suggestView addGestureRecognizer:tap];
                            
                            
                            UILabel *optionLable = [[UILabel alloc] init];
                            optionLable.text = L(@"Why not try some of these options");
                            optionLable.frame  = CGRectMake(LEFTLEFTSET, suggestView.endPointY, DeviceWidth-LEFTLEFTSET, 44);
                            optionLable.font = [UIFont boldSystemFontOfSize:13];
                            [noResultHeaderView addSubview:optionLable];
                            
                            UITableView *optionTableView= [[UITableView alloc] init];
                            optionTableView.delegate = self;
                            optionTableView.dataSource = self;
                            optionTableView.tag = 10000;
                            optionTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
                            optionTableView.frame = CGRectMake(0, optionLable.endPointY, DeviceWidth, noResultHeaderView.frame.size.height - optionLable.endPointY);
                            [noResultHeaderView addSubview:optionTableView];
                            
//                            UIImage *image = [UIImage imageNamed:@"noSearch"];
//                            UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth/2-image.size.width/2, 80, image.size.width, image.size.height)];
//                            imageView.image = image;
//                            [noResultHeaderView addSubview:imageView];
//                            NSString *noRestaurantStr = @"We can't find any results for this search. You can try searching for something more general.";
//                            CGSize expectSize = [noRestaurantStr sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16] constrainedToSize:CGSizeMake(DeviceWidth-LEFTLEFTSET*4, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
//                            Label *noRestaurantLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET*2, imageView.endPointY+15, DeviceWidth-4*LEFTLEFTSET, expectSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16] andTextColor:[UIColor color222222] andTextLines:0];
//                            noRestaurantLabel.lineBreakMode = NSLineBreakByWordWrapping;
//                            noRestaurantLabel.textAlignment = NSTextAlignmentCenter;
//                            noRestaurantLabel.text = noRestaurantStr;
//                            [noResultHeaderView addSubview:noRestaurantLabel];
                            
                            
                        }
                        
                        if ((lastDistrictArray == nil)&&(lastRestaurantArray == nil)&&(lastCuisineTypeArray == nil)&&(lastFoodTypeArray== nil)) {
                            [searchResultsTableView setTableHeaderView:noResultHeaderView];
                        }else{
                            if (resultHeaderView == nil) {
                                resultHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, CELL_HEIGHT_45)];
                                UIButton* _nearbyButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, CELL_HEIGHT_45)];
                                nearbyButtonImage = [UIImage imageNamed:@"nearby_bg.png"];
                                //                        [_nearbyButton setImage:nearbyButtonImage forState:UIControlStateNormal];
                                [_nearbyButton setBackgroundImage:nearbyButtonImage forState:UIControlStateNormal];
                                [_nearbyButton addTarget:self action:@selector(goNearby:) forControlEvents:UIControlEventTouchUpInside];
                                [_nearbyButton setBackgroundColor:[UIColor clearColor]];
                                float i=0;
                                if ([UIDevice isiPadHiRes]) {
                                    i=60;
                                }
                                
                                SearchCellLabel *label = [[SearchCellLabel alloc] initWith16BoldBlackColorAndFrame:CGRectMake(55+i, 0, DeviceWidth, CELL_HEIGHT_45)];
                                [label setText:L(@"Nearby")];
                                [label setBackgroundColor:[UIColor clearColor]];
                                [_nearbyButton addSubview:label];
                                [resultHeaderView addSubview:_nearbyButton];
                            }
                            [searchResultsTableView setTableHeaderView:resultHeaderView];
                            [filterResultsDictionary setValue:lastDistrictArray forKey:KEY_LOCATION];
                            [filterResultsDictionary setValue:lastRestaurantArray forKey:KEY_RESTAURANT];
                            [filterResultsDictionary setValue:lastCuisineTypeArray forKey:KEY_CUISINE_TYPE];
                            [filterResultsDictionary setValue:lastFoodTypeArray forKey:KEY_FOOD_TYPE];
                            isUseLastSearchBarText = YES;
                        }
                        
                        [searchResultsTableView reloadData];
                    }
                    
                }
                else
                {
                    lastDistrictArray = districtArray;
                    lastRestaurantArray = restaurantArray;
                    lastCuisineTypeArray = cuisineTypeArray;
                    lastFoodTypeArray = foodTypeArray;
                    lastSearchBarText = searchBarText;
                    
                    if (resultHeaderView == nil) {
                        resultHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, CELL_HEIGHT_45)];
                        UIButton* _nearbyButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, CELL_HEIGHT_45)];
                        nearbyButtonImage = [UIImage imageNamed:@"nearby_bg.png"];
//                        [_nearbyButton setImage:nearbyButtonImage forState:UIControlStateNormal];
                        [_nearbyButton setBackgroundImage:nearbyButtonImage forState:UIControlStateNormal];
                        [_nearbyButton addTarget:self action:@selector(goNearby:) forControlEvents:UIControlEventTouchUpInside];
                        [_nearbyButton setBackgroundColor:[UIColor clearColor]];
                        float i=0;
                        if ([UIDevice isiPadHiRes]) {
                            i=60;
                        }
  
                        SearchCellLabel *label = [[SearchCellLabel alloc] initWith16BoldBlackColorAndFrame:CGRectMake(55+i, 0, DeviceWidth, CELL_HEIGHT_45)];
                        [label setText:L(@"Nearby")];
                        [label setBackgroundColor:[UIColor clearColor]];
                        [_nearbyButton addSubview:label];
                        [resultHeaderView addSubview:_nearbyButton];
                    }
                    [searchResultsTableView setTableHeaderView:resultHeaderView];
                    
                    [searchResultsTableView reloadData];
                }
                
                
            }];
            
            
            [searchResultsTableView setHidden:false];
        }
        else
        {
            [discoverCategoryView setHidden:false];
            [searchResultsTableView setHidden:true];
            
        }
        [loadingImageView stopLoading];
    }
}
- (void)gotoSuggest
{
    [locationSearchBar resignFirstResponder];
    if (self.isSearchJournal)
    {
        [[Amplitude instance] logEvent:@"UC - Suggest New Journal Initiate" withEventProperties:@{@"Origin":@" Journal search result page"}];

        JournalSuggestViewController *jsvc = [[JournalSuggestViewController alloc] init];
        [self.navigationController pushViewController:jsvc animated:YES];
    }
    else
    {
        [[Amplitude instance] logEvent:@"UC - Suggest New Restaurant Initiate" withEventProperties:@{@"Origin":@"Restaurant search result page"}];

        RestaurantSuggestViewController *rsvc = [[RestaurantSuggestViewController alloc] init];
        rsvc.amplitudeType = @"Restaurant search result page";
//        [self.navigationController pushViewController:rsvc animated:YES];
        [[AppDelegate ShareApp].discoverListNavigationController pushViewController:rsvc animated:NO];
        [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 1;
        [AppDelegate ShareApp].selectedNavigationController =[AppDelegate ShareApp].discoverListNavigationController;

    }
}


- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
//    searchBar.showsCancelButton=YES;
    searchAllCategoryScrol.hidden=YES;
    discoverCategoryView.hidden=NO;
    for(id cc in [searchBar subviews])
    {
        if([cc isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton *)cc;
            [btn setTitle:L(@"Cancel")  forState:UIControlStateNormal];
        }
    }
    return YES;
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
//    searchBar.text = @"";
    
    
    [loadingImageView startLoading];
    
    UIButton* btn=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    [btn setImage:[UIImage imageNamed:@"BackTop"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(backBtnAct:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:btn];

    
}
-(void)backBtnAct:(UIButton*)btn{
    [locationSearchBar resignFirstResponder];

    searchResultsTableView.hidden=YES;
    searchAllCategoryScrol.hidden=NO;
    discoverCategoryView.hidden=YES;
    [loadingImageView stopLoading];
    self.navigationItem.leftBarButtonItem=nil;
    locationSearchBar.text=nil;

}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    if (self.isSearchJournal)
    {
        JournalListViewController *jlvc = [[JournalListViewController alloc] init];
        jlvc.isFromSearch = YES;
        jlvc.amplitudeType = @"Search page";
        jlvc.isPop = YES;
        jlvc.searchText = locationSearchBar.text;
        jlvc.needLog = YES;
        jlvc.isTrending = self.isTrending;
        [self.navigationController pushViewController:jlvc animated:YES];
        
       
        //Event Tracking
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:jlvc.searchText forKey:@"Keyword"];
        [eventProperties setValue:@"Search Page" forKey:@"Location"];
        [[Amplitude instance] logEvent:@"SC - Search Journal" withEventProperties:eventProperties];
        if (searchBar.text.length>0&&![self isEmpty:searchBar.text]) {
            NSMutableArray *tempArry = [NSMutableArray arrayWithArray:draftJournalArray];
            if (tempArry.count>9) {
                [tempArry removeObjectsInRange:NSMakeRange(9, tempArry.count-9)];
            }
            for (NSString *str in draftJournalArray) {
                if ([str isEqualToString:searchBar.text]) {
                    [tempArry removeObject:str];
                }
            }
            
            draftJournalArray = [NSMutableArray arrayWithArray:tempArry];
            [draftJournalArray insertObject:searchBar.text atIndex:0];
            
            [[NSUserDefaults standardUserDefaults] setObject:draftJournalArray forKey:@"searchJournal"];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hasRecentJournal"];
        }

    }
    else
    {
        if (searchBar.text.length>0&&![self isEmpty:searchBar.text]) {
            NSMutableArray *tempArry = [NSMutableArray arrayWithArray:draftRestaurantArray];
            if (tempArry.count>9) {
                [tempArry removeObjectsInRange:NSMakeRange(9, tempArry.count-9)];
            }
            for (NSString *str in draftRestaurantArray) {
                if ([str isEqualToString:searchBar.text]) {
                    [tempArry removeObject:str];
                }
            }
          
            draftRestaurantArray = [NSMutableArray arrayWithArray:tempArry];
            [draftRestaurantArray insertObject:searchBar.text atIndex:0];
        
            [[NSUserDefaults standardUserDefaults] setObject:draftRestaurantArray forKey:@"searchRestaurant"];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"hasRecentRestaurant"];
        }
        IMGDiscover *discover=[[IMGDiscover alloc]init];
        if ( (searchBar.text == nil) || ([@"" isEqualToString:searchBar.text])) {
            discover.sortby=SORTBY_POPULARITY;
        }else{
            discover.sortby=SORTBY_RELEVANCE;
        }
        discover.keyword = searchBar.text;
        discover.fromWhere=@0;
        discover.needLog = YES;
        RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc]initWithMap];
        restaurantsViewController.amplitudeType = @"Search restaurant page";
        restaurantsViewController.amplitudeKind = @"Keyword based";
        restaurantsViewController.isPop = YES;
        [restaurantsViewController initDiscover:discover];
//        [self.navigationController pushViewController:restaurantsViewController animated:YES];
        [[AppDelegate ShareApp].discoverListNavigationController pushViewController:restaurantsViewController animated:NO];
        [AppDelegate ShareApp].tabMenuViewController.selectedIndex = 1;
        [AppDelegate ShareApp].selectedNavigationController =[AppDelegate ShareApp].discoverListNavigationController;
        
        //Event Tracking
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:discover.keyword forKey:@"Keyword"];
        [eventProperties setValue:discover.sortby forKey:@"Sortby"];
        if (!discover.keyword.length) {
            [eventProperties setValue:@"withSearchButton" forKey:@"searchType"];
            [[Amplitude instance] logEvent:@" Search - View Search Result Restaurant With Nothing" withEventProperties:eventProperties];
        }else {
         [[Amplitude instance] logEvent:@" SC - Search Restaurant" withEventProperties:eventProperties];
        }
        
       
        
        
        
    }
    
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
//    searchBar.showsCancelButton=NO;
    searchAllCategoryScrol.hidden=NO;
}
- (BOOL) isEmpty:(NSString *) str {
    
    if (!str) {
        return true;
    } else {
        //A character set containing only the whitespace characters space (U+0020) and tab (U+0009) and the newline and nextline characters (U+000A–U+000D, U+0085).
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        
        //Returns a new string made by removing from both ends of the receiver characters contained in a given character set.
        NSString *trimedString = [str stringByTrimmingCharactersInSet:set];
        
        if ([trimedString length] == 0) {
            return true;
        } else {
            return false;
        }
    }
}
 

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
