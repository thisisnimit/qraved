//
//  TagListViewController.h
//  Qraved
//
//  Created by lucky on 16/9/5.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "IMGDiscover.h"
#import "IMGTagUpdate.h"

@interface TagListViewController : BaseChildViewController<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, retain) UISearchBar *searchBar;
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) UIView   * headView;
@property (nonatomic, retain) UIButton * goButton;
@property(nonatomic,assign)BOOL isfromSearchCategary;
@property (nonatomic, copy)   NSString	*searchStr;
-(id)initWithDiscover:(IMGDiscover *)paramDiscover andTagUpdate:(IMGTagUpdate *)tagUpdate;

@end
