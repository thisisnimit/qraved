//
//  MapViewController.h
//  Qraved
//
//  Created by Jeff on 8/19/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import <MapKit/MapKit.h>
#import "IMGRestaurant.h"
#import "RestaurantsViewController.h"
#import "SearchUtil.h"
#import "SavedMapListView.h"
@interface MapViewController : BaseChildViewController<MKMapViewDelegate>

@property (retain, nonatomic) MKMapView *mapView;
@property (assign, nonatomic) BOOL fromDetail;
@property(nonatomic,assign)BOOL fromSearchPage;
@property(nonatomic,assign)BOOL srpAmplitude;
@property(nonatomic,retain)UIImageView *restaurantImageView;
@property(nonatomic,retain)NSNumber* restaurantId;
@property(nonatomic,copy)NSString* amplitudeType;
@property (assign, nonatomic) BOOL fromLeftMenu;
@property (assign, nonatomic) BOOL fromProfile;
@property (assign, nonatomic) BOOL fromHomeCard;
@property (assign, nonatomic) BOOL fromSaved;
@property (assign, nonatomic) BOOL fromWantToGo;
@property (assign, nonatomic) BOOL showSearchThisArea;
@property (assign, nonatomic) BOOL isSavedAmplitude;
@property (nonatomic, retain)NSNumber* guideId;



@property(nonatomic,retain)NSMutableArray *restaurantArray;
@property(nonatomic,retain) UIViewController *restaurantsViewController;
@property(nonatomic, copy) void (^savedClick)(IMGRestaurant *pressRstaurant, SavedMapListView *savedMapView);
@property(nonatomic, copy) void (^searchThisArea)();

-(MapViewController *)initWithFrame:(CGRect )frame;
-(MapViewController *)initWithFrame:(CGRect )frame andSearchThisArea:(BOOL)searchThisArea;

-(void)setData:(IMGRestaurant *)restaurantData;
-(void)setDatas:(NSMutableArray *)restaurantArray;
-(void)setMapDatas:(NSMutableArray *)restaurantArray offset:(NSInteger)offset;
-(void)setSearchMapDatas:(NSMutableArray *)restaurantArray offset:(NSInteger)offset;
-(MapViewController *)initWithRestaurant:(IMGRestaurant *)restaurant;
-(MapViewController *)initWithRestaurant:(IMGRestaurant *)restaurant fromMenu:(BOOL)from;



@end
