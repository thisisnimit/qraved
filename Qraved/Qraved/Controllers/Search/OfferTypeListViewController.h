//
//  OfferTypeListViewController.h
//  Qraved
//
//  Created by Jeff on 8/22/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "IMGDiscover.h"

@interface OfferTypeListViewController : BaseChildViewController<UISearchBarDelegate,UISearchDisplayDelegate,UITableViewDataSource,UITableViewDelegate>


-(id)initWithDiscover:(IMGDiscover *)paramDiscover;

@end
