//
//  SortTypeViewController.h
//  Qraved
//
//  Created by Jeff on 8/18/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "IMGSortType.h"
#import "IMGDiscover.h"

@protocol SortTypeListViewControllerDelegate <NSObject>

- (void)search:(NSNotification *)notification;

@end


@interface SortTypeListViewController : BaseChildViewController<UITableViewDataSource,UITableViewDelegate>
-(id)initWithDiscover:(IMGDiscover *)discover;

@property (nonatomic,assign) id<SortTypeListViewControllerDelegate>stlvcDelegate;

@end
