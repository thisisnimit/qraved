//
//  OfferTypeListViewController.m
//  Qraved
//
//  Created by Jeff on 8/22/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "OfferTypeListViewController.h"
#import "UIConstants.h"
#import "NotificationConsts.h"

#import "UIColor+Helper.h"
#import "UIViewController+Helper.h"
#import "IMGOfferType.h"
#import "OfferTypeCell.h"
#import "IMGEvent.h"
#import "HomeUtil.h"
#import "OfferHandler.h"
@interface OfferTypeListViewController ()

@end

@implementation OfferTypeListViewController{
    
    UITableView *objectTableView;
    NSMutableArray *objectArray;
    IMGDiscover *discover;
    NSMutableArray *offerArray;

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

-(id)initWithDiscover:(IMGDiscover *)paramDiscover{
    self = [super init];
    if(self){
        discover = paramDiscover;
        if(discover==nil){
            discover = [[IMGDiscover alloc]init];
        }
        if(discover.offerTypeArray==nil){
            discover.offerTypeArray = [[NSMutableArray alloc]initWithCapacity:0];
        }
        if(discover.eventArray==nil){
            discover.eventArray = [[NSMutableArray alloc]initWithCapacity:0];
        }
        if(discover.offArray==nil){
            discover.offArray = [[NSMutableArray alloc]initWithCapacity:0];
        }
    }
    return self;
}

- (void)initSortTypeArray{
    if(objectArray==nil){
//        IMGOfferType *offerType1=[[IMGOfferType alloc] init];
//        offerType1.eventType=[NSNumber numberWithInt:0];
//        offerType1.offerId = [NSNumber numberWithInt:1];
//        offerType1.typeId = [NSNumber numberWithInt:0];
//        offerType1.off = [NSNumber numberWithInt:50];
//        offerType1.name = L(@"50% Qraved Discount");
//        
//        IMGOfferType *offerType2=[[IMGOfferType alloc] init];
//        offerType2.eventType=[NSNumber numberWithInt:0];
//        offerType2.offerId = [NSNumber numberWithInt:2];
//        offerType2.typeId = [NSNumber numberWithInt:0];
//        offerType2.off = [NSNumber numberWithInt:40];
//        offerType2.name = L(@"40% Qraved Discount");
//        
//        IMGOfferType *offerType3=[[IMGOfferType alloc] init];
//        offerType3.eventType=[NSNumber numberWithInt:0];
//        offerType3.offerId = [NSNumber numberWithInt:3];
//        offerType3.typeId = [NSNumber numberWithInt:0];
//        offerType3.off = [NSNumber numberWithInt:30];
//        offerType3.name = L(@"30% Qraved Discount");
//        
//        IMGOfferType *offerType4=[[IMGOfferType alloc] init];
//        offerType4.eventType=[NSNumber numberWithInt:0];
//        offerType4.offerId = [NSNumber numberWithInt:4];
//        offerType4.typeId = [NSNumber numberWithInt:0];
//        offerType4.off = [NSNumber numberWithInt:20];
//        offerType4.name = L(@"20% Qraved Discount");
//        
//        IMGOfferType *offerType5=[[IMGOfferType alloc] init];
//        offerType5.eventType=[NSNumber numberWithInt:0];
//        offerType5.offerId = [NSNumber numberWithInt:5];
//        offerType5.typeId = [NSNumber numberWithInt:0];
//        offerType5.off = [NSNumber numberWithInt:10];
//        offerType5.name = L(@"10% Qraved Discount");
//        
//        IMGOfferType *offerType6=[[IMGOfferType alloc] init];
//        offerType6.eventType=[NSNumber numberWithInt:0];
//        offerType6.offerId = [NSNumber numberWithInt:6];
//        offerType6.typeId = [NSNumber numberWithInt:1];
//        offerType6.off = [NSNumber numberWithInt:0];
//        offerType6.name = L(@"Special Set Menu");
//        
//        IMGOfferType *offerType7=[[IMGOfferType alloc] init];
//        offerType7.eventType=[NSNumber numberWithInt:0];
//        offerType7.offerId = [NSNumber numberWithInt:7];
//        offerType7.typeId = [NSNumber numberWithInt:2];
//        offerType7.off = [NSNumber numberWithInt:0];
//        offerType7.name = L(@"In-restaurant Promotion");
//        
//        IMGOfferType *offerType8=[[IMGOfferType alloc] init];
//        offerType8.eventType=[NSNumber numberWithInt:0];
//        offerType8.offerId = [NSNumber numberWithInt:8];
//        offerType8.typeId = [NSNumber numberWithInt:3];
//        offerType8.off = [NSNumber numberWithInt:0];
//        offerType8.name = L(@"Featured Promo");
//        objectArray = [[NSMutableArray alloc] initWithObjects:offerType1,offerType2,offerType3,offerType4,offerType5,offerType6,offerType7,offerType8, nil];
//
//        NSArray *events = [HomeUtil addEventToArray];
//        for (int i=0; i<events.count; i++) {
//            IMGEvent *event = [events objectAtIndex:i];
//            IMGOfferType *offerType = [[IMGOfferType alloc]init];
//            offerType.offerId = event.eventId;
//            offerType.eventType=[NSNumber numberWithInt:1];
//            offerType.typeId = [NSNumber numberWithInt:4];
//            offerType.off = [NSNumber numberWithInt:0];
//            offerType.name = event.name;
//            [objectArray addObject:offerType];
//        }
        
        
        objectArray = [[NSMutableArray alloc] initWithCapacity:0];
        [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"select * from IMGOfferType order by typeId asc,off desc ;"] successBlock:^(FMResultSet *resultSet) {
            while([resultSet next]) {
                IMGOfferType *cuisine = [[IMGOfferType alloc]init];
                [cuisine setValueWithResultSet:resultSet];
                [objectArray addObject:cuisine];
            }
            [resultSet close];
            
            
            NSArray *events = [HomeUtil addEventToArray];
            for (int i=0; i<events.count; i++) {
                IMGEvent *event = [events objectAtIndex:i];
                IMGOfferType *offerType = [[IMGOfferType alloc]init];
                offerType.offerId = event.eventId;
                offerType.eventType=[NSNumber numberWithInt:1];
                offerType.typeId = [NSNumber numberWithInt:4];
                offerType.off = [NSNumber numberWithInt:0];
                offerType.name = event.name;
                [objectArray addObject:offerType];
            }
            
            
            [objectTableView reloadData];
            
            
            
        }failureBlock:^(NSError *error) {
            NSLog(@"CuisineListViewController initData error when get datas:%@",error.description);
        }];
        
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = L(@"Promo");
    self.view.backgroundColor = [UIColor whiteColor];
    [self loadTableView];
    [self initSortTypeArray];
    [self setBackBarButtonOffset30];

}

- (void)loadTableView{
    objectTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-44) style:UITableViewStylePlain];
    objectTableView.dataSource = self;
    objectTableView.delegate = self;
    [objectTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.view addSubview:objectTableView];
}

-(void)delayClose
{
    [self performSelector:@selector(close) withObject:nil afterDelay:0.3];
}

-(void)close
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

 


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(objectArray!=nil) {
        return [objectArray count];
    }else{
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = [[NSString alloc] initWithFormat:@"OfferTypeCell"];
    OfferTypeCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[OfferTypeCell alloc] initWithStyle:UITableViewCellStyleDefault selectionStyle:UITableViewCellSelectionStyleNone reuseIdentifier:identifier];
    }
    [cell setText:((IMGOfferType *)objectArray[indexPath.row]).name];
    IMGOfferType * offerType = [objectArray objectAtIndex:indexPath.row];
    
    [cell setChecked:[discover hasOfferType:offerType]];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return CELL_HEIGHT_45;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];

    OfferTypeCell *cell = (OfferTypeCell *)[tableView  cellForRowAtIndexPath:indexPath];
    IMGOfferType *offerType = [objectArray objectAtIndex:indexPath.row];

    [cell setChecked:![discover hasOfferType:offerType]];
    
    if (![discover hasOfferType:offerType]) {
        [discover addOfferType:offerType];
    }else{
        [discover removeOfferType:offerType];
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:SEARCH_FILTER object:nil userInfo:@{@"discover":discover,@"tempArray":offerType}];
}

@end
