//
//  SearchViewController.h
//  Qraved
//
//  Created by Jeff on 8/8/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "IMGDistrict.h"
@protocol DiscverListViewControllerDelegate <NSObject>

- (void)search:(NSNotification *)notification;

@end

@interface DiscoverListViewController : BaseChildViewController<UISearchBarDelegate,UISearchDisplayDelegate,UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,assign) BOOL isFromAppDelegate;
@property(nonatomic,retain) NSString *searchText;
@property(nonatomic,assign) BOOL isSearchJournal;
@property(nonatomic,assign) BOOL isTrending;
@property (nonatomic,assign) BOOL isFromTabMenu;
@property(nonatomic,assign)id<DiscverListViewControllerDelegate>delegate;
@end
