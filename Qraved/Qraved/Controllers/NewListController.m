//
//  NewListController.m
//  Qraved
//
//  Created by System Administrator on 9/24/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "NewListController.h"
#import "NewListView.h"
#import "ListHandler.h"
#import "UIConstants.h"
#import "UIViewController+Helper.h"
#import "MyListEmptyController.h"
#import "IMGMyList.h"
#import "NSString+Helper.h"
#import "AppDelegate.h"
#import "SelectCityController.h"

@interface NewListController ()<NewListViewDelegate>

@end
@implementation NewListController{
	NewListView *listView;
	IMGUser *user;
    BOOL isViewdidload;
}

-(void)viewDidLoad{
	[super viewDidLoad];
    self.screenName = @"My List form page";
	self.title=L(@"New List");
	self.navigationController.navigationBarHidden=NO;
	[self setBackBarButtonOffset30];
	user=[IMGUser currentUser];
	listView=[[NewListView alloc] initWithFrame:CGRectMake(0,0,DeviceWidth,DeviceHeight) withDelegate:self];
    listView.delegate =self;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(newListNameChanged:) name:UITextFieldTextDidChangeNotification object:nil];
	[self.view addSubview:listView];
    isViewdidload = YES;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (isViewdidload) {
        listView.cityLabel.text = [[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT] capitalizedString];

    }else{
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"listCityId"]) {
            
            listView.cityLabel.text = [[[NSUserDefaults standardUserDefaults] objectForKey:@"listCityname"] capitalizedString];
        }else{
            listView.cityLabel.text = [[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT] capitalizedString];
            
        }
    }
    isViewdidload = NO;

}

-(void)dealloc{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
}

-(BOOL)isValidateText:(NSString *)text {
    NSString *regex = @"[a-zA-Z0-9\\s]*";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [test evaluateWithObject:text];
}

-(void)saveBtnClick{
    if (listView.field.text.length > 100) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:L(@"Message") message:L(@"Maximum 100 characters for list name.") delegate:nil cancelButtonTitle:L(@"OK") otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
	if([self isValidateText:listView.field.text] && listView.field.text.length && ![listView.field.text isBlankString] ){
        IMGUser * currentUser = [IMGUser currentUser];
        if(currentUser.userId==nil || currentUser.token==nil){
            [[LoadingView sharedLoadingView] stopLoading];
            LoginViewController *loginVC=[[LoginViewController alloc]init];
            UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
            loginVC.autoPostDelegate = self;
            [self.navigationController presentViewController:loginNavagationController animated:YES completion:^{
                
            }];
            return;
        }
        if ([user.userId isEqualToNumber:(currentUser.userId)]) {
            user = currentUser;
        }
        [ListHandler addListWithUser:user andListName:listView.field.text andCityId:[[NSUserDefaults standardUserDefaults] objectForKey:@"listCityId"] andBlock:^(NSDictionary *dic){
			if([dic objectForKey:@"id"]){
				IMGMyList *mylist=[[IMGMyList alloc] init];
				mylist.listCount=[NSNumber numberWithInt:0];
				mylist.listId=[dic objectForKey:@"id"];
				mylist.listName=listView.field.text;
				mylist.listUserId=user.userId;
				mylist.listType=[NSNumber numberWithInt:99];
                mylist.cityId = [[NSUserDefaults standardUserDefaults] objectForKey:@"listCityId"];
                if ( (self.listDelegate != nil) && [self.listDelegate respondsToSelector:@selector(newListDelegateGoToSuccessPage:)]) {
                    [self.navigationController popViewControllerAnimated:NO];
                    [self.listDelegate newListDelegateGoToSuccessPage:mylist];
                }else{
                    MyListEmptyController *controller=[[MyListEmptyController alloc] initWithMyList:mylist];
                    [self.navigationController pushViewController:controller animated:YES];
                }
			}
        } failure:^(NSString *exceptionMsg){
            if([exceptionMsg isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
                [[AppDelegate ShareApp] goToLoginController];
            }
        }];
	}else{
		UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:L(@"Message") message:L(@"Sorry, we are not able to process your list") delegate:nil cancelButtonTitle:L(@"OK") otherButtonTitles:nil, nil];
        [alertView show];
        return;
	}
}
-(void)changeCity{
    SelectCityController *selectCityController;
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"listCityId"]) {
         selectCityController = [[SelectCityController alloc] initWithSelectedIndex:[[NSUserDefaults standardUserDefaults] objectForKey:@"listCityId"]];

    }else{
         selectCityController = [[SelectCityController alloc] initWithSelectedIndex:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID]];

    }
    selectCityController.isFromList = YES;
    [self.navigationController pushViewController:selectCityController animated:YES];
 }

-(void)textFieldDidBeginEditing:(UITextField*)field{
	
}

-(void)textFieldDidEndEditing:(UITextField*)field{
	
}

-(void)newListNameChanged:(NSNotification*)notification{
	UITextField *field=(UITextField*)notification.object;
	if(field.text.length>0){
		field.font=[UIFont systemFontOfSize:18];
		field.textColor=[UIColor color333333];
		UIBarButtonItem *saveBtn=[[UIBarButtonItem alloc] initWithTitle:L(@"Done") style:UIBarButtonItemStyleDone target:self action:@selector(saveBtnClick)];
		saveBtn.tintColor=[UIColor whiteColor];
		self.navigationItem.rightBarButtonItem=saveBtn;
	}else{
		field.font=[UIFont systemFontOfSize:18];
		self.navigationItem.rightBarButtonItem=nil;
	}
}

-(BOOL)textFieldShouldReturn:(UITextField*)field{
	if(field.text){
		[self saveBtnClick];
	}	
	return NO;
}

@end
