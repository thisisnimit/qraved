//
//  V2_DiscoverListResultFiltersDetailViewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/28.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverListResultFiltersDetailViewController.h"

@interface V2_DiscoverListResultFiltersDetailViewController ()<UISearchBarDelegate>

@property (nonatomic, strong)   UISearchBar *locationSearchBar;
@end

@implementation V2_DiscoverListResultFiltersDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor greenColor];
     [self addBackBtn];
    [self initSearchbar];
//    [self initUI];
}

- (void)initSearchbar{

    UIView *headerview= [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 50)];
    headerview.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:headerview];
    [self.view bringSubviewToFront:headerview];
    
    
    _locationSearchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(15, 5, DeviceWidth - 30 , 40)];
    _locationSearchBar.delegate = self;
    _locationSearchBar.enablesReturnKeyAutomatically=NO;
    _locationSearchBar.returnKeyType=UIReturnKeySearch;
    [headerview addSubview:_locationSearchBar];
    _locationSearchBar.showsCancelButton =NO;
    _locationSearchBar.placeholder = L(@"Search ");
    
    [[[_locationSearchBar.subviews objectAtIndex:0].subviews objectAtIndex:1] setTintColor:[UIColor lightGrayColor]];
    UIView *searchTextField = [[[_locationSearchBar.subviews firstObject]subviews]lastObject];
    searchTextField.backgroundColor =[UIColor colorWithRed:242.0/255.0 green:225.0/255.0 blue:226.0/255.0 alpha:1];
    for (UIView *subview in _locationSearchBar.subviews) {
        for(UIView* grandSonView in subview.subviews){
            if ([grandSonView isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                grandSonView.alpha = 0.0f;
            }else if([grandSonView isKindOfClass:NSClassFromString(@"UISearchBarTextField")] ){
            }else{
                grandSonView.alpha = 0.0f;
            }
        }
    }



}
@end
