//
//  MyListAddRestaurantController.h
//  Qraved
//
//  Created by System Administrator on 9/24/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "MyListDelegate.h"

@class MyListAddRestaurantView;
@class IMGMyList;


@interface MyListAddRestaurantController : BaseChildViewController <MyListDelegate>

@property(nonatomic,assign) BOOL redirectToListDetail;
@property(nonatomic,assign) BOOL isfromSlideMenu;
@property(nonatomic,assign) BOOL isfromNewList;

@property (copy, nonatomic) void (^refreshRestaurantList)();


@property(nonatomic,strong) IMGMyList *mylist;

@property(nonatomic,strong) MyListAddRestaurantView *restaurantView; 

-(instancetype)initWithMyList:(IMGMyList*)mylist;

@end
