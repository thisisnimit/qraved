//
//  SelectReviewRestaurantViewController.m
//  Qraved
//
//  Created by Laura on 14-8-15.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "SelectRestaurantViewController.h"
#import <CoreText/CoreText.h>

#import "ReviewScoreViewController.h"
#import "AddPhotoViewController2.h"
#import "DetailViewController.h"

#import "Label.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "NSNumber+Helper.h"
#import "UIImage+Helper.h"
#import "UIViewController+Helper.h"
#import "UIDevice+Util.h"
#import "RestaurantCell.h"
#import "DLStarRatingControl.h"
#import "ReviewHeaderView.h"
#import "NSString+Helper.h"

#import "DBManager.h"
//#import "MixpanelHelper.h"
#import "ReviewPublishViewController.h"

@interface SelectRestaurantViewController ()
{
    NSArray *restaurantArray;
    NSMutableArray *recommendArray;
    IMGRestaurant *currentRestaurant;
}
@end

@implementation SelectRestaurantViewController
-(id)initWithRestaurantArray:(NSArray *)restaurants{
    self = [super init];
    if (self) {
        restaurantArray=restaurants;
        recommendArray=[[NSMutableArray alloc]initWithCapacity:0];
        NSString *sqlString = [NSString stringWithFormat:@"SELECT r.* FROM IMGRestaurant r order by r.discount desc,r.offerCount desc,r.boost desc,r.ratingScore desc limit %d,%d;",0,20];
        [[DBManager manager]selectWithSql:sqlString successBlock:^(FMResultSet *resultSet) {
            while ([resultSet next]) {
                IMGRestaurant *restaurant=[[IMGRestaurant alloc]init];
                [restaurant setValueWithResultSet:resultSet];
                [recommendArray addObject:restaurant];
            }
            [resultSet close];
        }failureBlock:^(NSError *error) {
            NSLog(@"initWithRestaurantArray error when get datas:%@",error.description);
        }];
    }
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
    [super viewWillAppear:YES];
}


- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        
    self.view.backgroundColor = [UIColor blackColor];
    [self loadMainView];
}

-(void)loadMainView{
#pragma mark 设置headerView
    NSString *headerTitle;
    if(self.isWriteReview){
        headerTitle = L(@"Select restaurant to review");
    }else{
        headerTitle = L(@"Select restaurant to add photo");
    }
    ReviewHeaderView *headeView = [[ReviewHeaderView alloc]initWithTitle:headerTitle andIsNeedWriteReview:NO];
    [headeView.closeButton addTarget:self action:@selector(closeButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:headeView];
    
    if(restaurantArray.count==0){
        UIScrollView *restaurantView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, headeView.endPointY, DeviceWidth, DeviceHeight-headeView.endPointY+20)];
        restaurantView.backgroundColor = [UIColor defaultColor];
        [self.view addSubview:restaurantView];
        
        Label *noRestaurantLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 0, DeviceWidth, 51) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16] andTextColor:[UIColor color222222] andTextLines:1];
        NSString *promtStr;
        if (self.isWriteReview) {
            noRestaurantLabel.text = @"You have no restaurant to review";
            promtStr = @"Only those who honour their reservation can leave a review. Book more on Qraved to be able to review more restaurants.";
        }else{
            noRestaurantLabel.text = @"You have no restaurant to add photo";
            promtStr = @"Only users that honored their bookings can white a review. Visit this page again after you dined to upload photo.";
        }
        [restaurantView addSubview:noRestaurantLabel];
        
        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, noRestaurantLabel.endPointY, DeviceWidth, 1)];
        lineImage.backgroundColor = [UIColor colorEDEDED];
        [restaurantView addSubview:lineImage];
        
        
        CGSize expectSize = [promtStr sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12] constrainedToSize:CGSizeMake(DeviceWidth-40, 100)lineBreakMode:NSLineBreakByWordWrapping];
        Label *promtLabel = [[Label alloc]initWithFrame:CGRectMake(20, noRestaurantLabel.endPointY+20, DeviceWidth-40, expectSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12] andTextColor:[UIColor color999999] andTextLines:0];
        promtLabel.lineBreakMode = NSLineBreakByWordWrapping;
        promtLabel.text = promtStr;
        [restaurantView addSubview:promtLabel];
        
        Label *recommendLabel = [[Label alloc]initWithFrame:CGRectMake(0, promtLabel.endPointY+20, DeviceWidth, 30) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:15] andTextColor:[UIColor color222222] andTextLines:1];
        recommendLabel.textAlignment = NSTextAlignmentCenter;
        recommendLabel.text = @"Some recommendation";
        [restaurantView addSubview:recommendLabel];

        UITableView *favoritesTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, recommendLabel.endPointY+20, DeviceWidth, recommendArray.count*120) style:UITableViewStylePlain];
//        favoritesTableView.scrollEnabled = NO;
        favoritesTableView.tag = 1001;
        favoritesTableView.delegate = self;
        favoritesTableView.dataSource = self;
        [restaurantView addSubview:favoritesTableView];
        restaurantView.contentSize = CGSizeMake(DeviceWidth, recommendArray.count*120+recommendLabel.endPointY+20);
    }else{
        NSString *promtStr ;
        if (_isWriteReview){
            promtStr= @"Only those who honour their reservation can leave a review. Book more on Qraved to be able to review more restaurants.";
        }else{
            promtStr = @"Only users that honored their bookings can white a review. Visit this page again after you dined to upload photo.";
        }
        
        CGSize expectSize = [promtStr sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12] constrainedToSize:CGSizeMake(DeviceWidth-40, 100)lineBreakMode:NSLineBreakByWordWrapping];
        
        UIView *restaurantView = [[UIView alloc]initWithFrame:CGRectMake(0, headeView.endPointY, DeviceWidth, DeviceHeight-headeView.endPointY+[UIDevice heightDifference])];
        restaurantView.backgroundColor = [UIColor defaultColor];
        [self.view addSubview:restaurantView];
        
        UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, headeView.endPointY, DeviceWidth, DeviceHeight-headeView.endPointY-[UIDevice heightDifference]-expectSize.height) style:UITableViewStylePlain];
        tableView.scrollEnabled = YES;
        tableView.dataSource = self;
        tableView.delegate = self;
        tableView.tag = 1002;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:tableView];
        
        Label *promtLabel = [[Label alloc]initWithFrame:CGRectMake(20, tableView.endPointY+20, DeviceWidth-40, expectSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12] andTextColor:[UIColor color999999] andTextLines:0];
        promtLabel.text = promtStr;
        promtLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self.view addSubview:promtLabel];
    }
}

-(void)closeButtonClick
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 1001) {
        static NSString *cellIdentifier = @"recomCellIdentifier";
        RestaurantCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if(cell == nil){
            cell = [[RestaurantCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        [cell setRestaurant:recommendArray[indexPath.row] andBookDate:nil andBookTime:nil andPax:nil andRestaurantOffer:nil];
        return cell;
    }else if(tableView.tag == 1002){
        static NSString *cellIdentifier = @"CellIdentifier";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if(cell == nil){
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 52, DeviceWidth, 1)];
            lineImage.backgroundColor = [UIColor colorEDEDED];
            [cell.contentView addSubview:lineImage];
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.textColor = [UIColor color222222];
        cell.textLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16];
        if(indexPath.row==0){
            UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 1)];
            lineImage.backgroundColor = [UIColor colorEDEDED];
            [cell.contentView addSubview:lineImage];
        }
        IMGRestaurant *restaurant=[restaurantArray objectAtIndex:indexPath.row];
        cell.textLabel.text =restaurant.title;
        return cell;
    }
    
    return nil;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag == 1001){
        return recommendArray.count;
    }
    if(tableView.tag == 1002){
        return restaurantArray.count;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 1001){
        return 120.0f;
    }
    if(tableView.tag == 1002){
        return 52;
    }
    return 0;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 1001) {
        IMGRestaurant *tmpRestaurant=[recommendArray objectAtIndex:indexPath.row];
        if(tmpRestaurant!=nil){
            DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurant:tmpRestaurant];
            [self.navigationController pushViewController:detailViewController animated:YES];
        }
    }else if (tableView.tag == 1002){
        currentRestaurant = [restaurantArray objectAtIndex:indexPath.row];
        if (self.isWriteReview) {
//            ReviewScoreViewController *markingViewController = [[ReviewScoreViewController alloc]initWithRestaurant:currentRestaurant];
//            [self.navigationController pushViewController:markingViewController animated:YES];
            ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc]initWithRestaurant:currentRestaurant andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
            publishReviewVC.amplitudeType = @"HomePage";
            
            [self.navigationController pushViewController:publishReviewVC animated:YES];
        }else{
            UIImagePickerController *imagepicker = [[UIImagePickerController alloc] init];
            imagepicker.delegate = (id)self;
            imagepicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            imagepicker.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            imagepicker.allowsEditing = YES;
            [self presentViewController:imagepicker animated:YES completion:nil];
        }
    }
}

#pragma mark -ImagePickerController delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage=[info objectForKey:UIImagePickerControllerEditedImage];
    CGFloat imageHeight=0;
    CGFloat imageWidth=0;
    if(chosenImage.size.width>UPLOAD_PICTURE_SIZE_WIDTH)
    {
        imageHeight=UPLOAD_PICTURE_SIZE_WIDTH*chosenImage.size.height/chosenImage.size.width;
        imageWidth=UPLOAD_PICTURE_SIZE_WIDTH;
    }
    else
    {
        imageHeight=chosenImage.size.height;
        imageWidth=chosenImage.size.width;
    }
    
    CGSize tagetSize=CGSizeMake(imageWidth, imageHeight);
    UIImage*tagetImage=[chosenImage imageWithScaledToSize:tagetSize];
    
    [picker dismissViewControllerAnimated:NO completion:^{
        AddPhotoViewController2 *addPhotoVC = [[AddPhotoViewController2 alloc]initWithRestauarnt:currentRestaurant];
        addPhotoVC.isWriteReview = self.isWriteReview;
        addPhotoVC.selectedImage = tagetImage;
        [self.navigationController pushViewController:addPhotoVC animated:YES];
    }];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

 

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
