//
//  SelectReviewRestaurantViewController.h
//  Qraved
//
//  Created by Laura on 14-8-15.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "IMGRestaurant.h"

@interface SelectRestaurantViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,UIImagePickerControllerDelegate>
@property(nonatomic,assign)BOOL isWriteReview;
-(id)initWithRestaurantArray:(NSArray *)restaurants;
@end
