//
//  MenuPublishViewController.h
//  Qraved
//
//  Created by carl on 16/6/6.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "IMGRestaurant.h"
#import "ZYQAssetPickerController.h"
#import "AddPhotoTableViewCell.h"
#import "DLStarRatingControl.h"
#import "UIKeyboardCoView.h"
#import "CameraViewController.h"
#import "IMGUserReview.h"
#import "LoginViewController.h"

@protocol MenuPublishViewControllerDelegate <NSObject>

-(void)refreshView;

@end

@interface MenuPublishViewController : BaseViewController<UITextFieldDelegate,ZYQAssetPickerControllerDelegate,UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource,DelPhotoDelegate,AutoPostDelegate>

@property (nonatomic)       NSMutableArray *photosArrM;
@property (nonatomic,retain) UIImage *selectedImage;
@property(nonatomic,retain) NSString *amplitudeType;
@property(nonatomic,weak)id<MenuPublishViewControllerDelegate>menuPublishViewControllerDelegate;

-(id)initWithRestaurant:(IMGRestaurant *)resto;

@end
