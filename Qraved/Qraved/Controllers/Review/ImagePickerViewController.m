//
//  ImagePickerViewController.m
//  Qraved
//
//  Created by Laura on 15/3/10.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "ImagePickerViewController.h"
#import "UIView+Helper.h"
#import "GrayscaleContrastFilter.h"
#import "FilterViewController.h"
#import "UIConstants.h"
#import "UIImage+Helper.h"

@interface ImagePickerViewController ()
{
    UIImagePickerController *imagepicker;
}
@end

@implementation ImagePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    // Do any additional setup after loading the view.
    self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cancelButton setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    self.cancelButton.frame = CGRectMake(15, 20, 44, 44);
    [self.cancelButton addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    
    self.flashToggleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.flashToggleButton.frame = CGRectMake((DeviceWidth-4*44-30)/3+self.cancelButton.endPointX, 20, 44, 44);
    [self.flashToggleButton setImage:[UIImage imageNamed:@"flash-off.png"] forState:UIControlStateNormal];
    [self.flashToggleButton addTarget:self action:@selector(toggleFlash:) forControlEvents:UIControlEventTouchUpInside];
    
    self.cameraToggleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cameraToggleButton setImage:[UIImage imageNamed:@"front-camera.png"] forState:UIControlStateNormal];
    self.cameraToggleButton.frame = CGRectMake((DeviceWidth-4*44-30)/3+self.flashToggleButton.endPointX, 20, 44, 44);
    [self.cameraToggleButton addTarget:self action:@selector(switchCamera) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.nextButton.frame = CGRectMake((DeviceWidth-4*44-30)/3+self.cameraToggleButton.endPointX, 20, 44, 44);
    [self.nextButton addTarget:self action:@selector(nextClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.nextButton setTitle:L(@"Next") forState:UIControlStateNormal];
    self.nextButton.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15];
    [self.nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    
    
    self.imageView = [[GPUImageView alloc]initWithFrame:CGRectMake(0, 44, DeviceWidth, DeviceHeight-64-44)];
    [self.view addSubview:self.imageView];
    
    self.photoBar = [[UIView alloc]initWithFrame:CGRectMake(0, DeviceHeight-44, DeviceWidth, 64)];
    [self.view addSubview:self.photoBar];
    
    
    self.retakeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.retakeButton.frame = CGRectMake(11, 7, 71, 29);
    [self.retakeButton setTitle:L(@"Retake") forState:UIControlStateNormal];
    [self.retakeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.retakeButton addTarget:self action:@selector(retakePhoto:) forControlEvents:UIControlEventTouchUpInside];
    
    self.photoCaptureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.photoCaptureButton setImage:[UIImage imageNamed:@"camera-button.png"] forState:UIControlStateNormal];
    self.photoCaptureButton.frame = CGRectMake(115, 3, 90, 37);
    [self.photoCaptureButton addTarget:self action:@selector(takePhoto:) forControlEvents:UIControlEventTouchUpInside];
    
    self.localLibraryButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.localLibraryButton setImage:[UIImage imageNamed:@"filter-open.png"] forState:UIControlStateNormal];
    self.localLibraryButton.frame = CGRectMake(263, 3, 65, 37);
    [self.localLibraryButton addTarget:self action:@selector(localPhoto) forControlEvents:UIControlEventTouchUpInside];
    
    [self.photoBar addSubview:self.retakeButton];
    [self.photoBar addSubview:self.photoCaptureButton];
    [self.photoBar addSubview:self.localLibraryButton];
    
    
    [self.view addSubview:self.flashToggleButton];
    [self.view addSubview:self.cameraToggleButton];
    [self.view addSubview:self.nextButton];
    [self.view addSubview:self.cancelButton];
    
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:
                                 [UIImage imageNamed:@"micro_carbon"]];
    
    self.photoBar.backgroundColor = [UIColor colorWithPatternImage:
                                     [UIImage imageNamed:@"photo_bar"]];
    
    self.topBar.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"photo_bar"]];
    //button states
    [self.nextButton setSelected:NO];
    [self.localLibraryButton setSelected:NO];
    
    hasBlur = NO;
    
    //fill mode for video
    //self.imageView.fillMode = kGPUImageFillModePreserveAspectRatioAndFill;
    
    
    //we need a crop filter for the live video
    cropFilter = [[GPUImageCropFilter alloc] initWithCropRegion:CGRectMake(0.0f, 0.0f, 1.0f, 0.75f)];
    filter = [[GPUImageRGBFilter alloc] init];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        [self setUpCamera];
    });

    
}
-(void)localPhoto
{
    imagepicker = [[UIImagePickerController alloc] init];
    imagepicker.delegate = (id)self;
    imagepicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagepicker.allowsEditing = YES;
    [self presentViewController:imagepicker animated:YES completion:nil];
}
#pragma mark -ImagePickerController delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage=[info objectForKey:UIImagePickerControllerEditedImage];
    [self imagePicked:chosenImage];
}

-(void)imagePicked:(UIImage *)chosenImage {
    
    CGFloat imageHeight=0;
    CGFloat imageWidth=0;
    if(chosenImage.size.width>UPLOAD_PICTURE_SIZE_WIDTH){
        imageHeight=UPLOAD_PICTURE_SIZE_WIDTH*chosenImage.size.height/chosenImage.size.width;
        imageWidth=UPLOAD_PICTURE_SIZE_WIDTH;
    }else{
        imageHeight=chosenImage.size.height;
        imageWidth=chosenImage.size.width;
    }
    CGSize newSize=CGSizeMake(imageWidth, imageHeight);
    
    UIImage *targetImage=[chosenImage imageWithScaledToSize:newSize];
    
    [imagepicker dismissViewControllerAnimated:YES completion:^{
        FilterViewController *addPhotoVC = [[FilterViewController alloc]init];
        addPhotoVC.isWriteReview = self.isWriteReview;
        addPhotoVC.selectedImage = targetImage;
        [self.navigationController pushViewController:addPhotoVC animated:YES];
        
    }];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {

    [picker dismissViewControllerAnimated:YES completion:NULL];
}
-(void) setUpCamera {
    
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
        // Has camera
        
        stillCamera = [[GPUImageStillCamera alloc] initWithSessionPreset:AVCaptureSessionPresetPhoto cameraPosition:AVCaptureDevicePositionBack];
        
        stillCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
        runOnMainQueueWithoutDeadlocking(^{
            [stillCamera startCameraCapture];
            if([stillCamera.inputCamera hasFlash]){
                [self.flashToggleButton setEnabled:NO];
                [stillCamera.inputCamera lockForConfiguration:nil];
                if([stillCamera.inputCamera flashMode] == AVCaptureFlashModeOff){
                    [self.flashToggleButton setImage:[UIImage imageNamed:@"flash-off"] forState:UIControlStateNormal];
                }else if([stillCamera.inputCamera flashMode] == AVCaptureFlashModeAuto){
                    [self.flashToggleButton setImage:[UIImage imageNamed:@"flash-auto"] forState:UIControlStateNormal];
                }else{
                    [self.flashToggleButton setImage:[UIImage imageNamed:@"flash"] forState:UIControlStateNormal];
                }
                [stillCamera.inputCamera unlockForConfiguration];
                [self.flashToggleButton setEnabled:YES];
            }else{
                [self.flashToggleButton setEnabled:NO];
            }
        });
    } else {
        // No camera
        NSLog(@"No camera");
    }
    
}

-(void) removeAllTargets {
    [stillCamera removeAllTargets];
    [staticPicture removeAllTargets];
    [cropFilter removeAllTargets];
    
    //regular filter
    [filter removeAllTargets];
    
    //blur
    [blurFilter removeAllTargets];
}

-(void)toggleFlash:(UIButton *)sender{
    
    if([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]
       && stillCamera
       && [stillCamera.inputCamera hasFlash]) {
        [self.flashToggleButton setEnabled:NO];
        [stillCamera.inputCamera lockForConfiguration:nil];
        if([stillCamera.inputCamera flashMode] == AVCaptureFlashModeOff){
            [stillCamera.inputCamera setFlashMode:AVCaptureFlashModeAuto];
            [self.flashToggleButton setImage:[UIImage imageNamed:@"flash-auto"] forState:UIControlStateNormal];
        }else if([stillCamera.inputCamera flashMode] == AVCaptureFlashModeAuto){
            [stillCamera.inputCamera setFlashMode:AVCaptureFlashModeOn];
            [self.flashToggleButton setImage:[UIImage imageNamed:@"flash"] forState:UIControlStateNormal];
        }else{
            [stillCamera.inputCamera setFlashMode:AVCaptureFlashModeOff];
            [self.flashToggleButton setImage:[UIImage imageNamed:@"flash-off"] forState:UIControlStateNormal];
        }
        [stillCamera.inputCamera unlockForConfiguration];
        [self.flashToggleButton setEnabled:YES];
    }
    
}


-(void) switchCamera {
    [self.cameraToggleButton setEnabled:NO];
    [stillCamera rotateCamera];
    [self.cameraToggleButton setEnabled:YES];
}

-(void) takePhoto:(id)sender{
    [self.photoCaptureButton setEnabled:NO];
    
    if (!isStatic) {
        [stillCamera capturePhotoAsImageProcessedUpToFilter:cropFilter
                                      withCompletionHandler:^(UIImage *processed, NSError *error) {
                                          isStatic = YES;
                                          runOnMainQueueWithoutDeadlocking(^{
                                              @autoreleasepool {
                                                  [stillCamera stopCameraCapture];
                                                  [self removeAllTargets];
                                                  [self.retakeButton setHidden:NO];
                                                  [self.cameraToggleButton setEnabled:NO];
                                                  [self.flashToggleButton setEnabled:NO];
                                                  staticPicture = [[GPUImagePicture alloc] initWithImage:processed smoothlyScaleOutput:YES];
                                                  [self.photoCaptureButton setEnabled:YES];
                                                  
                                                  [self nextClicked];
                                              }
                                          });
                                      }];
        
    } else {
        GPUImageOutput<GPUImageInput> *processUpTo;
        if (hasBlur) {
            processUpTo = blurFilter;
        } else {
            processUpTo = filter;
        }
        
        [staticPicture processImage];
        
        UIImage *currentFilteredVideoFrame = [processUpTo imageFromCurrentlyProcessedOutput];
        NSDictionary *info = [[NSDictionary alloc] initWithObjectsAndKeys:
                              UIImageJPEGRepresentation(currentFilteredVideoFrame, 1), @"data", nil];
        [self.delegate imagePickerController:self didFinishPickingMediaWithInfo:info];
    }
}

-(void) retakePhoto:(UIButton *)button{
    [self.retakeButton setHidden:YES];
    staticPicture = nil;
    isStatic = NO;
    [self removeAllTargets];
    [stillCamera startCameraCapture];
    [self.cameraToggleButton setEnabled:YES];
    
    if([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]
       && stillCamera
       && [stillCamera.inputCamera hasFlash]) {
        [self.flashToggleButton setEnabled:YES];
    }
    
    [self.photoCaptureButton setImage:[UIImage imageNamed:@"camera-icon"] forState:UIControlStateNormal];
    [self.photoCaptureButton setTitle:nil forState:UIControlStateNormal];
    
    cropFilter = [[GPUImageCropFilter alloc] initWithCropRegion:CGRectMake(0.0f, 0.0f, 1.0f, 0.75f)];
}

-(void) cancel:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
    [self.delegate imagePickerControllerDidCancel:self];
}

-(void)nextClicked{
    FilterViewController *addPhotoVC = [[FilterViewController alloc]init];
    addPhotoVC .selectedImage=staticPicture.imageFromCurrentlyProcessedOutput;
    addPhotoVC.isWriteReview = self.isWriteReview;
    [self.navigationController pushViewController:addPhotoVC animated:YES];
}
-(void) toggleFilters:(UIButton *)sender{
    sender.enabled = NO;
    if (sender.selected){

    } else {

    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
