//
//  FilterViewController.h
//  Qraved
//
//  Created by Laura on 15/3/10.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"

@interface FilterViewController : BaseViewController
@property(nonatomic,assign)BOOL isWriteReview;
@property (nonatomic,retain)NSString *reviewDescription;
@property (nonatomic,retain)UIImage *selectedImage;
@end
