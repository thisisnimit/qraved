//
//  AddPhotoTableViewCell.h
//  Qraved
//
//  Created by Lucky on 15/6/24.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGUploadPhoto.h"

@protocol DelPhotoDelegate <NSObject>

- (void)delPhotoClicked:(NSInteger)tag;

@end

@interface AddPhotoTableViewCell : UITableViewCell

@property (nonatomic,retain,readonly) UIImageView *photoImageView;

@property (nonatomic,retain,readonly) UITextField *textField;

@property (nonatomic,retain,readonly) UIImageView *delBtnImageView;

@property (nonatomic,assign) id<DelPhotoDelegate>delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andIsUploadMenuPhoto:(BOOL)isUploadMenuPhoto;

- (void)setPhoto:(IMGUploadPhoto *)photo;
-(void)setTextFieldInputAccessoryView:(UIView *)view;
@end
