//
//  Photo.h
//  REPhotoCollectionControllerExample
//
//  Created by Roman Efimov on 7/27/12.
//  Copyright (c) 2012 Roman Efimov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IMGPhoto : NSObject

@property (nonatomic, strong) NSURL *thumbnailURL;
@property (nonatomic, strong) UIImage *thumbnail;
@property (nonatomic,strong) UIImage *fullScreenImage;
@property (nonatomic,strong) NSURL *fullScreenURL;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic,strong)  NSString *name;

- (id)initWithThumbnailURL:(NSURL *)thumbnailURL date:(NSDate *)date;
+ (IMGPhoto *)photoWithURLString:(NSString *)urlString date:(NSDate *)date;

@end
