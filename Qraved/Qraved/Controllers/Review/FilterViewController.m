//
//  FilterViewController.m
//  Qraved
//
//  Created by Laura on 15/3/10.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "FilterViewController.h"
#import "ReviewHeaderView.h"
#import "AddPhotoViewController2.h"
#import "UIConstants.h"
#import "Label.h"
#import "UIColor+Helper.h"
#import "GPUImage.h"
#import "GrayscaleContrastFilter.h"


@interface FilterViewController ()
{
    ReviewHeaderView *headerView;
    GPUImageView *imageView;
    GPUImageOutput<GPUImageInput> *filter;
    GPUImageCropFilter *cropFilter;
    GPUImagePicture *staticPicture;
    GPUImageOutput<GPUImageInput> *blurFilter;

    
}
@property (nonatomic, retain) UIScrollView *filterScrollView;

@end

@implementation FilterViewController
{
    BOOL isStatic;
    BOOL hasBlur;
    int selectedFilter;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 20, DeviceWidth, 44)];
    [self.view addSubview:topView];
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setImage:[UIImage imageNamed:@"close.png"] forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake(LEFTLEFTSET, 0, 44, 44);
    [cancelButton addTarget:self action:@selector(cancel:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = CGRectMake(DeviceWidth-LEFTLEFTSET-44, 0, 44, 44);
    [nextButton addTarget:self action:@selector(nextClicked) forControlEvents:UIControlEventTouchUpInside];
    [nextButton setTitle:L(@"Next") forState:UIControlStateNormal];
    nextButton.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15];
    [nextButton setTitleColor:[UIColor color222222] forState:UIControlStateNormal];
    [topView addSubview:cancelButton];
    [topView addSubview:nextButton];
    topView.backgroundColor = [UIColor whiteColor];

    Label *headeLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET+44, 0, DeviceWidth-2*(44+LEFTLEFTSET), 45) andTextFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:16] andTextColor:[UIColor color222222] andTextLines:1];
    headeLabel.textAlignment = NSTextAlignmentCenter;
    headeLabel.text = L(@"SELECT");
    [topView addSubview:headeLabel];
    
    imageView = [[GPUImageView alloc]initWithFrame:CGRectMake(0, 64, DeviceWidth, DeviceHeight-64)];
//    imageView.image = self.selectedImage;
    [self.view addSubview:imageView];
    
    self.filterScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, DeviceHeight-64, DeviceWidth, 84)];
    [self.view addSubview:self.filterScrollView];
    
    
    [self loadFilters];
    
    selectedFilter = 0;
    [self setFilter:0];
    [self prepareFilter];
}
-(void) loadFilters {
    for(int i = 0; i < 10; i++) {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"%d.jpg", i + 1]] forState:UIControlStateNormal];
        button.frame = CGRectMake(10+i*(60+10), 5.0f, 60.0f, 60.0f);
        button.layer.cornerRadius = 7.0f;
        button.layer.masksToBounds = YES;
        
        UIBezierPath *bi = [UIBezierPath bezierPathWithRoundedRect:button.bounds
                                                 byRoundingCorners:UIRectCornerAllCorners
                                                       cornerRadii:CGSizeMake(7.0,7.0)];
        
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = button.bounds;
        maskLayer.path = bi.CGPath;
        button.layer.mask = maskLayer;
        button.layer.borderWidth = 1;
        button.layer.borderColor = [[UIColor blackColor] CGColor];
        
        [button addTarget:self
                   action:@selector(filterClicked:)
         forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        [button setTitle:@"*" forState:UIControlStateSelected];
        if(i == 0){
            [button setSelected:YES];
        }
        [self.filterScrollView addSubview:button];
    }
    [self.filterScrollView setContentSize:CGSizeMake(10 + 10*(60+10), 75.0)];
}
-(void) filterClicked:(UIButton *) sender {
    for(UIView *view in self.filterScrollView.subviews){
        if([view isKindOfClass:[UIButton class]]){
            [(UIButton *)view setSelected:NO];
        }
    }
    
    [sender setSelected:YES];
    
    selectedFilter = sender.tag;
    [self setFilter:sender.tag];
    [self prepareFilter];
}

-(void) setFilter:(int) index {
    switch (index) {
        case 0:{
            filter = [[GrayscaleContrastFilter alloc] init];
        } break;
            
        case 1:{
            filter = [[GPUImageContrastFilter alloc] init];
            [(GPUImageContrastFilter *) filter setContrast:1.75];
        } break;
        case 2: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"crossprocess.acv"];
        } break;
        case 3: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"02.acv"];
        } break;
        case 4: {
            filter = [[GrayscaleContrastFilter alloc] init];
        } break;
        case 5: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"17.acv"];
        } break;
        case 6: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"aqua.acv"];
        } break;
        case 7: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"yellow-red.acv"];
        } break;
        case 8: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"06.acv"];
        } break;
        case 9: {
            filter = [[GPUImageToneCurveFilter alloc] initWithACV:@"purple-green.acv"];
        } break;
        default:
            filter = [[GPUImageRGBFilter alloc] init];
            break;
    }
}

-(void) prepareFilter {
    if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
        isStatic = YES;
    }
    
    if (!isStatic) {
        [self prepareLiveFilter];
    } else {
        [self prepareStaticFilter];
    }
}

-(void) prepareLiveFilter {
    
    [cropFilter addTarget:filter];
    //blur is terminal filter
    if (hasBlur) {
        [filter addTarget:blurFilter];
        [blurFilter addTarget:imageView];
        //regular filter is terminal
    } else {
        [filter addTarget:imageView];
    }
    
    [filter prepareForImageCapture];
    
}

-(void) prepareStaticFilter {
    
    if (!staticPicture) {
        NSLog(@"Creating new static picture");
        UIImage *inputImage = [UIImage imageNamed:@"sample1.jpg"];
        staticPicture = [[GPUImagePicture alloc] initWithImage:inputImage smoothlyScaleOutput:YES];
    }
    
    [staticPicture addTarget:filter];
    
    // blur is terminal filter
    if (hasBlur) {
        [filter addTarget:blurFilter];
        [blurFilter addTarget:imageView];
        //regular filter is terminal
    } else {
        [filter addTarget:imageView];
    }
    
    [staticPicture processImage];
    
    
}


-(void)nextClicked{
    AddPhotoViewController2 *addPhotoVC = [[AddPhotoViewController2 alloc]init];
    addPhotoVC.selectedImage = self.selectedImage;
    addPhotoVC.isWriteReview = self.isWriteReview;
    [self.navigationController pushViewController:addPhotoVC animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
