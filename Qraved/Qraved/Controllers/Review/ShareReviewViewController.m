//
//  ShareReviewViewController.m
//  Qraved
//
//  Created by Laura on 14-8-18.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "ShareReviewViewController.h"

#import "AppDelegate.h"
#import "UIColor+Helper.h"
#import "Label.h"
#import "UIView+Helper.h"
#import "UIViewController+Helper.h"
#import "UIImage+Helper.h"
//#import "MixpanelHelper.h"
#import "ReviewHeaderView.h"
#import <MessageUI/MessageUI.h>
#import "LoadingView.h"
#import "IMGUser.h"

@interface ShareReviewViewController ()<MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate>

@end

@implementation ShareReviewViewController{
    IMGUserReview *review;

}
-(id)initWithRestaurantTitle:(NSString *)restaurantTitle andReviewID:(int)reviewID_{
    if (self = [super init]) {
        review = [[IMGUserReview alloc] init];
        review.restaurantTitle = restaurantTitle;
        review.reviewId = [NSNumber numberWithInt:reviewID_];
    }
    return self;
}

-(id)initWithRestaurant:(IMGUserReview *)review_{
    self = [super init];
    if(self){
        review = [[IMGUserReview alloc] init];

        review = review_;
    }
    return self;
}
-(id)initWithRestaurant:(IMGRestaurant *)restaurant_ andReviewID:(int)reviewID_{
    self = [super init];
    if (self) {
        review.restaurantTitle = restaurant_.title;
        review.reviewId=[NSNumber numberWithInt:reviewID_];
        self.restaurant_ = restaurant_;
    }
    return self;
}

-(id)initWithRestaurant:(IMGRestaurant *)restaurant_ anPhotoIDs:(NSMutableString *)photoIDs{

    self = [super init];
    if (self) {
        self.restaurant_ = restaurant_;
        
        NSArray *arr = [photoIDs componentsSeparatedByString:@","];
        
        self.firstPhotoID = arr.firstObject;
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    [super viewWillAppear:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor blackColor];
    

    [self loadMainView];
}
-(void)loadMainView
{
#pragma mark 设置headerView
    NSString *headerTitle;
    if (self.isWriteReview) {
        headerTitle = L(@"Share your review");
    }
    else
    {
        headerTitle = L(@"Share your photo");
    }
    
    self.navigationItem.title = headerTitle;
    
    UIBarButtonItem *rightBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ReviewClosed"] style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonClick)];
    rightBtn.tintColor=[UIColor blackColor];
    self.navigationItem.rightBarButtonItem=rightBtn;
    
  
//    ReviewHeaderView *headeView = [[ReviewHeaderView alloc]initWithTitle:headerTitle andIsNeedWriteReview:NO];
//    [headeView.closeButton addTarget:self action:@selector(closeButtonClick) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:headeView];
    
    UIView *shareListView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+20)];
    shareListView.backgroundColor = [UIColor defaultColor];
    [self.view addSubview:shareListView];
    
    NSArray *imagesArr = @[@"facebook.png",@"twitter.png",@"message.png",@"email.png"];
    NSArray *titlesArr = @[L(@"Facebook"),L(@"Twitter"),L(@"Message"),L(@"Email")];
    
    for (int i=0;i<imagesArr.count; i++) {
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 12+54*i, 30, 30)];
        imageView.image = [UIImage imageNamed:[imagesArr objectAtIndex:i]];
        [shareListView addSubview:imageView];
        
        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, imageView.endPointY+12, DeviceWidth, 1)];
        lineImage.backgroundColor = [UIColor colorEDEDED];
        [shareListView addSubview:lineImage];
        
        Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(imageView.endPointX+20, 12+54*i, DeviceWidth-imageView.endPointX, 30) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16] andTextColor:[UIColor color222222] andTextLines:1];
        titleLabel.text = [titlesArr objectAtIndex:i];
        [shareListView addSubview:titleLabel];
        
        UIControl *control = [[UIControl alloc]initWithFrame:CGRectMake(0, 54*i, DeviceWidth, 54)];
        control.tag = 1000+i;
        [control addTarget:self action:@selector(gotoShare:) forControlEvents:UIControlEventTouchUpInside];
        [shareListView addSubview:control];
    }
    
}
-(void)gotoShare:(UIControl*)control
{
    switch (control.tag) {
        case 1000:
        {//facebook
            [self shareWithFacebook];
        }
            break;
            
        case 1001:
        {//twitter
            [self shareWithTwitter];
        }
            break;
        case 1002:
        {//message
            [self shareWithSMS];
        }
            break;
        case 1003:
        {//email
            [self shareWithEmail];
        }
            break;
        default:
            break;
    }
}
-(void)shareWithTwitter{
    IMGUser *user = [IMGUser currentUser];
    [[LoadingView sharedLoadingView]startLoading];
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
    [eventProperties setValue:review.userFullName forKey:@"ReviewerName"];
    [eventProperties setValue: _restaurant_.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:@"share own review" forKey:@"Location"];
    [eventProperties setValue:@"Channel" forKey:@"Twitter"];
    [[Amplitude instance] logEvent:@"SH - Share Review" withEventProperties:eventProperties];

    NSString *shareContentString ;
     NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:L(@"%@post/photo/%@"),QRAVED_WEB_SERVER_OLD,self.firstPhotoID]];
    SLComposeViewController *slComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    if(self.isWriteReview){
        shareContentString = [NSString stringWithFormat:L(@"Check out %@ review at %@ on #Qraved!"),user.fullName,review.restaurantTitle];
        url = [NSURL URLWithString:[NSString stringWithFormat:L(@"%@post/review/%@"),QRAVED_WEB_SERVER_OLD,review.reviewId]];
        [slComposerSheet addURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=TwitterReview",url]]];
    }else{
       
        if (self.isUploadMenuPhoto)
        {
            shareContentString = [NSString stringWithFormat:L(@"Check out the following menu at %@"),self.restaurant_.title];
        }
        else
            shareContentString = [NSString stringWithFormat:L(@"Check out this photo at %@ on Qraved. %@"),self.restaurant_.title,url];
            [slComposerSheet addURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=TwitterPhoto",url]]];
    }

    [slComposerSheet setInitialText:shareContentString];
//    [slComposerSheet addURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@post/review/%@?shareType=TwitterReview",QRAVED_WEB_SERVER,review.reviewId]]];
    

    if(slComposerSheet){
        [self presentViewController:slComposerSheet animated:YES completion:^{
            [[LoadingView sharedLoadingView]stopLoading];
        }];
        [slComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            NSString *output;
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    output = L(@"Action Cancelled");
                    break;
                case SLComposeViewControllerResultDone:{
                    output = L(@"Post Successful");
//                    [MixpanelHelper trackShareToTwitterWithshareObject:@"Share Review" andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
                }
                    break;
                default:
                    break;
            }
            if (result != SLComposeViewControllerResultCancelled){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter Message" message:output delegate:nil cancelButtonTitle:L(@"Ok") otherButtonTitles:nil];
                [alert show];
            }
        }];
    }
}

-(void)shareWithFacebook{
    IMGUser *user = [IMGUser currentUser];
    [[LoadingView sharedLoadingView]startLoading];
    
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
    [eventProperties setValue:review.userFullName forKey:@"ReviewerName"];
    [eventProperties setValue: _restaurant_.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:@"share own review" forKey:@"Location"];
    [eventProperties setValue:@"Channel" forKey:@"Facebook"];
    [[Amplitude instance] logEvent:@"SH - Share Review" withEventProperties:eventProperties];
    
    NSString *shareContentString ;
    NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:L(@"%@post/photo/%@"),QRAVED_WEB_SERVER_OLD,self.firstPhotoID]];
    SLComposeViewController *slComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    if(self.isWriteReview){
        shareContentString = [NSString stringWithFormat:L(@"%@\n%@ post review on Qraved\n%@"),user.fullName,user.fullName,self.reviewDescription];
        url = [NSURL URLWithString:[NSString stringWithFormat:L(@"%@post/review/%@"),QRAVED_WEB_SERVER_OLD,review.reviewId]];
        [slComposerSheet addURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=FacebookReview",url]]];
    }else{
        if (self.isUploadMenuPhoto)
        {
            
            shareContentString = [NSString stringWithFormat:L(@"Check out the following menu at %@"),self.restaurant_.title];
            
        }
        else
            shareContentString = [NSString stringWithFormat:L(@"Check out this photo at %@ on Qraved. %@"),self.restaurant_.title,url];
            [slComposerSheet addURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@?shareType=FacebookPhoto",url]]];
    }

    [slComposerSheet setInitialText:shareContentString];

    if(slComposerSheet){
        [self presentViewController:slComposerSheet animated:YES completion:^{
            [[LoadingView sharedLoadingView]stopLoading];
        }];
        
        [slComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result){
            NSString *output;
            switch(result){
                case SLComposeViewControllerResultCancelled:
                    output = L(@"Action Cancelled");
                    break;
                case SLComposeViewControllerResultDone:{
                    output = L(@"Post Successful");
//                    [MixpanelHelper trackShareToFacebookWithshareObject:@"Share Review" andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
                }
                    break;
                default:
                    break;
            }
            if (result != SLComposeViewControllerResultCancelled){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook Message" message:output delegate:nil cancelButtonTitle:L(@"Ok") otherButtonTitles:nil];
                [alert show];
            }
        }];
    }
}

-(void)shareWithEmail{
    if ([MFMailComposeViewController canSendMail]){
        IMGUser *user = [IMGUser currentUser];
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
        [eventProperties setValue:review.userFullName forKey:@"ReviewerName"];
        [eventProperties setValue: _restaurant_.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:@"share own review" forKey:@"Location"];
        [eventProperties setValue:@"Channel" forKey:@"Email"];
        [[Amplitude instance] logEvent:@"SH - Share Review" withEventProperties:eventProperties];

        [[LoadingView sharedLoadingView]startLoading];
        NSString *subjectString;
        NSString *shareContentString;
        if(self.isWriteReview){
            subjectString = [NSString stringWithFormat:@"CHECK IT OUT NOW! A Restaurant Review by %@ on Qraved",user.fullName];
            shareContentString = [NSString stringWithFormat:@"<html><body> \
                                  Hi, <br /> \
                                  Check out %@ review at %@ on Qraved!<br /> \
                                  <a href=\"%@\">%@</a></body></html>",user.fullName,review.restaurantTitle,[NSString stringWithFormat:@"%@post/review/%@",QRAVED_WEB_SERVER_OLD,review.reviewId],[NSString stringWithFormat:@"%@post/review/%@",QRAVED_WEB_SERVER_OLD,review.reviewId]];
        }else{
            if (self.isUploadMenuPhoto)
            {
//                NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/menu/%@",QRAVED_WEB_SERVER,self.menuID]];
//                subjectString = @"Let’s find out great menu on Qraved";
//                shareContentString = [NSString stringWithFormat:@"<html><body> \
//                        Hi\n  <br /> \
//                        Check out this menu at %@ on Qraved!<br />\
//                        <a href=\"%@\">%@</a></body></html>",self.restaurant_.title,url];

            }
            else
            {
                NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/photo/%@",QRAVED_WEB_SERVER_OLD,self.firstPhotoID]];
                subjectString = [NSString stringWithFormat:L(@"CHECK IT OUT NOW! A Restaurant Photo by %@ on Qraved!"),user.fullName];
                shareContentString = [NSString stringWithFormat:@"<html><body> \
                                      Hi\n  <br /> \
                                      Check out %@ photo at %@ on Qraved<br /> \
                                      <a href=\"%@\">%@</a></body></html>",user.fullName,self.restaurant_.title,url,url];
            }
        }
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:subjectString];
        [mailer setMessageBody:shareContentString isHTML:YES];
        [self presentViewController:mailer animated:YES completion:^{
            [[LoadingView sharedLoadingView]stopLoading];
        }];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Oops! This device does not support email."
                                                       delegate:nil
                                              cancelButtonTitle:L(@"OK")
                                              otherButtonTitles: nil];
        [alert show];
    }
}

-(void)shareWithSMS{
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Oops! This device does not support sms."
                                                       delegate:nil
                                              cancelButtonTitle:L(@"OK")
                                              otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    IMGUser *user = [IMGUser currentUser];
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
    [eventProperties setValue:review.userFullName forKey:@"ReviewerName"];
    [eventProperties setValue: _restaurant_.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:@"share own review" forKey:@"Location"];
    [eventProperties setValue:@"Channel" forKey:@"SMS"];
    [[Amplitude instance] logEvent:@"SH - Share Review" withEventProperties:eventProperties];

    NSString *shareContentString ;
    if(self.isWriteReview){
        shareContentString = [NSString stringWithFormat:@"Hi,\nCheck out %@ review at %@ on Qraved!\n%@",user.fullName,review.restaurantTitle,[NSString stringWithFormat:@"%@post/review/%@",QRAVED_WEB_SERVER_OLD,review.reviewId]];
    }else{
//        shareContentString = [NSString stringWithFormat:@"%@\n%@ post photo on Qraved. Check out at %@",user.fullName,user.fullName,[NSString stringWithFormat:@"%@%@/%@",QRAVED_WEB_SERVER,[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],restaurant.seoKeyword]];
        
         NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/photo/%@",QRAVED_WEB_SERVER_OLD,self.firstPhotoID]];
        shareContentString = [NSString stringWithFormat:L(@"Hi\nCheck out this photo at %@ on Qraved!\n%@"),self.restaurant_.title,url];
    }
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setBody:shareContentString];
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:{
//            [MixpanelHelper trackShareToEmailWithshareObject:@"Share Review" andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
        }
            break;
        case MFMailComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:L(@"Failure") message:L(@"Failed to send E-Mail!") delegate:nil cancelButtonTitle:L(@"OK") otherButtonTitles:nil];
            [warningAlert show];
        }
            break;
            
        default:
            break;
    }
    [self back];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:L(@"Failure") message:L(@"Failed to send SMS!") delegate:nil cancelButtonTitle:L(@"OK") otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:{
//            [MixpanelHelper trackShareToSMSWithshareObject:@"Share Review" andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
        }
            break;
            
        default:
            break;
    }
    
    [self back];
}

-(void)closeButtonClick
{
    if (self.isFromOnboard) {
        self.view.backgroundColor = [UIColor blackColor];
        for (UIView *view in self.view.subviews)
        {
            [view removeFromSuperview];
        }
//        [self dismissViewControllerAnimated:NO completion:nil];
        [AppDelegate ShareApp].window.rootViewController = [AppDelegate ShareApp].tabMenuViewController;
    }
    else
        [self dismissViewControllerAnimated:YES completion:nil];
        
//        [self.navigationController popToRootViewControllerAnimated:NO];
//        [[AppDelegate ShareApp] gotoHomePage];
}
- (void)back
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


 

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
