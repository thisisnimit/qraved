//  PublishReviewViewController.h
//  Qraved
//
//  Created by Laura on 14-8-18.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "IMGRestaurant.h"
#import "ZYQAssetPickerController.h"
#import "AddPhotoTableViewCell.h"
#import "DLStarRatingControl.h"
#import "CameraViewController.h"
#import "IMGUserReview.h"
#import "LoginViewController.h"
@protocol ReviewPublishViewControllerDelegate

- (void)writeReviewReloadUploadPhotoCell:(id)reviewPublishViewController;
-(void)reloadPhotosImageView:(NSArray*)dishArr;
@end


@interface ReviewPublishViewController : BaseViewController<UITextViewDelegate,UITextFieldDelegate,ZYQAssetPickerControllerDelegate,UINavigationControllerDelegate,UITableViewDelegate,UITableViewDataSource,DelPhotoDelegate,UIScrollViewDelegate,DLStarRatingDelegate,ReviewPublishViewDelegate,UISearchBarDelegate,AutoPostDelegate>

@property (nonatomic)       NSMutableArray *photosArrM;

@property (nonatomic,retain) UIImage *selectedImage;

@property (nonatomic,assign) BOOL isUploadPhoto;
@property (nonatomic,assign) BOOL isUpdateReview;
@property (nonatomic,assign) BOOL isCanSearch;
@property (nonatomic,assign) BOOL isFromDetail;
@property (nonatomic,assign) BOOL isFromOnboard;
@property (nonatomic,assign) BOOL isFromJourney;
@property (nonatomic,assign) BOOL isFromHotkey;
@property (nonatomic,assign) BOOL isFromDraft;
@property(nonatomic,strong) NSNumber *uploadedPhotoId;
@property (nonatomic,assign) BOOL ifSuccessLoginClickPost;
@property (nonatomic,assign) BOOL isEdit;
@property (nonatomic,assign) BOOL isEditReview;
@property (nonatomic,assign) NSIndexPath *fromCellIndexPath;
@property (nonatomic,strong)        IMGUserReview *userReview;
@property(nonatomic,retain) NSString *amplitudeType;
@property (nonatomic,weak) id<ReviewPublishViewControllerDelegate>reviewPublishViewControllerDelegate;

@property(nonatomic,copy) NSString *reviewTitle;
@property(nonatomic,copy) NSString *reviewContent;
@property(nonatomic,assign)float overallRatings;
@property (nonatomic,strong)    NSIndexPath *jnCellIndexPath;
@property(nonatomic,strong) NSNumber *reviewId;

@property (nonatomic, strong) NSMutableArray *restaurantArr;

-(id)initWithRestaurant:(IMGRestaurant *)resto andOverallRating:(float)rating0 andCuisineRating:(float)rating1 andAmbientRating:(float)rating2 andServiceRating:(float)rating3;


@property (nonatomic,assign) BOOL isFromProfile;

@end
