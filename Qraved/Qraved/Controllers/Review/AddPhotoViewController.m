//
//  AddPhotoViewController.m
//  Qraved
//
//  Created by Laura on 14-8-18.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "AddPhotoViewController.h"

#import "ShareReviewViewController.h"

#import "UIColor+Helper.h"
#import "Label.h"
#import "UIView+Helper.h"
#import "UIViewController+Helper.h"
#import "UIImage+Helper.h"
#import "ShareReviewViewController.h"
#import "Helper.h"
#import "DBManager.h"
#import "UIConstants.h"
#import "PostDataHandler.h"
#import "ReviewHeaderView.h"
#import "LoadingView.h"
#import "IMGUser.h"
#import "AppDelegate.h"
#import "IMGDish.h"
//#import "MixpanelHelper.h"
#import "IMGUploadPhoto.h"
#import "AddPhotoTableViewCell.h"
#import "ZYQAssetPickerController.h"
#import "UIKeyboardCoView.h"

@interface AddPhotoViewController ()<UITextFieldDelegate,DelPhotoDelegate,ZYQAssetPickerControllerDelegate,UINavigationControllerDelegate,UIKeyboardCoViewDelegate>

@end
@implementation AddPhotoViewController{
    ReviewHeaderView *headerView;
    UITableView *photoView;
    UILabel *promptLabel;
    IMGRestaurant *restaurant;
    NSInteger replaceIndex;
    BOOL replaceImageView;
    int reviewId;
    NSInteger selectIndex;
    UIImagePickerController *imagePicker;
    UIButton *sendButton;
}

-(id)initWithRestauarnt:(IMGRestaurant *)resto{
    self=[super init];
    if(self){
        restaurant=resto;
        self.photoArray = [[NSMutableArray alloc]init];
    }
    return self;
}

-(id)initWithRestauarnt:(IMGRestaurant *)resto andReviewId:(int)tmpReviewId{
    self=[super init];
    if(self){
        restaurant=resto;
        reviewId=tmpReviewId;
    }
    return self;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(savePhoto:) name:@"savePhotoFromCameraNotification" object:nil];

    self.navigationController.navigationBarHidden = YES;
    self.view.backgroundColor = [UIColor color333333];
    
    [self loadMainView];
    
}
-(void)loadMainView
{
#pragma mark 设置headerView
    headerView = [[ReviewHeaderView alloc]initWithTitle:restaurant.title andIsNeedWriteReview:YES];
    headerView.closeButton.tag = 100;
    [headerView.closeButton addTarget:self action:@selector(skipButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [headerView.postButton addTarget:self action:@selector(skipButtonClick:) forControlEvents:UIControlEventTouchUpInside];

    [self.view addSubview:headerView];
    
    if (self.selectedImage) {
        IMGUploadPhoto *uploadPhoto = [[IMGUploadPhoto alloc]init];
        uploadPhoto.image = self.selectedImage;
        [self.photoArray addObject:uploadPhoto];
    }
    
    photoView = [[UITableView alloc]initWithFrame:CGRectMake(0, headerView.endPointY, DeviceWidth, DeviceHeight - 35 +20 -64) style:UITableViewStylePlain];
    photoView.delegate = self;
    photoView.dataSource = self;
    photoView.separatorStyle = UITableViewCellSeparatorStyleNone;
    photoView.backgroundColor = [UIColor defaultColor];
    [self.view addSubview:photoView];
    
    UIView *footView = [[UIView alloc] init];
    footView.frame = CGRectMake(0, 0, DeviceWidth, 180);
    photoView.tableFooterView = footView;
    
    UIKeyboardCoView *btnsView = [[UIKeyboardCoView alloc] init];
    btnsView.delegate = self;
    btnsView.backgroundColor = [UIColor colorWithRed:241/255.0 green:241/255.0 blue:241/255.0 alpha:1];
    btnsView.frame = CGRectMake(0, photoView.endPointY, DeviceWidth, DeviceHeight - photoView.endPointY + 20);
    [self.view addSubview:btnsView];
    
    UIButton *addPhotoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [addPhotoBtn setImage:[UIImage imageNamed:@"upload-photo"] forState:UIControlStateNormal];
    addPhotoBtn.frame = CGRectMake(22, btnsView.frame.size.height - 27, 22, 18);
    [btnsView addSubview:addPhotoBtn];
    [addPhotoBtn addTarget:self action:@selector(addPhotoClick) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)addPhotoClick
{
    ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] initWithRestaurantTitle:restaurant.title andIsFromUploadPhoto:NO andIsUploadMenuPhoto:NO andCurrentRestaurantId:restaurant.restaurantId];
    picker.maximumNumberOfSelection = 9;
    picker.assetsFilter = [ALAssetsFilter allPhotos];
    picker.showEmptyGroups=NO;
    picker.delegate = self;
    picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
            NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
            return duration >= 5;
        } else {
            return YES;
        }
    }];
    //    [self.navigationController pushViewController:picker animated:YES];
    
    [self presentViewController:picker animated:YES completion:NULL];
}


-(void)uploadDishePhotos:(IMGUser *)user currentIndex:(NSInteger)currentIndex maxIndex:(NSInteger)maxIndex{
    PostDataHandler *postDataHandler=[[PostDataHandler alloc]init];
    [[LoadingView sharedLoadingView]startLoading];
    IMGUploadPhoto *uploadPhoto = [self.photoArray objectAtIndex:currentIndex];
    
    
    NSString *dishTitle=uploadPhoto.title;
    NSString *descriptionStr=uploadPhoto.descriptionStr;
    NSNumber *menuId=[NSNumber numberWithInt:0];
    [postDataHandler postImage:uploadPhoto.image andTitle:@"image" andDescription:@"image" andSuccessBlock:^(id postArray){
        NSString *imageUrl=[postArray objectForKey:@"imgPath"];
        NSDictionary *parameters=@{@"t": user.token,@"userID":user.userId,@"restaurantID":restaurant.restaurantId,@"dishTitle":(dishTitle && dishTitle.length>0)?dishTitle:@"",@"imageUrl":imageUrl,@"description":(descriptionStr && descriptionStr.length>0)?descriptionStr:@"",@"menuID":menuId,@"reviewId":[NSNumber numberWithInt:reviewId]};

        [self requestToAddDish:parameters currentIndex:currentIndex maxIndex:maxIndex user:user];
    }andFailedBlock:^(NSError *error){
        NSLog(@"upload photo failed, error:%@",error.description);
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:restaurant.restaurantId forKey:@"RestaurantID"];
        [eventProperties setValue:restaurant.title forKey:@"RestaurantTitle"];
        [eventProperties setValue:[NSString stringWithFormat:@"%@",error.description] forKey:@"Reason"];
        [eventProperties setValue:user.userId forKey:@"UploaderUser_ID"];
        [eventProperties setValue:uploadPhoto.dishId forKey:@"Photo_ID"];
        [[Amplitude instance] logEvent:@"UC - Upload Photo Failed" withEventProperties:eventProperties];
        
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

-(void)requestToAddDish:(NSDictionary *)parameters  currentIndex:(NSInteger)currentIndex maxIndex:(NSInteger)maxIndex user:(IMGUser *)user{
    [(NSMutableDictionary *)parameters setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [(NSMutableDictionary *)parameters setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    [[IMGNetWork sharedManager]POST:@"uploadDish" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSString *returnStatusString = [responseObject objectForKey:@"status"];
        NSString * exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp]goToLoginController];
            return;
        }else if([returnStatusString isEqualToString:@"succeed"]){
            NSDictionary *dishDictionary = [responseObject objectForKey:@"dish"];
            if(dishDictionary!=nil){
                IMGDish *dish = [[IMGDish alloc]init];
                [dish setValuesForKeysWithDictionary:dishDictionary];
                [[DBManager manager]insertModel:dish selectField:@"dishId" andSelectID:dish.dishId];
//                [MixpanelHelper trackUploadPhoto:restaurant andDishName:dish.title];
            }
            
            [[LoadingView sharedLoadingView] stopLoading];
            if(currentIndex<maxIndex){
                [self uploadDishePhotos:user currentIndex:currentIndex+1 maxIndex:maxIndex];
            }else{
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_HOME object:nil];
                if (self.navigationController) {
                    ShareReviewViewController *shareReviewViewController = [[ShareReviewViewController alloc]initWithRestaurant:restaurant andReviewID:reviewId];
                    shareReviewViewController.isWriteReview = self.isWriteReview;
                    shareReviewViewController.reviewDescription = self.reviewDescription;
                    [self.navigationController pushViewController:shareReviewViewController animated:YES];
                }
                else
                {
                    [self closeButtonClick];
                }
            }
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        [[LoadingView sharedLoadingView] stopLoading];
        NSLog(@"requestToAddDish error: %@",error.localizedDescription);
    }];
}


-(void)closeButtonClick
{
    [self dismissViewControllerAnimated:YES completion:nil];

}

-(void)skipButtonClick:(id)sender
{
    UIButton *doneButton = (UIButton*)sender;
    if (doneButton.tag == 100)
    {
        ShareReviewViewController *shareReviewViewController = [[ShareReviewViewController alloc]initWithRestaurant:restaurant andReviewID:reviewId];
        shareReviewViewController.isWriteReview = self.isWriteReview;
        shareReviewViewController.reviewDescription = self.reviewDescription;
        [self.navigationController pushViewController:shareReviewViewController animated:YES];
    }
    else
    {
        doneButton.enabled=FALSE;
        for (int i=0; i<self.photoArray.count; i++) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            AddPhotoTableViewCell *cell = (AddPhotoTableViewCell *)[photoView cellForRowAtIndexPath:indexPath];
            [cell.textField resignFirstResponder];
            
            IMGUploadPhoto *uploadPhoto = [self.photoArray objectAtIndex:indexPath.row];
            uploadPhoto.title = (cell.textField.text && cell.textField.text.length>0)?cell.textField.text:@"";
            [self.photoArray replaceObjectAtIndex:indexPath.row withObject:uploadPhoto];
        }
        
        IMGUser *user = [IMGUser currentUser];
        if(user.userId==nil){
            [[AppDelegate ShareApp]goToLoginController];
            return;
        }
        if(user.token==nil){
            [[AppDelegate ShareApp]goToLoginController];
            return;
        }
        if(self.photoArray==nil||self.photoArray.count==0){
            ShareReviewViewController *shareReviewViewController = [[ShareReviewViewController alloc]initWithRestaurant:restaurant andReviewID:reviewId];
            shareReviewViewController.isWriteReview = self.isWriteReview;
            shareReviewViewController.reviewDescription = self.reviewDescription;
            [self.navigationController pushViewController:shareReviewViewController animated:YES];
            //[self closeButtonClick];
        }else{
            [self uploadDishePhotos:user currentIndex:0 maxIndex:self.photoArray.count-1];
        }
    }
}


#pragma mark -ImagePickerController delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
//    UIImage *originalImage=[info objectForKey:UIImagePickerControllerOriginalImage];
    UIImage *chosenImage=[info objectForKey:UIImagePickerControllerEditedImage];
    
//    if(originalImage.size.height!=chosenImage.size.height || originalImage.size.width!=chosenImage.size.width){
//        UIImageWriteToSavedPhotosAlbum(chosenImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
//    }else{
        [self imagePicked:chosenImage];
//    }
    
//    NSValue *a = [info objectForKey:UIImagePickerControllerCropRect];
//
//    NSLog(@"a is here");
}

-(void)imagePicked:(UIImage *)chosenImage {
    
    CGFloat imageHeight=0;
    CGFloat imageWidth=0;
    if(chosenImage.size.width>UPLOAD_PICTURE_SIZE_WIDTH){
        imageHeight=UPLOAD_PICTURE_SIZE_WIDTH*chosenImage.size.height/chosenImage.size.width;
        imageWidth=UPLOAD_PICTURE_SIZE_WIDTH;
    }else{
        imageHeight=chosenImage.size.height;
        imageWidth=chosenImage.size.width;
    }
    CGSize newSize=CGSizeMake(imageWidth, imageHeight);
    
    UIImage *targetImage=[chosenImage imageWithScaledToSize:newSize];
    
    if (replaceImageView) {
        IMGUploadPhoto *uploadPhoto = [self.photoArray objectAtIndex:replaceIndex];
        uploadPhoto.image = targetImage;
        [self.photoArray replaceObjectAtIndex:replaceIndex withObject:uploadPhoto];
    }else{
        IMGUploadPhoto *uploadPhoto = [[IMGUploadPhoto alloc]init];
        uploadPhoto.image = targetImage;
        [self.photoArray addObject:uploadPhoto];
    }
    replaceImageView = NO;
    [imagePicker dismissViewControllerAnimated:YES completion:NULL];
    
    if (self.photoArray.count == 0)
    {
        sendButton.userInteractionEnabled = NO;
        sendButton.alpha = 0.4f;
    }
    else
    {
        sendButton.userInteractionEnabled = YES;
        sendButton.alpha = 1.0f;
    }
    
    [photoView reloadData];
}

-(void)imagePicked:(UIImage *)chosenImage size:(CGSize)newSize point:(CGPoint)newPoint{
    
//    CGFloat imageHeight=0;
//    CGFloat imageWidth=0;
//    if(chosenImage.size.width>UPLOAD_PICTURE_SIZE_WIDTH){
//        imageHeight=UPLOAD_PICTURE_SIZE_WIDTH*chosenImage.size.height/chosenImage.size.width;
//        imageWidth=UPLOAD_PICTURE_SIZE_WIDTH;
//    }else{
//        imageHeight=chosenImage.size.height;
//        imageWidth=chosenImage.size.width;
//    }
//    CGSize tagetSize=CGSizeMake(imageWidth, imageHeight);
    
    UIImage *targetImage=[chosenImage imageWithScaledToSize:newSize fromOrigin:newPoint];
    
    
    if (replaceImageView) {
        IMGUploadPhoto *uploadPhoto = [self.photoArray objectAtIndex:replaceIndex];
        uploadPhoto.image = targetImage;
        [self.photoArray replaceObjectAtIndex:replaceIndex withObject:uploadPhoto];
    }else{
        IMGUploadPhoto *uploadPhoto = [[IMGUploadPhoto alloc]init];
        uploadPhoto.image = targetImage;
        [self.photoArray addObject:uploadPhoto];
    }
    replaceImageView = NO;
    [imagePicker dismissViewControllerAnimated:YES completion:NULL];
    
    if (self.photoArray.count == 0)
    {
        sendButton.userInteractionEnabled = NO;
        sendButton.alpha = 0.4f;
    }
    else
    {
        sendButton.userInteractionEnabled = YES;
        sendButton.alpha = 1.0f;
    }
    
    [photoView reloadData];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    replaceImageView = NO;
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark textFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField.tag<100)
    {
        for (IMGUploadPhoto *photo in _photoArray)
        {
            if (photo.tag == textField.tag)
            {
                //_currentCellTag = textField.tag;
                photo.descriptionStr = toBeString;
                break;
            }
        }
        
    }
    return YES;
}

#pragma mark tableViewDataDelegate
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier =@"cell";
    AddPhotoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[AddPhotoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle=UITableViewCellSelectionStyleGray;
    }
    IMGUploadPhoto *photo = [self.photoArray objectAtIndex:indexPath.row];
    photo.tag = indexPath.row;
    cell.textField.delegate = self;
    cell.delegate = self;
    cell.textField.tag = indexPath.row;
    [cell setPhoto:photo];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //cell.cellBtn.tag = indexPath.row;
    //[cell.cellBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

#pragma mark tableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.photoArray.count;
}
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.row == self.photoArray.count) {
//        return 260.0f;
//    }
//    return 110.;
//}

- (void)delPhotoClicked:(NSInteger)tag
{
    
    for (IMGUploadPhoto *photo in self.photoArray)
    {
        if (photo.tag == tag)
        {
            [self.photoArray removeObject:photo];
            if (self.photoArray.count == 0)
            {
                sendButton.userInteractionEnabled = NO;
                sendButton.alpha = 0.4f;
            }
            else
            {
                sendButton.userInteractionEnabled = YES;
                sendButton.alpha = 1.0f;
            }
            [photoView reloadData];
            break;
        }
    }
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IMGUploadPhoto *photo = [_photoArray objectAtIndex:indexPath.row];
    CGSize size = CGSizeFromString(NSStringFromCGSize(photo.image.size));
    CGFloat height = size.height/(size.width/DeviceWidth);
    return height + 50;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

#pragma mark scrollViewDelegate
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    for (int i=0; i<self.photoArray.count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        AddPhotoTableViewCell *cell = (AddPhotoTableViewCell *)[photoView cellForRowAtIndexPath:indexPath];
        [cell.textField resignFirstResponder];
    }
}

#pragma mark - ZYQAssetPickerController Delegate
-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    
    for (int i=0; i<assets.count; i++) {
        ALAsset *asset=assets[i];
        UIImageView *imgview=[[UIImageView alloc] init];
        imgview.contentMode=UIViewContentModeScaleAspectFill;
        imgview.clipsToBounds=YES;
        UIImage *tempImg=[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage];
        // [imgview setImage:tempImg];
        IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
        photo.image = tempImg;
        [self.photoArray addObject:photo];
    }
    
    if (self.photoArray.count == 0)
    {
        sendButton.userInteractionEnabled = NO;
        sendButton.alpha = 0.4f;
    }
    else
    {
        sendButton.userInteractionEnabled = YES;
        sendButton.alpha = 1.0f;
    }
    
    [photoView reloadData];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Rotation control
- (void) willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
    [[NSNotificationCenter defaultCenter] postNotificationName:UIKeyboardCoViewWillRotateNotification object:nil];
}

- (void) didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [[NSNotificationCenter defaultCenter] postNotificationName:UIKeyboardCoViewDidRotateNotification object:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return YES;
}

#pragma mark - UI Keyboard Co View Delegate
- (void) keyboardCoViewWillAppear:(UIKeyboardCoView*)keyboardCoView{
    NSLog(@"Keyboard Co View Will Appear");
}

- (void) keyboardCoViewDidAppear:(UIKeyboardCoView*)keyboardCoView{
    NSLog(@"Keyboard Co View Did Appear");
}

- (void) keyboardCoViewWillDisappear:(UIKeyboardCoView*)keyboardCoView{
    NSLog(@"Keyboard Co View Will Disappear");
}


- (void) keyboardCoViewDidDisappear:(UIKeyboardCoView*)keyboardCoView{
    NSLog(@"Keyboard Co View Did Disappear");
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"savePhotoFromCameraNotification" object:nil];
}
@end
