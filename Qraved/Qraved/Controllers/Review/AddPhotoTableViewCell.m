//
//  AddPhotoTableViewCell.m
//  Qraved
//
//  Created by Lucky on 15/6/24.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "AddPhotoTableViewCell.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
 
#import "UIImageView+Helper.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Resize.h"
#import "NSString+Helper.h"

@implementation AddPhotoTableViewCell
{
    UIButton *canceBtn;
    NSInteger currentTag;
    UIView *spaceView;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andIsUploadMenuPhoto:(BOOL)isUploadMenuPhoto
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        _photoImageView = [[UIImageView alloc] init];
        _photoImageView.frame = CGRectMake(0, 0, DeviceWidth, DeviceWidth);
        
        [self addSubview:_photoImageView];
        
        
        
        _textField = [[UITextField alloc] init];
        _textField.placeholder = @"Say something about this photo";
        _textField.frame = CGRectMake(LEFTLEFTSET, _photoImageView.endPointY, DeviceWidth - LEFTLEFTSET *2, 44);
        _textField.textAlignment = NSTextAlignmentLeft;
        _textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        
        _textField.font = [UIFont systemFontOfSize:14];
        _textField.textColor = [UIColor color999999];
        _textField.backgroundColor = [UIColor whiteColor];
        [self addSubview:_textField];

        if (isUploadMenuPhoto)
        {
            _textField.hidden = YES;
        }
        
        spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, _textField.endPointY, DeviceWidth, 20)];
        spaceView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
        [self addSubview:spaceView];


        canceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        canceBtn.frame = CGRectMake(_photoImageView.endPointX - 27, 7, 24, 24);
        [canceBtn setImage:[UIImage imageNamed:@"ic_cancel_photo"] forState:UIControlStateNormal];
        [canceBtn addTarget:self action:@selector(imageClicked1) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:canceBtn];
        
    }
    return self;
}
- (void)imageClicked1
{
    [self.delegate delPhotoClicked:currentTag];
}
- (void)setPhoto:(IMGUploadPhoto *)photo
{
    
    if (photo.isOldPhoto)
    {
        NSString *urlString = [photo.imageUrl returnFullImageUrlWithWidth:DeviceWidth];
        __weak typeof(_photoImageView) weakRestaurantImageView = _photoImageView;
        [_photoImageView setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
            [UIView animateWithDuration:1.0 animations:^{
                [weakRestaurantImageView setAlpha:1.0];
            }];
        }];
        _photoImageView.frame = CGRectMake(_photoImageView.frame.origin.x, _photoImageView.frame.origin.y, DeviceWidth, DeviceWidth);
        _photoImageView.clipsToBounds = YES;
    }
    else
    {
        NSLog(@"size = %@",NSStringFromCGSize(photo.image.size));
        CGFloat cropWidth=self.frame.size.width;
        CGFloat cropHeight=self.frame.size.height;
        
        CGSize size = CGSizeFromString(NSStringFromCGSize(photo.image.size));
        CGFloat height = size.height/(size.width/DeviceWidth);
        
        [_photoImageView setImage:photo.image];
        UIImage *cropImage=[UIImage new];
        //
        cropImage = [photo.image imageByScalingAndCroppingForSize:CGSizeMake(cropWidth, cropHeight)];
        
        [_photoImageView setContentMode:UIViewContentModeScaleAspectFill];
        _photoImageView.frame = CGRectMake(_photoImageView.frame.origin.x, _photoImageView.frame.origin.y, DeviceWidth, height);
        NSLog(@"_photoImageView.size.height = %lf",cropImage.size.height);
    }
    
    currentTag = photo.tag;
    [_textField setText:photo.descriptionStr];
    _textField.frame = CGRectMake(LEFTLEFTSET, _photoImageView.endPointY, DeviceWidth - LEFTLEFTSET *2, 44);
    
    spaceView.frame = CGRectMake(0, _textField.endPointY, DeviceWidth, 20);
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setTextFieldInputAccessoryView:(UIView *)view{
    _textField.inputAccessoryView = view;
}
@end
