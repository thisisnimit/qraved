//
// REPhotoCollectionController.m
// REPhotoCollectionController
//
// Copyright (c) 2012 Roman Efimov (https://github.com/romaonthego)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#import "PhotoCollectionController.h"
#import "PhotoTableViewCell.h"
#import "UIConstants.h"
#import "AddPhotoViewController2.h"
#import "UIView+Helper.h"

#define kZDStickerViewControlSize 36.0

@interface PhotoCollectionController ()<PhotoClickDelegate>
{
    UIImageView *imageView;
    IMGPhoto *selectPhoto;
    UIScrollView *_scrollView;
    CGPoint prevPoint;
    UIImageView *upImageView;
    

}
@property (nonatomic) BOOL preventsLayoutWhileResizing;
@property (nonatomic) BOOL preventsPositionOutsideSuperview; //default = YES
@property (nonatomic) BOOL preventsResizing; //default = NO
@property (nonatomic) BOOL preventsDeleting; //default = NO

@end

@implementation PhotoCollectionController

@synthesize datasource = _datasource;
@synthesize groupByDate = _groupByDate;
@synthesize thumbnailViewClass = _thumbnailViewClass;
#pragma mark -
#pragma mark UITableViewController functions

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(void)viewDidLoad{
    [super viewDidLoad];
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 64)];
    topView.backgroundColor = [UIColor color333333];
    [self.view addSubview:topView];
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.frame = CGRectMake(0, 20, 50, 44);
    [topView addSubview:closeButton];
    [closeButton setImage:[UIImage imageNamed:@"camera_cancel"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(closeViewController) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = CGRectMake(DeviceWidth-50, 20, 44, 44);
    [nextButton addTarget:self action:@selector(nextClicked) forControlEvents:UIControlEventTouchUpInside];
    [nextButton setTitle:L(@"Next") forState:UIControlStateNormal];
    nextButton.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15];
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [topView addSubview:nextButton];
    
    

    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, DeviceWidth, DeviceWidth+20)];
    [self.view addSubview:_scrollView];
    _scrollView.delegate = self;
    
    imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceWidth)];
    [_scrollView addSubview:imageView];
    imageView.backgroundColor = [UIColor whiteColor];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.userInteractionEnabled = YES;
    if (self.selectImage != nil) {
        imageView.image = self.selectImage;
    }
    
    upImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, imageView.endPointY, DeviceWidth, 20)];
    upImageView.backgroundColor = [UIColor grayColor];
    upImageView.userInteractionEnabled = YES;
    [_scrollView addSubview:upImageView];
    
    
    UIPanGestureRecognizer *swipGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(swipUp:)];
    [upImageView addGestureRecognizer: swipGesture];
    
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, _scrollView.endPointY, DeviceWidth, DeviceHeight-_scrollView.endPointY+20) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getDataSources:) name:@"GetPhotosCompleted" object:nil];
    
}
-(void)getDataSources:(NSNotification*)notification{
    self.datasource = [notification.userInfo objectForKey:@"data"];
    [self.tableView reloadData];
}
-(void)swipUp:(UIPanGestureRecognizer *)recognizer
{
    if ([recognizer state]== UIGestureRecognizerStateBegan)
    {
        prevPoint = [recognizer locationInView:_scrollView];
    }
    else if ([recognizer state] == UIGestureRecognizerStateChanged)
    {
        CGPoint point = [recognizer locationInView:_scrollView];
        float  hChange = 0.0;
        
        hChange = (point.y - prevPoint.y);
        
        CGRect scrollViewFrame = _scrollView.frame;
        if (scrollViewFrame.size.height + (hChange)<20) {
            return;
        }
        if (scrollViewFrame.size.height + (hChange)>(20+DeviceWidth)) {
            return;
        }
        _scrollView.frame = CGRectMake(0, 64,DeviceWidth,scrollViewFrame.size.height + (hChange));
        upImageView.frame =CGRectMake(0,_scrollView.frame.size.height-20,DeviceWidth, 20);
        _tableView.frame = CGRectMake(0, _scrollView.endPointY, DeviceWidth, DeviceHeight-_scrollView.endPointY+20);

        prevPoint = [recognizer locationInView:_scrollView];
        
    }
    else if ([recognizer state] == UIGestureRecognizerStateEnded)
    {
        CGRect scrollViewFrame = _scrollView.frame;
        if (scrollViewFrame.size.height<DeviceWidth-100) {
            [UIView animateWithDuration:0.3 animations:^{
                _scrollView.frame = CGRectMake(0, 64, DeviceWidth, 20);
                upImageView.frame =CGRectMake(0,_scrollView.frame.size.height-20,DeviceWidth, 20);
                _tableView.frame = CGRectMake(0, _scrollView.endPointY, DeviceWidth, DeviceHeight-_scrollView.endPointY+20);
            }];

        }else{
           [UIView animateWithDuration:0.3 animations:^{
               _scrollView.frame = CGRectMake(0, 64, DeviceWidth, 20+DeviceWidth);
               upImageView.frame =CGRectMake(0,_scrollView.frame.size.height-20,DeviceWidth, 20);
               _tableView.frame = CGRectMake(0, _scrollView.endPointY, DeviceWidth, DeviceHeight-_scrollView.endPointY+20);

           }];
        }
        prevPoint = [recognizer locationInView:_scrollView];
    }

   
}
-(void)nextClicked{
    AddPhotoViewController2 *addPhotoVC = [[AddPhotoViewController2 alloc]initWithRestauarnt:self.restaurant];
    addPhotoVC .selectedImage=selectPhoto.fullScreenImage;
    addPhotoVC.isWriteReview = self.isWriteReview;
    [self.navigationController pushViewController:addPhotoVC animated:YES];
}

-(void)closeViewController{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger num = self.datasource.count/3;
    NSInteger num2 = self.datasource.count %3;
    if (num2>0) {
        num += 1;
    }
    return num;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"REPhotoThumbnailsCell";
    PhotoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[PhotoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
    }
//    cell = [[PhotoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.delegate = self;

    if (indexPath.row *3 <self.datasource.count) {
        IMGPhoto *photo1 = [self.datasource objectAtIndex:indexPath.row *3];
        cell.imageView1.image = photo1.thumbnail;
        cell.imageView3.hidden = NO;
        cell.photo1 = photo1;

    }else{
        cell.imageView1.hidden = YES;

    }
    if ((indexPath.row *3 +1)<self.datasource.count) {
        IMGPhoto *photo2 = [self.datasource objectAtIndex:(indexPath.row *3+1)];
        cell.imageView2.image = photo2.thumbnail;
        cell.imageView2.hidden = NO;
        cell.photo2 = photo2;


    }else{
        cell.imageView2.hidden = YES;
    }
    if ((indexPath.row *3+2)<self.datasource.count) {
        IMGPhoto *photo3 = [self.datasource objectAtIndex:(indexPath.row *3+2)];
        cell.imageView3.image = photo3.thumbnail;
        cell.imageView3.hidden = NO;
        cell.photo3 = photo3;

    }
    else{
        cell.imageView3.hidden = YES;

    }

    return cell;
}
-(void)photoClicked:(IMGPhoto *)photo{
    selectPhoto = photo;
    imageView.image = photo.fullScreenImage;

}
#pragma mark -
#pragma mark Table view delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return DeviceWidth/3;
}
@end
