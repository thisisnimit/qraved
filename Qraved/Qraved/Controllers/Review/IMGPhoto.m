//
//  Photo.m
//  REPhotoCollectionControllerExample
//
//  Created by Roman Efimov on 7/27/12.
//  Copyright (c) 2012 Roman Efimov. All rights reserved.
//

#import "IMGPhoto.h"

@implementation IMGPhoto

@synthesize thumbnailURL = _thumbnailURL, date = _date, thumbnail = _thumbnail;

- (id)initWithThumbnailURL:(NSURL *)thumbnailURL date:(NSDate *)date
{
    self = [super init];
    if (self) {
        _thumbnailURL = thumbnailURL;
        _date = date;
    }
    return self;
}

+ (IMGPhoto *)photoWithURLString:(NSString *)urlString date:(NSDate *)date
{
    return [[IMGPhoto alloc] initWithThumbnailURL:[NSURL URLWithString:urlString] date:date];
}

@end
