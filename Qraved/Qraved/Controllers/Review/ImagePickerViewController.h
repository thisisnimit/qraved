//
//  ImagePickerViewController.h
//  Qraved
//
//  Created by Laura on 15/3/10.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "GPUImage.h"

@class ImagePickerViewController;
@protocol DLCImagePickerDelegate <NSObject>
@optional

- (void)imagePickerController:(ImagePickerViewController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;
- (void)imagePickerControllerDidCancel:(ImagePickerViewController *)picker;
@end
@interface ImagePickerViewController : BaseViewController {
    GPUImageStillCamera *stillCamera;
    GPUImageOutput<GPUImageInput> *filter;
    GPUImageOutput<GPUImageInput> *blurFilter;
    GPUImageCropFilter *cropFilter;
    GPUImagePicture *staticPicture;
    BOOL isStatic;
    BOOL hasBlur;
    int selectedFilter;
}
@property(nonatomic,assign)BOOL isWriteReview;
@property (nonatomic, retain) GPUImageView *imageView;
@property (nonatomic, assign) id <DLCImagePickerDelegate> delegate;
@property (nonatomic, strong) UIButton *photoCaptureButton;

@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *cameraToggleButton;
@property (nonatomic, strong) UIButton *nextButton;
@property (nonatomic, strong) UIButton *localLibraryButton;

@property (nonatomic, strong) UIButton *flashToggleButton;
@property (nonatomic, strong) UIButton *retakeButton;

@property (nonatomic, retain) UIView *photoBar;
@property (nonatomic, retain) UIView *topBar;
@end
