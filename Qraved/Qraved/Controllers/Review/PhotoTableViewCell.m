//
//  PhotoTableViewCell.m
//  Qraved
//
//  Created by Laura on 15/3/6.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "PhotoTableViewCell.h"

@implementation PhotoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.imageView1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth/3, DeviceWidth/3)];
        self.imageView1.userInteractionEnabled = YES;
        [self.contentView addSubview:self.imageView1];
        
        self.imageView2 = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth/3, 0, DeviceWidth/3, DeviceWidth/3)];
        self.imageView2.userInteractionEnabled = YES;
        [self.contentView addSubview:self.imageView2];
        
        self.imageView3 = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth/3*2, 0, DeviceWidth/3, DeviceWidth/3)];
        self.imageView3.userInteractionEnabled = YES;
        [self.contentView addSubview:self.imageView3];
        
        UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageClicked1)];
        [self.imageView1 addGestureRecognizer:tapGesture1];
        UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageClicked2)];
        [self.imageView2 addGestureRecognizer:tapGesture2];
        UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageClicked3)];
        [self.imageView3 addGestureRecognizer:tapGesture3];
        
    }
    return self;
}
-(void)imageClicked1{
    [self.delegate photoClicked:self.photo1];
}
-(void)imageClicked2{
    [self.delegate photoClicked:self.photo2];

}
-(void)imageClicked3{
    [self.delegate photoClicked:self.photo3];

}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
