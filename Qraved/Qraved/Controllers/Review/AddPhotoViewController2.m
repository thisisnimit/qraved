//
//  AddPhotoViewController2.m
//  Qraved
//
//  Created by Laura on 15/3/6.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "AddPhotoViewController2.h"
#import <MessageUI/MessageUI.h>
#import "UIConstants.h"
#import "ShareToPathView.h"
#import "NXOAuth2AccountStore.h"
#import "UIDevice+Util.h"
#import "LoadingView.h"
//#import "MixpanelHelper.h"
#import "AppDelegate.h"
#import "ReviewHeaderView.h"
#import "PostDataHandler.h"
#import "IMGDish.h"

@interface AddPhotoViewController2 ()<MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate>
{
    ReviewHeaderView *headerView;

    UILabel *promptLabel;
    IMGRestaurant *restaurant;
    NSInteger replaceIndex;
    BOOL replaceImageView;
    int reviewId;
    NSInteger selectIndex;
    UIImagePickerController *imagePicker;
    UITextField *descriptionTextField;
    UIImageView *iconImageView;
}
@end

@implementation AddPhotoViewController2
-(id)initWithRestauarnt:(IMGRestaurant *)resto{
    self = [super init];
    if(self){
        restaurant = resto;
    }
    return self;
}
-(id)initWithRestauarnt:(IMGRestaurant *)resto andReviewId:(int)tmpReviewId{
    self=[super init];
    if(self){
        restaurant=resto;
        reviewId=tmpReviewId;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
#pragma mark 设置headerView
    self.view.backgroundColor = [UIColor blackColor];

    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 64)];
    topView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:topView];
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    closeButton.frame = CGRectMake(0, 20, 50, 44);
    [topView addSubview:closeButton];
    [closeButton setImage:[UIImage imageNamed:@"camera_cancel"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(closeViewController) forControlEvents:UIControlEventTouchUpInside];
    
//    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    nextButton.frame = CGRectMake(DeviceWidth-50, 20, 44, 44);
//    [nextButton addTarget:self action:@selector(nextClicked) forControlEvents:UIControlEventTouchUpInside];
//    [nextButton setTitle:@"Next" forState:UIControlStateNormal];
//    nextButton.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15];
//    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [topView addSubview:nextButton];
    
    
    
    [self.view addSubview:headerView];
    UIView *mainView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, DeviceWidth, DeviceHeight-44)];
    [self.view addSubview:mainView];
    mainView.backgroundColor = [UIColor whiteColor];
    
    if (self.selectedImage) {
        
        iconImageView = [[UIImageView alloc]init];
        iconImageView.image = self.selectedImage;
        iconImageView.frame = CGRectMake(LEFTLEFTSET ,10, 100, 100);
        [mainView addSubview:iconImageView];
        iconImageView.userInteractionEnabled = YES;
        
//        UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        [deleteButton setImage:[UIImage imageNamed:@"closeWhite"] forState:UIControlStateNormal];
//        deleteButton.frame = CGRectMake(70, -10, 40, 40);
//        [iconImageView addSubview:deleteButton];
//        [deleteButton addTarget:self action:@selector(deleteClick) forControlEvents:UIControlEventTouchUpInside];
//        
//        UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(deleteImageView)];
//        tapGesture1.numberOfTapsRequired = 1;
//        [iconImageView addGestureRecognizer:tapGesture1];
//        
//        
//        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(replaceImageView)];
//        tapGesture.numberOfTapsRequired = 2;
//        [iconImageView addGestureRecognizer:tapGesture];
        
//        UITextField *titleField = [[UITextField alloc]initWithFrame: CGRectMake(iconImageView.endPointX+LEFTLEFTSET, 10, DeviceWidth-iconImageView.endPointX-2*LEFTLEFTSET, 40)];
//        titleField.placeholder = @"Title";
//        titleField.textAlignment = NSTextAlignmentLeft;
//        titleField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
//        [mainView addSubview:titleField];
        
//        UIImageView *lineImageView = [[UIImageView alloc]initWithFrame:CGRectMake(iconImageView.endPointX+LEFTLEFTSET, titleField.endPointY+4.5, DeviceWidth-(iconImageView.endPointX+LEFTLEFTSET), 1)];
//        lineImageView.backgroundColor = [UIColor colorE4E4E4];
//        [mainView addSubview:lineImageView];
        
        descriptionTextField = [[UITextField alloc]initWithFrame: CGRectMake(iconImageView.endPointX+LEFTLEFTSET,10, DeviceWidth-iconImageView.endPointX-2*LEFTLEFTSET, 40)];
        descriptionTextField.placeholder = L(@"White a Caption...");
        descriptionTextField.textAlignment = NSTextAlignmentLeft;
        descriptionTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [mainView addSubview:descriptionTextField];
        
    }
    
    
    
    NSArray *imagesArr = @[@"facebook",@"EmailGray",@"MessageGreen",@"path2"];
    for (int i=0; i<imagesArr.count; i++) {
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET+DeviceWidth/4*i, 164+15, 50, 50)];
        imageView.image = [UIImage imageNamed:[imagesArr objectAtIndex:i]];
        imageView.tag = i;
        imageView.userInteractionEnabled = YES;
        [mainView addSubview:imageView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(inviteButtonClick:)];
        [tap setNumberOfTapsRequired:1];
        [tap setNumberOfTouchesRequired:1];
        [imageView addGestureRecognizer:tap];
    }
    
    
    UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareButton setTitle:L(@"SHARE ->") forState:UIControlStateNormal];
    [shareButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [shareButton setBackgroundColor:[UIColor colorWithRed:96/255.0 green:143/255.0 blue:216/255.0 alpha:1]];
    [shareButton setFrame:CGRectMake(0, DeviceHeight-160, DeviceWidth, 50)];
    [mainView addSubview:shareButton];
    [shareButton addTarget:self action:@selector(shareButtonClick) forControlEvents:UIControlEventTouchUpInside];
    shareButton.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15];

}
-(void)closeViewController{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)closeButtonClick
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
-(void)shareButtonClick{
    IMGUser *user = [IMGUser currentUser];
    if(user.userId==nil){
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    if(user.token==nil){
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    if(self.selectedImage == nil){
        [self closeButtonClick];
    }else{
        [self uploadDishePhotos:user];
    }
    
}
-(void)uploadDishePhotos:(IMGUser *)user{
    PostDataHandler *postDataHandler=[[PostDataHandler alloc]init];
    [[LoadingView sharedLoadingView]startLoading];
    
    NSString *dishTitle=@"";
    NSString *descriptionStr=(descriptionTextField.text.length>0)?descriptionTextField.text:@"";
    NSNumber *menuId=[NSNumber numberWithInt:0];
    [postDataHandler postImage:self.selectedImage andTitle:@"image" andDescription:@"image" andSuccessBlock:^(id postArray){
        NSString *imageUrl=[postArray objectForKey:@"imgPath"];
        NSDictionary *parameters=@{@"t": user.token,@"userID":user.userId,@"restaurantID":restaurant.restaurantId,@"dishTitle":dishTitle,@"imageUrl":imageUrl,@"description":descriptionStr,@"menuID":menuId,@"reviewId":[NSNumber numberWithInt:reviewId]};
        [self requestToAddDish:parameters user:user];
    }andFailedBlock:^(NSError *error){
        NSLog(@"upload photo failed, error:%@",error.description);
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

-(void)requestToAddDish:(NSDictionary *)parameters user:(IMGUser *)user{
    [(NSMutableDictionary *)parameters setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [(NSMutableDictionary *)parameters setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    [[IMGNetWork sharedManager]POST:@"uploadDish" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSString *returnStatusString = [responseObject objectForKey:@"status"];
        NSString * exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp]goToLoginController];
            return;
        }else if([returnStatusString isEqualToString:@"succeed"]){
            NSDictionary *dishDictionary = [responseObject objectForKey:@"dish"];
            if(dishDictionary!=nil){
                IMGDish *dish = [[IMGDish alloc]init];
                [dish setValuesForKeysWithDictionary:dishDictionary];
                [[DBManager manager]insertModel:dish selectField:@"dishId" andSelectID:dish.dishId];
//                [MixpanelHelper trackUploadPhoto:restaurant andDishName:dish.title];
            }
            [[LoadingView sharedLoadingView] stopLoading];
            [self closeButtonClick];
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        [[LoadingView sharedLoadingView] stopLoading];
        NSLog(@"requestToAddDish error: %@",error.localizedDescription);
    }];
}


-(void)inviteButtonClick:(UITapGestureRecognizer*)gesture
{
    NSInteger tag = gesture.view.tag;
    if(tag==0){//facebook
        [self shareWithFacebook];
    }else if(tag==1){//email
        [self shareWithEmail];
    }else if(tag==2){//sms
        [self shareWithSMS];
    }else if(tag==3){//path
        [self shareToPath];
    }else{//whatsapp
        
    }
}
-(void)shareToPath{
    NSString *pathToken = [[NSUserDefaults standardUserDefaults] objectForKey:QRAVED_PATH_TOKEN_KEY];
    if(pathToken==nil){
        [[NXOAuth2AccountStore sharedStore] requestAccessToAccountWithType:@"Path"];
    }else{
        NSLog(@"get path token directly from NSUserDefaults");
    }
    
    IMGRestaurant *currentRestaurant=[[IMGRestaurant alloc]init];
    NSString *sqlString=[NSString stringWithFormat:@"select * from IMGRestaurant where restaurantId=%@;",@"1568"];
    FMResultSet * resultSet2 = [[DBManager manager] executeQuery:sqlString];
    if([resultSet2 next]){
        [currentRestaurant setValueWithResultSet:resultSet2];
    }
    [resultSet2 close];
    NSString *shareContentString = [NSString stringWithFormat:@"I just made a booking at %@ on Qraved.com",currentRestaurant.title];
    
    NSString *imgURL;
    //    FMResultSet *resultSet=[[DBManager manager]executeQuery:[NSString stringWithFormat:@"SELECT imageUrl FROM IMGRestaurantImage WHERE restaurantId = '%@' limit 1;",currentRestaurant.restaurantId]];
    //    if ([resultSet next]) {
    //        imgURL=[resultSet stringForColumn:@"imageUrl"];
    //    }
    //    [resultSet close];
    
    ShareToPathView *shareToPathView = [[ShareToPathView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+[UIDevice heightDifference]) title:shareContentString url:imgURL latitude:currentRestaurant.latitude longitude:currentRestaurant.longitude restaurantTitle:currentRestaurant.title phone:currentRestaurant.phoneNumber address:[currentRestaurant address]];
    [self.view addSubview:shareToPathView];
//    [MixpanelHelper trackShareToPathWithshareObject:currentRestaurant.title andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
}

-(void)shareWithTwitter{
    IMGUser *user = [IMGUser currentUser];
    [[LoadingView sharedLoadingView]startLoading];
    NSString *shareContentString ;
    if(self.isWriteReview){
        shareContentString = [NSString stringWithFormat:L(@"%@\n%@ post review on Qraved\n%@"),user.fullName,user.fullName,self.reviewDescription];
    }else{
        shareContentString = [NSString stringWithFormat:L(@"%@\n%@ post photo on Qraved"),user.fullName,user.fullName];
    }
    SLComposeViewController *slComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    [slComposerSheet setInitialText:shareContentString];
    [slComposerSheet addURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],restaurant.seoKeyword]]];
    if(slComposerSheet){
        [self presentViewController:slComposerSheet animated:YES completion:^{
            [[LoadingView sharedLoadingView]stopLoading];
        }];
        [slComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
            NSString *output;
            switch (result) {
                case SLComposeViewControllerResultCancelled:
                    output = L(@"Action Cancelled");
                    break;
                case SLComposeViewControllerResultDone:{
                    output = L(@"Post Successful");
//                    [MixpanelHelper trackShareToTwitterWithshareObject:@"Share Review" andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
                }
                    break;
                default:
                    break;
            }
            if (result != SLComposeViewControllerResultCancelled){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Twitter Message" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
        }];
    }
}

-(void)shareWithFacebook{
    IMGUser *user = [IMGUser currentUser];
    [[LoadingView sharedLoadingView]startLoading];
    NSString *shareContentString ;
    if(self.isWriteReview){
        shareContentString = [NSString stringWithFormat:L(@"%@\n%@ post review on Qraved\n%@"),user.fullName,user.fullName,self.reviewDescription];
    }else{
        shareContentString = [NSString stringWithFormat:L(@"%@\n%@ post photo on Qraved"),user.fullName,user.fullName];
    }
    SLComposeViewController *slComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [slComposerSheet setInitialText:shareContentString];
    [slComposerSheet addURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],restaurant.seoKeyword]]];
    if(slComposerSheet){
        [self presentViewController:slComposerSheet animated:YES completion:^{
            [[LoadingView sharedLoadingView]stopLoading];
        }];
        
        [slComposerSheet setCompletionHandler:^(SLComposeViewControllerResult result){
            NSString *output;
            switch(result){
                case SLComposeViewControllerResultCancelled:
                    output = L(@"Action Cancelled");
                    break;
                case SLComposeViewControllerResultDone:{
                    output = L(@"Post Successful");
//                    [MixpanelHelper trackShareToFacebookWithshareObject:@"Share Review" andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
                }
                    break;
                default:
                    break;
            }
            if (result != SLComposeViewControllerResultCancelled){
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Facebook Message" message:output delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alert show];
            }
        }];
    }
}

-(void)shareWithEmail{
    if ([MFMailComposeViewController canSendMail]){
        IMGUser *user = [IMGUser currentUser];
        [[LoadingView sharedLoadingView]startLoading];
        NSString *subjectString;
        NSString *shareContentString;
        if(self.isWriteReview){
            subjectString = L(@"Reviews on Qraved");
            shareContentString = [NSString stringWithFormat:L(@"%@\n%@ post review on Qraved\n%@. Check out at %@"),user.fullName,user.fullName,self.reviewDescription,[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],restaurant.seoKeyword]];
        }else{
            subjectString = L(@"Photos on Qraved");
            shareContentString = [NSString stringWithFormat:L(@"%@\n%@ post photo on Qraved. Check out at %@"),user.fullName,user.fullName,[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],restaurant.seoKeyword]];
        }
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [mailer setSubject:subjectString];
        [mailer setMessageBody:shareContentString isHTML:YES];
        [self presentViewController:mailer animated:YES completion:^{
            [[LoadingView sharedLoadingView]stopLoading];
        }];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Oops! This device does not support email."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
    }
}

-(void)shareWithSMS{
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                        message:@"Oops! This device does not support sms."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    IMGUser *user = [IMGUser currentUser];
    NSString *shareContentString ;
    if(self.isWriteReview){
        shareContentString = [NSString stringWithFormat:L(@"%@\n%@ post review on Qraved\n%@. Check out at %@"),user.fullName,user.fullName,self.reviewDescription,[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],restaurant.seoKeyword]];
    }else{
        shareContentString = [NSString stringWithFormat:L(@"%@\n%@ post photo on Qraved. Check out at %@"),user.fullName,user.fullName,[NSString stringWithFormat:@"http://www.qraved.com/%@/%@",[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],restaurant.seoKeyword]];
    }
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setBody:shareContentString];
    [self presentViewController:messageController animated:YES completion:nil];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:{
//            [MixpanelHelper trackShareToEmailWithshareObject:@"Share Review" andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
        }
            break;
        case MFMailComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Failed to send E-Mail!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
        }
            break;
            
        default:
            break;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:{
//            [MixpanelHelper trackShareToSMSWithshareObject:@"Share Review" andSharePerson:([AppDelegate ShareApp].userLogined)?[IMGUser currentUser].fullName:@""];
        }
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
