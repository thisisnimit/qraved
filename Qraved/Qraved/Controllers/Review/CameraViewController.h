//
//  DXViewController.h
//  XDWatermarkDemo
//
//  Created by xieyajie on 13-7-26.
//  Copyright (c) 2013年 xieyajie. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AVFoundation/AVFoundation.h>
#import "IMGRestaurant.h"

@protocol CameraViewControllerDelegate <NSObject>

- (void)savePhoto;

@end

@interface CameraViewController : UIViewController

@property (nonatomic, strong)  UIView *cameraView;
@property (nonatomic, strong)  UIButton *takePhotoButton;
@property (nonatomic, strong)  UIButton *flashButton;
@property (nonatomic, strong)  UIButton *positionButton;
@property (nonatomic, strong)  UIButton *saveButton;
@property (nonatomic, strong)  UIButton *cancelButton;
@property (nonatomic, assign)  BOOL isCanSearch;
@property (nonatomic)          NSNumber *restaurantId;
@property (nonatomic, retain)IMGRestaurant *restaurant;
@property (nonatomic, assign) BOOL isWriteReview;
@property (nonatomic, copy) NSString *reviewDescriptionStr;
@property (nonatomic, assign) BOOL isFromHotkey;

@property (nonatomic, strong) UIScrollView *watermarkScroll;
@property (nonatomic, assign) BOOL isFromUploadPhoto;
@property (nonatomic, assign) BOOL isUploadMenuPhoto;
@property(nonatomic,assign) BOOL isReview;
@property (nonatomic, assign) BOOL isFromContribution;

@property (nonatomic,assign) id<CameraViewControllerDelegate>delegate;

@end
