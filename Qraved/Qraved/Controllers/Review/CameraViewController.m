//
//  DXViewController.m
//  XDWatermarkDemo
//
//  Created by xieyajie on 13-7-26.
//  Copyright (c) 2013年 xieyajie. All rights reserved.
//

#import <ImageIO/ImageIO.h>

#import "CameraViewController.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "SCCaptureSessionManager.h"
#import "PhotoCollectionController.h"
#import "IMGPhoto.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "AddPhotoViewController2.h"
#import "AddPhotoViewController.h"
#import "ReviewPublishViewController.h"
#import "UIImage+DJResize.h"
#import <CoreMotion/CoreMotion.h>
#import "MenuPublishViewController.h"
#import <Photos/Photos.h>


@interface CameraViewController ()<UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIAccelerometerDelegate>
{
    AVCaptureSession *_session;
    AVCaptureDeviceInput *_captureInput;
    AVCaptureStillImageOutput *_captureOutput;
    AVCaptureVideoPreviewLayer *_preview;
    AVCaptureDevice *_device;
    
    UIImage *_finishImage;
    
    UIButton *photoBtn;
    NSData *imageData;
    
    UIDeviceOrientation orientation;

}
@property (nonatomic, strong) SCCaptureSessionManager *captureManager;
@property (nonatomic, strong) CMMotionManager * motionManager;


@end

@implementation CameraViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.navigationController.navigationBarHidden = YES;
    NSLog(@"viewDidLoad");

    self.view.backgroundColor = [UIColor blackColor];
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 64)];
    topView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:topView];
    self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.cancelButton.frame = CGRectMake(0, 20, 50, 44);
    [self.cancelButton setImage:[UIImage imageNamed:@"camera_cancel"] forState:UIControlStateNormal];
    [self.cancelButton addTarget:self action:@selector(closeViewController) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:self.cancelButton];
    
    
    self.saveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.saveButton .frame = CGRectMake(DeviceWidth-50, 20, 44, 44);
    [self.saveButton  addTarget:self action:@selector(nextClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.saveButton  setTitle:L(@"Next") forState:UIControlStateNormal];
    self.saveButton .titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15];
    [self.saveButton  setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [topView addSubview:self.saveButton ];
    
    
//    photoBtn = [UIButton buttonWithType:UIButtonTypeSystem];
//    photoBtn.frame = CGRectMake(DeviceWidth-50, 20, 44, 44);
//    [photoBtn addTarget:self action:@selector(photoBtnClick) forControlEvents:UIControlEventTouchUpInside];
//    [photoBtn setTitle:@"Photo" forState:UIControlStateNormal];
//    photoBtn.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15];
//    [photoBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [topView addSubview:photoBtn];
    
    //session manager
    
//    self.captureManager = [[SCCaptureSessionManager alloc] init];
//    [self.captureManager configureWithParentLayer:self.view previewRect:CGRectMake(0, 64, DeviceWidth, DeviceHeight-64-100)];
//    [_captureManager.session startRunning];
    
    self.cameraView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, DeviceWidth, DeviceHeight-64-100)];
    [self.view addSubview:self.cameraView];
    
    

    [self startMotionManager];
    
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleDeviceOrientationDidChange:)
                                                 name:UIDeviceOrientationDidChangeNotification
                                               object:nil
     ];
    NSLog(@"viewWillAppear");
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    UIView *centerView = [[UIView alloc]initWithFrame:CGRectMake(0, DeviceHeight-100, DeviceWidth, 120)];
    centerView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:centerView];
    
    self.takePhotoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.takePhotoButton.frame = CGRectMake(DeviceWidth/2-30, 50, 60.0f, 60.0f);
    self.takePhotoButton.clipsToBounds = YES;
    self.takePhotoButton.layer.cornerRadius = self.takePhotoButton.frame.size.width / 2.0f;
    self.takePhotoButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.takePhotoButton.layer.borderWidth = 2.0f;
    self.takePhotoButton.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];
    self.takePhotoButton.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.takePhotoButton.layer.shouldRasterize = YES;
    
    [self.takePhotoButton addTarget:self action:@selector(takePhoto:) forControlEvents:UIControlEventTouchUpInside];
    [centerView addSubview:self.takePhotoButton];
    //
    //
    UIButton *lineButton = [[UIButton alloc] initWithFrame:CGRectMake(15, 5, 44, 44)];
    [lineButton setImage:[UIImage imageNamed:@"camera_line_h.png"] forState:UIControlStateSelected];
    [lineButton setImage:[UIImage imageNamed:@"camera_line.png"] forState:UIControlStateNormal];
    [lineButton addTarget:self action:@selector(gridButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [centerView addSubview:lineButton];
    
    
    self.flashButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.flashButton.frame = CGRectMake(DeviceWidth/2-22, 0, 44.0f, 44.0f);
    [self.flashButton setImage:[UIImage imageNamed:@"camera-flash-off.png"] forState:UIControlStateNormal];
    [self.flashButton addTarget:self action:@selector(changeFlash:) forControlEvents:UIControlEventTouchUpInside];
    [centerView addSubview:self.flashButton];
    
    
    // button to toggle camera positions
    self.positionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.positionButton.frame = CGRectMake(DeviceWidth-15-44, 0, 44.0f, 44.0f);
    [self.positionButton setImage:[UIImage imageNamed:@"camera-switch.png"] forState:UIControlStateNormal];
    [self.positionButton addTarget:self action:@selector(positionCnange:) forControlEvents:UIControlEventTouchUpInside];
    [centerView addSubview:self.positionButton];
    
    _cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(20, 50, 70, 60)];
    //    [_cancelButton setBackgroundImage:[UIImage imageNamed:@"camera_album.png"] forState:UIControlStateNormal];
    [_cancelButton  setTitle:L(@"Retake") forState:UIControlStateNormal];
    [_cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _cancelButton .titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15];
    [_cancelButton addTarget:self action:@selector(retakePhoto) forControlEvents:UIControlEventTouchUpInside];
    [centerView addSubview:_cancelButton];
    _cancelButton.hidden = YES;
    _saveButton.hidden = YES;
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self initialize];

        
        dispatch_async(dispatch_get_main_queue(), ^{

            _preview = [AVCaptureVideoPreviewLayer layerWithSession: _session];
            _preview.frame = CGRectMake(0, 0, self.cameraView.frame.size.width, self.cameraView.frame.size.height);
            _preview.videoGravity = AVLayerVideoGravityResizeAspectFill;
            
            [self.cameraView.layer addSublayer:_preview];
            [_session startRunning];
        });
    });

    
    
    
    
    //    UIButton *photoBtn = [[UIButton alloc] initWithFrame:CGRectMake(DeviceWidth-90, 50, 70, 60)];
    //    [photoBtn setBackgroundImage:[UIImage imageNamed:@"camera_album.png"] forState:UIControlStateNormal];
    //    [photoBtn addTarget:self action:@selector(showPhoto) forControlEvents:UIControlEventTouchUpInside];
    //    [centerView addSubview:photoBtn];
    

}

//拍照页面，网格按钮
-(void)gridButtonClick:(UIButton *)sender{
    sender.selected = !sender.selected;
    [_captureManager switchGrid:sender.selected];
    
}

-(void)retakePhoto{
    photoBtn.hidden = NO;
    _saveButton.hidden = YES;
    _cancelButton.hidden = YES;
    _takePhotoButton.hidden = NO;
    
    [_session startRunning];
}
-(void)showPhoto{
    dispatch_async(dispatch_get_main_queue(), ^{
        
//        ALAssetsLibraryAccessFailureBlock failureblock = ^(NSError *myerror){
//            NSLog(@"相册访问失败 =%@", [myerror localizedDescription]);
//            if ([myerror.localizedDescription rangeOfString:@"Global denied access"].location!=NSNotFound) {
//                NSLog(@"无法访问相册.请在'设置->定位服务'设置为打开状态.");
//            }else{
//                NSLog(@"相册访问失败.");
//            }
//        };
        if(self.isUploadMenuPhoto) {
            [self dismissViewControllerAnimated:YES completion:^{
                
                
                if (!_restaurant)
                {
                    _restaurant = [IMGRestaurant findById:_restaurantId];
                }
                
                MenuPublishViewController *addPhotoVC = [[MenuPublishViewController alloc]initWithRestaurant:_restaurant];
                addPhotoVC.amplitudeType = @"Restaurant detail page";
                addPhotoVC.selectedImage = _finishImage;
                
                [[self getCurrentVC] presentViewController:[[UINavigationController alloc] initWithRootViewController:addPhotoVC] animated:YES completion:^{

                }];
            }];
        }else if (_isFromUploadPhoto)
        {
            [self dismissViewControllerAnimated:YES completion:^{
                BOOL isEdit;
                if (_restaurant) {
                     isEdit = YES;
                }
         
                if (!_restaurant)
                {
                    _restaurant = [IMGRestaurant findById:_restaurantId];
                }


                ReviewPublishViewController *addPhotoVC = [[ReviewPublishViewController alloc]initWithRestaurant:_restaurant andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
                addPhotoVC.isEdit = isEdit;
                //addPhotoVC.isWriteReview = NO;
                addPhotoVC.isCanSearch = self.isCanSearch;
                addPhotoVC.isUploadPhoto = YES;
                addPhotoVC.isFromHotkey = self.isFromHotkey;
//                addPhotoVC.selectedImage = _finishImage;
                addPhotoVC.photosArrM = [NSMutableArray array];
                [self latestAsset:^(ALAsset * _Nullable asset, NSError * _Nullable error) {
                    IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
                    photo.image=_finishImage;
                    photo.latitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"];
                    photo.longitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"];
                    photo.photoUrl = [NSString stringWithFormat:@"%@",[asset valueForProperty:ALAssetPropertyAssetURL]];
                    photo.isFromCamera = YES;
                    [addPhotoVC.photosArrM addObject:photo];
                    [[self getCurrentVC] presentViewController:[[UINavigationController alloc] initWithRootViewController:addPhotoVC] animated:YES completion:^{
                        
                    }];
                    
                }];
             
            }];
        }
        else
        {
            NSMutableDictionary* postDic = [[NSMutableDictionary alloc] init];
            [postDic setObject:_finishImage forKey:@"finishImage"];
            if (!self.isReview) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"savePhotoFromCameraNotification" object:nil userInfo:postDic];

            }else{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"writeReviewNotification" object:nil userInfo:postDic];}
            ReviewPublishViewController *addPhotoVC = [[ReviewPublishViewController alloc]initWithRestaurant:_restaurant andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
            [self dismissViewControllerAnimated:YES completion:nil];
            //addPhotoVC.isWriteReview = NO;
            addPhotoVC.isUploadPhoto = YES;
//            addPhotoVC.amplitudeType = @"Review card on homepage";
            addPhotoVC.selectedImage = _finishImage;
            addPhotoVC.isFromHotkey = self.isFromHotkey;
//            [self.navigationController pushViewController:addPhotoVC animated:YES];
        

        }
        
        
       
//        AddPhotoViewController2 *addPhotoVC = [[AddPhotoViewController2 alloc]initWithRestauarnt:_restaurant];
//        addPhotoVC.isWriteReview = self.isWriteReview;
//        addPhotoVC .selectedImage=_finishImage;
//        [self.navigationController pushViewController:addPhotoVC animated:YES];
        
//        ALAssetsGroupEnumerationResultsBlock groupEnumerAtion = ^(ALAsset *result, NSUInteger index, BOOL *stop){
//            if (result!=NULL) {
//                
//                if ([[result valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypePhoto]) {
//                    IMGPhoto *photo = [[IMGPhoto alloc] init];
//                    photo.thumbnail = [UIImage imageWithCGImage:result.thumbnail];
//                    photo.fullScreenImage = [UIImage imageWithCGImage:result.defaultRepresentation.fullScreenImage];
//                    photo.date = [result valueForProperty:ALAssetPropertyDate];
//                    [datasource addObject:photo];
//                    
////                    NSString *urlstr=[NSString stringWithFormat:@"%@",result.defaultRepresentation.url];//图片的url
////                    NSRange range1=[urlstr rangeOfString:@"id="];
////                    NSString *resultName=[urlstr substringFromIndex:range1.location+3];
////                    resultName=[resultName stringByReplacingOccurrencesOfString:@"&ext=" withString:@"."];//格式demo:123456.png
////
////                    NSLog(@"%@--%@",urlstr,resultName);
//                    /*result.defaultRepresentation.fullScreenImage//图片的大图
//                     result.thumbnail                             //图片的缩略图小图
//                                         NSRange range1=[urlstr rangeOfString:@"id="];
//                                         NSString *resultName=[urlstr substringFromIndex:range1.location+3];
//                                         resultName=[resultName stringByReplacingOccurrencesOfString:@"&ext=" withString:@"."];//格式demo:123456.png
//                     */
//                    
//                    //                [self._dataArray addObject:urlstr];
//                }
//            }
//            
//        };
        
        
        
        
//        PhotoCollectionController *photoCollectionController = [[PhotoCollectionController alloc] init];
//        if (_finishImage) {
//            photoCollectionController.selectImage = _finishImage;
//        }
//        
//        photoCollectionController.datasource = datasource;
//        photoCollectionController.isWriteReview = self.isWriteReview;
//        photoCollectionController.restaurant = self.restaurant;
//        [self.navigationController pushViewController:photoCollectionController animated:YES];
//        ALAssetsLibraryGroupsEnumerationResultsBlock libraryGroupsEnumeration = ^(ALAssetsGroup* group, BOOL* stop){
//            
//            if (group == nil)
//            {
//                [[NSNotificationCenter defaultCenter]postNotificationName:@"GetPhotosCompleted" object:nil userInfo:@{@"data":datasource}];
//
//            }
//            
//            if (group!=nil) {
//                NSString *g=[NSString stringWithFormat:@"%@",group];//获取相簿的组
//                NSString *g1=[g substringFromIndex:16 ] ;
//                NSArray *arr=[[NSArray alloc] init];
//                arr=[g1 componentsSeparatedByString:@","];
//                NSString *g2=[[arr objectAtIndex:0] substringFromIndex:5];
//                if ([g2 isEqualToString:@"Camera Roll"]) {
//                    g2=@"相机胶卷";
//                }
////                NSString *groupName=g2;//组的name
//                
//                [group enumerateAssetsUsingBlock:groupEnumerAtion];
//            }
//            
//        };
        
//        ALAssetsLibrary* library = [[ALAssetsLibrary alloc] init];
//        [library enumerateGroupsWithTypes:ALAssetsGroupAll
//                               usingBlock:libraryGroupsEnumeration
//                             failureBlock:failureblock];
    });
    
}
- (UIViewController *)getCurrentVC
{
    UIViewController *result = nil;
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    
    if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;
    else
        result = window.rootViewController;
    
    return result;
}



-(void)showPhoto2
{
    NSMutableArray *datasource = [[NSMutableArray alloc] init];
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if (group) {
            [group setAssetsFilter:[ALAssetsFilter allPhotos]];
            [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
                if (result) {
                    IMGPhoto *photo = [[IMGPhoto alloc] init];
                    photo.thumbnail = [UIImage imageWithCGImage:result.thumbnail];
                    photo.fullScreenImage = [UIImage imageWithCGImage:result.defaultRepresentation.fullScreenImage];
                    photo.date = [result valueForProperty:ALAssetPropertyDate];
                    [datasource addObject:photo];
                }
            }];
        } else {
            PhotoCollectionController *photoCollectionController = [[PhotoCollectionController alloc] init];
            if (_finishImage) {
                photoCollectionController.selectImage = _finishImage;
            }
            photoCollectionController.datasource = datasource;
            photoCollectionController.isWriteReview = self.isWriteReview;
            photoCollectionController.restaurant = self.restaurant;
            [self.navigationController pushViewController:photoCollectionController animated:YES];
        }
    } failureBlock:^(NSError *error) {
        NSLog(@"Failed.");
    }];
    
}

-(void)closeViewController{
    if (self.isFromContribution) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}
-(void)nextClicked{
    
    [self saveAction];
}
 
#pragma mark - private

- (void) initialize
{
    //1.创建会话层
    _session = [[AVCaptureSession alloc] init];
    [_session setSessionPreset:AVCaptureSessionPreset640x480];
    
    //2.创建、配置输入设备
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    NSError *error;
    _captureInput = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
    if (!_captureInput)
    {
        NSLog(@"Error: %@", error);
        return;
    }
    [_session addInput:_captureInput];
    
    
    //3.创建、配置输出
    _captureOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys:AVVideoCodecJPEG,AVVideoCodecKey,nil];
    [_captureOutput setOutputSettings:outputSettings];
    [_session addOutput:_captureOutput];
}

- (void)initWaterScroll
{
    _watermarkScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, self.cameraView.frame.size.height)];
    _watermarkScroll.contentSize = CGSizeMake(320 * 3, _watermarkScroll.frame.size.height);
    _watermarkScroll.pagingEnabled = YES;
    _watermarkScroll.backgroundColor = [UIColor clearColor];
    CGFloat width = 320;
    for (int i = 0; i < 3; i++) {
//        UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"1.jpg"]];
//        imgView.frame = CGRectMake(i * width, 95 * i, width, 95);
//        [_watermarkScroll addSubview:imgView];
    }
    [self.cameraView.layer addSublayer:_watermarkScroll.layer];
}

- (UIImage *)composeImage:(UIImage *)subImage toImage:(UIImage *)superImage atFrame:(CGRect)frame
{
    CGSize superSize = superImage.size;
    CGFloat widthScale = frame.size.width / self.cameraView.frame.size.width;
    CGFloat heightScale = frame.size.height / self.cameraView.frame.size.height;
    CGFloat xScale = frame.origin.x / self.cameraView.frame.size.width;
    CGFloat yScale = frame.origin.y / self.cameraView.frame.size.height;
    CGRect subFrame = CGRectMake(xScale * superSize.width, yScale * superSize.height, widthScale * superSize.width, heightScale * superSize.height);
    
    UIGraphicsBeginImageContext(superSize);
    [superImage drawInRect:CGRectMake(0, 0, superSize.width, superSize.height)];
    [subImage drawInRect:subFrame];
    __autoreleasing UIImage *finish = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return finish;
}



- (void)addHollowOpenToView:(UIView *)view
{
    CATransition *animation = [CATransition animation];
    animation.duration = 0.5f;
    animation.delegate = self;
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    animation.fillMode = kCAFillModeForwards;
    animation.type = @"cameraIrisHollowOpen";
    [view.layer addAnimation:animation forKey:@"animation"];
}

- (void)addHollowCloseToView:(UIView *)view
{
//    CATransition *animation = [CATransition animation];//初始化动画
//    animation.duration = 0.5f;//间隔的时间
//    animation.timingFunction = UIViewAnimationCurveEaseInOut;
//    animation.type = @"cameraIrisHollowClose";
//    
//    [view.layer addAnimation:animation forKey:@"HollowClose"];
}

- (AVCaptureDevice *)cameraWithPosition:(AVCaptureDevicePosition)position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
    for (AVCaptureDevice *device in devices)
    {
        if (device.position == position)
        {
            return device;
        }
    }
    return nil;
}


#pragma mark - IBAction

- (void)takePhoto:(id)sender
{
    [self addHollowCloseToView:self.cameraView];
    
    //get connection
    AVCaptureConnection *videoConnection = nil;
    for (AVCaptureConnection *connection in _captureOutput.connections) {
        for (AVCaptureInputPort *port in [connection inputPorts]) {
            if ([[port mediaType] isEqual:AVMediaTypeVideo] ) {
                videoConnection = connection;
                break;
            }
        }
        if (videoConnection) { break; }
    }
    //get UIImage
//    [_captureOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler:
//     ^(CMSampleBufferRef imageSampleBuffer, NSError *error) {
//         _saveButton.hidden = NO;
//         photoBtn.hidden = YES;
//         _cancelButton.hidden = NO;
//         _takePhotoButton.hidden = YES;
//         //[self addHollowCloseToView:self.cameraView];
//         [_session stopRunning];
//         //[self addHollowOpenToView:self.cameraView];
//         NSData *imageData_ = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
//         _finishImage = [[UIImage alloc] initWithData:imageData_] ;
//     }];
    [_captureOutput captureStillImageAsynchronouslyFromConnection:videoConnection completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
        NSData *imageData_ = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
        UIImage *originImage = [[UIImage alloc] initWithData:imageData_];
        NSLog(@"originImage=%@",originImage);
        CGFloat squareLength = _preview.bounds.size.width;
        CGFloat previewLayerH;
        if (IS_IPHONE_4S) {
            previewLayerH = 320;
        }
        if (IS_IPHONE_5) {
            previewLayerH= 420;
        }
        if (IS_IPHONE_6){
            previewLayerH = DeviceHeight-150;
        }
        if (IS_IPHONE_6_PLUS){
            previewLayerH = DeviceHeight-180;
        }
        
        //            CGFloat headHeight = weak.previewLayer.bounds.size.height - squareLength;
        //            NSLog(@"heeadHeight=%f",headHeight);
        CGSize size = CGSizeMake(squareLength*2, previewLayerH*2);
        UIImage *scaledImage = [originImage resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:size interpolationQuality:kCGInterpolationHigh];
        NSLog(@"scaledImage=%@",scaledImage);
        CGRect cropFrame = CGRectMake((scaledImage.size.width - size.width) / 2, (scaledImage.size.height - size.height) / 2, size.width, size.height);
        NSLog(@"cropFrame:%@", [NSValue valueWithCGRect:cropFrame]);
        UIImage *croppedImage = [scaledImage croppedImage:cropFrame];
        NSLog(@"croppedImage=%@",croppedImage);

        if (orientation != UIDeviceOrientationPortrait) {
            CGFloat degree = 0;
            if (orientation == UIDeviceOrientationPortraitUpsideDown) {
                degree = 180;// M_PI;
            } else if (orientation == UIDeviceOrientationLandscapeLeft) {
                degree = -90;// -M_PI_2;
            } else if (orientation == UIDeviceOrientationLandscapeRight) {
                degree = 90;// M_PI_2;
            }
            croppedImage = [croppedImage rotatedByDegrees:degree];
            scaledImage = [scaledImage rotatedByDegrees:degree];
            originImage = [originImage rotatedByDegrees:degree];
            [_session stopRunning];

        }
        else{
            [_session stopRunning];

        }

        if (orientation!=UIDeviceOrientationPortrait ) {
            _saveButton.hidden = NO;
            photoBtn.hidden = YES;
            _cancelButton.hidden = NO;
            
            _takePhotoButton.hidden = YES;

            _finishImage = croppedImage;
        }else
        {
            _saveButton.hidden = NO;
            photoBtn.hidden = YES;
            _cancelButton.hidden = NO;
            _takePhotoButton.hidden = YES;
            
            _finishImage = originImage;
        }
        _finishImage = [_finishImage orientationCorrectedImage];

    }];
}

- (void)changeFlash:(id)sender
{
    if([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera] && [_device hasFlash])
    {
        [_flashButton setEnabled:NO];
        [_device lockForConfiguration:nil];
        if([_device flashMode] == AVCaptureFlashModeOff)
        {
            [_device setFlashMode:AVCaptureFlashModeAuto];
            [_flashButton setImage:[UIImage imageNamed:@"flash-auto"] forState:UIControlStateNormal];
        }
        else if([_device flashMode] == AVCaptureFlashModeAuto)
        {
            [_device setFlashMode:AVCaptureFlashModeOn];
            [_flashButton setImage:[UIImage imageNamed:@"flash"] forState:UIControlStateNormal];
        }
        else{
            [_device setFlashMode:AVCaptureFlashModeOff];
            [_flashButton setImage:[UIImage imageNamed:@"flash-off"] forState:UIControlStateNormal];
        }
        [_device unlockForConfiguration];
        [_flashButton setEnabled:YES];
    }
}

- (void)positionCnange:(id)sender
{
    //添加动画
    CATransition *animation = [CATransition animation];
    animation.delegate = self;
    animation.duration = .8f;
    animation.timingFunction = UIViewAnimationCurveEaseInOut;
    animation.type = @"oglFlip";
    if (_device.position == AVCaptureDevicePositionFront) {
        animation.subtype = kCATransitionFromRight;
    }
    else if(_device.position == AVCaptureDevicePositionBack){
        animation.subtype = kCATransitionFromLeft;
    }
    [_preview addAnimation:animation forKey:@"animation"];
    
    NSArray *inputs = _session.inputs;
    for ( AVCaptureDeviceInput *input in inputs )
    {
        AVCaptureDevice *device = input.device;
        if ([device hasMediaType:AVMediaTypeVideo])
        {
            AVCaptureDevicePosition position = device.position;
            AVCaptureDevice *newCamera = nil;
            AVCaptureDeviceInput *newInput = nil;
            
            if (position == AVCaptureDevicePositionFront)
            {
                newCamera = [self cameraWithPosition:AVCaptureDevicePositionBack];
            }
            else
            {
                newCamera = [self cameraWithPosition:AVCaptureDevicePositionFront];
            }
            _device = newCamera;
            newInput = [AVCaptureDeviceInput deviceInputWithDevice:newCamera error:nil];
            
            // beginConfiguration ensures that pending changes are not applied immediately
            [_session beginConfiguration];
            
            [_session removeInput:input];
            [_session addInput:newInput];
            
            // Changes take effect once the outermost commitConfiguration is invoked.
            [_session commitConfiguration];
            break;
        }
    }
}

- (void)saveAction
{
//    UIImage * flippedImage = [UIImage imageWithCGImage:_finishImage.CGImage scale:_finishImage.scale orientation:UIImageOrientationUp];
//    
//    _finishImage = flippedImage;
    self.saveButton.userInteractionEnabled = NO;
    
    self.saveButton.alpha = 0.4;
    [PHPhotoLibrary requestAuthorization:^(PHAuthorizationStatus status) {
        if (status == PHAuthorizationStatusAuthorized) {
            UIImageWriteToSavedPhotosAlbum(_finishImage, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
        }else{
            [self showPhoto];
        }
    }];
}

-(void) image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    [self showPhoto];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(nullable NSDictionary<NSString *,id> *)editingInfo{
    
}
- (void)photoBtnClick
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        [self getImagePickerControllerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }

}
- (void)getImagePickerControllerWithSourceType:(UIImagePickerControllerSourceType)sourceType
{
    //iPhone的照相机功能和系统的相册资源,访问通过实例化UIImagePickerController
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    //picker会根据不同的sourceType加载不同的资源
    //sourceType为UIImagePickerControllerSourceTypeCamera时,会调用照相功能
    //为UIImagePickerControllerSourceTypePhotoLibrary,会加载相册
    picker.sourceType = sourceType;
    //允许对资源进行后续处理
    picker.allowsEditing = YES;
    picker.delegate = self;
    //一般情况下,习惯上将picker通过模态化的方式弹出
    [self  presentViewController:picker animated:YES completion:^{
        
    }];
}
#pragma mark---UIImagePickerControllerDelegate---
//选中具体的图片资源后触发此方法,info中有必要的资源信息
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
//照片库中有视频和图片两种资源,需要拿到资源类型
//kUTTypeImage类型为图片资源
{
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        //拿到选中的照片,选中后的照片往往比较大,需要进行后续的处理(压缩处理)
        UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
        //        imageData = UIImagePNGRepresentation(image,0.0001);
        imageData = UIImageJPEGRepresentation(image, 0.001);
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:nil delegate:self cancelButtonTitle:@"上传" otherButtonTitles:@"存储本地", nil];
        //        alert.tag = 300;
        //        [alert show];
        
        
        
        AddPhotoViewController2 *addPhotoVC = [[AddPhotoViewController2 alloc]initWithRestauarnt:_restaurant];
        addPhotoVC.isWriteReview = self.isWriteReview;
        addPhotoVC .selectedImage=image;
        
        [self.navigationController pushViewController:addPhotoVC animated:YES];
    }
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

//系统通知部分过来的操作
//这个是系统横竖屏通知过来，自己需要操作的方法
- (void)handleDeviceOrientationDidChange:(UIInterfaceOrientation)interfaceOrientation
{
    //1.获取 当前设备 实例
    UIDevice *device = [UIDevice currentDevice] ;
    /**
     *  2.取得当前Device的方向，Device的方向类型为Integer
     *
     *  必须调用beginGeneratingDeviceOrientationNotifications方法后，此orientation属性才有效，否则一直是0。orientation用于判断设备的朝向，与应用UI方向无关
     *
     *  @param device.orientation
     *
     */
    
    switch (device.orientation) {
        case UIDeviceOrientationLandscapeLeft:
            NSLog(@"屏幕向左横置");
            orientation = UIDeviceOrientationLandscapeLeft;
            
            break;
            
        case UIDeviceOrientationLandscapeRight:
            NSLog(@"屏幕向右橫置");
            orientation = UIDeviceOrientationLandscapeRight;

            break;
        case UIDeviceOrientationPortrait:
            orientation = UIDeviceOrientationPortrait;
            break;
        case UIDeviceOrientationPortraitUpsideDown:
            orientation = UIDeviceOrientationPortraitUpsideDown;
        default:
            break;
    }
    
}
//初始化
- (void)startMotionManager{
    if (_motionManager == nil) {
        _motionManager = [[CMMotionManager alloc] init];
    }
    _motionManager.deviceMotionUpdateInterval = 1/15.0;//多长时间刷新一次
    if (_motionManager.deviceMotionAvailable) {
        NSLog(@"Device Motion Available");
        [_motionManager startDeviceMotionUpdatesToQueue:[NSOperationQueue currentQueue]
                                            withHandler: ^(CMDeviceMotion *motion, NSError *error){
                                                [self performSelectorOnMainThread:@selector(handleDeviceMotion:) withObject:motion waitUntilDone:YES];
                                                
                                            }];
    } else {
        NSLog(@"No device motion on device.");
        [self setMotionManager:nil];
    }
}
//这里是重力感应的处理方法
- (void)handleDeviceMotion:(CMDeviceMotion *)deviceMotion{
    double x = deviceMotion.gravity.x;
    double y = deviceMotion.gravity.y;
    if (fabs(y) >= fabs(x))
    {
        if (y >= 0){
            // UIDeviceOrientationPortraitUpsideDown;
            orientation = UIDeviceOrientationPortraitUpsideDown;
        }
        else{
            // UIDeviceOrientationPortrait;
            orientation = UIDeviceOrientationPortrait;
        }
    }
    else
    {
 
        if (x >= 0){
            // UIDeviceOrientationLandscapeRight;
            orientation = UIDeviceOrientationLandscapeRight;

        }
        else{
            // UIDeviceOrientationLandscapeLeft;
            orientation = UIDeviceOrientationLandscapeLeft;
        }
    }
}
//用完别忘记关掉
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_motionManager stopDeviceMotionUpdates];
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
}
- (void)latestAsset:(void (^)(ALAsset * _Nullable, NSError *_Nullable))block {
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if (group) {
            [group setAssetsFilter:[ALAssetsFilter allPhotos]];
            [group enumerateAssetsWithOptions:NSEnumerationReverse/*遍历方式*/ usingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
                if (result) {
                    if (block) {
                        block(result,nil);
                    }
                    *stop = YES;
                }
            }];
            *stop = YES;
        }
    } failureBlock:^(NSError *error) {
        if (error) {
            if (block) {
                block(nil,error);
            }
        }
    }];
}

@end
