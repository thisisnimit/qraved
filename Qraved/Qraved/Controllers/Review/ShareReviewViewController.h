//
//  ShareReviewViewController.h
//  Qraved
//
//  Created by Laura on 14-8-18.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "IMGUserReview.h"
#import "IMGRestaurant.h"
#import "IMGUploadPhoto.h"


@interface ShareReviewViewController : BaseViewController
@property(nonatomic,assign)BOOL isWriteReview;
@property (nonatomic,retain)NSString *reviewDescription;
@property(nonatomic,assign) BOOL isFromOnboard;
@property(nonatomic,assign) BOOL isUploadMenuPhoto;

@property (nonatomic , strong) IMGRestaurant *restaurant_;

@property (nonatomic , strong) NSMutableArray *photoArray_;

@property (nonatomic , strong) NSString *firstPhotoID;


-(id)initWithRestaurant:(IMGUserReview *)review_;

-(id)initWithRestaurant:(IMGRestaurant *)restaurant_ andReviewID:(int)reviewID_;

-(id)initWithRestaurantTitle:(NSString *)restaurantTitle andReviewID:(int)reviewID_;

-(id)initWithRestaurant:(IMGRestaurant *)restaurant_ anPhotoIDs:(NSMutableString *)photoIDS;

@end
