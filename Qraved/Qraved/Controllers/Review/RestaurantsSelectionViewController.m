//
//  SelectReviewRestaurantViewController.m
//  Qraved
//
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "RestaurantsSelectionViewController.h"
#import <CoreText/CoreText.h>

#import "ReviewScoreViewController.h"
#import "AddPhotoViewController.h"
#import "RestaurantsSelectionView.h"
#import "ZYQAssetPickerController.h"


#import "Label.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "NSNumber+Helper.h"
#import "UIImage+Helper.h"
#import "UIViewController+Helper.h"

#import "RestaurantCell.h"
#import "DLStarRatingControl.h"
#import "ReviewHeaderView.h"
#import "LoadingView.h"
#import "DBManager.h"
//#import "MixpanelHelper.h"
#import "SearchUtil.h"
#import "RestaurantSelectionCell.h"
#import "IMGUploadPhoto.h"
#import "ReviewPublishViewController.h"
#import "SpecialOffersViewController.h"
#import "ListHandler.h"
#import "SelectPaxViewController.h"
#import "IMGRestaurantOffer.h"
#import "UIDevice+Util.h"
#import "DBManager.h"
#import "RestaurantHandler.h"


@implementation RestaurantsSelectionViewController{
    IMGRestaurant *currentRestaurant;
    
    NSInteger max;
    NSInteger offset;
    NSMutableArray *restaurants;
    IMGUser *user;
    BOOL hasData;
    BOOL start;
    NSMutableArray *regularTimeArray;
    NSMutableArray *restaurantOfferArray;
    NSURLSessionTask *operation;
}

@synthesize searchBar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //self.navigationController.navigationBarHidden = YES;
    // [UIApplication sharedApplication].statusBarHidden=YES;
    [super viewWillAppear:YES];
    
    for (UIView *view in self.navigationController.navigationBar.subviews) {
        if ([view isKindOfClass:(NSClassFromString(@"UINavigationItemButtonView"))]) {
            view.hidden = YES;
        }
    }
}


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[LoadingView sharedLoadingView]stopLoading];
}

- (void)viewDidLoad{
    [super viewDidLoad];
    self.screenName = @"Select/search restaurant form page";
    hasData=YES;
    max=10;
    offset=0;
    user=[IMGUser currentUser];
    self.view.backgroundColor = [UIColor blackColor];
   // NSString *headerTitle;
//    switch(self.sourceType){
//        case selectionSourceReviewType:headerTitle=L(@"Select restaurant to review");break;
//        case selectionSourceUploadType:headerTitle=L(@"Select restaurant to add photo");break;
//        case selectionSourceBookingType:headerTitle=L(@"Select restaurant to booking");break;
//        case selectionSourceWiteReviewType:headerTitle=L(@"Write Review");break;
//        case selectionSourceUploadPotoType:headerTitle=L(@"Upload Photo");break;
//    }
//
    [self addBackBtn];
    self.navigationItem.title = @"Select Restaurants";
    // Do any additional setup after loading the view.
    if (self.sourceType==selectionSourceWiteReviewType||self.sourceType==selectionSourceUploadPotoType) {
        _restaurantView=[[RestaurantsSelectionView alloc] initWithFrame:CGRectMake(0,0,DeviceWidth,DeviceHeight+[UIDevice heightDifference]) title:nil isNeedWriteReview:YES];
    }else{
        _restaurantView=[[RestaurantsSelectionView alloc] initWithFrame:CGRectMake(0,0,DeviceWidth,DeviceHeight+[UIDevice heightDifference]) title:nil];
    }
    _restaurantView.delegate=self;
    [self.view addSubview:_restaurantView];
    [self loadRestaurantData:@"" block:^(BOOL status){
        
    }];
}

-(void)loadRestaurantData:(NSString*)searchText block:(void(^)(BOOL))block{
    __block __weak RestaurantsSelectionViewController *weakSelf=self;
    if(hasData){
        [[LoadingView sharedLoadingView] startLoading];
        NSNumber *cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
        if(cityId==nil){
            cityId=[NSNumber numberWithInteger:1];
        }
        NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithDictionary:@{@"cityId":cityId,@"max":[NSNumber numberWithInteger:max],@"offset":[NSNumber numberWithInteger:offset]}];
        IMGUser *currentUser = [IMGUser currentUser];
        if (currentUser.userId && currentUser.token)
        {
            [params setObject:currentUser.userId forKey:@"userId"];
        }
        if(searchText.length){
            if(self.sourceType==selectionSourceBookingType){
                operation=[ListHandler searchBookedRestaurant:searchText withParams:params andBlock:^(NSNumber *total,NSArray *restaurants_){
                    [weakSelf callback:searchText total:total restaurants:restaurants_ block:block];
                }];
            }else{
                operation=[ListHandler searchRestaurant:searchText withParams:params andBlock:^(NSNumber *total,NSArray *restaurants_){
                    [weakSelf callback:searchText total:total restaurants:restaurants_ block:block];
                }];
            }
        }else{
            [params setObject:cityId forKey:@"cityId"];
            if(self.sourceType==selectionSourceBookingType){
                operation=[ListHandler recentlyViewBookedRestaurants:params block:^(NSNumber *total,NSArray *restaurants_){
                    [weakSelf callback:searchText total:total restaurants:restaurants_ block:block];
                }];
            }else{
                operation=[ListHandler recentlyViewRestaurants:params block:^(NSNumber *total,NSArray *restaurants_){
                    [weakSelf callback:searchText total:total restaurants:restaurants_ block:block];
                }];
            }
        }
    }
}

-(void)callback:(NSString*)searchText total:(NSNumber*)total restaurants:(NSArray*)restaurants_ block:(void(^)(BOOL))block{
    if(![searchText isEqualToString:self.restaurantView.searchBar.text]){
        return;
    }
    offset+=max;
    NSMutableArray *searchRestaurants=[[NSMutableArray alloc] initWithArray:self.restaurantView.restaurants];
    [searchRestaurants addObjectsFromArray:restaurants_];
    self.restaurantView.restaurants=searchRestaurants;
    hasData=offset>[total intValue]?NO:YES;
//    if(hasData){
//        [self.restaurantView resetRefreshViewFrame];
//        self.restaurantView..hidden=NO;
//    }
//    if(!hasData && self.restaurantView.refreshView){
//        self.restaurantView.refreshView.hidden=YES;
//        // [self.resturantView.refreshView removeFromSuperview];
//    }
    [[LoadingView sharedLoadingView] stopLoading];
    block(hasData);
}

-(void)closeButtonClick{
    [searchBar resignFirstResponder];
    if (self.sourceType==selectionSourceWiteReviewType||self.sourceType==selectionSourceUploadPotoType) {
        [self.navigationController popViewControllerAnimated:YES];

    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)savePhoto:(NSNotification *)notification
{
    
    ReviewPublishViewController *addPhotoVC = [[ReviewPublishViewController alloc] initWithRestaurant:currentRestaurant andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
    addPhotoVC.isFromHotkey = self.isFromHotkey;
    addPhotoVC.amplitudeType = @"New photo taken from camera";
    addPhotoVC.selectedImage = [notification.userInfo objectForKey:@"finishImage"];
    addPhotoVC.isUploadPhoto = self.sourceType==selectionSourceUploadType;
    [self.navigationController pushViewController:addPhotoVC animated:YES];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"savePhotoFromCameraNotification" object:nil];

}

#pragma mark -ImagePickerController delegate methods
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage=[info objectForKey:UIImagePickerControllerEditedImage];
    CGFloat imageHeight=0;
    CGFloat imageWidth=0;
    if(chosenImage.size.width>UPLOAD_PICTURE_SIZE_WIDTH)
    {
        imageHeight=UPLOAD_PICTURE_SIZE_WIDTH*chosenImage.size.height/chosenImage.size.width;
        imageWidth=UPLOAD_PICTURE_SIZE_WIDTH;
    }
    else
    {
        imageHeight=chosenImage.size.height;
        imageWidth=chosenImage.size.width;
    }
    
    CGSize tagetSize=CGSizeMake(imageWidth, imageHeight);
    UIImage*tagetImage=[chosenImage imageWithScaledToSize:tagetSize];
    
    [picker dismissViewControllerAnimated:NO completion:^{
        AddPhotoViewController *addPhotoVC = [[AddPhotoViewController alloc]initWithRestauarnt:currentRestaurant];
        addPhotoVC.isWriteReview = self.sourceType==selectionSourceReviewType;
        addPhotoVC.selectedImage = tagetImage;
        [self.navigationController pushViewController:addPhotoVC animated:YES];
        
    }];
}

-(void)searchRestaurants:(NSString*)text block:(void(^)(BOOL))block{
    offset=0;
    hasData=YES;
    self.restaurantView.restaurants=nil;
    [operation cancel];
    [self loadRestaurantData:text block:^(BOOL hasData_){
        block(hasData_);
    }];
}

-(void)pickPhotosWithRestaurant:(IMGRestaurant*)restaurant block:(void(^)(BOOL)) block{
    currentRestaurant=restaurant;
    switch(self.sourceType){
        case selectionSourceReviewType:{
            ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc] initWithRestaurant:restaurant andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
            publishReviewVC.isFromHotkey = self.isFromHotkey;
            publishReviewVC.ifSuccessLoginClickPost = YES;
            publishReviewVC.amplitudeType = @"Hot Key";

            [self.navigationController pushViewController:publishReviewVC animated:YES];
            break;
        }
        case selectionSourceUploadType:{
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(savePhoto:) name:@"savePhotoFromCameraNotification" object:nil];
            ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] initWithRestaurantTitle:restaurant.title andIsFromUploadPhoto:NO andIsUploadMenuPhoto:NO andCurrentRestaurantId:restaurant.restaurantId];
            picker.maximumNumberOfSelection = 9;
            picker.assetsFilter = [ALAssetsFilter allPhotos];
            picker.showEmptyGroups=NO;
            picker.delegate = self;
            picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
                    NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
                    return duration >= 5;
                } else {
                    return YES;
                }
            }];

            [self.navigationController presentViewController:picker animated:YES completion:^{
                
            }];
            break;
        }
        case selectionSourceBookingType:{
            [[LoadingView sharedLoadingView] startLoading];
            [RestaurantHandler geOfferFromServer:restaurant.restaurantId andBookDate:nil andBlock:^(NSMutableArray *restaurantOfferArray_) {
                SelectPaxViewController *selectDinersViewController = [[SelectPaxViewController alloc]initWithRestaurant:restaurant andTimeArray:nil andOfferArray:restaurantOfferArray_ andOffer:nil];
                [self.navigationController pushViewController:selectDinersViewController animated:YES];

            }];
            break;
        }
        case selectionSourceWiteReviewType:{
            ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc] initWithRestaurant:restaurant andOverallRating:self.overallRating/2 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
            publishReviewVC.isFromHotkey = self.isFromHotkey;
            publishReviewVC.ifSuccessLoginClickPost = YES;
            publishReviewVC.amplitudeType = @"Hot Key";
            publishReviewVC.reviewContent = self.reviewContent;
            publishReviewVC.reviewTitle = self.reviewTitle;
            publishReviewVC.photosArrM = self.photosArrM;
            publishReviewVC.overallRatings = self.overallRating;
            publishReviewVC.isCanSearch = YES;
            [self.navigationController pushViewController:publishReviewVC animated:YES];
            break;
 
        }
        case selectionSourceUploadPotoType:{
            ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc] initWithRestaurant:restaurant andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
            publishReviewVC.isFromHotkey = self.isFromHotkey;
            publishReviewVC.ifSuccessLoginClickPost = YES;
            publishReviewVC.photosArrM = self.photosArrM;
            publishReviewVC.isUploadPhoto=YES;
            publishReviewVC.isCanSearch = YES;
            [self.navigationController pushViewController:publishReviewVC animated:YES];
            break;
        }
    }
}

-(void)loadSpecialOffersViewControllerWithBookDate:(NSDate *)bookDate andBookTime:(NSString *)bookTime andPax:(NSNumber *)pax{
    if(bookDate!=nil&&bookTime!=nil&&pax!=nil&&![bookTime isEqualToString:@""]){
        
    }else{
        [self initRestaurantOfferFromLocal:nil];
    }
    SpecialOffersViewController *specialOffersViewController = [[SpecialOffersViewController alloc] initWithRestaurant:currentRestaurant andTimeArray:regularTimeArray andOfferArray:restaurantOfferArray];
    if(bookDate!=nil&&bookTime!=nil&&pax!=nil&&![bookTime isEqualToString:@""]){
        specialOffersViewController.pax = pax;
        specialOffersViewController.bookDate = bookDate;
        specialOffersViewController.bookTime = bookTime;
    }
    [self.navigationController pushViewController:specialOffersViewController animated:YES];
}

-(void)initRestaurantOfferFromLocal:(NSDate *)bookDate{
    NSString *currentDateStr;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    if(bookDate==nil){
        NSDate *date = [NSDate date];
        currentDateStr = [formatter stringFromDate:date];
        regularTimeArray = [[NSMutableArray alloc] initWithCapacity:0];
        if(restaurantOfferArray!=nil){
            [restaurantOfferArray removeAllObjects];
        }else{
            restaurantOfferArray=[[NSMutableArray alloc] initWithCapacity:0];
        }
        
        [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGRestaurantOffer WHERE restaurantId = '%@' and offerStartDate<='%@' and offerEndDate>='%@' and offerType=4 ORDER BY offerSlotMaxDisc desc;",currentRestaurant.restaurantId,currentDateStr,currentDateStr] successBlock:^(FMResultSet *resultSet) {
            while ([resultSet next]) {
                IMGRestaurantOffer  *tmpRestaurantOffer=[[IMGRestaurantOffer alloc]init];
                [tmpRestaurantOffer setValueWithResultSet:resultSet];
                [restaurantOfferArray addObject:tmpRestaurantOffer];
            }
            [resultSet close];
        }failureBlock:^(NSError *error) {
            NSLog(@"initRestaurantOfferFromLocal error when get datas:%@",error.description);
        }];
        
        [[DBManager manager] selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGRestaurantOffer WHERE restaurantId = '%@' and offerStartDate<='%@' and offerEndDate>='%@' and offerType!=4 ORDER BY offerType,offerSlotMaxDisc desc;",currentRestaurant.restaurantId,currentDateStr,currentDateStr] successBlock:^(FMResultSet *resultSet) {
            while ([resultSet next]) {
                IMGRestaurantOffer  *tmpRestaurantOffer=[[IMGRestaurantOffer alloc]init];
                [tmpRestaurantOffer setValueWithResultSet:resultSet];
                [restaurantOfferArray addObject:tmpRestaurantOffer];
            }
            [resultSet close];
        }failureBlock:^(NSError *error) {
            NSLog(@"initRestaurantOfferFromLocal error when get datas:%@",error.description);
        }];

    }
}

#pragma mark - ZYQAssetPickerController Delegate
-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    
    NSMutableArray *photosArrM = [[NSMutableArray alloc] init];
    
    for (int i=0; i<assets.count; i++) {
        ALAsset *asset=assets[i];
        UIImageView *imgview=[[UIImageView alloc] init];
        imgview.contentMode=UIViewContentModeScaleAspectFill;
        imgview.clipsToBounds=YES;
        UIImage *tempImg=[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage];
        // [imgview setImage:tempImg];
        IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
        photo.image = tempImg;
        photo.photoUrl =[NSString stringWithFormat:@"%@",[asset valueForProperty:ALAssetPropertyAssetURL]];
        [photosArrM addObject:photo];
    }
    
    [picker dismissViewControllerAnimated:NO completion:^{
        ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc]initWithRestaurant:currentRestaurant andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
        publishReviewVC.isFromHotkey = self.isFromHotkey;
        publishReviewVC.amplitudeType = @"Existing photo on device image gallery";
        publishReviewVC.isUploadPhoto = self.sourceType==selectionSourceUploadType;
        publishReviewVC.photosArrM = [NSMutableArray arrayWithArray:photosArrM];
        publishReviewVC.isFromOnboard = self.isFromOnboard;
        publishReviewVC.ifSuccessLoginClickPost = YES;
        [self dismissViewControllerAnimated:YES completion:^{
            
            [[self getCurrentVC] presentViewController:[[UINavigationController alloc] initWithRootViewController:publishReviewVC] animated:YES completion:nil];
        }];
        
//        AddPhotoViewController *addPhotoVC = [[AddPhotoViewController alloc]initWithRestauarnt:currentRestaurant];
//        addPhotoVC.isWriteReview = NO;
//        addPhotoVC.photoArray = photosArrM;
//        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:addPhotoVC] animated:YES completion:nil];
    }];
}

- (UIViewController *)getCurrentVC
{
    UIViewController *result = nil;
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    
    if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;
    else
        result = window.rootViewController;
    
    return result;
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)scrollViewDidScroll:(UIScrollView *)scroll {
    [searchBar resignFirstResponder];
}

 



@end
