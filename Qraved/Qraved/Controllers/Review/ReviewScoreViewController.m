//  MarkingViewController.m
//  Qraved
//
//  Created by Laura on 14-8-15.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "ReviewScoreViewController.h"

#import "ReviewPublishViewController.h"

#import "Helper.h"
#import "Label.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "UIViewController+Helper.h"
#import "UIImage+Helper.h"
#import "UIColor+Hex.h"
#import "ReviewHeaderView.h"

@implementation ReviewScoreViewController{
    UIScrollView *scrollView;
    IMGRestaurant *restaurant;
    NSMutableArray *starArr;
    UIView *btnView;
    DLStarRatingControl *starRating0;
    DLStarRatingControl *starRating1;
    DLStarRatingControl *starRating2;
    DLStarRatingControl *starRating3;
    UIButton *subBut;
}

-(id)initWithRestaurant:(IMGRestaurant *)resto{
    self=[super init];
    if(self){
        restaurant=resto;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    starArr = [[NSMutableArray alloc] init];
    self.view.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBarHidden = YES;
    [self loadMainView];
}
-(void)loadMainView
{
    ReviewHeaderView *headeView = [[ReviewHeaderView alloc]initWithTitle:restaurant.title andIsNeedWriteReview:NO];
    [headeView.closeButton addTarget:self action:@selector(closeButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:headeView];
 
    scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, headeView.endPointY, DeviceWidth, DeviceHeight-headeView.endPointY+20)];
    scrollView.backgroundColor = [UIColor defaultColor];
    [self.view addSubview:scrollView];
    
    Label *overallLabel = [[Label alloc]initWithFrame:CGRectMake(0, 10, DeviceWidth, 30) andTextFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:20] andTextColor:[UIColor color222222] andTextLines:1];
    overallLabel.textAlignment = NSTextAlignmentCenter;
    overallLabel.text = L(@"Overall Rating");
    [scrollView addSubview:overallLabel];
    
    DLStarRatingControl *startRating = [[DLStarRatingControl alloc]initWithFrame:CGRectMake(DeviceWidth/2-112.5, overallLabel.endPointY+10, 225, 45) andStars:5 andStarWidth:30 isFractional:NO spaceWidth:15];
    startRating.delegate = self;
    startRating.tag = 99;
    [scrollView addSubview:startRating];

    
    UIImageView *shadowView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:startRating.endPointY andHeight:8];
    [scrollView addSubview:shadowView];
    
    NSArray *titleArr = @[@"Cuisine",@"Ambience",@"Service"];
    NSArray *promptArr = @[@"What do you think about the food?",@"Do you like the look & feel of the place?",@"Is the service satisfactory?"];
    NSArray *defaultStarImageArr = @[@"cuisine_gray",@"ambient_gray",@"service_gray"];
    NSArray *highlightedStarImageArr = @[@"cuisine_yellow",@"ambient_yellow",@"service_yellow"];

    for (int i=0; i<titleArr.count; i++) {
        UIView *starView = [[UIView alloc]initWithFrame:CGRectMake(0, (157+8)*i + shadowView.frame.origin.y + 8, DeviceWidth, 157)];
        starView.backgroundColor = [UIColor defaultColor];
        [scrollView addSubview:starView];
        
        if (i<titleArr.count-1) {
            [self addLineImage:starView.frame.size.height withCurrentView:starView];
        }
        
        Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(0, 15, DeviceWidth, 30) andTextFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:20] andTextColor:[UIColor color222222] andTextLines:1];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.text = [titleArr objectAtIndex:i];
        [starView addSubview:titleLabel];
        
        Label *promptLabel = [[Label alloc]initWithFrame:CGRectMake(0, titleLabel.endPointY+15, DeviceWidth, 30) andTextFont:[UIFont fontWithName:FONT_GOTHAM_LIGHT size:12] andTextColor:[UIColor color222222] andTextLines:1];
        promptLabel.textAlignment = NSTextAlignmentCenter;
        promptLabel.text = [promptArr objectAtIndex:i];
        [starView addSubview:promptLabel];
        
        
        
        DLStarRatingControl *startRating = [[DLStarRatingControl alloc]initWithFrame:CGRectMake(DeviceWidth/2-112.5, promptLabel.endPointY+10, 225, 45) andStars:5 andStarWidth:30 isFractional:NO spaceWidth:15];
        [startRating setStar:[UIImage imageNamed:[defaultStarImageArr objectAtIndex:i]] highlightedStar:[UIImage imageNamed:[highlightedStarImageArr objectAtIndex:i]]];
        startRating.delegate = self;
        startRating.tag = 100+i;
        [starView addSubview:startRating];
        
        scrollView.contentSize = CGSizeMake(DeviceWidth, starView.endPointY+20);
        
        if (i == 2)
        {
            subBut = [UIButton buttonWithType:UIButtonTypeCustom];
            subBut.frame = CGRectMake(15, 2, DeviceWidth-30, 38);
            subBut.backgroundColor = [UIColor colorWithHexString:@"#3baf24"];
            [subBut addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
            [subBut.layer setCornerRadius:5];
            [subBut setTitle:L(@"Submit") forState:UIControlStateNormal];
            subBut.userInteractionEnabled=NO;
            subBut.alpha=0.4;

            
            btnView = [[UIView alloc] init];
            btnView.frame = CGRectMake(0, starView.endPointY+20, DeviceWidth, 42);
            [btnView addSubview:subBut];
            btnView.backgroundColor = [UIColor clearColor];
            [scrollView addSubview:btnView];
            
            scrollView.contentSize = CGSizeMake(DeviceWidth, btnView.endPointY+60);
        }
    }
    
    
    
}
- (void)btnClick
{
    ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc]initWithRestaurant:restaurant andOverallRating:starRating0.rating andCuisineRating:starRating1.rating andAmbientRating:starRating2.rating andServiceRating:starRating3.rating];
    publishReviewVC.isEdit = YES;
    publishReviewVC.amplitudeType = @"Review Card Detail" ;
    [self.navigationController pushViewController:publishReviewVC animated:YES];
}

-(void)newRating:(DLStarRatingControl *)control :(float)rating
{
    starRating0 = (id)[self.view viewWithTag:99];
    starRating1 = (id)[self.view viewWithTag:100];
    starRating2 = (id)[self.view viewWithTag:101];
    starRating3 = (id)[self.view viewWithTag:102];
    NSLog(@"0 = %f,1= %f,2 = %f,3 = %f",starRating0.rating,starRating1.rating,starRating2.rating,starRating3.rating);
    if (starRating0.rating!=0 && starRating1.rating!=0 && starRating2.rating!=0 && starRating3.rating!=0)
    {
        subBut.userInteractionEnabled = YES;
        subBut.alpha = 1.0f;
    }
//
//
//        int pointY;
//        if (![UIDevice isLaterThanIphone5])
//        {
//            pointY = 220;
//            scrollView.contentSize = CGSizeMake(DeviceWidth, btnView.endPointY+60);
//            
//            [scrollView setContentOffset:CGPointMake(0, btnView.frame.size.height+pointY+60) animated:YES];
//  
//        }
//        else
//        {
//            pointY = 180;
//            scrollView.contentSize = CGSizeMake(DeviceWidth, btnView.endPointY+60);
//            [scrollView setContentOffset:CGPointMake(0, btnView.frame.size.height+pointY) animated:YES];
//        }
//    }
}

- (void)addLineImage:(CGFloat)currentPointY withCurrentView:(UIView *)currentView
{
    UIImageView *lineImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, currentPointY+5, DeviceWidth, 1)];
    lineImage3.image = [UIImage imageNamed:@"CutOffRule"];
    currentPointY = lineImage3.endPointY;
    [currentView addSubview:lineImage3];
}


-(void)closeButtonClick
{
    [self dismissViewControllerAnimated:YES completion:nil];

}

 


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
