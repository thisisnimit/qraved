//
//  SelectReviewRestaurantViewController.h
//  Qraved
//
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "IMGRestaurant.h"
#import "ZYQAssetPickerController.h"


@class RestaurantsSelectionView;

@protocol RestaurantsSelectionDelegate

    
-(void)loadRestaurantData:(NSString*)searchText block:(void(^)(BOOL))block;
-(void)pickPhotosWithRestaurant:(IMGRestaurant*)restaurant block:(void(^)(BOOL)) block;
-(void)closeButtonClick;
-(void)searchRestaurants:(NSString*)text block:(void(^)(BOOL))block;
    
@end

typedef enum{
	selectionSourceReviewType=0,
	selectionSourceUploadType,
	selectionSourceBookingType,
    selectionSourceWiteReviewType,
    selectionSourceUploadPotoType
    
} selectionSourceType;

@interface RestaurantsSelectionViewController : BaseViewController<UISearchBarDelegate,RestaurantsSelectionDelegate,ZYQAssetPickerControllerDelegate,UINavigationControllerDelegate>
@property(nonatomic,assign) selectionSourceType sourceType;
@property(nonatomic,assign) UISearchBar *searchBar;
@property(nonatomic,strong) RestaurantsSelectionView *restaurantView;
@property(nonatomic,assign) BOOL isFromOnboard;

@property(nonatomic,copy) NSString *reviewTitle;
@property(nonatomic,copy) NSString *reviewContent;
@property(nonatomic,strong)NSMutableArray *photosArrM;
@property(nonatomic,assign)float overallRating;
@property (nonatomic,assign) BOOL isFromHotkey;


@end
