//
//  PhotoTableViewCell.h
//  Qraved
//
//  Created by Laura on 15/3/6.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGPhoto.h"

@protocol PhotoClickDelegate <NSObject>

-(void)photoClicked:(IMGPhoto *)photo;

@end
@interface PhotoTableViewCell : UITableViewCell
@property (nonatomic,retain) UIImageView *imageView1;
@property (nonatomic,retain) UIImageView *imageView3;

@property (nonatomic,retain) UIImageView *imageView2;

@property (nonatomic,retain) IMGPhoto *photo1;
@property (nonatomic,retain) IMGPhoto *photo2;

@property (nonatomic,retain) IMGPhoto *photo3;

@property (nonatomic,assign) id<PhotoClickDelegate>delegate;
@end
