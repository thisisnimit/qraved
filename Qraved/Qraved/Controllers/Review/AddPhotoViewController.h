//
//  AddPhotoViewController.h
//  Qraved
//
//  Created by Laura on 14-8-18.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "IMGRestaurant.h"

@interface AddPhotoViewController : BaseViewController<UIImagePickerControllerDelegate,UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,assign)BOOL isWriteReview;
@property (nonatomic,retain)NSMutableArray *photoArray;
@property (nonatomic,retain)UIImage *selectedImage;
@property (nonatomic,retain)NSString *reviewDescription;


-(id)initWithRestauarnt:(IMGRestaurant *)resto;
-(id)initWithRestauarnt:(IMGRestaurant *)resto andReviewId:(int)tmpReviewId;

@end
