
//
//  PublishReviewViewController.m
//  Qraved
//
//  Created by Laura on 14-8-18.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "ReviewPublishViewController.h"

#import <Photos/Photos.h>
#import "Helper.h"
#import "UIColor+Helper.h"
#import "Label.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "UIViewController+Helper.h"
#import "UIAlertView+BlocksKit.h"
#import "ReviewHeaderView.h"
#import "IMGUser.h"
#import "IMGDish.h"
#import "LoadingView.h"
#import "IMGReview.h"
#import "DBManager.h"
#import "AddPhotoViewController.h"
#import "AddPhotoTableViewCell.h"
#import "PostDataHandler.h"
#import "IMGDish.h"
#import "IMGUploadPhoto.h"
#import "ShareReviewViewController.h"
#import "UIImageView+WebCache.h"
#import "UIDevice+Util.h"
#import "LoginViewController.h"
#import "UserIncentiveViewController.h"
#import "UIDevice+Util.h"
#import "RestaurantsSelectionView.h"
#import "SuggestViewController.h"
#import "SVProgressHUD.h"
#import "DetailDataHandler.h"
#import "ReviewPublishButtonView.h"
#import "ReviewPublishTitleView.h"
#import "ReviewPublishRatingView.h"
#import "ReviewPublishHandler.h"
#import "QRStarComponent.h"
#import "ReviewPublishPhotoView.h"
#import "ReviewPublishAddToListView.h"
#import "HomeService.h"
#import "ReviewPublishMoreRestaurantView.h"

#define REVIEW_DESC_LENGTH 40

@interface ReviewPublishViewController()<ReviewPublishButtonViewDelegate,QRStarRatingDelegate>

@end
@implementation ReviewPublishViewController {
    UITextView *textView;
    UITextField *textField;
    Label *labeltext;
    UIAlertView *alert;
    IMGRestaurant *restaurant;
    float overallRating;
    
    UITableView *_tableView;
    NSMutableString *_photoIds;
    NSMutableArray *dishArry;
    NSMutableArray *updateDishArry;
    UIButton *addPhotoBtn;
    
    NSString *delDishIds;
    NSMutableArray *dishIdsArr;
    
    BOOL hasUploadPhoto;
    DLStarRatingControl *startRating;
    UIScrollView *publishView;
    
    CGFloat lastTextViewHeight;
    
    NSString *userInteractCount;
    BOOL isAlreadyFirstOpenGoToHead;
    BOOL isGuestMode;
    NSNumber *gustModeReviewId;
    NSNumber *preReviewId;
    
//    ReviewPublishButtonView *btnsView;
//    ReviewPublishButtonView *buttonBtnsView;
    UISearchBar *searchBar;
    ReviewPublishTitleView *titleView;
    RestaurantsSelectionViewController *selectReviewRestaurantViewController;
    NSMutableArray *imageUrlArr;
    ReviewPublishRatingView *ratingView;
    UITapGestureRecognizer *recognizerTap;
    UIAlertView *alertView;
    NSString *titleString;
    NSString *contentString;
    UIAlertController *alerController;
    BOOL isAlert;
    UIButton *btnPost;
    
    
    ReviewPublishAddToListView *vAddToList;
    UIView *vUpload;
    ReviewPublishMoreRestaurantView *moreRestaurantView;
}


#pragma mark - init method
-(id)initWithRestaurant:(IMGRestaurant *)resto andOverallRating:(float)rating0 andCuisineRating:(float)rating1 andAmbientRating:(float)rating2 andServiceRating:(float)rating3{
    self=[super init];
    if(self){
        restaurant=resto;
        overallRating=rating0*2;
    }
    return self;
}

#pragma mark - life cycle
- (void)viewDidLoad{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (self.isUploadPhoto)
    {
        self.screenName = @"Upload photo form page";
    }
    else
    {
        self.screenName = @"Write review form page";
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(savePhoto:) name:@"writeReviewNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(titleChanged:) name:UITextFieldTextDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reviewChanged:) name:UITextViewTextDidChangeNotification object:nil];
    
    self.view.backgroundColor = [UIColor blackColor];
//    self.navigationController.navigationBarHidden = YES;
    dishArry = [[NSMutableArray alloc] init];
    dishIdsArr=[[NSMutableArray alloc] init];
    updateDishArry= [[NSMutableArray alloc] init];
    lastTextViewHeight = 0;
    imageUrlArr = [[NSMutableArray alloc] init];
    [self getRestaurantImage];
    if (self.overallRatings==0) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"hasDraft%@",restaurant.restaurantId]]) {
            float starCount =[[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"rating%@",restaurant.restaurantId]];
            if (starCount) {
                overallRating=starCount;
            }
        }

    }
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self loadMainView];
    [self judageDraft];
    
    //contributionArray = [NSMutableArray array];
//    if ([restaurant.restaurantId intValue]==0||restaurant.title.length==0) {
//        [HomeService getContribution:nil andBlock:^(id contributionArr) {
//            
//            moreRestaurantView.restaurantArr = contributionArr;
//            //            [contributionCollectionView reloadData];
//            
//        } andError:^(NSError *error) {
//            
//        }];
//    }
//    _restaurantArr = [NSMutableArray array];
    if (([restaurant.restaurantId intValue]==0||restaurant.title.length==0)){
        [self RequestrestaurantArray];
    }
    
    
//    if (self.isFromProfile) {
//        UIBarButtonItem *leftBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_back"] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
//        leftBtn.tintColor=[UIColor blackColor];
//        self.navigationItem.leftBarButtonItem=leftBtn;
//    }
    
}
//- (void)back{
//
//    [self dismissViewControllerAnimated:YES completion:nil];
//}
- (void)RequestrestaurantArray{
    
    [[LoadingView sharedLoadingView] startLoading];
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];
    
    if (self.isUploadPhoto) {
        for (IMGUploadPhoto *model in self.photosArrM) {
            if (model.latitude != nil) {
                [paramDic setObject:model.latitude forKey:@"photoLatitude"];
                [paramDic setObject:model.longitude forKey:@"photoLongitude"];
                break;
            }
        }
    }
    
    [HomeService getContribution:paramDic andBlock:^(id contributionArr) {
        
        [[LoadingView sharedLoadingView] stopLoading];
        //[_restaurantArr addObjectsFromArray:contributionArr];
        moreRestaurantView.restaurantArr = contributionArr;
        //            [self addMoreRestaurantView];
        //            [self loadMainView];
        //            [self judageDraft];
        //            [_tableView reloadData];
        
    } andError:^(NSError *error) {
        [[LoadingView sharedLoadingView] stopLoading];
    }];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    for (UIView *view in self.navigationController.navigationBar.subviews) {
        if ([view isKindOfClass:(NSClassFromString(@"UINavigationItemButtonView"))]) {
            view.hidden = YES;
        }
    }
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardShowChange:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHiddenChange:) name:UIKeyboardWillHideNotification object:nil];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[LoadingView sharedLoadingView]stopLoading];
    [self.view endEditing:YES];
    
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillHideNotification];
    
    selectReviewRestaurantViewController.reviewTitle = textField.text;
    selectReviewRestaurantViewController.reviewContent = textView.text;
    selectReviewRestaurantViewController.photosArrM = _photosArrM;
    selectReviewRestaurantViewController.overallRating = overallRating;
    
  [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = YES;
    self.navigationItem.hidesBackButton = YES;
    [self updatePostStatus];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [[self rdv_tabBarController] setTabBarHidden:NO animated:NO];
}





-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"writeReviewNotification" object:nil];
    
}

#pragma mark - add subviews
-(void)addButtonBtnsView{
//    buttonBtnsView = [[ReviewPublishButtonView alloc] init];
//    buttonBtnsView.delegate = self;
//    [self.view addSubview:buttonBtnsView];
}
-(void)loadMainView{
    
    self.view.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    if (!_photosArrM)
    {
        _photosArrM = [[NSMutableArray alloc] init];
        for (IMGDish *dish in _userReview.dishListArrM)
        {
            IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
            photo.isOldPhoto = YES;
            photo.dishId = dish.dishId;
            photo.imageUrl = dish.imageUrl;
            photo.descriptionStr = dish.descriptionStr;
            [_photosArrM addObject:photo];
        }
    }
    _photoIds = [[NSMutableString alloc] init];
    
    if (self.selectedImage) {
        IMGUploadPhoto *uploadPhoto = [[IMGUploadPhoto alloc]init];
        uploadPhoto.image = self.selectedImage;
        [_photosArrM addObject:uploadPhoto];
    }
    
    if(_photosArrM.count){
        hasUploadPhoto=YES;
    }
    
    UIBarButtonItem *leftBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ic_back"] style:UIBarButtonItemStylePlain target:self action:@selector(closeButtonClick)];
    leftBtn.tintColor=[UIColor blackColor];
    self.navigationItem.leftBarButtonItem=leftBtn;
    if (self.isUploadPhoto) {
        self.navigationItem.title=@"Images";
    }else{
        self.navigationItem.title=@"Add Review" ;
    }
    
    btnPost = [UIButton buttonWithType:UIButtonTypeCustom];
    btnPost.frame = CGRectMake(0, 0, 45, 30);
    btnPost.titleLabel.font = [UIFont systemFontOfSize:17];
    if (self.isUploadPhoto) {
        btnPost.frame = CGRectMake(0, 0, 45, 30);
        [btnPost setTitle:@"Done" forState:UIControlStateNormal];
    }else{
        btnPost.frame = CGRectMake(0, 0, 55, 30);
        [btnPost setTitle:@"Submit" forState:UIControlStateNormal];
    }
    
    [btnPost setTitleColor:[UIColor colorWithHexString:@"09BFD3"] forState:UIControlStateNormal];
    [btnPost addTarget:self action:@selector(postPhoto) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnPost];

    titleView = [[ReviewPublishTitleView alloc] initWith:restaurant];
    titleView.frame = CGRectMake(0, 0, DeviceWidth, 45);
    
    if (self.isCanSearch) {
        titleView.userInteractionEnabled = YES;
    }else{
        titleView.userInteractionEnabled = NO;
    }
    __weak typeof(self) weakSelf = self;
    titleView.gotoSearchRestaurant = ^{
        [weakSelf editImageViewClicked];
    };
    [self.view addSubview:titleView];
    
    if ([restaurant.restaurantId intValue]==0||restaurant.title.length==0) {
        if (!self.isUploadPhoto) {
      
            [self addMoreRestaurantView];
//            [self RequestrestaurantArray];
            [self createReviewUI];
            
        }else{
            [self addMoreRestaurantView];
//            [self RequestrestaurantArray];
            [self addTableview];
            [self addReviewView];
        }
    }else
    {

        if (!self.isUploadPhoto) {

            [self createReviewUI];
            
        }else{
            [self addTableview];
            [self addReviewView];
        }
    }
}

- (void)createReviewUI{
    [self addRatingView];
    [self addTableview];
    [self addReviewView];
}

-(void)addTableview{
    if (_tableView) {
        [_tableView removeFromSuperview];
    }
    
    CGFloat pointY = 0.0;
    CGFloat heightIploadPhoto = 0.0;
    if (moreRestaurantView) {
        pointY = moreRestaurantView.endPointY;
        heightIploadPhoto = 45;
    }else{
        pointY = titleView.endPointY;
    }
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, self.isUploadPhoto?pointY:ratingView.endPointY+5, DeviceWidth, self.isUploadPhoto?DeviceHeight-44 - pointY - 45:DeviceHeight - 49 - ratingView.endPointY - 45) style:UITableViewStyleGrouped];
    
    _tableView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
}

- (void)addMoreRestaurantView{
    moreRestaurantView = [[ReviewPublishMoreRestaurantView alloc] initWithFrame:CGRectMake(0, titleView.endPointY, DeviceWidth, 46)];
    if (self.restaurantArr.count>0) {
        moreRestaurantView.restaurantArr = self.restaurantArr;
    }
    moreRestaurantView.selectRestaurant = ^(IMGRestaurant *tapRestaurant){
        restaurant = tapRestaurant;
        titleView.restaurant = tapRestaurant;
        vAddToList.currentRestaurant = restaurant;
        moreRestaurantView.hidden = YES;
        ratingView.hidden = NO;
        
        if (self.isUploadPhoto) {
            _tableView.frame = CGRectMake(0, titleView.endPointY, DeviceWidth, DeviceHeight - 45 +20 -64-45);
        }
    };
    [self.view addSubview:moreRestaurantView];
}

-(void)addRatingView{
    
    ratingView = [[ReviewPublishRatingView alloc] initWithFrame:CGRectMake(0, titleView.endPointY, DeviceWidth, 44)];
    if ([restaurant.restaurantId intValue]==0||restaurant.title.length==0) {
        ratingView.hidden = YES;
    }else{
        ratingView.hidden = NO;
    }
    __weak typeof(self) weakSelf = self;
    ratingView.ratingRestaurant = ^(int rating){
        overallRating = (rating+1)*2;
        
        [weakSelf updatePostStatus];
    };
    [self.view addSubview:ratingView];
    [ratingView updateRating:(int)overallRating];
}
-(void)addReviewView{
    publishView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, titleView.endPointY + 20, DeviceWidth, DeviceHeight-titleView.endPointY+20)];
    publishView.backgroundColor = [UIColor whiteColor];
    publishView.delegate = self;
    publishView.userInteractionEnabled = YES;
    [self.view addSubview:publishView];
    
    
    if (!self.isUploadPhoto) {
        textField = [[UITextField alloc]initWithFrame: CGRectMake(LEFTLEFTSET, 0, DeviceWidth-2*LEFTLEFTSET, 40)];
        textField.delegate = self;
        textField.tag = 100;
        textField.placeholder =@"Title (optional)";
        textField.font = [UIFont systemFontOfSize:14];
        textField.textColor = [UIColor color999999];
        textField.textAlignment = NSTextAlignmentLeft;
        textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [publishView addSubview:textField];
        if (_userReview && _userReview.reviewTitle.length)
        {
            textField.text = _userReview.reviewTitle;
            if (!(titleString.length>0)) {
                titleString = _userReview.reviewTitle;
                
            }
        }
        if (titleString.length>0) {
            textField.text = titleString;
        }
        UIImageView *lineImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, textField.endPointY, DeviceWidth, 0.5)];
        lineImageView.backgroundColor = [UIColor colorWithHexString:@"#C1C1C1"];
        [publishView addSubview:lineImageView];
        
        labeltext = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, lineImageView.endPointY+10, DeviceWidth-2*LEFTLEFTSET, 20) andTextFont:[UIFont systemFontOfSize:14] andTextColor:[UIColor color999999] andTextLines:1];
        labeltext.text =@"Write your review";
        labeltext.numberOfLines = 0;
        labeltext.font=[UIFont systemFontOfSize:14];
        if (contentString.length>0) {
            labeltext.hidden=YES;
        }
        [publishView addSubview:labeltext];
        if (self.reviewContent.length>0||contentString.length>0) {
            labeltext.hidden=YES;
        }
        
        UIFont *textViewFont=[UIFont systemFontOfSize:14];
        
        if (_userReview && _userReview.reviewSummarize)
        {
            textView = [[UITextView alloc]init];
            textView.textColor = [UIColor color999999];
            textView.font = textViewFont;
            textView.delegate = self;
            textView.scrollEnabled = NO;
            textView.tag = 101;
            textView.backgroundColor = [UIColor clearColor];
            [publishView insertSubview:textView aboveSubview:labeltext];
            textView.text = _userReview.reviewSummarize;
            if(_userReview.reviewSummarize.length>0&&contentString.length==0) {
                contentString = _userReview.reviewSummarize;
            }
            labeltext.hidden = YES;
            
            CGSize size = [textView sizeThatFits:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, MAXFLOAT)];
            textView.frame=CGRectMake(LEFTLEFTSET-3, lineImageView.endPointY+3, DeviceWidth-2*LEFTLEFTSET, size.height);
            lastTextViewHeight = textView.contentSize.height;
        }
        else
        {
            textView = [[UITextView alloc]initWithFrame:CGRectMake(LEFTLEFTSET-3, lineImageView.endPointY+3, DeviceWidth-2*LEFTLEFTSET, 90)];
            textView.textColor = [UIColor color999999];
            textView.font = textViewFont;
            textView.scrollEnabled = NO;
            textView.delegate = self;
            textView.tag = 101;
            textView.backgroundColor = [UIColor clearColor];
            if (contentString.length>0) {
                textView.text = contentString;
                CGSize size = [textView sizeThatFits:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, MAXFLOAT)];
                textView.frame=CGRectMake(LEFTLEFTSET-3, lineImageView.endPointY+3, DeviceWidth-2*LEFTLEFTSET, size.height);
                lastTextViewHeight = textView.contentSize.height;
                
            }
            [publishView insertSubview:textView aboveSubview:labeltext];
        }
        if (contentString.length>0) {
            textView.text = contentString;
        }

    }
    
    CGFloat uploadFloat = self.isUploadPhoto?0:textView.endPointY;
    vUpload = [[UIView alloc] initWithFrame:CGRectMake(0, uploadFloat, DeviceWidth, 81)];
    vUpload.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    [publishView addSubview:vUpload];
    
    UIButton *btnUpload = [UIButton buttonWithType:UIButtonTypeCustom];
    btnUpload.frame = CGRectMake(15, 15, 166, 36);
    [btnUpload setImage:[UIImage imageNamed:@"ic_uploadphoto"] forState:UIControlStateNormal];
    [btnUpload bk_whenTapped:^{
       [self addPhotoClick]; 
    }];
    [vUpload addSubview:btnUpload];
    
    
    vAddToList = [[ReviewPublishAddToListView alloc] initWithFrame:CGRectMake(0, _tableView.bottom, DeviceWidth, 45)];
    vAddToList.currentRestaurant = restaurant;
    __weak typeof(self)weakSelf = self;
    vAddToList.addToListClick = ^(IMGRestaurant *addRestaurant) {
        [weakSelf addToList:addRestaurant];
    };
    [self.view addSubview:vAddToList];

    publishView.frame = CGRectMake(0, 0, DeviceWidth, vUpload.endPointY);
    _tableView.tableHeaderView = publishView;
}

- (void)addToList:(IMGRestaurant *)restaurant{
    
    [IMGAmplitudeUtil trackSavedWithName:@"BM - Save Restaurant Initiate" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andOrigin:nil andListId:nil andType:@"Custom List" andLocation:@"Want to go List"];
    
    [CommonMethod saveRestaurantToList:restaurant andViewController:self andSavedToListBlock:^(BOOL isSaved, NSString *listName) {
        [IMGAmplitudeUtil trackSavedWithName:@"BM - Save Restaurant Succeed" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andOrigin:nil andListId:nil andType:@"Custom List" andLocation:@"Want to go List"];
        
        restaurant.saved = [NSNumber numberWithBool:isSaved];
        restaurant.stateName = listName;
        vAddToList.currentRestaurant = restaurant;
    }];
}

#pragma mark - keyboadrd notification
-(void)keyboardShowChange:(NSNotification *)notification{
    NSDictionary *userInfo = [notification userInfo];
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [aValue CGRectValue];
    
    vAddToList.frame = CGRectMake(0, DeviceHEIGHT-64-keyboardRect.size.height-45, DeviceWidth, 45);
}
-(void)keyboardHiddenChange:(NSNotification *)notification{
    vAddToList.frame = CGRectMake(0, _tableView.bottom, DeviceWidth, 45);
}

#pragma mark - view click event
- (void)addPhotoClick{
    [textField resignFirstResponder];
    [textView resignFirstResponder];
    if (_photosArrM.count>=9)
    {
        return;
    }
    
    ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] initWithRestaurantTitle:restaurant.title andIsFromUploadPhoto:NO andIsUploadMenuPhoto:NO andCurrentRestaurantId:restaurant.restaurantId andIsreview:YES];
    picker.publishViewController = self;
    if (!_photosArrM || _photosArrM.count == 0)
    {
        picker.maximumNumberOfSelection = 9;
    }
    else
        picker.maximumNumberOfSelection = 9 - _photosArrM.count;
    
    picker.assetsFilter = [ALAssetsFilter allPhotos];
    picker.showEmptyGroups=NO;
    picker.delegate = self;
    picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
            NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
            return duration >= 5;
        } else {
            return YES;
        }
    }];
    
    [self presentViewController:picker animated:YES completion:NULL];
}

-(void)closeButtonClick{
    [textField resignFirstResponder];
    [textView resignFirstResponder];
    if ((overallRating>0||_photosArrM.count>0)&&!self.isEditReview&&!self.isUpdateReview&&!self.isUploadPhoto) {
        isAlert = YES;
        [[Amplitude instance] logEvent:@"CL - Back on Automatic Save Rating"];

        alerController = [UIAlertController alertControllerWithTitle:nil message:@"Hey! Are you leaving?\nWhy not post something first?" preferredStyle:UIAlertControllerStyleAlert];
        [alerController addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [recognizerTap removeTarget:self action:nil];
            
            NSString *valueString;
            if (textField.text.length>0||textView.text.length>0) {
                valueString = @"Review";
            }
            if (overallRating>0){
                valueString = [NSString stringWithFormat:@"%@,Rating",valueString];
            }
            if(_photosArrM.count>0){
                valueString =[NSString stringWithFormat:@"%@,Photo",valueString];
            }
            if (self.isUploadPhoto) {
                valueString =@"Photo";
            }
            
            [IMGAmplitudeUtil trackReviewWithName:@"UC - Save as Draft" andRestaurantId:nil andRatingValue:nil andReviewId:nil andReviewUserId:nil andRatingId:nil andUploadUserId:nil andSource:nil andType:@"Manual" andOrigin:nil andValue:valueString];
                        
            [self saveInfomation];
            [self dismissViewControllerAnimated:YES completion:nil];
        }]];
        [alerController addAction:[UIAlertAction actionWithTitle:@"Post it" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [[Amplitude instance] logEvent:@"CL - Save Rating "];
            [recognizerTap removeTarget:self action:nil];
            
            if (![[IMGUser currentUser].userId intValue]) {
                LoginViewController *loginVC=[[LoginViewController alloc]init];
                UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
                if (self.ifSuccessLoginClickPost) {
                    loginVC.reviewPublishViewDelegate = self;
                }
                [self.navigationController presentViewController:loginNavagationController animated:YES completion:^{
                    
                }];
                
                return;
            }
            
            [self postRating];
            
        }]];
        recognizerTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                action:@selector(handleTapBehind:)];
        [recognizerTap setNumberOfTapsRequired:1];
        recognizerTap.cancelsTouchesInView = NO;
        
        [[AppDelegate ShareApp].window addGestureRecognizer:recognizerTap];
        [self presentViewController:alerController animated:YES completion:nil];
        return;
    }
    if (self.isUploadPhoto&&_photosArrM.count>0) {
        isAlert = YES;
        [[Amplitude instance] logEvent:@"CL - Back on Automatic Save Rating"];
        alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Hey! Are you leaving?\nWhy not post something first?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"No",@"Post it", nil];
        [alertView show];
        recognizerTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                action:@selector(handleTapBehind:)];
        
        [recognizerTap setNumberOfTapsRequired:1];
        recognizerTap.cancelsTouchesInView = NO;
        [alertView.window addGestureRecognizer:recognizerTap];
        return;
        
    }
    if ((!self.isUploadPhoto) && _isUpdateReview) {
        NSNumber *updateReviewId;
        if (isGuestMode || (gustModeReviewId!=nil) ) {
            updateReviewId = gustModeReviewId;
        }else{
            updateReviewId = _userReview.reviewId;
        }
        if (preReviewId!=nil) {
            updateReviewId = preReviewId;
        }
        
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:updateReviewId forKey:@"Review_ID"];
        [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
        
        [[Amplitude instance] logEvent:@"UC - Edit Review Cancel" withEventProperties:eventProperties];
    }
    
    if (self.isUploadPhoto) {
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:[IMGUser currentUser].userId forKey:@"UploaderUser_ID"];
        [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:_photoIds forKey:@"Photo_ID"];
        
        
        [[Amplitude instance] logEvent:@"UC - Upload Photo Cancel" withEventProperties:eventProperties];
    }else {
        if ([textField.text isEmpty]&&[textView.text isEmpty]) {
            
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:@(overallRating) forKey:@"Rating_Value"];
            [[Amplitude instance] logEvent:@"UC - Give Rating Cancel" withEventProperties:eventProperties];
            
        }else if(!_isUpdateReview){
            
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:[IMGUser currentUser].userId forKey:@"ReviewerUser_ID"];
            [[Amplitude instance] logEvent:@"UC - Write Review Cancel" withEventProperties:eventProperties];
        }
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"writeReviewNotification" object:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)editImageViewClicked{
    if (!self.isUploadPhoto) {
        selectReviewRestaurantViewController = [[RestaurantsSelectionViewController alloc] init];
        selectReviewRestaurantViewController.sourceType = selectionSourceWiteReviewType;
        selectReviewRestaurantViewController.isFromHotkey = self.isFromHotkey;
        [self.navigationController pushViewController:selectReviewRestaurantViewController animated:YES];

    }else{
        selectReviewRestaurantViewController = [[RestaurantsSelectionViewController alloc] init];
        selectReviewRestaurantViewController.sourceType = selectionSourceUploadPotoType;
        selectReviewRestaurantViewController.isFromHotkey = self.isFromHotkey;
        [self.navigationController pushViewController:selectReviewRestaurantViewController animated:YES];
    }
    
}
-(void)starClick{
    [startRating setRating:0.0];
    [UIView animateWithDuration:0.7 animations:^{
        
        _tableView.transform = CGAffineTransformMakeTranslation(0, (DeviceHeight- titleView.endPointY));
    }];

}
- (void)delPhotoClicked:(NSInteger)tag{
    
    for (IMGUploadPhoto *photo in _photosArrM)
    {
        if (photo.tag == tag)
        {
            if (photo.isOldPhoto)
            {
                if (!delDishIds)
                {
                    delDishIds = [[NSString alloc] initWithFormat:@"%@",photo.dishId];
                }
                else
                    delDishIds = [NSString stringWithFormat:@"%@,%@",delDishIds,photo.dishId];
            }
            [_photosArrM removeObject:photo];
            [_tableView reloadData];
//            if (_isUploadPhoto) {
//                [_tableView reloadData];
//            }else{
//                reviewPhotoView.photoArray = _photosArrM;
//            }
            
            break;
        }
    }
    if(_photosArrM.count==0){
        hasUploadPhoto=NO;
    }
    [self updatePostStatus];
}
-(void)newRating:(DLStarRatingControl *)control :(float)rating{
    NSLog(@"rating = %f",rating);
    overallRating = rating*2;
    if (control.tag ==101) {
        [self addTableview];
        
        _tableView.frame = CGRectMake(0, DeviceHeight, DeviceWidth, _tableView.frame.size.height);
        [UIView animateWithDuration:0.7 animations:^{
            _tableView.transform = CGAffineTransformMakeTranslation(0, -(DeviceHeight- titleView.endPointY));
        }];

        [self updatePostStatus];
        return;
    }
    if (self.isUploadPhoto){
        self.isUploadPhoto = !self.isUploadPhoto;
        return;
    }
    [self updatePostStatus];
}
#pragma mark - scrollView delegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    NSLog(@"scrollViewWillBeginDragging");
    [textField resignFirstResponder];
    [textView resignFirstResponder];

    for (int i=0; i<_photosArrM.count; i++) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        AddPhotoTableViewCell *cell = (AddPhotoTableViewCell *)[_tableView cellForRowAtIndexPath:indexPath];
        [cell.textField resignFirstResponder];
    }
}
#pragma mark - alerView method
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    _isUpdateReview = YES;
    
    if(buttonIndex==0){
        [recognizerTap removeTarget:self action:nil];

        NSString *valueString;
        if (textField.text.length>0||textView.text.length>0) {
            valueString = @"Review";
        }
        if (overallRating>0){
            valueString = [NSString stringWithFormat:@"%@,Rating",valueString];
        }
        if(_photosArrM.count>0){
            valueString =[NSString stringWithFormat:@"%@,Photo",valueString];
        }
        NSString *origin;
        if (self.isUploadPhoto) {
            origin = @"Add Photo Page";
            valueString =@"Photo";

        }else{
            origin = @"Review Page";

        }
        [[Amplitude instance] logEvent:@"UC - Save as Draft" withEventProperties:@{@"type":@"Automatic",@"Origin":origin,@"Value":valueString}];
        [self saveInfomation];
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }else if (buttonIndex==1){
        [recognizerTap removeTarget:self action:nil];

        [[Amplitude instance] logEvent:@"CL - Save Rating "];
        if (![[IMGUser currentUser].userId intValue]) {
            LoginViewController *loginVC=[[LoginViewController alloc]init];
            UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
            if (self.ifSuccessLoginClickPost) {
                loginVC.reviewPublishViewDelegate = self;
            }
            [self.navigationController presentViewController:loginNavagationController animated:YES completion:^{
                
            }];
            return;
        }
        [self postRating];
    }
    
}
-(void)alertDissmiss{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}
#pragma mark - upload reviews and photos
-(void)executePostPhoto{
    IMGUser *user = [IMGUser currentUser];
    if (!self.isUploadPhoto && overallRating == 0){
        NSString *notice=@"How many stars will you rate this restaurant?";
        
        [SVProgressHUD showImage:[UIImage imageNamed:@""] status:notice];
        [SVProgressHUD dismissWithDelay:1.5];
        [self updatePostStatus];
        return;
    }
    
    NSString *reviewTitle=[textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *reviewDesc=[textView.text stringByReplacingOccurrencesOfString:@" " withString:@""];
//    if(!isAlert&&!self.isUploadPhoto && ([reviewDesc isBlankString] || reviewDesc.length < REVIEW_DESC_LENGTH) && (![reviewTitle isBlankString] || hasUploadPhoto || ![reviewDesc isBlankString])){
//        NSString *notice=L(@"A great review should be at least 40 characters!");
//        [SVProgressHUD showImage:[UIImage imageNamed:@""] status:notice];
//        [SVProgressHUD dismissWithDelay:1.5];
//        [self updatePostStatus];
//        return;
//    }
//    if (!self.isUploadPhoto&&reviewDesc.length<REVIEW_DESC_LENGTH&&_photosArrM.count>0) {
//        NSString *notice=L(@"A great review should be at least 40 characters!");
//        [SVProgressHUD showImage:[UIImage imageNamed:@""] status:notice];
//        [SVProgressHUD dismissWithDelay:1.5];
        [self updatePostStatus];
//        return;
//    }
    [textField resignFirstResponder];
    [textView resignFirstResponder];
    
    [[LoadingView sharedLoadingView] startLoading];
    
    if (_photosArrM.count)
    {
        if (self.isFromJourney) {
            if (delDishIds!=nil)
            {
                IMGUser *user = [IMGUser currentUser];
                NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:user.token,@"t",user.userId,@"userId",delDishIds,@"ids", nil];
                [ReviewPublishHandler deleteDishWithParams:parameters andBlock:^{
                    NSMutableDictionary* faDictonary = [[NSMutableDictionary alloc] init];
                    [faDictonary setValue:self.jnCellIndexPath forKey:@"indexPath"];
                    [faDictonary setValue:[delDishIds componentsSeparatedByString:@","]  forKey:@"delDishIdArr"];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadJourneyUploadPhoto" object:nil userInfo:faDictonary];
                    [self publishReview];
                }];
            }else{
                [self uploadDishePhotos:user currentIndex:0 maxIndex:_photosArrM.count-1];
            }
        }else{
            [self uploadDishePhotos:user currentIndex:0 maxIndex:_photosArrM.count-1];
        }
    }
    else
        [self publishReview];

}
-(void)postRating{
    IMGUser *user = [IMGUser currentUser];
    if(user.userId==nil||user.token==nil){
        [self dismissViewControllerAnimated:YES completion:nil];
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    if (![restaurant.restaurantId intValue]) {
        [SVProgressHUD showImage:[UIImage imageNamed:@""] status:@"Oops! You have not picked a restaurant!"];
        [SVProgressHUD dismissWithDelay:1.5];

        return;
    }
    
    if (!self.isFromJourney && !isGuestMode) {
        [self deleteDishWithService];
    }
    [self editDishDescription];
    if (self.isFromJourney) {
        [self dishIdAndDescription];
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_HOME object:nil userInfo:nil];
    }
//    if (self.isFromHotkey) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_TAB_MENU object:nil];
//        [[AppDelegate ShareApp] goToPage:0];
//        [[AppDelegate ShareApp].homeSlideViewController setCurrentTabIndex:0];
//    }

    if ([textField.text isEmpty])
    {
        textField.text = @"";
    }
    if ([textView.text isEmpty])
    {
        textView.text = @"";
    }
    
    if ([textField.text isEmpty]&&[textView.text isEmpty]&&!_isUpdateReview) {
        [IMGAmplitudeUtil trackReviewWithName:@"UC - Give Rating Initiate" andRestaurantId:restaurant.restaurantId andRatingValue:@(overallRating) andReviewId:nil andReviewUserId:nil andRatingId:nil andUploadUserId:nil andSource:nil andType:nil andOrigin:@"Review Page" andValue:nil];
    }
    if (_photosArrM.count)
    {
        [self uploadDishePhotos:user currentIndex:0 maxIndex:_photosArrM.count-1];
        
    }else{
        NSDictionary *parameters=[[NSDictionary alloc] init];
        
        NSString *postUrl = [[NSString alloc] init];
        if (_isUpdateReview)
        {
            NSNumber *updateReviewId;
            if (isGuestMode || (gustModeReviewId!=nil) ) {
                updateReviewId = gustModeReviewId;
            }else{
                updateReviewId = _userReview.reviewId;
            }
            if (preReviewId!=nil) {
                updateReviewId = preReviewId;
            }
            parameters=@{@"t": user.token,
                         @"userId":user.userId,
                         @"restaurantId":restaurant.restaurantId,
                         @"rating":[NSNumber numberWithFloat:overallRating],
                         @"foodScore":[NSNumber numberWithFloat:overallRating],
                         @"ambianceScore":[NSNumber numberWithFloat:overallRating],
                         @"serviceScore":[NSNumber numberWithFloat:overallRating],
                         @"title":(textField.text && textField.text.length>0)?textField.text:@"",/*&&textView.text.length>40*/
                         @"review":textView.text?textView.text:@"",/* && textView.text.length>40*/
                         @"reviewerId":updateReviewId,
                         @"dishes":updateDishArry,
                         @"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] ,
                         @"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],
                         @"client":QRAVED_WEB_SERVICE_CLIENT,
                         @"v":QRAVED_WEB_SERVICE_VERSION,
                         @"appApiKey":QRAVED_APIKEY

                         };
            
            postUrl = @"review/update";
            [IMGAmplitudeUtil trackReviewWithName:@"UC - Edit Review Initiate" andRestaurantId:restaurant.restaurantId andRatingValue:nil andReviewId:updateReviewId andReviewUserId:nil andRatingId:nil andUploadUserId:nil andSource:nil andType:((_photoIds && _photoIds.length>0)?@"Text + Photo":@"Text only") andOrigin:@"Review Page" andValue:nil];
            [ReviewPublishHandler updateReviewWithParams:parameters andBlock:^(NSDictionary *reviewDictionary, NSString *exceptionMsg,NSString *ratingMap) {
                if([exceptionMsg isLogout]){
                    [[LoadingView sharedLoadingView] stopLoading];
                    [[IMGUser currentUser]logOut];
                    btnPost.enabled = YES;
                    
                    LoginViewController *loginVC=[[LoginViewController alloc]init];
                    UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
                    loginVC.autoPostDelegate = self;
                    [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
                    
                    return;
                }
                if (!self.isUploadPhoto) {
                    [self deleteDraft];
                }
                if (textView.text.length>0) {//&&textView.text.length<40
                    
                    [[NSUserDefaults standardUserDefaults] setValue:textView.text forKey:[NSString stringWithFormat:@"reviewContent%@",restaurant.restaurantId]];
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"hasDraft%@",restaurant.restaurantId]];
                    if (textField.text.length>0) {
                        [[NSUserDefaults standardUserDefaults] setValue:textField.text forKey:[NSString stringWithFormat:@"reviewTitle%@",restaurant.restaurantId]];                }
                    NSMutableArray *reviewDraftArray = [NSMutableArray array];
                    NSMutableArray* tempArray = (NSMutableArray*)[[NSUserDefaults standardUserDefaults] objectForKey:@"reviewDrafts"];
                    for (NSDictionary *dic in tempArray) {
                        if ([[dic objectForKey:@"restaurantId"] intValue]==[restaurant.restaurantId intValue]) {
                            return;
                        }
                    }
                    NSMutableDictionary *draftDict = [NSMutableDictionary dictionary];
                    if (restaurant.imageUrl.length>0) {
                        [draftDict setValue:restaurant.imageUrl forKey:@"restaurantImageUrl"];
                        
                    }
                    if ([restaurant.ratingScore intValue]>0) {
                        [draftDict setValue:restaurant.ratingScore forKey:@"restaurantRatingCount"];
                        
                    }
                    if (restaurant.landMarkList.firstObject) {
                        [draftDict setValue:[restaurant.landMarkList.firstObject objectForKey:@"name" ]forKey:@"restaurantArea"];
                        
                    }else if (restaurant.districtName.length>0) {
                        [draftDict setValue:restaurant.districtName forKey:@"restaurantArea"];
                        
                    }
                    if (restaurant.title.length>0) {
                        [draftDict setValue:restaurant.title forKeyPath:@"restaurantTitle"];
                    }
                    
                    [draftDict setValue:restaurant.restaurantId forKey:@"restaurantId"];
                    [reviewDraftArray addObject:draftDict];
                    [reviewDraftArray addObjectsFromArray:tempArray];
                    [[NSUserDefaults standardUserDefaults] setObject:reviewDraftArray forKey:@"reviewDrafts"];
                }
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:updateReviewId forKey:@"Review_ID"];
                [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
                [[Amplitude instance] logEvent:@"UC - Edit Review Succeed" withEventProperties:eventProperties];
                
                if (self.reviewPublishViewControllerDelegate)
                {
                    [self.reviewPublishViewControllerDelegate writeReviewReloadUploadPhotoCell:self];
                }
                
                [[LoadingView sharedLoadingView] stopLoading];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadUserReviewsView" object:nil userInfo:nil];
                
                NSMutableDictionary* faDictonary = [[NSMutableDictionary alloc] init];
                [faDictonary setValue:reviewDictionary forKey:@"newReview"];
                
                [faDictonary setValue:ratingMap forKey:@"newRatingMap"];
                
                IMGReview *review = [[IMGReview alloc]init];
                [review setValuesForKeysWithDictionary:reviewDictionary];
                review.restaurantId = restaurant.restaurantId;
                
                [[DBManager manager]insertModel:review selectField:@"reviewId" andSelectID:review.reviewId];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_HOME object:nil userInfo:nil];
                
                ShareReviewViewController *shareReviewViewController;
                if (gustModeReviewId!=nil) {
                    shareReviewViewController = [[ShareReviewViewController alloc]initWithRestaurantTitle:restaurant.title andReviewID:[gustModeReviewId intValue]];
                }else{
                    shareReviewViewController = [[ShareReviewViewController alloc]initWithRestaurant:_userReview];
                }
                shareReviewViewController.isWriteReview = !self.isUploadPhoto;
                shareReviewViewController.isFromOnboard = self.isFromOnboard;
                shareReviewViewController.reviewDescription = textView.text;
                [self.navigationController pushViewController:shareReviewViewController animated:YES];
                
//                if (self.navigationController) {
//
//                }
//                else
//                {
//                    [self closeButtonClick];
//                }
                
                if(textView.text && textView.text.length>0){
                    [faDictonary setValue:@(YES) forKey:@"isNotJustRating"];
                }else{
                    [faDictonary setValue:@(NO) forKey:@"isNotJustRating"];
                }
                if (self.jnCellIndexPath) {
                    [faDictonary setValue:self.jnCellIndexPath forKey:@"indexPath"];
                }
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadReviewsView_v1WithUpdateUserReview" object:nil userInfo:faDictonary];
                if (exceptionMsg.length>0) {
                    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
                    [eventProperties setValue:restaurant.title forKey:@"RestaurantTitle"];
                    [eventProperties setValue:exceptionMsg forKey:@"Reason"];
                    [eventProperties setValue:updateReviewId forKey:@"Review_ID"];
                    
                    [[Amplitude instance] logEvent:@"UC - Edit Review Failed" withEventProperties:eventProperties];
                }
                [SVProgressHUD showImage:[UIImage imageNamed:@"PostSuccess"] status:@"Thank you for your review!"];
                [SVProgressHUD dismissWithDelay:1.5];
            } anderrorBlock:^(NSString *str) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:restaurant.title forKey:@"RestaurantTitle"];
                [eventProperties setValue:str forKey:@"Reason" ];
                [eventProperties setValue:updateReviewId forKey:@"Review_ID"];
                [[Amplitude instance] logEvent:@"UC - Edit Review Failed" withEventProperties:eventProperties];
            }
             ];
        }
            
        
        else
        {
            parameters=@{@"t": user.token,
                         @"userId":user.userId,
                         @"restaurantId":restaurant.restaurantId,
                         @"rating":[NSNumber numberWithFloat:overallRating],
                         @"foodScore":[NSNumber numberWithFloat:overallRating],
                         @"ambianceScore":[NSNumber numberWithFloat:overallRating],
                         @"serviceScore":[NSNumber numberWithFloat:overallRating],
                         @"title":(textField.text && textField.text.length>0)?textField.text:@"",/*&&textView.text.length>40*/
                         @"review":textView.text?textView.text:@"",/* && textView.text.length>40*/
                         @"reviewerId":user.userId,
                         @"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] ,
                         @"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],
                         @"client":QRAVED_WEB_SERVICE_CLIENT,
                         @"v":QRAVED_WEB_SERVICE_VERSION,
                         @"appApiKey":QRAVED_APIKEY,
                         @"dishes":dishArry
                         };
            
            postUrl = @"user/writereview";
            userInteractCount = nil;
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
//            [eventProperties setValue:restaurant.title forKey:@"RestaurantTitle"];
            [eventProperties setValue:user.userId forKey:@"ReviewerUser_ID"];
            
            [eventProperties setValue:[self trackOrigin] forKey:@"Origin"];
            
            
//            if (_photoIds && _photoIds.length>0) {
//                [eventProperties setValue:@"Text + Photo" forKey:@"Type"];
//
//            }else{
//                [eventProperties setValue:@"Text only" forKey:@"Type"];
//
           // }
            if (![textView.text isEmpty]) {
                [[Amplitude instance] logEvent:@"UC - Write Review Initiate" withEventProperties:eventProperties];
            }
            [ReviewPublishHandler writereviewWithParams:parameters andBlock:^(NSString *returnStatusString, NSString *exceptionMsg, NSString *userInteractCounts, NSNumber *returnReviewId, NSDictionary *reviewDictionary,NSString *ratingMap) {
                
                if ([exceptionMsg hasPrefix:@"review exists, reviewId:"]) {
                    NSRange idRange = [exceptionMsg rangeOfString:@"review exists, reviewId:"];
                    NSString *reviewId = [exceptionMsg substringFromIndex:idRange.length];
                    preReviewId = [NSNumber numberWithLongLong:[reviewId longLongValue]];
                    //                _isUpdateReview = YES;
                    return ;
                }
                userInteractCount = userInteractCounts;
                if ([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"hasDraft%@",restaurant.restaurantId]]){
                    [[Amplitude instance] logEvent:@"UC - Post Draft"];
                }
                if (!self.isUploadPhoto) {
                    [self deleteDraft];
                    
                }
                if (textView.text.length>0) {//&&textView.text.length<40
                    [[NSUserDefaults standardUserDefaults] setValue:textView.text forKey:[NSString stringWithFormat:@"reviewContent%@",restaurant.restaurantId]];
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"hasDraft%@",restaurant.restaurantId]];
                    if (textField.text.length>0) {
                        [[NSUserDefaults standardUserDefaults] setValue:textField.text forKey:[NSString stringWithFormat:@"reviewTitle%@",restaurant.restaurantId]];                }
                    NSMutableArray *reviewDraftArray = [NSMutableArray array];
                    NSMutableArray* tempArray = (NSMutableArray*)[[NSUserDefaults standardUserDefaults] objectForKey:@"reviewDrafts"];
                    for (NSDictionary *dic in tempArray) {
                        if ([[dic objectForKey:@"restaurantId"] intValue]==[restaurant.restaurantId intValue]) {
                            return;
                        }
                    }
                    NSMutableDictionary *draftDict = [NSMutableDictionary dictionary];
                    if (restaurant.imageUrl.length>0) {
                        [draftDict setValue:restaurant.imageUrl forKey:@"restaurantImageUrl"];
                        
                    }
                    if ([restaurant.ratingScore intValue]>0) {
                        [draftDict setValue:restaurant.ratingScore forKey:@"restaurantRatingCount"];
                        
                    }
                    if (restaurant.landMarkList.firstObject) {
                        [draftDict setValue:[restaurant.landMarkList.firstObject objectForKey:@"name" ]forKey:@"restaurantArea"];
                        
                    }else if (restaurant.districtName.length>0) {
                        [draftDict setValue:restaurant.districtName forKey:@"restaurantArea"];
                        
                    }
                    if (restaurant.title.length>0) {
                        [draftDict setValue:restaurant.title forKeyPath:@"restaurantTitle"];
                    }
                    
                    [draftDict setValue:restaurant.restaurantId forKey:@"restaurantId"];
                    [reviewDraftArray addObject:draftDict];
                    [reviewDraftArray addObjectsFromArray:tempArray];
                    [[NSUserDefaults standardUserDefaults] setObject:reviewDraftArray forKey:@"reviewDrafts"];
                }
                if([exceptionMsg isLogout]){
                    [[LoadingView sharedLoadingView] stopLoading];
                    [[IMGUser currentUser]logOut];
                    btnPost.enabled = YES;
                    LoginViewController *loginVC=[[LoginViewController alloc]init];
                    UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
                    loginVC.autoPostDelegate = self;
                    [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
                    return;
                }else if([returnStatusString isEqualToString:@"succeed"]){
                    
                    if (self.reviewPublishViewControllerDelegate)
                    {
                        [self.reviewPublishViewControllerDelegate writeReviewReloadUploadPhotoCell:self];
                    }
                    
                    if(reviewDictionary!=nil && overallRating!=0){
                        IMGReview *review = [[IMGReview alloc]init];
                        [review setValuesForKeysWithDictionary:reviewDictionary];
                        review.restaurantId = restaurant.restaurantId;
                        
                        NSArray *dishArr=[reviewDictionary objectForKey:@"dishList"];
                        NSMutableArray *dishArray=[[NSMutableArray alloc] init];
                        for (NSDictionary *dishdic in dishArr) {
                            IMGDish *dish = [[IMGDish alloc] init];
                            [dish setValuesForKeysWithDictionary:dishdic];
                            dish.descriptionStr=[dishdic objectForKey:@"description"];
                            dish.reviewId=returnReviewId;
                            [dishArray addObject:dish];
                        }
                        
                        int reviewCount = [restaurant.reviewCount intValue];
                        int newReviewCount = reviewCount + 1;
                        float avgScore = overallRating;
                        float newRatingScore = ([restaurant.ratingScore floatValue] * reviewCount + avgScore)/newReviewCount;
                        
                        NSMutableArray *reviewListArr = (NSMutableArray *)[restaurant.ratingCounts componentsSeparatedByString:@","];
                        
                        if (reviewListArr.count > 2)
                        {
                            int oldRatingScoreNum = [[reviewListArr objectAtIndex:avgScore/2-1] intValue];
                            [reviewListArr replaceObjectAtIndex:avgScore/2-1  withObject:[NSString stringWithFormat:@"%d",oldRatingScoreNum+1]];
                            NSString *reviewListStr = [[NSString alloc] init];
                            for (NSString *str in reviewListArr)
                            {
                                if (!reviewListStr.length)
                                {
                                    reviewListStr = [NSString stringWithFormat:@"%@",str];
                                }
                                else
                                    reviewListStr = [NSString stringWithFormat:@"%@,%@",reviewListStr,str];
                            }
                            restaurant.ratingCounts = reviewListStr;
                        }
                        restaurant.ratingScore = [NSString stringWithFormat:@"%f",newRatingScore];
                        restaurant.reviewCount = [NSNumber numberWithInt:newReviewCount];
                        [[DBManager manager] insertModel:restaurant selectField:@"restaurantId" andSelectID:restaurant.restaurantId];
                        [[DBManager manager]insertModel:review selectField:@"reviewId" andSelectID:review.reviewId];
                        //                    [MixpanelHelper trackWriteReview:restaurant];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_HOME object:nil userInfo:nil];
                        
                        NSMutableDictionary* faDictonary = [[NSMutableDictionary alloc] init];
                        [faDictonary setValue:reviewDictionary forKey:@"newReview"];
                        
                        [faDictonary setValue:ratingMap forKey:@"newRatingMap"];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadUserReviewsView" object:nil userInfo:nil];
                        
                        if(textView.text && textView.text.length>0){
                            [faDictonary setValue:@(YES) forKey:@"isNotJustRating"];
                        }else{
                            [faDictonary setValue:@(NO) forKey:@"isNotJustRating"];
                        }
                        if (self.jnCellIndexPath) {
                            [faDictonary setValue:self.jnCellIndexPath forKey:@"indexPath"];
                        }
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadReviewsView_v1WithUpdateUserReview" object:nil userInfo:faDictonary];
                        
                        if ([textField.text isEmpty]&&[textView.text isEmpty]) {
                            //Rating_ID, Restaurant_ID, Rating_Value
                            
                            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                            [eventProperties setValue:review.reviewId forKey:@"Rating_ID"];
                            [eventProperties setValue:review.restaurantId forKey:@"Restaurant_ID"];
                            [eventProperties setValue:@(overallRating) forKey:@"Rating_Value"];
                            [eventProperties setValue:[self trackOrigin] forKey:@"Origin"];
                            [[Amplitude instance] logEvent:@"UC - Give Rating Succeed" withEventProperties:eventProperties];
                            [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Give Rating Succeed" withValues:eventProperties];
                        }else{
                            
                            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                            [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
                            [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
                            [eventProperties setValue:review.restaurantId forKey:@"Restaurant_ID"];
                            [eventProperties setValue:[self trackOrigin] forKey:@"Origin"];
                            [[Amplitude instance] logEvent:@"UC - Write Review Succeed" withEventProperties:eventProperties];
                            [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Write Review Succeed" withValues:eventProperties];
                        }
                    }
                    
                    [ReviewPublishHandler dashboardWithParams:parameters];
                    
                    if(userInteractCount&&[userInteractCount intValue]<=3&&[userInteractCount intValue]>0){
                        UserIncentiveViewController *userIncentivevc = [[UserIncentiveViewController alloc] initWithCount:[userInteractCount intValue]];
                        [self.navigationController presentViewController:userIncentivevc animated:YES completion:^{
                            ShareReviewViewController *shareReviewViewController = [[ShareReviewViewController alloc]initWithRestaurantTitle:restaurant.title andReviewID:[returnReviewId intValue]];
                            shareReviewViewController.isWriteReview = !self.isUploadPhoto;
                            shareReviewViewController.reviewDescription = textView.text;
                            shareReviewViewController.isFromOnboard = self.isFromOnboard;
                            userInteractCount = nil;
                            [self.navigationController pushViewController:shareReviewViewController animated:NO];
                        }];
                        
                    }else{
                        [SVProgressHUD showImage:[UIImage imageNamed:@"PostSuccess"] status:@"Thank you for your review!"];
                        [SVProgressHUD dismissWithDelay:1.5];
                        ShareReviewViewController *shareReviewViewController = [[ShareReviewViewController alloc]initWithRestaurantTitle:restaurant.title andReviewID:[returnReviewId intValue]];
                        shareReviewViewController.isWriteReview = !self.isUploadPhoto;
                        shareReviewViewController.reviewDescription = textView.text;
                        shareReviewViewController.isFromOnboard = self.isFromOnboard;
                        userInteractCount = nil;
                        
                        [self.navigationController pushViewController:shareReviewViewController animated:YES];
                    }
                    
//                    if (self.navigationController) {
//
//                    }
//                    else
//                    {
//                        [self closeButtonClick];
//                    }
                    
                }
                else{
                    [[LoadingView sharedLoadingView] stopLoading];
                }
                if (exceptionMsg.length>0) {
                    
                    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
                    [eventProperties setValue:restaurant.title forKey:@"RestaurantTitle"];
                    [eventProperties setValue:user.userId forKey:@"ReviewerUser_ID"];
                    [eventProperties setValue:exceptionMsg forKey:@"Reason"];
                    if ([textField.text isEmpty]&&[textView.text isEmpty]) {
                        [eventProperties setValue:@(overallRating) forKey:@"Rating_Value"];
                        [[Amplitude instance] logEvent:@"UC - Give Rating Failed" withEventProperties:eventProperties];
                    }else{
                        [[Amplitude instance] logEvent:@"UC - Write Review Failed" withEventProperties:eventProperties];
                    }
                    
                }
                [SVProgressHUD showImage:[UIImage imageNamed:@"PostSuccess"] status:@"Thank you for your review!"];
                [SVProgressHUD dismissWithDelay:1.5];
                
            } anderrorBlock:^(NSString *str) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:restaurant.title forKey:@"RestaurantTitle"];
                [eventProperties setValue:user.userId forKey:@"ReviewerUser_ID"];
                [eventProperties setValue:str forKey:@"Reason"];
                if ([textField.text isEmpty]&&[textView.text isEmpty]) {
                    [eventProperties setValue:@(overallRating) forKey:@"Rating_Value"];
                    [[Amplitude instance] logEvent:@"UC - Give Rating Failed" withEventProperties:eventProperties];
                }else{
                    [[Amplitude instance] logEvent:@"UC - Write Review Failed" withEventProperties:eventProperties];
                }
                
                [[LoadingView sharedLoadingView] stopLoading];

            }];
            
        }
    }}
- (void)postPhoto{
    if(!self.isUploadPhoto){
        NSMutableDictionary* eventDic=[[NSMutableDictionary alloc]init];
        [eventDic setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
        [eventDic setValue:@"Hot Key" forKey:@"Location"];
        [[Amplitude instance] logEvent:@"CL - Review Photo Post button" withEventProperties:eventDic];
    }
    if (![restaurant.restaurantId intValue]) {
        [SVProgressHUD showImage:[UIImage imageNamed:@""] status:@"Oops! You have not picked a restaurant!"];
        [SVProgressHUD dismissWithDelay:1.5];

        return;
    }
    if (!self.isUploadPhoto && overallRating == 0){
        NSString *notice=@"How many stars will you rate this restaurant?";
        [SVProgressHUD showImage:[UIImage imageNamed:@""] status:notice];
        [SVProgressHUD dismissWithDelay:1.5];
        [self updatePostStatus];
        return;
    }
    
    NSString *reviewTitle=[textField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *reviewDesc=[textView.text stringByReplacingOccurrencesOfString:@" " withString:@""];
//    if(!isAlert&&!self.isUploadPhoto && ([reviewDesc isBlankString] || reviewDesc.length < REVIEW_DESC_LENGTH) && (![reviewTitle isBlankString] || hasUploadPhoto || ![reviewDesc isBlankString])){
//        NSString *notice=@"A great review should be at least 40 characters!";
//        [SVProgressHUD showImage:[UIImage imageNamed:@""] status:notice];
//        [SVProgressHUD dismissWithDelay:1.5];
//
//        
//        //                dispatch_time_t delay=dispatch_time(DISPATCH_TIME_NOW,NSEC_PER_SEC*2);
//        //                dispatch_after(delay,dispatch_get_main_queue(),^{
//        //                    postNoticeLabel.hidden=YES;
        [self updatePostStatus];
//        return;
//    }
    btnPost.enabled = NO;
    IMGUser *user = [IMGUser currentUser];
    if(user.userId==nil){
        LoginViewController *loginVC=[[LoginViewController alloc]init];
        UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
        if (self.ifSuccessLoginClickPost) {
            loginVC.reviewPublishViewDelegate = self;
        }
        [self.navigationController presentViewController:loginNavagationController animated:YES completion:^{
            
        }];
        return;
    }
    if(user.token==nil){
        LoginViewController *loginVC=[[LoginViewController alloc]init];
        if (self.ifSuccessLoginClickPost) {
            loginVC.reviewPublishViewDelegate = self;
        }
        [self.navigationController presentViewController:loginVC animated:YES completion:^{
            
        }];
        
        return;
    }
    [self executePostPhoto];
}

- (NSString *)trackOrigin{
    if (self.isFromHotkey) {
        return @"Hot Key";
    }else{
        return self.amplitudeType?self.amplitudeType:@"Review Page";
    }
}

-(void)dishIdAndDescription{
    NSMutableDictionary* faDictonary = [[NSMutableDictionary alloc] init];
    NSMutableArray *dishList = [[NSMutableArray alloc] init];
    for (IMGUploadPhoto *photo in _photosArrM){
        NSMutableDictionary *dishDic = [[NSMutableDictionary alloc] init];
        if (photo.dishId && photo.descriptionStr) {
            [dishDic setValue:photo.dishId forKey:@"dishId"];
            [dishDic setValue:photo.descriptionStr forKey:@"description"];
        }
        [dishList addObject:dishDic];
    }
    [faDictonary setValue:dishList forKey:@"dishIdAndDescription"];
    [faDictonary setValue:self.jnCellIndexPath forKey:@"indexPath"];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadDishIdAndDescription" object:nil userInfo:faDictonary];
}


-(void)closeKeyBoard{
    [textField resignFirstResponder];
    [textView resignFirstResponder];
}

-(void)publishReview{
    IMGUser *user = [IMGUser currentUser];
    if(user.userId==nil){
        [self dismissViewControllerAnimated:YES completion:nil];
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    if(user.token==nil){
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    if (!self.isFromJourney && !isGuestMode) {
        [self deleteDishWithService];
    }
    [self editDishDescription];
    if (self.isFromJourney) {
        [self dishIdAndDescription];
//        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_HOME object:nil userInfo:nil];
    }
    int rating=[[NSNumber numberWithFloat:overallRating] intValue];
    if (self.navigationController && rating == 0) {
        [[LoadingView sharedLoadingView] stopLoading];
        
        if(userInteractCount&&[userInteractCount intValue]<=3&&[userInteractCount intValue]>0){
            UserIncentiveViewController *userIncentivevc = [[UserIncentiveViewController alloc] initWithCount:[userInteractCount intValue]];
            [self.navigationController presentViewController:userIncentivevc animated:YES completion:^{
                ShareReviewViewController *shareReviewViewController = [[ShareReviewViewController alloc]initWithRestaurant:restaurant anPhotoIDs:_photoIds];
                shareReviewViewController.isWriteReview = !self.isUploadPhoto;
                shareReviewViewController.reviewDescription = textView.text;
                shareReviewViewController.isFromOnboard = self.isFromOnboard;
                userInteractCount = nil;
                [self.navigationController pushViewController:shareReviewViewController animated:NO];
            }];
            
        }else{
            ShareReviewViewController *shareReviewViewController = [[ShareReviewViewController alloc]initWithRestaurant:restaurant anPhotoIDs:_photoIds];
            shareReviewViewController.isWriteReview = !self.isUploadPhoto;
            shareReviewViewController.reviewDescription = textView.text;
            shareReviewViewController.isFromOnboard = self.isFromOnboard;
            [self.navigationController pushViewController:shareReviewViewController animated:YES];
        }
        return;
    }
    
    if ([textField.text isEmpty])
    {
        textField.text = @"";
    }
    if ([textView.text isEmpty])
    {
        textView.text = @"";
    }
    if ([textField.text isEmpty]&&[textView.text isEmpty]&&!_isUpdateReview&&rating>0) {
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:@(overallRating) forKey:@"Rating_Value"];
        [eventProperties setValue:[self trackOrigin] forKey:@"Origin"];
        [[Amplitude instance] logEvent:@"UC - Give Rating Initiate" withEventProperties:eventProperties];
        
    }
    
    NSDictionary *parameters=[[NSDictionary alloc] init];
    
    NSString *postUrl = [[NSString alloc] init];
    if (_isUpdateReview)
    {
        NSNumber *updateReviewId;
        if (isGuestMode || (gustModeReviewId!=nil) ) {
            updateReviewId = gustModeReviewId;
        }else{
            updateReviewId = _userReview.reviewId;
        }
        if (preReviewId!=nil) {
            updateReviewId = preReviewId;
        }
        parameters=@{@"t": user.token,
                     @"userId":user.userId,
                     @"restaurantId":restaurant.restaurantId,
                     @"rating":[NSNumber numberWithFloat:overallRating],
                     @"foodScore":[NSNumber numberWithFloat:overallRating],
                     @"ambianceScore":[NSNumber numberWithFloat:overallRating],
                     @"serviceScore":[NSNumber numberWithFloat:overallRating],
                     @"title":(textField.text && textField.text.length>0)?textField.text:@"",/*&&textView.text.length>40*/
                     @"review":textView.text?textView.text:@"",/* && textView.text.length>40*/
                     @"reviewerId":updateReviewId,
                     @"dishes":updateDishArry,
                     @"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] ,
                     @"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],
                     @"client":QRAVED_WEB_SERVICE_CLIENT,
                     @"v":QRAVED_WEB_SERVICE_VERSION,
                     @"appApiKey":QRAVED_APIKEY
                     };
        
        postUrl = @"review/update";
        
        
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:[self trackOrigin] forKey:@"Origin"];
        [eventProperties setValue:((_photoIds && _photoIds.length>0)?@"Text + Photo":@"Text only") forKey:@"Type"];
        [eventProperties setValue:updateReviewId forKey:@"Review_ID"];
        [[Amplitude instance] logEvent:@"UC - Edit Review Initiate" withEventProperties:eventProperties];
        
        [ReviewPublishHandler updateReviewWithParams:parameters andBlock:^(NSDictionary *reviewDictionary, NSString *exceptionMsg, NSString *ratingMap) {
            if([exceptionMsg isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
               
                btnPost.enabled = YES;
                
                LoginViewController *loginVC=[[LoginViewController alloc]init];
                UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
                loginVC.autoPostDelegate = self;
                [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
                
                return;
            }
            
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:updateReviewId forKey:@"Review_ID"];
            [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
            [[Amplitude instance] logEvent:@"UC - Edit Review Succeed" withEventProperties:eventProperties];
            
            if (self.reviewPublishViewControllerDelegate)
            {
                [self.reviewPublishViewControllerDelegate writeReviewReloadUploadPhotoCell:self];
            }
            
            [[LoadingView sharedLoadingView] stopLoading];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadUserReviewsView" object:nil userInfo:nil];
            
            NSMutableDictionary* faDictonary = [[NSMutableDictionary alloc] init];
            [faDictonary setValue:reviewDictionary forKey:@"newReview"];
            
            [faDictonary setValue:ratingMap forKey:@"newRatingMap"];
            
            IMGReview *review = [[IMGReview alloc]init];
            [review setValuesForKeysWithDictionary:reviewDictionary];
            review.restaurantId = restaurant.restaurantId;
            
            [[DBManager manager]insertModel:review selectField:@"reviewId" andSelectID:review.reviewId];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_HOME object:nil userInfo:nil];
            
            ShareReviewViewController *shareReviewViewController;
            if (gustModeReviewId!=nil) {
                shareReviewViewController = [[ShareReviewViewController alloc]initWithRestaurantTitle:restaurant.title andReviewID:[gustModeReviewId intValue]];
            }else{
                shareReviewViewController = [[ShareReviewViewController alloc]initWithRestaurant:_userReview];
            }
            shareReviewViewController.isWriteReview = !self.isUploadPhoto;
            shareReviewViewController.isFromOnboard = self.isFromOnboard;
            shareReviewViewController.reviewDescription = textView.text;
            [self.navigationController pushViewController:shareReviewViewController animated:YES];

            
            if(textView.text && textView.text.length>0){
                [faDictonary setValue:@(YES) forKey:@"isNotJustRating"];
            }else{
                [faDictonary setValue:@(NO) forKey:@"isNotJustRating"];
            }
            if (self.jnCellIndexPath) {
                [faDictonary setValue:self.jnCellIndexPath forKey:@"indexPath"];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadReviewsView_v1WithUpdateUserReview" object:nil userInfo:faDictonary];
            if (exceptionMsg.length>0) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:restaurant.title forKey:@"RestaurantTitle"];
                [eventProperties setValue:exceptionMsg forKey:@"Reason"];
                [eventProperties setValue:updateReviewId forKey:@"Review_ID"];
                
                [[Amplitude instance] logEvent:@"UC - Edit Review Failed" withEventProperties:eventProperties];
            }
            [SVProgressHUD showImage:[UIImage imageNamed:@"PostSuccess"] status:@"Thank you for your review!"];
            [SVProgressHUD dismissWithDelay:1.5];

            if (self.isUploadPhoto) {
                [self deleteUploadPhotoDraft];
            }else{
                [self deleteDraft];
            }
            if (textView.text.length>0) {//&&textView.text.length<40
                
                [[NSUserDefaults standardUserDefaults] setValue:textView.text forKey:[NSString stringWithFormat:@"reviewContent%@",restaurant.restaurantId]];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"hasDraft%@",restaurant.restaurantId]];
                if (textField.text.length>0) {
                    [[NSUserDefaults standardUserDefaults] setValue:textField.text forKey:[NSString stringWithFormat:@"reviewTitle%@",restaurant.restaurantId]];                }
                NSMutableArray *reviewDraftArray = [NSMutableArray array];
                NSMutableArray* tempArray = (NSMutableArray*)[[NSUserDefaults standardUserDefaults] objectForKey:@"reviewDrafts"];
                for (NSDictionary *dic in tempArray) {
                    if ([[dic objectForKey:@"restaurantId"] intValue]==[restaurant.restaurantId intValue]) {
                        return;
                    }
                }
                NSMutableDictionary *draftDict = [NSMutableDictionary dictionary];
                if (restaurant.imageUrl.length>0) {
                    [draftDict setValue:restaurant.imageUrl forKey:@"restaurantImageUrl"];
                    
                }
                if ([restaurant.ratingScore intValue]>0) {
                    [draftDict setValue:restaurant.ratingScore forKey:@"restaurantRatingCount"];
                    
                }
                if (restaurant.landMarkList.firstObject) {
                    [draftDict setValue:[restaurant.landMarkList.firstObject objectForKey:@"name" ]forKey:@"restaurantArea"];
                    
                }else if (restaurant.districtName.length>0) {
                    [draftDict setValue:restaurant.districtName forKey:@"restaurantArea"];
                    
                }
                if (restaurant.title.length>0) {
                    [draftDict setValue:restaurant.title forKeyPath:@"restaurantTitle"];
                }
                
                [draftDict setValue:restaurant.restaurantId forKey:@"restaurantId"];
                [reviewDraftArray addObject:draftDict];
                [reviewDraftArray addObjectsFromArray:tempArray];
                [[NSUserDefaults standardUserDefaults] setObject:reviewDraftArray forKey:@"reviewDrafts"];
            }

        } anderrorBlock:^(NSString *str) {
            btnPost.enabled = YES;
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:restaurant.title forKey:@"RestaurantTitle"];
            [eventProperties setValue:str forKey:@"Reason"];
            [eventProperties setValue:updateReviewId forKey:@"Review_ID"];
            
            [[Amplitude instance] logEvent:@"UC - Edit Review Failed" withEventProperties:eventProperties];
        }];
        
    }
    else
    {
        parameters=@{@"dishes":dishArry,
                     @"t": user.token,
                     @"userId":user.userId,
                     @"restaurantId":restaurant.restaurantId,
                     @"rating":[NSNumber numberWithFloat:overallRating],
                     @"foodScore":[NSNumber numberWithFloat:overallRating],
                     @"ambianceScore":[NSNumber numberWithFloat:overallRating],
                     @"serviceScore":[NSNumber numberWithFloat:overallRating],
                     @"title":(textField.text && textField.text.length>0)?textField.text:@"",/*&&textView.text.length>40*/
                     @"review":textView.text?textView.text:@"",/*&& textView.text.length>40*/
                     @"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] ,
                     @"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],
                     @"client":QRAVED_WEB_SERVICE_CLIENT,
                     @"v":QRAVED_WEB_SERVICE_VERSION,
                     @"appApiKey":QRAVED_APIKEY
                     };
         

        userInteractCount = nil;
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
//        [eventProperties setValue:restaurant.title forKey:@"RestaurantTitle"];
        [eventProperties setValue:user.userId forKey:@"ReviewerUser_ID"];
        [eventProperties setValue:[self trackOrigin] forKey:@"Origin"];
        
        if (![textView.text isEmpty]) {
            [[Amplitude instance] logEvent:@"UC - Write Review Initiate" withEventProperties:eventProperties];
        }

        [ReviewPublishHandler writereviewWithParams:parameters andBlock:^(NSString *returnStatusString, NSString *exceptionMsg, NSString *userInteractCounts, NSNumber *returnReviewId, NSDictionary *reviewDictionary, NSString *ratingMap) {
            userInteractCount =userInteractCounts;
            if ([exceptionMsg
                 hasPrefix:@"Vendor can not write review for his restaurant"]) {
                [[LoadingView sharedLoadingView] stopLoading];
                
                [SVProgressHUD showImage:[UIImage imageNamed:@""] status:L(@"ERROR\nYou cannot  give rating or write review on your own restaurant")];
                [SVProgressHUD dismissWithDelay:1.5];

                return ;
                
            }
            
            if ([exceptionMsg hasPrefix:@"review exists, reviewId:"]) {
                NSRange idRange = [exceptionMsg rangeOfString:@"review exists, reviewId:"];
                NSString *reviewId = [exceptionMsg substringFromIndex:idRange.length];
                preReviewId = [NSNumber numberWithLongLong:[reviewId longLongValue]];
                //                _isUpdateReview = YES;
                return ;
            }
            if ([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"hasDraft%@",restaurant.restaurantId]]){
                [[Amplitude instance] logEvent:@"UC - Post Draft"];
            }
            if (!self.isUploadPhoto) {
                [self deleteDraft];
                
            }
            if (textView.text.length>0) {//&&textView.text.length<40
                
                [[NSUserDefaults standardUserDefaults] setValue:textView.text forKey:[NSString stringWithFormat:@"reviewContent%@",restaurant.restaurantId]];
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"hasDraft%@",restaurant.restaurantId]];
                if (textField.text.length>0) {
                    [[NSUserDefaults standardUserDefaults] setValue:textField.text forKey:[NSString stringWithFormat:@"reviewTitle%@",restaurant.restaurantId]];                }
                NSMutableArray *reviewDraftArray = [NSMutableArray array];
                NSMutableArray* tempArray = (NSMutableArray*)[[NSUserDefaults standardUserDefaults] objectForKey:@"reviewDrafts"];
                for (NSDictionary *dic in tempArray) {
                    if ([[dic objectForKey:@"restaurantId"] intValue]==[restaurant.restaurantId intValue]) {
                        return;
                    }
                }
                NSMutableDictionary *draftDict = [NSMutableDictionary dictionary];
                if (restaurant.imageUrl.length>0) {
                    [draftDict setValue:restaurant.imageUrl forKey:@"restaurantImageUrl"];
                    
                }
                if ([restaurant.ratingScore intValue]>0) {
                    [draftDict setValue:restaurant.ratingScore forKey:@"restaurantRatingCount"];
                    
                }
                if (restaurant.landMarkList.firstObject) {
                    [draftDict setValue:[restaurant.landMarkList.firstObject objectForKey:@"name" ]forKey:@"restaurantArea"];
                    
                }else if (restaurant.districtName.length>0) {
                    [draftDict setValue:restaurant.districtName forKey:@"restaurantArea"];
                    
                }
                if (restaurant.title.length>0) {
                    [draftDict setValue:restaurant.title forKeyPath:@"restaurantTitle"];
                }
                
                [draftDict setValue:restaurant.restaurantId forKey:@"restaurantId"];
                [reviewDraftArray addObject:draftDict];
                [reviewDraftArray addObjectsFromArray:tempArray];
                [[NSUserDefaults standardUserDefaults] setObject:reviewDraftArray forKey:@"reviewDrafts"];
            }
            if([exceptionMsg isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
                btnPost.enabled = YES;
                //headeView.postButton.userInteractionEnabled = YES;
                
                LoginViewController *loginVC=[[LoginViewController alloc]init];
                UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
                loginVC.autoPostDelegate = self;
                [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
                
                return;
            }else if([returnStatusString isEqualToString:@"succeed"]){
                
                if (self.reviewPublishViewControllerDelegate)
                {
                    [self.reviewPublishViewControllerDelegate writeReviewReloadUploadPhotoCell:self];
                }
                
                if(reviewDictionary!=nil && overallRating!=0){
                    IMGReview *review = [[IMGReview alloc]init];
                    [review setValuesForKeysWithDictionary:reviewDictionary];
                    review.restaurantId = restaurant.restaurantId;
                    
                    NSArray *dishArr=[reviewDictionary objectForKey:@"dishList"];
                    NSMutableArray *dishArray=[[NSMutableArray alloc] init];
                    for (NSDictionary *dishdic in dishArr) {
                        IMGDish *dish = [[IMGDish alloc] init];
                        [dish setValuesForKeysWithDictionary:dishdic];
                        dish.descriptionStr=[dishdic objectForKey:@"description"];
                        dish.reviewId=returnReviewId;
                        [dishArray addObject:dish];
                        
                    }
                    
                    int reviewCount = [restaurant.reviewCount intValue];
                    int newReviewCount = reviewCount + 1;
                    float avgScore = overallRating;
                    float newRatingScore = ([restaurant.ratingScore floatValue] * reviewCount + avgScore)/newReviewCount;
                    
                    NSMutableArray *reviewListArr = (NSMutableArray *)[restaurant.ratingCounts componentsSeparatedByString:@","];
                    
                    if (reviewListArr.count > 2)
                    {
                        int oldRatingScoreNum = [[reviewListArr objectAtIndex:avgScore/2-1] intValue];
                        [reviewListArr replaceObjectAtIndex:avgScore/2-1  withObject:[NSString stringWithFormat:@"%d",oldRatingScoreNum+1]];
                        NSString *reviewListStr = [[NSString alloc] init];
                        for (NSString *str in reviewListArr)
                        {
                            if (!reviewListStr.length)
                            {
                                reviewListStr = [NSString stringWithFormat:@"%@",str];
                            }
                            else
                                reviewListStr = [NSString stringWithFormat:@"%@,%@",reviewListStr,str];
                        }
                        restaurant.ratingCounts = reviewListStr;
                    }
                    
                    
                    restaurant.ratingScore = [NSString stringWithFormat:@"%f",newRatingScore];
                    restaurant.reviewCount = [NSNumber numberWithInt:newReviewCount];
                    [[DBManager manager] insertModel:restaurant selectField:@"restaurantId" andSelectID:restaurant.restaurantId];
                    
                    
                    [[DBManager manager]insertModel:review selectField:@"reviewId" andSelectID:review.reviewId];
                    //                    [MixpanelHelper trackWriteReview:restaurant];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_HOME object:nil userInfo:nil];
                    
                    NSMutableDictionary* faDictonary = [[NSMutableDictionary alloc] init];
                    [faDictonary setValue:reviewDictionary forKey:@"newReview"];
                    
                    [faDictonary setValue:ratingMap forKey:@"newRatingMap"];
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadUserReviewsView" object:nil userInfo:nil];
                    
                    if(textView.text && textView.text.length>0){
                        [faDictonary setValue:@(YES) forKey:@"isNotJustRating"];
                    }else{
                        [faDictonary setValue:@(NO) forKey:@"isNotJustRating"];
                    }
                    if (self.jnCellIndexPath) {
                        [faDictonary setValue:self.jnCellIndexPath forKey:@"indexPath"];
                    }
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadReviewsView_v1WithUpdateUserReview" object:nil userInfo:faDictonary];
                    
                    if ([textField.text isEmpty]&&[textView.text isEmpty]) {
                        //Rating_ID, Restaurant_ID, Rating_Value
                        
                        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                        [eventProperties setValue:review.reviewId forKey:@"Rating_ID"];
                        [eventProperties setValue:review.restaurantId forKey:@"Restaurant_ID"];
                        [eventProperties setValue:@(overallRating) forKey:@"Rating_Value"];
                        [eventProperties setValue:[self trackOrigin] forKey:@"Origin"];
                        [[Amplitude instance] logEvent:@"UC - Give Rating Succeed" withEventProperties:eventProperties];
                        [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Give Rating Succeed" withValues:eventProperties];
                    }else{
                        
                        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                        [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
                        [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
                        [eventProperties setValue:review.restaurantId forKey:@"Restaurant_ID"];
                        [eventProperties setValue:[self trackOrigin] forKey:@"Origin"];
                        [[Amplitude instance] logEvent:@"UC - Write Review Succeed" withEventProperties:eventProperties];
                        [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Write Review Succeed" withValues:eventProperties];
                    }
                }
                
                [ReviewPublishHandler dashboardWithParams:parameters];
                
                if(userInteractCount&&[userInteractCount intValue]<=3&&[userInteractCount intValue]>0){
                    UserIncentiveViewController *userIncentivevc = [[UserIncentiveViewController alloc] initWithCount:[userInteractCount intValue]];
                    [self.navigationController presentViewController:userIncentivevc animated:YES completion:^{
                        ShareReviewViewController *shareReviewViewController = [[ShareReviewViewController alloc]initWithRestaurantTitle:restaurant.title andReviewID:[returnReviewId intValue]];
                        shareReviewViewController.isWriteReview = !self.isUploadPhoto;
                        shareReviewViewController.reviewDescription = textView.text;
                        shareReviewViewController.isFromOnboard = self.isFromOnboard;
                        userInteractCount = nil;
                        [self.navigationController pushViewController:shareReviewViewController animated:NO];
                    }];
                    
                }else{
                    [SVProgressHUD showImage:[UIImage imageNamed:@"PostSuccess"] status:@"Thank you for your review!"];
                    [SVProgressHUD dismissWithDelay:1.5];
                    ShareReviewViewController *shareReviewViewController = [[ShareReviewViewController alloc]initWithRestaurantTitle:restaurant.title andReviewID:[returnReviewId intValue]];
                    shareReviewViewController.isWriteReview = !self.isUploadPhoto;
                    shareReviewViewController.reviewDescription = textView.text;
                    shareReviewViewController.isFromOnboard = self.isFromOnboard;
                    userInteractCount = nil;
                    
                    [self.navigationController pushViewController:shareReviewViewController animated:YES];
                }
//                if (self.navigationController) {
//
//                }
//                else
//                {
//                    [self closeButtonClick];
//                }
                
            }
            else{
                [[LoadingView sharedLoadingView] stopLoading];
            }
            if (exceptionMsg.length>0) {
                
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:restaurant.title forKey:@"RestaurantTitle"];
                [eventProperties setValue:user.userId forKey:@"ReviewerUser_ID"];
                [eventProperties setValue:exceptionMsg forKey:@"Reason"];
                if ([textField.text isEmpty]&&[textView.text isEmpty]) {
                    [eventProperties setValue:@(overallRating) forKey:@"Rating_Value"];
                    [[Amplitude instance] logEvent:@"UC - Give Rating Failed" withEventProperties:eventProperties];
                }else{
                    [[Amplitude instance] logEvent:@"UC - Write Review Failed" withEventProperties:eventProperties];
                }
                
            }

        } anderrorBlock:^(NSString *str) {
            btnPost.enabled = YES;
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:restaurant.title forKey:@"RestaurantTitle"];
            [eventProperties setValue:user.userId forKey:@"ReviewerUser_ID"];
            [eventProperties setValue:str forKey:@"Reason"];
            if ([textField.text isEmpty]&&[textView.text isEmpty]) {
                [eventProperties setValue:@(overallRating) forKey:@"Rating_Value"];
                [[Amplitude instance] logEvent:@"UC - Give Rating Failed" withEventProperties:eventProperties];
            }else{
                [[Amplitude instance] logEvent:@"UC - Write Review Failed" withEventProperties:eventProperties];
            }
            
            [[LoadingView sharedLoadingView] stopLoading];

        }];
     
        
    }
}
-(void)uploadDishePhotos:(IMGUser *)user currentIndex:(NSInteger)currentIndex maxIndex:(NSInteger)maxIndex{
    PostDataHandler *postDataHandler=[[PostDataHandler alloc]init];
    [[LoadingView sharedLoadingView]startLoading];
    IMGUploadPhoto *uploadPhoto = [_photosArrM objectAtIndex:currentIndex];
    if (uploadPhoto.isOldPhoto)
    {
        [updateDishArry addObject:@{@"imageUrl":uploadPhoto.imageUrl,@"title":@"",@"description":uploadPhoto.descriptionStr,@"id":uploadPhoto.dishId}];

        if(currentIndex<maxIndex){
            [self uploadDishePhotos:user currentIndex:currentIndex+1 maxIndex:maxIndex];
        }else{
            [self publishReview];
        }
        return;
    }
    
    NSString *descriptionStr=uploadPhoto.descriptionStr;
    if (!descriptionStr)
    {
        descriptionStr = @"";
    }
    NSNumber *menuId=[NSNumber numberWithInt:0];
    
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:uploadPhoto.dishId forKey:@"Photo_ID"];
    [eventProperties setValue:user.userId forKey:@"UploaderUser_ID"];
    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:(uploadPhoto.isFromCamera?@"New photo taken from camera":@"Existing photo on device image gallery") forKey:@"Source"];
    [eventProperties setValue:[self trackOrigin] forKey:@"Origin"];
    
    [[Amplitude instance] logEvent:@"UC - Upload Photo Initiate" withEventProperties:eventProperties];
    [postDataHandler postImage:uploadPhoto.image andTitle:@"image" andDescription:@"image" andSuccessBlock:^(id postArray){
        if (self.isUploadPhoto) {
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:uploadPhoto.dishId forKey:@"Photo_ID"];
            [eventProperties setValue:user.userId forKey:@"UploaderUser_ID"];
            [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:(uploadPhoto.isFromCamera?@"New photo taken from camera":@"Existing photo on device image gallery") forKey:@"Source"];
            [eventProperties setValue:[self trackOrigin] forKey:@"Origin"];
            [[Amplitude instance] logEvent:@"UC - Upload Photo Succeed" withEventProperties:eventProperties];
            [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Upload Photo Succeed" withValues:eventProperties];
        }
        NSString *imageUrl=[postArray objectForKey:@"imgPath"];
        NSLog(@"reviewPublish imageUrl = %@",imageUrl);
//'{"userId":107,"t":"token","restaurantId":570,"rating":5,"dishes":[{"imageUrl":"Google","title":"Googlet","description":"Googled"},{"imageUrl":"Baidu","title":"Baidut","description":"Baidud"},{"imageUrl":"SoSo","title":"SoSot","description":"SoSod"}],"review":"woiewhoewfoiew","foodScore":5,"ambianceScore":5,"title":"","latitude":0,"longitude":1.14,"client":1,"v":"4.0.2","serviceScore":5}'
        [dishArry addObject:@{@"imageUrl":imageUrl,@"title":@"",@"description":descriptionStr}];
        [updateDishArry addObject:@{@"imageUrl":imageUrl,@"title":@"",@"description":descriptionStr,@"id":@0}];
        if (currentIndex<maxIndex&&!self.isUploadPhoto) {
            [self uploadDishePhotos:user currentIndex:currentIndex+1 maxIndex:maxIndex];
            return ;
        }
        NSDictionary *parameters=@{@"t": user.token,@"userID":user.userId,@"restaurantID":restaurant.restaurantId,@"dishTitle":@"",@"imageUrl":imageUrl,@"description":descriptionStr,@"menuID":menuId,@"reviewId":[NSNumber numberWithInt:0]};
        if (self.isFromOnboard)
        {
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:restaurant.title forKey:@"RestaurantTitle"];
            [eventProperties setValue:self.amplitudeType forKey:@"Source"];
            [[Amplitude instance] logEvent:@"OB - Click Upload 1st Photo" withEventProperties:eventProperties];
            [self requestToAddDish:parameters currentIndex:currentIndex maxIndex:maxIndex user:user];
        }else if(self.isFromJourney) {
            NSMutableDictionary *dishList = [[NSMutableDictionary alloc] init];
            [dishList setObject:@"" forKey:@"dishTitle"];
            [dishList setObject:imageUrl forKey:@"imageUrl"];
            [imageUrlArr addObject:dishList];
            
            uploadPhoto.imageUrl = imageUrl;
            
            if(currentIndex<maxIndex){
                [self uploadDishePhotos:user currentIndex:currentIndex+1 maxIndex:maxIndex];
            }else{
                [self batchupload];
            }
        }else{

            if (self.isUploadPhoto) {
                [self requestToAddDish:parameters currentIndex:currentIndex maxIndex:maxIndex user:user];

            }else{
                [self publishReview];
            }
        }
    }andFailedBlock:^(NSError *error){
        NSLog(@"upload photo failed, error:%@",error.description);
        btnPost.enabled = YES;
        //headeView.postButton.userInteractionEnabled = YES;
        if (self.isUploadPhoto) {
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:uploadPhoto.dishId forKey:@"Photo_ID"];
            [eventProperties setValue:user.userId forKey:@"UploaderUser_ID"];
            [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:error.localizedDescription forKey:@"Reason"];
            [[Amplitude instance] logEvent:@"UC - Upload Photo Failed" withEventProperties:eventProperties];
        }
        
        [[LoadingView sharedLoadingView] stopLoading];
        NSString* noticeStr=L(@"Please check your internet connection and try again.");
        [SVProgressHUD showImage:[UIImage imageNamed:@""] status:noticeStr];
        [SVProgressHUD dismissWithDelay:1.5];
    }];
}
-(void)batchupload{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:restaurant.restaurantId forKey:@"restaurantId"];
    [param setObject:[IMGUser currentUser].userId forKey:@"userId"];
    [param setObject:[IMGUser currentUser].token forKey:@"token"];
    [param setObject:self.uploadedPhotoId forKey:@"uploadedPhotoId"];
    [param setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [param setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    [param setObject:imageUrlArr forKey:@"dishList"];
    
    [PostDataHandler batchupload:param andSuccessBlock:^(id responseObject){
        
        for(IMGUploadPhoto *item in _photosArrM){
            if (item.dishId == nil) {
                for (NSDictionary *dicItem in responseObject){
                    NSDictionary *dic = [dicItem objectForKey:@"dishMap"];
                    if ([item.imageUrl isEqualToString:[dic objectForKey:@"imageUrl"]]) {
                        item.dishId = [dic objectForKey:@"id"];
                    }
                }
            }
        }
        
        NSMutableDictionary* faDictonary = [[NSMutableDictionary alloc] init];
        [faDictonary setValue:self.jnCellIndexPath forKey:@"indexPath"];
        [faDictonary setValue:responseObject forKey:@"responseObject"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadJourneyUploadPhoto" object:nil userInfo:faDictonary];
        
        [self publishReview];
    } andFailedBlock:^{
        [self publishReview];
    }];
}
-(void)requestToAddDish:(NSDictionary *)parameters  currentIndex:(NSInteger)currentIndex maxIndex:(NSInteger)maxIndex user:(IMGUser *)user{
    userInteractCount = nil;
    NSMutableDictionary *params=[[NSMutableDictionary alloc] init];
    [params setDictionary:parameters];
    [params setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [params setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    [ReviewPublishHandler uploadDishWithPrama:params andSuccessBlock:^(NSDictionary *dishDictionary, NSString *returnStatusString, NSString *exceptionMsg,NSString *photoId) {
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            btnPost.enabled = YES;
            //headeView.postButton.userInteractionEnabled = YES;
            LoginViewController *loginVC=[[LoginViewController alloc]init];
            UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
            loginVC.reviewPublishViewDelegate = self;
            [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
            return;
        }else if([returnStatusString isEqualToString:@"succeed"]){
            
            if (_photoIds.length == 0) {
                [_photoIds appendFormat:@"%@",photoId];
            }else{
                [_photoIds appendFormat:@",%@",photoId];
            }
            if(dishDictionary!=nil){
                IMGDish *dish = [[IMGDish alloc]init];
                [dish setValuesForKeysWithDictionary:dishDictionary];
                dish.descriptionStr=[dishDictionary objectForKey:@"description"];
                dish.userName=[NSString stringWithFormat:@"%@ %@",user.firstName,user.lastName];
                dish.createTime=(NSNumber*)[[[dishDictionary objectForKey:@"createTime"] stringValue] substringToIndex:10];
                [[DBManager manager]insertModel:dish selectField:@"dishId" andSelectID:dish.dishId];
                [self.reviewPublishViewControllerDelegate reloadPhotosImageView:@[dish]];
                //                [MixpanelHelper trackUploadPhoto:restaurant andDishName:dish.title];
                [dishIdsArr addObject:dish.dishId];
            }
            if(currentIndex<maxIndex){
                [self uploadDishePhotos:user currentIndex:currentIndex+1 maxIndex:maxIndex];
            }else{
                if(dishIdsArr.count && self.isUploadPhoto){
                    [PostDataHandler uploadDishes:[dishIdsArr componentsJoinedByString:@","] andSuccessBlock:^(id responseObject){
//                        if (!self.isFromHotkey) {
//                            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_HOME object:nil userInfo:nil];
//                        }
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadJourneyUploadPhoto" object:nil userInfo:nil];
                        userInteractCount = [responseObject objectForKey:@"userInteractCount"];
                        [self publishReview];
                        [self deleteUploadPhotoDraft];
                        
                    } andFailedBlock:^{
                        [self publishReview];
                    }];
                }
                else if (!self.isUploadPhoto)
                {
                    [self publishReview];
                }
            }
        }

    } anderrorBlock:^(NSString *str) {
        [[LoadingView sharedLoadingView] stopLoading];
        btnPost.enabled = YES;
        //headeView.postButton.userInteractionEnabled = YES;

    }];
}
- (void)handleTapBehind:(UITapGestureRecognizer *)sender{
    [alerController dismissViewControllerAnimated:YES completion:nil];
    [[Amplitude instance] logEvent:@"CL - Dismiss Automatic Save Prompt"];
    [recognizerTap removeTarget:self action:nil];

}
- (void)deleteDishWithService{
    if (!delDishIds)
    {
        return;
    }
    IMGUser *user = [IMGUser currentUser];
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:user.token,@"t",user.userId,@"userId",delDishIds,@"ids", nil];
    [ReviewPublishHandler deleteDishWithParams:parameters andBlock:^{
        
    }];
    
}
- (void)editDishDescription{
    NSMutableArray *dishList = [[NSMutableArray alloc] init];
    for (IMGUploadPhoto *photo in _photosArrM)
    {
        NSDictionary *dishDic = [[NSDictionary alloc] initWithObjectsAndKeys:photo.dishId,@"dishId",photo.descriptionStr,@"description", nil];
        [dishList addObject:dishDic];
    }
    if (!_photosArrM.count)
    {
        return;
    }
    
    IMGUser *user = [IMGUser currentUser];
    
    NSMutableDictionary *dataDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",dishList,@"dishList",_userReview.reviewId,@"reviewId", nil];
    [ReviewPublishHandler editdescriptionWithParams:dataDic];
    
}
#pragma mark - text fieled and view delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField.tag == 100)
    {
        return YES;
    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:textField.tag inSection:0];
    CGRect cellRect = [_tableView rectForRowAtIndexPath:indexPath];
    CGFloat bgViewHeight;
    IMGUploadPhoto *photo = [_photosArrM objectAtIndex:indexPath.row];
    if (photo.isOldPhoto)
    {
        bgViewHeight = DeviceWidth;
    }
    else
    {
        CGSize size = CGSizeFromString(NSStringFromCGSize(photo.image.size));
        CGFloat height = size.height/(size.width/DeviceWidth);
        bgViewHeight = height;
    }
    
    [_tableView setContentOffset:CGPointMake(0, cellRect.origin.y + bgViewHeight - 80) animated:YES];
    
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField.tag<100)
    {
        for (IMGUploadPhoto *photo in _photosArrM)
        {
            if (photo.tag == textField.tag)
            {
                photo.descriptionStr = toBeString;
                break;
            }
        }
        
    }
    return YES;
}
-(void)textViewDidChangeSelection:(UITextView *)textView{
    if (isAlreadyFirstOpenGoToHead) {
        CGPoint cursorPosition = [textView caretRectForPosition:textView.selectedTextRange.start].origin;
        [_tableView setContentOffset:CGPointMake(0,textField.endPointY+cursorPosition.y-50) animated:YES];
    }
    if (textView.text.length>0) {
        labeltext.hidden=YES;
    }
    isAlreadyFirstOpenGoToHead = YES;
}
-(void)textViewDidBeginEditing:(UITextView *)textView{
    isAlreadyFirstOpenGoToHead = NO;
    if (!isAlreadyFirstOpenGoToHead) {
        CGPoint cursorPosition = [textView caretRectForPosition:textView.selectedTextRange.start].origin;
//        [_tableView setContentOffset:CGPointMake(0,textField.endPointY+cursorPosition.y) animated:YES];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    titleString = textField.text;
    return YES;
}
-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    contentString = textView.text;
    CGRect textViewFrame=textView.frame;
    CGPoint offset=_tableView.contentOffset;
    CGFloat lineHeight=22;
    CGSize size = [textView sizeThatFits:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, MAXFLOAT)];

    if (size.height>90) {
        textView.frame=CGRectMake(textViewFrame.origin.x,textViewFrame.origin.y,textViewFrame.size.width,size.height+ lineHeight);
        
        vUpload.frame = CGRectMake(0, textView.endPointY, DeviceWidth, 81);
        publishView.frame = CGRectMake(0, 0, DeviceWidth, vUpload.endPointY);
        _tableView.tableHeaderView=publishView;
        lastTextViewHeight = textView.contentSize.height + lineHeight;
        _tableView.contentOffset=CGPointMake(0,offset.y+lineHeight);
    }
    
    return YES;
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    textView.scrollEnabled = YES;
    if (textView.text.length==0){
        if ([text isEqualToString:@""]) {
            labeltext.hidden=NO;
        }else{
            labeltext.hidden=YES;
        }
    }else{
        if (textView.text.length==1){
            if ([text isEqualToString:@""]) {
                labeltext.hidden=NO;
            }else{
                labeltext.hidden=YES;
            }
        }else{
            labeltext.hidden=YES;
        }
        
        CGRect textViewFrame=textView.frame;
        CGPoint offset=_tableView.contentOffset;
        CGFloat lineHeight=22;
        
        if (!lastTextViewHeight)
        {
            lastTextViewHeight = textView.contentSize.height;
            return YES;
        }
        
        if (lastTextViewHeight == textView.contentSize.height)
        {
            if ([text isEqualToString:@"\n"])
            {
                textView.frame=CGRectMake(textViewFrame.origin.x,textViewFrame.origin.y,textViewFrame.size.width,textView.contentSize.height+ lineHeight);
                
                vUpload.frame = CGRectMake(0, textView.endPointY, DeviceWidth, 81);
                publishView.frame = CGRectMake(0, 0, DeviceWidth, vUpload.endPointY);
                _tableView.tableHeaderView=publishView;
                lastTextViewHeight = textView.contentSize.height+ lineHeight;
                _tableView.contentOffset=CGPointMake(0,offset.y+lineHeight);
                textView.scrollEnabled = NO;

                return YES;
            }
            else
                textView.scrollEnabled = NO;

                return YES;
        }
        
        if (lastTextViewHeight < textView.contentSize.height)
        {
            textView.frame=CGRectMake(textViewFrame.origin.x,textViewFrame.origin.y,textViewFrame.size.width,textView.contentSize.height);
            if ([text isEqualToString:@"\n"])
            {
                textView.frame=CGRectMake(textViewFrame.origin.x,textViewFrame.origin.y,textViewFrame.size.width,textView.contentSize.height+ lineHeight);
            }
            vUpload.frame = CGRectMake(0, textView.endPointY, DeviceWidth, 81);
            publishView.frame = CGRectMake(0, 0, DeviceWidth, vUpload.endPointY);

            _tableView.tableHeaderView=publishView;
            lastTextViewHeight = textView.contentSize.height;
            _tableView.contentOffset=CGPointMake(0,offset.y+lineHeight);
        }
        else
        {
            textView.frame=CGRectMake(textViewFrame.origin.x,textViewFrame.origin.y,textViewFrame.size.width,textView.contentSize.height);
            if ([text isEqualToString:@"\n"])
            {
                textView.frame=CGRectMake(textViewFrame.origin.x,textViewFrame.origin.y,textViewFrame.size.width,textView.contentSize.height+ lineHeight);
            }

            vUpload.frame = CGRectMake(0, textView.endPointY, DeviceWidth, 81);
            publishView.frame = CGRectMake(0, 0, DeviceWidth, vUpload.endPointY);
            _tableView.tableHeaderView=publishView;
            lastTextViewHeight = textView.contentSize.height;
            if (offset.y > 0)
            {
                _tableView.contentOffset=CGPointMake(0,offset.y-lineHeight);
            }
            
        }
    }
    textView.scrollEnabled = NO;

    return YES;
}
#pragma mark - tableView delegate and data source
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _photosArrM.count;
//    if (self.isUploadPhoto) {
//        return _photosArrM.count;
//    }else{
//        return 0;
//    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier =@"cell";
    AddPhotoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[AddPhotoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier andIsUploadMenuPhoto:NO];
        cell.selectionStyle=UITableViewCellSelectionStyleGray;
        //[cell setTextFieldInputAccessoryView:btnsView];
    }
    IMGUploadPhoto *photo = [_photosArrM objectAtIndex:indexPath.row];
    photo.tag = indexPath.row;
    cell.textField.delegate = self;
    cell.delegate = self;
    cell.textField.tag = indexPath.row;
    [cell setPhoto:photo];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    IMGUploadPhoto *photo = [_photosArrM objectAtIndex:indexPath.row];
    if (photo.isOldPhoto)
    {
        return DeviceWidth + 64;
    }
    else
    {
        CGSize size = CGSizeFromString(NSStringFromCGSize(photo.image.size));
        CGFloat height = size.height/(size.width/DeviceWidth);
        return height + 64;
    }
}
#pragma mark - save photo notification
- (void)savePhoto:(NSNotification *)notification{
    hasUploadPhoto=YES;
    [self updatePostStatus];
    IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
    photo.image = [notification.userInfo objectForKey:@"finishImage"];
    photo.isFromCamera = YES;
    [self latestAsset:^(ALAsset * _Nullable asset, NSError * _Nullable error) {
        photo.photoUrl = [NSString stringWithFormat:@"%@",[asset valueForProperty:ALAssetPropertyAssetURL]];
        [_photosArrM addObject:photo];
        [_tableView reloadData];
//        if (_isUploadPhoto) {
//            [_tableView reloadData];
//        }else{
//            reviewPhotoView.photoArray = _photosArrM;
//        }

    }];

    //    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - ZYQAssetPickerController Delegate
-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    hasUploadPhoto=YES;
    [self updatePostStatus];
    for (int i=0; i<assets.count; i++) {
        ALAsset *asset=assets[i];
        UIImageView *imgview=[[UIImageView alloc] init];
        imgview.contentMode=UIViewContentModeScaleAspectFill;
        imgview.clipsToBounds=YES;
        UIImage *tempImg=[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage];
        IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
        photo.image = tempImg;
        photo.photoUrl =[NSString stringWithFormat:@"%@",[asset valueForProperty:ALAssetPropertyAssetURL]];
        [_photosArrM addObject:photo];
    }
    [_tableView reloadData];
//    if (_isUploadPhoto) {
//        [_tableView reloadData];
//    }else{
//        reviewPhotoView.photoArray = _photosArrM;
//    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}
#pragma mark - textFieled and textView notification
-(void)titleChanged:(NSNotification*)notification{
    [self updatePostStatus];
}
-(void)reviewChanged:(NSNotification*)notification{
    UITextView *textview=notification.object;
    NSString *newReview=[textview.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    [self updatePostStatus];
    if(newReview.length==0){

        labeltext.hidden=NO;
        return;
    }
//    else if(newReview.length>=REVIEW_DESC_LENGTH){
//        btnsView.buttonCharNoticeLabel.hidden=YES;
//        buttonBtnsView.buttonCharNoticeLabel.hidden=YES;
//    }else{
//        NSString *minimum=[NSString stringWithFormat:@"%lu",REVIEW_DESC_LENGTH-newReview.length];
//        NSMutableAttributedString *notice=[[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"minimum character %@",minimum]];
//        [notice setAttributes:@{NSForegroundColorAttributeName:[UIColor color333333],NSFontAttributeName:[UIFont boldSystemFontOfSize:12]} range:NSMakeRange(notice.length-minimum.length,minimum.length)];
//        btnsView.buttonCharNoticeLabel.attributedText=notice;
//        buttonBtnsView.buttonCharNoticeLabel.attributedText=notice;
//        btnsView.buttonCharNoticeLabel.hidden=NO;
//        buttonBtnsView.buttonCharNoticeLabel.hidden=NO;
//    }
    labeltext.hidden=YES;
}

#pragma mark - searchBar delegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
    if (!self.isUploadPhoto) {
        selectReviewRestaurantViewController = [[RestaurantsSelectionViewController alloc] init];
        selectReviewRestaurantViewController.isFromHotkey = self.isFromHotkey;
        selectReviewRestaurantViewController.sourceType = selectionSourceWiteReviewType;
        selectReviewRestaurantViewController.photosArrM = _photosArrM;
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"writeReviewNotification" object:nil];
        [self.navigationController pushViewController:selectReviewRestaurantViewController animated:YES];

    }else{
        selectReviewRestaurantViewController = [[RestaurantsSelectionViewController alloc] init];
        selectReviewRestaurantViewController.isFromHotkey = self.isFromHotkey;
        if (self.isUploadPhoto) {
            selectReviewRestaurantViewController.sourceType = selectionSourceUploadPotoType;
        }
        selectReviewRestaurantViewController.photosArrM = _photosArrM;
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"writeReviewNotification" object:nil];
        [self.navigationController pushViewController:selectReviewRestaurantViewController animated:YES];
    }
}

#pragma mark -  review and photo draft
-(void)judageDraft{
    if (self.reviewTitle.length==0&&self.reviewContent.length==0&&self.overallRatings==0&&_photosArrM.count==0&&!self.isUploadPhoto&&!self.isEditReview&&!self.isUpdateReview) {
        [self loadInfomation];
    }
    if (_photosArrM.count==0&&self.isUploadPhoto&&self.isFromHotkey) {
        [self loadInfomation];
    }
    if (self.isUploadPhoto&&!self.isFromHotkey) {
        [self loadInfomation];
    }
    if (self.isUploadPhoto)
    {
        return;
    }
    if (self.reviewTitle.length>0||self.reviewContent.length>0||self.overallRatings>0)
    {
        textView.text = self.reviewContent;
        textField.text = self.reviewTitle;
        //        overallRating = self.overallRating;
       // [startRating setRating:overallRating/2];
        [ratingView updateRating:overallRating];
        CGSize size = [textView sizeThatFits:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, MAXFLOAT)];
        textView.frame=CGRectMake(LEFTLEFTSET-3, textField.endPointY+4, DeviceWidth-2*LEFTLEFTSET, size.height);
//        reviewPhotoView.frame = CGRectMake(0, textView.endPointY, DeviceWidth, 55);
        lastTextViewHeight = textView.contentSize.height;
        vUpload.frame = CGRectMake(0, textView.endPointY, DeviceWidth, 81);
        publishView.frame = CGRectMake(0, 0, DeviceWidth, vUpload.endPointY);
        _tableView.tableHeaderView = publishView;
    }else{
        if (self.isEditReview)
        {
            [self getReview];
        }
        else
        {
            if (_userReview.reviewSummarize.length)
            {
                labeltext.hidden = YES;
            }
            else
                labeltext.hidden = NO;
        }
    }

}
-(void)saveInfomation{
    if ([restaurant.restaurantId intValue]) {
        NSString *valueString=@"";
        if (overallRating>0){
            valueString =@"Rating";
        }
        if (textField.text.length>0) {
            if (valueString.length>0) {
                valueString=[NSString stringWithFormat:@"%@,Title",valueString];

            }else{
                valueString = @"Title";
            }
        }
        if (textView.text.length>0) {
            if (valueString.length>0) {
                valueString = [NSString stringWithFormat:@"%@,Review",valueString];

            }else{
                valueString = @"Review";
            }
        }
        
        if(_photosArrM.count>0){
            if (valueString.length>0) {
                valueString =[NSString stringWithFormat:@"%@,Photo",valueString];

            }else{
                valueString = @"Photo";
            }
        }
        NSString *origin;
        if (self.isUploadPhoto) {
            origin = @"Add Photo Page";
            valueString =@"Photo";
            
        }else{
            origin = @"Review Page";
        }
        
        [[Amplitude instance] logEvent:@"UC – Save as Draft" withEventProperties:@{@"type":@"Manual",@"Value":valueString}];
        if (overallRating&&!self.isUploadPhoto) {
            [[NSUserDefaults standardUserDefaults] setInteger:overallRating forKey:[NSString stringWithFormat:@"rating%@",restaurant.restaurantId]];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"hasDraft%@",restaurant.restaurantId]];
        }
        
        if (textField.text.length>0&&!self.isUploadPhoto) {
            [[NSUserDefaults standardUserDefaults] setValue:textField.text forKey:[NSString stringWithFormat:@"reviewTitle%@",restaurant.restaurantId]];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"hasDraft%@",restaurant.restaurantId]];

        }
        if (textField.text.length==0&&!self.isUploadPhoto) {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"reviewTitle%@",restaurant.restaurantId]];

        }
        if (textView.text.length>0&&!self.isUploadPhoto) {
            [[NSUserDefaults standardUserDefaults] setValue:textView.text forKey:[NSString stringWithFormat:@"reviewContent%@",restaurant.restaurantId]];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"hasDraft%@",restaurant.restaurantId]];
        }
        if (textView.text.length==0&&!self.isUploadPhoto) {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"reviewContent%@",restaurant.restaurantId]];
            
        }
        if (_photosArrM.count==0&&!self.isUploadPhoto) {
            [[NSUserDefaults standardUserDefaults] setInteger:_photosArrM.count forKey:[NSString stringWithFormat:@"reviewCount%@",restaurant.restaurantId]];
        }
        if (_photosArrM.count==0&&self.isUploadPhoto) {
            [[NSUserDefaults standardUserDefaults] setInteger:_photosArrM.count forKey:[NSString stringWithFormat:@"uploadPhotoCount%@",restaurant.restaurantId]];
            [[NSUserDefaults standardUserDefaults] setInteger:_photosArrM.count forKey:[NSString stringWithFormat:@"reviewCount%@",restaurant.restaurantId]];
        }
        if (_photosArrM.count>0&&!self.isUploadPhoto) {
            for (int i =0; i<_photosArrM.count; i++) {
                IMGUploadPhoto *poto = _photosArrM[i];
                [[NSUserDefaults standardUserDefaults] setValue:poto.photoUrl forKey:[NSString stringWithFormat:@"photo%@%d",restaurant.restaurantId,i]];
                if (poto.descriptionStr.length>0) {
                    [[NSUserDefaults standardUserDefaults] setValue:poto.descriptionStr forKey:[NSString stringWithFormat:@"PhotoDescription%@%d",restaurant.restaurantId,i]];
                    
                }

            }
            [[NSUserDefaults standardUserDefaults] setInteger:_photosArrM.count forKey:[NSString stringWithFormat:@"reviewCount%@",restaurant.restaurantId]];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"hasDraft%@",restaurant.restaurantId]];


        }else if(_photosArrM.count>0&&self.isUploadPhoto){
            for (int i =0; i<_photosArrM.count; i++) {
                IMGUploadPhoto *poto = _photosArrM[i];
                [[NSUserDefaults standardUserDefaults] setValue:poto.photoUrl forKey:[NSString stringWithFormat:@"photo%@%d1",restaurant.restaurantId,i]];
                if (poto.descriptionStr.length>0) {
                    [[NSUserDefaults standardUserDefaults] setValue:poto.descriptionStr forKey:[NSString stringWithFormat:@"PhotoDescription%@%d1",restaurant.restaurantId,i]];
                    [[NSUserDefaults standardUserDefaults] setValue:poto.descriptionStr forKey:[NSString stringWithFormat:@"PhotoDescription%@%d2",restaurant.restaurantId,i]];

                }
                [[NSUserDefaults standardUserDefaults] setValue:poto.photoUrl forKey:[NSString stringWithFormat:@"photo%@%d2",restaurant.restaurantId,i]];


            }
            [[NSUserDefaults standardUserDefaults] setInteger:_photosArrM.count forKey:[NSString stringWithFormat:@"uploadPhotoCount%@",restaurant.restaurantId]];
            [[NSUserDefaults standardUserDefaults] setInteger:_photosArrM.count forKey:[NSString stringWithFormat:@"reviewCount%@",restaurant.restaurantId]];

            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"hasDraftUploadPhoto%@",restaurant.restaurantId]];
            
            NSMutableArray *uploadPhotDraftArray = [NSMutableArray array];
            NSMutableArray* tempArray = (NSMutableArray*)[[NSUserDefaults standardUserDefaults] objectForKey:@"uploadPhotoDraft"];
            for (NSDictionary *dic in tempArray) {
                if ([[dic objectForKey:@"restaurantId"] intValue]==[restaurant.restaurantId intValue]) {
                    return;
                }
            }
            NSMutableDictionary *draftDict = [NSMutableDictionary dictionary];
            if (restaurant.imageUrl.length>0) {
                [draftDict setValue:restaurant.imageUrl forKey:@"restaurantImageUrl"];
                
            }
            if ([restaurant.ratingScore intValue]>0) {
                [draftDict setValue:restaurant.ratingScore forKey:@"restaurantRatingCount"];
                
            }
            if (restaurant.landMarkList.firstObject) {
                [draftDict setValue:[restaurant.landMarkList.firstObject objectForKey:@"name" ]forKey:@"restaurantArea"];
                
            }else if (restaurant.districtName.length>0) {
                [draftDict setValue:restaurant.districtName forKey:@"restaurantArea"];
                
            }
            if (restaurant.title.length>0) {
                [draftDict setValue:restaurant.title forKeyPath:@"restaurantTitle"];
            }
            
            [draftDict setValue:restaurant.restaurantId forKey:@"restaurantId"];
            [uploadPhotDraftArray addObject:draftDict];
            [uploadPhotDraftArray addObjectsFromArray:tempArray];
            [[NSUserDefaults standardUserDefaults] setObject:uploadPhotDraftArray forKey:@"uploadPhotoDraft"];

        }
        if ([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"hasDraftUploadPhoto%@",restaurant.restaurantId]]) {
            [SVProgressHUD showImage:[UIImage imageNamed:@""] status:@"A draft has been saved to this device"];
            [SVProgressHUD dismissWithDelay:1.5];
            
        }
        if ([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"hasDraft%@",restaurant.restaurantId]]) {
            [SVProgressHUD showImage:[UIImage imageNamed:@""] status:@"A draft has been saved to this device"];
            [SVProgressHUD dismissWithDelay:1.5];
            NSMutableArray *reviewDraftArray = [NSMutableArray array];
            NSMutableArray* tempArray = (NSMutableArray*)[[NSUserDefaults standardUserDefaults] objectForKey:@"reviewDrafts"];
            for (NSDictionary *dic in tempArray) {
                if ([[dic objectForKey:@"restaurantId"] intValue]==[restaurant.restaurantId intValue]) {
                    return;
                }
            }
            NSMutableDictionary *draftDict = [NSMutableDictionary dictionary];
            if (restaurant.imageUrl.length>0) {
                [draftDict setValue:restaurant.imageUrl forKey:@"restaurantImageUrl"];

            }
            if ([restaurant.ratingScore intValue]>0) {
                [draftDict setValue:restaurant.ratingScore forKey:@"restaurantRatingCount"];

            }
            if (restaurant.landMarkList.firstObject) {
                [draftDict setValue:[restaurant.landMarkList.firstObject objectForKey:@"name" ]forKey:@"restaurantArea"];

            }else if (restaurant.districtName.length>0) {
                [draftDict setValue:restaurant.districtName forKey:@"restaurantArea"];

            }
            if (restaurant.title.length>0) {
                [draftDict setValue:restaurant.title forKeyPath:@"restaurantTitle"];
            }
            
            [draftDict setValue:restaurant.restaurantId forKey:@"restaurantId"];
            [reviewDraftArray addObject:draftDict];
            [reviewDraftArray addObjectsFromArray:tempArray];
            [[NSUserDefaults standardUserDefaults] setObject:reviewDraftArray forKey:@"reviewDrafts"];
        }
    
    }
}
-(void)loadInfomation{
    if ([restaurant.restaurantId intValue]) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"hasDraft%@",restaurant.restaurantId]]||[[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"hasDraftUploadPhoto%@",restaurant.restaurantId]]) {
            if (self.amplitudeType.length>0) {
                [[Amplitude instance] logEvent:@"UC - Continue Draft" withEventProperties:@{@"Origin":self.amplitudeType}];
            }
            if (!self.isUploadPhoto&&!self.isFromDraft) {
                [SVProgressHUD showImage:[UIImage imageNamed:@""] status:@"You have an unpublished draft"];
                [SVProgressHUD dismissWithDelay:1.5];
    


            }
            float starCount =[[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"rating%@",restaurant.restaurantId]];
            if (starCount) {
                overallRating=starCount;
            }
            NSString *tempTitle =  [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"reviewTitle%@",restaurant.restaurantId]]];
            if (tempTitle.length>0&&![tempTitle isEqualToString:@"(null)"]) {
                textField.text=tempTitle;
                titleString=textField.text;
                self.reviewTitle=titleString;
            }
            NSString *tempContent=  [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"reviewContent%@",restaurant.restaurantId]]];
            if (tempContent.length>0&&![tempContent isEqualToString:@"(null)"]) {
                textView.text=tempContent;
                self.reviewContent = textView.text;
                contentString = textView.text;
                CGSize size = [textView sizeThatFits:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, MAXFLOAT)];
                textView.frame=CGRectMake(LEFTLEFTSET-3, textField.endPointY+4, DeviceWidth-2*LEFTLEFTSET, size.height);
                lastTextViewHeight = textView.contentSize.height;
                vUpload.frame = CGRectMake(0, textView.endPointY, DeviceWidth, 81);
                publishView.frame = CGRectMake(0, 0, DeviceWidth, vUpload.endPointY);
                _tableView.tableHeaderView = publishView;
            }
            NSInteger photoCount = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"reviewCount%@",restaurant.restaurantId]];
            if (photoCount>0&&!self.isUploadPhoto) {
                for (int i =0; i<photoCount; i++) {
                    NSString *descriptionString = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"PhotoDescription%@%d",restaurant.restaurantId,i]];
                    NSString *descriptionString2 = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"PhotoDescription%@%d2",restaurant.restaurantId,i]];

                    [self getImageWith:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"photo%@%d",restaurant.restaurantId,i]]] andDescriptionStr:descriptionString];
                    [self getImageWith:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"photo%@%d2",restaurant.restaurantId,i]]] andDescriptionStr:descriptionString2];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"photo%@%d2",restaurant.restaurantId,i]];
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"PhotoDescription%@%d2",restaurant.restaurantId,i]];
                }
            }
             photoCount = [[NSUserDefaults standardUserDefaults] integerForKey:[NSString stringWithFormat:@"uploadPhotoCount%@",restaurant.restaurantId]];

            if (photoCount>0&&self.isUploadPhoto) {
                for (int i =0; i<photoCount; i++) {
                    NSString *descriptionString = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"PhotoDescription%@%d1",restaurant.restaurantId,i]];

                    [self getImageWith:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"photo%@%d1",restaurant.restaurantId,i]]] andDescriptionStr:descriptionString];
                    
                }

            }

        }
    }
}
-(void)getImageWith:(NSString*)string andDescriptionStr:(NSString*)descriptionStr{
    NSURL *imagePath = [NSURL URLWithString:string];
    if ([[[imagePath scheme] lowercaseString] isEqualToString:@"assets-library"]) {
        // Load from asset library async
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            @autoreleasepool {
                @try {
                    ALAssetsLibrary *assetslibrary = [[ALAssetsLibrary alloc] init];
                    [assetslibrary assetForURL:imagePath
                                   resultBlock:^(ALAsset *asset){
                                       NSLog(@"获取图片成功");
                                       ALAssetRepresentation *rep = [asset defaultRepresentation];
                                       CGImageRef iref = [rep fullScreenImage];
                                       if (iref) {
                                           //进行UI修改
                                           dispatch_sync(dispatch_get_main_queue(), ^{
                                               if (self.isUploadPhoto&&!self.isFromDraft) {
                                                   [SVProgressHUD showImage:[UIImage imageNamed:@""] status:@"You have an unpublished draft"];
                                                   [SVProgressHUD dismissWithDelay:1.5];

                                               }
                                               IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
                                               photo.image =[[UIImage alloc] initWithCGImage:iref];
                                               photo.photoUrl = string;
                                               if (descriptionStr.length>0) {
                                                   photo.descriptionStr = descriptionStr;
                                               }
                                               [_photosArrM addObject:photo];
                                               [_tableView reloadData];
//                                               if (_isUploadPhoto) {
//                                                   [_tableView reloadData];
//                                               }else{
//                                                   reviewPhotoView.photoArray = _photosArrM;
//                                               }
                                               

                                           });
                                           
                                       }
                                       
                                   }
                                  failureBlock:^(NSError *error) {
                                      
                                      NSLog(@"从图库获取图片失败: %@",error);
                                      
                                  }];
                } @catch (NSException *e) {
                    NSLog(@"从图库获取图片异常: %@", e);
                }
            }
        });
        
    }

}
- (void)latestAsset:(void (^)(ALAsset * _Nullable, NSError *_Nullable))block {
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if (group) {
            [group setAssetsFilter:[ALAssetsFilter allPhotos]];
            [group enumerateAssetsWithOptions:NSEnumerationReverse/*遍历方式*/ usingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
                if (result) {
                    if (block) {
                        block(result,nil);
                    }
                    *stop = YES;
                }
            }];
            *stop = YES;
        }
    } failureBlock:^(NSError *error) {
        if (error) {
            if (block) {
                block(nil,error);
            }
        }
    }];
}
-(void)deleteDraft{
    NSMutableArray *drftArry = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"reviewDrafts"]];
    for (int i =0; i<drftArry.count; i++) {
        NSMutableDictionary *dic = drftArry[i];
        if ([[dic objectForKey:@"restaurantId"] intValue]==[restaurant.restaurantId intValue]) {
            [drftArry removeObjectAtIndex:i];
            [[NSUserDefaults standardUserDefaults] setObject:drftArry forKey:@"reviewDrafts"];
        }

    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"rating%@",restaurant.restaurantId]];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"hasDraft%@",restaurant.restaurantId]];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"reviewTitle%@",restaurant.restaurantId]];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"reviewContent%@",restaurant.restaurantId]];
    [[NSUserDefaults standardUserDefaults] setValue:textView.text forKey:[NSString stringWithFormat:@"reviewContent%@",restaurant.restaurantId]];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"reviewCount%@",restaurant.restaurantId]];
    if (_photosArrM.count>0) {
        for (int i =0; i<_photosArrM.count; i++) {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"photo%@%d",restaurant.restaurantId,i]];
        }
    }
}
-(void)deleteUploadPhotoDraft{
    NSMutableArray *draftArry = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"uploadPhotoDraft"]];
    for (int i =0; i<draftArry.count; i++) {
        NSMutableDictionary *dic = draftArry[i];
        if ([[dic objectForKey:@"restaurantId"] intValue]==[restaurant.restaurantId intValue]) {
            [draftArry removeObjectAtIndex:i];
            [[NSUserDefaults standardUserDefaults] setObject:draftArry forKey:@"uploadPhotoDraft"];
        }
        
    }
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"uploadPhotoCount%@",restaurant.restaurantId]];


    if (_photosArrM.count>0) {
        for (int i =0; i<_photosArrM.count; i++) {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:[NSString stringWithFormat:@"photo%@%d1",restaurant.restaurantId,i]];
        }
    }
    
}

#pragma mark - private method
-(NSString*)getPageIdByAmplitudeType{
    NSString *pageId = @"";
    if ([@"Homepage CTA" isEqualToString:self.amplitudeType]) {
        pageId = @"Homepage";
    }
    return [NSString stringWithFormat:@"%@",pageId];
}
-(void)getReview{
    IMGUser *user = [IMGUser currentUser];
    if(user.userId==nil){
        isGuestMode = YES;
    }else{
        isGuestMode = NO;
        [[LoadingView sharedLoadingView] startLoading];
        if ([self.reviewId intValue]) {
            NSDictionary *params = @{@"reviewId":self.reviewId};
            [ReviewPublishHandler getReviewWithParams:params andBlock:^(NSDictionary *dict) {
                self.userReview = [[IMGUserReview alloc] init];
                [self.userReview setValuesForKeysWithDictionary:dict];
                
                for (NSDictionary *dishDic in [dict objectForKey:@"dishList"])
                {
                    IMGDish *dish = [[IMGDish alloc] init];
                    dish.dishId = [dishDic objectForKey:@"id"];
                    dish.imageUrl = [dishDic objectForKey:@"imageUrl"];
                    dish.title = [dishDic objectForKey:@"title"];
                    dish.descriptionStr = [dishDic objectForKey:@"description"];
                    dish.createTime = [dishDic objectForKey:@"createTime"];
                    if (!self.userReview.dishListArrM)
                    {
                        self.userReview.dishListArrM = [[NSMutableArray alloc] init];
                    }
                    [self.userReview.dishListArrM addObject:dish];
                }
                overallRating = [self.userReview.reviewScore floatValue];
                [startRating setRating:overallRating/2];
                int ratingCout = overallRating;
                
                [ratingView updateRating:ratingCout];
//                starLabel.text = [NSString stringWithFormat:@"%d★",ratingCout/2];
                //[self descriptionLabelTextWithRatingCout:ratingCout];
                textView.text = _userReview.reviewSummarize;
                textField.text = _userReview.reviewTitle;
                
                if (_userReview.reviewSummarize.length)
                {
                    labeltext.hidden = YES;
                    CGSize size = [textView sizeThatFits:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, MAXFLOAT)];
                    textView.frame=CGRectMake(LEFTLEFTSET-3, textField.endPointY+4, DeviceWidth-2*LEFTLEFTSET, size.height);
//                    reviewPhotoView.frame = CGRectMake(0, textView.endPointY, DeviceWidth, 55);
                    lastTextViewHeight = _tableView.contentSize.height;
                    vUpload.frame = CGRectMake(0, textView.endPointY, DeviceWidth, 81);
                    publishView.frame = CGRectMake(0, 0, DeviceWidth, vUpload.endPointY);
                    _tableView.tableHeaderView = publishView;
                }
                if (self.reviewContent.length==0&&self.reviewTitle.length==0) {
                    for (IMGDish *dish in _userReview.dishListArrM)
                    {
                        IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
                        photo.isOldPhoto = YES;
                        photo.dishId = dish.dishId;
                        photo.imageUrl = dish.imageUrl;
                        photo.descriptionStr = dish.descriptionStr;
                        [_photosArrM addObject:photo];
                    }
                    
                    if(_photosArrM.count){
                        hasUploadPhoto=YES;
                    }
                    [_tableView reloadData];
//                    if (_isUploadPhoto) {
//                        [_tableView reloadData];
//                    }else{
//                        reviewPhotoView.photoArray = _photosArrM;
//                    }
                    self.isUpdateReview = YES;
                    [self updatePostStatus];
                    
                }

            }];
        }else{
            NSMutableDictionary *params=[[NSMutableDictionary alloc] init];
            [params setObject:user.userId forKey:@"userId"];
            if ([restaurant.restaurantId intValue]) {
                [params setObject:restaurant.restaurantId forKey:@"restaurantId"];
            }
            [ReviewPublishHandler getLastReviewWithParams:params andBlock:^(NSDictionary *dict) {
                self.userReview = [[IMGUserReview alloc] init];
                [self.userReview setValuesForKeysWithDictionary:dict];
                
                for (NSDictionary *dishDic in [dict objectForKey:@"dishList"])
                {
                    IMGDish *dish = [[IMGDish alloc] init];
                    dish.dishId = [dishDic objectForKey:@"id"];
                    dish.imageUrl = [dishDic objectForKey:@"imageUrl"];
                    dish.title = [dishDic objectForKey:@"title"];
                    dish.descriptionStr = [dishDic objectForKey:@"description"];
                    dish.createTime = [dishDic objectForKey:@"createTime"];
                    if (!self.userReview.dishListArrM)
                    {
                        self.userReview.dishListArrM = [[NSMutableArray alloc] init];
                    }
                    [self.userReview.dishListArrM addObject:dish];
                }
                overallRating = [self.userReview.reviewScore floatValue];
                [startRating setRating:overallRating/2];
                int ratingCout = overallRating;
                [ratingView updateRating:ratingCout];
//                starLabel.text = [NSString stringWithFormat:@"%d★",ratingCout/2];
                //[self descriptionLabelTextWithRatingCout:ratingCout];
                
                textView.text = _userReview.reviewSummarize;
                textField.text = _userReview.reviewTitle;
                if (_userReview.reviewSummarize.length)
                {
                    labeltext.hidden = YES;
                    CGSize size = [textView sizeThatFits:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, MAXFLOAT)];
                    textView.frame=CGRectMake(LEFTLEFTSET-3, textField.endPointY+4, DeviceWidth-2*LEFTLEFTSET, size.height);
//                    reviewPhotoView.frame = CGRectMake(0, textView.endPointY, DeviceWidth, 55);
                    lastTextViewHeight = _tableView.contentSize.height;
                    vUpload.frame = CGRectMake(0, textView.endPointY, DeviceWidth, 81);
                    publishView.frame = CGRectMake(0, 0, DeviceWidth, vUpload.endPointY);
                    _tableView.tableHeaderView = publishView;
                }
                if (self.reviewContent.length==0&&self.reviewTitle.length==0) {
                    for (IMGDish *dish in _userReview.dishListArrM)
                    {
                        IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
                        photo.isOldPhoto = YES;
                        photo.dishId = dish.dishId;
                        photo.imageUrl = dish.imageUrl;
                        photo.descriptionStr = dish.descriptionStr;
                        [_photosArrM addObject:photo];
                    }
                    
                    if(_photosArrM.count){
                        hasUploadPhoto=YES;
                    }
                    [_tableView reloadData];
//                    if (_isUploadPhoto) {
//                        [_tableView reloadData];
//                    }else{
//                        reviewPhotoView.photoArray = _photosArrM;
//                    }
                    self.isUpdateReview = YES;
                    [self updatePostStatus];
                    
                }
                
                [[LoadingView sharedLoadingView] stopLoading];

            }];
        }
    }
    
}
-(void)updatePostStatus{
    if((!self.isUploadPhoto && (overallRating>0 ||  ![textView.text isBlankString] || ![textField.text isBlankString])) || hasUploadPhoto){
        btnPost.alpha = 1.0f;
//        btnPost.enabled = YES;
        [btnPost setTitleColor:[UIColor colorWithHexString:@"09BFD3"] forState:UIControlStateNormal];
//        headeView.postButton.userInteractionEnabled = YES;
    }else{
        btnPost.alpha = 0.5f;
//        btnPost.enabled = NO;
//        headeView.postButton.userInteractionEnabled = NO;
        [btnPost setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
        
    }
}
-(void)getRestaurantImage{
    if (restaurant.imageUrl.length==0&&[restaurant.restaurantId intValue]>0) {
        [DetailDataHandler getRestaurantFirstDataFromServiceNow:restaurant.restaurantId andBlock:^(IMGRestaurant *restaurant_, NSArray *eventList, IMGRestaurantOffer *restaurantOffer, NSArray *banerImageArr, IMGRDPRestaurant *rdpRest, NSNumber *showClaim,NSArray *couponArray) {
            restaurant.ratingScore = restaurant_.ratingScore;
            restaurant.imageUrl= restaurant_.imageUrl;
            restaurant.title = restaurant.title;
        }];
    }

}
-(void)setIsUploadPhoto:(BOOL)_isUpload{
    _isUploadPhoto=_isUpload;
    if(_isUpload){
        hasUploadPhoto=YES;
    }
}
@end

