//
//  AddPhotoViewController2.h
//  Qraved
//
//  Created by Laura on 15/3/6.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "IMGRestaurant.h"

@interface AddPhotoViewController2 : BaseViewController
@property(nonatomic,assign)BOOL isWriteReview;
@property (nonatomic,retain)NSString *reviewDescription;
@property (nonatomic,retain)UIImage *selectedImage;


-(id)initWithRestauarnt:(IMGRestaurant *)resto;
-(id)initWithRestauarnt:(IMGRestaurant *)resto andReviewId:(int)tmpReviewId;

@end
