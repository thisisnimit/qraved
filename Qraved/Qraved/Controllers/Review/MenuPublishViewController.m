//
//  MenuPublishViewController.m
//  Qraved
//
//  Copyright (c) 2016年 Imaginato. All rights reserved.
//

#import "MenuPublishViewController.h"
#import "UIColor+Helper.h"
#import "Label.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "UIViewController+Helper.h"
#import "UIAlertView+BlocksKit.h"
#import "ReviewHeaderView.h"
#import "IMGUser.h"
#import "IMGDish.h"
#import "AppDelegate.h"
#import "LoadingView.h"
#import "IMGReview.h"
#import "DBManager.h"
#import "AddPhotoViewController.h"
#import "AddPhotoTableViewCell.h"
#import "PostDataHandler.h"
#import "IMGDish.h"
#import "IMGUploadPhoto.h"
#import "ShareReviewViewController.h"
#import "UIDevice+Util.h"
#import "LoginViewController.h"
#import "UIDevice+Util.h"

@implementation MenuPublishViewController{
    UIAlertView *alert;
    IMGRestaurant *restaurant;
    
    UITableView *_tableView;
    
    ReviewHeaderView *headeView;
    UIButton *addPhotoBtn;
    UILabel *charNoticeLabel;
    UILabel *postNoticeLabel;
    
    BOOL hasUploadPhoto;
    
    NSMutableArray *imageUrlArrM;
    
    UIView *btnsView;
    UIView *buttonBtnsView;
    UILabel *buttonCharNoticeLabel;
}

-(id)initWithRestaurant:(IMGRestaurant *)resto{
    self=[super init];
    if(self){
        restaurant=resto;
    }
    return self;
}

- (void)viewDidLoad{
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(savePhoto:) name:@"savePhotoFromCameraNotification" object:nil];
    imageUrlArrM = [[NSMutableArray alloc] init];
    self.view.backgroundColor = [UIColor blackColor];
    self.navigationController.navigationBarHidden = YES;
    
    [self loadMainView];
    
}

-(void)addButtonBtnsView{
    buttonBtnsView = [[UIView alloc] init];
    buttonBtnsView.backgroundColor = [UIColor colorWithRed:241/255.0 green:241/255.0 blue:241/255.0 alpha:1];
    buttonBtnsView.frame = CGRectMake(0, _tableView.endPointY, DeviceWidth, DeviceHeight - _tableView.endPointY + 20);
    
    UIButton *buttonAddPhotoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonAddPhotoBtn setImage:[UIImage imageNamed:@"upload_review_photo"] forState:UIControlStateNormal];
    buttonAddPhotoBtn.frame = CGRectMake(22, buttonBtnsView.frame.size.height - 27, 22, 18);
    [buttonBtnsView addSubview:buttonAddPhotoBtn];
    [buttonAddPhotoBtn addTarget:self action:@selector(addPhotoClick) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel* addPhotoLabel=[[UILabel alloc]initWithFrame:CGRectMake(buttonAddPhotoBtn.endPointX+5, buttonAddPhotoBtn.startPointY, 150, 18)];
    addPhotoLabel.text=@"Upload Photo";
    addPhotoLabel.font=[UIFont systemFontOfSize:16];
    UIControl* addphoto=[[UIControl alloc]init];
    [addphoto addTarget:self action:@selector(addPhotoClick) forControlEvents:UIControlEventTouchUpInside];
    addphoto.frame=addPhotoLabel.frame;
    
    [buttonBtnsView addSubview:addphoto];
    [buttonBtnsView addSubview:addPhotoLabel];
    
    buttonCharNoticeLabel=[[UILabel alloc] initWithFrame:CGRectMake(50,buttonBtnsView.frame.size.height-27,DeviceWidth-50-LEFTLEFTSET,18)];
    buttonCharNoticeLabel.textColor=[UIColor grayColor];
    buttonCharNoticeLabel.font=[UIFont systemFontOfSize:12];
    buttonCharNoticeLabel.textAlignment=NSTextAlignmentRight;
    [buttonBtnsView addSubview:buttonCharNoticeLabel];
    
    UIView *writeBackground = [[UIView alloc] initWithFrame:buttonBtnsView.frame];
    writeBackground.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:writeBackground];
    [self.view addSubview:buttonBtnsView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updatePostStatus];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardShowChange:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardHiddenChange:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardWillHideNotification];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[LoadingView sharedLoadingView]stopLoading];
}
-(void)keyboardShowChange:(NSNotification *)notification{
    buttonBtnsView.hidden = YES;
}

-(void)keyboardHiddenChange:(NSNotification *)notification{
    buttonBtnsView.hidden = NO;
}

-(void)loadMainView{
    if (!_photosArrM) {
        _photosArrM = [[NSMutableArray alloc] init];
    }
    
    if (self.selectedImage) {
        IMGUploadPhoto *uploadPhoto = [[IMGUploadPhoto alloc]init];
        uploadPhoto.image = self.selectedImage;
        [_photosArrM addObject:uploadPhoto];
    }
    
    if(_photosArrM.count){
        hasUploadPhoto=YES;
    }
    
    headeView = [[ReviewHeaderView alloc]initWithTitle:restaurant.title andIsNeedWriteReview:YES];
    [headeView.closeButton addTarget:self action:@selector(closeButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [headeView.postButton addTarget:self action:@selector(postPhoto) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:headeView];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, headeView.endPointY, DeviceWidth, DeviceHeight - 35 +20 -64)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    btnsView = [[UIView alloc] init];
    btnsView.backgroundColor = [UIColor colorWithRed:241/255.0 green:241/255.0 blue:241/255.0 alpha:1];
    btnsView.frame = CGRectMake(0, _tableView.endPointY, DeviceWidth, DeviceHeight - _tableView.endPointY + 20);
    
    addPhotoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [addPhotoBtn setImage:[UIImage imageNamed:@"upload_review_photo"] forState:UIControlStateNormal];
    addPhotoBtn.frame = CGRectMake(22, btnsView.frame.size.height - 27, 22, 18);
    [btnsView addSubview:addPhotoBtn];
    [addPhotoBtn addTarget:self action:@selector(addPhotoClick) forControlEvents:UIControlEventTouchUpInside];
    UILabel* addPhotoLabel=[[UILabel alloc]initWithFrame:CGRectMake(addPhotoBtn.endPointX+5, addPhotoBtn.startPointY, 150, 18)];
    addPhotoLabel.text=@"Upload Photo";
    addPhotoLabel.font=[UIFont systemFontOfSize:16];
    UIControl* addphoto=[[UIControl alloc]init];
    [addphoto addTarget:self action:@selector(addPhotoClick) forControlEvents:UIControlEventTouchUpInside];
    addphoto.frame=addPhotoLabel.frame;
    
    [btnsView addSubview:addphoto];
    [btnsView addSubview:addPhotoLabel];
    
    charNoticeLabel=[[UILabel alloc] initWithFrame:CGRectMake(50,btnsView.frame.size.height-27,DeviceWidth-50-LEFTLEFTSET,18)];
    charNoticeLabel.textColor=[UIColor grayColor];
    charNoticeLabel.font=[UIFont systemFontOfSize:12];
    charNoticeLabel.textAlignment=NSTextAlignmentRight;
    [btnsView addSubview:charNoticeLabel];
    
    UIView *footView = [[UIView alloc] init];
    footView.frame = CGRectMake(0, 0, DeviceWidth, 260);
    _tableView.tableFooterView = footView;
    
    postNoticeLabel=[[UILabel alloc] init];
    postNoticeLabel.layer.zPosition=1000;
    postNoticeLabel.backgroundColor=[UIColor colorWithRed:241/255.0 green:241/255.0 blue:241/255.0 alpha:1];
    postNoticeLabel.font=[UIFont systemFontOfSize:12];
    postNoticeLabel.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:postNoticeLabel];
    
    [self addButtonBtnsView];
}

- (void)postPhoto
{
    imageUrlArrM = [[NSMutableArray alloc] init];
    headeView.postButton.userInteractionEnabled = NO;
    IMGUser *user = [IMGUser currentUser];
    if(user.userId==nil){
        LoginViewController *loginVC=[[LoginViewController alloc]init];
        UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
        [self.navigationController presentViewController:loginNavagationController animated:YES completion:^{}];
        return;
    }
    if(user.token==nil){
        LoginViewController *loginVC=[[LoginViewController alloc]init];
        [self.navigationController presentViewController:loginVC animated:YES completion:^{}];
        return;
    }
    if ( (_photosArrM !=nil) && (_photosArrM.count!=0) ){
        [self uploadDishePhotos:user currentIndex:0 maxIndex:_photosArrM.count-1];
    }
}

-(void)uploadDishePhotos:(IMGUser *)user currentIndex:(NSInteger)currentIndex maxIndex:(NSInteger)maxIndex{
    PostDataHandler *postDataHandler=[[PostDataHandler alloc]init];
    [[LoadingView sharedLoadingView]startLoading];
    IMGUploadPhoto *uploadPhoto = [_photosArrM objectAtIndex:currentIndex];
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:uploadPhoto.dishId forKey:@"Photo_ID"];
    [eventProperties setValue:user.userId forKey:@"UploaderUser_ID"];
    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:(uploadPhoto.isFromCamera?@"New photo taken from camera":@"Existing photo on device image gallery") forKey:@"Source"];
    [eventProperties setValue:[self getPageIdByAmplitudeType] forKey:@"Origin"];
    [[Amplitude instance] logEvent:@"UC - Upload Photo Initiate" withEventProperties:eventProperties];

    [postDataHandler postImage:uploadPhoto.image andTitle:@"image" andDescription:@"image" andSuccessBlock:^(id postArray){
        
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:uploadPhoto.dishId forKey:@"Photo_ID"];
        [eventProperties setValue:user.userId forKey:@"UploaderUser_ID"];
        [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
        [[Amplitude instance] logEvent:@"UC - Upload Photo Succeed" withEventProperties:eventProperties];
        [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Upload Photo Succeed" withValues:eventProperties];
        
        NSString *imageUrl=[postArray objectForKey:@"imgPath"];
        [imageUrlArrM addObject:imageUrl];
        if(currentIndex<maxIndex){
            [self uploadDishePhotos:user currentIndex:currentIndex+1 maxIndex:maxIndex];
        }else{
            [self publishReview];
        }
    }andFailedBlock:^(NSError *error){
        NSLog(@"upload photo failed, error:%@",error.description);
        headeView.postButton.userInteractionEnabled = YES;
        [[LoadingView sharedLoadingView] stopLoading];
        
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:uploadPhoto.dishId forKey:@"Photo_ID"];
        [eventProperties setValue:user.userId forKey:@"UploaderUser_ID"];
        [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:error.localizedDescription forKey:@"Reason"];
        [[Amplitude instance] logEvent:@"UC - Upload Photo Failed" withEventProperties:eventProperties];
    }];
}

-(void)publishReview{
    PostDataHandler *postDataHandler=[[PostDataHandler alloc]init];
    IMGUser *user = [IMGUser currentUser];
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:user.token forKey:@"t"];
    [parameters setValue:restaurant.restaurantId forKey:@"restaurantId"];
    [parameters setValue:user.userId forKey:@"userId"];
    [parameters setValue:[[NSString alloc]initWithData:[IMGEntity toJSONData:[[NSMutableArray alloc]initWithArray:imageUrlArrM]] encoding:NSUTF8StringEncoding] forKey:@"imageUrl"];
    [postDataHandler postMenuImage:parameters andSuccessBlock:^{
        [[LoadingView sharedLoadingView] stopLoading];
        if ([self.menuPublishViewControllerDelegate respondsToSelector:@selector(refreshView)]) {
            [self.menuPublishViewControllerDelegate refreshView];
        }
        [self dismissViewControllerAnimated:YES completion:^{
            alert = [[UIAlertView alloc] initWithTitle:nil message:L(@"Thank you for your contribution Qravers") delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
            [alert show];
            [self performSelector:@selector(alertDissmiss) withObject:nil afterDelay:2];
        }];
        
    } andFailedBlock:^(NSString *exceptionMsg){
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            
            headeView.postButton.userInteractionEnabled = YES;
            
            LoginViewController *loginVC=[[LoginViewController alloc]init];
            UINavigationController *loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
            loginVC.autoPostDelegate = self;
            [self.navigationController presentViewController:loginNavagationController animated:YES completion:nil];
        }else{
            [[LoadingView sharedLoadingView] stopLoading];
            UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:NSLocalizedStringFromTable(L(@"Please check your internet connection!"),nil,nil) delegate:nil cancelButtonTitle:L(@"OK") otherButtonTitles:nil];
            successAlert.alpha=0.6;
            [successAlert show];
        }
    }];
}

-(void)alertDissmiss{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)addPhotoClick{
    
    if (_photosArrM.count>=9){
        return;
    }
    
    ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] initWithRestaurantTitle:restaurant.title andIsFromUploadPhoto:NO andIsUploadMenuPhoto:NO andCurrentRestaurantId:restaurant.restaurantId];
    picker.publishViewController = self;
    if (!_photosArrM || _photosArrM.count == 0)
    {
        picker.maximumNumberOfSelection = 9;
    }
    else
        picker.maximumNumberOfSelection = 9 - _photosArrM.count;
    
    picker.assetsFilter = [ALAssetsFilter allPhotos];
    picker.showEmptyGroups=NO;
    picker.delegate = self;
    picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
            NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
            return duration >= 5;
        } else {
            return YES;
        }
    }];
    [self presentViewController:picker animated:YES completion:NULL];
}
-(void)closeButtonClick{
    
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:[IMGUser currentUser].userId forKey:@"UploaderUser_ID"];
    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
    
    [[Amplitude instance] logEvent:@"UC - Upload Photo Cancel" withEventProperties:eventProperties];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"savePhotoFromCameraNotification" object:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _photosArrM.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier =@"cell";
    AddPhotoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[AddPhotoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier andIsUploadMenuPhoto:YES];
        cell.selectionStyle=UITableViewCellSelectionStyleGray;
        [cell setTextFieldInputAccessoryView:btnsView];
    }
    IMGUploadPhoto *photo = [_photosArrM objectAtIndex:indexPath.row];
    photo.tag = indexPath.row;
    cell.textField.delegate = self;
    cell.delegate = self;
    cell.textField.tag = indexPath.row;
    [cell setPhoto:photo];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

- (void)delPhotoClicked:(NSInteger)tag
{
    
    for (IMGUploadPhoto *photo in _photosArrM)
    {
        if (photo.tag == tag)
        {
            [_photosArrM removeObject:photo];
            [_tableView reloadData];
            break;
        }
    }
    if(_photosArrM.count==0){
        hasUploadPhoto=NO;
    }
    [self updatePostStatus];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IMGUploadPhoto *photo = [_photosArrM objectAtIndex:indexPath.row];
    CGSize size = CGSizeFromString(NSStringFromCGSize(photo.image.size));
    CGFloat height = size.height/(size.width/DeviceWidth);
    return height + 50;
}

- (void)savePhoto:(NSNotification *)notification
{
    hasUploadPhoto=YES;
    [self updatePostStatus];
    IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
    photo.image = [notification.userInfo objectForKey:@"finishImage"];
    photo.isFromCamera = YES;
    [_photosArrM addObject:photo];
    [_tableView reloadData];
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - ZYQAssetPickerController Delegate
-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    hasUploadPhoto=YES;
    [self updatePostStatus];
    for (int i=0; i<assets.count; i++) {
        ALAsset *asset=assets[i];
        UIImage *tempImg=[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage];
        IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
        photo.image = tempImg;
        [_photosArrM addObject:photo];
    }
    [_tableView reloadData];
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return YES;
}

-(void)updatePostStatus{
    if(hasUploadPhoto){
        headeView.postButton.alpha = 1.0f;
        headeView.postButton.userInteractionEnabled = YES;
    }else{
        headeView.postButton.alpha = 0.5f;
        headeView.postButton.userInteractionEnabled = NO;
    }
}
 
-(NSString*)getPageIdByAmplitudeType{
    NSString *pageId = @"";
    if ([@"Hot Key" isEqualToString:self.amplitudeType]) {
        pageId = @",Homepage";
    }
    if ([@"Review Card Detail" isEqualToString:self.amplitudeType]) {
        pageId = @",Card detail page";
    }
    return [NSString stringWithFormat:@"%@%@",self.amplitudeType,pageId];
}
@end
