//
//  MarkingViewController.h
//  Qraved
//
//  Created by Laura on 14-8-15.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "DLStarRatingControl.h"
#import "IMGRestaurant.h"
@interface ReviewScoreViewController : BaseViewController<DLStarRatingDelegate>

-(id)initWithRestaurant:(IMGRestaurant *)restaurant;

@end
