//
//  NotificationSettingViewController.m
//  Qraved
//
//  Created by Laura on 14-8-15.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "NotificationSettingViewController.h"

#import "UIColor+Helper.h"
#import "Label.h"
#import "UIView+Helper.h"
#import "UIDevice+Util.h"
#import "UIViewController+Helper.h"
#import "UIImageView+Helper.h"

#import "NotificationTableViewCell.h"
#import "IMGUser.h"
#import "IMGNotificationSetting.h"

NS_ENUM(NSInteger, NotificationState){
    NotificationOpen =0,
    NotificationClose =1
};

static NSString * const BookingConfirmation;
static NSString * const BookingReminder;
static NSString * const PostDine;

static NSString * const SpecialOffers;
static NSString * const Events;
static NSString * const ReviewsPhotos;

@interface NotificationSettingViewController ()
{
    NSArray *sectionsArr;
    NSArray *personalArr;
    NSArray *updatesArr;
    UITableView *tableView ;
    NSMutableDictionary *boolPhoneDictionary ;
    NSMutableDictionary *boolEmailDictionary;
}
@end

@implementation NotificationSettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = L(@"Notifications");
    self.navigationController.navigationBarHidden = NO;
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor defaultColor];
    [self setBackBarButtonOffset30];
    [self setSaveBarButton];
    [self loadMainView];
    
//    boolPhoneDictionary = [NSMutableDictionary dictionaryWithObjects:@[@"Y",@"N",@"N",@"N",@"N",@"N"] forKeys:@[@"bookConfirmation",@"bookReminder",@"postDine",@"specialOffers",@"events",@"reviews"]];
//    
//    boolEmailDictionary = [NSMutableDictionary dictionaryWithObjects:@[@"Y",@"Y",@"Y",@"Y",@"Y",@"Y"] forKeys:@[@"bookConfirmation",@"bookReminder",@"postDine",@"specialOffers",@"events",@"reviews"]];
    boolPhoneDictionary = [[NSMutableDictionary alloc]init];
    boolEmailDictionary = [[NSMutableDictionary alloc]init];
    [self initData];
    
}
-(void)initData{
/*
 
 {
 setting =     {
 class = "com.imaginato.user.UserNotificationSetting";
 
 emailCancelReservationFlag = Y;
 emailCommentPhotoFlag = Y;
 emailFollowFlag = N;
 emailMakeReservationFlag = Y;
 emailQravePhotoFlag = Y;
 id = 42330;
 lastUpdated = 1417750177229;
 receiveNotifyFlag = Y;
 receiveWeeklyFlag = Y;
 shareQuaveFlag = Y;
 shareReserveFlag = Y;
 };
 status = succeed;
 }
 
 */
    NSDictionary *parameters=@{@"userID":[IMGUser currentUser].userId,@"t":[IMGUser currentUser].token};
    [[IMGNetWork sharedManager]GET:@"user/notification/get" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        if ([[responseObject objectForKey:@"status"] isEqualToString:@"succeed"]) {
            NSDictionary *dict = [responseObject objectForKey:@"setting"];
            IMGNotificationSetting*notificationSetting = [[IMGNotificationSetting alloc]init];
            [notificationSetting setValuesForKeysWithDictionary:dict];
            
            [boolEmailDictionary setObject:notificationSetting.emailCommentPhotoFlag forKey:@"bookConfirmation"];
            [boolEmailDictionary setObject:notificationSetting.emailCancelReservationFlag forKey:@"bookReminder"];
            [boolEmailDictionary setObject:notificationSetting.emailCommentPhotoFlag forKey:@"postDine"];
            [boolEmailDictionary setObject:notificationSetting.emailFollowFlag forKey:@"specialOffers"];
            [boolEmailDictionary setObject:notificationSetting.emailCommentPhotoFlag forKey:@"events"];
            [boolEmailDictionary setObject:notificationSetting.emailCommentPhotoFlag forKey:@"reviews"];
            
            
            [boolPhoneDictionary setObject:notificationSetting.emailCommentPhotoFlag forKey:@"bookConfirmation"];
            [boolPhoneDictionary setObject:notificationSetting.emailCancelReservationFlag forKey:@"bookReminder"];
            [boolPhoneDictionary setObject:notificationSetting.emailCommentPhotoFlag forKey:@"postDine"];
            [boolPhoneDictionary setObject:notificationSetting.emailFollowFlag forKey:@"specialOffers"];
            [boolPhoneDictionary setObject:notificationSetting.emailFollowFlag forKey:@"events"];
            [boolPhoneDictionary setObject:notificationSetting.emailCommentPhotoFlag forKey:@"reviews"];
            [tableView reloadData];
        }
        NSLog(@"%@",responseObject);
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        
    }];
    
}
-(void)setSaveBarButton
{
    UIBarButtonItem *shareBtn=[[UIBarButtonItem alloc] initWithTitle:L(@"Save") style:UIBarButtonItemStylePlain target:self action:@selector(SaveButtonClick:)];
    shareBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=shareBtn;
}
-(void)SaveButtonClick
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)loadMainView
{
    

    sectionsArr = @[L(@"Personal"),L(@"Updates")];
    personalArr = @[L(@"Booking confirmation"),L(@"Booking reminder"),L(@"Post-dine")];
    updatesArr =  @[L(@"Special Offers"),L(@"Events"),L(@"Reviews / photos")];
    
    
    tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 47*6+37*2) style:UITableViewStylePlain];
    tableView.scrollEnabled = NO;
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tableView];
    
    NSString *promtStr = L(@"The more restaurants you favorited, the more we can give you relevant notification updates of new offers or events happening.");
    CGSize expectSize = [promtStr sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12] constrainedToSize:CGSizeMake(DeviceWidth-40, 100)];
    Label *promtLabel = [[Label alloc]initWithFrame:CGRectMake(20, [UIDevice isIphone5]?(tableView.endPointY+20):(tableView.endPointY), DeviceWidth-40, expectSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12] andTextColor:[UIColor color999999] andTextLines:0];
    promtLabel.text = promtStr;
    [self.view addSubview:promtLabel];
    
    //标题下阴影
    UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:0 andHeight:4];
    [self.view addSubview:shadowImageView];
    
}
-(UITableViewCell*)tableView:(UITableView *)TableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CellIdentifier";
    NotificationTableViewCell *cell = [TableView dequeueReusableCellWithIdentifier:cellIdentifier];

    if (cell == nil) {
        cell = [[NotificationTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        
        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 46, DeviceWidth, 1)];
        lineImage.backgroundColor = [UIColor colorEDEDED];
        [cell.contentView addSubview:lineImage];
    }
    cell.textLabel.backgroundColor=[UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.textColor = [UIColor color222222];
    cell.textLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16];
    if (indexPath.row==0) {
        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 1)];
        lineImage.backgroundColor = [UIColor colorEDEDED];
        [cell.contentView addSubview:lineImage];
    }
    
    
    cell.phoneImageView.tag = indexPath.row+10*indexPath.section;
    cell.emailImageView.tag = indexPath.row+10*indexPath.section;
    
    UITapGestureRecognizer *tapPhoneGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(phoneNotification:)];
    UITapGestureRecognizer *tapEmailGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(emailNotification:)];
    [cell.phoneImageView addGestureRecognizer:tapPhoneGesture];
    [cell.emailImageView addGestureRecognizer:tapEmailGesture];
    
    if (indexPath.section == 0) {
        cell.textLabel.text = [personalArr objectAtIndex:indexPath.row];
        if (indexPath.row==0) {
            if([[boolPhoneDictionary objectForKey:@"bookConfirmation"]isEqualToString:@"N"])
            {
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhoneGray"];
            }
            else
            {
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhone"];
            }
            if([[boolEmailDictionary objectForKey:@"bookConfirmation"]isEqualToString:@"N"])
            {
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmailGray"];
            }
            else
            {
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmail"];
            }
            
        }
        else if(indexPath.row == 1)
        {
            if([[boolPhoneDictionary objectForKey:@"bookReminder"]isEqualToString:@"N"])
            {
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhoneGray"];
            }
            else
            {
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhone"];
            }
            if([[boolEmailDictionary objectForKey:@"bookReminder"]isEqualToString:@"N"])
            {
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmailGray"];
            }
            else
            {
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmail"];
            }
        }
        else{
            if([[boolPhoneDictionary objectForKey:@"postDine"]isEqualToString:@"N"])
            {
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhoneGray"];
            }
            else
            {
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhone"];
            }
            if([[boolEmailDictionary objectForKey:@"postDine"]isEqualToString:@"N"])
            {
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmailGray"];
            }
            else
            {
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmail"];
            }
        }
    }
    if (indexPath.section == 1) {
        cell.textLabel.text = [updatesArr objectAtIndex:indexPath.row];
        
        if (indexPath.row==0) {
            if([[boolPhoneDictionary objectForKey:@"specialOffers"]isEqualToString:@"N"])
            {
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhoneGray"];
            }
            else
            {
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhone"];
            }
            if([[boolEmailDictionary objectForKey:@"specialOffers"]isEqualToString:@"N"])
            {
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmailGray"];
            }
            else
            {
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmail"];
            }
            
        }
        else if(indexPath.row == 1)
        {
            if([[boolPhoneDictionary objectForKey:@"events"]isEqualToString:@"N"])
            {
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhoneGray"];
            }
            else
            {
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhone"];
            }
            if([[boolEmailDictionary objectForKey:@"events"]isEqualToString:@"N"])
            {
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmailGray"];
            }
            else
            {
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmail"];
            }
        }
        else{
            if([[boolPhoneDictionary objectForKey:@"reviews"]isEqualToString:@"N"])
            {
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhoneGray"];
            }
            else
            {
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhone"];
            }
            if([[boolEmailDictionary objectForKey:@"reviews"]isEqualToString:@"N"])
            {
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmailGray"];
            }
            else
            {
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmail"];
            }
        }
    }
    
    return cell;
}
-(void)phoneNotification:(UITapGestureRecognizer*)gesture
{
    NSIndexPath *indexPath;
    if (gesture.view.tag<10) {
        indexPath = [NSIndexPath indexPathForRow:gesture.view.tag inSection:0];
    }
    else
    {
        indexPath = [NSIndexPath indexPathForRow:gesture.view.tag-10 inSection:1];
    }
    NotificationTableViewCell *cell = (NotificationTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    switch (gesture.view.tag) {
        case 0:
        {
            if([[boolPhoneDictionary objectForKey:@"bookConfirmation"]isEqualToString:@"Y"])
            {
                [boolPhoneDictionary setObject:@"N" forKey:@"bookConfirmation"];
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhoneGray"];
            }
            else
            {
                [boolPhoneDictionary setObject:@"Y" forKey:@"bookConfirmation"];
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhone"];
            }

        }
            break;
        case 1:
        {
            if([[boolPhoneDictionary objectForKey:@"bookReminder"]isEqualToString:@"Y"])
            {
                [boolPhoneDictionary setObject:@"N" forKey:@"bookReminder"];
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhoneGray"];
            }
            else
            {
                [boolPhoneDictionary setObject:@"Y" forKey:@"bookReminder"];
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhone"];
            }
        }
            break;
        case 2:
        {
            
            if([[boolPhoneDictionary objectForKey:@"postDine"]isEqualToString:@"Y"])
            {
                [boolPhoneDictionary setObject:@"N" forKey:@"postDine"];
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhoneGray"];
            }
            else
            {
                [boolPhoneDictionary setObject:@"Y" forKey:@"postDine"];
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhone"];
            }
        }
            break;
            
            
        case 10:
        {
            
            if([[boolPhoneDictionary objectForKey:@"specialOffers"]isEqualToString:@"Y"])
            {
                [boolPhoneDictionary setObject:@"N" forKey:@"specialOffers"];
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhoneGray"];
            }
            else
            {
                [boolPhoneDictionary setObject:@"Y" forKey:@"specialOffers"];
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhone"];
            }
        }
            break;
        case 11:
        {
            
            if([[boolPhoneDictionary objectForKey:@"events"]isEqualToString:@"Y"])
            {
                [boolPhoneDictionary setObject:@"N" forKey:@"events"];
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhoneGray"];
            }
            else
            {
                [boolPhoneDictionary setObject:@"Y" forKey:@"events"];
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhone"];
            }
        }
            break;
        case 12:
        {
            
            if([[boolPhoneDictionary objectForKey:@"reviews"]isEqualToString:@"Y"])
            {
                [boolPhoneDictionary setObject:@"N" forKey:@"reviews"];
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhoneGray"];
            }
            else
            {
                [boolPhoneDictionary setObject:@"Y" forKey:@"reviews"];
                cell.phoneImageView.image = [UIImage imageNamed:@"SettingPhone"];
            }
        }
            break;
            
        default:
            break;
    }
    
}
-(void)emailNotification:(UITapGestureRecognizer*)gesture
{
    NSIndexPath *indexPath;
    if (gesture.view.tag<10) {
        indexPath = [NSIndexPath indexPathForRow:gesture.view.tag inSection:0];
    }
    else
    {
        indexPath = [NSIndexPath indexPathForRow:gesture.view.tag-10 inSection:1];
    }
    NotificationTableViewCell *cell = (NotificationTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    switch (gesture.view.tag) {
        case 0:
        {
            if([[boolEmailDictionary objectForKey:@"bookConfirmation"]isEqualToString:@"Y"])
            {
                [boolEmailDictionary setObject:@"N" forKey:@"bookConfirmation"];
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmailGray"];
            }
            else
            {
                [boolEmailDictionary setObject:@"Y" forKey:@"bookConfirmation"];
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmail"];
            }
            
        }
            break;
        case 1:
        {
            if([[boolEmailDictionary objectForKey:@"bookReminder"]isEqualToString:@"Y"])
            {
                [boolEmailDictionary setObject:@"N" forKey:@"bookReminder"];
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmailGray"];
            }
            else
            {
                [boolEmailDictionary setObject:@"Y" forKey:@"bookReminder"];
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmail"];
            }
        }
            break;
        case 2:
        {
            
            if([[boolEmailDictionary objectForKey:@"postDine"]isEqualToString:@"Y"])
            {
                [boolEmailDictionary setObject:@"N" forKey:@"postDine"];
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmailGray"];
            }
            else
            {
                [boolEmailDictionary setObject:@"Y" forKey:@"postDine"];
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmail"];
            }
        }
            break;
            
            
        case 10:
        {
            
            if([[boolEmailDictionary objectForKey:@"specialOffers"]isEqualToString:@"Y"])
            {
                [boolEmailDictionary setObject:@"N" forKey:@"specialOffers"];
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmailGray"];
            }
            else
            {
                [boolEmailDictionary setObject:@"Y" forKey:@"specialOffers"];
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmail"];
            }
        }
            break;
        case 11:
        {
            
            if([[boolEmailDictionary objectForKey:@"events"]isEqualToString:@"Y"])
            {
                [boolEmailDictionary setObject:@"N" forKey:@"events"];
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmailGray"];
            }
            else
            {
                [boolEmailDictionary setObject:@"Y" forKey:@"events"];
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmail"];
            }
        }
            break;
        case 12:
        {
            
            if([[boolEmailDictionary objectForKey:@"reviews"]isEqualToString:@"Y"])
            {
                [boolEmailDictionary setObject:@"N" forKey:@"reviews"];
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmailGray"];
            }
            else
            {
                [boolEmailDictionary setObject:@"Y" forKey:@"reviews"];
                cell.emailImageView.image = [UIImage imageNamed:@"SettingEmail"];
            }
        }
            break;
            
        default:
            break;
    }}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return sectionsArr.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return personalArr.count;
    }
    if (section==1) {
        return updatesArr.count;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 37)];
    headerView.backgroundColor = [UIColor colorFBFBFB];
    Label *headerLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 0, DeviceWidth-2*LEFTLEFTSET, 36) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:16] andTextColor:[UIColor color222222] andTextLines:1];
    headerLabel.text = [sectionsArr objectAtIndex:section];
    
    
    [headerView addSubview:headerLabel];
    
    
    
    return headerView;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 47.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 37.0f;
}
 

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
