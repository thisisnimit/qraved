//
//  AboutUsViewController.m
//  Qraved
//
//  Created by Laura on 14-8-15.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "AboutUsViewController.h"

#import "UIColor+Helper.h"
#import "Label.h"
#import "UIView+Helper.h"
#import "UIViewController+Helper.h"
#import "UIImageView+Helper.h"

@interface AboutUsViewController ()

@end

@implementation AboutUsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = L(@"About Us");
    self.navigationController.navigationBarHidden = NO;
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor defaultColor];
    [self setBackBarButtonOffset30];
    [self loadMainView];
}
-(void)loadMainView
{
    
    
    UIImageView *qravedLogoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET, LEFTLEFTSET*2, 115, 28)];
    qravedLogoImageView.image = [UIImage imageNamed:@"SettingQraved"];
    
    [self.view addSubview:qravedLogoImageView];
    
    NSString *aboutStr = L(@"Qraved is the No #1 Online Dining Solution in Indonesia!\n\nWant to find the most delicious restaurants? The latest food trends? Or the best dining offers? Qraved is the app for you!\nQraved is Indonesia's fastest growing & leading food discovery service solving the problem of \"What & Where to eat\"! To do this, Qraved provides a full stack solution bringing content, community and commerce together through web and mobile apps, connecting more than 1.8 million monthly active users to more than 35,000 offline food & beverage outlets across Jabodetabek, Bandung and Bali.");
    CGSize expectSize = [aboutStr sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] constrainedToSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
    Label *aboutLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, qravedLogoImageView.endPointY+LEFTLEFTSET, DeviceWidth-2*LEFTLEFTSET, expectSize.height*1.4) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor color666666] andTextLines:0];
    aboutLabel.text = aboutStr;
    [self.view addSubview:aboutLabel];
    
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineSpacing = 6;
    
    NSDictionary *attributes = @{ NSFontAttributeName:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13], NSParagraphStyleAttributeName:paragraphStyle};
    aboutLabel.attributedText = [[NSAttributedString alloc]initWithString:aboutLabel.text attributes:attributes];

    
    //标题下阴影
    UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:0 andHeight:4];
    [self.view addSubview:shadowImageView];

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
