//
//  SavedViewController.m
//  Qraved
//
//  Created by apple on 2017/5/11.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "SavedViewController.h"
#import "SearchScrolView.h"
#import "IMGCuisine.h"
#import "HMSegmentedControl.h"
#import "SavedTableViewCell.h"
#import "IMGRestaurant.h"
#import "UIScrollView+Helper.h"
#import "SavedMyListTableViewCell.h"
#import "ListHandler.h"
#import "IMGMyList.h"
#import "SavedDataHandler.h"
#import "DetailViewController.h"
#import "AddToListController.h"
#import "MyListRestaurantsViewController.h"
#import "IMGRefeshFooter.h"
#import "MyListEmptyController.h"
#import "SavedAddListView.h"
#import "EmptyListView.h"
#import "MyListAddRestaurantController.h"
#import "MapViewController.h"
#import "MapButton.h"
@interface SavedViewController ()<SearchScrolViewDelegate,UITableViewDelegate,UITableViewDataSource,SavedCellDelegate,SavedMyListTableViewCellDelgate,MyListRestaurantsViewControllerDelegate,UIAlertViewDelegate,MyListDelegate>

@end

@implementation SavedViewController{
    UIScrollView *bgScrollView;
    UIScrollView *contentScrollView;
    SearchScrolView *previouslyView;
    HMSegmentedControl *tab;
    UITableView *wantToGoTab;
    NSMutableArray *savedArr;
    NSInteger selectedIndex;
    NSMutableArray *myListArr;
    NSMutableArray *allListArr;
    NSNumber *listId_;
    NSInteger pressRestaurantId;
    int max;
    int offset;
    int myListOffset;
    UIAlertController *alertController;
    BOOL finishLoad;
    BOOL finishLoadList;
    BOOL unGotoListPage;
    MapButton *mapBtn;
    MapViewController *mapViewController;
    MKMapView *mapView;
    BOOL isFreshWantToGo;
    BOOL isFreshList;
    UIButton *navigateButton;
    
    IMGShimmerView *shimmerView;
}

#pragma mark -life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    savedArr = [NSMutableArray array];
    id savedJson = [CommonMethod getJsonFromFile:SAVED_CACHE];
    if (savedJson) {
        [savedArr addObjectsFromArray:[CommonMethod getSavedRestaurant:savedJson]];
    }
    myListArr = [NSMutableArray array];
    //restaurantsArr = [NSMutableArray array];
    allListArr = [NSMutableArray array];
    selectedIndex = 0;
    max = 10;
    offset = 0;
    myListOffset = 0;
    [self loadMainView];
    [self requestData];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(clickTabMenuRefreshPage) name:NOTIFICATION_REFRESH_TAB_SAVED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshList) name:@"RefreshList" object:nil];
    
    wantToGoTab.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(downRefreshData)];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[self rdv_tabBarController] setTabBarHidden:NO animated:NO];
    self.navigationController.navigationBar.hidden = NO;
    self.title = @"Saved";
    UIFont *font = [UIFont boldSystemFontOfSize:16];
    NSDictionary *dic = @{NSFontAttributeName:font, NSForegroundColorAttributeName:[UIColor color333333]};
    self.navigationController.navigationBar.titleTextAttributes =dic;
    
    [self.navigationController.navigationBar setTintColor:[UIColor barButtonTitleColor]];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"New List" style:UIBarButtonItemStylePlain target:self action:@selector(createNewlist)];
    [[self rdv_tabBarController] setTabBarHidden:NO animated:NO];
    
}
- (void)downRefreshData{
    myListOffset = 0;
    offset = 0;
    [self getListsData];
    [self getWantTogoList];

}
- (void)refreshList{
    myListOffset = 0;
    [self getListsData];
}
- (void)createNewlist{
     alertController = [UIAlertController alertControllerWithTitle:@"Create New List" message:@"Enter your list name" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder  =@"List Name";
        
    }];
    
    UIAlertAction *submitAction = [UIAlertAction actionWithTitle:@"Submit" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSString *str = alertController.textFields.firstObject.text;
        if (str.length>0 && str.length <=100) {
            [[LoadingView sharedLoadingView] startLoading];
            [ListHandler addListWithUser:[IMGUser currentUser] andListName:str andCityId:@0 andBlock:^(NSDictionary *dataDic) {
                
                [IMGAmplitudeUtil trackSavedWithName:@"BM - Create List" andRestaurantId:nil andRestaurantTitle:nil andOrigin:nil andListId:[dataDic objectForKey:@"id"] andType:nil andLocation:nil];
                
                if (unGotoListPage) {
                    NSArray *listArr= @[[dataDic objectForKey:@"id"]];
                    NSArray *restaurantArr = @[[NSNumber numberWithInteger:pressRestaurantId]];
                    
                    [IMGAmplitudeUtil trackSavedWithName:@"BM - Save Restaurant Initiate" andRestaurantId:[NSNumber numberWithInteger:pressRestaurantId] andRestaurantTitle:nil andOrigin:nil andListId:nil andType:@"Custom List" andLocation:@"Want to go List"];
                    
                    [self addRestaurantToList:listArr andRestaurantId:restaurantArr];
                    [self createSelectViewWithTitle:@"Saved to your list" andButtonTitle:@"Change" andListId:[dataDic objectForKey:@"id"]];
                    
                }
                else{
                    IMGMyList *list = [[IMGMyList alloc] init];
                    list.listId = [dataDic objectForKey:@"id"];
                    list.listName = str;
                    list.restaurantListItemCount = @0;
                    
                    [self gotoListPage:list];
                }
                
                myListOffset = 0;
                [self getListsData];
            
                unGotoListPage = NO;
                [[LoadingView sharedLoadingView] stopLoading];
            } failure:^(NSString *exceptionMsg) {
                unGotoListPage = NO;
            }];
            return ;
        }
        NSString *message;
        if (str.length==0) {
            message = @"List name cannot be empty";
        }else if (str.length > 100){
            message = L(@"Maximum 100 characters for list name.");
        }
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:L(@"OK") otherButtonTitles:nil, nil];
        [alertView show];
        


    }];
    //[submitAction setValue:[UIColor redColor] forKey:@"_titleTextColor"];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {


    }];
    [alertController addAction:cancelAction];
    [alertController addAction:submitAction];
    
    
    [self presentViewController:alertController animated:YES completion:nil];

}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];

    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
}
#pragma mark - map view
- (void)createMap{
    mapViewController=[[MapViewController alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-44)];
    mapViewController.fromSearchPage=YES;
    mapViewController.fromSaved = YES;
    mapViewController.fromWantToGo = YES;
    mapViewController.restaurantsViewController=self;
    __weak typeof(self) weakSelf = self;
    mapViewController.savedClick = ^(IMGRestaurant *pressRestaurant, SavedMapListView *savedMapView){
        [weakSelf removeFromList:pressRestaurant.restaurantId];
    };
    
    mapView = mapViewController.mapView;
    
    mapView.frame = CGRectMake(0,0, DeviceWidth, self.view.frame.size.height);
    mapView.hidden = YES;
    [self.view addSubview:mapViewController.mapView];
    
    
    navigateButton = [[UIButton alloc] initWithFrame:CGRectMake(DeviceWidth-50, 10, 35, 35)];
    
    navigateButton.backgroundColor = [UIColor clearColor];
    [navigateButton addTarget:self action:@selector(nearYouBtnClick) forControlEvents:UIControlEventTouchUpInside];
    [navigateButton setImage:[UIImage imageNamed:@"ic_located"] forState:UIControlStateNormal];
    
    [mapView addSubview:navigateButton];
    [self.view bringSubviewToFront:mapBtn];
}
- (void)nearYouBtnClick
{
    CLLocationCoordinate2D center;
    
    center.latitude=[AppDelegate ShareApp].locationManager.location.coordinate.latitude;
    center.longitude = [AppDelegate ShareApp].locationManager.location.coordinate.longitude;
    CLLocationCoordinate2D theCoordinate =CLLocationCoordinate2DMake(center.latitude, center.longitude);
    MKCoordinateSpan span;
    span.latitudeDelta=0.03;
    span.longitudeDelta=0.005;
    MKCoordinateRegion theRegion;
    theRegion.center=theCoordinate;
    theRegion.span=span;
    [mapView setRegion:theRegion animated:NO];
    
    [mapView regionThatFits:theRegion];
    mapView.showsUserLocation = YES;
}
#pragma mark - add subviews
-(void)loadMainView{

    wantToGoTab = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-88) style:UITableViewStylePlain];
    wantToGoTab.delegate = self;
    wantToGoTab.dataSource = self;
    wantToGoTab.scrollsToTop = YES;
    wantToGoTab.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    wantToGoTab.mj_footer = [IMGRefeshFooter footerWithRefreshingBlock:^{
        if (selectedIndex==0) {
            [self getWantTogoList];
        }else{
            [self getListsData];
        }
    }];
    wantToGoTab.mj_footer.hidden = YES;
    [self.view addSubview:wantToGoTab];
    
    
   
    [self createMapbutton];
    [self createMap];
    
    id savedJson = [CommonMethod getJsonFromFile:SAVED_CACHE];
    if (!savedJson) {
        shimmerView = [[IMGShimmerView alloc] initWithFrame:CGRectMake(0, 40, DeviceWidth, self.view.bounds.size.height - 40)];
        [shimmerView createRestaurantShimmerView];
        [self.view addSubview:shimmerView];
    }
}

- (void)createMapbutton{
    if (mapBtn) {
        [mapBtn removeFromSuperview];
    }
    mapBtn = [MapButton buttonWithType:UIButtonTypeCustom];
    mapBtn.frame = CGRectMake(DeviceWidth/2-45, DeviceHeight-144, 90, 40);
    mapBtn.hidden = YES;
    mapBtn.selected = NO;

    [mapBtn addTarget:self action:@selector(mapButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:mapBtn];
    [self.view bringSubviewToFront:mapBtn];
}
#pragma mark - map button click
-(void)mapButtonTapped{
    mapBtn.selected = !mapBtn.selected;
    if (mapBtn.selected) {
        
        [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Map" andRestaurantId:nil andRestaurantTitle:nil andLocation:nil andOrigin:@"Want to Go Page" andType:nil];
        
        mapBtn.frame = CGRectMake(DeviceWidth/2-45, DeviceHeight-260-54, 90, 40);
        wantToGoTab.hidden = YES;
        mapView.hidden = NO;
    }else{
        mapBtn.frame = CGRectMake(DeviceWidth/2-45, DeviceHeight-100-44, 90, 40);
        wantToGoTab.hidden = NO;
        mapView.hidden = YES;
    }
}
#pragma mark - tabelView datasouce and delegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (selectedIndex==0) {
        return savedArr.count;
        
    }else{
        NSInteger count;
        
        if (myListArr.count%2>0) {
            count = myListArr.count/2+1;
        }else{
            count = myListArr.count/2;
        }
        
        return count;
    }
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (selectedIndex==0) {
        //            static NSString *cellId = @"SavedCell";
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell1%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        SavedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[SavedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.delegate = self;
        if (savedArr.count>0) {
            IMGRestaurant *restaurant = [savedArr objectAtIndex:indexPath.row];
            cell.restaurant = restaurant;
        }
        
        return cell;
        
    }else{
        //            static NSString *savedCellId = @"MyListCell";
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        
        SavedMyListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[SavedMyListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        cell.delegate = self;
        if (myListArr.count==0) {
            [cell setFirstMyList:nil withIndex:0];
            
        }else{
            IMGMyList *firstList = myListArr[2*indexPath.row];
            [cell setFirstMyList:firstList withIndex:indexPath.row];
            cell.secondImageView.hidden = NO;
            cell.lblTitle.hidden = NO;
            cell.maskView.hidden = NO;
            if (myListArr.count>2*indexPath.row+1) {
                IMGMyList *secondList = myListArr[2*indexPath.row+1];
                [cell setSecondMyList:secondList];
                
            }
            else{
                cell.secondImageView.hidden = YES;
                cell.lblTitle.hidden = YES;
                cell.maskView.hidden = YES;
            }
            
        }
        
        return cell;
    }

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (selectedIndex==0) {
        IMGRestaurant *restaurant = [savedArr objectAtIndex:indexPath.row];
        return [tableView cellHeightForIndexPath:indexPath model:restaurant keyPath:@"restaurant" cellClass:[SavedTableViewCell class] contentViewWidth:DeviceWidth];
    }else{
        return (DeviceWidth-35)/2+8;
    }
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

    NSArray *tabs = @[@"Want to Go",@"My List"];
    
    tab=[[HMSegmentedControl alloc] initWithSectionTitles:tabs];
    tab.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    tab.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    tab.selectionIndicatorColor = [UIColor colorWithHexString:@"#D20000"];
    tab.selectedTitleTextAttributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:14],NSForegroundColorAttributeName : [UIColor colorWithHexString:@"#D20000"]};
    tab.titleTextAttributes = @{NSFontAttributeName:[UIFont boldSystemFontOfSize:14],NSForegroundColorAttributeName : [UIColor color333333]};
    
    tab.selectionIndicatorHeight = 2.5;
    [tab setFrame:CGRectMake(0,0,DeviceWidth,40)];
    tab.selectedSegmentIndex = selectedIndex;
    __weak typeof (mapBtn) weakMapBtn = mapBtn;
    __weak typeof (wantToGoTab) weakWantToGoTab = wantToGoTab;
    __weak typeof(shimmerView) weakShimmerView = shimmerView;
    [tab setIndexChangeBlock:^(NSInteger index){
        if (index==0) {
            if (savedArr.count>0) {
                weakMapBtn.hidden = NO;
            }
            if (weakShimmerView) {
                weakShimmerView.hidden = NO;
            }
            [[Amplitude instance] logEvent:@"CL - Saved Want to Go Tab" withEventProperties:@{}];
            selectedIndex=0;
            [weakWantToGoTab reloadData];

        }else{
            weakMapBtn.hidden = YES;
            if (weakShimmerView) {
                weakShimmerView.hidden = YES;
            }
            [[Amplitude instance] logEvent:@"CL - Saved My List Tab" withEventProperties:@{}];
            selectedIndex = 1;
            [weakWantToGoTab reloadData];
        }
    }];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 39.5, DeviceWidth, 0.5)];
    lineView.backgroundColor = [UIColor colorCCCCCC];
    [tab addSubview:lineView];
    return tab;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{

    if (savedArr.count==0 &&selectedIndex==0 && finishLoad) {
        
        EmptyListView *emptyListView=[[EmptyListView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 230) withType:WanttoGoType];
        emptyListView.delegate=self;
        
        return emptyListView;
    }else if (myListArr.count==0 &&selectedIndex==1&& finishLoadList){
        EmptyListView *emptyListView=[[EmptyListView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 230) withType:MyListType];
        emptyListView.delegate=self;
        
        return emptyListView;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{

    if (savedArr.count==0 && selectedIndex==0 && finishLoad) {
        return 230;
    }else if (myListArr.count==0 &&selectedIndex==1&&finishLoadList){
        return 230;
    }
    return 0.0001;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (selectedIndex==0) {
        IMGRestaurant *restaurant =savedArr[indexPath.row];
        
        [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Page" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:nil andOrigin:@"Saved - Want to go list" andType:nil];
        
        DetailViewController *devc = [[DetailViewController alloc] initWithRestaurant:restaurant];
        [self.navigationController pushViewController:devc animated:YES];
    }

}

#pragma  mark - get data
- (void)requestData{
    [self getListsData];
    [self getWantTogoList];
}
-(void)getListsData{
    isFreshList = YES;
    [ListHandler getListWithUser:[IMGUser currentUser] andOffset:myListOffset andMax:max andBlock:^(NSArray *dataArray, int listCount, BOOL hasData) {
        [wantToGoTab.mj_header endRefreshing];
        if (myListOffset==0) {
            [myListArr removeAllObjects];
            [allListArr removeAllObjects];
        }
        if (listCount>myListOffset) {
            myListOffset = myListOffset +10;
        }
        
        [wantToGoTab.mj_footer endRefreshing];
        for (IMGMyList *list in dataArray) {
            if (![list.listName isEqualToString:@"Want to go"]) {
                [myListArr addObject:list];
            }
        }
        [allListArr addObjectsFromArray:dataArray];
        finishLoadList = YES;
        isFreshList = NO;
        [wantToGoTab reloadData];
    } andFailedBlock:^{
        [wantToGoTab.mj_header endRefreshing];
    }];
    
}

-(void)getWantTogoList{
    isFreshWantToGo = YES;
    IMGUser *user = [IMGUser currentUser];
    NSDictionary *para=@{@"userId":user.userId,@"offset":[NSNumber numberWithInt:offset],@"max":[NSNumber numberWithInt:max],@"targetUserId":user.userId};
    [SavedDataHandler getWantToGoListWithPara:para andBlock:^(NSArray *restaurantsArr_,NSNumber* restaurantListItemCount,NSNumber *listId) {
        [wantToGoTab.mj_header endRefreshing];
        listId_ = listId;
        if (offset==0) {
            [savedArr removeAllObjects];
            [shimmerView removeFromSuperview];
        }
        wantToGoTab.mj_footer.hidden = NO;
        if ([restaurantListItemCount intValue]>offset) {
            offset = offset+10;
            [wantToGoTab.mj_footer endRefreshing];
        }
        [savedArr addObjectsFromArray:restaurantsArr_];
        [mapViewController setMapDatas:savedArr offset:0];
        if (savedArr.count!=0 && selectedIndex==0) {
            mapBtn.hidden = NO;
        }else{
            mapBtn.hidden=YES;
        }
        wantToGoTab.hidden = NO;
        [wantToGoTab.mj_footer endRefreshing];
        finishLoad = YES;
        isFreshWantToGo = NO;
        [wantToGoTab reloadData];
    } andFailedBlock:^{
        [wantToGoTab.mj_header endRefreshing];
    }];
}
-(void)clickTabMenuRefreshPage{
    
//    if (isFreshList  && isFreshWantToGo) {
//        return;
//    }
//    mapView.hidden = YES;
//    wantToGoTab.hidden = YES;
//    [self createMapbutton];
//
//
//    [savedArr removeAllObjects];
//    [myListArr removeAllObjects];
//    //[restaurantsArr removeAllObjects];

    offset = 0;
    myListOffset = 0;
    finishLoad = NO;
    finishLoadList = NO;
    
    [self requestData];
}
#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex ==0) {
        [self presentViewController:alertController animated:YES completion:nil];
    }
}
#pragma mark - SearchScrolViewDelegate
-(void)gotoPage:(SearchType)type andIndex:(NSInteger)index{
    DetailViewController *deVC = [[DetailViewController alloc] initWithRestaurantId:[NSNumber numberWithInteger:index]];
    [self.navigationController pushViewController:deVC animated:YES];
}

#pragma mark - cell delegate
-(void)reloadForNewList{
    [ListHandler getListWithUser:[IMGUser currentUser] andOffset:0 andMax:max andBlock:^(NSArray *dataArray, int listCount, BOOL hasData) {
        [[LoadingView sharedLoadingView] stopLoading];
        
        myListOffset= 0;
        [myListArr removeAllObjects];
        [allListArr removeAllObjects];
        if (listCount>myListOffset) {
            myListOffset = myListOffset +10;
        }
        [wantToGoTab.mj_footer endRefreshing];
        for (IMGMyList *list in dataArray) {
            if (![list.listName isEqualToString:@"Want to go"]) {
                [myListArr addObject:list];
            }
        }
        [allListArr addObjectsFromArray:dataArray];
        [wantToGoTab reloadData];
    } andFailedBlock:^{
        
        
    }];
}
-(void)addTolist:(IMGRestaurant *)restaurant{
    AddToListController *addToList=[[AddToListController alloc] initWithRestaurant:restaurant];
    void(^callback)(BOOL saved)=^(BOOL saved){
        [ListHandler getRestaurantSaved:restaurant.restaurantId withUser:[IMGUser currentUser] andBlock:^(BOOL status,NSInteger savedCount){
        }];
    };
    addToList.addToListBlock=callback;
    addToList.removeFromListBlock=callback;
    [self.navigationController pushViewController:addToList animated:YES];

}
-(void)gotoListPage:(IMGMyList *)mylist{
    
    [IMGAmplitudeUtil trackSavedWithName:@"RC - List Detail Page" andRestaurantId:nil andRestaurantTitle:nil andOrigin:nil andListId:mylist.listId andType:nil andLocation:nil];
    
    MyListRestaurantsViewController *listDetail=[[MyListRestaurantsViewController alloc] initWithMyList:mylist];
    listDetail.delegate = self;
    
    [self.navigationController pushViewController:listDetail animated:NO];

}
-(void)addRestaurantTolistWithRestrantId:(NSInteger)restaurantId{
    
    pressRestaurantId = restaurantId;
    [self createSavedAddListView];

}

- (void)createSavedAddListView{
    SavedAddListView *savedAddListView = [[SavedAddListView alloc] init];
    savedAddListView.delegate = self;
    savedAddListView.frame = [AppDelegate ShareApp].window.bounds;
    [[AppDelegate ShareApp].window addSubview:savedAddListView];
}

- (void)removeFromList:(NSNumber *)restaurantId{
    
    [IMGAmplitudeUtil trackSavedWithName:@"BM - Unsave Restaurant Initiate" andRestaurantId:restaurantId andRestaurantTitle:nil andOrigin:nil andListId:nil andType:@"Want to go" andLocation:nil];
    
    pressRestaurantId = [restaurantId integerValue];
    [[LoadingView sharedLoadingView] startLoading];
    [ListHandler removeRestaurantFromListWithRestaurantId:restaurantId andListId:listId_ andBlock:^{
        
        [[LoadingView sharedLoadingView] stopLoading];
        
        [IMGAmplitudeUtil trackSavedWithName:@"BM - Unsave Restaurant Succeed" andRestaurantId:restaurantId andRestaurantTitle:nil andOrigin:nil andListId:nil andType:@"Want to go" andLocation:nil];
        
        for (IMGRestaurant *restaurant in savedArr) {
            if ([restaurantId isEqual:restaurant.restaurantId]) {
                [savedArr removeObject:restaurant];
                break;
            }
        }
        if (savedArr.count==0) {
            mapBtn.hidden = YES;
        }else{
            [mapViewController setMapDatas:savedArr offset:0];
        }
        
        [wantToGoTab reloadData];
        [self createSelectViewWithTitle:@"Removed from list" andButtonTitle:@"Undo" andListId:listId_];
        
    } failure:^(NSString *exceptionMsg) {
        
    }];
}

#pragma mark - SavedAddListViewDelegate
//- (void)addNewList{
//    unGotoListPage = YES;
//    [self createNewlist];
//}
- (void)createSelectViewWithTitle:(NSString *)title andButtonTitle:(NSString *)buttonTitle andListId:(NSNumber *)listId{
    
    UIView *selectView = [[UIView alloc] initWithFrame:CGRectMake(0, DeviceHeight-137, DeviceWidth, 45)];
    
    selectView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:selectView];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 1)];
    lineView.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:248/255.0 alpha:1];
    [selectView addSubview:lineView];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, 12, 200, 20)];
    lblTitle.font = [UIFont systemFontOfSize:14];
    lblTitle.textColor = [UIColor grayColor];
    lblTitle.text = title;
    [selectView addSubview:lblTitle];
    
    UIButton *btnSelect = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSelect.frame = CGRectMake(DeviceWidth-LEFTLEFTSET-60, 0, 60, 45);
    btnSelect.titleLabel.font = [UIFont systemFontOfSize:15];
    [btnSelect setTitle:buttonTitle forState:UIControlStateNormal];
    [btnSelect setTitleColor:[UIColor barButtonTitleColor] forState:UIControlStateNormal];
    [btnSelect addTarget:self action:@selector(unDoButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [selectView addSubview:btnSelect];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(),^{
        [selectView removeFromSuperview];
    });
}
- (void)addRestaurantToList:(NSArray *)listIds andRestaurantId:(NSArray *)restaurantIDs{
    [ListHandler listAddRestaurantsWithUser:[IMGUser currentUser] andListIds:listIds andRestaurantIds:restaurantIDs andBlock:^(BOOL succeed, id status) {
        if (succeed) {
            [IMGAmplitudeUtil trackSavedWithName:@"BM - Save Restaurant Succeed" andRestaurantId:[NSNumber numberWithInteger:pressRestaurantId] andRestaurantTitle:nil andOrigin:nil andListId:nil andType:nil andLocation:@"Custom List"];
        }
        
        offset = 0;
        [self getWantTogoList];
        
    }];
}
- (void)unDoButtonClick:(UIButton *)button{
    if ([button.titleLabel.text isEqualToString:@"Undo"]) {
        NSArray *listArr = @[listId_];
        NSArray *restaurantArr = @[[NSNumber numberWithInteger:pressRestaurantId]];
        [self addRestaurantToList:listArr andRestaurantId:restaurantArr];
        
    }else{
        [ListHandler removeRestaurantFromListWithRestaurantId:[NSNumber numberWithInteger:pressRestaurantId] andListId:listId_ andBlock:^{
            
            [[LoadingView sharedLoadingView] stopLoading];
            
            for (IMGRestaurant *restaurant in savedArr) {
                if ([[NSNumber numberWithInteger:pressRestaurantId] isEqual:restaurant.restaurantId]) {
                    [savedArr removeObject:restaurant];
                    break;
                }
            }
            if (savedArr.count==0) {
                mapBtn.hidden = YES;
            }else{
                [mapViewController setMapDatas:savedArr offset:0];
            }
            [self createSavedAddListView];
            
            [wantToGoTab reloadData];
            
        } failure:^(NSString *exceptionMsg) {
            
        }];

    }
}

#pragma mark -MyListDelegate
-(void)goToAddRestaurant:(NSString *)title{
    if ([title isEqualToString:@"Create New List"]) {
        [self createNewlist];
    }else{
        [self addBtnClick];
    }
}

-(void)addBtnClick{
    for (IMGMyList *list in allListArr) {
        if ([list.listName isEqualToString:@"Want to go"]) {
            
            MyListAddRestaurantController *listController=[[MyListAddRestaurantController alloc] initWithMyList:list];
            listController.refreshRestaurantList = ^{
                offset = 0;
                [self getWantTogoList];
            };
            
            [self.navigationController pushViewController:listController animated:YES];
        }
    }
}

- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"RefreshList" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOTIFICATION_REFRESH_TAB_SAVED object:nil];
}

@end
