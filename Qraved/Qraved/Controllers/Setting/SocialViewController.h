//
//  SocialViewController.h
//  Qraved
//
//  Created by Laura on 14-8-15.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "IMGThirdPartyUser.h"
#import "EnterEmailViewController.h"
#import "SHKTwitter.h"

@interface SocialViewController : BaseChildViewController<UITableViewDataSource,UITableViewDelegate,registerDelegate>


@end
