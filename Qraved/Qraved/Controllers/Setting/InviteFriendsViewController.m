//
//  InviteFriendsViewController.m
//  Qraved
//
//  Created by apple on 2017/5/9.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "InviteFriendsViewController.h"

@interface InviteFriendsViewController ()

@end

@implementation InviteFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self loadMainView];
    [self setBackBarButton];
    self.view.backgroundColor = [UIColor whiteColor];

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.title = L(@"Invite Friends");
    self.navigationController.navigationBarHidden = NO;

}
-(void)loadMainView{
    UILabel *neverEatLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 40, DeviceWidth-30, 49)];
    neverEatLabel.text = @"Never Eat Alone!";
    neverEatLabel.font = [UIFont systemFontOfSize:28];
    [self.view addSubview:neverEatLabel];
    UILabel *inviteLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, neverEatLabel.endPointY+15, DeviceWidth-30, 49)];
    inviteLabel.numberOfLines = 2;
    inviteLabel.text = @"Invite your friend and help them experience a better way to discover food!!!";
    [self.view addSubview:inviteLabel];
    UILabel *rewardLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, inviteLabel.endPointY+14, DeviceWidth-30, 49)];
    rewardLabel.numberOfLines = 2;
    rewardLabel.text = @"Earn 100 reward points for each friend who joins Qraved App";
    [self.view addSubview:rewardLabel];
    NSArray *nameArr = @[@"Invite via Facebook",@"Invite via Whatsapp",@"Invite via Email",@"Copy link"];
    for (int i = 0 ; i<nameArr.count; i++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.backgroundColor = [UIColor grayColor];
        btn.frame = CGRectMake(30, rewardLabel.endPointY+50+(44+4)*i, DeviceWidth-60, 44);
        [btn setTitle:nameArr[i] forState:UIControlStateNormal];
        btn.tintColor = [UIColor color333333];
        [self.view addSubview:btn];
    }
}

@end
