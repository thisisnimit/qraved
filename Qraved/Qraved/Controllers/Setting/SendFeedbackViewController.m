//
//  SendFeedbackViewController.m
//  Qraved
//
//  Created by Laura on 14-8-15.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "SendFeedbackViewController.h"

#import "UIColor+Helper.h"
#import "Label.h"
#import "UIView+Helper.h"
#import "UIViewController+Helper.h"
#import "UIImageView+Helper.h"
#import "UIAlertView+BlocksKit.h"
#import "NSString+Helper.h"
#import "IMGUser.h"
#import "LoadingView.h"

@interface SendFeedbackViewController ()
{
    UITextView *_textView;
    Label *labeltext;
    UILabel *stringCountLabel;
}
@end

@implementation SendFeedbackViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = L(@"Feedback");
    self.navigationController.navigationBarHidden = NO;
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"Feedback form page";
    self.view.backgroundColor = [UIColor defaultColor];
    [self setBackBarButtonOffset30];
    [self loadMainView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changed:) name:UITextViewTextDidChangeNotification object:nil];

}
-(void)loadMainView
{
    

    
//    UIView *headeView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 37)];
//    headeView.backgroundColor = [UIColor colorFBFBFB];
//    Label *headeLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 0, DeviceWidth-2*LEFTLEFTSET, 36) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:16] andTextColor:[UIColor color222222] andTextLines:1];
//    headeLabel.text = L(@"Write to us");
//    [self.view addSubview:headeView];
//    [headeView addSubview:headeLabel];
//    
//    UIImageView *lineImageView1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, headeView.endPointY, DeviceWidth, 1)];
//    lineImageView1.backgroundColor = [UIColor colorEDEDED];
//    [self.view addSubview:lineImageView1];
    
    UIView *mainView = [[UIView alloc] initWithFrame:CGRectMake(15, 15, DeviceWidth-30, 180)];
    mainView.backgroundColor = [UIColor whiteColor];
    mainView.clipsToBounds = YES;
    mainView.layer.cornerRadius = 10;
    mainView.layer.borderWidth = 1;
    mainView.layer.borderColor = [UIColor grayColor].CGColor;
    [self.view addSubview:mainView];
    
    labeltext = [[Label alloc]initWithFrame:CGRectMake(5, 5, DeviceWidth-2*LEFTLEFTSET, 20) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor color666666] andTextLines:1];
    labeltext.text =L(@"Please let us know what you think of the app");
    labeltext.textAlignment = NSTextAlignmentLeft;
    [mainView addSubview:labeltext];
    stringCountLabel =[[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth-55, 140, 30, 30)];
    stringCountLabel.text = @"40";
    [mainView addSubview:stringCountLabel];

    
    _textView = [[UITextView alloc]initWithFrame:CGRectMake(5, 5, DeviceWidth-2*LEFTLEFTSET, 180)];
    _textView.textColor = [UIColor color222222];
    _textView.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14];
    _textView.delegate = self;
    _textView.backgroundColor = [UIColor clearColor];
    [mainView insertSubview:_textView aboveSubview:labeltext];
    
    
    UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    sendButton.frame = CGRectMake(LEFTLEFTSET, _textView.bottom +25, DeviceWidth-2*LEFTLEFTSET, 53);
//    [sendButton setBackgroundImage:[UIImage imageNamed:@"SendBtn"] forState:UIControlStateNormal];
    [sendButton setBackgroundColor:[UIColor colortextBlue]];//colorWithRed:9/255.0f green:191/255.0f blue:211/255.0f alpha:1.0f
    sendButton.layer.cornerRadius = sendButton.height/2;
    sendButton.layer.masksToBounds = YES;
    [sendButton setTitle:L(@"Send") forState:UIControlStateNormal];
    [sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sendButton addTarget:self action:@selector(sendButtonClick) forControlEvents:UIControlEventTouchUpInside];
    sendButton.clipsToBounds = YES;
    sendButton.layer.cornerRadius = 5;
    [self.view addSubview:sendButton];
    
    //标题下阴影
    UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:0 andHeight:4];
    [self.view addSubview:shadowImageView];
    
}
-(void)sendButtonClick
{
    IMGUser *user=[IMGUser currentUser];
    
    if ([_textView.text isBlankString]) {
        UIAlertView *alert=[UIAlertView bk_alertViewWithTitle:ALERT_TITLE message:(@"Feedback cannot be empty.")];
        [alert bk_addButtonWithTitle:@"OK" handler:^{
            
        }];
     
        [alert show];
        return;
    }
    
    [[LoadingView sharedLoadingView] startLoading];
    NSDictionary * parameters = @{@"userID":user.userId,@"content":_textView.text};
    [[IMGNetWork sharedManager]POST:@"feedback/add" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSString *returnStatusString = [responseObject objectForKey:@"status"];
        if([returnStatusString isEqualToString:@"succeed"]){
            NSLog(@"requestSaveUser succeed");
            [[Amplitude instance] logEvent:@"UC - Give Feedback Succeed" withEventProperties:nil];
        }
        [[LoadingView sharedLoadingView]stopLoading];
        [_textView resignFirstResponder];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"requestSaveUser error: %@",error.localizedDescription);
        [[LoadingView sharedLoadingView]stopLoading];
        [_textView resignFirstResponder];
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    labeltext.hidden=NO;
    return YES;
}
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{

    if ([text isEqualToString:@"\n"]) {//检测到“完成”
        [textView resignFirstResponder];//释放键盘
        return NO;
    }
    if (textView.text.length==0){//textview长度为0
        if ([text isEqualToString:@""]) {//判断是否为删除键
            labeltext.hidden=NO;//隐藏文字
        }else{
            labeltext.hidden=YES;
        }
    }else{//textview长度不为0
        if (textView.text.length==1){//textview长度为1时候
            if ([text isEqualToString:@""]) {//判断是否为删除键
                labeltext.hidden=NO;
            }else{//不是删除
                labeltext.hidden=YES;
            }
        }else{//长度不为1时候
            labeltext.hidden=YES;
        }
    }
    return YES;
}
 
-(void)changed:(NSNotification*)notification{
    UITextView *textview=notification.object;
    NSString *newReview=[textview.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *minimum=[NSString stringWithFormat:@"%lu",40-newReview.length];
    stringCountLabel.text = minimum;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
