//
//  EditProfileViewController.h
//  Qraved
//
//  Created by Laura on 14-8-15.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"

@interface EditProfileViewController : BaseChildViewController<UITextFieldDelegate>

@property (nonatomic, assign)  BOOL  isFromProfile;
@end
