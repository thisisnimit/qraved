//
//  SetNotificationViewController.m
//  Qraved
//
//  Created by josn.liu on 2016/12/14.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "SetNotificationViewController.h"
#import "UIViewController+Helper.h"
#import "NotificationHandler.h"
#import "NavigationBarButtonItem.h"
#import "UIDevice+Util.h"
#import "LoadingView.h"
#import "UIConstants.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "Label.h"
#import "UIDevice+Util.h"
#import "UIImageView+Helper.h"
@interface SetNotificationViewController ()<UITableViewDelegate,UITableViewDataSource>
{

    UITableView *mainTableview;
    NSArray *tiltleArr;
    NSMutableDictionary *setInfoDic;
    NSMutableDictionary *pushSetInfoDic;
    NSArray *sectionArr;
    UILabel *titleLable;
}
@end

@implementation SetNotificationViewController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.title = L(@"Notifications");
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor defaultColor];

    [self setBackBarButton];
    NSString *titleStr=L(@"Subscribe to Qraved E-newsletter");
    
    if ([UIDevice isIphone5]||[UIDevice isIphone4Now]) {
        titleStr=L(@"Subscribe to Qraved \nE-newsletter");
    }
    
    tiltleArr=@[@[L(@"User Like & Comment"),L(@"Food Recommendation & Promo")],@[titleStr]];
    pushSetInfoDic=[[NSMutableDictionary alloc] init];

    sectionArr=@[@"RECOMMENDATIONS",@"NEWSLETTER"];
    [self addmainView];
    [self getSetInfoFromService];
    
}
-(void)getSetInfoFromService{
    if ([IMGUser currentUser].userId!=nil&&[IMGUser currentUser].token!=nil) {
        
        NSNumber *userId=[IMGUser currentUser].userId;
        NSMutableDictionary *pargramer=[[NSMutableDictionary alloc]init];
        [pargramer setValue:userId forKey:@"userId"];
        
        [[LoadingView sharedLoadingView] startLoading];
       [NotificationHandler getUserNotificationSetByUserId:pargramer andBlock:^(id responseObject) {

           setInfoDic=[[NSMutableDictionary alloc] initWithDictionary:[responseObject objectForKey:@"userEdmSetting"]];
           [mainTableview reloadData];
            [[LoadingView sharedLoadingView] stopLoading];
       }];
    }

    

}
-(void)setBackBarButton{

    NavigationBarButtonItem *navigationBarLeftButton = [[NavigationBarButtonItem alloc ]initWithImage:[UIImage imageNamed:NavigationBackImage] andTitle:@"" target:self action:@selector(goToBackViewController) width:43.0f offset:-15-[UIDevice heightDifference]];
    self.navigationItem.leftBarButtonItem = navigationBarLeftButton;

}
-(void)addmainView{

    mainTableview=[[UITableView alloc]initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    mainTableview.backgroundColor=[UIColor whiteColor];
    mainTableview.delegate=self;
    mainTableview.separatorStyle=UITableViewCellSeparatorStyleNone;
    mainTableview.dataSource=self;
    [self.view addSubview:mainTableview];
    UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:0 andHeight:4];
    [self.view addSubview:shadowImageView];

}
-(void)goToBackViewController{
    
    [self.navigationController popViewControllerAnimated:YES];
    
    if (![pushSetInfoDic objectForKey:@"likeCommentNotification"]&&[setInfoDic objectForKey:@"likeCommentNotification"]) {
        
        [pushSetInfoDic setObject:[setInfoDic objectForKey:@"likeCommentNotification"] forKey:@"likeCommentNotification"];
    }
    if (![pushSetInfoDic objectForKey:@"promoNotification"]&&[setInfoDic objectForKey:@"promoNotification"]) {
        [pushSetInfoDic setObject:[setInfoDic objectForKey:@"promoNotification"] forKey:@"promoNotification"];
    }
    if (![pushSetInfoDic objectForKey:@"subscribleEmail"]&&[setInfoDic objectForKey:@"subscribleEmail"] ) {
        [pushSetInfoDic setObject:[setInfoDic objectForKey:@"subscribleEmail"] forKey:@"subscribleEmail"];
    }
    [[LoadingView sharedLoadingView] startLoading];
    [NotificationHandler userNotificationSetSaveAndUpdate:pushSetInfoDic andBlock:^(id responseObject) {
        [[LoadingView sharedLoadingView] stopLoading];
    }];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return tiltleArr.count;


}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 37.0f;

}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{

    return 0.1;

}
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{

//    UILabel *sectionlable=[[UILabel alloc]init];
//    sectionlable.text=sectionArr[section];
//    sectionlable.textColor=[UIColor color222222];
//    sectionlable.font=[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:18];
//    sectionlable.backgroundColor=[UIColor redColor];
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 37)];
    headerView.backgroundColor = [UIColor colorFBFBFB];
    Label *headerLabel = [[Label alloc]initWithFrame:CGRectMake(15, 0, DeviceWidth-2*LEFTLEFTSET, 36) andTextFont:[UIFont systemFontOfSize:13] andTextColor:[UIColor lightGrayColor] andTextLines:1];
    headerLabel.text = [sectionArr objectAtIndex:section];
    [headerView addSubview:headerLabel];
    return headerView;

}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    NSArray *arr=tiltleArr[section];
    
    return arr.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *setCell=[tableView dequeueReusableCellWithIdentifier:@"setcell"];

    if (!setCell) {
        setCell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"setcell"];
        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0,46.0f, DeviceWidth, 1)];
        lineImage.backgroundColor = [UIColor colorEDEDED];
        [setCell.contentView addSubview:lineImage];
        if ([UIDevice isIphone5]||[UIDevice isIphone4Now]) {
            lineImage.frame=CGRectMake(0, 51, DeviceWidth, 1);
        }

    }
    setCell.textLabel.backgroundColor=[UIColor clearColor];
    setCell.selectionStyle = UITableViewCellSelectionStyleNone;
    setCell.textLabel.textColor = [UIColor blackColor];
    setCell.textLabel.font = [UIFont systemFontOfSize:17];
    if (indexPath.row==0) {
        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 1)];
        lineImage.backgroundColor = [UIColor colorEDEDED];
        [setCell.contentView addSubview:lineImage];
    }
    NSArray *arr=tiltleArr[indexPath.section];
    setCell.textLabel.text=arr[indexPath.row];
    setCell.textLabel.numberOfLines=2;
    UISwitch *switchBtn=[[UISwitch alloc]initWithFrame:CGRectZero];
    switchBtn.onTintColor = [UIColor colortextBlue];
    if (indexPath.section==1) {
        switchBtn.tag=2;
    }else{
    
        switchBtn.tag=indexPath.row+indexPath.section;
    
    }
    [switchBtn addTarget:self action:@selector(swichBtn:) forControlEvents:UIControlEventValueChanged];
    setCell.accessoryView=switchBtn;
    NSString *info=[[NSString alloc]init];
    NSInteger number=indexPath.row+indexPath.section;
    if (indexPath.section==1) {
        number=2;
    }
    switch (number) {
        case 0:
        {
            info=[setInfoDic objectForKey:@"likeCommentNotification"];
        }
            break;
        case 1:
        {
            info=[setInfoDic objectForKey:@"promoNotification"];

        }
            break;
        case 2:
        {
            info=[setInfoDic objectForKey:@"subscribleEmail"];

        }
            break;
        default:
            break;
    }
    BOOL isON;
    if ([info isEqualToString:@"on"]) {
        isON=YES;
    }else{
        isON=NO;
    }
    [switchBtn setOn:isON animated:NO];
    return setCell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    if ([UIDevice isIphone5]||[UIDevice isIphone4Now]) {
        return 52;
    }else{
        return 47.0f;

    }

}
-(void)swichBtn:(UISwitch*)swichBtn{
    
    if ([IMGUser currentUser].userId!=nil) {
        [pushSetInfoDic setObject:[IMGUser currentUser].userId forKey:@"userId"];
    }
    switch (swichBtn.tag) {
        case 0:
        {
            if (swichBtn.on) {
                [pushSetInfoDic setObject:@"on" forKey:@"likeCommentNotification"];
            }else{
                [pushSetInfoDic setObject:@"off" forKey:@"likeCommentNotification"];
            }
            
        }
            break;
        case 1:
        {
            if (swichBtn.on) {
                [pushSetInfoDic setObject:@"on" forKey:@"promoNotification"];
            }else{
                [pushSetInfoDic setObject:@"off" forKey:@"promoNotification"];
            }


        }
            break;

        case 2:
        {
            if (swichBtn.on) {
                [pushSetInfoDic setObject:@"on" forKey:@"subscribleEmail"];
            }else{
                [pushSetInfoDic setObject:@"off" forKey:@"subscribleEmail"];
            }


        }
            break;

            
        default:
            break;
    }
    
}
 

@end
