//
//  SocialViewController.m
//  Qraved
//
//  Created by Laura on 14-8-15.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "SocialViewController.h"

#import "UIColor+Helper.h"
#import "Label.h"
#import "UIView+Helper.h"
#import "UIViewController+Helper.h"
#import "UIDevice+Util.h"
#import "UIImageView+Helper.h"
#import "NSString+Helper.h"
#import "DBManager.h"
#import "CustomIOS7AlertView.h"
#import "IMGUser.h"
#import "LoadingView.h"
#import "AppDelegate.h"
#import "PushHelper.h"

@implementation SocialViewController{
    NSArray *socialAccountsArr;
    NSArray *imagesArr;
    UITableView *tableView;
    
    IMGThirdPartyUser *facebookUser;
    IMGThirdPartyUser *twitterUser;
    BOOL connectInProgress;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    self.navigationItem.title = L(@"Social");
    self.navigationController.navigationBarHidden = NO;
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(loginWithTwitterInQraved:) name:TwitterConnectPartDone object:nil];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TwitterConnectPartDone object:nil];
}
- (void)viewDidLoad{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor defaultColor];
    [self setBackBarButtonOffset30];
    [self initData];
//    [self setSaveBarButton];
    [self loadMainView];
    
}

-(void)initData{
    IMGUser *user=[IMGUser currentUser];
    NSString *sqlString=[NSString stringWithFormat:@"select * from IMGThirdPartyUser where userId=%@;",user.userId];
    FMResultSet * resultSet = [[DBManager manager] executeQuery:sqlString];
    while([resultSet next]){
        IMGThirdPartyUser *thirdPartyUser=[[IMGThirdPartyUser alloc]init];
        [thirdPartyUser setValueWithResultSet:resultSet];
        if([thirdPartyUser.type intValue]==1){
            facebookUser=thirdPartyUser;
        }else if([thirdPartyUser.type intValue]==2){
            twitterUser=thirdPartyUser;
        }
    }
    [resultSet close];
}
-(void)setSaveBarButton{
    UIBarButtonItem *searchBtn=[[UIBarButtonItem alloc] initWithTitle:L(@"Save") style:UIBarButtonItemStylePlain target:self action:@selector(SaveButtonClick:)];
    searchBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=searchBtn;
}
-(void)SaveButtonClick{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)loadMainView{
    socialAccountsArr = @[L(@"Facebook"),L(@"Twitter")];
    imagesArr = @[@"facebook",@"twitter"];
    tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight) style:UITableViewStylePlain];
    tableView.scrollEnabled = NO;
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tableView];
    
    //标题下阴影
    UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:0 andHeight:4];
    [self.view addSubview:shadowImageView];

    
}
-(UITableViewCell*)tableView:(UITableView *)TableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CellIdentifier";
    // UITableViewCell *cell = [TableView dequeueReusableCellWithIdentifier:cellIdentifier];
    //  cell = nil;
    // if (cell == nil) {
    UITableViewCell *    cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    
    UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 61, DeviceWidth, 1)];
    lineImage.backgroundColor = [UIColor colorEDEDED];
    [cell.contentView addSubview:lineImage];
    
    //  }
    cell.textLabel.backgroundColor=[UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.textColor = [UIColor color222222];
    cell.textLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16];
    if(indexPath.row==0){
        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 1)];
        lineImage.backgroundColor = [UIColor colorEDEDED];
        [cell.contentView addSubview:lineImage];
    }
    cell.imageView.image = [UIImage imageNamed:[imagesArr objectAtIndex:indexPath.row]];
    if ((indexPath.row==0&&facebookUser!=nil)||(indexPath.row==1&&twitterUser!=nil)){
        Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(60, 8, 140, 25) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:14] andTextColor:[UIColor color222222] andTextLines:1];
        titleLabel.text = [socialAccountsArr objectAtIndex:indexPath.row];
        [cell.contentView addSubview:titleLabel];
        
        UIButton *disconectButton = [UIButton buttonWithType:UIButtonTypeCustom];
        disconectButton.frame = CGRectMake(DeviceWidth-100, 20, 90, 23);
        [disconectButton setTitle:L(@"Disconnect") forState:UIControlStateNormal];
        [disconectButton setTitleColor:[UIColor color222222] forState:UIControlStateNormal];
        disconectButton.tag = indexPath.row;
        disconectButton.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:14];
        [disconectButton addTarget:self action:@selector(disconectButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:disconectButton];
        
        Label *nameLabel = [[Label alloc]initWithFrame:CGRectMake(60, titleLabel.endPointY, 140, 25) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14] andTextColor:[UIColor color222222] andTextLines:1];
        if(indexPath.row==0){
            nameLabel.text = [NSString stringWithFormat:@"%@ %@",facebookUser.firstName,facebookUser.lastName];
        }else if(indexPath.row==1){
            nameLabel.text = [NSString stringWithFormat:@"%@ %@",twitterUser.firstName,twitterUser.lastName];
        }
        [cell.contentView addSubview:nameLabel];
    }else{
        cell.textLabel.text = [NSString stringWithFormat:L(@"Connect %@ account"),[socialAccountsArr objectAtIndex:indexPath.row]];
    }
    return cell;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return socialAccountsArr.count;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 37)];
    headerView.backgroundColor = [UIColor defaultColor];
    Label *headerLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 0, DeviceWidth-2*LEFTLEFTSET, 36) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:16] andTextColor:[UIColor color222222] andTextLines:1];
    headerLabel.text = L(@"Social Accounts");
    
    [headerView addSubview:headerLabel];
    return headerView;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 63.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 37.0f;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0&&facebookUser==nil){
        [self facebookConnect];
    }else if(indexPath.row==1&&twitterUser==nil){
        [self twitterConnect];
    }
}

-(void)twitterConnect{
    if(connectInProgress==TRUE){
        return;
    }
    connectInProgress=TRUE;
    [[NSUserDefaults standardUserDefaults]setObject:@"connect" forKey:TWITTERNOTIFICATIONTYPE];
    [SHK setRootViewController:self];
    SHKTwitter *twitter = [[SHKTwitter alloc] initWithNextDelegate:self];
    [twitter authorize];
    [self performSelector:@selector(progressAvailable) withObject:nil afterDelay:0.5];
}

-(void)progressAvailable{
    connectInProgress=FALSE;
}

-(void)loginWithTwitterInQraved:(NSNotification *)notification{
    NSDictionary *twitterResult = [notification.userInfo objectForKey:@"twitterResult"];
    NSError *error = (NSError *)[notification.userInfo objectForKey:@"error"];
    UserDataHandler *userDataController = [[UserDataHandler alloc] init];
    [[LoadingView sharedLoadingView]startLoading];
    [userDataController twitterConnect:twitterResult withError:error withDelegateBlock: ^(){
        [self loginSuccessful];
        [[LoadingView sharedLoadingView] stopLoading];
    }failedBlock:^{
        [[LoadingView sharedLoadingView] stopLoading];

    }];
}

-(void)facebookConnect{
    if(connectInProgress==TRUE){
        return;
    }
    connectInProgress=TRUE;
    NSArray *permissions = @[@"public_profile", @"email"];
    UserDataHandler *userDataHandler = [[UserDataHandler alloc] init];
    [userDataHandler faceBookConnect:permissions delegateWithBlock:^{
        [self loginSuccessful];
    } failedBlock:^{
        
    }];
//    [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
//        UserDataHandler *userDataHandler = [[UserDataHandler alloc] init];
//        [userDataHandler faceBookConnect:session state:state error:error delegateWithBlock: ^() {
//            [self loginSuccessful];
//        }failedBlock:^{
//
//        }];
//    }];
//    [FBSession openActiveSessionWithReadPermissions:permissions allowLoginUI:YES fromViewController:self completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
//        UserDataHandler *userDataHandler = [[UserDataHandler alloc] init];
//                [userDataHandler faceBookConnect:session state:status error:error delegateWithBlock: ^() {
//                    [self loginSuccessful];
//                }failedBlock:^{
//        
//                }];
//        
//    }];
    
    [self performSelector:@selector(progressAvailable) withObject:nil afterDelay:0.5];
}

-(void)loginSuccessful{
    [self initData];
    [tableView reloadData];
}
-(void)registerSuccess
{
    [self initData];
    [tableView reloadData];
}
-(void)disconectButtonClick:(UIButton*)button{
    UIView *popUpView = [[UIView alloc]init];
    IMGUser *user=[IMGUser currentUser];
    NSString *offerTitleStr = L(@"Are you sure want to disconnect this account?");
    CGSize maxSize = [offerTitleStr sizeWithFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
    Label *seeMenuTitle = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 64/2-(maxSize.height+4)/2, maxSize.width, maxSize.height+4) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] andTextColor:[UIColor color333333] andTextLines:0];
    seeMenuTitle.text = offerTitleStr;
    seeMenuTitle.textAlignment = NSTextAlignmentCenter;
    [popUpView addSubview:seeMenuTitle];
    
    popUpView.frame = CGRectMake(0, 0, DeviceWidth-2*LEFTLEFTSET, seeMenuTitle.endPointY);
    NSString *thirdId;
    NSNumber *type;
    if(button.tag==0&&facebookUser!=nil){
        thirdId=facebookUser.thirdId;
        type=[NSNumber numberWithInt:1];
    }else if(button.tag==1&&twitterUser!=nil){
        thirdId=twitterUser.thirdId;
        type=[NSNumber numberWithInt:2];
    }else{
        return;
    }
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc]init];
    alertView.buttonTitles = @[@"No, keep it",@"Yes"];
    [alertView setContainerView:popUpView];
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, NSInteger buttonIndex){
        if(buttonIndex==1){
            NSDictionary * parameters = @{@"userId":user.userId,@"t":user.token,@"thirdId":thirdId,@"type":type};
            [[LoadingView sharedLoadingView]startLoading];
            [[IMGNetWork sharedManager]POST:@"user/third/disconnect" parameters:parameters progress:nil  success:^(NSURLSessionDataTask *operation, id responseObject){
                NSString * exceptionMsg=[responseObject objectForKey:@"exceptionmsg"];
                if(exceptionMsg!=nil && [exceptionMsg isLogout]){
                    [[LoadingView sharedLoadingView]stopLoading];
                    [[IMGUser currentUser]logOut];
                    [[AppDelegate ShareApp]goToLoginController];
                }else{
                    NSString *returnStatusString = [responseObject objectForKey:@"status"];
                    if([returnStatusString isEqualToString:@"succeed"]){
                        NSString *sqlString=[NSString stringWithFormat:@"delete from IMGThirdPartyUser where userId='%@' and thirdId='%@' and type='%@';",user.userId,thirdId,type];
                        [[DBManager manager] deleteWithSql:sqlString];
                        if(button.tag == 0){
                            facebookUser=nil;
                        }else if(button.tag==1){
                            twitterUser=nil;
                        }
                        [[LoadingView sharedLoadingView]stopLoading];
                        [tableView reloadData];
                    }
                }
            }failure:^(NSURLSessionDataTask *operation, NSError *error){
                NSLog(@"requestSaveUser error: %@",error.localizedDescription);
                [[LoadingView sharedLoadingView]stopLoading];
            }];
        }
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    [alertView show];

}
 

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
