//
//  EditProfileViewController.m
//  Qraved
//
//  Created by Laura on 14-8-15.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "EditProfileViewController.h"

#import "UIColor+Helper.h"
#import "Label.h"
#import "UIView+Helper.h"
#import "UIDevice+Util.h"
#import "UIViewController+Helper.h"
#import "UIImageView+Helper.h"
#import "IMGUser.h"
#import "DBManager.h"
#import "LoadingView.h"
#import "NSString+Helper.h"
#import "IMGCountryCode.h"
#import "NSDate+Helper.h"
#import <CoreText/CoreText.h>
#import "Amplitude.h"
#import "AppDelegate.h"
#import "UIButton+WebCache.h"
#import "ZYQAssetPickerController.h"
#import "PostDataHandler.h"
#import "AvatarImageViewController.h"
#import "ProfileHandler.h"
#import "PersonalChangePasswordViewController.h"
#import "CustomButton.h"
#import "NSString+Helper.h"
@interface EditProfileViewController ()<UIPickerViewDataSource,UIPickerViewDelegate,UIScrollViewDelegate,ZYQAssetPickerControllerDelegate,UINavigationControllerDelegate>
{
    UIView *dateView;
    UIDatePicker *datePicker;
    UIButton *doneButton;
    BOOL isfinishiUploadPhoto;
    BOOL isFinishUploadInfomation;
    NSString *birthdayTextFieldString;
}
@end

@implementation EditProfileViewController{
    UIScrollView *scrollView;

    IMGUser * user;

    UITextField *firstNameTextField;
    UITextField *lastNameTextField;
    UITextField *birthdayTextField;
    UITextField *phoneTextField;
    UITextField *websiteTextField;
    UITextField *bioTextField;
    UITextField *pwdTextField;
    UITextField *emailTextField;
    
    int gender;
    
    UIView *codeNumView;
    UIPickerView *codeNumPicker;
    UIButton *doneButton2;
    
    NSMutableArray *codeArray;
    NSString *selectCode;
    NSInteger selectCodeIndex;
    NSString *phoneWithoutCountryCode;
    UITextField *onFocusTextField;
    UIView *editAvatarView;
    UIButton *avatarLinkButton;
    BOOL isChangeAvatar;
    BOOL ischangeBackimage;
    UIImageView *backGroundImageView;
    
    NSString *EmailString;
    NSString *bioString;
    BOOL imageBool;
    
    BOOL isSelecteImage;
    IMGProfileModel *profileModel;
    
    UIView *currentView;
    UIButton *btnCode;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = L(@"Edit Profile");
    self.navigationController.navigationBarHidden = NO;
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[Amplitude instance] logEvent:@"ST - Profile Update Initiate"];
    self.view.backgroundColor = [UIColor defaultColor];
    user = [IMGUser currentUser];
    
    phoneWithoutCountryCode=user.phone;
    selectCode=@"62";
    NSRange range =[user.phone rangeOfString:@"-"];
    if(range.location!=NSNotFound){
        selectCode=[user.phone substringToIndex:range.location];
        if(selectCode!=nil){
            NSRange range2 =[selectCode rangeOfString:@"+"];
            if (range2.location != NSNotFound) {
                selectCode = [selectCode substringFromIndex:range2.location+1];
            }
        }else{
            selectCode=@"62";
        }
        phoneWithoutCountryCode = [user.phone substringFromIndex:range.location+1];
    }else{
        range =[user.phone rangeOfString:@"+"];
        if(range.location!=NSNotFound){
            phoneWithoutCountryCode = [user.phone substringFromIndex:range.location+1];
        }
    }
    
    [self RequestData];
    
    [self setBackBarButtonOffset30];
    [self setSaveBarButton];

    codeArray = [[NSMutableArray alloc]init];
    
    UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:0 andHeight:4];
    [self.view addSubview:shadowImageView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(savePhoto:) name:@"savePhotoFromCameraNotification" object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(savebackPhoto:) name:@"savebackPhotoFromCameraNotification" object:nil];
}
- (void)RequestData{
    
    user = [IMGUser currentUser];
    if (user.userId!=nil) {
         [[LoadingView sharedLoadingView] startLoading];
        
        [ProfileHandler getEditProfile:@{@"targetUserId":user.userId} andSuccessBlock:^(IMGProfileModel * model) {
            [[LoadingView sharedLoadingView] stopLoading];
            profileModel = model;
            EmailString = model.email;
            bioString=   model.des;
            [self createMainUI];
            bioTextField.text = bioString;
            
        } anderrorBlock:^{
            [[LoadingView sharedLoadingView] stopLoading];
            
        }];
    }
    
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"savePhotoFromCameraNotification" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"savebackPhotoFromCameraNotification" object:nil];
}
-(void)addTopView{
    
    
    backGroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 130)];
    backGroundImageView.userInteractionEnabled = YES;
    backGroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    imageBool = YES;
    NSString *bgPath = NSHomeDirectory();
    NSURL *bgUrlString;
    NSString *bgUserAvatar = profileModel.coverImage;
    
    bgUserAvatar = [bgUserAvatar stringByReplacingPercentEscapesUsingEncoding:
                  NSUTF8StringEncoding];
    if ([bgUserAvatar hasPrefix:@"http"]){
        bgUrlString = [NSURL URLWithString:bgUserAvatar];
    }else{
        bgUrlString = [NSURL URLWithString:[bgUserAvatar returnFullImageUrl]];
    }
    NSString *bgImagePath = [bgPath stringByAppendingString:@"/Documents/coverImage.png"];
    UIImage *bgImage = [UIImage imageWithContentsOfFile:bgImagePath];
    if (bgImage&&[user.userId intValue]== [[IMGUser currentUser].userId intValue]) {
        [backGroundImageView setImage:bgImage];
    }else{
        [backGroundImageView sd_setImageWithURL:bgUrlString placeholderImage:[UIImage imageNamed:@"p_headerBackground"]];
    }

    
    [editAvatarView addSubview:backGroundImageView];
    
    //changetextbtn
    UIButton *changeBackGroundImageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    changeBackGroundImageBtn.frame = CGRectMake(DeviceWidth - 60, backGroundImageView.bottom - 12, 50, 12);
    [changeBackGroundImageBtn setImage:[UIImage imageNamed:@"changetextbtn"] forState:UIControlStateNormal];
    [changeBackGroundImageBtn addTarget:self action:@selector(changeBackGroundImageBtnClick:) forControlEvents:UIControlEventTouchUpInside];

    [backGroundImageView addSubview:changeBackGroundImageBtn];
    
    avatarLinkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    avatarLinkButton.frame=CGRectMake(0, 20, 90, 90);
    avatarLinkButton.centerX = backGroundImageView.centerX;
    avatarLinkButton.layer.cornerRadius = 45;
    avatarLinkButton.layer.masksToBounds = YES;
    imageBool = NO;
    [avatarLinkButton addTarget:self action:@selector(avatarClicked) forControlEvents:UIControlEventTouchUpInside];
    NSString *path_sandox = NSHomeDirectory();

    NSURL *urlString ;
    
    NSString *userAvatar = profileModel.avatar;
    
    userAvatar = [userAvatar stringByReplacingPercentEscapesUsingEncoding:
                  NSUTF8StringEncoding];
    if ([userAvatar hasPrefix:@"http"]){
        urlString = [NSURL URLWithString:userAvatar];
    }else{
        urlString = [NSURL URLWithString:[userAvatar returnFullImageUrl]];
    }
    NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/avastar.png"];
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    if (image&&[user.userId intValue]== [[IMGUser currentUser].userId intValue]) {
        [avatarLinkButton setImage:image forState:UIControlStateNormal];
    }else{
        [avatarLinkButton sd_setImageWithURL:urlString forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"headDefault.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        }];
    }
    [backGroundImageView addSubview:avatarLinkButton];
    UIButton *changePictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    changePictureBtn.frame = CGRectMake(DeviceWidth/2+30/2, backGroundImageView.bottom - 50, 30, 30);
    [changePictureBtn addTarget:self action:@selector(changePictureBtnClicked) forControlEvents:UIControlEventTouchUpInside];

    [changePictureBtn setImage:[UIImage imageNamed:@"ic_change_photo"] forState:UIControlStateNormal];
    
    changePictureBtn.clipsToBounds = YES;
    changePictureBtn.layer.cornerRadius = changePictureBtn.height/2;
    changePictureBtn.backgroundColor = [UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1];
    [changePictureBtn setTitleColor:[UIColor colorWithRed:149/255.0 green:149/255.0 blue:149/255.0 alpha:1] forState:UIControlStateNormal];
     changePictureBtn.contentMode = UIViewContentModeScaleAspectFill;
    [backGroundImageView addSubview:changePictureBtn];
   
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[DBManager manager]selectWithSql:@"select *from IMGCountryCode order by CAST(code AS INTeger)" successBlock:^(FMResultSet *resultSet) {
        while ([resultSet next]) {
            IMGCountryCode *countryCode = [[IMGCountryCode alloc]init];
            [countryCode setValueWithResultSet:resultSet];
            [codeArray addObject:countryCode];
        }
        [resultSet close];
        for (int i =0;i<codeArray.count;i++) {
            IMGCountryCode *code = [codeArray objectAtIndex:i];
            if ([code.code intValue] == [selectCode intValue]) {
                selectCode = [NSString stringWithFormat:@"%@",code.code];
                selectCodeIndex = i;
                break;
            }
        }
    } failureBlock:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[Amplitude instance] logEvent:@"ST - Profile Update Cancel"];
}
-(void)errorViewShowAlertText:(NSString*)alertText
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:alertText delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

-(void)setSaveBarButton{
    UIBarButtonItem *saveBtn=[[UIBarButtonItem alloc] initWithTitle:L(@"Done") style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonTapped)];
    saveBtn.tintColor=[UIColor colorWithRed:9/255.0f green:191/255.0f blue:211/255.0f alpha:1.0f];
    self.navigationItem.rightBarButtonItem=saveBtn;
}

-(void)saveButtonTapped{
    [[Amplitude instance] logEvent:@"CL - Confirm Profile Update" withEventProperties:@{@"Location":@"My Profile"}];
    NSString *completePhone=[NSString stringWithFormat:@"%@-%@",selectCode,phoneTextField.text];

    if (firstNameTextField.text.length>20) {
        [self errorViewShowAlertText:@"First name should be at most 20 characters"];
        return;
        
    }
    if (lastNameTextField.text.length>20) {
        [self errorViewShowAlertText:@"Last name should be at most 20 characters"];
        return;
    }
    
    if (bioTextField.text.length>150) {
        [self errorViewShowAlertText:@"Bio should be no more than 150 characters"];
        return;
    }
    if (phoneTextField.text.length != 0 &&![completePhone isValidateInternationPhone]) {
        [self errorViewShowAlertText:@"Incorrect phone number format"];
        return;
    }
    [self editDataPost];
}

-(void)requestSaveUser{
    
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil){
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    if(user.userId==nil){
        //lead to login
        return;
    }
    if(user.token==nil){
        //lead to login
        return;
    }

    if ([user.userId isEqualToNumber:(currentUser.userId)]) {
        user.token = currentUser.token;
    }
    if (isChangeAvatar) {
        [[LoadingView sharedLoadingView] startLoading];
            [[PostDataHandler sharedPostData] postImage:avatarLinkButton.imageView.image andTitle:@"editPofileIcon" andDescription:@"edit my profile icon" andSuccessBlock:^(id postDic){
                if (postDic) {
                    
//                    [[LoadingView sharedLoadingView] stopLoading];
                    if ([user.userId isEqualToNumber:([IMGUser currentUser].userId)]) {
                        user.token = [IMGUser currentUser].token;
                    }
                    NSDictionary *parameters = @{@"t":user.token,@"userID":user.userId,@"imageUrl":[postDic objectForKey:@"imgPath"]};
                    [[IMGNetWork sharedManager] POST:@"user/updateUserAvatar" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                        NSString *exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
                        if([exceptionMsg isLogout]){
                            [self dismissViewControllerAnimated:YES completion:^{
                                
                            }];
                            [[LoadingView sharedLoadingView] stopLoading];
                            [[IMGUser currentUser]logOut];
                            [[AppDelegate ShareApp] goToPage:0];
                            [[AppDelegate ShareApp] goToLoginController];
                            return;
                        }
                        
                        if ([[responseObject objectForKey:@"status"] isEqualToString:@"success"]) {
                            NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
                            [shared setObject:[postDic objectForKey:@"imgPath"] forKey:@"imgUrl"];
                            
                            [shared synchronize];
                            
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"changeAvatar" object:avatarLinkButton.imageView.image];
                           
                            
                            NSString *path_sandox = NSHomeDirectory();
                        
                            NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/avastar.png"];
                      
                            [UIImagePNGRepresentation(avatarLinkButton.imageView.image) writeToFile:imagePath atomically:YES];
                            [[LoadingView sharedLoadingView]stopLoading];
                            
                            [[Amplitude instance] logEvent:@"ST - Change User Profile Picture" withEventProperties:nil];
                            isfinishiUploadPhoto = YES;
                            if (isFinishUploadInfomation) {
                                [[LoadingView sharedLoadingView]stopLoading];
                                //[self.navigationController popViewControllerAnimated:YES];
                            }
                        }
                        
                    }failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                        [[LoadingView sharedLoadingView] stopLoading];
                        
                    }];

                }
                
            } andFailedBlock:^(NSError *error){
            }];

    }
    if (ischangeBackimage) {
         [[LoadingView sharedLoadingView] startLoading];
        [[PostDataHandler sharedPostData] postbackImage:backGroundImageView.image andTitle:@"editPofilebackicon" andDescription:@"edit my profile back icon" andSuccessBlock:^(id postDic){
            if (postDic) {
//                NSLog(@"%@",[postDic[@"imgPath"] returnFullImageUrl]);
               
                if ([user.userId isEqualToNumber:([IMGUser currentUser].userId)]) {
                    user.token = [IMGUser currentUser].token;
                }
                NSDictionary *parameters = @{@"t":user.token,@"userId":user.userId,@"imageUrl":[postDic objectForKey:@"imgPath"]};
                [[IMGNetWork sharedManager] POST:@"user/updateCoverImage" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    NSString *exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
                    if([exceptionMsg isLogout]){
                        [self dismissViewControllerAnimated:YES completion:^{
                            
                        }];
                        [[LoadingView sharedLoadingView] stopLoading];
                        [[IMGUser currentUser]logOut];
                        [[AppDelegate ShareApp] goToPage:0];
                        [[AppDelegate ShareApp] goToLoginController];
                        return;
                    }
                    
                    if ([[responseObject objectForKey:@"status"] isEqualToString:@"success"]) {
                         [[LoadingView sharedLoadingView]stopLoading];
                        NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
                        [shared setObject:[postDic objectForKey:@"imgPath"] forKey:@"imgUrl"];
                        
                        [shared synchronize];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"changeCoverImage" object:backGroundImageView.image];
                        
                        
                        NSString *path_sandox = NSHomeDirectory();
    
                        NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/coverImage.png"];
           
                        [UIImagePNGRepresentation(backGroundImageView.image) writeToFile:imagePath atomically:YES];
                        [[LoadingView sharedLoadingView]stopLoading];
//                         [AppDelegate ShareApp].backGroundImage = backGroundImageView.image;
                       
                        [[Amplitude instance] logEvent:@"ST - Change User Profile Picture" withEventProperties:nil];
                        isfinishiUploadPhoto = YES;
                        if (isFinishUploadInfomation) {
                           // [self.navigationController popViewControllerAnimated:YES];
                        }
                    }
                   
                }failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    [[LoadingView sharedLoadingView] stopLoading];
                    
                }];
            }
        } andFailedBlock:^{
    
        }];
    }
}
- (void)editDataPost{
    
    NSString *mobileNumber = [[selectCode stringByAppendingString:@"-"] stringByAppendingString:phoneTextField.text];
    if(![[mobileNumber substringToIndex:1] isEqualToString:@"+"]){
        mobileNumber=[@"+" stringByAppendingString:mobileNumber];
    }
//    NSLog(@"%@=======",birthdayTextField.text);
//    NSLog(@"%@=======",[NSString dateFormart2:birthdayTextField.text]);
    NSString *birthday;
    if (birthdayTextField.text == nil || birthdayTextField.text.length==0) {
        birthday = @"";
    }else{
        birthday = [NSString dateFormart2:birthdayTextField.text];
    }
    NSDictionary * parameters = @{@"userID":user.userId,@"t":user.token,@"email":user.email,@"username":user.userName,@"first_name":firstNameTextField.text,@"last_name":lastNameTextField.text,@"website":websiteTextField.text,@"mobile_number":mobileNumber,@"birthDate":birthday,@"gender":[NSString stringWithFormat:@"%d",gender], @"description":bioTextField.text};
    [[IMGNetWork sharedManager]POST:@"updateprofile" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSString *exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToPage:0];
            [[AppDelegate ShareApp] goToLoginController];
            return;
        }
        NSString *returnStatusString = [responseObject objectForKey:@"status"];
        if([returnStatusString isEqualToString:@"succeed"]){
            user.firstName=firstNameTextField.text;
            user.lastName=lastNameTextField.text;
            user.website=websiteTextField.text;
            user.phone = mobileNumber;
            user.gender=[NSString stringWithFormat:@"%d",gender];
            user.birthday=birthdayTextField.text;
            user.avatar=[responseObject objectForKey:@"userAvatar"];
            [[DBManager manager]insertModel:user selectField:@"userId" andSelectID:user.userId];
            NSUserDefaults *userd = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:user];
            [userd setObject:data forKey:@"person"];
            [userd synchronize];
            
//            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
//            [eventProperties setValue:[NSString stringWithFormat:@"%@,%@,%@,%@,%@",firstNameTextField.text,[NSString stringWithFormat:@"%d",gender],birthdayTextField.text,bioTextField.text,mobileNumber] forKey:@"Updated Information"];
            [[Amplitude instance] logEvent:@"ST - Profile Update Succeed" withEventProperties:nil];
        }
        [[LoadingView sharedLoadingView]stopLoading];
        isFinishUploadInfomation=YES;
        //        [[NSNotificationCenter defaultCenter] postNotificationName:@"avatarChangeNotifucation" object:nil userInfo:nil];
        if (_isFromProfile == NO) {
            
            [self.navigationController popViewControllerAnimated:YES];
        }else{
        
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"requestSaveUser error: %@",error.localizedDescription);
        [[LoadingView sharedLoadingView]stopLoading];
        [[Amplitude instance] logEvent:@"ST - Profile Update Failed"];
    }];
}

-(void)sexSelected:(UIButton *)button
{
    long tag = button.tag;
    UIButton *btnSex = (id)[self.view viewWithTag:tag];
    gender = (int)tag;
    [btnSex setTitleColor:[UIColor colorWithHexString:@"37AC29"] forState:UIControlStateNormal];
    switch (tag) {
        case 1:
        {
            UIButton *btnFemale = (id)[self.view viewWithTag:tag+1];
            [btnFemale setTitleColor:[UIColor color333333] forState:UIControlStateNormal];

        }
            break;
        case 2:
        {
            UIButton *btnMale = (id)[self.view viewWithTag:tag-1];
            [btnMale setTitleColor:[UIColor color333333] forState:UIControlStateNormal];

        }
            break;
        default:
            break;
    }
    
}
#pragma mark textfield delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    onFocusTextField = textField;
    dateView.hidden=YES;
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag != 102) {
        return YES;
    }
    if (textField.text.length == 2){
        textField.text = [NSString stringWithFormat:@"%@/",textField.text];
    }else if (textField.text.length == 5){
        textField.text = [NSString stringWithFormat:@"%@/",textField.text];
    }else if (textField.text.length >= 9){
        textField.text = [NSString stringWithFormat:@"%@",[textField.text substringToIndex:9]];
    }
    
    return YES;
}

-(void)addDatePicker{
    [self.view endEditing:YES];
    if (dateView == nil) {
        dateView = [[UIView alloc]initWithFrame:CGRectMake(0, DeviceHeight-290, DeviceWidth, 290)];
    }
    dateView.backgroundColor = [UIColor whiteColor];
    dateView.hidden=NO;
    if (datePicker == nil) {
        datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 40, DeviceWidth, 250)];
    }
    datePicker.datePickerMode = UIDatePickerModeDate;

    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *now = [NSDate date];;
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    comps = [calendar components:unitFlags fromDate:now];
    NSInteger  year = [comps year];
    NSString *maxStr =[NSString stringWithFormat:@"%ld-12-31",(long)year-5];
    NSDateFormatter *maxDateFormatter = [[NSDateFormatter alloc]init];
    [maxDateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *maxDate = [maxDateFormatter dateFromString:maxStr];
    NSString *minStr = [NSString stringWithFormat:@"%ld-01-01",(long)year-80];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *minDate = [dateFormatter dateFromString:minStr];
    datePicker.minimumDate=minDate;
    datePicker.maximumDate=maxDate;
    [dateView addSubview:datePicker];
    [self.view addSubview:dateView];
    doneButton = [UIButton buttonWithType:UIButtonTypeCustom];
    doneButton.frame = CGRectMake(DeviceWidth-60, 0, 50, 40);
    [doneButton setTitleColor:[UIColor color222222] forState:UIControlStateNormal];
    [doneButton setTitle:L(@"Done") forState:UIControlStateNormal];
    [dateView addSubview:doneButton];
    [doneButton addTarget:self action:@selector(datePickerDone) forControlEvents:UIControlEventTouchUpInside];
}
#pragma mark - change password
- (void)changePassword{

    PersonalChangePasswordViewController *PersonalChangePasswordV = [[PersonalChangePasswordViewController alloc] init];
    [self.navigationController pushViewController:PersonalChangePasswordV animated:YES];
}

-(void)datePickerDone{
    
    [datePicker removeFromSuperview];
    [dateView removeFromSuperview];
    NSLog(@"%@",datePicker.date);
    NSString *datestr = [datePicker.date getHDateFullString];
    NSLog(@"%@",datestr);
    birthdayTextFieldString = [datePicker.date get_MM_DD_YYYYFormatString];
    birthdayTextField.text = datestr;

}

-(void)addCodeNumberPicker{
    [self.view endEditing:YES];
    
    if (codeNumView == nil) {
        codeNumView = [[UIView alloc]initWithFrame:CGRectMake(0, DeviceHeight-290+20, DeviceWidth, 290)];
    }
    codeNumView.backgroundColor = [UIColor whiteColor];
    if (codeNumPicker == nil) {
        codeNumPicker = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 40, DeviceWidth, 250)];
    }
    codeNumPicker.delegate = self;
    codeNumPicker.dataSource = self;
    [codeNumPicker selectRow:selectCodeIndex inComponent:1 animated:YES];
    [codeNumView addSubview:codeNumPicker];
    [self.view addSubview:codeNumView];
    
    doneButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
    doneButton2.frame = CGRectMake(DeviceWidth-60, 0, 50, 40);
    [doneButton2 setTitleColor:[UIColor color222222] forState:UIControlStateNormal];
    [doneButton2 setTitle:L(@"Done") forState:UIControlStateNormal];
    [codeNumView addSubview:doneButton2];
    [doneButton2 addTarget:self action:@selector(codeNumDone) forControlEvents:UIControlEventTouchUpInside];
}
-(void)codeNumDone{
    [codeNumPicker removeFromSuperview];
    [codeNumView removeFromSuperview];
    if (selectCode) {
        [btnCode setTitle:[NSString stringWithFormat:@"+%@",selectCode] forState:UIControlStateNormal];
    }else{
        IMGCountryCode *code = [codeArray firstObject];
        [btnCode setTitle:[NSString stringWithFormat:@"+%@",code.code] forState:UIControlStateNormal];
    }
}
-(void)hideCodeNumPicker{
    [codeNumPicker removeFromSuperview];
    [codeNumView removeFromSuperview];
}
#pragma mark picker view delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component == 0) {
        return;
    }
    IMGCountryCode *code = [codeArray objectAtIndex:row];
    selectCode = [NSString stringWithFormat:@"%@",code.code];
    selectCodeIndex = row;
    
}
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0) {
        return 1;
    }
    return codeArray.count;
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}
-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component{
    if (component == 0) {
        return 40.0f;
    }
    return 100.0f;
}
#pragma mark picker view dataSource
-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (component == 0) {
        return @"+";
    }
    IMGCountryCode *code = [codeArray objectAtIndex:row];
    return [NSString stringWithFormat:@"%@",code.code];
}
-(NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (component == 0) {
        return nil;
    }
    IMGCountryCode *code = [codeArray objectAtIndex:row];
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc]initWithString:[NSString stringWithFormat:@"%@",code.code]];
    [attriString addAttribute:(NSString *)kCTFontAttributeName
                        value:(id)CFBridgingRelease(CTFontCreateWithName((CFStringRef)[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13].fontName,13,NULL))range:NSMakeRange(0, [NSString stringWithFormat:@"%@",code.code].length-1)];
    return attriString;
}

 

-(void)scrollViewWillBeginDragging:(UIScrollView *)_scrollView{
    dateView.hidden=YES;
    if (onFocusTextField!=nil) {
        [onFocusTextField resignFirstResponder];
    }
}
-(void)avatarClicked{
    AvatarImageViewController *avaVC = [[AvatarImageViewController alloc] init];
    avaVC.avatarImage = avatarLinkButton.imageView.image;
    [self presentViewController:avaVC animated:NO completion:nil];
}

-(void)changePictureBtnClicked{
    isSelecteImage = NO;
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] initWithRestaurantTitle:restaurant.title andIsFromUploadPhoto:NO andIsUploadMenuPhoto:NO andCurrentRestaurantId:restaurant.restaurantId];
    picker.maximumNumberOfSelection = 1;
    picker.assetsFilter = [ALAssetsFilter allPhotos];
    picker.showEmptyGroups=NO;
    picker.delegate = self;
    picker.isFromUploadPhoto = NO;
    picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
            NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
            return duration >= 5;
        } else {
            return YES;
        }
    }];
    
    [self presentViewController:picker animated:YES completion:NULL];
}
#pragma mark - ZYQAssetPickerController Delegate
-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{

    if (isSelecteImage == YES) {
        if (assets.count>0) {
            ALAsset *asset=assets[0];
            [backGroundImageView setImage:[self imageCompressForSize:[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage] targetSize:backGroundImageView.frame.size]];
            isChangeAvatar = NO;
            ischangeBackimage = YES;
            [picker dismissViewControllerAnimated:YES completion:^{
                //ischangeBackimage = YES;
                
            }];
            [self requestSaveUser];
        }
        

    }else{
        if (assets.count>0) {
            ALAsset *asset=assets[0];
            [avatarLinkButton setImage:[self imageCompressForSize:[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage] targetSize:avatarLinkButton.frame.size]forState:UIControlStateNormal];
            isChangeAvatar = YES;
            ischangeBackimage = NO;
            [picker dismissViewControllerAnimated:YES completion:^{
                //isChangeAvatar = YES;
                
            }];
            [self requestSaveUser];
            
        }
    }
}

- (void)savePhoto:(NSNotification *)notification{
    
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
        [[LoadingView sharedLoadingView] stopLoading];
        [[IMGUser currentUser]logOut];
        [[AppDelegate ShareApp] goToPage:0];
        [[AppDelegate ShareApp] goToLoginController];
        return;
    }
    
    [avatarLinkButton setImage:[self imageCompressForSize:[notification.userInfo objectForKey:@"finishImage"] targetSize:avatarLinkButton.frame.size]forState:UIControlStateNormal];
    
    isChangeAvatar=YES;
}
- (void)savebackPhoto:(NSNotification *)notification{
    
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
        [[LoadingView sharedLoadingView] stopLoading];
        [[IMGUser currentUser]logOut];
        [[AppDelegate ShareApp] goToPage:0];
        [[AppDelegate ShareApp] goToLoginController];
        return;
    }
    [backGroundImageView setImage:[self imageCompressForSize:[notification.userInfo objectForKey:@"finishbackImage"] targetSize:backGroundImageView.frame.size]];
    ischangeBackimage=YES;
}
-(UIImage *) imageCompressForSize:(UIImage *)sourceImage targetSize:(CGSize)size{
    // Create a graphics image context
    CGFloat width = size.width *2;
    CGFloat height = size.height *2;
    CGSize newSize = CGSizeMake(width, height);
    UIGraphicsBeginImageContext(newSize);
    
    // Tell the old image to draw in this new context, with the desired
    // new size
    [sourceImage drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    
    // Get the new image from the context
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // End the context
    UIGraphicsEndImageContext();
    
    // Return the new image.
    return newImage;
}


- (void)changeBackGroundImageBtnClick:(UIButton *)changeBackGroundImageBtnClick{

    isSelecteImage = YES;
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] initWithRestaurantTitle:restaurant.title andIsFromUploadPhoto:NO andIsUploadMenuPhoto:NO andCurrentRestaurantId:restaurant.restaurantId];
    picker.maximumNumberOfSelection = 1;
    picker.assetsFilter = [ALAssetsFilter allPhotos];
    picker.showEmptyGroups=NO;
    picker.delegate = self;
    picker.isFromUploadPhoto = NO;
    picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
            NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
            return duration >= 5;
        } else {
            return YES;
        }
    }];
    
    [self presentViewController:picker animated:YES completion:NULL];

}

- (void)createMainUI{
    scrollView = [UIScrollView new];
    [self.view addSubview:scrollView];
    
    scrollView.sd_layout
    .topSpaceToView(self.view, 0)
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .bottomSpaceToView(self.view, 0);
    
    editAvatarView = [UIView new];
    editAvatarView.backgroundColor = [UIColor defaultColor];
    [scrollView addSubview:editAvatarView];
    
    [self addTopView];
    
    UILabel *lblPersonal = [UILabel new];
    lblPersonal.text = @"PERSONAL INFORMATION";
    lblPersonal.textColor = [UIColor color999999];
    lblPersonal.font = [UIFont systemFontOfSize:13];
    lblPersonal.textAlignment = NSTextAlignmentLeft;
    
    UIView *lineview = [UIView new];
    lineview.backgroundColor = [UIColor colorEDEDED];
    
    [editAvatarView sd_addSubviews:@[lblPersonal,lineview]];
    
    editAvatarView.sd_layout
    .topSpaceToView(scrollView, 0)
    .leftSpaceToView(scrollView, 0)
    .rightSpaceToView(scrollView, 0)
    .heightIs(170);
    
    lblPersonal.sd_layout
    .topSpaceToView(editAvatarView, 145)
    .leftSpaceToView(editAvatarView, 15)
    .rightSpaceToView(editAvatarView, 15)
    .heightIs(20);
    
    lineview.sd_layout
    .bottomSpaceToView(editAvatarView, 1)
    .leftSpaceToView(editAvatarView, 0)
    .rightSpaceToView(editAvatarView, 0)
    .heightIs(1);
    
    currentView = editAvatarView;
    
    gender = [user.gender intValue];
    
    firstNameTextField = [UITextField new];
    firstNameTextField.placeholder = L(@"First Name");
    if (user.firstName != nil) {
        firstNameTextField.text = [user.firstName filterHtml];
    }
    [self createFieldViewWith:firstNameTextField andTitle:L(@"First Name") isBirthday:NO isBio:NO isPhone:NO isEmail:NO isPassword:NO];
    
    lastNameTextField = [UITextField new];
    lastNameTextField.placeholder = L(@"Last Name");
    if (user.lastName != nil) {
        lastNameTextField.text = [user.lastName filterHtml];
    }
    [self createFieldViewWith:lastNameTextField andTitle:L(@"Last Name") isBirthday:NO isBio:NO isPhone:NO isEmail:NO isPassword:NO];
    
    [self createGenderView:@"Gender"];
    
    birthdayTextField = [UITextField new];
    birthdayTextField.placeholder = L(@"Birthday(12/12/1980)");
    birthdayTextField.enabled = NO;
    if (user.birthday != nil) {
        NSString *string = [NSString dateFormart1:user.birthday];
        birthdayTextField.text = string;
    }
    [self createFieldViewWith:birthdayTextField andTitle:L(@"Birthday") isBirthday:YES isBio:NO isPhone:NO isEmail:NO isPassword:NO];
    
    websiteTextField = [UITextField new];
    websiteTextField.placeholder = L(@"Website");
    if (user.website != nil) {
        websiteTextField.text = user.website;
    }
    [self createFieldViewWith:websiteTextField andTitle:L(@"Website") isBirthday:NO isBio:NO isPhone:NO isEmail:NO isPassword:NO];
    
    bioTextField = [UITextField new];
    bioTextField.placeholder = @"Tell me about yourself";
    bioTextField.text = bioString;
    [self createFieldViewWith:bioTextField andTitle:L(@"Bio") isBirthday:NO isBio:YES isPhone:NO isEmail:NO isPassword:NO];
    
    UIView *bgPrivate = [UIView new];
    bgPrivate.backgroundColor = [UIColor defaultColor];
    [scrollView addSubview:bgPrivate];
    
    UILabel *lblPrivate = [UILabel new];
    lblPrivate.text = @"PRIVATE INFORMATION";
    lblPrivate.textColor = [UIColor color999999];
    lblPrivate.font = [UIFont systemFontOfSize:13];
    lblPrivate.textAlignment = NSTextAlignmentLeft;
    
    UIView *vPrivate = [UIView new];
    vPrivate.backgroundColor = [UIColor colorEDEDED];
    [bgPrivate sd_addSubviews:@[lblPrivate,vPrivate]];
    
    bgPrivate.sd_layout
    .topSpaceToView(currentView, 0)
    .leftSpaceToView(scrollView, 0)
    .rightSpaceToView(scrollView, 0)
    .heightIs(40);
    
    lblPrivate.sd_layout
    .topSpaceToView(bgPrivate, 15)
    .leftSpaceToView(bgPrivate, 15)
    .rightSpaceToView(bgPrivate, 15)
    .heightIs(20);
    
    vPrivate.sd_layout
    .bottomSpaceToView(bgPrivate, 1)
    .leftSpaceToView(bgPrivate, 0)
    .rightSpaceToView(bgPrivate, 0)
    .heightIs(1);
    
    currentView = bgPrivate;
    
    emailTextField = [UITextField new];
    emailTextField.placeholder = L(@"Email");
    emailTextField.enabled = NO;
    if (user.email != nil) {
        emailTextField.text = user.email;
    }
    [self createFieldViewWith:emailTextField andTitle:L(@"Email") isBirthday:NO isBio:NO isPhone:NO isEmail:YES isPassword:NO];
    
    phoneTextField = [UITextField new];
    phoneTextField.placeholder = L(@"Mobile Number");
    phoneTextField.keyboardType=UIKeyboardTypePhonePad;
    if (phoneWithoutCountryCode!=nil && ![phoneWithoutCountryCode isEqualToString:@""]) {
        phoneTextField.text = phoneWithoutCountryCode;
    }
    [self createFieldViewWith:phoneTextField andTitle:L(@"Phone") isBirthday:NO isBio:NO isPhone:YES isEmail:NO isPassword:NO];
    
    pwdTextField = [UITextField new];
    pwdTextField.text = @"•••••••••";
    pwdTextField.enabled = NO;
    [self createFieldViewWith:pwdTextField andTitle:L(@"Password") isBirthday:NO isBio:NO isPhone:NO isEmail:NO isPassword:YES];
    
    [scrollView setupAutoContentSizeWithBottomView:currentView bottomMargin:10];
}


- (void)createFieldViewWith:(UITextField *)textField andTitle:(NSString *)title isBirthday:(BOOL)isBirthday isBio:(BOOL)isBio isPhone:(BOOL)isPhone isEmail:(BOOL)isEmail isPassword:(BOOL)isPassword{
    UIView *bgView = [UIView new];
    [scrollView addSubview:bgView];
    
    bgView.sd_layout
    .topSpaceToView(currentView, 0)
    .leftSpaceToView(scrollView, 0)
    .rightSpaceToView(scrollView, 0)
    .heightIs(isBio?55:50);
    
    textField.font = [UIFont systemFontOfSize:16];
    textField.textColor = isEmail?[UIColor color999999]:[UIColor color333333];
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.delegate = self;
    textField.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    
    UILabel *label = [UILabel new];
    label.text = title;
    label.font = [UIFont systemFontOfSize:17];
    label.textColor = [UIColor color333333];
    label.textAlignment = NSTextAlignmentLeft;
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor colorEDEDED];
    
    [bgView sd_addSubviews:@[label,textField,lineView]];
    
    label.sd_layout
    .topSpaceToView(bgView, 0)
    .leftSpaceToView(bgView, 15)
    .widthIs(100)
    .heightIs(49);
    
    if (isPhone) {
        btnCode = [UIButton new];
        btnCode.titleLabel.font = [UIFont systemFontOfSize:16];
        [btnCode setTitle:@"code" forState:UIControlStateNormal];
        if (selectCode!=nil && ![selectCode isEqualToString:@""]) {
            [btnCode setTitle:[NSString stringWithFormat:@"+%@",selectCode] forState:UIControlStateNormal];
        }
        [btnCode setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
        [btnCode addTarget:self action:@selector(addCodeNumberPicker) forControlEvents:UIControlEventTouchUpInside];
        
        UIView *codeLine = [UIView new];
        codeLine.backgroundColor = [UIColor colorEDEDED];
        [bgView sd_addSubviews:@[btnCode,codeLine]];
        
        btnCode.sd_layout
        .topSpaceToView(bgView, 0)
        .leftSpaceToView(label, 0)
        .widthIs(50)
        .heightIs(49);
        
        codeLine.sd_layout
        .topSpaceToView(bgView, 0)
        .leftSpaceToView(btnCode, 0)
        .widthIs(1)
        .heightIs(49);
        
        textField.sd_layout
        .topSpaceToView(bgView, 0)
        .leftSpaceToView(codeLine, 15)
        .rightSpaceToView(bgView, 15)
        .heightIs(49);
    }else{
        textField.sd_layout
        .topSpaceToView(bgView, 0)
        .leftSpaceToView(label, 0)
        .rightSpaceToView(bgView, 15)
        .heightIs(49);
    }
    
    if (isBirthday) {
        UIControl *control = [UIControl new];
        [control addTarget:self action:@selector(addDatePicker) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:control];
        control.sd_layout
        .topSpaceToView(bgView, 0)
        .leftSpaceToView(label, 0)
        .rightSpaceToView(bgView, 15)
        .heightIs(49);
     }
    
    if (isBio) {
        UILabel *lblMax = [UILabel new];
        lblMax.font = [UIFont systemFontOfSize:10];
        lblMax.textColor = [UIColor color999999];
        lblMax.textAlignment = NSTextAlignmentRight;
        lblMax.text = @"(max. 150 characters)";
        [bgView addSubview:lblMax];
        
        lblMax.sd_layout
        .rightSpaceToView(bgView, 15)
        .bottomSpaceToView(bgView, 5)
        .widthIs(150)
        .heightIs(15);
    }
    
    if (isPassword) {
        UIControl *control = [UIControl new];
        [control addTarget:self action:@selector(changePassword) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:control];
        control.sd_layout
        .topSpaceToView(bgView, 0)
        .leftSpaceToView(label, 0)
        .rightSpaceToView(bgView, 15)
        .heightIs(49);
    }
    
    lineView.sd_layout
    .bottomSpaceToView(bgView, 1)
    .leftSpaceToView(bgView, 0)
    .rightSpaceToView(bgView, 0)
    .heightIs(1);
    
    currentView = bgView;
}

- (void)createGenderView:(NSString *)title{
    UIView *bgView = [UIView new];
    [scrollView addSubview:bgView];
    
    bgView.sd_layout
    .topSpaceToView(currentView, 0)
    .leftSpaceToView(scrollView, 0)
    .rightSpaceToView(scrollView, 0)
    .heightIs(50);
    
    UILabel *label = [UILabel new];
    label.text = title;
    label.font = [UIFont systemFontOfSize:17];
    label.textColor = [UIColor color333333];
    label.textAlignment = NSTextAlignmentLeft;
    
    UIButton *btnMale = [UIButton new];
    btnMale.titleLabel.font = [UIFont systemFontOfSize:16];
    [btnMale setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    btnMale.tag = 1;
    [btnMale setTitle:@"Male" forState:UIControlStateNormal];
    [btnMale addTarget:self action:@selector(sexSelected:) forControlEvents:UIControlEventTouchUpInside];
    btnMale.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    UIButton *btnFemale = [UIButton new];
    btnFemale.titleLabel.font = [UIFont systemFontOfSize:16];
    btnFemale.tag = 2;
    [btnFemale setTitle:@"Female" forState:UIControlStateNormal];
    [btnFemale addTarget:self action:@selector(sexSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    if (gender==1) {
        [btnMale setTitleColor:[UIColor colorWithHexString:@"37AC29"] forState:UIControlStateNormal];
        [btnFemale setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    }else{
        [btnFemale setTitleColor:[UIColor colorWithHexString:@"37AC29"] forState:UIControlStateNormal];
        [btnMale setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    }
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor colorEDEDED];
    
    [bgView sd_addSubviews:@[label,btnMale,btnFemale,lineView]];
    
    label.sd_layout
    .topSpaceToView(bgView, 0)
    .leftSpaceToView(bgView, 15)
    .widthIs(100)
    .heightIs(49);
    
    btnMale.sd_layout
    .topSpaceToView(bgView, 0)
    .leftSpaceToView(label, 0)
    .widthIs(70)
    .heightIs(49);
    
    btnFemale.sd_layout
    .topEqualToView(btnMale)
    .leftSpaceToView(btnMale, 30)
    .widthIs(70)
    .heightIs(49);
    
    lineView.sd_layout
    .bottomSpaceToView(bgView, 1)
    .leftSpaceToView(bgView, 0)
    .rightSpaceToView(bgView, 0)
    .heightIs(1);

    currentView = bgView;
}

@end
