//
//  SettingViewController.m
//  Qraved
//
//  Created by Laura on 14-8-15.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "SettingViewController.h"

#import "AppDelegate.h"

#import "EditProfileViewController.h"
#include "NotificationSettingViewController.h"
#import "SocialViewController.h"
#import "SendFeedbackViewController.h"
#import "AboutUsViewController.h"

#import "UIColor+Helper.h"

#import "NavigationBarButtonItem.h"
#import "CustomIOS7AlertView.h"
#import "DBManager.h"
#import "IMGUser.h"
#import "UserDataHandler.h"
#import "SetNotificationViewController.h"

@interface SettingViewController ()
{
    NSArray *sectionsArr;
    NSArray *accountArr;
    NSArray *aboutArr;
}
@end

@implementation SettingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"My Account Setting page";
    self.view.backgroundColor = [UIColor defaultColor];
    [self addNotification];
    [self addBackBtn];
    [self loadMainView];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = L(@"Settings");
    self.navigationController.navigationBarHidden = NO;
}
-(void)loadMainView
{
    
    
    sectionsArr = @[L(@"Account"),L(@"About")];
    accountArr = @[L(@"Preference"),L(@"Social accounts"),L(@"Signout")];
    aboutArr = @[L(@"Rate us on AppStore"),L(@"About Us")];
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight) style:UITableViewStylePlain];
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:tableView];
    
    //标题下阴影
    UIImageView *shadowImageView = [[UIImageView alloc]initShadowImageViewWithShadowOriginY:0 andHeight:4];
    [self.view addSubview:shadowImageView];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        
        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 46, DeviceWidth, 1)];
        lineImage.backgroundColor = [UIColor colorEDEDED];
        [cell.contentView addSubview:lineImage];
        
    }
    cell.textLabel.backgroundColor=[UIColor clearColor];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.textColor = [UIColor color222222];
    cell.textLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16];
    if (indexPath.row==0) {
        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 1)];
        lineImage.backgroundColor = [UIColor colorEDEDED];
        [cell.contentView addSubview:lineImage];
    }
    if (indexPath.section == 0) {
        cell.textLabel.text = [accountArr objectAtIndex:indexPath.row];
    }
    if (indexPath.section == 1) {
        cell.textLabel.text = [aboutArr objectAtIndex:indexPath.row];
    }
    
    return cell;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return sectionsArr.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return accountArr.count;
    }
    if (section==1) {
        return aboutArr.count;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 37)];
    headerView.backgroundColor = [UIColor colorFBFBFB];
    Label *headerLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 0, DeviceWidth-2*LEFTLEFTSET, 36) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:16] andTextColor:[UIColor color222222] andTextLines:1];
    headerLabel.text = [sectionsArr objectAtIndex:section];
    
    
    [headerView addSubview:headerLabel];
    
    
    
    return headerView;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 47.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 37.0f;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        switch (indexPath.row) {
                //            case 0:
                //            {//Edit profile
                //                if ([AppDelegate ShareApp].userLogined) {
                //                    EditProfileViewController *editProfileVC = [[EditProfileViewController alloc]init];
                //                    [self.navigationController pushViewController:editProfileVC animated:YES];
                //                }
                //                else
                //                {
                //                    [[AppDelegate ShareApp] goToLoginController];
                //                }
                //            }
                //                break;
                //            case 1:
                //            {//Notification setting
                //                NotificationSettingViewController *notificationSettingVC = [[NotificationSettingViewController alloc]init];
                //                [self.navigationController pushViewController:notificationSettingVC animated:YES];
                //            }
                //                break;
                
            case 0:{
                if ([AppDelegate ShareApp].userLogined) {
                    SetNotificationViewController *socialAccountsVC = [[SetNotificationViewController alloc]init];
                    [self.navigationController pushViewController:socialAccountsVC animated:YES];
                }
                else
                {
                    [[AppDelegate ShareApp] goToLoginController];
                }
                
            }
                break;
                
            case 1:
            {//Social accounts
                if ([AppDelegate ShareApp].userLogined) {
                    SocialViewController *socialAccountsVC = [[SocialViewController alloc]init];
                    [self.navigationController pushViewController:socialAccountsVC animated:YES];
                }
                else
                {
                    [[AppDelegate ShareApp] goToLoginController];
                }
            }
                break;
            case 2:
            {//Signout
                
                UIView *signOutView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth-2*LEFTLEFTSET, 100)];
                
                Label *signOutTitle = [[Label alloc]initWithFrame:CGRectMake(0, 20, DeviceWidth-2*LEFTLEFTSET, 30) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] andTextColor:[UIColor color222222] andTextLines:0];
                signOutTitle.text = L(@"Log out");
                signOutTitle.textAlignment = NSTextAlignmentCenter;
                [signOutView addSubview:signOutTitle];
                NSString *loginType = [[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"];
                if (loginType.length>0) {
                    [[Amplitude instance] logEvent:@"SO - Sign Out Initiate" withEventProperties:@{@"Sign out Type":loginType}];
                    
                }
                Label *signOutAlertLabel = [[Label alloc]initWithFrame:CGRectMake(0, 50, DeviceWidth-2*LEFTLEFTSET, 40) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15] andTextColor:[UIColor color333333] andTextLines:1];
                signOutAlertLabel.textAlignment = NSTextAlignmentCenter;
                signOutAlertLabel.text = L(@"Are you sure you want to log out?");
                signOutAlertLabel.lineBreakMode = NSLineBreakByWordWrapping;
                [signOutView addSubview:signOutAlertLabel];
                
                CustomIOS7AlertView *signOutAlertView = [[CustomIOS7AlertView alloc]init];
                
                signOutAlertView.buttonTitles = @[L(@"No"),L(@"Yes")];
                
                // Add some custom content to the alert view
                [signOutAlertView setContainerView:signOutView];
                
                //                signOutAlertView.delegate = self;
                
                // You may use a Block, rather than a delegate.
                [signOutAlertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, NSInteger buttonIndex) {
                    if (buttonIndex==0) {
                        if (loginType.length>0) {
                            [[Amplitude instance] logEvent:@"SO - Sign Out Cancel" withEventProperties:@{@"Sign out Type":loginType}];
                            
                        }
                    }
                    else if (buttonIndex == 1)
                    {
                        [AppDelegate ShareApp].isShowPersonalization = NO;
                        [[AppDelegate ShareApp].ProfileNavigationController popToRootViewControllerAnimated:NO];
                        //                        [[AppDelegate ShareApp].myProfileViewController setCurrentTabIndex:0];
                        
                        [UserDataHandler logout];
                        [[Amplitude instance] setUserId:nil];
                        [[Amplitude instance] clearUserProperties];
                        [AppDelegate ShareApp].isAlreadySetAmplitudeUserProperties = NO;
                        
                        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"userLogined"];
                        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"loginUserId"];
                        
                        NSString *path_sandox = NSHomeDirectory();
                        //设置一个图片的存储路径
                        NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/avastar.png"];
                        NSFileManager * fileManager = [[NSFileManager alloc]init];
                        [fileManager removeItemAtPath:imagePath error:nil];
                        
                        [AppDelegate ShareApp].userLogined=NO;
                        
                        [SHK logoutOfAll];
                        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isPushSelCity"];
                        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"emailPushSelCity"];
                        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"registPushSelCity"];
                        
                        
                        NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
                        [shared removeObjectForKey:@"userId"];
                        [shared removeObjectForKey:@"userToken"];
                        [shared removeObjectForKey:@"person"];
                        [shared removeObjectForKey:@"imgUrl"];
                        [shared synchronize];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"logoutNotification" object:nil userInfo:nil];
                        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_HOME object:nil];
                        [[AppDelegate ShareApp] requestToShowNotificationCount];
                        [[AppDelegate ShareApp] goToPage:0];
                        if (loginType.length>0) {
                            [[Amplitude instance] logEvent:@"SO - Sign Out Succeed" withEventProperties:@{@"Sign out Type":loginType}];
                            
                        }
                        
                    }
                    
                    [alertView close];
                }];
                
                [signOutAlertView setUseMotionEffects:true];
                
                // And launch the dialog
                [signOutAlertView show];
                
            }
                break;
            default:
                break;
        }
    }
    if (indexPath.section == 1) {
        switch (indexPath.row) {
            case 0:
            {//Rate us on AppStore
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://itunes.apple.com/cn/app/qraved/id731842943?l=en&mt=8"]];
            }
                break;
                
            case 1:
            {//ABout US
                AboutUsViewController *aboutUsVC = [[AboutUsViewController alloc]init];
                [self.navigationController pushViewController:aboutUsVC animated:YES];
            }
                break;
                
            default:
                break;
        }
    }
    
    
    
}

-(void)addNotification{
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(LogoutSuccess) name:@"logoutNotification" object:nil];
}
-(void)removeNotification{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logoutNotification" object:nil];
}
-(void)LogoutSuccess{
    //    [loginButton setTitle:@"Login" forState:UIControlStateNormal];
    
    [AppDelegate ShareApp].userLogined=NO;
    [IMGUser currentUser].userId = nil;
    [IMGUser currentUser].email = nil;
    [IMGUser currentUser].firstName = nil;
    [IMGUser currentUser].lastName = nil;
    [IMGUser currentUser].fullName = nil;
    [IMGUser currentUser].userName = nil;
    [IMGUser currentUser].avatar = nil;
    [IMGUser currentUser].token = nil;
    [IMGUser currentUser].birthday = nil;
    [IMGUser currentUser].phone = nil;
    [IMGUser currentUser].website = nil;
    [IMGUser currentUser].gender = nil;
    [IMGUser currentUser].cityName = nil;
    [IMGUser currentUser].districtName = nil;
    [IMGUser currentUser].facebookAcount = nil;
    [IMGUser currentUser].twitterAcount = nil;
    [IMGUser currentUser].lastLoginDate = nil;
    [IMGUser currentUser].bookingCount = nil;
    [IMGUser currentUser].redeemablePoints = nil;
    [IMGUser currentUser].reviewCount = nil;
    [IMGUser currentUser].islogin = nil;
    
    [AppDelegate ShareApp].myProfileViewController=nil;
    [AppDelegate ShareApp].pointRewardViewController=nil;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
