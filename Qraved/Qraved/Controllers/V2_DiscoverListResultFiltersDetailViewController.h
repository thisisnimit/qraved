//
//  V2_DiscoverListResultFiltersDetailViewController.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/28.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"

@interface V2_DiscoverListResultFiltersDetailViewController : BaseChildViewController

@property (nonatomic, strong)  NSString  *searchString;

@end
