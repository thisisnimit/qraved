//
//  AddToListController.h
//  Qraved
//
//  Created by System Administrator on 9/25/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "BaseChildViewController.h"
#import "MyListDelegate.h"

@class IMGRestaurant;
@class AddToListView;

@interface AddToListController : BaseChildViewController<MyListDelegate>
@property(nonatomic,retain) NSString *amplitudeType;
@property(nonatomic,strong) IMGRestaurant *restaurant;
@property(nonatomic,strong) AddToListView *listView;
@property(nonatomic,copy) void(^addToListBlock)(BOOL saved);
@property(nonatomic,copy) void(^removeFromListBlock)(BOOL revmoed);

@property(nonatomic,copy) void(^toListBlock)(BOOL isssaved);


-(instancetype)initWithRestaurant:(IMGRestaurant*)restaurant;

@end
