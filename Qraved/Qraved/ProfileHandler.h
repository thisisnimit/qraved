//
//  ProfileHandler.h
//  Qraved
//
//  Created by Gary.yao on 2017/6/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGSummary.h"
#import "IMGReservation.h"
#import "IMGProfileModel.h"
#import "IMGListModel.h"
#import "PersonalSummaryTopReviewModel.h"

@interface ProfileHandler : NSObject

+(void)getsummary:(NSDictionary*)dic andSuccessBlock:(void(^)(IMGSummary * summary ))successBlock anderrorBlock:(void(^)())errorBlock;

+(void)getUpCommingBooking:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array))successBlock anderrorBlock:(void(^)())errorBlock;

+(void)getPhotos:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array))successBlock anderrorBlock:(void(^)())errorBlock;


+(void)getSelfProfile:(NSDictionary*)dic andSuccessBlock:(void(^)(IMGProfileModel * model))successBlock anderrorBlock:(void(^)())errorBlock;

+(void)getOtherProfile:(NSDictionary*)dic andSuccessBlock:(void(^)(IMGProfileModel * model))successBlock anderrorBlock:(void(^)())errorBlock;

+(void)getOtherList:(NSDictionary*)dic andSuccessBlock:(void(^)(IMGListModel * model))successBlock anderrorBlock:(void(^)())errorBlock;


+(void)getJourney:(NSDictionary *)dic andSuccessBlock:(void(^)(NSMutableArray * journeyArray))successBlock anderrorBlock:(void(^)())errorBlock;


+(void)getEditProfile:(NSDictionary*)dic andSuccessBlock:(void(^)(IMGProfileModel * model))successBlock anderrorBlock:(void(^)())errorBlock;

+(void)changePassword:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableDictionary *twoDict))successBlock anderrorBlock:(void(^)())errorBlock;


+(void)getOtherJourney:(NSDictionary *)dic andSuccessBlock:(void(^)(NSMutableArray * journeyArray))successBlock anderrorBlock:(void(^)())errorBlock;

@end
