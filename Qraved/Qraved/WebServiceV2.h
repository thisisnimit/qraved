//
// Created by Tek Yin on 5/16/17.
// Copyright (c) 2017 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TaskResponder;


@interface WebServiceV2 : NSObject

+ (NSString *)getFullLinkForImage:(NSString *)imgUrl;

//+ (TaskResponder *)getCities;
//
//+ (TaskResponder *)getDistrictInCity:(NSString *)cityId;
//
//+ (TaskResponder *)getHomePageDataWithId:(NSString *)userId cityId:(NSString *)cityId;
//
//+ (TaskResponder *)getInstagramPostWithCityId:(NSString *)cityId districtId:(NSArray<NSString *> *)districtId count:(int)count page:(int)page;
//
//+ (TaskResponder *)getContributions:(NSString *)userId;
+ (void)getBanners:(NSDictionary *)params andBlock:(void(^)(id bannerArr))successBlock andError:(void(^)(NSError *error))errorBlock;
//+ (void)getCities:(void(^)(id object))successBlock andError:(void(^)(NSError *error))errorBlock;

+ (void)getHomeData:(NSString *)page andBlock:(void(^)(id homedata))successBlock andError:(void(^)(NSError *error))errorBlock;

+ (void)getStripBanner:(NSDictionary *)params andBlock:(void(^)(id bannerArr))successBlock andError:(void(^)(NSError *error))errorBlock;

+ (void)getGoFoodBanner:(NSDictionary *)params andBlock:(void(^)(NSArray  *bannerArr))successBlock andError:(void(^)(NSError *error))errorBlock;

+ (void)instagramRestaurant:(NSDictionary *)params andBlock:(void(^)(IMGRestaurant *restaurant,NSNumber *instagramLocationId,NSDictionary *photoDic))successBlock andError:(void(^)(NSError *error))errorBlock;

+ (void)relatedArticleArray:(NSDictionary *)params andBlock:(void(^)(NSArray *relatedArr))successBlock andError:(void(^)(NSError *error))errorBlock;



@end
