//
//  V2_QuickSearchView.m
//  Qraved
//
//  Created by harry on 2017/6/15.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_QuickSearchView.h"
#import "V2_QuickSearchModel.h"
#import "UIColor+Hex.h"
@implementation V2_QuickSearchView


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self createUI];
    }
    return self;
}

- (void)createUI{
    
    NSArray *searchArr = @[@"Restaurants",@"Jajanan",@"Cafe",@"Bars",@"Drinks",@"Sweets",@"Street Foods",@"Promos"];
    UIView *temView = [UIView new];

    NSMutableArray *tempArr = [NSMutableArray array];
    [self addSubview:temView];
    for (int i = 0; i < searchArr.count; i ++) {
        
        UIView *mainView = [UIView new];
        [temView addSubview:mainView];
        
        UIView *searchView = [UIView new];
        searchView.layer.masksToBounds = YES;
        
        UIImageView *typeImageView = [UIImageView new];
        UIImage *typeImage = [UIImage imageNamed:[NSString stringWithFormat:@"ic_%@",[searchArr objectAtIndex:i]]];
        typeImageView.image = typeImage;
        
        UILabel *lblTitle = [UILabel new];
        lblTitle.font = [UIFont systemFontOfSize:12];
        lblTitle.text = [searchArr objectAtIndex:i];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        
        [mainView sd_addSubviews:@[searchView,typeImageView,lblTitle]];
        
        mainView.sd_layout
        .heightIs((DeviceWidth-40)/4);
        
        
        searchView.sd_layout
        .centerXEqualToView(mainView)
        .topSpaceToView(mainView,5)
        .heightIs(48)
        .widthIs(48);
        
        typeImageView.sd_layout
        .widthIs(typeImage.size.width)
        .heightIs(typeImage.size.height)
        .centerXEqualToView(searchView)
        .centerYEqualToView(searchView);
        
        lblTitle.sd_layout
        .topSpaceToView(searchView,2)
        .centerXEqualToView(mainView)
        .heightIs(14);
        [lblTitle setSingleLineAutoResizeWithMaxWidth:(DeviceWidth-40)/4];
        
        [tempArr addObject:mainView];
        
    }
    
    [temView setupAutoMarginFlowItems:tempArr withPerRowItemsCount:4 itemWidth:(DeviceWidth-40)/4 verticalMargin:0 verticalEdgeInset:0 horizontalEdgeInset:0];
    
    
    temView.sd_layout
    .topSpaceToView(self,0)
    .leftSpaceToView(self,20)
    .rightSpaceToView(self,20);
    
    UIView *hSpace = [UIView new];
    hSpace.backgroundColor = [UIColor colorWithHexString:@"C1C1C1"];
    [self addSubview:hSpace];
    
    hSpace.sd_layout
    .widthIs(DeviceWidth-50)
    .heightIs(0.5)
    .topSpaceToView(self,(DeviceWidth-40)/4)
    .leftSpaceToView(self,25);
    
    for (int i = 1; i < 4; i ++) {
        UIView *spaceLine = [UIView new];
        spaceLine.backgroundColor = [UIColor colorWithHexString:@"C1C1C1"];
        [self addSubview:spaceLine];
        
        spaceLine.sd_layout
        .topSpaceToView(self,15)
        .leftSpaceToView(self,20+(DeviceWidth-40)/4*i)
        .widthIs(0.5)
        .heightIs((DeviceWidth-40)/2-30);
    }
    
    UIView *spaceView = [UIView new];
    spaceView.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    [self addSubview:spaceView];
    
    spaceView.sd_layout
    .topSpaceToView(temView,10)
    .widthIs(DeviceWidth)
    .heightIs(10)
    .leftSpaceToView(self,0);
    

    
    [self setupAutoHeightWithBottomView:spaceView bottomMargin:0];
}

- (void)setLayerInview:(UIView *)view andType:(NSNumber *)type{
    
    CAGradientLayer *layer;
    switch ([type intValue]) {
        case 1 :
            layer = [self gradientBackgroundWithTopColor:[UIColor colorWithHexString:@"F0405F"] bottomColor:[UIColor colorWithHexString:@"F76560"]];
            break;
        case 2 :
            layer = [self gradientBackgroundWithTopColor:[UIColor colorWithHexString:@"FF8852"] bottomColor:[UIColor colorWithHexString:@"FEA326"]];
            break;
        case 3 :
            layer = [self gradientBackgroundWithTopColor:[UIColor colorWithHexString:@"7BB02C"] bottomColor:[UIColor colorWithHexString:@"8BCC3A"]];
            break;
        case 4 :
            layer = [self gradientBackgroundWithTopColor:[UIColor colorWithHexString:@"0BBBDD"] bottomColor:[UIColor colorWithHexString:@"08C1CD"]];
            break;
        case 5 :
            layer = [self gradientBackgroundWithTopColor:[UIColor colorWithHexString:@"2F88EC"] bottomColor:[UIColor colorWithHexString:@"1CACF2"]];
            break;
        case 6 :
            layer = [self gradientBackgroundWithTopColor:[UIColor colorWithHexString:@"5E60CE"] bottomColor:[UIColor colorWithHexString:@"4168C5"]];
            break;
        case 7 :
            layer = [self gradientBackgroundWithTopColor:[UIColor colorWithHexString:@"02A880"] bottomColor:[UIColor colorWithHexString:@"0DB993"]];
            break;
        case 8 :
            layer = [self gradientBackgroundWithTopColor:[UIColor colorWithHexString:@"C03F82"] bottomColor:[UIColor colorWithHexString:@"EA54A0"]];
            break;
        default:
            break;
    }

    layer.frame = view.bounds;
    
    [view.layer setSublayers:@[layer]];
}

- (CAGradientLayer *)gradientBackgroundWithTopColor:(UIColor *)topColor bottomColor:(UIColor *)bottomColor {
    NSArray *gradientColors    = @[(id) topColor.CGColor, (id) bottomColor.CGColor];
    NSArray *gradientLocations = @[@(0.0), @(1.0)];
    
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.colors    = gradientColors;
    gradientLayer.locations = gradientLocations;
    
    return gradientLayer;
}

@end
