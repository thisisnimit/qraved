//
//  V2_RestaurantCell.m
//  Qraved
//
//  Created by harry on 2017/6/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_RestaurantCell.h"
#import "WebServiceV2.h"
#import "GoFoodBadgeForViewedView.h"
@implementation V2_RestaurantCell
{
    GoFoodBadgeForViewedView *goFoodBadgeView;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lblName.textColor = [UIColor color333333];
    self.lblDistance.textColor = [UIColor color999999];
    self.lblPriceLevel.textColor = [UIColor color999999];
    self.starRatingControl = [[DLStarRatingControl alloc]initWithFrame:CGRectMake(0, 160, 73, 14) andStars:5 andStarWidth:13 isFractional:YES spaceWidth:2];
    self.starRatingControl.userInteractionEnabled = NO;
    [self.contentView addSubview:self.starRatingControl];
    
    goFoodBadgeView = [[GoFoodBadgeForViewedView alloc] init];
    goFoodBadgeView.hidden = YES;
    [self.restaurantImageView addSubview:goFoodBadgeView];
    
    goFoodBadgeView.sd_layout
    .bottomSpaceToView(self.restaurantImageView, 10)
    .leftSpaceToView(self.restaurantImageView, 10)
    .widthIs(155)
    .heightIs(18);
}


- (void)setModel:(IMGRestaurant *)model{
    _model = model;
    
//    __weak typeof(self.restaurantImageView) weakRestaurantImageView = self.restaurantImageView;
    UIImage *placeHoderImage =[UIImage imageNamed:DEFAULT_IMAGE_STRING];
    [self.restaurantImageView sd_setImageWithURL:[NSURL URLWithString:[model.logoImage returnCurrentImageString]] placeholderImage:placeHoderImage options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//        image = [image imageByScalingAndCroppingForSize:CGSizeMake(216, 135)];
//        [weakRestaurantImageView setImage:image];
    }];
    
    self.lblName.text = model.title;
    
    float score = [model.ratingScore floatValue];
    int scoreInt =(int)roundf(score);
    if (scoreInt/2.0-scoreInt/2>0) {
        [self.starRatingControl updateRating:[NSNumber numberWithFloat:scoreInt]];
        
    }else{
        [self.starRatingControl updateRating:[NSNumber numberWithFloat:scoreInt]];
    }
    
    NSString *distance = [model distance];
    if ([distance isEqualToString:@""]) {
        
        if ([model.ratingCount isEqual:@0]) {
            self.lblDistance.text = @"";
        }else{
            self.lblDistance.text = [NSString stringWithFormat:@"(%@)",model.ratingCount];
        }
        
    }else{
        if ([model.ratingCount isEqual:@0]) {
            self.lblDistance.text = [NSString stringWithFormat:@"%@",distance];
        }else{
            self.lblDistance.text = [NSString stringWithFormat:@"(%@) • %@",model.ratingCount,distance];
        }
        
    }
    
    if ([model.cuisineName isEqualToString:@""]) {
        self.lblPriceLevel.text = [NSString stringWithFormat:@"%@ • %@",model.districtName,model.priceName];
    }else{
        self.lblPriceLevel.text = [NSString stringWithFormat:@"%@ • %@ • %@",model.cuisineName,model.districtName,model.priceName];
    }
    //[self.lblPriceLevel sizeToFit];
    self.lblWellkonwnHeight.constant = 0;
    self.lblWellknownSpace.constant = 0;
    

    if ([Tools isBlankString:model.goFoodLink]) {
        goFoodBadgeView.hidden = YES;
        
        if ([model.brandLogo isEqual:[NSNull null]] || model.brandLogo==nil) {
            self.logoImageView.hidden = YES;
        }else{
            self.logoImageView.hidden = NO;
            [self.logoImageView sd_setImageWithURL:[NSURL URLWithString:[model.brandLogo returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]] placeholderImage:placeHoderImage completed:nil];
        }
    }else{
        goFoodBadgeView.hidden = NO;
        self.logoImageView.hidden = YES;
    }
    
    
    if ([model.saved isEqual:@1]) {
        [self.btnSaved setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
    }else{
        [self.btnSaved setImage:[UIImage imageNamed:@"ic_home_heart"] forState:UIControlStateNormal];
    }
    
}
- (void)freshSavedButton:(BOOL)saved{
    if (saved) {
        [self.btnSaved setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
    }else{
        [self.btnSaved setImage:[UIImage imageNamed:@"ic_home_heart"] forState:UIControlStateNormal];
    }
}

- (IBAction)savedClick:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(savedToList:IndexPath:)]) {
        [self.delegate savedToList:self.model IndexPath:self.currentIndex];
    }
}
@end
