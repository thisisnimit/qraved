//
//  V2_DiscoverView.m
//  Qraved
//
//  Created by harry on 2017/9/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverView.h"

@implementation V2_DiscoverView

- (instancetype)init{
    self = [super init];
    if(self){
        [self createUI];
    }
    return self;
}

//- (void)createUI{
//
//    self.backgroundColor = [UIColor whiteColor];
//
//    NSString *cityName = [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT];
//    NSString *showDelivery = [[NSUserDefaults standardUserDefaults] objectForKey:@"ShowDelivery"];
//
//    NSArray *imgArr;
//    NSArray *titleArr;
//    if (showDelivery != nil && [showDelivery isEqualToString:@"delivery"] && [[cityName capitalizedString] isEqualToString:CITY_JAKARTA]) {
//        imgArr = @[@"ic_homeResto", @"ic_homeCoffee", @"ic_homeSweets", @"ic_homeBars", @"ic_homeDelivery"];
//        titleArr = @[@"Restaurants",@"Coffee & Drinks", @"Sweets & Desserts", @"Bars", @"Delivery"];
//    }else{
//        imgArr = @[@"ic_homeResto", @"ic_homeCoffee", @"ic_homeSweets", @"ic_homeBars"];
//        titleArr = @[@"Restaurants",@"Coffee & Drinks", @"Sweets & Desserts", @"Bars"];
//    }
//    NSArray *tagIdArr = @[@0, @59, @61, @60];
//
//
//    for (int i = 0; i < imgArr.count; i ++) {
//        UIView *btnView = [UIView new];
//        [self addSubview:btnView];
//
//        [btnView bk_whenTapped:^{
//            if (i == 4) {
//                if (self.deliveryTapped) {
//                    self.deliveryTapped();
//                }
//            }else{
//                if (self.foodTagTapped) {
//                    self.foodTagTapped([tagIdArr objectAtIndex:i]);
//                }
//            }
//        }];
//
//        CGFloat btnWidth = (DeviceWidth - 20)/imgArr.count;
//
//        btnView.sd_layout
//        .topSpaceToView(self, 0)
//        .leftSpaceToView(self, 10 + btnWidth * i)
//        .widthIs(btnWidth)
//        .heightIs(120);
//
//        UIImageView *imgView = [UIImageView new];
//        imgView.image = [UIImage imageNamed:[imgArr objectAtIndex:i]];
//
//        UILabel *label = [UILabel new];
//        if (IS_IPHONE_5 || IS_IPHONE_4S) {
//            label.font = [UIFont systemFontOfSize:10];
//        }else{
//            label.font = [UIFont systemFontOfSize:11];
//        }
//        label.textColor = [UIColor color333333];
//        label.textAlignment = NSTextAlignmentCenter;
//        label.text = [titleArr objectAtIndex:i];
//        [btnView sd_addSubviews:@[imgView,label]];
//
//        imgView.sd_layout
//        .topSpaceToView(btnView, 20)
//        .centerXEqualToView(btnView)
//        .widthIs(45)
//        .heightIs(45);
//
//        label.sd_layout
//        .topSpaceToView(imgView, 10)
//        .leftSpaceToView(btnView, 0)
//        .widthIs(btnWidth)
//        .autoHeightRatio(0);
//
//        [label setMaxNumberOfLinesToShow:2];
//    }
//
//    UIView *hSpace = [UIView new];
//    hSpace.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
//    [self addSubview:hSpace];
//    hSpace.sd_layout
//    .bottomSpaceToView(self, 0)
//    .leftSpaceToView(self, 0)
//    .rightSpaceToView(self, 0)
//    .heightIs(5);
//}
- (void)createUI{
    
    self.backgroundColor = [UIColor whiteColor];
    
    UIView *disView = [UIView new];
    disView.layer.masksToBounds = YES;
    disView.layer.borderWidth = 1;
    disView.layer.borderColor = [UIColor colorWithHexString:@"EAEAEA"].CGColor;
    [self addSubview:disView];
    
    disView.sd_layout
    .topSpaceToView(self, 0)
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .heightIs(100);
    
    NSString *cityName = [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT];
    NSString *showDelivery = [[NSUserDefaults standardUserDefaults] objectForKey:@"ShowDelivery"];
    
    NSArray *imgArr;
    NSArray *titleArr;
    if (showDelivery != nil && [showDelivery isEqualToString:@"delivery"] && [[cityName capitalizedString] isEqualToString:CITY_JAKARTA]) {
        imgArr = @[@"ic_homeFood",@"ic_homeLocation", @"ic_homeDelivery"];
        titleArr = @[@"Food",@"Location", @"Delivery"];
    }else{
        imgArr = @[@"ic_homeFood",@"ic_homeLocation"];
        titleArr = @[@"Food",@"Location"];
    }
    
    
    
    for (int i = 0; i < imgArr.count; i ++) {
        UIView *btnView = [UIView new];
        [disView addSubview:btnView];
        
        UIView *lineView = [UIView new];
        lineView.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
        [disView addSubview:lineView];
        
        lineView.sd_layout
        .topSpaceToView(disView, 0)
        .leftSpaceToView(disView, DeviceWidth/imgArr.count * i)
        .widthIs(1)
        .heightIs(100);
        
        [btnView bk_whenTapped:^{
            if (i == 0) {
                if (self.discoverBy) {
                    self.discoverBy(NO);
                }
            }else if(i == 1) {
                if (self.discoverBy) {
                    self.discoverBy(YES);
                }
            }else{
                if (self.deliveryTapped) {
                    self.deliveryTapped();
                }
            }
        }];
        
        
        btnView.sd_layout
        .topSpaceToView(disView, 0)
        .leftSpaceToView(disView, DeviceWidth/imgArr.count * i)
        .widthIs(DeviceWidth/imgArr.count)
        .heightIs(100);
        
        UIImageView *imgView = [UIImageView new];
        imgView.image = [UIImage imageNamed:[imgArr objectAtIndex:i]];
        
        UILabel *label = [UILabel new];
        label.font = [UIFont systemFontOfSize:14];
        label.textColor = [UIColor color333333];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = [titleArr objectAtIndex:i];
        [btnView sd_addSubviews:@[imgView,label]];
        
        if (i == 0) {
            imgView.sd_layout
            .topSpaceToView(btnView, 20)
            .centerXEqualToView(btnView)
            .widthIs(21)
            .heightIs(30);
        }else if (i == 1){
            imgView.sd_layout
            .topSpaceToView(btnView, 20)
            .centerXEqualToView(btnView)
            .widthIs(24)
            .heightIs(30);
        }else{
            imgView.sd_layout
            .topSpaceToView(btnView, 20)
            .centerXEqualToView(btnView)
            .widthIs(41)
            .heightIs(30);
        }
        
        label.sd_layout
        .bottomSpaceToView(btnView, 20)
        .leftSpaceToView(btnView, 0)
        .widthIs(DeviceWidth/imgArr.count)
        .heightIs(18);
        
    }
}



@end
