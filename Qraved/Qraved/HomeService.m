  //
//  HomeService.m
//  Qraved
//
//  Created by harry on 2017/7/12.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "HomeService.h"
#import "V2_CityModel.h"
#import "V2_InstagramModel.h"
#import "V2_LocationModel.h"
#import "V2_QuickSearchModel.h"
#import "V2_PersonalizedModel.h"
#import "V2_GuideModel.h"
#import "V2_ArticleModel.h"
#import "V2_RestaurantModel.h"
#import "V2_DistrictModel.h"
#import "ContributionListModel.h"
#import "DiscoverCategories.h"
#import "InstagramFilterModel.h"
#import "DeliveryBannerModel.h"
#import "IMGMediaComment.h"
@implementation HomeService


static AFHTTPSessionManager *_manager;

+ (AFHTTPSessionManager *)manager {
    if (!_manager) {
        //https://apigw.qraved.com/id/
        _manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[[NSURL alloc] initWithString:QRAVED_WEB_SERVICE_SERVER]];
        _manager.responseSerializer = [AFJSONResponseSerializer serializer];
  
        _manager.requestSerializer = [AFHTTPRequestSerializer serializer];
        //[_manager.requestSerializer setTimeoutInterval:10.0];
        
        _manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        [_manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", @"text/plain", @"text/javascript", @"text/json",  nil];
        
        _manager.securityPolicy.allowInvalidCertificates = NO;
    }
    return _manager;
}


+ (void)getHomeDataWithParam:(NSDictionary *)params andBlock:(void(^)(id instagram,id location,id instagramImageArr,id locationImageArr))successBlock andError:(void(^)(NSError *error))errorBlock{
    
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        [paramDic setObject:user.userId forKey:@"userId"];
    }

    NSString *string = [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    if (string) {
        [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
    }else{
        [paramDic setObject:@"2" forKey:@"cityId"];
    }
    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    //id/home/api/Homepage/all
    [[HomeService manager] GET:@"id/home/api/v2/mobile/homepage"
                     parameters:paramDic
                       progress:nil
                        success:^(NSURLSessionDataTask *task, id responseObject) {
                            
                            if ([responseObject isKindOfClass:[NSDictionary class]]) {
//                                NSDictionary *dic = [responseObject objectForKey:@"data"];
                                NSMutableArray *instagramArr = [NSMutableArray array];
                                NSMutableArray *locationArr = [NSMutableArray array];
                                NSMutableArray *instagramImage = [NSMutableArray array];
                                NSMutableArray *locationImage = [NSMutableArray array];
                                for (NSDictionary *instagramDic in [responseObject objectForKey:@"instagram"]) {
                                    V2_InstagramModel *model = [[V2_InstagramModel alloc] init];
                                    [model setValuesForKeysWithDictionary:instagramDic];
                                    [instagramArr addObject:model];
                                    
                                    [instagramImage addObject:[instagramDic objectForKey:@"low_resolution_image"]];
                                }
                                
                                for (NSDictionary *locationDic in [responseObject objectForKey:@"location"]) {
                                    V2_LocationModel *model = [[V2_LocationModel alloc] init];
                                    [model setValuesForKeysWithDictionary:locationDic];
                                    [locationArr addObject:model];
                                    [locationImage addObject:[locationDic objectForKey:@"image_url"]];
                                }
                                
                                successBlock(instagramArr,locationArr,instagramImage,locationImage);
                            }
                            
                        }
                        failure:^(NSURLSessionDataTask *task, NSError *error) {
                            errorBlock(error);
                        }];
    
}
+(void)getInstagramPhotos:(NSDictionary *)params andBlock:(void(^)(id instagramPhotos))successBlock andError:(void(^)(NSError *error))errorBlock{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithDictionary:params];
    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    
    [[HomeService manager] GET:@"id/home/api/Instagram/photos"
                    parameters:paramDic
                      progress:nil
                       success:^(NSURLSessionDataTask *task, id responseObject) {
                           
                           if (responseObject != nil) {
                               [CommonMethod setJsonToFile:HOME_CACHE_INSTAGRAM andJson:responseObject];
                           }
                           
                           successBlock([CommonMethod getHomeInstagram:responseObject]);
                           
                       }
                       failure:^(NSURLSessionDataTask *task, NSError *error) {
                           
                       }];
    
    
}
+(void)getLocation:(NSDictionary *)params andIsPreferred:(BOOL)isPreferred andBlock:(void(^)(id locationArr))successBlock andError:(void(^)(NSError *error))errorBlock{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];

    [param setObject:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
    [param setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [param setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];

    //id/home/api/Location/area/district
    [[HomeService manager] GET:@"id/home/api/v2/mobile/locations"
                     parameters:param
                       progress:nil
                        success:^(NSURLSessionDataTask *task, id responseObject) {
                            
                            NSMutableArray *districtArr = [NSMutableArray array];
                            if ([responseObject isKindOfClass:[NSArray class]]) {
                                for (int i = 0;i < [responseObject count]; i ++) {
                                    NSDictionary *dic = [responseObject objectAtIndex:i];
                                    
                                    V2_DistrictModel *model = [[V2_DistrictModel alloc] init];
                                    model.name = [dic objectForKey:@"name"];
                                    model.districtId = [dic objectForKey:@"id"];
                                    NSMutableArray *locationArr = [NSMutableArray array];
                                    if (!isPreferred) {
                                        if ([[dic objectForKey:@"name"] isEqualToString:@"Nearby"]) {
                                            V2_AreaModel *model = [[V2_AreaModel alloc] init];
                                            model.name = @"My Location";
                                            model.selected = NO;
                                            
                                            [locationArr addObject:model];
                                        }else if ([[dic objectForKey:@"name"] isEqualToString:@"Popular Locations"]){
                                            
                                        }else{
                                            V2_AreaModel *model = [[V2_AreaModel alloc] init];
                                            model.name = @"All";
                                            model.selected = NO;
                                            model.districtId = [dic objectForKey:@"id"];
                                            [locationArr addObject:model];
                                        }
                                    }
                                    
                                    for (NSDictionary *locationDic in [dic objectForKey:@"locations"]) {
                                        V2_AreaModel *model = [[V2_AreaModel alloc] init];
                                        [model setValuesForKeysWithDictionary:locationDic];
                                        model.selected = NO;
                                        [locationArr addObject:model];
                                        
                                    }
                                    
                                    model.districts = locationArr;
                                    [districtArr addObject:model];
                                }
                            }
                            
                            
                            successBlock(districtArr);
                        }
     
                        failure:^(NSURLSessionDataTask *task, NSError *error) {
                            
                        }];
    
    
}

+(void)getContribution:(NSDictionary *)params andBlock:(void(^)(id contributionArr))successBlock andError:(void(^)(NSError *error))errorBlock{
//    NSDictionary *dic;
//    if ([IMGUser currentUser].userId != nil) {
//        dic = @{@"userId":[IMGUser currentUser].userId,@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]};
//    }
    
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithDictionary:params];
    if ([IMGUser currentUser].userId != nil) {
        [paramDic setObject:[IMGUser currentUser].userId forKey:@"userId"];
        [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
        [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    }
    
    NSString *deviceId =[[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [paramDic setObject:@"ios" forKey:@"os"];
    [paramDic setObject:@"mobileapp" forKey:@"app"];
    [paramDic setObject:QRAVED_APIKEY forKey:@"apiKey"];
    [paramDic setObject:QRAVED_WEB_SERVICE_VERSION forKey:@"versionName"];
    [paramDic setObject:QRAVED_VERSION forKey:@"versionCode"];
    [paramDic setObject:deviceId forKey:@"deviceId"];
    [paramDic setObject:QRAVED_WEB_SERVICE_CLIENT forKey:@"client"];
    [paramDic setObject:QRAVED_APIKEY forKey:@"appApiKey"];
    [paramDic setObject:QRAVED_WEB_SERVICE_VERSION forKey:@"v"];


    [[HomeService manager] GET:@"id/home/api/Contribution/all" parameters:paramDic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSMutableArray *contributionArray = [NSMutableArray array];
        for (NSDictionary *contributionDic in [[responseObject objectForKey:@"data"] objectForKey:@"data"]) {
            ContributionListModel *model = [[ContributionListModel alloc] init];
            [model setValuesForKeysWithDictionary:contributionDic];
            [contributionArray addObject:model];
        }
        
        successBlock(contributionArray);
        
        
        NSLog(@"-------------------");
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock(error);
    }];
}


+(void)deleteErrorImageUrlerrorCode:(NSInteger )errorCode imageUrl:(NSString *)imageUrl mediaId:(NSString *)mediaId photoId:(NSString *)photoId celebrityPhotoId:(NSString *)celebrityPhotoId  andBlock:(void(^)(id contributionArr))successBlock andError:(void(^)(NSError *error))errorBlock{


    if (errorCode == 404 || errorCode == -1100) {
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        // 设置超时时间
        manager.requestSerializer.timeoutInterval = 8.0f;
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", @"text/plain", @"text/javascript", @"text/json",  nil];
        
        
        [manager GET:imageUrl parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"%@",responseObject);
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            NSLog(@"%@",error);
            NSMutableDictionary * pamars = [NSMutableDictionary dictionary];
            if (error.code == -1011) {//@{@"mediaId":mediaId}
                if (![mediaId isEqualToString:@""]) {
                    
                    [pamars setObject:mediaId forKey:@"mediaId"];
                }else{
                    [pamars setObject:photoId forKey:@"photoId"];
                }
                [[IMGNetWork sharedManager] POST:@"instagramphoto/delete" parameters:pamars progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    
                    NSLog(@"%@",responseObject);
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    NSLog(@"%@",error);
                    
                }];

            }
            
        }];

    }

}


+(void)getCategories:(NSDictionary *)params andBlock:(void(^)(id locationArr))successBlock andError:(void(^)(NSError *error))errorBlock{
    
    ///v2/food_discovery
    [[HomeService manager] GET:@"id/home/api/v2/food_discovery" parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        
        NSMutableArray *districtArr = [NSMutableArray array];
        for (NSDictionary *dic in responseObject) {
            DiscoverCategories *discoverCategories = [[DiscoverCategories alloc] init];
            discoverCategories.name = dic[@"name"];
            discoverCategories.object_id = dic[@"object_id"];
            discoverCategories.myID = dic[@"id"];
            discoverCategories.order = dic[@"order"];
            discoverCategories.parent_object_id = dic[@"parent_object_id"];
            NSMutableArray *locationArr = [NSMutableArray array];
            DiscoverCategoriesModel *disModel = [[DiscoverCategoriesModel alloc] init];
            disModel.myID = dic[@"object_id"];
            disModel.name = @"All";
            [locationArr addObject:disModel];
            for (NSDictionary *chiDic in dic[@"childrens"]) {
                DiscoverCategoriesModel *disModel = [[DiscoverCategoriesModel alloc] init];
                disModel.myID = [chiDic objectForKey:@"object_id"];
                disModel.name = [chiDic objectForKey:@"name"];
                disModel.order = [chiDic objectForKey:@"order"];
                disModel.object_id = [chiDic objectForKey:@"id"];
                disModel.parent_object_id = [chiDic objectForKey:@"parent_object_id"];
                //[disModel setValuesForKeysWithDictionary:chiDic];
                [locationArr addObject:disModel];
            }
            discoverCategories.childrens = locationArr;
            [districtArr addObject:discoverCategories];
        }
        successBlock(districtArr);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock(error);
    }];
}

+(void)getFoodTag:(NSDictionary *)params andBlock:(void(^)(id locationArr))successBlock andError:(void(^)(NSError *error))errorBlock{
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:params];
    [dic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
    
    [dic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [dic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    
//    if (foodTagID != nil) {
//        [dic setObject:foodTagID forKey:@"footTagId"];
//    }
    
    [[HomeService manager] GET:@"id/home/api/v2/search/foodtag_suggestion" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        
        NSMutableArray *districtArr = [NSMutableArray array];
        for (NSDictionary *dic in responseObject) {
            DiscoverCategoriesModel *disModel = [[DiscoverCategoriesModel alloc] init];
            disModel.myID = [dic objectForKey:@"id"];
            disModel.name  = [dic objectForKey:@"name"];
            [districtArr addObject:disModel];
        }
        successBlock(districtArr);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock(error);
    }];
}

+(void)getQuicklySearchFoodTag:(NSDictionary *)params andBlock:(void(^)(id foodtagArr))successBlock andError:(void(^)(NSError *error))errorBlock{
    [[HomeService manager] GET:@"id/home/api/v2/mobile/foodtag/quick_search" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSMutableArray *foodTagArr = [NSMutableArray array];
        if ([responseObject isKindOfClass:[NSArray class]] && [responseObject count]>0) {
            for (NSDictionary *dic in responseObject) {
                DiscoverCategoriesModel *disModel = [[DiscoverCategoriesModel alloc] init];
                disModel.myID = [dic objectForKey:@"id"];
                disModel.name  = [dic objectForKey:@"name"];
                disModel.icon_image_url  = [dic objectForKey:@"icon_image_url"];
                [foodTagArr addObject:disModel];
            }
        }
        successBlock(foodTagArr);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock(error);
    }];
}

+(void)getRecentlyViewedRestaurant:(NSDictionary *)params andBlock:(void(^)(id restaurantArr))successBlock andError:(void(^)(NSError *error))errorBlock{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithDictionary:params];
    NSString *deviceId =[[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [paramDic setObject:@"ios" forKey:@"os"];
    [paramDic setObject:@"mobileapp" forKey:@"app"];
    //[paramDic setObject:QRAVED_APIKEY forKey:@"apiKey"];
    [paramDic setObject:QRAVED_WEB_SERVICE_VERSION forKey:@"versionName"];
    [paramDic setObject:QRAVED_VERSION forKey:@"versionCode"];
    [paramDic setObject:deviceId forKey:@"deviceId"];
    [paramDic setObject:@"0" forKey:@"districtId"];
    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    [paramDic setValue:@"0" forKey:@"offset"];
    [paramDic setValue:@"10" forKey:@"limit"];
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        [paramDic setObject:user.userId forKey:@"userId"];
    }
    NSString *string = [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
    [paramDic setValue:string forKey:@"cityId"];
    [[HomeService manager] GET:@"id/home/api/v2/mobile/restaurants/recently_viewed" parameters:paramDic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSMutableArray *restaurantArray = [NSMutableArray array];
        if ([responseObject isKindOfClass:[NSArray class]] && [responseObject count]>0) {
            for (NSDictionary *restaurantDic in responseObject) {
                IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
                restaurant.restaurantId = [restaurantDic objectForKey:@"id"];
                restaurant.logoImage = [restaurantDic objectForKey:@"image"];
                restaurant.title = [restaurantDic objectForKey:@"title"];
                restaurant.state = [restaurantDic objectForKey:@"state"];
                if ([[restaurantDic objectForKey:@"rating"] isEqual:[NSNull null]]) {
                    restaurant.ratingScore = @"0";
                }else{
                    restaurant.ratingScore = [restaurantDic objectForKey:@"rating"];
                }
                restaurant.goFoodLink = [restaurantDic objectForKey:@"gofoodLink"];
                restaurant.saved = [restaurantDic objectForKey:@"userSaved"];
                restaurant.priceName = [restaurantDic objectForKey:@"priceLevelDisplayName"];
                restaurant.longitude = [restaurantDic objectForKey:@"longitude"];
                restaurant.latitude = [restaurantDic objectForKey:@"latitude"];
                restaurant.districtName = [restaurantDic objectForKey:@"districtName"];
                if ([[restaurantDic objectForKey:@"ratingCount"] isEqual:[NSNull null]]) {
                    restaurant.ratingCount = @0;
                }else{
                    restaurant.ratingCount = [restaurantDic objectForKey:@"ratingCount"];
                }
                restaurant.priceLevel = [restaurantDic objectForKey:@"priceLevel"];
                restaurant.cuisineName = [restaurantDic objectForKey:@"primaryCuisineName"];
           
                restaurant.brandLogo = [restaurantDic objectForKey:@"brandImage"];
                
                [restaurantArray addObject:restaurant];
            }
        }
        successBlock(restaurantArray);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

+ (void)getInstagramFilter:(NSDictionary *)params andBlock:(void(^)(NSArray *filterArr))successBlock andError:(void(^)(NSError *error))errorBlock{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithDictionary:params];
    [paramDic setValue:@"BhaxC05eXMSwQ4HK" forKey:@"apiKey"];
    [paramDic setValue:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
    [paramDic setValue:@"false" forKey:@"exceptHashtag"];
    [paramDic setValue:@"20" forKey:@"limit"];

    [[HomeService manager] GET:@"id/reco/v1/instagram/trending/discovery/filters" parameters:paramDic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (responseObject != nil) {
            [CommonMethod setJsonToFile:HOME_CACHE_FILTER andJson:responseObject];
        }

        successBlock([CommonMethod getHomeInstagramFilter:responseObject]);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

    }];
    
}

+ (void)getInstagramHashtags:(NSDictionary *)params andBlock:(void(^)(NSArray *hashtagsArr))successBlock andError:(void(^)(NSError *error))errorBlock{
        NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithDictionary:params];
        [paramDic setValue:@"BhaxC05eXMSwQ4HK" forKey:@"apiKey"];
        [paramDic setValue:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
        [paramDic setValue:@"10" forKey:@"limit"];
    
        [[HomeService manager] GET:@"id/reco/v1/instagram/trending/discovery/hashtags" parameters:paramDic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if (responseObject != nil) {
                [CommonMethod setJsonToFile:HOME_CACHE_HASHTAG andJson:responseObject];
            }
            successBlock([CommonMethod getHomeInstagramFilter:responseObject]);
    
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
    
        }];
}

+ (void)getHomeLocation:(NSDictionary *)params andBlock:(void(^)(NSArray *locationArr))successBlock andError:(void(^)(NSError *error))errorBlock{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionaryWithDictionary:params];
    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [paramDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    [paramDic setValue:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
    [paramDic setValue:@"1" forKey:@"landmarkTypeId"];
    
    [[HomeService manager] GET:@"id/home/api/v2/mobile/locations/popular" parameters:paramDic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (responseObject != nil) {
            [CommonMethod setJsonToFile:HOME_CACHE_LOCATION andJson:responseObject];
        }
        successBlock([CommonMethod getHomeLocation:responseObject]);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

+ (void)getDeliveryFoodTagWithBlock:(void(^)(NSArray *tagArr))successBlock andError:(void(^)())errorBlock{
    [[HomeService manager] GET:@"id/search/api/foodtag/delivery" parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSMutableArray *districtArr = [NSMutableArray array];
        for (NSDictionary *dic in responseObject) {
            DiscoverCategoriesModel *disModel = [[DiscoverCategoriesModel alloc] init];
            disModel.myID = [dic objectForKey:@"id"];
            disModel.name  = [dic objectForKey:@"name"];
            [districtArr addObject:disModel];
        }
        successBlock(districtArr);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock();
    }];
}

+ (void)getDeliveryBannerWithBlock:(void(^)(NSArray *bannerArr))successBlock andError:(void(^)())errorBlock{
    [[HomeService manager] GET:@"id/home/api/v2/mobile/delivery/banners" parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (responseObject != nil) {
            [CommonMethod setJsonToFile:DELIVERY_BANNER_CACHE andJson:responseObject];
        }
        successBlock([CommonMethod getDeliveryBanner:responseObject]);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock();
    }];
}

+ (void)getDeliveryJournal:(NSDictionary *)params andBlock:(void(^)(NSArray *journalArr, NSString *journalTitle))successBlock andError:(void(^)())errorBlock{
    [[HomeService manager] GET:@"id/search/api/journal/delivery" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (responseObject != nil && [[params objectForKey:@"offset"] intValue] == 0) {
            [CommonMethod setJsonToFile:DELIVERY_CACHE andJson:responseObject];
        }
        
        successBlock([CommonMethod getDeliveryJournalList:responseObject], [responseObject objectForKey:@"page_title"]);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock();
    }];
}

@end
