//
//  V2_InstagramViewController.m
//  Qraved
//
//  Created by harry on 2017/6/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_InstagramViewController.h"
#import "V2_HomeHandler.h"
#import "V2_InstagramModel.h"
#import "LoadingView.h"
#import "InstagramDetailController.h"
#import "HomeService.h"
#import "V2_PhotoDetailViewController.h"
#import "V2_DistrictView.h"
#import "V2_DistrictModel.h"
#import "InstagramFilterView.h"
@interface V2_InstagramViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, AndroidRefreshDelegate,UIAlertViewDelegate>{
    UICollectionView *instagramCollectionView;
    int offset;
    UIButton *cityButton;
    V2_DistrictView *districtView;
    NSString *currentDistrict;
    NSString *currentLandmark;
    NSString *currentArea;
    BOOL isRefresh;
    BOOL isCheckLocation;
    InstagramFilterView *filterView;
    BOOL updateFilter;
    NSMutableArray *filterArray;
    BOOL isHaveLoadMore;
}
@property (nonatomic, strong) NSMutableArray *districtArr;
@property (nonatomic, strong) NSMutableArray *instagramArr;
@end

@implementation V2_InstagramViewController

CGFloat threshold = 0.7;
CGFloat itemPerPage = 10;
CGFloat currentPage = 0;

- (void)viewDidLoad {
    [super viewDidLoad];

    [self loadData];
    [self handleFilterArr];
    [self loadMianUI];
    [self requestData];
    [self addBackBtn];
    [self loadNavbutton];
}

- (void)handleFilterArr{
    if (self.selectModel != nil) {
        self.selectModel.isSelect = YES;
        [filterArray addObject:self.selectModel];
    }
    
    [HomeService getInstagramFilter:nil andBlock:^(NSArray *filterArr) {
        
        for (InstagramFilterModel *model in filterArr) {
            if ([model.type isEqualToString:self.selectModel.type]) {
                if ([model.type isEqualToString:@"hashtag"] && [model.tag isEqualToString:self.selectModel.tag]) {
                }else if ([model.type isEqualToString:@"foodtag"] && [model.tagId isEqual:self.selectModel.tagId]){
                }else if ([model.type isEqualToString:@"user"] && [model.type isEqualToString:self.selectModel.type] && [model.username isEqualToString:self.selectModel.username]){

                }else if ([model.type isEqualToString:@"location"] && [model.locationTypeName isEqualToString:self.selectModel.locationTypeName] && [model.locationId isEqual:self.selectModel.locationId]){
                }else{
                    [filterArray addObject:model];
                }
            }else{
                [filterArray addObject:model];
            }
        }
        updateFilter = YES;
        [instagramCollectionView reloadData];
        
    } andError:^(NSError *error) {
        
    }];

}

#pragma mark - AndroidRefreshDelegate

- (BOOL)androidRefreshView:(ARAndroidRefreshView *)refreshView shouldRefreshView:(UIView *)targetView{

    if (instagramCollectionView.contentOffset.y >0) {
        return NO
        ;
    }else{
        
        return YES;
    }
    
}
- (void)androidRefreshView:(ARAndroidRefreshView *)refreshView beginRefreshingView:(UIView *)targetView {
    
    [self downRefreshData];
    [self.view.ar_headerView finishRefreshing];
  
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    NSLog(@"========偏移量:%f",instagramCollectionView.contentOffset.y);
    if (instagramCollectionView.contentOffset.y <= 0) {
        instagramCollectionView.bounces = NO;
        
//        NSLog(@"jinzhixiala");
    }
    else
        if (instagramCollectionView.contentOffset.y > 0){
            instagramCollectionView.bounces = YES;
//            NSLog(@"allow shangla");
            
        }
}


- (void)downRefreshData{
    isRefresh = YES;
    offset=0;
    self.instagramArr = [NSMutableArray array];
    self.districtArr = [NSMutableArray array];
   [self getInstagramList];
}
- (void)loadNavbutton{
    cityButton = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if (self.isMall) {
        [cityButton setTitle:self.model.name forState:UIControlStateNormal];
    }else{
        if ([[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT] != nil ) {
            [cityButton setTitle:[[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT] capitalizedString] forState:UIControlStateNormal];
        }
        
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"currentCityId"] != nil && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied){
            [cityButton setTitle:NEARBYTITLE forState:UIControlStateNormal];
        }
    }
    
    [cityButton setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    if (!self.isMall) {
        [cityButton setImage:[UIImage imageNamed:@"ic_city_down"] forState:UIControlStateNormal];
        [cityButton setImage:[UIImage imageNamed:@"ic_city_up"] forState:UIControlStateSelected];
        cityButton.enabled  = NO;
        [cityButton addTarget:self action:@selector(doSelectCity:) forControlEvents:UIControlEventTouchUpInside];
        cityButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 15);
        [cityButton.layer setAffineTransform:CGAffineTransformMakeScale(-1, 1)];
        [cityButton.titleLabel.layer setAffineTransform:CGAffineTransformMakeScale(-1, 1)];
    }
   cityButton.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    cityButton.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    
    [cityButton sizeToFit];
    cityButton.height = 35;
    cityButton.width+=50;
    cityButton.layer.masksToBounds = YES;
    cityButton.layer.cornerRadius = 18;
    
    cityButton.x = (self.view.width - cityButton.width) / 2;
    cityButton.y = 4;
}
- (void)doSelectCity:(UIButton *)btn{
    //btn.selected = !btn.selected;
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setValue:cityButton.titleLabel.text forKey:@"City Selection"];
    [param setValue:@"Instagram Photo Viewer" forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL – City Selection" withEventProperties:param];
    
    if (self.districtArr.count>0) {
        [self createDistrictView];
    }else{
         [self getDistrictData];
    }
   
}

- (void)filterByArea:(V2_AreaModel *)model{
    [self resetSelectArea];
    if ([model.type isEqualToString:@"landmark"]) {
        currentLandmark = [NSString stringWithFormat:@"%@",model.districtId];
    }else{
        currentDistrict = [NSString stringWithFormat:@"%@",model.districtId];
    }
    if (self.selectModel != nil && [self.selectModel.type isEqualToString:@"location"]) {
        self.selectModel.isSelect = NO;
        self.selectModel = nil;
        updateFilter = YES;
    }
    
    [self refreshInstagram:model.name];
}

- (void)resetSelectArea{
    currentLandmark = @"";
    currentDistrict = @"";
    currentArea = @"";
}

- (void)filterByDistrict:(V2_DistrictModel *)model{
    [self resetSelectArea];
    currentArea = [NSString stringWithFormat:@"%@",model.districtId];
    [self refreshInstagram:model.name];
}

- (void)refreshInstagram:(NSString *)name{
    
    if (!self.isMall) {
        [cityButton setTitle:[name capitalizedString] forState:UIControlStateNormal];
        CGSize titleSize = [cityButton.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17]}];
        cityButton.height = 35;
        cityButton.width=titleSize.width+50;
        cityButton.y = 4;
        cityButton.x = (self.view.width - cityButton.width) / 2;
    }
    
    offset=0;
    isRefresh = YES;
    [self getInstagramList];
}

- (void)loadData{
    isRefresh = NO;
    offset=0;
    isHaveLoadMore = NO;
//    updateFilter = YES;
    self.instagramArr = [NSMutableArray array];
    self.districtArr = [NSMutableArray array];
    filterArray = [NSMutableArray array];
}
- (void)loadMianUI{
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    CGFloat width = (DeviceWidth-2)/3;
    layout.itemSize = CGSizeMake(width, width);
    layout.minimumLineSpacing = 1;
    layout.minimumInteritemSpacing = 1;
    
    instagramCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,0, DeviceWidth, DeviceHeight-44) collectionViewLayout:layout];
    instagramCollectionView.backgroundColor = [UIColor whiteColor];
    instagramCollectionView.delegate = self;
    instagramCollectionView.dataSource = self;
    instagramCollectionView.showsHorizontalScrollIndicator = NO;
    instagramCollectionView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:instagramCollectionView];

    instagramCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    
    [instagramCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"instagramCell"];
    [instagramCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"instagramHeader"];
    
    [self.view ar_addAndroidRefreshWithDelegate:self];
    self.view.ar_headerView.refreshOffset = 64.0f;

}

- (void)loadMoreData{
    [self getInstagramList];
}
- (void)requestData{
   
    //[self getDistrictData];
    
    [self getInstagramList];
}

- (void)getInstagramList{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];

    [param setValue:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
    [param setValue:@"21" forKey:@"limit"];
    [param setValue:[NSString stringWithFormat:@"%d",offset] forKey:@"offset"];
    
    if (self.isMall) {
        [param setValue:self.model.brandId forKey:@"landmarkIds"];
    }else{
        [param setValue:self.model.brandId forKey:@"brandIds"];
    }
  
    if (currentDistrict != nil &&![currentDistrict isEqualToString:@""]) {
        [param setValue:currentDistrict forKey:@"districtIds"];
    }
    
    if (currentLandmark != nil && ![currentLandmark isEqualToString:@""]) {
        [param setValue:currentLandmark forKey:@"landmarkIds"];
    }
    
    if (currentArea != nil && ![currentArea isEqualToString:@""]) {
        [param setValue:currentArea forKey:@"areaIds"];
    }
    
    if (isRefresh) {
        [param setObject:@"true" forKey:@"shuffle"];
    }
    
    if (self.model != nil) {
        [param setValue:self.model.city_id forKey:@"cityId"];
    }
    
    if (self.selectModel != nil && [self.selectModel.type isEqualToString:@"location"]) {
        if ([self.selectModel.locationTypeName isEqualToString:@"district"]) {
            [param setObject:self.selectModel.locationId forKey:@"districtId"];
        }else{
            [param setObject:self.selectModel.locationId forKey:@"landmarkIds"];
        }
    }
    
    [param setObject:self.selectModel.tag?self.selectModel.tag:@"" forKey:@"hashtag"];
    [param setObject:self.selectModel.username?self.selectModel.username:@"" forKey:@"instagramUsername"];
    [param setObject:self.selectModel.tagId?self.selectModel.tagId:@"" forKey:@"foodTagId"];
    
    [[LoadingView sharedLoadingView] startLoading];
    [HomeService getInstagramPhotos:param andBlock:^(id instagramPhotos) {
        cityButton.enabled = YES;
   
        if (offset==0) {
            [self.instagramArr removeAllObjects];
        }
        [instagramCollectionView.mj_footer endRefreshing];
//         [instagramCollectionView.mj_header endRefreshing];
        [self.view.ar_headerView finishRefreshing];
        offset += 21;
        isRefresh = NO;
        if ([instagramPhotos count] == 0) {
            isHaveLoadMore = NO;
            instagramCollectionView.mj_footer.hidden = YES;
        }else{
            isHaveLoadMore = YES;
            instagramCollectionView.mj_footer.hidden = NO;
        }
        
        if ([instagramPhotos isKindOfClass:[NSArray class]]) {
            [self.instagramArr addObjectsFromArray:instagramPhotos];
        }
        [UIView performWithoutAnimation:^{
            [instagramCollectionView reloadData];
        }];
       

        [[LoadingView sharedLoadingView] stopLoading];
        
    } andError:^(NSError *error) {
        cityButton.enabled = NO;
        isRefresh = NO;
        [instagramCollectionView.mj_footer endRefreshing];
        [[LoadingView sharedLoadingView] stopLoading];
//         [instagramCollectionView.mj_header endRefreshing];
         [self.view.ar_headerView finishRefreshing];
    }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (self.instagramArr) {
        return self.instagramArr.count;
    }
    return 0;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"instagramCell" forIndexPath:indexPath];
    for (UIView *subView in cell.contentView.subviews) {
        [subView removeFromSuperview];
    }
    if (self.instagramArr.count>0) {
        V2_InstagramModel *model = [self.instagramArr objectAtIndex:indexPath.row];
//        NSLog(@"%@",model.instagram_media_id);
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.contentView.bounds];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        [imageView sd_setImageWithURL:[NSURL URLWithString:model.low_resolution_image] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
   
            //image animation
            SDWebImageManager *manager = [SDWebImageManager sharedManager];//1587683825038077646_31462947
            if ([manager diskImageExistsForURL:imageURL]) {
//                NSLog(@"dont loading animation");
            }else {
                if (image) {
                    imageView.alpha = 0.0;
                    [UIView transitionWithView:imageView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                        imageView.image = image;
                        imageView.alpha = 1.0;
                    } completion:NULL];
                }
            }

            
            NSLog(@"%@====%ld", error, (long)error.code);
            [HomeService deleteErrorImageUrlerrorCode:error.code imageUrl:model.low_resolution_image mediaId:[NSString stringWithFormat:@"%@",model.instagram_media_id] photoId:@"" celebrityPhotoId:@"" andBlock:^(id contributionArr) {
                
                
            } andError:^(NSError *error) {
                
                
            }];
            
        }];
        [cell.contentView addSubview:imageView];

    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == self.instagramArr.count - 9 && isHaveLoadMore){
        [self loadMoreData];
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    UICollectionReusableView *reusableView = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"instagramHeader" forIndexPath:indexPath];
        if (updateFilter) {
            
            UIView *spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 5)];
            spaceView.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
            [header addSubview:spaceView];
            
            filterView = [[InstagramFilterView alloc] init];
            filterView.filterArr = filterArray;
            if (filterArray.count) {
                filterView.frame = CGRectMake(0, 18, DeviceWidth, 120);
            }
            __weak typeof(self) weakSelf = self;
            filterView.chooseFilter = ^(InstagramFilterModel *filterModel) {

                [weakSelf filterTapped:filterModel];
            };
            updateFilter = NO;
            [header addSubview:filterView];
        }
        reusableView = header;
    }
    return reusableView;
}

- (void)filterTapped:(InstagramFilterModel *)model{
    if (model.isSelect) {
        self.selectModel = model;
    }else{
        self.selectModel = nil;
        
    }
    if ([model.type isEqualToString:@"location"]) {
        [self resetSelectArea];
        [self resetDistrict];
        [self refreshInstagram:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT]];
    }else{
        offset=0;
        isRefresh = YES;
        [self getInstagramList];
    }
    
}

- (void)resetDistrict{
    for (int i = 0; i < self.districtArr.count; i ++) {
        V2_DistrictModel *districtModel = [self.districtArr objectAtIndex:i];
        for (V2_AreaModel *model in districtModel.districts) {
            model.selected = NO;
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (filterArray.count>0) {
        return CGSizeMake(DeviceWidth, 138);
    }else{
        return CGSizeMake(DeviceWidth, 0);
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    V2_InstagramModel *model = [self.instagramArr objectAtIndex:indexPath.row];
    
    V2_PhotoDetailViewController *photoDetail = [[V2_PhotoDetailViewController alloc] init];
    photoDetail.currentInstagram = model;
    photoDetail.photoArray = self.instagramArr;
    photoDetail.currentIndex = indexPath.row;
    [self.navigationController pushViewController:photoDetail animated:YES];

}

- (void)getDistrictData{
    //[[LoadingView sharedLoadingView] startLoading];
    [HomeService getLocation:nil andIsPreferred:NO andBlock:^(id locationArr) {
        
        [self.districtArr addObjectsFromArray:locationArr];
        
        [self createDistrictView];
        //[[LoadingView sharedLoadingView] stopLoading];
        
    } andError:^(NSError *error) {
        //[[LoadingView sharedLoadingView] stopLoading];
    }];
    
}

- (void)createDistrictView{
    if (districtView) {
        [districtView removeFromSuperview];
    }
    districtView = [[V2_DistrictView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight+20)];
    districtView.districtArr = self.districtArr;
    __weak typeof(self) weakSelf = self;
    districtView.selectedDistrict = ^(id model){
        if ([model isKindOfClass:[V2_AreaModel class]]) {
            [weakSelf filterByArea:model];
        }else if ([model isKindOfClass:[V2_DistrictModel class]]){
            [weakSelf filterByDistrict:model];
        }
        
    };
    districtView.nearbyCheck = ^{
        if ([CLLocationManager authorizationStatus] ==kCLAuthorizationStatusDenied) {
            isCheckLocation = YES;
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"To continue, let your device turn on location" delegate:weakSelf cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
            [alert show];
            
        }else{
            [weakSelf refreshInstagram:NEARBYTITLE];
        }
        
    };
    
    [[AppDelegate ShareApp].window addSubview:districtView];

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController.navigationBar addSubview:cityButton];
    [self.navigationController.navigationBar layoutIfNeeded];
    
    if (isCheckLocation && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
        [self refreshInstagram:NEARBYTITLE];
    }
    
    //[self getDistrictData];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    
    [cityButton removeFromSuperview];
     [[LoadingView sharedLoadingView] stopLoading];
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
    self.selectModel.isSelect = NO;
}


@end
