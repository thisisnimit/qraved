//
//  V2_CityModel.m
//  Qraved
//
//  Created by harry on 2017/6/14.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_CityModel.h"

@implementation V2_CityModel

- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
    if ([key isEqualToString:@"id"]) {
        self.cityId = value;
    }
}

@end
