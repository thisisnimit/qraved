//
//  BookHandler.m
//  Qraved
//
//  Created by apple on 17/4/10.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "BookHandler.h"
#import "LoadingView.h"

@implementation BookHandler
+(void)getMonthTimeWithParams:(NSDictionary*)params andBlock:(void(^)(NSArray *returnArray))block{

    [[IMGNetWork sharedManager] GET:@"app/month/times/v3" parameters:params progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSArray *returnArray = [responseObject objectForKey:@"result"];
        block(returnArray);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        
    }];

}
+(void)getOfferDateWithParams:(NSDictionary*)params andBlock:(void(^)(NSArray *thisMonthArr,NSArray *nextMonthArry))block{
    [[IMGNetWork sharedManager]POST:@"date/bookable" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSLog(@"responseObject = %@",responseObject);
        NSArray* currentMonthArr = [responseObject objectForKey:@"thisMonth"];
       NSArray*  nextMonthArr = [responseObject objectForKey:@"nextmonth"];
         block(currentMonthArr,nextMonthArr);
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"getCalendar error: %@",error.localizedDescription);
     }];

}
+(void)getDayAndTimesWithParams:(NSDictionary*)params andBlock:(void(^)(NSArray *returnArray))block andErroBlock:(void(^)())erroblock{
    [[IMGNetWork sharedManager] GET:@"day/times" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSString *exceptionMessage = [responseObject objectForKey:@"exceptionmsg"];
        if(exceptionMessage!=nil){
            NSLog(@"dateSelected erors when get datas, exceptionmsg is:%@",exceptionMessage);
            [[LoadingView sharedLoadingView]stopLoading];
            return;
        }
        
        NSArray *responseArray = [responseObject objectForKey:@"return"];
        block(responseArray);
    }failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"AFHTTPRequestOperation erors when get datas:%@",error.localizedDescription);
        erroblock();
    }];
}
+(void)confirmBookingInfomationWithParams:(NSDictionary*)params andBlock:(void(^)(id dic))block{
    NSDictionary *dic;
    if ([params objectForKey:@"offId"]) {
        dic= @{@"offer":@"withOffer"};
    }else{
        dic = @{@"offer":@"withoutOffer"};
    }
    [[IMGNetWork sharedManager]POST:@"book" parameters:params  progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        
        NSString *exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
            return;
        }
        
        block(responseObject);
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"BookingInfoViewController.requestBook.error: %@",error.localizedDescription);
        [[Amplitude instance] logEvent:@"CL - Book Now Failed" withEventProperties:dic];
        
        [[LoadingView sharedLoadingView]stopLoading];
        //       UIAlertView *alert=[[UIAlertView alloc]initWithTitle:ALERT_TITLE message:[NSString stringWithFormat:@"%@",error.localizedDescription]];
        //        [alert addButtonWithTitle:@"OK" handler:^(){
        //
        //        }];
        //        [alert show];
        UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:ALERT_TITLE message:[NSString stringWithFormat:@"%@",error.localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alerView show];
        
        
        
    }];


}
+(void)judgeVoucherCodeWithParams:(NSDictionary*)params andBlock:(void(^)(NSString *status,NSString *exceptionType))block andErrorBlock:(void(^)(NSString *errorStr))errorBlock{
    [[IMGNetWork sharedManager] POST:@"restaurant/voucher/validate" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSString *status=[responseObject objectForKey:@"status"];
        NSString *exceptionType=[responseObject objectForKey:@"exceptiontype"];
        block(status,exceptionType);
    
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"**********%@",error.localizedDescription);
        errorBlock(error.localizedDescription);
        [[LoadingView sharedLoadingView] stopLoading];
    }];

}
@end
