//
//  V2_RestaurantView.h
//  Qraved
//
//  Created by harry on 2017/6/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface V2_RestaurantView : UIView

@property (nonatomic, copy) NSArray *restaurantArray;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *componentId;
@property (nonatomic, copy) void(^gotoRestaurantDetail)(IMGRestaurant *restaurant);
@property (nonatomic, copy) void(^savedTapped)(IMGRestaurant *restaurant);
@property (nonatomic, copy) void(^seeAllRestaurant)(NSNumber *componentId);

- (void)freshSavedButton:(BOOL)liked;
@end
