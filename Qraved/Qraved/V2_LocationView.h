//
//  V2_LocationView.h
//  Qraved
//
//  Created by harry on 2017/6/16.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_LocationModel.h"
@interface V2_LocationView : UIView

@property (nonatomic, copy) NSArray *locationArr;
@property (nonatomic, copy) void(^gotoLocationList)();
@property (nonatomic, copy) void(^nearBy)();
@property (nonatomic, copy) void(^gotoSRPbyLocation)(V2_LocationModel *model);
@end
