//
//  V2_GuideCell.h
//  Qraved
//
//  Created by harry on 2017/6/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_GuideModel.h"
#import "IMGDiningGuide.h"
#import "IMGGuideModel.h"
@interface V2_GuideCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *guideImageView;
@property (weak, nonatomic) IBOutlet UILabel *guideName;
@property (weak, nonatomic) IBOutlet UILabel *guideIden;

@property (copy, nonatomic) IMGDiningGuide *model;
@property (copy, nonatomic) IMGGuideModel *guideModel;

@property (copy, nonatomic) IMGDiningGuide *diningGuideModel;

@end
