//
//  SearchCategryHandler.m
//  Qraved
//
//  Created by josn.liu on 2017/1/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "SearchCategryHandler.h"
#import "JAKARTANetWork.h"
#import "IMGCuisine.h"
#import "IMGLandMark.h"
#import "IMGDistrict.h"
#import "IMGDiningGuide.h"
#import "IMGTag.h"
@implementation SearchCategryHandler
+(void)getSearchAllCategryDataFromeService:(NSDictionary*)dic andSuccessBlock:(void(^)(NSArray* cusinearr,NSArray* landmark,NSArray* district,NSArray* dininguide))successBlock anderrorBlock:(void(^)( NSString *str))errorBlock{


    [[JAKARTANetWork sharedManager] GET:@"api/searchServiceRecommendation/getDataDefaultPage" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *cuisineArr=[[NSMutableArray alloc]init];
        NSMutableArray *diningArr=[[NSMutableArray alloc]init];
        NSMutableArray *mallArr=[[NSMutableArray alloc]init];
        NSMutableArray *districtArr=[[NSMutableArray alloc]init];
        NSDictionary *dataDic=[responseObject objectForKey:@"data"];
        NSArray *cuisines=[dataDic objectForKey:@"cuisine"];
        for (NSDictionary *dic in cuisines) {
            IMGCuisine *cuisne=[[IMGCuisine alloc]init];
            cuisne.cuisineId=[dic objectForKey:@"id"];
            cuisne.imageUrl=[dic objectForKey:@"image_url"];
            cuisne.name=[dic objectForKey:@"name"];
            cuisne.restaurantCount=[dic objectForKey:@"val"];
            [cuisineArr addObject:cuisne];
        }
        
        NSArray *malls=[dataDic objectForKey:@"mall"];
        for (NSDictionary *dic in malls) {
            IMGTag * landmark=[[IMGTag alloc]init];
            landmark.tagId=[dic objectForKey:@"id"];
            landmark.name=[dic objectForKey:@"name"];
            landmark.restaurantCount=[dic objectForKey:@"val"];
            landmark.imageUrl=[dic objectForKey:@"image_url"];
            [mallArr addObject:landmark];
        }
        
        NSArray *districts=[dataDic objectForKey:@"district"];
        for (NSDictionary *dic in districts) {
             IMGDistrict* district=[[IMGDistrict alloc]init];
            district.districtId=[dic objectForKey:@"id"];
            district.name=[dic objectForKey:@"name"];
            district.restaurantCount=[dic objectForKey:@"val"];
            district.imageUrl=[dic objectForKey:@"image_url"];
            [districtArr addObject:district];
        }

        NSArray *diningGuides=[dataDic objectForKey:@"diningGuide"];
        for (NSDictionary *dic in diningGuides) {
            IMGDiningGuide* district=[[IMGDiningGuide alloc]init];
            district.diningGuideId=[dic objectForKey:@"id"];
            district.name=[dic objectForKey:@"name"];
            district.restaurantCount=[dic objectForKey:@"val"];
            district.headerImage=[dic objectForKey:@"image_url"];
            district.cityId=[dic objectForKey:@"city_id"];
            district.pageUrl=[dic objectForKey:@"page_url"];
            district.pageContent=[dic objectForKey:@"page_content"];
            [diningArr addObject:district];
        }

        
        successBlock(cuisineArr,mallArr,districtArr,diningArr);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock(error.description);
    }];


}

@end
