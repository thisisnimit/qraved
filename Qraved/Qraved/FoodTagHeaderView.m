//
//  FoodTagHeaderView.m
//  Qraved
//
//  Created by harry on 2017/11/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "FoodTagHeaderView.h"
#import "FoodTagCollectionViewCell.h"
@interface FoodTagHeaderView ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    UICollectionView *foodTagCollectionView;
}

@end

@implementation FoodTagHeaderView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    CGFloat space = (DeviceWidth-30-70*4)/3;
    layout.itemSize = CGSizeMake(68, 90);
    layout.minimumInteritemSpacing = space;
    layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 15);
    
    foodTagCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,0, DeviceWidth, 90) collectionViewLayout:layout];
    foodTagCollectionView.backgroundColor = [UIColor whiteColor];
    foodTagCollectionView.delegate = self;
    foodTagCollectionView.dataSource = self;
    foodTagCollectionView.showsHorizontalScrollIndicator = NO;
    [self addSubview:foodTagCollectionView];
    
    [foodTagCollectionView registerClass:[FoodTagCollectionViewCell class] forCellWithReuseIdentifier:@"foodTagCell"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.foodTagArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    FoodTagCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"foodTagCell" forIndexPath:indexPath];
    DiscoverCategoriesModel *model = [self.foodTagArr objectAtIndex:indexPath.item];
    cell.model = model;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    DiscoverCategoriesModel *model = [self.foodTagArr objectAtIndex:indexPath.item];
    if (self.quicklySearchFoodTag) {
        self.quicklySearchFoodTag(model);
    }
}
- (void)setFoodTagArr:(NSArray *)foodTagArr{
    _foodTagArr = foodTagArr;
    
    int lines = (int)foodTagArr.count/4;
    if (foodTagArr.count%4 >0) {
        lines = (int)foodTagArr.count/4 +1;
    }
    foodTagCollectionView.frame = CGRectMake(0,0, DeviceWidth, 100*lines);
    
    UIView *spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, foodTagCollectionView.endPointY+11, DeviceWidth, 5)];
    spaceView.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    [self addSubview:spaceView];
    
    [foodTagCollectionView reloadData];
    
}


@end
