//
//  V2_CItyViewController.h
//  Qraved
//
//  Created by harry on 2017/8/17.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "IMGCity.h"
@interface V2_CItyViewController : BaseViewController


@property (nonatomic, strong) NSArray *cityArray;
@property (nonatomic, copy) void (^citySelected)(IMGCity *city);
@property (nonatomic, copy) void (^nearbySelected)();

@end
