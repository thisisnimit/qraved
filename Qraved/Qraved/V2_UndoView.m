//
//  V2_UndoView.m
//  Qraved
//
//  Created by harry on 2017/7/14.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_UndoView.h"

@implementation V2_UndoView

- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title andButtonTitle:(NSString *)buttonTitle andTarget:(id)target andSelector:(SEL)selector{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIView *selectView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 45)];
        
        selectView.backgroundColor = [UIColor whiteColor];
        [self addSubview:selectView];
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 1)];
        lineView.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:248/255.0 alpha:1];
        [selectView addSubview:lineView];
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, 12, 200, 20)];
        lblTitle.font = [UIFont systemFontOfSize:14];
        lblTitle.textColor = [UIColor grayColor];
        lblTitle.text = title;
        [selectView addSubview:lblTitle];
        
        UIButton *btnSelect = [UIButton buttonWithType:UIButtonTypeCustom];
        btnSelect.frame = CGRectMake(DeviceWidth-LEFTLEFTSET-60, 0, 60, 45);
        btnSelect.titleLabel.font = [UIFont systemFontOfSize:15];
        [btnSelect setTitle:buttonTitle forState:UIControlStateNormal];
        [btnSelect setTitleColor:[UIColor barButtonTitleColor] forState:UIControlStateNormal];
        [btnSelect addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
        [selectView addSubview:btnSelect];
    }
    
    return self;
}

@end
