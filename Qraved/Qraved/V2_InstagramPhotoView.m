//
//  QRInstagramPhotoView.m
//  Qraved-2
//
//  Created by Tek Yin on 5/15/17.
//  Copyright © 2017 Qraved. All rights reserved.
//

#import <AFNetworking/UIKit+AFNetworking.h>
#import "V2_InstagramPhotoView.h"
#import "UIImage+Colorfy.h"

@implementation V2_InstagramPhotoView


- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self construct];
    }

    return self;
}


- (void)construct {
    self.uImage             = [[UIImageView alloc] init];
    //self.uImage.frame       = self.bounds;
    //self.uImage.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:self.uImage];
    
    self.uImage.sd_layout
    .topSpaceToView(self,146)
    .leftSpaceToView(self,0)
    .rightSpaceToView(self,0)
    .heightEqualToWidth(0);

//    self.delegate = self;

//    UITapGestureRecognizer *twoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTwice:)];
//    twoTap.cancelsTouchesInView    = NO;
//    twoTap.numberOfTapsRequired    = 2;
//    twoTap.numberOfTouchesRequired = 1;
//
//    UITapGestureRecognizer *oneTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnce:)];
//    oneTap.cancelsTouchesInView    = NO;
//    oneTap.numberOfTapsRequired    = 1;
//    oneTap.numberOfTouchesRequired = 1;
//    [oneTap requireGestureRecognizerToFail:twoTap];
//
//    [self.uImage addGestureRecognizer:oneTap];
//    [self.uImage addGestureRecognizer:twoTap];
    self.uImage.userInteractionEnabled = YES;
}

- (void)loadImage:(UIImage *)image {
    self.uImage.image = image;
    self.backgroundColor = [self.uImage.image averageColor];
   // [self configureZoomScale];
}

- (void)loadImageUrl:(NSString *)imageUrl {
    
    [self.uImage sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [self loadImage:image];
    }];
}

- (void)configureZoomScale {
//    self.uImage.image = [UIImage imageNamed:@"listNoRest"];
    if (self.uImage.image != nil) {
        self.backgroundColor = [self.uImage.image averageColor];

        CGFloat frameW = DeviceWidth;
        CGFloat frameH = self.frame.size.height;

        CGFloat imageW = frameW;
        CGFloat imageH = frameW * (self.uImage.image.size.height / self.uImage.image.size.width);
        self.uImage.frame = CGRectMake(0, (frameH - imageH) / 2, imageW, imageH);
        

        self.minimumZoomScale = self.frame.size.width / self.uImage.frame.size.width;
        self.maximumZoomScale = 3.0;
        [self setZoomScale:self.minimumZoomScale];
        self.contentSize = self.uImage.frame.size;
        [self layoutIfNeeded];
    }
}

- (void)tapTwice:(id)tapTwice {
    if (self.uImage.image != nil) {
        if ((self.zoomScale - self.minimumZoomScale) / (self.maximumZoomScale - self.minimumZoomScale) < 0.5) {
            [self setZoomScale:self.maximumZoomScale animated:YES];
        } else {
            [self setZoomScale:self.minimumZoomScale animated:YES];
        }
    }
}

- (void)tapOnce:(id)tapOnce {
    if (self.imageTapBlock != nil) {
        self.imageTapBlock();
    }
}


- (nullable UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.uImage;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    self.uImage.frame = [self centeredFrameForScrollView:scrollView andUIView:self.uImage];
}


- (CGRect)centeredFrameForScrollView:(UIScrollView *)scroll andUIView:(UIView *)rView {
    CGSize boundsSize    = scroll.bounds.size;
    CGRect frameToCenter = rView.frame;
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width) {
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    } else {
        frameToCenter.origin.x = 0;
    }
    // center vertically
    if (frameToCenter.size.height < boundsSize.height) {
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    } else {
        frameToCenter.origin.y = 0;
    }
    return frameToCenter;
}


@end
