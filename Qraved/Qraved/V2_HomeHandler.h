//
//  V2_HomeHandler.h
//  Qraved
//
//  Created by harry on 2017/6/14.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface V2_HomeHandler : NSObject

+ (void)getCities:(void(^)(id object))successBlock andError:(void(^)(NSError *error))errorBlock;
+ (void)getHomeDataWithParam:(NSDictionary *)params andBlock:(void(^)(id instagram,id location,id quickSearch,id personalized))successBlock andError:(void(^)(NSError *error))errorBlock;
+(void)getInstagramPhotos:(NSDictionary *)params andBlock:(void(^)(id instagramPhotos))successBlock andError:(void(^)(NSError *error))errorBlock;
+(void)getLocation:(NSDictionary *)params andBlock:(void(^)(id locationArr))successBlock andError:(void(^)(NSError *error))errorBlock;
@end
