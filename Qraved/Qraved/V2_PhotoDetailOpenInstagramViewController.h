//
//  V2_PhotoDetailOpenInstagramViewController.h
//  Qraved
//
//  Created by Adam.zhang on 2017/8/31.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"

@interface V2_PhotoDetailOpenInstagramViewController : BaseViewController

@property (nonatomic, strong) NSString  *instagram_link;
@property (nonatomic, assign) BOOL isFromWebSite;
@end
