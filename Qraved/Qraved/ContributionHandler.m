//
//  ContributionHandler.m
//  Qraved
//
//  Created by harry on 2017/7/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "ContributionHandler.h"

@implementation ContributionHandler


+(void)getContribution:(NSDictionary *)params andBlock:(void(^)(id contributionArr))successBlock andError:(void(^)(NSError *error))errorBlock{
    NSDictionary *dic = @{@"userId":[IMGUser currentUser].userId,@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]};
    [[IMGNetWork sharedManager] GET:@"contribution/all" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

@end
