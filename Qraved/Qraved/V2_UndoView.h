//
//  V2_UndoView.h
//  Qraved
//
//  Created by harry on 2017/7/14.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface V2_UndoView : UIView

- (instancetype)initWithFrame:(CGRect)frame andTitle:(NSString *)title andButtonTitle:(NSString *)buttonTitle andTarget:(id)target andSelector:(SEL)selector;

@end
