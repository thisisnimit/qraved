//
//  PersonalizationHandler.m
//  Qraved
//
//  Created by harry on 2017/6/16.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "PersonalizationHandler.h"
#import "V2_BrandModelSuper.h"
@implementation PersonalizationHandler


+ (void)getUserPersonalizationWithPrama:(NSDictionary*)param andSuccessBlock:(void(^)(id arayList,id brandList ,id cuisineList, id eatingList))successBlock anderrorBlock:(void(^)())errorBlock{
    [[IMGNetWork sharedManager] GET:@"app/personalization/option/fetch/v3" parameters:param progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSMutableArray *aray = [NSMutableArray array];
        NSMutableArray *brandArray = [NSMutableArray array];
        NSMutableArray *brandArrayS = [NSMutableArray array];

        aray = [V2_BrandModelSuper mj_objectArrayWithKeyValuesArray:responseObject[@"brandList"]];
        
        
//        NSArray *brandArr = [responseObject objectForKey:@"brandList"];
//        NSMutableArray *brandArray = [NSMutableArray array];
        for (V2_BrandModelSuper *models in aray) {
//            NSLog(@"%@",models.brandType);
            brandArray = [V2_BrandModel mj_objectArrayWithKeyValuesArray:models.brandList];
            [brandArrayS addObject:brandArray];
            for (V2_BrandModel *model in models.brandList) {
                model.state = @"0";
            }
//            V2_BrandModel *model = [[V2_BrandModel alloc] init];
//            NSLog(@"%@",brandArray);
//            [model setValuesForKeysWithDictionary:model];
//            model.state = @"0";
            
        }
//        NSLog(@"%@",brandArrayS);
        NSArray *cuisineArr = [responseObject objectForKey:@"cuisineList"];
        NSMutableArray *cuisineArray = [NSMutableArray array];
        for (NSDictionary *cuisineDic in cuisineArr) {
            V2_LovePreferenceModel *model = [[V2_LovePreferenceModel alloc] init];
            [model setValuesForKeysWithDictionary:cuisineDic];

            [cuisineArray addObject:model];
        }
        NSArray *eatingArr = [responseObject objectForKey:@"eatingList"];
        NSMutableArray *eatingArray = [NSMutableArray array];
        for (NSDictionary *eatingDic in eatingArr) {
            V2_EatModel *model = [[V2_EatModel alloc] init];
            [model setValuesForKeysWithDictionary:eatingDic];
            model.state = @"0";
            [eatingArray addObject:model];
        }
        
        successBlock(aray,brandArray,cuisineArray,eatingArray);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock();
    }];
}

+ (void)chooseCuisineWithPrama:(NSDictionary*)param andSuccessBlock:(void(^)())successBlock anderrorBlock:(void(^)())errorBlock{
    
    [[IMGNetWork sharedManager] requestBodyWithPath:@"user/personalization/save/v2" withBody:param withSuccessBlock:^(id responseObject) {
        successBlock(responseObject);
        
    } withFailurBlock:^(NSError *error) {
        errorBlock(error);
    }];

}
+ (void)getProfileWithPrama:(NSDictionary*)param andSuccessBlock:(void(^)(id aarayList))successBlock anderrorBlock:(void(^)())errorBlock{

    [[IMGNetWork sharedManager] GET:@"user/personalization/v2" parameters:param progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        NSLog(@"%@",responseObject);
//        NSMutableArray *array = [NSMutableArray array];
        
        successBlock(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        errorBlock();
    }];

}
@end
