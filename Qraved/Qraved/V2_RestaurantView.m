//
//  V2_RestaurantView.m
//  Qraved
//
//  Created by harry on 2017/6/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_RestaurantView.h"
#import "UIColor+Hex.h"
#import "V2_RestaurantCell.h"
@interface V2_RestaurantView ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,V2_RestaurantCellDelegate>{
    UICollectionView *restaurantCollectionView;
    UILabel *lblTitle;
    IMGRestaurant *currentRestaurant;
    NSIndexPath *currntIndexPath;
}

@end

@implementation V2_RestaurantView
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    self.backgroundColor = [UIColor whiteColor];
    
    lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 17, DeviceWidth-90, 16)];
    lblTitle.textColor = [UIColor color333333];
    lblTitle.font = [UIFont boldSystemFontOfSize:14];
    [self addSubview:lblTitle];
    
    UIButton *btnChange = [UIButton buttonWithType:UIButtonTypeCustom];
    btnChange.frame = CGRectMake(15, 0, DeviceWidth-30, 50);
    [btnChange addTarget:self action:@selector(seeAllClick) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnChange];
    [btnChange setImage:[UIImage imageNamed:@"ic_home_arrow"] forState:UIControlStateNormal];
    btnChange.imageEdgeInsets = UIEdgeInsetsMake(0, DeviceWidth-30-10, 0, 0);
    
    UILabel *lblSeeAll = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth-95, 6, 50, 38)];
    lblSeeAll.text = @"See All";
    lblSeeAll.textAlignment = NSTextAlignmentRight;
    lblSeeAll.textColor = [UIColor color999999];
    lblSeeAll.font =[UIFont systemFontOfSize:12];
    [btnChange addSubview:lblSeeAll];
    
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(216, 195);
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 10;
    layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 0);
    
    restaurantCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,50, DeviceWidth, 195) collectionViewLayout:layout];
    restaurantCollectionView.backgroundColor = [UIColor whiteColor];
    restaurantCollectionView.delegate = self;
    restaurantCollectionView.dataSource = self;
    restaurantCollectionView.showsHorizontalScrollIndicator = NO;
    [self addSubview:restaurantCollectionView];
    
    
    
    [restaurantCollectionView registerNib:[UINib nibWithNibName:@"V2_RestaurantCell" bundle:nil] forCellWithReuseIdentifier:@"restaurantCell"];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.restaurantArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    V2_RestaurantCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"restaurantCell" forIndexPath:indexPath];
    IMGRestaurant *model = [self.restaurantArray objectAtIndex:indexPath.row];
    cell.currentIndex = indexPath;
    cell.delegate = self;
    cell.model = model;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    IMGRestaurant *model = [self.restaurantArray objectAtIndex:indexPath.row];
    if (self.gotoRestaurantDetail) {
        self.gotoRestaurantDetail(model);
    }
}

- (void)setRestaurantArray:(NSArray *)restaurantArray{
    _restaurantArray = restaurantArray;
    [self setupAutoHeightWithBottomView:restaurantCollectionView bottomMargin:20];
    [restaurantCollectionView reloadData];
    
}
- (void)setName:(NSString *)name{
    _name = name;
    lblTitle.text = name;
}

- (void)seeAllClick{
    if (self.seeAllRestaurant) {
        self.seeAllRestaurant(self.componentId);
    }
}

- (void)savedToList:(IMGRestaurant *)restaurant IndexPath:(NSIndexPath *)indexPath{
    currentRestaurant = restaurant;
    currntIndexPath = indexPath;
    if (self.savedTapped) {
        self.savedTapped(restaurant);
    }
}

- (void)freshSavedButton:(BOOL)liked{
    if (liked) {
        currentRestaurant.saved = @1;
    }else{
        currentRestaurant.saved = @0;
    }
    
    V2_RestaurantCell *cell = (V2_RestaurantCell *)[restaurantCollectionView cellForItemAtIndexPath:currntIndexPath];
    [cell freshSavedButton:liked];
    
    //[restaurantCollectionView reloadData];
    
    
}

@end
