//
//  V2_CityView.h
//  Qraved
//
//  Created by harry on 2017/6/15.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGCity.h"
@interface V2_CityView : UIView

@property (nonatomic, strong) UITableView *cityTableView;
@property (nonatomic, strong) NSArray *cityArray;
@property (nonatomic, assign) NSNumber *selectCity;
@property (nonatomic, copy) void (^citySelected)(IMGCity *city);
@property (nonatomic, copy) void (^removeSelected)();
@end
