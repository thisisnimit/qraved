//
//  InstagramFilterModel.h
//  Qraved
//
//  Created by harry on 2017/12/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstagramFilterModel : NSObject


@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSNumber *cityId;
@property (nonatomic, copy) NSNumber *commentCount;
@property (nonatomic, copy) NSNumber *likeCount;
@property (nonatomic, copy) NSNumber *postCount;
@property (nonatomic, copy) NSString *tag;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSNumber *areaId;
@property (nonatomic, copy) NSString *areaName;
@property (nonatomic, copy) NSNumber *locationId;
@property (nonatomic, copy) NSString *locationName;
@property (nonatomic, copy) NSNumber *locationTypeId;
@property (nonatomic, copy) NSString *locationTypeName;
@property (nonatomic, copy) NSNumber *tagId;
@property (nonatomic, copy) NSString *tagName;
@property (nonatomic, copy) NSNumber *qravedUserId;
@property (nonatomic, copy) NSString *userType;
@property (nonatomic, copy) NSString *username;
@property (nonatomic, assign) BOOL isSelect;
@end
