//
//  V2_PhotoSectionView.h
//  Qraved
//
//  Created by harry on 2017/8/30.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface V2_PhotoSectionView : UIView

@property (nonatomic, strong)NSArray *photoArr;
@property (nonatomic, copy) void (^seeAll)();
@property (nonatomic, copy) void (^photoClick)(NSArray *photoArr, NSInteger index);


- (instancetype)initWithFrame:(CGRect)frame WithTitle:(NSString *)title;

@end
