//
//  V2_CityView.m
//  Qraved
//
//  Created by harry on 2017/6/15.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_CityView.h"
#import "CityHandler.h"
@interface V2_CityView ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *cityArr;
    
}
@end
@implementation V2_CityView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    
    self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4f];
    
    self.cityTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 100) style:UITableViewStylePlain];
    self.cityTableView.backgroundColor = [UIColor whiteColor];
    self.cityTableView.delegate = self;
    self.cityTableView.dataSource = self;
    [self addSubview:self.cityTableView];
    
    cityArr = [NSMutableArray array];
    [self fadeIn];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    self.removeSelected();
    [self fadeOut];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.cityArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *idenT = @"cityCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenT];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenT];
    }
    IMGCity *city = [self.cityArray objectAtIndex:indexPath.row];
    cell.textLabel.text = [city.name capitalizedString];
    if ([city.cityId isEqual:self.selectCity]) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-30, 16, 13, 11)];
        imageView.image = [UIImage imageNamed:@"ic_city_check"];
        [cell.contentView addSubview:imageView];
    }
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    
    IMGCity *city = [self.cityArray objectAtIndex:indexPath.row];
    
    IMGUser *user=[IMGUser currentUser];
    if(user.userId){
        [CityHandler changeCity:city.cityId withUser:user.userId block:nil];
    }
    [[NSUserDefaults standardUserDefaults] setObject:city.name forKey:CITY_SELECT];
    [[NSUserDefaults standardUserDefaults] setObject:city.cityId forKey:CITY_SELECT_ID];
    [[NSNotificationCenter defaultCenter] postNotificationName:CITY_SELECT_NOTIFICATION object:nil userInfo:nil];
    
    self.citySelected(city);
    [self fadeOut];
}

- (void)setCityArray:(NSArray *)cityArray{
    _cityArray = cityArray;
    
    self.cityTableView.height = cityArray.count * 44;
    //[self fadeIn];
    [self.cityTableView reloadData];
}

#pragma mark - view fade animation
- (void)fadeIn{
    
    self.cityTableView.transform = CGAffineTransformMakeTranslation(0.01, -self.cityTableView.height-64);
    [UIView animateWithDuration:0.3 animations:^{
        
        self.cityTableView.transform= CGAffineTransformMakeTranslation(0.01, 0.01);
        
    }];


}

- (void)fadeOut{
//    [UIView animateWithDuration:0.5
//                     animations:^{
//                         self.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
//                         self.cityTableView.top  = -self.cityTableView.height;
//                     }
//                     completion:^(BOOL finished) {
//                         [self removeFromSuperview];
//                     }];
    
    self.cityTableView.transform = CGAffineTransformMakeTranslation(0.01,0.01);
    [UIView animateWithDuration:0.3 animations:^{
        self.cityTableView.transform= CGAffineTransformMakeTranslation(0.01, -self.cityTableView.height-64);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    

}

@end
