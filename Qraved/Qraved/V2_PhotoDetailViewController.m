//
//  V2_PhotoDetailViewController.m
//  Qraved
//
//  Created by harry on 2017/7/17.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_PhotoDetailViewController.h"
#import <BlocksKit/BlocksKit+UIKit.h>
#import "V2_PhotoScrollView.h"
#import "V2_InstagramBottomView.h"
#import "WebServiceV2.h"
#import "LoadingView.h"
#import "V2_MiniRDPView.h"
#import "V2_PhotoDetailOpenInstagramViewController.h"

#import "DetailViewController.h"


@interface V2_PhotoDetailViewController ()<UIScrollViewDelegate>
{
    UIImageView *authorImageView;
    UILabel *lblName;
    //UILabel *lblState;
    UIWebView *captionView;
    YYLabel *lblCaption;
    UIButton *btnClose;
    V2_PhotoScrollView *photoScrollView;
    UIWebView *authorWeb;
    FunctionButton *btnInstagram;
    BOOL ishiddenInfo;
    V2_InstagramBottomView *instagramBottomView;
    UIView *alphaView;
    UILabel *lblTime;
    UIView *MiniRdpView;
    V2_MiniRDPView *miniRDPView;
    UIScrollView *scroll;
    NSNumber *currentLocationId;
    NSDictionary *currentPhotoDic;
    int lastContentOffset;
    FunctionButton *goFoodButton;
    IMGRestaurant *currentRestaurant;
}
@end

@implementation V2_PhotoDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createPhotoScroll];
    [self createCloseButton];
    [self authorInfo];
    [self authorInfoSet:self.currentInstagram];
    
    [IMGAmplitudeUtil trackRestaurantWithName:@"RC – View Restaurant Photo Detail" andRestaurantId:nil andRestaurantTitle:nil andLocation:nil andOrigin:@"Homepage" andType:@"Instagram"];
}

- (void)goFoodTapped{
    [IMGAmplitudeUtil trackGoFoodWithName:@"CL - Go-Food CTA" andJournalId:nil andRestaurantId:self.currentInstagram.restaurant_id andPhotoId:self.currentInstagram.instagram_media_id andLocation:@"Instagram Photo Viewer" andOrigin:nil andOrder:nil];
    [CommonMethod goFoodButtonTapped:currentRestaurant andViewController:self andRestaurantState:[currentRestaurant isOpenRestaurant] andCallBackBlock:^(BOOL isSaved) {
        [instagramBottomView freshSavedButton:isSaved];
    }];
}

- (void)requestData:(NSNumber *)restaurantId{
    [[LoadingView sharedLoadingView] startLoading];
    NSDictionary *dic= @{@"restaurantId":restaurantId};
    [WebServiceV2 instagramRestaurant:dic andBlock:^(IMGRestaurant *restaurant,NSNumber *instagramLocationId,NSDictionary *photoDic) {
        
        if (![Tools isBlankString:restaurant.goFoodLink]) {
            goFoodButton.hidden = NO;
            [goFoodButton addTarget:self action:@selector(goFoodTapped) forControlEvents:UIControlEventTouchUpInside];
            btnInstagram.frame = CGRectMake(15, DeviceHEIGHT- 89 - FUNCTION_BUTTON_HEIGHT, (DeviceWidth - 40)/2, FUNCTION_BUTTON_HEIGHT);
        }else{
            goFoodButton.hidden = YES;
            btnInstagram.frame = CGRectMake(15, DeviceHEIGHT- 89 - FUNCTION_BUTTON_HEIGHT, DeviceWidth - 30, FUNCTION_BUTTON_HEIGHT);
        }
        currentRestaurant = restaurant;
        currentLocationId = instagramLocationId;
        currentPhotoDic = photoDic;
        instagramBottomView.restaurant = restaurant;
        instagramBottomView.locationId = instagramLocationId;
        instagramBottomView.photoDictionary = photoDic;
        if (instagramBottomView.currentPointY>DeviceHeight+20) {
            scroll.frame =  CGRectMake(0, DeviceHeight+20-79, DeviceWidth, instagramBottomView.currentPointY);
            instagramBottomView.frame = CGRectMake(0, 0, DeviceWidth, instagramBottomView.currentPointY);
            scroll.contentSize = CGSizeMake(0, instagramBottomView.currentPointY*2-79);
            
        }else{
            scroll.frame =  CGRectMake(0, DeviceHeight+20-79, DeviceWidth, DeviceHeight+99);
            instagramBottomView.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight+99);
            scroll.contentSize = CGSizeMake(0, DeviceHeight+20+DeviceHeight+20+79);
        }
        __weak typeof(scroll) weakscroll = scroll;
        instagramBottomView.gotoMiniRDP = ^(){
            if (@available(iOS 11.0, *)) {
                [UIView animateWithDuration:0.5 animations:^{
                    weakscroll.contentOffset = CGPointMake(0, DeviceHeight+20 +20);
                 }];
            }else{
                [UIView animateWithDuration:0.5 animations:^{
                    weakscroll.contentOffset = CGPointMake(0, DeviceHeight+20);
                 }];
            }
            
            [[Amplitude instance] logEvent:@"RRC - Mini RDP" withEventProperties:@{}];
        };
        //
        NSLog(@"%f----------",instagramBottomView.currentPointY);
        __weak typeof(self) weakSelf = self;
        __weak typeof(instagramBottomView) weakinstagramBottomView = instagramBottomView;
        instagramBottomView.saveRestaurant = ^(IMGRestaurant *restaurant) {
            [CommonMethod doSaveWithRestaurant:restaurant andViewController:weakSelf andCallBackBlock:^(BOOL isSaved) {
                [weakinstagramBottomView freshSavedButton:isSaved];
            }];
        };
        [[LoadingView sharedLoadingView] stopLoading];
        
    } andError:^(NSError *error) {
        [[LoadingView sharedLoadingView] stopLoading];
    }];
}

- (void)createPhotoScroll{
    photoScrollView = [[V2_PhotoScrollView alloc] initWithFrame:self.view.bounds withPhotoArray:self.photoArray withCurrentIndex:self.currentIndex];
  
    __weak typeof(self) weakSelf = self;
    [photoScrollView setScrollPhoto:^(NSInteger index) {
        V2_InstagramModel *model = [weakSelf.photoArray objectAtIndex:index];
        weakSelf.currentInstagram = model;
        [[LoadingView sharedLoadingView] stopLoading];
        [weakSelf authorInfoSet:model];
    }];
    [photoScrollView setImageTapped:^{
        //        [self hiddenInfo];
        
        
    }];

    __weak typeof(photoScrollView) weakPhotoScrollView = photoScrollView;
     __weak typeof(scroll) weakScroll = scroll;
//    __weak typeof(instagramBottomView) weakinstagramBottomView = instagramBottomView;
    [photoScrollView setPanTapped:^(UIPanGestureRecognizer *pan) {
        
        CGPoint pt = [pan translationInView:weakPhotoScrollView];
        if (pt.y < 0) {
            weakScroll.contentOffset = CGPointMake(0,  -pt.y);
            if (scroll.contentOffset.y > 10) {
                if (pan.state == UIGestureRecognizerStateEnded) {
                    if (@available(iOS 11.0, *)) {
                        [UIView animateWithDuration:0.5 animations:^{
                            weakScroll.contentOffset = CGPointMake(0,  DeviceHeight + 20 +20);
                        }];
                    }else{
                        [UIView animateWithDuration:0.5 animations:^{
                            weakScroll.contentOffset = CGPointMake(0,  DeviceHeight + 20);
                        }];
                    }
                }
                
            }
        }
    }];
    
    [self.view addSubview:photoScrollView];
}


- (void)hiddenInfo{
    if (ishiddenInfo) {
        authorImageView.hidden = NO;
        lblName.hidden = NO;
        lblTime.hidden = NO;
        lblCaption.hidden = NO;
        btnInstagram.hidden = NO;
        //instagramBottomView.hidden = NO;
        ishiddenInfo = NO;
        btnClose.hidden = NO;
    }else{
        authorImageView.hidden = YES;
        lblName.hidden = YES;
        lblTime.hidden = YES;
        lblCaption.hidden = YES;
        btnInstagram.hidden = YES;
        //instagramBottomView.hidden = YES;
        ishiddenInfo = YES;
        btnClose.hidden = YES;
    }
}
- (void)authorInfo{
    alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight +20)];
    alphaView.backgroundColor = [UIColor colorWithRed:0/255.0f green:0/255.0f blue:0/255.0f alpha:0.8];
//    alphaView.userInteractionEnabled = NO;
    alphaView.hidden = YES;
    [self.view addSubview:alphaView];
    UITapGestureRecognizer *alphaTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(alphaTapClick:)];
    [alphaView addGestureRecognizer:alphaTap];
    
    if (@available(iOS 11.0, *)) {
        authorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(12, 31 +25, 32, 32)];
    }else{
        authorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(12, 31, 32, 32)];
    }
    authorImageView.layer.masksToBounds = YES;
    authorImageView.layer.cornerRadius = 16;
    [self.view addSubview:authorImageView];
    
    if (@available(iOS 11.0, *)) {
        lblName = [[UILabel alloc]initWithFrame:CGRectMake(authorImageView.endPointX+10, 31 +25, DeviceWidth-100, 16)];
    }else{
        lblName = [[UILabel alloc]initWithFrame:CGRectMake(authorImageView.endPointX+10, 31, DeviceWidth-100, 16)];
    }
    lblName.font = [UIFont systemFontOfSize:14];
    lblName.textColor = [UIColor whiteColor];
    [self.view addSubview:lblName];
    
    lblTime = [[UILabel alloc] initWithFrame:CGRectMake(authorImageView.endPointX+10, lblName.endPointY+3, DeviceWidth-100, 14)];
    lblTime.textAlignment = NSTextAlignmentLeft;
    lblTime.textColor = [UIColor whiteColor];
    lblTime.font = [UIFont systemFontOfSize:12];
    
    [self.view addSubview:lblTime];
    
    //    captionView = [[UIWebView alloc] init];
    lblCaption = [[YYLabel alloc] initWithFrame:CGRectMake(15, authorImageView.endPointY + 20, DeviceWidth-30, 40)];
    lblCaption.numberOfLines = 2;
    lblCaption.textColor = [UIColor whiteColor];
    lblCaption.font = [UIFont systemFontOfSize:14];
    lblCaption.textVerticalAlignment =YYTextVerticalAlignmentTop;
    [self.view addSubview:lblCaption];
    [self addSeeMoreButton];
    
    
    
    btnInstagram = [FunctionButton buttonWithType:UIButtonTypeCustom andTitle:@"Open in Instagram" andBGColor:[UIColor clearColor] andTitleColor:[UIColor whiteColor]];
    btnInstagram.frame = CGRectMake(15, DeviceHEIGHT- 89 - FUNCTION_BUTTON_HEIGHT, DeviceWidth - 30, FUNCTION_BUTTON_HEIGHT);
    btnInstagram.layer.masksToBounds = YES;
    btnInstagram.layer.borderWidth = 1;
    btnInstagram.layer.borderColor = [UIColor whiteColor].CGColor;
    [self.view addSubview:btnInstagram];
    [btnInstagram addTarget:self action:@selector(openInstagramTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    goFoodButton = [FunctionButton buttonWithType:UIButtonTypeCustom];
    goFoodButton.frame = CGRectMake((DeviceWidth - 40)/2 + 25, btnInstagram.startPointY, (DeviceWidth - 40)/2, FUNCTION_BUTTON_HEIGHT);
    [self.view addSubview:goFoodButton];
    
    if (!self.isFromMiniRDP) {
        if (scroll) {
            [scroll removeFromSuperview];
        }
        
        scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, DeviceHeight+20-79, DeviceWidth, DeviceHeight+99)];
        scroll.backgroundColor = [UIColor clearColor];
        scroll.delegate = self;
        [self.view addSubview:scroll];
        
        if (instagramBottomView) {
            [instagramBottomView removeFromSuperview];
        }
        instagramBottomView = [[V2_InstagramBottomView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 79+DeviceHeight+20)];
        __weak typeof(scroll) weakScroll = scroll;
        instagramBottomView.closeMiniRDP = ^(){
            [UIView animateWithDuration:0.5 animations:^{
                
                CGPoint offset = weakScroll.contentOffset;
                offset.y = -20;
                weakScroll.contentOffset = offset;
            }];
        };
        __weak typeof(self) weakSelf = self;
        instagramBottomView.gotoRDP = ^(){
            DetailViewController *detailViewController = [[DetailViewController alloc] initWithRestaurantId:weakSelf.currentInstagram.restaurant_id];
            [weakSelf.navigationController pushViewController:detailViewController animated:YES];
        };
        instagramBottomView.controller = self.navigationController;
        [scroll addSubview:instagramBottomView];
    }
    
    NSLog(@"%f----------",instagramBottomView.currentPointY);
    
}

- (void)addSeeMoreButton {
//    __weak typeof(self) _self =self;
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:@"...See More"];
    
    YYTextHighlight *hi = [YYTextHighlight new];
    [hi setColor:[UIColor colorWithRed:0.578 green:0.790 blue:1.000 alpha:1.000]];
    hi.tapAction = ^(UIView *containerView,NSAttributedString *text,NSRange range, CGRect rect ) {
    
        [UIView animateWithDuration:0.3 animations:^{
            
            YYLabel *label = lblCaption;
            label.numberOfLines = 0;
            [label sizeToFit];
            CGSize size = [label.text sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(DeviceWidth-30, 500) lineBreakMode:NSLineBreakByWordWrapping];
//            if ((DeviceHeight+20-DeviceWidth)/2 +DeviceWidth - size.height +50 -20 -30 <lblTime.endPointY) {
//                label.frame = CGRectMake(15, lblTime.endPointY, DeviceWidth-30, size.height);
//            }else{
//                 label.frame = CGRectMake(15, (DeviceHeight+20-DeviceWidth)/2 +DeviceWidth - size.height +50 -20 -30, DeviceWidth-30, size.height);
//            }
            label.frame = CGRectMake(15, authorImageView.endPointY + 20, DeviceWidth-30, size.height);
            
            
           

            alphaView.hidden = NO;
//            lblTime.hidden = NO;
//            lblTime.frame = CGRectMake(15, lblCaption.endPointY, DeviceWidth-30, 20);
           // btnInstagram.frame = CGRectMake(2, lblCaption.endPointY+20, 150, 30);

        }];
    };
    
    [text yy_setColor:[UIColor color999999] range:[text.string rangeOfString:@"...See More"]];
    [text yy_setTextHighlight:hi range:[text.string rangeOfString:@"...See More"]];
    text.yy_font = lblCaption.font;
    
    YYLabel *seeMore = [YYLabel new];
    seeMore.attributedText = text;
    [seeMore sizeToFit];
    
    NSAttributedString *truncationToken = [NSAttributedString yy_attachmentStringWithContent:seeMore contentMode:UIViewContentModeCenter attachmentSize:seeMore.frame.size alignToFont:text.yy_font alignment:YYTextVerticalAlignmentCenter];
    lblCaption.truncationToken = truncationToken;
    
}



- (void)alphaTapClick:(UITapGestureRecognizer *)tap{

    NSLog(@"kkkkkkk");
    [UIView animateWithDuration:0.3 animations:^{
        lblCaption.frame = CGRectMake(15, authorImageView.endPointY + 20, DeviceWidth-30, 40);
         lblCaption.numberOfLines = 2;
        alphaView.hidden = YES;
        //btnInstagram.frame = CGRectMake(2, lblCaption.endPointY, 150, 30);
        
    }];
    
}


//start

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{

    lastContentOffset = scrollView.contentOffset.y;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{

    scroll.frame = CGRectMake(0, DeviceHeight+20-79 - scrollView.contentOffset.y, DeviceWidth, scroll.size.height);
    instagramBottomView.frame = CGRectMake(0, scrollView.contentOffset.y, DeviceWidth, instagramBottomView.size.height);

}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    
    [scrollView setContentOffset:scrollView.contentOffset animated:NO];
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
//         NSLog(@"====huadong====%f=======%d",scrollView.contentOffset.y, lastContentOffset);
    
    if (lastContentOffset == 0) {
        //向上滚动
        if (scrollView.contentOffset.y <  150) {
            [UIView animateWithDuration:0.5 animations:^{
                CGPoint offset = scroll.contentOffset;
                offset.y = 0;
                scrollView.contentOffset = offset;
                
            }];
            
        }
        
        if (scrollView.contentOffset.y >DeviceHeight+20 ) {
            NSLog(@"超出屏幕");
            
            
        }
        
        
        if (scrollView.contentOffset.y < DeviceHeight +20 && scrollView.contentOffset.y > DeviceHeight +20 -150 ) {
            [UIView animateWithDuration:0.5 animations:^{
                
                CGPoint offset = scroll.contentOffset;
                offset.y = 0;
                scrollView.contentOffset = offset;
            }];
            
        }
        
        if (scrollView.contentOffset.y >  150 &&  scrollView.contentOffset.y < (DeviceHeight+20)/2 - 79) {
            [UIView animateWithDuration:0.5 animations:^{
                CGPoint offset = scroll.contentOffset;
                offset.y = DeviceHeight+20;
                scrollView.contentOffset = offset;
                [[Amplitude instance] logEvent:@"RRC - Mini RDP" withEventProperties:@{}];
            }];
        }
        
        
        if (scrollView.contentOffset.y <  DeviceHeight -20 &&  scrollView.contentOffset.y > (DeviceHeight+20)/2 - 79) {
            [UIView animateWithDuration:0.5 animations:^{
                CGPoint offset = scroll.contentOffset;
                offset.y = DeviceHeight+20;
                scrollView.contentOffset = offset;
                [[Amplitude instance] logEvent:@"RRC - Mini RDP" withEventProperties:@{}];
            }];
        }

    }
    if (lastContentOffset >0) {
        NSLog(@"向下滚动");
        if (lastContentOffset > DeviceHeight +20) {
              NSLog(@"超出屏幕");
            [UIView animateWithDuration:0.5 animations:^{
            
            }];
//            scroll.frame = CGRectMake(0, DeviceHeight+20-79 - scrollView.contentOffset.y, DeviceWidth, scroll.size.height);
//            instagramBottomView.frame = CGRectMake(0, scrollView.contentOffset.y, DeviceWidth, instagramBottomView.size.height);
            
            if (scrollView.contentOffset.y < DeviceHeight +20) {
                [UIView animateWithDuration:0.5 animations:^{
                    
                    CGPoint offset = scroll.contentOffset;
                    offset.y = 0;
                    scrollView.contentOffset = offset;
                }];
                
                
            }
        }else{
            if (scrollView.contentOffset.y < DeviceHeight +20) {
                [UIView animateWithDuration:0.5 animations:^{
                    
                    CGPoint offset = scroll.contentOffset;
                    offset.y = 0;
                    scrollView.contentOffset = offset;
                }];
                

            }

            
        }
        
}

}


- (void)openInstagramTapped:(UIButton *)button{
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.currentInstagram.instagram_link]];
    V2_PhotoDetailOpenInstagramViewController *V2_PhotoDetailOpenInstagramV = [[V2_PhotoDetailOpenInstagramViewController alloc] init];
    V2_PhotoDetailOpenInstagramV.instagram_link = self.currentInstagram.instagram_link;
    [self.navigationController pushViewController:V2_PhotoDetailOpenInstagramV animated:YES];
    
}

- (void)authorInfoSet:(V2_InstagramModel *)model{
    goFoodButton.hidden = YES;
    btnInstagram.frame = CGRectMake(15, DeviceHEIGHT- 89 - FUNCTION_BUTTON_HEIGHT, DeviceWidth - 30, FUNCTION_BUTTON_HEIGHT);
    [authorImageView sd_setImageWithURL:[NSURL URLWithString:model.instagram_user_profile_picture] placeholderImage:[UIImage imageNamed:@"headDefault.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
    NSString *str =  [NSString stringWithFormat:@"@%@",model.instagram_user_name];//photo's
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:str];
    
    UIFont *baseFont = [UIFont boldSystemFontOfSize:14];
    
    NSRange range = [str rangeOfString:model.instagram_user_name];
    
    if (range.location != NSNotFound) {
        
        [attrString addAttribute:NSFontAttributeName value:baseFont range:range];
    }
    
    lblName.attributedText = attrString;
//    lblState.text = @"In Instagram";
    
    NSRange range1 = NSMakeRange(0,10);
    NSString* prefix = [model.instagram_create_date substringWithRange:range1];
    NSString *timeStr = [NSString dateFormart:prefix];
    if (timeStr==nil) {
        lblTime.text = @"In Instagram";
    }else{
        lblTime.text = [NSString dateFormart:prefix];
    }
    
    NSString *captionStr = [model.caption stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    
    lblCaption.text = captionStr;
    
    //    CGSize size = [lblCaption.text sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(DeviceWidth-30, 55) lineBreakMode:NSLineBreakByWordWrapping];
    //
    //    lblCaption.frame = CGRectMake(15, (DeviceHeight+20-DeviceWidth)/2 +DeviceWidth, DeviceWidth-30, size.height);
    if (model.instagram_link==nil) {
        btnInstagram.hidden = YES;
    }else{
        btnInstagram.hidden = NO;
    }
    //btnInstagram.frame = CGRectMake(2, lblCaption.endPointY, 150, 30);
    
    if (!self.isFromMiniRDP) {
        [self requestData:model.restaurant_id];
    }else{
        if (![Tools isBlankString:self.restaurant.goFoodLink]) {
            currentRestaurant = self.restaurant;
            goFoodButton.hidden = NO;
            [goFoodButton addTarget:self action:@selector(goFoodTapped) forControlEvents:UIControlEventTouchUpInside];
            btnInstagram.frame = CGRectMake(15, DeviceHEIGHT- 89 - FUNCTION_BUTTON_HEIGHT, (DeviceWidth - 40)/2, FUNCTION_BUTTON_HEIGHT);
        }else{
            goFoodButton.hidden = YES;
            btnInstagram.frame = CGRectMake(15, DeviceHEIGHT- 89 - FUNCTION_BUTTON_HEIGHT, DeviceWidth - 30, FUNCTION_BUTTON_HEIGHT);
        }
    }

}

- (void)createCloseButton{
    btnClose = [UIButton buttonWithType:UIButtonTypeCustom];
    if (@available(iOS 11.0, *)) {
         btnClose.frame = CGRectMake(DeviceWidth-45, 31 +25, 30, 30);
    }else{
         btnClose.frame = CGRectMake(DeviceWidth-45, 31, 30, 30);
    }
    btnClose.imageEdgeInsets = UIEdgeInsetsMake(0, 8, 8, 0);
    [btnClose setImage:[UIImage imageNamed:@"ic_close_photo"] forState:UIControlStateNormal];
    [self.view addSubview:btnClose];
    
    [btnClose bk_whenTapped:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.navigationController.navigationBar.hidden = YES;
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    self.navigationController.navigationBar.hidden = NO;
}


@end
