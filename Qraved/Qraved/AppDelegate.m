//
//  AppDelegate.m
//  Qraved
//
//  Created by Shine Wang on 5/30/13.
//  Copyright (c) 2013 ShineWang. All rights reserved.
//

#define ScreenHeight [[UIScreen mainScreen] bounds].size.height

#define ScreenWidth [[UIScreen mainScreen] bounds].size.width


#import "AppDelegate.h"
#import "PushHelper.h"
//#import "Mixpanel.h"
#import "UIConstants.h"
#import "UncaughtExceptionHandler.h"
#import "NdUncaughtExceptionHandler.h"
#import "iRate.h"
//#import "AFHTTPRequestOperationManager.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "VersionHelper.h"
#import "UIImage+Resize.h"
#import "UIImage+Helper.h"
#import "NSString+Helper.h"
#import "UIColor+Helper.h"
#import "ZipArchive.h"
#import "RestaurantHandler.h"

#import "PostDataHandler.h"
#import "IMGRestaurant.h"
#import "IMGUser.h"
#import "NXOAuth2.h"
#import "DBManager.h"
#import "DataInitialHandler.h"
//#import "LayerViewController.h"
#import "ReviewViewController.h"
#import "PaxBookTimePickerViewController.h"
#import "IMGRestaurant.h"
#import "SectionViewController.h"
#import "DetailViewController.h"
#import "SearchPhotosViewController.h"
#import "IMGDiningGuide.h"
#import "DiningGuideRestaurantsViewController.h"
#import "IMGEvent.h"
#import "IMGDiscover.h"
#import "RestaurantsViewController.h"
#import "DiscoverListViewController.h"
#import "JournalViewController.h"
#import "IMGMediaComment.h"
#import "WebViewController.h"
#import "trackHandler.h"
#import "MapViewController.h"
#import "SpecialOffersViewController.h"
#import "JournalListViewController.h"
#import "JournalDetailViewController.h"
#import "JournalHandler.h"
#import "IMGRestaurantOffer.h"
#import "HomeUtil.h"
#import "BookingConfirmViewController.h"
#import "IMGReservation.h"
#import "NotificationHandler.h"
#import <AudioToolbox/AudioToolbox.h>
#import "UploadPhotoCardViewController.h"
#import "ReviewCardViewController.h"
#import "Amplitude.h"
#import "ReviewCardViewController.h"
#import <AppsFlyer/AppsFlyer.h>
#import "ChatStyling.h"
#import "UploadPhotoHandler.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "AlbumViewController.h"
#import "V2_DiscoverListResultViewController.h"
#import "HomeSlideViewController.h"
#import "ReviewPublishViewController.h"
#import "V2_JournalListViewController.h"
#import "V2_DiningGuideListViewController.h"
#import "CouponDetailViewController.h"
#import "BrandViewController.h"
#import "CampaignWebViewController.h"
#import "DeliveryViewController.h"
#import "DetailSeeAllPhotoViewController.h"

//#import <UberRides/UberRides-Swift.h>
#import "V2_HomeViewController.h"
#import <IQKeyboardManager/IQKeyboardManager.h>
#import "V2_PreferenceViewController.h"
/******* Set your tracking ID here *******/
static NSString *const kTrackingId = @"UA-42073500-2";
//static NSString *const kTrackingId =@"UA-80205108-2";
//static NSString *const kTrackingId = @"UA-84809024-2";
static NSString *const kAllowTracking = @"allowTracking";
BOOL NeedExpandMenu=NO;
BOOL ChangeCity=NO;
//static NSString *const kGAIScreenName =@"Qraved_IOS";
@interface AppDelegate()<ZYQAssetPickerControllerDelegate>
{
    NSMutableString *notificationStrM;
    BOOL isFirst;
    BOOL isOpenFromNotification;
    BOOL isOpenFromMobileWeb;
    BOOL isNotificationOrMobileWeb;
    UIVisualEffectView *effectview;
    UIImageView* rwimageView;
    UIImageView* uploadImageView;
    UIImageView* listImageView;
    UILabel *rwlable;
    UILabel *uploadLable;
    UILabel *listLoadLable;
    CAShapeLayer *_shapeCircle;
    UIImageView *centerImageview;
    RDVTabBarItem *centerItem;
    NSNumber *_notificationCount;

}
//@property (nonatomic) LYRClient *layerClient;
//@property (nonatomic,retain) LayerViewController *viewController;
@end


@implementation AppDelegate


@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;
@synthesize tabMenuViewController,currentLocation,userLogined;

@synthesize touchedIndex;
@synthesize homeViewController;
@synthesize homeSlideViewController;
@synthesize savedViewController;
@synthesize discoverListViewController;
@synthesize hotKeyViewController;
@synthesize notificationsViewController;
@synthesize myProfileViewController;
@synthesize pointRewardViewController;
@synthesize settingViewController;
@synthesize contributionController;
@synthesize ReviewPublishViewController;


@synthesize homeNavigationController;
@synthesize homeSlideNavigationController;
@synthesize savedNavigationController;
@synthesize discoverListNavigationController;
@synthesize hotKeyNavigationController;
@synthesize notificationsNavigationController;
@synthesize qraveNavigationController;
@synthesize searchNavigationController;
@synthesize filterNavigationController;
@synthesize ProfileNavigationController;
@synthesize offersNavigationController;
@synthesize nearByNavigationController;
@synthesize diningGuideNavigationController;
@synthesize qravedJournalNavigationController;
@synthesize selectedNavigationController;
@synthesize loginNavagationController;
@synthesize pointRewardsNavigationController;
@synthesize settingNavigationController;
@synthesize feedNavifationController;
@synthesize contributionNavigationController;
@synthesize ReviewPublishNavigationController;


- (void)initializeRate
{

    [iRate sharedInstance].applicationBundleID = @"com.qraved.app";
    [iRate sharedInstance].onlyPromptIfLatestVersion = NO;
    
    [iRate sharedInstance].daysUntilPrompt = 5;
    [iRate sharedInstance].usesUntilPrompt = 3;
    
    [iRate sharedInstance].previewMode = NO;
}
- (void)deepLinkhandle{
    NSDictionary *dic = [[NSUserDefaults standardUserDefaults] objectForKey:@"deepLinkDic"];
    [self jumpToAppWithUrl:[dic objectForKey:@"urlString"] andNotificationType:[dic objectForKey:@"type"] andNotificationId:[dic objectForKey:@"NotificationId"] andLandId:[dic objectForKey:@"landId"]];
}
- (void)referrerDeeplink{
    NSString *referrerUrl = [[NSUserDefaults standardUserDefaults] objectForKey:@"deepLinkUrl"];
    [self jumpToAppWithUrl:referrerUrl andNotificationType:@0 andNotificationId:nil andLandId:nil];
}

- (NSString *)returnPushId:(NSString *)urlString withIdRangeStr:(NSString *)idRangeStr{
    NSArray *arr = [urlString componentsSeparatedByString:@"&"];
    for (NSString *str in arr) {
        NSRange idRange = [str rangeOfString:idRangeStr];
        if (idRange.location != NSNotFound) {
            return [str substringFromIndex:idRange.location+idRange.length];
        }
    }
    return @"";
}

- (void)jumpToAppWithUrl:(NSString *)urlString andNotificationType:(NSNumber *)type andNotificationId:(NSNumber *)Nid andLandId:(NSNumber *)landId
{
    if ( [[NSUserDefaults standardUserDefaults] boolForKey:@"isAppTerminated"]) {
        if (!isOpenFromMobileWeb) {
            [[Amplitude instance] logEvent:@"GN - Open App" withEventProperties:@{@"Source":@"Push Notification"}];
            isOpenFromNotification=YES;
        }
        isOpenFromMobileWeb=NO;
        [[NSUserDefaults standardUserDefaults] setObject:false forKey:@"isAppTerminated"];
    }
    isNotificationOrMobileWeb = YES;
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"deepLinkDic"];
        NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",Nid,@"notificationId", nil];
        [NotificationHandler readNotificationWithParams:params];
    }else{
        NSDictionary *dic = @{@"urlString":urlString,@"type":type?type:@"",@"NotificationId":Nid?Nid:@"",@"landId":landId?landId:@""};
        
        [[NSUserDefaults standardUserDefaults] setObject:dic forKey:@"deepLinkDic"];
        
        
        NSRange linkRange = [urlString rangeOfString:@"urlType=openInApp"];
        if (linkRange.location != NSNotFound) {
            [[NSUserDefaults standardUserDefaults] setObject:@"openInApp" forKey:@"isOpenInApp"];
        }
        
        LoginViewController *login = [[LoginViewController alloc]init];
        login.isFromonboard = YES;
        UINavigationController *loginNav=[[UINavigationController alloc]initWithRootViewController:login];
        self.window.rootViewController=  loginNav;
        return;
    }
    
    if ([type intValue] == 1 || [type intValue] == 0)
    {
        if([urlString rangeOfString:@"qraved731842943"].location == NSNotFound)
        {
            [[UIApplication sharedApplication]openURL:[NSURL URLWithString:urlString]];
            return;
        }
        NSRange range=[urlString rangeOfString:@"app/restaurant"];
        NSRange reviewRange=[urlString rangeOfString:@"app/review"];
        NSRange offerRanger=[urlString rangeOfString:@"app/offer"];
        NSRange photoRanger=[urlString rangeOfString:@"app/photo"];
        NSRange menuRanger=[urlString rangeOfString:@"app/menu"];
        NSRange mapRanger=[urlString rangeOfString:@"app/map"];
        NSRange homeRange = [urlString rangeOfString:@"app/home"];
        NSRange diningguideRange = [urlString rangeOfString:@"app/diningguide"];
        NSRange eventRange = [urlString rangeOfString:@"app/event"];
        
        NSRange cardRange = [urlString rangeOfString:@"app/card"];
        NSRange writeReviewRange = [urlString rangeOfString:@"app/writeReview"];
        NSString *restString=@"restaurantid=";

        if (cardRange.location !=NSNotFound) {
            
            if ([urlString rangeOfString:@"reviewId="].location != NSNotFound) {
                
                NSString *reviewId = [self returnPushId:urlString withIdRangeStr:@"reviewId="];
                
                IMGReview *review = [[IMGReview alloc] init];
                review.reviewId = [NSNumber numberWithInt:[reviewId intValue]];
                
                ReviewCardViewController *rcvc = [[ReviewCardViewController alloc] initWithRestaurant:nil andReview:review andDishList:nil];
                rcvc.isFromNotification = YES;
                rcvc.amplitudeType = @"Notification";
                [self.homeNavigationController pushViewController:rcvc animated:YES];
                
            }else if ([urlString rangeOfString:@"dishId="].location != NSNotFound)
            {
                NSString *dishId = [self returnPushId:urlString withIdRangeStr:@"dishId="];
                IMGDish *dish = [[IMGDish alloc] init];
                dish.dishId = [NSNumber numberWithInt:[dishId intValue]];
                NSArray *dishArr = [[NSArray alloc] initWithObjects:dish, nil];
                UploadPhotoCardViewController *upcvc = [[UploadPhotoCardViewController alloc] initWithRestaurant:nil andDishList:dishArr andUser:nil];
                upcvc.isFromNotification = YES;
                upcvc.amplitudeType = @"Notification";
                [self.homeNavigationController pushViewController:upcvc animated:YES];
            }
        }
        else if (writeReviewRange.location!=NSNotFound){
            [self.homeNavigationController dismissViewControllerAnimated:NO completion:nil];
            
            NSString *restaurantID = [self returnPushId:urlString withIdRangeStr:@"id="];
            [RestaurantHandler getRestaurantDetailFromServer:[NSNumber numberWithInt:[restaurantID intValue]] andBlock:^(IMGRestaurant* tmpRestaurant){
                ReviewPublishViewController *reviewPublishViewController = [[ReviewPublishViewController alloc] initWithRestaurant:tmpRestaurant andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
                reviewPublishViewController.amplitudeType = @"Notification";
                UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:reviewPublishViewController];
                [self.homeNavigationController presentViewController:nav animated:YES completion:nil];
                [[Amplitude instance] logEvent:@"UC - Write Review Initiate" withEventProperties:@{@"Origin":@"Open in App"}];
//                [self.homeNavigationController pushViewController:reviewPublishViewController animated:YES];
            }];

        }
       else if (range.location!=NSNotFound)
        {
            NSString *restaurantId = [self returnPushId:urlString withIdRangeStr:@"id="];
            DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurantId:[NSNumber numberWithInt:[restaurantId intValue] ]];
            detailViewController.amplitudeType = @"Open in App";
            [self.homeNavigationController pushViewController:detailViewController animated:YES];
            
        }
        else if (homeRange.location != NSNotFound)
        {
            if ([urlString rangeOfString:@"type=journal"].location != NSNotFound)
            {
                NSRange idRange = [urlString rangeOfString:@"journalid="];
                if (idRange.location != NSNotFound)
                {

                    NSString *journalId = [self returnPushId:urlString withIdRangeStr:@"journalid="];
                    [JournalHandler getJournalDetailWithJournalId:[NSNumber numberWithInt:[journalId intValue]] andJournalBlock:^(IMGMediaComment *journal) {
                        
                        if(journal==nil){
                            V2_JournalListViewController *JVC = [[V2_JournalListViewController alloc] init];
                            [self.homeNavigationController pushViewController:JVC animated:YES];
                            
                        }else if ([journal.imported intValue]== 1)
                        {
                            [JournalHandler getJournalDetailWithJournalArticleId:journal.mediaCommentId andBlock:^(NSString *content,NSString *webSite) {
                                journal.contents = content;
                                WebViewController *wvc = [[WebViewController alloc] init];
                                wvc.content = journal.contents;
                                wvc.journal = journal;
                                wvc.title = journal.title;
                                wvc.webSite = webSite;
                                wvc.fromDetail = NO;
                                [self.homeNavigationController pushViewController:wvc animated:YES];
                            }];
                        }
                        else
                        {
                            JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
                            jdvc.journal = journal;
                            [self.homeNavigationController pushViewController:jdvc animated:YES];
                        }
                        
                        
                    }];
                }
                else
                {
                    V2_JournalListViewController *JVC = [[V2_JournalListViewController alloc] init];
                    [self.homeNavigationController pushViewController:JVC animated:YES];
                    
                }
            }
            else if ([urlString rangeOfString:@"type=search"].location != NSNotFound)
            {
                V2_DiscoverListResultViewController *discoverListResultVC = [[V2_DiscoverListResultViewController alloc] init];
                discoverListResultVC.keywordString = @"";
                [self.homeNavigationController pushViewController:discoverListResultVC animated:YES];
            }
            else
            {
                
            }
            
        }
        else if ([urlString rangeOfString:@"app/journal"].location != NSNotFound)
        {
            
            NSRange idRange = [urlString rangeOfString:@"journalid="];
            if (idRange.location != NSNotFound)
            {
                
                NSString *journalId = [self returnPushId:urlString withIdRangeStr:@"journalid="];
                
                IMGMediaComment *journal = [[IMGMediaComment alloc] init];
                journal.mediaCommentId = [NSNumber numberWithInt:[journalId intValue]];
                [JournalHandler getJournalDetailTypeArticleIdWithJournalId:journal.mediaCommentId andJournalBlock:^(IMGMediaComment *journal) {
                    
                    if(journal==nil){
                        V2_JournalListViewController *JVC = [[V2_JournalListViewController alloc] init];
                        [selectedNavigationController pushViewController:JVC animated:YES];
                        
                    }else if ([journal.imported intValue]== 1)
                    {
                        [JournalHandler getJournalDetailWithJournalArticleId:journal.mediaCommentId andBlock:^(NSString *content,NSString *webSite) {
                            journal.contents = content;
                            WebViewController *wvc = [[WebViewController alloc] init];
                            wvc.content = journal.contents;
                            wvc.journal = journal;
                            wvc.webSite = webSite;
                            wvc.title = journal.title;
                            wvc.fromDetail = NO;
                            [selectedNavigationController pushViewController:wvc animated:YES];
                        }];
                    }
                    else
                    {
                        JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
                        jdvc.journal = journal;
                        [selectedNavigationController pushViewController:jdvc animated:YES];
                    }
                    
                    
                }];
            }else if ([urlString rangeOfString:@"trending"].location!=NSNotFound){
                
                V2_JournalListViewController *JVC = [[V2_JournalListViewController alloc] init];
                [self.homeNavigationController pushViewController:JVC animated:YES];
                
                
            }else if ([urlString rangeOfString:@"?s="].location!=NSNotFound){
                
                V2_JournalListViewController *JVC = [[V2_JournalListViewController alloc] init];
                //JVC.isPop = YES;
                JVC.needLog = YES;
               // JVC.searchText =str;
                [self.homeNavigationController pushViewController:JVC animated:YES];
                
            }else{
                
                V2_JournalListViewController *JVC = [[V2_JournalListViewController alloc] init];
                [self.homeNavigationController pushViewController:JVC animated:YES];
                
            }
            
            
        }else if(reviewRange.location !=NSNotFound){
            
            NSRange idRange = [urlString rangeOfString:restString];
            if(idRange.location != NSNotFound){
                NSString *restaurantId = [urlString substringFromIndex:idRange.location+restString.length];
                IMGRestaurant *tmpRestaurant = [[IMGRestaurant alloc] init];
                tmpRestaurant.restaurantId = [NSNumber numberWithInt:[restaurantId intValue]];
    
                if (tmpRestaurant){
                    ReviewViewController *reviewController = [[ReviewViewController alloc] initWithRestaurant:tmpRestaurant];
                    [self.selectedNavigationController pushViewController:reviewController animated:YES];
                }
            }
            
        }else if ([urlString rangeOfString:@"app/sortbyOffer"].location != NSNotFound){

            V2_DiscoverListResultViewController *discoverListResultVC = [[V2_DiscoverListResultViewController alloc] init];
            discoverListResultVC.keywordString = @"";
            [self.homeNavigationController pushViewController:discoverListResultVC animated:YES];
        }else if(offerRanger.location != NSNotFound){
            NSRange idRange = [urlString rangeOfString:restString];
            if(idRange.location != NSNotFound){
                NSString *restaurantId = [urlString substringFromIndex:idRange.location+restString.length];
                [RestaurantHandler getRestaurantDetailFromServer:[NSNumber numberWithInt:[restaurantId intValue]] andBlock:^(IMGRestaurant* tmpRestaurant){
                    NSMutableArray *viewControllers=[self.homeNavigationController.viewControllers mutableCopy];
                    DetailViewController *detailViewController = [[DetailViewController alloc] initWithRestaurant:tmpRestaurant];
                    detailViewController.amplitudeType = @"Notification";
                    [viewControllers addObject:detailViewController];
                    SpecialOffersViewController *specialOffersViewController = [[SpecialOffersViewController alloc]initWithRestaurant:tmpRestaurant andTimeArray:nil andOfferArray:nil];
                    [viewControllers addObject:specialOffersViewController];
                    [self.homeNavigationController setViewControllers:viewControllers animated:YES];
                }];
            }
        }else if(menuRanger.location != NSNotFound){
            NSRange idRange = [urlString rangeOfString:restString];
            if(idRange.location != NSNotFound){
                NSString *restaurantId = [urlString substringFromIndex:idRange.location+restString.length];
                IMGRestaurant *tmpRestaurant=[IMGRestaurant findById:[NSNumber numberWithInt:[restaurantId intValue]]];
                
                if (tmpRestaurant){
                    NSMutableArray *viewControllers=[self.homeNavigationController.viewControllers mutableCopy];
                    DetailViewController *detailViewController = [[DetailViewController alloc] initWithRestaurant:tmpRestaurant];
                    detailViewController.amplitudeType = @"Notification";
                    [viewControllers addObject:detailViewController];
                    SectionViewController *menusViewController=[[SectionViewController alloc]initWithRestaurant:tmpRestaurant];
                    [viewControllers addObject:menusViewController];
                    [self.homeNavigationController setViewControllers:viewControllers animated:YES];
                }
            }
        }else if(photoRanger.location != NSNotFound){
            NSRange idRange = [urlString rangeOfString:restString];
            if(idRange.location != NSNotFound){
                NSString *restaurantId = [urlString substringFromIndex:idRange.location+restString.length];
                IMGRestaurant *tmpRestaurant=[[IMGRestaurant alloc] init];
                tmpRestaurant.restaurantId = [NSNumber numberWithInt:[restaurantId intValue]];
                if (tmpRestaurant){
                    DetailSeeAllPhotoViewController *photoListViewController = [[DetailSeeAllPhotoViewController alloc] initWithRestaurant:tmpRestaurant restaurantPhotoArray:nil];
                    [self.selectedNavigationController pushViewController:photoListViewController animated:YES];
                }
            }
        }else if(mapRanger.location != NSNotFound){
            NSRange idRange = [urlString rangeOfString:restString];
            if(idRange.location != NSNotFound){
                NSString *restaurantId = [urlString substringFromIndex:idRange.location+restString.length];
                IMGRestaurant *tmpRestaurant=[IMGRestaurant findById:[NSNumber numberWithInt:[restaurantId intValue]]];
                
                if (tmpRestaurant){
                    IMGUser *user = [IMGUser currentUser];
                    NSNumber *userId = [[NSNumber alloc] init];
                    if (user.userId != nil)
                    {
                        userId = user.userId;
                    }
                    else
                        userId = [NSNumber numberWithInt:0];
                    
                    [TrackHandler trackWithUserId:userId andRestaurantId:[NSNumber numberWithInt:[restaurantId intValue]] andContent:[NSDictionary dictionaryWithObject:@"1" forKey:@"type"]];
                    NSMutableArray *viewControllers=[self.homeNavigationController.viewControllers mutableCopy];
                    DetailViewController *detailViewController = [[DetailViewController alloc] initWithRestaurant:tmpRestaurant];
                    detailViewController.amplitudeType = @"Notification";
                    [viewControllers addObject:detailViewController];
                    MapViewController *mapViewController=[[MapViewController alloc]initWithRestaurant:tmpRestaurant];
                    mapViewController.amplitudeType = @"Notification";
                    [viewControllers addObject:mapViewController];
                    [self.homeNavigationController setViewControllers:viewControllers animated:YES];
                }
            }
        }
        else if (diningguideRange.location != NSNotFound)
        {
            NSRange idRange = [urlString rangeOfString:@"diningguideid="];
            if (idRange.location != NSNotFound)
            {
                NSString *diningguideId = [self returnPushId:urlString withIdRangeStr:@"diningguideid="];
                
                IMGDiningGuide *diningGuide = [IMGDiningGuide findDiningGuideById:[NSNumber numberWithInt:[diningguideId intValue]]];
                if (diningGuide)
                {
                    DiningGuideRestaurantsViewController *diningResList = [[DiningGuideRestaurantsViewController alloc]initWithDiningGuide:diningGuide];
                    [self.homeNavigationController pushViewController:diningResList animated:YES];
                }
                else
                {
                    [HomeUtil getSingleDiningGuideFromServiceWithDiningGuideId:[NSNumber numberWithInt:[diningguideId intValue]] andBlock:^(IMGDiningGuide *guide) {
//                        IMGDiningGuide *diningGuide = [IMGDiningGuide findDiningGuideById:[NSNumber numberWithInt:[diningguideId intValue]]];
                        DiningGuideRestaurantsViewController *diningResList = [[DiningGuideRestaurantsViewController alloc]initWithDiningGuide:guide];
                        [self.homeNavigationController pushViewController:diningResList animated:YES];
                    }];
                }
            }
            else
            {
                V2_DiningGuideListViewController *dglvc = [[V2_DiningGuideListViewController alloc] init];
                [self.homeNavigationController pushViewController:dglvc animated:YES];
                
            }
            
        }
        else if (eventRange.location != NSNotFound)
        {
            NSRange idRange = [urlString rangeOfString:@"eventid="];
            if (idRange.location != NSNotFound)
            {
                NSString *eventId = [urlString substringFromIndex:idRange.location+8];
                
                IMGEvent *event = [IMGEvent findById:[NSNumber numberWithInt:[eventId intValue]]];
                
                if (event)
                {
                    IMGDiscover *discover=[[IMGDiscover alloc]init];
                    discover.sortby = SORTBY_POPULARITY;
                    if(event!=nil && [event.eventId intValue]>0){
                        discover.eventArray = [[NSMutableArray alloc]initWithCapacity:0];
                        IMGOfferType *offerType = [[IMGOfferType alloc]init];
                        offerType.offerId = event.eventId;
                        offerType.eventType=[NSNumber numberWithInt:1];
                        offerType.typeId = [NSNumber numberWithInt:4];
                        offerType.off = [NSNumber numberWithInt:0];
                        offerType.name = event.name;
                        [discover.eventArray addObject:offerType];
                    }
                    discover.fromWhere=@4;
                    
                    RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc]initWithEventId:[event.eventId intValue]];
                    [restaurantsViewController initDiscover:discover];
                    [self.homeNavigationController pushViewController:restaurantsViewController animated:YES];
                }
                else
                {
                    [HomeUtil getSingleEventFromServiceWithEventId:[NSNumber numberWithInt:[eventId intValue]] andBlock:^{
                        IMGEvent *event = [IMGEvent findById:[NSNumber numberWithInt:[eventId intValue]]];
                        IMGDiscover *discover=[[IMGDiscover alloc]init];
                        discover.sortby = SORTBY_POPULARITY;
                        if(event!=nil && [event.eventId intValue]>0){
                            discover.eventArray = [[NSMutableArray alloc]initWithCapacity:0];
                            IMGOfferType *offerType = [[IMGOfferType alloc]init];
                            offerType.offerId = event.eventId;
                            offerType.eventType=[NSNumber numberWithInt:1];
                            offerType.typeId = [NSNumber numberWithInt:4];
                            offerType.off = [NSNumber numberWithInt:0];
                            offerType.name = event.name;
                            [discover.eventArray addObject:offerType];
                        }
                        discover.fromWhere=@4;
                        RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc]initWithEventId:[event.eventId intValue]];
                        [restaurantsViewController initDiscover:discover];
                        [self.homeNavigationController pushViewController:restaurantsViewController animated:YES];
                    } failure:^{
                        
                    }];
                }
                
            }
            else
            {
                
            }
        }
        else if ([urlString rangeOfString:@"app/search"].location != NSNotFound)
        {

            V2_DiscoverListResultViewController *discoverListResultVC = [[V2_DiscoverListResultViewController alloc] init];
            discoverListResultVC.keywordString = @"";
            //discoverListResultVC.cityName = cityButton.titleLabel.text;
            
            
            NSRange range = [urlString rangeOfString:@"componentid="];
            if (range.location != NSNotFound) {
                discoverListResultVC.isFromHome = YES;
                discoverListResultVC.homeSectionComponentId = [NSNumber numberWithInt:[[urlString substringFromIndex:range.location+range.length] intValue]];
            }
            
            
            //[self.navigationController pushViewController:discoverListResultVC animated:YES];
            
            [selectedNavigationController pushViewController:discoverListResultVC animated:NO];
            
        }else if ([urlString rangeOfString:@"app/nearby"].location!=NSNotFound){

            V2_DiscoverListResultViewController *discoverListResultVC = [[V2_DiscoverListResultViewController alloc] init];
            discoverListResultVC.keywordString = @"";
            discoverListResultVC.isHomeNearBy = YES;
            [self.homeNavigationController pushViewController:discoverListResultVC animated:YES];
            
        }else if ([urlString rangeOfString:@"app/uploadphoto"].location!=NSNotFound){
            
            ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] initWithRestaurantTitle:nil andIsFromUploadPhoto:YES andIsUploadMenuPhoto:NO andCurrentRestaurantId:nil];
            picker.publishViewController = self.window.rootViewController;
            picker.maximumNumberOfSelection = 9;
            picker.assetsFilter = [ALAssetsFilter allPhotos];
            picker.showEmptyGroups=NO;
            picker.delegate = self;
            picker.isFromUploadPhoto = YES;
            picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
                    NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
                    return duration >= 5;
                } else {
                    return YES;
                }
            }];
            [self.selectedNavigationController presentViewController:picker animated:YES completion:nil];

        }else if ([urlString rangeOfString:@"app/writereview"].location!=NSNotFound){
            
            ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc] init];
            publishReviewVC.isFromHotkey = YES;
            publishReviewVC.amplitudeType = @"Hot Key";
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:publishReviewVC];
            [self.selectedNavigationController presentViewController:navigationController animated:YES completion:nil];
        }else if ([urlString rangeOfString:@"app/coupon"].location != NSNotFound){
            
            NSRange idRange = [urlString rangeOfString:@"couponId="];
            if (idRange.location != NSNotFound)
            {
                NSString *couponId = [self returnPushId:urlString withIdRangeStr:@"couponId="];
                
                [IMGAmplitudeUtil trackCampaignWithName:@"RC - View Coupon Detail" andCampaignId:nil andBrandId:nil andCouponId:[NSNumber numberWithInt:[couponId intValue]] andLocation:nil andOrigin:@"Notification" andIsMall:NO];
                
                CouponDetailViewController *detailViewController = [[CouponDetailViewController alloc] init];
                detailViewController.couponId = [NSNumber numberWithInt:[couponId intValue]];
                MLNavigationController *nav = [[MLNavigationController alloc] initWithRootViewController:detailViewController];
                [self.selectedNavigationController presentViewController:nav animated:YES completion:nil];
            }
        }else if ([urlString rangeOfString:@"app/brand"].location != NSNotFound){
            NSRange idRange = [urlString rangeOfString:@"brandId="];
            if (idRange.location != NSNotFound) {
                NSString *brandId = [self returnPushId:urlString withIdRangeStr:@"brandId="];
                BrandViewController *brandViewController = [[BrandViewController alloc] init];
                brandViewController.brandId = brandId;
                [self.selectedNavigationController pushViewController:brandViewController animated:YES];
            }
        }else if ([urlString rangeOfString:@"app/mall"].location != NSNotFound){
            NSRange idRange = [urlString rangeOfString:@"mallId="];
            if (idRange.location != NSNotFound) {
                NSString *mallId = [self returnPushId:urlString withIdRangeStr:@"mallId="];
                BrandViewController *brandViewController = [[BrandViewController alloc] init];
                brandViewController.mallId = mallId;
                brandViewController.isMall = YES;
                [self.selectedNavigationController pushViewController:brandViewController animated:YES];
            }
        }else if ([urlString rangeOfString:@"app/campaign"].location != NSNotFound){
            NSRange idRange = [urlString rangeOfString:@"url="];
            if (idRange.location != NSNotFound) {
                NSString *campaignStr = [self returnPushId:urlString withIdRangeStr:@"url="];
                CampaignWebViewController *campaignController = [[CampaignWebViewController alloc] init];
                campaignController.campaignUrl = campaignStr;
                [self.selectedNavigationController pushViewController:campaignController animated:YES];
            }
        }else if ([urlString rangeOfString:@"app/delivery"].location != NSNotFound){
            DeliveryViewController *deliveryVC = [[DeliveryViewController alloc] init];
            [self.selectedNavigationController pushViewController:deliveryVC animated:YES];
        }
    }else if ([type intValue] == 3){
        IMGReservation *reservation = [[IMGReservation alloc] init];
        reservation.reservationId = landId;
        
        BookingConfirmViewController *bookConfirmVC = [[BookingConfirmViewController alloc] initWithReservation:reservation];
        bookConfirmVC.isFromProfile = YES;
        [self.homeNavigationController pushViewController:bookConfirmVC animated:YES];
    }
    else if ([type intValue] == 4)
    {
        IMGRestaurant *tmpRestaurant = [[IMGRestaurant alloc] init];
        tmpRestaurant.restaurantId = landId;
        DetailViewController *detailViewController = [[DetailViewController alloc]initWithRestaurantId:landId];
        detailViewController.amplitudeType = @"Notification";
        detailViewController.isFromAppDelegate = YES;
        [self.homeNavigationController pushViewController:detailViewController animated:YES];
    }
    else if ([type intValue] == 5)
    {
        IMGReview *review = [[IMGReview alloc] init];
        review.reviewId = landId;
        ReviewCardViewController *rcvc = [[ReviewCardViewController alloc] initWithRestaurant:nil andReview:review andDishList:nil];
        rcvc.isFromNotification = YES;
        rcvc.amplitudeType = @"Notification";
        [self.homeNavigationController pushViewController:rcvc animated:YES];
    }
    else if ([type intValue] == 6)
    {
        IMGDish *dish = [[IMGDish alloc] init];
        dish.dishId = landId;
        NSArray *dishArr = [[NSArray alloc] initWithObjects:dish, nil];
        UploadPhotoCardViewController *upcvc = [[UploadPhotoCardViewController alloc] initWithRestaurant:nil andDishList:dishArr andUser:nil];
        upcvc.isFromNotification = YES;
        upcvc.amplitudeType = @"Notification";
        [self.homeNavigationController pushViewController:upcvc animated:YES];
    }else if ([type intValue]==7){
    
      
        ImageListViewController*  imageListVC=[[ImageListViewController alloc]init];
        imageListVC.isfromNotification=YES;
        imageListVC.moderateId=landId;
        [self.homeNavigationController pushViewController:imageListVC animated:YES];
//        NSMutableDictionary* pragram=[[NSMutableDictionary alloc]init];
    }else if ([type intValue] == 8){
        JournalDetailViewController *jdvc = [[JournalDetailViewController alloc] init];
        IMGMediaComment *journal = [[IMGMediaComment alloc] init];
        journal.mediaCommentId = landId;
        jdvc.journal = journal;
        [self.homeNavigationController pushViewController:jdvc animated:YES];
    }else if ([type intValue] == 9){
        [RestaurantHandler getRestaurantDetailFromServer:landId andBlock:^(IMGRestaurant* tmpRestaurant){
            NSMutableArray *viewControllers=[self.homeNavigationController.viewControllers mutableCopy];
            DetailViewController *detailViewController = [[DetailViewController alloc] initWithRestaurant:tmpRestaurant];
            detailViewController.amplitudeType = @"Notification";
            [viewControllers addObject:detailViewController];
            SpecialOffersViewController *specialOffersViewController = [[SpecialOffersViewController alloc]initWithRestaurant:tmpRestaurant andTimeArray:nil andOfferArray:nil];
            specialOffersViewController.notificationOfferId = landId;
            [viewControllers addObject:specialOffersViewController];
            [self.homeNavigationController setViewControllers:viewControllers animated:YES];
        }];
    }else if ([type intValue] == 10){
        NSDictionary *dicT = @{@"restaurantEventId":landId};
        [RestaurantHandler getPromoInfo:dicT andBlock:^(IMGRestaurantEvent *restaurantEvent) {
            NSArray *eventPhotoArray = @[restaurantEvent];
            IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
            restaurant.title = restaurantEvent.restaurantTitle;
            restaurant.restaurantId = restaurantEvent.restaurantId;
            AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:eventPhotoArray andPhotoTag:0 andRestaurant:restaurant andFromDetail:YES];
            albumViewController.title = restaurant.title;
            albumViewController.showPage=NO;
            albumViewController.restaurantId = restaurant.restaurantId;
            albumViewController.isUseWebP = YES;
            [self.homeNavigationController pushViewController:albumViewController animated:YES];
        }];
    }else if ([type intValue] == 12){
        [IMGAmplitudeUtil trackCampaignWithName:@"RC - View Coupon Detail" andCampaignId:nil andBrandId:nil andCouponId:landId andLocation:nil andOrigin:@"Notification" andIsMall:NO];
        CouponDetailViewController *couponDetailViewController = [[CouponDetailViewController alloc] init];
        couponDetailViewController.couponId = landId;
        MLNavigationController *nav = [[MLNavigationController alloc] initWithRootViewController:couponDetailViewController];
        [self.selectedNavigationController presentViewController:nav animated:YES completion:nil];
    }
}
-(NSString*)subWord:(NSString*)str withNumber:(int)num{

    NSUInteger wordsNumber = 0;
    NSScanner *scanner = [NSScanner scannerWithString:str];
    NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    while ([scanner scanUpToCharactersFromSet:whiteSpace intoString:nil]) {
        wordsNumber ++;
    }
    
    NSString *sPartForTitle = nil;
    NSMutableString *stringMutable = [[NSMutableString alloc] initWithString:str] ;
    if (wordsNumber > num) {
        
        if ([stringMutable hasPrefix:@"“"]) { //s这个英文句子开始处可能存在着引号
            
            
            [stringMutable deleteCharactersInRange:NSMakeRange(0, 1)];
        }
        if ([stringMutable hasSuffix:@"”"]) {//s这个英文句子结尾处可能存在着引号
            
            [stringMutable deleteCharactersInRange:NSMakeRange([stringMutable length]-1, 1)];
        }
        
        NSArray *sWords = [stringMutable componentsSeparatedByString:@" "];
        sPartForTitle = [[[[[[[[[[[[[[[[sWords objectAtIndex:0] stringByAppendingString:@" "] stringByAppendingString:[sWords objectAtIndex:1]  ] stringByAppendingString:@" "] stringByAppendingString:[sWords objectAtIndex:2]] stringByAppendingString:@" "] stringByAppendingString:[sWords objectAtIndex:3]] stringByAppendingString:@" "] stringByAppendingString:[sWords objectAtIndex:4]] stringByAppendingString:@" "] stringByAppendingString:[sWords objectAtIndex:5]] stringByAppendingString:@" "] stringByAppendingString:[sWords objectAtIndex:6]] stringByAppendingString:@" "] stringByAppendingString:[sWords objectAtIndex:7]] stringByAppendingString:@"..."];
    } else {
        sPartForTitle = stringMutable;
    }
    return sPartForTitle;

}
//-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options{
//    
//    if ([url.scheme isEqualToString:@"com.googleusercontent.apps.579100815297-f9g0rjctic5mjogchklfj4nndclj14r7"]) {
//        return [[GIDSignIn sharedInstance] handleURL:url
//                                   sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
//                                          annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
//        
//        
//    }
//    return YES;
//}

//- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
//    BOOL handledURL = [[UBSDKRidesAppDelegate sharedInstance] application:app openURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey] annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
//    if (!handledURL) {
//        // Other URL logic
//    }
//    
//    return true;
//}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    
    if(url!=nil){
        NSLog(@"return url2 is:%@",url);
        NSString *urlString = [url absoluteString];
        NSString *urlScheme = [url scheme];
        NSLog(@"urlScheme = %@",urlScheme);
        if([urlScheme isEqualToString:@"qraved731842943"]){
            
            [self jumpToAppWithUrl:urlString andNotificationType:0 andNotificationId:nil andLandId:nil];
        }
        else if ([urlScheme isEqualToString:@"qravedjumphome"])
        {
            
        }else if ([urlScheme isEqualToString:@"com.googleusercontent.apps.579100815297-f9g0rjctic5mjogchklfj4nndclj14r7"]){
            return [[GIDSignIn sharedInstance] handleURL:url
                                       sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                              annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
        }else{
            return [[FBSDKApplicationDelegate sharedInstance] application:app
                                                                  openURL:url
                                                        sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                               annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
                    ];
        }
    }
    
    return YES;
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
//    BOOL handledURL = [[UBSDKRidesAppDelegate shared] application:application open:url sourceApplication:sourceApplication annotation:annotation];
//    if (!handledURL) {
//        // Other URL logic
//    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isAppTerminated"]) {
        [[Amplitude instance] logEvent:@"GN - Open App" withEventProperties:@{@"Source":@"From Mobile Web"}];
        isOpenFromMobileWeb=YES;

    }

    if(url!=nil){
        NSLog(@"return url2 is:%@",url);
        NSString *urlString = [url absoluteString];
        NSString *urlScheme = [url scheme];
        NSLog(@"urlScheme = %@",urlScheme);
        if([urlScheme isEqualToString:@"qraved731842943"]){
            
            [self jumpToAppWithUrl:urlString andNotificationType:0 andNotificationId:nil andLandId:nil];
        }
        else if ([urlScheme isEqualToString:@"qravedjumphome"])
        {
            
        }else if ([urlScheme isEqualToString:@"com.googleusercontent.apps.579100815297-f9g0rjctic5mjogchklfj4nndclj14r7"]){
            return [[GIDSignIn sharedInstance]handleURL:url sourceApplication:sourceApplication annotation:annotation];
        }else{
            return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];;
        }
    }
    
    return YES;
}


- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    if(url!=nil){
        NSLog(@"return url2 is:%@",url);
        NSString *urlScheme = [url scheme];
        if([urlScheme isEqualToString:@"qraved731842943"]){
            NSString *code = [[url query]substringFromIndex:5];
            if(!code) return NO;
            NSLog(@"return code is:%@",code);
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
            
            [parameters setObject:@"authorization_code" forKey:@"grant_type"];
            [parameters setObject:QRAVED_PATH_CLIENT_ID forKey:@"client_id"];
            [parameters setObject:QRAVED_PATH_CLIENT_SECRET forKey:@"client_secret"];

            [parameters setObject:code forKey:@"code"];
            [[IMGNetWork sharedManager] POST:@"oauth2/access_token" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                NSLog(@"**********responseObject%@",responseObject);
                NSString *userId = [responseObject objectForKey:@"user_id"];
                NSString *accessToken = [responseObject objectForKey:@"access_token"];
                [[NSUserDefaults standardUserDefaults] setObject:userId forKey:@"PATH_USERID"];
                [[NSUserDefaults standardUserDefaults] setObject:accessToken forKey:QRAVED_PATH_TOKEN_KEY];

            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                
            }];
//            [ [AFHTTPRequestOperationManager pathmanager] POST:@"oauth2/access_token" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                NSLog(@"**********responseObject%@",responseObject);
//                NSString *userId = [responseObject objectForKey:@"user_id"];
//                NSString *accessToken = [responseObject objectForKey:@"access_token"];
//                [[NSUserDefaults standardUserDefaults] setObject:userId forKey:@"PATH_USERID"];
//                [[NSUserDefaults standardUserDefaults] setObject:accessToken forKey:QRAVED_PATH_TOKEN_KEY];
//            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                NSLog(@"**********%@",error.localizedDescription);
//            }];

        }else{
           // return [FBSession.activeSession handleOpenURL:url];
        }
    }
    
    return YES;
}
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler
{
    NSDictionary * userInfo = notification.request.content.userInfo;
    UNNotificationRequest *request = notification.request; // 收到推送的请求
    UNNotificationContent *content = request.content; // 收到推送的消息内容
    NSNumber *badge = content.badge;  // 推送消息的角标
    NSString *body = content.body;    // 推送消息体
    UNNotificationSound *sound = content.sound;  // 推送消息的声音
    NSString *subtitle = content.subtitle;  // 推送消息的副标题
    NSString *title = content.title;  // 推送消息的标题
    
    if([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]) {
        NSLog(@"iOS10 前台收到远程通知");
        
    }
    else {
        // 判断为本地通知
        NSLog(@"iOS10 前台收到本地通知:{\\\\nbody:%@，\\\\ntitle:%@,\\\\nsubtitle:%@,\\\\nbadge：%@，\\\\nsound：%@，\\\\nuserInfo：%@\\\\n}",body,title,subtitle,badge,sound,userInfo);
    }
    completionHandler(UNNotificationPresentationOptionBadge|UNNotificationPresentationOptionSound|UNNotificationPresentationOptionAlert); // 需要执行这个方法，选择是否提醒用户，有Badge、Sound、Alert三种类型可以设置

}
//-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler
//{
//    
//    
//    
//    completionHandler();  // 系统要求执行这个方法
//    
//    
//}

//获取当前屏幕显示的viewcontroller
- (UIViewController *)getCurrentVC
{
    UIViewController *result = nil;
    
    UIWindow * window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal)
    {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(UIWindow * tmpWin in windows)
        {
            if (tmpWin.windowLevel == UIWindowLevelNormal)
            {
                window = tmpWin;
                break;
            }
        }
    }
    
    UIView *frontView = [[window subviews] objectAtIndex:0];
    id nextResponder = [frontView nextResponder];
    
    if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;
    else
        result = window.rootViewController;
    
    return result;
}

+(AppDelegate *)ShareApp
{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}

-(void)installUncaughtExceptionHandler
{
    InstallUncaughtExceptionHandler();
}

-(void)readException
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *directoryPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = [directoryPaths objectAtIndex:0];
    
    NSString *filePath = [documentDirectory stringByAppendingPathComponent:@"Exception.txt"];
    
    if ([fileManager fileExistsAtPath:filePath]) {
        NSData* reader = [NSData dataWithContentsOfFile:filePath];
        NSString *exceptionString= [[NSString alloc] initWithData:reader encoding:NSUTF8StringEncoding];
        
        NSDictionary *parameters=[NSMutableDictionary dictionary];
        [parameters setValue:exceptionString forKey:@"crashText"];
        
        [[IMGNetWork sharedManager] POST:@"crash/report" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
            NSLog(@"crash report has send to service.");
        } failure:^(NSURLSessionDataTask *operation, NSError *error) {
            
        }];
        [fileManager removeItemAtPath:filePath error:nil];
        
    }
}




-(void)deleteSQLite:(NSString *)fileName
{
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:fileName];
        if([[NSFileManager defaultManager] fileExistsAtPath:filePath]){
            [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
        }
}
#pragma mark google分析
-(void)initGAI
{
    NSDictionary *appDefaults = @{kAllowTracking: @(YES)};
    [[NSUserDefaults standardUserDefaults] registerDefaults:appDefaults];
    id<GAITracker> tracker = [[GAI sharedInstance] trackerWithTrackingId:kTrackingId];

    [GAI sharedInstance].optOut =
    ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
    [GAI sharedInstance].dispatchInterval = 120;
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    tracker.allowIDFACollection = YES;
//    [[GAI sharedInstance] trackerWithTrackingId:kTrackingId];
    
}
-(void)initPath{
    [[NXOAuth2AccountStore sharedStore]
     setClientID:QRAVED_PATH_CLIENT_ID
     secret:QRAVED_PATH_CLIENT_SECRET
     authorizationURL:[NSURL URLWithString:@"https://partner.path.com/oauth2/authenticate"]
     tokenURL:[NSURL URLWithString:@"https://partner.path.com/oauth2/access_token"]
     redirectURL:[NSURL URLWithString:@"qraved731842943://www.qraved.com/oauth2/redirect"]
     forAccountType:@"Path"];
}
#pragma mark Amplitude分析
- (void)initAmplitude
{
    [[Amplitude instance] initializeApiKey:QRAVED_AMPLITUDE_API_KEY];
//    [[Amplitude instance] logEvent:@"EVENT_IDENTIFIER_HERE"];
    
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        [[Amplitude instance] setUserId:[NSString stringWithFormat:@"%@",user.userId]];
        
        NSMutableDictionary *userProperties = [NSMutableDictionary dictionary];
        [userProperties setValue:user.email forKey:@"email"];
        [userProperties setValue:user.firstName forKey:@"firstName"];
        [userProperties setValue:user.gender forKey:@"gender"];
        [userProperties setValue:user.lastName forKey:@"lastName"];
        [[Amplitude instance] setUserProperties:userProperties];
        
        self.isAlreadySetAmplitudeUserProperties = YES;
    }else{
        [[Amplitude instance] setUserId:nil];
        [[Amplitude instance] clearUserProperties];
        [AppDelegate ShareApp].isAlreadySetAmplitudeUserProperties = NO;
    }
}
- (void)initZopimChat
{
    [self styleApp];
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    // Chat setup
    ////////////////////////////////////////////////////////////////////////////////////////////////
    
    // apply appearance styling first if you want to customise the look of the chat
    [ChatStyling applyStyling];
    
    // configure account key and pre-chat form
    [ZDCChat initializeWithAccountKey:@"1fT2nteoYMWyaq8ENDrtEEcckY7VDIwa"];
//    [ZDCChat configure:^(ZDCConfig *defaults) {
//        
//        defaults.accountKey = @"1fT2nteoYMWyaq8ENDrtEEcckY7VDIwa";
//        defaults.preChatDataRequirements.name = ZDCPreChatDataOptionalEditable;
//        defaults.preChatDataRequirements.email = ZDCPreChatDataOptionalEditable;
//        defaults.preChatDataRequirements.phone = ZDCPreChatDataOptionalEditable;
//        defaults.preChatDataRequirements.department = ZDCPreChatDataOptionalEditable;
//        defaults.preChatDataRequirements.message = ZDCPreChatDataOptional;
//        
//    }];
    
    // Uncomment to disable visitor data persistence between application runs
    //[[ZDCChat instance].session visitorInfo].shouldPersist = NO;
    
    // Uncomment if you don't want open chat sessions to be automatically resumed on application launch
    //[ZDCChat instance].shouldResumeOnLaunch = NO;
    
    // remember to switch off debug logging before app store submission!
    [ZDCLog enable:YES];
    [ZDCLog setLogLevel:ZDCLogLevelWarn];
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    // sample app boiler plate
    ////////////////////////////////////////////////////////////////////////////////////////////////
    
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);

}
#pragma mark AppsFlyer分析
- (void)initAppsFlyer
{
    [AppsFlyerTracker sharedTracker].appsFlyerDevKey = QRAVED_APPSFLYER_API_KEY;
    [AppsFlyerTracker sharedTracker].appleAppID = @"731842943";
    [AppsFlyerTracker sharedTracker].disableAppleAdSupportTracking = YES;
}

//#pragma mark Mixpanel分析
//-(void)initMixpanel
//{
//    [Mixpanel sharedInstanceWithToken:MIXPANEL_TOKEN];
//    [Mixpanel sharedInstance];
//
//}

-(void)loadUser
{
    DBManager * manager = [DBManager manager];
    if(![manager isExistsTable:NSStringFromClass(IMGUser.class)]){
        [manager createTableWithClass:[IMGUser class]];
    }
    
    IMGUser *user = [IMGUser currentUser];
    if (user.userId !=nil && user.token !=nil) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"userLogined"];
    }else{
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"userLogined"];
    }
    
    //loading user
    BOOL logined = [[NSUserDefaults standardUserDefaults] boolForKey:@"userLogined"];
    
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
    if(logined){
        NSString * loginUserId = [[NSUserDefaults standardUserDefaults] objectForKey:@"loginUserId"];
        NSString * selectSql = [NSString stringWithFormat:@"select * from IMGUser where userId = %@",loginUserId];
        FMResultSet *resultSet = [manager executeQuery:selectSql];
        if([resultSet next]){
            IMGUser * user = [IMGUser currentUser];
            [user setValueWithResultSet:resultSet];
            
            NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
            [shared setObject:user.userId forKey:@"userId"];
            [shared setObject:user.token forKey:@"userToken"];
            [shared synchronize];

            
            [[NSUserDefaults standardUserDefaults] setObject:user.userId forKey:@"loginUserId"];
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
            [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"needLogin"];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"loginNotification" object:nil];

            self.userLogined = YES;
            [resultSet close];
        }
        else if ([NSKeyedUnarchiver unarchiveObjectWithData:[shared objectForKey:@"person"]])
        {
            IMGUser * user = [IMGUser currentUser];
            user=(IMGUser*)[NSKeyedUnarchiver unarchiveObjectWithData:[shared objectForKey:@"person"]];
            [[DBManager manager] insertModel:user update:YES selectKey:@"userId" andSelectValue:user.userId];
            [shared setObject:user.userId forKey:@"userId"];
            [shared setObject:user.token forKey:@"userToken"];
            [shared synchronize];
            
            
            [[NSUserDefaults standardUserDefaults] setObject:user.userId forKey:@"loginUserId"];
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
            [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"needLogin"];
            self.userLogined = YES;
            logined = YES;
            [[NSNotificationCenter defaultCenter]postNotificationName:@"loginNotification" object:nil];

            [resultSet close];

        }
        else
        {
            logined = NO;
        }
    }
    if(!logined){
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"loginUserId"];
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"userLogined"];
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"needLogin"];
        NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
        [shared removeObjectForKey:@"userId"];
        [shared removeObjectForKey:@"userToken"];
        [shared removeObjectForKey:@"person"];
        [shared synchronize];

    }
    self.userLogined=[[NSUserDefaults standardUserDefaults] boolForKey:@"userLogined"];
}

#pragma mark  eventSplash
-(void)requestEventSplash
{
    NSString *urlString = [NSString stringWithFormat:@"%@events4splash",QRAVED_WEB_SERVICE_SERVER];
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];
    [request setHTTPMethod:@"GET"];
    NSData *received = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    if (received) {
        NSDictionary *returnJson=[NSJSONSerialization JSONObjectWithData:received
                                                                 options: NSJSONReadingMutableContainers
                                                                   error:nil];
        if (returnJson) {
            self.eatjktData = [[NSMutableArray alloc]init];
            for (NSDictionary *dict in returnJson) {
                [self.eatjktData addObject:dict];
            }
            for (int i=0; i<self.eatjktData.count; i++) {
                NSInteger sortOrder1 = [[[self.eatjktData objectAtIndex:i] objectForKey:@"sortOrder"] intValue];
                for (int j=i+1; j<returnJson.count; j++) {
                    NSInteger sortOrder2 = [[[self.eatjktData objectAtIndex:j] objectForKey:@"sortOrder"] intValue];
                    if (sortOrder1>sortOrder2) {
                        [self.eatjktData exchangeObjectAtIndex:i withObjectAtIndex:j];
                    }
                    
                }
            }
        }
    }

}
-(BOOL)isShowEvent{
    if (!self.eatjktData) {
        return NO;
    }
    if (self.eatjktData==nil) {
        return NO;
    }
    self.splashsArray = [[NSMutableArray alloc]init];
    for (int i=0;i<self.eatjktData.count;i++) {
        NSDictionary *dict = [self.eatjktData objectAtIndex:i];
        NSString *urlString = [dict objectForKey:@"splash"];
        if (![urlString isEqualToString:@""] && ![urlString isEqualToString:[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:@"&width=640.000000"]]) {
            NSString *startDateS = [[NSString stringWithFormat:@"%@",[dict objectForKey:@"startDate"]] substringToIndex:10];
            NSString *endDateS = [[NSString stringWithFormat:@"%@",[dict objectForKey:@"endDate"]]substringToIndex:10];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSDate *startdate = [dateFormatter dateFromString:startDateS];
            NSDate *enddate = [dateFormatter dateFromString:endDateS];
            //
            
            NSDate *date= [dateFormatter dateFromString: [dateFormatter stringFromDate:[NSDate date]]];
            if (([[startdate earlierDate:date] isEqualToDate:startdate] && [[enddate laterDate:date] isEqualToDate:enddate]) ) {
                [self.splashsArray addObject:dict];
            }
            
        }
        
    }
    if (self.splashsArray.count) {
        return NO;
    }
    else
    {
        return NO;
    }
}

#pragma mark 设置navigation
-(void)initAppearance
{
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isShowSplashView"];
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isHadNotification"];
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isHadHomeFirstData"];

    //init navigation
    NSDictionary *navbarTitleTextAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIColor color333333],NSForegroundColorAttributeName,[UIFont boldSystemFontOfSize:17],NSFontAttributeName, nil];
    [[UINavigationBar appearance] setTitleTextAttributes:navbarTitleTextAttributes];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    if(floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1){
        [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }else{
        [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    }
    //home
    self.homeSlideViewController=[[HomeSlideViewController alloc]init];
    homeNavigationController=[[MLNavigationController alloc] initWithRootViewController:self.homeSlideViewController];
//    self.homeViewController = [[V2_HomeViewController alloc] init];
//    homeNavigationController = [[MLNavigationController alloc] initWithRootViewController:self.homeViewController];
    //saved
    self.savedViewController = [[SavedViewController alloc] init];
    savedNavigationController =[[MLNavigationController alloc] initWithRootViewController:self.savedViewController];

    //search
    self.discoverListViewController = [[DiscoverListViewController alloc] initWithCloseButton];
    self.discoverListViewController.isFromTabMenu = YES;
    self.discoverListViewController.isSearchJournal = NO;
    self.discoverListViewController.isJournalSearch = NO;
    discoverListNavigationController =[[MLNavigationController alloc] initWithRootViewController:self.discoverListViewController];
    
    self.contributionController = [[ContributionController alloc] init];
    contributionNavigationController = [[MLNavigationController alloc] initWithRootViewController:self.contributionController];
    
    //notification
    self.notificationsViewController = [[NotificationsViewController alloc] init];
    self.notificationsViewController.isFromTabMenu = YES;
    notificationsNavigationController = [[MLNavigationController alloc] initWithRootViewController:self.notificationsViewController];
    
    //time line
    self.feedViewcontroller = [[FeedViewController alloc] init];
    feedNavifationController = [[MLNavigationController alloc] initWithRootViewController:self.feedViewcontroller];
    
    //profile
    self.myProfileViewController = [[ProfileViewController alloc] init];
    self.myProfileViewController.isFromTabMenu = YES;
    ProfileNavigationController=[[MLNavigationController alloc] initWithRootViewController:self.myProfileViewController];
    
    selectedNavigationController =homeNavigationController;
    //UITabBarController
    tabMenuViewController = [[RDVTabBarController alloc] init];
    tabMenuViewController.delegate = self;
    tabMenuViewController.viewControllers = [NSArray arrayWithObjects:homeNavigationController,savedNavigationController,contributionNavigationController,feedNavifationController,ProfileNavigationController, nil];
    NSArray *selectedArr = @[@"ic_home_active",@"ic_heart_active",@"ic_contribution_active",@"ic_timeline_active",@"ic_profile_active"];
    NSArray *unselectedArr = @[@"ic_home_idle",@"ic_heart_idle",@"ic_contribution_idle",@"ic_timeline_idle",@"ic_profile_idle"];
    
    NSInteger index = 0;
    for (RDVTabBarItem *item in [[tabMenuViewController tabBar] items]) {

        UIImage *selectedimage = [UIImage imageNamed:[selectedArr objectAtIndex:index]];
        UIImage *unselectedimage = [UIImage imageNamed:[unselectedArr objectAtIndex:index]];
        [item setFinishedSelectedImage:selectedimage withFinishedUnselectedImage:unselectedimage];
        
        if (index==2) {
           centerItem=item;
        }
        if (index==3) {
            item.badgePositionAdjustment = UIOffsetMake(-6, 12);
            [self getNotificationCount:item];
        }
        
        index++;
    }
    
    
    BOOL isNewString = [[NSUserDefaults standardUserDefaults] boolForKey:@"isfirstDownLoadAPP"];//no
    BOOL isFirstOpen=[[NSUserDefaults standardUserDefaults] boolForKey:@"isFirstOpen"];//yes
     
    if (!isNewString && isFirstOpen) {
        
        IMGUser *user = [IMGUser currentUser];
        if ((!user.userId) || (!user.token)){
            LoginViewController *login = [[LoginViewController alloc]init];
            login.isFromonboard = YES;
            UINavigationController *loginNav=[[UINavigationController alloc]initWithRootViewController:login];
            self.window.rootViewController=  loginNav;
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isfirstDownLoadAPP"];
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstNew"];
        }else{
//            self.window.rootViewController=tabMenuViewController;
            V2_PreferenceViewController *V2_PreferenceVC = [[V2_PreferenceViewController alloc] init];
            V2_PreferenceVC.isFromProfile = YES;
            V2_PreferenceVC.isHaveOnBoarding = YES;
            self.window.rootViewController=V2_PreferenceVC;
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isfirstDownLoadAPP"];
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstNew"];
        }
    }else if (isFirstOpen && isNewString){
        IMGUser *user = [IMGUser currentUser];
        if ((!user.userId) || (!user.token)){
            LoginViewController *login = [[LoginViewController alloc]init];
            login.isFromonboard = YES;
            UINavigationController *loginNav=[[UINavigationController alloc]initWithRootViewController:login];
            self.window.rootViewController=  loginNav;
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstNew"];
        }else{
            self.window.rootViewController=tabMenuViewController;
//            V2_PreferenceViewController *V2_PreferenceVC = [[V2_PreferenceViewController alloc] init];
//            V2_PreferenceVC.isFromProfile = YES;
//            V2_PreferenceVC.isHaveOnBoarding = YES;
//            self.window.rootViewController=V2_PreferenceVC;
            
            [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstNew"];
        }
    }else{
        LoginViewController *login = [[LoginViewController alloc]init];
        login.isFromonboard = YES;
        UINavigationController *loginNav=[[UINavigationController alloc]initWithRootViewController:login];
        self.window.rootViewController = loginNav;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstOpen"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isfirstDownLoadAPP"];
         [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstNew"];
    }
    
    self.window.backgroundColor = [UIColor clearColor];
    [self.window makeKeyAndVisible];
}
- (void)getNotificationCount:(RDVTabBarItem*)item
{
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        NSDictionary *params = [[NSDictionary alloc] initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t", nil];
        [NotificationHandler getNotificationCountWithParams:params andBlock:^(NSNumber *notificationCount,NSString *exceptionmsg) {
            if([exceptionmsg isLogout]){
            }else{
                
                NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:kRemoteNotificationDeviceToken];
                [UIApplication sharedApplication].applicationIconBadgeNumber =[notificationCount integerValue];
                if (deviceToken!=nil) {
                    NSMutableDictionary *pargrammer=[[NSMutableDictionary alloc]init];
                    [pargrammer setObject:deviceToken forKey:@"deviceToken"];
                    [pargrammer setObject:notificationCount forKey:@"badge"];
                    [NotificationHandler updateNotificationCountWithParams:pargrammer andBlock:^(id responseObject) {
                        
                    }];

                }
               
                _notificationCount = notificationCount;
                item.badgeBackgroundColor = [UIColor redColor];
                
                if ([_notificationCount intValue]>0){
                    item.badgeValue = [NSString stringWithFormat:@"%@",notificationCount];
                }else{
                    item.badgeValue = @"";
                }
            }  
        }];
    }
    else
    {
//        NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:kRemoteNotificationDeviceToken];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:kRemoteNotificationDeviceToken];
        if (deviceToken!=nil) {
            NSMutableDictionary *pargrammer=[[NSMutableDictionary alloc]init];
            [pargrammer setObject:deviceToken forKey:@"deviceToken"];
            [pargrammer setObject:[NSNumber numberWithInt:0] forKey:@"badge"];
            [NotificationHandler updateNotificationCountWithParams:pargrammer andBlock:^(id responseObject) {
                
            }];
            
        }
        item.badgeValue = @"";
    }
}

- (BOOL)tabBarController:(RDVTabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    NSUInteger index = [tabBarController.viewControllers indexOfObject:viewController];
    BOOL ifCanJump = YES;
    for (RDVTabBarItem *item in [[tabMenuViewController tabBar] items]) {
        if (index==3) {
            item.badgeValue = @"";
        }
    }
    NSString *location = @"";
    if ([selectedNavigationController isEqual:homeNavigationController]) {
        location = @"home page";
    }else if ([selectedNavigationController isEqual:savedNavigationController]) {
        location = @"search page";
    }else if ([selectedNavigationController isEqual:feedNavifationController]) {
        location = @"feed page";
    }else if ([selectedNavigationController isEqual:ProfileNavigationController]) {
        location = @"profile page";
    }
    if (index==1) {
        IMGUser *user = [IMGUser currentUser];
        if ((!user.userId) || (!user.token)){
            [[AppDelegate ShareApp]goToLoginControllerWithindex:1];
            ifCanJump = NO;
        }

    }
    if (index == 3) {
        [[Amplitude instance] logEvent:@"CL – Bottom Bar Feed" withEventProperties:@{}];
        IMGUser *user = [IMGUser currentUser];
        if ((!user.userId) || (!user.token)){
            [[AppDelegate ShareApp]goToLoginControllerWithindex:3];
            ifCanJump = NO;
        }
    }
    if (index==4) {
        [[Amplitude instance] logEvent:@"CL – Bottom Bar Profile" withEventProperties:@{}];
        IMGUser *user = [IMGUser currentUser];
        if ((!user.userId) || (!user.token)){
            [[AppDelegate ShareApp]goToLoginControllerWithindex:4];
            ifCanJump = NO;
        }

    }
    if (ifCanJump) {
        if (index == 0) {
            [[Amplitude instance] logEvent:@"CL – Bottom Bar Homepage" withEventProperties:@{}];
//            if ([selectedNavigationController isEqual:homeNavigationController]) {
//                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_TAB_MENU object:nil];
//            }
            selectedNavigationController =homeNavigationController;
        }else if (index == 1){
            [[Amplitude instance] logEvent:@"CL – Bottom Bar Saved" withEventProperties:@{}];
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_TAB_SAVED object:nil];
            selectedNavigationController =savedNavigationController;
            
        }else if (index == 2){
            [[Amplitude instance] logEvent:@"CL – Hot Key Initiate" withEventProperties:@{}];
            selectedNavigationController = contributionNavigationController;
//            [self loadHotkeyView];
//            return NO;
        }else if (index == 3){
//            if ([selectedNavigationController isEqual:timeLineNavifationController]) {
//                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_TAB_NOTIFICATION object:nil];
//            }
            selectedNavigationController =feedNavifationController;
        }else if (index == 4){
//            if ([selectedNavigationController isEqual:ProfileNavigationController]) {
//                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_TAB_PROFILE object:nil];
//            }else{
////                [self.myProfileViewController setCurrentTabIndex:0];
//            }
            selectedNavigationController =ProfileNavigationController;
        }
    }
    return ifCanJump;
}
-(void)loadHotkeyView{
    centerItem.hidden=YES;
    UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    effectview = [[UIVisualEffectView alloc] initWithEffect:blur];
    effectview.frame =CGRectMake(0, DeviceHeight-DeviceWidth+DeviceWidth/2+20, DeviceWidth, DeviceWidth);
    effectview.layer.cornerRadius=DeviceWidth/2;
    effectview.clipsToBounds=YES;
    effectview.alpha=0.3;
    UITapGestureRecognizer* tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapgesture:)];
    [effectview addGestureRecognizer:tap];
    [self.window addSubview:effectview];
    
    [UIView animateWithDuration:0.1 animations:^{
        
        effectview.transform=CGAffineTransformScale(effectview.transform, 3.8, 3.8);
        effectview.alpha=1;
        
        
    }];
    centerImageview=[[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth/2-21, DeviceHeight-26, 41, 43)];
    centerImageview.image=[UIImage imageNamed:@"plus icon"];
    [self.window addSubview:centerImageview];
    
    
    rwimageView=[[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth/2-22, DeviceHeight-80, 46, 46)];
    rwimageView.image=[UIImage imageNamed:@"write review"];
    rwimageView.alpha=0;
    rwimageView.userInteractionEnabled=YES;
    rwimageView.tag=100;
    rwimageView.layer.cornerRadius=23;
    rwlable=[[UILabel alloc]initWithFrame:CGRectMake(rwimageView.startPointX-10, rwimageView.endPointY+2, 80, 18)];
    rwlable.text=@"Write Review";
    rwlable.alpha=0;
    rwlable.font=[UIFont boldSystemFontOfSize:13];
    [self.window addSubview:rwlable];

    
    uploadImageView=[[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth/2-22, DeviceHeight-80, 46, 46)];
    uploadImageView.tag=101;
    uploadImageView.image=[UIImage imageNamed:@"upload photo"];
    uploadImageView.alpha=0;
    uploadImageView.userInteractionEnabled=YES;
    uploadImageView.layer.cornerRadius=23;
    
    uploadLable=[[UILabel alloc]initWithFrame:CGRectMake(uploadImageView.startPointX-10, uploadImageView.endPointY+2, 80, 18)];
    uploadLable.text=@"Upload Photo";
    uploadLable.alpha=0;
    uploadLable.font=[UIFont boldSystemFontOfSize:13];
    [self.window addSubview:uploadLable];

    
    listImageView=[[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth/2-22, DeviceHeight-80, 46, 46)];
    listImageView.alpha=0;
    listImageView.tag=102;
    listImageView.userInteractionEnabled=YES;
    listImageView.layer.cornerRadius=23;
    listImageView.image=[UIImage imageNamed:@"my list"];
    listLoadLable=[[UILabel alloc]initWithFrame:CGRectMake(listImageView.startPointX, listImageView.endPointY+2, 80, 18)];
    listLoadLable.text=@"My List";
    listLoadLable.alpha=0;
    listLoadLable.font=[UIFont boldSystemFontOfSize:13];
    [self.window addSubview:listLoadLable];
    [self.window addSubview:rwimageView];
    [self.window addSubview:uploadImageView];
    [self.window addSubview:listImageView];
    [UIView animateWithDuration:0.3 animations:^{
        rwimageView.alpha=1;
        rwimageView.frame=CGRectMake(centerImageview.frame.origin.x-80, centerImageview.frame.origin.y-68 , 50, 50);
        UITapGestureRecognizer* rwimageHotkeyTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hotkeygesture:)];
        [rwimageView addGestureRecognizer:rwimageHotkeyTap];
        rwlable.frame=  CGRectMake(rwimageView.startPointX-18, rwimageView.endPointY+2, 85, 18);
        rwlable.alpha=1;
        
        uploadImageView.alpha=1;
        uploadImageView.frame=CGRectMake(centerImageview.frame.origin.x-3, centerImageview.frame.origin.y-135 , 50, 50);
        UITapGestureRecognizer* uploadHotkeyTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hotkeygesture:)];
        [uploadImageView addGestureRecognizer:uploadHotkeyTap];
        uploadLable.frame=CGRectMake(uploadImageView.startPointX-18, uploadImageView.endPointY+2, 86, 18);
        uploadLable.alpha=1;
        
        listImageView.alpha=1;
        listImageView.frame=CGRectMake(centerImageview.frame.origin.x+75, centerImageview.frame.origin.y-68 , 50, 50);
        UITapGestureRecognizer* listHotkeyTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hotkeygesture:)];
        [listImageView addGestureRecognizer:listHotkeyTap];
        listLoadLable.frame=CGRectMake(listImageView.startPointX+5, listImageView.endPointY+2, 85, 18);
        listLoadLable.alpha=1;
    }];
    
    
    
}
-(void)removehotkeyBtn{

    [effectview removeFromSuperview];
    [centerImageview removeFromSuperview];
    [rwimageView removeFromSuperview];
    [rwlable removeFromSuperview];
    [uploadLable removeFromSuperview];
    [uploadImageView removeFromSuperview];
    [listImageView removeFromSuperview];
    [listLoadLable removeFromSuperview];
    centerItem.hidden=NO;



}
-(void)hotkeygesture:(UITapGestureRecognizer*)tap{


    UIView* view=tap.view;
    switch (view.tag) {
        case 100:{
            ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc] init];
            publishReviewVC.isFromHotkey = YES;
            [self removehotkeyBtn];
            publishReviewVC.amplitudeType = @"Homepage CTA";
            UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:publishReviewVC];
            [self.selectedNavigationController presentViewController:navigationController animated:YES completion:nil];
        }
            break;
        case 101:
        {
        
            ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc] initWithRestaurantTitle:nil andIsFromUploadPhoto:YES andIsUploadMenuPhoto:NO andCurrentRestaurantId:nil];
            picker.publishViewController = self.window.rootViewController;
            picker.maximumNumberOfSelection = 9;
            picker.assetsFilter = [ALAssetsFilter allPhotos];
            picker.showEmptyGroups=NO;
            picker.delegate = self;
            picker.isFromUploadPhoto = YES;
            picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
                if ([[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
                    NSTimeInterval duration = [[(ALAsset*)evaluatedObject valueForProperty:ALAssetPropertyDuration] doubleValue];
                    return duration >= 5;
                } else {
                    return YES;
                }
            }];
            [self removehotkeyBtn];
            [self.selectedNavigationController presentViewController:picker animated:YES completion:nil];
            

        
        
        
        }
            break;
        case 102:
        {
            [self removehotkeyBtn];
            if( [AppDelegate ShareApp].userLogined)
            {
                self.tabMenuViewController.selectedIndex = 4;
                selectedNavigationController =ProfileNavigationController;
                [selectedNavigationController popToRootViewControllerAnimated:YES];
//                [self.myProfileViewController setCurrentTabIndex:0];
            }
            else
            {
                [[AppDelegate ShareApp]goToLoginController];
                
            }
            

        }

            break;
        default:
            break;
    }


}
-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets{
    
    NSMutableArray *photosArrM = [[NSMutableArray alloc] init];
    
    for (int i=0; i<assets.count; i++) {
        ALAsset *asset=assets[i];
        UIImageView *imgview=[[UIImageView alloc] init];
        imgview.contentMode=UIViewContentModeScaleAspectFill;
        imgview.clipsToBounds=YES;
        UIImage *tempImg=[UIImage imageWithCGImage:asset.defaultRepresentation.fullScreenImage];
        // [imgview setImage:tempImg];
        IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
        photo.image = tempImg;
        photo.photoUrl = [NSString stringWithFormat:@"%@",[asset valueForProperty:ALAssetPropertyAssetURL]];

        [photosArrM addObject:photo];
    }
    
    [picker dismissViewControllerAnimated:NO completion:^{
        ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc]initWithRestaurant:nil andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
        publishReviewVC.isFromHotkey = YES;
        publishReviewVC.amplitudeType = @"Existing photo on device image gallery";
        publishReviewVC.isUploadPhoto = YES;
        publishReviewVC.photosArrM = [NSMutableArray arrayWithArray:photosArrM];
        publishReviewVC.isFromOnboard = NO;
        publishReviewVC.ifSuccessLoginClickPost = YES;
        [self.selectedNavigationController presentViewController:[[UINavigationController alloc] initWithRootViewController:publishReviewVC] animated:YES completion:nil];
        
        
    }];
}

-(void)tapgesture:(UITapGestureRecognizer*)gesture{
    NSString *location = @"";
    if ([selectedNavigationController isEqual:homeNavigationController]) {
        location = @"home page";
    }else if ([selectedNavigationController isEqual:savedNavigationController]) {
        location = @"saved page";
    }else if ([selectedNavigationController isEqual:notificationsNavigationController]) {
        location = @"notification page";
    }else if ([selectedNavigationController isEqual:ProfileNavigationController]) {
        location = @"profile page";
    }
    [[Amplitude instance] logEvent:@"CL - Hotkey Close" withEventProperties:@{@"Location":location}];
    [UIView animateWithDuration:0.3 animations:^{
        effectview.transform=CGAffineTransformIdentity;
        effectview.alpha=0;
        centerImageview.alpha=0;
        rwimageView.frame=CGRectMake(DeviceWidth/2-30, DeviceHeight-80, 60, 60);
        rwimageView.alpha=0;
        rwlable.alpha=0;
        rwlable.frame=CGRectMake(rwimageView.startPointX-10, rwimageView.endPointY+2, 80, 18);

        uploadImageView.frame=CGRectMake(DeviceWidth/2-30, DeviceHeight-80, 60, 60);
        uploadImageView.alpha=0;
        uploadLable.alpha=0;
        uploadLable.frame=CGRectMake(uploadImageView.startPointX-10, uploadImageView.endPointY+2, 80, 18);
        listImageView.frame=CGRectMake(DeviceWidth/2-30, DeviceHeight-80, 60, 60);
        listImageView.alpha=0;
        listLoadLable.alpha=0;
        listLoadLable.frame=CGRectMake(listImageView.startPointX, listImageView.endPointY+2, 80, 18);
        centerItem.hidden=NO;

    } completion:^(BOOL finished) {

        [effectview removeFromSuperview];
        
    }];
    
}

#pragma mark 检测新版本
-(void)checkNewVersion{
    VersionHelper *versionHelper = [[VersionHelper alloc]init];
    [versionHelper checkNewVersion];
}

#pragma mark 请求notificationCount
-(void)requestToShowNotificationCount
{
    if(self.userLogined){
        IMGUser *user=[IMGUser currentUser];
        if(user!=nil&&user.userId!=nil){
            //NSDictionary *filterParameters=[NSDictionary dictionaryWithObjectsAndKeys:user.userId,@"userID", user.token, @"t", @"0", @"readStatus", nil];
//            [[IMGNetWork sharedManager] GET:@"notification/count" parameters:filterParameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    NSNumber *notificationCount = [responseObject objectForKey:@"count"];
//                    if(notificationCount!=nil){
//                        [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationCountUpdatedNotification" object:nil userInfo:@{@"notificationCount":notificationCount}];
//                    }
//                });
//            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                NSLog(@"postData requestCountNotification failed,%@",error.localizedDescription);
//            }];
        }else{
        }
    }else{

    }
}

#pragma mark 清除notification
-(void)clearNotificationCount
{
    
}
#pragma mark copy imageCache
-(void)copyImageCache
{
    NSString *docPath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@%@",@"com.hackemist.SDWebImageCache.default",QRAVED_VERSION] ofType:@"zip"];
    // 沙盒Library目录
    NSString * appDir = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject];
    //appLib  Library/Caches目录
    NSString *appLib = [appDir stringByAppendingString:@"/Caches"];
    NSLog(@"appLib Caches:%@",appLib);
    BOOL filesPresent = [self copyMissingFile:docPath toPath:appLib];
    if (filesPresent) {
//        NSLog(@"OK");
    }
    else
    {
        NSLog(@"NO");
    }
    
    
#pragma mark 解压
    ZipArchive* zip = [[ZipArchive alloc] init];
    NSString* zipFile = [appDir stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@%@",@"Caches/com.hackemist.SDWebImageCache.default",QRAVED_VERSION,@".zip"]] ;
    NSString* unZipTo = [appDir stringByAppendingPathComponent:@"Caches"] ;
    if( [zip UnzipOpenFile:zipFile] ){
        BOOL result = [zip UnzipFileTo:unZipTo overWrite:YES];
        if( NO==result ){
            
        }
        [zip UnzipCloseFile];
    }

}
- (BOOL)addSkipBackupAttributeToDocumentDirectory
{
    NSURL *URL = [[NSURL alloc] initFileURLWithPath:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject]];
    
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

- (BOOL)addSkipBackupAttributeToLibraryDirectory
{
    NSURL *URL = [[NSURL alloc] initFileURLWithPath:[NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) lastObject]];
    
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [IQKeyboardManager sharedManager].enable = YES;
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;

//    [[UBSDKRidesAppDelegate shared] application:application didFinishLaunchingWithOptions:launchOptions];
//    [[UBSDKConfiguration shared] setUseFallback:NO];
//    [UBSDKConfiguration setFallbackEnabled:NO];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
        UNUserNotificationCenter* center = [UNUserNotificationCenter currentNotificationCenter];
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert + UNAuthorizationOptionSound+UNAuthorizationOptionBadge)
                              completionHandler:^(BOOL granted, NSError * _Nullable error) {
                                  if(!error)
                                  {
                                      NSLog(@"@授权成功");
                                  }
                              }];
        [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
            NSLog(@"%@",settings);
        }];
        
        center.delegate = self;
        [application registerForRemoteNotifications];
    }else{
        
        if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
            [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            [application registerForRemoteNotifications];
        }else{
            //[application registerForRemoteNotificationTypes: UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert];
        }
        
        
        
    }
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isPushSelCity"];
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"emailPushSelCity"];
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"registPushSelCity"];

    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    [GIDSignIn sharedInstance].clientID = @"579100815297-f9g0rjctic5mjogchklfj4nndclj14r7.apps.googleusercontent.com";

    AppDelegate *myDelegate = [[UIApplication sharedApplication] delegate];
    if (ScreenHeight == 480 || [UIDevice isIphone5]){
        myDelegate.autoSizeScaleX = 1.0;
        myDelegate.autoSizeScaleY = 1.0;
    }
    else if ([UIDevice isIphone6])
    {
        myDelegate.autoSizeScaleX=1.171875;
        myDelegate.autoSizeScaleY=1.17429577;
    }else if ([UIDevice isIphone6Plus])
    {
        myDelegate.autoSizeScaleX=1.29375;
        myDelegate.autoSizeScaleY=1.2957;
    }
    else
    {
        myDelegate.autoSizeScaleX = 1.0;
        myDelegate.autoSizeScaleY = 1.0;
    }

    [self removeEcodingFile:qravedRDPCachePath];
    [self addSkipBackupAttributeToDocumentDirectory];
    [self addSkipBackupAttributeToLibraryDirectory];
    [[[DataInitialHandler alloc]init]initFromServer];
    [self createFolder:qravedRDPCachePath];
    [self createFolder:qravedGIFCachePath];
//    NSLog(@"RDPCacheFilePath = %@",RDPCacheFilePath);
    NSArray *oldDB=[NSArray arrayWithObjects:@"2",@"207",@"208",@"209",@"2010",@"2011",@"2011",@"2012",@"2013",@"2014",@"2015",@"2016",@"2017",@"2018",@"220",@"221",@"222",@"223",@"224",@"225",@"226",@"227",@"228",@"2281",@"232",@"233",@"234",@"238",@"239",@"240",@"241",@"242",@"243",@"244",@"249",@"251",@"254",@"257",@"264",@"265",@"266",@"267",@"268",@"269",@"270",@"271",@"272",@"273",@"274",@"275",@"300",@"301",@"302",@"303",@"304",@"305",@"306",@"307",@"308",@"309",@"400",@"401",@"402",@"403",@"404",@"405",@"406",@"407",@"408",@"409",@"410",@"411",nil];
   
    [oldDB enumerateObjectsUsingBlock:^(id obj, NSUInteger idx,BOOL *stop){
       
        [self deleteSQLite:[NSString stringWithFormat:@"%@qraved%@.sqlite",QRAVED_ENVIROMENT,obj]];
    }];
    
    [self readException];
    [self initGAI];
    [self initAmplitude];
    [self initAppsFlyer];
//    [self initMixpanel];
    [self initZopimChat];
    [self initPath];
    [self loadUser];

    [self initAppearance];
    [Fabric with:@[[Crashlytics class]]];


    [NdUncaughtExceptionHandler setDefaultHandler];
    [self installUncaughtExceptionHandler];
    
//    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
//        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
//        [application registerForRemoteNotifications];
//    } else {
//        [application registerForRemoteNotificationTypes: UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert];
//    }
    [self initializeRate];
    [self checkNewVersion];
    
    [self requestToShowNotificationCount];

    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    if (launchOptions != nil)
    {
        NSDictionary* dictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (dictionary != nil)
        {//Launched from push notification:
            [[Amplitude instance] logEvent:@"CL - Notifications on Device Notification page" withEventProperties:@{@"Landing Page":@"Notification"}];
            if ([dictionary objectForKey:@"layer"]) {
                
            }else{
//                [self.homeViewController presentLeftMenuViewController:nil];
                
                if (notificationStrM)
                {
                    [notificationStrM appendString:@"---didFinishLaunchingWithOptions"];
                }
                else
                {
                    notificationStrM = [[NSMutableString alloc] initWithString:@"didFinishLaunchingWithOptions"];
                }
                
            }
            
        }
    }
//Layer
    // Add support for shake gesture
    application.applicationSupportsShakeToEdit = YES;
    
//    [self initLayer];

    isFirst=YES;
    return YES;
}



- (void)application:(UIApplication*)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken--%@",deviceToken);
    //022c5979be1f573c7486ad2772099374c634508319cfd3ac05b4a4f366662bd5
//    Mixpanel *mixpanel = [Mixpanel sharedInstance];
//    [mixpanel.people addPushDeviceToken:deviceToken];
    NSString* deviceTokenStr = [[[[deviceToken description]
                                  stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                 stringByReplacingOccurrencesOfString: @">" withString: @""]
                                stringByReplacingOccurrencesOfString: @" " withString: @""];
    [PushHelper postUserDeviceToken: deviceTokenStr];
    [[NSUserDefaults standardUserDefaults] setObject:deviceTokenStr forKey:@"deviceToken"];
//    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
//    pasteboard.string = deviceTokenStr;
    
    //Layer
//    NSError *error;
//    BOOL success = [self.layerClient updateRemoteNotificationDeviceToken:deviceToken error:&error];
//    if (success) {
//        NSLog(@"Application did register for remote notifications");
//    } else {
//        NSLog(@"Error updating Layer device token for push:%@", error);
//    }
    
}

- (void)application:(UIApplication*)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSString *str = [NSString stringWithFormat: @"Error: %@", err];
    NSLog(@"didFailToRegisterForRemoteNotificationsWithError %@",str);
    
}

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);//震动
    
    for (id key in userInfo) {
        NSLog(@"didReceiveRemoteNotification---key: %@, value: %@", key, [userInfo objectForKey:key]);
    }
    
    switch (application.applicationState) {
        case UIApplicationStateActive:{
            NSLog(@"UIApplicationStateActive");//

            
        }
            break;
        case UIApplicationStateInactive:{
//            [self.homeViewController presentLeftMenuViewController:nil];
            NSDictionary *apsDic = [userInfo objectForKey:@"aps"];

            NSDictionary *alertDic=[apsDic objectForKey:@"alert"];
            NSString *value=[alertDic objectForKey:@"body"];
            value=[self subWord:value withNumber:10];
            NSNumber *type=[userInfo objectForKey:@"type"];
            NSString* typename=@"";
            switch ([type intValue]) {
                case 1:case 0:
                    typename= @"Landing Page Message";
                case 2:
                    typename= @"Splash Notification";
                case 3:
                    typename= @"Reservation";
                case 4:
                    typename= @"Restaurant info update";
                case 5:
                    typename= @"Review";
                case 6:
                    typename= @"Photo";
                    
                case 7:
                    typename=@"Photo List";
                default:
                    typename= @"unKnow type";
            }
            
            NSString *landpage=@"";
            switch ([type intValue]) {
                case 1:
                    landpage=@"Landing Page";
                case 2:
                    landpage=@"Splash Page";
                case 3:
                    landpage=@"Reservation Detial Page";
                case 4:
                    landpage=@"Restaurant Detail Page";
                case 5:
                    landpage=@"Review Card Detial Page";
                case 6:
                    landpage=@"Photo Card Detial Page";
                case 7:
                    landpage=@"Photo Card Detail Page - Photo List";
                case 0:
                    landpage=@"Notification List Page";
                default:
                    landpage=@"unKnow Page";
            }
            NSString *deviceToken=[[NSUserDefaults standardUserDefaults] objectForKey:kRemoteNotificationDeviceToken];
            NSNumber *notiId=[userInfo objectForKey:@"nid"];
            NSMutableDictionary *pargram=[[NSMutableDictionary alloc]init];
            [pargram setValue:landpage forKey:@"LandingPage"];
            [pargram setValue:typename forKey:@"Type"];
            [pargram setValue:value forKey:@"value"];
            [pargram setValue:deviceToken forKey:@"Device Token"];
            [pargram setValue:notiId forKey:@"Notifictaion Id"];
            
            [[Amplitude instance] logEvent:@"CL - Notifications on Device Notification page" withEventProperties:pargram];

            NSNumber *landId = [[NSNumber alloc] init];
            landId = 0;
            if ([[userInfo objectForKey:@"type"] intValue] == 3 || [[userInfo objectForKey:@"type"] intValue] == 4 || [[userInfo objectForKey:@"type"] intValue] == 5 || [[userInfo objectForKey:@"type"] intValue] == 6|| [[userInfo objectForKey:@"type"] intValue] == 7 || [[userInfo objectForKey:@"type"] intValue] == 8 || [[userInfo objectForKey:@"type"] intValue] == 10)
            {
                landId = [userInfo objectForKey:@"landid"];
            }else if ([[userInfo objectForKey:@"type"] intValue] == 9){
                landId = [userInfo objectForKey:@"restaurantId"];
            }
            
            [self jumpToAppWithUrl:[userInfo objectForKey:@"layer.message_identifier"] andNotificationType:[userInfo objectForKey:@"type"] andNotificationId:[userInfo objectForKey:@"nid"] andLandId:landId];

        }
            break;
        case UIApplicationStateBackground:
            NSLog(@"UIApplicationStateBackground");
            break;
        default:
            break;
    }
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        //hide the badge
        application.applicationIconBadgeNumber = 0;
        NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:kRemoteNotificationDeviceToken];
        if (deviceToken!=nil) {
            NSMutableDictionary *pargrammer=[[NSMutableDictionary alloc]init];
            [pargrammer setObject:deviceToken forKey:@"deviceToken"];
            [pargrammer setObject:[NSNumber numberWithInt:0] forKey:@"badge"];
            [NotificationHandler updateNotificationCountWithParams:pargrammer andBlock:^(id responseObject) {
                
            }];
            
        }

    });
//
    
}
-(void)removeEcodingFile:(NSString*)path{
    NSFileManager * manger=[NSFileManager defaultManager];
  BOOL a=  [manger removeItemAtPath:path error:nil];
    if (a) {
        NSLog(@" RDP缓存清理成功");
    }




}
-(void)goToPage:(NSInteger)pageIndex{
    [self goToPage:pageIndex withSearchTag:nil withEventLogo:nil];
}

-(void)goToPage:(NSInteger)pageIndex  withSearchTag:(NSDictionary*)searchTag withEventLogo:(NSString*)eventLogo{
    
    [self requestToShowNotificationCount];
    switch (pageIndex) {
        case 0:
        {
            self.tabMenuViewController.selectedIndex = 0;
            NeedExpandMenu=NO;
//            selectedNavigationController = homeNavigationController;
//            [selectedNavigationController popToRootViewControllerAnimated:YES];
//            [self goToLoginController];
            LoginViewController *login = [[LoginViewController alloc]init];
            login.isFromonboard = YES;
            UINavigationController *loginNav=[[UINavigationController alloc]initWithRootViewController:login];
            self.window.rootViewController= loginNav;
            break;
        }
        case 1:
        {
            if(self.userLogined){
                IMGUser *user=[IMGUser currentUser];
                if(user.userId!=nil){
                    self.tabMenuViewController.selectedIndex = 4;
                    selectedNavigationController =ProfileNavigationController;
                    [selectedNavigationController popToRootViewControllerAnimated:YES];
                }else{
                    [self goToLoginController];
                }
            }else{
                [self goToLoginController];
            }
            
            break;
        }
        case 3:
        {
            if(self.userLogined)
            {
                self.tabMenuViewController.selectedIndex = 4;
                selectedNavigationController =ProfileNavigationController;
                [selectedNavigationController popToRootViewControllerAnimated:YES];
            }
            else
            {
                [self goToLoginController];
            }
            
            break;
        }
        case 4:
        {
            if (self.userLogined) {
                self.tabMenuViewController.selectedIndex = 3;
                selectedNavigationController = notificationsNavigationController;
                [selectedNavigationController popToRootViewControllerAnimated:YES];
            }else{
                [self goToLoginControllerWithindex:3];
            }
        }
        case 8:
        {
            [[Amplitude instance] logEvent:@"CL - Restaurant Nearby" withEventProperties:@{@"Location":@"Main menu"}];
            [[AppsFlyerTracker sharedTracker] trackEvent:@"CL - Near You CTA" withValues:@{@"Location":@"Main menu"}];

            IMGDiscover *discover=[[IMGDiscover alloc]init];
            discover.sortby=SORTBY_DISTANCE;
            discover.fromWhere=[NSNumber numberWithInt:1];
            discover.cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
            RestaurantsViewController *restaurantsViewController = [[RestaurantsViewController alloc] initWithMap];
            restaurantsViewController.isNearBy = YES;
            [restaurantsViewController initDiscover:discover];
            nearByNavigationController=[[MLNavigationController alloc] initWithRootViewController:restaurantsViewController];
            
            [self.homeNavigationController pushViewController:restaurantsViewController animated:YES];
            break;
        }
        case 10:
        {
            IMGUser *user = [IMGUser currentUser];
            if (user.userId && user.token)
            {
//                [[ZDCChat instance].session trackEvent:@"Chat button pressed: (pre-set data)"];
                
                // before starting the chat set the visitor data
                [ZDCChat updateVisitor:^(ZDCVisitorInfo *visitor) {
                    
                    visitor.phone = user.phone;
                    visitor.name = user.fullName;
                    visitor.email = user.email;
                }];
                
                // start a chat pushed on to the current navigation controller
                // with a session config requiring all pre-chat fields and setting tags and department

                [ZDCChat startChatIn:self.homeNavigationController withConfig:^(ZDCConfig *config) {
                    
                    config.preChatDataRequirements.name = ZDCPreChatDataRequired;
                    config.preChatDataRequirements.email = ZDCPreChatDataRequired;
                    config.preChatDataRequirements.phone = ZDCPreChatDataRequired;
                    config.preChatDataRequirements.department = ZDCPreChatDataRequired;
                    config.preChatDataRequirements.message = ZDCPreChatDataRequired;
                    config.department = @"The date";
                    config.tags = @[@"tag1", @"tag2"];
                }];
                self.tabMenuViewController.selectedIndex = 0;
            }
            else
            {
                // track the event
//                [[ZDCChat instance].api trackEvent:@"Chat button pressed: (all fields required)"];
                // Start a chat pushed on to the current navigation controller
                // with session config making all pre-chat fields required

                [ZDCChat startChatIn:self.homeNavigationController withConfig:^(ZDCConfig *config) {
                    
                    
                    config.preChatDataRequirements.name = ZDCPreChatDataRequiredEditable;
                    config.preChatDataRequirements.email = ZDCPreChatDataRequiredEditable;
                    config.preChatDataRequirements.phone = ZDCPreChatDataRequiredEditable;
                    config.preChatDataRequirements.department = ZDCPreChatDataRequiredEditable;
                    config.preChatDataRequirements.message = ZDCPreChatDataRequired;
                }];
                self.tabMenuViewController.selectedIndex = 0;
                selectedNavigationController =homeNavigationController;
            }

        }
            break;
        case 11:
        {
            self.tabMenuViewController.selectedIndex = 0;
            NeedExpandMenu=YES;
            selectedNavigationController =homeNavigationController;
            [selectedNavigationController popToRootViewControllerAnimated:YES];
            break;
        }
        case 12:
            self.tabMenuViewController.selectedIndex = 1;
            selectedNavigationController = discoverListNavigationController;
            [selectedNavigationController popToRootViewControllerAnimated:YES];
            break;
        default:
            break;
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [self.timer setFireDate:[NSDate distantFuture]];
    [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^(){
        //程序在10分钟内未被系统关闭或者强制关闭，则程序会调用此代码块，可以在这里做一些保存或者清理工作
    }];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
  
    self.homeViewController.isBocomeActive=YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [GAI sharedInstance].optOut =
    ![[NSUserDefaults standardUserDefaults] boolForKey:kAllowTracking];
   // [FBSession.activeSession handleDidBecomeActive];
    
    // Track Installs, updates & sessions(app opens) (You must include this API to enable tracking)
    [[AppsFlyerTracker sharedTracker] trackAppLaunch];

    if (!isOpenFromMobileWeb&&!isOpenFromNotification&&isFirst&&!isNotificationOrMobileWeb) {
        [[Amplitude instance] logEvent:@"GN - Open App" withEventProperties:@{@"Source":@"Direct"}];
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"isAppTerminated"];
        isFirst=NO;
    }
    
    if (self.isSettingNotification && [self checkNotificationState]) {
        self.isSettingNotification = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RefreshNotificationSettingView" object:nil];
    }
}

- (BOOL)checkNotificationState{
    UIUserNotificationSettings *setting = [[UIApplication sharedApplication] currentUserNotificationSettings];
    if (setting.types == UIUserNotificationTypeNone) {
        return NO;
    }else{
        return YES;
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
   // [FBSession.activeSession close];
    [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:@"categoryName"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isAppTerminated"];

}

#pragma mark - Application's Documents directory
// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

-(void)goToLoginController:(NSNumber *)fromWhere
{
    LoginViewController *loginVC=[[LoginViewController alloc]initWithFromWhere:fromWhere];
    loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
    [self.window.rootViewController presentViewController:loginNavagationController animated:YES completion:nil];
}

-(void)goToLoginController{
    LoginViewController *loginVC=[[LoginViewController alloc]init];
    loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
    [self.window.rootViewController presentViewController:loginNavagationController animated:YES completion:nil];
}
-(void)goToLoginControllerWithindex:(int)index
{
    LoginViewController *loginVC=[[LoginViewController alloc]init];
    if (index==3) {
        loginVC.isFromNotification = YES;
    }
    if (index==4) {
        loginVC.isFromProfile = YES;
    }
    loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
    [self.window.rootViewController presentViewController:loginNavagationController animated:YES completion:nil];
}

-(void)goToLoginControllerByDelegate:(id)autoPostDelegate dictionary:(NSDictionary *)autoPostDictionary{
    LoginViewController *loginVC=[[LoginViewController alloc]init];
    loginVC.autoPostDelegate = autoPostDelegate;
    loginVC.autoPostDictionary = autoPostDictionary;
    loginNavagationController=[[UINavigationController alloc]initWithRootViewController:loginVC];
    [self.window.rootViewController presentViewController:loginNavagationController animated:YES completion:nil];
}

/**
 *    @brief    把Resource文件夹下的save1.dat拷贝到沙盒
 *
 *    @param     sourcePath     Resource文件路径
 *    @param     toPath     把文件拷贝到XXX文件夹
 *
 *    @return    BOOL
 */
- (BOOL)copyMissingFile:(NSString *)sourcePath toPath:(NSString *)toPath
{
    BOOL retVal = YES; // If the file already exists, we'll return success…
    NSString * finalLocation = [toPath stringByAppendingPathComponent:[sourcePath lastPathComponent]];
    if (![[NSFileManager defaultManager] fileExistsAtPath:finalLocation])
    {
        retVal = [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:finalLocation error:NULL];
    }
    return retVal;
}

/**
 *    @brief    创建文件夹
 *
 *    @param     createDir     创建文件夹路径
 */
- (void)createFolder:(NSString *)createDir
{
    BOOL isDir = NO;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL existed = [fileManager fileExistsAtPath:createDir isDirectory:&isDir];
    if ( !(isDir == YES && existed == YES) )
    {
        [fileManager createDirectoryAtPath:createDir withIntermediateDirectories:YES attributes:nil error:nil];
    }
}
// Documents下创建文件夹
- (void)createFolderWithName:(NSString *)folderName
{
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    NSString *pathDocuments = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *createPath = [NSString stringWithFormat:@"%@/%@", pathDocuments,folderName];
//    NSString *createDir = [NSString stringWithFormat:@"%@/MessageQueueImage", pathDocuments];
    
    // 判断文件夹是否存在，如果不存在，则创建
    if (![[NSFileManager defaultManager] fileExistsAtPath:createPath]) {
        [fileManager createDirectoryAtPath:createPath withIntermediateDirectories:YES attributes:nil error:nil];
//        [fileManager createDirectoryAtPath:createDir withIntermediateDirectories:YES attributes:nil error:nil];
    } else {
        NSLog(@"FileDir is exists.");
    }
}

#pragma mark Layer
-(void)loadLayerButton{
//    UIButton *layerButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [layerButton setBackgroundColor:[UIColor redColor]];
//    [self.window addSubview:layerButton];
//    [layerButton setBackgroundImage:[UIImage imageNamed:@"Layer_button"] forState:UIControlStateNormal];
//    layerButton.frame = CGRectMake(0, DeviceHeight, DeviceWidth/2, 20);
//    [layerButton addTarget:self action:@selector(showChatWindow) forControlEvents:UIControlEventTouchUpInside];
    
    
    
}
//-(void)showChatWindow{
////    self.viewController.view.frame = CGRectMake(0, 100, 300, DeviceHeight-100);
//    [self.window addSubview:self.viewController.view];
//    
//}
//-(void)HideLayerWindow{
//    
//    [self.viewController.view removeFromSuperview];
//    self.viewController.view = nil;
//
//}
//-(void)initLayer{
//    
//    
//    //Show a usage the first time the app is launched
//    [self showFirstTimeMessage];
//    self.viewController = [[LayerViewController alloc]init];
//    
//    
//    // Initializes a LYRClient object
//    NSUUID *appID = [[NSUUID alloc] initWithUUIDString:kAppID];
//    self.layerClient = [LYRClient clientWithAppID:appID];
//    self.viewController.layerClient = self.layerClient;
//    
//    // Authenticate Layer
//    [self.layerClient connectWithCompletion:^(BOOL success, NSError *error) {
//        if (!success) {
//            NSLog(@"Failed to connect to Layer: %@", error);
//            //abort();
//            return;
//        }
//        
//        // If the current user isn't authenticated, request authentication
//        if (!self.layerClient.authenticatedUserID) {
//            [self.layerClient requestAuthenticationNonceWithCompletion:^(NSString *nonce, NSError *error) {
//                if (!nonce) {
//                    NSLog(@"Request for Layer authentication nonce failed: %@", error);
//                    abort();
//                    return;
//                }
//                
//                // Get identity token from the Layer Identity Service (used only for staging apps)
//                NSURL *identityTokenURL = [NSURL URLWithString:@"https://layer-identity-provider.herokuapp.com/identity_tokens"];
//                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:identityTokenURL];
//                request.HTTPMethod = @"POST";
//                [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//                [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//                NSDictionary *parameters = @{ @"app_id": [self.layerClient.appID UUIDString], @"user_id": kUserID, @"nonce": nonce };
//                __block NSError *serializationError = nil;
//                NSData *requestBody = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&serializationError];
//                if (!requestBody) {
//                    NSLog(@"Failed serialization of request parameters: %@", serializationError);
//                    abort();
//                    return;
//                }
//                request.HTTPBody = requestBody;
//                
//                NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration ephemeralSessionConfiguration];
//                NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
//                [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                    if (!data) {
//                        NSLog(@"Failed requesting identity token: %@", error);
//                        abort();
//                        return;
//                    }
//                    
//                    NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&serializationError];
//                    if (!responseObject) {
//                        NSLog(@"Failed deserialization of response: %@", serializationError);
//                        abort();
//                        return;
//                    }
//                    
//                    NSString *identityToken = responseObject[@"identity_token"];
//                    [self.layerClient authenticateWithIdentityToken:identityToken completion:^(NSString *authenticatedUserID, NSError *error) {
//                        if (!authenticatedUserID) {
//                            NSLog(@"Failed authentication with Layer: %@", error);
//                            return;
//                        }
//                    }];
//                }] resume];
//            }];
//        }
//    }];
//    [self loadLayerButton];
//}
- (void)showFirstTimeMessage;
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"])
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedOnce"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        // This is the first launch ever
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hello!" message:@"This app is a very simple chat app using Layer.  Launch this app on a Simulator and a Device to start a 1:1 conversation." delegate:self cancelButtonTitle:nil otherButtonTitles:nil];
        [alert addButtonWithTitle:@"Got It!"];
        [alert show];
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    // Increment badge count if a message
    NSDictionary *apsDic = [userInfo objectForKey:@"aps"];
    if ([[apsDic objectForKey:@"badge"] integerValue] != 0) {
//        NSInteger badgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
        NSString *deviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:kRemoteNotificationDeviceToken];
        if (deviceToken!=nil) {
            NSMutableDictionary *pargrammer=[[NSMutableDictionary alloc]init];
            [pargrammer setObject:deviceToken forKey:@"deviceToken"];
            [pargrammer setObject:[NSNumber numberWithInt:0] forKey:@"badge"];
            [NotificationHandler updateNotificationCountWithParams:pargrammer andBlock:^(id responseObject) {
                
            }];
            
        }

        
    }
    
    NSDictionary *alertDic=[apsDic objectForKey:@"alert"];
    NSString *value=[alertDic objectForKey:@"body"];

    value=[self subWord:value withNumber:10];
    NSNumber *type=[userInfo objectForKey:@"type"];
    NSString* typename=@"";
    switch ([type intValue]) {
        case 1:case 0:
            typename= @"Landing Page Message";
        case 2:
            typename= @"Splash Notification";
        case 3:
            typename= @"Reservation";
        case 4:
            typename= @"Restaurant info update";
        case 5:
            typename= @"Review";
        case 6:
            typename= @"Photo";
            
        case 7:
            typename=@"Photo List";
        default:
            typename= @"unKnow type";
    }
    
    NSString *landpage=@"";
    switch ([type intValue]) {
        case 1:
            landpage=@"Landing Page";
        case 2:
            landpage=@"Splash Page";
        case 3:
            landpage=@"Reservation Detial Page";
        case 4:
            landpage=@"Restaurant Detail Page";
        case 5:
            landpage=@"Review Card Detial Page";
        case 6:
            landpage=@"Photo Card Detial Page";
        case 7:
            landpage=@"Photo Card Detail Page - Photo List";
        case 0:
            landpage=@"Notification List Page";
        default:
            landpage=@"unKnow Page";
    }
    NSString *deviceToken=[[NSUserDefaults standardUserDefaults] objectForKey:kRemoteNotificationDeviceToken];
    NSNumber *notiId=[userInfo objectForKey:@"nid"];
    NSMutableDictionary *pargram=[[NSMutableDictionary alloc]init];
    [pargram setValue:landpage forKey:@"LandingPage"];
    [pargram setValue:typename forKey:@"Type"];
    [pargram setValue:value forKey:@"value"];
    [pargram setValue:deviceToken forKey:@"Device Token"];
    [pargram setValue:notiId forKey:@"Notifictaion Id"];

    [[Amplitude instance] logEvent:@"CL - Notifications on Device Notification page" withEventProperties:pargram];
    NSString *urlString = [userInfo objectForKey:@"layer.message_identifier"];
    NSNumber *landId = [[NSNumber alloc] init];
    landId = 0;
    if ([[userInfo objectForKey:@"type"] intValue] == 3 || [[userInfo objectForKey:@"type"] intValue] == 4 || [[userInfo objectForKey:@"type"] intValue] == 5 || [[userInfo objectForKey:@"type"] intValue] == 6 ||[[userInfo objectForKey:@"type"] intValue] == 7 || [[userInfo objectForKey:@"type"] intValue] == 8 || [[userInfo objectForKey:@"type"] intValue] == 10 || [[userInfo objectForKey:@"type"] intValue] == 12)
    {
        landId = [userInfo objectForKey:@"landid"];
    }else if ([[userInfo objectForKey:@"type"] intValue] == 9){
        landId = [userInfo objectForKey:@"restaurantId"];
    }
    
    [self jumpToAppWithUrl:urlString andNotificationType:[userInfo objectForKey:@"type"] andNotificationId:[userInfo objectForKey:@"nid"] andLandId:landId];
    
    //Get Message from Metadata
//    __block LYRMessage *message = [self messageFromRemoteNotification:userInfo];
//    if (application.applicationState == UIApplicationStateInactive) {
//        [self showChatWindow];
//    }
    
//    NSError *error;
//    BOOL success = [self.layerClient synchronizeWithRemoteNotification:userInfo completion:^(UIBackgroundFetchResult fetchResult, NSError *error) {
//        if (fetchResult == UIBackgroundFetchResultFailed) {
//            NSLog(@"Failed processing remote notification: %@", error);
//        }
    
//        message = [self messageFromRemoteNotification:userInfo];
        
        //Uncomment this code if you are using silent notifications and want to show a local notification
        //NSString *alertString = [[NSString alloc] initWithData:[message.parts[0] data] encoding:NSUTF8StringEncoding];
        //UILocalNotification *localNotification = [UILocalNotification new];
        //localNotification.alertBody = alertString;
        //[[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
//        completionHandler(fetchResult);
//    }];
//    if (success) {
//        NSLog(@"Application did complete remote notification sycn");
//    } else {
//        NSLog(@"Error handling push notification: %@", error);
//        completionHandler(UIBackgroundFetchResultNoData);
//    }
    
}

//- (LYRMessage *)messageFromRemoteNotification:(NSDictionary *)remoteNotification
//{
//    // Retrieve message URL from Push Notification
//    NSURL *messageURL = [NSURL URLWithString:[remoteNotification valueForKeyPath:@"layer.message_identifier"]];
//    
//    //Retrieve LYRMessage from Message URL
//    LYRQuery *query = [LYRQuery queryWithClass:[LYRMessage class]];
//    query.predicate = [LYRPredicate predicateWithProperty:@"identifier" operator:LYRPredicateOperatorIsIn value:[NSSet setWithObject:messageURL]];
//    
//    NSError *error;
//    NSOrderedSet *messages = [self.layerClient executeQuery:query error:&error];
//    if (!error) {
//        NSLog(@"Push Conversation contains %tu messages", messages.count);
//    } else {
//        NSLog(@"Push Conversation Query failed with error %@", error);
//    }
//    return [messages firstObject];
//}
- (void) styleApp
{
    // status bar
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    // nav bar
    //NSDictionary *navbarAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                      //[UIColor whiteColor] ,NSForegroundColorAttributeName, nil];
    //[[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    //[[UINavigationBar appearance] setTitleTextAttributes:navbarAttributes];
    //[[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.91f green:0.16f blue:0.16f alpha:1.0f]];
    
    if ([self isVersionOrNewer:@"8.0"]) {
        
 
        
        // For translucent nav bars set YES
        [[UINavigationBar appearance] setTranslucent:NO];
    }

}
- (BOOL) isVersionOrNewer:(NSString*)majorVersionNumber {
    return [[[UIDevice currentDevice] systemVersion] compare:majorVersionNumber options:NSNumericSearch] != NSOrderedAscending;
}
void uncaughtExceptionHandler(NSException *exception) {
    [ZDCLog e:@"CRASH: %@", exception];
    [ZDCLog e:@"Stack Trace: %@", [exception callStackSymbols]];
}

-(void)addPopLoginNSTimer{
    if ([self ifCanPopLogin]) {
        [self removePopLoginNSTimer];
        self.popLoginNSTimer = [NSTimer scheduledTimerWithTimeInterval:8 target:self selector:@selector(goToLoginPage) userInfo:nil repeats:NO];
    }
}
-(void)goToLoginPage{
    if ([self ifCanPopLogin]){
        self.isAlreadyPopLogin = YES;
        LoginViewController *loginVC=[[LoginViewController alloc]init];
        UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:loginVC];
        [self.window.rootViewController presentViewController:nav animated:YES completion:nil];
    }
    [self removePopLoginNSTimer];
}
- (void)gotoHomePage{
    self.tabMenuViewController.selectedIndex = 0;
    NeedExpandMenu=NO;
    selectedNavigationController = homeNavigationController;
    self.window.rootViewController = tabMenuViewController;
}


-(BOOL)ifCanPopLogin{
    IMGUser *currentUser = [IMGUser currentUser];
    return ((currentUser.userId == nil) && (!self.isAlreadyPopLogin));
}
-(void)removePopLoginNSTimer{
    if (self.popLoginNSTimer!=nil) {
        [self.popLoginNSTimer invalidate];
        self.popLoginNSTimer = nil;
    }
}





@end
