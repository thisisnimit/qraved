//

//  SHKTwitter.m

//  ShareKit

//

//  Created by Nathan Weiner on 6/21/10.



//

//  Permission is hereby granted, free of charge, to any person obtaining a copy

//  of this software and associated documentation files (the "Software"), to deal

//  in the Software without restriction, including without limitation the rights

//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell

//  copies of the Software, and to permit persons to whom the Software is

//  furnished to do so, subject to the following conditions:

//

//  The above copyright notice and this permission notice shall be included in

//  all copies or substantial portions of the Software.

//

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR

//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,

//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE

//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER

//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,

//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN

//  THE SOFTWARE.

//

//



// TODO - SHKTwitter supports offline sharing, however the url cannot be shortened without an internet connection.  Need a graceful workaround for this.





#import "SHKTwitter.h"
#import "Consts.h"
#import "AppDelegate.h"



@implementation SHKTwitter



@synthesize xAuth;



- (id)init

{

	if (self = [super init])

	{	

		// OAUTH		

		self.consumerKey = SHKTwitterConsumerKey;		

		self.secretKey = SHKTwitterSecret;

 		self.authorizeCallbackURL = [NSURL URLWithString:SHKTwitterCallbackUrl];// HOW-TO: In your Twitter application settings, use the "Callback URL" field.  If you do not have this field in the settings, set your application type to 'Browser'.

		

		// XAUTH

		self.xAuth = SHKTwitterUseXAuth?YES:NO;

		

		

		// -- //

		

		

		// You do not need to edit these, they are the same for everyone

	    self.authorizeURL = [NSURL URLWithString:@"https://twitter.com/oauth/authorize"];

	    self.requestURL = [NSURL URLWithString:@"https://twitter.com/oauth/request_token"];

	    self.accessURL = [NSURL URLWithString:@"https://twitter.com/oauth/access_token"]; 

	}

    

	return self;

}



- (id)initWithNextDelegate:(id) nextDelegate {

    if (self = [super init])

	{

		// OAUTH

		self.consumerKey = SHKTwitterConsumerKey;

		self.secretKey = SHKTwitterSecret;

 		self.authorizeCallbackURL = [NSURL URLWithString:SHKTwitterCallbackUrl];// HOW-TO: In your Twitter application settings, use the "Callback URL" field.  If you do not have this field in the settings, set your application type to 'Browser'.

		

		// XAUTH

		self.xAuth = SHKTwitterUseXAuth?YES:NO;

		

		

		// -- //

		

		

		// You do not need to edit these, they are the same for everyone

	    self.authorizeURL = [NSURL URLWithString:@"https://twitter.com/oauth/authorize"];

	    self.requestURL = [NSURL URLWithString:@"https://twitter.com/oauth/request_token"];

	    self.accessURL = [NSURL URLWithString:@"https://twitter.com/oauth/access_token"];

	}

    self.nextDelegate = nextDelegate;

	return self;

}





#pragma mark -

#pragma mark Configuration : Service Defination



+ (NSString *)sharerTitle

{

	return @"Twitter";

}



+ (BOOL)canShareURL

{

    return YES;

}



+ (BOOL)canShareText

{

	return YES;

}



// TODO use img.ly to support this

+ (BOOL)canShareImage

{

	return YES;

}





#pragma mark -

#pragma mark Configuration : Dynamic Enable



- (BOOL)shouldAutoShare

{

	return YES;

}





#pragma mark -

#pragma mark Authorization



- (BOOL)isAuthorized

{		

	if ([self restoreAccessToken]){

//        [self.nextDelegate loginSuccessful];

        [self startLoginApp];

        return YES;

    }else {

        return NO;

    }

}



- (void)promptAuthorization

{

		[super promptAuthorization]; // OAuth process		

}





#pragma mark xAuth



+ (NSString *)authorizationFormCaption

{

	return SHKLocalizedString(@"Create a free account at %@", @"Twitter.com");

}



+ (NSArray *)authorizationFormFields

{

	if ([SHKTwitterUsername isEqualToString:@""])

		return [super authorizationFormFields];

	

	return [NSArray arrayWithObjects:

			[SHKFormFieldSettings label:SHKLocalizedString(@"Username") key:@"username" type:SHKFormFieldTypeText start:nil],

			[SHKFormFieldSettings label:SHKLocalizedString(@"Password") key:@"password" type:SHKFormFieldTypePassword start:nil],

			[SHKFormFieldSettings label:SHKLocalizedString(@"Follow %@", SHKTwitterUsername) key:@"followMe" type:SHKFormFieldTypeSwitch start:SHKFormFieldSwitchOn],			

			nil];

}



- (void)authorizationFormValidate:(SHKFormController *)form

{

	self.pendingForm = form;

	[self tokenAccess];

}



- (void)tokenAccessModifyRequest:(OAMutableURLRequest *)oRequest

{	

	

}



- (void)tokenAccessTicket:(OAServiceTicket *)ticket didFinishWithData:(NSData *)data

{

	if (xAuth) 

	{

		if (ticket.didSucceed)

		{

			[item setCustomValue:[[pendingForm formValues] objectForKey:@"followMe"] forKey:@"followMe"];

			[pendingForm close];

		}

		

		else

		{

			NSString *response = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];

			

			SHKLog(@"tokenAccessTicket Response Body: %@", response);

			

			[self tokenAccessTicket:ticket didFailWithError:[SHK error:response]];

			return;

		}

	}



	[self tokenAccessTicketOAuth:ticket didFinishWithData:data];

}



- (void)tokenAccessTicketOAuth:(OAServiceTicket *)ticket didFinishWithData:(NSData *)data

{

	if (SHKDebugShowLogs) // check so we don't have to alloc the string with the data if we aren't logging

		SHKLog(@"tokenAccessTicket Response Body: %@", [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease]);

	

	[[SHKActivityIndicator currentIndicator] hide];

	

	if (ticket.didSucceed)

	{

		NSString *responseBody = [[NSString alloc] initWithData:data

													   encoding:NSUTF8StringEncoding];

		self.accessToken = [[[OAToken alloc] initWithHTTPResponseBody:responseBody]autorelease];

		[responseBody release];

		

		[self storeAccessToken];

		

        [self startLoginApp];

	}

	

	

	else

		// TODO - better error handling here

		[self tokenAccessTicket:ticket didFailWithError:[SHK error:SHKLocalizedString(@"There was a problem requesting access from %@", [self sharerTitle])]];

}







- (void)startLoginApp {

    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/account/verify_credentials.json"];

    OAMutableURLRequest *oRequest = [[OAMutableURLRequest alloc] initWithURL:url

                                                                    consumer:consumer

                                                                       token:accessToken

                                                                       realm:nil

                                                           signatureProvider:nil];

	

	[oRequest setHTTPMethod:@"GET"];

	



	OAAsynchronousDataFetcher *fetcher = [OAAsynchronousDataFetcher asynchronousFetcherWithRequest:oRequest

                                                                                          delegate:self

                                                                                 didFinishSelector:@selector(sendLoginStatusTicket:didFinishWithData:)

                                                                                   didFailSelector:@selector(sendLoginStatusTicket:didFailWithError:)];	

    

	[fetcher start];

	[oRequest release];

    

        

}



- (void)sendLoginStatusTicket:(OAServiceTicket *)ticket didFinishWithData:(NSData *)data

{

	if (ticket.didSucceed) {

//        NSLog(@"sendLoginStatusTicket ticket.didSucceed1:%@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);

        NSError *error = nil;

        NSJSONReadingOptions JSONReadingOptions=NSJSONReadingMutableContainers;

        NSDictionary*  responseJSON = [NSJSONSerialization JSONObjectWithData:data options:JSONReadingOptions error:&error];

            TWitterUser *twitterUser = [[[TWitterUser alloc] init]autorelease];
        
            NSString *fullName = [responseJSON objectForKey:@"name"];
            NSArray *nameArray = [fullName componentsSeparatedByString:@" "];
            
            if(nameArray.count<2) {
                nameArray = [fullName componentsSeparatedByString:@"."];
                if(nameArray.count<2) {
                    nameArray = [fullName componentsSeparatedByString:@"_"];
                }
            }
        
            NSString *firstName = fullName;
            NSString *lastName = fullName;
            if(nameArray.count>1) {
                firstName = [nameArray objectAtIndex:0];
                lastName = [nameArray objectAtIndex:1];
            }
            NSLog(@"sendLoginStatusTicket ticket.didSucceed2:%@,%@", firstName, lastName);
        
            NSDictionary*  twitterResult = [NSDictionary dictionaryWithObjectsAndKeys:
                                            
                                            [responseJSON objectForKey: twitterUser.screenName],twitterUser.email,
                                            [responseJSON objectForKey: twitterUser.userID],twitterUser.userID,
                                            @"0", twitterUser.gender,
                                            [responseJSON objectForKey:twitterUser.screenName],twitterUser.userName,
                                            firstName,twitterUser.firstName,
                                            lastName,twitterUser.lastName,
                                            [responseJSON objectForKey:@"profile_image_url"],twitterUser.picture,
                                            twitterUser.birthday,twitterUser.birthday,
                                            twitterUser.updatedTime,twitterUser.updatedTime,nil];
        NSDictionary *userInfo;
        if(error){
            userInfo=@{@"twitterResult":twitterResult,@"error":error};
        }else{
            userInfo=@{@"twitterResult":twitterResult};
        }
        NSString *twitterNotificationType = [[NSUserDefaults standardUserDefaults]objectForKey:TWITTERNOTIFICATIONTYPE];
        if([twitterNotificationType isEqualToString:@"login"]){
            [[NSNotificationCenter defaultCenter] postNotificationName:TwitterLoginSelfPartDone object:nil userInfo:userInfo];
        }else if([twitterNotificationType isEqualToString:@"connect"]){
            [[NSNotificationCenter defaultCenter] postNotificationName:TwitterConnectPartDone object:nil userInfo:userInfo];
        }else{
            [[NSNotificationCenter defaultCenter] postNotificationName:TwitterLoginSelfPartDone object:nil userInfo:userInfo];
        }
        
    }else{
        NSLog(@"Twitter Send Status Error-data NSUTF8StringEncoding: %@ ", [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease]);
    }

}


- (void)sendLoginStatusTicket:(OAServiceTicket *)ticket didFailWithError:(NSError*)error

{

	NSLog(@"Twitter Send Status Error-error.description: %@", error.description);

}



- (void)loginApp {

    

}



- (void)redirectToNext {
    [self dismissViewControllerAnimated:NO completion:nil];
    

}



#pragma mark -

#pragma mark UI Implementation



- (void)show

{

	if (item.shareType == SHKShareTypeURL)

	{

		[self shortenURL];

	}

	

	else if (item.shareType == SHKShareTypeImage)

	{

		[item setCustomValue:item.title forKey:@"status"];

		[self showTwitterForm];

	}

	

	else if (item.shareType == SHKShareTypeText)

	{

		[item setCustomValue:item.text forKey:@"status"];

		[self showTwitterForm];

	}

}



- (void)showTwitterForm

{

//	SHKTwitterForm *rootView = [[SHKTwitterForm alloc] initWithNibName:nil bundle:nil];	
//
//	rootView.delegate = self;
//
//	
//
//	// force view to load so we can set textView text
//
//	[rootView view];
//
//	
//
//	rootView.textView.text = [item customValueForKey:@"status"];
//
//	rootView.hasAttachment = item.image != nil;
//
//	
//
//	[self pushViewController:rootView animated:NO];
//
//	
//
//	[[SHK currentHelper] showViewController:self];
    
    if(![self shouldAutoShare]){ //test autoshare value and send direct without displaying the form if =YES
        SHKTwitterForm *rootView = [[SHKTwitterForm alloc] initWithNibName:nil bundle:nil];
        rootView.delegate = self;
        
        // force view to load so we can set textView text
        [rootView view];
        
        rootView.textView.text = [item customValueForKey:@"status"];
        rootView.hasAttachment = item.image != nil;
        
        [self pushViewController:rootView animated:NO];
        
        [[SHK currentHelper] showViewController:self];      
    }else {
        [self tryToSend];
    }

}



- (void)sendForm:(SHKTwitterForm *)form

{	

	[item setCustomValue:form.textView.text forKey:@"status"];

	[self tryToSend];

}





#pragma mark -



- (void)shortenURL

{	

	if (![SHK connected])

	{

		[item setCustomValue:[NSString stringWithFormat:@"%@ %@", item.title, [item.URL.absoluteString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] forKey:@"status"];

		[self showTwitterForm];		

		return;

	}

	

	if (!quiet)

		[[SHKActivityIndicator currentIndicator] displayActivity:SHKLocalizedString(@"Shortening URL...")];

	

	self.request = [[[SHKRequest alloc] initWithURL:[NSURL URLWithString:[NSMutableString stringWithFormat:@"http://api.bit.ly/v3/shorten?login=%@&apikey=%@&longUrl=%@&format=txt",

																		 SHKBitLyLogin,

																		  SHKBitLyKey,																		  

																		  SHKEncodeURL(item.URL)

																		 ]]

											params:nil

										  delegate:self

								isFinishedSelector:@selector(shortenURLFinished:)

											method:@"GET"

										  autostart:YES] autorelease];

}



- (void)shortenURLFinished:(SHKRequest *)aRequest

{

	[[SHKActivityIndicator currentIndicator] hide];

	

	NSString *result = [[aRequest getResult] stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];

	

	if (result == nil || [NSURL URLWithString:result] == nil)

	{

		// TODO - better error message

		[[[[UIAlertView alloc] initWithTitle:SHKLocalizedString(@"Shorten URL Error")

									 message:SHKLocalizedString(@"We could not shorten the URL.")

									delegate:nil

						   cancelButtonTitle:SHKLocalizedString(@"Continue")

						   otherButtonTitles:nil] autorelease] show];

		

		[item setCustomValue:[NSString stringWithFormat:@"%@ %@", item.text ? item.text : item.title, [item.URL.absoluteString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] forKey:@"status"];

	}

	

	else

	{		

		///if already a bitly login, use url instead

		if ([result isEqualToString:@"ALREADY_A_BITLY_LINK"])

			result = [item.URL.absoluteString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

		

		[item setCustomValue:[NSString stringWithFormat:@"%@ %@", item.text ? item.text : item.title, result] forKey:@"status"];

	}



	[self showTwitterForm];

}



#pragma mark -

#pragma mark Share API Methods



- (BOOL)validate

{

	NSString *status = [item customValueForKey:@"status"];

	BOOL validated = status != nil && (status.length > 0 || status.length == 0) && status.length <= 140;
    return validated;
}



- (BOOL)send

{
	// Check if we should send follow request too

	if (xAuth && [item customBoolForSwitchKey:@"followMe"])

		[self followMe];	

	

	if (![self validate])

		[self show];

	

	else

	{	

		if (item.shareType == SHKShareTypeImage) {

			[self sendImage];

		} else {

			[self sendStatus];

		}

		

		// Notify delegate

		[self sendDidStart];	

		

		return YES;

	}

	

	return NO;

}



- (void)sendStatus

{

	OAMutableURLRequest *oRequest = [[OAMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://api.twitter.com/1.1/statuses/update.json"]

																   consumer:consumer

																	  token:accessToken

																	  realm:nil

														  signatureProvider:nil];

	

	[oRequest setHTTPMethod:@"POST"];

	

	OARequestParameter *statusParam = [[OARequestParameter alloc] initWithName:@"status"

																		 value:[item customValueForKey:@"status"]];

	NSArray *params = [NSArray arrayWithObjects:statusParam, nil];

	[oRequest setParameters:params];

	[statusParam release];

	

	OAAsynchronousDataFetcher *fetcher = [OAAsynchronousDataFetcher asynchronousFetcherWithRequest:oRequest

						 delegate:self

				didFinishSelector:@selector(sendStatusTicket:didFinishWithData:)

				  didFailSelector:@selector(sendStatusTicket:didFailWithError:)];	



	[fetcher start];

	[oRequest release];

}


- (void)sendStatusTicket:(OAServiceTicket *)ticket didFinishWithData:(NSData *)data 

{	
	// TODO better error handling here

	if (ticket.didSucceed) 

		[self sendDidFinish];
	else
	{		

		if (SHKDebugShowLogs)

			SHKLog(@"Twitter Send Status Error: %@", [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease]);

		

		// CREDIT: Oliver Drobnik
		NSString *string = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
        
		// in case our makeshift parsing does not yield an error message
        NSLog(@"Twitter Send Status Error: %@", [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease]);
		NSString *errorMessage = @"Unknown Error";		

		

		NSScanner *scanner = [NSScanner scannerWithString:string];
        
		// skip until error message

		[scanner scanUpToString:@"\"error\":\"" intoString:nil];

		

		

		if ([scanner scanString:@"\"error\":\"" intoString:nil])

		{

			// get the message until the closing double quotes

			[scanner scanUpToCharactersFromSet:[NSCharacterSet characterSetWithCharactersInString:@"\""] intoString:&errorMessage];

		}


		// this is the error message for revoked access
        
		if ([errorMessage isEqualToString:@"Invalid / used nonce"])

		{

			[self sendDidFailShouldRelogin];
		}

		else
		{
            NSDictionary *dictionary=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            
            NSArray *errCode=[dictionary objectForKey:@"errors"];
//            NSLog(@"errCode is %@",errCode);
//            NSLog(@"errCode is %@",[[errCode objectAtIndex:0] objectForKey:@"code"]);
            if([[NSString stringWithFormat:@"%@",[[errCode objectAtIndex:0] objectForKey:@"code"]] isEqualToString:@"187"])
            {
			NSError *error = [NSError errorWithDomain:@"Twitter" code:2 userInfo:[NSDictionary dictionaryWithObject:@"This dish has been Tweeted already" forKey:NSLocalizedDescriptionKey]];

			[self sendDidFailWithError:error];
            }
            else
            {
                NSError *error = [NSError errorWithDomain:@"Twitter" code:2 userInfo:[NSDictionary dictionaryWithObject:string forKey:NSLocalizedDescriptionKey]];
                
                [self sendDidFailWithError:error];
            }

		}

	}

}



- (void)sendStatusTicket:(OAServiceTicket *)ticket didFailWithError:(NSError*)error
{
	[self sendDidFailWithError:error];

}



- (void)sendImage {
	NSURL *serviceURL = nil;

	if([item customValueForKey:@"profile_update"]){

		serviceURL = [NSURL URLWithString:@"http://api.twitter.com/1.1/account/update_profile_image.json"];

	} else {

		serviceURL = [NSURL URLWithString:@"https://api.twitter.com/1.1/account/verify_credentials.json"];

	}

	

	OAMutableURLRequest *oRequest = [[OAMutableURLRequest alloc] initWithURL:serviceURL

																	consumer:consumer

																	   token:accessToken

																	   realm:@"http://api.twitter.com/"

														   signatureProvider:signatureProvider];

	[oRequest setHTTPMethod:@"GET"];

	

	if([item customValueForKey:@"profile_update"]){

		[oRequest prepare];

	} else {

		[oRequest prepare];



		NSDictionary * headerDict = [oRequest allHTTPHeaderFields];

		NSString * oauthHeader = [NSString stringWithString:[headerDict valueForKey:@"Authorization"]];

		

		[oRequest release];

		oRequest = nil;

		

		serviceURL = [NSURL URLWithString:@"http://img.ly/api/2/upload.xml"];

		oRequest = [[OAMutableURLRequest alloc] initWithURL:serviceURL

												   consumer:consumer

													  token:accessToken

													  realm:@"http://api.twitter.com/"

										  signatureProvider:signatureProvider];

		[oRequest setHTTPMethod:@"POST"];

		[oRequest setValue:@"https://api.twitter.com/1.1/account/verify_credentials.json" forHTTPHeaderField:@"X-Auth-Service-Provider"];

		[oRequest setValue:oauthHeader forHTTPHeaderField:@"X-Verify-Credentials-Authorization"];

	}

		

	CGFloat compression = 0.9f;

	NSData *imageData = UIImageJPEGRepresentation([item image], compression);

	

	// TODO

	// Note from Nate to creator of sendImage method - This seems like it could be a source of sluggishness.

	// For example, if the image is large (say 3000px x 3000px for example), it would be better to resize the image

	// to an appropriate size (max of img.ly) and then start trying to compress.

	

	while ([imageData length] > 700000 && compression > 0.1) {

		// NSLog(@"Image size too big, compression more: current data size: %d bytes",[imageData length]);

		compression -= 0.1;

		imageData = UIImageJPEGRepresentation([item image], compression);

		

	}

	

	NSString *boundary = @"0xKhTmLbOuNdArY";

	NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];

	[oRequest setValue:contentType forHTTPHeaderField:@"Content-Type"];

	

	NSMutableData *body = [NSMutableData data];

	NSString *dispKey = @"";

	if([item customValueForKey:@"profile_update"]){

		dispKey = @"Content-Disposition: form-data; name=\"image\"; filename=\"upload.jpg\"\r\n";

	} else {

		dispKey = @"Content-Disposition: form-data; name=\"media\"; filename=\"upload.jpg\"\r\n";

	}



	

	[body appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];

	[body appendData:[dispKey dataUsingEncoding:NSUTF8StringEncoding]];

	[body appendData:[@"Content-Type: image/jpg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

	[body appendData:imageData];

	[body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

	

	if([item customValueForKey:@"profile_update"]){

		// no ops

	} else {

		[body appendData:[[NSString stringWithFormat:@"--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];

		[body appendData:[@"Content-Disposition: form-data; name=\"message\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

		[body appendData:[[item customValueForKey:@"status"] dataUsingEncoding:NSUTF8StringEncoding]];

		[body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];	

	}

	

	[body appendData:[[NSString stringWithFormat:@"--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];

	

	// setting the body of the post to the reqeust

	[oRequest setHTTPBody:body];

		

	// Notify delegate

	[self sendDidStart];

		

	// Start the request

	OAAsynchronousDataFetcher *fetcher = [OAAsynchronousDataFetcher asynchronousFetcherWithRequest:oRequest

																						  delegate:self

																				 didFinishSelector:@selector(sendImageTicket:didFinishWithData:)

																				   didFailSelector:@selector(sendImageTicket:didFailWithError:)];	

	

	[fetcher start];

	

	

	[oRequest release];

}



- (void)sendImageTicket:(OAServiceTicket *)ticket didFinishWithData:(NSData *)data {

	// TODO better error handling here

	// NSLog([[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease]);

	

	if (ticket.didSucceed) {

		[self sendDidFinish];

		// Finished uploading Image, now need to posh the message and url in twitter

		NSString *dataString = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];

		NSRange startingRange = [dataString rangeOfString:@"<url>" options:NSCaseInsensitiveSearch];

		//NSLog(@"found start string at %d, len %d",startingRange.location,startingRange.length);

		NSRange endingRange = [dataString rangeOfString:@"</url>" options:NSCaseInsensitiveSearch];

		//NSLog(@"found end string at %d, len %d",endingRange.location,endingRange.length);

		

		if (NO || (startingRange.location != NSNotFound && endingRange.location != NSNotFound)) {

			NSString *urlString = [dataString substringWithRange:NSMakeRange(startingRange.location + startingRange.length, endingRange.location - (startingRange.location + startingRange.length))];

			//NSLog(@"extracted string: %@",urlString);

			[item setCustomValue:[NSString stringWithFormat:@"%@ %@",[item customValueForKey:@"status"],urlString] forKey:@"status"];

			[self sendStatus];

		}

		

		

	} else {

		[self sendDidFailWithError:nil];

	}

}



- (void)sendImageTicket:(OAServiceTicket *)ticket didFailWithError:(NSError*)error {

	[self sendDidFailWithError:error];

}





- (void)followMe

{

	// remove it so in case of other failures this doesn't get hit again

	[item setCustomValue:nil forKey:@"followMe"];

	

	OAMutableURLRequest *oRequest = [[OAMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://api.twitter.com/1.1/friendships/create/%@.json", SHKTwitterUsername]]

																	consumer:consumer

																	   token:accessToken

																	   realm:nil

														   signatureProvider:nil];

	

	[oRequest setHTTPMethod:@"POST"];

	

	OAAsynchronousDataFetcher *fetcher = [OAAsynchronousDataFetcher asynchronousFetcherWithRequest:oRequest

						 delegate:nil // Currently not doing any error handling here.  If it fails, it's probably best not to bug the user to follow you again.

				didFinishSelector:nil

				  didFailSelector:nil];	

	

	[fetcher start];

	[oRequest release];

}

- (BOOL)twitterFrameworkAvailable {
        return NO;
}

- (void)tokenAuthorizeCancelledView:(SHKOAuthView *)authView {

//    [[SHKActivityIndicator currentIndicator] hide];

//    UIViewController *vc3 = vc2.parentViewController;

    

//    NSLog(@"ok, arrived here, tokenAuthorizeCancelledView");

}



@end

