//
//  ImageFilterProcessViewController.h
//  MeiJiaLove
//
//  Created by Laura on 15-3-6.
//  Copyright (c) 2013年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"

@protocol ImageFitlerProcessDelegate;
@interface ImageFilterProcessViewController : UIViewController
{
    UIImageView *rootImageView;
    UIScrollView *scrollerView;
    UIImage *currentImage;
}
@property(nonatomic,assign) id<ImageFitlerProcessDelegate> delegate;
@property (nonatomic,retain)IMGRestaurant *restaurant;
@property (nonatomic,assign)BOOL isWriteReview;

@property(nonatomic,retain)UIImage *currentImage;
@end

@protocol ImageFitlerProcessDelegate <NSObject>

- (void)imageFitlerProcessDone:(UIImage *)image;
@end
