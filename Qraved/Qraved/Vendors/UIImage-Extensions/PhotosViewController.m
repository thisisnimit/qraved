//
//  PhotosViewController.m
//  Qraved
//
//  Created by Laura on 15/3/6.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "PhotosViewController.h"
#import "PhotoCollectionController.h"
#import "IMGPhoto.h"
#import <AssetsLibrary/AssetsLibrary.h>


@interface PhotosViewController ()

@end

@implementation PhotosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self cameraRollButtonPressed];
}
- (void)cameraRollButtonPressed
{
    NSMutableArray *datasource = [[NSMutableArray alloc] init];
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if (group) {
            [group setAssetsFilter:[ALAssetsFilter allPhotos]];
            [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
                if (result) {
                    IMGPhoto *photo = [[IMGPhoto alloc] init];
                    photo.thumbnail = [UIImage imageWithCGImage:result.thumbnail];
                    photo.date = [result valueForProperty:ALAssetPropertyDate];
                    [datasource addObject:photo];
                    NSLog(@"%ld", (unsigned long)[datasource count]);
                }
            }];
        } else {
            [self performSelectorOnMainThread:@selector(showPhotoCollectionController:) withObject:datasource waitUntilDone:NO];
        }
    } failureBlock:^(NSError *error) {
        NSLog(@"Failed.");
    }];
}
 
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
