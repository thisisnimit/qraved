//
//  NADiscountCell.h
//  Qraved
//
//  Created by Shine Wang on 12/12/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "NAPickerCell.h"

@interface NADiscountCell : NAPickerCell
@property (strong, nonatomic) UILabel *timeLabel;
@property (strong, nonatomic) UILabel *discountLabel;
@end
