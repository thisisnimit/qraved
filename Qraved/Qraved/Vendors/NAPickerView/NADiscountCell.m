//
//  NADiscountCell.m
//  Qraved
//
//  Created by Shine Wang on 12/12/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "NADiscountCell.h"
#import "UILabel+Helper.h"
#import "UIConstants.h"
#import "UIView+Helper.h"

@implementation NADiscountCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier cellWidth:(CGFloat)cellWidth
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.timeLabel = [[UILabel alloc] init];
        [self.timeLabel configPickerSubTitle];
        CGFloat timeLabelHeight=self.timeLabel.font.lineHeight;
        [self.timeLabel setFrame:CGRectMake(10, 12, 80, timeLabelHeight)];
        [self addSubview:self.timeLabel];
        
        self.discountLabel = [[UILabel alloc] init];
        [self.discountLabel configPickerTitle];
        CGFloat discountLabelHeight=self.discountLabel.font.lineHeight;
        [self.discountLabel setFrame:CGRectMake(10, self.timeLabel.endPointY, 120, discountLabelHeight)];
        [self addSubview:self.discountLabel];
        
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

+ (CGFloat)cellHeight
{
    return PICKER_VIEW_HEIGHT;
}

@end
