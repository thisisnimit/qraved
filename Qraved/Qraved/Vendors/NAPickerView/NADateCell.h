//
//  NADateCell.h
//  Qraved
//
//  Created by Shine Wang on 12/12/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "NAPickerCell.h"

@interface NADateCell : NAPickerCell

@property (strong, nonatomic) UILabel *weekLabel;
@property (strong, nonatomic) UILabel *dateLabel;
@end
