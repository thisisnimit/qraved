//
//  NADateCell.m
//  Qraved
//
//  Created by Shine Wang on 12/12/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "NADateCell.h"
#import "UILabel+Helper.h"
#import "UIConstants.h"
#import "UIView+Helper.h"

@implementation NADateCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier cellWidth:(CGFloat)cellWidth
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.weekLabel = [[UILabel alloc] init];
        [self.weekLabel setText:@""];
        [self.weekLabel configPickerSubTitle];
        CGFloat labelHeight=self.weekLabel.font.lineHeight;
        [self.weekLabel setFrame:CGRectMake(10, 12, 100, labelHeight)];
        [self addSubview:self.weekLabel];
        
        self.dateLabel = [[UILabel alloc] init];
        [self.dateLabel setText:@""];
        [self.dateLabel configPickerTitle];
         CGFloat labelSubHeight=self.dateLabel.font.lineHeight;
        [self.dateLabel setFrame:CGRectMake(10,self.weekLabel.endPointY, 100, labelSubHeight)];
        [self addSubview:self.dateLabel];
        
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


+ (CGFloat)cellHeight
{
    return PICKER_VIEW_HEIGHT;
}
@end
