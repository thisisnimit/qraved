//
//  NACustomCell.m
//  NAPickerView
//
//  Created by iNghia on 8/5/13.
//  Copyright (c) 2013 nghialv. All rights reserved.
//

#import "NACustomCell.h"
#import "UILabel+Helper.h"
#import "UIConstants.h"

@implementation NACustomCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier cellWidth:(CGFloat)cellWidth
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.label = [[UILabel alloc] init];
        [self.label setFrame:CGRectMake(0, 0, cellWidth, PICKER_VIEW_HEIGHT)];
        [self addSubview:self.label];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

+ (CGFloat)cellHeight
{
    return PICKER_VIEW_HEIGHT;
}

@end
