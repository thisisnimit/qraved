/*
 
 DLStarRating
 Copyright (C) 2011 David Linsin <dlinsin@gmail.com>
 
 All rights reserved. This program and the accompanying materials
 are made available under the terms of the Eclipse Public License v1.0
 which accompanies this distribution, and is available at
 http://www.eclipse.org/legal/epl-v10.html
 
 */

#import "DLStarRatingControl.h"
#import "DLStarView.h"
#import "UIView+Subviews.h"
#import "IMGUser.h"
@interface DLStarRatingControl ()
 @property(nonatomic,assign) CGFloat space;
@end

@implementation DLStarRatingControl{
   
}

@synthesize star, highlightedStar, delegate, isFractionalRatingEnabled;

#pragma mark -
#pragma mark Initialization

- (UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize
{
    
//    UIGraphicsBeginImageContext(CGSizeMake(image.size.width * scaleSize, image.size.height * scaleSize));
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(image.size.width * scaleSize, image.size.height * scaleSize),NO,image.scale);
    
    [image drawInRect:CGRectMake(0, 0, image.size.width * scaleSize, image.size.height * scaleSize)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();

    UIGraphicsEndImageContext();
    
    return scaledImage;
}

- (void)setupView {
    self.userInteractionEnabled = YES;

	self.clipsToBounds = YES;
	currentIdx = -1;
//    IMGUser *user = [IMGUser currentUser];
//    if ((![user.userId intValue])&&self.isFromRDP) {
//        self.star = [UIImage imageNamed:@"ic_star_full.png"];
//    }else{
//        self.star = [UIImage imageNamed:@"ic_star_full.png"];
//    }
//	self.highlightedStar = [UIImage imageNamed:@"ic_star_full.png"];
    if (!isFractionalRatingEnabled) {
        self.star = [UIImage imageNamed:@"star_kong"];
        self.highlightedStar = [UIImage imageNamed:@"ic_star_full"];
    }
    
    
//        star = [[UIImage imageNamed:@"star"] retain];
//    	highlightedStar = [[UIImage imageNamed:@"star_highlighted"] retain];
    
    
    if(fabs(star.size.width-self.starWidth)>0.001 && isFractionalRatingEnabled){
        self.star = [self scaleImage:star toScale:self.starWidth/self.star.size.width*0.9 ];
        self.highlightedStar = [self scaleImage:highlightedStar toScale:self.starWidth/self.highlightedStar.size.width ];
    }

    
	for (int i=0; i<numberOfStars; i++) {
        DLStarView *v;
        if(self.starWidth)
        {
		    v = [[DLStarView alloc] initWithDefault:self.star highlighted:self.highlightedStar position:i allowFractions:isFractionalRatingEnabled andWidth:self.starWidth spaceWidth:_space];
        }
        else
        {
            v = [[DLStarView alloc] initWithDefault:self.star highlighted:self.highlightedStar position:i allowFractions:isFractionalRatingEnabled];
        }
		[self addSubview:v];
        
//        [self setBackgroundColor:[UIColor redColor]];
		[v release];
	}
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
		numberOfStars = kDefaultNumberOfStars;
        if (isFractionalRatingEnabled)
            numberOfStars *=kNumberOfFractions;
		[self setupView];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if (self) {
		numberOfStars = kDefaultNumberOfStars;
        if (isFractionalRatingEnabled)
            numberOfStars *=kNumberOfFractions;
        [self setupView];
        
	}
	return self;
}

- (id)initWithFrame:(CGRect)frame andStars:(NSUInteger)_numberOfStars isFractional:(BOOL)isFract{
	self = [super initWithFrame:frame];
	if (self) {
        isFractionalRatingEnabled = isFract;
		numberOfStars = _numberOfStars;
        if (isFractionalRatingEnabled)
            numberOfStars *=kNumberOfFractions;
		[self setupView];
	}
	return self;
}
- (id)initWithFrame:(CGRect)frame andStars:(NSUInteger)_numberOfStars andStarWidth:(CGFloat)starWidth isFractional:(BOOL)isFract spaceWidth:(CGFloat) spaceWidth{
    self.space=spaceWidth;
    return  [self initWithFrame:frame andStars:_numberOfStars andStarWidth:starWidth isFractional:isFract];
}
- (id)initWithFrame:(CGRect)frame andStars:(NSUInteger)_numberOfStars andStarWidth:(CGFloat)starWidth isFractional:(BOOL)isFract spaceWidth:(CGFloat) spaceWidth isFromRDP:(BOOL)isFromRDP{
    self.space=spaceWidth;
    self.isFromRDP = isFromRDP;
    return  [self initWithFrame:frame andStars:_numberOfStars andStarWidth:starWidth isFractional:isFract];
}
- (id)initWithFrame:(CGRect)frame andStars:(NSUInteger)_numberOfStars andStarWidth:(CGFloat)starWidth isFractional:(BOOL)isFract
{
    self = [super initWithFrame:frame];
	if (self) {
        isFractionalRatingEnabled = isFract;
		numberOfStars = _numberOfStars;
        self.starWidth=starWidth;
        if (isFractionalRatingEnabled)
            numberOfStars *=kNumberOfFractions;
		[self setupView];
	}
	return self;
    
}

//- (void)layoutSubviews {
//	for (int i=0; i < numberOfStars; i++) {
//		//[(DLStarView*)[self subViewWithTag:i] centerIn:self.frame with:numberOfStars];
//	}
//}

#pragma mark -
#pragma mark Customization

- (void)setStar:(UIImage*)defaultStarImage highlightedStar:(UIImage*)highlightedStarImage {
    for(NSInteger i = 0; i < numberOfStars; i++){
        [self setStar:defaultStarImage highlightedStar:highlightedStarImage atIndex:i];
    }
}

- (void)setStar:(UIImage*)defaultStarImage highlightedStar:(UIImage*)highlightedStarImage atIndex:(NSInteger)index {
    DLStarView *selectedStar = (DLStarView*)[self subViewWithTag:index];
    
    // check if star exists
    if (!selectedStar) return;
    
    // check images for nil else use default stars
    defaultStarImage = (defaultStarImage) ? defaultStarImage : star;
    highlightedStarImage = (highlightedStarImage) ? highlightedStarImage : highlightedStar;
    
    [selectedStar setStarImage:defaultStarImage highlightedStarImage:highlightedStarImage];
}

#pragma mark -
#pragma mark Touch Handling

- (UIButton*)starForPoint:(CGPoint)point {
	for (int i=0; i < numberOfStars; i++) {
		if (CGRectContainsPoint([self subViewWithTag:i].frame, point)) {
			return (UIButton*)[self subViewWithTag:i];
		}
	}
	return nil;
}

- (void)disableStarsDownToExclusive:(NSInteger)idx {
	for (NSInteger i=numberOfStars; i > idx; --i) {
		UIButton *b = (UIButton*)[self subViewWithTag:i];
		b.highlighted = NO;
        if (!isFractionalRatingEnabled) {
            for (UIView *subView in b.subviews) {
                if ([subView isKindOfClass:[UIButton class]]) {
                    UIButton *button = (UIButton*)subView;
                    button.highlighted = NO;
                }
            }
        }
	}
}

- (void)disableStarsDownTo:(int)idx {
	for (NSInteger i=numberOfStars; i >= idx; --i) {
		UIButton *b = (UIButton*)[self subViewWithTag:i];
		b.highlighted = NO;
        if (!isFractionalRatingEnabled) {
            for (UIView *subView in b.subviews) {
                if ([subView isKindOfClass:[UIButton class]]) {
                    UIButton *button = (UIButton*)subView;
                    button.highlighted = NO;
                }
            }
        }
	}
}


- (void)enableStarsUpTo:(NSInteger)idx {
	for (int i=0; i <= idx; i++) {
		UIButton *b = (UIButton*)[self subViewWithTag:i];
		b.highlighted = YES;
        if (!isFractionalRatingEnabled) {
            for (UIView *subView in b.subviews) {
                if ([subView isKindOfClass:[UIButton class]]) {
                    UIButton *button = (UIButton*)subView;
                    button.highlighted = YES;
                }
            }
        }
	}
}

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
	CGPoint point = [touch locationInView:self];
	UIButton *pressedButton = [self starForPoint:point];
	if (pressedButton) {
		NSInteger idx = pressedButton.tag;
		if (pressedButton.highlighted) {
			[self disableStarsDownToExclusive:idx];
		} else {
			[self enableStarsUpTo:idx];
		}
		currentIdx = idx;
	}
	return YES;
}

- (void)cancelTrackingWithEvent:(UIEvent *)event {
	[super cancelTrackingWithEvent:event];
}

- (BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
	CGPoint point = [touch locationInView:self];
	
	UIButton *pressedButton = [self starForPoint:point];
	if (pressedButton) {
		NSInteger idx = pressedButton.tag;
		UIButton *currentButton = (UIButton*)[self subViewWithTag:currentIdx];
		
		if (idx < currentIdx) {
			currentButton.highlighted = NO;
			currentIdx = idx;
			[self disableStarsDownToExclusive:idx];
		} else if (idx > currentIdx) {
			currentButton.highlighted = YES;
			pressedButton.highlighted = YES;
			currentIdx = idx;
			[self enableStarsUpTo:idx];
		}
	} else if (point.x < [self subViewWithTag:0].frame.origin.x) {
        UIButton*v=(UIButton*)[self subViewWithTag:0];
        v.highlighted = NO;
        if (!isFractionalRatingEnabled) {
            for (UIView *subView in v.subviews) {
                if ([subView isKindOfClass:[UIButton class]]) {
                    UIButton *button = (UIButton *)subView;
                    button.highlighted = NO;
                }
            }
        }
		currentIdx = -1;
		[self disableStarsDownToExclusive:0];
	}
	return YES;
}

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
	[self.delegate newRating:self :self.rating];
	[super endTrackingWithTouch:touch withEvent:event];
}

#pragma mark -
#pragma mark Rating Property

- (void)setRating:(float)_rating {
    if (isFractionalRatingEnabled) {
        _rating *=kNumberOfFractions;
    }
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    IMGUser *user = [IMGUser currentUser];
    if ((![user.userId intValue])&&self.isFromRDP) {
        self.star = [UIImage imageNamed:@"star_.png"];
    }else{
        self.star = [UIImage imageNamed:@"ic_star_grey1.png"];
    }
    if (_rating<=3) {
        self.highlightedStar = [UIImage imageNamed:@"ic_star_brown"];
    }else if (_rating>=4 && _rating<=5){
        self.highlightedStar = [UIImage imageNamed:@"ic_star_yellow"];
    }else if (_rating>=6 && _rating<=7){
        self.highlightedStar = [UIImage imageNamed:@"ic_star_orange.png"];
    }else if (_rating>=8 && _rating<=9){
        self.highlightedStar = [UIImage imageNamed:@"ic_star_rose.png"];
    }else{
        self.highlightedStar = [UIImage imageNamed:@"ic_star_red.png"];
    }
    if (!isFractionalRatingEnabled) {
        self.star = [UIImage imageNamed:@"star_kong.png"];
        self.highlightedStar = [UIImage imageNamed:@"ic_star_full.png"];
    }
     //self.highlightedStar = [UIImage imageNamed:@"ic_star_full.png"];
    
    //    star = [[UIImage imageNamed:@"star"] retain];
    //	highlightedStar = [[UIImage imageNamed:@"star_highlighted"] retain];
    
    
    if(fabs(star.size.width-self.starWidth)>0.001 && isFractionalRatingEnabled){
        self.star = [self scaleImage:star toScale:self.starWidth/self.star.size.width*0.9 ];
        self.highlightedStar = [self scaleImage:highlightedStar toScale:self.starWidth/self.highlightedStar.size.width ];
    }
    
    
    for (int i=0; i<numberOfStars; i++) {
        DLStarView *v;
        if(self.starWidth)
        {
            v = [[DLStarView alloc] initWithDefault:self.star highlighted:self.highlightedStar position:i allowFractions:isFractionalRatingEnabled andWidth:self.starWidth spaceWidth:_space];
        }
        else
        {
            v = [[DLStarView alloc] initWithDefault:self.star highlighted:self.highlightedStar position:i allowFractions:isFractionalRatingEnabled];
        }
        [self addSubview:v];
        
        //        [self setBackgroundColor:[UIColor redColor]];
        [v release];
    }

	[self disableStarsDownTo:0];
	currentIdx = (int)_rating-1;
	[self enableStarsUpTo:currentIdx];
}

- (void)updateRating:(NSNumber *) inputRating{
    
    float rat = [inputRating floatValue]/2;
    float fff = floorf(rat);
    float f2= rat-fff;
    if (f2<0.3) {
        f2 =0;
    }else if (f2<0.8){
        f2 = 0.5;
    }else {
        f2 = 1;
    }
    rat = fff + f2;
    
    self.rating = rat;
}

- (float)rating {
    if (isFractionalRatingEnabled) {
        return (float)(currentIdx+1)/kNumberOfFractions;
    }
	return (NSUInteger)currentIdx+1;
}


#pragma mark -
#pragma mark Memory Management

- (void)dealloc {
	self.star = nil;
	self.highlightedStar = nil;
	self.delegate = nil;
	[super dealloc];
}

@end
