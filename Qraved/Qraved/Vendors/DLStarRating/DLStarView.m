/*
 
 DLStarRating
 Copyright (C) 2011 David Linsin <dlinsin@gmail.com>
 
 All rights reserved. This program and the accompanying materials
 are made available under the terms of the Eclipse Public License v1.0
 which accompanies this distribution, and is available at
 http://www.eclipse.org/legal/epl-v10.html
 
 */

#import "DLStarView.h"
#import "DLStarRatingControl.h"

@implementation DLStarView
{
    UIImage *star;
    UIImage *highlightedStar;
    UIButton *button;
}
#pragma mark -
#pragma mark Initialization

- (id)initWithDefault:(UIImage*)_star highlighted:(UIImage*)_highlightedStar position:(int)index allowFractions:(BOOL)fractions {
	return [self initWithDefault:_star highlighted:_highlightedStar position:index allowFractions:fractions andWidth:[self getStarWidth]];
}
- (id)initWithDefault:(UIImage*)_star highlighted:(UIImage*)_highlightedStar position:(int)index allowFractions:(BOOL)fractions andWidth:(CGFloat)starWidth {
    return [self initWithDefault:_star highlighted:_highlightedStar position:index allowFractions:fractions  andWidth:starWidth spaceWidth:0];
}

- (id)initWithDefault:(UIImage*)_star highlighted:(UIImage*)_highlightedStar position:(int)index allowFractions:(BOOL)fractions andWidth:(CGFloat)starWidth spaceWidth:(CGFloat) spaceWidth {
    self = [super initWithFrame:CGRectZero];
    if (self) {
        [self setTag:index];
        
        
        if (fractions) {
            highlightedStar = [self croppedImage:_highlightedStar];
            star = [self croppedImage:_star];
        }else{
            star = _star;
            highlightedStar = _highlightedStar;
        }
        
        self.frame = CGRectMake((starWidth*index), 0, starWidth, [self getStarHeigth]+kEdgeInsetBottom);
        
        if(!fractions)
        {
            self.frame = CGRectMake((starWidth+spaceWidth)*index, 0, starWidth+spaceWidth, starWidth+spaceWidth+kEdgeInsetBottom);
            button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(spaceWidth/2, spaceWidth/2, starWidth, starWidth+kEdgeInsetBottom);
            [self addSubview:button];
        }
        else
        {
            self.frame = CGRectMake((star.size.width*index)+(index>0?index/2*spaceWidth:0), 0, star.size.width, star.size.height+kEdgeInsetBottom);
        }
        [self setStarImage:star highlightedStarImage:highlightedStar];
		[self setImageEdgeInsets:UIEdgeInsetsMake(0, 0, kEdgeInsetBottom, 0)];
		[self setBackgroundColor:[UIColor clearColor]];
        if (index == 0) {
   	        [self setAccessibilityLabel:@"1 star"];
        } else {
   	        [self setAccessibilityLabel:[NSString stringWithFormat:@"%d stars", index+1]];
        }
	}
	return self;

}



-(float)getStarWidth{
    return 36;
}
-(float)getStarHeigth{
    return 36;
}

- (UIImage *)croppedImage:(UIImage*)image {
    float partWidth = image.size.width/kNumberOfFractions * image.scale;
    int part = (self.tag+kNumberOfFractions)%kNumberOfFractions;

    float xOffset = partWidth*part;
    CGRect newFrame = CGRectMake(xOffset, 0, partWidth , image.size.height * image.scale);
    CGImageRef resultImage = CGImageCreateWithImageInRect([image CGImage], newFrame);
    UIImage *result = [UIImage imageWithCGImage:resultImage scale:image.scale orientation:image.imageOrientation];
    CGImageRelease(resultImage);
    return result;
}



#pragma mark -
#pragma mark UIView methods

- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
	return self.superview;
}

#pragma mark -
#pragma mark Layouting

- (void)centerIn:(CGRect)_frame with:(int)numberOfStars {
	CGSize size = self.frame.size;
	
	float height = self.frame.size.height;
	float frameHeight = _frame.size.height;
	float newY = (frameHeight-height)/2;
	
	float widthOfStars = self.frame.size.width * numberOfStars;
	float frameWidth = _frame.size.width;
	float gapToApply = (frameWidth-widthOfStars)/2;
	
	self.frame = CGRectMake((size.width*self.tag) + gapToApply, newY, size.width, size.height);
}

- (void)setStarImage:(UIImage*)starImage highlightedStarImage:(UIImage*)highlightedImage {
    if (button) {
        [button setImage:starImage forState:UIControlStateNormal];
        [button setImage:highlightedImage forState:UIControlStateSelected];
        [button setImage:highlightedImage forState:UIControlStateHighlighted];
    }
    else{
        [self setImage:starImage forState:UIControlStateNormal];
        [self setImage:highlightedImage forState:UIControlStateSelected];
        [self setImage:highlightedImage forState:UIControlStateHighlighted];
    }
}

@end
