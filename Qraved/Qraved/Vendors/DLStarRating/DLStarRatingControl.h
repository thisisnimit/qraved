/*
 
 DLStarRating
 Copyright (C) 2011 David Linsin <dlinsin@gmail.com>
 
 All rights reserved. This program and the accompanying materials
 are made available under the terms of the Eclipse Public License v1.0
 which accompanies this distribution, and is available at
 http://www.eclipse.org/legal/epl-v10.html
 
 */

#import <UIKit/UIKit.h>

#define kDefaultNumberOfStars 5

#define kNumberOfFractions 2

@protocol DLStarRatingDelegate;

@interface DLStarRatingControl : UIControl {
	NSInteger numberOfStars;
	NSInteger currentIdx;
	UIImage *star;
	UIImage *highlightedStar;
	IBOutlet id<DLStarRatingDelegate> delegate;
    BOOL isFractionalRatingEnabled;
}

- (id)initWithFrame:(CGRect)frame;
- (id)initWithFrame:(CGRect)frame andStars:(NSUInteger)_numberOfStars isFractional:(BOOL)isFract;
- (id)initWithFrame:(CGRect)frame andStars:(NSUInteger)_numberOfStars andStarWidth:(CGFloat)starWidth isFractional:(BOOL)isFract;
- (id)initWithFrame:(CGRect)frame andStars:(NSUInteger)_numberOfStars andStarWidth:(CGFloat)starWidth isFractional:(BOOL)isFract spaceWidth:(CGFloat) spaceWidth;
- (void)setStar:(UIImage*)defaultStarImage highlightedStar:(UIImage*)highlightedStarImage;
- (void)setStar:(UIImage*)defaultStarImage highlightedStar:(UIImage*)highlightedStarImage atIndex:(NSInteger)index;
- (id)initWithFrame:(CGRect)frame andStars:(NSUInteger)_numberOfStars andStarWidth:(CGFloat)starWidth isFractional:(BOOL)isFract spaceWidth:(CGFloat) spaceWidth isFromRDP:(BOOL)isFromRDP;
-(void)setupView;

- (void)updateRating:(NSNumber *) inputRating;

@property (retain,nonatomic) UIImage *star;
@property (retain,nonatomic) UIImage *highlightedStar;
@property (nonatomic) float rating;
@property (assign,nonatomic) id<DLStarRatingDelegate> delegate;
@property (nonatomic,assign) BOOL isFractionalRatingEnabled;
@property(nonatomic,assign)CGFloat starWidth;
@property(nonatomic,assign)BOOL isFromRDP;

@end

@protocol DLStarRatingDelegate

-(void)newRating:(DLStarRatingControl *)control :(float)rating;


@end
