//
//  EScrollerView.m
//  icoiniPad
//
//  Created by Ethan on 12-11-24.
//
//

#import "PhotosEScrollerView.h"
#import "AFNetworking.h"
#import "UIConstants.h"

#import "UIImageView+WebCache.h"
#import "UIImage+Helper.h"
#import "UIImage+Resize.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"


#import "IMGEvent.h"

@implementation PhotosEScrollerView
{
//    NSMutableArray *imageViewArray;
//    NSMutableArray *imageUrlArray;
    UIImageView *placeImageView;
}
@synthesize delegate;
-(void)setNilAndDealloc{
    
    
}
- (void)dealloc {
//	NSLog(@"%d",self.retainCount);
	delegate=nil;
    if (pageControl) {
        pageControl = nil;
        [pageControl release];
    }
    if (objectArray) {
        objectArray=nil;
        [objectArray release];
    }
    [noteTitle release];
    [scrollView release];
    [super dealloc];
    
}
-(void) initPageControl{
    /*
     PageControl
     */
    float pageControlWidth;
    float pagecontrolHeight=10.0f;
    
    NSInteger numberOfPages;
    if (_isCirculation) {
        pageControlWidth=(_pageCount-2)*10.0f;
        numberOfPages = _pageCount-2;
    } else{
        pageControlWidth=_pageCount*10.0f;
        numberOfPages = _pageCount;
    }
    
    pageControl=[[CustomPageControl alloc]init];
    
    pageControl.numberOfPages=numberOfPages;
    
    //
    
	[pageControl setNumberOfPages: numberOfPages] ;
	[pageControl setCurrentPage: 0] ;
	[pageControl addTarget: self action: @selector(pageControlClicked:) forControlEvents: UIControlEventValueChanged] ;
	[pageControl setDefersCurrentPageDisplay: YES] ;
	[pageControl setType: DDPageControlTypeOnFullOffFull] ;
	[pageControl setOnColor: [UIColor whiteColor]] ;
	[pageControl setOffColor: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:0.2]] ;
	[pageControl setIndicatorDiameter: 3.5f] ;
	[pageControl setIndicatorSpace: 10.0f] ;
    //
    
    if (_isStartScroll) {
        if (_PageControlCenter) {
            [pageControl setCenter: CGPointMake(self.center.x, self.bounds.size.height-15.0f)] ;
        } else{
            [pageControl setCenter: CGPointMake(self.center.x+80, self.bounds.size.height-17.0f)] ;
        }
        if (_photoTag) {
            pageControl.currentPage = _photoTag;
        } else{
            pageControl.currentPage = 0;
        }
    }else{
        pagecontrolHeight=20.0f;
        
        [pageControl setFrame:CGRectMake((self.frame.size.width-pageControlWidth),38, pageControlWidth, pagecontrolHeight)];
        
        pageControl.currentPage=0;
        pageControl.numberOfPages=numberOfPages;
        
    }
    [self addSubview:pageControl];
}

-(id)initWithFrameRect:(CGRect)rect objectArray:(NSArray *)tmpObjectArray isStartScroll:(BOOL)isStartScroll withPhotoTag:(NSInteger)photoTag pageControlCenter:(BOOL)PageControlCenter isCirculation:(BOOL)isCirculation isPageControl:(BOOL)isPageControl isEvent:(BOOL)isEvent isPlaceHolder:(BOOL)placeHolder sizeFitWidth:(BOOL)sizeFitWidth
{
    
    _isStartScroll =isStartScroll;
    _isCirculation = isCirculation;
    _photoTag = photoTag;
    _PageControlCenter = PageControlCenter;
    _placeHolder = placeHolder;
    _event = isEvent;
    _sizeFitWidth=sizeFitWidth;
    if (tmpObjectArray.count<=1)
    {
        _isCirculation=NO;
    }
    
    if ((self=[super initWithFrame:rect])) {
        self.userInteractionEnabled=YES;
        if (_isCirculation) {
            if(tmpObjectArray!=nil && tmpObjectArray.count>0){
                NSMutableArray *tempArray=[tmpObjectArray mutableCopy];
                [tempArray insertObject:[tmpObjectArray objectAtIndex:([tmpObjectArray count]-1)] atIndex:0];
                [tempArray addObject:[tmpObjectArray objectAtIndex:0]];
                objectArray=[tempArray copy];
                [tempArray release];
            }
        }
        else
        {
            objectArray = tmpObjectArray;
        }
        
        viewSize=rect;
        _pageCount=[objectArray count];
        scrollView=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, viewSize.size.width, viewSize.size.height)];
        scrollView.pagingEnabled = YES;
        scrollView.contentSize = CGSizeMake(viewSize.size.width * _pageCount, viewSize.size.height);
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.showsVerticalScrollIndicator = NO;
        scrollView.scrollsToTop = NO;
        scrollView.delegate = self;

        if(_pageCount>3){
            [self addImageView:_photoTag+2];
            [self addImageView:_photoTag];
            [self addImageView:_photoTag+1];
        }else{
            for(int i=0;i<_pageCount;i++){
                [self addImageView:i];
            }
        }

        if(_isCirculation){
            [self showImage:_photoTag+1];    
        }else{
            [self showImage:_photoTag];
        }
        
        [self addSubview:scrollView];
        
        if (isPageControl) {
            if (tmpObjectArray.count > 1)
            {
                [self initPageControl];
            }
        }
    }
    
    return self;
}

- (void)willMoveToWindow:(UIWindow *)newWindow
{
    scrollView.pagingEnabled = YES;
    [super willMoveToWindow:newWindow];
    if(newWindow)
    {
        [scrollView.panGestureRecognizer addTarget:self action:@selector(gestureDidChange:)];
        [scrollView.pinchGestureRecognizer addTarget:self action:@selector(gestureDidChange:)];
    }
    else
    {
        [self stopScrolling];
        [scrollView.panGestureRecognizer removeTarget:self action:@selector(gestureDidChange:)];
        [scrollView.pinchGestureRecognizer removeTarget:self action:@selector(gestureDidChange:)];
    }
}
#pragma mark - Touch methods

- (BOOL)touchesShouldBegin:(NSSet *)touches withEvent:(UIEvent *)event inContentView:(UIView *)view
{
    [self stopScrolling];
    return [scrollView touchesShouldBegin:touches withEvent:event inContentView:view];
}

- (void)gestureDidChange:(UIGestureRecognizer *)gesture
{
    switch (gesture.state)
    {
        case UIGestureRecognizerStateBegan:
        {
            [self stopScrolling];
            
        }
            break;
        case UIGestureRecognizerStateEnded:
        {
            if (_isCirculation) {
                if (_isStartScroll) {
                    [self startScrolling];
                    if (currentPageIndex==([objectArray count]-1)) {
                        
                        [scrollView setContentOffset:CGPointMake(viewSize.size.width, 0)];
                        
                    }
                }
                else
                {
                    if (currentPageIndex==([objectArray count]-1)) {
                        
                        [scrollView setContentOffset:CGPointMake(viewSize.size.width, 0)];
                        
                    }
                }
            }
            else
            {
                
            }
            
        }
        default:
            break;
    }
}

-(void)showImage:(NSInteger)page{

    [scrollView setContentOffset:CGPointMake(viewSize.size.width*page, 0)];
}

-(void)addImageView:(NSInteger)tag{

    if((![self viewWithTag:tag] || self.subviews.count==0) && objectArray.count>=tag){
        
        
        NSObject *entity = [objectArray objectAtIndex:tag];
        
        NSString *imgURL=((IMGEntity *)entity).imageUrl;
            
        UIImageView *imgView=[[UIImageView alloc] initWithFrame: CGRectMake(viewSize.size.width*tag, 0,viewSize.size.width, viewSize.size.height)];
            
        placeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, viewSize.size.width, viewSize.size.height)];
        imgView.clipsToBounds = YES;
        if(_PageControlCenter == NO){
            imgView.contentMode = UIViewContentModeTop;
        }else{
            imgView.contentMode = UIViewContentModeScaleAspectFill;
        }
            
        UIImage *placeHolderImage1 = [UIImage imageWithColor:[UIColor colorWithRed:.6 green:.6 blue:.6 alpha:0.2] size:CGSizeMake(DeviceWidth, 160)];
            
        if ([imgURL hasPrefix:@"data/"] || [imgURL hasPrefix:@"/data"]) {

            [imgView sd_setImageWithURL:[NSURL URLWithString:[imgURL returnFullImageUrlWithWidth:viewSize.size.width-LEFTLEFTSET*2]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2]];
//            [imgView setImageWithURL:[NSURL URLWithString:[imgURL returnFullImageUrlWithWidth:viewSize.size.width-LEFTLEFTSET*2]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] sizeFitWidth:_sizeFitWidth];

        }else{
            UIImage *img=[UIImage imageNamed:((IMGEntity *)[objectArray objectAtIndex:tag]).imageUrl];
            [imgView setImage:img];
        }
            
        if (_placeHolder) {
            placeImageView.image = [UIImage imageNamed:@"DetailTopBg@2x.png"];
            
        }else{
            placeImageView.image = placeHolderImage1;
        }

        [imgView addSubview:placeImageView];
        UITapGestureRecognizer *Tap =[[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagePressed:)] autorelease];
        placeImageView.userInteractionEnabled=YES;
        placeImageView.tag = tag;
        imgView.tag = tag;
        imgView.userInteractionEnabled = YES;
        [placeImageView addGestureRecognizer:Tap];
        [scrollView addSubview:imgView];
        
    }
}

- (void)startScrolling
{
    [self stopScrolling];
    _scrollTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(updateScroll) userInfo:nil repeats:YES];
}

- (void)stopScrolling
{
    if (_isCirculation) {
        [_scrollTimer invalidate];
        _scrollTimer = nil;
    }
    else
    {
        //        scrollView.scrollEnabled = YES;
    }
    
    
}
- (void)updateScroll
{
    CGFloat pointChange = viewSize.size.width;
    CGPoint newOffset = scrollView.contentOffset;
    
    
    if (_isCirculation) {
        if (currentPageIndex==([objectArray count]-2)) {
            newOffset.x = pointChange;
            scrollView.contentOffset = newOffset;
        }
        else
        {
            newOffset.x = newOffset.x + pointChange;
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:1];
            scrollView.contentOffset = newOffset;
            [UIView commitAnimations];
        }
    }
    else
    {
        
    }
}



- (void)scrollViewDidScroll:(UIScrollView *)sender
{
 
    if (_isCirculation) {
        CGFloat pageWidth = scrollView.frame.size.width;
        NSInteger page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        currentPageIndex=page;
        if (page-1 == objectArray.count-2) {
            page = 1;
        }else if(page==0){
            page=objectArray.count-2;
        }
        //        pageControl.currentPage=(page-1);
        
        if (pageControl.currentPage != page-1)
        {
            pageControl.currentPage = page-1 ;
            
            // if we are dragging, we want to update the page control directly during the drag
            if (scrollView.dragging)
                [pageControl updateCurrentPageDisplay];
        }
        if ([delegate respondsToSelector:@selector(PhotosEScrollerViewDidClicked: ContentOffset:)]) {
            [self addImageView:pageControl.currentPage+2];
            [self addImageView:pageControl.currentPage];
            if(pageControl.currentPage==0){
                [self addImageView:0];
                [self addImageView:_pageCount-2];
            }
            if(pageControl.currentPage==_pageCount-3){
                [self addImageView:1];
                [self addImageView:_pageCount-1];
            }
            
            [delegate PhotosEScrollerViewDidClicked:page ContentOffset:sender.contentOffset];
        }
        //        UIImageView *imageView = [imageViewArray objectAtIndex:pageControl.currentPage+1];
        //        if (([[imageUrlArray objectAtIndex:pageControl.currentPage+1] hasPrefix:@"data/"] || [[imageUrlArray objectAtIndex:pageControl.currentPage+1] hasPrefix:@"/data"]) && imageView.image == nil) {
        //            [imageView setImageWithURL:[NSURL URLWithString:[[imageUrlArray objectAtIndex:pageControl.currentPage+1] returnFullImageUrlWithWidth:viewSize.size.width]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        //                [imageViewArray replaceObjectAtIndex:pageControl.currentPage+1 withObject:imageView];
        //            }];
        //        }
        
    }
    else
    {
        if (sender.contentOffset.x < 0) {
            sender.contentOffset = CGPointMake(0, 0);
            sender.bounces = NO;
        }
        CGFloat pageWidth = scrollView.frame.size.width;
        int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        currentPageIndex=page;
        
        if (pageControl.currentPage != page)
        {
            pageControl.currentPage = page ;
            
            // if we are dragging, we want to update the page control directly during the drag
            if (scrollView.dragging)
                [pageControl updateCurrentPageDisplay] ;
        }
        if ([delegate respondsToSelector:@selector(PhotosEScrollerViewDidClicked: ContentOffset:)]) {
            [self addImageView:pageControl.currentPage+2];
            [self addImageView:pageControl.currentPage];
            [delegate PhotosEScrollerViewDidClicked:pageControl.currentPage ContentOffset:sender.contentOffset];
        }
        //        UIImageView *imageView = [imageViewArray objectAtIndex:pageControl.currentPage];
        //        if (([[imageUrlArray objectAtIndex:pageControl.currentPage] hasPrefix:@"data/"] || [[imageUrlArray objectAtIndex:pageControl.currentPage] hasPrefix:@"/data"] ) && imageView.image == nil) {
        //            [imageView setImageWithURL:[NSURL URLWithString:[[imageUrlArray objectAtIndex:pageControl.currentPage] returnFullImageUrlWithWidth:viewSize.size.width]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        //                [imageViewArray replaceObjectAtIndex:pageControl.currentPage withObject:imageView];
        //            }];
        //        }
        
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)_scrollView
{
    _scrollView.bounces = YES;
    if (_isCirculation) {
        if (currentPageIndex==0) {
            
            [_scrollView setContentOffset:CGPointMake(([objectArray count]-2)*viewSize.size.width, 0)];
        }
        if (currentPageIndex==([objectArray count]-1)) {
            
            [_scrollView setContentOffset:CGPointMake(viewSize.size.width, 0)];
            
        }
    }
    else
    {
    }
    
    
}
#pragma mark DDPageControl triggered actions

- (void)pageControlClicked:(id)sender
{
	CustomPageControl *thePageControl = (CustomPageControl *)sender ;
	
	// we need to scroll to the new index
	[scrollView setContentOffset: CGPointMake(scrollView.bounds.size.width * thePageControl.currentPage, scrollView.contentOffset.y) animated: YES] ;
}
- (void)imagePressed:(UITapGestureRecognizer *)sender
{
    if ([delegate respondsToSelector:@selector(PhotosEScrollerViewDidClicked:)]) {
        [delegate PhotosEScrollerViewDidClicked:sender.view.tag];
    }
}

@end
