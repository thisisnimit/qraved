//
//  EScrollerView.m
//  icoiniPad
//
//  Created by Ethan on 12-11-24.
//
//

#import "MenuPhotosEScrollerView.h"
#import "AFNetworking.h"
#import "UIConstants.h"
#import "UIImage+Colorfy.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Helper.h"
#import "UIImage+Resize.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "UIView+Helper.h"
#import "UIImage+GIF.h"
#import "IMGEvent.h"
#import "IMGDish.h"
#define CACHE_NUM 2

@interface MenuPhotosEScrollerView()<UIScrollViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate>{
    int zoomStatus;
    CGAffineTransform preTransform;
    long currentIndex;
    CGRect viewSize;
    NSMutableArray *imagesArray;
    NSMutableDictionary *uiCacheDictionary;
    CGFloat preScale;
    BOOL isNotReturn;
    UIImageView *imgView;
    NSString *imgURL;
}

@end

@implementation MenuPhotosEScrollerView

static NSString * const carouselID = @"MenuPhotosEScrollerView";

-(instancetype)initWithFrame:(CGRect)frame{
    
    UICollectionViewFlowLayout *carouseLayout = [[UICollectionViewFlowLayout alloc]init];
    
    carouseLayout.itemSize = frame.size;
    
    carouseLayout.minimumLineSpacing = 0;
    
    carouseLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    [self registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:carouselID];
    
    self.dataSource = self;
    self.delegate = self;
    
    viewSize = frame;
    MenuPhotosEScrollerView *obj = [super initWithFrame:frame collectionViewLayout:carouseLayout];
    if (obj != nil) {
        [self scrollToItemAtIndexPath:currentIndex];
    }
    return obj;
    
    
    
}

-(instancetype)initWithFrame:(CGRect)frame imagesArray:(NSMutableArray *)tempArray index:(long)photoTag isMenuPhotoView:(BOOL)isMenuPhotoView{
    
    if ((tempArray !=nil) && (tempArray.count == 1)) {
        self.scrollEnabled = NO;
    }else{
        self.scrollEnabled = YES;
    }
    uiCacheDictionary = [[NSMutableDictionary alloc] init];
    self.isMenuPhotoView = isMenuPhotoView;
    currentIndex = photoTag+1;
    imagesArray = [tempArray mutableCopy];
    [imagesArray insertObject:[tempArray objectAtIndex:([tempArray count]-1)] atIndex:0];
    [imagesArray addObject:[tempArray objectAtIndex:0]];
    isNotReturn = NO;
    return [self initWithFrame:frame];
}

-(void)updateImagesArray:(NSMutableArray *)tempArray{
    if ((tempArray !=nil) && (tempArray.count == 1)) {
        self.scrollEnabled = NO;
    }else{
        self.scrollEnabled = YES;
    }
    imagesArray = [tempArray mutableCopy];
    [imagesArray insertObject:[tempArray objectAtIndex:([tempArray count]-1)] atIndex:0];
    [imagesArray addObject:[tempArray objectAtIndex:0]];
    [self reloadData];
    isNotReturn = YES;
    [self scrollViewDidScroll:self];
}

-(void)scrollToItemAtIndexPath:(long)index{
    [self layoutIfNeeded];
    NSIndexPath *indexPath=[NSIndexPath indexPathForItem:index inSection:0];
    [self scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    //    self.bounces = NO;
    self.showsHorizontalScrollIndicator = NO;
    self.pagingEnabled = YES;
}

#pragma mark - UICollectionViewDataSource,UICollectionViewDelegate

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return imagesArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:carouselID forIndexPath:indexPath];
    for (UIView *item in cell.subviews) {
        [item removeFromSuperview];
    }
    UIScrollView *imageView =[self fetchUI:indexPath.row];
    [cell addSubview:imageView];
    
    UITapGestureRecognizer *oneTap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagePressed:)];
    oneTap.numberOfTapsRequired=1;
    oneTap.numberOfTouchesRequired=1;
    [imageView addGestureRecognizer:oneTap];
    
//    UITapGestureRecognizer *twoTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleImagePressed:)];
//    twoTap.numberOfTapsRequired=2;
//    twoTap.numberOfTouchesRequired=1;
//    [imageView addGestureRecognizer:twoTap];
//    [oneTap requireGestureRecognizerToFail:twoTap];
    
//    UIPanGestureRecognizer *panTap = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
//    
//    [self addGestureRecognizer:panTap];
    return cell;
}

//- (void)handlePan:(UIPanGestureRecognizer *)recognizer{
//
//
//    //    CGPoint pt = [recognizer translationInView:photoScrollView];
//    //    recognizer.view.center = CGPointMake(recognizer.view.center.x , recognizer.view.center.y + pt.y);
//    //    //每次移动完，将移动量置为0，否则下次移动会加上这次移动量
//    //    [recognizer setTranslation:CGPointMake(0, 0) inView:photoScrollView];
//    //    if (recognizer.state == UIGestureRecognizerStateEnded) {
//    //        NSLog(@"pan.view == %f", recognizer.view.center.y);
//    if (self.panTapped) {
//        self.panTapped(recognizer);
//    }
//    //    }
//
//
//
//}


-(UIScrollView*)createCacheUI:(long)index{
    
    long rightIndex;
    long leftIndex;
    if (index == 0) {
        rightIndex = imagesArray.count-1;
        leftIndex = 1;
    }else if(index == imagesArray.count-1) {
        rightIndex = imagesArray.count-2;
        leftIndex = 0;
    }else{
        rightIndex = index-1;
        leftIndex = index+1;
    }
    NSEnumerator *enumerator = [uiCacheDictionary keyEnumerator];
    id key;
    NSMutableArray *removeArray=[[NSMutableArray alloc] init];
    while (key = [enumerator nextObject]) {
        if ( ([key intValue]!=rightIndex) &&  ([key intValue]!=index) &&  ([key intValue]!=leftIndex) ) {
            [removeArray addObject:key];
        }
    }
    for (id item in removeArray) {
        [uiCacheDictionary removeObjectForKey:[NSString stringWithFormat:@"%@",item]];
    }
    UIScrollView *rightScrollView = [self fetchUI:rightIndex];
    if (rightScrollView!=nil) {
        [uiCacheDictionary setObject:rightScrollView forKey:[NSString stringWithFormat:@"%ld",rightIndex]];
    }
    UIScrollView *middleScrollView = [self fetchUI:index];
    if (middleScrollView!=nil) {
        [uiCacheDictionary setObject:middleScrollView forKey:[NSString stringWithFormat:@"%ld",index]];
    }
    UIScrollView *leftScrollView = [self fetchUI:leftIndex];
    if (leftScrollView!=nil) {
        [uiCacheDictionary setObject:leftScrollView forKey:[NSString stringWithFormat:@"%ld",leftIndex]];
    }
    return middleScrollView;
}

-(UIScrollView *)fetchUI:(long)index{
    
    NSObject *entity = [imagesArray objectAtIndex:index];
    if ([entity isKindOfClass:[IMGEntity class]]) {
        if (((IMGEntity*)entity).entityId == [NSNumber numberWithInt:-1]) {
            
            UIScrollView *backView = [uiCacheDictionary objectForKey:[NSString stringWithFormat:@"%ld",index]];
            if (backView!=nil) {
                [backView removeFromSuperview];
                [uiCacheDictionary removeObjectForKey:[NSString stringWithFormat:@"%ld",index]];
            }
            return nil;
        }
    }
    
    UIScrollView *backView = [uiCacheDictionary objectForKey:[NSString stringWithFormat:@"%ld",index]];
    if (backView!=nil) {
        return backView;
    }
    
    backView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, viewSize.size.width, viewSize.size.height)];
    [backView setShowsVerticalScrollIndicator:NO];
    [backView setShowsHorizontalScrollIndicator:NO];
    backView.backgroundColor = [UIColor blackColor];
    backView.maximumZoomScale = 3.0;
    backView.minimumZoomScale = 1.0;
    backView.tag = 99;
    backView.delegate = self;
    
    imgView=[[UIImageView alloc]initWithFrame: CGRectMake(0, 0, backView.frame.size.width, backView.frame.size.height)];
    imgView.tag = 100+index;
    imgView.userInteractionEnabled = YES;
    imgView.contentMode = UIViewContentModeScaleAspectFill;
    backView.contentSize = imgView.image.size;
    [backView addSubview:imgView];
    if([entity isKindOfClass:[IMGEvent class]]){
        imgURL=((IMGEvent *)[imagesArray objectAtIndex:index]).appBanner;
    }else{
//        NSString * standardtring = ((IMGDish *)[imagesArray objectAtIndex:index]).standardResolutionImage;
//        if (standardtring.length >0) {
//            imgURL = standardtring;
//        }else{
//         NSString * thumbnailtring = ((IMGDish *)[imagesArray objectAtIndex:index]).thumbnailImage;
//            if (thumbnailtring.length > 0) {
//                imgURL = thumbnailtring;
//            }else{
//            NSString * lowtring = ((IMGDish *)[imagesArray objectAtIndex:index]).lowResolutionImage;
//                if (lowtring.length > 0) {
//                    imgURL = lowtring;
//                }
//            }
//            
//        }
        imgURL=((IMGEntity *)[imagesArray objectAtIndex:index]).imageUrl;
//        if (imgURL.length >0) {
//            
//        }else{
//            
//        }
//        imgURL=((IMGDish *)[imagesArray objectAtIndex:index]).standardResolutionImage;//imageUrl
    }
    if ([imgURL hasPrefix:@"data/"] || [imgURL hasPrefix:@"/data"]) {
        
        NSString *filePath =[NSString stringWithFormat:@"%@/%@.gif",qravedGIFCachePath,[imgURL MD5String]];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        if ([imgURL rangeOfString:@"gif"].location!=NSNotFound&&[fileManager fileExistsAtPath:filePath]) {
            
            int height;
            if (((IMGDish*)imagesArray[index]).JournalGifH==0.0||((IMGDish*)imagesArray[index]).JournalGifW==0.0) {
                height=280;
            }else{
                height=imgView.frame.size.width*((IMGDish*)imagesArray[index]).JournalGifH/((IMGDish*)imagesArray[index]).JournalGifW;
                
            }
            //          imgView.frame=CGRectMake(0, 30, imgView.frame.size.width,height);
            int i=40;
            if (height<(DeviceHeight)/2+30) {
                i=(DeviceHeight)/4;
            }
            
            UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, i, backView.frame.size.width,height)];
            webView.backgroundColor=[UIColor blackColor];
            [webView setScalesPageToFit:YES];
            // webView.contentMode=UIViewContentModeBottom;
            webView.scrollView.bounces = NO;
            webView.userInteractionEnabled = YES;
            webView.scrollView.showsHorizontalScrollIndicator = NO;
            webView.scrollView.showsVerticalScrollIndicator = NO;
            
            // webView.frame=imgView.frame;
            [backView addSubview:webView];
            NSData* gifData = [NSData dataWithContentsOfFile:filePath];
            [webView loadData:gifData MIMEType:@"image/gif" textEncodingName:@"" baseURL:[NSURL URLWithString:@""]];
            
        }else{
            
            
            NSString *url;
            if (self.isUseWebP) {
                url = [NSString stringWithFormat:@"%@%@",[imgURL returnFullWebPImageUrlWithWidth:DeviceWidth-LEFTLEFTSET*2],(self.isMenuPhotoView?@"&waterMask=true":@"")];
            }else{
                url = [NSString stringWithFormat:@"%@%@",[imgURL returnFullImageUrlWithWidth:DeviceWidth-LEFTLEFTSET*2],(self.isMenuPhotoView?@"&waterMask=true":@"")];
            }
            NSLog(@"%lu",(unsigned long)[SDWebImageDownloader sharedDownloader].currentDownloadCount);
            __weak typeof(imgView) weakImageView = imgView;

            NSDate *datenow = [NSDate date];

            [imgView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:nil options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
                double deltaTime = [[NSDate date] timeIntervalSinceDate:datenow];
                NSLog(@"%ld load end time =%f ",index,deltaTime)
                if (image) {
                    weakImageView.image = image;
                    //backView.backgroundColor = [image averageColor];
                    [weakImageView setContentMode:UIViewContentModeScaleAspectFit];
                }
                
            }];
            
            //            [imgView setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] sizeFitWidth:NO];
            
        }
        
        
    }else if ([imgURL hasPrefix:@"http"]){
        __weak typeof(imgView) weakImageView = imgView;

        [imgView sd_setImageWithURL:[NSURL URLWithString:imgURL] placeholderImage:nil options:SDWebImageRetryFailed|SDWebImageAllowInvalidSSLCertificates completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image) {
                //backView.backgroundColor = [image averageColor];
                weakImageView.image = image;
                [weakImageView setContentMode:UIViewContentModeScaleAspectFit];

            }
        }];
    }else{
        NSString *url;
        if (self.isUseWebP) {
            url = [NSString stringWithFormat:@"%@%@",[imgURL returnFullWebPImageUrlWithWidth:DeviceWidth-LEFTLEFTSET*2],(self.isMenuPhotoView?@"&waterMask=true":@"")];
        }else{
            url = [NSString stringWithFormat:@"%@%@",[imgURL returnFullImageUrlWithWidth:DeviceWidth-LEFTLEFTSET*2],(self.isMenuPhotoView?@"&waterMask=true":@"")];
        }
        
        __weak typeof(imgView) weakImageView = imgView;
        
        [imgView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            //backView.backgroundColor = [image averageColor];
            [weakImageView setContentMode:UIViewContentModeScaleAspectFit];
            
        }];
        
        

        
//        [imgView setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] sizeFitWidth:NO];
        
        //        UIImage *img=[UIImage imageNamed:[((IMGEntity *)[imagesArray objectAtIndex:index]).imageUrl returnFullImageUrl]];
        //        [imgView setImage:img];
    }
    
    return backView;
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (sender.tag == 99) {
        return;
    }
    if (self.ifCanNotFromBeginToEnd) {
        CGFloat x =  sender.contentOffset.x;
        if (x < sender.frame.size.width) {
            sender.contentOffset = CGPointMake(sender.frame.size.width, sender.contentOffset.y);
        }
    }
    
    CGFloat pageWidth = sender.frame.size.width;
    int page = floor((sender.contentOffset.x - pageWidth / 2) / pageWidth)+1;
    
    if (page == currentIndex) {
        if (isNotReturn) {
            isNotReturn = NO;
        }else{
            return;
        }
    }
    currentIndex = page;
    
    if (currentIndex == (imagesArray.count-1)) {
        return;
    }
    if (currentIndex == 0) {
        return;
    }
    
    if ([self.escrollerViewDelegate respondsToSelector:@selector(EScrollerViewDidClicked: ContentOffset:)]) {
        [self.escrollerViewDelegate EScrollerViewDidClicked:page ContentOffset:sender.contentOffset];
    }
    
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (currentIndex == (imagesArray.count-1)) {
        NSIndexPath *indexPath=[NSIndexPath indexPathForItem:1 inSection:0];
        [self scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
        return;
    }
    if (currentIndex == 0) {
        NSIndexPath *indexPath=[NSIndexPath indexPathForItem:imagesArray.count-2 inSection:0];
        [self scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionLeft animated:NO];
        return;
    }
}

- (void)imagePressed:(UITapGestureRecognizer *)sender
{
//    if ([self.escrollerViewDelegate respondsToSelector:@selector(EScrollerViewDidClicked:)]) {
//        [self.escrollerViewDelegate EScrollerViewDidClicked:currentIndex];
//    }
    NSInteger index = self.contentOffset.x / DeviceWidth;
//    if ((index+1)< imagesArray.count) {
//        [UIView animateWithDuration:0.5 animations:^{
//            [self setContentOffset:CGPointMake(DeviceWidth*(index+1), 0)];
//        }];
//        
//        [self scrollViewDidScroll:photoScrollView];
//    }
//    
    
    if ((index+1) < imagesArray.count) {
        [self scrollToItemAtIndexPath:index+1];
        
//        if ([self.escrollerViewDelegate respondsToSelector:@selector(EScrollerViewDidClicked: ContentOffset:)]) {
//            [self.escrollerViewDelegate EScrollerViewDidClicked:index+1 ContentOffset:self.contentOffset];
//        }
        
    }
}

- (void)doubleImagePressed:(UITapGestureRecognizer *)sender
{
    UIImageView *currentImageView = [self fetchCurrentImageView];
    UIScrollView *parentUIScrollView = (UIScrollView *)[currentImageView superview];
    if (zoomStatus == 0) {
        zoomStatus = 1;
        preScale = parentUIScrollView.zoomScale;
        [parentUIScrollView setZoomScale:3.0 animated:YES];
    }else{
        zoomStatus = 0;
        [parentUIScrollView setZoomScale:preScale animated:YES];
    }
    if ([self.escrollerViewDelegate respondsToSelector:@selector(EScrollerViewDidDoubleClicked:)]) {
        [self.escrollerViewDelegate EScrollerViewDidDoubleClicked:currentIndex];
    }
}

-(UIImageView *)fetchCurrentImageView{
    NSIndexPath *currentIndexPath=[NSIndexPath indexPathForItem:currentIndex inSection:0];
    UICollectionViewCell *cell = [self cellForItemAtIndexPath:currentIndexPath];
    UIImageView *currentImageView = [cell viewWithTag:(100+currentIndex)];
    return currentImageView;
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)backScrollView{
    if (backScrollView.tag == 99) {
        for (UIView *item in backScrollView.subviews) {
            if ([item isKindOfClass:[UIImageView class]]) {
                return item;
            }
        }
    }
    return nil;
}
- (void)scrollViewDidZoom:(UIScrollView *)backScrollView
{
    if (backScrollView.tag == 99) {
        CGFloat offsetX = (backScrollView.bounds.size.width > backScrollView.contentSize.width)?(backScrollView.bounds.size.width - backScrollView.contentSize.width)/2 : 0.0;
        CGFloat offsetY = (backScrollView.bounds.size.height > backScrollView.contentSize.height)?(backScrollView.bounds.size.height - backScrollView.contentSize.height)/2 : 0.0;
        for (UIView *item in backScrollView.subviews) {
            if ([item isKindOfClass:[UIImageView class]]) {
                item.center = CGPointMake(backScrollView.contentSize.width/2 + offsetX,backScrollView.contentSize.height/2 + offsetY);
            }
        }
    }
}

@end
