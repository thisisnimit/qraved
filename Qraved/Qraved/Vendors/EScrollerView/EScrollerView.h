//
//  EScrollerView.h
//  icoiniPad
//
//  Created by Ethan on 12-11-24.
//
//

#import <UIKit/UIKit.h>
#import "IMGEntity.h"
#import "Label.h"
#define IMAGE_PATH(image_name) [NSString stringWithFormat:@"%@/%@",[[NSBundle mainBundle] bundlePath],image_name]
#import "CustomPageControl.h"
@protocol EScrollerViewDelegate <NSObject>
@optional
-(void)EScrollerViewDidClicked:(NSUInteger)index;
-(void)EScrollerViewDidDoubleClicked:(NSUInteger)index;
-(void)EScrollerViewDidClicked:(NSUInteger)index ContentOffset:(CGPoint)contentoffset;
-(void)EScrollerViewBtnClicked:(NSUInteger)btnTag;
@end

@interface EScrollerView : UIView<UIScrollViewDelegate> {
	CGRect viewSize;
	NSArray *objectArray;

    int currentPageIndex;
    UILabel *noteTitle;
    BOOL _isStartScroll;
    BOOL _isCirculation;
    NSTimer *_scrollTimer;
    NSUInteger _pageCount;
    BOOL _PageControlCenter;
    NSInteger _photoTag;
    BOOL _placeHolder;
    BOOL _event;
    BOOL _sizeFitWidth;
    BOOL _isPhotosDetail;
    BOOL _isOnBoarding;
}

@property (nonatomic) CGFloat pointsPerSecond;
@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,strong) CustomPageControl *pageControl;
@property(nonatomic,assign)BOOL isOnboard;
@property(nonatomic,assign)int currentIndex;


- (void)startScrolling;
- (void)stopScrolling;
@property(nonatomic,assign)id<EScrollerViewDelegate> delegate;


-(id)initWithFrameRect:(CGRect)rect objectArray:(NSArray *)tmpObjectArray isStartScroll:(BOOL)isStartScroll withPhotoTag:(NSInteger)photoTag pageControlCenter:(BOOL)PageControlCenter isCirculation:(BOOL)isCirculation isPageControl:(BOOL)isPageControl isEvent:(BOOL)isEvent;
-(id)initWithFrameRect:(CGRect)rect objectArray:(NSArray *)tmpObjectArray isStartScroll:(BOOL)isStartScroll withPhotoTag:(NSInteger)photoTag pageControlCenter:(BOOL)PageControlCenter isCirculation:(BOOL)isCirculation isPageControl:(BOOL)isPageControl isEvent:(BOOL)isEvent isPlaceHolder:(BOOL)placeHolder;

-(id)initWithFrameRect:(CGRect)rect objectArray:(NSArray *)tmpObjectArray isStartScroll:(BOOL)isStartScroll withPhotoTag:(NSInteger)photoTag pageControlCenter:(BOOL)PageControlCenter isCirculation:(BOOL)isCirculation isPageControl:(BOOL)isPageControl isEvent:(BOOL)isEvent isPlaceHolder:(BOOL)placeHolder sizeFitWidth:(BOOL)sizeFitWidth;
// photoDetail
//-(id)initWithFrameRect:(CGRect)rect objectArray:(NSArray *)tmpObjectArray isStartScroll:(BOOL)isStartScroll withPhotoTag:(NSInteger)photoTag pageControlCenter:(BOOL)PageControlCenter isCirculation:(BOOL)isCirculation isPageControl:(BOOL)isPageControl isEvent:(BOOL)isEvent isPlaceHolder:(BOOL)placeHolder sizeFitWidth:(BOOL)sizeFitWidth isPhotosDetail:(BOOL)isPhotosDetail;

-(id)initWithFrameRect:(CGRect)rect objectArray:(NSArray *)tmpObjectArray isStartScroll:(BOOL)isStartScroll withPhotoTag:(NSInteger)photoTag pageControlCenter:(BOOL)PageControlCenter isCirculation:(BOOL)isCirculation isPageControl:(BOOL)isPageControl isEvent:(BOOL)isEvent isOnBoarding:(BOOL)isOnBoarding;

-(void)setNilAndDealloc;

@end
