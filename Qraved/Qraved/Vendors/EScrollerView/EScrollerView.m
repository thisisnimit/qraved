//
//  EScrollerView.m
//  icoiniPad
//
//  Created by Ethan on 12-11-24.
//
//

#import "EScrollerView.h"
#import "AFNetworking.h"
#import "UIConstants.h"

#import "UIImageView+WebCache.h"
#import "UIImage+Helper.h"
#import "UIImage+Resize.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "UIView+Helper.h"

#import "IMGEvent.h"
//#import "GAIFields.h"
//#import "GAIDictionaryBuilder.h"
//#import "GAITrackedViewController.h"
//#import "GAI.h"
#import <GoogleAnalytics/GAIFields.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import <GoogleAnalytics/GAITrackedViewController.h>
#import <GoogleAnalytics/GAI.h>

@implementation EScrollerView
{
    //    NSMutableArray *imageViewArray;
    //    NSMutableArray *imageUrlArray;
    UIImageView *placeImageView;
    CGFloat endContentOffsetX;
    CGFloat startContentOffsetX;
    CGFloat willEndContentOffsetX;
}
@synthesize delegate;
@synthesize scrollView;
@synthesize pageControl;
-(void)setNilAndDealloc{
    
    
}
//- (void)dealloc {
//    //	NSLog(@"%d",self.retainCount);
//    
//    delegate=nil;
//    
//    //memory leak
////    if (pageControl) {
////        pageControl = nil;
////        [pageControl release];
////    }
////    if (objectArray) {
////        objectArray=nil;
////        [objectArray release];
////    }
//    [pageControl release], pageControl = nil;
//    [objectArray release], objectArray = nil;
//
//    [noteTitle release];
//    [scrollView release];
//    [super dealloc];
//}
-(void) initPageControl{
    /*
     PageControl
     */
    float pageControlWidth;
    float pagecontrolHeight=10.0f;
    
    NSInteger numberOfPages;
    if (_isCirculation) {
        pageControlWidth=(_pageCount-2)*10.0f;
        numberOfPages = _pageCount-2;
    } else{
        pageControlWidth=_pageCount*10.0f;
        numberOfPages = _pageCount;
    }
    
    pageControl=[[CustomPageControl alloc]init];
    
    pageControl.numberOfPages=numberOfPages;
    
    //
    
    [pageControl setNumberOfPages: numberOfPages] ;
    [pageControl setCurrentPage: 0] ;
    [pageControl addTarget: self action: @selector(pageControlClicked:) forControlEvents: UIControlEventValueChanged] ;
    [pageControl setDefersCurrentPageDisplay: YES] ;
    [pageControl setType: DDPageControlTypeOnFullOffFull] ;
    [pageControl setOnColor: [UIColor whiteColor]] ;
    [pageControl setOffColor: [UIColor colorWithRed:153/255.0 green:153/255.0 blue:153/255.0 alpha:0.2]] ;
    [pageControl setIndicatorDiameter: 3.5f] ;
    [pageControl setIndicatorSpace: 10.0f] ;
    //
    
    if (_isStartScroll) {
        if (_PageControlCenter) {
            [pageControl setCenter: CGPointMake(self.center.x, self.bounds.size.height-15.0f)] ;
        } else{
            [pageControl setCenter: CGPointMake(self.center.x+80, self.bounds.size.height-17.0f)] ;
        }
        if (_photoTag) {
            pageControl.currentPage = _photoTag;
        } else{
            pageControl.currentPage = 0;
        }
    }else{
        pagecontrolHeight=20.0f;
        
        [pageControl setFrame:CGRectMake((self.frame.size.width-pageControlWidth),38, pageControlWidth, pagecontrolHeight)];
        
        pageControl.currentPage=0;
        pageControl.numberOfPages=numberOfPages;
        
    }
    [self addSubview:pageControl];
}

-(id)initWithFrameRect:(CGRect)rect objectArray:(NSArray *)tmpObjectArray isStartScroll:(BOOL)isStartScroll withPhotoTag:(NSInteger)photoTag pageControlCenter:(BOOL)PageControlCenter isCirculation:(BOOL)isCirculation isPageControl:(BOOL)isPageControl isEvent:(BOOL)isEvent isPlaceHolder:(BOOL)placeHolder sizeFitWidth:(BOOL)sizeFitWidth
{
    if ((self=[super initWithFrame:rect])) {
        _isStartScroll =isStartScroll;
        _isCirculation = isCirculation;
        _photoTag = photoTag;
        _PageControlCenter = PageControlCenter;
        _placeHolder = placeHolder;
        _event = isEvent;
        _sizeFitWidth=sizeFitWidth;
        self.userInteractionEnabled=YES;
        if (_isCirculation) {
            if(tmpObjectArray!=nil && tmpObjectArray.count>0){
                NSMutableArray *tempArray=[tmpObjectArray mutableCopy];
                [tempArray insertObject:[tmpObjectArray objectAtIndex:([tmpObjectArray count]-1)] atIndex:0];
                [tempArray addObject:[tmpObjectArray objectAtIndex:0]];
                objectArray=[tempArray copy];
//                [tempArray release];
            }
        }
        else
        {
            objectArray = tmpObjectArray;
        }
        
        viewSize=rect;
        _pageCount=[objectArray count];
        scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, viewSize.size.width, viewSize.size.height)];
        scrollView.pagingEnabled = YES;
        
        if (objectArray.count == 1) {
            scrollView.contentSize = CGSizeMake(viewSize.size.width, viewSize.size.height);
        }else{scrollView.contentSize = CGSizeMake(viewSize.size.width * _pageCount, viewSize.size.height);
        }
        
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.showsVerticalScrollIndicator = NO;
        scrollView.scrollsToTop = NO;
        scrollView.delegate = self;
        
        
        
        //        imageUrlArray = [[NSMutableArray alloc]init];
        //        imageViewArray = [[NSMutableArray alloc]init];
        for (int i=0; i<_pageCount; i++) {
            
            NSObject *entity = [objectArray objectAtIndex:i];
            NSString *imgURL;
            if([entity isKindOfClass:[IMGEvent class]]){
                imgURL=((IMGEvent *)[objectArray objectAtIndex:i]).appBanner;
            }else{
                imgURL=((IMGEntity *)[objectArray objectAtIndex:i]).imageUrl;
            }
            
            UIImageView *imgView=[[UIImageView alloc]initWithFrame: CGRectMake(viewSize.size.width*i, 0,viewSize.size.width, viewSize.size.height)];
            
            placeImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, viewSize.size.width, viewSize.size.height)];
            imgView.clipsToBounds = YES;
            if(isPageControl == NO){
                imgView.contentMode = UIViewContentModeTop;
            }else{
                imgView.contentMode = UIViewContentModeScaleAspectFill;
            }

            UIImage *placeHolderImage1 = [UIImage imageWithColor:[UIColor clearColor] size:CGSizeMake(DeviceWidth, 160)];
            
            //            [imageViewArray addObject:imgView];
            //            [imageUrlArray addObject:imgURL];
            
            if ([imgURL hasPrefix:@"data/"] || [imgURL hasPrefix:@"/data"]) {
                //网络图片 请使用ego异步图片库
                //                if (i==0) {
                __weak typeof(imgView) weakImageView = imgView;
                [imgView sd_setImageWithURL:[NSURL URLWithString:[imgURL returnFullImageUrlWithWidth:viewSize.size.width]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    image =[image imageByScalingAndCroppingForSize:CGSizeMake(weakImageView.frame.size.width, weakImageView.frame.size.height)];
                    weakImageView.image = image;
                }];

                
//                [imgView setImageWithURL:[NSURL URLWithString:[imgURL returnFullImageUrlWithWidth:viewSize.size.width]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] sizeFitWidth:sizeFitWidth];
                
                //                }
            }else{
                UIImage *img=[UIImage imageNamed:((IMGEntity *)[objectArray objectAtIndex:i]).imageUrl];
                [imgView setImage:img];
                
                [imgView setContentScaleFactor:[[UIScreen mainScreen] scale]];
                imgView.contentMode =  UIViewContentModeScaleAspectFill;
                imgView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
                imgView.clipsToBounds  = YES;

                
            }
            if (_event) {
                IMGEvent *event = [objectArray objectAtIndex:i];
                CGSize eventDetailExpectSize = [[event.contents removeHTML] sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_LIGHT size:10] constrainedToSize:CGSizeMake(DeviceWidth-106, 160) lineBreakMode:NSLineBreakByWordWrapping];
                Label * eventDeatilLabel = [[Label alloc]initWithFrame:CGRectMake(53, 90, DeviceWidth-106, eventDetailExpectSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_LIGHT size:10] andTextColor:[UIColor whiteColor] andTextLines:0];
                eventDeatilLabel.textAlignment = NSTextAlignmentCenter;
                eventDeatilLabel.text = [event.contents removeHTML];
                [placeImageView addSubview:eventDeatilLabel];
            }
            
            if (_placeHolder) {
                placeImageView.image = [UIImage imageNamed:@"DetailTopBg@2x.png"];
                
            }else{
                placeImageView.image = placeHolderImage1;
            }
            if(!_isOnBoarding){
                [imgView addSubview:placeImageView];
            }
            else
            {
                if (i == 0)
                {
                    [self sendManualScreenName:@"Onboarding 1 page"];
                }
                else if (i == 1)
                {
                    [self sendManualScreenName:@"Onboarding 2 page"];
                }
                else if (i == 2)
                {
                    [self sendManualScreenName:@"Onboarding 3 page"];
                }
                else if (i == 3)
                {
                    [self sendManualScreenName:@"Onboarding 4 page"];
                }
            }
            if (_isOnBoarding && i == _pageCount - 1){
                UIButton *uploadPhtooBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                uploadPhtooBtn.alpha = 0.7f;
                [imgView addSubview:uploadPhtooBtn];
                float uploadPhtooBtnWidth = DeviceWidth*0.8;
                float height = viewSize.size.height;
                float uploadPhtooBtnHeight = height*0.08;
                uploadPhtooBtn.frame = CGRectMake((DeviceWidth - uploadPhtooBtnWidth)/2, DeviceHeight - (height*0.09) - uploadPhtooBtnHeight, uploadPhtooBtnWidth, uploadPhtooBtnHeight);
                uploadPhtooBtn.tag = 100;
                [uploadPhtooBtn addTarget:self action:@selector(onBoardingBtnClick:) forControlEvents:UIControlEventTouchUpInside];
                
                UIButton *skipBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                skipBtn.alpha = 0.7f;
                [imgView addSubview:skipBtn];
                float skipBtnWidth = DeviceWidth*0.08;
                skipBtn.frame = CGRectMake((DeviceWidth - skipBtnWidth)/2, uploadPhtooBtn.endPointY + 24, skipBtnWidth, 20);
                skipBtn.tag = 101;
                [skipBtn addTarget:self action:@selector(onBoardingBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            }
            
            UITapGestureRecognizer *Tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagePressed:)];
            placeImageView.userInteractionEnabled=YES;
            placeImageView.tag = i;
            imgView.tag = i;
            imgView.userInteractionEnabled = YES;
            [placeImageView addGestureRecognizer:Tap];
            [scrollView addSubview:imgView];
//            [imgView release];
        }
        
        if (_isCirculation) {
            [scrollView setContentOffset:CGPointMake(viewSize.size.width*(photoTag+1), 0)];
        }
        else
        {
            [scrollView setContentOffset:CGPointMake(viewSize.size.width*photoTag, 0)];
        }
        
        [self startScrolling];
        
        [self addSubview:scrollView];
        
        if (isPageControl) {
            if (tmpObjectArray.count > 1)
            {
                [self initPageControl];
            }
        }
    }
    
    return self;
}

-(id)initWithFrameRect:(CGRect)rect objectArray:(NSArray *)tmpObjectArray isStartScroll:(BOOL)isStartScroll withPhotoTag:(NSInteger)photoTag pageControlCenter:(BOOL)PageControlCenter isCirculation:(BOOL)isCirculation isPageControl:(BOOL)isPageControl isEvent:(BOOL)isEvent isPlaceHolder:(BOOL)placeHolder
{
    return [self initWithFrameRect:rect objectArray:(NSArray *)tmpObjectArray isStartScroll:isStartScroll withPhotoTag:photoTag pageControlCenter:PageControlCenter isCirculation:isCirculation isPageControl:isPageControl isEvent:isEvent isPlaceHolder:placeHolder sizeFitWidth:YES];
}

-(id)initWithFrameRect:(CGRect)rect objectArray:(NSArray *)tmpObjectArray isStartScroll:(BOOL)isStartScroll withPhotoTag:(NSInteger)photoTag pageControlCenter:(BOOL)PageControlCenter isCirculation:(BOOL)isCirculation isPageControl:(BOOL)isPageControl isEvent:(BOOL)isEvent
{
    return [self initWithFrameRect:rect objectArray:tmpObjectArray isStartScroll:isStartScroll withPhotoTag:photoTag pageControlCenter:PageControlCenter isCirculation:isCirculation isPageControl:isPageControl isEvent:isEvent isPlaceHolder:NO];
}

-(id)initWithFrameRect:(CGRect)rect objectArray:(NSArray *)tmpObjectArray isStartScroll:(BOOL)isStartScroll withPhotoTag:(NSInteger)photoTag pageControlCenter:(BOOL)PageControlCenter isCirculation:(BOOL)isCirculation isPageControl:(BOOL)isPageControl isEvent:(BOOL)isEvent isOnBoarding:(BOOL)isOnBoarding
{
    _isOnBoarding=isOnBoarding;
    return [self initWithFrameRect:rect objectArray:tmpObjectArray isStartScroll:isStartScroll withPhotoTag:photoTag pageControlCenter:PageControlCenter isCirculation:isCirculation isPageControl:isPageControl isEvent:isEvent isPlaceHolder:NO];
}

- (void)willMoveToWindow:(UIWindow *)newWindow
{
    scrollView.pagingEnabled = YES;
    [super willMoveToWindow:newWindow];
    if(newWindow)
    {
        [self startScrolling];
        [scrollView.panGestureRecognizer addTarget:self action:@selector(gestureDidChange:)];
        [scrollView.pinchGestureRecognizer addTarget:self action:@selector(gestureDidChange:)];
    }
    else
    {
        [self stopScrolling];
        [scrollView.panGestureRecognizer removeTarget:self action:@selector(gestureDidChange:)];
        [scrollView.pinchGestureRecognizer removeTarget:self action:@selector(gestureDidChange:)];
    }
}
#pragma mark - Touch methods

- (BOOL)touchesShouldBegin:(NSSet *)touches withEvent:(UIEvent *)event inContentView:(UIView *)view
{
    [self stopScrolling];
    return [scrollView touchesShouldBegin:touches withEvent:event inContentView:view];
}

- (void)gestureDidChange:(UIGestureRecognizer *)gesture
{
    switch (gesture.state)
    {
        case UIGestureRecognizerStateBegan:
        {
            [self stopScrolling];
            
        }
            break;
        case UIGestureRecognizerStateEnded:
        {
            if (_isCirculation) {
                [self startScrolling];
                if (currentPageIndex==([objectArray count]-1)) {
                    
                    [scrollView setContentOffset:CGPointMake(viewSize.size.width, 0)];
                }
            }
        }
        default:
            break;
    }
}

- (void)startScrolling
{
    if(_isStartScroll){
        [self stopScrolling];
        _scrollTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(updateScroll) userInfo:nil repeats:YES];
    }
}

- (void)stopScrolling
{
    if (_isStartScroll) {
        [_scrollTimer invalidate];
        _scrollTimer = nil;
    }
    else
    {
        //        scrollView.scrollEnabled = YES;
    }
    
    
}
- (void)updateScroll
{
    CGFloat pointChange = viewSize.size.width;
    CGPoint newOffset = scrollView.contentOffset;
    pageControl.currentPage=floor((scrollView.contentOffset.x - pointChange / 2) / pointChange) + 1;
    [pageControl updateCurrentPageDisplay];
    
    if (_isCirculation) {
        if (currentPageIndex==([objectArray count]-2)) {
            newOffset.x = pointChange;
            scrollView.contentOffset = newOffset;
        }
        else
        {
            newOffset.x = newOffset.x + pointChange;
            
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:1];
            scrollView.contentOffset = newOffset;
            [UIView commitAnimations];
        }
    }
    else
    {
        
    }
}



- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (_isCirculation) {
        CGFloat pageWidth = scrollView.frame.size.width;
        int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        currentPageIndex=page;
        if (page-1 == objectArray.count-2) {
            page = 1;
        }else if(page==0){
            page=(int)objectArray.count-2;
        }
        //        pageControl.currentPage=(page-1);
        
        if (pageControl.currentPage != page-1)
        {
            pageControl.currentPage = page-1 ;
            // if we are dragging, we want to update the page control directly during the drag
            if (scrollView.dragging)
                [pageControl updateCurrentPageDisplay] ;
        }
        if ([delegate respondsToSelector:@selector(EScrollerViewDidClicked: ContentOffset:)]) {
            [delegate EScrollerViewDidClicked:page ContentOffset:sender.contentOffset];
        }
        //        UIImageView *imageView = [imageViewArray objectAtIndex:pageControl.currentPage+1];
        //        if (([[imageUrlArray objectAtIndex:pageControl.currentPage+1] hasPrefix:@"data/"] || [[imageUrlArray objectAtIndex:pageControl.currentPage+1] hasPrefix:@"/data"]) && imageView.image == nil) {
        //            [imageView setImageWithURL:[NSURL URLWithString:[[imageUrlArray objectAtIndex:pageControl.currentPage+1] returnFullImageUrlWithWidth:viewSize.size.width]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        //                [imageViewArray replaceObjectAtIndex:pageControl.currentPage+1 withObject:imageView];
        //            }];
        //        }
        
    }
    else
    {
        if (sender.contentOffset.x < 0) {
            sender.contentOffset = CGPointMake(0, 0);
            sender.bounces = NO;
        }
        CGFloat pageWidth = scrollView.frame.size.width;
        int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        currentPageIndex=page;
        
        if (pageControl.currentPage != page)
        {
            pageControl.currentPage = page ;
            
            // if we are dragging, we want to update the page control directly during the drag
            if (scrollView.dragging)
                [pageControl updateCurrentPageDisplay] ;
        }
        if ([delegate respondsToSelector:@selector(EScrollerViewDidClicked: ContentOffset:)]) {
            [delegate EScrollerViewDidClicked:pageControl.currentPage ContentOffset:sender.contentOffset];
        }
        //        UIImageView *imageView = [imageViewArray objectAtIndex:pageControl.currentPage];
        //        if (([[imageUrlArray objectAtIndex:pageControl.currentPage] hasPrefix:@"data/"] || [[imageUrlArray objectAtIndex:pageControl.currentPage] hasPrefix:@"/data"] ) && imageView.image == nil) {
        //            [imageView setImageWithURL:[NSURL URLWithString:[[imageUrlArray objectAtIndex:pageControl.currentPage] returnFullImageUrlWithWidth:viewSize.size.width]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        //                [imageViewArray replaceObjectAtIndex:pageControl.currentPage withObject:imageView];
        //            }];
        //        }
        
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)_scrollView
{
    _scrollView.bounces = YES;
    if (_isCirculation) {
        if (currentPageIndex==0) {
            
            [_scrollView setContentOffset:CGPointMake(([objectArray count]-2)*viewSize.size.width, 0)];
        }
        if (currentPageIndex==([objectArray count]-1)) {
            
            [_scrollView setContentOffset:CGPointMake(viewSize.size.width, 0)];
            
        }
    }
    else
    {
        
    }
    endContentOffsetX = _scrollView.contentOffset.x;
    self.currentIndex  = currentPageIndex;
    if (endContentOffsetX < willEndContentOffsetX && willEndContentOffsetX < startContentOffsetX) {        NSLog(@"前一页");
        NSString *deviceId =[[[UIDevice currentDevice] identifierForVendor] UUIDString];
        //SUBACTION_PREVIOUS
        NSNumber *subAction = [NSNumber numberWithInt:1];
        NSNumber *slideIndex = [NSNumber numberWithInt:currentPageIndex+1];
        NSDictionary *parameters = @{@"slideIndex":slideIndex,@"subAction":subAction,@"deviceId":deviceId};
        [[IMGNetWork sharedManager] POST:@"useraction/add/onboarding" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
        }];

    } else if (endContentOffsetX > willEndContentOffsetX && willEndContentOffsetX > startContentOffsetX) {
        NSLog(@"后一页");
        NSString *deviceId =[[[UIDevice currentDevice] identifierForVendor] UUIDString];
        //SUBACTION_NEXT
        NSNumber *subAction = [NSNumber numberWithInt:0];
        NSNumber *slideIndex = [NSNumber numberWithInt:currentPageIndex-1];
        NSDictionary *parameters = @{@"slideIndex":slideIndex,@"subAction":subAction,@"deviceId":deviceId};
        [[IMGNetWork sharedManager] POST:@"useraction/add/onboarding" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
        }];

    }

}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView_{
    startContentOffsetX = scrollView_.contentOffset.x;
    
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView_ withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
    
    willEndContentOffsetX = scrollView.contentOffset.x;
    
}

#pragma mark DDPageControl triggered actions

- (void)pageControlClicked:(id)sender
{
    CustomPageControl *thePageControl = (CustomPageControl *)sender ;
    
    // we need to scroll to the new index
    [scrollView setContentOffset: CGPointMake(scrollView.bounds.size.width * thePageControl.currentPage, scrollView.contentOffset.y) animated: YES] ;
}
- (void)imagePressed:(UITapGestureRecognizer *)sender
{
    if ([delegate respondsToSelector:@selector(EScrollerViewDidClicked:)]) {
        [delegate EScrollerViewDidClicked:sender.view.tag-1];
    }
}

- (void)onBoardingBtnClick:(UIButton *)btn
{
    if ([delegate respondsToSelector:@selector(EScrollerViewBtnClicked:)]) {
        [delegate EScrollerViewBtnClicked:btn.tag];
    }
}

- (void)sendManualScreenName:(NSString*)screenName
{
    id tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName
           value:screenName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

@end
