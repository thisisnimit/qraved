//
//  EScrollerView.h
//  icoiniPad
//
//  Created by Ethan on 12-11-24.
//
//

#import <UIKit/UIKit.h>
#import "IMGEntity.h"
#import "EScrollerView.h"

@interface MenuPhotosEScrollerView : UICollectionView

@property (nonatomic,assign) BOOL ifCanNotFromBeginToEnd;
@property (nonatomic,assign) BOOL isMenuPhotoView;
@property (nonatomic,assign) BOOL isUseWebP;
@property(nonatomic,assign)id<EScrollerViewDelegate> escrollerViewDelegate;

-(instancetype)initWithFrame:(CGRect)frame imagesArray:(NSMutableArray *)array index:(long)photoTag isMenuPhotoView:(BOOL)isMenuPhotoView;
-(void)scrollToItemAtIndexPath:(long)index;
-(void)updateImagesArray:(NSMutableArray *)array;
@property (nonatomic, copy) void(^panTapped)(UIPanGestureRecognizer *panGesture);
@end
