//
//  EScrollerView.h
//  icoiniPad
//
//  Created by Ethan on 12-11-24.
//
//

#import <UIKit/UIKit.h>
#import "IMGEntity.h"
#import "Label.h"
#define IMAGE_PATH(image_name) [NSString stringWithFormat:@"%@/%@",[[NSBundle mainBundle] bundlePath],image_name]
#import "CustomPageControl.h"
@protocol PhotosEScrollerViewDelegate <NSObject>
@optional
-(void)PhotosEScrollerViewDidClicked:(NSUInteger)index;
-(void)PhotosEScrollerViewDidClicked:(NSUInteger)index ContentOffset:(CGPoint)contentoffset;
@end

@interface PhotosEScrollerView : UIView<UIScrollViewDelegate> {
	CGRect viewSize;
	UIScrollView *scrollView;
	NSArray *objectArray;

    CustomPageControl *pageControl;
    NSInteger currentPageIndex;
    UILabel *noteTitle;
    BOOL _isStartScroll;
    BOOL _isCirculation;
    NSTimer *_scrollTimer;
    NSUInteger _pageCount;
    BOOL _PageControlCenter;
    NSInteger _photoTag;
    BOOL _placeHolder;
    BOOL _event;
    BOOL _sizeFitWidth;
}

@property (nonatomic) CGFloat pointsPerSecond;

- (void)startScrolling;
- (void)stopScrolling;
@property(nonatomic,assign)id<PhotosEScrollerViewDelegate> delegate;


//-(id)initWithFrameRect:(CGRect)rect objectArray:(NSArray *)tmpObjectArray isStartScroll:(BOOL)isStartScroll withPhotoTag:(NSInteger)photoTag pageControlCenter:(BOOL)PageControlCenter isCirculation:(BOOL)isCirculation isPageControl:(BOOL)isPageControl isEvent:(BOOL)isEvent;
//-(id)initWithFrameRect:(CGRect)rect objectArray:(NSArray *)tmpObjectArray isStartScroll:(BOOL)isStartScroll withPhotoTag:(NSInteger)photoTag pageControlCenter:(BOOL)PageControlCenter isCirculation:(BOOL)isCirculation isPageControl:(BOOL)isPageControl isEvent:(BOOL)isEvent isPlaceHolder:(BOOL)placeHolder;

-(id)initWithFrameRect:(CGRect)rect objectArray:(NSArray *)tmpObjectArray isStartScroll:(BOOL)isStartScroll withPhotoTag:(NSInteger)photoTag pageControlCenter:(BOOL)PageControlCenter isCirculation:(BOOL)isCirculation isPageControl:(BOOL)isPageControl isEvent:(BOOL)isEvent isPlaceHolder:(BOOL)placeHolder sizeFitWidth:(BOOL)sizeFitWidth;
// photoDetail
@end
