//
//  UIView+Extension_ZLJ.h
//  GLYCAN
//
//  Created by apple on 16/9/21.
//  Copyright © 2016年 DeliveLee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Extension_ZLJ)
//label
+ (UILabel *)zlj_labelWithtitle:(NSString *)ktitle withFont:(CGFloat )kfont withFrameX:(CGFloat)kFrameX withFrameY:(CGFloat)kFrameY withFrameW:(CGFloat)kFrameW withFrameH:(CGFloat)kFrameH withBackGroundColor:(UIColor *)kbackgroundColor  withTextColor:(UIColor *)kTextColor withCornerRadius:(CGFloat )kcornerRadius withMasksToBounds:(BOOL )kMasksToBounds withTextAlignment:(NSTextAlignment ) kTextAlignment withBorderColor:(CGColorRef)kBorderColor withBorderWidth:(CGFloat)kBorderWidth;

//imageview
+ (UIImageView *)zlj_imageViewWithImage:(NSString *)kImage withFrameX:(CGFloat)kFrameX withFrameY:(CGFloat)kFrameY withFrameW:(CGFloat)kFrameW withFrameH:(CGFloat)kFrameH withCenterX :(CGFloat)kCenterX withShadowColor:(UIColor *)shadowColor  withShadowOffset:(CGSize )kshadowOffset  withShadowOpacity:(CGFloat )kShadowOpacity withShadowRadius:(CGFloat )kShadowRadius ;

//button
+ (UIButton *)zlj_buttonWithtitle:(NSString *)ktitle withFont:(CGFloat )kfont withFrameX:(CGFloat)kFrameX withFrameY:(CGFloat)kFrameY withFrameW:(CGFloat)kFrameW withFrameH:(CGFloat)kFrameH withTextColor:(UIColor *)kTextColor withBackGroundColor:(UIColor *)kbackgroundColor  withCornerRadius:(CGFloat )kcornerRadius withMasksToBounds:(BOOL )kMasksToBounds withBorderColor:(CGColorRef )KBorderColor withBorderWidth:(CGFloat)kBorderWidth withBackgroundImage:(NSString *)kBackgroundImage withImage:(NSString *)kImage WithTarget:(id)target Action:(SEL)action;

//宽度自适应(label)
+ (CGFloat)zlj_getHeightByWidth:(CGFloat)width title:(NSString *)title font:(UIFont*)font;

//高度自适应(label)
+ (CGFloat)zlj_getWidthWithTitle:(NSString *)title font:(UIFont *)font;

@end
