//
//  RollView.m
//  testScrollView
//
//  Created by apple on 17/2/20.
//  Copyright © 2017年 apple. All rights reserved.
//

#import "RollView.h"
#import "RestaurantMapCell.h"
#import "IMGRestaurant.h"
#import "SavedMapListView.h"

@interface RollView ()<UIScrollViewDelegate>


@property (nonatomic, strong) NSArray *rollDataArr;   // 图片数据

@property (nonatomic, assign) float halfGap;   // 图片间距的一半

@property (nonatomic, assign) CGFloat listWidth;

@end
@implementation RollView

- (instancetype)initWithFrame:(CGRect)frame withDistanceForScroll:(float)distance withGap:(float)gap
{
    
    self = [super initWithFrame:frame];
    if (self) {
        
        self.halfGap = gap / 2;
        
        /** 设置 UIScrollView */
        self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(distance, 0, self.frame.size.width - 2 * distance, self.frame.size.height)];
        self.scrollView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.scrollView];
        
        self.scrollView.pagingEnabled = YES;
        self.scrollView.delegate = self;
        
        self.scrollView.clipsToBounds = NO;
      
        
        /** 添加手势 */
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapAction:)];
        tap.numberOfTapsRequired = 1;
        tap.numberOfTouchesRequired = 1;
        [self.scrollView addGestureRecognizer:tap];
        self.scrollView.showsHorizontalScrollIndicator = NO;
        
        /** 数据初始化 */
        self.rollDataArr = [NSArray array];
        
    }
    
    
    return self;
}

#pragma mark - 视图数据
-(void)rollView:(NSArray *)dataArr{
    
    self.rollDataArr = dataArr;
    
    //循环创建添加轮播图片, 前后各添加一张
    for (int i = 0; i < self.rollDataArr.count; i++) {
        
        IMGRestaurant *restaurant = _rollDataArr[i];
        if (self.isfromSaved) {
            CGFloat listWidth = self.scrollView.frame.size.width-10;
            SavedMapListView *savedMapListView = [[SavedMapListView alloc] initWithFrame:CGRectMake(30+(10+listWidth)*i , 0, listWidth, self.frame.size.height)];
            savedMapListView.restaurant = restaurant;
            __weak typeof(savedMapListView) weakSavedMapListView = savedMapListView;
            savedMapListView.buttonSavedClick = ^(IMGRestaurant *pressRestaurant){
                if (self.delegate && [self.delegate respondsToSelector:@selector(savedClick: andSavedMapView:)]) {
                    [self.delegate savedClick:pressRestaurant andSavedMapView:weakSavedMapListView];
                }
            };
            savedMapListView.backgroundColor = [UIColor whiteColor];
            
            [self.scrollView addSubview:savedMapListView];
        }else{
            RestaurantMapCell *picImageView = [[RestaurantMapCell alloc] initWithFrame:CGRectZero];
            picImageView.userInteractionEnabled = YES;
            [picImageView setClipsToBounds:YES];
            [picImageView.layer setBorderWidth:0.5];
            [picImageView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
            [picImageView.layer setCornerRadius:5.0];
            
            picImageView.frame = CGRectMake((2 * i + 1) * self.halfGap + i * (self.scrollView.frame.size.width - 2 * self.halfGap), 0, (self.scrollView.frame.size.width - 2 * self.halfGap), self.frame.size.height);
            picImageView.backgroundColor = [UIColor whiteColor];
            
            [picImageView setRestaurant:restaurant andNumber:i];
            [self.scrollView addSubview:picImageView];
        }
        
        /**  说明
         *   1. 设置完 ScrollView的width, 那么分页的宽也为 width.
         *   2. 图片宽为a 间距为 gap, 那么 图片应该在ScrollView上居中, 距离ScrollView左右间距为halfGap.
         *   与 ScrollView的width关系为 width = halfGap + a + halfGap.
         *   3. distance : Scroll距离 底层视图View两侧距离.
         *   假设 要露出上下页内容大小为 m ,   distance = m + halfGap
         *
         *  图片位置对应关系 :
         *  0 ->  2 * halfGap ;
         *  1 ->  3 * halfGap + a ;
         *  2 ->  5 * halfGap + 2 * a ;
         .
         .
         *  i   -> (2 * i +1) *  halfGap + 2 *(width - 2 * halfGap )
         */
    //设置轮播图当前的显示区域
//    self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width, 0);

    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * (self.rollDataArr.count ), 0);
    
    }

}

#pragma mark - UIScrollViewDelegate 方法
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    NSInteger curIndex = scrollView.contentOffset.x  / self.scrollView.frame.size.width;
    if ([self.rollDataArr isKindOfClass:[NSArray class]] && (self.rollDataArr.count > 0)) {
        [_delegate didScrollToIndexpath:curIndex];
    }
//
//    if (curIndex == self.rollDataArr.count + 1) {
//        
//        scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width, 0);
//    }else if (curIndex == 0){
//        
//        scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width * self.rollDataArr.count, 0);
//    }
    
}

#pragma mark - 轻拍手势的方法
-(void)tapAction:(UITapGestureRecognizer *)tap{
    
    if ([self.rollDataArr isKindOfClass:[NSArray class]] && (self.rollDataArr.count > 0)) {
        
        [_delegate didSelectPicWithIndexPath:(self.scrollView.contentOffset.x / self.scrollView.frame.size.width)];
    }else{
        
        [_delegate didSelectPicWithIndexPath:-1];
    }
    
}


@end

