


#import <UIKit/UIKit.h>
#import "SavedMapListView.h"

@protocol RollViewDelegate <NSObject>

-(void)didSelectPicWithIndexPath:(NSInteger)index;
-(void)didScrollToIndexpath:(NSInteger)index;
-(void)savedClick:(IMGRestaurant *)restaurant andSavedMapView:(SavedMapListView *)savedMapView;
@end


@interface RollView : UIView

@property (nonatomic, assign) id<RollViewDelegate> delegate;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, assign) BOOL isfromSaved;


- (instancetype)initWithFrame:(CGRect)frame withDistanceForScroll:(float)distance withGap:(float)gap;


-(void)rollView:(NSArray *)dataArr;

@end
