//
//  UIView+Extension_ZLJ.m
//  GLYCAN
//
//  Created by apple on 16/9/21.
//  Copyright © 2016年 DeliveLee. All rights reserved.
//

#import "UIView+Extension_ZLJ.h"

@implementation UIView (Extension_ZLJ)
+ (UILabel *)zlj_labelWithtitle:(NSString *)ktitle withFont:(CGFloat )kfont withFrameX:(CGFloat)kFrameX withFrameY:(CGFloat)kFrameY withFrameW:(CGFloat)kFrameW withFrameH:(CGFloat)kFrameH withBackGroundColor:(UIColor *)kbackgroundColor  withTextColor:(UIColor *)kTextColor withCornerRadius:(CGFloat )kcornerRadius withMasksToBounds:(BOOL )kMasksToBounds withTextAlignment:(NSTextAlignment ) kTextAlignment withBorderColor:(CGColorRef)kBorderColor withBorderWidth:(CGFloat)kBorderWidth{
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(kFrameX, kFrameY, kFrameW, kFrameH);
    label.text =  ktitle;
    label.font = [UIFont systemFontOfSize:kfont];
    label.backgroundColor = kbackgroundColor;
    label.textColor = kTextColor;
    label.layer.cornerRadius = kcornerRadius;
    label.layer.masksToBounds = kMasksToBounds;
    label.textAlignment = kTextAlignment;
    label.layer.borderColor = kBorderColor;
    label.layer.borderWidth = kBorderWidth;
    return label;
    
}
+ (UIImageView *)zlj_imageViewWithImage:(NSString *)kImage withFrameX:(CGFloat)kFrameX withFrameY:(CGFloat)kFrameY withFrameW:(CGFloat)kFrameW withFrameH:(CGFloat)kFrameH withCenterX :(CGFloat)kCenterX withShadowColor:(UIColor *)shadowColor  withShadowOffset:(CGSize )kshadowOffset  withShadowOpacity:(CGFloat )kShadowOpacity withShadowRadius:(CGFloat )kShadowRadius {

    UIImageView *imageV = [UIImageView new];
    imageV.frame = CGRectMake(kFrameX, kFrameY, kFrameW, kFrameH);
    imageV.centerX = kCenterX;
    imageV.image = [UIImage imageNamed:kImage];
    imageV.layer.shadowColor = shadowColor.CGColor;//阴影颜色
    imageV.layer.shadowOffset = kshadowOffset;//偏移距离
    imageV.layer.shadowOpacity = kShadowOpacity;//不透明度
    imageV.layer.shadowRadius = kShadowRadius;//半径
    return imageV;

}
+ (UIButton *)zlj_buttonWithtitle:(NSString *)ktitle withFont:(CGFloat )kfont withFrameX:(CGFloat)kFrameX withFrameY:(CGFloat)kFrameY withFrameW:(CGFloat)kFrameW withFrameH:(CGFloat)kFrameH withTextColor:(UIColor *)kTextColor withBackGroundColor:(UIColor *)kbackgroundColor  withCornerRadius:(CGFloat )kcornerRadius withMasksToBounds:(BOOL )kMasksToBounds withBorderColor:(CGColorRef )KBorderColor withBorderWidth:(CGFloat)kBorderWidth withBackgroundImage:(NSString *)kBackgroundImage withImage:(NSString *)kImage WithTarget:(id)target Action:(SEL)action{

    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(kFrameX, kFrameY, kFrameW, kFrameH);
    [button setTitle:ktitle forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:kfont];
    [button setTitleColor:kTextColor forState:UIControlStateNormal];
    button.backgroundColor = kbackgroundColor;
//    [button setBackgroundColor:kbackgroundColor forState:UIControlStateNormal];
    button.layer.cornerRadius = kcornerRadius;
    button.layer.masksToBounds = kMasksToBounds;
    button.layer.borderColor = KBorderColor;
    button.layer.borderWidth = kBorderWidth;
    [button setBackgroundImage:[UIImage imageNamed:kBackgroundImage] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:kImage] forState:UIControlStateNormal];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];

    return button;
}



+ (CGFloat)zlj_getHeightByWidth:(CGFloat)width title:(NSString *)title font:(UIFont *)font
{
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 0)];
    label.text = title;
    label.font = font;
    label.numberOfLines = 0;
    [label sizeToFit];
    CGFloat height = label.frame.size.height;
    return height;
}

+ (CGFloat)zlj_getWidthWithTitle:(NSString *)title font:(UIFont *)font {
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 1000, 0)];
    label.text = title;
    label.font = font;
    [label sizeToFit];
    return label.frame.size.width;
}


@end
