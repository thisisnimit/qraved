//
//  UIImage+imageWithName.h
//  Qraved
//
//  Created by Olaf on 14-9-2.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (imageWithName)
+ (UIImage *)imageWithNamed:(NSString *)name;
@end
