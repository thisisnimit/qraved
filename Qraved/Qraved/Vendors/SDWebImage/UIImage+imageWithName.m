//
//  UIImage+imageWithName.m
//  Qraved
//
//  Created by Olaf on 14-9-2.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "UIImage+imageWithName.h"
#define IMAGE_PATH(image_name) [NSString stringWithFormat:@"%@/%@",[[NSBundle mainBundle] bundlePath],image_name]
@implementation UIImage (imageWithName)

+ (UIImage *)imageWithNamed:(NSString *)name{
    
    __autoreleasing  UIImage * image=[[UIImage alloc]initWithContentsOfFile:IMAGE_PATH(name)];
    return image;
}

@end
