//
//  BookUtil.m
//  Qraved
//
//  Created by Jeff on 10/15/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "BookUtil.h"
@implementation BookUtil

+(NSDate *)dateFromString:(NSString *)dateString withFormat:(NSString *)format{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:format];
    return [dateFormatter dateFromString:dateString];
}

+(NSString*)dateStringWithFormat:(NSString *)format forDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:format];
    return [dateFormatter stringFromDate:date];
}


+(NSString*)todayStringWithFormat:(NSString *)format{
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:format];
    return [dateFormatter stringFromDate:today];
}

+(NSArray *)around30minutes:(NSString *)time{
    NSArray *minuteArray;
    int intTime=[BookUtil timeStringToInt:time];
    int time1=0;
    int time2=12*60*60*1000;
    if(intTime!=0){
        time1=intTime-30*60*1000;
    }
    if(intTime!=24*60*60*1000){
        time2=intTime+30*60*1000;
    }
    minuteArray=[[NSArray alloc]initWithObjects:[BookUtil timeIntToString:time1],[BookUtil timeIntToString:time2], nil];
    return minuteArray;
}

+(int)timeStringToInt:(NSString *)time{
    NSArray *timeStringArray = [time componentsSeparatedByString:@":"];
    if(timeStringArray!=nil && [timeStringArray count]==2){
        return ([[timeStringArray objectAtIndex:0] intValue]*60*60+[[timeStringArray objectAtIndex:1] intValue]*60)*1000;
    }
    return 0;
}

+(NSString *)minBookTimeString:(NSNumber *)minimumAvailableBooking{
    NSDate *now=[NSDate date];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSString *nowTime=[dateFormatter stringFromDate:now];
    int nowTimeIntValue=[BookUtil timeStringToInt:nowTime];
    return [BookUtil timeIntToString:(nowTimeIntValue+[minimumAvailableBooking intValue]*60*1000)];
}

+(NSString *)timeIntToString:(int)time{
    int hour = (time/(1000*60*60));
    int minute = (time-hour*1000*60*60)/(1000*60);
    NSString *hourString = [NSString stringWithFormat:@"%d",hour];
    NSString *minuteString = [NSString stringWithFormat:@"%d",minute];
    return [NSString stringWithFormat:@"%@:%@",(hour<=9?[@"0" stringByAppendingString:hourString]:hourString),(minute<=9?[@"0" stringByAppendingString:minuteString]:minuteString )];
    
}

+(NSString *)currentTimeAt:(NSDate *)date{
    NSString *bookTime;
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"HH"];
    NSString *bookHour = [formatter stringFromDate:date];
    [formatter setDateFormat:@"mm"];
    NSString *bookMinute = [formatter stringFromDate:date];
    int intBookHour = [bookHour intValue];
    int intBookMinute = [bookMinute intValue];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    if(intBookHour==0 && intBookMinute==00){
        intBookMinute = 1;
    }
    if(intBookHour==23 && intBookMinute>=30){
        bookTime =@"00:00" ;
    }else if(intBookHour==23 && intBookMinute<30){
        bookTime =@"23:30";
    }else{
        if(intBookMinute>=30){
            bookTime = [NSString stringWithFormat:@"%.2d:00",intBookHour+1];
        }else{
            bookTime = [NSString stringWithFormat:@"%.2d:30",intBookHour];
        }
    }
    
    return bookTime;
}

+(BOOL)timeAvailableAt:(NSDate *)date andTime:(NSString *)time withPax:(NSNumber *)pax  forRestaurantOffer:(IMGRestaurantOffer *)tmpRestaurantOffer{
    int timeValue = [BookUtil timeStringToInt:time];
    
    NSCalendar *calendar=[NSCalendar currentCalendar];
    NSDateComponents*comps1=[calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth |NSCalendarUnitDay | NSCalendarUnitWeekday) fromDate:date];
    NSInteger currentWeekDay = [comps1 weekday];
    currentWeekDay=currentWeekDay-1;
    
    NSString *todayString = [BookUtil todayStringWithFormat:@"yyyy-MM-dd HH:mm"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSArray *slotStringArray = [tmpRestaurantOffer.offerSlotList componentsSeparatedByString:@","];
    //    concat_ws('|', IFNULL(b.book_time,0), IFNULL(b.off,0), IFNULL(b.seat,0),IFNULL(b.weekDay,0),IFNULL(o.booked,0),IFNULL(s.booked,0))
    if(slotStringArray!=nil){
        for(NSString *slotString in slotStringArray){
            NSArray *slotInfoArray = [slotString componentsSeparatedByString:@"|"];
            NSString *offerSlotAvailString = [slotInfoArray objectAtIndex:2];
            NSString *bookTimeString = [slotInfoArray objectAtIndex:0];
            int offerSlotAvail = [offerSlotAvailString intValue];
            NSNumber *weekDay = [slotInfoArray objectAtIndex:3];
            
            NSString *formatedBookTimeString = [BookUtil timeIntToString:[bookTimeString intValue]];
            NSString *formatedBookDateString = [[dateFormatter stringFromDate:date] stringByAppendingString:[NSString stringWithFormat:@" %@",formatedBookTimeString]];
            if([todayString compare:formatedBookDateString]==NSOrderedAscending){
                if([weekDay intValue]==currentWeekDay && timeValue==[ bookTimeString intValue] && offerSlotAvail>=[pax intValue]){
                    return YES;
                }
            }
        }
    }
    return NO;
}

+(NSArray *)timeSlotAround:(NSDate *)date andTime:(NSString *)time{
    NSMutableArray *bookDate = [[NSMutableArray alloc]initWithCapacity:0];
    NSMutableArray *bookTime = [[NSMutableArray alloc]initWithCapacity:0];
    
    NSDate *nextDate=[NSDate dateWithTimeIntervalSinceNow:(24*60*60)];
    
    NSArray *hourAndMinute = [time componentsSeparatedByString:@":"];
    int intBookHour = [[hourAndMinute objectAtIndex:0] intValue];
    int intBookMinute = [[hourAndMinute objectAtIndex:1] intValue];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
//    if(intBookHour==0 && intBookMinute==00){
//        intBookMinute = 1;
//    }
    if(intBookHour==23 && intBookMinute==30){
        [bookDate addObject:[formatter stringFromDate:date]];
        [bookDate addObject:[formatter stringFromDate:date]];
        [bookDate addObject:[formatter stringFromDate:nextDate]];
        [bookTime addObject:@"23:00"];
        [bookTime addObject:@"23:30"];
        [bookTime addObject:@"00:00"];
    }else if(intBookHour==0 && intBookMinute<0){
        [bookDate addObject:[formatter stringFromDate:[NSDate dateWithTimeIntervalSinceNow:-1*(24*60*60)]]];
        [bookDate addObject:[formatter stringFromDate:date]];
        [bookDate addObject:[formatter stringFromDate:date]];
        [bookTime addObject:@"23:30"];
        [bookTime addObject:@"00:00"];
        [bookTime addObject:@"00:30"];
    }else{
        [bookDate addObject:[formatter stringFromDate:date]];
        [bookDate addObject:[formatter stringFromDate:date]];
        [bookDate addObject:[formatter stringFromDate:date]];
        if(intBookMinute==30){
            [bookTime addObject:[NSString stringWithFormat:@"%.2d:00",intBookHour]];
            [bookTime addObject:[NSString stringWithFormat:@"%.2d:30",intBookHour]];
            [bookTime addObject:[NSString stringWithFormat:@"%.2d:00",intBookHour+1]];
        }else{
            [bookTime addObject:[NSString stringWithFormat:@"%.2d:30",intBookHour-1]];
            [bookTime addObject:[NSString stringWithFormat:@"%.2d:00",intBookHour]];
            [bookTime addObject:[NSString stringWithFormat:@"%.2d:30",intBookHour]];
        }
    }
    
    NSArray *timeSlotArray = [[NSArray alloc] initWithObjects:bookDate,bookTime, nil];
    return timeSlotArray;
}



+(NSArray *)availableTimesFromRegular:(NSArray *)regularTimeArray withPax:(NSNumber *)pax withAllTimeArray:(NSArray *)allTimeArray{
    NSMutableArray *availableRegularTimesArray=[[NSMutableArray alloc]initWithCapacity:0];
    if(regularTimeArray!=nil){
        for(NSDictionary *slotDictionary in regularTimeArray){
            NSString *slotBookTimeString = [slotDictionary objectForKey:@"bookTime"];
            NSNumber *avail = [slotDictionary objectForKey:@"avail"];
            if([avail intValue]>=[pax intValue]){
                if([allTimeArray indexOfObject:slotBookTimeString]==NSNotFound){
                    [availableRegularTimesArray addObject:slotBookTimeString];
                }
            }
        }
    }
    return availableRegularTimesArray;
}

+(NSArray *)availableTimesFromOffer:(IMGRestaurantOffer *)restaurantOffer inOfferArray:(NSArray *)restaurantOfferArray withPax:(NSNumber *)pax{
    NSMutableArray *availableTimesArray=[[NSMutableArray alloc]initWithCapacity:0];
    IMGRestaurantOffer *tmpRestaurantOffer;
    if(restaurantOffer!=nil && restaurantOfferArray!=nil && restaurantOfferArray.count>0){
        for(IMGRestaurantOffer *offer in restaurantOfferArray){
            if([offer.offerId intValue]==[restaurantOffer.offerId intValue]){
                tmpRestaurantOffer = offer;
                break;
            }
        }
    }
    if(tmpRestaurantOffer!=nil){
        NSArray *slotStringArray = [tmpRestaurantOffer.offerSlotList componentsSeparatedByString:@","];
        //    concat_ws('|', IFNULL(b.book_time,0), IFNULL(b.off,0), IFNULL(b.seat,0),IFNULL(b.weekDay,0),IFNULL(o.booked,0),IFNULL(s.booked,0))
        if(slotStringArray!=nil){
            for(NSString *slotString in slotStringArray){
                NSArray *slotInfoArray = [slotString componentsSeparatedByString:@"|"];
                NSString *offerSlotAvailString = [slotInfoArray objectAtIndex:2];
                NSString *bookTimeString = [slotInfoArray objectAtIndex:0];
                NSString *offerBookedString = [slotInfoArray objectAtIndex:4];
                int booked = [offerBookedString intValue];
                NSString *bookTime = [BookUtil timeIntToString:[ bookTimeString intValue]];
                int offerSlotAvail = [offerSlotAvailString intValue];
                if(offerSlotAvail-booked>=[pax intValue]){
                    if([availableTimesArray indexOfObject:bookTime]==NSNotFound){
                        [availableTimesArray addObject:bookTime];
                    }
                }
            }
        }
    }
    return availableTimesArray;
}

+(NSArray *)availableTimesNotFromOffer:(IMGRestaurantOffer *)restaurantOffer inOfferArray:(NSArray *)restaurantOfferArray withPax:(NSNumber *)pax withAvailableTimeArray:(NSArray *)availableTimeArray{
    NSMutableArray *otherOfferAvailableTimeArray=[[NSMutableArray alloc]initWithCapacity:0];
    if(restaurantOfferArray!=nil){
        for(IMGRestaurantOffer *tmpRestaurantOffer in restaurantOfferArray){
            if(restaurantOffer==nil || [tmpRestaurantOffer.offerId intValue]!=[restaurantOffer.offerId intValue]){
                NSArray *slotStringArray = [tmpRestaurantOffer.offerSlotList componentsSeparatedByString:@","];
                //    concat_ws('|', IFNULL(b.book_time,0), IFNULL(b.off,0), IFNULL(b.seat,0),IFNULL(b.weekDay,0),IFNULL(o.booked,0),IFNULL(s.booked,0))
                if(slotStringArray!=nil){
                    for(NSString *slotString in slotStringArray){
                        NSArray *slotInfoArray = [slotString componentsSeparatedByString:@"|"];
                        NSString *offerSlotAvailString = [slotInfoArray objectAtIndex:2];
                        NSString *offerBookedString = [slotInfoArray objectAtIndex:4];
                        int booked = [offerBookedString intValue];
                        NSString *bookTimeString = [slotInfoArray objectAtIndex:0];
                        NSString *bookTime = [BookUtil timeIntToString:[bookTimeString intValue]];
                        int offerSlotAvail = [offerSlotAvailString intValue];
                        if(offerSlotAvail-booked>=[pax intValue]){
                            if([availableTimeArray indexOfObject:bookTime]==NSNotFound){
                                [otherOfferAvailableTimeArray addObject:bookTime];
                            }
                        }
                    }
                }
            }
        }
    }
    
    return otherOfferAvailableTimeArray;
}


+(NSArray *)availableWeekDaysFromOffer:(IMGRestaurantOffer *)restaurantOffer withPax:(NSNumber *)pax{
    NSMutableArray *availableWeekDayArray=[[NSMutableArray alloc]initWithCapacity:0];
    if(restaurantOffer!=nil){
        NSArray *slotStringArray = [restaurantOffer.offerSlotList componentsSeparatedByString:@","];
        //    concat_ws('|', IFNULL(b.book_time,0), IFNULL(b.off,0), IFNULL(b.seat,0),IFNULL(b.weekDay,0),IFNULL(o.booked,0),IFNULL(s.booked,0))
        if(slotStringArray!=nil){
            for(NSString *slotString in slotStringArray){
                NSArray *slotInfoArray = [slotString componentsSeparatedByString:@"|"];
                if(slotInfoArray!=nil&&slotInfoArray.count>3){
                    NSString *offerSlotAvailString = [slotInfoArray objectAtIndex:2];
                    NSString *weekDayString = [slotInfoArray objectAtIndex:3];
                    int offerSlotAvail = [offerSlotAvailString intValue];
                    if(offerSlotAvail>=[pax intValue]){
                        if([availableWeekDayArray indexOfObject:weekDayString]==NSNotFound){
                            [availableWeekDayArray addObject:weekDayString];
                        }
                    }
                }
            }
        }
    }
    return availableWeekDayArray;
}

+(BOOL)bookable:(NSNumber *)pax andSlotTimeBookedArray:(NSArray *)tmpSlotTimeBookedArray atDate:(NSDate *)date restaurant:(IMGRestaurant *)restaurant{
    int sumBooked=[BookUtil sumBookedFromSlotTimeBookedArray:tmpSlotTimeBookedArray atDate:date];
    if([restaurant.dailyBookingLimit intValue]!=0&&sumBooked+[pax intValue]>=[restaurant.dailyBookingLimit intValue]){
        return FALSE;
    }
    return TRUE;
}
+(BOOL)bookableFromRegular:(NSArray *)regularTimeArray withPax:(NSNumber *)pax  andSlotTimeBookedArray:(NSArray *)tmpSlotTimeBookedArray  atDate:(NSDate *)date restaurant:(IMGRestaurant *)restaurant{
    
    if([BookUtil bookable:pax andSlotTimeBookedArray:tmpSlotTimeBookedArray atDate:date restaurant:restaurant]==FALSE){
        return FALSE;
    }
    
    NSCalendar *calendar=[NSCalendar currentCalendar];
    NSDateComponents*comps1=[calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth |NSCalendarUnitDay | NSCalendarUnitWeekday) fromDate:date];
    NSInteger currentYear = [comps1 year];
    NSInteger currentMounth = [comps1 month];
    NSInteger currentDay = [comps1 day];
    NSString *currentMounthString =[NSString stringWithFormat:@"%zd",currentMounth];
    NSString *currentDayString =[NSString stringWithFormat:@"%zd",currentDay];

    if (currentMounthString.length==1) {
        currentMounthString = [NSString stringWithFormat:@"0%@",currentMounthString];
    }
    if (currentDayString.length==1) {
        currentDayString = [NSString stringWithFormat:@"0%@",currentDayString];
    }

    NSString *dateString = [NSString stringWithFormat:@"%zd/%@/%@",currentYear,currentMounthString,currentDayString];
    
    if(regularTimeArray!=nil){
        for (int i = 0; i<regularTimeArray.count; i++) {
            NSDictionary *weekDayRegularTimeDic=[regularTimeArray objectAtIndex:i];

            NSString *bookDate = [weekDayRegularTimeDic objectForKey:@"bookDate"];
            if ([bookDate isEqualToString:dateString]){
                return [[weekDayRegularTimeDic objectForKey:@"bookable"] boolValue];
            }
//            NSDictionary *weekDayRegularTimeDic=[regularTimeArray objectAtIndex:i];
//            if(![weekDayRegularTimeDic isKindOfClass:[NSNull class]]){
//                NSString *bookDate = [weekDayRegularTimeDic objectForKey:@"bookDate"];
//                    if ([bookDate isEqualToString:dateString]) {
//                        NSArray *timesArry = [weekDayRegularTimeDic objectForKey:@"times"];
//                        if (timesArry.count>0) {
//                            return YES;
//                        }
//                    }
//                }
//            }
        }
        }
    
    return FALSE;
}
+(BOOL)bookableFromOffer:(NSArray *)regularTimeArray withPax:(NSNumber *)pax  andSlotTimeBookedArray:(NSArray *)tmpSlotTimeBookedArray  atDate:(NSDate *)date restaurant:(IMGRestaurant *)restaurant{
    
    if([BookUtil bookable:pax andSlotTimeBookedArray:tmpSlotTimeBookedArray atDate:date restaurant:restaurant]==FALSE){
        return FALSE;
    }
    
    NSCalendar *calendar=[NSCalendar currentCalendar];
    NSDateComponents*comps1=[calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth |NSCalendarUnitDay | NSCalendarUnitWeekday) fromDate:date];
    NSInteger currentYear = [comps1 year];
    NSInteger currentMounth = [comps1 month];
    NSInteger currentDay = [comps1 day];
    NSString *currentMounthString =[NSString stringWithFormat:@"%zd",currentMounth];
    NSString *currentDayString =[NSString stringWithFormat:@"%zd",currentDay];
    
    if (currentMounthString.length==1) {
        currentMounthString = [NSString stringWithFormat:@"0%@",currentMounthString];
    }
    if (currentDayString.length==1) {
        currentDayString = [NSString stringWithFormat:@"0%@",currentDayString];
    }
    
    NSString *dateString = [NSString stringWithFormat:@"%zd/%@/%@",currentYear,currentMounthString,currentDayString];
    
    if(regularTimeArray!=nil){
        for (int i = 0; i<regularTimeArray.count; i++) {
            NSDictionary *weekDayRegularTimeDic=[regularTimeArray objectAtIndex:i];
            
            NSString *bookDate = [weekDayRegularTimeDic objectForKey:@"bookDate"];
            if ([bookDate isEqualToString:dateString]){
                return [[weekDayRegularTimeDic objectForKey:@"bookable"] boolValue];
            }
            //            NSDictionary *weekDayRegularTimeDic=[regularTimeArray objectAtIndex:i];
            //            if(![weekDayRegularTimeDic isKindOfClass:[NSNull class]]){
            //                NSString *bookDate = [weekDayRegularTimeDic objectForKey:@"bookDate"];
            //                    if ([bookDate isEqualToString:dateString]) {
            //                        NSArray *timesArry = [weekDayRegularTimeDic objectForKey:@"times"];
            //                        if (timesArry.count>0) {
            //                            return YES;
            //                        }
            //                    }
            //                }
            //            }
        }
    }
    
    return FALSE;
}

+(BOOL)bookableFromRegular:(NSArray *)regularTimeArray{
    
    if (regularTimeArray.count>0) {
//        NSArray *array =  [regularTimeArray.firstObject objectForKey:@"times"];
        BOOL canBook = [[regularTimeArray.firstObject objectForKey:@"bookable"] boolValue];
        if (canBook) {
            return YES;
        }
    }
    return NO;
}
//+(BOOL)bookableFromRegular:(NSArray *)regularTimeArray withPax:(NSNumber *)pax  andSlotTimeBookedArray:(NSArray *)tmpSlotTimeBookedArray  atDate:(NSDate *)date restaurant:(IMGRestaurant *)restaurant{
//    
//    if([BookUtil bookable:pax andSlotTimeBookedArray:tmpSlotTimeBookedArray atDate:date restaurant:restaurant]==FALSE){
//        return FALSE;
//    }
//    
//    NSCalendar *calendar=[NSCalendar currentCalendar];
//    NSDateComponents*comps1=[calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth |NSCalendarUnitDay | NSCalendarUnitWeekday) fromDate:date];
//    NSInteger currentWeekDay = [comps1 weekday];
//    currentWeekDay=currentWeekDay-1;
//    
//    int booked=[BookUtil bookedFromSlotTimeBookedArray:tmpSlotTimeBookedArray atDate:date];
//    
//    if(regularTimeArray!=nil && regularTimeArray.count>=(currentWeekDay+1)){
//        NSArray *weekDayRegularTimeArray=[regularTimeArray objectAtIndex:currentWeekDay];
//        if(weekDayRegularTimeArray!=nil){
//            for(NSDictionary *slotDictionary in weekDayRegularTimeArray){
//                NSNumber *weekDay = [slotDictionary objectForKey:@"weekDay"];
//                if([weekDay intValue]==currentWeekDay){
//                    NSNumber *avail = [slotDictionary objectForKey:@"avail"];
//                    if([avail intValue]-booked>=[pax intValue]){
//                        return TRUE;
//                    }
//                }
//            }
//        }
//    }
//    return FALSE;
//}

+(BOOL)bookableFromOffer:(IMGRestaurantOffer *)restaurantOffer withPax:(NSNumber *)pax andSlotTimeBookedArray:(NSArray *)tmpSlotTimeBookedArray andOfferTimeBookedArray:(NSArray *)tmpOfferTimeBookedArray atDate:(NSDate *)date restaurant:(IMGRestaurant *)restaurant{
    
    if([BookUtil bookable:pax andSlotTimeBookedArray:tmpSlotTimeBookedArray atDate:date restaurant:restaurant]==FALSE){
        return FALSE;
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];

    NSString *dateString = [formatter stringFromDate:date];

    NSCalendar *calendar=[NSCalendar currentCalendar];
    NSDateComponents*comps1=[calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth |NSCalendarUnitDay | NSCalendarUnitWeekday) fromDate:date];
    NSInteger currentWeekDay = [comps1 weekday];
    currentWeekDay=currentWeekDay-1;

//    int booked=[BookUtil bookedFromOfferTimeBookedArray:tmpOfferTimeBookedArray atDate:date offerId:[restaurantOffer.offerId intValue]];
    
    if(tmpOfferTimeBookedArray!=nil){
//        NSArray *slotStringArray = [restaurantOffer.offerSlotList componentsSeparatedByString:@","];
        //    concat_ws('|', IFNULL(b.book_time,0), IFNULL(b.off,0), IFNULL(b.seat,0),IFNULL(b.weekDay,0),IFNULL(o.booked,0),IFNULL(s.booked,0))
        for (int i=0 ; i<tmpOfferTimeBookedArray.count; i++) {
            
            NSDictionary *weekDayRegularTimeDic=[tmpOfferTimeBookedArray objectAtIndex:i];
            
            NSString *bookDate = [weekDayRegularTimeDic objectForKey:@"bookDate"];
            if ([bookDate isEqualToString:dateString]){
                return [[weekDayRegularTimeDic objectForKey:@"bookableWithOffer"] boolValue];
            }
        }
//        if(slotStringArray!=nil){
//            for(NSString *slotString in slotStringArray){
//                NSArray *slotInfoArray = [slotString componentsSeparatedByString:@"|"];
//                if(slotInfoArray!=nil&&slotInfoArray.count>5){
//                    NSString *offerSlotAvailString = [slotInfoArray objectAtIndex:2];
//                    NSString *weekDayString = [slotInfoArray objectAtIndex:3];
//                    if(currentWeekDay==[weekDayString intValue]){
//                        int offerSlotAvail = [offerSlotAvailString intValue];
//                        if(offerSlotAvail-booked>=[pax intValue]){
//                            return TRUE;
//                        }
//                    }
//                }
//            }
//        }
    }
    return FALSE;
}


+(BOOL)bookableNotFromOffer:(IMGRestaurantOffer *)restaurantOffer inOfferArray:(NSArray *)restaurantOfferArray withPax:(NSNumber *)pax  andSlotTimeBookedArray:(NSArray *)tmpSlotTimeBookedArray andOfferTimeBookedArray:(NSArray *)tmpOfferTimeBookedArray atDate:(NSDate *)date restaurant:(IMGRestaurant *)restaurant{
    
    if([BookUtil bookable:pax andSlotTimeBookedArray:tmpSlotTimeBookedArray atDate:date restaurant:restaurant]==FALSE){
        return FALSE;
    }
    
    NSCalendar *calendar=[NSCalendar currentCalendar];
    NSDateComponents*comps1=[calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth |NSCalendarUnitDay | NSCalendarUnitWeekday) fromDate:date];
    NSInteger currentWeekDay = [comps1 weekday];
    currentWeekDay=currentWeekDay-1;
    
    if(restaurantOfferArray!=nil){
        for(IMGRestaurantOffer *tmpRestaurantOffer in restaurantOfferArray){
            if(restaurantOffer==nil || [tmpRestaurantOffer.offerId intValue]!=[restaurantOffer.offerId intValue]){
                NSString *tmpOfferDataString = [NSString stringWithFormat:@"%@ 00:00:01",tmpRestaurantOffer.offerEndDate];
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
                NSDate* tmpDate = [formatter dateFromString:tmpOfferDataString];
             NSTimeInterval tmpDateTime  = [tmpDate timeIntervalSince1970];
                if (tmpDateTime>=[date timeIntervalSince1970]) {
                    int booked=[BookUtil bookedFromOfferTimeBookedArray:tmpOfferTimeBookedArray atDate:date offerId:[tmpRestaurantOffer.offerId intValue]];
                    
                    NSArray *slotStringArray = [tmpRestaurantOffer.offerSlotList componentsSeparatedByString:@","];
                    //    concat_ws('|', IFNULL(b.book_time,0), IFNULL(b.off,0), IFNULL(b.seat,0),IFNULL(b.weekDay,0),IFNULL(o.booked,0),IFNULL(s.booked,0))
                    if(slotStringArray!=nil){
                        for(NSString *slotString in slotStringArray){
                            NSArray *slotInfoArray = [slotString componentsSeparatedByString:@"|"];
                            if(slotInfoArray!=nil&slotInfoArray.count>5){
                                NSString *offerSlotAvailString = [slotInfoArray objectAtIndex:2];
                                int offerBooked = [[slotInfoArray objectAtIndex:4] intValue];
                                int restaurantBooked = [[slotInfoArray objectAtIndex:5] intValue];
                                booked = offerBooked>restaurantBooked?offerBooked:restaurantBooked;
                                NSString *weekDayString = [slotInfoArray objectAtIndex:3];
                                if(currentWeekDay==[weekDayString intValue]){
                                    int offerSlotAvail = [offerSlotAvailString intValue];
                                    if(offerSlotAvail-booked>=[pax intValue]){
                                        return TRUE;
                                    }
                                }
                                
                                
                            }
                        }
                    }

                }
            }
        }
    }
    
    return FALSE;
}

+(int)sumBookedFromSlotTimeBookedArray:(NSArray *)tmpSlotTimeBookedArray atDate:(NSDate *)date{
    if(tmpSlotTimeBookedArray==nil||tmpSlotTimeBookedArray.count==0){
        return 0;
    }else{
        long dateValue=[[NSNumber numberWithFloat:[date timeIntervalSince1970]] longValue];

        long dateValueInterval = [[NSTimeZone defaultTimeZone] secondsFromGMT]-7*3600;
        dateValue=dateValue+dateValueInterval;
        
        for(NSArray *tmpArray in tmpSlotTimeBookedArray){
            NSNumber *tmpDateValue=[tmpArray objectAtIndex:1];
            long longTmpDateValue=[tmpDateValue longLongValue]/1000;
            if(dateValue==longTmpDateValue){
                NSNumber *tmpBooked=[tmpArray objectAtIndex:2];
                return [tmpBooked intValue];
            }
        }
    }
    return 0;
}

+(int)bookedFromSlotTimeBookedArray:(NSArray *)tmpSlotTimeBookedArray atDate:(NSDate *)date{
    if(tmpSlotTimeBookedArray==nil||tmpSlotTimeBookedArray.count==0){
        return 0;
    }else{
        long dateValue=[[NSNumber numberWithFloat:[date timeIntervalSince1970]] longValue];
        long dateValueInterval = [[NSTimeZone defaultTimeZone] secondsFromGMT]-7*3600;
        dateValue=dateValue+dateValueInterval;
        
        for(NSArray *tmpArray in tmpSlotTimeBookedArray){
            NSNumber *tmpDateValue=[tmpArray objectAtIndex:1];
            long longTmpDateValue=[tmpDateValue longLongValue]/1000;
            if(dateValue==longTmpDateValue){
                NSNumber *tmpBooked=[tmpArray objectAtIndex:0];
                return [tmpBooked intValue];
            }
        }
    }
    return 0;
}

+(int)bookedFromOfferTimeBookedArray:(NSArray *)tmpOfferTimeBookedArray atDate:(NSDate *)date offerId:(int)offerId{
//    if(tmpOfferTimeBookedArray==nil||tmpOfferTimeBookedArray.count==0){
//        return 0;
//    }else{
//        long dateValue=[[NSNumber numberWithFloat:[date timeIntervalSince1970]] longValue];
//        long dateValueInterval = [[NSTimeZone defaultTimeZone] secondsFromGMT]-7*3600;
//        dateValue=dateValue+dateValueInterval;
//        
//        for(NSArray *tmpArray in tmpOfferTimeBookedArray){
//            NSNumber *tmpOfferId=[tmpArray objectAtIndex:0];
//            if([tmpOfferId intValue]==offerId){
//                NSNumber *tmpDateValue=[tmpArray objectAtIndex:2];
//                long longTmpDateValue=[tmpDateValue longLongValue]/1000;
//                if(dateValue==longTmpDateValue){
//                    NSNumber *tmpBooked=[tmpArray objectAtIndex:1];
//                    return [tmpBooked intValue];
//                }
//            }
//        }
//    }
//    return 0;
    return 0;
}


+(NSArray *)availableWeekDaysNotFromOffer:(IMGRestaurantOffer *)restaurantOffer inOfferArray:(NSArray *)restaurantOfferArray withPax:(NSNumber *)pax withAvailableWeekDays:(NSArray *)availableWeekDayArray{
    NSMutableArray *otherOfferAvailableWeekDayArray=[[NSMutableArray alloc]initWithCapacity:0];
    if(restaurantOfferArray!=nil){
        for(IMGRestaurantOffer *tmpRestaurantOffer in restaurantOfferArray){
            if(restaurantOffer==nil || [tmpRestaurantOffer.offerId intValue]!=[restaurantOffer.offerId intValue]){
                NSArray *slotStringArray = [tmpRestaurantOffer.offerSlotList componentsSeparatedByString:@","];
                //    concat_ws('|', IFNULL(b.book_time,0), IFNULL(b.off,0), IFNULL(b.seat,0),IFNULL(b.weekDay,0),IFNULL(o.booked,0),IFNULL(s.booked,0))
                if(slotStringArray!=nil){
                    for(NSString *slotString in slotStringArray){
                        NSArray *slotInfoArray = [slotString componentsSeparatedByString:@"|"];
                        if(slotInfoArray!=nil&slotInfoArray.count>6){
                            NSString *offerSlotAvailString = [slotInfoArray objectAtIndex:2];
                            NSString *weekDayString = [slotInfoArray objectAtIndex:3];
                            int offerSlotAvail = [offerSlotAvailString intValue];
                            if(offerSlotAvail>=[pax intValue]){
                                if([availableWeekDayArray indexOfObject:weekDayString]==NSNotFound){
                                    [otherOfferAvailableWeekDayArray addObject:weekDayString];
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    return otherOfferAvailableWeekDayArray;
}

+(int)maxAvailablePaxFromOffer:(IMGRestaurantOffer *)restaurantOffer{
    int maxAvailablePaxFromOffer = 0;
    if(restaurantOffer!=nil){
        NSArray *slotStringArray = [restaurantOffer.offerSlotList componentsSeparatedByString:@","];
        //    concat_ws('|', IFNULL(b.book_time,0), IFNULL(b.off,0), IFNULL(b.seat,0),IFNULL(b.weekDay,0),IFNULL(o.booked,0),IFNULL(s.booked,0))
        if(slotStringArray!=nil){
            for(NSString *slotString in slotStringArray){
                NSArray *slotInfoArray = [slotString componentsSeparatedByString:@"|"];
                if(slotInfoArray!=nil && slotInfoArray.count>4){
                    NSString *offerSlotAvailString = [slotInfoArray objectAtIndex:2];
                    NSString *offerSlotBookedString = [slotInfoArray objectAtIndex:4];
                    int offerSlotAvail = [offerSlotAvailString intValue];
                    int offerSlotBooked = [offerSlotBookedString intValue];
                    //            NSString *slotBooked  = [slotInfoArray objectAtIndex:5];
                    int leftSlotAvail = offerSlotAvail-offerSlotBooked;
                    if(leftSlotAvail>maxAvailablePaxFromOffer){
                        maxAvailablePaxFromOffer = leftSlotAvail;
                    }
                }
            }
        }
    }
    return maxAvailablePaxFromOffer;
}

+(int)maxAvailablePaxNotFromOffer:(IMGRestaurantOffer *)restaurantOffer inOfferArray:(NSArray *)restaurantOfferArray{
    int maxAvailablePaxFromOffer = 0;
    if(restaurantOfferArray!=nil){
        for(IMGRestaurantOffer *tmpRestaurantOffer in restaurantOfferArray){
            if(restaurantOffer==nil || [tmpRestaurantOffer.offerId intValue]!=[restaurantOffer.offerId intValue]){
                NSArray *slotStringArray = [tmpRestaurantOffer.offerSlotList componentsSeparatedByString:@","];
                //    concat_ws('|', IFNULL(b.book_time,0), IFNULL(b.off,0), IFNULL(b.seat,0),IFNULL(b.weekDay,0),IFNULL(o.booked,0),IFNULL(s.booked,0))
                if(slotStringArray!=nil){
                    for(NSString *slotString in slotStringArray){
                        NSArray *slotInfoArray = [slotString componentsSeparatedByString:@"|"];
                        if(slotInfoArray!=nil && slotInfoArray.count>4){
                            NSString *offerSlotAvailString = [slotInfoArray objectAtIndex:2];
                            NSString *offerSlotBookedString = [slotInfoArray objectAtIndex:4];
                            int offerSlotAvail = [offerSlotAvailString intValue];
                            int offerSlotBooked = [offerSlotBookedString intValue];
                            //            NSString *slotBooked  = [slotInfoArray objectAtIndex:5];
                            int leftSlotAvail = offerSlotAvail-offerSlotBooked;
                            if(leftSlotAvail>maxAvailablePaxFromOffer){
                                maxAvailablePaxFromOffer = leftSlotAvail;
                            }
                        }
                    }
                }
            }
        }
    }
    
    return maxAvailablePaxFromOffer;
}

+(BOOL)availableOfferSlotBy:(IMGRestaurantOffer *)restaurantOffer andPeopleCount:(NSInteger)peopleCount andDate:(NSDate *)date andStartTime:(NSString *)time{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [formatter stringFromDate:date];
    
    NSArray *timeSlotArray = [BookUtil timeSlotAround:date andTime:time];
    NSArray *bookDate = [timeSlotArray objectAtIndex:0];
    NSArray *bookTime = [timeSlotArray objectAtIndex:1];
    
    NSArray *slotStringArray = [restaurantOffer.offerSlotList componentsSeparatedByString:@","];
    //    concat_ws('|', IFNULL(b.book_time,0), IFNULL(b.off,0), IFNULL(b.seat,0),IFNULL(b.weekDay,0),IFNULL(o.booked,0),IFNULL(s.booked,0))
    if(slotStringArray!=nil){
        for(NSString *slotString in slotStringArray){
            NSArray *slotInfoArray = [slotString componentsSeparatedByString:@"|"];
            if(slotInfoArray!=nil&&slotInfoArray.count>4){
                NSString *offerTimeString = [slotInfoArray objectAtIndex:0];
                NSString *offerSlotAvailString = [slotInfoArray objectAtIndex:2];
                NSString *offerSlotBookedString = [slotInfoArray objectAtIndex:4];
                int offerSlotAvail = [offerSlotAvailString intValue];
                int offerSlotBooked = [offerSlotBookedString intValue];
                //            NSString *slotBooked  = [slotInfoArray objectAtIndex:5];
                
                for(int i=0;i<bookTime.count;i++){
                    NSString *bookDateString = [bookDate objectAtIndex:i];
                    NSString *bookTimeString = [bookTime objectAtIndex:i];
                    if([bookDateString isEqual:dateString] && [BookUtil timeStringToInt:bookTimeString]==[offerTimeString intValue]){
                        if(offerSlotAvail>=peopleCount+offerSlotBooked){
                            return TRUE;
                        }
                    }
                }
            }
        }
    }
    
    return FALSE;
}


+(NSArray *)selectOfferSlotBy:(IMGRestaurantOffer *)restaurantOffer andPeopleCount:(NSInteger)peopleCount andDate:(NSDate *)date andStartTime:(NSString *)time{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [formatter stringFromDate:date];
    
    NSArray *timeSlotArray = [BookUtil timeSlotAround:date andTime:time];
    NSArray *bookDate = [timeSlotArray objectAtIndex:0];
    NSArray *bookTime = [timeSlotArray objectAtIndex:1];
    
    NSMutableArray *returnOfferSlotArray = [[NSMutableArray alloc]initWithCapacity:0];
    
    NSString *maxBookTimeString=nil;
    for(int i=0;i<[bookTime count];i++){
        NSArray *timeSlot = [[NSArray alloc] initWithObjects:@"Not Available",[bookTime objectAtIndex:i],@"0", nil];
        [returnOfferSlotArray addObject:timeSlot];
        NSString *bookDateString = [bookDate objectAtIndex:i];
        if([bookDateString isEqual:dateString]){
            maxBookTimeString = [bookTime objectAtIndex:i];
        }
    }
    
    NSString *todayDateString = [BookUtil todayStringWithFormat:@"yyyy-MM-dd"];
    NSString *todayTimeString = [BookUtil todayStringWithFormat:@"HH:mm"];
    int todayTimeValue = [BookUtil timeStringToInt:todayTimeString];
    
    NSArray *slotStringArray = [restaurantOffer.offerSlotList componentsSeparatedByString:@","];
    //    concat_ws('|', IFNULL(b.book_time,0), IFNULL(b.off,0), IFNULL(b.seat,0),IFNULL(b.weekDay,0),IFNULL(o.booked,0),IFNULL(s.booked,0))
    if(slotStringArray!=nil){
        for(NSString *slotString in slotStringArray){
            NSArray *slotInfoArray = [slotString componentsSeparatedByString:@"|"];
            if(slotInfoArray!=nil&&slotInfoArray.count>4){
                NSString *offerTimeString = [slotInfoArray objectAtIndex:0];
                NSString *offerSlotAvailString = [slotInfoArray objectAtIndex:2];
                NSString *offerSlotBookedString = [slotInfoArray objectAtIndex:4];
                int offerSlotAvail = [offerSlotAvailString intValue];
                int offerSlotBooked = [offerSlotBookedString intValue];
                //            NSString *slotBooked  = [slotInfoArray objectAtIndex:5];
                int offerTimeIntValue = [offerTimeString intValue];
                
                for(int i=0;i<bookTime.count;i++){
                    NSString *bookDateString = [bookDate objectAtIndex:i];
                    NSString *bookTimeString = [bookTime objectAtIndex:i];
                    if(([todayDateString compare:bookDateString]==NSOrderedAscending && [bookDateString isEqual:dateString] && [BookUtil timeStringToInt:bookTimeString]==offerTimeIntValue) ||
                       ([bookDateString isEqual:dateString] && [BookUtil timeStringToInt:bookTimeString]==offerTimeIntValue && [bookDateString isEqualToString:todayDateString] && offerTimeIntValue>todayTimeValue)){
                        if(offerSlotAvail>=peopleCount+offerSlotBooked){
                            NSArray *timeSlot = [[NSArray alloc]initWithObjects:@"Book Now",bookTimeString,[NSString stringWithFormat:@"%d",offerSlotAvail-offerSlotBooked],nil];
                            [returnOfferSlotArray replaceObjectAtIndex:i withObject:timeSlot];
                        }
                        break;
                    }
                }
              
                
            }
        }
    }
    todayTimeValue = [BookUtil timeStringToInt:time]+30*60*1000;
    
    if(slotStringArray!=nil){
        for(NSString *slotString in slotStringArray){
            NSArray *slotInfoArray = [slotString componentsSeparatedByString:@"|"];
            if(slotInfoArray!=nil&&slotInfoArray.count>4){
                NSString *offerTimeString = [slotInfoArray objectAtIndex:0];
                NSString *offerSlotAvailString = [slotInfoArray objectAtIndex:2];
                NSString *offerSlotBookedString = [slotInfoArray objectAtIndex:4];
                int offerSlotAvail = [offerSlotAvailString intValue];
                int offerSlotBooked = [offerSlotBookedString intValue];
                //            NSString *slotBooked  = [slotInfoArray objectAtIndex:5];
                int offerTimeIntValue = [offerTimeString intValue];
                

                if(maxBookTimeString!=nil && [BookUtil timeStringToInt:maxBookTimeString]<offerTimeIntValue && offerTimeIntValue>todayTimeValue){
                    if(offerSlotAvail>=peopleCount+offerSlotBooked){
                        NSArray *timeSlot = [[NSArray alloc]initWithObjects:@"Book Now",[BookUtil timeIntToString:offerTimeIntValue],[NSString stringWithFormat:@"%d",offerSlotAvail-offerSlotBooked], nil];
                        [returnOfferSlotArray addObject:timeSlot];
                    }
                }
            }
        }
    }

    return returnOfferSlotArray;
}

+(NSArray *)selectRegularTimeSlotBy:(NSInteger)peopleCount andDate:(NSDate *)date inRegularTimeArray:(NSArray *)regularTimeArray  andStartTime:(NSString *)time{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [formatter stringFromDate:date];
    
    NSArray *timeSlotArray = [BookUtil timeSlotAround:date andTime:time];
    NSArray *bookDate = [timeSlotArray objectAtIndex:0];
    NSArray *bookTime = [timeSlotArray objectAtIndex:1];
    
    NSMutableArray *returnTimeSlotArray = [[NSMutableArray alloc]initWithCapacity:0];
    
    NSString *maxBookTimeString=nil;
    for(int i=0;i<[bookDate count];i++){
        NSArray *timeSlot = [[NSArray alloc] initWithObjects:@"Not Available",[bookTime objectAtIndex:i],@"0", nil];
        [returnTimeSlotArray addObject:timeSlot];
        NSString *bookDateString = [bookDate objectAtIndex:i];
        if([bookDateString isEqual:dateString]){
            maxBookTimeString = [bookTime objectAtIndex:i];
        }
    }
    
    if(regularTimeArray!=nil){
        for(NSDictionary *slotDictionary in regularTimeArray){
            NSString *slotBookTimeString = [slotDictionary objectForKey:@"bookTime"];
            NSNumber *avail = [slotDictionary objectForKey:@"avail"];
            
            for(int i=0;i<bookTime.count;i++){
                NSString *bookDateString = [bookDate objectAtIndex:i];
                NSString *bookTimeString = [bookTime objectAtIndex:i];
                if([bookDateString isEqual:dateString] && [slotBookTimeString isEqualToString:bookTimeString]){
                    if([avail intValue]>=peopleCount){
                        NSArray *timeSlot = [[NSArray alloc]initWithObjects:@"Book Now",bookTimeString,[NSString stringWithFormat:@"%d",[avail intValue]], nil];
                        [returnTimeSlotArray replaceObjectAtIndex:i withObject:timeSlot];
                    }
                    break;
                }
            }
            
            if(maxBookTimeString!=nil && [BookUtil timeStringToInt:maxBookTimeString]<[BookUtil timeStringToInt:slotBookTimeString]){
                if([avail intValue]>=peopleCount){
                    NSArray *timeSlot = [[NSArray alloc]initWithObjects:@"Book Now",slotBookTimeString,[NSString stringWithFormat:@"%d",[avail intValue]], nil];
                    [returnTimeSlotArray addObject:timeSlot];
                }
            }
            
        }
    }
    
    
    return returnTimeSlotArray;
}
+ (void)getReservationInfoWithparameters:(NSDictionary *)parameters andBlock:(void (^)(IMGReservation *reservation))block
{
    [[IMGNetWork sharedManager]POST:@"app/reservation/detailInfo" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         IMGReservation *reservation = [[IMGReservation alloc] init];
         reservation.cuisineName=[responseObject objectForKey:@"restaurantCuisine"];
         reservation.districtName=[responseObject objectForKey:@"restaurantDistrict"];
         reservation.specialRequest = [responseObject objectForKey:@"remark"];
         reservation.priceName=[responseObject objectForKey:@"restaurantPriceLevel"];
         NSDictionary* simleResturant=[responseObject objectForKey:@"simpleRestaurant"];
         reservation.ratingScore=[simleResturant objectForKey:@"ratingScore"];
         NSDictionary* location=[simleResturant objectForKey:@"location"];
         reservation.longitude=[location objectForKey:@"longitude"];
         reservation.latitude=[location objectForKey:@"latitude"];
         
         [reservation setValuesForKeysWithDictionary:responseObject];
         reservation.restaurantPhone = [[responseObject objectForKey:@"simpleRestaurant"] objectForKey:@"phoneNumber"];
         reservation.restaurantSeoKeywords = [[responseObject objectForKey:@"simpleRestaurant"] objectForKey:@"seoKeyword"];
         reservation.restaurantTitle = [[responseObject objectForKey:@"simpleRestaurant"] objectForKey:@"title"];
         reservation.restaurantImage = [[responseObject objectForKey:@"simpleRestaurant"] objectForKey:@"imageUrl"];
         reservation.ratingScore = [[responseObject objectForKey:@"simpleRestaurant"] objectForKey:@"ratingScore"];
         
         block(reservation);
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"getReservationInfo error: %@",error.localizedDescription);
     }];
}
+(BOOL)bookableWithNowTime:(NSArray *)tmpSlotTimeBookedArray andCurrentWeekDay:(long)currentWeekDay
{
    NSMutableArray *bookTimeArrM = [[NSMutableArray alloc] init];
    long currentWeekDayTemp;
    if (currentWeekDay == 7)
    {
        currentWeekDayTemp = 0;
    }
    else
    {
        currentWeekDayTemp = currentWeekDay;
    }
    if (tmpSlotTimeBookedArray.count == 7)
    {
        for (NSDictionary *dic in [tmpSlotTimeBookedArray objectAtIndex:currentWeekDayTemp])
        {
            if ([[dic objectForKey:@"weekDay"] intValue] == currentWeekDayTemp)
            {
                [bookTimeArrM addObject:[dic objectForKey:@"bookTime"]];
            }
        }
        long maxBookTime = 0;
        for (NSNumber *bookTime in bookTimeArrM)
        {
            if (maxBookTime < [bookTime longValue])
            {
                maxBookTime = [bookTime longValue];
            }
        }
        maxBookTime = maxBookTime/3600/1000;
        
        
        NSDate *now = [NSDate date];

        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSUInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
        NSDateComponents *dateComponent = [calendar components:unitFlags fromDate:now];

        long hour = [dateComponent hour];
        float minute = [dateComponent minute];
//        NSLog(@"hour = %ld,minute = %ld",hour,minute);
        NSString *minuteTmp = [NSString stringWithFormat:@"%f",minute/60];
        if (minuteTmp.length)
        {
            minuteTmp = [minuteTmp substringFromIndex:1];
        }
        NSString *nowTimeStr = [NSString stringWithFormat:@"%ld%@",hour,minuteTmp];
        
        if ([nowTimeStr floatValue] > maxBookTime)
        {
            return NO;
        }
        else
            return YES;

    }
    return NO;
}

@end
