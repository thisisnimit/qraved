//
//  MixpanelHelper.m
//  Qraved
//
//  Created by Laura on 14/12/3.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "MixpanelHelper.h"
#import "Mixpanel.h"
#import "DBManager.h"
#import "IMGDish.h"
#import "IMGCuisine.h"
#import "NSDate+Helper.h"
#import "NSNumber+Helper.h"

@implementation MixpanelHelper

+(void)trackViewRestaurantDetailWithRestaurant:(IMGRestaurant*)restaurant{
    NSString *sql=[NSString stringWithFormat:@"SELECT * FROM IMGCuisine c left join IMGRestaurantCuisine rc on c.cuisineId=rc.cuisineId WHERE rc.restaurantId = %@ limit 1;",restaurant.restaurantId];
    [[DBManager manager]selectWithSql:sql successBlock:^(FMResultSet *resultSet) {
        if ([resultSet next]) {
            IMGCuisine *cuisine = [[IMGCuisine alloc]init];
            [cuisine setValueWithResultSet:resultSet];
            
            NSMutableDictionary *properties = [[NSMutableDictionary alloc]init];
            if (restaurant.title) {
                [properties setObject:restaurant.title forKey:@"Restaurant"];
            }
            if (restaurant.restaurantId) {
                [properties setObject:restaurant.restaurantId forKey:@"Restaurant ID"];
            }
            if (cuisine.name) {
                [properties setObject:cuisine.name forKey:@"Cuisine"];
            }
            if (cuisine.cuisineId) {
                [properties setObject:cuisine.cuisineId forKey:@"Cuisine ID"];
            }
            if (restaurant.districtName) {
                [properties setObject:restaurant.districtName forKey:@"Disrtict"];
            }
            if (restaurant.districtId) {
                [properties setObject:restaurant.districtId forKey:@"Disrtict ID"];
            }
            if (restaurant.priceLevel) {
                switch ([restaurant.priceLevel integerValue]) {
                    case 0:
                        [properties setObject:@"0" forKey:@"Price Level"];
                        break;
                    case 1:
                        [properties setObject:@"$" forKey:@"Price Level"];
                        break;
                    case 2:
                        [properties setObject:@"$$" forKey:@"Price Level"];
                        break;
                    case 3:
                        [properties setObject:@"$$$" forKey:@"Price Level"];
                        break;
                    case 4:
                        [properties setObject:@"$$$$" forKey:@"Price Level"];
                        break;
                    default:
                        break;
                }
                
            }
            if (restaurant.ratingScore) {
                [properties setObject:[NSNumber numberWithFloat:[[NSNumber numberWithFloat:[restaurant.ratingScore floatValue]] getRatingNumber]] forKey:@"Rating"];
            }
            if (restaurant.disCount) {
                [properties setObject:restaurant.disCount forKey:@"Top Discount Off"];
            }
            if (restaurant.address) {
                [properties setObject:restaurant.address forKey:@"Address"];
            }
            if (restaurant.qravedCount) {
                [properties setObject:restaurant.qravedCount forKey:@"Number of Qraved Photos"];
            }
            if (restaurant.reviewCount) {
                [properties setObject:restaurant.reviewCount forKey:@"Number of Reviews"];
            }

            
            NSMutableArray *dishs = [[NSMutableArray alloc]init];
            NSString *sqlString=[NSString stringWithFormat:@"SELECT * FROM IMGDish where restaurantId=%@;",restaurant.restaurantId];
            [[DBManager manager]selectWithSql:sqlString successBlock:^(FMResultSet *resultSet) {
                while ([resultSet next]) {
                    IMGDish *dish = [[IMGDish alloc]init];
                    [dish setValueWithResultSet:resultSet];
                    [dishs addObject:dish.title];
                }
                if (dishs.count) {
                    [properties setObject:dishs forKey:@"Qraved Dishes"];
                }
                
                [resultSet close];
            } failureBlock:^(NSError *error) {
                
            }];

            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"View Restaurant Detail" properties:properties];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"ViewRestaurantDetailWithRestaurant error:%@",error.description);
    }];
    
    
}

+(void)trackRestaurantTabFilterWithTab:(NSString*)tab{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Restaurant Tab Filter" properties:@{@"Tab": tab,@"Service Client Type":QRAVED_WEB_SERVICE_CLIENT,@"Service Version":QRAVED_WEB_SERVICE_VERSION}];
}
+(void)trackSerachRestaurantPriceLevelsWithArray:(NSArray*)array;{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Search Restaurant" properties:@{@"Price Level": array,@"Service Client Type":QRAVED_WEB_SERVICE_CLIENT,@"Service Version":QRAVED_WEB_SERVICE_VERSION}];
}

+(void)trackSerachRestaurantPriceLevelWithTab:(NSString*)tab{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Search Restaurant" properties:@{@"Price Level": tab,@"Service Client Type":QRAVED_WEB_SERVICE_CLIENT,@"Service Version":QRAVED_WEB_SERVICE_VERSION}];
}
+(void)trackSerachRestaurantCuisine:(NSArray*)cuisines{
    NSMutableArray *cuisineArray = [[NSMutableArray alloc]init];
    for (int i=0; i<cuisines.count; i++) {
        IMGCuisine *cuisine = [cuisines objectAtIndex:i];
        [cuisineArray addObject:cuisine.name];
    }
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Search Restaurant" properties:@{@"Cuisine": cuisineArray,@"Service Client Type":QRAVED_WEB_SERVICE_CLIENT,@"Service Version":QRAVED_WEB_SERVICE_VERSION}];

}
+(void)trackViewMethodforSearchListWithTab:(NSString*)tab{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"View Method for Search List" properties:@{@"View Method": tab,@"Service Client Type":QRAVED_WEB_SERVICE_CLIENT,@"Service Version":QRAVED_WEB_SERVICE_VERSION}];
}
+(void)trackSerachRestaurantWithKeyWord:(NSString*)keyword{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Search Restaurant" properties:@{@"Key Words":keyword,@"Service Client Type":QRAVED_WEB_SERVICE_CLIENT,@"Service Version":QRAVED_WEB_SERVICE_VERSION}];
}
+(void)trackSignUpWithTab:(NSString*)tab{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Sign Up" properties:@{@"Sign Up Type": tab,@"Service Client Type":QRAVED_WEB_SERVICE_CLIENT,@"Service Version":QRAVED_WEB_SERVICE_VERSION}];
}
+(void)trackSerachRestaurantNeighborhood:(NSString*)tab{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Search Restaurant" properties:@{@"Neighborhood":tab,@"Service Client Type":QRAVED_WEB_SERVICE_CLIENT,@"Service Version":QRAVED_WEB_SERVICE_VERSION}];
}
+(void)trackLoginWithType:(NSString*)type{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Login" properties:@{@"Login Type": type,@"Service Client Type":QRAVED_WEB_SERVICE_CLIENT,@"Service Version":QRAVED_WEB_SERVICE_VERSION}];
}
+(void)trackOpenBookPopupWindowWithRestaurant:(IMGRestaurant*)restaurant{
    
    NSString *sql=[NSString stringWithFormat:@"SELECT * FROM IMGCuisine c left join IMGRestaurantCuisine rc on c.cuisineId=rc.cuisineId WHERE rc.restaurantId = %@ limit 1;",restaurant.restaurantId];
    [[DBManager manager]selectWithSql:sql successBlock:^(FMResultSet *resultSet) {
        if ([resultSet next]) {
            IMGCuisine *cuisine = [[IMGCuisine alloc]init];
            [cuisine setValueWithResultSet:resultSet];
            
            NSMutableDictionary *properties = [[NSMutableDictionary alloc]init];
            if (restaurant.title) {
                [properties setObject:restaurant.title forKey:@"Restaurant"];
            }
            if (restaurant.restaurantId) {
                [properties setObject:restaurant.restaurantId forKey:@"Restaurant ID"];
            }
            if (cuisine.name) {
                [properties setObject:cuisine.name forKey:@"Cuisine"];
            }
            if (cuisine.cuisineId) {
                [properties setObject:cuisine.cuisineId forKey:@"Cuisine ID"];
            }
            if (restaurant.districtName) {
                [properties setObject:restaurant.districtName forKey:@"Disrtict"];
            }
            if (restaurant.districtId) {
                [properties setObject:restaurant.districtId forKey:@"Disrtict ID"];
            }
            if (restaurant.priceLevel) {
                switch ([restaurant.priceLevel integerValue]) {
                    case 0:
                        [properties setObject:@"0" forKey:@"Price Level"];
                        break;
                    case 1:
                        [properties setObject:@"$" forKey:@"Price Level"];
                        break;
                    case 2:
                        [properties setObject:@"$$" forKey:@"Price Level"];
                        break;
                    case 3:
                        [properties setObject:@"$$$" forKey:@"Price Level"];
                        break;
                    case 4:
                        [properties setObject:@"$$$$" forKey:@"Price Level"];
                        break;
                    default:
                        break;
                }
                
            }
            if (restaurant.ratingScore) {
                [properties setObject:[NSNumber numberWithFloat:[[NSNumber numberWithFloat:[restaurant.ratingScore floatValue]] getRatingNumber]] forKey:@"Rating"];
            }

            if (restaurant.disCount) {
                [properties setObject:restaurant.disCount forKey:@"Top Discount Off"];
            }
            if (restaurant.address) {
                [properties setObject:restaurant.address forKey:@"Address"];
            }
            if (restaurant.qravedCount) {
                [properties setObject:restaurant.qravedCount forKey:@"Number of Qraved Photos"];
            }
            if (restaurant.reviewCount) {
                [properties setObject:restaurant.reviewCount forKey:@"Number of Reviews"];
            }
            
            NSMutableArray *dishs = [[NSMutableArray alloc]init];
            NSString *sqlString=[NSString stringWithFormat:@"SELECT * FROM IMGDish where restaurantId=%@;",restaurant.restaurantId];
            [[DBManager manager]selectWithSql:sqlString successBlock:^(FMResultSet *resultSet) {
                while ([resultSet next]) {
                    IMGDish *dish = [[IMGDish alloc]init];
                    [dish setValueWithResultSet:resultSet];
                    [dishs addObject:dish.title];
                }
                if (dishs.count>0) {
                    [properties setObject:dishs forKey:@"Qraved Dishes"];
                }
                [resultSet close];
            } failureBlock:^(NSError *error) {
                
            }];
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Open Book Popup Window" properties:properties];
            
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"ViewRestaurantDetailWithRestaurant error:%@",error.description);
    }];
    
    
}
+(void)trackTapBottomCallButtonPropertiesWithRestaurant:(IMGRestaurant*)restaurant{
    NSString *sql=[NSString stringWithFormat:@"SELECT * FROM IMGCuisine c left join IMGRestaurantCuisine rc on c.cuisineId=rc.cuisineId WHERE rc.restaurantId = %@ limit 1;",restaurant.restaurantId];
    [[DBManager manager]selectWithSql:sql successBlock:^(FMResultSet *resultSet) {
        if ([resultSet next]) {
            IMGCuisine *cuisine = [[IMGCuisine alloc]init];
            [cuisine setValueWithResultSet:resultSet];
            
            NSMutableDictionary *properties = [[NSMutableDictionary alloc]init];
            if (restaurant.title) {
                [properties setObject:restaurant.title forKey:@"Restaurant"];
            }
            if (restaurant.restaurantId) {
                [properties setObject:restaurant.restaurantId forKey:@"Restaurant ID"];
            }
            if (cuisine.name) {
                [properties setObject:cuisine.name forKey:@"Cuisine"];
            }
            if (cuisine.cuisineId) {
                [properties setObject:cuisine.cuisineId forKey:@"Cuisine ID"];
            }
            if (restaurant.districtName) {
                [properties setObject:restaurant.districtName forKey:@"Disrtict"];
            }
            if (restaurant.districtId) {
                [properties setObject:restaurant.districtId forKey:@"Disrtict ID"];
            }
            if (restaurant.priceLevel) {
                switch ([restaurant.priceLevel integerValue]) {
                    case 0:
                        [properties setObject:@"0" forKey:@"Price Level"];
                        break;
                    case 1:
                        [properties setObject:@"$" forKey:@"Price Level"];
                        break;
                    case 2:
                        [properties setObject:@"$$" forKey:@"Price Level"];
                        break;
                    case 3:
                        [properties setObject:@"$$$" forKey:@"Price Level"];
                        break;
                    case 4:
                        [properties setObject:@"$$$$" forKey:@"Price Level"];
                        break;
                    default:
                        break;
                }
                
            }
            if (restaurant.ratingScore) {
                [properties setObject:[NSNumber numberWithFloat:[[NSNumber numberWithFloat:[restaurant.ratingScore floatValue]] getRatingNumber]] forKey:@"Rating"];
            }
            if (restaurant.disCount) {
                [properties setObject:restaurant.disCount forKey:@"Top Discount Off"];
            }
            
            [properties setObject:QRAVED_WEB_SERVICE_CLIENT forKey:@"Service Client Type"];
            [properties setObject:QRAVED_WEB_SERVICE_VERSION forKey:@"Service Version"];
            
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Tap Bottom Call Button" properties:properties];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"trackTapBottomCallButtonPropertiesWithRestaurant error:%@",error.description);
    }];
    
}

+(void)trackTapPageCallButtonPropertiesWithRestaurant:(IMGRestaurant*)restaurant{
    NSString *sql=[NSString stringWithFormat:@"SELECT * FROM IMGCuisine c left join IMGRestaurantCuisine rc on c.cuisineId=rc.cuisineId WHERE rc.restaurantId = %@ limit 1;",restaurant.restaurantId];
    [[DBManager manager]selectWithSql:sql successBlock:^(FMResultSet *resultSet) {
        if ([resultSet next]) {
            IMGCuisine *cuisine = [[IMGCuisine alloc]init];
            [cuisine setValueWithResultSet:resultSet];
            
            NSMutableDictionary *properties = [[NSMutableDictionary alloc]init];
            if (restaurant.title) {
                [properties setObject:restaurant.title forKey:@"Restaurant"];
            }
            if (restaurant.restaurantId) {
                [properties setObject:restaurant.restaurantId forKey:@"Restaurant ID"];
            }
            if (cuisine.name) {
                [properties setObject:cuisine.name forKey:@"Cuisine"];
            }
            if (cuisine.cuisineId) {
                [properties setObject:cuisine.cuisineId forKey:@"Cuisine ID"];
            }
            if (restaurant.districtName) {
                [properties setObject:restaurant.districtName forKey:@"Disrtict"];
            }
            if (restaurant.districtId) {
                [properties setObject:restaurant.districtId forKey:@"Disrtict ID"];
            }
            if (restaurant.priceLevel) {
                switch ([restaurant.priceLevel integerValue]) {
                    case 0:
                        [properties setObject:@"0" forKey:@"Price Level"];
                        break;
                    case 1:
                        [properties setObject:@"$" forKey:@"Price Level"];
                        break;
                    case 2:
                        [properties setObject:@"$$" forKey:@"Price Level"];
                        break;
                    case 3:
                        [properties setObject:@"$$$" forKey:@"Price Level"];
                        break;
                    case 4:
                        [properties setObject:@"$$$$" forKey:@"Price Level"];
                        break;
                    default:
                        break;
                }
                
            }
            if (restaurant.ratingScore) {
                [properties setObject:[NSNumber numberWithFloat:[[NSNumber numberWithFloat:[restaurant.ratingScore floatValue]] getRatingNumber]] forKey:@"Rating"];
            }
            if (restaurant.disCount) {
                [properties setObject:restaurant.disCount forKey:@"Top Discount Off"];
            }
            
            [properties setObject:QRAVED_WEB_SERVICE_CLIENT forKey:@"Service Client Type"];
            [properties setObject:QRAVED_WEB_SERVICE_VERSION forKey:@"Service Version"];
            
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Tap in Page Call Button" properties:properties];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"trackTapBottomCallButtonPropertiesWithRestaurant error:%@",error.description);
    }];
    
}

+(void)trackChooseBookingPropertiesWithRestaurant:(IMGRestaurant*)restaurant andReservation:(IMGRestaurantOffer*)reservationOff{
    NSString *sql=[NSString stringWithFormat:@"SELECT * FROM IMGCuisine c left join IMGRestaurantCuisine rc on c.cuisineId=rc.cuisineId WHERE rc.restaurantId = %@ limit 1;",restaurant.restaurantId];
    [[DBManager manager]selectWithSql:sql successBlock:^(FMResultSet *resultSet) {
        if ([resultSet next]) {
            IMGCuisine *cuisine = [[IMGCuisine alloc]init];
            [cuisine setValueWithResultSet:resultSet];
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            NSString *dinners = [userDefaults objectForKey:@"dinners"];
            NSDate *date = [userDefaults objectForKey:@"date"];
            NSString *time = [userDefaults objectForKey:@"time"];
            NSString *week = [date getLowerWeekString];
            
            NSMutableDictionary *properties = [[NSMutableDictionary alloc]init];
            if (restaurant.title) {
                [properties setObject:restaurant.title forKey:@"Restaurant"];
            }
            if (restaurant.restaurantId) {
                [properties setObject:restaurant.restaurantId forKey:@"Restaurant ID"];
            }
            if (cuisine.name) {
                [properties setObject:cuisine.name forKey:@"Cuisine"];
            }
            if (cuisine.cuisineId) {
                [properties setObject:cuisine.cuisineId forKey:@"Cuisine ID"];
            }
            if (restaurant.districtName) {
                [properties setObject:restaurant.districtName forKey:@"Disrtict"];
            }
            if (restaurant.districtId) {
                [properties setObject:restaurant.districtId forKey:@"Disrtict ID"];
            }
            if (restaurant.priceLevel) {
                switch ([restaurant.priceLevel integerValue]) {
                    case 0:
                        [properties setObject:@"0" forKey:@"Price Level"];
                        break;
                    case 1:
                        [properties setObject:@"$" forKey:@"Price Level"];
                        break;
                    case 2:
                        [properties setObject:@"$$" forKey:@"Price Level"];
                        break;
                    case 3:
                        [properties setObject:@"$$$" forKey:@"Price Level"];
                        break;
                    case 4:
                        [properties setObject:@"$$$$" forKey:@"Price Level"];
                        break;
                    default:
                        break;
                }
                
            }
            if (restaurant.ratingScore) {
                [properties setObject:[NSNumber numberWithFloat:[[NSNumber numberWithFloat:[restaurant.ratingScore floatValue]] getRatingNumber]] forKey:@"Rating"];
            }
            if (restaurant.disCount) {
                [properties setObject:restaurant.disCount forKey:@"Top Discount Off"];
            }
            if (reservationOff.offerId) {
                [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"select *from IMGRestaurantOffer where offerId=%@",reservationOff.offerId] successBlock:^(FMResultSet *resultSet) {
                    if ([resultSet next]) {
                        IMGRestaurantOffer *restaurantOffer = [[IMGRestaurantOffer alloc]init];
                        [restaurantOffer setValueWithResultSet:resultSet];
                        [properties setObject:restaurantOffer.offerSlotMaxDisc forKey:@"Discount Off"];
                        [resultSet close];
                    }
                } failureBlock:^(NSError *error) {
                    
                }];
                
            }
            
            
            if (dinners) {
                [properties setObject:dinners forKey:@"Diners"];
            }
            if ([date getMonthDayString]) {
                [properties setObject:[date getMonthDayString] forKey:@"Date"];
            }
            if (week) {
                [properties setObject:week forKey:@"Week Day"];
            }
            if (time) {
                [properties setObject:time forKey:@"Time"];
            }
            [properties setObject:QRAVED_WEB_SERVICE_CLIENT forKey:@"Service Client Type"];
            [properties setObject:QRAVED_WEB_SERVICE_VERSION forKey:@"Service Version"];
            
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Choose Booking Properties" properties:properties];
            
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"ViewRestaurantDetailWithRestaurant error:%@",error.description);
    }];
    
}
+(void)trackApplyVoucharCodeWithRestaurant:(IMGRestaurant*)restaurant andReservation:(IMGReservation*)reservation{
    NSString *sql=[NSString stringWithFormat:@"SELECT * FROM IMGCuisine c left join IMGRestaurantCuisine rc on c.cuisineId=rc.cuisineId WHERE rc.restaurantId = %@ limit 1;",restaurant.restaurantId];
    [[DBManager manager]selectWithSql:sql successBlock:^(FMResultSet *resultSet) {
        if ([resultSet next]) {
            IMGCuisine *cuisine = [[IMGCuisine alloc]init];
            [cuisine setValueWithResultSet:resultSet];
            
            NSMutableDictionary *properties = [[NSMutableDictionary alloc]init];
            if (restaurant.title) {
                [properties setObject:restaurant.title forKey:@"Restaurant"];
            }
            if (restaurant.restaurantId) {
                [properties setObject:restaurant.restaurantId forKey:@"Restaurant ID"];
            }
            if (cuisine.name) {
                [properties setObject:cuisine.name forKey:@"Cuisine"];
            }
            if (cuisine.cuisineId) {
                [properties setObject:cuisine.cuisineId forKey:@"Cuisine ID"];
            }
            if (restaurant.districtName) {
                [properties setObject:restaurant.districtName forKey:@"Disrtict"];
            }
            if (restaurant.districtId) {
                [properties setObject:restaurant.districtId forKey:@"Disrtict ID"];
            }
            if (restaurant.priceLevel) {
                switch ([restaurant.priceLevel integerValue]) {
                    case 0:
                        [properties setObject:@"0" forKey:@"Price Level"];
                        break;
                    case 1:
                        [properties setObject:@"$" forKey:@"Price Level"];
                        break;
                    case 2:
                        [properties setObject:@"$$" forKey:@"Price Level"];
                        break;
                    case 3:
                        [properties setObject:@"$$$" forKey:@"Price Level"];
                        break;
                    case 4:
                        [properties setObject:@"$$$$" forKey:@"Price Level"];
                        break;
                    default:
                        break;
                }
                
            }
            if (restaurant.ratingScore) {
                [properties setObject:[NSNumber numberWithFloat:[[NSNumber numberWithFloat:[restaurant.ratingScore floatValue]] getRatingNumber]] forKey:@"Rating"];
            }
            if (restaurant.disCount) {
                [properties setObject:restaurant.disCount forKey:@"Top Discount Off"];
            }
            if (reservation.off) {
                [properties setObject:reservation.off forKey:@"Discount Off"];
            }
            if (reservation.party) {
                [properties setObject:reservation.party forKey:@"Diners"];
            }
            if (reservation.bookDate) {//[date getMonthDayString]
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *bookDate = [dateFormatter dateFromString:reservation.bookDate];
                [properties setObject:[bookDate getMonthDayString] forKey:@"Date"];
                [properties setObject:[bookDate getLowerWeekString] forKey:@"Week Day"];
            }
            if (reservation.bookTime) {
                [properties setObject:reservation.bookTime forKey:@"Time"];
            }
            if (reservation.voucherCode) {
                [properties setObject:reservation.voucherCode forKey:@"Vouchar Code"];
            }
            
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Apply Vouchar Code" properties:properties];
            
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"ViewRestaurantDetailWithRestaurant error:%@",error.description);
    }];
}
+(void)trackConfirmBookingWithRestaurant:(IMGRestaurant*)restaurant andReservation:(IMGReservation*)reservation{
    NSString *sql=[NSString stringWithFormat:@"SELECT * FROM IMGCuisine c left join IMGRestaurantCuisine rc on c.cuisineId=rc.cuisineId WHERE rc.restaurantId = %@ limit 1;",restaurant.restaurantId];
    [[DBManager manager]selectWithSql:sql successBlock:^(FMResultSet *resultSet) {
        if ([resultSet next]) {
            IMGCuisine *cuisine = [[IMGCuisine alloc]init];
            [cuisine setValueWithResultSet:resultSet];
            
            
            NSMutableDictionary *properties = [[NSMutableDictionary alloc]init];
            if (restaurant.title) {
                [properties setObject:restaurant.title forKey:@"Restaurant"];
            }
            if (restaurant.restaurantId) {
                [properties setObject:restaurant.restaurantId forKey:@"Restaurant ID"];
            }
            if (cuisine.name) {
                [properties setObject:cuisine.name forKey:@"Cuisine"];
            }
            if (cuisine.cuisineId) {
                [properties setObject:cuisine.cuisineId forKey:@"Cuisine ID"];
            }
            if (restaurant.districtName) {
                [properties setObject:restaurant.districtName forKey:@"Disrtict"];
            }
            if (restaurant.districtId) {
                [properties setObject:restaurant.districtId forKey:@"Disrtict ID"];
            }
            if (restaurant.priceLevel) {
                switch ([restaurant.priceLevel integerValue]) {
                    case 0:
                        [properties setObject:@"0" forKey:@"Price Level"];
                        break;
                    case 1:
                        [properties setObject:@"$" forKey:@"Price Level"];
                        break;
                    case 2:
                        [properties setObject:@"$$" forKey:@"Price Level"];
                        break;
                    case 3:
                        [properties setObject:@"$$$" forKey:@"Price Level"];
                        break;
                    case 4:
                        [properties setObject:@"$$$$" forKey:@"Price Level"];
                        break;
                    default:
                        break;
                }
                
            }
            if (restaurant.ratingScore) {
                [properties setObject:[NSNumber numberWithFloat:[[NSNumber numberWithFloat:[restaurant.ratingScore floatValue]] getRatingNumber]] forKey:@"Rating"];
            }
            if (restaurant.disCount) {
                [properties setObject:restaurant.disCount forKey:@"Top Discount Off"];
            }
            if (reservation.off) {
                [properties setObject:reservation.off forKey:@"Discount Off"];
            }
            if (reservation.party) {
                [properties setObject:reservation.party forKey:@"Diners"];
            }
            if (reservation.bookDate) {//[date getMonthDayString]
            
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *bookDate = [dateFormatter dateFromString:reservation.bookDate];
                [properties setObject:[bookDate getMonthDayString] forKey:@"Date"];
                [properties setObject:[bookDate getLowerWeekString] forKey:@"Week Day"];
                
            }
            if (reservation.bookTime) {
                [properties setObject:reservation.bookTime forKey:@"Time"];
            }
            if (reservation.voucherCode) {
                [properties setObject:reservation.voucherCode forKey:@"Vouchar Code"];
            }
            
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Confirm Booking" properties:properties];
            
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"ViewRestaurantDetailWithRestaurant error:%@",error.description);
    }];
}
+(void)trackBookingDoneWithRestaurant:(IMGRestaurant*)restaurant andReservation:(IMGReservation*)reservation{
    NSString *sql=[NSString stringWithFormat:@"SELECT * FROM IMGCuisine c left join IMGRestaurantCuisine rc on c.cuisineId=rc.cuisineId WHERE rc.restaurantId = %@ limit 1;",restaurant.restaurantId];
    [[DBManager manager]selectWithSql:sql successBlock:^(FMResultSet *resultSet) {
        if ([resultSet next]) {
            IMGCuisine *cuisine = [[IMGCuisine alloc]init];
            [cuisine setValueWithResultSet:resultSet];
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            NSDate *dateDone = [userDefaults objectForKey:@"dateDone"];
            NSMutableDictionary *properties = [[NSMutableDictionary alloc]init];
            if (restaurant.title) {
                [properties setObject:restaurant.title forKey:@"Restaurant"];
            }
            if (restaurant.restaurantId) {
                [properties setObject:restaurant.restaurantId forKey:@"Restaurant ID"];
            }
            if (cuisine.name) {
                [properties setObject:cuisine.name forKey:@"Cuisine"];
            }
            if (cuisine.cuisineId) {
                [properties setObject:cuisine.cuisineId forKey:@"Cuisine ID"];
            }
            if (restaurant.districtName) {
                [properties setObject:restaurant.districtName forKey:@"Disrtict"];
            }
            if (restaurant.districtId) {
                [properties setObject:restaurant.districtId forKey:@"Disrtict ID"];
            }
            if (restaurant.priceLevel) {
                switch ([restaurant.priceLevel integerValue]) {
                    case 0:
                        [properties setObject:@"0" forKey:@"Price Level"];
                        break;
                    case 1:
                        [properties setObject:@"$" forKey:@"Price Level"];
                        break;
                    case 2:
                        [properties setObject:@"$$" forKey:@"Price Level"];
                        break;
                    case 3:
                        [properties setObject:@"$$$" forKey:@"Price Level"];
                        break;
                    case 4:
                        [properties setObject:@"$$$$" forKey:@"Price Level"];
                        break;
                    default:
                        break;
                }
                
            }
            if (restaurant.ratingScore) {
                [properties setObject:[NSNumber numberWithFloat:[[NSNumber numberWithFloat:[restaurant.ratingScore floatValue]] getRatingNumber]] forKey:@"Rating"];
            }
            if (restaurant.disCount) {
                [properties setObject:restaurant.disCount forKey:@"Top Discount Off"];
            }
            if (reservation.off) {
                [properties setObject:reservation.off forKey:@"Discount Off"];
            }
            if (reservation.party) {
                [properties setObject:reservation.party forKey:@"Diners"];
            }
            if (reservation.bookDate) {//[date getMonthDayString]
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *bookDate = [dateFormatter dateFromString:reservation.bookDate];
                [properties setObject:[bookDate getMonthDayString] forKey:@"Date"];
                [properties setObject:[bookDate getLowerWeekString] forKey:@"Week Day"];
            }
            if (reservation.bookTime) {
                [properties setObject:reservation.bookTime forKey:@"Time"];
            }
            if (reservation.voucherCode) {
                [properties setObject:reservation.voucherCode forKey:@"Vouchar Code"];
            }
            if (reservation.code) {
                [properties setObject:reservation.code forKey:@"Booking Number"];
            }
            if (reservation.reservationId) {
                [properties setObject:reservation.reservationId forKey:@"Booking ID"];
            }
            if (dateDone) {
                [properties setObject:[dateDone getMMDD_hhmmFormatString] forKey:@"Operation time"];
            }
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Booking Done" properties:properties];
            
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"ViewRestaurantDetailWithRestaurant error:%@",error.description);
    }];
}
+(void)trackCancelBookingWithRestaurant:(IMGRestaurant*)restaurant andReservation:(IMGReservation*)reservation{
    NSString *sql=[NSString stringWithFormat:@"SELECT * FROM IMGCuisine c left join IMGRestaurantCuisine rc on c.cuisineId=rc.cuisineId WHERE rc.restaurantId = %@ limit 1;",restaurant.restaurantId];
    [[DBManager manager]selectWithSql:sql successBlock:^(FMResultSet *resultSet) {
        if ([resultSet next]) {
            IMGCuisine *cuisine = [[IMGCuisine alloc]init];
            [cuisine setValueWithResultSet:resultSet];
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            NSDate *dateCancel = [userDefaults objectForKey:@"dateCancel"];
            
            NSMutableDictionary *properties = [[NSMutableDictionary alloc]init];
            if (restaurant.title) {
                [properties setObject:restaurant.title forKey:@"Restaurant"];
            }
            if (restaurant.restaurantId) {
                [properties setObject:restaurant.restaurantId forKey:@"Restaurant ID"];
            }
            if (cuisine.name) {
                [properties setObject:cuisine.name forKey:@"Cuisine"];
            }
            if (cuisine.cuisineId) {
                [properties setObject:cuisine.cuisineId forKey:@"Cuisine ID"];
            }
            if (restaurant.districtName) {
                [properties setObject:restaurant.districtName forKey:@"Disrtict"];
            }
            if (restaurant.districtId) {
                [properties setObject:restaurant.districtId forKey:@"Disrtict ID"];
            }
            if (restaurant.priceLevel) {
                switch ([restaurant.priceLevel integerValue]) {
                    case 0:
                        [properties setObject:@"0" forKey:@"Price Level"];
                        break;
                    case 1:
                        [properties setObject:@"$" forKey:@"Price Level"];
                        break;
                    case 2:
                        [properties setObject:@"$$" forKey:@"Price Level"];
                        break;
                    case 3:
                        [properties setObject:@"$$$" forKey:@"Price Level"];
                        break;
                    case 4:
                        [properties setObject:@"$$$$" forKey:@"Price Level"];
                        break;
                    default:
                        break;
                }
                
            }
            if (restaurant.ratingScore) {
                [properties setObject:[NSNumber numberWithFloat:[[NSNumber numberWithFloat:[restaurant.ratingScore floatValue]] getRatingNumber]] forKey:@"Rating"];
            }
            if (restaurant.disCount) {
                [properties setObject:restaurant.disCount forKey:@"Top Discount Off"];
            }
            if (reservation.off) {
                [properties setObject:reservation.off forKey:@"Discount Off"];
            }
            if (reservation.party) {
                [properties setObject:reservation.party forKey:@"Diners"];
            }
            if (reservation.bookDate) {
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *bookDate = [dateFormatter dateFromString:reservation.bookDate];
                [properties setObject:[bookDate getMonthDayString] forKey:@"Date"];
                [properties setObject:[bookDate getLowerWeekString] forKey:@"Week Day"];
            }
            if (reservation.bookTime) {
                [properties setObject:reservation.bookTime forKey:@"Time"];
            }
            if (reservation.voucherCode) {
                [properties setObject:reservation.voucherCode forKey:@"Vouchar Code"];
            }
            if (reservation.dateCreated) {
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *dateCreated = [dateFormatter dateFromString:[reservation.dateCreated substringToIndex:10]];
                
                [properties setObject:[dateCreated getMMDD_hhmmFormatString] forKey:@"Operation Date"];
            }
            if (dateCancel) {
                [properties setObject:[dateCancel getMMDD_hhmmFormatString] forKey:@"Cancel Date"];
            }
            
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Cancel Booking" properties:properties];
            
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"ViewRestaurantDetailWithRestaurant error:%@",error.description);
    }];
}

+(void)trackUploadPhoto:(IMGRestaurant*)restaurant andDishName:(NSString *)dishName{
    NSString *sql=[NSString stringWithFormat:@"SELECT * FROM IMGCuisine c left join IMGRestaurantCuisine rc on c.cuisineId=rc.cuisineId WHERE rc.restaurantId = %@ limit 1;",restaurant.restaurantId];
    [[DBManager manager]selectWithSql:sql successBlock:^(FMResultSet *resultSet) {
        if ([resultSet next]) {
            IMGCuisine *cuisine = [[IMGCuisine alloc]init];
            [cuisine setValueWithResultSet:resultSet];
            
            NSMutableDictionary *properties = [[NSMutableDictionary alloc]init];
            if (restaurant.title) {
                [properties setObject:restaurant.title forKey:@"Restaurant"];
            }
            if (restaurant.restaurantId) {
                [properties setObject:restaurant.restaurantId forKey:@"Restaurant ID"];
            }
            if (cuisine.name) {
                [properties setObject:cuisine.name forKey:@"Cuisine"];
            }
            if (cuisine.cuisineId) {
                [properties setObject:cuisine.cuisineId forKey:@"Cuisine ID"];
            }
            if (restaurant.districtName) {
                [properties setObject:restaurant.districtName forKey:@"Disrtict"];
            }
            if (restaurant.districtId) {
                [properties setObject:restaurant.districtId forKey:@"Disrtict ID"];
            }
            if (restaurant.priceLevel) {
                [properties setObject:restaurant.priceLevel forKey:@"Price Level"];
            }
            if (restaurant.ratingScore) {
                [properties setObject:restaurant.ratingScore forKey:@"Rating"];
            }
            if (restaurant.disCount) {
                [properties setObject:restaurant.disCount forKey:@"Top Discount Off"];
            }
            
            [properties setObject:dishName forKey:@"Dish"];
            
            [properties setObject:QRAVED_WEB_SERVICE_CLIENT forKey:@"Service Client Type"];
            [properties setObject:QRAVED_WEB_SERVICE_VERSION forKey:@"Service Version"];
            
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Upload photo in user center " properties:properties];
            
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"trackUploadPhoto error:%@",error.description);
    }];
    
}

+ (void)trackOpenJournalArticleWithArticleId:(NSNumber *)articleId andArticleTitle:(NSString *)articleTitle
{
    NSMutableDictionary *properties = [[NSMutableDictionary alloc]init];
    
    [properties setObject:articleId forKey:@"Article ID"];
    [properties setObject:articleTitle forKey:@"Article Name"];
    
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"Open Article" properties:properties];

}
+ (void)trackOpenJournal:(NSString *)journalType
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:journalType properties:nil];
}
+ (void)trackOpenMyList
{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"My List" properties:nil];
    
}
+(void)trackWriteReview:(IMGRestaurant*)restaurant {
    NSString *sql=[NSString stringWithFormat:@"SELECT * FROM IMGCuisine c left join IMGRestaurantCuisine rc on c.cuisineId=rc.cuisineId WHERE rc.restaurantId = %@ limit 1;",restaurant.restaurantId];
    [[DBManager manager]selectWithSql:sql successBlock:^(FMResultSet *resultSet) {
        if ([resultSet next]) {
            IMGCuisine *cuisine = [[IMGCuisine alloc]init];
            [cuisine setValueWithResultSet:resultSet];
            
            NSMutableDictionary *properties = [[NSMutableDictionary alloc]init];
            if (restaurant.title) {
                [properties setObject:restaurant.title forKey:@"Restaurant"];
            }
            if (restaurant.restaurantId) {
                [properties setObject:restaurant.restaurantId forKey:@"Restaurant ID"];
            }
            if (cuisine.name) {
                [properties setObject:cuisine.name forKey:@"Cuisine"];
            }
            if (cuisine.cuisineId) {
                [properties setObject:cuisine.cuisineId forKey:@"Cuisine ID"];
            }
            if (restaurant.districtName) {
                [properties setObject:restaurant.districtName forKey:@"Disrtict"];
            }
            if (restaurant.districtId) {
                [properties setObject:restaurant.districtId forKey:@"Disrtict ID"];
            }
            if (restaurant.priceLevel) {
                [properties setObject:restaurant.priceLevel forKey:@"Price Level"];
            }
            if (restaurant.ratingScore) {
                [properties setObject:restaurant.ratingScore forKey:@"Rating"];
            }
            if (restaurant.disCount) {
                [properties setObject:restaurant.disCount forKey:@"Top Discount Off"];
            }
            
            [properties setObject:QRAVED_WEB_SERVICE_CLIENT forKey:@"Service Client Type"];
            [properties setObject:QRAVED_WEB_SERVICE_VERSION forKey:@"Service Version"];
            
            Mixpanel *mixpanel = [Mixpanel sharedInstance];
            [mixpanel track:@"Write reviews in user center " properties:properties];
            
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"trackUploadPhoto error:%@",error.description);
    }];
    
}
+(void)trackRedeemPoints:(IMGUser*)user;{

    
}
+(void)trackShareToTwitterWithshareObject:(NSString *)shareObject andSharePerson:(NSString *)sharePerson;{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"ShareToTwitter" properties:@{@"ShareObject": shareObject,@"sharePerson":sharePerson,@"Service Client Type":QRAVED_WEB_SERVICE_CLIENT,@"Service Version":QRAVED_WEB_SERVICE_VERSION}];
}
+(void)trackShareToFacebookWithshareObject:(NSString *)shareObject andSharePerson:(NSString *)sharePerson;{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"ShareToFacebook" properties:@{@"ShareObject": shareObject,@"sharePerson":sharePerson,@"Service Client Type":QRAVED_WEB_SERVICE_CLIENT,@"Service Version":QRAVED_WEB_SERVICE_VERSION}];
}
+(void)trackShareToEmailWithshareObject:(NSString *)shareObject andSharePerson:(NSString *)sharePerson;{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"ShareToEmail" properties:@{@"ShareObject": shareObject,@"sharePerson":sharePerson,@"Service Client Type":QRAVED_WEB_SERVICE_CLIENT,@"Service Version":QRAVED_WEB_SERVICE_VERSION}];
}
+(void)trackShareToSMSWithshareObject:(NSString *)shareObject andSharePerson:(NSString *)sharePerson;{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"ShareToSMS" properties:@{@"ShareObject": shareObject,@"sharePerson":sharePerson,@"Service Client Type":QRAVED_WEB_SERVICE_CLIENT,@"Service Version":QRAVED_WEB_SERVICE_VERSION}];
}
+(void)trackShareToPathWithshareObject:(NSString *)shareObject andSharePerson:(NSString *)sharePerson;{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"ShareToPath" properties:@{@"ShareObject": shareObject,@"sharePerson":sharePerson,@"Service Client Type":QRAVED_WEB_SERVICE_CLIENT,@"Service Version":QRAVED_WEB_SERVICE_VERSION}];
}

+(void)trackViewDiningGuideWithDiningGuideName:(NSString*)diningGuideName{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"open_dining_guide" properties:@{@"dining_guide_name": diningGuideName,@"Service Client Type":QRAVED_WEB_SERVICE_CLIENT,@"Service Version":QRAVED_WEB_SERVICE_VERSION}];
}
+(void)trackViewEventWithEventName:(NSString*)eventName;{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"open_event" properties:@{@"event_name": eventName,@"Service Client Type":QRAVED_WEB_SERVICE_CLIENT,@"Service Version":QRAVED_WEB_SERVICE_VERSION}];

}
+(void)trackViewDiscoverWithDiscoverName:(NSString*)discoverName;{
    Mixpanel *mixpanel = [Mixpanel sharedInstance];
    [mixpanel track:@"open_discover" properties:@{@"discover_name": discoverName,@"Service Client Type":QRAVED_WEB_SERVICE_CLIENT,@"Service Version":QRAVED_WEB_SERVICE_VERSION}];
    
}






@end
