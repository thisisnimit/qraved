//
//  GoFoodButton.h
//  Qraved
//
//  Created by harry on 2018/2/26.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FunctionButton : UIButton


+ (instancetype)buttonWithType:(UIButtonType)buttonType andTitle:(NSString *)title andBGColor:(UIColor *)bgColor andTitleColor:(UIColor *)titleColor;

+ (instancetype)signButtonWithType:(UIButtonType)buttonType andTitle:(NSString *)title andBGColor:(UIColor *)bgColor andTitleColor:(UIColor *)titleColor;


@end
