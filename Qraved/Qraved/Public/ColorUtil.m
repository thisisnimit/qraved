//
//  ColorUtil.m
//  Qraved
//
//  Created by Jeff on 2/12/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "ColorUtil.h"

@implementation ColorUtil

+( UIColor *) getColor:( NSString *)hexColor
{
    
    unsigned int red, green, blue;
    
    NSRange range;
    
    range. length = 2 ;
    
    range. location = 1 ;
    
    [[ NSScanner scannerWithString :[hexColor substringWithRange :range]] scanHexInt :&red];
    
    range. location = 3 ;
    
    [[ NSScanner scannerWithString :[hexColor substringWithRange :range]] scanHexInt :&green];
    
    range. location = 5 ;
    
    [[ NSScanner scannerWithString :[hexColor substringWithRange :range]] scanHexInt :&blue];
    
    return [ UIColor colorWithRed :( float )(red/ 255.0f ) green :( float )(green/ 255.0f ) blue :( float )(blue/ 255.0f ) alpha : 1.0f ];
    
}
@end
