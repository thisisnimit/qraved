// SongAPIClient.m
//
// Copyright (c) 2012 Mattt Thompson (http://mattt.me)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
#import "QravedAPIClient.h"
//static NSString * const kAFAppMyneAPIBaseURLString = @"http://imaginato.dnsdojo.com:6055/";
//static NSString * const kAFAppMyneAPIBaseURLString = @"http://192.168.1.9:7055/";

@implementation QravedAPIClient

+ (QravedAPIClient *)sharedClient {
    static QravedAPIClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:QRAVED_WEB_SERVICE_SERVER]];
    });
    
    return _sharedClient;
    
}

- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
    [self setDefaultHeader:@"Accept" value:@"application/json"];
    
    return self;
}

#pragma mark - AFIncrementalStore

- (NSURLRequest *)requestForFetchRequest:(NSFetchRequest *)fetchRequest
                             withContext:(NSManagedObjectContext *)context
{
    NSMutableURLRequest *mutableURLRequest = nil;
    NSDictionary *parameters=[FetchPrameter sharedParameter].paramters;

    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:parameters];
    [dict setObject:@"2" forKey:@"client"];
    [dict setObject:Verson forKey:@"v"];
    mutableURLRequest = [self requestWithMethod:@"GET" path:[[PathPrameter sharedParameter].paramters objectForKey:@"path"] parameters:dict];
    
    return mutableURLRequest;
}

- (id)representationOrArrayOfRepresentationsFromResponseObject:(id)responseObject {
    if([[[PathPrameter sharedParameter].paramters objectForKey:@"path"] isEqualToString:PATH_NOTIFICATION_LIST]){
        id jsonObject =  [responseObject valueForKey:@"notificationList"];
        return jsonObject;
    }else
    {
        id jsonObject =  [responseObject valueForKey:@"restaurantList"];
        return jsonObject;
    }
}

- (NSDictionary *)attributesForRepresentation:(NSDictionary *)representation
                                     ofEntity:(NSEntityDescription *)entity 
                                 fromResponse:(NSHTTPURLResponse *)response 
{
    NSMutableDictionary *mutablePropertyValues = [[super attributesForRepresentation:representation ofEntity:entity fromResponse:response] mutableCopy];
    if ([entity.name isEqualToString:@"Restaurant"]) {
//        NSLog(@"ss=%@",representation);
        [mutablePropertyValues setValue:[NSNumber numberWithInteger:[[representation valueForKey:@"id"] integerValue]] forKey:@"restaurantID"];
        [mutablePropertyValues setValue:[[representation valueForKey:@"title"] stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&" ] forKey:@"title"];
        [mutablePropertyValues setValue:[representation valueForKey:@"displayTime"] forKey:@"displayTime"];
        [mutablePropertyValues setValue:[representation valueForKey:@"ratingScore"] forKey:@"ratingScore"];
        
        [mutablePropertyValues setValue:[representation valueForKey:@"eventLogo"] forKey:@"eventLogo"];
        
        [mutablePropertyValues setValue:[representation valueForKey:@"rating"] forKey:@"rating"];
        [mutablePropertyValues setValue:[representation valueForKey:@"distance"] forKey:@"distance"];
        [mutablePropertyValues setValue:[representation valueForKey:@"reviewCount"] forKey:@"reviewCount"];
        [mutablePropertyValues setValue:[representation valueForKey:@"dishCount"] forKey:@"qravesCount"];
        [mutablePropertyValues setValue:[representation valueForKey:@"score"] forKey:@"score"];
        [mutablePropertyValues setValue:[representation valueForKey:@"phoneNumber"] forKey:@"phoneNumber"];
        [mutablePropertyValues setValue:[representation valueForKey:@"discount"] forKey:@"discount"];
        //[mutablePropertyValues setValue:[representation valueForKey:@"location"] forKey:@"location"];
        NSString *urlStr = [representation valueForKey:@"imageUrl"];
        //urlStr=[urlStr stringByReplacingOccurrencesOfString:@".jpg" withString:@"-100x100.jpg"];
//        urlStr=[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:urlStr];
        [mutablePropertyValues setValue:urlStr forKey:@"imageUrl"];
//       NSURL  *imageUrl =[NSURL URLWithString:[ImageURLHead stringByAppendingString:urlStr]];
    }
    else if([entity.name isEqualToString:@"Notification"])
    {
        [mutablePropertyValues setValue:[representation valueForKey:@"id"] forKey:@"notificationID"];
        [mutablePropertyValues setValue:[representation valueForKey:@"message"] forKey:@"message"];
        [mutablePropertyValues setValue:[representation valueForKey:@"userId"] forKey:@"userID"];
        [mutablePropertyValues setValue:[representation valueForKey:@"lastUpdatedStr"] forKey:@"lastUpdated"];
        [mutablePropertyValues setValue:[representation valueForKey:@"pushedStatus"] forKey:@"pushedStatus"];
        [mutablePropertyValues setValue:[representation valueForKey:@"readStatus"] forKey:@"readStatus"];
        [mutablePropertyValues setValue:[representation valueForKey:@"imageUrl"] forKey:@"imageUrl"];
        
    }
    else if([entity.name isEqualToString:@"Cuisine"])
    {
        [mutablePropertyValues setValue:[representation valueForKey:@"id"] forKey:@"cuisineID"];
        [mutablePropertyValues setValue:[representation valueForKey:@"name"] forKey:@"name"];
    }
    else if([entity.name isEqualToString:@"Location"])
    {
        [mutablePropertyValues  setValue:[representation valueForKey:@"address1"] forKey:@"address1"];
        [mutablePropertyValues setValue:[representation valueForKey:@"address2"] forKey:@"address2"];
        [mutablePropertyValues setValue:[representation valueForKey:@"latitude"] forKey:@"latitude"];
        [mutablePropertyValues setValue:[representation valueForKey:@"longitude"] forKey:@"longitude"];
    }
    else if([entity.name isEqualToString:@"District"])
    {
        //NSLog(@"ss=%@",representation);
        [mutablePropertyValues setValue:[representation valueForKey:@"name"] forKey:@"name"];
        [mutablePropertyValues setValue:[representation valueForKey:@"latitude"] forKey:@"latitude"];
        [mutablePropertyValues setValue:[representation valueForKey:@"longitude"] forKey:@"longitude"];
    }

    return mutablePropertyValues;
}

- (BOOL)shouldFetchRemoteAttributeValuesForObjectWithID:(NSManagedObjectID *)objectID
                                 inManagedObjectContext:(NSManagedObjectContext *)context
{
    return NO;
}

- (BOOL)shouldFetchRemoteValuesForRelationship:(NSRelationshipDescription *)relationship
                               forObjectWithID:(NSManagedObjectID *)objectID
                        inManagedObjectContext:(NSManagedObjectContext *)context
{
    return NO;
}

@end
