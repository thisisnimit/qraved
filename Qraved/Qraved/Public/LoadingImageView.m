//
//  LoadingImageView.m
//  Qraved
//
//  Created by Laura on 14/12/23.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "LoadingImageView.h"
#import "UIImageView+WebCache.h"
#import "UIConstants.h"
#import "UIImage+Helper.h"
#import "UIDevice+Util.h"
#import "AppDelegate.h"
@implementation LoadingImageView
UIActivityIndicatorView *_activityView;

-(void)startLoading {
    [_activityView startAnimating];
}

-(void)stopLoading {
    [_activityView stopAnimating];
    [self removeFromSuperview];
}

- (id)initWithFrame:(CGRect)frame andViewNum:(NSInteger)num andShadowWidth:(CGFloat)width
{
    self = [super init];
    if (self) {
        // Initialization code
        self.frame = frame;
        [self addLoadingImageViewWithframe:frame andViewNum:num andShadowWidth:width];
    }
    return self;
}
-(void)addLoadingImageViewWithframe:(CGRect)frame andViewNum:(NSInteger)num andShadowWidth:(CGFloat)width{

    CGFloat imageHeight = (frame.size.height-(num *width))/num;
    UIImage *image=[UIImage imageWithCGImage:CGImageCreateWithImageInRect([[UIImage imageNamed:DEFAULT_IMAGE_STRING2] CGImage], CGRectMake(0, 1136/2-imageHeight, 640, imageHeight*2*[AppDelegate ShareApp].autoSizeScaleY))];
    for (int i=0; i<num; i++) {
        //            self.backgroundColor = [UIColor whiteColor];
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, (imageHeight + width)*i, frame.size.width, imageHeight*[AppDelegate ShareApp].autoSizeScaleY)];
        
        imageView.image =  image;
        [self addSubview:imageView];
        
        UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activityView.frame = imageView.bounds;
        [imageView addSubview:activityView];
        [activityView startAnimating];
    }
}




@end
