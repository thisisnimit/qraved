//
//  Date.m
//  Qraved
//
//  Created by Shine Wang on 6/8/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "Date.h"
#import "NSDate+Helper.h"

@implementation Date

+(NSString *)date
{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *date=[dateFormatter stringFromDate:[NSDate date]];
    return date;
}

+(NSString *)weekDay
{
    NSCalendar *calendar=[[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDate *now=[NSDate date];

    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;

    NSDateComponents *comps = [calendar components:unitFlags fromDate:now];
    
    return [NSString stringWithFormat:@"%ld",[comps weekday]-1];
}

+(NSString *)getJakartaTime:(NSString *)dateStr
{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date1 = [dateFormatter1 dateFromString:dateStr];
    //    NSLog(@"date : %@",date1);
    
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"WIT"];
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:date1];
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:date1];
    NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
    
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:date1];
    return [destinationDate getYYYY_MM_DD_hh_mmssFormatString];
    
}

+(NSString *)getTimeInteval:(NSString *)dateStr
{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    NSDate *date1 = [dateFormatter1 dateFromString:dateStr];
    if (date1==nil)
    {
        NSArray *ymdArr = [dateStr componentsSeparatedByString:@"T"];
        NSArray *hmsArr = [[ymdArr objectAtIndex:1] componentsSeparatedByString:@"Z"];
        NSMutableString *strM = [NSMutableString stringWithFormat:@"%@ %@",[ymdArr objectAtIndex:0],[hmsArr objectAtIndex:0]];
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                                      //@"2015-04-27T10:13:24Z"
        date1 = [dateFormatter1 dateFromString:strM];
    }
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"WIT"];
    
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:date1];
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:date1];
    NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
    
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:date1] ;
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"MM/dd/yyyy HH:mm:ss"];

    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];

    
    NSTimeInterval seconds=[[NSDate date] timeIntervalSinceDate:destinationDate];
    
    
//    NSLog(@"相隔时间为%d",CharDays);
    
//    NSLog(@"charDays is %d",CharDays);
    
    
    int days = seconds / (24 * 60 * 60);
    
    if(days!=0)
    {
        if(days==1)
        {
            return [NSString stringWithFormat:@"%d day ago",days];
        }
        if(days<=365)
        {
            return [NSString stringWithFormat:@"%d days ago",days];
        }
        else
        {
            int years=days/365;
            if(years==1)
            {
                return [NSString stringWithFormat:@"%d year ago",years];
            }
            else
            {
                return [NSString stringWithFormat:@"%d years ago",years];
            }

        }
    }
    
    else
    {
        int hours = seconds/(60*60);
        
        if(hours!=0)
        {
            if(hours==1)
            {
                return [NSString stringWithFormat:@"%d hour ago",abs(hours)];
            }
            
            return [NSString stringWithFormat:@"%d hours ago",abs(hours)];
        }
        
        int minutes=seconds/60;
        
        if(minutes!=0)
        {
            if(minutes==1)
            {
                return [NSString stringWithFormat:@"%d minute ago",abs(minutes)];
            }
            
            return [NSString stringWithFormat:@"%d minutes ago",abs(minutes)];
        }
        
        if(seconds<=1)
        {
             return [NSString stringWithFormat:@"%d second ago",(int)fabs(seconds)];
        }
        return [NSString stringWithFormat:@"%d seconds ago",(int)fabs(seconds)];
    }
 

}

+(NSString *)getTimeInteval_v2:(NSString *)dateStr
{
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    NSDate *date1 = [dateFormatter1 dateFromString:dateStr];
    if (date1==nil)
    {
        NSArray *ymdArr = [dateStr componentsSeparatedByString:@"T"];
        NSArray *hmsArr = [[ymdArr objectAtIndex:1] componentsSeparatedByString:@"Z"];
        NSMutableString *strM = [NSMutableString stringWithFormat:@"%@ %@",[ymdArr objectAtIndex:0],[hmsArr objectAtIndex:0]];
        [dateFormatter1 setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                                      //@"2015-04-27T10:13:24Z"
        date1 = [dateFormatter1 dateFromString:strM];
    }
    NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
    NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    
    
    NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:date1];
    NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:date1];
    NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
    
    NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:date1] ;
    
    NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
    [dateFormatters setDateFormat:@"MM/dd/yyyy HH:mm:ss"];

    [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];

    
    NSTimeInterval seconds=[[NSDate date] timeIntervalSinceDate:destinationDate];
    
    
//    NSLog(@"相隔时间为%d",CharDays);
    
//    NSLog(@"charDays is %d",CharDays);
    
    
    int days = seconds / (24 * 60 * 60);
    
    if(days!=0)
    {
        if(days==1)
        {
            return [NSString stringWithFormat:@"%d day ago",days];
        }
        if(days<=365)
        {
            return [NSString stringWithFormat:@"%d days ago",days];
        }
        else
        {
            int years=days/365;
            if(years==1)
            {
                return [NSString stringWithFormat:@"%d year ago",years];
            }
            else
            {
                return [NSString stringWithFormat:@"%d years ago",years];
            }

        }
    }
    
    else
    {
        int hours = seconds/(60*60);
        
        if(hours!=0)
        {
            if(hours==1)
            {
                return [NSString stringWithFormat:@"%d hour ago",abs(hours)];
            }
            
            return [NSString stringWithFormat:@"%d hours ago",abs(hours)];
        }
        
        int minutes=seconds/60;
        
        if(minutes!=0)
        {
            if(minutes==1)
            {
                return [NSString stringWithFormat:@"%d minute ago",abs(minutes)];
            }
            
            return [NSString stringWithFormat:@"%d minutes ago",abs(minutes)];
        }
        
        if(seconds<=1)
        {
             return [NSString stringWithFormat:@"%d second ago",(int)fabs(seconds)];
        }
        return [NSString stringWithFormat:@"%d seconds ago",(int)fabs(seconds)];
    }
 

}

+(NSString *)getTimeInteval_v3:(double)timestamp{

    NSString *timeStr = [NSString stringWithFormat:@"%.f",timestamp];
    NSInteger digitNum = timeStr.length;
    if(digitNum>10){
        NSString *subString = [[NSString stringWithFormat:@"%.f",timestamp] substringWithRange:NSMakeRange(0, 10)];
        timestamp = [subString doubleValue];
    }
    double seconds=[NSDate date].timeIntervalSince1970-timestamp;
    
    
//    NSLog(@"相隔时间为%d",CharDays);
    
//    NSLog(@"charDays is %d",CharDays);
    
    
    int days = seconds / (24 * 60 * 60);
    
    if(days!=0)
    {
        if(days==1)
        {
            return [NSString stringWithFormat:@"%d day ago",days];
        }
        if(days<=365)
        {
            return [NSString stringWithFormat:@"%d days ago",days];
        }
        else
        {
            int years=days/365;
            if(years==1)
            {
                return [NSString stringWithFormat:@"%d year ago",years];
            }
            else
            {
                return [NSString stringWithFormat:@"%d years ago",years];
            }

        }
    }
    
    else
    {
        int hours = seconds/(60*60);
        
        if(hours!=0)
        {
            if(hours==1)
            {
                return [NSString stringWithFormat:@"%d hour ago",abs(hours)];
            }
            
            return [NSString stringWithFormat:@"%d hours ago",abs(hours)];
        }
        
        int minutes=seconds/60;
        
        if(minutes!=0)
        {
            if(minutes==1)
            {
                return [NSString stringWithFormat:@"%d minute ago",abs(minutes)];
            }
            
            return [NSString stringWithFormat:@"%d minutes ago",abs(minutes)];
        }
        
        if(seconds<=1)
        {
             return [NSString stringWithFormat:@"%d second ago",abs((int)seconds)];
        }
        return [NSString stringWithFormat:@"%d seconds ago",abs((int)seconds)];
    }
 

}

+(NSString *)getJournalTime:(double)timestamp{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    NSDate *now = [NSDate date];
    double seconds=now.timeIntervalSince1970-timestamp;
    NSDate *date=[[NSDate alloc]initWithTimeIntervalSince1970:timestamp];
    
    if(seconds<6*60){
        return L(@"Now");
    }else if(seconds<60*60){
        return [NSString stringWithFormat:L(@"%d minutes ago"),(int)seconds/60];
    }else if(seconds<2*60*60){
        return [NSString stringWithFormat:L(@"1 hour ago")];
    }else if(seconds<24*60*60){
        return [NSString stringWithFormat:L(@"%d hours ago"),(int)seconds/(60*60)];
    }else if(seconds<48*60*60){
        [dateFormatter setDateFormat:@"dd"];
        NSString *dateDD = [dateFormatter stringFromDate:date];
        NSDate *yesterday = [[NSDate alloc]initWithTimeInterval:-24*60*60 sinceDate:now];
        NSString *yesterdayDD = [dateFormatter stringFromDate:yesterday];
        if([dateDD isEqualToString:yesterdayDD]){
            [dateFormatter setDateFormat:@"HH:mm"];
            return [NSString stringWithFormat:L(@"Yesterday")];
        }
    }
    
    [dateFormatter setDateFormat:@"MMMM dd, yyyy"];
    return [dateFormatter stringFromDate:date];
}

+(NSString *)getTimeInteval_v4:(double)timestamp{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    NSDate *now = [NSDate date];
    double seconds=now.timeIntervalSince1970-timestamp;
    NSDate *date=[[NSDate alloc]initWithTimeIntervalSince1970:timestamp];
    
    if(seconds<6*60){
        return L(@"Now");
    }else if(seconds<60*60){
        return [NSString stringWithFormat:L(@"%d minutes ago"),(int)seconds/60];
    }else if(seconds<2*60*60){
        return [NSString stringWithFormat:L(@"1 hour ago")];
    }else if(seconds<24*60*60){
        return [NSString stringWithFormat:L(@"%d hours ago"),(int)seconds/(60*60)];
    }else if(seconds<48*60*60){
        [dateFormatter setDateFormat:@"dd"];
        NSString *dateDD = [dateFormatter stringFromDate:date];
        NSDate *yesterday = [[NSDate alloc]initWithTimeInterval:-24*60*60 sinceDate:now];
        NSString *yesterdayDD = [dateFormatter stringFromDate:yesterday];
        if([dateDD isEqualToString:yesterdayDD]){
            [dateFormatter setDateFormat:@"HH:mm"];
            return [NSString stringWithFormat:L(@"Yesterday, at %@"),[dateFormatter stringFromDate:date]];
        }
    }
    
    [dateFormatter setDateFormat:@"MMMM dd, yyyy 'at' HH:mm"];
    return [dateFormatter stringFromDate:date];
}

+(NSString *)getTimeInteval_v10:(double)timestamp{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    NSDate *now = [NSDate date];
    double seconds=now.timeIntervalSince1970-timestamp;
    NSDate *date=[[NSDate alloc]initWithTimeIntervalSince1970:timestamp];
    
    if(seconds<6*60){
        return L(@"Now");
    }else if(seconds<60*60){
        return [NSString stringWithFormat:L(@"%d minutes ago"),(int)seconds/60];
    }else if(seconds<2*60*60){
        return [NSString stringWithFormat:L(@"1 hour ago")];
    }else if(seconds<24*60*60){
        return [NSString stringWithFormat:L(@"%d hours ago"),(int)seconds/(60*60)];
    }else if(seconds<48*60*60){
        [dateFormatter setDateFormat:@"dd"];
        NSString *dateDD = [dateFormatter stringFromDate:date];
        NSDate *yesterday = [[NSDate alloc]initWithTimeInterval:-24*60*60 sinceDate:now];
        NSString *yesterdayDD = [dateFormatter stringFromDate:yesterday];
        if([dateDD isEqualToString:yesterdayDD]){
            [dateFormatter setDateFormat:@"HH:mm"];
            return [NSString stringWithFormat:L(@"Yesterday, at %@"),[dateFormatter stringFromDate:date]];
        }
    }
    
    [dateFormatter setDateFormat:@"MMMM dd, yyyy"];
    return [dateFormatter stringFromDate:date];
}

+(NSString *)getTimeInteval_v5:(double)timestamp{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    NSDate *now = [NSDate date];
    double seconds=now.timeIntervalSince1970-timestamp;
    NSDate *date=[[NSDate alloc]initWithTimeIntervalSince1970:timestamp];
    
    if(seconds<6*60){
        return L(@"Now");
    }else if(seconds<60*60){
        return [NSString stringWithFormat:L(@"%d minutes ago"),(int)seconds/60];
    }else if(seconds<2*60*60){
        return [NSString stringWithFormat:L(@"1 hour ago")];
    }else if(seconds<24*60*60){
        return [NSString stringWithFormat:L(@"%d hours ago"),(int)seconds/(60*60)];
    }else if(seconds<48*60*60){
        [dateFormatter setDateFormat:@"dd"];
        NSString *dateDD = [dateFormatter stringFromDate:date];
        NSDate *yesterday = [[NSDate alloc]initWithTimeInterval:-24*60*60 sinceDate:now];
        NSString *yesterdayDD = [dateFormatter stringFromDate:yesterday];
        if([dateDD isEqualToString:yesterdayDD]){
            [dateFormatter setDateFormat:@"HH:mm"];
            return [NSString stringWithFormat:L(@"Yesterday")];
        }
    }
    
    [dateFormatter setDateFormat:@"MMMM dd, yyyy"];
    return [dateFormatter stringFromDate:date];
}

+ (NSString *)ConvertStrToTime:(NSString *)timeStr

{
    
    long long time=[timeStr longLongValue];
    
    NSDate *d = [[NSDate alloc]initWithTimeIntervalSince1970:time/1000.0];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    
    [formatter setDateFormat:@"MMM dd yyyy"];
    
    NSString*timeString=[formatter stringFromDate:d];
    
    return timeString;
    
}

+ (NSString *)dateFormatForCoupon:(NSNumber *)dateNum{
    long long time=[dateNum longLongValue];
    
    NSDate *d = [[NSDate alloc]initWithTimeIntervalSince1970:time/1000.0];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    
    [formatter setDateFormat:@"yyyy/MM/dd HH:mm"];
    
    NSString*timeString=[formatter stringFromDate:d];
    return timeString;
}


+ (NSString *)dateFormart:(NSString *)dateString{
    NSDateFormatter *dateFormat1 = [ [NSDateFormatter alloc] init];
    [dateFormat1 setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormat1 dateFromString:dateString];
    NSDateFormatter *dateFormat2 = [ [NSDateFormatter alloc] init];
    [dateFormat2 setDateFormat:@"MMM dd yyyy"];
    NSString *string = [dateFormat2 stringFromDate: date];
    return string;
}

+ (NSString *)timeFormart:(NSString *)timeString{
    NSDateFormatter *dateFormat4 = [ [NSDateFormatter alloc] init];
    [dateFormat4 setDateFormat:@"HH:mm"];
    NSDate *date1 = [dateFormat4 dateFromString:timeString];
    NSDateFormatter *dateFormat3 = [ [NSDateFormatter alloc] init];
    [dateFormat3 setDateFormat:@"hh:mm a"];
//    dateFormat3.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    NSString  *string =  [dateFormat3 stringFromDate: date1];
    
    return string;
    
}
+ (NSString *)dateTimeChangeDateFormart:(NSString *)dateAndTime{
    
    NSDateFormatter *dateFormat1 = [ [NSDateFormatter alloc] init];
    [dateFormat1 setDateFormat:@"MM/dd/yyyy HH:mm:ss"];
    NSDate *date = [dateFormat1 dateFromString:dateAndTime];
    NSDateFormatter *dateFormat2 = [ [NSDateFormatter alloc] init];
    [dateFormat2 setDateFormat:@"MMM dd yyyy"];
    NSString *string = [dateFormat2 stringFromDate: date];
    return string;
}

+ (int)compareCurrentTimeWithTime:(NSNumber *)dateNum{
    //currentDate
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSString *dateTime=[formatter stringFromDate:[NSDate date]];
    NSDate *date = [formatter dateFromString:dateTime];
    
    long long time=[dateNum longLongValue];
    NSDate *d = [[NSDate alloc]initWithTimeIntervalSince1970:time/1000.0];

    NSString*timeString=[formatter stringFromDate:d];
    NSDate *compareDate = [formatter dateFromString:timeString];
    NSComparisonResult result = [compareDate compare:date];
    NSLog(@"%@----%@",dateTime,timeString);
    if (result == NSOrderedDescending) {
        return 1;
    }else if (result == NSOrderedAscending){
        return -1;
    }
    return 0;
}

@end
