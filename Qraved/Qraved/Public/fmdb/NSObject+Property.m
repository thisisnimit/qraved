//
//  NSObject+Property.m
//   
//
//  Created on 12-12-15.
//
//

#import "NSObject+Property.h"

@implementation NSObject (Property)

 
- (NSArray *)getPropertyList{
    return [self getPropertyList:[self class]];
}

- (NSArray *)getPropertyList: (Class)clazz
{
    u_int count;
    objc_property_t *properties  = class_copyPropertyList(clazz, &count);
    NSMutableArray *propertyArray = [NSMutableArray arrayWithCapacity:count];
    
    for (int i = 0; i < count ; i++)
    {
        const char* propertyName = property_getName(properties[i]);
        [propertyArray addObject: [NSString  stringWithUTF8String: propertyName]];
    }
    
    free(properties);
    
    return propertyArray;
}
- (objc_property_t *)getObjcPropertyList: (Class)clazz
{
    u_int count;

    objc_property_t * properties  = class_copyPropertyList(clazz, &count);

    return properties;
}
- (NSString *)tableSql:(NSString *)tablename{

    NSMutableString *sql = [[NSMutableString alloc] init];
    u_int count;
    objc_property_t * properties  = class_copyPropertyList([self class], &count);

    [sql appendFormat:@"create table %@ (",tablename] ;
    for (int i=0; i<count; i++) {
        if (i>0) {
            [sql appendString:@","];
        }
        u_int outCount;
        objc_property_attribute_t * attribute= property_copyAttributeList(properties[i],&outCount);

       // const char * attributeName = attribute->name;
        const char * attributeValue =attribute->value;
        const char * propertyName = property_getName(properties[i]);
      //  const char * arrributes=property_getAttributes(properties[i]);

        if(strcmp(attributeValue,"i")==0){
            [sql appendFormat:@"%s int",propertyName];
        }else if(strcmp(attributeValue, "f")==0){
            [sql appendFormat:@"%s double",propertyName];
        }else if(strcmp(attributeValue, "@\"NSString\"")==0){
            [sql appendFormat:@"%s text",propertyName];
        }else if (strcmp(attributeValue, "@\"NSNumber\"")==0){
            [sql appendFormat:@"%s text",propertyName];
        }
    }
    [sql appendString:@")"];
    return sql;
}
- (NSString *)tableSql{
    return [self tableSql:[self className]];
}
 

- (NSDictionary *)convertDictionary{
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSArray *propertyList = [self getPropertyList];
    for (NSString *key in propertyList) {
        SEL selector = NSSelectorFromString(key);
        
        #pragma clang diagnostic push
        #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            id value = [self performSelector:selector];
        #pragma clang diagnostic pop
        
        if (value == nil) {
            value = [NSNull null];
        }
        [dict setObject:value forKey:key];
    }
    return dict;
}
- (id)initWithDictionary:(NSDictionary *)dict{
    self = [self init];
    if(self)
        [self dictionaryForObject:dict];
    return self;
    
}
- (NSString *)className{
    return [NSString stringWithUTF8String:object_getClassName(self)];
}

- (BOOL)checkPropertyName:(NSString *)name {
    unsigned int propCount, i;
    objc_property_t * properties = class_copyPropertyList([self class], &propCount);
    for (i = 0; i < propCount; i++) {
        objc_property_t prop = properties[i];
        const char *propName = property_getName(prop);
        if(propName) {
            NSString *_name = [NSString stringWithCString:propName encoding:NSUTF8StringEncoding];
            if ([name isEqualToString:_name]) {
                return YES;
            }
        }
    }
    return NO;
}


- (void)dictionaryForObject:(NSDictionary*) dict{
    for (NSString *key in [dict allKeys]) {
        id value = [dict objectForKey:key];
        
        if (value==[NSNull null]) {
            continue;
        }
        if ([value isKindOfClass:[NSDictionary class]]) {
            id subObj = [self valueForKey:key];
            if (subObj)
                [subObj dictionaryForObject:value];
        }
        else{
             [self setValue:value forKeyPath:key];
        }
    }
}

@end
