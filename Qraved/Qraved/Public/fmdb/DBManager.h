//
//  CityManager.h
//  FMDB_AFNETWORKING
//
//  Created by Olaf on 14-9-9.
//  Copyright (c) 2014年 Olaf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>
#import "NSObject+Property.h"

#import "FMDataBase.h"
#import "FMDatabaseQueue.h"


#import "IMGEntity.h"


#define DDBLog(vags) NSLog(vags);

typedef NSString IMGSQL;

@interface DBManager : NSObject


@property (nonatomic,retain,readonly) FMDatabaseQueue * dbQueue;
@property (nonatomic,retain,readonly) FMDatabase * fmdb;

@property (nonatomic,copy,readonly) NSString * basePath;

//creator
+(instancetype)manager;
-(instancetype)initWithBasePath;
//-(instancetype)initWithBasePath:(NSString *)basePath;
-(NSObject *)loadModelWithId:(NSNumber *)modelId class:(Class)clazz;
-(IMGEntity *)loadObjectWithKey:(NSString *)key andValue:(NSValue *)value forClassName:(NSString *)className;
-(NSArray *)getObjectWithKey:(NSString *)key andValue:(NSValue *)value forClassName:(NSString *)className;
//init operation
-(BOOL)openDataBase;

//base operation

//判断数据库是否为空
-(BOOL)SQLNullWithClass:(Class)classs;
-(BOOL)isExistsTable:(NSString *)tablename;
//创建表
-(void)createTableWithSql:(IMGSQL *)sql;
-(void)createTableWithModel:(id<IMGModelProtrol>)model;
-(void)createTableWithClass:(Class<IMGModelProtrol>)model;
//插入语句
-(void)insertWithSql:(IMGSQL *)sql;
-(void)insertModel:(IMGEntity *)model;
-(void)insertModel:(IMGEntity *)model selectField:(NSString*)selectField andSelectID:(NSNumber*)selectID;
-(void)insertModel:(IMGEntity *)model update:(BOOL) update selectKey:(NSString * )selectField andSelectValue:(NSObject *)selectID;
//删除语句
-(void)deleteWithSql:(IMGSQL *)sql;
-(void)deleteWithModel:(id<IMGModelProtrol>)model;
//查找
-(void)selectWithSql:(IMGSQL *)sql successBlock:(void (^)(FMResultSet * resultSet)) success failureBlock:(void(^)(NSError *error)) failure;
-(void)selectWithClass:(Class)clazz successBlock:(void (^)(FMResultSet * resultSet))success failureBlock:(void (^)(NSError * error))failure;
-(void)selectArrayWithClass:(Class)clazz successBlock:(void (^)(NSArray * array))success failureBlock:(void (^)(NSError * error))failure;
//查找Journal
-(void)selectModel:(IMGEntity *)model selectField:(NSString*)selectField andSelectValue:(NSNumber *)selectString andBlock:(void (^)(NSString *contentStr))block;
//插入关系表
-(void)insertRelation:(id)model selectField:(NSString*)selectField andSelectID:(NSNumber*)selectID relationField:(NSString*)relationField andRelationId:(NSNumber*)relationId;

-(void)updateModel:(IMGEntity * )model params:(NSDictionary *)params;

-(id)setValueWithResult:(FMResultSet *)result forClass:(Class)clazz;

-(BOOL)dropExistsTable:(NSString*)tableName;
-(FMResultSet *)executeQuery:(NSString*)sql;

-(void)insertRelation:(id)model selectField:(NSString*)selectField andSelectID:(NSNumber*)selectID relationField:(NSString*)relationField andRelationId:(NSNumber*)relationId  andFMDatabase:(FMDatabase *)fmdb;
-(void)insertModel:(IMGEntity *)model selectField:(NSString*)selectField andSelectID:(NSNumber*)selectID andFMDatabase:(FMDatabase *)fmdb;
-(void)updateModel:(IMGEntity * )model params:(NSDictionary *)params andFMDatabase:(FMDatabase *)fmdb;
-(void)insertModel:(IMGEntity *)model andFMDatabase:(FMDatabase *)fmdb;


@end
