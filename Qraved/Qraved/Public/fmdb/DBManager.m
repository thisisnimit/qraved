//
//  CityManager.m
//  FMDB_AFNETWORKING
//
//  Created by Olaf on 14-9-9.
//  Copyright (c) 2014年 Olaf. All rights reserved.
//

#import "DBManager.h"
#import "FMDatabase.h"
#import "Consts.h"

#define ERROR_DOMAIN @"ERROR.MAIN"




@interface DBManager ()

-(void)openDataBaseFailure;
-(void)initAllTables;
@end

@implementation DBManager

+(instancetype)manager{
    static DBManager * manager = nil;
    static dispatch_once_t one;
    dispatch_once(&one, ^{
        manager=[[DBManager alloc]initWithBasePath];
    });
    return  manager;
}

//manager初始化函数
-(instancetype)initWithBasePath{
    if(self=[super init]){
        NSLog(@"%@qraved.sqlite original filePath():%@",QRAVED_ENVIROMENT,qravedDefaultDataBaseFilePath);
        
        NSLog(@"%@qraved.sqlite filePath():%@",QRAVED_ENVIROMENT,qravedDataBaseFilePath);
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL success = [fileManager fileExistsAtPath:qravedDataBaseFilePath];
        if (!success){
            //copy
            NSError *error;
            success = [fileManager copyItemAtPath:qravedDefaultDataBaseFilePath toPath:qravedDataBaseFilePath error:&error];
            if (!success) {
                NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
            }
            NSURL *qravedDataBaseFileUrl = [NSURL fileURLWithPath:qravedDataBaseFilePath];
            [self addSkipBackupAttributeToItemAtURL:qravedDataBaseFileUrl];
        }
            
        _basePath = qravedDataBaseFilePath;

        NSLog(@"%@qraved.sqlite filePath():%@",QRAVED_ENVIROMENT,qravedDataBaseFilePath);

        _fmdb=[[FMDatabase alloc]initWithPath:_basePath];
        [_fmdb setLogsErrors:YES];
        [_fmdb open];
        _dbQueue = [FMDatabaseQueue databaseQueueWithPath:_basePath];
    }
    return self;
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath: [URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    return success;
}

//打开数据库
-(BOOL)openDataBase
{
    NSAssert(_fmdb!=nil, @"dataBase is nil");
    if ([_fmdb open]) {
        return YES;
    }else{
        [self openDataBaseFailure];
    }
    return NO;
}

//数据库打开错误是 的解决方法
-(void)openDataBaseFailure{

}
//初始化所有的数据表
-(void)initAllTables{



}

-(NSObject *)loadModelWithId:(NSNumber *)modelId class:(Class)clazz{
    NSString * tableName=NSStringFromClass(clazz);
    NSString * columnIdName = [NSStringFromClass(clazz) stringByAppendingString:@"Id"];
    NSString * selectSQL= [NSString stringWithFormat:@"select * from %@ where %@ = %@",tableName,columnIdName,modelId];
    [self selectWithSql:selectSQL successBlock:^(FMResultSet *resultSet) {
        while ([resultSet next]) {

        }
        [resultSet close];
    } failureBlock:^(NSError *error) {

    }];
    return  nil;
}
-(IMGEntity *)loadObjectWithKey:(NSString *)key andValue:(NSValue *)value forClassName:(NSString *)className{
    NSArray * arr = [self getObjectWithKey:key andValue:value forClassName:className];
    if(arr.count<1)return nil;
    else return [arr objectAtIndex:0];
}
-(NSArray *)getObjectWithKey:(NSString *)key andValue:(NSValue *)value forClassName:(NSString *)className{
    NSString * selectSQL= [NSString stringWithFormat:@"select * from %@ where %@ = %@",className,key,value];
    FMResultSet * set =  [self.fmdb executeQuery:selectSQL];
    NSMutableArray * resultArr=[[NSMutableArray alloc]initWithCapacity:0];
    while ([set next]) {
        IMGEntity * entity = [[NSClassFromString(className) alloc]init];
        [entity setValueWithResultSet:set];
        [resultArr addObject:entity];
    }
    return [resultArr copy];
}
#pragma mark - create
-(void)createTableWithSql:(NSString *)sql{
    if([self.fmdb executeUpdate:sql]){
        DDBLog(@"create success");
        return ;
    }
    DDBLog(@"create failure");
}
-(void)createTableWithModel:(id<IMGModelProtrol>)model{
    if([model respondsToSelector:@selector(createTable)]){
        NSString * sql=[model createTable];
        [self createTableWithSql:sql];
    }
}
//为 某个 模型创建 对应的数据表
-(BOOL)createTableWithTableName:(NSString *)tableName{
    if([self isExistsTable:tableName]){
        return YES;
    }

    NSString * createSQL = [self tableSql:tableName];
    return [self.fmdb executeUpdate:createSQL];
}
-(void)createTableWithClass:(Class) clazz{
    [self createTableWithTableName:NSStringFromClass(clazz)];
}


#pragma mark -insert
-(void)insertWithSql:(NSString *)sql{
    [self.fmdb executeUpdate:sql];
}
-(void)insertObject:(IMGEntity *)object{
    [self insertModel:object];
}
-(void)insertModel:(IMGEntity *)model{

    if(![self isExistsTable:NSStringFromClass(model.class)]){
        [self createTableWithTableName:NSStringFromClass(model.class)];
    }

    NSString * insertSQL=[self insertSql:model];
    [_fmdb executeUpdate:insertSQL];
}
-(void)insertModel:(IMGEntity *)model update:(BOOL) update selectKey:(NSString * )selectKey andSelectValue:(NSObject *)selectValue{

    if(![self isExistsTable:NSStringFromClass(model.class)]){
        [self createTableWithTableName:NSStringFromClass(model.class)];
    }

    if(update){
        NSString * selectSQL =nil;
        if([selectValue isKindOfClass:[NSString class]]){
            selectSQL = [NSString stringWithFormat:@"select count(*)as count from %@ where %@ = '%@'",NSStringFromClass([model class]),selectKey,selectValue];
        }else{
            selectSQL=[NSString stringWithFormat:@"select count(*)as count from %@ where %@ = %@",NSStringFromClass([model class]),selectKey,selectValue];
        }

        FMResultSet *resultSet = [_fmdb executeQuery:selectSQL];
        int count = 0;
        if ([resultSet next]) {
             count=[resultSet intForColumn:@"count"];
        }
        [resultSet close];
        if (count > 0) {
            [self updateModel:model params:@{selectKey:selectValue}];
        }else{
            [self insertModel:model];
        }
    }else{
        [self insertModel:model];
    }


}
-(void)insertRelation:(id)model selectField:(NSString*)selectField andSelectID:(NSNumber*)selectID relationField:(NSString*)relationField andRelationId:(NSNumber*)relationId{
    NSString * selectSQL = [NSString stringWithFormat:@"select count(*)as count from %@ where %@ = %@ and %@ = %@",NSStringFromClass([model class]),selectField,selectID,relationField,relationId];
    FMResultSet *resultSet = [_fmdb executeQuery:selectSQL];
    if ([resultSet next]) {
        int count=[resultSet intForColumn:@"count"];
        if (count) {
            [self updateModel:model params:@{selectField:selectID,relationField:relationId}];
        }
        else
        {
            [self insertModel:model];
        }
        [resultSet close];
    }
    else
    {
        [self insertModel:model];
    }
}
-(void)selectModel:(IMGEntity *)model selectField:(NSString*)selectField andSelectValue:(NSNumber *)selectString andBlock:(void (^)(NSString *))block
{
    NSString * selectSQL = [NSString stringWithFormat:@"select * from %@ where %@ = %@",NSStringFromClass([model class]),selectField,selectString];
    //NSString *selectSQL = [NSString stringWithFormat:@"select *from %@ where mediaCommentId = 12798",NSStringFromClass([model class])];
    FMResultSet *resultSet = [_fmdb executeQuery:selectSQL];
    if ([resultSet next])
    {
        NSString *contentsStr = [resultSet stringForColumn:@"contents"];
        NSRange range=[contentsStr rangeOfString:@"<div class=\"title\">"];
        if (range.location == NSNotFound)
        {
            NSArray *contentsStrArr = [contentsStr componentsSeparatedByString:@"<div class=\"single-box clearfix entry-content\">"];
            NSString *contentStr = [contentsStrArr objectAtIndex:1];
            NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"contentStyle" ofType:@"plist"];
            NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
            NSString *contentFirstStr = [data objectForKey:@"content"];
            NSString *title = model.title;
            NSString *cssTitleStr = [NSString stringWithFormat:@"<div class=\"title\">%@</div>",title];
            NSString *newContentsStr = [[NSString alloc] initWithFormat:@"%@%@%@%@",contentFirstStr,cssTitleStr,contentStr,[data objectForKey:@"contentEnd"]];
            contentsStr = [NSString stringWithString:newContentsStr];
        }
        
        block(contentsStr);
    }
    else
    {
        //12798
//        
//        NSString * selectSQL = [NSString stringWithFormat:@"select * from %@ where %@ = 12798",NSStringFromClass([model class]),selectField];
//        FMResultSet *resultSet = [_fmdb executeQuery:selectSQL];
//        if ([resultSet next])
//        {
//            block([resultSet stringForColumn:@"contents"]);
//        }
        block(nil);
    }
}
-(void)insertModel:(IMGEntity *)model selectField:(NSString*)selectField andSelectID:(NSNumber*)selectID{
    NSString * selectSQL = [NSString stringWithFormat:@"select count(*)as count from %@ where %@ = %@",NSStringFromClass([model class]),selectField,selectID];
    FMResultSet *resultSet = [_fmdb executeQuery:selectSQL];
    if ([resultSet next]) {
        int count=[resultSet intForColumn:@"count"];
        if (count) {
            [self updateModel:model params:@{selectField:selectID}];
        }
        else
        {
            [self insertModel:model];
        }
        [resultSet close];
    }
    else
    {
        [self insertModel:model];
    }
}


#pragma mark -delete
-(void)deleteWithSql:(NSString *)sql{
    [_dbQueue inDatabase:^(FMDatabase *db) {
        [db executeUpdate:sql];
    }];

}
-(void)deleteModel:(IMGEntity *)model withID:(NSInteger)ID{
    NSString * deleteSQL = [NSString stringWithFormat:@"delete from %@ where ID = %ld",NSStringFromClass([model class]),(long)model.ID];
    bool resu= [_fmdb executeUpdate:deleteSQL];
    printf("%d",resu);
}
-(void)deleteWithModel:(id<IMGModelProtrol>)model{
    if([model respondsToSelector:@selector(deleteSQL)]){
        [self deleteWithSql:[model deleteSQL]];
    }
}
#pragma mark -update
-(void)updateModel:(IMGEntity * )model params:(NSDictionary *)params{
    u_int count;
    objc_property_t * properties  = class_copyPropertyList([model class], &count);
    NSMutableString * updateSQL=[NSMutableString  stringWithFormat:@"update %@ set ",NSStringFromClass([model class])];
    for (int i=0; i<count; i++) {
        if(i!=0){
            [updateSQL appendString:@","];
        }
        const char * pro_name = property_getName(properties[i]);
        NSString * proKey=[[NSString alloc]initWithCString: pro_name encoding:NSUTF8StringEncoding];
        [updateSQL appendFormat:@"%@",proKey];
        [updateSQL appendString:@" = "];
        NSObject * value=[model valueForKey:proKey];
        if(value ==nil){
            [updateSQL appendString:@"\"\""];
        }else
        if ([value isKindOfClass:[NSString class]]) {
            value = [[(NSString *)value stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""] stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
            [updateSQL appendFormat:@"\"%@\"",value];
        }else if([value isKindOfClass:[NSNumber class]]){
            [updateSQL appendFormat:@"\"%@\"",value];
        }else{
            [updateSQL appendFormat:@"%@",value];
        }
    }
    if(params!=nil){
        [updateSQL appendString:@" where "];
    }
    NSArray * allkeys=[params allKeys];
    for (int i=0; i < allkeys.count; i++) {
        if(i!=0){
            [updateSQL appendFormat:@" and "];
        }
        NSObject *value = [params objectForKey:allkeys[i]];
        if([value isKindOfClass:[NSString class]]){
            value = [[(NSString *)value stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""] stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
            [updateSQL appendFormat:@"%@ = \"%@\"",allkeys[i],value];
        }else{
            [updateSQL appendFormat:@"%@ = %@",allkeys[i],[params objectForKey:allkeys[i]]];
        }
    }
//    NSLog(@"%@",updateSQL);
    [self.fmdb executeUpdate:updateSQL];
}

#pragma mark - select
-(void)selectWithSql:(NSString *)sql successBlock:(void (^)(FMResultSet * result))success failureBlock:(void (^)(NSError * error))failure{

    NSAssert(sql!=nil, @"SQL IS NIL");

    NSDictionary * userInfo=@{NSLocalizedDescriptionKey:self.fmdb.debugDescription};

    NSError * error=[[NSError alloc]initWithDomain:ERROR_DOMAIN code:1000 userInfo:userInfo];


    FMResultSet * rs =  [self.fmdb executeQuery:sql];
    if (!rs) {
        DDBLog(@"result is nil");
        return ;
    }

    if(success){
        success(rs);
        return;
    }
    if(failure){
        failure(error);
    }
}


-(void)selectWithModel:(id<IMGModelProtrol>)model successBlock:(void (^)(FMResultSet * result))success failureBlock:(void (^)(NSError *))failure{
    NSString * sql = nil;
    if([model respondsToSelector:@selector(selectSql)]){
        sql= [model selectSql];
    }
    [self selectWithSql:sql successBlock:success failureBlock:failure];
}

-(void)selectWithClass:(Class)clazz successBlock:(void (^)(FMResultSet *))success failureBlock:(void (^)(NSError *))failure{
    id <IMGModelProtrol> obj= [[clazz alloc]init];
    [self selectWithModel:obj successBlock:success failureBlock:failure];
}
-(void)selectArrayWithClass:(Class)clazz successBlock:(void (^)(NSArray * array))success failureBlock:(void (^)(NSError * error))failure{
    NSString * selectSql=[self selectSql:clazz];
    __block  NSMutableArray * array=[[NSMutableArray alloc]initWithCapacity:0];
    [self selectWithSql:selectSql successBlock:^(FMResultSet * resultSet) {
        while([resultSet next]){
            [array addObject:[self setValueWithResult:resultSet forClass:clazz]];

        }
        [resultSet close];
        success(array);
    } failureBlock:^(NSError *error) {

    }];
}

-(BOOL)SQLNullWithClass:(Class)class{
    if ([self isExistsTable:NSStringFromClass([class class])]) {
        NSString * selectSQL = [NSString stringWithFormat:@"select count(*)as count from %@",NSStringFromClass([class class])];
        FMResultSet * resultSet=[_fmdb executeQuery:selectSQL];
        if ([resultSet next]) {
            int count=[resultSet intForColumn:@"count"];
            if (count) {
                [resultSet close];
                return NO;
            }
            [resultSet close];
        }
        return YES;
    }
    else
    {
        return YES;
    }
    
}
#pragma mark --private

-(BOOL)isExistsTable:(NSString *)tablename{
    FMResultSet *rs = [_fmdb executeQuery:@"select count(*) as 'count' from sqlite_master where type ='table' and name = ?", tablename];
    BOOL ret = NO;
    while ([rs next])
    {

        NSInteger count = [rs intForColumn:@"count"];

        if (0 == count)
        {
            ret = NO;
        }
        else
        {
            ret = YES;
        }
    }
    return ret;
}
//drop exists table
-(BOOL)dropExistsTable:(NSString*)tableName{
    if(![self isExistsTable:tableName]){
        return YES;
    }
    NSString *sql = [NSString stringWithFormat:@"drop table %@",tableName];
    BOOL ret = [_fmdb executeUpdate:sql];
    return ret;
}

- (NSString *)tableSql:(NSString *)tablename{

    NSMutableString *sql = [[NSMutableString alloc] init];
    u_int count;
    objc_property_t * properties  = class_copyPropertyList(NSClassFromString(tablename), &count);

    [sql appendFormat:@"create table %@ (",tablename] ;
    for (int i=0; i<count; i++) {
        if (i>0) {
            [sql appendString:@","];
        }
       // u_int outCount;
      //  objc_property_attribute_t * attribute= property_copyAttributeList(properties[i],&outCount);

        const char * propertyName = property_getName(properties[i]);

      //  const char * attributeInfor = property_getAttributes(properties[i]);
        const char * attributeValue = property_copyAttributeValue(properties[i], "T");

        if(strcmp(attributeValue,"i")==0){
            [sql appendFormat:@"%s int",propertyName];
        }else if(strcmp(attributeValue, "f")==0){
            [sql appendFormat:@"%s double",propertyName];
        }else if(strcmp(attributeValue, "@\"NSString\"")==0){
            [sql appendFormat:@"%s text",propertyName];
        }else if (strcmp(attributeValue, "@\"NSNumber\"")==0){
            [sql appendFormat:@"%s text",propertyName];
        }else{
            [sql appendFormat:@"%s text",propertyName];
        }

      //  free(attribute);
    }
    [sql appendString:@")"];
    free(properties);
    return sql;
}
- (NSString * )getValueOfProperty:(objc_property_t ) property  withModel:(id)model{
    const char * propertyInfo= property_getAttributes(property);

    NSArray  * arr          =   [[NSString stringWithUTF8String:propertyInfo] componentsSeparatedByString:@","];
    NSString * att          =   [arr objectAtIndex:0];
    NSString * propertyName =   [NSString stringWithUTF8String:property_getName(property)];

    id value=[model valueForKey:propertyName];
    if(value==nil){return  @"''";}
    if([att isEqualToString:@"T@\"NSString\""]){
        if([value isKindOfClass:[NSString class]]){
            value = [[value stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""] stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
        }
        return [NSString stringWithFormat:@"\"%@\"",value];
    }
    if([att isEqualToString:@"T@\"NSNumber\""]){
        return value;
    }
    if([att isEqualToString:@"T@\"NSNull\""]){
        return @"";
    }
    return value;
}

- (NSString * )getInsertValue:(id )value type:(NSString * )type{

    if([type isEqualToString:@"i"]){

    }else if([type isEqualToString:@"f"]){

    }else if([type isEqualToString:@"@\"NSString\""]){
        return [NSString stringWithFormat:@"'%@'",value];   
    }else if ([type isEqualToString:@"@\"NSNumber\""]){
        return  [((NSNumber *)value) stringValue];
    }
    return value;
}
-(NSString *) insertSql:(id)model{

    NSMutableString *sql = [[NSMutableString alloc] init];
    u_int count;

    objc_property_t * properties  = class_copyPropertyList([model class], &count);

    NSString * tablename=NSStringFromClass([model class]);

    [sql appendFormat:@"insert into %@ (",tablename] ;

    for (int i=0; i<count; i++) {
        if (i>0) {
            [sql appendString:@","];
        }
        const char * propertyName = property_getName(properties[i]);
        [sql appendFormat:@"%s",propertyName];
    }

    [sql appendString:@") values ("];

    for (int i=0; i < count; i++) {
        if (i>0) {
            [sql appendString:@","];
        }
        [sql appendFormat:@"%@",[self getValueOfProperty:properties[i] withModel:model]];
    }
    [sql appendString:@")"];

    free(properties);
    return sql;
}

-(NSString *)selectSql:(Class) clazz{

    NSString * tablename=NSStringFromClass(clazz);

    NSMutableString * string=[NSMutableString stringWithFormat:@"select * from %@",tablename];

    return  string;
}
-(id)setValueWithResult:(FMResultSet *)result forClass:(Class)clazz{
    id  model=[[clazz alloc]init];
    int columnCount= [result columnCount];

    for (int i=0; i<columnCount; i++) {
        
        NSString * columnName=[result columnNameForIndex:i];
        
        [model setValue:[result objectForColumnName:columnName] forKey:columnName];

    }
    return  model;
}

-(FMResultSet *)executeQuery:(NSString*)sql{
    return  [_fmdb executeQuery:sql];
}


-(void)insertRelation:(id)model selectField:(NSString*)selectField andSelectID:(NSNumber*)selectID relationField:(NSString*)relationField andRelationId:(NSNumber*)relationId  andFMDatabase:(FMDatabase *)fmdb{
    NSString * selectSQL = [NSString stringWithFormat:@"select count(*)as count from %@ where %@ = %@ and %@ = %@",NSStringFromClass([model class]),selectField,selectID,relationField,relationId];
    FMResultSet *resultSet = [fmdb executeQuery:selectSQL];
    if ([resultSet next]) {
        int count=[resultSet intForColumn:@"count"];
        if (count) {
            [self updateModel:model params:@{selectField:selectID,relationField:relationId} andFMDatabase:fmdb];
        }
        else
        {
            [self insertModel:model andFMDatabase:fmdb];
        }
        [resultSet close];
    }else{
        [self insertModel:model andFMDatabase:fmdb];
    }
}
-(void)insertModel:(IMGEntity *)model selectField:(NSString*)selectField andSelectID:(NSNumber*)selectID andFMDatabase:(FMDatabase *)fmdb{
    NSString * selectSQL = [NSString stringWithFormat:@"select count(*)as count from %@ where %@ = %@",NSStringFromClass([model class]),selectField,selectID];
    FMResultSet *resultSet = [fmdb executeQuery:selectSQL];
    if ([resultSet next]) {
        int count=[resultSet intForColumn:@"count"];
        if (count) {
            [self updateModel:model params:@{selectField:selectID} andFMDatabase:fmdb];
        }
        else
        {
            [self insertModel:model andFMDatabase:fmdb];
        }
        [resultSet close];
    }
    else
    {
        [self insertModel:model andFMDatabase:fmdb];
    }
}

-(void)updateModel:(IMGEntity * )model params:(NSDictionary *)params andFMDatabase:(FMDatabase *)fmdb{
    u_int count;
    objc_property_t * properties  = class_copyPropertyList([model class], &count);
    NSMutableString * updateSQL=[NSMutableString  stringWithFormat:@"update %@ set ",NSStringFromClass([model class])];
    for (int i=0; i<count; i++) {
        if(i!=0){
            [updateSQL appendString:@","];
        }
        const char * pro_name = property_getName(properties[i]);
        NSString * proKey=[[NSString alloc]initWithCString: pro_name encoding:NSUTF8StringEncoding];
        [updateSQL appendFormat:@"%@",proKey];
        [updateSQL appendString:@" = "];
        NSObject * value=[model valueForKey:proKey];
        if(value ==nil){
            [updateSQL appendString:@"\"\""];
        }else
            if ([value isKindOfClass:[NSString class]]) {
                value = [[(NSString *)value stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""] stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
                [updateSQL appendFormat:@"\"%@\"",value];
            }else if([value isKindOfClass:[NSNumber class]]){
                [updateSQL appendFormat:@"\"%@\"",value];
            }else{
                [updateSQL appendFormat:@"%@",value];
            }
    }
    if(params!=nil){
        [updateSQL appendString:@" where "];
    }
    NSArray * allkeys=[params allKeys];
    for (int i=0; i < allkeys.count; i++) {
        if(i!=0){
            [updateSQL appendFormat:@" and "];
        }
        NSObject *value = [params objectForKey:allkeys[i]];
        if([value isKindOfClass:[NSString class]]){
            value = [[(NSString *)value stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""] stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
            [updateSQL appendFormat:@"%@ = \"%@\"",allkeys[i],value];
        }else{
            [updateSQL appendFormat:@"%@ = %@",allkeys[i],[params objectForKey:allkeys[i]]];
        }
    }
    //    NSLog(@"%@",updateSQL);
    [fmdb executeUpdate:updateSQL];
}

-(void)insertModel:(IMGEntity *)model andFMDatabase:(FMDatabase *)fmdb{
    
    if(![self isExistsTable:NSStringFromClass(model.class)]){
        [self createTableWithTableName:NSStringFromClass(model.class)];
    }
    
    NSString * insertSQL=[self insertSql:model];
    [fmdb executeUpdate:insertSQL];
}
-(BOOL)createTableWithTableName:(NSString *)tableName  andFMDatabase:(FMDatabase *)fmdb{
    if([self isExistsTable:tableName]){
        return YES;
    }
    
    NSString * createSQL = [self tableSql:tableName];
    return [fmdb executeUpdate:createSQL];
}
@end
