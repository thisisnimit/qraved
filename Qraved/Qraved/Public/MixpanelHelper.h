//
//  MixpanelHelper.h
//  Qraved
//
//  Created by Laura on 14/12/3.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGRestaurant.h"
#import "IMGReservation.h"
#import "IMGRestaurantOffer.h"
#import "IMGUser.h"
@interface MixpanelHelper : NSObject
+(void)trackViewRestaurantDetailWithRestaurant:(IMGRestaurant*)restaurant;
+(void)trackRestaurantTabFilterWithTab:(NSString*)tab;
+(void)trackSerachRestaurantPriceLevelWithTab:(NSString*)tab;
+(void)trackSerachRestaurantCuisine:(NSArray*)cuisines;
+(void)trackViewMethodforSearchListWithTab:(NSString*)tab;
+(void)trackSerachRestaurantWithKeyWord:(NSString*)keyword;
+(void)trackSignUpWithTab:(NSString*)tab;
+(void)trackSerachRestaurantNeighborhood:(NSString*)tab;
+(void)trackLoginWithType:(NSString*)type;
+(void)trackOpenBookPopupWindowWithRestaurant:(IMGRestaurant*)restaurant;
+(void)trackTapBottomCallButtonPropertiesWithRestaurant:(IMGRestaurant*)restaurant;
+(void)trackTapPageCallButtonPropertiesWithRestaurant:(IMGRestaurant*)restaurant;
+(void)trackChooseBookingPropertiesWithRestaurant:(IMGRestaurant*)restaurant andReservation:(IMGRestaurantOffer*)reservationOff;
+(void)trackApplyVoucharCodeWithRestaurant:(IMGRestaurant*)restaurant andReservation:(IMGReservation*)reservation;
+(void)trackConfirmBookingWithRestaurant:(IMGRestaurant*)restaurant andReservation:(IMGReservation*)reservation;
+(void)trackBookingDoneWithRestaurant:(IMGRestaurant*)restaurant andReservation:(IMGReservation*)reservation;
+(void)trackCancelBookingWithRestaurant:(IMGRestaurant*)restaurant andReservation:(IMGReservation*)reservation;
+(void)trackSerachRestaurantPriceLevelsWithArray:(NSArray*)array;
+(void)trackUploadPhoto:(IMGRestaurant*)restaurant andDishName:(NSString *)dishName;
+(void)trackWriteReview:(IMGRestaurant*)restaurant;
+(void)trackRedeemPoints:(IMGUser*)user;

//share
+(void)trackShareToTwitterWithshareObject:(NSString *)shareObject andSharePerson:(NSString *)sharePerson;
+(void)trackShareToFacebookWithshareObject:(NSString *)shareObject andSharePerson:(NSString *)sharePerson;
+(void)trackShareToEmailWithshareObject:(NSString *)shareObject andSharePerson:(NSString *)sharePerson;
+(void)trackShareToSMSWithshareObject:(NSString *)shareObject andSharePerson:(NSString *)sharePerson;
+(void)trackShareToPathWithshareObject:(NSString *)shareObject andSharePerson:(NSString *)sharePerson;


+(void)trackViewDiningGuideWithDiningGuideName:(NSString*)diningGuideName;
+(void)trackViewEventWithEventName:(NSString*)eventName;
+(void)trackViewDiscoverWithDiscoverName:(NSString*)discoverName;

+ (void)trackOpenJournalArticleWithArticleId:(NSNumber *)articleId andArticleTitle:(NSString *)articleTitle;
+ (void)trackOpenJournal:(NSString *)journalType;
+ (void)trackOpenMyList;

@end
