//
//  UIButton+Style.m
//  Qraved
//
//  Created by Sean Liao on 9/10/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//
#import "UIConstants.h"
#import "UIButton+Style.h"

@implementation UIButton (Style)

-(id)initDefaultButton
{
    self=[super init];
    if(self)
    {
        self = [UIButton buttonWithType:UIButtonTypeCustom];
        
    }
    return self;
}

- (id)initProfileTextButton {
    self=[self initDefaultButton];
    
    if(self) {
        self.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:DEFAULT_INFO_HEADER_FONT_SIZE];
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        
    }
    return self;
}
-(id)initUtilityButton {
    self=[self initDefaultButton];
    
    if(self) {
        self.backgroundColor = [UIColor colorWithHue:(DEFAULT_BACKGROUND_LIGHT_HUE/360) saturation:DEFAULT_BACKGROUND_LIGHT_SAT brightness:DEFAULT_BACKGROUND_LIGHT_BRI alpha:1.0];
    }
    self.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:10];
    
    return self;
    
    
}
-(id)initHighlightButton {
    self=[self initDefaultButton];
    
    if(self) {
        self.backgroundColor = [UIColor colorWithHue:(DEFAULT_HIGHLIGHT_HUE/360) saturation:DEFAULT_HIGHLIGHT_SAT brightness:DEFAULT_HIGHLIGHT_BRI alpha:1.0];
    }
    self.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:10];
    [self.titleLabel setTextColor:[UIColor whiteColor]];
    
    return self;
}

@end
