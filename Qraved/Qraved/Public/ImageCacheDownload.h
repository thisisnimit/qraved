//
//  ImageCacheDownload.h
//  Qraved
//
//  Created by Admin on 8/15/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SDWebImageManager.h"


@interface ImageCacheDownload : NSObject <SDWebImageManagerDelegate>


//@property (nonatomic ,retain) NSURL *url;
@property (nonatomic, strong) UIImageView *imgView;

@property (nonatomic, strong) UIImage *downloadImage;


-(void)loadImageToImageView:(UIImageView *)toImgView andUrl:(NSURL *)url;

-(void)loadImageToImage:(UIImage *)toImage andUrl:(NSURL *)url;

+(BOOL)imgHasDownloaded:(NSURL *)imgUrl;

@end
