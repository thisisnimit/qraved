//
//  NdUncaughtExceptionHandler.h
//  EasyPayment
//
//  Created by wx on 12-12-12.
//  Copyright (c) 2012年 wx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NdUncaughtExceptionHandler : NSObject


+(void)setDefaultHandler;

+(NSUncaughtExceptionHandler *)getHandler;

//+(void)TakeException:(NSException *) exception;
@end
