//
//  ZYQAssetPickerController.m
//  ZYQAssetPickerControllerDemo
//
//  Created by Zhao Yiqi on 13-12-25.
//  Copyright (c) 2013年 heroims. All rights reserved.
//

#import "ZYQAssetPickerController.h"
#import "ReviewHeaderView.h"
#import "IMGRestaurant.h"
#import "UIView+Helper.h"
#import "UIDevice+Util.h"
#import "Label.h"
#import "UIColor+Helper.h"
#import "LoadingView.h"

#define IS_IOS7             ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending)


#define DeviceHeight   [UIScreen mainScreen].bounds.size.height-20
#define DeviceWidth    [UIScreen mainScreen].bounds.size.width

#define kThumbnailLength    (DeviceWidth-20)/3
#define kThumbnailSize      CGSizeMake(kThumbnailLength, kThumbnailLength)

#define kPopoverContentSize CGSizeMake(DeviceWidth, DeviceHeight)

#pragma mark -

@interface NSDate (TimeInterval)

+ (NSDateComponents *)componetsWithTimeInterval:(NSTimeInterval)timeInterval;
+ (NSString *)timeDescriptionOfTimeInterval:(NSTimeInterval)timeInterval;

@end

@implementation NSDate (TimeInterval)

+ (NSDateComponents *)componetsWithTimeInterval:(NSTimeInterval)timeInterval
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDate *date1 = [[NSDate alloc] init];
    NSDate *date2 = [[NSDate alloc] initWithTimeInterval:timeInterval sinceDate:date1];
    
    unsigned int unitFlags =
    NSCalendarUnitSecond | NSCalendarUnitMinute | NSCalendarUnitHour |
    NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear;
    
    return [calendar components:unitFlags
                       fromDate:date1
                         toDate:date2
                        options:0];
}

+ (NSString *)timeDescriptionOfTimeInterval:(NSTimeInterval)timeInterval
{
    NSDateComponents *components = [self.class componetsWithTimeInterval:timeInterval];
    NSInteger roundedSeconds = lround(timeInterval - (components.hour * 60) - (components.minute * 60 * 60));
    
    if (components.hour > 0)
    {
        return [NSString stringWithFormat:@"%ld:%02ld:%02ld", (long)components.hour, (long)components.minute, (long)roundedSeconds];
    }
    
    else
    {
        return [NSString stringWithFormat:@"%ld:%02ld", (long)components.minute, (long)roundedSeconds];
    }
}

@end

#pragma mark - ZYQAssetPickerController

@interface ZYQAssetPickerController ()

@property (nonatomic, copy) NSArray *indexPathsForSelectedItems;

@end

#pragma mark - ZYQVideoTitleView

@implementation ZYQVideoTitleView

-(void)drawRect:(CGRect)rect{
    CGFloat colors [] = {
        0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.8,
        0.0, 0.0, 0.0, 1.0
    };
    
    CGFloat locations [] = {0.0, 0.75, 1.0};
    
    CGColorSpaceRef baseSpace   = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient      = CGGradientCreateWithColorComponents(baseSpace, colors, locations, 2);
    
    CGContextRef context    = UIGraphicsGetCurrentContext();
    
    CGFloat height          = rect.size.height;
    CGPoint startPoint      = CGPointMake(CGRectGetMidX(rect), height);
    CGPoint endPoint        = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
    
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, kCGGradientDrawsBeforeStartLocation);
    
    CGSize titleSize        = [self.text sizeWithFont:self.font];
    [self.textColor set];
    [self.text drawAtPoint:CGPointMake(rect.size.width - titleSize.width - 2 , (height - 12) / 2)
                   forWidth:kThumbnailLength
                   withFont:self.font
                   fontSize:12
              lineBreakMode:NSLineBreakByTruncatingTail
         baselineAdjustment:UIBaselineAdjustmentAlignCenters];

    UIImage *videoIcon=[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"ZYQAssetPicker.Bundle/Images/AssetsPickerVideo@2x.png"]];
    
    [videoIcon drawAtPoint:CGPointMake(2, (height - videoIcon.size.height) / 2)];
    
}

@end

#pragma mark - ZYQTapAssetView

@interface ZYQTapAssetView ()

@property(nonatomic,retain)UIImageView *selectView;

@end

@implementation ZYQTapAssetView

static UIImage *checkedIcon;
static UIImage *uncheckIcon;
static UIColor *selectedColor;
static UIColor *disabledColor;

+ (void)initialize
{
//    checkedIcon     = [UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"ZYQAssetPicker.Bundle/Images/%@@2x.png",(!IS_IOS7) ? @"AssetsPickerChecked~iOS6" : @"AssetsPickerChecked"]]];
    checkedIcon = [UIImage imageNamed:@"ic_choose"];
    uncheckIcon = [UIImage imageNamed:@""];
    selectedColor   = [UIColor colorWithRed:210/255.0 green:0/255.0 blue:0/255.0 alpha:0.4];
    disabledColor   = [UIColor colorWithWhite:1 alpha:0.9];
}

-(id)initWithFrame:(CGRect)frame{
    if (self=[super initWithFrame:frame]) {
        _selectView=[[UIImageView alloc] initWithFrame:CGRectMake((frame.size.width-24)/2 , (frame.size.height-18)/2, 24, 18)];
        [self addSubview:_selectView];
    }
    return self;
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    if (_disabled) {
        return;
    }
    
    if (_delegate!=nil&&[_delegate respondsToSelector:@selector(shouldTap)]) {
        if (![_delegate shouldTap]&&!_selected) {
            return;
        }
    }

    if ((_selected=!_selected)) {
        self.backgroundColor=selectedColor;
        [_selectView setImage:checkedIcon];
    }
    else{
        self.backgroundColor=[UIColor clearColor];
        [_selectView setImage:uncheckIcon];
    }
    if (_delegate!=nil&&[_delegate respondsToSelector:@selector(touchSelect:)]) {
        [_delegate touchSelect:_selected];
    }
}

-(void)setDisabled:(BOOL)disabled{
    _disabled=disabled;
    if (_disabled) {
        self.backgroundColor=disabledColor;
    }
    else{
        self.backgroundColor=[UIColor clearColor];
    }
}

-(void)setSelected:(BOOL)selected{
    if (_disabled) {
        self.backgroundColor=disabledColor;
        [_selectView setImage:nil];
        return;
    }

    _selected=selected;
    if (_selected) {
        self.backgroundColor=selectedColor;
        [_selectView setImage:checkedIcon];
    }
    else{
        self.backgroundColor=[UIColor clearColor];
        [_selectView setImage:uncheckIcon];
    }
}

@end

#pragma mark - ZYQAssetView

@interface ZYQAssetView ()<ZYQTapAssetViewDelegate>

@property (nonatomic, strong) ALAsset *asset;

@property (nonatomic, weak) id<ZYQAssetViewDelegate> delegate;

@property (nonatomic, retain) UIImageView *imageView;
@property (nonatomic, retain) ZYQVideoTitleView *videoTitle;
@property (nonatomic, retain) ZYQTapAssetView *tapAssetView;

@end

@implementation ZYQAssetView

static UIFont *titleFont = nil;

static CGFloat titleHeight;
static UIColor *titleColor;

+ (void)initialize
{
    titleFont       = [UIFont systemFontOfSize:12];
    titleHeight     = 20.0f;
    titleColor      = [UIColor whiteColor];
}

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        self.opaque                     = YES;
        self.isAccessibilityElement     = YES;
        self.accessibilityTraits        = UIAccessibilityTraitImage;
        
        _imageView=[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kThumbnailSize.width, kThumbnailSize.height)];
        [self addSubview:_imageView];
        
        _videoTitle=[[ZYQVideoTitleView alloc] initWithFrame:CGRectMake(0, kThumbnailSize.height-20, kThumbnailSize.width, titleHeight)];
        _videoTitle.hidden=YES;
        _videoTitle.font=titleFont;
        _videoTitle.textColor=titleColor;
        _videoTitle.textAlignment=NSTextAlignmentRight;
        _videoTitle.backgroundColor=[UIColor clearColor];
        [self addSubview:_videoTitle];
        
        _tapAssetView=[[ZYQTapAssetView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _tapAssetView.delegate=self;
        [self addSubview:_tapAssetView];
    }
    
    return self;
}

- (void)bind:(ALAsset *)asset selectionFilter:(NSPredicate*)selectionFilter isSeleced:(BOOL)isSeleced
{
    self.asset=asset;

    [_imageView setImage:[UIImage imageWithCGImage:asset.aspectRatioThumbnail scale:2.f orientation:UIImageOrientationUp]];
    _imageView.contentMode=UIViewContentModeScaleAspectFill;
    _imageView.clipsToBounds=YES;
    if ([[asset valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo]) {
        _videoTitle.hidden=NO;
        _videoTitle.text=[NSDate timeDescriptionOfTimeInterval:[[asset valueForProperty:ALAssetPropertyDuration] doubleValue]];
    }
    else{
        _videoTitle.hidden=YES;
    }
    
    _tapAssetView.disabled=! [selectionFilter evaluateWithObject:asset];
    
    _tapAssetView.selected=isSeleced;
}

#pragma mark - ZYQTapAssetView Delegate

-(BOOL)shouldTap{
    if (_delegate!=nil&&[_delegate respondsToSelector:@selector(shouldSelectAsset:)]) {
        return [_delegate shouldSelectAsset:_asset];
    }
    return YES;
}

-(void)touchSelect:(BOOL)select{
    if (_delegate!=nil&&[_delegate respondsToSelector:@selector(tapSelectHandle:asset:)]) {
        [_delegate tapSelectHandle:select asset:_asset];
    }
}

@end

#pragma mark - ChooseViewCell

@interface ChooseViewCell ()

@end

@implementation ChooseViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        UIButton *chooseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [chooseBtn setTitle:@"choose" forState:UIControlStateNormal];
        chooseBtn.backgroundColor = [UIColor greenColor];
        chooseBtn.frame = CGRectMake((DeviceWidth-kThumbnailSize.width*3-2*2)/2, 20, DeviceWidth - (DeviceWidth-kThumbnailSize.width*3-2*2), 44);
        //[self addSubview:chooseBtn];
    }
    return self;
}

@end

#pragma mark - ZYQAssetViewCell

@interface ZYQAssetViewCell ()<ZYQAssetViewDelegate>

@end

@class ZYQAssetViewController;

@implementation ZYQAssetViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    return self;
}

- (void)bind:(NSArray *)assets selectionFilter:(NSPredicate*)selectionFilter minimumInteritemSpacing:(float)minimumInteritemSpacing minimumLineSpacing:(float)minimumLineSpacing columns:(int)columns assetViewX:(float)assetViewX{
    
    if (self.contentView.subviews.count<assets.count) {
        for (int i=0; i<assets.count; i++) {
            if (i>((NSInteger)self.contentView.subviews.count-1)) {
                ZYQAssetView *assetView=[[ZYQAssetView alloc] initWithFrame:CGRectMake(assetViewX+(kThumbnailSize.width+minimumInteritemSpacing)*i, minimumLineSpacing-1, kThumbnailSize.width, kThumbnailSize.height)];
                [assetView bind:assets[i] selectionFilter:selectionFilter isSeleced:[((ZYQAssetViewController*)_delegate).indexPathsForSelectedItems containsObject:assets[i]]];
                assetView.delegate=self;
                
                UIImageView *tagImageView = [[UIImageView alloc] init];
                tagImageView.image = [UIImage imageNamed:@"AssetsPickerChecked"];
                tagImageView.frame = CGRectMake(75, 3, 22, 22);
                [assetView setImageView:tagImageView];
                
                [self.contentView addSubview:assetView];
            }
            else{
                ((ZYQAssetView*)self.contentView.subviews[i]).frame=CGRectMake(assetViewX+(kThumbnailSize.width+minimumInteritemSpacing)*(i), minimumLineSpacing-1, kThumbnailSize.width, kThumbnailSize.height);
                [(ZYQAssetView*)self.contentView.subviews[i] bind:assets[i] selectionFilter:selectionFilter isSeleced:[((ZYQAssetViewController*)_delegate).indexPathsForSelectedItems containsObject:assets[i]]];
            }

        }
        
    }
    else{
        for (int i=(int)self.contentView.subviews.count; i>0; i--) {
            if (i>assets.count) {
                [((ZYQAssetView*)self.contentView.subviews[i-1]) removeFromSuperview];
            }
            else{
                ((ZYQAssetView*)self.contentView.subviews[i-1]).frame=CGRectMake(assetViewX+(kThumbnailSize.width+minimumInteritemSpacing)*(i-1), minimumLineSpacing-1, kThumbnailSize.width, kThumbnailSize.height);
                [(ZYQAssetView*)self.contentView.subviews[i-1] bind:assets[i-1] selectionFilter:selectionFilter isSeleced:[((ZYQAssetViewController*)_delegate).indexPathsForSelectedItems containsObject:assets[i-1]]];
            }
        }
    }
}

#pragma mark - ZYQAssetView Delegate

-(BOOL)shouldSelectAsset:(ALAsset *)asset{
    if (_delegate!=nil&&[_delegate respondsToSelector:@selector(shouldSelectAsset:)]) {
        return [_delegate shouldSelectAsset:asset];
    }
    return YES;
}

-(void)tapSelectHandle:(BOOL)select asset:(ALAsset *)asset{
    if (select) {
        if (_delegate!=nil&&[_delegate respondsToSelector:@selector(didSelectAsset:)]) {
            [_delegate didSelectAsset:asset];
        }
    }
    else{
        if (_delegate!=nil&&[_delegate respondsToSelector:@selector(didDeselectAsset:)]) {
            [_delegate didDeselectAsset:asset];
        }
    }
}

@end

#pragma mark - ZYQAssetViewController

@interface ZYQAssetViewController ()<ZYQAssetViewCellDelegate,UIImagePickerControllerDelegate,UITableViewDelegate,UITableViewDataSource>{
    int columns;
    
    float minimumInteritemSpacing;
    float minimumLineSpacing;
    
    BOOL unFirst;
}

@property (nonatomic, strong) NSMutableArray *assets;
@property (nonatomic, assign) NSInteger numberOfPhotos;
@property (nonatomic, assign) NSInteger numberOfVideos;

@property (nonatomic, strong) ALAssetsLibrary *assetsLibrary;
@property (nonatomic, strong) NSMutableArray *groups;


@end



#define kAssetViewCellIdentifier           @"AssetViewCellIdentifier"
#import "CameraViewController.h"
#import "UIColor+Hex.h"

@implementation ZYQAssetViewController
{
    UIView *backgroundView;
    UIView *chooseGroupView;
    UITableView *groupsTableView;
    //Label *headeLabel;
    UIButton *btnHeader;
    UIImageView *blackTriangleImageView;
    UIButton *confirmBtn;
}
- (id)initWithRestaurantTitle:(NSString *)restaurantTitle
{
    
    columns = 3;
    _restaurantTitle = restaurantTitle;
    _indexPathsForSelectedItems=[[NSMutableArray alloc] init];
    self.tableView = [[UITableView alloc] init];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight - 44);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tag = 100;
    [self.view addSubview:self.tableView];
    
    UIView *footView = [[UIView alloc] init];
    footView.frame = CGRectMake(0, 0, DeviceWidth, 30);
    self.tableView.tableFooterView = footView;
    
    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
    {
        self.tableView.contentInset=UIEdgeInsetsMake(9.0, 2.0, 0, 2.0);
        
        minimumInteritemSpacing=3;
        minimumLineSpacing=3;
        
    }
    else
    {
        self.tableView.contentInset=UIEdgeInsetsMake(9.0, 0, 0, 0);
        
        minimumInteritemSpacing=2;
        minimumLineSpacing=2;
    }
    
    if (self = [super init])
    {
        if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
            [self setEdgesForExtendedLayout:UIRectEdgeNone];
        
        if ([self respondsToSelector:@selector(setContentSizeForViewInPopover:)])
            [self setContentSizeForViewInPopover:kPopoverContentSize];
        
    }
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    UIButton *photoClickBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    photoClickBtn.backgroundColor = [UIColor colorEBEBEB];
    photoClickBtn.frame = CGRectMake(8, 0, kThumbnailLength, kThumbnailLength);
    [photoClickBtn setImage:[UIImage imageNamed:@"ic_camera_take"] forState:UIControlStateNormal];
    [photoClickBtn addTarget:self action:@selector(photoClick) forControlEvents:UIControlEventTouchUpInside];
    [self.tableView addSubview:photoClickBtn];
    
    return self;
}

- (void)photoClick{
    [CommonMethod checkCameraStateWithViewController:self andCallBackBlock:^{
        CameraViewController *cameraVC = [[CameraViewController alloc]init];
        cameraVC.isFromUploadPhoto = self.isFromUploadPhoto;
        cameraVC.isUploadMenuPhoto = self.isUploadMenuPhoto;
        cameraVC.isCanSearch = self.isCanSearch;
        cameraVC.restaurantId = self.restaurantID;
        cameraVC.restaurant = self.restaurant;
        cameraVC.isReview = self.isReview;
        [self.navigationController pushViewController:cameraVC animated:YES];
    }];
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    //[self setupViews];
    [self setupButtons];
    [self setupGroup];
}
- (void)setupGroup
{
    if (!self.assetsLibrary)
        self.assetsLibrary = [self.class defaultAssetsLibrary];
    
    if (!self.groups)
        self.groups = [[NSMutableArray alloc] init];
    else
        [self.groups removeAllObjects];
    
    ZYQAssetPickerController *picker = (ZYQAssetPickerController *)self.navigationController;
    ALAssetsFilter *assetsFilter = picker.assetsFilter;
    
    ALAssetsLibraryGroupsEnumerationResultsBlock resultsBlock = ^(ALAssetsGroup *group, BOOL *stop) {
        
        if (group)
        {
            [group setAssetsFilter:assetsFilter];
            if (group.numberOfAssets > 0 || picker.showEmptyGroups)
            {
                
                NSLog(@"ALAssetsGroupPropertyType:%@",[group valueForProperty:ALAssetsGroupPropertyType]);
                
                if ([[group valueForProperty:ALAssetsGroupPropertyType] intValue] == 1)
                {
                    // Library
                }
                else
                {
                    [self.groups addObject:group];
                }

            }
        }
        else
        {
            if (self.groups.count==0) {
                [btnHeader setTitle:[NSString stringWithFormat:@"%@",@"All Photos"] forState:UIControlStateNormal];
            }else{
                self.assetsGroup = [self.groups objectAtIndex:0];
                [btnHeader setTitle:[NSString stringWithFormat:@"%@",[self.assetsGroup valueForProperty:ALAssetsGroupPropertyName]] forState:UIControlStateNormal];
            }
            
            
            CGSize titleSize = [btnHeader.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:17]}];
            btnHeader.frame = CGRectMake(0, 0, titleSize.width + 40, 45);
            [self setupAssets];
            
        }
    };
    
    
    ALAssetsLibraryAccessFailureBlock failureBlock = ^(NSError *error) {
        
        //[self showNotAllowed];
        
    };
    
    // Enumerate Camera roll first
    [self.assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos
                                      usingBlock:resultsBlock
                                    failureBlock:failureBlock];
    
    // Then all other groups
    NSUInteger type =
    ALAssetsGroupLibrary | ALAssetsGroupAlbum | ALAssetsGroupEvent |
    ALAssetsGroupFaces | ALAssetsGroupPhotoStream;
    
    [self.assetsLibrary enumerateGroupsWithTypes:type
                                      usingBlock:resultsBlock
                                    failureBlock:failureBlock];
}
+ (ALAssetsLibrary *)defaultAssetsLibrary
{
    static dispatch_once_t pred = 0;
    static ALAssetsLibrary *library = nil;
    dispatch_once(&pred, ^{
        library = [[ALAssetsLibrary alloc] init];
    });
    return library;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!unFirst) {
        columns=floor(self.view.frame.size.width/(kThumbnailSize.width+minimumInteritemSpacing));
        
        //self.navigationController.navigationBarHidden = YES;
    
        //[self setupAssets];
        
        unFirst=YES;
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[LoadingView sharedLoadingView] stopLoading];

}
#pragma mark - Rotation

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation))
    {
        self.tableView.contentInset=UIEdgeInsetsMake(9.0, 0, 0, 0);
        
        minimumInteritemSpacing=3;
        minimumLineSpacing=3;
    }
    else
    {
        self.tableView.contentInset=UIEdgeInsetsMake(9.0, 0, 0, 0);
        
        minimumInteritemSpacing=2;
        minimumLineSpacing=2;
    }
    
    columns=floor(self.view.frame.size.width/(kThumbnailSize.width+minimumInteritemSpacing));

    [self.tableView reloadData];
}

#pragma mark - Setup

- (void)setupViews
{
//    if( floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) {
//        self.edgesForExtendedLayout= UIRectEdgeNone;
//        [self setNeedsStatusBarAppearanceUpdate];
//        self.navigationController.navigationBar.translucent = NO;
//    }
//    self.view.backgroundColor = [UIColor blackColor];
//    UIImageView *headerView = [[UIImageView alloc] init];
//    headerView.frame  =CGRectMake(0, [UIDevice heightDifference], DeviceWidth, 45);
//    headerView.backgroundColor = [UIColor whiteColor];
//    headerView.userInteractionEnabled = YES;
//    headerView.image =[UIImage imageNamed:@"TopBar"];
//    [self.view addSubview:headerView];
//    
//    headeLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 0, DeviceWidth-LEFTLEFTSET*2, 45) andTextFont:[UIFont boldSystemFontOfSize:16] andTextColor:[UIColor color333333] andTextLines:1];
//    headeLabel.lineBreakMode = NSLineBreakByTruncatingTail;
//    headeLabel.text = @"";
//    headeLabel.textAlignment = NSTextAlignmentCenter;
//    [headerView addSubview:headeLabel];
//    headeLabel.userInteractionEnabled = YES;
//
//    
////    blackTriangleImageView = [[UIImageView alloc] init];
////    blackTriangleImageView.image = [UIImage imageNamed:@"blackTriangle"];
////    blackTriangleImageView.frame = CGRectMake(headeLabel.endPointX + 5, headeLabel.frame.origin.y + 5, 12, 12);
////    [headerView addSubview:blackTriangleImageView];
//    
//    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headLabelClick)];
//    tapGesture.numberOfTouchesRequired = 1;
//    tapGesture.numberOfTapsRequired = 1;
//    [headeLabel addGestureRecognizer:tapGesture];
//    
//
//    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    //[closeButton setImage:[UIImage imageNamed:@"ReviewClosed"] forState:UIControlStateNormal];
//    closeButton.frame = CGRectMake(-5, 0, 80, 44);
//    [closeButton setTitle:@"Cancel" forState:UIControlStateNormal];
//    closeButton.titleLabel.font = [UIFont systemFontOfSize:15];
//    [closeButton setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
//    [headerView addSubview:closeButton];
////    [headerView insertSubview:closeButton aboveSubview:headeLabel];
//    [closeButton addTarget:self action:@selector(closeButtonClick) forControlEvents:UIControlEventTouchUpInside];
//
//    confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    confirmBtn.frame = CGRectMake(DeviceWidth - LEFTLEFTSET - 60, 0, 80, 44);
//    [confirmBtn setTitle:@"Done" forState:UIControlStateNormal];
//    confirmBtn.titleLabel.font = [UIFont systemFontOfSize:15];
////    [confirmBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
//    confirmBtn.userInteractionEnabled = NO;
////    [headerView insertSubview:confirmBtn aboveSubview:headeLabel];
//    [headerView addSubview:confirmBtn];
//    [confirmBtn addTarget:self action:@selector(finishPickingAssets:) forControlEvents:UIControlEventTouchUpInside];
//
    
//    ReviewHeaderView *headeView = [[ReviewHeaderView alloc]initWithTitle:_restaurantTitle andIsNeedWriteReview:NO];
//    [headeView.closeButton addTarget:self action:@selector(closeButtonClick) forControlEvents:UIControlEventTouchUpInside];
//    [self.view addSubview:headeView];
}
- (void)headLabelClick
{
    btnHeader.selected = !btnHeader.selected;
    if (btnHeader.selected) {
        if (backgroundView) {
            [backgroundView removeFromSuperview];
        }
        
        if (chooseGroupView) {
            [chooseGroupView removeFromSuperview];
        }
        
        backgroundView = [[UIView alloc] initWithFrame:self.view.bounds];
        backgroundView.backgroundColor = [UIColor whiteColor];
        [self.view addSubview:backgroundView];
        backgroundView.userInteractionEnabled = YES;
        
        //    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backgroundViewClick)];
        //    tapGesture.numberOfTouchesRequired = 1;
        //    tapGesture.numberOfTapsRequired = 1;
        //    [backgroundView addGestureRecognizer:tapGesture];
        
        
        //    chooseGroupView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, )];
        //    chooseGroupView.frame = CGRectMake(0, self.tableView.frame.origin.y + 5, DeviceWidth, 200);
        //    [self.view addSubview:chooseGroupView];
        //    [chooseGroupView.layer setMasksToBounds:YES];
        //    [chooseGroupView.layer setCornerRadius:5.0];
        
        //    UIImageView *triangleImageView = [[UIImageView alloc] init];
        //    triangleImageView.image = [UIImage imageNamed:@"whiteTriangle"];
        //    triangleImageView.frame = CGRectMake(DeviceWidth/2-8, chooseGroupView.frame.origin.y - 10, 16, 16);
        //    [backgroundView addSubview:triangleImageView];
        
        groupsTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-44)];
        groupsTableView.tag = 101;
        groupsTableView.delegate = self;
        groupsTableView.dataSource = self;
        //    groupsTableView.frame = CGRectMake(0, 0, DeviceWidth, 200);
        [backgroundView addSubview:groupsTableView];
        groupsTableView.rowHeight = 72;
        //    groupsTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        groupsTableView.contentInset=UIEdgeInsetsMake(9.0, 0, 0, 0);
        groupsTableView.separatorStyle = UITableViewCellStyleDefault;
    }else{
        [self backgroundViewClick];
    }
   

}

- (void)backgroundViewClick
{
    [backgroundView removeFromSuperview];
    [chooseGroupView removeFromSuperview];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


#pragma mark - UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 100)
    {
        
    }
    else
    {
        ALAssetsGroup *assetsGroupTemp = [self.groups objectAtIndex:indexPath.row];
        btnHeader.selected = !btnHeader.selected;
        if (assetsGroupTemp == self.assetsGroup)
        {
            [btnHeader setTitle:[NSString stringWithFormat:@"%@",[self.assetsGroup valueForProperty:ALAssetsGroupPropertyName]] forState:UIControlStateNormal];
            
            CGSize titleSize = [btnHeader.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:17]}];
            btnHeader.frame = CGRectMake(0, 0, titleSize.width + 40, 45);
            [self backgroundViewClick];
            return;
        }
        
//        [headeLabel setText:[NSString stringWithFormat:@"%@  ▼",[self.assetsGroup valueForProperty:ALAssetsGroupPropertyName]]];
//        NSRange range = [headeLabel.text rangeOfString:@"▼"];
//        if (range.location != NSNotFound)
//        {
//            [headeLabel setFont:[UIFont boldSystemFontOfSize:17] range:range];
//        }
        
        
        self.assetsGroup = [self.groups objectAtIndex:indexPath.row];
        [btnHeader setTitle:[NSString stringWithFormat:@"%@",[self.assetsGroup valueForProperty:ALAssetsGroupPropertyName]] forState:UIControlStateNormal];
        CGSize titleSize = [btnHeader.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:17]}];
        btnHeader.frame = CGRectMake(0, 0, titleSize.width + 40, 45);
        [self setupAssets];
        [self backgroundViewClick];
    }
    NSLog(@"didselectRow");
    
}



- (UIStatusBarStyle)preferredStatusBarStyle {
    
    return UIStatusBarStyleLightContent;
}
- (BOOL)prefersStatusBarHidden
{
    
    return NO;
}


-(void)closeButtonClick
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)setupButtons
{
//    headeLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 0, DeviceWidth-LEFTLEFTSET*2, 45) andTextFont:[UIFont boldSystemFontOfSize:17] andTextColor:[UIColor color333333] andTextLines:1];
//    headeLabel.lineBreakMode = NSLineBreakByTruncatingTail;
//    headeLabel.text = @"";
//    headeLabel.textAlignment = NSTextAlignmentCenter;
//    
//    headeLabel.userInteractionEnabled = YES;
    
    btnHeader = [UIButton buttonWithType:UIButtonTypeCustom];
    btnHeader.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    [btnHeader setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    [btnHeader setTitle:@"Camera roll" forState:UIControlStateNormal];
     btnHeader.titleLabel.lineBreakMode =  NSLineBreakByTruncatingTail;
    [btnHeader setImage:[UIImage imageNamed:@"ic_city_down"] forState:UIControlStateNormal];
    [btnHeader setImage:[UIImage imageNamed:@"ic_city_up"] forState:UIControlStateSelected];
    btnHeader.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 15);
    [btnHeader.layer setAffineTransform:CGAffineTransformMakeScale(-1, 1)];
    [btnHeader.titleLabel.layer setAffineTransform:CGAffineTransformMakeScale(-1, 1)];
    
    CGSize titleSize = [btnHeader.titleLabel.text sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:17]}];
    btnHeader.frame = CGRectMake(0, 0, titleSize.width + 40, 45);
    
    [btnHeader addTarget:self action:@selector(headLabelClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.titleView = btnHeader;
    
//    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headLabelClick)];
//    tapGesture.numberOfTouchesRequired = 1;
//    tapGesture.numberOfTapsRequired = 1;
//    [headeLabel addGestureRecognizer:tapGesture];
    
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //[closeButton setImage:[UIImage imageNamed:@"ReviewClosed"] forState:UIControlStateNormal];
    closeButton.frame = CGRectMake(0, 0, 80, 44);
    [closeButton setTitle:@"Cancel" forState:UIControlStateNormal];
    closeButton.titleLabel.font = [UIFont systemFontOfSize:17];
    [closeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    closeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    //    [headerView insertSubview:closeButton aboveSubview:headeLabel];
    [closeButton addTarget:self action:@selector(closeButtonClick) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:closeButton];
    
    confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmBtn.frame = CGRectMake(0, 0, 80, 44);
    [confirmBtn setTitle:@"Done" forState:UIControlStateNormal];
    confirmBtn.titleLabel.font = [UIFont systemFontOfSize:17];
    [confirmBtn setTitleColor:[UIColor colorWithHexString:@"09BFD3"] forState:UIControlStateNormal];
    confirmBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    confirmBtn.userInteractionEnabled = NO;
    //    [headerView insertSubview:confirmBtn aboveSubview:headeLabel];
   
    [confirmBtn addTarget:self action:@selector(finishPickingAssets:) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:confirmBtn];
    
}

- (void)setupAssets
{
    //∨∧
//    self.title = [self.assetsGroup valueForProperty:ALAssetsGroupPropertyName];
//    [headeLabel setText:[NSString stringWithFormat:@"Camera roll  ^"]];
//    [btnHeader setTitle:[self.assetsGroup valueForProperty:ALAssetsGroupPropertyName] forState:UIControlStateNormal];
    //NSRange range = [headeLabel.text rangeOfString:@"▼"];
//    if (range.location != NSNotFound)
//    {
//        [headeLabel setFont:[UIFont boldSystemFontOfSize:17] range:range];
//    }
    self.numberOfPhotos = 0;
    self.numberOfVideos = 0;
    
    if (!self.assets)
        self.assets = [[NSMutableArray alloc] init];
    else
        [self.assets removeAllObjects];
    
    [_indexPathsForSelectedItems removeAllObjects];
    
    if (_indexPathsForSelectedItems.count == 0)
    {
//        [confirmBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        confirmBtn.userInteractionEnabled = NO;
    }
    
    ALAssetsGroupEnumerationResultsBlock resultsBlock = ^(ALAsset *asset, NSUInteger index, BOOL *stop) {
    
        
        if (asset)
        {
            [self.assets addObject:asset];
            NSString *type = [asset valueForProperty:ALAssetPropertyType];
            
            if ([type isEqual:ALAssetTypePhoto])
                self.numberOfPhotos ++;
            if ([type isEqual:ALAssetTypeVideo])
                self.numberOfVideos ++;
        }
        
        else if (self.assets.count > 0)
        {
            [self.tableView reloadData];

            //[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:ceil(self.assets.count*1.0/columns)  inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        }
    };
    //[self.assets addObject:[self.assets lastObject]];
    
//    ALAsset *newAsset = [self.assets objectAtIndex:0];
//    UIImage *image = [UIImage imageNamed:@"share2"];
//    [newAsset setImageData:UIImageJPEGRepresentation(image,1.0) metadata:nil completionBlock:^(NSURL *assetURL, NSError *error) {
//        
//    }];
//    [newAsset writeModifiedImageDataToSavedPhotosAlbum:UIImageJPEGRepresentation(image, 1.0) metadata:nil completionBlock:nil];
//    [self.assets addObject:newAsset];
   
    [self.assetsGroup enumerateAssetsUsingBlock:resultsBlock];
    NSArray *tempAssets = [[self.assets reverseObjectEnumerator] allObjects];
    self.assets = [NSMutableArray arrayWithArray:tempAssets];
}

#pragma mark - UITableView DataSource
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag == 100)
    {
        if (indexPath.row==ceil(self.assets.count*1.0/columns)+1) {
            UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cellFooter"];
            
            if (cell==nil) {
                cell=[[ChooseViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellFooter"];
                
            }
            
            
            cell.tag = 10000;
            
            return cell;
        }
        
        
        NSMutableArray *tempAssets=[[NSMutableArray alloc] init];
        if (indexPath.row == 0)
        {
            for (int i=0; i<columns-1; i++) {
                if ((indexPath.row*columns+i)<self.assets.count) {
                    [tempAssets addObject:[self.assets objectAtIndex:indexPath.row*columns+i]];
                }
            }
        }
        else
        {
            for (int i=0; i<columns; i++) {
                if ((indexPath.row*columns+i)<self.assets.count+1) {
                    [tempAssets addObject:[self.assets objectAtIndex:indexPath.row*columns+i-1]];
                }
            }
        }
        
        
        //[tempAssets addObject:newAsset];
        
        static NSString *CellIdentifier = kAssetViewCellIdentifier;
        ZYQAssetPickerController *picker = (ZYQAssetPickerController *)self.navigationController;
        
        ZYQAssetViewCell *cell = [[ZYQAssetViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        //[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell==nil) {
            cell=[[ZYQAssetViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.delegate=self;
        
        if (indexPath.row == 0)
        {
            [cell bind:tempAssets selectionFilter:picker.selectionFilter minimumInteritemSpacing:minimumInteritemSpacing minimumLineSpacing:minimumLineSpacing columns:columns assetViewX:(self.tableView.frame.size.width-kThumbnailSize.width-minimumInteritemSpacing*(tempAssets.count-1))/2];
        }
        else
            [cell bind:tempAssets selectionFilter:picker.selectionFilter minimumInteritemSpacing:minimumInteritemSpacing minimumLineSpacing:minimumLineSpacing columns:columns assetViewX:(self.tableView.frame.size.width-kThumbnailSize.width*3-minimumInteritemSpacing*2)/2];
        
        NSLog(@"self.tableView.frame.size.width = %f",self.tableView.frame.size.width);
        NSLog(@"kThumbnailSize.width = %lf",kThumbnailSize.width);
        NSLog(@"tempAssets.count = %d",(int)tempAssets.count);
        NSLog(@"minimumInteritemSpacing = %lf",minimumInteritemSpacing);
        
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"Cell";
        
        ZYQAssetGroupViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[ZYQAssetGroupViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        cell.assetsGroup = self.assetsGroup;
        [cell bind:[self.groups objectAtIndex:indexPath.row]];
        
        return cell;
    }
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView.tag == 100)
    {
        NSLog(@"section = %f",ceil(self.assets.count*1.0/columns)+1);
        return ceil(self.assets.count*1.0/columns)+1;
    }
    else{
        if (self.groups.count == 1)
        {
            tableView.frame = CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, kThumbnailLength/2 + 12 + 5);
            chooseGroupView.frame = CGRectMake(chooseGroupView.frame.origin.x, chooseGroupView.frame.origin.y, chooseGroupView.frame.size.width, tableView.frame.size.height);
            
        }else if (self.groups.count == 2)
        {
            tableView.frame = CGRectMake(tableView.frame.origin.x, tableView.frame.origin.y, tableView.frame.size.width, (kThumbnailLength/2 + 12)*2 + 5);
            chooseGroupView.frame = CGRectMake(chooseGroupView.frame.origin.x, chooseGroupView.frame.origin.y, chooseGroupView.frame.size.width, tableView.frame.size.height);
        }
        
        return self.groups.count;
    }
}


#pragma mark - UITableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag == 100)
    {
        if (indexPath.row==ceil(self.assets.count*1.0/columns)) {
            return 80;
        }
        return kThumbnailSize.height+minimumLineSpacing;
    }
    else
    {
        return kThumbnailLength/2 + 12;
    }
    
}


#pragma mark - ZYQAssetViewCell Delegate

- (BOOL)shouldSelectAsset:(ALAsset *)asset
{
    ZYQAssetPickerController *vc = (ZYQAssetPickerController *)self.navigationController;
    BOOL selectable = [vc.selectionFilter evaluateWithObject:asset];
    if (_indexPathsForSelectedItems.count > vc.maximumNumberOfSelection) {
        if (vc.delegate!=nil&&[vc.delegate respondsToSelector:@selector(assetPickerControllerDidMaximum:)]) {
            [vc.delegate assetPickerControllerDidMaximum:vc];
        }
    }
    
    return (selectable && _indexPathsForSelectedItems.count < vc.maximumNumberOfSelection);
}

- (void)didSelectAsset:(ALAsset *)asset
{
    [_indexPathsForSelectedItems addObject:asset];
    
    if (_indexPathsForSelectedItems.count)
    {
        [confirmBtn setTitleColor:[UIColor colorWithHexString:@"09BFD3"] forState:UIControlStateNormal];
        confirmBtn.userInteractionEnabled = YES;
    }
    
    ZYQAssetPickerController *vc = (ZYQAssetPickerController *)self.navigationController;
    vc.indexPathsForSelectedItems = _indexPathsForSelectedItems;
    
    if (vc.delegate!=nil&&[vc.delegate respondsToSelector:@selector(assetPickerController:didSelectAsset:)])
        [vc.delegate assetPickerController:vc didSelectAsset:asset];
    
    [self setTitleWithSelectedIndexPaths:_indexPathsForSelectedItems];
}

- (void)didDeselectAsset:(ALAsset *)asset
{
    [_indexPathsForSelectedItems removeObject:asset];
    
    if (_indexPathsForSelectedItems.count == 0)
    {
//        [confirmBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        confirmBtn.userInteractionEnabled = NO;
    }
    
    ZYQAssetPickerController *vc = (ZYQAssetPickerController *)self.navigationController;
    vc.indexPathsForSelectedItems = _indexPathsForSelectedItems;
    
    if (vc.delegate!=nil&&[vc.delegate respondsToSelector:@selector(assetPickerController:didDeselectAsset:)])
        [vc.delegate assetPickerController:vc didDeselectAsset:asset];
    
    [self setTitleWithSelectedIndexPaths:_indexPathsForSelectedItems];
}


#pragma mark - Title

- (void)setTitleWithSelectedIndexPaths:(NSArray *)indexPaths
{
    // Reset title to group name
    if (indexPaths.count == 0)
    {
        self.title = [self.assetsGroup valueForProperty:ALAssetsGroupPropertyName];
        return;
    }
    
    BOOL photosSelected = NO;
    BOOL videoSelected  = NO;
    
    for (int i=0; i<indexPaths.count; i++) {
        ALAsset *asset = indexPaths[i];
        
        if ([[asset valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypePhoto])
            photosSelected  = YES;
        
        if ([[asset valueForProperty:ALAssetPropertyType] isEqual:ALAssetTypeVideo])
            videoSelected   = YES;
        
        if (photosSelected && videoSelected)
            break;

    }
    
    NSString *format;
    
    if (photosSelected && videoSelected)
        format = NSLocalizedString(@"已选择 %ld 个项目", nil);
    
    else if (photosSelected)
        format = (indexPaths.count > 1) ? NSLocalizedString(@"已选择 %ld 张照片", nil) : NSLocalizedString(@"已选择 %ld 张照片 ", nil);
    
    else if (videoSelected)
        format = (indexPaths.count > 1) ? NSLocalizedString(@"已选择 %ld 部视频", nil) : NSLocalizedString(@"已选择 %ld 部视频 ", nil);
    
    self.title = [NSString stringWithFormat:format, (long)indexPaths.count];
}


#pragma mark - Actions

- (void)finishPickingAssets:(id)sender
{
    ZYQAssetPickerController *picker = (ZYQAssetPickerController *)self.navigationController;
    picker.isFromUploadPhoto = self.isFromUploadPhoto;
    picker.isUploadMenuPhoto = self.isUploadMenuPhoto;
    
    if (_indexPathsForSelectedItems.count < picker.minimumNumberOfSelection) {
        if (picker.delegate!=nil&&[picker.delegate
                                     respondsToSelector:@selector(assetPickerControllerDidMaximum:)]) {
            [picker.delegate assetPickerControllerDidMaximum:picker];
        }
    }
    

    if ([picker.delegate respondsToSelector:@selector(assetPickerController:didFinishPickingAssets:)])
    {
        [picker.delegate assetPickerController:picker didFinishPickingAssets:_indexPathsForSelectedItems];
    }else
    {
        [picker.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
        
    }
}

@end

#pragma mark - ZYQAssetGroupViewCell

@interface ZYQAssetGroupViewCell ()
{
    UIImageView *imgGroup;
    UILabel *lblGroupName;
    UILabel *lblPhotoCount;
    UIImageView *imgCheck;
}


@end

@implementation ZYQAssetGroupViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    imgGroup = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 50, 50)];
    imgGroup.contentMode = UIViewContentModeScaleAspectFill;
    imgGroup.clipsToBounds = YES;
    [self.contentView addSubview:imgGroup];
    
    lblGroupName = [[UILabel alloc] initWithFrame:CGRectMake(imgGroup.endPointX+15, 17, DeviceWidth - 100, 16)];
    lblGroupName.font = [UIFont systemFontOfSize:14];
    lblGroupName.textColor = [UIColor color333333];
    [self.contentView addSubview:lblGroupName];
    
    
    lblPhotoCount = [[UILabel alloc] initWithFrame:CGRectMake(imgGroup.endPointX + 15, lblGroupName.endPointY + 4, DeviceWidth-100, 14)];
    lblPhotoCount.font = [UIFont systemFontOfSize:12];
    lblPhotoCount.textColor = [UIColor color999999];
    [self.contentView addSubview:lblPhotoCount];
    
    UIView *bottomLine = [[UIView alloc] initWithFrame:CGRectMake(15, 71, DeviceWidth, 1)];
    bottomLine.backgroundColor = [UIColor colorCCCCCC];
    [self.contentView addSubview:bottomLine];
    
    imgCheck = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-30, (71-12)/2, 13, 12)];
    imgCheck.hidden = YES;
    imgCheck.image = [UIImage imageNamed:@"ic_Checkmark1"];
    [self.contentView addSubview:imgCheck];
}

- (void)bind:(ALAssetsGroup *)assetsGroup
{
    if (self.assetsGroup !=nil && self.assetsGroup == assetsGroup) {
        imgCheck.hidden = NO;
    }else{
        imgCheck.hidden = YES;
    }
    self.assetsGroup            = assetsGroup;
    
    CGImageRef posterImage      = assetsGroup.posterImage;
    size_t height               = CGImageGetHeight(posterImage);
    float scale                 = height / kThumbnailLength;
    
    imgGroup.image = [UIImage imageWithCGImage:posterImage];
    lblGroupName.text = [assetsGroup valueForProperty:ALAssetsGroupPropertyName];
    lblPhotoCount.text = [NSString stringWithFormat:@"%ld", (long)[assetsGroup numberOfAssets]];
//    self.imageView.image        = [UIImage imageWithCGImage:posterImage scale:scale orientation:UIImageOrientationUp];
//    self.textLabel.text         = [assetsGroup valueForProperty:ALAssetsGroupPropertyName];
//    self.detailTextLabel.text   = [NSString stringWithFormat:@"%ld", (long)[assetsGroup numberOfAssets]];
//    self.accessoryType          = UITableViewCellAccessoryDisclosureIndicator;
}

- (NSString *)accessibilityLabel
{
    NSString *label = [self.assetsGroup valueForProperty:ALAssetsGroupPropertyName];
    
    return [label stringByAppendingFormat:NSLocalizedString(@"%ld 张照片", nil), (long)[self.assetsGroup numberOfAssets]];
}

@end


#pragma mark - ZYQAssetGroupViewController

#import "ReviewHeaderView.h"

@interface ZYQAssetGroupViewController()<UITableViewDataSource,UITableViewDelegate>


@property (nonatomic, strong) ALAssetsLibrary *assetsLibrary;
@property (nonatomic, strong) NSMutableArray *groups;

@end

@implementation ZYQAssetGroupViewController

//- (id)init
//{
//    if (self = [super initWithStyle:UITableViewStylePlain])
//    {
//#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
//        self.preferredContentSize=kPopoverContentSize;
//#else
//        if ([self respondsToSelector:@selector(setContentSizeForViewInPopover:)])
//            [self setContentSizeForViewInPopover:kPopoverContentSize];
//#endif
//    }
//    
//    return self;
//}

- (instancetype)init
{
    self = [super init];
    if (self) {
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
        self.preferredContentSize=kPopoverContentSize;
#else
        if ([self respondsToSelector:@selector(setContentSizeForViewInPopover:)])
            [self setContentSizeForViewInPopover:kPopoverContentSize];
#endif

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self localize];
    [self setupViews];
    [self setupButtons];
    [self setupGroup];
}


#pragma mark - Rotation

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}


#pragma mark - Setup

- (void)setupViews
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    self.view.backgroundColor = [UIColor blackColor];
    self.tableView = [[UITableView alloc] init];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight - 44);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    self.tableView.rowHeight = kThumbnailLength + 12;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)setupButtons
{
    
    
    ZYQAssetPickerController *picker = (ZYQAssetPickerController *)self.navigationController;
    
    if (picker.showCancelButton)
    {
//        self.navigationItem.rightBarButtonItem =
//        [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"取消", nil)
//                                         style:UIBarButtonItemStylePlain
//                                        target:self
//                                        action:@selector(dismiss:)];
        self.navigationController.navigationBarHidden = YES;
    }
}

- (void)localize
{
    //self.title = NSLocalizedString(@"相簿", nil);
    ReviewHeaderView *headeView = [[ReviewHeaderView alloc]initWithTitle:_restaurantTitle andIsNeedWriteReview:NO];
    [headeView.closeButton addTarget:self action:@selector(closeButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:headeView];
}
-(void)closeButtonClick
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (void)setupGroup
{
    if (!self.assetsLibrary)
        self.assetsLibrary = [self.class defaultAssetsLibrary];
    
    if (!self.groups)
        self.groups = [[NSMutableArray alloc] init];
    else
        [self.groups removeAllObjects];
    
    ZYQAssetPickerController *picker = (ZYQAssetPickerController *)self.navigationController;
    ALAssetsFilter *assetsFilter = picker.assetsFilter;
    
    ALAssetsLibraryGroupsEnumerationResultsBlock resultsBlock = ^(ALAssetsGroup *group, BOOL *stop) {
        
        if (group)
        {
            [group setAssetsFilter:assetsFilter];
            if (group.numberOfAssets > 0 || picker.showEmptyGroups)
                [self.groups addObject:group];
        }
        else
        {
            [self reloadData];
//            //setupAssetsNotification
//            NSMutableDictionary* faDictonary = [[NSMutableDictionary alloc] init];
//            [faDictonary setValue:[self.groups objectAtIndex:0] forKey:@"assetsGroup"];
//
        }
    };
    
    
    ALAssetsLibraryAccessFailureBlock failureBlock = ^(NSError *error) {
        
        //[self showNotAllowed];
        
    };
    
    // Enumerate Camera roll first
    [self.assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos
                                      usingBlock:resultsBlock
                                    failureBlock:failureBlock];
    
    // Then all other groups
    NSUInteger type =
    ALAssetsGroupLibrary | ALAssetsGroupAlbum | ALAssetsGroupEvent |
    ALAssetsGroupFaces | ALAssetsGroupPhotoStream;
    
    [self.assetsLibrary enumerateGroupsWithTypes:type
                                      usingBlock:resultsBlock
                                    failureBlock:failureBlock];
}


#pragma mark - Reload Data

- (void)reloadData
{
    if (self.groups.count == 0)
        [self showNoAssets];
    
    [self.tableView reloadData];
}


#pragma mark - ALAssetsLibrary

+ (ALAssetsLibrary *)defaultAssetsLibrary
{
    static dispatch_once_t pred = 0;
    static ALAssetsLibrary *library = nil;
    dispatch_once(&pred, ^{
        library = [[ALAssetsLibrary alloc] init];
    });
    return library;
}


#pragma mark - Not allowed / No assets

- (void)showNotAllowed
{
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
        [self setEdgesForExtendedLayout:UIRectEdgeLeft | UIRectEdgeRight | UIRectEdgeBottom];
    
    self.title              = nil;
    
    UIImageView *padlock    = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"ZYQAssetPicker.Bundle/Images/AssetsPickerLocked@2x.png"]]];
    padlock.translatesAutoresizingMaskIntoConstraints = NO;
    
    UILabel *title          = [UILabel new];
    title.translatesAutoresizingMaskIntoConstraints = NO;
    title.preferredMaxLayoutWidth = 304.0f;
    
    UILabel *message        = [UILabel new];
    message.translatesAutoresizingMaskIntoConstraints = NO;
    message.preferredMaxLayoutWidth = 304.0f;
    
    title.text              = NSLocalizedString(@"error", nil);
    title.font              = [UIFont boldSystemFontOfSize:17.0];
    title.textColor         = [UIColor colorWithRed:129.0/255.0 green:136.0/255.0 blue:148.0/255.0 alpha:1];
    title.textAlignment     = NSTextAlignmentCenter;
    title.numberOfLines     = 5;
    
    message.text            = NSLocalizedString(@"error", nil);
    message.font            = [UIFont systemFontOfSize:14.0];
    message.textColor       = [UIColor colorWithRed:129.0/255.0 green:136.0/255.0 blue:148.0/255.0 alpha:1];
    message.textAlignment   = NSTextAlignmentCenter;
    message.numberOfLines   = 5;
    
    [title sizeToFit];
    [message sizeToFit];
    
    UIView *centerView = [UIView new];
    centerView.translatesAutoresizingMaskIntoConstraints = NO;
    [centerView addSubview:padlock];
    [centerView addSubview:title];
    [centerView addSubview:message];
    
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(padlock, title, message);
    
    [centerView addConstraint:[NSLayoutConstraint constraintWithItem:padlock attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:centerView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [centerView addConstraint:[NSLayoutConstraint constraintWithItem:title attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:padlock attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [centerView addConstraint:[NSLayoutConstraint constraintWithItem:message attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:padlock attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [centerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[padlock]-[title]-[message]|" options:0 metrics:nil views:viewsDictionary]];
    
    UIView *backgroundView = [UIView new];
    [backgroundView addSubview:centerView];
    [backgroundView addConstraint:[NSLayoutConstraint constraintWithItem:centerView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:backgroundView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [backgroundView addConstraint:[NSLayoutConstraint constraintWithItem:centerView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:backgroundView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    
    self.tableView.backgroundView = backgroundView;
}

- (void)showNoAssets
{
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
        [self setEdgesForExtendedLayout:UIRectEdgeLeft | UIRectEdgeRight | UIRectEdgeBottom];
    
    UILabel *title          = [UILabel new];
    title.translatesAutoresizingMaskIntoConstraints = NO;
    title.preferredMaxLayoutWidth = 304.0f;
    UILabel *message        = [UILabel new];
    message.translatesAutoresizingMaskIntoConstraints = NO;
    message.preferredMaxLayoutWidth = 304.0f;
    
    title.text              = NSLocalizedString(@"no photo", nil);
    title.font              = [UIFont systemFontOfSize:26.0];
    title.textColor         = [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1];
    title.textAlignment     = NSTextAlignmentCenter;
    title.numberOfLines     = 5;
    
    message.text            = NSLocalizedString(@"error", nil);
    message.font            = [UIFont systemFontOfSize:18.0];
    message.textColor       = [UIColor colorWithRed:153.0/255.0 green:153.0/255.0 blue:153.0/255.0 alpha:1];
    message.textAlignment   = NSTextAlignmentCenter;
    message.numberOfLines   = 5;
    
    [title sizeToFit];
    [message sizeToFit];
    
    UIView *centerView = [UIView new];
    centerView.translatesAutoresizingMaskIntoConstraints = NO;
    [centerView addSubview:title];
    [centerView addSubview:message];
    
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(title, message);
    
    [centerView addConstraint:[NSLayoutConstraint constraintWithItem:title attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:centerView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [centerView addConstraint:[NSLayoutConstraint constraintWithItem:message attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:title attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [centerView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[title]-[message]|" options:0 metrics:nil views:viewsDictionary]];
    
    UIView *backgroundView = [UIView new];
    [backgroundView addSubview:centerView];
    [backgroundView addConstraint:[NSLayoutConstraint constraintWithItem:centerView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:backgroundView attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f]];
    [backgroundView addConstraint:[NSLayoutConstraint constraintWithItem:centerView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:backgroundView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
    
    self.tableView.backgroundView = backgroundView;
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.groups.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    ZYQAssetGroupViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[ZYQAssetGroupViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    [cell bind:[self.groups objectAtIndex:indexPath.row]];
    
    return cell;
}


#pragma mark - UITableView Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kThumbnailLength + 12;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZYQAssetViewController *vc = [[ZYQAssetViewController alloc] initWithRestaurantTitle:_restaurantTitle];
    vc.assetsGroup = [self.groups objectAtIndex:indexPath.row];
    vc.isFromUploadPhoto = self.isFromUploadPhoto;
    vc.isUploadMenuPhoto = self.isUploadMenuPhoto;
    vc.restaurantID = self.restaurantId;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Actions

- (void)dismiss:(id)sender
{
    ZYQAssetPickerController *picker = (ZYQAssetPickerController *)self.navigationController;
    
    if ([picker.delegate respondsToSelector:@selector(assetPickerControllerDidCancel:)])
        [picker.delegate assetPickerControllerDidCancel:picker];
    
    [picker.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
}

@end

#pragma mark - ZYQAssetPickerController

@implementation ZYQAssetPickerController
- (id)initWithRestaurantTitle:(NSString *)title andIsFromUploadPhoto:(BOOL)isFromUploadPhoto andIsUploadMenuPhoto:(BOOL)isUploadMenuPhoto andCurrentRestaurantId:(NSNumber *)restaurantId  andIsreview:(BOOL)isReview
{
    ZYQAssetViewController *vc = [[ZYQAssetViewController alloc] initWithRestaurantTitle:title];
    //vc.assetsGroup = [groupViewController.groups objectAtIndex:0];
    vc.isFromUploadPhoto = isFromUploadPhoto;
    vc.isUploadMenuPhoto = isUploadMenuPhoto;
    vc.restaurantID = restaurantId;
    vc.isReview = isReview;
    if (self = [super initWithRootViewController:vc])
    {
        _maximumNumberOfSelection      = 10;
        _minimumNumberOfSelection      = 0;
        _assetsFilter                  = [ALAssetsFilter allAssets];
        _showCancelButton              = YES;
        _showEmptyGroups               = NO;
        _selectionFilter               = [NSPredicate predicateWithValue:YES];
        _isFinishDismissViewController = YES;
        
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
        self.preferredContentSize=kPopoverContentSize;
#else
        if ([self respondsToSelector:@selector(setContentSizeForViewInPopover:)])
            [self setContentSizeForViewInPopover:kPopoverContentSize];
#endif
    }
    
    return self;


}
- (id)initWithRestaurantTitle:(NSString *)title andIsFromUploadPhoto:(BOOL)isFromUploadPhoto andIsUploadMenuPhoto:(BOOL)isUploadMenuPhoto andCurrentRestaurantId:(NSNumber *)restaurantId andRestaurant:(IMGRestaurant*)restraurant
{
    ZYQAssetViewController *vc = [[ZYQAssetViewController alloc] initWithRestaurantTitle:title];
    //vc.assetsGroup = [groupViewController.groups objectAtIndex:0];
    vc.isFromUploadPhoto = isFromUploadPhoto;
    vc.isUploadMenuPhoto = isUploadMenuPhoto;
    vc.restaurantID = restaurantId;
    vc.restaurant = restraurant;
    if (self = [super initWithRootViewController:vc])
    {
        _maximumNumberOfSelection      = 10;
        _minimumNumberOfSelection      = 0;
        _assetsFilter                  = [ALAssetsFilter allAssets];
        _showCancelButton              = YES;
        _showEmptyGroups               = NO;
        _selectionFilter               = [NSPredicate predicateWithValue:YES];
        _isFinishDismissViewController = YES;
        
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
        self.preferredContentSize=kPopoverContentSize;
#else
        if ([self respondsToSelector:@selector(setContentSizeForViewInPopover:)])
            [self setContentSizeForViewInPopover:kPopoverContentSize];
#endif
    }
    
    return self;
    
    
}

- (id)initWithRestaurantTitle:(NSString *)title andIsFromUploadPhoto:(BOOL)isFromUploadPhoto andIsUploadMenuPhoto:(BOOL)isUploadMenuPhoto andCurrentRestaurantId:(NSNumber *)restaurantId 
{
//    ZYQAssetGroupViewController *groupViewController = [[ZYQAssetGroupViewController alloc] init];
//    groupViewController.restaurantTitle = title;
//    groupViewController.isFromUploadPhoto = isFromUploadPhoto;
//    groupViewController.restaurantId = restaurantId;
//    [groupViewController setupGroup];
    
    ZYQAssetViewController *vc = [[ZYQAssetViewController alloc] initWithRestaurantTitle:title];
    //vc.assetsGroup = [groupViewController.groups objectAtIndex:0];
    vc.isFromUploadPhoto = isFromUploadPhoto;
    vc.isUploadMenuPhoto = isUploadMenuPhoto;
    
    if (restaurantId == nil) {
        vc.isCanSearch = YES;
    }
    vc.restaurantID = restaurantId;
    vc.restaurant = self.restaurant;
    if (self = [super initWithRootViewController:vc])
    {
        _maximumNumberOfSelection      = 10;
        _minimumNumberOfSelection      = 0;
        _assetsFilter                  = [ALAssetsFilter allAssets];
        _showCancelButton              = YES;
        _showEmptyGroups               = NO;
        _selectionFilter               = [NSPredicate predicateWithValue:YES];
        _isFinishDismissViewController = YES;
        
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
        self.preferredContentSize=kPopoverContentSize;
#else
        if ([self respondsToSelector:@selector(setContentSizeForViewInPopover:)])
            [self setContentSizeForViewInPopover:kPopoverContentSize];
#endif
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

 

@end
