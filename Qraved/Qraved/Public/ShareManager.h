//
//  ShareManager.h
//  Qraved
//
//  Created by Laura on 7/17/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShareManager : NSObject
@property(nonatomic,retain)NSString *eventLogo;
+ (ShareManager *)sharedEvent;
@end
