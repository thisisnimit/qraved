// Copyright (c) 2009 Imageshack Corp.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#import "LocationManager.h"
static LocationManager *globalLocationManager = nil;
static BOOL initialized = NO;

@implementation LocationManager

@synthesize tinyURL;


+ (LocationManager*)locationManager
{
//	if(!globalLocationManager) 
//		globalLocationManager = [[LocationManager allocWithZone:nil] init];
//	return globalLocationManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        globalLocationManager=[[LocationManager allocWithZone:nil] init];
    });
    
    return globalLocationManager;
    
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
	{
		if (globalLocationManager == nil) 
			globalLocationManager = [super allocWithZone:zone];
    }
	
    return globalLocationManager;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}


-(void)reset
{
	locationDefined = NO;
	latitude = 0.f;
	longitude = 0.f;
}

- (id)init
{
	if(initialized)
		return globalLocationManager;
	
	self = [super init];
    if (!self)
	{
		return nil;
	}

	
	locationManager = nil;
	initialized = YES;
	locationDenied = NO;
	[self reset];
    return self;
}

- (void) stopUpdates
{
	if (locationManager)
	{
		[locationManager stopUpdatingLocation];
	}
	[self reset];
}

- (void) startUpdates
{

		locationManager = [[CLLocationManager alloc] init];
		locationManager.delegate = self;
		locationManager.distanceFilter = 100;
		locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;

    
	[locationManager startUpdatingLocation];
}


- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation  fromLocation:(CLLocation *)oldLocation
{
    // If it's a relatively recent event, turn off updates to save power
    NSDate* eventDate = newLocation.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (fabs(howRecent) < 15.0)
    {
        
        [self getThePlace:newLocation];
        [self stopUpdates];
    }

}

- (BOOL) locationDenied
{
	return locationDenied;
}

- (BOOL) locationServicesEnabled
{

    return YES;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	[self reset];

    if ([error domain] == kCLErrorDomain) 
	{
        switch ([error code]) 
		{
            case kCLErrorDenied:
				locationDenied = YES;
				[self stopUpdates];
                break;
            case kCLErrorLocationUnknown:
                break;
            default:
                break;
        }
	}
	
}

-(void)getThePlace:(CLLocation *)location
{

    [[ NSNotificationCenter defaultCenter] postNotificationName:@"refreshLocation" object:location];
        
    
}

- (BOOL) locationDefined
{
	return locationDefined;
}

- (float) latitude
{
	return latitude;
}

- (float) longitude
{
	return longitude;
}
- (NSString*) mapURL
{
    return @"";
}
- (NSString*) longURL
{
    return @"";
}

- (void) setTinyURLOnMainThread:(NSArray*)urls
{
NSString* tiny = [urls objectAtIndex:0] == [NSNull null] ? nil : [urls objectAtIndex:0];
	NSString* full =[urls objectAtIndex:1];
	if([full isEqualToString:[self longURL]] || !self.tinyURL)
		self.tinyURL = tiny;
}
@end
