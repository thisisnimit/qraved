//
//  SimpleLoadingImageView.m
//  Qraved
//
//  Copyright (c) 2016年 Imaginato. All rights reserved.
//

#import "SimpleLoadingImageView.h"
#import "UIConstants.h"

@implementation SimpleLoadingImageView{
    UIImageView *imageView;
    UIActivityIndicatorView *activityView;
}

-(void)startLoading {
    [activityView startAnimating];
}

-(void)stopLoading {
    [activityView stopAnimating];
    [self removeFromSuperview];
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        imageView.image = [UIImage imageNamed:DEFAULT_IMAGE_STRING2];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        [imageView setClipsToBounds: YES];
        [self addSubview:imageView];
        
        activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        activityView.center = CGPointMake(frame.size.width/2.0, frame.size.height/2.0);
        [imageView addSubview:activityView];
        [activityView startAnimating];
    }
    return self;
}

-(void)layoutSubviews{
    [super layoutSubviews];
    imageView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    activityView.center = CGPointMake(imageView.frame.size.width/2.0, imageView.frame.size.height/2.0);
}

@end
