//
//  ShareManager.m
//  Qraved
//
//  Created by Laura on 7/17/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "ShareManager.h"

@implementation ShareManager
+ (ShareManager *)sharedEvent
{
    static ShareManager *manager = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        manager = [[self alloc] init];
    });
    return manager;
}
@end
