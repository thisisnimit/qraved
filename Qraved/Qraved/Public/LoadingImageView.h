//
//  LoadingImageView.h
//  Qraved
//
//  Created by Laura on 14/12/23.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingImageView : UIView
-(void)startLoading;
-(void)stopLoading;
- (id)initWithFrame:(CGRect)frame andViewNum:(NSInteger)num andShadowWidth:(CGFloat)width;

@end
