//
//  ImageSizeENUM.h
//  Qraved
//
//  Created by harry on 2017/9/6.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#ifndef ImageSizeENUM_h
#define ImageSizeENUM_h

typedef NS_ENUM(NSInteger, ImageSizeType)
{
    ImageSizeTypeNone          = 0,
    ImageSizeTypeLowResolution = 1,
    ImageSizeTypeThumbnail     = 2,
    ImageSizeTypeStandard      = 3,
};
#endif /* ImageSizeENUM_h */
