//
//  IMGAmplitudeUtil.h
//  Qraved
//
//  Created by harry on 2017/11/27.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IMGAmplitudeUtil : NSObject

+ (void)trackRestaurantWithName:(NSString *)name andRestaurantId:(NSNumber *)restaurantId andRestaurantTitle:(NSString *)restaurantTitle andLocation:(NSString *)location andOrigin:(NSString *)origin andType:(NSString *)typeName;

+ (void)trackPhotoWithName:(NSString *)name andRestaurantId:(NSNumber *)restaurantId andRestaurantTitle:(NSString *)restaurantTitle andOrigin:(NSString *)origin andUploaderUser_ID:(NSNumber *)uploadUserId andPhoto_ID:(NSNumber *)photoId andRestaurantMenu_ID:(NSNumber *)menuId andRestaurantEvent_ID:(NSNumber *)eventId andPhotoSource:(NSString *)photoSource andLocation:(NSString *)location;

+ (void)trackSavedWithName:(NSString *)name andRestaurantId:(NSNumber *)restaurantId andRestaurantTitle:(NSString *)restaurantTitle andOrigin:(NSString *)origin andListId:(NSNumber *)listId andType:(NSString *)type andLocation:(NSString *)location;

+ (void)trackReviewWithName:(NSString *)name andRestaurantId:(NSNumber *)restaurantId andRatingValue:(NSNumber *)ratingValue andReviewId:(NSNumber *)reviewId andReviewUserId:(NSNumber *)reviewUserId andRatingId:(NSNumber *)ratingId andUploadUserId:(NSNumber *)uploadUserId andSource:(NSString *)source andType:(NSString *)type andOrigin:(NSString *)origin andValue:(NSString *)value;

+ (void)trackGuideWithName:(NSString *)name andDiningGuideId:(NSNumber *)guideId andDiningGuideName:(NSString *)guideName andOrigin:(NSString *)origin;

+ (void)trackJournalListWithName:(NSString *)name andSorting:(NSString *)sorting andFilter:(NSNumber *)filter andOrigin:(NSString *)origin;

+ (void)trackRestaurantInGuideWithName:(NSString *)name andDiningGuideId:(NSNumber *)guideId andOrigin:(NSString *)origin andRestaurantId:(NSNumber *)restaurantId andRestaurantTitle:(NSString *)restaurantTitle;

+ (void)trackJDPWithName:(NSString *)name andJournalId:(NSNumber *)journalId andOrigin:(NSString *)origin andChannel:(NSString *)channel andLocation:(NSString *)location;

+ (void)trackJDPPhotoWithName:(NSString *)name andJournalId:(NSNumber *)journalId andOrigin:(NSString *)origin andPhotoId:(NSNumber *)photoId;

+ (void)trackRDPWithName:(NSString *)name andRestaurantId:(NSNumber *)restaurantId andOrigin:(NSString *)origin andLocation:(NSString *)location;

+ (void)trackReviewViewWithName:(NSString *)name andRestaurantId:(NSNumber *)restaurantId andReviewId:(NSNumber *)reviewId andReviewUserId:(NSString *)reviewUserId andOrigin:(NSString *)origin andLocation:(NSString *)location andChannel:(NSString *)channel;

+ (void)trackPromoWithName:(NSString *)name andRestaurantId:(NSNumber *)restaurantId andPromoId:(NSNumber *)promoId;

+ (void)trackSignInWithName:(NSString *)name andType:(NSString *)type andLocation:(NSString *)location andReason:(NSString *)reason;

+ (void)trackPreferenceWithName:(NSString *)name andEatPreference:(NSString *)eatPreference andCuisinePreference:(NSString *)cuisinePreference andCuisineName:(NSString *)cuisineName andBrandName:(NSString *)brandName andOrigin:(NSString *)origin;

+ (void)trackBookWithName:(NSString *)name andRestaurantId:(NSNumber *)restaurantId andReservationId:(NSNumber *)reservationId andLocation:(NSString *)location andChannel:(NSString *)channel;

+ (void)trackProfileWithName:(NSString *)name andUserId:(NSNumber *)userId andLocation:(NSString *)location;

+ (void)trackDiscoverWithName:(NSString *)name andFoodName:(NSString *)foodName andLocationName:(NSString *)locationName andLocation:(NSString *)location;

+ (void)trackCampaignWithName:(NSString *)name andCampaignId:(NSNumber *)campaignId andBrandId:(NSString *)brandId andCouponId:(NSNumber *)couponId andLocation:(NSString *)location andOrigin:(NSString *)origin andIsMall:(BOOL)isMall;

+ (void)trackBrandPageWithName:(NSString *)name andBrandId:(NSString *)brandId andIsMall:(BOOL)isMall;

+ (void)trackDeliveryWithName:(NSString *)name andJournalId:(NSNumber *)journalId andFoodTagId:(NSNumber *)foodTagId andLocation:(NSString *)location andOrigin:(NSString *)origin;

+ (void)trackGoFoodWithName:(NSString *)name andJournalId:(NSNumber *)journalId andRestaurantId:(NSNumber *)restaurantId andPhotoId:(NSNumber *)photoId andLocation:(NSString *)location andOrigin:(NSString *)origin andOrder:(NSString *)order;

+ (void)trackGoFoodBannerWithName:(NSString *)name andBannerId:(NSString *)bannerId;

+ (void)trackDeliveryBannerWithName:(NSString *)name andOrder:(NSString *)order andType:(NSString *)type andTypeValue:(NSString *)typeValue;

@end
