//
//  JAKARTANetWork.m
//  Qraved
//
//  Created by josn.liu on 2017/1/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "JAKARTANetWork.h"
static JAKARTANetWork *manager = nil;
static dispatch_once_t pred;

@implementation JAKARTANetWork
+ (instancetype)sharedManager {
    dispatch_once(&pred, ^{
        manager = [[self alloc] initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",QRAVED_WEB_SERVICE_SERVER]]];
        manager.requestSerializer.timeoutInterval = 100;
        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", @"text/plain", @"text/javascript", @"text/json",  nil];
        
        manager.securityPolicy.allowInvalidCertificates = NO;
        
//        [manager setValue:string forHTTPHeaderField:@"TOKEN"];
    });
    return manager;
}

-(instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    if (self) {
        
        self.requestSerializer.timeoutInterval = 10;
        self.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [self.requestSerializer setValue:url.absoluteString forHTTPHeaderField:@"Referer"];
        
        self.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", @"text/plain", @"text/javascript", @"text/json",  nil];
        
        self.securityPolicy.allowInvalidCertificates = YES;
        
        
        //        NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"GET" URLString:[[NSURL URLWithString:URLString relativeToURL:self.baseURL] absoluteString] parameters:parameterDictionary error:nil];
        //        NSLog(@"http----%@", request.URL.absoluteString);
        //
        //        NSString *authStr = [NSString stringWithFormat:@"%@:%@",APP_LOGIN_USERNAME,APP_LOGIN_PASSWORD];
        //        NSData *authData = [authStr dataUsingEncoding:NSASCIIStringEncoding];
        //        NSString *authValue = [authData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
        //        [request setValue:[NSString stringWithFormat:@"Basic %@",authValue] forHTTPHeaderField:@"Authorization"];
        
    }
    return self;
}
- (NSURLSessionDataTask *)GET:(NSString *)URLString
                   parameters:(id)parameters
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    URLString = [URLString stringByAppendingString:QRAVED_WEB_SERVICE_CLIENT_VERSION];
    return [super GET:URLString parameters:parameters progress:nil success:success failure:failure];
}
- (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(id)parameters
                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    URLString = [URLString stringByAppendingString:QRAVED_WEB_SERVICE_CLIENT_VERSION];
    
    return [super POST:URLString parameters:parameters progress:nil success:success failure:failure];
}

- (void)deallocManager
{
    manager = nil;
    pred = 0l;
}

@end
