//
//  SendCall.m
//  Qraved
//
//  Created by Shine Wang on 7/24/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "SendCall.h"

@implementation SendCall

+(void)sendCall:(NSString *)phoneNum
{
    NSLog(@"phoneNum is %@",phoneNum);
    NSString *phoneString=[[[phoneNum stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *telStr = [NSString stringWithFormat:@"Tel:%@",phoneString];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:telStr]]; //拨号
}
@end
