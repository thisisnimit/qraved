//
//  ResultSetToObject.h
//  FMDB_AFNETWORKING
//
//  Created by Olaf on 14-9-9.
//  Copyright (c) 2014年 Olaf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDB.h"
@class FMResultSet;
@protocol ResultSetToObject <NSObject>
@required

+(instancetype)objectWithResult:(FMResultSet * ) resultSet;
-(instancetype)initWithResult:(FMResultSet *)resultSet;

@optional

@end
