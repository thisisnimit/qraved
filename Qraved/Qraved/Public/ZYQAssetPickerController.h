//
//  ZYQAssetPickerController.h
//  ZYQAssetPickerControllerDemo
//
//  Created by Zhao Yiqi on 13-12-25.
//  Copyright (c) 2013年 heroims. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "IMGRestaurant.h"
#pragma mark - ZYQAssetPickerController

@protocol ZYQAssetPickerControllerDelegate;

@interface ZYQAssetPickerController : UINavigationController

@property (nonatomic, weak) id <UINavigationControllerDelegate, ZYQAssetPickerControllerDelegate> delegate;

@property (nonatomic, strong) ALAssetsFilter *assetsFilter;

@property(nonatomic,strong)id publishViewController;

@property (nonatomic, copy, readonly) NSArray *indexPathsForSelectedItems;

@property (nonatomic, assign) NSInteger maximumNumberOfSelection;
@property (nonatomic, assign) NSInteger minimumNumberOfSelection;

@property (nonatomic, strong) NSPredicate *selectionFilter;

@property (nonatomic, assign) BOOL showCancelButton;

@property (nonatomic, assign) BOOL showEmptyGroups;

@property (nonatomic, assign) BOOL isFinishDismissViewController;

@property (nonatomic, assign) BOOL isFromUploadPhoto;
@property (nonatomic, assign) BOOL isUploadMenuPhoto;
@property (nonatomic, assign) BOOL isFromJourney;
@property (nonatomic, assign) BOOL isCanSearch;
@property(nonatomic,strong)IMGRestaurant *restaurant;

- (id)initWithRestaurantTitle:(NSString *)title andIsFromUploadPhoto:(BOOL)isFromUploadPhoto andIsUploadMenuPhoto:(BOOL)isUploadMenuPhoto andCurrentRestaurantId:(NSNumber *)restaurantId;
- (id)initWithRestaurantTitle:(NSString *)title andIsFromUploadPhoto:(BOOL)isFromUploadPhoto andIsUploadMenuPhoto:(BOOL)isUploadMenuPhoto andCurrentRestaurantId:(NSNumber *)restaurantId andIsreview:(BOOL)isReview;
- (id)initWithRestaurantTitle:(NSString *)title andIsFromUploadPhoto:(BOOL)isFromUploadPhoto andIsUploadMenuPhoto:(BOOL)isUploadMenuPhoto andCurrentRestaurantId:(NSNumber *)restaurantId andRestaurant:(IMGRestaurant*)restraurant;

@end

@protocol ZYQAssetPickerControllerDelegate <NSObject>

-(void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets;

@optional

-(void)assetPickerControllerDidCancel:(ZYQAssetPickerController *)picker;

-(void)assetPickerController:(ZYQAssetPickerController *)picker didSelectAsset:(ALAsset*)asset;

-(void)assetPickerController:(ZYQAssetPickerController *)picker didDeselectAsset:(ALAsset*)asset;

-(void)assetPickerControllerDidMaximum:(ZYQAssetPickerController *)picker;

-(void)assetPickerControllerDidMinimum:(ZYQAssetPickerController *)picker;

@end

#pragma mark - ZYQAssetViewController
#import "BaseViewController.h"
#import "IMGRestaurant.h"
@interface ZYQAssetViewController : BaseViewController

@property (nonatomic, strong) ALAssetsGroup *assetsGroup;
@property (nonatomic, strong) NSMutableArray *indexPathsForSelectedItems;
@property (nonatomic)         UITableView *tableView;
@property (nonatomic)         NSString *restaurantTitle;
@property (nonatomic)         BOOL isFromUploadPhoto;
@property (nonatomic)         BOOL isUploadMenuPhoto;
@property (nonatomic,strong) IMGRestaurant *restaurant;
@property (nonatomic)         NSNumber *restaurantID;
@property(nonatomic,assign) BOOL isReview;
@property (nonatomic, assign) BOOL isCanSearch;

- (id)initWithRestaurantTitle:(NSString *)restaurantTitle;

@end

#pragma mark - ZYQVideoTitleView

@interface ZYQVideoTitleView : UILabel

@end

#pragma mark - ZYQTapAssetView

@protocol ZYQTapAssetViewDelegate <NSObject>

-(void)touchSelect:(BOOL)select;
-(BOOL)shouldTap;

@end

@interface ZYQTapAssetView : UIView

@property (nonatomic, assign) BOOL selected;
@property (nonatomic, assign) BOOL disabled;
@property (nonatomic, weak) id<ZYQTapAssetViewDelegate> delegate;

@end

#pragma mark - ZYQAssetView

@protocol ZYQAssetViewDelegate <NSObject>

-(BOOL)shouldSelectAsset:(ALAsset*)asset;
-(void)tapSelectHandle:(BOOL)select asset:(ALAsset*)asset;

@end

@interface ZYQAssetView : UIView

- (void)bind:(ALAsset *)asset selectionFilter:(NSPredicate*)selectionFilter isSeleced:(BOOL)isSeleced;

@end

#pragma mark - ChooseViewCell

@interface ChooseViewCell : UITableViewCell


@end


#pragma mark - ZYQAssetViewCell

@protocol ZYQAssetViewCellDelegate;

@interface ZYQAssetViewCell : UITableViewCell

@property(nonatomic,weak)id<ZYQAssetViewCellDelegate> delegate;

- (void)bind:(NSArray *)assets selectionFilter:(NSPredicate*)selectionFilter minimumInteritemSpacing:(float)minimumInteritemSpacing minimumLineSpacing:(float)minimumLineSpacing columns:(int)columns assetViewX:(float)assetViewX;

@end

@protocol ZYQAssetViewCellDelegate <NSObject>

- (BOOL)shouldSelectAsset:(ALAsset*)asset;
- (void)didSelectAsset:(ALAsset*)asset;
- (void)didDeselectAsset:(ALAsset*)asset;

@end

#pragma mark - ZYQAssetGroupViewCell

@interface ZYQAssetGroupViewCell : UITableViewCell
@property (nonatomic, strong) ALAssetsGroup *assetsGroup;
- (void)bind:(ALAssetsGroup *)assetsGroup;

@end

#pragma mark - ZYQAssetGroupViewController
#import "BaseViewController.h"

@interface ZYQAssetGroupViewController : BaseViewController
@property (nonatomic)       NSString *restaurantTitle;
@property (nonatomic)       UITableView *tableView;
@property (nonatomic)       BOOL isFromUploadPhoto;
@property (nonatomic)       BOOL isUploadMenuPhoto;
@property (nonatomic)       NSNumber *restaurantId;
- (void)setupGroup;
@end

