//
//  JAKARTANetWork.h
//  Qraved
//
//  Created by josn.liu on 2017/1/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "AFNetworking/AFNetworking.h"
typedef void (^requestSuccessBlock)(id responseObject);

typedef void (^requestFailureBlock)(NSError *error);
@interface JAKARTANetWork : AFHTTPSessionManager
+ (instancetype)sharedManager;
- (void)requestWithMethod:(HTTPMethod)method
                 withPath:(NSString *)path
               withParams:(NSDictionary*)params
         withSuccessBlock:(requestSuccessBlock)success
          withFailurBlock:(requestFailureBlock)failure;
- (void)deallocManager;

@end
