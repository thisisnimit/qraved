//
//  Consts.h
//  Qraved
//
//  Created by Shine Wang on 7/17/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#ifndef Qraved_Consts_h
#define Qraved_Consts_h
#endif
//NetWork

#define TagsFilterFromHistory @"TagsFilterFromHistory"
#define TagsFilterFromHome @"TagsFilterFromHome"
#define TagsFilterSelected @"TagsFilterSelected"
#define TagsFilterCleared @"TagsFilterCleared"
#define TagsFilterSet @"TagsFilterSet"

#define TwitterLoginSelfPartDone @"TwitterLoginSelfPartDone"
#define TwitterConnectPartDone @"TwitterConnectPartDone"
#define TWITTERNOTIFICATIONTYPE @"TWITTERNOTIFICATIONTYPE"

#define NOTIFICATION_POPUP_LOCATIONLISTVIEWCONTROLLER @"NOTIFICATION_POPUP_LOCATIONLISTVIEWCONTROLLER"
#define NOTIFICATION_REFRESH_HOME @"NOTIFICATION_REFRESH_HOME"

#define NOTIFICATION_REFRESH_TAB_MENU @"NOTIFICATION_REFRESH_TAB_MENU"
#define NOTIFICATION_REFRESH_TAB_SAVED @"NOTIFICATION_REFRESH_TAB_SAVED"
#define NOTIFICATION_REFRESH_TAB_NOTIFICATION @"NOTIFICATION_REFRESH_TAB_NOTIFICATION"
#define NOTIFICATION_REFRESH_TAB_PROFILE @"NOTIFICATION_REFRESH_TAB_PROFILE"

//on boarding
#define ONBOARDING @"ONBOARDING"

#define NEARBYTITLE @"Nearby"

#define RGBCOLOR(r, g, b)       [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]
#define RGBACOLOR(r, g, b, a)   [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]

//navagationTab item image
#define NavigationNewBackImage (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) ? @"ic_back" : @"ic_back"
#define NavigationBackImage (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) ? @"ic_back" : @"ic_back"
#define NavigationCloseImage (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) ? @"CloseTop" : @"CloseTop"
#define NavigationShareImage (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) ? @"UploadTop" : @"UploadTop"
#define NavigationPhotoImage (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) ? @"PhotoLarge" : @"PhotoLarge"

#define NavigationMenuImage (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1) ? @"new_logo": @"new_logo"

#define DEFAULTPROFILEIMAGEURL @"http://himg.bdimg.com/sys/portrait/item/250c6c6f766574616e673465766572fb0d.jpg"

//#define QRAVED_WEB_SERVER @"http://192.168.1.12/"
//#define QRAVED_WEB_SERVICE_SERVER @"http://192.168.1.12:7055/"
//#define QRAVED_WEB_IMAGE_SERVER @"http://192.168.1.12/mobile/image?name="

#define CITY_SELECT @"city_select"
#define CITY_SELECT_ID @"city_select_id"
#define CITY_JAKARTA @"Jakarta"

#define HOME_CACHE_BANNER @"HomeBanner"
#define HOME_CACHE_LOCATION @"HomeLocation"
#define HOME_CACHE_INSTAGRAM @"HomeInstagram"
#define HOME_CACHE_HASHTAG @"HomeHashtag"
#define HOME_CACHE_FILTER @"HomeFilter"
#define HOME_CACHE_COMPONENTS @"HomeComponents"
#define HOME_CACHE_STRIPBANNER @"HomeStripBanner"
#define SAVED_CACHE @"SavedCache"
#define DELIVERY_CACHE @"DeliveryCache"
#define DELIVERY_BANNER_CACHE @"DeliveryBannerCache"
#define GUIDELIST_CACHE @"GuideListCache"


//image url
#define QRAVED_WEB_NEW_IMAGE_SERVER @"https://img.qraved.co/v2/image/"

//staging
//#define QRAVED_WEB_SERVER_OLD @"https://staging.qraved.com/"
//#define QRAVED_WEB_SERVER @"https://staging.qraved.com/"
//#define QRAVED_WEB_JAKARTASERVER @"http://kimchi01.qraved.com:9196/"
//#define QRAVED_WEB_SERVICE_SERVER @"https://apigw.qraved.com/staging/"
//#define QRAVED_WEB_UPLOAD_PHOTO_SERVER @"https://staging.qraved.com/"
//#define QRAVED_WEB_IMAGE_SERVER @"https://www.qraved.co/mobile/image?name="
//#define QRAVED_WEB_WEBP_IMAGE_SERVER @"https://staging.qraved.com/mobile/image/appImageForWebp?version=1.0&name="
//#define QRAVED_WEB_SERVICE_CLIENT_VERSION @"?client=2&v=4.1.2&appApiKey=test"
//#define QRAVED_APIKEY @"test"
//#define QRAVED_WEB_SERVICE_CLIENT @"2"
//#define QRAVED_WEB_SERVICE_VERSION @"4.1.2"
//#define QRAVED_ENVIROMENT @"staging"
//#define QRAVED_VERSION @"412"
//#define QRAVED_PATH_CLIENT_ID @"65da2c334634d061064b79981e9f3fb11c6963a7"
//#define QRAVED_PATH_CLIENT_SECRET @"4f460033c2db0b3e3c3328f36124f39945b689a3"
//#define QRAVED_PATH_TOKEN_KEY @"STAGING_PATH_TOKEN"
//#define QRAVED_AMPLITUDE_API_KEY @"ba347005396b45d36b9851fe3d071df6"
//#define QRAVED_APPSFLYER_API_KEY @"rXs8EEbaVYwcmajomzQexT"

//live
#define QRAVED_WEB_SERVER_OLD @"https://www.qraved.com/"
#define QRAVED_WEB_SERVER @"https://service.qraved.io/"
#define QRAVED_WEB_JAKARTASERVER @"https://batagor.qraved.co/"
#define QRAVED_WEB_SERVICE_SERVER @"https://apigw.qraved.com/"
#define QRAVED_WEB_UPLOAD_PHOTO_SERVER @"https://www.qraved.co/"
#define QRAVED_WEB_IMAGE_SERVER @"https://www.qraved.co/mobile/image?name="
#define QRAVED_WEB_WEBP_IMAGE_SERVER @"https://www.qraved.co/mobile/image/appImageForWebp?version=1.0&name="
#define QRAVED_WEB_SERVICE_CLIENT_VERSION @"?client=2&v=4.1.2&appApiKey=BhaxC05eXMSwQ4HK"
#define QRAVED_APIKEY @"BhaxC05eXMSwQ4HK"
#define QRAVED_WEB_SERVICE_CLIENT @"2"
#define QRAVED_WEB_SERVICE_VERSION @"4.1.2"
#define QRAVED_ENVIROMENT @""
#define QRAVED_VERSION @"412"
#define QRAVED_PATH_CLIENT_ID @"ed1c34431acd4147b2a93c4345c2e77b32e9fad7"
#define QRAVED_PATH_CLIENT_SECRET @"419d6c70308b3052ac91d48d74aaa19521e0df72"
#define QRAVED_PATH_TOKEN_KEY @"PATH_TOKEN"
#define QRAVED_AMPLITUDE_API_KEY @"47673d7b962ba4184a669544f722bfb2"
#define QRAVED_APPSFLYER_API_KEY @"rXs8EEbaVYwcmajomzQexT"

// dev
//#define QRAVED_WEB_SERVER_OLD @"https://dev.qraved.com/"
//#define QRAVED_WEB_SERVER @"https://service.qraved.io/"
//#define QRAVED_WEB_JAKARTASERVER @"https://batagor.qraved.co/"
//#define QRAVED_WEB_SERVICE_SERVER @"https://apigw.qraved.com/dev/"
//#define QRAVED_WEB_UPLOAD_PHOTO_SERVER @"https://www.qraved.co/"
//#define QRAVED_WEB_IMAGE_SERVER @"https://www.qraved.co/mobile/image?name="
//#define QRAVED_WEB_WEBP_IMAGE_SERVER @"https://www.qraved.co/mobile/image/appImageForWebp?version=1.0&name="
//#define QRAVED_WEB_SERVICE_CLIENT_VERSION @"?client=2&v=4.1.2&appApiKey=test"
//#define QRAVED_APIKEY @"test"
//#define QRAVED_WEB_SERVICE_CLIENT @"2"
//#define QRAVED_WEB_SERVICE_VERSION @"4.1.2"
//#define QRAVED_ENVIROMENT @"staging"
//#define QRAVED_VERSION @"412"
//#define QRAVED_PATH_CLIENT_ID @"65da2c334634d061064b79981e9f3fb11c6963a7"
//#define QRAVED_PATH_CLIENT_SECRET @"4f460033c2db0b3e3c3328f36124f39945b689a3"
//#define QRAVED_PATH_TOKEN_KEY @"STAGING_PATH_TOKEN"
//#define QRAVED_AMPLITUDE_API_KEY @"477b49def7dc1793e759173d0548b6ee"
//#define QRAVED_APPSFLYER_API_KEY @"rXs8EEbaVYwcmajomzQexT"


// #define MIXPANEL_TOKEN @"efa4132eedd97642d5fba2921e4ec735"



//#define qravedRDPCachePath [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@",@"RDPCache"]]

#define qravedRDPCachePath [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"/RDPCache"]]
#define qravedGIFCachePath [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"/GIFCache"]]


#define qravedDataBaseFilePath [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%@%@%@",QRAVED_ENVIROMENT,@"qraved",QRAVED_VERSION,@".sqlite"]]

#define qravedDefaultDataBaseFilePath [[NSBundle mainBundle]pathForResource:[NSString stringWithFormat:@"%@%@%@",QRAVED_ENVIROMENT,@"qraved",QRAVED_VERSION] ofType:@"sqlite"]











