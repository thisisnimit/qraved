//
//  AFClient.m
//  App
//
//  Created by jefftang on 12/11/12.
//  Copyright (c) 2012 Imaginato. All rights reserved.
//

#import "AFClient.h"
#import "AFJSONRequestOperation.h"

@implementation AFClient



+ (AFClient *)sharedClient {
    static AFClient *_sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedClient = [[AFClient alloc] initWithBaseURL:[NSURL URLWithString:QRAVED_WEB_SERVICE_SERVER]];
    });
    
    return _sharedClient;
}

- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (!self) {
        return nil;
    }
    
    [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
    
    [self setDefaultHeader:@"Accept" value:@"application/json"];
    
    return self;
}


@end
