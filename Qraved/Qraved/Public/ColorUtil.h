//
//  ColorUtil.h
//  Qraved
//
//  Created by Jeff on 2/12/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ColorUtil : NSObject

+( UIColor *) getColor:( NSString *)hexColor;

@end
