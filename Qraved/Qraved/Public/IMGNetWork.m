//
//  IMGNetWork.m
//  Qraved
//
//  Created by apple on 16/8/17.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "IMGNetWork.h"

static IMGNetWork *manager = nil;
static dispatch_once_t pred;
@implementation IMGNetWork
+ (instancetype)sharedManager {
    dispatch_once(&pred, ^{
        manager = [[self alloc] initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",QRAVED_WEB_SERVICE_SERVER]]];
        //manager.requestSerializer.timeoutInterval = 100;
        manager.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", @"text/plain", @"text/javascript", @"text/json",  nil];
        
        manager.securityPolicy.allowInvalidCertificates = NO;
        

    });
    return manager;
}

-(instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    if (self) {
        
        self.requestSerializer.timeoutInterval = 10;
        self.requestSerializer.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
        [self.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [self.requestSerializer setValue:url.absoluteString forHTTPHeaderField:@"Referer"];
        
        self.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html", @"application/json", @"text/plain", @"text/javascript", @"text/json",  nil];
        
        self.securityPolicy.allowInvalidCertificates = YES;
        
        
        //        NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"GET" URLString:[[NSURL URLWithString:URLString relativeToURL:self.baseURL] absoluteString] parameters:parameterDictionary error:nil];
        //        NSLog(@"http----%@", request.URL.absoluteString);
        //
        //        NSString *authStr = [NSString stringWithFormat:@"%@:%@",APP_LOGIN_USERNAME,APP_LOGIN_PASSWORD];
        //        NSData *authData = [authStr dataUsingEncoding:NSASCIIStringEncoding];
        //        NSString *authValue = [authData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
        //        [request setValue:[NSString stringWithFormat:@"Basic %@",authValue] forHTTPHeaderField:@"Authorization"];
        
    }
    return self;
}
- (NSURLSessionDataTask *)GET:(NSString *)URLString
                   parameters:(id)parameters
                   progress:(void (^)(NSProgress * _Nonnull))uploadProgress
                      success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                      failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    URLString = [URLString stringByAppendingString:QRAVED_WEB_SERVICE_CLIENT_VERSION];
    return [super GET:URLString parameters:parameters progress:nil success:success failure:failure];
}
- (NSURLSessionDataTask *)POST:(NSString *)URLString
                    parameters:(id)parameters
                     progress:(void (^)(NSProgress * _Nonnull))uploadProgress
                       success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                       failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure
{
    URLString = [URLString stringByAppendingString:QRAVED_WEB_SERVICE_CLIENT_VERSION];

    return [super POST:URLString parameters:parameters progress:nil success:success failure:failure];
}

- (void)deallocManager
{
    manager = nil;
    pred = 0l;
}
- (void)requestWithPath:(NSString *)path
               withBody:(NSData *)body
       withSuccessBlock:(requestSuccessBlock)success
        withFailurBlock:(requestFailureBlock)failure{
    
    path = [path stringByAppendingString:QRAVED_WEB_SERVICE_CLIENT_VERSION];

    NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"POST" URLString:[[NSURL URLWithString:path relativeToURL:self.baseURL] absoluteString] parameters:nil error:nil];
    request.timeoutInterval= 10;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:body];
    [[self dataTaskWithRequest:request completionHandler:^(NSURLResponse   * _Nullable response, id  _Nullable responseObject, NSError   * _Nullable error) {
        
        if (!error) {
            
            success(responseObject);
            
        } else {
            failure(error);
        }
    }] resume];
 
}

- (void)requestBodyWithPath:(NSString *)path
                   withBody:(NSDictionary *)params
           withSuccessBlock:(requestSuccessBlock)success
            withFailurBlock:(requestFailureBlock)failure{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValuesForKeysWithDictionary:params];
    [dic setObject:QRAVED_WEB_SERVICE_CLIENT forKey:@"client"];
    [dic setObject:QRAVED_WEB_SERVICE_VERSION forKey:@"v"];
    [dic setObject:QRAVED_APIKEY forKey:@"appApiKey"];

    NSData *data = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
    NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"POST" URLString:[[NSURL URLWithString:path relativeToURL:self.baseURL] absoluteString] parameters:nil error:nil];
    request.timeoutInterval= 10;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:data];
    [[self dataTaskWithRequest:request completionHandler:^(NSURLResponse   * _Nullable response, id  _Nullable responseObject, NSError   * _Nullable error) {
        
        if (!error) {
            
            success(responseObject);
            
        } else {
            failure(error);
        }
    }] resume];

}


- (void)homeGetRequest:(NSString *)path withBody:(NSData *)body
      withSuccessBlock:(requestSuccessBlock)success
       withFailurBlock:(requestFailureBlock)failure{
    
    NSMutableURLRequest *request = [self.requestSerializer requestWithMethod:@"GET" URLString:[[NSURL URLWithString:path relativeToURL:self.baseURL] absoluteString] parameters:nil error:nil];
    
    [[self dataTaskWithRequest:request completionHandler:^(NSURLResponse   * _Nullable response, id  _Nullable responseObject, NSError   * _Nullable error) {
        
        if (!error) {
            
            success(responseObject);
            
        } else {
            failure(error);
        }
    }] resume];
}


@end

