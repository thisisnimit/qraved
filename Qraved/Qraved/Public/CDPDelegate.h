//
//  CDPDelegate.h
//  Qraved
//
//  Created by Jeff on 3/14/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

@protocol CDPDelegate <NSObject>
@optional
- (void)CDP:(id)cdp reloadTableCell:(id)cell;
- (void)reloadCell;
@end
