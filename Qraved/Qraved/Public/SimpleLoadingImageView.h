//
//  SimpleLoadingImageView.h
//  Qraved
//
//  Copyright (c) 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleLoadingImageView : UIView

-(void)startLoading;
-(void)stopLoading;
- (instancetype)initWithFrame:(CGRect)frame;

@end
