//
//  LoadingView.m
//  LoadingTest
//
//  Created by Admin on 8/27/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import "LoadingView.h"
#import "AppDelegate.h"
#import "UIImage+animatedGIF.h"

static LoadingView *loadingView = nil;
@implementation LoadingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


+(LoadingView *)sharedLoadingView {
    @synchronized(self) {
        if (!loadingView) {
            loadingView = [[LoadingView alloc]init];
        }
    }
    return loadingView;
}

-(id)init {
    
     self = [super initWithFrame:CGRectMake(0, 100, 63, 62)];
    CGRect rect = [UIScreen mainScreen].bounds;
    self.center = CGPointMake(rect.size.width/2, (rect.size.height-20)/2);
    if (self) {
        NSMutableArray *muArr =[NSMutableArray array];
        for (int i = 10001; i <= 10009; i++) {
            NSString *imgStr =[NSString stringWithFormat:@"%d",i];
            UIImage *img = [UIImage imageNamed:imgStr];
            [muArr addObject:img];
        }
        for (int i = 10010; i <= 10030; i++) {
            NSString *imgStr =[NSString stringWithFormat:@"%d",i];
            UIImage *img = [UIImage imageNamed:imgStr];
            [muArr addObject:img];
        }

//        loadingImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 15, 100, 100)];
        loadingImgView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 15, 30, 30)];
        
//        loadingImgView.image=[UIImage animatedImageWithAnimatedGIFURL:[[NSBundle mainBundle] URLForResource:@"looping_loader" withExtension:@"gif"]];
        
        UIImageView *loadingBGView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loading-background"]];
        [loadingBGView setAlpha:0.3];
        loadingBGView.frame = CGRectMake(0, 0, 63, 62);
//
        [self addSubview:loadingBGView];
        [self addSubview:loadingImgView];
        
        loadingImgView.animationImages = muArr;
        loadingImgView.animationDuration=1.0;
    }
    return self;
}

-(void)startLoading {
    [loadingImgView startAnimating];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    NSLog(@"%@",[UIApplication sharedApplication].keyWindow.subviews);
}

-(void)stopLoading {
    [loadingImgView stopAnimating];
//    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [self removeFromSuperview];
}


@end
