//
//  IMGAmplitudeUtil.m
//  Qraved
//
//  Created by harry on 2017/11/27.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "IMGAmplitudeUtil.h"

@implementation IMGAmplitudeUtil

+ (void)trackRestaurantWithName:(NSString *)name andRestaurantId:(NSNumber *)restaurantId andRestaurantTitle:(NSString *)restaurantTitle andLocation:(NSString *)location andOrigin:(NSString *)origin andType:(NSString *)typeName{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    if (restaurantId != nil) {
        [param setValue:restaurantId forKey:@"Restaurant_ID"];
    }
    
    if (restaurantTitle != nil) {
        [param setValue:restaurantTitle forKey:@"Restaurant_name"];
    }
    
    if (location != nil) {
        [param setValue:location forKey:@"Loaction"];
    }
    
    if (origin != nil) {
        [param setValue:origin forKey:@"Origin"];
    }
    
    if (typeName != nil) {
        [param setValue:typeName forKey:@"Type"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}
+ (void)trackPhotoWithName:(NSString *)name andRestaurantId:(NSNumber *)restaurantId andRestaurantTitle:(NSString *)restaurantTitle andOrigin:(NSString *)origin andUploaderUser_ID:(NSNumber *)uploadUserId andPhoto_ID:(NSNumber *)photoId andRestaurantMenu_ID:(NSNumber *)menuId andRestaurantEvent_ID:(NSNumber *)eventId andPhotoSource:(NSString *)photoSource andLocation:(NSString *)location{
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    if (restaurantId != nil) {
        [param setValue:restaurantId forKey:@"Restaurant_ID"];
    }
    
    if (restaurantTitle != nil) {
        [param setValue:restaurantTitle forKey:@"restaurant_Name"];
    }
    
    if (origin != nil) {
        [param setValue:origin forKey:@"Origin"];
    }
    
    if (uploadUserId != nil) {
        [param setValue:uploadUserId forKey:@"UploaderUser_ID"];
    }
    
    if (photoId != nil) {
        [param setValue:photoId forKey:@"Photo_ID"];
    }
    
    if (menuId != nil) {
        [param setValue:menuId forKey:@"RestaurantMenu_ID"];
    }
    
    if (eventId != nil) {
        [param setValue:eventId forKey:@"RestaurantEvent_ID"];
    }
    
    if (photoSource != nil) {
        [param setValue:photoSource forKey:@"Photo Source"];
    }
    
    if (location != nil) {
        [param setValue:location forKey:@"Loaction"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackSavedWithName:(NSString *)name andRestaurantId:(NSNumber *)restaurantId andRestaurantTitle:(NSString *)restaurantTitle andOrigin:(NSString *)origin andListId:(NSNumber *)listId andType:(NSString *)type andLocation:(NSString *)location{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    if (restaurantId != nil) {
        [param setValue:restaurantId forKey:@"Restaurant_ID"];
    }
    
    if (restaurantTitle != nil) {
        [param setValue:restaurantTitle forKey:@"Restaurant_name"];
    }
    
    if (origin != nil) {
        [param setValue:origin forKey:@"Origin"];
    }
    
    if (listId != nil) {
        [param setValue:listId forKey:@"List_ID"];
    }
    
    if (type != nil) {
        [param setValue:type forKey:@"Type"];
    }
    
    if (location != nil) {
        [param setValue:location forKey:@"Loaction"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackReviewWithName:(NSString *)name andRestaurantId:(NSNumber *)restaurantId andRatingValue:(NSNumber *)ratingValue andReviewId:(NSNumber *)reviewId andReviewUserId:(NSNumber *)reviewUserId andRatingId:(NSNumber *)ratingId andUploadUserId:(NSNumber *)uploadUserId andSource:(NSString *)source andType:(NSString *)type andOrigin:(NSString *)origin andValue:(NSString *)value{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    if (restaurantId != nil) {
        [param setValue:restaurantId forKey:@"Restaurant_ID"];
    }
    
    if (ratingValue != nil) {
        [param setValue:ratingValue forKey:@"Rating_Value"];
    }
    
    if (reviewId != nil) {
        [param setValue:reviewId forKey:@"Review_ID"];
    }
    
    if (reviewUserId != nil) {
        [param setValue:reviewUserId forKey:@"ReviewerUser_ID"];
    }
    
    if (ratingId != nil) {
        [param setValue:ratingId forKey:@"Rating_ID"];
    }
    
    if (uploadUserId != nil) {
        [param setValue:uploadUserId forKey:@"UploaderUser_ID"];
    }
    
    if (source != nil) {
        [param setValue:source forKey:@"Source"];
    }
    
    if (value != nil) {
        [param setValue:value forKey:@"Value"];
    }
    
    if (type != nil) {
        [param setValue:type forKey:@"Type"];
    }
    
    if (origin != nil) {
        [param setValue:origin forKey:@"Origin"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackGuideWithName:(NSString *)name andDiningGuideId:(NSNumber *)guideId andDiningGuideName:(NSString *)guideName andOrigin:(NSString *)origin{
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    if (guideId != nil) {
        [param setValue:guideId forKey:@"DiningGuide_ID"];
    }
    
    if (guideName != nil) {
        [param setValue:guideName forKey:@"DiningGuide_name"];
    }
    
    if (origin != nil) {
        [param setValue:origin forKey:@"Origin"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackRestaurantInGuideWithName:(NSString *)name andDiningGuideId:(NSNumber *)guideId andOrigin:(NSString *)origin andRestaurantId:(NSNumber *)restaurantId andRestaurantTitle:(NSString *)restaurantTitle{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    if (restaurantId != nil) {
        [param setValue:restaurantId forKey:@"Restaurant_ID"];
    }
    
    if (restaurantTitle != nil) {
        [param setValue:restaurantTitle forKey:@"Restaurant_name"];
    }
    
    if (guideId != nil) {
        [param setValue:guideId forKey:@"DiningGuide_ID"];
    }
    
    if (origin != nil) {
        [param setValue:origin forKey:@"Origin"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackJournalListWithName:(NSString *)name andSorting:(NSString *)sorting andFilter:(NSNumber *)filter andOrigin:(NSString *)origin{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    if (sorting != nil) {
        [param setValue:sorting forKey:@"Sorting"];
    }
    
    if (filter != nil) {
        [param setValue:filter forKey:@"Filter"];
    }
    
    if (origin != nil) {
        [param setValue:origin forKey:@"Origin"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackJDPWithName:(NSString *)name andJournalId:(NSNumber *)journalId andOrigin:(NSString *)origin andChannel:(NSString *)channel andLocation:(NSString *)location{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    if (journalId != nil) {
        [param setValue:journalId forKey:@"Journal_ID"];
    }
    
    if (channel != nil) {
        [param setValue:channel forKey:@"Channel"];
    }
    
    if (location != nil) {
        [param setValue:location forKey:@"Location"];
    }
    
    if (origin != nil) {
        [param setValue:origin forKey:@"Origin"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackJDPPhotoWithName:(NSString *)name andJournalId:(NSNumber *)journalId andOrigin:(NSString *)origin andPhotoId:(NSNumber *)photoId{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    if (journalId != nil) {
        [param setValue:journalId forKey:@"Journal_ID"];
    }
    
    if (photoId != nil) {
        [param setValue:photoId forKey:@"Photo_ID"];
    }
    
    if (origin != nil) {
        [param setValue:origin forKey:@"Origin"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackRDPWithName:(NSString *)name andRestaurantId:(NSNumber *)restaurantId andOrigin:(NSString *)origin andLocation:(NSString *)location{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    if (restaurantId != nil) {
        [param setValue:restaurantId forKey:@"Restaurant_ID"];
    }
    
    if (location != nil) {
        [param setValue:location forKey:@"Location"];
    }
    
    if (origin != nil) {
        [param setValue:origin forKey:@"Origin"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackReviewViewWithName:(NSString *)name andRestaurantId:(NSNumber *)restaurantId andReviewId:(NSNumber *)reviewId andReviewUserId:(NSString *)reviewUserId andOrigin:(NSString *)origin andLocation:(NSString *)location andChannel:(NSString *)channel{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    if (restaurantId != nil) {
        [param setValue:restaurantId forKey:@"Restaurant_ID"];
    }
    
    if (reviewId != nil) {
        [param setValue:reviewId forKey:@"Review_ID"];
    }
    
    if (reviewUserId != nil) {
        [param setValue:reviewUserId forKey:@"ReviewerUser_ID"];
    }
    
    if (location != nil) {
        [param setValue:location forKey:@"Location"];
    }
    
    if (origin != nil) {
        [param setValue:origin forKey:@"Origin"];
    }
    
    if (channel) {
        [param setValue:channel forKey:@"Channel"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackPromoWithName:(NSString *)name andRestaurantId:(NSNumber *)restaurantId andPromoId:(NSNumber *)promoId{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    if (restaurantId != nil) {
        [param setValue:restaurantId forKey:@"Restaurant_ID"];
    }
    
    if (promoId != nil) {
        [param setValue:promoId forKey:@"Promo_ID"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackSignInWithName:(NSString *)name andType:(NSString *)type andLocation:(NSString *)location andReason:(NSString *)reason{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    if (type != nil) {
        [param setValue:type forKey:@"Sign In Type"];
    }
    
    if (location != nil) {
        [param setValue:location forKey:@"Location"];
    }
    
    if (reason != nil) {
        [param setValue:reason forKey:@"Reason"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackPreferenceWithName:(NSString *)name andEatPreference:(NSString *)eatPreference andCuisinePreference:(NSString *)cuisinePreference andCuisineName:(NSString *)cuisineName andBrandName:(NSString *)brandName andOrigin:(NSString *)origin{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    if (eatPreference != nil) {
        [param setValue:eatPreference forKey:@"Eating Preference"];
    }
    
    if (cuisinePreference != nil) {
        [param setValue:cuisinePreference forKey:@"Cuisine Preference"];
    }
    
    if (cuisineName != nil) {
        [param setValue:cuisineName forKey:@"Cuisine_Name"];
    }
    
    if (brandName != nil) {
        [param setValue:brandName forKey:@"Brand"];
    }
    
    if (origin != nil) {
        [param setValue:origin forKey:@"Origin"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackBookWithName:(NSString *)name andRestaurantId:(NSNumber *)restaurantId andReservationId:(NSNumber *)reservationId andLocation:(NSString *)location andChannel:(NSString *)channel{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    if (restaurantId != nil) {
        [param setValue:restaurantId forKey:@"Restaurant_ID"];
    }
    
    if (reservationId != nil) {
        [param setValue:reservationId forKey:@"Reservation_ID"];
    }
    
    if (location != nil) {
        [param setValue:location forKey:@"Location"];
    }
    
    if (channel != nil) {
        [param setValue:channel forKey:@"Channel"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackProfileWithName:(NSString *)name andUserId:(NSNumber *)userId andLocation:(NSString *)location{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    if (userId != nil) {
        [param setValue:userId forKey:@"User_ID"];
    }
    
    if (location != nil) {
        [param setValue:location forKey:@"Location"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackDiscoverWithName:(NSString *)name andFoodName:(NSString *)foodName andLocationName:(NSString *)locationName andLocation:(NSString *)location{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    if (foodName != nil) {
        [param setValue:foodName forKey:@"Food Name"];
    }
    
    if (locationName != nil) {
        [param setValue:locationName forKey:@"Location Name"];
    }
    
    if (location != nil) {
        [param setValue:location forKey:@"Location"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackCampaignWithName:(NSString *)name andCampaignId:(NSNumber *)campaignId andBrandId:(NSString *)brandId andCouponId:(NSNumber *)couponId andLocation:(NSString *)location andOrigin:(NSString *)origin andIsMall:(BOOL)isMall{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    if (campaignId != nil) {
        [param setValue:campaignId forKey:@"Campaign_ID"];
    }
    
    NSString *idStr = isMall ? @"Mall_ID" : @"Brand_ID";
    if (brandId != nil) {
        [param setValue:brandId forKey:idStr];
    }
    
    if (couponId != nil) {
        [param setValue:couponId forKey:@"Coupon_ID"];
    }
    
    if (location != nil) {
        [param setValue:location forKey:@"Location"];
    }
    
    if (origin != nil) {
        [param setValue:origin forKey:@"Origin"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackBrandPageWithName:(NSString *)name andMallId:(NSNumber *)mallId andBrandId:(NSString *)brandId{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    if (mallId != nil) {
        [param setValue:mallId forKey:@"Mall_ID"];
    }
    
    if (brandId != nil) {
        [param setValue:brandId forKey:@"Brand_ID"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackBrandPageWithName:(NSString *)name andBrandId:(NSString *)brandId andIsMall:(BOOL)isMall{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    NSString *idStr = isMall ? @"Mall_ID" : @"Brand_ID";
    if (brandId != nil) {
        [param setValue:brandId forKey:idStr];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackDeliveryWithName:(NSString *)name andJournalId:(NSNumber *)journalId andFoodTagId:(NSNumber *)foodTagId andLocation:(NSString *)location andOrigin:(NSString *)origin{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    if (journalId != nil) {
        [param setValue:journalId forKey:@"Journal_ID"];
    }
    
    if (foodTagId != nil) {
        [param setValue:foodTagId forKey:@"FoodTag_ID"];
    }
    
    if (location != nil) {
        [param setValue:location forKey:@"Location"];
    }
    
    if (origin != nil) {
        [param setValue:origin forKey:@"Origin"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackGoFoodWithName:(NSString *)name andJournalId:(NSNumber *)journalId andRestaurantId:(NSNumber *)restaurantId andPhotoId:(NSNumber *)photoId andLocation:(NSString *)location andOrigin:(NSString *)origin andOrder:(NSString *)order{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    if (journalId != nil) {
        [param setValue:journalId forKey:@"Journal_ID"];
    }
    
    if (restaurantId != nil) {
        [param setValue:restaurantId forKey:@"Restaurant_ID"];
    }
    
    if (photoId != nil) {
        [param setValue:photoId forKey:@"Photo_ID"];
    }
    
    if (location != nil) {
        [param setValue:location forKey:@"Location"];
    }
    
    if (origin != nil) {
        [param setValue:origin forKey:@"Origin"];
    }
    
    if (order != nil) {
        [param setValue:order forKey:@"Order"];
        
        if ([order isEqualToString:@"1"]) {
            [param setValue:@"Full width" forKey:@"Thumbnail Type"];
        }else{
            [param setValue:@"Half width" forKey:@"Thumbnail Type"];
        }
    }
    
    NSString *latitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"];
    NSString *longitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"];
    
    if (latitude != nil &&longitude != nil) {
        [param setValue:[NSString stringWithFormat:@"%@,%@",latitude,longitude] forKey:@"Latitude,Longitude"];
    }
    
    if ([self currentTime] != nil) {
        [param setValue:[self currentTime] forKey:@"Date,Time"];
    }
    
    IMGUser *user = [IMGUser currentUser];
    if (user.userId != nil) {
        [param setValue:user.userId forKey:@"User_ID"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}
+ (void)trackGoFoodBannerWithName:(NSString *)name andBannerId:(NSString *)bannerId{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    if (bannerId != nil) {
        [param setValue:bannerId forKey:@"Banner_ID"];
    }
    
    NSString *latitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"];
    NSString *longitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"];
    
    if (latitude != nil &&longitude != nil) {
        [param setValue:[NSString stringWithFormat:@"%@,%@",latitude,longitude] forKey:@"Latitude,Longitude"];
    }
    
    if ([self currentTime] != nil) {
        [param setValue:[self currentTime] forKey:@"Date,Time"];
    }
    
    IMGUser *user = [IMGUser currentUser];
    if (user.userId != nil) {
        [param setValue:user.userId forKey:@"User_ID"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (void)trackDeliveryBannerWithName:(NSString *)name andOrder:(NSString *)order andType:(NSString *)type andTypeValue:(NSString *)typeValue{
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    
    if (order != nil) {
        [param setValue:order forKey:@"Order"];
    }
    if ([type isEqualToString:@"url"]) {
        [param setValue:@"URL" forKey:@"Target"];
        [param setValue:typeValue forKey:@"URL"];
    }else if ([type isEqualToString:@"jdp"]){
        [param setValue:@"Journal Detail Page" forKey:@"Target"];
        [param setValue:typeValue forKey:@"Journal_ID"];
    }else if ([type isEqualToString:@"rdp"]){
        [param setValue:@"Restaurant Detail Page" forKey:@"Target"];
        [param setValue:typeValue forKey:@"Restaurant_ID"];
    }
    
    IMGUser *user = [IMGUser currentUser];
    if (user.userId != nil) {
        [param setValue:user.userId forKey:@"User_ID"];
    }
    
    [[Amplitude instance] logEvent:name withEventProperties:param];
}

+ (NSString *)currentTime{
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy,HH:mm:ss"];
    NSString *dateStr = [dateFormatter stringFromDate:date];
    return dateStr;
}


@end
