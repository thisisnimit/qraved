//
//  BookUtil.h
//  Qraved
//
//  Created by Jeff on 10/15/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGRestaurantOffer.h"
#import "IMGRestaurant.h"
#import "IMGReservation.h"

@interface BookUtil : NSObject

+ (void)getReservationInfoWithparameters:(NSDictionary *)parameters andBlock:(void (^)(IMGReservation *reservation))block;
+(NSArray *)around30minutes:(NSString *)time;

+(NSDate *)dateFromString:(NSString *)dateString withFormat:(NSString *)format;
+(NSString*)dateStringWithFormat:(NSString *)format forDate:(NSDate *)date;
+(NSString*)todayStringWithFormat:(NSString *)format;
+(NSString *)minBookTimeString:(NSNumber *)minimumAvailableBooking;
+(NSString *)timeIntToString:(int)time;
+(int)timeStringToInt:(NSString *)time;
+(NSString *)currentTimeAt:(NSDate *)date;
+(BOOL)timeAvailableAt:(NSDate *)date andTime:(NSString *)time withPax:(NSNumber *)pax  forRestaurantOffer:(IMGRestaurantOffer *)tmpRestaurantOffer;
+(NSArray *)timeSlotAround:(NSDate *)date andTime:(NSString *)time;

+(BOOL)availableOfferSlotBy:(IMGRestaurantOffer *)restaurantOffer andPeopleCount:(NSInteger)peopleCount andDate:(NSDate *)date andStartTime:(NSString *)time;
+(NSArray *)selectRegularTimeSlotBy:(NSInteger)peopleCount andDate:(NSDate *)date inRegularTimeArray:(NSArray *)regularTimeArray andStartTime:(NSString *)time;
+(NSArray *)selectOfferSlotBy:(IMGRestaurantOffer *)restaurantOffer andPeopleCount:(NSInteger)peopleCount andDate:(NSDate *)date andStartTime:(NSString *)time;

+(NSArray *)availableTimesFromRegular:(NSArray *)regularTimeArray withPax:(NSNumber *)pax withAllTimeArray:(NSArray *)allTimeArray;
+(NSArray *)availableTimesFromOffer:(IMGRestaurantOffer *)restaurantOffer inOfferArray:(NSArray *)restaurantOfferArray withPax:(NSNumber *)pax;
+(NSArray *)availableTimesNotFromOffer:(IMGRestaurantOffer *)restaurantOffer inOfferArray:(NSArray *)restaurantOfferArray withPax:(NSNumber *)pax withAvailableTimeArray:(NSArray *)availableTimeArray;
+(int)maxAvailablePaxNotFromOffer:(IMGRestaurantOffer *)restaurantOffer inOfferArray:(NSArray *)restaurantOfferArray;
+(int)maxAvailablePaxFromOffer:(IMGRestaurantOffer *)restaurantOffer;
+(BOOL)bookableFromRegular:(NSArray *)regularTimeArray;
+(NSArray *)availableWeekDaysFromOffer:(IMGRestaurantOffer *)restaurantOffer withPax:(NSNumber *)pax;
+(NSArray *)availableWeekDaysNotFromOffer:(IMGRestaurantOffer *)restaurantOffer inOfferArray:(NSArray *)restaurantOfferArray withPax:(NSNumber *)pax withAvailableWeekDays:(NSArray *)availableWeekDayArray;

+(int)bookedFromSlotTimeBookedArray:(NSArray *)tmpSlotTimeBookedArray atDate:(NSDate *)date;
+(int)bookedFromOfferTimeBookedArray:(NSArray *)tmpOfferTimeBookedArray atDate:(NSDate *)date offerId:(int)offerId;
+(int)sumBookedFromSlotTimeBookedArray:(NSArray *)tmpSlotTimeBookedArray atDate:(NSDate *)date;


+(BOOL)bookable:(NSNumber *)pax andSlotTimeBookedArray:(NSArray *)tmpSlotTimeBookedArray atDate:(NSDate *)date restaurant:(IMGRestaurant *)restaurant;
+(BOOL)bookableFromOffer:(IMGRestaurantOffer *)restaurantOffer withPax:(NSNumber *)pax andSlotTimeBookedArray:(NSArray *)tmpSlotTimeBookedArray andOfferTimeBookedArray:(NSArray *)tmpOfferTimeBookedArray atDate:(NSDate *)date restaurant:(IMGRestaurant *)restaurant;
+(BOOL)bookableNotFromOffer:(IMGRestaurantOffer *)restaurantOffer inOfferArray:(NSArray *)restaurantOfferArray withPax:(NSNumber *)pax  andSlotTimeBookedArray:(NSArray *)tmpSlotTimeBookedArray andOfferTimeBookedArray:(NSArray *)tmpOfferTimeBookedArray atDate:(NSDate *)date restaurant:(IMGRestaurant *)restaurant;
+(BOOL)bookableFromRegular:(NSArray *)regularTimeArray withPax:(NSNumber *)pax  andSlotTimeBookedArray:(NSArray *)tmpSlotTimeBookedArray  atDate:(NSDate *)date restaurant:(IMGRestaurant *)restaurant;
+(BOOL)bookableWithNowTime:(NSArray *)tmpSlotTimeBookedArray andCurrentWeekDay:(long)currentWeekDay;
@end
