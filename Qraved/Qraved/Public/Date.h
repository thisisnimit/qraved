//
//  Date.h
//  Qraved
//
//  Created by Shine Wang on 6/8/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Date : NSObject

+(NSString *)date;
+(NSString *)weekDay;

+(NSString *)getTimeInteval:(NSString *)timeIn;

+(NSString *)getJakartaTime:(NSString *)dateStr;

+(NSString *)getTimeInteval_v2:(NSString *)dateStr;

+(NSString *)getTimeInteval_v3:(double)timestamp;

+(NSString *)getTimeInteval_v4:(double)timestamp;
+(NSString *)getJournalTime:(double)timestamp;
+(NSString *)getTimeInteval_v5:(double)timestamp;
+(NSString *)getTimeInteval_v10:(double)timestamp;

+ (NSString *)ConvertStrToTime:(NSString *)timeStr;//毫秒转时间
+ (NSString *)dateFormart:(NSString *)dateString; // 年月日
+ (NSString *)timeFormart:(NSString *)timeString; //时间
+ (NSString *)dateFormatForCoupon:(NSNumber *)dateNum;

+ (NSString *)dateTimeChangeDateFormart:(NSString *)dateAndTime;//

+ (int)compareCurrentTimeWithTime:(NSNumber *)dateNum;

@end
