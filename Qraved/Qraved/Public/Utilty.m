//
//  Utility.m
//  Qraved
//
//  Created by Sean Liao on 8/17/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "Utility.h"

@implementation Utility


+(NSString *)generatePriceLevel:(int)score {
    NSMutableString *scoreString = [[NSMutableString alloc] init];
     
    while (score > 0) {
        [scoreString appendFormat:@"$"];
        score--;
    }
    return scoreString;
}
        
@end
