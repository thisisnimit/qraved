//
//  ImageCacheDownload.m
//  Qraved
//
//  Created by Admin on 8/15/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "ImageCacheDownload.h"



@implementation ImageCacheDownload
@synthesize imgView;
@synthesize downloadImage;

-(void)loadImageToImageView:(UIImageView *)toImgView andUrl:(NSURL *)url {
    self.imgView = toImgView;
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    UIImage *cachedImage =[manager imageWithURL:url];
    if (cachedImage) {
        imgView.image = cachedImage;
    } else {
        [manager downloadWithURL:url delegate:self];
    }
}

-(void)loadImageToImage:(UIImage *)toImage andUrl:(NSURL *)url {
    self.downloadImage = toImage;
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    UIImage *cachedImage = [manager imageWithURL:url];
    if (cachedImage) {
        self.downloadImage = cachedImage;
    } else {
        [manager downloadWithURL:url delegate:self];
    }
}

+(BOOL)imgHasDownloaded:(NSURL *)imgUrl {
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    UIImage *cachedImage = [manager imageWithURL:imgUrl];
    if (cachedImage) {
        return YES;
    } else {
        return NO;
    }
}

- (void)webImageManager:(SDWebImageManager *)imageManager didFinishWithImage:(UIImage *)image {
    self.imgView.image = image;
    
    self.downloadImage = image;
 
}


@end
