//
//  MapUtil.m
//  Qraved
//
//  Created by Jeff on 11/17/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "MapUtil.h"

@implementation MapUtil

+(double)minLatitude:(double)centerLatitude deltaLatitude:(double)deltaLatitude{
    return centerLatitude-(deltaLatitude*0.93)/2;
}

+(double)minLongitude:(double)centerLongitude deltaLongitude:(double)deltaLongitude{
    return centerLongitude-deltaLongitude/2;
}

+(double)maxLatitude:(double)centerLatitude deltaLatitude:(double)deltaLatitude{
    return centerLatitude+deltaLatitude*0.93/2;
}

+(double)maxLongitude:(double)centerLongitude deltaLongitude:(double)deltaLongitude{
    return centerLongitude+deltaLongitude/2;
}

@end
