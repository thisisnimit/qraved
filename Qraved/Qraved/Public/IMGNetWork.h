//
//  IMGNetWork.h
//  Qraved
//
//  Created by apple on 16/8/17.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "AFNetworking/AFNetworking.h"


typedef void (^requestSuccessBlock)(id responseObject);

typedef void (^requestFailureBlock)(NSError *error);

typedef enum {
    GET,
    POST,
    PUT,
    DELETE,
    HEAD
} HTTPMethod;
@interface IMGNetWork : AFHTTPSessionManager
+ (instancetype)sharedManager;

- (void)deallocManager;
- (void)requestWithPath:(NSString *)path
               withBody:(NSData *)body
       withSuccessBlock:(requestSuccessBlock)success
        withFailurBlock:(requestFailureBlock)failure;

- (void)requestBodyWithPath:(NSString *)path
               withBody:(NSDictionary *)params
       withSuccessBlock:(requestSuccessBlock)success
        withFailurBlock:(requestFailureBlock)failure;

- (void)homeGetRequest:(NSString *)path withBody:(NSData *)body
   withSuccessBlock:(requestSuccessBlock)success
    withFailurBlock:(requestFailureBlock)failure;
@end
