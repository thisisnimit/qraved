//
//  UncaughtExceptionHandler.m
//  EasyPayment
//
//  Created by wx on 12-12-12.
//  Copyright (c) 2012年 wx. All rights reserved.
//

#import "UncaughtExceptionHandler.h"

#include <libkern/OSAtomic.h>
#include <execinfo.h>
NSString * const UncaughtExceptionHandlerSignalExceptionName = @"UncaughtExceptionHandlerSignalExceptionName";



NSString * const UncaughtExceptionHandlerSignalKey = @"UncaughtExceptionHandlerSignalKey";



NSString * const UncaughtExceptionHandlerAddressesKey = @"UncaughtExceptionHandlerAddressesKey";



volatile int32_t UncaughtExceptionCount = 0;



const int32_t UncaughtExceptionMaximum = 10;



const NSInteger UncaughtExceptionHandlerSkipAddressCount = 4;



const NSInteger UncaughtExceptionHandlerReportAddressCount = 5;



@implementation UncaughtExceptionHandler



+ (NSArray *)backtrace {
    
    
    
    void* callstack[128];
    
    
    
    int frames = backtrace(callstack, 128);
    
    
    
    char **strs = backtrace_symbols(callstack, frames);
    
    
    
    int i;
    
    
    
    NSMutableArray *backtrace = [NSMutableArray arrayWithCapacity:frames];
    
    
    
    for ( i = UncaughtExceptionHandlerSkipAddressCount;i < UncaughtExceptionHandlerSkipAddressCount +UncaughtExceptionHandlerReportAddressCount;i++) {
        
        
        
        [backtrace addObject:[NSString stringWithUTF8String:strs[i]]];
        
    }
    
    
    
    free(strs);
    
    
    
    return backtrace;
    
}

- (void)alertView:(UIAlertView *)anAlertView clickedButtonAtIndex:(NSInteger)anIndex {
    
    
    if (anIndex == 0) {
        
        dismissed = YES;
    }
    
}



-(void)handleException:(NSException *)exception {

    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *arr=[exception callStackSymbols];
    NSString *reason=[exception reason];
    NSString *name=[exception name];
    
    NSString *url=[NSString stringWithFormat:@"=============Signal exception report=============\nname:\n%@\nreason:\n%@\ncallStackSymbols:\n%@",name,reason,[arr componentsJoinedByString:@"\n"]];
    
//    NSString *path = [applicationDocumentsDirectory() stringByAppendingPathComponent:@"Exception.txt"];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"Exception.txt"];
    
    if([fileManager fileExistsAtPath:path])
    {
        NSFileHandle *fileHandle=[NSFileHandle fileHandleForUpdatingAtPath:path];
        
        [fileHandle seekToEndOfFile];
        
        NSData *data=[url dataUsingEncoding:NSUTF8StringEncoding];
        
        [fileHandle writeData:data];
        [fileHandle closeFile];
    }
    
    else
    {
        [url writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
        
    }
    
    
   
    
}

@end

NSString* getAppInfo() {
    
    NSString *appInfo = [NSString stringWithFormat:@"App : %@ %@(%@)\nDevice : %@\nOS Version : %@ %@\n",
                         
                         [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"],
                         
                         [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],
                         
                         [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"],
                         
                         [UIDevice currentDevice].model,
                         
                         [UIDevice currentDevice].systemName,
                         
                         [UIDevice currentDevice].systemVersion];
                         
//                         [UIDevice currentDevice].uniqueIdentifier];
    
    NSLog(@"Crash!!!! %@", appInfo);
    
    return appInfo;
    
}


void MySignalHandler(int signal) {
    
    int32_t exceptionCount = OSAtomicIncrement32(&UncaughtExceptionCount);
    
    if (exceptionCount > UncaughtExceptionMaximum) {
        
        return;
        
    }
    
    NSMutableDictionary *userInfo =[NSMutableDictionary dictionaryWithObject:[NSNumber numberWithInt:signal]                                                     forKey:UncaughtExceptionHandlerSignalKey];
    
    NSArray *callStack = [UncaughtExceptionHandler backtrace];
    
    [userInfo setObject:callStack forKey:UncaughtExceptionHandlerAddressesKey];
    
    [[[UncaughtExceptionHandler alloc] init] performSelectorOnMainThread:@selector(handleException:) withObject:
     
     [NSException exceptionWithName:UncaughtExceptionHandlerSignalExceptionName reason:
      
      [NSString stringWithFormat:NSLocalizedString(@"Signal %d was raised.\n" @"%@", nil),signal, getAppInfo()] userInfo:
      
      [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:signal] forKey:UncaughtExceptionHandlerSignalKey]]waitUntilDone:YES];
    
}

void InstallUncaughtExceptionHandler()
{
    signal(SIGABRT, MySignalHandler);
    
    signal(SIGILL, MySignalHandler);
    
    signal(SIGSEGV, MySignalHandler);
    
    signal(SIGFPE, MySignalHandler);
    
    signal(SIGBUS, MySignalHandler);
    
    signal(SIGPIPE, MySignalHandler);
}

