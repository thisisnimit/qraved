//
//  LoadingView.h
//  LoadingTest
//
//  Created by Admin on 8/27/13.
//  Copyright (c) 2013 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView {
    UIImageView *loadingImgView;
}

+(LoadingView *)sharedLoadingView;

-(void)startLoading;

-(void)stopLoading;

@end
