//
//  SendCall.h
//  Qraved
//
//  Created by Shine Wang on 7/24/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SendCall : NSObject

+(void)sendCall:(NSString *)phoneNum;
@end
