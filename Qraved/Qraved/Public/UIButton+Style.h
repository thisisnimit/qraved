//
//  UIButton+Style.h
//  Qraved
//
//  Created by Sean Liao on 9/10/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Style)

-(id)initProfileTextButton;
-(id)initUtilityButton;
-(id)initHighlightButton;
@end
