//
//  MapUtil.h
//  Qraved
//
//  Created by Jeff on 11/17/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MapUtil : NSObject

+(double)minLatitude:(double)centerLatitude deltaLatitude:(double)deltaLatitude;

+(double)minLongitude:(double)centerLongitude deltaLongitude:(double)deltaLongitude;

+(double)maxLatitude:(double)centerLatitude deltaLatitude:(double)deltaLatitude;

+(double)maxLongitude:(double)centerLongitude deltaLongitude:(double)deltaLongitude;

@end
