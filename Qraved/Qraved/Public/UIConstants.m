//
//  Constants.m
//  App
//
//  Created by Imaginato on 12/11/12.
//  Copyright (c) 2012 Imaginato. All rights reserved.
//

#import "UIConstants.h"

@implementation UIConstants


CGFloat const LEFTLEFTSET=15;
CGFloat const DOUBLELEFTLEFTSET=30;
CGFloat const TOPOFFSET=9;

CGFloat const IPHONE5_PIXEL=640;
CGFloat const IPHONE6_PIXEL=750;
CGFloat const IPHONE6P_PIXEL=1242;


//UIFont
NSString *const FONT_OPEN_SANS_DEFAULT = @"OpenSans";
NSString *const FONT_OPEN_SANS_LIGHT = @"OpenSans-Light";
NSString *const FONT_OPEN_SANS_ITALIC = @"OpenSans-Italic";
NSString *const FONT_OPEN_SANS_SEMIBOLD = @"OpenSans-Semibold";
NSString *const FONT_OPEN_SANS_BOLD = @"OpenSans-Bold";
NSString *const FONT_OPEN_SANS_LIGHT_ITALIC = @"OpenSans-LightItalic";

NSString *const FONT_MONTSERRAT_REGULAR = @"Montserrat-Regular";

NSString *const FONT_PROXIMANOVA_REGULAR = @"ProximaNova-Regular";
NSString *const FONT_GOTHAM_MEDIUM = @"Gotham-Medium";
NSString *const FONT_GOTHAM_BOOK = @"GothamBook";
NSString *const FONT_GOTHAM_LIGHT = @"GothamLight";

NSString *const ALERT_TITLE=@"Message";



NSString *const DEFAULT_FONT_NAME = @"HelveticaNeue";
NSString *const DEFAULT_FONT_BOLD_NAME = @"HelveticaNeue-Bold";
NSString *const DEFAULT_FONT_LIGHT_NAME = @"HelveticaNeue-Light";

double const DEFAULT_BUTTON_TITLE_SIZE=19.0;

double const DEFAULT_INFO_HEADER_FONT_SIZE = 13.0;
double const DEFAULT_INFO_DATA_FONT_SIZE = 12.0;

double const DEFAULT_FONT_LIGHT_HUE = 0.0;
double const DEFAULT_FONT_LIGHT_SAT = 0.0;
double const DEFAULT_FONT_LIGHT_BRI = 0.67;


double const DEFAULT_FONT_NORMAL_HUE = 0.0;
double const DEFAULT_FONT_NORMAL_SAT = 0.0;
double const DEFAULT_FONT_NORMAL_BRI = 0.30;


double const DEFAULT_FONT_BOLD_HUE = 0.0;
double const DEFAULT_FONT_BOLD_SAT = 0.0;
double const DEFAULT_FONT_BOLD_BRI = 0.2;

double const DEFAULT_FONT_INVERSE_BOLD_HUE = 0.0;
double const DEFAULT_FONT_INVERSE_BOLD_SAT = 0.0;
double const DEFAULT_FONT_INVERSE_BOLD_BRI = 0.8;

double const DEFAULT_HIGHLIGHT_HUE = 359;
double const DEFAULT_HIGHLIGHT_SAT = 0.93;
double const DEFAULT_HIGHLIGHT_BRI = 0.70;


NSString *const IMAGE_SUFFIX_LARGE = @"-300px.jpg";
NSString *const IMAGE_SUFFIX_MEDIUM = @"-200px.jpg";
NSString *const IMAGE_SUFFIX_SMALL = @"-60px.jpg";


double const CUISINE_TITLE_FONT_SIZE = 12.0;
double const DETAIL_BODY_FONT_SIZE = 14.0;

double const DETAIL_SECTION_TITLE_SIZE=17.0;
double const PAGE_TITLE_FONT_SIZE = 15.0;
double const PAGE_TITLE_CAPTION_FONT_SIZE = 12.0;

NSString *const POST_TITLE_FONT_NAME = @"HelveticaNeue-Bold";
double const POST_TITLE_FONT_SIZE = 12.0;
double const POST_TITLE_FONT_HUE = 0.0;
double const POST_TITLE_FONT_SAT = 0.0;
double const POST_TITLE_FONT_BRI = 0.50;

double const POST_TITLE_CAPTION_FONT_SIZE = 9.5;
double const POST_TIMESTAMP_FONT_SIZE = 9.0;
double const POST_MESSAGE_FONT_SIZE = 13.0;
double const POST_COUNT_FONT_SIZE = 10.0;
double const POST_COMMENT_FONT_SIZE = 11.0;
double const POST_COMMENT_TIMESTAMP_FONT_SIZE = 9.0;
double const POST_COMMENT_NAME_FONT_SIZE = 11.0;
double const POST_MESSAGE_NICKNAME_FONT_SIZE = 13.0;
double const POST_MESSAGE_TIMESTAMP_FONT_SIZE = 13.0;
double const POST_DETAIL_TITLE_FONT_SIZE = 14.0;
double const POST_DETAIL_TITLE_CAPTION_FONT_SIZE = 13.0;
double const POST_DETAIL_NICKNAME_FONT_SIZE = 12.0;
double const POST_DETAIL_TIMESTAMP_FONT_SIZE = 11.0;
double const POST_DETAIL_DESCRIPION_FONT_SIZE = 11.0;


double const POST_PROFILE_IMAGE_WIDTH = 40.0;
double const POST_PROFILE_IMAGE_HEIGHT = 40.0;
double const POST_MESSAGE_SPACING = 3.0;

NSString *const PERSONAL_MENU_FONT_NAME = @"HelveticaNeue";
double const PERSONAL_MENU_FONT_SIZE = 16;

double const SECTION_HEADER_HEIGHT = 40.0;
double const SECTION_HEADER_FONT_SIZE = 14.0;
NSString *const SECTION_HEADER_FONT_NAME = @"HelveticaNeue-Bold";


//DEFAULT SIZES ************************************************************

double const FONT_SIZE_9 = 9.0;
double const FONT_SIZE_10 = 10.0;
double const FONT_SIZE_11 = 11.0;
double const FONT_SIZE_12 = 12.0;
double const FONT_SIZE_13 = 13.0;
double const FONT_SIZE_14 = 14.0;
double const FONT_SIZE_15 = 15.0;
double const FONT_SIZE_18 = 18.0;
double const FONT_SIZE_16= 16.0;
double const FONT_SIZE_17= 17.0;

//Section colors
double const DEFAULT_BACKGROUND_DARK_HUE = 0.0;
double const DEFAULT_BACKGROUND_DARK_SAT = 0.0;
double const DEFAULT_BACKGROUND_DARK_BRI = 0.2;
double const DEFAULT_BACKGROUND_NORMAL_HUE = 37.0;
double const DEFAULT_BACKGROUND_NORMAL_SAT = 0.12;
double const DEFAULT_BACKGROUND_NORMAL_BRI = 1.00;
double const DEFAULT_BACKGROUND_LIGHT_HUE = 0.0;
double const DEFAULT_BACKGROUND_LIGHT_SAT = 0.0;
double const DEFAULT_BACKGROUND_LIGHT_BRI = 0.97;

//Graphics colors
double const DEFAULT_FRAME_HUE = 0.0;
double const DEFAULT_FRAME_SAT = 0.0;
double const DEFAULT_FRAME_BRI = 0.2;

//sizes and spacing
double const IMAGE_WIDTH = 250.0;
double const POST_CONTENT_WIDTH = 250.0;
double const POST_MESSAGE_CONTENT_WIDTH = 200.0;
double const POST_CELL_MARGIN = 10.0;



//line colors
double const DEFAULT_BORDER_HUE = 0.0;
double const DEFAULT_BORDER_SAT = 0.00;
double const DEFAULT_BORDER_BRI = 0.90;

//table settings
NSString *const TABLE_SECTION_HEADER_FONT_NAME = @"HelveticaNeue-Light";
double const TABLE_SECTION_HEADER_FONT_SIZE = 15.0;

//button colors
double const BUTTON_MAIN_HUE = 0.0;
double const BUTTON_MAIN_SAT = 0.0;
double const BUTTON_MAIN_BRI = 0.0;

double const BUTTON_GRAY_HUE = 0.0;
double const BUTTON_GRAY_SAT = 0.0;
double const BUTTON_GRAY_BRI = 0.80;

NSString *const DEFAULT_APP_FONT = @"HelveticaNeue";
NSString *const DEFAULT_APP_BOLD_FONT = @"HelveticaNeue-Bold";
NSString *const DEFAULT_APP_LIGHT_FONT = @"HelveticaNeue";
double const DEFAULT_STATIC_CELL_LABEL_FONT_SIZE = 14.0;

double const USER_MESSAGE_CONTENT_WIDTH = 200.0;
double const USER_MESSAGE_SPACING = 3.0;
NSString *const USER_MESSAGE_FONT_NAME = @"HelveticaNeue";
NSString *const USER_MESSAGE_NICKNAME_FONT_NAME = @"HelveticaNeue";
double const USER_MESSAGE_FONT_SIZE = 13.0;

double const TAG_CAMERA = 10;
double const TAG_LIKES = 20;
double const TAG_COMMENTS = 30;
double const TAG_POST =40;
double const TAG_MESSAGE = 50;
double const TAG_PROFILE = 60;

NSString *const DEFAULT_IMAGE_STRING=@"placeholder";
NSString *const DEFAULT_IMAGE_STRING2=@"placeholder";

float const CELL_CONTENT_OFFSET=8;

float const MARGIN_CONTENT_OFFSET=10;

CGFloat const SWREVEAL_VIEW_WIDTH=280;

float const BIG_AVATAR_IMAGE_WIDTH=55;
float const BIG_AVATAR_IMAGE_HEIGHT=55;
float const MIDDLE_AVATAR_IMAGE_WIDTH=44;
float const MIDDLE_AVATAR_IMAGE_HEIGHT=44;
float const COMMENT_BIG_AVATAR_IMAGE_WIDTH=35;
float const COMMENT_BIG_AVATAR_IMAGE_HEIGHT=35;
float const COMMENT_SMALL_AVATAR_IMAGE_WIDTH=20;
float const COMMENT_SMALL_AVATAR_IMAGE_HEIGHT=20;
float const SMALL_AVATAR_IMAGE_WIDTH=20;
float const SMALL_AVATAR_IMAGE_HEIGHT=20;
float const SCROLL_IMAGE_WIDTH=75;
float const SCROLL_IMAGE_HEIGHT=75;
float const SCROLL_IMAGE_OFFSET=4;
float const MENU_IMAGE_WIDTH=62;
float const MENU_IMAGE_HEIGHT=62;


float const WATERFALL_SPACING_WIDTH=5;
float const WATERFALL_SPACING_HEIGHT=1;

float const PICKER_VIEW_HEIGHT=42;

float const RATING_STAR_WIDTH=14;

double const UPLOAD_PICTURE_SIZE_WIDTH=1200;
double const DEFAULT_BOTTOM_BUTTON_HEIGHT=40;
double const NAVIGATION_BAR_HEIGHT=44;
double const HOME_VIEW_SIZE_WIDTH=(320-15)/2;
double const DISH_VIEW_SIZE_WIDTH=305.0/2;

float const CELL_HEIGHT_45=45;
float const CELL_HEIGHT_50=50;
float const CELL_HEIGHT_70=70;
float const CELL_HEIGHT_120=120;

NSString *const GET_USERPOINTS_NOTIFICATION=@"GET_USERPOINTS_NOTIFICATION";

float const DELTA_MAP = 0.01;
float const NEAR_MAP = 0.004;

//button style
float const FUNCTION_BUTTON_HEIGHT = 42;
double const FUNCTION_BUTTON_FONT_SIZE = 15.0;
double const SIGN_BUTTON_FONT_SIZE = 12.0;

//Layer

#if TARGET_IPHONE_SIMULATOR
//If on simulator set the user ID to Simulator and participant to Device
NSString * const kUserID = @"Simulator";
NSString * const kParticipant = @"Device";
NSString * const kInitialMessage = @"Simulator test";
#else
//If on device set the user ID to Device and participant to Simulator
NSString * const kUserID = @"Device";
NSString * const kParticipant = @"Simulator";
NSString * const kInitialMessage =  @"Device test";
#endif

NSString * const kAppID = @"1aa89142-bd8e-11e4-b056-3c9b000000f4";
NSString * const kMIMETypeTextPlain = @"text/plain";




@end
