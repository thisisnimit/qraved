//
//  NotificationConsts.h
//  Qraved
//
//  Created by Laura on 14-9-26.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#ifndef Qraved_NotificationConsts_h
#define Qraved_NotificationConsts_h

#define SEARCH_RESTAURANT @"SEARCH_RESTAURANT"
#define GO_DETAIL @"GO_DETAIL"
#define GO_DETAIL_FROM_SEARCH @"GO_DETAIL_FROM_SEARCH"
#define SEARCH_FILTER @"SEARCH_FILTER"

#define BOOKING_DONE @"BOOKING_DONE"

#define CITY_SELECT_NOTIFICATION @"city_select_notification"
#endif
