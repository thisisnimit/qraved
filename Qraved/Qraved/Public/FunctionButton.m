//
//  GoFoodButton.m
//  Qraved
//
//  Created by harry on 2018/2/26.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "FunctionButton.h"

@implementation FunctionButton

+(instancetype)buttonWithType:(UIButtonType)buttonType{
    FunctionButton *foodButton = [super buttonWithType:buttonType];
    
    foodButton.layer.masksToBounds = YES;
    foodButton.layer.cornerRadius = 4;
    
    foodButton.backgroundColor = [UIColor greenButtonColor];
    [foodButton setTitle:@"Order Now" forState:UIControlStateNormal];
    [foodButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    foodButton.titleLabel.font = [UIFont boldSystemFontOfSize:FUNCTION_BUTTON_FONT_SIZE];
    [foodButton setImage:[UIImage imageNamed:@"ic_Deliver.png"] forState:UIControlStateNormal];
    [foodButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
    return foodButton;
}


+ (instancetype)buttonWithType:(UIButtonType)buttonType andTitle:(NSString *)title andBGColor:(UIColor *)bgColor andTitleColor:(UIColor *)titleColor{
    FunctionButton *foodButton = [super buttonWithType:buttonType];
    
    foodButton.layer.masksToBounds = YES;
    foodButton.layer.cornerRadius = 4;
    
    foodButton.backgroundColor = bgColor;
    [foodButton setTitle:title forState:UIControlStateNormal];
    [foodButton setTitleColor:titleColor forState:UIControlStateNormal];
    foodButton.titleLabel.font = [UIFont boldSystemFontOfSize:FUNCTION_BUTTON_FONT_SIZE];
    return foodButton;
}

+ (instancetype)signButtonWithType:(UIButtonType)buttonType andTitle:(NSString *)title andBGColor:(UIColor *)bgColor andTitleColor:(UIColor *)titleColor{
    FunctionButton *foodButton = [super buttonWithType:buttonType];
    
    foodButton.layer.masksToBounds = YES;
    foodButton.layer.cornerRadius = 3;
    
    foodButton.backgroundColor = bgColor;
    [foodButton setTitle:title forState:UIControlStateNormal];
    [foodButton setTitleColor:titleColor forState:UIControlStateNormal];
    foodButton.titleLabel.font = [UIFont boldSystemFontOfSize:SIGN_BUTTON_FONT_SIZE];
    return foodButton;
}

//- (void)goFoodButtonTapped{
//    if ([self.restaurant isOpenRestaurant]) {
//        if (![Tools isBlankString:self.restaurant.goFoodLink]) {
//            [IMGAmplitudeUtil trackGoFoodWithName:@"CL - Go-Food CTA" andJournalId:nil andRestaurantId:self.restaurant.restaurantId andPhotoId:nil andLocation:@"Restaurant Detail Page" andOrigin:nil andOrder:nil];
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.restaurant.goFoodLink]];
//        }
//    }else{
//        [UIAlertController showAlertCntrollerWithViewController:self.viewController alertControllerStyle:UIAlertControllerStyleAlert title:@"Restaurant Closed" message:@"This restaurant is currently closed but don’t worry! You can save it for later." cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Save for later" CallBackBlock:^(NSInteger btnIndex) {
//            if (btnIndex == 1) {
//                [CommonMethod saveRestaurantToList:self.restaurant andViewController:self andCallBackBlock:^(BOOL isSaved) {
////                    restaurant.saved = @1;
////                    [self changeSaveBtn:isSaved];
//                }];
//            }
//        }];
//    }
//}

@end
