//
//  AFClient.h
//  App
//
//  Created by jefftang on 12/11/12.
//  Copyright (c) 2012 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFHTTPClient.h"

@interface AFClient : AFHTTPClient

+ (AFClient *)sharedClient;

@end
