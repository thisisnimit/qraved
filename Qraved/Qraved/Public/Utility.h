//
//  Utility.h
//  Qraved
//
//  Created by Sean Liao on 8/17/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject
+(NSString *)generatePriceLevel:(int)score;


@end
