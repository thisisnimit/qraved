//
//  v2_DiscoveryCategoriesTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/9/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiscoverCategories.h"
@interface v2_DiscoveryCategoriesTableViewCell : UITableViewCell

@property (nonatomic, strong)  DiscoverCategories  *model;
@property (nonatomic, assign) CGFloat pointY;
@property (nonatomic, strong) NSArray *locationArr;//all
@property (nonatomic, strong) NSArray *selectArr;// only
@property (nonatomic, copy) void(^gotoDiscoverResultPage)(DiscoverCategoriesModel *model);

@end
