//
//  MenuPhotoGalleryHandler.h
//  Qraved
//
//  Created by imaginato on 16/4/7.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGMenu.h"

@interface MenuPhotoGalleryHandler : NSObject

+(void)getMenuGalleryDataFromService:(NSNumber *)restaurantId offset:(NSNumber*)offset andBlock:(void (^)(NSArray *menuList,NSNumber *menuPhotoCount,NSNumber *menuPhotoCountTypeRestant,NSNumber *menuPhotoCountTypeBar,NSNumber *menuPhotoCountTypeDelivery,IMGMenu *menu_))block;
+(void)getSelfMenuGalleryDataFromService:(NSNumber *)restaurantId userId:(NSNumber*)userId offset:(int )offset andBlock:(void (^)(NSArray *menuList,NSNumber *menuPhotoCount,NSNumber *menuPhotoCountTypeRestant,NSNumber *menuPhotoCountTypeBar,NSNumber *menuPhotoCountTypeDelivery,IMGMenu *menu_))block;

@end
