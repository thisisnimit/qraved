//
//  V2_ArticleCell.m
//  Qraved
//
//  Created by harry on 2017/6/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_ArticleCell.h"
#import "WebServiceV2.h"
@implementation V2_ArticleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lblTitle.textColor = [UIColor color333333];
    
}

- (void)setModel:(IMGMediaComment *)model{
    _model = model;
    
    UIImage *placeHoderImage =[UIImage imageNamed:DEFAULT_IMAGE_STRING];
    
    if ([model.journalImageUrl hasPrefix:@"http"]) {
        [self.articleImageView sd_setImageWithURL:[NSURL URLWithString:model.journalImageUrl] placeholderImage:placeHoderImage];
    }else{
        [self.articleImageView sd_setImageWithURL:[NSURL URLWithString:[model.journalImageUrl returnFullImageUrlWithWidth:125 andHeight:85]] placeholderImage:placeHoderImage];
    }
 
    
    self.lblTitle.text = model.journalTitle;
    if ([model.hasVideo
         isEqual:@1]) {
        self.videoImageView.hidden = NO;
    }else{
        self.videoImageView.hidden = YES;
    }
}

- (void)setIsBrand:(BOOL)isBrand{
    _isBrand = isBrand;
    if (isBrand) {
        self.lblTitle.numberOfLines = 3;
    }
}

- (void)setJournalModel:(V2_DiscoverListResultRestaurantTableViewCellSubModel *)journalModel{
    _journalModel = journalModel;
    
    UIImage *placeHoderImage =[UIImage imageNamed:DEFAULT_IMAGE_STRING];
    
    if ([journalModel.img hasPrefix:@"http"]) {
        [self.articleImageView sd_setImageWithURL:[NSURL URLWithString:journalModel.img] placeholderImage:placeHoderImage];
    }else{
        [self.articleImageView sd_setImageWithURL:[NSURL URLWithString:[journalModel.img returnFullImageUrlWithWidth:125 andHeight:85]] placeholderImage:placeHoderImage];
    }
    
    
    self.lblTitle.text = journalModel.title;
    if ([journalModel.hasVideo
         isEqual:@1]) {
        self.videoImageView.hidden = NO;
    }else{
        self.videoImageView.hidden = YES;
    }
}

@end
