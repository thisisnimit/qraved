//
//  V2_PersonalizedModel.h
//  Qraved
//
//  Created by harry on 2017/6/15.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface V2_PersonalizedModel : NSObject

@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSNumber *type;
@property (nonatomic,copy) NSArray *data;

@end


//@interface V2_GuideModel : NSObject
//
//@property (nonatomic,copy) NSString *header_image;
//@property (nonatomic,copy) NSString *header_image_alt_text;
////
//@property (nonatomic,copy) NSNumber *guideId;
//@property (nonatomic,copy) NSString *image_url;
//@property (nonatomic,copy) NSString *meta_description;
//@property (nonatomic,copy) NSString *meta_keyword;
//@property (nonatomic,copy) NSString *page_content;
//@property (nonatomic,copy) NSNumber *page_show;
//@property (nonatomic,copy) NSString *page_title;
//@property (nonatomic,copy) NSString *page_url;
//@property (nonatomic,copy) NSNumber *sort_order;
//
//@end
