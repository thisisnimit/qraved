//
//  DiscoverCategories.h
//  Qraved
//
//  Created by Adam.zhang on 2017/9/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DiscoverCategories : NSObject

@property (nonatomic, strong)  NSNumber  *myID;
@property (nonatomic, strong)  NSNumber  *object_id;
@property (nonatomic, strong)  NSNumber  *parent_object_id;
@property (nonatomic, strong)  NSString  *name;
@property (nonatomic, strong)  NSNumber  *order;
@property (nonatomic, strong)  NSArray  *childrens;
@end



@interface DiscoverCategoriesModel : NSObject
@property (nonatomic, strong)  NSNumber  *myID;
@property (nonatomic, strong)  NSNumber  *object_id;
@property (nonatomic, strong)  NSNumber  *parent_object_id;
@property (nonatomic, strong)  NSString  *name;
@property (nonatomic, strong)  NSNumber  *order;
@property (nonatomic, strong)  NSString  *icon_image_url;
@end
