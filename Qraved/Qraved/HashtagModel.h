//
//  HashtagModel.h
//  Qraved
//
//  Created by harry on 2017/12/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HashtagModel : NSObject

@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSNumber *cityId;
@property (nonatomic, copy) NSNumber *commentCount;
@property (nonatomic, copy) NSNumber *likeCount;
@property (nonatomic, copy) NSNumber *postCount;
@property (nonatomic, copy) NSString *tag;
@property (nonatomic, copy) NSString *type;


@end
