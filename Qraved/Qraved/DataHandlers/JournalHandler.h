//
//  JournalHandler.h
//  Qraved
//
//  Created by Lucky on 15/6/29.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataHandler.h"
#import "IMGMediaComment.h"
#import "IMGUser.h"
#import "IMGJournalComment.h"

@interface JournalHandler : DataHandler

+ (void)getJournalListWithParams:(NSMutableDictionary *)params andBlock:(void (^)(NSArray *dataArray,int totalCount))block failure:(void (^)(NSError *error))failureBlock;
+ (void)getJournalDetailWithJournal:(IMGMediaComment *)journal andJournalBlock:(void (^)(IMGMediaComment *journal, NSNumber *goFoodCount))journalBlock andAllDataBlock:(void (^)(NSNumber *_componentCount,NSMutableArray *dataDic,NSArray *commentList,NSNumber *commentCount,NSNumber *menuPhotoCount))allDataBlock andRelatedPostBlock:(void (^)(NSArray *relatedArr))relatedPostBlock;
+ (void)getMoreJournalDetailWithJournal:(IMGMediaComment *)journal andComponentoffset:(NSNumber*)componentoffset andComponents:(NSMutableArray*) _components andAllDataBlock:(void (^)(NSNumber *_componentCount, NSMutableArray *dataDic))allDataBlock failure:(void (^)(NSError *error))failureBlock;


+ (void)getJournalDetailWithJournalArticleId:(NSNumber *)journalId andBlock:(void (^)(NSString *content,NSString *webSite))block;
+ (void)journalLikeWithJournal:(IMGMediaComment *)journal andUser:(IMGUser *)user;
+ (void)journalLikeWithJournal:(IMGMediaComment *)journal andUser:(IMGUser *)user completion:(void(^)(BOOL status,NSString *message))block;
+ (void)journalUnlikeWithJournal:(IMGMediaComment *)journal andUser:(IMGUser *)user completion:(void(^)(BOOL status,NSString *message))block;
+ (void)getJournalCategoryWithBlock:(void (^)(NSArray *dataArray))block;
+ (void)getJournalDetailWithJournalId:(NSNumber *)journalId andJournalBlock:(void (^)(IMGMediaComment *journal))journalBlock;

+ (void)searchJournalarticleWithContent:(NSString *)content andBlock:(void (^)(NSArray *dataArray))block;

+ (void)getJournalDetailTypeArticleIdWithJournalId:(NSNumber *)journalId andJournalBlock:(void (^)(IMGMediaComment *journal))journalBlock;

+(void)previousComment:(NSString *)commentIdStr targetId:(NSNumber*)articleId andBlock:(void (^)(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount))block;
+(void)postComment:(NSString *)commentStr articleId:(NSNumber*)articleId andBlock:(void (^)(IMGJournalComment *journalComment))block  failure:(void(^)(NSString *errorReason))failureBlock;
+(void)likeJournal:(BOOL)liked articleId:(NSNumber*)articleId andBlock:(void (^)())block  failure:(void(^)(NSString *exceptionMsg))failureBlock;
+ (void)getGoFoodRestaurantList:(IMGMediaComment *)journal andBlock:(void(^)(NSArray  *restaurantArr))successBlock andError:(void(^)(NSError *error))errorBlock;
@end
