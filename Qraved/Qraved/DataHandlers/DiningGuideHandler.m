//
//  Qraved
//
//  Created by Jeff on 10/8/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "DiningGuideHandler.h"
#import "DBManager.h"
#import "IMGDiningGuide.h"

@implementation DiningGuideHandler


-(void)getDatasFromServer
{
    [[IMGNetWork sharedManager] GET:@"diningguides" parameters:nil progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueue];
        [queue inDatabase:^(FMDatabase *db) {
            [db executeUpdate:@"delete from IMGDiningGuide"];
            NSArray * array =responseObject;
            for (int i=0; i<array.count; i++) {
                NSArray *diningGuideArray = [array objectAtIndex:i];
                NSDictionary * diningGuideDictionary = [diningGuideArray objectAtIndex:0];
                IMGDiningGuide * guide =[[IMGDiningGuide alloc]init];
                [guide setValuesForKeysWithDictionary:diningGuideDictionary];
                guide.restaurantCount = [diningGuideArray objectAtIndex:1];
                guide.diningGuideId = [diningGuideDictionary objectForKey:@"id"];
                [[DBManager manager] insertModel:guide selectField:@"diningGuideId" andSelectID:guide.diningGuideId andFMDatabase:db];
            }
        }];
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"%@",error.localizedDescription);
    }];
    
}


@end
