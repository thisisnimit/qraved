//
//  RestaurantHandler.m
//  Qraved
//
//  Created by Laura on 14-9-28.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "RestaurantHandler.h"
#import "DBManager.h"

#import "IMGRestaurantTag.h"
#import "IMGRestaurantEvent.h"
#import "IMGRestaurantCuisine.h"
#import "IMGRestaurantDistrict.h"

#import "IMGReview.h"
#import "IMGMenu.h"
#import "IMGLocation.h"
#import "IMGRestaurantOffer.h"
#import "IMGRestaurantImage.h"
#import "IMGRestaurantDiningGuide.h"
#import "IMGMediaComment.h"
#import "IMGRestaurantLike.h"
#import "IMGDish.h"
#import "IMGSlot.h"
#import "IMGRestaurantLink.h"
#import "BookUtil.h"
#import "IMGUser.h"
#import "LoadingView.h"
#import "IMGRestaurantEventComment.h"
#import "IMGRestaurantEvent.h"
#import "AppDelegate.h"
//#import "IMGDishComment.h"

#define PAGE_MAX 50

@implementation RestaurantHandler
{
    
    NSNumber *offset;
}

-(void)initFromServer
{
    [[DBManager manager] deleteWithSql:@"delete from IMGRestaurantLike"];
    [[DBManager manager] deleteWithSql:@"delete from IMGRestaurantDiningGuide"];
    [[DBManager manager] deleteWithSql:@"delete from IMGRestaurantImage"];
    [[DBManager manager] deleteWithSql:@"delete from IMGRestaurantTag"];
    [[DBManager manager] deleteWithSql:@"delete from IMGMenu"];
    [[DBManager manager] deleteWithSql:@"delete from IMGRestaurantCuisine"];
    [[DBManager manager] deleteWithSql:@"delete from IMGRestaurantDistrict"];
    [[DBManager manager] deleteWithSql:@"delete from IMGRestaurantEvent"];
    [[DBManager manager] deleteWithSql:@"delete from IMGRestaurantOffer"];
    [[DBManager manager] deleteWithSql:@"delete from IMGDish"];
    
    [[DBManager manager] deleteWithSql:@"delete from IMGMediaComment"];
    [[DBManager manager] deleteWithSql:@"delete from IMGRestaurant"];
    [[DBManager manager] deleteWithSql:@"delete from IMGReview"];
    [[DBManager manager] deleteWithSql:@"delete from IMGSlot"];
    offset = @0;
    
    dataArray = [[NSMutableArray array]init];
    NSDictionary *parameters = [[NSDictionary alloc] initWithObjectsAndKeys:offset,@"offset", [NSNumber numberWithInt: PAGE_MAX ],@"max",@"1",@"downloadFromApp", nil];
    [self getDatasFromServer:parameters];
}

-(void)getDatasFromServer:(NSDictionary *)parameters
{
    NSLog(@"next offset-------%@",offset);
    [[IMGNetWork sharedManager] GET:@"app/restaurant/list" parameters:parameters success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSString *exceptionMessage = [responseObject objectForKey:@"exceptionmsg"];
         if(exceptionMessage!=nil){
             NSLog(@"getRestaurantsWithParameters erors when get datas, exceptionmsg is:%@",exceptionMessage);
             return;
         }
         
         NSArray *array = [responseObject objectForKey:@"restaurantList"];
         
         for (int i=0; i<array.count; i++) {
             NSDictionary *restaurantDictionary = [array objectAtIndex:i];
             
             IMGRestaurant *restaurant =  [RestaurantHandler parseJson2Entity:(NSDictionary *)restaurantDictionary andBlock:^(IMGRestaurant *restaurant) {
                 
             }];
             [dataArray addObject:restaurant];
             NSLog(@"restaurant.restaurantId-------%@",restaurant.restaurantId);
             
         }
         NSLog(@"current offset-------%@",offset);
         NSInteger restaurantCount = [[responseObject objectForKey:@"restaurantCount"] integerValue];
         if(restaurantCount==0){
             //NSLog(@"So werid the restaurantCount-------%ld",restaurantCount);
         }
        // NSLog(@"restaurantCount-------%ld",restaurantCount);
         offset = [NSNumber numberWithInt:[offset intValue]+PAGE_MAX];
         if([offset integerValue]<restaurantCount){
             NSDictionary *newParameters = [[NSDictionary alloc] initWithObjectsAndKeys:offset,@"offset",[NSNumber numberWithInt: PAGE_MAX ],@"max",@"1",@"downloadFromApp", nil];
             [self getDatasFromServer:newParameters];
         }
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error) {
         NSLog(@"getRestaurantsWithParameters erors when get datas:%@",error.localizedDescription);
     }];
    
}

+(void)getRestaurantDetailFromServer:(NSNumber*)restaurantId andBlock:(void(^)(IMGRestaurant* restaurant))block{
//    NSDictionary *parameters=@{@"restaurantId":restaurantId};
    NSDictionary *parameters=[[NSDictionary alloc]initWithObjectsAndKeys:restaurantId,@"restaurantId", nil];
        [[IMGNetWork sharedManager] GET:@"app/restaurant/detail" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation,id responseObject){
        NSString *exceptionmsg=[responseObject objectForKey:@"exceptionmsg"];
        if(!exceptionmsg){
            IMGRestaurant *restaurant=[[IMGRestaurant alloc] init];
            [restaurant setValuesForKeysWithDictionary:responseObject];
            block(restaurant);
        }else{
            block(nil);
        }
    } failure:^(NSURLSessionDataTask *operation,NSError *error){

    }];
}

+(void)getDetailFromServer:(NSNumber*)restaurantId andBlock:(void (^)(IMGRestaurant * restaurant))block{
    long timeUpdated=0;
    IMGRestaurant *existRestaurant = [IMGRestaurant findById:restaurantId];
    if(existRestaurant!=nil&&existRestaurant.timeUpdated!=nil&& ![existRestaurant.timeUpdated isKindOfClass:[NSNull class]]){
        timeUpdated=[existRestaurant.timeUpdated integerValue];
    }
    NSNumber *maxMenuId=[NSNumber numberWithInt:0];
    FMResultSet *resultSet = [[DBManager manager]executeQuery:[NSString stringWithFormat:@"select max(menuId) as maxMenuId from IMGMenu where restaurantId=%@",restaurantId]];
    if([resultSet next]){
        maxMenuId = [resultSet objectForColumnName:@"maxMenuId"];
        //maxMenuId = 0;
    }
    [resultSet close];
    
    NSDictionary * params = @{@"id":restaurantId,@"downloadFromApp":@"1",@"timeUpdated":[NSNumber numberWithLong:timeUpdated],@"maxMenuId":maxMenuId};
    //NSDictionary * params = @{@"id":restaurantId,@"downloadFromApp":@"1",@"timeUpdated":[NSNumber numberWithInt:0],@"maxMenuId":maxMenuId};
    [[IMGNetWork sharedManager] GET:@"app/restaurant" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSDictionary *restaurantDictionary = (NSDictionary *)responseObject;
         if([restaurantDictionary objectForKey:@"noupdate"]!=nil && [[restaurantDictionary objectForKey:@"noupdate"] intValue]==1){
             block(nil);
         }else{
             [RestaurantHandler parseJson2Entity:(NSDictionary *)restaurantDictionary andBlock:^(IMGRestaurant *restaurant) {
                 block(nil);
             }];
         }
     }failure:^(NSURLSessionDataTask *operation, NSError *error) {
         block(nil);
     }];
}
+ (void)getOffersFromServer:(NSString *)offerIdsStr andRestaurantId:(NSNumber *)restaurantId
{
    NSDictionary * params = @{@"ids":offerIdsStr};
    [[IMGNetWork sharedManager] GET:@"restaurant/offer/list" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        //NSLog(@"responseObject --> 0 = %@",[responseObject objectAtIndex:0] );
        if ([responseObject count] != 0)
        {
            for (NSDictionary *dic in responseObject)
            {
                [RestaurantHandler addOffer:dic andRestaurantId:restaurantId];
            }
        }
        
        
    }failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"getOffersFromServer erors when get datas:%@",error.description);
    }];
}
+ (void)getOfferFromServer:(NSNumber *)restaurantId andBookDate:(NSDate *)bookDate andBlock:(void (^)())block
{
    NSDictionary * params;
    IMGUser *user = [IMGUser currentUser];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *currentDateStr = [[NSString alloc] init];
    if (bookDate)
    {
        currentDateStr = [dateFormatter stringFromDate:bookDate];
    }

    
    if (user.userId && user.token)
    {
        if (bookDate)
        {
            params = @{@"restaurantId":restaurantId,@"userId":user.userId,@"offerUnsign":@"1",@"bookDate":currentDateStr};
        }
        else
        {
            params = @{@"restaurantId":restaurantId,@"userId":user.userId,@"offerUnsign":@"1"};
        }
    }
    else
    {
        if (bookDate)
        {
            params = @{@"restaurantId":restaurantId,@"bookDate":currentDateStr};
        }
        else
        {
            params = @{@"restaurantId":restaurantId};
        }
    }
    
    [[IMGNetWork sharedManager] GET:@"restaurant/offers" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        [[DBManager manager] deleteWithSql:[NSString stringWithFormat:@"delete from IMGRestaurantOffer where restaurantId=%@ ;",restaurantId]];
        
        if ([responseObject count] != 0)
        {
            for (NSDictionary *dic in responseObject)
            {
                [RestaurantHandler addOffer:dic andRestaurantId:restaurantId];
            }
        }
        block();
        
    }failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"getOffersFromServer erors when get datas:%@",error.description);
    }];
}
+ (void)geOfferFromServer:(NSNumber *)restaurantId andBookDate:(NSDate *)bookDate andBlock:(void (^)(NSMutableArray *restaurantOfferArray))block
{
    
    NSDictionary * params;
    IMGUser *user = [IMGUser currentUser];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *currentDateStr = [[NSString alloc] init];
    if (bookDate)
    {
        currentDateStr = [dateFormatter stringFromDate:bookDate];
    }
    
    
    if (user.userId && user.token)
    {
        if (bookDate)
        {
            params = @{@"restaurantId":restaurantId,@"userId":user.userId,@"offerUnsign":@"1",@"bookDate":currentDateStr};
        }
        else
        {
            params = @{@"restaurantId":restaurantId,@"userId":user.userId,@"offerUnsign":@"1"};
        }
    }
    else
    {
        if (bookDate)
        {
            params = @{@"restaurantId":restaurantId,@"bookDate":currentDateStr};
        }
        else
        {
            params = @{@"restaurantId":restaurantId};
        }
    }
    
    [[IMGNetWork sharedManager] GET:@"restaurant/offers" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        NSArray *returnArray = (NSArray*)responseObject;
        [[LoadingView sharedLoadingView] stopLoading];

        if (returnArray.count>0) {
            NSMutableArray *offerArray = [NSMutableArray array];
            for (NSDictionary *dic in returnArray) {
                IMGRestaurantOffer *restaurantOffer= [[IMGRestaurantOffer alloc] init];
                [restaurantOffer setValuesForKeysWithDictionary:dic];
                restaurantOffer.restaurantId = restaurantId;
                if (!restaurantOffer.offerSlotId.intValue)
                {
                    restaurantOffer.offerSlotId = [NSNumber numberWithInt:0];
                }
                
                NSDictionary *pictureInfo=[dic objectForKey:@"pictureInfo"];
                double width = [[pictureInfo objectForKey:@"width"] doubleValue];
                double height = [[pictureInfo objectForKey:@"height"] doubleValue];
                //        restaurantOffer.offerImageH = [NSNumber numberWithInt:((DeviceWidth - LEFTLEFTSET*2)*height)/width];
                
                
                if (width!=0||height!=0) {
                    restaurantOffer.offerImageH=[NSNumber numberWithDouble:(DeviceWidth*height)/width];
                    
                }
                if ([restaurantOffer.offerImageH floatValue] < 0)
                {
                    restaurantOffer.offerImageH = 0;
                }
                
                if (!restaurantOffer.offerSlotId.intValue)
                {
                    restaurantOffer.offerSlotId = [NSNumber numberWithInt:0];
                }

                [offerArray addObject:restaurantOffer];
            }
            block(offerArray);
        }
        else{
            block(nil);
        }
        [[DBManager manager] deleteWithSql:[NSString stringWithFormat:@"delete from IMGRestaurantOffer where restaurantId=%@ ;",restaurantId]];
        
        if ([responseObject count] != 0)
        {
            for (NSDictionary *dic in responseObject)
            {
                [RestaurantHandler addOffer:dic andRestaurantId:restaurantId];
            }
        }
        
    }failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"getOffersFromServer erors when get datas:%@",error.description);
        [[LoadingView sharedLoadingView] stopLoading];

    }];
}

+(void)getAllEventsFromServer:(NSNumber*)restaurantId andBlock:(void(^)(NSArray *))block;{
    NSDictionary * params = @{@"restaurantId":restaurantId};
    [[IMGNetWork sharedManager] GET:@"app/restauranteventphoto/list" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        
        
        NSArray *phoyoList = [[NSArray alloc]initWithArray:responseObject[@"restaurantEventList"]];
        NSMutableArray *photoEventList = [[NSMutableArray alloc]init];
        for(id item in phoyoList){
            IMGRestaurantEvent *event = [[IMGRestaurantEvent alloc]init];
            [event setValuesForKeysWithDictionary:item];
            [event updatePhotoCredit:item[@"photoCreditImpl"]];
            event.eventId=[item objectForKey:@"id"];
            event.restaurantId = [NSNumber numberWithInt:[restaurantId intValue]];
            [photoEventList addObject:event];
        }
        
        block(photoEventList);

    }failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"getEventsFromServer erors when get datas:%@",error.description);
    }];
}


+(void)getEventsFromServer:(NSNumber*)restaurantId andBlock:(void(^)(NSArray *))block;{
    NSDictionary * params = @{@"restaurantId":restaurantId};
    [[IMGNetWork sharedManager] GET:@"restaurant/event/list" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        
        
        NSArray *phoyoList = [[NSArray alloc]initWithArray:responseObject[@"eventList"]];
        NSMutableArray *photoEventList = [[NSMutableArray alloc]init];
        for(id item in phoyoList){
            IMGRestaurantEvent *event = [[IMGRestaurantEvent alloc]init];
            [event setValuesForKeysWithDictionary:item];
            event.eventId=[item objectForKey:@"id"];
            event.restaurantId = [NSNumber numberWithInt:[restaurantId intValue]];
            [photoEventList addObject:event];
        }
        
        block(photoEventList);
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            NSArray  *eventArray=[responseObject objectForKey:@"eventList"];
            for(int i=0; i<eventArray.count; i++){
                NSDictionary *eventDictionary = [eventArray objectAtIndex:i];
                IMGRestaurantEvent *restaurantEvent = [[IMGRestaurantEvent alloc]init];
                restaurantEvent.eventId=[eventDictionary objectForKey:@"id"];
                restaurantEvent.restaurantId = [NSNumber numberWithInt:[restaurantId intValue]];
                [[DBManager manager]insertRelation:restaurantEvent selectField:@"eventId" andSelectID:restaurantEvent.eventId relationField:@"restaurantId" andRelationId:restaurantEvent.restaurantId];
            }
        });
    }failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"getEventsFromServer erors when get datas:%@",error.description);
    }];
}
+(void)getDetailReviewsFromServer:(NSDictionary*)params andBlock:(void(^)(NSArray *reviwList,NSDictionary *,NSString *restaurantScore,NSNumber *reviewCount,NSString *ratingCounts,NSString *ratingCount))block;
{
    [[IMGNetWork sharedManager] GET:@"app/restaurant/reviews/groupby/user" parameters:params progress:nil success:^(NSURLSessionDataTask *operation,id responseObject){
        NSLog(@"reviews %@",responseObject);
    
        NSArray *reviewList=[responseObject objectForKey:@"reviewList"];
        NSMutableDictionary *reviewDishDic=[[NSMutableDictionary alloc] init];
        NSMutableArray *reviews=[[NSMutableArray alloc] init];
        for(NSDictionary *reviewdic in reviewList){
            IMGReview *review=[[IMGReview alloc] init];
            [review setValuesForKeysWithDictionary:reviewdic];
            review.restaurantId = [responseObject objectForKey:@"restaurantId"];
            [reviews addObject:review];
            NSMutableArray *dishArray=[[NSMutableArray alloc] init];
            for(id dishdic in [reviewdic objectForKey:@"dishList"]){
                IMGDish *dish=[[IMGDish alloc] init];
                [dish setValuesForKeysWithDictionary:dishdic];
                dish.descriptionStr=[dishdic objectForKey:@"description"];
                [dishArray addObject:dish];
            }
            [reviewDishDic setObject:dishArray forKey:[reviewdic objectForKey:@"id"]];
        }
        block(reviewList,reviewDishDic,[responseObject objectForKey:@"restaurantScore"],[responseObject objectForKey:@"reviewCount"],[responseObject objectForKey:@"ratingCounts"],[responseObject objectForKey:@"ratingCount"]);
    } failure:^(NSURLSessionDataTask *operation,NSError *error){
        block([NSArray arrayWithObject:error.description],nil,nil,nil,nil,nil);
    }];
}

+(void)getAllReviews:(NSMutableDictionary*)params andBlock:(void (^)(NSArray *reviewList, IMGRestaurant *restaurant)) block failure:(void(^)())failureBlock{
    [[IMGNetWork sharedManager] GET:@"app/restaurant/allreviews" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *reviews=[[NSMutableArray alloc] init];
        
        for (NSDictionary *reviewdic in [responseObject objectForKey:@"reviewList"]) {
            IMGReview *review=[[IMGReview alloc] init];
            [review setValuesForKeysWithDictionary:reviewdic];
            if ([[reviewdic objectForKey:@"reviewType"] isEqual:@1]) {
                review.isReadMore = NO;
                review.restaurantId = [responseObject objectForKey:@"restaurantId"];
                NSMutableArray *dishArray=[[NSMutableArray alloc] init];
                for(id dishdic in [reviewdic objectForKey:@"dishList"]){
                    IMGDish *dish=[[IMGDish alloc] init];
                    [dish setValuesForKeysWithDictionary:dishdic];
                    dish.descriptionStr=[dishdic objectForKey:@"description"];
                    [dish updatePhotoCredit:[dishdic objectForKey:@"photoCredit"]];
                    NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
                    [formatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
                    NSDate *createTime=[formatter dateFromString:[NSString stringWithFormat:@"%@",dish.createTime]];
                    dish.createTime=[NSNumber numberWithDouble:createTime.timeIntervalSince1970];
                    [dishArray addObject:dish];
                }
                review.imageArray = dishArray;
            }else if ([[reviewdic objectForKey:@"reviewType"] isEqual:@2]){
                review.restaurantId = [responseObject objectForKey:@"restaurantId"];
                review.reviewId = [reviewdic objectForKey:@"postId"];
                review.timeCreated = [reviewdic objectForKey:@"createTime"];
                NSMutableArray *dishArray=[[NSMutableArray alloc] init];
                for(id dishdic in [reviewdic objectForKey:@"photoList"]){
                    IMGDish *dish=[[IMGDish alloc] init];
                    [dish setValuesForKeysWithDictionary:dishdic];
                    dish.descriptionStr=[dishdic objectForKey:@"description"];
                    [dish updatePhotoCredit:[dishdic objectForKey:@"photoCredit"]];
                    dish.instagramLink = [reviewdic objectForKey:@"link"];
                    [dishArray addObject:dish];
                }
                review.imageArray = dishArray;
            }
            [reviews addObject:review];
        }
        
        IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
        [restaurant setValuesForKeysWithDictionary:responseObject];
        restaurant.ratingScore = [responseObject objectForKey:@"restaurantScore"];
        block(reviews, restaurant);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failureBlock();
    }];
}

+(void)getReviews:(NSMutableDictionary*)params andBlock:(void (^)(NSArray *reviewList)) block failure:(void(^)())failureBlock{
    [[IMGNetWork sharedManager] GET:@"app/restaurant/qravedreviews" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSArray *reviewList=[responseObject objectForKey:@"reviewList"];
        //NSMutableDictionary *reviewDishDic=[[NSMutableDictionary alloc] init];
        NSMutableArray *reviews=[[NSMutableArray alloc] init];
        for(NSDictionary *reviewdic in reviewList){
            IMGReview *review=[[IMGReview alloc] init];
            [review setValuesForKeysWithDictionary:reviewdic];
            review.isReadMore = NO;
            review.restaurantId = [responseObject objectForKey:@"restaurantId"];
            
            NSMutableArray *dishArray=[[NSMutableArray alloc] init];
            for(id dishdic in [reviewdic objectForKey:@"dishList"]){
                IMGDish *dish=[[IMGDish alloc] init];
                [dish setValuesForKeysWithDictionary:dishdic];
                dish.descriptionStr=[dishdic objectForKey:@"description"];
                [dish updatePhotoCredit:[dishdic objectForKey:@"photoCredit"]];
                NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
                NSDate *createTime=[formatter dateFromString:[NSString stringWithFormat:@"%@",dish.createTime]];
                dish.createTime=[NSNumber numberWithDouble:createTime.timeIntervalSince1970];
                [dishArray addObject:dish];
            }
            review.imageArray = dishArray;
            
            [reviews addObject:review];
        }
        block(reviews);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failureBlock();
    }];
}
+(void)getInstagramReviews:(NSMutableDictionary*)params andBlock:(void (^)(NSArray *instagramReviewList)) block failure:(void(^)())failureBlock{
    [[IMGNetWork sharedManager] GET:@"app/restaurant/instagramreviews" parameters:params progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSArray *reviewList=[responseObject objectForKey:@"reviewList"];
        //NSMutableDictionary *reviewDishDic=[[NSMutableDictionary alloc] init];
        NSMutableArray *reviews=[[NSMutableArray alloc] init];
        for(NSDictionary *reviewdic in reviewList){
            IMGReview *review=[[IMGReview alloc] init];
            [review setValuesForKeysWithDictionary:reviewdic];
            review.restaurantId = [responseObject objectForKey:@"restaurantId"];
            review.reviewId = [reviewdic objectForKey:@"postId"];
            review.timeCreated = [reviewdic objectForKey:@"createTime"];
            NSMutableArray *dishArray=[[NSMutableArray alloc] init];
            for(id dishdic in [reviewdic objectForKey:@"photoList"]){
                IMGDish *dish=[[IMGDish alloc] init];
                [dish setValuesForKeysWithDictionary:dishdic];
                dish.descriptionStr=[dishdic objectForKey:@"description"];
                [dish updatePhotoCredit:[dishdic objectForKey:@"photoCredit"]];
                dish.instagramLink = [reviewdic objectForKey:@"link"];
//                NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
//                [formatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
//                NSDate *createTime=[formatter dateFromString:[NSString stringWithFormat:@"%@",[reviewdic objectForKey:@"createTimeStr"]]];
//                dish.createTime=[NSNumber numberWithDouble:createTime.timeIntervalSince1970];
                [dishArray addObject:dish];
            }
            review.imageArray = dishArray;
            
            [reviews addObject:review];
        }
        block(reviews);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failureBlock();
    }];
}

+(void)getUserAllReviewsFromServer:(NSMutableDictionary*)params andBlock:(void(^)(BOOL status,NSUInteger total,NSArray *,NSDictionary *))block{
    IMGUser *currentUser = [IMGUser currentUser];
    if ([currentUser.userId intValue]) {
        [params setValue:currentUser.userId forKey:@"loginUserId"];
    }else{
        [params setValue:@(0) forKey:@"loginUserId"];
    }
    [[IMGNetWork sharedManager] POST:@"app/restaurant/reviews/byuser" parameters:params progress:nil success:^(NSURLSessionDataTask *operation,id responseObject){
        NSLog(@"reviews %@",responseObject);
        //        NSNumber *total=[responseObject objectForKey:@"reviewCount"];
        NSNumber *ratingCount=[responseObject objectForKey:@"userReviewCount"];
        NSArray *reviewList=[responseObject objectForKey:@"reviewList"];
        NSMutableDictionary *reviewDishDic=[[NSMutableDictionary alloc] init];
        NSMutableArray *reviews=[[NSMutableArray alloc] init];
        for(NSDictionary *reviewdic in reviewList){
            IMGReview *review=[[IMGReview alloc] init];
            [review setValuesForKeysWithDictionary:reviewdic];
            review.restaurantId = [responseObject objectForKey:@"restaurantId"];
            review.summarize = [review.summarize stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [reviews addObject:review];
            NSMutableArray *dishArray=[[NSMutableArray alloc] init];
            for(id dishdic in [reviewdic objectForKey:@"dishList"]){
                IMGDish *dish=[[IMGDish alloc] init];
                [dish setValuesForKeysWithDictionary:dishdic];
                dish.descriptionStr=[dishdic objectForKey:@"description"];
                [dish updatePhotoCredit:[dishdic objectForKey:@"photoCredit"]];
                dish.createTime = [dishdic objectForKey:@"dishCreateTime"];
                [dishArray addObject:dish];
            }
            [reviewDishDic setObject:dishArray forKey:[reviewdic objectForKey:@"id"]];
        }
        block(YES,[ratingCount unsignedIntegerValue],reviews,reviewDishDic);
    } failure:^(NSURLSessionDataTask *operation,NSError *error){
        block(NO,0,[NSArray arrayWithObject:error.description],nil);
    }];
}

+(void)getReviewsFromServer:(NSMutableDictionary*)params andBlock:(void(^)(BOOL status,NSUInteger total,NSArray *,NSDictionary *))block{
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token){
        [params setValue:user.userId forKey:@"userId"];
    }
    [[IMGNetWork sharedManager] GET:@"app/restaurant/reviews/groupby/user" parameters:params progress:nil success:^(NSURLSessionDataTask *operation,id responseObject){
        NSLog(@"reviews %@",responseObject);
//        NSNumber *total=[responseObject objectForKey:@"reviewCount"];
        NSNumber *ratingCount=[responseObject objectForKey:@"ratingCount"];
        NSArray *reviewList=[responseObject objectForKey:@"reviewList"];
        NSMutableDictionary *reviewDishDic=[[NSMutableDictionary alloc] init];
        NSMutableArray *reviews=[[NSMutableArray alloc] init];
        for(NSDictionary *reviewdic in reviewList){
            IMGReview *review=[[IMGReview alloc] init];
            [review setValuesForKeysWithDictionary:reviewdic];
            review.restaurantId = [responseObject objectForKey:@"restaurantId"];
            review.summarize = [review.summarize stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [reviews addObject:review];
            NSMutableArray *dishArray=[[NSMutableArray alloc] init];
            for(id dishdic in [reviewdic objectForKey:@"dishList"]){
                IMGDish *dish=[[IMGDish alloc] init];
                [dish setValuesForKeysWithDictionary:dishdic];
                dish.descriptionStr=[dishdic objectForKey:@"description"];
                [dish updatePhotoCredit:[dishdic objectForKey:@"photoCredit"]];
                dish.createTime = [dishdic objectForKey:@"dishCreateTime"];
                [dishArray addObject:dish];
            }
            [reviewDishDic setObject:dishArray forKey:[reviewdic objectForKey:@"id"]];
        }
        block(YES,[ratingCount unsignedIntegerValue],reviews,reviewDishDic);
    } failure:^(NSURLSessionDataTask *operation,NSError *error){
        block(NO,0,[NSArray arrayWithObject:error.description],nil);
    }];
}
+(void)getPromoInfo:(NSDictionary *)params andBlock:(void(^)(IMGRestaurantEvent *restaurantEvent))block{

    [[IMGNetWork sharedManager] GET:@"app/restauranteventphoto" parameters:params progress:nil success:^(NSURLSessionDataTask *operation,id responseObject){
        IMGRestaurantEvent *restaurantEvent=[[IMGRestaurantEvent alloc]init];
        [restaurantEvent setValuesForKeysWithDictionary:responseObject];
        restaurantEvent.descriptionStr = [responseObject objectForKey:@"description"];
        restaurantEvent.eventId = [responseObject objectForKey:@"id"];
        NSDictionary *photoCreditImpl = [responseObject objectForKey:@"photoCreditImpl"];
        restaurantEvent.restaurantId = [photoCreditImpl objectForKey:@"restaurantId"];
        restaurantEvent.restaurantTitle = [photoCreditImpl objectForKey:@"photoCredit"];
        block(restaurantEvent);
    } failure:^(NSURLSessionDataTask *operation,NSError *error){
//        block(nil);
    }];
}


+(void)getSectionsFromServer:(NSDictionary*)params andBlock:(void(^)(BOOL,NSDictionary *,NSDictionary *))block{
    [[IMGNetWork sharedManager] GET:@"app/restaurant/sections" parameters:params progress:nil success:^(NSURLSessionDataTask *operation,id responseObject){
        NSLog(@"sections %@",responseObject);
        NSMutableDictionary *sectionDict=[[NSMutableDictionary alloc] init];
        NSMutableDictionary *sectionIdDict=[[NSMutableDictionary alloc] init];
        for(NSDictionary *dic in responseObject){
            NSString *typeName=[dic objectForKey:@"typeName"];
            NSString *name=[dic objectForKey:@"name"];
            NSString *hasNewSign=[dic objectForKey:@"hasNewMenuSign"];
            NSMutableDictionary *section=[[NSMutableDictionary alloc] initWithCapacity:2];
            [section setObject:name forKey:@"name"];
            [section setObject:hasNewSign forKey:@"hasNewSign"];
            [sectionIdDict setObject:[dic objectForKey:@"id"] forKey:[NSString stringWithFormat:@"%@%@",typeName,name]];
            if(![[sectionDict allKeys] containsObject:typeName]){
                [sectionDict setObject:@[section] forKey:typeName];
            }else{
                NSMutableArray *sections=[[sectionDict objectForKey:typeName] mutableCopy];
                [sections addObject:section];
                [sectionDict setObject:sections forKey:typeName];
            }
        }
        block(YES,sectionDict,sectionIdDict);
    } failure:^(NSURLSessionDataTask *operation,NSError *error){
        block(NO,nil,nil);
    }];
}

+(void)getMenusFromServer:(NSDictionary*)params andBlock:(void(^)(BOOL,NSArray *))block{
    [[IMGNetWork sharedManager] GET:@"app/restaurant/section/menus" parameters:params progress:nil success:^(NSURLSessionDataTask *operation,id responseObject){
        NSLog(@"section menus %@",responseObject);
        NSMutableArray *menus=[[NSMutableArray alloc] init];
        for(NSDictionary *dic in responseObject){
            IMGMenu *menu=[[IMGMenu alloc] init];
            [menu setValuesForKeysWithDictionary:dic];
            [menus addObject:menu];
        }
        block(YES,menus);
    } failure:^(NSURLSessionDataTask *operation,NSError *error){
        block(NO,nil);
    }];
}
+(IMGRestaurant *)searchUpdate:(NSArray *)restaurantArray forRestaurant:(IMGRestaurant *)restaurant
{
    
    //[restaurantId,state,offerMap,ratingMap,eventLogo,offerIds,restaurant.title,cuisineName,districtName,restaurant.score,restaurant.latitude,restaurant.longitude,restaurant.phoneNumber,imageCount,dishList,faveCount,isFaved,savedCount,isSaved,menuCount,restaurant.bookStatus,restaurantImagePath];
    
    
    restaurant.restaurantId = [restaurantArray objectAtIndex:0];
    restaurant.state = [restaurantArray objectAtIndex:1];
    
    // offer
    NSString *offerIds=[restaurantArray objectAtIndex:5];
    
    if ([offerIds isKindOfClass:[NSNull class]])
    {
        offerIds = @"";
    }
    restaurant.offerIds=offerIds;
    
    if (![[restaurantArray objectAtIndex:2] isKindOfClass:[NSNull class]])
    {
        restaurant.bestOfferName = [[restaurantArray objectAtIndex:2] objectForKey:@"offerTitle"];
    }
    
    // review
    id reviewInfoDic = [restaurantArray objectAtIndex:3];
    if ([reviewInfoDic isKindOfClass:[NSDictionary class]])
    {
        restaurant.ambianceScore = [reviewInfoDic objectForKey:@"ambianceScore"];
        restaurant.foodScore = [reviewInfoDic objectForKey:@"foodScore"];
        restaurant.ratingScore = [reviewInfoDic objectForKey:@"rating"];
        restaurant.reviewCount = [reviewInfoDic objectForKey:@"reviewCount"];
        restaurant.ratingCount = [reviewInfoDic objectForKey:@"ratingCount"];
        NSString *reviewListStr = [[NSString alloc] init];
        for (NSString *str in [reviewInfoDic objectForKey:@"ratingList"])
        {
            if (!reviewListStr.length)
            {
                reviewListStr = [NSString stringWithFormat:@"%@",str];
            }
            else
                reviewListStr = [NSString stringWithFormat:@"%@,%@",reviewListStr,str];
        }
        restaurant.ratingCounts = reviewListStr;
    }
    
    
    
    restaurant.logoImage = [restaurantArray objectAtIndex:4];
    restaurant.needUpdate = @"";
    NSString *restaurantTitle = [NSString stringWithFormat:@"%@",[restaurantArray objectAtIndex:6]];
    NSRange range=[restaurantTitle rangeOfString:@"amp;"];
    if (range.location != NSNotFound)
    {
        restaurantTitle = [restaurantTitle stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    }
    
    restaurant.title = restaurantTitle;
    restaurant.cuisineName = [restaurantArray objectAtIndex:7];
    restaurant.districtName = [restaurantArray objectAtIndex:8];
    restaurant.priceLevel = [restaurantArray objectAtIndex:9];
    restaurant.latitude = [restaurantArray objectAtIndex:10];
    restaurant.longitude = [restaurantArray objectAtIndex:11];
    restaurant.phoneNumber = [restaurantArray objectAtIndex:12];
    restaurant.imageCount = [restaurantArray objectAtIndex:13];
    
    restaurant.imageUrl = [restaurantArray objectAtIndex:21];
    
    return restaurant;
}

+(IMGRestaurant *)update:(NSArray *)restaurantArray forRestaurant:(IMGRestaurant *)restaurant{
    
    
    restaurant.restaurantId = [restaurantArray objectAtIndex:0];
    restaurant.state = [restaurantArray objectAtIndex:1];
    
    // offer
    NSString *offerIds=[restaurantArray objectAtIndex:5];
    
    if ([offerIds isKindOfClass:[NSNull class]])
    {
        offerIds = @"";
    }
    restaurant.offerIds=offerIds;
    
    if (![[restaurantArray objectAtIndex:2] isKindOfClass:[NSNull class]])
    {
        restaurant.bestOfferName = [[restaurantArray objectAtIndex:2] objectForKey:@"offerTitle"];
    }
    
    // review
    id reviewInfoDic = [restaurantArray objectAtIndex:3];
    if ([reviewInfoDic isKindOfClass:[NSDictionary class]])
    {
        restaurant.ambianceScore = [reviewInfoDic objectForKey:@"ambianceScore"];
        restaurant.foodScore = [reviewInfoDic objectForKey:@"foodScore"];
        restaurant.ratingScore = [reviewInfoDic objectForKey:@"rating"];
        restaurant.reviewCount = [reviewInfoDic objectForKey:@"reviewCount"];
        restaurant.ratingCount = [reviewInfoDic objectForKey:@"ratingCount"];
        NSString *reviewListStr = [[NSString alloc] init];
        for (NSString *str in [reviewInfoDic objectForKey:@"ratingList"])
        {
            if (!reviewListStr.length)
            {
                reviewListStr = [NSString stringWithFormat:@"%@",str];
            }
            else
                reviewListStr = [NSString stringWithFormat:@"%@,%@",reviewListStr,str];
        }
        restaurant.ratingCounts = reviewListStr;
    }
    
    
    
    restaurant.logoImage = [restaurantArray objectAtIndex:4];
    restaurant.needUpdate = @"";
    NSString *restaurantTitle = [NSString stringWithFormat:@"%@",[restaurantArray objectAtIndex:6]];
    NSRange range=[restaurantTitle rangeOfString:@"amp;"];
    if (range.location != NSNotFound)
    {
        restaurantTitle = [restaurantTitle stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    }
    
    restaurant.title = restaurantTitle;
    restaurant.cuisineName = [restaurantArray objectAtIndex:7];
    restaurant.districtName = [restaurantArray objectAtIndex:8];
    restaurant.priceLevel = [restaurantArray objectAtIndex:9];
    restaurant.latitude = [restaurantArray objectAtIndex:10];
    restaurant.longitude = [restaurantArray objectAtIndex:11];
    restaurant.imageUrl = [restaurantArray objectAtIndex:12];
    
    return restaurant;
}
+(NSArray *)updateForDetailViewControllerNow:(NSDictionary *)restaurantDic forRestaurant:(IMGRestaurant *)restaurant{

    restaurant.restaurantId = [restaurantDic objectForKey:@"restaurantId"];
    restaurant.state = [restaurantDic objectForKey:@"state"];
    
    // offer
    NSString *offerIds=[restaurantDic objectForKey:@"restaurantDic"];
    
    if ([offerIds isKindOfClass:[NSNull class]])
    {
        offerIds = @"";
    }
    restaurant.offerIds=offerIds;
    
    // review
    id reviewInfoDic = [restaurantDic objectForKey:@"ratingMap"];
    if ([reviewInfoDic isKindOfClass:[NSDictionary class]])
    {
        restaurant.ambianceScore = [reviewInfoDic objectForKey:@"ambianceScore"];
        restaurant.foodScore = [reviewInfoDic objectForKey:@"foodScore"];
        restaurant.ratingScore = [reviewInfoDic objectForKey:@"rating"];
        restaurant.reviewCount = [reviewInfoDic objectForKey:@"reviewCount"];
        restaurant.ratingCount = [reviewInfoDic objectForKey:@"ratingCount"];
        NSString *reviewListStr = [[NSString alloc] init];
        for (NSString *str in [reviewInfoDic objectForKey:@"ratingList"])
        {
            if (!reviewListStr.length)
            {
                reviewListStr = [NSString stringWithFormat:@"%@",str];
            }
            else
                reviewListStr = [NSString stringWithFormat:@"%@,%@",reviewListStr,str];
        }
        restaurant.ratingCounts = reviewListStr;
    }
    
    
    restaurant.brandLogo = [restaurantDic objectForKey:@"brandLogo"];
    restaurant.logoImage = [restaurantDic objectForKey:@"eventLogo"];
    restaurant.goFoodLink = [restaurantDic objectForKey:@"goFooLink"];
    restaurant.needUpdate = @"";
    NSString *restaurantTitle = [NSString stringWithString:[restaurantDic objectForKey:@"restaurantTitle"]];
    NSRange range=[restaurantTitle rangeOfString:@"amp;"];
    if (range.location != NSNotFound)
    {
        restaurantTitle = [restaurantTitle stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
    }
    
    restaurant.title = restaurantTitle;
    restaurant.cuisineName = [restaurantDic objectForKey:@"cuisineName"];
    restaurant.districtName = [restaurantDic objectForKey:@"districtName"];
    restaurant.priceLevel = [restaurantDic objectForKey:@"rating"];
    restaurant.latitude = [restaurantDic objectForKey:@"latitude"];
    restaurant.longitude = [restaurantDic objectForKey:@"longitude"];
    restaurant.imageUrl = [restaurantDic objectForKey:@"restaurantImagePath"];
    
    restaurant.phoneNumber = [restaurantDic objectForKey:@"phoneNumber"];
    restaurant.offerCount = [restaurantDic objectForKey:@"offerCount"];
    restaurant.liked = [restaurantDic objectForKey:@"liked"];
    restaurant.saved = [restaurantDic objectForKey:@"saved"];
    restaurant.address1 = [restaurantDic objectForKey:@"address1"];
    restaurant.address2 = [restaurantDic objectForKey:@"address2"];
    restaurant.todayTimeOpen = [restaurantDic objectForKey:@"todayOpen"];
    restaurant.todayTimeClosed = [restaurantDic objectForKey:@"todayClosed"];
    restaurant.cityId = [restaurantDic objectForKey:@"cityId"];
    restaurant.cityName=[restaurantDic objectForKey:@"cityName"];
    restaurant.bookStatus = [restaurantDic objectForKey:@"bookStatus"];
    restaurant.seoKeyword = [restaurantDic objectForKey:@"seoKeyword"];
    restaurant.dishCount = [restaurantDic objectForKey:@"imageCount"];
    restaurant.userRating = [restaurantDic objectForKey:@"lastRatingScore"];
    restaurant.td = [restaurantDic objectForKey:@"isTodayOpen"];
    restaurant.discountContent = [restaurantDic objectForKey:@"discountContent"];
    restaurant.restaurantEventCount =[[restaurantDic objectForKey:@"restaurantEvent"] objectForKey:@"restaurantEventCount"];
    NSDictionary* yesterOpenDic=[restaurantDic objectForKey:@"yesterDayOpenTime"];
    restaurant.yesteropenDay=[yesterOpenDic objectForKey:@"openDay"];
    restaurant.yesteropenTime=[yesterOpenDic objectForKey:@"openTime"];
    restaurant.yesterclosedTime=[yesterOpenDic objectForKey:@"closedTime"];
    
    
    NSDictionary* nextOpenDay=[restaurantDic objectForKey:@"nextOpenDay"];
    restaurant.nextopenTime=[nextOpenDay objectForKey:@"openTime"];
    restaurant.nextclosedTime=[nextOpenDay objectForKey:@"closedTime"];
    restaurant.nextopenDay=[nextOpenDay objectForKey:@"openDay"];
    
    NSDictionary* secondOpenDic=[restaurantDic objectForKey:@"secondOpenDay"];
    restaurant.secondclosedTime=[secondOpenDic objectForKey:@"closedTime"];
    restaurant.secondopenTime=[secondOpenDic objectForKey:@"openTime"];
    restaurant.secondopenDay=[secondOpenDic objectForKey:@"openDay"];
    NSArray *tempEventList = [[NSArray alloc]initWithArray:[[restaurantDic objectForKey:@"restaurantEvent"] objectForKey:@"restaurantEventList"]];
    restaurant.eventCount = [[restaurantDic objectForKey:@"restaurantEvent"] objectForKey:@"restaurantEventCount"];
    restaurant.instagramPhotoCount = [restaurantDic objectForKey:@"instagramPhotoCount"];
    restaurant.instagramLocationId = [restaurantDic objectForKey:@"instagramLocationId"];
    restaurant.landMarkList = [restaurantDic objectForKey:@"landMarkList"];
    
    restaurant.wellKnownFor = [restaurantDic objectForKey:@"wellKnownFor"];
    restaurant.couponBannerArr = [restaurantDic objectForKey:@"partnerBannerList"];
   
    return @[restaurant,tempEventList];
}
//+(NSArray *)updateForDetailViewController:(NSArray *)restaurantArray forRestaurant:(IMGRestaurant *)restaurant{
//    
//    
//    
//    //[restaurantId,state,offerMap,ratingMap,eventLogo,offerIds,restaurant.title,cuisineName,districtName,restaurant.score,restaurant.latitude,restaurant.longitude,restaurantImagePath,imageList,restaurant.phoneNumber,liked,saved,restaurant.address1,restaurant.address2,todayOpen,todayClosed,cityId,bookStatus,seoKeyword]
//    
//    
//    restaurant.restaurantId = [restaurantArray firstObject];
//    restaurant.state = [restaurantArray objectAtIndex:1];
//    
//    // offer
//    NSString *offerIds=[restaurantArray objectAtIndex:5];
//    
//    if ([offerIds isKindOfClass:[NSNull class]])
//    {
//        offerIds = @"";
//    }
//    restaurant.offerIds=offerIds;
//    
//    // review
//    id reviewInfoDic = [restaurantArray objectAtIndex:3];
//    if ([reviewInfoDic isKindOfClass:[NSDictionary class]])
//    {
//        restaurant.ambianceScore = [reviewInfoDic objectForKey:@"ambianceScore"];
//        restaurant.foodScore = [reviewInfoDic objectForKey:@"foodScore"];
//        restaurant.ratingScore = [reviewInfoDic objectForKey:@"rating"];
//        restaurant.reviewCount = [reviewInfoDic objectForKey:@"reviewCount"];
//        restaurant.ratingCount = [reviewInfoDic objectForKey:@"ratingCount"];
//        NSString *reviewListStr = [[NSString alloc] init];
//        for (NSString *str in [reviewInfoDic objectForKey:@"ratingList"])
//        {
//            if (!reviewListStr.length)
//            {
//                reviewListStr = [NSString stringWithFormat:@"%@",str];
//            }
//            else
//                reviewListStr = [NSString stringWithFormat:@"%@,%@",reviewListStr,str];
//        }
//        restaurant.ratingCounts = reviewListStr;
//    }
//    
//    
//    
//    restaurant.logoImage = [restaurantArray objectAtIndex:4];
//    restaurant.needUpdate = @"";
//    NSString *restaurantTitle = [NSString stringWithString:[restaurantArray objectAtIndex:6]];
//    NSRange range=[restaurantTitle rangeOfString:@"amp;"];
//    if (range.location != NSNotFound)
//    {
//        restaurantTitle = [restaurantTitle stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
//    }
//    
//    restaurant.title = restaurantTitle;
//    restaurant.cuisineName = [restaurantArray objectAtIndex:7];
//    restaurant.districtName = [restaurantArray objectAtIndex:8];
//    restaurant.priceLevel = [restaurantArray objectAtIndex:9];
//    restaurant.latitude = [restaurantArray objectAtIndex:10];
//    restaurant.longitude = [restaurantArray objectAtIndex:11];
//    restaurant.imageUrl = [restaurantArray objectAtIndex:12];
//    
//    restaurant.phoneNumber = [restaurantArray objectAtIndex:14];
//    restaurant.offerCount = [restaurantArray objectAtIndex:15];
//    restaurant.liked = [restaurantArray objectAtIndex:16];
//    restaurant.saved = [restaurantArray objectAtIndex:17];
//    restaurant.address1 = [restaurantArray objectAtIndex:18];
//    restaurant.address2 = [restaurantArray objectAtIndex:19];
//    restaurant.todayTimeOpen = [restaurantArray objectAtIndex:20];
//    restaurant.todayTimeClosed = [restaurantArray objectAtIndex:21];
//    restaurant.cityId = [restaurantArray objectAtIndex:22];
//    restaurant.bookStatus = [restaurantArray objectAtIndex:23];
//    restaurant.seoKeyword = [restaurantArray objectAtIndex:24];
//    restaurant.dishCount = [restaurantArray objectAtIndex:25];
//    restaurant.userRating = [restaurantArray objectAtIndex:26];
//    restaurant.td = [restaurantArray objectAtIndex:27];
//    restaurant.discountContent = [restaurantArray objectAtIndex:28];
//    restaurant.restaurantEventCount =[[restaurantArray objectAtIndex:31] objectForKey:@"restaurantEventCount"];
//    NSArray *tempEventList = [[NSArray alloc]initWithArray:[[restaurantArray objectAtIndex:31] objectForKey:@"restaurantEventList"]];
//    restaurant.eventCount = [[restaurantArray objectAtIndex:31] objectForKey:@"restaurantEventCount"];
//    return @[restaurant,tempEventList];
//}


+ (IMGRestaurant *)updateRestaurant:(NSArray *)restaurantArray
{
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    restaurant.restaurantId = [restaurantArray objectAtIndex:0];
    restaurant.state = [restaurantArray objectAtIndex:1];
    restaurant.bestOfferName=[restaurantArray objectAtIndex:3];
    restaurant.title = [restaurantArray objectAtIndex:4];
    restaurant.imageUrl = [restaurantArray objectAtIndex:5];
    restaurant.cuisineName = [restaurantArray objectAtIndex:6];
    restaurant.districtName = [restaurantArray objectAtIndex:7];
    restaurant.priceLevel = [restaurantArray objectAtIndex:8];
    restaurant.reviewCount = [restaurantArray objectAtIndex:9];
    restaurant.latitude = [restaurantArray objectAtIndex:10];
    restaurant.longitude = [restaurantArray objectAtIndex:11];
    restaurant.logoImage = [restaurantArray objectAtIndex:12];
    restaurant.ratingScore = [restaurantArray objectAtIndex:13];
    restaurant.needUpdate = @"needUpdate";
    if(!restaurant.restaurantId || [restaurant.restaurantId intValue]==0){
        [[DBManager manager]insertModel:restaurant];
        restaurant = [IMGRestaurant findById:restaurant.restaurantId];
    }else{
         [[DBManager manager] insertModel:restaurant update:YES selectKey:@"restaurantId" andSelectValue:restaurant.restaurantId];
    }
    return restaurant;
}

+ (IMGRestaurant *)parseJson2EntityByArray:(NSArray *)restaurantArray  andRestaurant:(IMGRestaurant *)restaurant andBlock:(void (^)(IMGRestaurant * restaurant))block
{
    
    if (restaurantArray.count < 6)
    {
        FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueue];
        [queue inDatabase:^(FMDatabase *db)
         {
             restaurant.state = [restaurantArray objectAtIndex:1];
             [[DBManager manager]insertModel:restaurant selectField:@"restaurantState" andSelectID:restaurant.state andFMDatabase:db];
             
             
         }];
        
        return restaurant;
    }
    else
    {
        FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueue];
        [queue inDatabase:^(FMDatabase *db)
         {
             IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
             
             restaurant.restaurantId = [restaurantArray objectAtIndex:0];
             [[DBManager manager]insertModel:restaurant selectField:@"restaurantId" andSelectID:restaurant.restaurantId andFMDatabase:db];
             
             //        if (![[restaurantArray objectAtIndex:2]isEqualToString:@"0"])
             //        {
             //            restaurant.bestOfferName = [restaurantArray objectAtIndex:3];
             //            [[DBManager manager]insertModel:restaurant selectField:@"offerName" andSelectID:[NSNumber numberWithInt:[restaurant.bestOfferName intValue]] andFMDatabase:db];
             //        }
             
             restaurant.state = [restaurantArray objectAtIndex:1];
             [[DBManager manager]insertModel:restaurant selectField:@"restaurantState" andSelectID:restaurant.state andFMDatabase:db];
             
             
         }];
        return restaurant;
    }
    
}
+(IMGRestaurant *)parseJson2Entity:(NSDictionary *)restaurantDictionary  andBlock:(void (^)(IMGRestaurant * restaurant))block
{
    IMGRestaurant *restaurant = [[IMGRestaurant alloc]init];
    [restaurant setValuesForKeysWithDictionary:restaurantDictionary];
    
    if([restaurant.latitude doubleValue]>90.0 || [restaurant.latitude doubleValue]<-90.0){
        restaurant.latitude=[NSNumber numberWithDouble:0.0];
    }
    if([restaurant.longitude doubleValue]>180.0 || [restaurant.longitude doubleValue]<-180.0){
        restaurant.longitude=[NSNumber numberWithDouble:0.0];
    }
    
    //handle restaurant tag
    NSArray *tagsArray = [restaurantDictionary objectForKey:@"tagList"];
    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueue];
    [queue inDatabase:^(FMDatabase *db) {
        [db executeUpdate:[NSString stringWithFormat:@"delete from IMGRestaurantDiningGuide where restaurantId=%@;",restaurant.restaurantId]];
        [db executeUpdate:[NSString stringWithFormat:@"delete from IMGRestaurantImage where restaurantId=%@;",restaurant.restaurantId]];
        [db executeUpdate:[NSString stringWithFormat:@"delete from IMGRestaurantTag where restaurantId=%@;",restaurant.restaurantId]];
        [db executeUpdate:[NSString stringWithFormat:@"delete from IMGRestaurantCuisine where restaurantId=%@;",restaurant.restaurantId]];
        [db executeUpdate:[NSString stringWithFormat:@"delete from IMGRestaurantDistrict where restaurantId=%@;",restaurant.restaurantId]];
        [db executeUpdate:[NSString stringWithFormat:@"delete from IMGRestaurantEvent where restaurantId=%@;",restaurant.restaurantId]];
        [db executeUpdate:[NSString stringWithFormat:@"delete from IMGRestaurantOffer where restaurantId=%@;",restaurant.restaurantId]];
        [db executeUpdate:[NSString stringWithFormat:@"delete from IMGSlot where restaurantId=%@;",restaurant.restaurantId]];
        [db executeUpdate:[NSString stringWithFormat:@"delete from IMGRestaurantLink where restaurantId=%@;",restaurant.restaurantId]];
        [db executeUpdate:[NSString stringWithFormat:@"delete from IMGMediaComment where restaurantId=%@;",restaurant.restaurantId]];
        
        if([restaurant.latitude doubleValue]>90.0 || [restaurant.latitude doubleValue]<-90.0){
            restaurant.latitude=[NSNumber numberWithDouble:0.0];
        }
        if([restaurant.longitude doubleValue]>180.0 || [restaurant.longitude doubleValue]<-180.0){
            restaurant.longitude=[NSNumber numberWithDouble:0.0];
        }
        [[DBManager manager]insertModel:restaurant selectField:@"restaurantId" andSelectID:restaurant.restaurantId andFMDatabase:db];
        
        
        for (int i=0; i<tagsArray.count; i++) {
            NSNumber *tagId = [tagsArray objectAtIndex:i];
            IMGRestaurantTag *restaurantTag = [[IMGRestaurantTag alloc]init];
            restaurantTag.tagId=tagId;
            restaurantTag.restaurantId = restaurant.restaurantId;
            
            [[DBManager manager]insertRelation:restaurantTag selectField:@"tagId" andSelectID:restaurantTag.tagId relationField:@"restaurantId" andRelationId:restaurantTag.restaurantId andFMDatabase:db];
        }
        
        //handle restaurant cuisine
        NSArray *cuisinesArray = [restaurantDictionary objectForKey:@"cuisineList"];
        for (int i=0; i<cuisinesArray.count; i++) {
            NSNumber *cuisineId = [cuisinesArray objectAtIndex:i];
            IMGRestaurantCuisine *restaurantCuisine = [[IMGRestaurantCuisine alloc]init];
            restaurantCuisine.cuisineId=cuisineId;
            restaurantCuisine.restaurantId = restaurant.restaurantId;
            [[DBManager manager]insertRelation:restaurantCuisine selectField:@"cuisineId" andSelectID:restaurantCuisine.cuisineId relationField:@"restaurantId" andRelationId:restaurantCuisine.restaurantId andFMDatabase:db];
        }
        
        //handle restaurant event
        NSArray *eventArray = [restaurantDictionary objectForKey:@"eventList"];
        for (int i=0; i<eventArray.count; i++) {
            NSNumber *eventId = [eventArray objectAtIndex:i];
            IMGRestaurantEvent *restaurantEvent = [[IMGRestaurantEvent alloc]init];
            restaurantEvent.eventId=eventId;
            restaurantEvent.restaurantId = restaurant.restaurantId;
            [[DBManager manager]insertRelation:restaurantEvent selectField:@"eventId" andSelectID:restaurantEvent.eventId relationField:@"restaurantId" andRelationId:restaurantEvent.restaurantId andFMDatabase:db];
        }
        
        //handle restaurant dining guide
        NSArray *diningGuidesArray = [restaurantDictionary objectForKey:@"diningGuideList"];
        for (int i=0; i<diningGuidesArray.count; i++) {
            NSNumber *diningGuideId = [diningGuidesArray objectAtIndex:i];
            IMGRestaurantDiningGuide *restaurantDiningGuide = [[IMGRestaurantDiningGuide alloc]init];
            restaurantDiningGuide.diningGuideId = diningGuideId;
            restaurantDiningGuide.restaurantId = restaurant.restaurantId;
            [[DBManager manager]insertRelation:restaurantDiningGuide selectField:@"diningGuideId" andSelectID:restaurantDiningGuide.diningGuideId relationField:@"restaurantId" andRelationId:restaurantDiningGuide.restaurantId andFMDatabase:db];
        }
        
        //handle restaurant media comment
        NSArray *mediaCommentArray = [restaurantDictionary objectForKey:@"mediaCommentList"];
        for (int i=0; i<mediaCommentArray.count; i++) {
            NSDictionary *dic = [mediaCommentArray objectAtIndex:i];
            IMGMediaComment *mediaComment = [[IMGMediaComment alloc]init];
            [mediaComment setValuesForKeysWithDictionary:dic];
            mediaComment.restaurantId = restaurant.restaurantId;
            
            [[DBManager manager]insertModel:mediaComment selectField:@"mediaCommentId" andSelectID:mediaComment.mediaCommentId andFMDatabase:db];
        }
        
        //handle slot time
        NSArray *slotTimeArray=[restaurantDictionary objectForKey:@"slotTimeList"];
        for(int i=0;slotTimeArray!=nil&&i<slotTimeArray.count;i++){
            NSArray *weekdaySlotTimeArray=[slotTimeArray objectAtIndex:i];
            if(weekdaySlotTimeArray!=nil){
                for(NSDictionary *slotDictionary in weekdaySlotTimeArray){
                    IMGSlot *slot=[[IMGSlot alloc]init];
                    slot.avail=[slotDictionary objectForKey:@"avail"];
                    slot.restaurantId=restaurant.restaurantId;
                    slot.weekDay=[slotDictionary objectForKey:@"weekDay"];
                    slot.bookTime=[BookUtil timeIntToString:[[slotDictionary objectForKey:@"bookTime"] intValue]];
                    [[DBManager manager]insertModel:slot andFMDatabase:db];
                }
            }
        }
        
        //handle restaurant image
        NSArray *banerImageList = [restaurantDictionary objectForKey:@"banerImageList"];
        for (int i=0; i<banerImageList.count; i++) {
            NSArray *banerImageUrlArray = [banerImageList objectAtIndex:i];
            IMGRestaurantImage *restaurantImage = [[IMGRestaurantImage alloc]init];
            restaurantImage.imageId = [banerImageUrlArray objectAtIndex:0];
            restaurantImage.imageUrl = [banerImageUrlArray objectAtIndex:1];
            restaurantImage.restaurantId = restaurant.restaurantId;
            
            [[DBManager manager]insertModel:restaurantImage selectField:@"imageId" andSelectID:restaurantImage.imageId andFMDatabase:db];
        }
        
        //handle restaurant review
        NSArray *reviewArray = [restaurantDictionary objectForKey:@"reviewList"];
        for (int i=0; i<reviewArray.count; i++) {
            NSDictionary *dic = [reviewArray objectAtIndex:i];
            IMGReview *review = [[IMGReview alloc]init];
            [review setValuesForKeysWithDictionary:dic];
            review.restaurantId = restaurant.restaurantId;
            if (review.summarize.length >0)
            {
                [[DBManager manager]insertModel:review selectField:@"reviewId" andSelectID:review.reviewId andFMDatabase:db];
            }
        }
        
        //handle restaurant menu
        NSArray *menuArray = [restaurantDictionary objectForKey:@"menuList"];
        for (int i=0; i<menuArray.count; i++) {
            NSDictionary *dic = [menuArray objectAtIndex:i];
            IMGMenu *menu = [[IMGMenu alloc]init];
            [menu setValuesForKeysWithDictionary:dic];
            [[DBManager manager]insertModel:menu selectField:@"menuId" andSelectID:menu.menuId andFMDatabase:db];
        }
        
        //handle restaurant link
        NSArray *linkArray = [restaurantDictionary objectForKey:@"urlList"];
        for (int i=0; i<linkArray.count; i++) {
            NSArray *dic = [linkArray objectAtIndex:i];
            IMGRestaurantLink *restaurantLink = [[IMGRestaurantLink alloc]init];
            restaurantLink.url=[dic objectAtIndex:1];
            restaurantLink.restaurantId=restaurant.restaurantId;
            [[DBManager manager]insertModel:restaurantLink andFMDatabase:db];
        }
        
        block(nil);
    }];
    
    //handle restaurant offer
    NSArray *offerArray = [restaurantDictionary objectForKey:@"restaurantOfferDetailList"];
    for (int i=0; i<offerArray.count; i++) {
        NSDictionary *dic = [offerArray objectAtIndex:i];
        [RestaurantHandler addOffer:dic andRestaurantId:restaurant.restaurantId date:nil];
    }
    
    return restaurant;
}


+(void)addOffer:(NSDictionary *)dic andRestaurantId:(NSNumber *)restaurantId date:(NSDate *)date{
    // NSLog(@"%f,addOffer0.1",[[NSDate date]timeIntervalSince1970]);
    IMGRestaurantOffer *restaurantOffer = [IMGRestaurantOffer findById:[NSNumber numberWithInt:[[dic objectForKey:@"id"] intValue]]];
    if(restaurantOffer==nil){
        restaurantOffer = [[IMGRestaurantOffer alloc]init];
        [restaurantOffer setValuesForKeysWithDictionary:dic];
        restaurantOffer.restaurantId = restaurantId;
        
        [[DBManager manager]insertModel:restaurantOffer update:YES selectKey:@"offerId" andSelectValue:restaurantOffer.offerId];
    }
    
    
    // NSLog(@"%f,addOffer0.3",[[NSDate date]timeIntervalSince1970]);
}


+(void)addOffer:(NSDictionary *)dic andRestaurantId:(NSNumber *)restaurantId {
    // NSLog(@"%f,addOffer0.1",[[NSDate date]timeIntervalSince1970]);
    IMGRestaurantOffer *restaurantOffer = [IMGRestaurantOffer findById:[NSNumber numberWithInt:[[dic objectForKey:@"id"] intValue]]];
    if(restaurantOffer==nil){
        restaurantOffer = [[IMGRestaurantOffer alloc]init];
        [restaurantOffer setValuesForKeysWithDictionary:dic];
        restaurantOffer.restaurantId = restaurantId;
        NSDictionary *pictureInfo=[dic objectForKey:@"pictureInfo"];
        double width = [[pictureInfo objectForKey:@"width"] doubleValue];
        double height = [[pictureInfo objectForKey:@"height"] doubleValue];
//        restaurantOffer.offerImageH = [NSNumber numberWithInt:((DeviceWidth - LEFTLEFTSET*2)*height)/width];
        
        
        if (width!=0||height!=0) {
            restaurantOffer.offerImageH=[NSNumber numberWithDouble:(DeviceWidth*height)/width];

        }
        if ([restaurantOffer.offerImageH floatValue] < 0)
        {
            restaurantOffer.offerImageH = 0;
        }

        if (!restaurantOffer.offerSlotId.intValue)
        {
            restaurantOffer.offerSlotId = [NSNumber numberWithInt:0];
        }
        [[DBManager manager]insertModel:restaurantOffer update:YES selectKey:@"offerId" andSelectValue:restaurantOffer.offerId];
    }
    
    
    // NSLog(@"%f,addOffer0.3",[[NSDate date]timeIntervalSince1970]);
}


+(IMGRestaurant *)parseJson2Entity_v1:(NSDictionary *)restaurantDictionary  andBlock:(void (^)(IMGRestaurant * restaurant))block
{
    // parseJson2Entity_v1 跟 parseJson2Entity 相比，parseJson2Entity_v1 只更新了少量信息
    
    
    IMGRestaurant *restaurant = [[IMGRestaurant alloc]init];
    [restaurant setValuesForKeysWithDictionary:restaurantDictionary];
    
    if([restaurant.latitude doubleValue]>90.0 || [restaurant.latitude doubleValue]<-90.0){
        restaurant.latitude=[NSNumber numberWithDouble:0.0];
    }
    if([restaurant.longitude doubleValue]>180.0 || [restaurant.longitude doubleValue]<-180.0){
        restaurant.longitude=[NSNumber numberWithDouble:0.0];
    }
    
    [[DBManager manager]insertModel:restaurant selectField:@"restaurantId" andSelectID:restaurant.restaurantId];
    
    //handle restaurant offer
    NSArray *offerArray = [restaurantDictionary objectForKey:@"restaurantOfferDetailList"];
    [[DBManager manager] deleteWithSql:[NSString stringWithFormat:@"delete from IMGRestaurantOffer where restaurantId=%@ ;",restaurant.restaurantId]];
    
    for (int i=0; i<offerArray.count; i++) {
        NSDictionary *dic = [offerArray objectAtIndex:i];
        [RestaurantHandler addOffer:dic andRestaurantId:restaurant.restaurantId date:nil];
    }

    block(nil);

    
//    FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueue];
//    [queue inDatabase:^(FMDatabase *db) {
//        
//        
//        
//        [[DBManager manager]insertModel:restaurant selectField:@"restaurantId" andSelectID:restaurant.restaurantId andFMDatabase:db];
//        
//        
//    }];
    
    
    
    
    return restaurant;
}


+(void)getDishesFromServer:(int)offset max:(int)max{
    if(offset==0){
        [[DBManager manager] deleteWithSql:@"delete from IMGDish"];
    }
    NSDictionary *parameters=@{@"offset":[NSNumber numberWithInt:offset],@"max":[NSNumber numberWithInt:max]};
    [[IMGNetWork sharedManager] GET:@"dishes" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        NSString *exceptionMessage = [responseObject objectForKey:@"exceptionmsg"];
        if(exceptionMessage!=nil){
            NSLog(@"getDishesFromServer erors when get datas, exceptionmsg is:%@",exceptionMessage);
            return;
        }
        NSNumber *dishCount=[responseObject objectForKey:@"dishCount"];
        NSArray *dishArray = [responseObject objectForKey:@"dishList"];
        
        for(int i=0; i<dishArray.count; i++){
            NSDictionary *dishDictionary = [dishArray objectAtIndex:i];
            IMGDish *dish = [[IMGDish alloc]init];
            [dish setValuesForKeysWithDictionary:dishDictionary];
            [[DBManager manager]insertModel:dish selectField:@"dishId" andSelectID:dish.dishId];
        }
        NSLog(@"getDishesFromServer, now offset is:%d",offset);
        if((offset+max) <[dishCount intValue]){
            int offset1 = offset+max;
            [self getDishesFromServer:offset1 max:max];
        }
        
    }failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"getDishesFromServer erors when get datas:%@",error.localizedDescription);
    }];
}

+(void)saveRestaurantLike:(IMGRestaurantLike *)restaurantLike liked:(BOOL)liked{
    if(liked){
        [[DBManager manager]insertRelation:restaurantLike selectField:@"userId" andSelectID:restaurantLike.userId relationField:@"restaurantId" andRelationId:restaurantLike.restaurantId];
    }else{
        NSString *deleteSql = [NSString stringWithFormat:@"delete from IMGRestaurantLike where restaurantId='%@' and userId='%@';",restaurantLike.restaurantId,restaurantLike.userId ];
        [[DBManager manager] deleteWithSql:deleteSql];
    }
}

+(NSArray *)getRestaurantLinkArray:(NSNumber *)restaurantId{
    NSMutableArray *restaurantLinkArray=[[NSMutableArray alloc]initWithCapacity:0];
    FMResultSet *resultSet = [[DBManager manager]executeQuery:[NSString stringWithFormat:@"select * from IMGRestaurantLink where restaurantId=%@",restaurantId]];
    while([resultSet next]){
        IMGRestaurantLink *restaurantLink=[[IMGRestaurantLink alloc]init];
        [restaurantLink setValueWithResultSet:resultSet];
        [restaurantLinkArray addObject:restaurantLink];
    }
    [resultSet close];
    return restaurantLinkArray;
}

+(void)previousComment:(NSString *)commentIdStr targetId:(NSNumber*)articleId andBlock:(void (^)(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount))block{
    
    NSDictionary *parameters;
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userId",articleId,@"restaurantEventId",commentIdStr,@"lastRestaurantEventCommentId", nil];
    }
    else
    {
        parameters=[[NSDictionary alloc]initWithObjectsAndKeys:articleId,@"restaurantEventId",commentIdStr,@"lastRestaurantEventCommentId", nil];
    }
    [[IMGNetWork sharedManager]GET:@"restaurantevent/comments/previous" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
         if(responseObject && errorMessage){
             NSLog(@"previous comment: error: %@",errorMessage);
         }else if(responseObject){
             NSMutableArray *commentList = [[NSMutableArray alloc]initWithCapacity:0];
             IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
             NSDictionary *restaurantDic = [responseObject objectForKey:@"restaurant"];
             restaurant.title = [restaurantDic objectForKey:@"title"];
             restaurant.imageUrl = [restaurantDic objectForKey:@"imageUrl"];
             restaurant.restaurantId = [restaurantDic objectForKey:@"id"];
             NSArray *commentArray = [responseObject objectForKey:@"commentList"];
             commentList=[IMGRestaurantEventComment commentListFromArray:commentArray andIsVendor:NO andRestaurant:restaurant];
             NSNumber *commentCount = [responseObject objectForKey:@"commentCount"];
             NSNumber *likeCount = [responseObject objectForKey:@"likeCount"];
             block(commentList,likeCount,commentCount);
             
         }
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         NSLog(@"getRestaurantFirstDataFromService error: %@",error.localizedDescription);
     }];
    
}
+(void)previousComment:(NSString *)commentIdStr offerComentId:(NSNumber*)offerComentId andBlock:(void (^)(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount))block{
    
    NSDictionary *parameters;
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userId",offerComentId,@"restaurantOfferId",commentIdStr,@"lastRestaurantOfferCommentId", nil];
    }
    else
    {
        parameters=[[NSDictionary alloc]initWithObjectsAndKeys:offerComentId,@"restaurantOfferId",commentIdStr,@"lastRestaurantOfferCommentId", nil];
    }
    [[IMGNetWork sharedManager]GET:@"restaurantoffer/comments/previous" parameters:parameters success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
         if(responseObject && errorMessage){
             NSLog(@"previous comment: error: %@",errorMessage);
         }else if(responseObject){
             NSMutableArray *commentList = [[NSMutableArray alloc]initWithCapacity:0];
             IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
             NSDictionary *restaurantDic = [responseObject objectForKey:@"restaurant"];
             restaurant.title = [restaurantDic objectForKey:@"title"];
             restaurant.imageUrl = [restaurantDic objectForKey:@"imageUrl"];
             restaurant.restaurantId = [restaurantDic objectForKey:@"id"];
             NSArray *commentArray = [responseObject objectForKey:@"commentList"];
             commentList=[IMGRestaurantEventComment commentListFromArray:commentArray andIsVendor:NO andRestaurant:restaurant];
             NSNumber *commentCount = [responseObject objectForKey:@"commentCount"];
             NSNumber *likeCount = [responseObject objectForKey:@"likeCount"];
             block(commentList,likeCount,commentCount);
             
         }
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         NSLog(@"getRestaurantFirstDataFromService error: %@",error.localizedDescription);
     }];
    
}


+(void)previousCardComment:(NSString *)commentIdStr homeTimelineRestaurantUpdateId:(NSNumber*)homeTimelineRestaurantUpdateId andBlock:(void (^)(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount))block{
    NSDictionary *parameters;
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userId",homeTimelineRestaurantUpdateId,@"homeTimelineRestaurantUpdateId",commentIdStr,@"lastRestaurantEventHomeCommentId", nil];
    }
    else
    {
        parameters=[[NSDictionary alloc]initWithObjectsAndKeys:homeTimelineRestaurantUpdateId,@"homeTimelineRestaurantUpdateId",commentIdStr,@"lastRestaurantEventHomeCommentId", nil];
    }
    [[IMGNetWork sharedManager]POST:@"restaurantupdate/comment/previous" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
         if(responseObject && errorMessage){
             NSLog(@"previous comment: error: %@",errorMessage);
         }else if(responseObject){
             NSMutableArray *commentList = [[NSMutableArray alloc]initWithCapacity:0];
             IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
             NSDictionary *restaurantDic = [responseObject objectForKey:@"restaurant"];
             restaurant.title = [restaurantDic objectForKey:@"title"];
             restaurant.imageUrl = [restaurantDic objectForKey:@"imageUrl"];
             restaurant.restaurantId = [restaurantDic objectForKey:@"id"];
             NSArray *commentArray = [responseObject objectForKey:@"commentList"];
             commentList=[IMGRestaurantEventComment commentListFromArray:commentArray andIsVendor:NO andRestaurant:restaurant];
             NSNumber *commentCount = [responseObject objectForKey:@"commentCount"];
             NSNumber *likeCount = [responseObject objectForKey:@"likeCount"];
             block(commentList,likeCount,commentCount);
             
         }
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         NSLog(@"getRestaurantFirstDataFromService error: %@",error.localizedDescription);
     }];

}


+(void)likeRestaurantEvent:(BOOL)liked restaurantEventId:(NSNumber*)restaurantEventId andBlock:(void (^)())block  failure:(void(^)(NSString *exceptionMsg))failureBlock
{
    NSString *likeOrUnlikeStr = @"restaurantevent/like";
    if(liked){
        likeOrUnlikeStr = @"restaurantevent/unlike";
    }
    
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        NSDictionary *parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userId",user.userId,@"userID",user.token,@"t",restaurantEventId,@"restaurantEventId",[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"latitude",[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],@"longitude", nil];
        [[IMGNetWork sharedManager]POST:likeOrUnlikeStr parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
         {
//             [[LoadingView sharedLoadingView] stopLoading];
             NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
             if(responseObject && errorMessage){
                 failureBlock(errorMessage);
                 NSLog(@"likeJournal: error: %@",errorMessage);
             }else if(responseObject){
                 block();
             }
         }failure:^(NSURLSessionDataTask *operation, NSError *error)
         {
             failureBlock(error.localizedDescription);
//             [[LoadingView sharedLoadingView] stopLoading];
             NSLog(@"getRestaurantFirstDataFromService error: %@",error.localizedDescription);
         }];
        
    }else{
//        [[LoadingView sharedLoadingView] stopLoading];
    }
}
+(void)likeRestaurantOfferCard:(BOOL)liked OfferCardId:(NSNumber*)OfferCardId andBlock:(void (^)())block  failure:(void(^)(NSString *exceptionMsg))failureBlock
{
    NSString *likeOrUnlikeStr = @"restaurantoffer/like";
    if(liked){
        likeOrUnlikeStr = @"restaurantoffer/unlike";
    }
    
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        NSDictionary *parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",OfferCardId,@"id",[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"latitude",[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],@"longitude", nil];
        [[IMGNetWork sharedManager]POST:likeOrUnlikeStr parameters:parameters success:^(NSURLSessionDataTask *operation, id responseObject)
         {
             //             [[LoadingView sharedLoadingView] stopLoading];
             NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
             if(responseObject && errorMessage){
                 failureBlock(errorMessage);
                 NSLog(@"likeJournal: error: %@",errorMessage);
             }else if(responseObject){
                 block();
             }
         }failure:^(NSURLSessionDataTask *operation, NSError *error)
         {
             failureBlock(error.localizedDescription);
             //             [[LoadingView sharedLoadingView] stopLoading];
             NSLog(@"getRestaurantFirstDataFromService error: %@",error.localizedDescription);
         }];
        
    }else{
        //        [[LoadingView sharedLoadingView] stopLoading];
    }
}


+(void)likeRestaurantEventCard:(BOOL)liked homeTimelineRestaurantUpdateId:(NSNumber*)homeTimelineRestaurantUpdateId andBlock:(void (^)())block  failure:(void(^)(NSString *exceptionMsg))failureBlock
{
    NSString *likeOrUnlikeStr = @"restaurantEvent/homeTimeLineLike";
    if(liked){
        likeOrUnlikeStr = @"restaurantEvent/homeTimeLineUnLike";
    }
    
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        NSDictionary *parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userId",user.userId,@"userID",user.token,@"t",homeTimelineRestaurantUpdateId,@"homeTimelineRestaurantUpdateId",[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"latitude",[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],@"longitude",nil];
        [[IMGNetWork sharedManager]POST:likeOrUnlikeStr parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
         {
             //             [[LoadingView sharedLoadingView] stopLoading];
             NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
             if(responseObject && errorMessage){
                 failureBlock(errorMessage);
                 NSLog(@"likeJournal: error: %@",errorMessage);
             }else if(responseObject){
                 block();
             }
         }failure:^(NSURLSessionDataTask *operation, NSError *error)
         {
             failureBlock(error.localizedDescription);
             //             [[LoadingView sharedLoadingView] stopLoading];
             NSLog(@"getRestaurantFirstDataFromService error: %@",error.localizedDescription);
         }];
        
    }else{
        //        [[LoadingView sharedLoadingView] stopLoading];
    }
}
+(void)postComment:(NSString *)commentStr restaurantEventId:(NSNumber*)restaurantEventId andBlock:(void (^)(IMGRestaurantEventComment *eventComment))block failure:(void(^)(NSString *errorReason))failureBlock{
    NSString *trimmedString = [commentStr stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        if (!(trimmedString.length==0)) {
            [[LoadingView sharedLoadingView] startLoading];
            NSDictionary *parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",restaurantEventId,@"restaurantEventId",commentStr,@"comment",[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"latitude",[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],@"longitude", nil];
            
            [[IMGNetWork sharedManager]POST:@"restaurantevent/comment" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
             {
                 [[LoadingView sharedLoadingView] stopLoading];
                 NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
                 if(responseObject && errorMessage){
                     failureBlock(errorMessage);
                     NSLog(@"postComment: error: %@",errorMessage);
                 }else if(responseObject){
                     
                     NSDictionary *commentDictionary = [responseObject objectForKey:@"commentInfo"];
                     if(commentDictionary){
                         IMGRestaurantEventComment *restaurantEventComment = [[IMGRestaurantEventComment alloc]initFromDictionary:commentDictionary];
                         block(restaurantEventComment);
                     }else{
                         block(nil);
                     }
                     
                 }
             }failure:^(NSURLSessionDataTask *operation, NSError *error)
             {
                 [[LoadingView sharedLoadingView] stopLoading];
                 NSLog(@"postCommentWithRestaurantEvent error: %@",error.localizedDescription);
                 failureBlock(error.localizedDescription);
             }];
        }
    }

}
+(void)postComment:(NSString *)commentStr restaurantOfferCardId:(NSNumber*)restaurantOfferCardId andBlock:(void (^)(IMGDishComment *dishComment))block failure:(void(^)(NSString *errorReason))failureBlock{
    NSString *trimmedString = [commentStr stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        if (!(trimmedString.length==0)) {
            [[LoadingView sharedLoadingView] startLoading];
            NSDictionary *parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",restaurantOfferCardId,@"restaurantOfferId",commentStr,@"comment",[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"latitude",[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],@"longitude", nil];
            
            [[IMGNetWork sharedManager]POST:@"restaurantoffer/comment" parameters:parameters success:^(NSURLSessionDataTask *operation, id responseObject)
             {
                 [[LoadingView sharedLoadingView] stopLoading];
                 NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
                 if(responseObject && errorMessage){
                     failureBlock(errorMessage);
                     NSLog(@"postComment: error: %@",errorMessage);
                 }else if(responseObject){
                     
                     NSDictionary *commentDictionary = [responseObject objectForKey:@"commentInfo"];
                     if(commentDictionary){
                         IMGDishComment *restaurantEventComment = [[IMGDishComment alloc]initFromDictionary:commentDictionary];
                         block(restaurantEventComment);
                     }else{
                         block(nil);
                     }
                     
                 }
             }failure:^(NSURLSessionDataTask *operation, NSError *error)
             {
                 [[LoadingView sharedLoadingView] stopLoading];
                 NSLog(@"postCommentWithrestaurantoffer error: %@",error.localizedDescription);
                 failureBlock(error.localizedDescription);
             }];
        }
    }
    
}


+(void)postCardComment:(NSString *)commentStr restaurantId:(NSNumber*)restaurantId homeTimeLineId:(NSNumber *)homeTimeLineId andBlock:(void (^)(IMGRestaurantEventComment *eventComment))block failure:(void(^)(NSString *errorReason))failureBlock{
    NSString *trimmedString = [commentStr stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        if (!(trimmedString.length==0)) {
            [[LoadingView sharedLoadingView] startLoading];
            NSDictionary *parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",restaurantId,@"restaurantId",commentStr,@"comment",homeTimeLineId,@"homeTimelineRestaurantUpdateId",[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"latitude",[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],@"longitude", nil];
            
            [[IMGNetWork sharedManager]POST:@"restaurantEvent/saveHomeTimeLineComment" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
             {
                 [[LoadingView sharedLoadingView] stopLoading];
                 NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
                 if(responseObject && errorMessage){
                     NSLog(@"postComment: error: %@",errorMessage);
                     failureBlock(errorMessage);
                 }else if(responseObject){
                     NSDictionary *commentDictionary = [responseObject objectForKey:@"commentInfo"];
                     if(commentDictionary){
                         
                         IMGRestaurantEventComment *restaurantEventComment = [[IMGRestaurantEventComment alloc]initFromDictionary:commentDictionary];
                         block(restaurantEventComment);
                     }else{
                         block(nil);
                     }
                     
                 }
             }failure:^(NSURLSessionDataTask *operation, NSError *error)
             {
                 [[LoadingView sharedLoadingView] stopLoading];
                 NSLog(@"postCommentWithRestaurantEvent error: %@",error.localizedDescription);
                 failureBlock(error.localizedDescription);
             }];
        }
    }
    
}

+(void)getRestaurantDishListFromService:(NSNumber *)restaurantId offset:(NSNumber*)offset andBlock:(void (^)(NSMutableArray *photoArray,NSNumber * dishCount,NSNumber *offsetBlock))block{
    NSDictionary *parameters;
    if ([AppDelegate ShareApp].userLogined) {
        parameters=@{@"userId":[IMGUser currentUser].userId,@"restaurantId":restaurantId,@"offset":offset,@"max":@10};
    }else{
        parameters=@{@"restaurantId":restaurantId,@"offset":offset,@"max":@10};
    }
    
    [[IMGNetWork sharedManager]POST:@"app/restaurant/dish/list/v2" parameters:parameters success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSNumber *dishCount =[responseObject objectForKey:@"dishCount"];
        
        NSArray *dishArray = [responseObject objectForKey:@"dishList"];
        NSMutableArray *userPhotoArray = [[NSMutableArray alloc] init];
        
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        NSDate *createTime=[[NSDate alloc] init];
        [formatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
        for(int i=0; i<dishArray.count; i++){
            NSDictionary *tempDic = [dishArray objectAtIndex:i];
            NSNumber *type=[ tempDic objectForKey:@"type"];
          
            IMGDish *dish = [[IMGDish alloc] init];
            if ([type intValue]==1) {
                NSDictionary *dishDictionary = [tempDic objectForKey:@"dish"];
                [dish setValuesForKeysWithDictionary:dishDictionary];
                dish.userName=[dishDictionary objectForKey:@"creator"];
                createTime=[formatter dateFromString:[dishDictionary objectForKey:@"createTime"]];
                dish.createTime=[NSNumber numberWithDouble:createTime.timeIntervalSince1970];
                dish.descriptionStr = [dishDictionary objectForKey:@"description"];
                [dish updatePhotoCredit:[dishDictionary objectForKey:@"photoCredit"]];
                [userPhotoArray addObject:dish];
            }else if([type intValue]==2){
                NSDictionary *dishDictionary = [tempDic objectForKey:@"instagramPhoto"];
                [dish setValuesForKeysWithDictionary:dishDictionary];
                dish.userName=[dishDictionary objectForKey:@"instagramUserName"];
                NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[[dishDictionary objectForKey:@"approvedDate"] longLongValue]/1000];
                NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];

                createTime=[formatter dateFromString:confromTimespStr];
                dish.createTime=[NSNumber numberWithDouble:createTime.timeIntervalSince1970];
                dish.imageUrl = [dishDictionary objectForKey:@"standardResolutionImage"];
                
                [userPhotoArray addObject:dish];

            }
            
          
        }
        
        block(userPhotoArray,dishCount,offset);
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"getRestaurantDishListFromService error: %@",error.localizedDescription);
    }];
}


+(void)getRestaurantEventPhotoListFromService:(NSNumber *)restaurantId offset:(NSNumber*)offset andBlock:(void (^)(NSMutableArray *photoArray,NSNumber * count,NSNumber *offsetBlock))block{
    NSDictionary *parameters;
    if ([AppDelegate ShareApp].userLogined) {
        parameters=@{@"userId":[IMGUser currentUser].userId,@"restaurantId":restaurantId,@"offset":offset,@"max":@10};
    }else{
        parameters=@{@"restaurantId":restaurantId,@"offset":offset,@"max":@10};
    }
    
    [[IMGNetWork sharedManager]POST:@"app/restauranteventphoto/list" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSNumber *restaurantEventCount =[responseObject objectForKey:@"restaurantEventCount"];
        
        NSArray *phoyoList = [[NSArray alloc]initWithArray:responseObject[@"restaurantEventList"]];
        NSMutableArray *photoEventList = [[NSMutableArray alloc]init];
        for(id item in phoyoList){
            IMGRestaurantEvent *event = [[IMGRestaurantEvent alloc]init];
            [event setValuesForKeysWithDictionary:item];
            [event updatePhotoCredit:item[@"photoCreditImpl"]];
            event.eventId=[item objectForKey:@"id"];
            event.restaurantId = [NSNumber numberWithInt:[restaurantId intValue]];
            [photoEventList addObject:event];
        }
        block(photoEventList,restaurantEventCount,offset);
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"getRestaurantDishListFromService error: %@",error.localizedDescription);
    }];
}

+(void)getRestaurantDetailByReviewIdFromServer:(NSNumber*)reviewId andBlock:(void(^)(IMGRestaurant* restaurant,NSNumber *reviewScore))block{
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:reviewId forKey:@"reviewId"];
    IMGUser *user = [IMGUser currentUser];
    if ( (user.userId !=nil) && (user.token !=nil) ) {
        [parameters setObject:user.userId forKey:@"userId"];
        [parameters setObject:user.token forKey:@"token"];
    }
    
    [[IMGNetWork sharedManager] GET:@"app/user/review/detail" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation,id responseObject){
        NSString *exceptionmsg=[responseObject objectForKey:@"exceptionmsg"];
        if(!exceptionmsg){
            NSDictionary *reviewDic = [responseObject objectForKey:@"review"];
            
            IMGRestaurant *restaurant=[[IMGRestaurant alloc] init];
            restaurant.restaurantId = [reviewDic objectForKey:@"restaurantId"];
            restaurant.title = [reviewDic objectForKey:@"restaurantTitle"];
            restaurant.cuisineName = [reviewDic objectForKey:@"restaurantCuisine"];
            restaurant.districtName = [reviewDic objectForKey:@"restaurantDistrict"];
            restaurant.cityName = [reviewDic objectForKey:@"restaurantCityName"];
            restaurant.priceLevel = [reviewDic objectForKey:@"restaurantPriceLevel"];
            restaurant.seoKeyword = [reviewDic objectForKey:@"restaurantSeoKeywords"];
            
            block(restaurant,[reviewDic objectForKey:@"reviewScore"]);
        }else{
            block(nil,nil);
        }
    } failure:^(NSURLSessionDataTask *operation,NSError *error){
        
    }];
}
@end
