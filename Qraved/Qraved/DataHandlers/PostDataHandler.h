//
//  PostData.h
//  Qraved
//
//  Created by Shine Wang on 7/25/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "AFClient.h"
//#import "AFHTTPClient.h"
//#import "AFJSONRequestOperation.h"


@interface PostDataHandler : NSObject
+(PostDataHandler *)sharedPostData;

-(void)postImage:(UIImage *)image andTitle:(NSString *)title andDescription:(NSString *)description andSuccessBlock:(void(^)())successBlock andFailedBlock:(void(^)())failedBlock;  //Post image
+(void)uploadDishes:(NSString*)dishes andSuccessBlock:(void(^)())successBlock andFailedBlock:(void(^)())failedBlock;
+(void)batchupload:(NSMutableDictionary *)param andSuccessBlock:(void(^)(id responseObject))successBlock andFailedBlock:(void(^)())failedBlock;
- (void)postMenuImage:(NSDictionary *)parameters andSuccessBlock:(void(^)())successBlock andFailedBlock:(void(^)(NSString *exceptionMsg))failedBlock;

-(void)postbackImage:(UIImage *)image andTitle:(NSString *)title andDescription:(NSString *)description andSuccessBlock:(void(^)())successBlock andFailedBlock:(void(^)())failedBlock;
@end
