//
//  DetailDataHandler.h
//  Qraved
//
//  Created by Lucky on 15/3/30.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBManager.h"
#import "IMGRestaurantOffer.h"
#import "IMGRestaurant.h"
#import "IMGRDPRestaurant.h"
#import "IMGRDPRestaurant.h"
#import "CouponModel.h"

@interface DetailDataHandler : NSObject


+ (NSMutableArray *)getChefRecommendsData:(NSNumber *)restaurantID;
//+ (IMGRestaurantOffer *)getOffersFromResponseObject:(NSArray *)responseObject;
//+ (void)getRestaurantFirstDataFromService:(NSNumber *)restaurantID andBlock:(void (^)(IMGRestaurant *restaurant,NSArray *eventList,IMGRestaurantOffer *restaurantOffer,NSArray *banerImageArr,IMGRDPRestaurant *rdpRest))block;

+ (void)getRestaurantSecondDataFromService:(NSNumber *)restaurantId and:(IMGRDPRestaurant*)restaurant andBlock:(void (^)(NSArray *menuList,NSArray *dishList,NSArray *moreDishList,NSArray *reviewList,NSArray *eventList,NSArray *diningGuideList,NSArray *journalList,NSNumber *menuPhotoCount,NSNumber *dishCount,NSNumber *reviewedUserCount,IMGRDPRestaurant *rdpCache))block andBlockEventCountWithDiningGuideCount:(void (^)(NSNumber *eventCount,NSNumber *diningGuideCount,NSNumber *journalCount))countBlock;
+ (void)getMoreInfoFromService:(IMGRestaurant *)restaurant andBlock:(void (^)())block;

+ (void)getEventAndDinningGuideFromService:(NSNumber *)restaurantId andBlock:(void (^)(NSArray *eventList,NSArray *dinningGuideList))block;

+ (void)getBookInfoFromService:(IMGRestaurant *)restaurant andBlock:(void (^)())block;
+ (IMGRestaurantOffer *)getOffersFromResponseObjectNow:(NSDictionary *)responseObject;
+ (NSArray *)getCouponsFromResponseObject:(NSDictionary *)responseObject;

+(void)getRestaurantJournal:(NSDictionary*)paramters block:(void(^)(NSNumber *total,NSArray *journals))block failure:(void(^)(NSString* description))failure;
+ (void)getRestaurantFirstDataFromServiceNow:(NSNumber *)restaurantID andBlock:(void (^)(IMGRestaurant *restaurant,NSArray *eventList,IMGRestaurantOffer *restaurantOffer,NSArray *banerImageArr,IMGRDPRestaurant *rdpRest,NSNumber *showClaim,NSArray *couponArray))block;
+(void)claimYourRestaurant:(NSDictionary*)parameters block:(void(^)())block failure:(void(^)(NSString *errorMsg))failure;
+(void)getRestaurantDetailInfoWithResturantID:(NSNumber*)restaurantID andBlock:(void (^)(id respondes))block withError:(void(^)(NSError* error))errorBlock;
+(void)getLastReviewWithParameter:(NSDictionary *)parameter andBlock:(void(^)(NSString *str,NSNumber *num,NSNumber*reviewID ))block;
+(void)saveRestaurantWithParameter:(NSDictionary *)param andUrl:(NSString*)url andBlock:(void(^)(NSString *succeedStr,NSString *failedString))block andErrorBlock:(void(^)(NSError *error))errorBlock;
+(void)getRestaurantIsFavouriteWithParameter:(NSDictionary *)param andBlock:(void(^)(NSString *likeStr))block andErrorBlock:(void(^)(NSError *error))errorBlock;

+(void)SellAllPhotoQraved:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array,NSMutableArray *imgArray,NSString *goFoodlink))successBlock anderrorBlock:(void(^)())errorBlock;
+(void)SellAllPhotoInstagram:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array,NSString *goFoodlink))successBlock anderrorBlock:(void(^)())errorBlock;

+(void)SellMenuBarInstagram:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array))successBlock anderrorBlock:(void(^)())errorBlock;

+(void)SeeMenuDelevery:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array))successBlock anderrorBlock:(void(^)())errorBlock;
+(void)SeeMenuRestaurant:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array, NSNumber *restaurantMenuCount, NSNumber *barMenuCount, NSNumber *deliveryMenuCount))successBlock anderrorBlock:(void(^)())errorBlock;

+(void)suggestAnEditSubmit:(NSDictionary*)dic andSuccessBlock:(void(^)(NSDictionary * array))successBlock anderrorBlock:(void(^)())errorBlock;




@end
