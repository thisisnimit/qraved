//
//  MenuPhotoHandler.m
//  Qraved
//
//  Created by Evan on 16/4/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "MenuPhotoHandler.h"

@implementation MenuPhotoHandler

+ (void)getUserUploadMenuDataFromService:(NSDictionary *)parameters andBlock:(void (^)(NSMutableArray *menuList,NSNumber *offset,NSMutableArray *menuPhotoArray,NSNumber *menuPhotoCount,NSNumber *menuPhotoCountTypeRestant,NSNumber *menuPhotoCountTypeBar,NSNumber *menuPhotoCountTypeDelivery,IMGMenu *menu_))block
{
    [[IMGNetWork sharedManager]POST:@"app/restaurant/menuphoto/userupload/list" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSMutableArray *menuPhotoList = [[NSMutableArray alloc] init];
        NSMutableArray *menuPhotoArray = [[NSMutableArray alloc]init];
        //        offset = offset + max;
        
        menuPhotoList = [responseObject objectForKey:@"menuPhotoList"];
        
        for (NSDictionary *dic in menuPhotoList) {
            IMGMenu *menu = [[IMGMenu alloc]init];
            [menu setValuesForKeysWithDictionary:dic];
            menu.menuId = [dic objectForKey:@"id"];
            [menuPhotoArray addObject:menu];
        }
        
        IMGMenu *menu = [[IMGMenu alloc]init];
        NSNumber *menePhotoCount = [responseObject objectForKey:@"menuPhotoCount"];
        NSNumber *menuPhotoCountTypeRestant = [responseObject objectForKey:@"menuPhotoCountType1"];
        NSNumber *menuPhotoCountTypeBar = [responseObject objectForKey:@"menuPhotoCountType2"];
        NSNumber *menuPhotoCountTypeDelivery = [responseObject objectForKey:@"menuPhotoCountType3"];
        menu.menuPhotoCount = menePhotoCount;
        menu.menuPhotoCountTypeRestant = menuPhotoCountTypeRestant;
        menu.menuPhotoCountTypeBar = menuPhotoCountTypeBar;
        menu.menuPhotoCountTypeDelivery = menuPhotoCountTypeDelivery;
        
        block(menuPhotoList,[NSNumber numberWithInt:10],menuPhotoArray,menePhotoCount,menuPhotoCountTypeRestant,menuPhotoCountTypeBar,menuPhotoCountTypeDelivery,menu);
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"getUserUploadMenuDataFromService error: %@",error.localizedDescription);
    }];

}

+(void)getMenuGalleryDataFromService:(NSNumber *)restaurantId offset:(NSNumber*)offset andBlock:(void (^)(NSMutableArray *menuList,NSNumber *offset,NSMutableArray *menuPhotoArray,NSNumber *menuPhotoCount,NSNumber *menuPhotoCountTypeRestant,NSNumber *menuPhotoCountTypeBar,NSNumber *menuPhotoCountTypeDelivery,IMGMenu *menu_,NSNumber *offsetBlock))block{
    
    NSDictionary *parameters=@{@"restaurantId":restaurantId,@"offset":offset,@"max":@10};
    [[IMGNetWork sharedManager]POST:@"app/restaurant/menuphoto/list" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSMutableArray *menuPhotoList = [[NSMutableArray alloc] init];
        NSMutableArray *menuPhotoArray = [[NSMutableArray alloc]init];
        //        offset = offset + max;
        
        menuPhotoList = [responseObject objectForKey:@"menuPhotoList"];
        
        for (NSDictionary *dic in menuPhotoList) {
            IMGMenu *menu = [[IMGMenu alloc]init];
            [menu setValuesForKeysWithDictionary:dic];
            menu.menuId = [dic objectForKey:@"id"];
            [menuPhotoArray addObject:menu];
        }
        
        IMGMenu *menu = [[IMGMenu alloc]init];
        NSNumber *menePhotoCount = [responseObject objectForKey:@"menuPhotoCount"];
        NSNumber *menuPhotoCountTypeRestant = [responseObject objectForKey:@"menuPhotoCountType1"];
        NSNumber *menuPhotoCountTypeBar = [responseObject objectForKey:@"menuPhotoCountType2"];
        NSNumber *menuPhotoCountTypeDelivery = [responseObject objectForKey:@"menuPhotoCountType3"];
        menu.menuPhotoCount = menePhotoCount;
        menu.menuPhotoCountTypeRestant = menuPhotoCountTypeRestant;
        menu.menuPhotoCountTypeBar = menuPhotoCountTypeBar;
        menu.menuPhotoCountTypeDelivery = menuPhotoCountTypeDelivery;
        
        block(menuPhotoList,offset,menuPhotoArray,menePhotoCount,menuPhotoCountTypeRestant,menuPhotoCountTypeBar,menuPhotoCountTypeDelivery,menu,offset);
        
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"getPhotoGalleryDataFromService error: %@",error.localizedDescription);
    }];
    
    
}


@end
