//
//  DataHandler.m
//  Qraved
//
//  Created by Jeff on 10/8/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "DataHandler.h"
#import "DBManager.h"
#import "IMGTagUpdate.h"
#import "CuisineHandler.h"
#import "TagHandler.h"
#import "DistrictHandler.h"
#import "OfferHandler.h"
@implementation DataHandler


-(void)initFromServer
{
    dataArray = [[NSMutableArray array]init];
    [self getDatasFromServer];
}

-(void)getDatasFromServer
{

    NSMutableArray *tagUpdateArrM = [[NSMutableArray alloc] init];
    NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM IMGTagUpdate;"];
    FMResultSet *resultSet = [[DBManager manager] executeQuery:sqlString];
    while ([resultSet next])
    {
        IMGTagUpdate *tagUpdate=[[IMGTagUpdate alloc]init];
        [tagUpdate setValueWithResultSet:resultSet];
        [tagUpdateArrM addObject:tagUpdate];
    }
    
    NSString *typeIds = [NSString new];
    NSString *updateDates = [NSString new];
    for (IMGTagUpdate *tagUpdate in tagUpdateArrM)
    {
        if (typeIds.length)
        {
            typeIds = [NSString stringWithFormat:@"%@,%@",typeIds,tagUpdate.typeId];
        }
        else
            typeIds = [NSString stringWithFormat:@"%@",tagUpdate.typeId];
        
        if (updateDates.length)
        {
            updateDates = [NSString stringWithFormat:@"%@,%@",updateDates,tagUpdate.updateDate];
        }
        else
            updateDates = [NSString stringWithFormat:@"%@",tagUpdate.updateDate];
    }
    NSDictionary *params = @{@"typeIds":typeIds,@"updateDates":updateDates};
    
    [[IMGNetWork sharedManager] GET:@"foodco/checkSearchLeftUpdate" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueue];
        [queue inDatabase:^(FMDatabase *db) {
            
//            NSString *sqlString = [NSString stringWithFormat:@"DELETE FROM IMGTagUpdate;"];
//            [[DBManager manager] deleteWithSql:sqlString];

            NSArray *searchTypeUpdateList = [responseObject objectForKey:@"searchTypeUpdateList"];
            for (NSDictionary *dic in searchTypeUpdateList)
            {
                IMGTagUpdate *tagUpdate = [IMGTagUpdate new];
                [tagUpdate setValuesForKeysWithDictionary:dic];
                if ([tagUpdate.type isEqualToString:@"Offers"])
                {
                    continue;
                }
                
                if ([tagUpdate.type isEqualToString:@"Cuisine"])
                {
                    tagUpdate.shows = @"Cuisine";
                }
                else if ([tagUpdate.type isEqualToString:@"Area"])
                {
                    tagUpdate.shows = @"Area";
                }
                else if ([tagUpdate.type isEqualToString:@"Location"])
                {
                    tagUpdate.shows = @"Location";
                }
                
                
                [[DBManager manager]insertModel:tagUpdate selectField:@"typeId" andSelectID:tagUpdate.typeId andFMDatabase:db];
                
                switch ([[dic objectForKey:@"typeId"] intValue])
                {
                    case 50:
                    {
                          [OfferHandler getOffersFromServer];
                    }
                        break;
                    case 1:
                    {
                        [[[TagHandler alloc] init] getAreaDatasFromServerWithTypeId:[NSNumber numberWithInt:1]];
                    }
                        break;
                    case 2:
                    {
                        [[[DistrictHandler alloc]init]initFromServer];
                    }
                        break;

                    case 3:
                    {
                        [[[CuisineHandler alloc]init]initFromServer];
                    }
                        break;
                        
                    default:
                    {
                        [[[TagHandler alloc] init] getDatasFromServerWithTagTypeId:[dic objectForKey:@"tagTypeId"]  typeId:[dic objectForKey:@"typeId"]];
                    }
                        break;
                }
            }
            
        }];
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"%@",error.localizedDescription);
    }];

}


@end
