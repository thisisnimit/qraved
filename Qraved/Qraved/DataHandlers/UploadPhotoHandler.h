//
//  UploadPhotoHandler.h
//  Qraved
//
//  Created by lucky on 16/3/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGDishComment.h"

@interface UploadPhotoHandler : NSObject
+(void)previousComment:(NSString *)commentIdStr targetId:(NSNumber*)articleId andBlock:(void (^)(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount))block;
+(void)previousCardComment:(NSString *)commentIdStr targetId:(NSNumber*)articleId andBlock:(void (^)(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount))block;
+(void)likeDish:(BOOL)liked dishId:(NSNumber*)dishId andBlock:(void (^)())block  failure:(void(^)(NSString *exceptionMsg))failureBlock;
+(void)likeCard:(BOOL)liked moderateReviewId:(NSNumber*)moderateReviewId andBlock:(void (^)())block  failure:(void(^)(NSString *exceptionMsg))failureBlock;
+(void)postComment:(NSString *)commentStr dishId:(NSNumber*)dishId restaurantId:(NSNumber *)restaurantId andBlock:(void (^)(IMGDishComment *dishComment))block failure:(void(^)(NSString *errorReason))failureBlock;
+(void)postCommentCard:(NSString *)commentStr moderateReviewId:(NSNumber*)moderateReviewId restaurantId:(NSNumber *)restaurantId andBlock:(void (^)(IMGDishComment *dishComment))block failure:(void(^)(NSString *errorReason))failureBlock;
+(void)getUploadPhotoCardData:(NSDictionary *)params andBlock:(void (^)(NSArray *dataArr))block;
+(void)getUploadPhotoDetailCardData:(NSDictionary *)params andBlock:(void (^)(NSArray *dataArr))block;

@end
