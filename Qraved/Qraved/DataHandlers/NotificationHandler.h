//
//  NotificationHandler.h
//  Qraved
//
//  Created by Lucky on 15/11/9.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import "DataHandler.h"
#import "IMGUser.h"

@interface NotificationHandler : DataHandler


+ (void)getNotificationListWithParams:(NSDictionary *)params andBlock:(void (^)(NSArray *dataArray))block  failure:(void(^)(NSString *exceptionMsg))failureBlock;
+ (void)readSplashWithParams:(NSDictionary *)params;
+ (void)readNotificationWithParams:(NSDictionary *)params;
+ (void)getNotificationCountWithParams:(NSDictionary *)params andBlock:(void (^)(NSNumber *notificationCount,NSString *exceptionmsg))block;
+ (void)updateNotificationCountWithParams:(NSDictionary *)params andBlock:(void (^)(id responseObject))block;
+ (void)getUserNotificationSetByUserId:(NSDictionary *)params andBlock:(void (^)(id responseObject))block;
+ (void)userNotificationSetSaveAndUpdate:(NSDictionary *)params andBlock:(void (^)(id responseObject))block;
@end
