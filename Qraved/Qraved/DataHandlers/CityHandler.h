//
//  MealHandler.h
//  Qraved
//
//  Created by Jeff on 10/23/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "DataHandler.h"

@interface CityHandler : DataHandler

+(void)changeCity:(NSNumber*)cityId withUser:(NSNumber*)userId block:(void(^)(BOOL))block;

@end
