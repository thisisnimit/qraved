//
//  UserData.m
//  Qraved
//
//  Created by Shine Wang on 7/15/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "UserDataHandler.h"
#import "AppDelegate.h"
#import "LoginParamter.h"
#import "LoadingView.h"
#import "PostDataHandler.h"
#import "IMGThirdPartyUser.h"
#import "IMGUser.h"
#import "DBManager.h"
#import "CityUtils.h"
#import "CityHandler.h"
#import "IMGCity.h"
#import <Google/SignIn.h>
#import <sys/socket.h>
#import <sys/sockio.h>
#import <sys/ioctl.h>
#import <net/if.h>
#import <arpa/inet.h>
#import "AMPDeviceInfo.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

//#import "MixpanelHelper.h"

@implementation UserDataHandler
NSString * const USER_LOGINTYPE_WITHFACEBOOK = @"loginwithfacebook";
NSString * const USER_LOGINTYPE_WITHSELF = @"self";
NSString * const USER_LOGINTYPE_WITHTWITTER = @"loginwithtwitter";
NSString * const USER_LOGINTYPE_WITHGoogle = @"loginwithgoogle";
NSMutableDictionary *parameters;
bool isSignUp;
bool isUpdate;
- (NSDictionary *)getParameters
{
    return parameters;
}
- (BOOL)isSignUp
{
    return isSignUp;
}
- (BOOL)isUpdate
{
    return isUpdate;
}
- (void)twitterLogin :(NSDictionary *)twitterResult withError:(NSError *)twitterOpenError withDelegateBlock: (void (^)())block failedBlock:(void(^)())failedBlock
{
//    [MixpanelHelper trackLoginWithType:@"Twitter"];
    
    TWitterUser *twitterUser = [[TWitterUser alloc] init];
    if (!twitterOpenError) {
        NSString *sessionID = [[NSUserDefaults standardUserDefaults] objectForKey:@"sessionID"] ;
        if(!sessionID){
            sessionID = @"NOSESSION";
        }
        parameters = [[NSMutableDictionary alloc] init];
        parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    twitterUser.password,twitterUser.password,
                                     USER_LOGINTYPE_WITHTWITTER, @"loginwith",
                                     [twitterResult objectForKey:twitterUser.gender],twitterUser.gender,
                                     [twitterResult objectForKey:twitterUser.userName],twitterUser.userName,
                                     [twitterResult objectForKey:twitterUser.firstName],twitterUser.firstName,
                                     [twitterResult objectForKey:twitterUser.lastName],twitterUser.lastName,
                                     [twitterResult objectForKey:twitterUser.picture],twitterUser.picture,
                                                                          [twitterResult objectForKey:twitterUser.updatedTime],twitterUser.updatedTime,
                                     [twitterResult objectForKey:twitterUser.userID ],@"thirdId",
                                     sessionID,@"sessionID",@"0",@"manual",nil];
        [parameters setObject:@"2" forKey:@"thirdPartyType"];
        //[twitterResult objectForKey:twitterUser.birthday],twitterUser.birthday,
        //[twitterResult objectForKey:twitterUser.email],twitterUser.email,
        [parameters setObject:[twitterResult objectForKey:twitterUser.email] forKey:twitterUser.email];
        [parameters setObject:[twitterResult objectForKey:twitterUser.userID ] forKey:@"thirdPartyId"];
        [parameters setObject:[twitterResult objectForKey:twitterUser.firstName] forKey:@"firstName"];
        [parameters setObject:[twitterResult objectForKey:twitterUser.lastName] forKey:@"lastName"];        [LoginParamter sharedParameter].loginwith=USER_LOGINTYPE_WITHTWITTER;
        
        NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:@"Login" action:@"Twitter" label:[NSString stringWithFormat:@"%@",[twitterResult objectForKey:twitterUser.userName]] value:nil] build];
        id tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"Login"];
        [tracker send:event];
        [self isSignIn:parameters withBlock:block failedBlock:failedBlock];
        [[Amplitude instance] logEvent:@"SU - Sign Up Succeed" withEventProperties:@{@"Sign Up Type":@"Twitter",@"Reason":@"Authorized Twitter"}];
        [[AppsFlyerTracker sharedTracker] trackEvent:@"SU - Sign Up Succeed" withValues:@{@"Sign Up Type":@"Twitter",@"Reason":@"Authorized Twitter"}];



        //[self requestLogin:parameters delegateWithBlock:block failedBlock:failedBlock];
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Error:"]
                                                            message:twitterOpenError.description
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [[Amplitude instance]
         logEvent:@"SU - Sign UP Failed" withEventProperties:@{@"Reason":twitterOpenError.description,@"Sign in type":@"Twitter"}];
        [alertView show];
        return ;
    }
}

- (void)twitterConnect:(NSDictionary *)twitterResult withError:(NSError *)twitterOpenError withDelegateBlock: (void (^)())block failedBlock:(void(^)())failedBlock{
//    [MixpanelHelper trackLoginWithType:@"Twitter"];
    
    TWitterUser *twitterUser = [[TWitterUser alloc] init];
    if (!twitterOpenError){
        NSString *sessionID = [[NSUserDefaults standardUserDefaults] objectForKey:@"sessionID"];
        if(!sessionID){
            sessionID = @"NOSESSION";
        }
        IMGUser *user=[IMGUser currentUser];
        NSDictionary * parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                     [twitterResult objectForKey:twitterUser.email],twitterUser.email,
                                     twitterUser.password,twitterUser.password,
                                     USER_LOGINTYPE_WITHTWITTER, @"loginwith",
                                     [twitterResult objectForKey:twitterUser.gender],twitterUser.gender,
                                     [twitterResult objectForKey:twitterUser.userName],twitterUser.userName,
                                     [twitterResult objectForKey:twitterUser.firstName],twitterUser.firstName,
                                     [twitterResult objectForKey:twitterUser.lastName],twitterUser.lastName,
                                     [twitterResult objectForKey:twitterUser.picture],twitterUser.picture,
                                     [twitterResult objectForKey:twitterUser.birthday],twitterUser.birthday,
                                     [twitterResult objectForKey:twitterUser.updatedTime],twitterUser.updatedTime,
                                     [twitterResult objectForKey:twitterUser.userID],@"thirdId",
                                     sessionID,@"sessionID",@"0",@"manual",user.userId,@"userID",user.token,@"t",nil];

        [LoginParamter sharedParameter].loginwith=USER_LOGINTYPE_WITHTWITTER;
        
        NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:@"Login" action:@"Twitter" label:[NSString stringWithFormat:@"%@",[twitterResult objectForKey:twitterUser.userName]] value:nil] build];
        id tracker = [[GAI sharedInstance] defaultTracker];
        [tracker set:kGAIScreenName value:@"Login"];
        [tracker send:event];
        

        [self requestConnect:parameters delegateWithBlock:block failedBlock:failedBlock];
    }else{
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Error:"]
                                                            message:twitterOpenError.description
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
        return ;
    }
}

-(void)faceBookLogin:(NSArray *)permissions delegateWithBlock:(void (^)())block failedBlock:(void(^)())failedBlock{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login
     logInWithReadPermissions: permissions
     fromViewController:nil
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         NSLog(@"facebook login result.grantedPermissions = %@,error = %@",result.grantedPermissions,error);
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             NSLog(@"Logged in");
             if ([result.grantedPermissions containsObject:@"public_profile"]) {
                 if ([FBSDKAccessToken currentAccessToken]) {
                     NSString *facebookSigninParameter = [NSString stringWithFormat:@"id,email,name,gender,first_name,last_name,picture,birthday,updated_time"];
                     [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:[NSDictionary dictionaryWithObject:facebookSigninParameter forKey:@"fields"]] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                         if (!error) {
                             NSString *sessionID = [[NSUserDefaults standardUserDefaults] objectForKey:@"sessionID"] ;
                             if(!sessionID){
                                 sessionID = @"NOSESSION";
                             }
                             parameters = [NSMutableDictionary dictionaryWithCapacity:0];
                             if ([result objectForKey:@"email"]) {
                                 [parameters setObject:[result objectForKey:@"email"] forKey:@"email"];
                             }
                             [parameters setObject:USER_LOGINTYPE_WITHFACEBOOK forKey:@"loginwith"];
                            
                             [LoginParamter sharedParameter].loginwith=USER_LOGINTYPE_WITHFACEBOOK;
                             [parameters setObject:[result objectForKey:@"first_name"] forKey:@"first_name"];
                             [parameters setObject:[result objectForKey:@"last_name"] forKey:@"last_name"];
//                             if (!errorPicture) {
//                                 [parameters setObject:[[resultPicture objectForKey:@"data"] objectForKey:@"url"] forKey:fbUser.picture];
//                             }else{
//                                 [parameters setObject:[[[result objectForKey:fbUser.picture] objectForKey:@"data"] objectForKey:@"url"] forKey:fbUser.picture];
//                             }
                             if ([result objectForKey:@"picture"]) {
                                 if ([[result objectForKey:@"picture"] objectForKey:@"data"]) {
                                     [parameters setObject:[[[result objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"] forKey:@"picture"];
                                 }
                             }
            
//                             [parameters setObject:@"" forKey:fbUser.mobileNumber];
                             [parameters setObject:[result objectForKey:@"updated_time"] forKey:@"updated_time"];
                             [parameters setObject:sessionID forKey:@"sessionID"];
//                             [parameters setObject:accessToken forKey:@"access_token"];
                             [parameters setObject:[result objectForKey:@"id"] forKey:@"thirdId"];
                             [parameters setObject:@"manual" forKey:@"0"];
                             
                             [parameters setObject:@"1" forKey:@"thirdPartyType"];
                             [parameters setObject:[result objectForKey:@"id"] forKey:@"thirdPartyId"];
//                             [parameters setObject:[result objectForKey:fbUser.firstName] forKey:@"firstName"];
//                             [parameters setObject:[result objectForKey:fbUser.lastName] forKey:@"lastName"];
                             
                             NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:@"Login" action:@"FaceBook" label:[NSString stringWithFormat:@"%@",[result objectForKey:@"name"]] value:nil] build];
                             id tracker = [[GAI sharedInstance] defaultTracker];
                             
                             [tracker set:kGAIScreenName value:@"Login"];
                             [tracker send:event];
                             
                             [self isSignIn:parameters withBlock:block failedBlock:failedBlock];
                         }else{
                             [[LoadingView sharedLoadingView] stopLoading];
                             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Error: %ld",error.code]
                                                                                 message:error.localizedDescription
                                                                                delegate:nil
                                                                       cancelButtonTitle:@"OK"
                                                                       otherButtonTitles:nil];
                             [[Amplitude instance]
                              logEvent:@"SI - Sign Ip Failed" withEventProperties:@{@"Reason":error.description,@"Sign in type":@"Facebook"}];
                             [alertView show];
                             return ;
                         }
                     }];
                 }
             }
         }
     }];
}
//-(void)faceBookLogin:(FBSession *)session state:(FBSessionState)state error:(NSError *)fbStateChangeError delegateWithBlock:(void (^)())block failedBlock:(void(^)())failedBlock
//{
//
//    [[LoadingView sharedLoadingView] startLoading];
//
////    [MixpanelHelper trackLoginWithType:@"Facebook"];
//
//    if (fbStateChangeError) {
//        [IMGAmplitudeUtil trackSignInWithName:@"SI - Sign In Cancel" andType:@"Facebook" andLocation:nil andReason:nil];
//        [[LoadingView sharedLoadingView] stopLoading];
//        return ;
//    }
//
//    NSString *accessToken = [[FBSession.activeSession accessTokenData] accessToken];
//
//    switch (state) {
//
//        case FBSessionStateOpen:
//        {
//            FBUser *fbUser=[[FBUser alloc]init];
//            NSString *facebookSigninParameter = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@",fbUser.userID,fbUser.email,fbUser.name,fbUser.gender,fbUser.firstName,fbUser.lastName,fbUser.picture,fbUser.birthday,fbUser.updatedTime];
//
//            [FBRequestConnection startWithGraphPath:@"me" parameters:[NSDictionary dictionaryWithObject:facebookSigninParameter forKey:@"fields"] HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
//                [dic setObject:@(200) forKey:@"height"];
//                [dic setObject:@(200) forKey:@"width"];
//                [dic setObject:@(false) forKey:@"redirect"];
//                [dic setObject:@"normal" forKey:@"type"];
//                [FBRequestConnection startWithGraphPath:@"me/picture" parameters:dic HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connectionPicture, id resultPicture, NSError *errorPicture) {
//                    if (!error) {
////
//                        NSString *sessionID = [[NSUserDefaults standardUserDefaults] objectForKey:@"sessionID"] ;
//                        if(!sessionID){
//                            sessionID = @"NOSESSION";
//                        }
//                        parameters = [NSMutableDictionary dictionaryWithCapacity:0];
//                        if ([result objectForKey:fbUser.email]) {
//                            [parameters setObject:[result objectForKey:fbUser.email] forKey:fbUser.email];
//                        }
//                        [parameters setObject:USER_LOGINTYPE_WITHFACEBOOK forKey:@"loginwith"];
//
//                        [LoginParamter sharedParameter].loginwith=USER_LOGINTYPE_WITHFACEBOOK;
//                        [parameters setObject:[result objectForKey:fbUser.firstName] forKey:fbUser.firstName];
//                        [parameters setObject:[result objectForKey:fbUser.lastName] forKey:fbUser.lastName];
//                        if (!errorPicture) {
//                            [parameters setObject:[[resultPicture objectForKey:@"data"] objectForKey:@"url"] forKey:fbUser.picture];
//                        }else{
//                            [parameters setObject:[[[result objectForKey:fbUser.picture] objectForKey:@"data"] objectForKey:@"url"] forKey:fbUser.picture];
//                        }
//                        [parameters setObject:@"" forKey:fbUser.mobileNumber];
//                        [parameters setObject:[result objectForKey:fbUser.updatedTime] forKey:fbUser.updatedTime];
//                        [parameters setObject:sessionID forKey:@"sessionID"];
//                        [parameters setObject:accessToken forKey:@"access_token"];
//                        [parameters setObject:[result objectForKey:fbUser.userID] forKey:@"thirdId"];
//                        [parameters setObject:@"manual" forKey:@"0"];
//
//                        [parameters setObject:@"1" forKey:@"thirdPartyType"];
//                        [parameters setObject:[result objectForKey:fbUser.userID] forKey:@"thirdPartyId"];
//                        [parameters setObject:[result objectForKey:fbUser.firstName] forKey:@"firstName"];
//                        [parameters setObject:[result objectForKey:fbUser.lastName] forKey:@"lastName"];
//
//                        NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:@"Login" action:@"FaceBook" label:[NSString stringWithFormat:@"%@",[result objectForKey:fbUser.name]] value:nil] build];
//                        id tracker = [[GAI sharedInstance] defaultTracker];
//
//                        [tracker set:kGAIScreenName value:@"Login"];
//                        [tracker send:event];
//
//                        [self isSignIn:parameters withBlock:block failedBlock:failedBlock];
//                        //block();
//
//                        //[self requestLogin:parameters delegateWithBlock:block failedBlock:failedBlock];
//
//                    }else{
//                        [[LoadingView sharedLoadingView] stopLoading];
//                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Error: %@",
//                                                                                     [self FBErrorCodeDescription:error.code]]
//                                                                            message:error.localizedDescription
//                                                                           delegate:nil
//                                                                  cancelButtonTitle:@"OK"
//                                                                  otherButtonTitles:nil];
//                        [[Amplitude instance]
//                         logEvent:@"SI - Sign Ip Failed" withEventProperties:@{@"Reason":error.description,@"Sign in type":@"Facebook"}];
//                        [alertView show];
//                        return ;
//                    }
//                }];
//            }];
//        }
//            break;
//        case FBSessionStateClosed: {
//            [FBSession.activeSession closeAndClearTokenInformation];
//        }
//            break;
//        case FBSessionStateClosedLoginFailed: {
//
//        }
//            break;
//
//        default:
//            break;
//    }
//}
-(void)requestLoginWithGoogle:(NSDictionary *)parameterDictionary delegateWithBlock:(void (^)())block failedBlock:(void (^)(NSString *errorMsg))faildBlock{
    
    [self isSignIn:parameterDictionary withBlock:block failedBlock:faildBlock];
    
    
    
    
}

- (void)faceBookSignUp:(NSDictionary *)parameter withBlock:(void (^)())block failedBlock:(void (^)(NSString *errorMsg))failedBlock{
    [[IMGNetWork sharedManager]POST:@"thirdparty/signup" parameters:parameter progress:nil  success:^(NSURLSessionDataTask *operation, id responseObject) {
        
        NSLog(@"faceBookSignUp responseObject = %@",responseObject);
        if ([[parameters objectForKey:@"loginwith"] isEqualToString:@"loginwithgoogle"]) {
            [IMGAmplitudeUtil trackSignInWithName:@"SU - Sign Up Initiate" andType:@"Google" andLocation:@"Onboarding Page" andReason:nil];
            [self requestLoginWithGoogle:parameters delegateWithBlock:block failedBlock:failedBlock];
            
        }else{
            [IMGAmplitudeUtil trackSignInWithName:@"SU - Sign Up Initiate" andType:@"Facebook" andLocation:@"Onboarding Page" andReason:nil];
            [self requestLogin:parameters delegateWithBlock:block failedBlock:failedBlock];
            
        }


        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Error"]
                                                            message:error.localizedDescription
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];

        [alertView show];
        failedBlock(error.localizedDescription);
        [[LoadingView sharedLoadingView] stopLoading];

    }];
}
- (void)isSignIn:(NSDictionary *)parameter withBlock:(void (^)())block failedBlock:(void(^)(NSString *errorMsg))failedBlock
{
    [[LoadingView sharedLoadingView] startLoading];
    [[IMGNetWork sharedManager] POST:@"thirdparty/signin" parameters:parameter progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"responseObject = %@",responseObject);
        if ([responseObject objectForKey:@"exceptionmsg"]) {
            [[LoadingView sharedLoadingView] stopLoading];
            return ;
        }
        if (![responseObject objectForKey:@"status"])
        {
            isSignUp = YES;
            NSString *lastUpdated = [responseObject objectForKey:@"lastUpdated"];
            [self isUpdateWithLastUpdated:lastUpdated];
            //            [self requestLogin:parameter delegateWithBlock:block failedBlock:failedBlock];
#pragma mark - login success
            if([LoginParamter sharedParameter].loginwith==nil){
                [LoginParamter sharedParameter].loginwith = USER_LOGINTYPE_WITHSELF;
            }
            NSString * loginWith = [LoginParamter sharedParameter].loginwith;
            [[NSUserDefaults standardUserDefaults] setObject:loginWith forKey:@"loginwith"];
            
            NSNumber *cityId=[responseObject objectForKey:@"lastCityId"];
            if(cityId!=nil && [cityId intValue]>0){
                [CityUtils getCitiesWithCountryId:0 andBlock:^(NSArray *cities){
                    for(IMGCity *city in cities){
                        if([city.cityId intValue]==[cityId intValue]){
                            [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"lastCityId"] forKey:CITY_SELECT_ID];
                            [[NSUserDefaults standardUserDefaults] setObject:city.name forKey:CITY_SELECT];
                            [[NSNotificationCenter defaultCenter] postNotificationName:CITY_SELECT_NOTIFICATION object:nil];
                            break;
                        }
                    }
                }];
            }else{
                if ([[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT] == nil || [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] == nil) {
                    [[NSUserDefaults standardUserDefaults] setObject:@"Jakarta" forKey:CITY_SELECT];
                    [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:CITY_SELECT_ID];
                }
                cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
                [CityHandler changeCity:cityId withUser:[responseObject objectForKey:@"id"] block:nil];
            }
            NSString *sqlString=@"delete from IMGUser";
            [[DBManager manager] deleteWithSql:sqlString];
            IMGUser * user = [IMGUser currentUser];
            user.islogin = @(YES);
            [user setValuesForKeysWithDictionary:responseObject];
            user.userId = [responseObject objectForKey:@"id"];
            user.fullName = [NSString stringWithFormat:@"%@ %@",user.firstName, user.lastName];
            
            NSNumber * gender=[responseObject objectForKey:@"gender"];
            user.gender=[NSString stringWithFormat:@"%@",gender];
            user.avatar = [responseObject objectForKey:@"avatar"];
            if([user.avatar isKindOfClass:[NSNull class]] || [user.avatar isEqualToString:@"image/data/photo/default.png"]){
                user.avatar =@"data/photo/default.png";
            }
            NSString * birthday =[responseObject objectForKey:@"birthDate"];
            if (birthday&&![birthday isEqual:[NSNull null]]) {
                NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
                [dateFormatter setDateFormat:@"MM/dd/yyyy"];
                NSDate * date = [dateFormatter dateFromString:[responseObject objectForKey:@"birthDate"]];
                user.birthday = [dateFormatter stringFromDate:date];
            }
            user.isNewUser = [responseObject objectForKey:@"isNewUser"];
            user.qravedCount = [responseObject objectForKey:@"qravedCount"];
            user.email = [responseObject objectForKey:@"email"];
            user.userName = [responseObject objectForKey:@"userName"];
            user.token = [responseObject objectForKey:@"token"];
            user.reviewCount = [responseObject objectForKey:@"reviewCount"];
            user.bookingCount = [responseObject objectForKey:@"bookingCount"];
            user.favoriteCount = [responseObject objectForKey:@"favoriteCount"];
            user.dateCreated=[responseObject objectForKey:@"dateCreated"];
            if(user.pointsPerBooking!=nil && [user.pointsPerBooking intValue]>0){
                
            }else{
                user.pointsPerBooking = [NSNumber numberWithInt:100];
            }
            NSString *phone = [responseObject objectForKey:@"mobileNumberWeb"];
            user.phone=nil;
            if(phone!=nil){
                NSRange range=[phone rangeOfString:@"type"];
                if(range.location==NSNotFound){
                    user.phone = phone;
                }
            }
            
            NSDictionary *location=[responseObject objectForKey:@"location"];
            if(location){
                NSDictionary *city=[location objectForKey:@"city"];
                if(city){
                    NSString *cityName = [city objectForKey:@"name"];
                    if(![cityName isKindOfClass:[NSNull class]]){
                        user.cityName=cityName;
                    }
                }
                NSDictionary * district=[location objectForKey:@"district"];
                if(![district isEqual:[NSNull null]]&&district!=nil){
                    user.districtName=[district objectForKey:@"name"];
                }
                NSDictionary * country=[location objectForKey:@"country"];
                if(![country isEqual:[NSNull null]]&&country!=nil){
                    user.countryName=[country objectForKey:@"displayName"];
                }
            }
            [[DBManager manager] insertModel:user update:YES selectKey:@"userId" andSelectValue:user.userId];
            NSUserDefaults *userd = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:user];
            [userd setObject:data forKey:@"person"];
            [userd setObject:user.dateCreated forKey:@"dateCreated"];
            [userd synchronize];
            
            sqlString=@"delete from IMGThirdPartyUser";
            [[DBManager manager] deleteWithSql:sqlString];
            NSArray *thirdPartyUserList = [responseObject objectForKey:@"thirdUserInfoList"];
            if(thirdPartyUserList!=nil){
                for(NSDictionary *thirdPartyUserDictionary in thirdPartyUserList){
                    IMGThirdPartyUser *thirdPartyUser=[[IMGThirdPartyUser alloc]init];
                    [thirdPartyUser setValuesForKeysWithDictionary:thirdPartyUserDictionary];
                    [[DBManager manager] insertModel:thirdPartyUser update:YES selectKey:@"thirdPartyUserId" andSelectValue:thirdPartyUser.thirdPartyUserId];
                }
            }
            
            
            [[LoadingView sharedLoadingView] stopLoading];
            [[NSUserDefaults standardUserDefaults] setObject:user.userId forKey:@"loginUserId"];
            //            [IMGUser registerMixpanelSuperProperties];
            //            [MixpanelHelper trackLoginWithType:@"Email"];
            NSNumber *hasPersonalization = [responseObject objectForKey:@"hasPersonalization"];
            if ([hasPersonalization isEqualToNumber:@(0)]) {
                [AppDelegate ShareApp].isShowPersonalization = YES;
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_TAB_SAVED object:nil];
            block();
        }
        else
        {
            isSignUp = NO;
            block();
            
            
            
            [[LoadingView sharedLoadingView] stopLoading];
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Error"]
                                                            message:error.localizedDescription
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];

        NSString *loginType;
        if ([USER_LOGINTYPE_WITHFACEBOOK isEqualToString:[parameter objectForKey:@"loginwith"]]) {
            loginType = @"Facebook";
        }
        
        if ([USER_LOGINTYPE_WITHTWITTER isEqualToString:[parameter objectForKey:@"loginwith"]]) {
            loginType = @"Email";
        }
        if ([USER_LOGINTYPE_WITHGoogle isEqualToString:[parameter objectForKey:@"loginwith"]]) {
            loginType = @"Google";
        }
        
        [IMGAmplitudeUtil trackSignInWithName:@"SI - Sign In Failed" andType:loginType andLocation:nil andReason:error.localizedDescription];

        [[LoadingView sharedLoadingView] stopLoading];

    }];
}
- (void)isUpdateWithLastUpdated:(NSString *)lastUpdated
{
    NSArray *lastTimeArr = [lastUpdated componentsSeparatedByString:@"T"];
    
    NSArray *yymmddArr = [[lastTimeArr objectAtIndex:0] componentsSeparatedByString:@"-"];
    NSString *hhStr = [[[lastTimeArr objectAtIndex:1] componentsSeparatedByString:@":"] objectAtIndex:0];
    
    if ([[yymmddArr objectAtIndex:0] intValue] < 2015)
    {
        isUpdate = YES;
    }else if ([[yymmddArr objectAtIndex:0] intValue] == 2015)
    {
        if ([[yymmddArr objectAtIndex:1] intValue] < 4)
        {
            isUpdate = YES;
        }else if ([[yymmddArr objectAtIndex:1] intValue] == 4)
        {
            if ([[yymmddArr objectAtIndex:2] intValue] < 15)
            {
                isUpdate = YES;
            }else if ([[yymmddArr objectAtIndex:2] intValue] == 15)
            {
                if ([hhStr intValue]<15)
                {
                    isUpdate = YES;
                }
            }
            else
                isUpdate = NO;
        }else
            isUpdate = NO;
    }else
        isUpdate = NO;

}
-(void)googleLogin:(GIDGoogleUser*)user error:(NSError*)error delegateWithBlock:(void (^)())block failedBlock:(void(^)())failedBlock{
       parameters=[[NSMutableDictionary alloc]init];
    
    if (user!=nil) {
        [parameters setObject:user.authentication.accessToken forKey:@"access_token"];
        [parameters setObject:@"3" forKey:@"thirdPartyType"];
        [parameters setObject:user.userID forKey:@"thirdPartyId"];
        [parameters setObject:user.profile.givenName forKey:@"first_name"];
        [parameters setObject:user.profile.familyName forKey:@"last_name"];
        [parameters setObject:user.profile.email forKey:@"email"];
        [parameters setObject:USER_LOGINTYPE_WITHGoogle forKey:@"loginwith"];
        [LoginParamter sharedParameter].loginwith=USER_LOGINTYPE_WITHGoogle;
        
        [self isSignIn:parameters withBlock:block failedBlock:failedBlock];
   }
}

- (void)updateUserInfo:(NSDictionary *)parameter withBlock:(void (^)())block failedBlock:(void (^)())failedBlock
{
    [[IMGNetWork sharedManager]GET:@"profile/update" parameters:parameter progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        
        NSLog(@"responseObject = %@",responseObject);
        
        if ([[responseObject objectForKey:@"status"] isEqualToString:@"succeed"])
        {
            isUpdate = NO;
            IMGUser *user = [IMGUser currentUser];
            user.email = [parameter objectForKey:@"email"];
            user.firstName = [parameter objectForKey:@"first_name"];
            user.lastName = [parameter objectForKey:@"last_name"];
            user.birthday = [parameter objectForKey:@"birthday"];
            [[DBManager manager] insertModel:user update:YES selectKey:@"userId" andSelectValue:user.userId];
            NSUserDefaults *userd = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:user];
            [userd setObject:data forKey:@"person"];
            [userd synchronize];
            block();
        }
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Error"]
                                                            message:error.localizedDescription
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
        
        [[LoadingView sharedLoadingView] stopLoading];
        
    }];
}

-(void)faceBookConnect:(NSArray *)permissions delegateWithBlock:(void (^)())block failedBlock:(void(^)())failedBlock{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: permissions
     fromViewController:nil
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         NSLog(@"facebook login result.grantedPermissions = %@,error = %@",result.grantedPermissions,error);
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             NSLog(@"Logged in");
             if ([result.grantedPermissions containsObject:@"public_profile"]) {
                 if ([FBSDKAccessToken currentAccessToken]) {
                     NSString *facebookSigninParameter = [NSString stringWithFormat:@"id,email,name,gender,first_name,last_name,picture,birthday,updated_time"];
                     [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:[NSDictionary dictionaryWithObject:facebookSigninParameter forKey:@"fields"]] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                         if (!error) {
                             NSString *sessionID = [[NSUserDefaults standardUserDefaults] objectForKey:@"sessionID"] ;
                             if(!sessionID){
                                 sessionID = @"NOSESSION";
                             }
                             parameters = [NSMutableDictionary dictionaryWithCapacity:0];
                             if ([result objectForKey:@"email"]) {
                                 [parameters setObject:[result objectForKey:@"email"] forKey:@"email"];
                             }
                             [parameters setObject:USER_LOGINTYPE_WITHFACEBOOK forKey:@"loginwith"];
                             
                             [LoginParamter sharedParameter].loginwith=USER_LOGINTYPE_WITHFACEBOOK;
                             [parameters setObject:[result objectForKey:@"first_name"] forKey:@"first_name"];
                             [parameters setObject:[result objectForKey:@"last_name"] forKey:@"last_name"];

                             if ([result objectForKey:@"picture"]) {
                                 if ([[result objectForKey:@"picture"] objectForKey:@"data"]) {
                                     [parameters setObject:[[[result objectForKey:@"picture"] objectForKey:@"data"] objectForKey:@"url"] forKey:@"picture"];
                                 }
                             }
                             [parameters setObject:[result objectForKey:@"updated_time"] forKey:@"updated_time"];
                             [parameters setObject:sessionID forKey:@"sessionID"];
//                            [parameters setObject:accessToken forKey:@"access_token"];
                            [parameters setObject:[result objectForKey:@"id"] forKey:@"thirdId"];
                             [parameters setObject:@"manual" forKey:@"0"];
                             
                             [parameters setObject:@"1" forKey:@"thirdPartyType"];
                             [parameters setObject:[result objectForKey:@"id"] forKey:@"thirdPartyId"];
                             
                             IMGUser *user=[IMGUser currentUser];
                             [parameters setObject:user.userId forKey:@"userID"];
                             [parameters setObject:user.token forKey:@"t"];
                             
                             NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:@"Login" action:@"FaceBook" label:[NSString stringWithFormat:@"%@",[result objectForKey:@"name"]] value:nil] build];
                             id tracker = [[GAI sharedInstance] defaultTracker];
                             
                             [tracker set:kGAIScreenName value:@"Login"];
                             [tracker send:event];
                             
                             [self requestConnect:parameters delegateWithBlock:block failedBlock:failedBlock];
                         }else{
                             [[LoadingView sharedLoadingView] stopLoading];
                             UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Error: %ld",error.code]
                                                                                 message:error.localizedDescription
                                                                                delegate:nil
                                                                       cancelButtonTitle:@"OK"
                                                                       otherButtonTitles:nil];
                             [alertView show];
                             return ;

                         }
                     }];
                 }
             }
         }
     }];
}

//-(void)faceBookConnect:(FBSession *)session state:(FBSessionState)state error:(NSError *)fbStateChangeError delegateWithBlock:(void (^)())block failedBlock:(void(^)())failedBlock
//{
//    
//    [[LoadingView sharedLoadingView] startLoading];
//    
////    [MixpanelHelper trackLoginWithType:@"Facebook"];
//    
//    if(fbStateChangeError){
//        [[LoadingView sharedLoadingView] stopLoading];
//        return ;
//    }
//    
//    NSString *accessToken = [[FBSession.activeSession accessTokenData] accessToken];
//    switch (state) {
//        case FBSessionStateOpen:
//        {
//            FBUser *fbUser=[[FBUser alloc]init];
//            NSString *facebookSigninParameter = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@,%@,%@",fbUser.userID,fbUser.email,fbUser.name,fbUser.gender,fbUser.firstName,fbUser.lastName,fbUser.picture,fbUser.birthday,fbUser.updatedTime];
//            
//            [FBRequestConnection startWithGraphPath:@"me" parameters:[NSDictionary dictionaryWithObject:facebookSigninParameter forKey:@"fields"] HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                
//                if (!error) {
//                    NSString *sessionID = [[NSUserDefaults standardUserDefaults] objectForKey:@"sessionID"] ;
//                    if(!sessionID){
//                        sessionID = @"NOSESSION";
//                    }
//                    NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithCapacity:0];
//                    if ([result objectForKey:fbUser.email]) {
//                        [parameters setObject:[result objectForKey:fbUser.email] forKey:fbUser.email];
//                    }
//                    [parameters setObject:USER_LOGINTYPE_WITHFACEBOOK forKey:@"loginwith"];
//                    
//                    [LoginParamter sharedParameter].loginwith=USER_LOGINTYPE_WITHFACEBOOK;
//                    [parameters setObject:[result objectForKey:fbUser.firstName] forKey:fbUser.firstName];
//                    [parameters setObject:[result objectForKey:fbUser.lastName] forKey:fbUser.lastName];
//                    [parameters setObject:[[[result objectForKey:fbUser.picture] objectForKey:@"data"] objectForKey:@"url"] forKey:fbUser.picture];
//                    [parameters setObject:@"" forKey:fbUser.mobileNumber];
//                    [parameters setObject:[result objectForKey:fbUser.updatedTime] forKey:fbUser.updatedTime];
//                    [parameters setObject:sessionID forKey:@"sessionID"];
//                    [parameters setObject:accessToken forKey:@"access_token"];
//                    [parameters setObject:[result objectForKey:fbUser.userID] forKey:@"thirdId"];
//                    [parameters setObject:@"manual" forKey:@"0"];
//                    IMGUser *user=[IMGUser currentUser];
//                    [parameters setObject:user.userId forKey:@"userID"];
//                    [parameters setObject:user.token forKey:@"t"];
//                    
//                    NSMutableDictionary *event = [[GAIDictionaryBuilder createEventWithCategory:@"Login" action:@"FaceBook" label:[NSString stringWithFormat:@"%@",[result objectForKey:fbUser.name]] value:nil] build];
//                    id tracker = [[GAI sharedInstance] defaultTracker];
//                    [tracker set:kGAIScreenName value:@"Login"];
//                    [tracker send:event];
//                    
//                    [self requestConnect:parameters delegateWithBlock:block failedBlock:failedBlock];
//                }else{
//                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Error: %@",
//                                                                                 [self FBErrorCodeDescription:error.code]]
//                                                                        message:error.localizedDescription
//                                                                       delegate:nil
//                                                              cancelButtonTitle:@"OK"
//                                                              otherButtonTitles:nil];
//                    [alertView show];
//                    return ;
//                }
//                
//            }];
//        }
//            break;
//        case FBSessionStateClosed: {
//            [FBSession.activeSession closeAndClearTokenInformation];
//        }
//            break;
//        case FBSessionStateClosedLoginFailed: {
//            
//        }
//            break;
//            
//        default:
//            break;
//    }
//}

- (void)requestConnect:(NSDictionary *)parameterDictionary delegateWithBlock:(void (^)())block failedBlock:(void(^)())faildBlock {
    [[LoadingView sharedLoadingView] startLoading];
    [[IMGNetWork sharedManager]POST:@"thirduser/connect" parameters:parameterDictionary progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSAssert([responseObject isKindOfClass:[NSDictionary class]],@"MUST DICTIONARY");
        NSString *exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
        if(exceptionMsg){
            //登陆错误
            if([exceptionMsg isEqualToString:@"signup failed, need to provide the email address"])
            {
                [LoginParamter sharedParameter].paramters=[NSMutableDictionary dictionaryWithDictionary:parameterDictionary];
                faildBlock();
            }else{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: [NSString stringWithFormat:@"Error"]
                                                                    message: exceptionMsg
                                                                   delegate: nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                [alertView show];
            }
            
            [[LoadingView sharedLoadingView] stopLoading];
            return ;
        }
        
#pragma mark - login success
        if([LoginParamter sharedParameter].loginwith==nil){
            [LoginParamter sharedParameter].loginwith = USER_LOGINTYPE_WITHSELF;
        }
        NSString * loginWith = [LoginParamter sharedParameter].loginwith;
        [[NSUserDefaults standardUserDefaults] setObject:loginWith forKey:@"loginwith"];
        
        IMGUser * user = [IMGUser currentUser];
        user.islogin = @(YES);
        [user setValuesForKeysWithDictionary:responseObject];
        user.userId = [responseObject objectForKey:@"id"];
        user.fullName = [NSString stringWithFormat:@"%@ %@",user.firstName, user.lastName];
        
        NSNumber * gender=[responseObject objectForKey:@"gender"];
        user.gender=[NSString stringWithFormat:@"%@",gender];
        user.avatar = [responseObject objectForKey:@"avatar"];
        if([user.avatar isKindOfClass:[NSNull class]] || [user.avatar isEqualToString:@"image/data/photo/default.png"]){
            user.avatar =@"data/photo/default.png";
        }
        NSString * birthday =[responseObject objectForKey:@"birthDate"];
        if (birthday&&![birthday isEqual:[NSNull null]]) {
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"MM/dd/yyyy"];
            NSDate * date = [dateFormatter dateFromString:[responseObject objectForKey:@"birthDate"]];
            user.birthday = [dateFormatter stringFromDate:date];
        }
        user.isNewUser = [responseObject objectForKey:@"isNewUser"];
        user.qravedCount = [responseObject objectForKey:@"qravedCount"];
        user.email = [responseObject objectForKey:@"email"];
        user.userName = [responseObject objectForKey:@"userName"];
        user.token = [responseObject objectForKey:@"token"];
        NSString *phone = [responseObject objectForKey:@"mobileNumberWeb"];
        user.phone=nil;
        if(phone!=nil){
            NSRange range=[phone rangeOfString:@"type"];
            if(range.location==NSNotFound){
                user.phone = phone;
            }
        }
        
        NSDictionary *location=[responseObject objectForKey:@"location"];
        if(location){
            NSDictionary *city=[location objectForKey:@"city"];
            if(city){
                NSString *cityName = [city objectForKey:@"name"];
                if(![cityName isKindOfClass:[NSNull class]]){
                    user.cityName=cityName;
                }
            }
            NSDictionary * district=[location objectForKey:@"district"];
            if(![district isEqual:[NSNull null]]&&district!=nil){
                user.districtName=[district objectForKey:@"name"];
            }
            NSDictionary * country=[location objectForKey:@"country"];
            if(![country isEqual:[NSNull null]]&&country!=nil){
                user.countryName=[district objectForKey:@"displayName"];
            }
        }
        
//        if (user.isNewUser) {
//            [MixpanelHelper trackSignUpWithTab:[NSString stringWithFormat:@"%@",loginWith]];
//        }
        
        
        NSString *sqlString=[NSString stringWithFormat:@"delete from IMGThirdPartyUser where userId='%@'",user.userId];
        [[DBManager manager] deleteWithSql:sqlString];
        NSArray *thirdPartyUserList = [responseObject objectForKey:@"thirdUserInfoList"];
        if(thirdPartyUserList!=nil){
            for(NSDictionary *thirdPartyUserDictionary in thirdPartyUserList){
                IMGThirdPartyUser *thirdPartyUser=[[IMGThirdPartyUser alloc]init];
                [thirdPartyUser setValuesForKeysWithDictionary:thirdPartyUserDictionary];
                [[DBManager manager] insertModel:thirdPartyUser update:YES selectKey:@"thirdPartyUserId" andSelectValue:thirdPartyUser.thirdPartyUserId];
            }
        }
        [[DBManager manager] insertModel:user update:YES selectKey:@"userId" andSelectValue:user.userId];
        NSUserDefaults *userd = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:user];
        [userd setObject:data forKey:@"person"];
        [userd synchronize];
        [[LoadingView sharedLoadingView] stopLoading];
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"userLogined"];
        [[NSUserDefaults standardUserDefaults] setObject:user.userId forKey:@"loginUserId"];
        [AppDelegate ShareApp].userLogined=YES;
        
//        [IMGUser registerMixpanelSuperProperties];
        
        block();
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"LoginClient.loginWithBlock.error %@",error.localizedDescription);
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Error"]
                                                            message:error.localizedDescription
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
        
        [[LoadingView sharedLoadingView] stopLoading];
        
        
    }];
}


- (void) requestLogin:(NSDictionary *)parameterDictionary delegateWithBlock:(void (^)())block failedBlock:(void(^)(NSString *errorMsg))faildBlock {
    [[LoadingView sharedLoadingView] startLoading];
    [[IMGNetWork sharedManager]POST:@"login/encrypted" parameters:parameterDictionary progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        
        NSLog(@"responseObject = %@",responseObject);
        NSAssert([responseObject isKindOfClass:[NSDictionary class]],@"MUST DICTIONARY");
        NSString *exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
        NSString *lastUpdated = [responseObject objectForKey:@"lastUpdated"];
        [self isUpdateWithLastUpdated:lastUpdated];
        if(exceptionMsg){
            
            if ([@"self" isEqualToString:[parameterDictionary objectForKey:@"loginwith"]]) {
                [IMGAmplitudeUtil trackSignInWithName:@"SI - Sign In Failed" andType:@"Email" andLocation:nil andReason:exceptionMsg];
            }
            
            if ([@"signupwithself" isEqualToString:[parameterDictionary objectForKey:@"loginwith"]]) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:@"Email" forKey:@"Sign Up Type"];
                if ([[NSString stringWithFormat:@"User with email '%@' exist",[parameterDictionary objectForKey:@"email"]] isEqualToString:exceptionMsg]) {
                    [eventProperties setValue:@"Account already registered" forKey:@"Reason"];
                }else{
                    [eventProperties setValue:@"Form validation" forKey:@"Reason"];
                }
                
                [[Amplitude instance] logEvent:@"SU - Sign Up Failed" withEventProperties:eventProperties];
            }
            
            if([exceptionMsg isEqualToString:@"signup failed, need to provide the email address"]){
                [LoginParamter sharedParameter].paramters=[NSMutableDictionary dictionaryWithDictionary:parameterDictionary];
                faildBlock(exceptionMsg);
            }else if ([exceptionMsg isEqualToString:@"Invalid email"]){
                faildBlock(exceptionMsg);
            }else if ([exceptionMsg isEqualToString:@"Invalid password"]){
                faildBlock(exceptionMsg);
            }else if ([exceptionMsg isEqualToString:[NSString stringWithFormat:@"User with email '%@' exist",[parameterDictionary objectForKey:@"email"]]]){
                faildBlock(exceptionMsg);
            }
            else{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: [NSString stringWithFormat:@"Error"]
                                                                    message: exceptionMsg
                                                                   delegate: nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                [alertView show];
                faildBlock(exceptionMsg);
            }

            [[LoadingView sharedLoadingView] stopLoading];
            return ;
        }

#pragma mark - login success
        if([LoginParamter sharedParameter].loginwith==nil){
            [LoginParamter sharedParameter].loginwith = USER_LOGINTYPE_WITHSELF;
        }
        NSString * loginWith = [LoginParamter sharedParameter].loginwith;
        [[NSUserDefaults standardUserDefaults] setObject:loginWith forKey:@"loginwith"];
        
        NSNumber *cityId=[responseObject objectForKey:@"lastCityId"];
        if(cityId!=nil && [cityId intValue]>0){
            [CityUtils getCitiesWithCountryId:0 andBlock:^(NSArray *cities){
                for(IMGCity *city in cities){
                    if([city.cityId intValue]==[cityId intValue]){
                        [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"lastCityId"] forKey:CITY_SELECT_ID];
                        [[NSUserDefaults standardUserDefaults] setObject:city.name forKey:CITY_SELECT];            
                        [[NSNotificationCenter defaultCenter] postNotificationName:CITY_SELECT_NOTIFICATION object:nil];
                        break;
                    }
                }
            }];
        }else{
            if ([[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT] == nil || [[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] == nil) {
                [[NSUserDefaults standardUserDefaults] setObject:@"Jakarta" forKey:CITY_SELECT];
                [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:CITY_SELECT_ID];
            }
            cityId=[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID];
            [CityHandler changeCity:cityId withUser:[responseObject objectForKey:@"id"] block:nil];
        }
        NSString *sqlString=@"delete from IMGUser";
        [[DBManager manager] deleteWithSql:sqlString];
        IMGUser * user = [IMGUser currentUser];
        user.islogin = @(YES);
        [user setValuesForKeysWithDictionary:responseObject];
        user.userId = [responseObject objectForKey:@"id"];
        user.fullName = [NSString stringWithFormat:@"%@ %@",user.firstName, user.lastName];

        NSNumber * gender=[responseObject objectForKey:@"gender"];
        user.gender=[NSString stringWithFormat:@"%@",gender];
        user.avatar = [responseObject objectForKey:@"avatar"];
        if([user.avatar isKindOfClass:[NSNull class]] || [user.avatar isEqualToString:@"image/data/photo/default.png"]){
            user.avatar =@"data/photo/default.png";
        }
        NSString * birthday =[responseObject objectForKey:@"birthDate"];
        if (birthday&&![birthday isEqual:[NSNull null]]) {
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
            [dateFormatter setDateFormat:@"MM/dd/yyyy"];
            NSDate * date = [dateFormatter dateFromString:[responseObject objectForKey:@"birthDate"]];
            user.birthday = [dateFormatter stringFromDate:date];
        }
        user.isNewUser = [responseObject objectForKey:@"isNewUser"];
        user.qravedCount = [responseObject objectForKey:@"qravedCount"];
        user.email = [responseObject objectForKey:@"email"];
        user.userName = [responseObject objectForKey:@"userName"];
        user.token = [responseObject objectForKey:@"token"];
        user.reviewCount = [responseObject objectForKey:@"reviewCount"];
        user.bookingCount = [responseObject objectForKey:@"bookingCount"];
        user.favoriteCount = [responseObject objectForKey:@"favoriteCount"];
        if(user.pointsPerBooking!=nil && [user.pointsPerBooking intValue]>0){
            
        }else{
            user.pointsPerBooking = [NSNumber numberWithInt:100];
        }
        NSString *phone = [responseObject objectForKey:@"mobileNumberWeb"];
        user.phone=nil;
        if(phone!=nil){
            NSRange range=[phone rangeOfString:@"type"];
            if(range.location==NSNotFound){
                user.phone = phone;
            }
        }

        NSDictionary *location=[responseObject objectForKey:@"location"];
        if(location){
            NSDictionary *city=[location objectForKey:@"city"];
            if(city){
                NSString *cityName = [city objectForKey:@"name"];
                if(![cityName isKindOfClass:[NSNull class]]){
                    user.cityName=cityName;
                }
            }
            NSDictionary * district=[location objectForKey:@"district"];
            if(![district isEqual:[NSNull null]]&&district!=nil){
                user.districtName=[district objectForKey:@"name"];
            }
            NSDictionary * country=[location objectForKey:@"country"];
            if(![country isEqual:[NSNull null]]&&country!=nil){
                user.countryName=[country objectForKey:@"displayName"];
            }
        }
        user.moderateReviewId = [NSNumber numberWithFloat:0];
        [[DBManager manager] insertModel:user update:YES selectKey:@"userId" andSelectValue:user.userId];
        NSUserDefaults *userd = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:user];
        [userd setObject:data forKey:@"person"];
        [userd setObject:user.dateCreated forKey:@"dateCreated"];
        [userd synchronize];
        if (![AppDelegate ShareApp].isAlreadySetAmplitudeUserProperties) {
            [[Amplitude instance] setUserId:[NSString stringWithFormat:@"%@",user.userId]];
            
            NSMutableDictionary *userProperties = [NSMutableDictionary dictionary];
            [userProperties setValue:user.email forKey:@"email"];
            [userProperties setValue:user.firstName forKey:@"firstName"];
            [userProperties setValue:user.gender forKey:@"gender"];
            [userProperties setValue:user.lastName forKey:@"lastName"];
            [[Amplitude instance] setUserProperties:userProperties];
            
            [AppDelegate ShareApp].isAlreadySetAmplitudeUserProperties = YES;
        }
        
        sqlString=@"delete from IMGThirdPartyUser";
        [[DBManager manager] deleteWithSql:sqlString];
        NSArray *thirdPartyUserList = [responseObject objectForKey:@"thirdUserInfoList"];
        if(thirdPartyUserList!=nil){
            for(NSDictionary *thirdPartyUserDictionary in thirdPartyUserList){
                IMGThirdPartyUser *thirdPartyUser=[[IMGThirdPartyUser alloc]init];
                [thirdPartyUser setValuesForKeysWithDictionary:thirdPartyUserDictionary];
                [[DBManager manager] insertModel:thirdPartyUser update:YES selectKey:@"thirdPartyUserId" andSelectValue:thirdPartyUser.thirdPartyUserId];
            }
        }
        

        [[LoadingView sharedLoadingView] stopLoading];
        [[NSUserDefaults standardUserDefaults] setObject:user.userId forKey:@"loginUserId"];
//        [IMGUser registerMixpanelSuperProperties];
//        [MixpanelHelper trackLoginWithType:@"Email"];
        NSNumber *hasPersonalization = [responseObject objectForKey:@"hasPersonalization"];
        if ([hasPersonalization isEqualToNumber:@(0)]) {
            [AppDelegate ShareApp].isShowPersonalization = YES;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_TAB_SAVED object:nil];
        block();
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"LoginClient.loginWithBlock.error %@",error.localizedDescription);
        
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:error forKey:@"LOGINERROR"];
        [[Amplitude instance] logEvent:@"Sign Up - Sign Up Failed" withEventProperties:eventProperties];

        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[NSString stringWithFormat:@"Error"]
                                                            message:error.localizedDescription
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];

        [[LoadingView sharedLoadingView] stopLoading];
        faildBlock(error.localizedDescription);

    }];
}

//- (NSString *)FBErrorCodeDescription:(FBErrorCode) code {
//    switch(code){
//        case FBErrorInvalid :{
//            return @"FBErrorInvalid";
//        }
//        case FBErrorOperationCancelled:{
//            return @"FBErrorOperationCancelled";
//        }
//        case FBErrorLoginFailedOrCancelled:{
//            return @"FBErrorLoginFailedOrCancelled";
//        }
//        case FBErrorRequestConnectionApi:{
//            return @"FBErrorRequestConnectionApi";
//        }case FBErrorProtocolMismatch:{
//            return @"FBErrorProtocolMismatch";
//        }
//        case FBErrorHTTPError:{
//            return @"FBErrorHTTPError";
//        }
//        case FBErrorNonTextMimeTypeReturned:{
//            return @"FBErrorNonTextMimeTypeReturned";
//        }
//        case FBErrorDialog:{
//            return @"FBErrorDialog";
//        }
//        default:
//            return @"[Unknown]";
//    }
//}

- (void)emailLogin :(NSDictionary *)param withError:(NSError *)error withDelegateBlock: (void (^)())block  failBlock:(void (^)(NSString *errorMsg))Failblock{

//    [self requestLogin:param delegateWithBlock:block failedBlock:^(NSString *errorMsg){
//        
//    }];
    [self requestLogin:param delegateWithBlock:^{
        
        block();
    } failedBlock:^(NSString *errorMsg) {
        Failblock(errorMsg);
        
    }];
    
}


+(void)loginFirst:(NSDictionary *)parameters andSuccessBlock:(void (^)(id responseObjectLocal))successBlock andFailedBlock:(void(^)(NSString *errorMsg)) failedBlock{

//    [[IMGNetWork sharedManager] GET:@"/user/personalization/viewed/v2" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        
//        successBlock(responseObject);
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        failedBlock(error);
//        
//    }];
    [[IMGNetWork sharedManager] POST:@"user/personalization/viewed/v2" parameters:parameters progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        successBlock(responseObject);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         failedBlock(error);
        
    }];

}

+(void)logout{
    IMGUser *user=[IMGUser currentUser];
    NSString *deviceToken=[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    if(user==nil || !user.userId || !deviceToken){
        return;
    }
    NSDictionary *params=@{@"userId":user.userId,@"deviceToken":deviceToken};
    [[IMGNetWork sharedManager] GET:@"app/push/unsetToken" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        if([[responseObject objectForKey:@"status"] isEqualToString:@"succeed"]){

        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {

    }];
}

+(void)personalizationOptionFetch:(NSDictionary *)parameters andSuccessBlock:(void (^)(id responseObjectLocal))successBlock andFailedBlock:(void(^)(NSString *errorMsg)) failedBlock{
    [[IMGNetWork sharedManager] POST:@"app/personalization/option/fetch" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
        if(responseObject && errorMessage){
            failedBlock(errorMessage);
        }else if(responseObject){
            successBlock(responseObject);
        }
    }failure:^(NSURLSessionDataTask *operation, NSError *error){
        failedBlock(error.localizedDescription);
    }];
}

+(void)userPersonalizationAdd:(NSDictionary *)parameters andSuccessBlock:(void (^)(id responseObjectLocal))successBlock andFailedBlock:(void(^)(NSString *errorMsg)) failedBlock{
    [[IMGNetWork sharedManager] POST:@"user/personalization/add" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
        if(responseObject && errorMessage){
            failedBlock(errorMessage);
        }else if(responseObject){
            successBlock(responseObject);
        }
    }failure:^(NSURLSessionDataTask *operation, NSError *error){
        failedBlock(error.localizedDescription);
    }];
}

+(void)preferredLocationAdd:(NSDictionary *)parameters andSuccessBlock:(void (^)(id responseObjectLocal))successBlock andFailedBlock:(void(^)(NSString *errorMsg)) failedBlock{
    //user/personalization/districtlandmark
    [[IMGNetWork sharedManager] POST:@"user/personalization/districtlandmark/v2" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
        if(responseObject && errorMessage){
            failedBlock(errorMessage);
        }else if(responseObject){
            successBlock(responseObject);
        }
    }failure:^(NSURLSessionDataTask *operation, NSError *error){
        failedBlock(error.localizedDescription);
    }];

}

+(void)deeplink:(NSDictionary *)parameters andSuccessBlock:(void (^)(id responseObject))successBlock andFailedBlock:(void(^)()) failedBlock{
    AMPDeviceInfo *info  = [[AMPDeviceInfo alloc] init];
    IMGUser *user = [IMGUser currentUser];
    NSLog(@"%@--%@--%@--%@--%@--%@",info.model,info.appVersion,info.osName,info.osVersion,info.vendorID,[info generateUUID]);
    NSMutableDictionary *paramDic =[NSMutableDictionary dictionaryWithDictionary:parameters];
    [paramDic setObject:@"BhaxC05eXMSwQ4HK" forKey:@"apiKey"];
    [paramDic setObject:@"ios" forKey:@"os"];
    [paramDic setObject:@"0" forKey:@"osVersionCode"];
    [paramDic setObject:info.osVersion forKey:@"osVersionName"];
    [paramDic setObject:QRAVED_VERSION forKey:@"appVersionCode"];
    [paramDic setObject:QRAVED_WEB_SERVICE_VERSION forKey:@"appVersionName"];
    [paramDic setObject:[info generateUUID] forKey:@"deviceId"];
    if (user.userId != nil) {
        [paramDic setObject:user.userId forKey:@"userId"];
    }
    [paramDic setObject:[self getIpAddress] forKey:@"ipAddress"];
    [paramDic setObject:[UIDevice currentDevice].model forKey:@"deviceModel"];
    
    [[IMGNetWork sharedManager] GET:@"id/reco/v1/app/referrer" parameters:paramDic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (responseObject != nil) {
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                if ([responseObject objectForKey:@"deepLink"] != nil) {
                    successBlock([responseObject objectForKey:@"deepLink"]);
                    [[NSUserDefaults standardUserDefaults] setObject:[responseObject objectForKey:@"deepLink"] forKey:@"deepLinkUrl"];
                }else{
                    successBlock(nil);
                }
                
            }
        }else{
            successBlock(nil);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        failedBlock();
    }];
}

+ (NSString *)getDeviceIPIpAddresses

{
    
    int sockfd =socket(AF_INET,SOCK_DGRAM, 0);
    
    //    if (sockfd <</span> 0) return nil;
    
    NSMutableArray *ips = [NSMutableArray array];
    
    
    
    int BUFFERSIZE =4096;
    
    struct ifconf ifc;
    
    char buffer[BUFFERSIZE], *ptr, lastname[IFNAMSIZ], *cptr;
    
    struct ifreq *ifr, ifrcopy;
    
    ifc.ifc_len = BUFFERSIZE;
    
    ifc.ifc_buf = buffer;
    
    if (ioctl(sockfd,SIOCGIFCONF, &ifc) >= 0){
        
        for (ptr = buffer; ptr < buffer + ifc.ifc_len; ){
            
            ifr = (struct ifreq *)ptr;
            
            int len =sizeof(struct sockaddr);
            
            if (ifr->ifr_addr.sa_len > len) {
                
                len = ifr->ifr_addr.sa_len;
                
            }
            
            ptr += sizeof(ifr->ifr_name) + len;
            
            if (ifr->ifr_addr.sa_family !=AF_INET) continue;
            
            if ((cptr = (char *)strchr(ifr->ifr_name,':')) != NULL) *cptr =0;
            
            if (strncmp(lastname, ifr->ifr_name,IFNAMSIZ) == 0)continue;
            
            memcpy(lastname, ifr->ifr_name,IFNAMSIZ);
            
            ifrcopy = *ifr;
            
            ioctl(sockfd,SIOCGIFFLAGS, &ifrcopy);
            
            if ((ifrcopy.ifr_flags &IFF_UP) == 0)continue;
            
            
            
            NSString *ip = [NSString stringWithFormat:@"%s",inet_ntoa(((struct sockaddr_in *)&ifr->ifr_addr)->sin_addr)];
            
            [ips addObject:ip];
            
        }
        
    }
    
    close(sockfd);

    NSString *deviceIP =@"";
    
    for (int i=0; i < ips.count; i++)
        
    {
        
        if (ips.count >0)
            
        {
            
            deviceIP = [NSString stringWithFormat:@"%@",ips.lastObject];
            
        }
        
    }
    
    NSLog(@"deviceIP========%@",deviceIP);
    return deviceIP;
}

+ (NSString *)getIpAddress{
    NSURL *ipURL = [NSURL URLWithString:@"http://ip.taobao.com/service/getIpInfo.php?ip=myip"];
    NSData *data = [NSData dataWithContentsOfURL:ipURL];
    if (data) {
        NSDictionary *ipDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
        NSString *ipStr = nil;
        if (ipDic && [[ipDic objectForKey:@"code"] integerValue] == 0) {
            ipStr = [[ipDic objectForKey:@"data"] objectForKey:@"ip"];
        }
        return ipStr?ipStr:@"";
    }
    return @"";
}




@end
