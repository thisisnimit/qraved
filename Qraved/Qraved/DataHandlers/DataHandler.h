//
//  DataHandler.h
//  Qraved
//
//  Created by Jeff on 10/8/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoadingView.h"

@interface DataHandler : NSObject
{
    NSMutableArray *dataArray;
}

-(void)initFromServer;
-(void)getDatasFromServer;

@end
