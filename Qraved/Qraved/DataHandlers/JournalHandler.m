//
//  JournalHandler.m
//  Qraved
//
//  Created by Lucky on 15/6/29.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//


#define RESTAURANT_TAG @"0"
#define TITLE_LABEL_TAG @"1"
#define PHOTO_IMAGE_TAG @"2"
#define CONTENT_LABEL_TAG @"3"
#define BUTTON_TAG @"4"


#import "JournalHandler.h"
#import "DBManager.h"
#import "IMGMediaComment.h"
#import "IMGJournalRestaurant.h"
#import "IMGRestaurant.h"
#import "IMGUser.h"
#import "IMGJournalDetail.h"
#import "IMGJournalCategory.h"
#import "IMGJournalComment.h"
#import "LoadingView.h"
#import "Amplitude.h"
#import "IMGNetWork.h"
@implementation JournalHandler

+ (void)getJournalDetailWithJournalArticleId:(NSNumber *)journalId andBlock:(void (^)(NSString *content,NSString *webSite))block;
{
    NSDictionary * parameters = @{@"articleId":journalId};
    [[IMGNetWork sharedManager]POST:@"journalarticle" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSDictionary *articleDic = [responseObject objectForKey:@"article"];
         block([articleDic objectForKey:@"content"],[articleDic objectForKey:@"link"]);
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"journalList error: %@",error.localizedDescription);
     }];
}
+ (void)journalLikeWithJournal:(IMGMediaComment *)journal andUser:(IMGUser *)user
{
    NSDictionary * parameters = @{@"articleId":journal.mediaCommentId,@"userID":user.userId,@"t":user.token,@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]};
    [[IMGNetWork sharedManager]POST:@"journalarticle/like" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSLog(@"journalLike succeed");
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"journalarticle/like error: %@",error.localizedDescription);
     }];
    
}
+ (void)journalLikeWithJournal:(IMGMediaComment *)journal andUser:(IMGUser *)user completion:(void(^)(BOOL status,NSString *message))block
{
    NSDictionary * parameters = @{@"articleId":journal.mediaCommentId,@"userID":user.userId,@"t":user.token,@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]};
    [[IMGNetWork sharedManager]POST:@"journalarticle/like" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         if(responseObject && [[responseObject objectForKey:@"status"] isEqualToString:@"succeed"]){
            block(YES,@"");
         }else{
            block(NO,@"");
         }
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
        block(NO,@"");
         NSLog(@"journalarticle/like error: %@",error.localizedDescription);
     }];
    
}
+ (void)journalUnlikeWithJournal:(IMGMediaComment *)journal andUser:(IMGUser *)user completion:(void(^)(BOOL status,NSString *message))block
{
    NSDictionary * parameters = @{@"articleId":journal.mediaCommentId,@"userId":user.userId,@"t":user.token,@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]};
    [[IMGNetWork sharedManager]POST:@"journalarticle/unlike" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         if(responseObject && [[responseObject objectForKey:@"status"] isEqualToString:@"succeed"]){
            block(YES,@"");
         }else{
            block(NO,@"");
         }
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
        block(NO,@"");
        NSLog(@"journalarticle/like error: %@",error.localizedDescription);
     }];
    
}
+ (void)getJournalListWithParams:(NSMutableDictionary *)parameters andBlock:(void (^)(NSArray *dataArray,int totalCount))block failure:(void (^)(NSError *error))failureBlock
{
    NSMutableArray *dataArrM = [[NSMutableArray alloc] init];
    
    IMGUser *user = [IMGUser currentUser];
    if (user.userId != nil && user.token != nil)
    {
        [parameters setValue:user.userId forKey:@"userId"];
        [parameters setValue:user.token forKey:@"t"];
    }
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID])
    {
        [parameters setValue:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
        
    }
    
    [parameters setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [parameters setValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    
    [[IMGNetWork sharedManager]GET:@"journalarticle/search" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         
         //         NSLog(@"journalList = %@",responseObject);
        // BOOL hasData = YES;
         NSArray *journalArticleList = [responseObject objectForKey:@"journalArticleList"];
         for (NSDictionary *journalDic in journalArticleList)
         {
             IMGMediaComment *journal = [[IMGMediaComment alloc] init];
             
             journal.journalTitle = [journalDic objectForKey:@"articleTitle"];
             NSRange range=[journal.journalTitle rangeOfString:@"amp;"];
             if (range.location != NSNotFound)
             {
                 journal.journalTitle = [journal.journalTitle stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
             }
             
             journal.mediaCommentId = [journalDic objectForKey:@"articleId"];
             journal.journalImageUrl = [journalDic objectForKey:@"articlePhoto"];
             journal.contents = [journalDic objectForKey:@"articleContent"];
             NSArray *categories=[journalDic objectForKey:@"category"];
             NSMutableArray *category_list=[[NSMutableArray alloc] init];
             if(categories.count){
                 for (NSDictionary *category in categories) {
                     [category_list addObject:[category objectForKey:@"name"]];
                 }
                 
                 journal.categoryName=[category_list componentsJoinedByString:@", "];
             }
             
             journal.shareCount = [journalDic objectForKey:@"shareCount"];
             journal.imported = [journalDic objectForKey:@"imported"];
             journal.hasVideo = [journalDic objectForKey:@"hasVedio"];
             [dataArrM addObject:journal];
         }
         block(dataArrM,[[responseObject objectForKey:@"count"] intValue]);
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         failureBlock(error);
         NSLog(@"journalList error: %@",error.localizedDescription);
     }];
}
+ (void)getJournalDetailWithJournalId:(NSNumber *)journalId andJournalBlock:(void (^)(IMGMediaComment *journal))journalBlock
{
    IMGMediaComment *journal = [[IMGMediaComment alloc] init];
    journal.mediaCommentId = journalId;
    NSDictionary * parameters;
    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        parameters = @{@"wordpressId":journal.mediaCommentId};
    }
    else
    {
        parameters = @{@"userID":user.userId,@"t":user.token,@"wordpressId":journal.mediaCommentId};
    }
    
    [[IMGNetWork sharedManager]POST:@"journalarticle" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         
         //         NSLog(@"journalList = %@",responseObject);
         
         if([responseObject objectForKey:@"exceptionmsg"]){
             journalBlock(nil);
         }else if([responseObject objectForKey:@"article"]){
             NSDictionary *articleDic = [responseObject objectForKey:@"article"];
             
             NSArray *categoryListArr = [articleDic objectForKey:@"categoryList"];
             NSMutableString *categoryStrM = [[NSMutableString alloc] init];
             for (NSDictionary *dic in categoryListArr)
             {
                 if (categoryStrM.length)
                 {
                     [categoryStrM appendFormat:@", %@",[dic objectForKey:@"name"]];
                 }
                 else
                 {
                     [categoryStrM appendFormat:@"%@",[dic objectForKey:@"name"]];
                 }
             }
             journal.categoryName = categoryStrM;
             journal.createdDate = [articleDic objectForKey:@"createTime"];
             journal.likeCount = [articleDic objectForKey:@"likeCount"];
             journal.link = [articleDic objectForKey:@"link"];
             journal.imported = [articleDic objectForKey:@"imported"];
             journal.mediaCommentId = [articleDic objectForKey:@"id"];
             journal.title = [articleDic objectForKey:@"title"];
             
             NSDictionary *journalArticleAuthorDic = [articleDic objectForKey:@"journalArticleAuthor"];
             journal.authorDescritpion = [journalArticleAuthorDic objectForKey:@"descritpion"];
             journal.authorName = [journalArticleAuthorDic objectForKey:@"name"];
             journal.authorPhoto = [journalArticleAuthorDic objectForKey:@"photo"];
             journal.isLike = [[articleDic objectForKey:@"isLike"] boolValue];
             journalBlock(journal);
             
         }else{
             journalBlock(nil);
         }
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"journalDetail error: %@",error.localizedDescription);
     }];
    
}
+ (void)getJournalDetailWithJournal:(IMGMediaComment *)journal andJournalBlock:(void (^)(IMGMediaComment *journal, NSNumber *goFoodCount))journalBlock andAllDataBlock:(void (^)(NSNumber *_componentCount,NSMutableArray *componentsArray,NSArray *commentList,NSNumber *commentCount,NSNumber *menuPhotoCount))allDataBlock andRelatedPostBlock:(void (^)(NSArray *relatedPostArray))relatedPostBlock
{
    //NSDictionary * parameters = @{@"articleId":journal.mediaCommentId};
    NSDictionary * parameters;
    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        parameters = @{@"articleId":journal.mediaCommentId};
    }
    else
    {
        parameters = @{@"userID":user.userId,@"t":user.token,@"articleId":journal.mediaCommentId};
    }
    [[IMGNetWork sharedManager]POST:@"journalarticle/v3" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSLog(@"%f,getJournalDetailData service",[[NSDate date]timeIntervalSince1970]);

//         NSLog(@"journalList = %@",responseObject);
         
         NSArray *restaurantComponentsArray = [[NSArray alloc]init];
         restaurantComponentsArray = [responseObject objectForKey:@"restaurantComponents"];
         
         NSDictionary *dic = [[NSDictionary alloc]init];
         dic = [restaurantComponentsArray firstObject];
         NSNumber *menuPhotoCount = [dic objectForKey:@"menuPhotoCount"];

         
         NSDictionary *articleDic = [responseObject objectForKey:@"article"];
         
         
         NSArray *categoryListArr = [articleDic objectForKey:@"categoryList"];
         
         NSMutableArray *categoryArr=[[NSMutableArray alloc] init];
         for (NSDictionary *dic in categoryListArr){
             
             [categoryArr addObject:[dic objectForKey:@"name"]];
         }
         NSString *categoryStrM=[categoryArr componentsJoinedByString:@", "];
         journal.categoryName = categoryStrM;
         //journal.createdDate = [articleDic objectForKey:@"createTime"];
         journal.createTimeUnixTimestamp = [articleDic objectForKey:@"createTimeUnixTimestamp"];
         
         journal.createdDate = [articleDic objectForKey:@"createTimeUnixTimestamp"];
         journal.likeCount = [articleDic objectForKey:@"likeCount"];
         journal.link = [articleDic objectForKey:@"link"];
         journal.title = [articleDic objectForKey:@"title"];
         journal.viewCount = [articleDic objectForKey:@"viewCount"];
         NSDictionary *journalArticleAuthorDic = [articleDic objectForKey:@"journalArticleAuthor"];
         journal.authorDescritpion = [journalArticleAuthorDic objectForKey:@"descritpion"];
         journal.authorName = [journalArticleAuthorDic objectForKey:@"name"];
         journal.authorPhoto = [journalArticleAuthorDic objectForKey:@"photo"];
         journal.imageUrl = [articleDic objectForKey:@"mainPhoto"];
         journal.isLike = [[articleDic objectForKey:@"isLike"] boolValue];
         journal.JournalGifH=[[articleDic objectForKey:@"height"] floatValue];
         journal.JournalGifW=[[articleDic objectForKey:@"width"] floatValue];
         journal.contents = [articleDic objectForKey:@"content"];
         journal.shareCount = [articleDic objectForKey:@"shareCount"];

         [journal updatePhotoCredit:[articleDic objectForKey:@"photoCredit4AppImpl"]];
         
         journalBlock(journal, [responseObject objectForKey:@"goFoodCount"]);
         
         NSNumber *componentCount = [responseObject objectForKey:@"componentCount"];
         NSMutableArray *components = [JournalHandler handleComponentsParam:responseObject andComponents:nil];
         
         NSArray *commentArray = [responseObject objectForKey:@"commentList"];
         NSMutableArray *commentList = [[NSMutableArray alloc]initWithCapacity:0];
         commentList=[IMGMediaComment commentListFromArray:commentArray];
         journal.commentList=commentList;
         
         NSString *commentCountNode =[responseObject objectForKey:@"commentCount"];
         if(commentCountNode){
             journal.commentCount =[NSNumber numberWithInteger:[commentCountNode integerValue]];
         }
         allDataBlock(componentCount,components,journal.commentList,journal.commentCount,menuPhotoCount);
         
         NSMutableArray *relatedPostArrM = [[NSMutableArray alloc] init];
         NSArray *postedArticlesArrFromService = [responseObject  objectForKey:@"postedArticles"];
         for (NSDictionary *dic in postedArticlesArrFromService)
         {
             IMGMediaComment *journal1 = [[IMGMediaComment alloc] init];
             journal1.mediaCommentId = [dic objectForKey:@"articleId"];
//             journal1.title = [[dic objectForKey:@"articleTitle"] stringByAddingHTMLEntities];
             journal1.title = [dic objectForKey:@"articleTitle"];
             journal1.imageUrl = [dic objectForKey:@"articlePhoto"];
             journal1.imported = [dic objectForKey:@"imported"];
             [relatedPostArrM addObject:journal1];
         }
         relatedPostBlock(relatedPostArrM);
         NSLog(@"%f,getJournalDetailData deal all time",[[NSDate date]timeIntervalSince1970]);
  
         
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"journalDetail error: %@",error.localizedDescription);
     }];
}

+ (void)getMoreJournalDetailWithJournal:(IMGMediaComment *)journal andComponentoffset:(NSNumber*)componentoffset andComponents:(NSMutableArray*) _components andAllDataBlock:(void (^)(NSNumber *_componentCount, NSMutableArray *dataDic))allDataBlock failure:(void (^)(NSError *error))failureBlock{
    NSDictionary * parameters;
    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        parameters = @{@"articleId":journal.mediaCommentId,@"offset":componentoffset,@"max":@(10)};
    }
    else
    {
        parameters = @{@"userID":user.userId,@"t":user.token,@"articleId":journal.mediaCommentId,@"offset":componentoffset,@"max":@(10)};
    }
    [[IMGNetWork sharedManager]POST:@"journalarticle/v3/loadmore" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        
        NSNumber *componentCount = [responseObject objectForKey:@"componentCount"];
        NSMutableArray *components = [JournalHandler handleComponentsParam:responseObject andComponents:_components];
        allDataBlock(componentCount,components);
    }failure:^(NSURLSessionDataTask *operation, NSError *error){
         failureBlock(error);
         NSLog(@"journalDetail error: %@",error.localizedDescription);
    }];
}

+(NSMutableArray *)handleComponentsParam:(id) responseObject andComponents:(NSMutableArray*) preComponents{
    NSArray *restaurantComponentsArr = [responseObject objectForKey:@"restaurantComponents"];
    NSArray *titleComponentsArr = [responseObject objectForKey:@"titleComponents"];
    NSArray *photoComponentsArr = [responseObject objectForKey:@"photoComponents"];
    NSArray *contentComponentsArr = [responseObject objectForKey:@"contentComponents"];
    NSArray *buttonComponentsArr = [responseObject objectForKey:@"buttonComponents"];
    NSArray *endComponentsArr = [responseObject objectForKey:@"restaurantEndComponents"];
    NSArray* VideoComponentsArr=[responseObject objectForKey:@"videoComponents"];
    NSMutableArray *components=[[NSMutableArray alloc] init];
    
    NSMutableArray *restaurants=[[NSMutableArray alloc] init];
    
    for (NSDictionary *restdic in restaurantComponentsArr){
        NSMutableDictionary *restaurant=[[NSMutableDictionary alloc] init];
        NSNumber *restaurantId=[restdic objectForKey:@"restaurantId"];
        BOOL bookStatus=((NSNumber*)[restdic objectForKey:@"bookStatus"]).boolValue;
        NSString*stameName=[restdic objectForKey:@"stateName"];
        if ([stameName isEqual:[NSNull null]]) {
            stameName=@"";
        }
        
        NSString *stateName=[stameName lowercaseString];
        BOOL stateStatus=!([stateName isEqualToString:@"under renovation"] || [stateName isEqualToString:@"temporarily closed"] || [stateName isEqualToString:@"permanantly closed"] || [stateName isEqualToString:@"coming soon"]);
        NSArray *cuisineList=[restdic objectForKey:@"cuisineList"];
        NSString *cuisine=@"";
        NSString *cuisineId=[NSString stringWithFormat:@"%@",[NSNumber numberWithInt:0]];
        if(cuisineList.count){
            cuisine=[[cuisineList objectAtIndex:0] objectForKey:@"name"];
            cuisineId=[[cuisineList objectAtIndex:0] objectForKey:@"id"];
        }
        [restaurant setDictionary:@{
                                    @"title":[restdic objectForKey:@"restaurantTitle"],
                                    @"address1":[restdic objectForKey:@"address1"],
                                    @"address2":[restdic objectForKey:@"address2"],
                                    @"liked":[restdic objectForKey:@"liked"],
                                    @"menuPhotoCount":[restdic objectForKey:@"menuPhotoCount"],
                                    @"saved":[restdic objectForKey:@"saved"],
                                    @"likeCount":[restdic objectForKey:@"faveCount"],
                                    @"saveCount":[restdic objectForKey:@"saveCount"],
                                    @"reviewCount":[restdic objectForKey:@"reviewCount"],
                                    @"menuCount":[restdic objectForKey:@"menuCount"],
                                    @"phoneNumber":[restdic objectForKey:@"phoneNumber"],
                                    @"bookStatus":[NSNumber numberWithBool:(stateStatus && bookStatus)],
                                    @"restaurantId":restaurantId,
                                    @"score":[restdic objectForKey:@"score"],
                                    @"district":[restdic objectForKey:@"districtName"],
                                    @"districtId":[restdic objectForKey:@"districtId"],
                                    @"priceLevel":[restdic objectForKey:@"priceLevel"],
                                    @"cuisine":cuisine,
                                    @"cuisineId":cuisineId,
                                    @"cityId":[[restdic objectForKey:@"area"] objectForKey:@"cityId"],
                                    @"order":[restdic objectForKey:@"order"],
                                    @"ratingCount":[restdic objectForKey:@"ratingCount"],
                                    }];
        
        NSDictionary *offer=[restdic objectForKey:@"offer"];
        
        [restaurant setObject:[restdic objectForKey:@"latitude"] forKey:@"latitude"];
        [restaurant setObject:[restdic objectForKey:@"longitude"] forKey:@"longitude"];
        [restaurant setObject:[restdic objectForKey:@"wellKnownFor"] forKey:@"wellKnownFor"];
        
        [restaurant setObject:[restdic objectForKey:@"state"] forKey:@"state"];
        [restaurant setObject:[restdic objectForKey:@"imageUrl"] forKey:@"imageUrl"];
        
        NSDictionary* yesterOpenDic=[restdic objectForKey:@"yesterDayOpenTime"];
        if ([yesterOpenDic allKeys].count > 0) {
            [restaurant setObject:[yesterOpenDic objectForKey:@"openDay"] forKey:@"yesteropenDay"];
            [restaurant setObject:[yesterOpenDic objectForKey:@"openTime"] forKey:@"yesteropenTime"];
            [restaurant setObject:[yesterOpenDic objectForKey:@"closedTime"] forKey:@"yesterclosedTime"];
        }

        NSDictionary* nextOpenDay=[restdic objectForKey:@"nextOpenDay"];
        if ([nextOpenDay allKeys].count > 0) {
            [restaurant setObject:[nextOpenDay objectForKey:@"openTime"] forKey:@"nextopenTime"];
            [restaurant setObject:[nextOpenDay objectForKey:@"closedTime"] forKey:@"nextclosedTime"];
            [restaurant setObject:[nextOpenDay objectForKey:@"openDay"] forKey:@"nextopenDay"];
        }
        
        NSDictionary* secondOpenDic=[restdic objectForKey:@"secondOpenDay"];
        if ([secondOpenDic allKeys].count > 0) {
            [restaurant setObject:[secondOpenDic objectForKey:@"openTime"] forKey:@"secondclosedTime"];
            [restaurant setObject:[secondOpenDic objectForKey:@"closedTime"] forKey:@"secondopenTime"];
            [restaurant setObject:[secondOpenDic objectForKey:@"openDay"] forKey:@"secondopenDay"];
        }
        
        
        if(offer && offer.count){
            [restaurant setObject:[offer objectForKey:@"offerTagLine"] forKey:@"tagline"];
            [restaurant setObject:[offer objectForKey:@"offerTitle"] forKey:@"offer"];
        }
        
        [restaurants addObject:@{@"restaurant":restaurant,@"order":[restdic objectForKey:@"order"]}];
    }
    
    restaurants=[NSMutableArray arrayWithArray:[restaurants sortedArrayUsingComparator:^(id objc1,id objc2){
        if([[objc1 objectForKey:@"order"] integerValue]>[[objc2 objectForKey:@"order"] integerValue]){
            return (NSComparisonResult)NSOrderedDescending;
        }
        if([[objc1 objectForKey:@"order"] integerValue]<[[objc2 objectForKey:@"order"] integerValue]){
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }]];
    
    
    int preRestaurantsCount = 0;
    for (NSDictionary *item in preComponents) {
        if ([item objectForKey:@"restaurant"]!=nil) {
            preRestaurantsCount++;
        }
    }
    for (int i = 0; i < restaurants.count; i++){
        
        NSMutableDictionary *rest=[[[restaurants objectAtIndex:i] objectForKey:@"restaurant"] mutableCopy];
        [rest setObject:[NSNumber numberWithInt:preRestaurantsCount+i+1] forKey:@"index"];
        [rest setObject:[NSNumber numberWithInt:(preRestaurantsCount+(int)restaurants.count)] forKey:@"count"];
        [components addObject:@{@"restaurant":rest,@"order":[[restaurants objectAtIndex:i] objectForKey:@"order"]}];
    }
    
    for (NSDictionary *dic in titleComponentsArr){
        NSDictionary *title=@{@"title":[dic objectForKey:@"title"]};
        [components addObject:@{@"title":title,@"order":[dic objectForKey:@"order"]}];
    }
    
    for (NSDictionary *dic in photoComponentsArr){
        NSDictionary *photo=@{@"photo":[dic objectForKey:@"photo"],@"credit":[dic objectForKey:@"credit"],@"creditType":[dic objectForKey:@"creditType"],@"url":[dic objectForKey:@"creditUrl"],@"restaurantId":[[dic objectForKey:@"simpleRestaurant"] objectForKey:@"restaurantId"],@"order":[dic objectForKey:@"order"],@"photoCreditImpl":[dic objectForKey:@"simpleRestaurant"],@"photoTarget":[dic objectForKey:@"photoTarget"],@"height":[dic objectForKey:@"height"],@"width":[dic objectForKey:@"width"]};
        [components addObject:@{@"photo":photo,@"order":[dic objectForKey:@"order"]}];
    }
    
    
    for (NSDictionary* dic in VideoComponentsArr) {
        NSDictionary *content=@{@"videoUrl":[dic objectForKey:@"videoUrl"],@"order":[dic objectForKey:@"order"]};
        [components addObject:@{@"video":content,@"order":[dic objectForKey:@"order"]}];
        
    }
    
    //NSDictionary *content=@{@"video":@123,@"order":[dic objectForKey:@"order"]};
    //         [components addObject:@{@"video":@{},@"order":@2}];
    
    
    for (NSDictionary *dic in contentComponentsArr){
        NSDictionary *content=@{@"content":[dic objectForKey:@"content"],@"restaurantId":[[dic objectForKey:@"simpleRestaurant"] objectForKey:@"restaurantId"],@"order":[dic objectForKey:@"order"]};
        [components addObject:@{@"content":content,@"order":[dic objectForKey:@"order"]}];
    }
    
    for (NSDictionary *dic in buttonComponentsArr){
        NSDictionary *button=@{@"button":[dic objectForKey:@"content"],@"url":[dic objectForKey:@"url"],@"restaurantId":[[dic objectForKey:@"simpleRestaurant"] objectForKey:@"restaurantId"],@"landingPage":[dic objectForKey:@"restaurantButtonsLandingPage"],@"SelectPromoId":[dic objectForKey:@"restaurantSelectPromo"],@"order":[dic objectForKey:@"order"], @"restaurant":[dic objectForKey:@"simpleRestaurant"]};
        [components addObject:@{@"button":button,@"order":[dic objectForKey:@"order"]}];
    }
    
    NSMutableArray *endArr=[[NSMutableArray alloc] init];
    
    for (NSDictionary *dic in endComponentsArr){
        NSDictionary *end=@{@"end":[NSNumber numberWithBool:YES]};
        [endArr addObject:@{@"end":end,@"order":[dic objectForKey:@"order"]}];
    }
    
    endArr=[NSMutableArray arrayWithArray:[endArr sortedArrayUsingComparator:^(id objc1,id objc2){
        if([[objc1 objectForKey:@"order"] integerValue]>[[objc2 objectForKey:@"order"] integerValue]){
            return (NSComparisonResult)NSOrderedDescending;
        }
        if([[objc1 objectForKey:@"order"] integerValue]<[[objc2 objectForKey:@"order"] integerValue]){
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }]];
    
    if (endArr.count!=0) {
        if ( (restaurants.count == 0)
            || ([[[endArr objectAtIndex:0] objectForKey:@"order"] longValue]<[[[restaurants objectAtIndex:0] objectForKey:@"order"] longValue]) ) {
            for (int i = 0; i < endArr.count; ++i){
                if (i == 0) {
                    for (int i = (int)(preComponents.count-1) ;i >= 0 ;i--) {
                        NSDictionary *item = preComponents[i];
                        if ([item objectForKey:@"restaurant"]!=nil) {
                            [components addObject:@{@"end":@{},@"order":[[endArr objectAtIndex:0] objectForKey:@"order"]}];
                            break;
                        }
                    }
                }else{
                    [components addObject:@{@"end":@{},@"order":[[endArr objectAtIndex:i] objectForKey:@"order"]}];
                }
            }
        }else{
            for (int i = 0; i < endArr.count; ++i){
                
                [components addObject:@{@"end":@{},@"order":[[endArr objectAtIndex:i] objectForKey:@"order"]}];
            }
        }
    }
    
    components=[NSMutableArray arrayWithArray:[components sortedArrayUsingComparator:^(id objc1,id objc2){
        if([[objc1 objectForKey:@"order"] integerValue]>[[objc2 objectForKey:@"order"] integerValue]){
            return (NSComparisonResult)NSOrderedDescending;
        }
        if([[objc1 objectForKey:@"order"] integerValue]<[[objc2 objectForKey:@"order"] integerValue]){
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }]];
    return components;
}


+ (void)getJournalCategoryWithBlock:(void (^)(NSArray *dataArray))block
{
    [[IMGNetWork sharedManager]POST:@"app/journalarticlecategory/list" parameters:nil progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSMutableArray *dataArrM = [[NSMutableArray alloc] init];
         for (NSDictionary *dic in responseObject)
         {
             IMGJournalCategory *category = [[IMGJournalCategory alloc] init];
             category.categoryName = [dic objectForKey:@"name"];
             category.categoryType = [dic objectForKey:@"type"];
             category.categoryOrder = [dic objectForKey:@"order"];
             [dataArrM addObject:category];
         }
         block(dataArrM);
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"app/journalarticlecategory/list error: %@",error.localizedDescription);
     }];
}

+ (void)searchJournalarticleWithContent:(NSString *)content andBlock:(void (^)(NSArray *))block
{
    NSDictionary * parameters = @{@"content":content};
    IMGUser *user = [IMGUser currentUser];
    if (user.userId != nil && user.token != nil)
    {
        [parameters setValue:user.userId forKey:@"userId"];
        [parameters setValue:user.token forKey:@"t"];
    }
    
    [[IMGNetWork sharedManager]POST:@"journalarticle/search" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSMutableArray *dataArrM = [[NSMutableArray alloc] init];
         NSArray *journalArticleList = [responseObject objectForKey:@"journalArticleList"];
         for (NSDictionary *journalDic in journalArticleList)
         {
             IMGMediaComment *journal = [[IMGMediaComment alloc] init];
             
             journal.title = [journalDic objectForKey:@"articleTitle"];
             NSRange range=[journal.title rangeOfString:@"amp;"];
             if (range.location != NSNotFound)
             {
                 journal.title = [journal.title stringByReplacingOccurrencesOfString:@"amp;" withString:@""];
             }
             
             journal.mediaCommentId = [journalDic objectForKey:@"articleId"];
             journal.imageUrl = [journalDic objectForKey:@"articlePhoto"];
             NSArray *categories=[journalDic objectForKey:@"category"];
             NSMutableArray *category_list=[[NSMutableArray alloc] init];
             if(categories.count){
                 for (NSDictionary *category in categories) {
                     [category_list addObject:[category objectForKey:@"name"]];
                 }
                 
                 journal.categoryName=[category_list componentsJoinedByString:@", "];
             }
             
             journal.shareCount = [journalDic objectForKey:@"shareCount"];
             journal.imported = [journalDic objectForKey:@"imported"];
             [dataArrM addObject:journal];
         }
         block(dataArrM);
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"app/journalarticlecategory/list error: %@",error.localizedDescription);
     }];
}

+ (void)getJournalDetailTypeArticleIdWithJournalId:(NSNumber *)journalId andJournalBlock:(void (^)(IMGMediaComment *journal))journalBlock
{
    IMGMediaComment *journal = [[IMGMediaComment alloc] init];
    journal.mediaCommentId = journalId;
    NSDictionary * parameters;
    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        parameters = @{@"articleId":journal.mediaCommentId};
    }
    else
    {
        parameters = @{@"userID":user.userId,@"t":user.token,@"articleId":journal.mediaCommentId};
    }
    
    [[IMGNetWork sharedManager]POST:@"journalarticle" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         
         //         NSLog(@"journalList = %@",responseObject);
         
         if([responseObject objectForKey:@"exceptionmsg"]){
             journalBlock(nil);
         }else if([responseObject objectForKey:@"article"]){
             NSDictionary *articleDic = [responseObject objectForKey:@"article"];
             
             NSArray *categoryListArr = [articleDic objectForKey:@"categoryList"];
             NSMutableString *categoryStrM = [[NSMutableString alloc] init];
             for (NSDictionary *dic in categoryListArr)
             {
                 if (categoryStrM.length){
                     [categoryStrM appendFormat:@", %@",[dic objectForKey:@"name"]];
                 }else{
                     [categoryStrM appendFormat:@"%@",[dic objectForKey:@"name"]];
                 }
             }
             journal.categoryName = categoryStrM;
             journal.createdDate = [articleDic objectForKey:@"createTime"];
             journal.likeCount = [articleDic objectForKey:@"likeCount"];
             journal.link = [articleDic objectForKey:@"link"];
             journal.imported = [articleDic objectForKey:@"imported"];
             journal.mediaCommentId = [articleDic objectForKey:@"id"];
             journal.title = [articleDic objectForKey:@"title"];
             
             NSDictionary *journalArticleAuthorDic = [articleDic objectForKey:@"journalArticleAuthor"];
             journal.authorDescritpion = [journalArticleAuthorDic objectForKey:@"descritpion"];
             journal.authorName = [journalArticleAuthorDic objectForKey:@"name"];
             journal.authorPhoto = [journalArticleAuthorDic objectForKey:@"photo"];
             
             journal.isLike = [[articleDic objectForKey:@"isLike"] boolValue];
             journalBlock(journal);
             
         }else{
             journalBlock(nil);
         }
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"journalDetail error: %@",error.localizedDescription);
     }];
    
}

+ (void)saveJournalWithJournalArticleId:(NSNumber *)articleId
{
    IMGUser *user=[IMGUser currentUser];
    NSDictionary *parameters = @{@"userId":user.userId,@"t":user.token,@"journalArticleId":articleId};
    
    [[IMGNetWork sharedManager]POST:@"user/journallist/add" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         
         
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"saveJournalWithJournalArticleId error: %@",error.localizedDescription);
     }];
    
    
}

+ (void)removeJournalWithJournalArticleId:(NSNumber *)articleId
{
    IMGUser *user=[IMGUser currentUser];
    NSDictionary *parameters = @{@"userId":user.userId,@"t":user.token,@"journalArticleId":articleId};
    
    [[IMGNetWork sharedManager]POST:@"user/journallist/remove" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"removeJournalWithJournalArticleId error: %@",error.localizedDescription);
     }];
}

+(void)previousComment:(NSString *)commentIdStr targetId:(NSNumber*)articleId andBlock:(void (^)(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount))block{
    
    NSDictionary *parameters=[[NSDictionary alloc]initWithObjectsAndKeys:articleId,@"articleId",commentIdStr,@"lastJournalArticleCommentId", nil];
    [[IMGNetWork sharedManager]GET:@"journal/comments/previous" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
         if(responseObject && errorMessage){
             NSLog(@"previous comment: error: %@",errorMessage);
         }else if(responseObject){
             NSMutableArray *commentList = [[NSMutableArray alloc]initWithCapacity:0];
             
             NSArray *commentArray = [responseObject objectForKey:@"commentList"];
             commentList=[IMGMediaComment commentListFromArray:commentArray];
             NSNumber *commentCount = [responseObject objectForKey:@"commentCount"];
             NSNumber *likeCount = [responseObject objectForKey:@"likeCount"];
             block(commentList,likeCount,commentCount);

         }
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         NSLog(@"getRestaurantFirstDataFromService error: %@",error.localizedDescription);
     }];

}

+(void)postComment:(NSString *)commentStr articleId:(NSNumber*)articleId andBlock:(void (^)(IMGJournalComment *journalComment))block failure:(void(^)(NSString *errorReason))failureBlock{

    NSString *trimmedString = [commentStr stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        if (!(trimmedString.length==0)) {
            [[LoadingView sharedLoadingView] startLoading];
            NSDictionary *parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",articleId,@"articleId",commentStr,@"commentContent",[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"latitude",[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],@"longitude", nil];
            [[IMGNetWork sharedManager]POST:@"journal/comment/write" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
             {
                 [[LoadingView sharedLoadingView] stopLoading];
                 NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
                 if(responseObject && errorMessage){
                     NSLog(@"postComment: error: %@",errorMessage);
                     failureBlock(errorMessage);
                 }else if(responseObject){
                     NSDictionary *commentDictionary = [responseObject objectForKey:@"comment"];

                     if(commentDictionary){
                         IMGJournalComment *journalComment = [[IMGJournalComment alloc]initFromDictionary:commentDictionary];
                         block(journalComment);
                     }else{
                         block(nil);
                     }
                     
                 }
             }failure:^(NSURLSessionDataTask *operation, NSError *error)
             {
                 failureBlock(error.localizedDescription);
                 [[LoadingView sharedLoadingView] stopLoading];
             }];
        }
    }
}

+(void)likeJournal:(BOOL)liked articleId:(NSNumber*)articleId andBlock:(void (^)())block  failure:(void(^)(NSString *exceptionMsg))failureBlock{
    NSString *likeOrUnlikeStr = @"journalarticle/like";
    if(liked){
        likeOrUnlikeStr = @"journalarticle/unlike";
    }
    
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
//        [[LoadingView sharedLoadingView] startLoading];        
        NSDictionary *parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userId",user.userId,@"userID",user.token,@"t",articleId,@"articleId",[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"latitude",[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],@"longitude", nil];
        [[IMGNetWork sharedManager]POST:likeOrUnlikeStr parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
         {
//             [[LoadingView sharedLoadingView] stopLoading];
             NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
             if(responseObject && errorMessage){
                 failureBlock(errorMessage);
             }else if(responseObject){
                 block();
             }
         }failure:^(NSURLSessionDataTask *operation, NSError *error)
         {
             failureBlock(error.localizedDescription);
//             [[LoadingView sharedLoadingView] stopLoading];
             NSLog(@"likeJournal error: %@",error.localizedDescription);
         }];

    }
}

+ (void)getGoFoodRestaurantList:(IMGMediaComment *)journal andBlock:(void(^)(NSArray  *restaurantArr))successBlock andError:(void(^)(NSError *error))errorBlock{
    NSMutableDictionary *paramDic = [NSMutableDictionary dictionary];
    [paramDic setValue:[IMGUser currentUser].userId forKey:@"userId"];
    [paramDic setValue:journal.mediaCommentId forKey:@"articleId"];
    
    [[IMGNetWork sharedManager] GET:@"journalarticle/gofood/list" parameters:paramDic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *restaurantArr = [NSMutableArray array];
        for (NSDictionary *dic in [responseObject objectForKey:@"goFoodList"]) {
            IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
            [restaurant setValuesForKeysWithDictionary:dic];
            restaurant.ratingScore = [dic objectForKey:@"rating"];
            restaurant.address1 = [dic objectForKey:@"address"];
            restaurant.bookStatus = [dic objectForKey:@"status"];
            NSDictionary* yesterOpenDic=[dic objectForKey:@"yesterDayOpenTime"];
            restaurant.yesteropenDay=[yesterOpenDic objectForKey:@"openDay"];
            restaurant.yesteropenTime=[yesterOpenDic objectForKey:@"openTime"];
            restaurant.yesterclosedTime=[yesterOpenDic objectForKey:@"closedTime"];
            restaurant.goFoodLink = [dic objectForKey:@"goFoodLink"];
            restaurant.saved = [dic objectForKey:@"isSaved"];
            
            NSDictionary* nextOpenDay=[dic objectForKey:@"nextOpenDay"];
            restaurant.nextopenTime=[nextOpenDay objectForKey:@"openTime"];
            restaurant.nextclosedTime=[nextOpenDay objectForKey:@"closedTime"];
            restaurant.nextopenDay=[nextOpenDay objectForKey:@"openDay"];
            
            NSDictionary* secondOpenDic=[dic objectForKey:@"secondOpenDay"];
            restaurant.secondclosedTime=[secondOpenDic objectForKey:@"closedTime"];
            restaurant.secondopenTime=[secondOpenDic objectForKey:@"openTime"];
            restaurant.secondopenDay=[secondOpenDic objectForKey:@"openDay"];
            
            [restaurantArr addObject:restaurant];
        }
        successBlock(restaurantArr);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock(error);
    }];
}

@end
