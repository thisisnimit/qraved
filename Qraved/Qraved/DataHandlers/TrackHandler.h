//
//  trackHandler.h
//  Qraved
//
//  Created by Lucky on 15/6/3.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataHandler.h"
#import "IMGUser.h"
#import "IMGRestaurant.h"

@interface TrackHandler : DataHandler

+ (void)trackWithUserId:(NSNumber *)userId andRestaurantId:(NSNumber *)restaurantId andContent:(NSDictionary *)typeDic;

+ (void)trackWithUserId:(NSNumber *)userId andJournalId:(NSNumber *)journalId type:(NSNumber*)type other:(NSDictionary *)other;


+ (void)trackForSearchWithProducttype:(NSNumber *)producttype andSearchresulttype:(NSNumber *)searchresulttype andContent:(NSString *)content andResultObjectId:(NSNumber *)resultObjectId;
@end
