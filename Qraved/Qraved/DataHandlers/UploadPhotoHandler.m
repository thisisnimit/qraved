//
//  UploadPhotoHandler.m
//  Qraved
//
//  Created by lucky on 16/3/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "UploadPhotoHandler.h"
#import "IMGUser.h"
#import "LoadingView.h"
#import "IMGDishComment.h"
#import "IMGRestaurant.h"
#import "IMGDish.h"
#import "Amplitude.h"

@implementation UploadPhotoHandler

+(void)previousComment:(NSString *)commentIdStr targetId:(NSNumber*)articleId andBlock:(void (^)(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount))block{
    
    NSDictionary *parameters;
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userId",articleId,@"dishId",commentIdStr,@"lastDishCommentId", nil];
    }
    else
    {
        parameters=[[NSDictionary alloc]initWithObjectsAndKeys:articleId,@"dishId",commentIdStr,@"lastDishCommentId", nil];
    }
    [[IMGNetWork sharedManager]GET:@"dish/comments/previous" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
         if(responseObject && errorMessage){
             NSLog(@"previous comment: error: %@",errorMessage);
         }else if(responseObject){
             NSMutableArray *commentList = [[NSMutableArray alloc]initWithCapacity:0];
             IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
             NSDictionary *restaurantDic = [responseObject objectForKey:@"restaurant"];
             restaurant.restaurantId = [restaurantDic objectForKey:@"id"];
             restaurant.imageUrl = [restaurantDic objectForKey:@"imageUrl"];
             restaurant.title = [restaurantDic objectForKey:@"title"];
             NSArray *commentArray = [responseObject objectForKey:@"commentList"];
             commentList=[IMGDishComment commentListFromArray:commentArray andIsVendor:[[responseObject objectForKey:@"isVendor"] boolValue]andRestaurant:restaurant];
             NSNumber *commentCount = [responseObject objectForKey:@"commentCount"];
             NSNumber *likeCount = [responseObject objectForKey:@"likeCount"];
             block(commentList,likeCount,commentCount);
             
         }
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         NSLog(@"getRestaurantFirstDataFromService error: %@",error.localizedDescription);
     }];
    
}
+(void)previousCardComment:(NSString *)commentIdStr targetId:(NSNumber*)articleId andBlock:(void (^)(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount))block{
    
    NSDictionary *parameters;
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userId",articleId,@"moderateReviewId",commentIdStr,@"lastModerateReviewCommentId", nil];
    }
    else
    {
        parameters=[[NSDictionary alloc]initWithObjectsAndKeys:articleId,@"moderateReviewId",commentIdStr,@"lastModerateReviewCommentId", nil];
    }
    [[IMGNetWork sharedManager]GET:@"moderatereview/comment/previous" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
         if(responseObject && errorMessage){
             NSLog(@"previous comment: error: %@",errorMessage);
         }else if(responseObject){
             NSMutableArray *commentList = [[NSMutableArray alloc]initWithCapacity:0];
             IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
             NSDictionary *restaurantDic = [responseObject objectForKey:@"restaurant"];
             restaurant.restaurantId = [restaurantDic objectForKey:@"id"];
             restaurant.imageUrl = [restaurantDic objectForKey:@"imageUrl"];
             restaurant.title = [restaurantDic objectForKey:@"title"];
             NSArray *commentArray = [responseObject objectForKey:@"commentList"];
             commentList=[IMGDishComment commentListFromArray:commentArray andIsVendor:[[responseObject objectForKey:@"isVendor"] boolValue]andRestaurant:restaurant];
             NSNumber *commentCount = [responseObject objectForKey:@"commentCount"];
             NSNumber *likeCount = [responseObject objectForKey:@"likeCount"];
             block(commentList,likeCount,commentCount);
             
         }
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         NSLog(@"getRestaurantFirstDataFromService error: %@",error.localizedDescription);
     }];
    
}



+(void)likeDish:(BOOL)liked dishId:(NSNumber*)dishId andBlock:(void (^)())block failure:(void(^)(NSString *exceptionMsg))failureBlock
{
    NSString *likeOrUnlikeStr = @"qrave";
    if(liked){
        likeOrUnlikeStr = @"unqrave";
    }
    
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
//        [[LoadingView sharedLoadingView] startLoading];
        NSDictionary *parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userID",user.token,@"t",dishId,@"dishID",[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"latitude",        [[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],@"longitude", nil];
        [[IMGNetWork sharedManager]POST:likeOrUnlikeStr parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
         {
//             [[LoadingView sharedLoadingView] stopLoading];
             NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
             if(responseObject && errorMessage){
                 failureBlock(errorMessage);
                 NSLog(@"likeJournal: error: %@",errorMessage);
             }else if(responseObject){
                 block();
             }
         }failure:^(NSURLSessionDataTask *operation, NSError *error)
         {
             failureBlock(error.localizedDescription);
//             [[LoadingView sharedLoadingView] stopLoading];
             NSLog(@"likeDish error: %@",error.localizedDescription);
         }];
    }
}
+(void)likeCard:(BOOL)liked moderateReviewId:(NSNumber*)moderateReviewId andBlock:(void (^)())block  failure:(void(^)(NSString *exceptionMsg))failureBlock
{
    NSString *likeOrUnlikeStr = @"moderateReview/like";
    if(liked){
        likeOrUnlikeStr = @"moderateReview/unlike";
    }
    
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        //        [[LoadingView sharedLoadingView] startLoading];
        NSDictionary *parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",moderateReviewId,@"moderateReviewId",[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"latitude",[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],@"longitude", nil];
        [[IMGNetWork sharedManager]POST:likeOrUnlikeStr parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
         {
             //             [[LoadingView sharedLoadingView] stopLoading];
             NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
             if(responseObject && errorMessage){
                 failureBlock(errorMessage);
                 NSLog(@"likeCard: error: %@",errorMessage);
             }else if(responseObject){
                 block();
             }
         }failure:^(NSURLSessionDataTask *operation, NSError *error)
         {
             failureBlock(error.localizedDescription);
             //             [[LoadingView sharedLoadingView] stopLoading];
             NSLog(@"likeCard error: %@",error.localizedDescription);
         }];
    }
}

+(void)postComment:(NSString *)commentStr dishId:(NSNumber*)dishId restaurantId:(NSNumber *)restaurantId andBlock:(void (^)(IMGDishComment *dishComment))block failure:(void(^)(NSString *errorReason))failureBlock{
    
    
    NSString *trimmedString = [commentStr stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        if (!(trimmedString.length==0)) {
            [[LoadingView sharedLoadingView] startLoading];
            NSDictionary *parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userID",user.token,@"t",dishId,@"dishID",restaurantId,@"restaurantID",commentStr,@"comment",[NSNumber numberWithInt:1],@"status",[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"latitude",[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],@"longitude", nil];
            [[IMGNetWork sharedManager]POST:@"dish/comment/write" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
             {
                 [[LoadingView sharedLoadingView] stopLoading];
                 NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
                 if(responseObject && errorMessage){
                     NSLog(@"postComment: error: %@",errorMessage);
                     failureBlock(errorMessage);
                 }else if(responseObject){
                     NSDictionary *commentDictionary = [responseObject objectForKey:@"commentInfo"];
                     if(commentDictionary){
                         IMGDishComment *dishComment = [[IMGDishComment alloc]initFromDictionary:commentDictionary];
                         block(dishComment);
                     }else{
                         block(nil);
                     }
                     
                 }
             }failure:^(NSURLSessionDataTask *operation, NSError *error)
             {
                 [[LoadingView sharedLoadingView] stopLoading];
                 NSLog(@"postCommentWithDish error: %@",error.localizedDescription);
                 failureBlock(error.localizedDescription);
             }];
        }
    }

}
+(void)getUploadPhotoDetailCardData:(NSDictionary *)params andBlock:(void (^)(NSArray *))block
{

    [[IMGNetWork sharedManager] POST:@"moderatereview/carddetail" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
        if(responseObject && errorMessage){
            NSLog(@"likeJournal: error: %@",errorMessage);
        }else if(responseObject){
            NSMutableArray *dicSubArrM = [[NSMutableArray alloc] init];
            IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
            restaurant.title = [responseObject objectForKey:@"restaurantTitle"];
            restaurant.restaurantId = [responseObject objectForKey:@"restaurantId"];
            restaurant.seoKeyword = [responseObject objectForKey:@"restaurantSeo"];
            restaurant.timeline = [responseObject objectForKey:@"timeline"];
            restaurant.likeCardCount=[responseObject objectForKey:@"likeCount"];
            restaurant.commentCardCount=[responseObject objectForKey:@"commentCount"];
            [restaurant setValuesForKeysWithDictionary:responseObject];
            NSArray *subDic = [responseObject objectForKey:@"dishList"];
            NSMutableArray* dishArr=[[NSMutableArray alloc]init];
            for (NSDictionary* dic in subDic) {
                IMGDish *dish = [[IMGDish alloc] init];
                [dish setValuesForKeysWithDictionary:dic];
                dish.dishId = [dic objectForKey:@"id"];
                dish.restaurantId = [responseObject objectForKey:@"restaurantId"];
                dish.createTime=[dic objectForKey:@"createTimestamp"];
                NSDictionary* imageInfo=[dic objectForKey:@"pictureInfo"];
                double width = [[imageInfo objectForKey:@"width"] doubleValue];
                double height = [[imageInfo objectForKey:@"height"] doubleValue];
                dish.imageHeight = [NSNumber numberWithInt:((DeviceWidth - 30)*height)/width];
                [dishArr addObject:dish];
            }
            
             IMGDish *dish = [[IMGDish alloc] init];
            if ([responseObject objectForKey:@"commentList"])
            {
                NSArray *commentArray=[responseObject objectForKey:@"commentList"];
               

                restaurant.commentCardList = [IMGDishComment commentListFromArray:commentArray andIsVendor:(bool)[responseObject objectForKey:@"isVendor"] andRestaurant:restaurant];
            }
            
            IMGUser *user = [[IMGUser alloc] init];
            [user setValuesForKeysWithDictionary:responseObject];
            user.reviewCount = [responseObject objectForKey:@"userReviewCount"];
            user.photoCount = [responseObject objectForKey:@"userPhotoCount"];
            
            [dicSubArrM addObject:restaurant];
            [dicSubArrM addObject:dish];
            [dicSubArrM addObject:user];
            [dicSubArrM addObject:dishArr];
            block(dicSubArrM);
        }

        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        
        
        
        
        
    }];









}
+(void)postCommentCard:(NSString *)commentStr moderateReviewId:(NSNumber*)moderateReviewId restaurantId:(NSNumber *)restaurantId andBlock:(void (^)(IMGDishComment *dishComment))block failure:(void(^)(NSString *errorReason))failureBlock{
    
    
    NSString *trimmedString = [commentStr stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        if (!(trimmedString.length==0)) {
            [[LoadingView sharedLoadingView] startLoading];
            NSDictionary *parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",moderateReviewId,@"moderateReviewId",restaurantId,@"restaurantId",commentStr,@"comment",[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"latitude",[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],@"longitude", nil];
            [[IMGNetWork sharedManager]POST:@"moderateReview/saveComment" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
             {
                 [[LoadingView sharedLoadingView] stopLoading];
                 NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
                 if(responseObject && errorMessage){
                     NSLog(@"postComment: error: %@",errorMessage);
                     failureBlock(errorMessage);
                 }else if(responseObject){
                     NSDictionary *commentDictionary = [responseObject objectForKey:@"commentInfo"];
                     if(commentDictionary){
                         IMGDishComment *dishComment = [[IMGDishComment alloc]initFromDictionary:commentDictionary];
                         block(dishComment);
                     }else{
                         block(nil);
                     }
                     
                 }
             }failure:^(NSURLSessionDataTask *operation, NSError *error)
             {
                 [[LoadingView sharedLoadingView] stopLoading];
                 failureBlock(error.localizedDescription);
                 NSLog(@"postCommentWithDish error: %@",error.localizedDescription);
             }];
        }
    }
    
}

+(void)getUploadPhotoCardData:(NSDictionary *)params andBlock:(void (^)(NSArray *dataArr))block
{
    [[LoadingView sharedLoadingView] startLoading];
    NSString *postUrl = @"dishphoto/carddetail";
    [[IMGNetWork sharedManager]POST:postUrl parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
         if(responseObject && errorMessage){
             [[LoadingView sharedLoadingView] stopLoading];
             NSLog(@"likeJournal: error: %@",errorMessage);
         }else if(responseObject){
             NSMutableArray *dicSubArrM = [[NSMutableArray alloc] init];
             IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
             restaurant.title = [responseObject objectForKey:@"restaurantTitle"];
             restaurant.restaurantId = [responseObject objectForKey:@"restaurantId"];
             restaurant.seoKeyword = [responseObject objectForKey:@"restaurantSeo"];
             restaurant.timeline = [responseObject objectForKey:@"timeline"];

             NSDictionary *subDic = [responseObject objectForKey:@"dishMap"];
             IMGDish *dish = [[IMGDish alloc] init];
             [dish setValuesForKeysWithDictionary:subDic];
             dish.dishId = [subDic objectForKey:@"id"];
             dish.restaurantSeo = [responseObject objectForKey:@"restaurantSeo"];
             dish.restaurantTitle = [responseObject objectForKey:@"restaurantTitle"];
             dish.restaurantId = [responseObject objectForKey:@"restaurantId"];
             dish.showWriteReview = [[responseObject objectForKey:@"showWriteReview"] boolValue];
             NSDictionary *pictureIndoDic = [subDic objectForKey:@"pictureInfo"];
             if (pictureIndoDic.count&&pictureIndoDic) {
                 double width = [[pictureIndoDic objectForKey:@"width"] doubleValue];
                 double height = [[pictureIndoDic objectForKey:@"height"] doubleValue];
                 dish.imageHeight = [NSNumber numberWithInt:((DeviceWidth - 30)*height)/width];
             }


             if ([dish.commentCount intValue])
             {
                 NSArray *commentArray=[subDic objectForKey:@"commentList"];
                 dish.commentList = [IMGDishComment commentListFromArray:commentArray andIsVendor:(bool)[responseObject objectForKey:@"isVendor"] andRestaurant:restaurant];
                 
             }
             
             IMGUser *user = [[IMGUser alloc] init];
             [user setValuesForKeysWithDictionary:responseObject];
             user.reviewCount = [responseObject objectForKey:@"userReviewCount"];
             user.photoCount = [responseObject objectForKey:@"userPhotoCount"];
             
             [dicSubArrM addObject:restaurant];
             [dicSubArrM addObject:dish];
             [dicSubArrM addObject:user];
             block(dicSubArrM);
             [[LoadingView sharedLoadingView] stopLoading];
         }
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         NSLog(@"getReviewCardData error: %@",error.localizedDescription);
     }];
}
@end
