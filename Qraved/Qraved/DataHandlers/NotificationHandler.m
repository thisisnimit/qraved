//
//  NotificationHandler.m
//  Qraved
//
//  Created by Lucky on 15/11/9.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import "NotificationHandler.h"
#import "IMGSplashNotification.h"
#import "IMGDish.h"
#import "SVProgressHUD.h"
@implementation NotificationHandler

+ (void)getNotificationListWithParams:(NSDictionary *)params andBlock:(void (^)(NSArray *dataArray))block  failure:(void(^)(NSString *exceptionMsg))failureBlock
{
    [[IMGNetWork sharedManager] GET:@"app/user/notificationList" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
        if(responseObject && errorMessage){
            failureBlock(errorMessage);
            return;
        }

        NSMutableArray *dataArrM = [[NSMutableArray alloc] init];
        NSArray *notificationList = [responseObject objectForKey:@"notificationList"];
        for (NSDictionary *listDic in notificationList)
        {
            int type = [[listDic objectForKey:@"type"] intValue];
            switch (type)
            {
                case 0:
                {
                    IMGNotifications *notifications = [[IMGNotifications alloc] init];
                    [notifications setValuesForKeysWithDictionary:listDic];
                    NSDictionary *dataDic = [listDic objectForKey:@"data"];
                    [notifications setValuesForKeysWithDictionary:dataDic];
                    [dataArrM addObject:notifications];
                }
                    break;
                    
                case 1:
                {
                    IMGNotifications *notifications = [[IMGNotifications alloc] init];
                    [notifications setValuesForKeysWithDictionary:listDic];
                    NSDictionary *dataDic = [listDic objectForKey:@"data"];
                    [notifications setValuesForKeysWithDictionary:dataDic];
                    NSDictionary *landingInfo = [dataDic objectForKey:@"landingInfo"];
                    [notifications setValuesForKeysWithDictionary:landingInfo];
                    [dataArrM addObject:notifications];
                }
                    break;
                    
                case 2:
                {
                    IMGNotifications *notifications = [[IMGNotifications alloc] init];
                    [notifications setValuesForKeysWithDictionary:listDic];
                    NSDictionary *dataDic = [listDic objectForKey:@"data"];
                    [notifications setValuesForKeysWithDictionary:dataDic];
                    NSDictionary *splashInfo = [dataDic objectForKey:@"splashInfo"];
                    [notifications setValuesForKeysWithDictionary:splashInfo];
                    [notifications setValue:[splashInfo objectForKey:@"id"] forKey:@"splashId"];
                    [notifications setValue:[splashInfo objectForKey:@"description"] forKey:@"splashDescription"];
                    [dataArrM addObject:notifications];
                    
                }
                    break;
                    
                case 3:
                {
                    IMGNotifications *notifications = [[IMGNotifications alloc] init];
                    [notifications setValuesForKeysWithDictionary:listDic];
                    NSDictionary *dataDic = [listDic objectForKey:@"data"];
                    [notifications setValuesForKeysWithDictionary:dataDic];
                    NSDictionary *bookingInfo = [dataDic objectForKey:@"bookingInfo"];
                    [notifications setValuesForKeysWithDictionary:bookingInfo];
                    [dataArrM addObject:notifications];
                    
                }
                    break;
                    
                case 4:
                {
                    IMGNotifications *notifications = [[IMGNotifications alloc] init];
                    [notifications setValuesForKeysWithDictionary:listDic];
                    NSDictionary *dataDic = [listDic objectForKey:@"data"];
                    [notifications setValuesForKeysWithDictionary:dataDic];
                    NSDictionary *restaurantInfo = [dataDic objectForKey:@"restaurantInfo"];
                    [notifications setValuesForKeysWithDictionary:restaurantInfo];
                    [dataArrM addObject:notifications];
                }
                    break;
                    
                case 5:
                {
                    IMGNotifications *notifications = [[IMGNotifications alloc] init];
                    [notifications setValuesForKeysWithDictionary:listDic];
                    NSDictionary *dataDic = [listDic objectForKey:@"data"];
                    [notifications setValuesForKeysWithDictionary:dataDic];
                    NSDictionary *reviewInfo = [dataDic objectForKey:@"reviewInfo"];
                    notifications.commentContext = [reviewInfo objectForKey:@"reviewCommentContext"];
                    notifications.isVendor = [[reviewInfo objectForKey:@"isVendor"] boolValue];
                    notifications.restaurantName = [reviewInfo objectForKey:@"restaurantTitle"];
                    [notifications setValuesForKeysWithDictionary:reviewInfo];
                    [dataArrM addObject:notifications];
                    
                }
                    break;
                case 6:
                {
                    IMGNotifications *notifications = [[IMGNotifications alloc] init];
                    [notifications setValuesForKeysWithDictionary:listDic];
                    NSDictionary *dataDic = [listDic objectForKey:@"data"];
                    [notifications setValuesForKeysWithDictionary:dataDic];
                    NSDictionary *dishInfo = [dataDic objectForKey:@"dishInfo"];
                    [notifications setValuesForKeysWithDictionary:dishInfo];
                    notifications.commentContext = [dishInfo objectForKey:@"dishCommentContext"];
                    notifications.isVendor = [[dishInfo objectForKey:@"isVendor"] boolValue];
                    notifications.restaurantName = [dishInfo objectForKey:@"restaurantTitle"];
                    NSMutableArray *dishArrM = [[NSMutableArray alloc] init];
                    for (NSDictionary *dic in [dishInfo objectForKey:@"dishList"])
                    {
                        IMGDish *dish = [[IMGDish alloc] init];
                        [dish setValuesForKeysWithDictionary:dic];
                        dish.imageUrl = [dic objectForKey:@"dishUrl"];
                        [dishArrM addObject:dish];
                    }
                    notifications.dishList = [NSMutableArray arrayWithArray:dishArrM];
                    [dataArrM addObject:notifications];
                }
                    break;
                    
                case 7:
                {
                    IMGNotifications *notifications = [[IMGNotifications alloc] init];
                    [notifications setValuesForKeysWithDictionary:listDic];
                    NSDictionary *dataDic = [listDic objectForKey:@"data"];
                    [notifications setValuesForKeysWithDictionary:dataDic];
                    NSDictionary* uploadPhotoInfoDic=[[NSMutableDictionary alloc]init];
                    uploadPhotoInfoDic=[dataDic objectForKey:@"uploadPhotoInfo"];
                    [notifications setValuesForKeysWithDictionary:uploadPhotoInfoDic];
                    if ([uploadPhotoInfoDic objectForKey:@"dishList"]) {
                        NSMutableArray *dishArrM = [[NSMutableArray alloc] init];
                        for (NSDictionary *dic in [uploadPhotoInfoDic objectForKey:@"dishList"])
                        {
                            IMGDish *dish = [[IMGDish alloc] init];
                            [dish setValuesForKeysWithDictionary:dic];
                            [dishArrM addObject:dish];
                        }
                        notifications.dishList = [NSMutableArray arrayWithArray:dishArrM];
                    }
                    
                    
                    [dataArrM addObject:notifications];
                    
                    
                }
                    break;
                case 12:{
                    IMGNotifications *notifications = [[IMGNotifications alloc] init];
                    [notifications setValuesForKeysWithDictionary:listDic];
                    NSDictionary *dataDic = [listDic objectForKey:@"data"];
                    [notifications setValuesForKeysWithDictionary:dataDic];
                    [dataArrM addObject:notifications];
                    break;
                }
            
                default:
                    break;
            }
        }
        block(dataArrM);
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        failureBlock(error.localizedDescription);
        NSString* noticeStr=L(@"Please check your internet connection and try again.");
        [SVProgressHUD showImage:[UIImage imageNamed:@""] status:noticeStr];
        [SVProgressHUD dismissWithDelay:1.5];
        NSLog(@"getNotificationList error:%@",error.localizedDescription);
    }];
}

+ (void)readSplashWithParams:(NSDictionary *)params
{
    [[IMGNetWork sharedManager] GET:@"app/user/readSplash" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"readSplashNotification error:%@",error.localizedDescription);
    }];
}
+ (void)readNotificationWithParams:(NSDictionary *)params
{
    [[IMGNetWork sharedManager] GET:@"app/user/readNotification" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSString* noticeStr=L(@"Please check your internet connection and try again.");
        [SVProgressHUD showImage:[UIImage imageNamed:@""] status:noticeStr];
        [SVProgressHUD dismissWithDelay:1.5];
        NSLog(@"readNotification error:%@",error.localizedDescription);
    }];
}
+ (void)getNotificationCountWithParams:(NSDictionary *)params andBlock:(void (^)(NSNumber *notificationCount,NSString *exceptionmsg))block
{
    [[IMGNetWork sharedManager] GET:@"app/user/notificationCount" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        block([responseObject objectForKey:@"count"],[responseObject objectForKey:@"exceptionmsg"]);
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"getNotificationCount error:%@",error.localizedDescription);
    }];
}
+ (void)updateNotificationCountWithParams:(NSDictionary *)params andBlock:(void (^)(id responseObject))block
{
    [[IMGNetWork sharedManager] GET:@"push/badge/update" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        block(responseObject);
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"getNotificationCount error:%@",error.localizedDescription);
    }];
}
+ (void)getUserNotificationSetByUserId:(NSDictionary *)params andBlock:(void (^)(id responseObject))block
{
    [[IMGNetWork sharedManager] GET:@"service/userEdmSetting/getUserEdmSettingByUserId" parameters:params success:^(NSURLSessionDataTask *operation, id responseObject){
        block(responseObject);
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"getUserEdmSettingByUserId error:%@",error.localizedDescription);
    }];
}
+ (void)userNotificationSetSaveAndUpdate:(NSDictionary *)params andBlock:(void (^)(id responseObject))block
{
    [[IMGNetWork sharedManager] GET:@"service/userEdmSetting/saveAndUpdate" parameters:params success:^(NSURLSessionDataTask *operation, id responseObject){
        block(responseObject);
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"service/userEdmSetting/saveAndUpdate error:%@",error.localizedDescription);
    }];
}


@end
