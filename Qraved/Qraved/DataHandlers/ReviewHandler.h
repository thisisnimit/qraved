//
//  ReviewHandler.h
//  Qraved
//
//  Created by lucky on 16/3/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "DataHandler.h"
#import "IMGReview.h"
#import "IMGReviewComment.h"

@interface ReviewHandler : DataHandler

+(void)previousComment:(NSString *)commentIdStr targetId:(NSNumber*)articleId andBlock:(void (^)(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount))block;

+(void)postComment:(NSString *)commentStr reviewId:(NSNumber*)reviewId restaurantId:(NSNumber *)restaurantId andBlock:(void (^)(IMGReviewComment *reviewComment))block failure:(void(^)(NSString *errorReason))failureBlock;

+(void)likeReview:(BOOL)liked reviewId:(NSNumber*)reviewId andBlock:(void (^)())block  failure:(void(^)(NSString *exceptionMsg))failureBlock;

+(void)getReviewCardData:(NSDictionary *)params andBlock:(void (^)(NSArray *dataArr))block;
@end
