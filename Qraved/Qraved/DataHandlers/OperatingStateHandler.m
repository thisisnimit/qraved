//
//  SettingHandler
//  Qraved
//
//  Created by Jeff on 10/8/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "OperatingStateHandler.h"
#import "DBManager.h"
#import "IMGOperatingState.h"

@implementation OperatingStateHandler

-(void)getDatasFromServer
{
    
    [[IMGNetWork sharedManager] GET:@"operatingstate/list" parameters:nil progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueue];
         [queue inDeferredTransaction:^(FMDatabase *db, BOOL *rollback) {
             [db executeUpdate:@"delete from IMGOperatingState"];
             NSArray *array = responseObject;
             for (int i=0; i<array.count; i++) {
                 NSArray *operatingStateArray = [array objectAtIndex:i];
                 IMGOperatingState *operatingState = [[IMGOperatingState alloc]init];
                 operatingState.color=[operatingStateArray objectAtIndex:0];
                 operatingState.stateName=[operatingStateArray objectAtIndex:1];
                 operatingState.sortOrder=[operatingStateArray objectAtIndex:2];
                 operatingState.stateId=[operatingStateArray objectAtIndex:3];
                 [[DBManager manager]insertModel:operatingState andFMDatabase:db];
             }
             NSLog(@"OperatingStateHandler update datas done");
         }];
     }failure:^(NSURLSessionDataTask *operation, NSError *error) {
         NSLog(@"AFHTTPRequestOperation erors when get datas:%@",error.localizedDescription);
     }];
    
}


@end
