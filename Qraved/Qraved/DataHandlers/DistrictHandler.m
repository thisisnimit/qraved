//
//  DistrictHandler.m
//  Qraved
//
//  Created by Jeff on 10/8/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "DistrictHandler.h"
#import "DBManager.h"
#import "IMGDistrict.h"

@implementation DistrictHandler

-(void)getDatasFromServer
{
    [[IMGNetWork sharedManager] GET:@"app/search/filter/district/list" parameters:nil progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSArray *array = responseObject;
         [[DBManager manager] deleteWithSql:@"delete from IMGDistrict"];
         for (int i=0; i<array.count; i++) {
             NSDictionary *dict = [array objectAtIndex:i];
             IMGDistrict *district = [[IMGDistrict alloc]init];
             [district setValuesForKeysWithDictionary:dict];
             [dataArray addObject:district];
             [[DBManager manager]insertModel:district selectField:@"districtId" andSelectID:district.districtId];
         }
     }failure:^(NSURLSessionDataTask *operation, NSError *error) {
         NSLog(@"AFHTTPRequestOperation erors when get datas:%@",error.localizedDescription);
     }];
    
}


@end
