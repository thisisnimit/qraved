//
//  ReviewHandler.m
//  Qraved
//
//  Created by lucky on 16/3/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "ReviewHandler.h"
#import "IMGUser.h"
#import "LoadingView.h"
#import "IMGRestaurant.h"
#import "IMGDish.h"
#import "IMGDishComment.h"
#import "Amplitude.h"

@implementation ReviewHandler

+(void)previousComment:(NSString *)commentIdStr targetId:(NSNumber*)articleId andBlock:(void (^)(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount))block{
    
    NSDictionary *parameters;
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userId",articleId,@"reviewId",commentIdStr,@"lastReviewCommentId", nil];
    }
    else
    {
        parameters=[[NSDictionary alloc]initWithObjectsAndKeys:articleId,@"reviewId",commentIdStr,@"lastReviewCommentId", nil];
    }
    [[IMGNetWork sharedManager]GET:@"review/comments/previous" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
         if(responseObject && errorMessage){
             NSLog(@"previous comment: error: %@",errorMessage);
         }else if(responseObject){
             NSMutableArray *commentList = [[NSMutableArray alloc]initWithCapacity:0];
             IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
             NSDictionary *restaurantDic = [responseObject objectForKey:@"restaurant"];
             restaurant.restaurantId = [restaurantDic objectForKey:@"id"];
             restaurant.imageUrl = [restaurantDic objectForKey:@"imageUrl"];
             restaurant.title = [restaurantDic objectForKey:@"title"];

             NSArray *commentArray = [responseObject objectForKey:@"commentList"];
             commentList=[IMGReviewComment commentListFromArray:commentArray andIsVendor:[[responseObject objectForKey:@"isVendor"] boolValue] andRestaurant:restaurant];
             
             NSNumber *commentCount = [responseObject objectForKey:@"commentCount"];
             NSNumber *likeCount = [responseObject objectForKey:@"likeCount"];
             block(commentList,likeCount,commentCount);
             
         }
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         NSLog(@"getRestaurantFirstDataFromService error: %@",error.localizedDescription);
     }];
    
}

+(void)postComment:(NSString *)commentStr reviewId:(NSNumber*)reviewId restaurantId:(NSNumber *)restaurantId andBlock:(void (^)(IMGReviewComment *reviewComment))block failure:(void(^)(NSString *errorReason))failureBlock
{
    NSString *trimmedString = [commentStr stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
        if (!(trimmedString.length==0)) {
            [[LoadingView sharedLoadingView] startLoading];
            NSDictionary *parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",reviewId,@"reviewId",commentStr,@"comment",restaurantId,@"restaurantId",[NSNumber numberWithInt:1],@"status", [[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"latitude",[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],@"longitude",nil];
            NSLog(@"%f,getCommentData 1.0",[[NSDate date]timeIntervalSince1970]);

            [[IMGNetWork sharedManager]POST:@"review/comment/writev2" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
             {
                 NSLog(@"%f,getCommentData 2.0",[[NSDate date]timeIntervalSince1970]);
                 [[LoadingView sharedLoadingView] stopLoading];
                 NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
                 if(responseObject && errorMessage){
                     NSLog(@"postComment: error: %@",errorMessage);
                     failureBlock(errorMessage);
                 }else if(responseObject){
                     NSDictionary *commentDictionary = [responseObject objectForKey:@"commentInfo"];
                     if(commentDictionary){
                         IMGReviewComment *reviewComment = [[IMGReviewComment alloc]initFromDictionary:commentDictionary];
                         block(reviewComment);
                     }else{
                         block(nil);
                     }
                     
                 }
             }failure:^(NSURLSessionDataTask *operation, NSError *error)
             {
                 [[LoadingView sharedLoadingView] stopLoading];
                 NSLog(@"postCommentWithReview error: %@",error.localizedDescription);
                 failureBlock(error.localizedDescription);
             }];
        }
    }

}

+(void)likeReview:(BOOL)liked reviewId:(NSNumber*)reviewId andBlock:(void (^)())block  failure:(void(^)(NSString *exceptionMsg))failureBlock
{
    NSString *likeOrUnlikeStr = @"review/like";
    if(liked){
        likeOrUnlikeStr = @"review/unlike";
    }
    
    IMGUser *user = [IMGUser currentUser];
    if (user.userId && user.token)
    {
//        [[LoadingView sharedLoadingView] startLoading];
        NSDictionary *parameters=[[NSDictionary alloc]initWithObjectsAndKeys:user.userId,@"userId",user.token,@"t",reviewId,@"id", [[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"latitude",        [[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],@"longitude",nil];
        [[IMGNetWork sharedManager]POST:likeOrUnlikeStr parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
         {
//             [[LoadingView sharedLoadingView] stopLoading];
             NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
             if(responseObject && errorMessage){
                 failureBlock(errorMessage);
                 NSLog(@"likeJournal: error: %@",errorMessage);
             }else if(responseObject){
                 block();
             }
         }failure:^(NSURLSessionDataTask *operation, NSError *error)
         {
             failureBlock(error.localizedDescription);
//             [[LoadingView sharedLoadingView] stopLoading];
             NSLog(@"likeReview error: %@",error.localizedDescription);
         }];
        
    }
}
+(void)getReviewCardData:(NSDictionary *)params andBlock:(void (^)(NSArray *dataArr))block
{
    [[LoadingView sharedLoadingView] startLoading];
    NSString *postUrl = @"home/review/carddetail";
    [[IMGNetWork sharedManager]POST:postUrl parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
         if(responseObject && errorMessage){
             [[LoadingView sharedLoadingView] stopLoading];
             NSLog(@"likeJournal: error: %@",errorMessage);
         }else if(responseObject){
             NSMutableArray *dicSubArrM = [[NSMutableArray alloc] init];
             IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
             restaurant.title = [responseObject objectForKey:@"restaurantTitle"];
             [restaurant setValuesForKeysWithDictionary:responseObject];
             restaurant.seoKeyword = [responseObject objectForKey:@"restaurantSeo"];
             IMGReview *review = [[IMGReview alloc] init];
             [review setValuesForKeysWithDictionary:[responseObject objectForKey:@"reviewMap"]];
             review.fullName = [[responseObject objectForKey:@"reviewMap"] objectForKey:@"userName"];
             review.restaurantTitle = [responseObject objectForKey:@"restaurantTitle"];
             review.userDishCount = [responseObject objectForKey:@"userPhotoCount"];
             review.userReviewCount = [responseObject objectForKey:@"userReviewCount"];
             review.timeline = [responseObject objectForKey:@"timeline"];
             review.summarize = [review.summarize stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
             NSArray *commentArr = [[responseObject objectForKey:@"reviewMap"] objectForKey:@"commentList"];
             review.commentList = [IMGReviewComment commentListFromArray:commentArr andIsVendor:[[responseObject objectForKey:@"isVendor"] boolValue] andRestaurant:restaurant];
             
             NSMutableArray *dishArrM = [[NSMutableArray alloc] init];
             NSArray *dishArr = [[responseObject objectForKey:@"reviewMap"] objectForKey:@"dishList"];
             for (NSDictionary *subDic in dishArr)
             {
                 IMGDish *dish = [[IMGDish alloc] init];
                 [dish setValuesForKeysWithDictionary:subDic];
                 NSDictionary* imageInfo=[subDic objectForKey:@"pictureInfo"];
                 double width = [[imageInfo objectForKey:@"width"] doubleValue];
                 double height = [[imageInfo objectForKey:@"height"] doubleValue];
                 dish.imageHeight = [NSNumber numberWithInt:((DeviceWidth - 30)*height)/width];
                 [dishArrM addObject:dish];
             }
             
             [dicSubArrM addObject:restaurant];
             [dicSubArrM addObject:review];
             [dicSubArrM addObject:dishArrM];
             block(dicSubArrM);
             [[LoadingView sharedLoadingView] stopLoading];
         }
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         [[LoadingView sharedLoadingView] stopLoading];
         NSLog(@"getReviewCardData error: %@",error.localizedDescription);
     }];
}


@end
