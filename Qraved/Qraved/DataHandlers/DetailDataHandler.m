//
//  DetailDataHandler.m
//  Qraved
//
//  Created by Lucky on 15/3/30.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "DetailDataHandler.h"
#import "IMGMenu.h"
#import "IMGTag.h"
#import "IMGDiningGuide.h"
#import "BookUtil.h"
#import "IMGEvent.h"
#import "IMGUser.h"
#import "RestaurantHandler.h"
#import "IMGRestaurantTag.h"
#import "IMGMediaComment.h"
#import "IMGRDPRestaurant.h"
#import "AppDelegate.h"
#import "DetailSeeAllPhotoQravedModel.h"
#import "DetailSeeAllPhotoInstagramModel.h"
#import "DetailSeeMenuBarModel.h"
#import "DetailSeeMenuDeleveryModel.h"
#import "DetailSeeMenuRestaurantModel.h"
@implementation DetailDataHandler

+ (NSMutableArray *)getChefRecommendsData:(NSNumber *)restaurantID
{
    NSMutableArray *menusArray=[[NSMutableArray alloc]initWithCapacity:0];
    FMResultSet *resultSet=[[DBManager manager]executeQuery:[NSString stringWithFormat:@"SELECT menuId,title,currencyName,descriptionStr,price,sectionId,sectionName,sectionTypeName FROM IMGMenu WHERE restaurantId = '%@' and dish=1 ORDER BY sectionTypeSortOrder, sectionTypeName;",restaurantID]];
    while ([resultSet next])
    {
        IMGMenu  *menu=[[IMGMenu alloc]init];
        [menu setValueWithResultSet:resultSet];
        [menusArray addObject:menu];
    }
    [resultSet close];
    return menusArray;
}

+ (NSMutableArray *)getGreatData:(NSNumber *)restaurantID
{
    NSMutableArray *occasionArray = [[NSMutableArray alloc]initWithCapacity:0];
    
    NSMutableArray *tagIdArray = [[NSMutableArray alloc]initWithCapacity:0];
    FMResultSet *resultSet1=[[DBManager manager]executeQuery:[NSString stringWithFormat:@"select tagId from IMGRestaurantTag where restaurantId=%@",restaurantID]];
    while([resultSet1 next]){
        [tagIdArray addObject:[resultSet1 objectForColumnName:@"tagId"]];
        
    }
    [resultSet1 close];
    
    FMResultSet *resultSet2=[[DBManager manager]executeQuery:@"select * from IMGTag t where t.type='Occasion' ORDER BY t.ranking;"];
    while([resultSet2 next]){
        for(NSNumber *tagId in tagIdArray){
            if([[resultSet2 objectForColumnName:@"tagId"] intValue]==[tagId intValue]){
                IMGTag *tag = [[IMGTag alloc]init];
                [tag setValueWithResultSet:resultSet2];
                [occasionArray addObject:tag];
            }
        }
    }
    [resultSet2 close];
    return occasionArray;
}

+ (NSMutableArray *)getFeaturesData:(NSNumber *)restaurantID
{
    NSMutableArray *tagNameArray = [[NSMutableArray alloc]initWithCapacity:0];
    
    NSMutableArray *tagIdArray = [[NSMutableArray alloc]initWithCapacity:0];
    FMResultSet *resultSet1=[[DBManager manager]executeQuery:[NSString stringWithFormat:@"select tagId from IMGRestaurantTag where restaurantId=%@",restaurantID]];
    while([resultSet1 next]){
        [tagIdArray addObject:[resultSet1 objectForColumnName:@"tagId"]];
    }
    [resultSet1 close];
    
    FMResultSet *resultSet2=[[DBManager manager]executeQuery:@"select tagId,name from IMGTag t where t.type='Features' ORDER BY t.ranking;"];
    while([resultSet2 next]){
        for(NSNumber *tagId in tagIdArray){
            if([[resultSet2 objectForColumnName:@"tagId"] intValue]==[tagId intValue]){
                [tagNameArray addObject:[resultSet2 stringForColumn:@"name"]];
            }
        }
    }
    [resultSet2 close];
    return tagNameArray;
}

+ (NSMutableArray *)getFeaturedInData:(NSNumber *)restaurantID
{
    NSMutableArray *diningGuideArray = [[NSMutableArray alloc]initWithCapacity:0];
    NSString *todayString = [BookUtil todayStringWithFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM IMGDiningGuide dg left join IMGRestaurantDiningGuide rdg on dg.diningGuideId=rdg.diningGuideId WHERE rdg.restaurantId = '%@' ORDER BY dg.sortOrder;",restaurantID];
    [[DBManager manager]selectWithSql:sql successBlock:^(FMResultSet *resultSet) {
        while ([resultSet next]) {
            IMGDiningGuide  *diningGuide=[[IMGDiningGuide alloc]init];
            [diningGuide setValueWithResultSet:resultSet];
            [diningGuideArray addObject:diningGuide];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"addFeaturedInView error when get datas:%@",error.description);
    }];
    
    sql = [NSString stringWithFormat:@"SELECT * FROM IMGEvent e left join IMGRestaurantEvent re on e.eventId=re.eventId WHERE re.restaurantId = '%@'  and e.startDate<='%@' and e.endDate>='%@' ORDER BY e.sortOrder;",restaurantID,todayString,todayString];
    
    NSMutableArray *eventArray = [[NSMutableArray alloc]initWithCapacity:0];
    [[DBManager manager]selectWithSql:sql successBlock:^(FMResultSet *resultSet) {
        while ([resultSet next]) {
            IMGEvent  *event=[[IMGEvent alloc]init];
            [event setValueWithResultSet:resultSet];
            [eventArray addObject:event];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"addFeaturedInView error when get datas:%@",error.description);
    }];
    
    return nil;
}
+(void)getRestaurantDetailInfoWithResturantID:(NSNumber*)restaurantID andBlock:(void (^)(id respondes))block withError:(void(^)(NSError* error))errorBlock{

    NSDictionary * parameters;
    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        parameters = @{@"restaurantId":restaurantID};
    }
    else
    {
        parameters = @{@"userId":user.userId,@"t":user.token,@"restaurantId":restaurantID};
    }
    [[IMGNetWork sharedManager]POST:@"app/restaurantbasicdetail/v2" parameters:parameters success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSLog(@"%f,getRestaurantFirstData 2.0",[[NSDate date]timeIntervalSince1970]);
         
         block(responseObject);
         
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         errorBlock(error);
         NSLog(@"getRestaurantFirstDataFromService error: %@",error.localizedDescription);
     }];
}
+ (void)getRestaurantFirstDataFromServiceNow:(NSNumber *)restaurantID andBlock:(void (^)(IMGRestaurant *restaurant,NSArray *eventList,IMGRestaurantOffer *restaurantOffer,NSArray *banerImageArr,IMGRDPRestaurant *rdpRest,NSNumber *showClaim,NSArray *couponArray))block{

    NSDictionary * parameters;
    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        parameters = @{@"restaurantId":restaurantID};
    }
    else
    {
        parameters = @{@"userId":user.userId,@"t":user.token,@"restaurantId":restaurantID};
    }
    
    [[IMGNetWork sharedManager]POST:@"app/restaurantbasicdetail/v3" parameters:parameters success:^(NSURLSessionDataTask *operation, id responseObject)
     {
//         NSLog(@"%f,getRestaurantFirstData 2.0",[[NSDate date]timeIntervalSince1970]);
         NSDictionary * exceptionDic = (NSDictionary*)responseObject;

         if ([[exceptionDic allKeys] containsObject:@"exceptionmsg"]) {
             return ;
         }
         IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    
         NSArray* restaurantArr=[RestaurantHandler updateForDetailViewControllerNow:responseObject forRestaurant:restaurant];
         restaurant=[restaurantArr objectAtIndex:0];
         restaurant.landMarkList = [responseObject objectForKey:@"landMarkList"];
         
         NSArray* eventArr=[RestaurantHandler updateForDetailViewControllerNow:responseObject forRestaurant:restaurant];
        NSArray *eventList = [eventArr objectAtIndex:1];

         NSArray *couponArr = [self getCouponsFromResponseObject:responseObject];
         
        IMGRestaurantOffer *restaurantOffer = [self getOffersFromResponseObjectNow:responseObject];
         NSLog(@"%f,getRestaurantFirstData 3.0",[[NSDate date]timeIntervalSince1970]);
         
         
         IMGRDPRestaurant *rdpRest = [[IMGRDPRestaurant alloc] init];
         rdpRest.restaurantId = restaurant.restaurantId;
         rdpRest.RDPFirstDataArr = responseObject;
         
         NSNumber *showClaim = [responseObject objectForKey:@"showClaim"];
         
         block(restaurant,eventList,restaurantOffer,[responseObject objectForKey:@"imageList"],rdpRest,showClaim,couponArr);
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"getRestaurantFirstDataFromService error: %@",error.localizedDescription);
     }];
}
//+ (void)getRestaurantFirstDataFromService:(NSNumber *)restaurantID andBlock:(void (^)(IMGRestaurant *restaurant,NSArray *eventList,IMGRestaurantOffer *restaurantOffer,NSArray *banerImageArr,IMGRDPRestaurant *rdpRest))block
//{
//    NSDictionary * parameters;
//    IMGUser *user=[IMGUser currentUser];
//    if(user==nil || user.userId==nil || user.token==nil){
//        parameters = @{@"restaurantId":restaurantID};
//    }
//    else
//    {
//        parameters = @{@"userId":user.userId,@"t":user.token,@"restaurantId":restaurantID};
//    }
//    
//    [[IMGNetWork sharedManager]POST:@"app/restaurantbasicdetail" parameters:parameters success:^(NSURLSessionDataTask *operation, id responseObject)
//     {
//         NSLog(@"%f,getRestaurantFirstData 2.0",[[NSDate date]timeIntervalSince1970]);
//         IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
//         restaurant = [RestaurantHandler updateForDetailViewController:responseObject forRestaurant:restaurant][0];
//         NSArray *eventList = [RestaurantHandler updateForDetailViewController:responseObject forRestaurant:restaurant][1];
//         IMGRestaurantOffer *restaurantOffer = [self getOffersFromResponseObject:responseObject];
//         NSLog(@"%f,getRestaurantFirstData 3.0",[[NSDate date]timeIntervalSince1970]);
//         
//         
//         IMGRDPRestaurant *rdpRest = [[IMGRDPRestaurant alloc] init];
//         rdpRest.restaurantId = restaurant.restaurantId;
//         rdpRest.RDPFirstDataArr = responseObject;
//
//         block(restaurant,eventList,restaurantOffer,[responseObject objectAtIndex:13],rdpRest);
//         
//     }failure:^(NSURLSessionDataTask *operation, NSError *error)
//     {
//         NSLog(@"getRestaurantFirstDataFromService error: %@",error.localizedDescription);
//     }];
//}
+ (IMGRestaurantOffer *)getOffersFromResponseObjectNow:(NSDictionary *)responseObject
{
    
    if (![[responseObject objectForKey:@"offerMap"] isKindOfClass:[NSNull class]])
    {
        NSDictionary *dic = [[NSDictionary alloc] initWithDictionary:[responseObject objectForKey:@"offerMap"]];
        if (![dic count])
        {
            return nil;
        }
        IMGRestaurantOffer *restaurantOffer = [[IMGRestaurantOffer alloc] init];
        [restaurantOffer setValuesForKeysWithDictionary:dic];
        restaurantOffer.tagLine = [dic objectForKey:@"offerTagLine"];
        if (restaurantOffer)
        {
            return restaurantOffer;
        }
    }
    
    return nil;
}

+ (NSArray *)getCouponsFromResponseObject:(NSDictionary *)responseObject{
    if (![[responseObject objectForKey:@"couponListCTA"] isKindOfClass:[NSNull class]]) {
        NSMutableArray *couponArr = [NSMutableArray array];
        for (NSDictionary *dic in [responseObject objectForKey:@"couponListCTA"]) {
            if (![Tools isBlankString:[dic objectForKey:@"strip_banner_image_url"]]) {
                CouponModel *model = [[CouponModel alloc] init];
                [model setValuesForKeysWithDictionary:dic];
                [couponArr addObject:model];
            }
        }
        return couponArr;
    }
    return nil;
}


//+ (IMGRestaurantOffer *)getOffersFromResponseObject:(NSArray *)responseObject
//{
//    
//    if (![[responseObject objectAtIndex:2] isKindOfClass:[NSNull class]])
//    {
//        NSDictionary *dic = [[NSDictionary alloc] initWithDictionary:[responseObject objectAtIndex:2]];
//        if (![dic count])
//        {
//            return nil;
//        }
//        IMGRestaurantOffer *restaurantOffer = [[IMGRestaurantOffer alloc] init];
//        [restaurantOffer setValuesForKeysWithDictionary:dic];
//        restaurantOffer.tagLine = [dic objectForKey:@"offerTagLine"];
//        if (restaurantOffer)
//        {
//            return restaurantOffer;
//        }
//    }
//    
//    return nil;
//}

+ (void)getRestaurantSecondDataFromService:(NSNumber *)restaurantId and:(IMGRDPRestaurant*)rdpRest andBlock:(void (^)(NSArray *menuList,NSArray *dishList,NSArray *moreDishList,NSArray *reviewList,NSArray *eventList,NSArray *diningGuideList,NSArray *journalList,NSNumber *menuPhotoCount,NSNumber *dishCount,NSNumber *reviewedUserCount,IMGRDPRestaurant *rdpCache))block andBlockEventCountWithDiningGuideCount:(void (^)(NSNumber *eventCount,NSNumber *diningGuideCount,NSNumber *journalCount))countBlock
{
    NSDictionary * parameters;
    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        parameters = @{@"id":restaurantId};
    }
    else
    {
        parameters = @{@"userId":user.userId,@"t":user.token,@"id":restaurantId};
    }
    
    [[IMGNetWork sharedManager]POST:@"app/restaurant/v6" parameters:parameters success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSLog(@"%f,getRestaurantSecondData 2.0",[[NSDate date]timeIntervalSince1970]);

         NSArray *menuArr = [[NSArray alloc] init];
         menuArr = [responseObject objectForKey:@"menuPhotoList"];

         NSArray *dishArr = [[NSArray alloc] init];
         dishArr = [responseObject objectForKey:@"dishList"];
         
         NSArray *moreDishArr = [[NSArray alloc] init];
         moreDishArr = [responseObject objectForKey:@"moreDishList"];
         
         NSArray *reviewArr = [[NSArray alloc] init];
         reviewArr = [responseObject objectForKey:@"reviewList"];
         
         NSArray *eventArr = [[NSArray alloc] init];
         eventArr = [responseObject objectForKey:@"eventList"];
         
         NSArray *diningGuideArr = [[NSArray alloc] init];
         diningGuideArr = [responseObject objectForKey:@"diningGuideList"];
         
         NSArray *journalArr = [[NSArray alloc] init];
         journalArr = [responseObject objectForKey:@"journalList1"];

         NSNumber *menuPhotoCount=[responseObject objectForKey:@"menuPhotoCount"];
         NSNumber *dishCount = [responseObject objectForKey:@"dishCount"];
         
         
//         IMGRDPRestaurant *rdpRest = [[IMGRDPRestaurant alloc] init];
         rdpRest.restaurantId = restaurantId;
         rdpRest.menuList = menuArr;
         rdpRest.dishList = dishArr;
         rdpRest.moreDishList = moreDishArr;
         rdpRest.reviewList = [NSMutableArray arrayWithArray:reviewArr];
         rdpRest.eventList = eventArr;
         rdpRest.diningGuideList = diningGuideArr;
         rdpRest.journalList = journalArr;
         rdpRest.menuPhotoCount = menuPhotoCount;
         rdpRest.dishCount = dishCount;
         rdpRest.eventCount = [responseObject objectForKey:@"eventCount"];
         rdpRest.diningGuideCount = [responseObject objectForKey:@"diningGuideCount"];
         rdpRest.journalCount = [responseObject objectForKey:@"journalCount1"];
         rdpRest.instagramLocationId = [responseObject objectForKey:@"instagramLocationId"];
//         NSMutableData *data = [NSMutableData data];
//         NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc]initForWritingWithMutableData:data];
//         [archiver encodeObject:rdpRest forKey:[NSString stringWithFormat:@"%@",rdpRest.restaurantId]];
//         [archiver finishEncoding];
//         //将数据写入文件里
//         [data writeToFile:RDPCacheFilePath atomically:YES];

         NSNumber *reviewedUserCount =[responseObject objectForKey:@"reviewedUserCount"];
         block(menuArr,dishArr,moreDishArr,reviewArr,eventArr,diningGuideArr,journalArr,menuPhotoCount,dishCount,reviewedUserCount,rdpRest);
         countBlock([responseObject objectForKey:@"eventCount"],[responseObject objectForKey:@"diningGuideCount"],[responseObject objectForKey:@"journalCount1"]
            );
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"getRestaurantSecondDataFromService error: %@",error.localizedDescription);
     }];

}


+ (void)getMoreInfoFromService:(IMGRestaurant *)restaurant andBlock:(void (^)())block
{
    
    NSDictionary * parameters = [[NSDictionary alloc] init];
    parameters = @{@"restaurantId":restaurant.restaurantId};
    
    [[IMGNetWork sharedManager]POST:@"app/detail/restauraninfo" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSString *sql=[NSString stringWithFormat:@"delete from IMGRestaurantTag where restaurantId='%@'",restaurant.restaurantId];
         [[DBManager manager] deleteWithSql:sql];
         
         NSArray *tagsArray = [responseObject objectForKey:@"tagList"];
         
         for (int i=0; i<tagsArray.count; i++)
         {
             NSNumber *tagId = [tagsArray objectAtIndex:i];
             IMGRestaurantTag *restaurantTag = [[IMGRestaurantTag alloc]init];
             restaurantTag.tagId=tagId;
             restaurantTag.restaurantId = restaurant.restaurantId;
             
             [[DBManager manager]insertRelation:restaurantTag selectField:@"tagId" andSelectID:restaurantTag.tagId relationField:@"restaurantId" andRelationId:restaurantTag.restaurantId];
             
         }
         
         
         NSMutableDictionary *tempResponseObject = [[NSMutableDictionary alloc] initWithDictionary:responseObject];
         [tempResponseObject removeObjectForKey:@"tagList"];
         
         [restaurant setValuesForKeysWithDictionary:responseObject];
         
         block();
         
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"getMoreInfoFromService error: %@",error.localizedDescription);
     }];
}

+ (void)getEventAndDinningGuideFromService:(NSNumber *)restaurantId andBlock:(void (^)(NSArray *eventList,NSArray *dinningGuideList))block
{
    NSDictionary * parameters = [[NSDictionary alloc] init];
    parameters = @{@"restaurantId":restaurantId};
    
    [[IMGNetWork sharedManager]POST:@"app/detail/eventsdiningguides" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSMutableArray *eventList = [[NSMutableArray alloc] init];
         for (NSDictionary *dic in [responseObject objectForKey:@"events"])
         {
             IMGEvent *event = [[IMGEvent alloc] init];
             [event setValuesForKeysWithDictionary:dic];
             [eventList addObject:event];
         }
         
         NSMutableArray *diningGuideList = [[NSMutableArray alloc] init];
         for (NSDictionary *dic in [responseObject objectForKey:@"diningGuides"])
         {
             IMGDiningGuide *diningGuide = [[IMGDiningGuide alloc] init];
             [diningGuide setValuesForKeysWithDictionary:dic];
             [diningGuideList addObject:diningGuide];
         }
         
         block(eventList,diningGuideList);
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"getMoreInfoFromService error: %@",error.localizedDescription);
     }];
}

+ (void)getBookInfoFromService:(IMGRestaurant *)restaurant andBlock:(void (^)())block
{
    NSDictionary * parameters;
    IMGUser *user=[IMGUser currentUser];
    if(user==nil || user.userId==nil || user.token==nil){
        parameters = @{@"restaurantId":restaurant.restaurantId};
    }
    else
    {
        parameters = @{@"userId":user.userId,@"t":user.token,@"restaurantId":restaurant.restaurantId};
    }

    [[IMGNetWork sharedManager]POST:@"app/restaurant/bookinfo" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         [restaurant setValuesForKeysWithDictionary:responseObject];
         block();
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"getBookInfoFromService error: %@",error.localizedDescription);
     }];

}

+(void)getRestaurantJournal:(NSDictionary*)parameters block:(void(^)(NSNumber *total,NSArray *journals))block failure:(void(^)(NSString* description))failure{
    [[IMGNetWork sharedManager] POST:@"app/restaurant/referredjournal" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSLog(@"%@",responseObject);
         NSNumber *total=[responseObject objectForKey:@"journalCount"];
         NSArray *journalList=[responseObject objectForKey:@"journalList"];
         NSMutableArray *journals=[[NSMutableArray alloc] init];
         [journalList enumerateObjectsUsingBlock:^(id obj,NSUInteger idx,BOOL *stop){
            IMGMediaComment *journal = [[IMGMediaComment alloc]init];
            journal.title = [obj objectForKey:@"journalTitle"];
            journal.imageUrl = [obj objectForKey:@"journalImageUrl"];
            journal.journalTitle = journal.title;
            journal.journalImageUrl = journal.imageUrl;
            journal.link = [obj objectForKey:@"journalWebsiteUrl"];
            journal.mediaCommentId = [obj objectForKey:@"journalId"];
            journal.contents = [obj objectForKey:@"journalWebsiteName"];
            [journals addObject:journal];
         }];
         block(total,journals);
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"getBookInfoFromService error: %@",error.localizedDescription);
     }];
}

+(void)claimYourRestaurant:(NSDictionary*)parameters block:(void(^)())block failure:(void(^)(NSString *errorMsg))failure{
    [[IMGNetWork sharedManager] POST:@"partnerRequest/claim" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        NSString *status = [responseObject objectForKey:@"status"];
        NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
        if ([@"failed" isEqualToString:status] && (errorMessage!=nil) ) {
            failure(errorMessage);
        }else if(errorMessage != nil){
            failure(nil);
        }else if([@"succeed" isEqualToString:status]) {
            block();
        }else{
            failure(nil);
        }
    }failure:^(NSURLSessionDataTask *operation, NSError *error){
        failure(nil);
    }];
}
+(void)getLastReviewWithParameter:(NSDictionary *)parameter andBlock:(void(^)(NSString *str,NSNumber *num,NSNumber*reviewID ))block{

    [[IMGNetWork sharedManager]POST:@"user/review/last" parameters:parameter progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        [[LoadingView sharedLoadingView] stopLoading];
        NSString *returnStatus = [responseObject objectForKey:@"hasReview"];
        NSNumber *scoreNum = [[responseObject objectForKey:@"review"] objectForKey:@"reviewScore"];
        NSNumber*reviewID = [[responseObject objectForKey:@"review"] objectForKey:@"restaurantId"];

        block(returnStatus,scoreNum,reviewID);
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        [[LoadingView sharedLoadingView] stopLoading];
        NSLog(@"user/review/last error: %@",error.localizedDescription);
        NSString* noticeStr=L(@"Please check your internet connection and try again.");
        [SVProgressHUD showImage:[UIImage imageNamed:@""] status:noticeStr];
        [SVProgressHUD dismissWithDelay:1.5];
    }];

}
+(void)saveRestaurantWithParameter:(NSDictionary *)param andUrl:(NSString*)url andBlock:(void(^)(NSString *succeedStr,NSString *failedString))block andErrorBlock:(void(^)(NSError *error))errorBlock{
    
    [[IMGNetWork sharedManager]POST:url parameters:param progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        
        NSString *exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
            return;
        }
        
        NSString *returnStatusString = [responseObject objectForKey:@"status"];
        
        NSString *exceptionmsg = [responseObject objectForKey:@"exceptionmsg"];
        block(returnStatusString,exceptionmsg);
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        errorBlock(error);
        NSLog(@"likeButtonTapped error: %@",error.localizedDescription);
        [[LoadingView sharedLoadingView]stopLoading];
    }];
}
+(void)getRestaurantIsFavouriteWithParameter:(NSDictionary *)param andBlock:(void(^)(NSString *likeStr))block andErrorBlock:(void(^)(NSError *error))errorBlock{
    [[IMGNetWork sharedManager] GET:@"user/restaurant/isfavorite" parameters:param progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSString *liked=[responseObject objectForKey:@"userFavorite"];
        block(liked);
        
    }failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"addLikeView erors when get datas:%@",error.localizedDescription);
    }];

}
+(void)SellAllPhotoQraved:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array,NSMutableArray *imgArray,NSString *goFoodlink))successBlock anderrorBlock:(void(^)())errorBlock{

    [[IMGNetWork sharedManager] GET:@"app/detail/photos/qraved" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *array = [NSMutableArray array];
        NSMutableArray *imgArray = [NSMutableArray array];
        for (NSDictionary *dict in responseObject[@"qravedList"]) {
            DetailSeeAllPhotoQravedModel *QravedModel   = [DetailSeeAllPhotoQravedModel mj_objectWithKeyValues:dict];
            [array addObject:QravedModel];
            [imgArray addObject:[dict objectForKey:@"imageUrl"]];
        }
        imgArray = (NSMutableArray *)[[imgArray reverseObjectEnumerator] allObjects];
        successBlock(array,imgArray,[responseObject objectForKey:@"goFoodLink"]);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        errorBlock();
    }];


}

+(void)SellAllPhotoInstagram:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array,NSString *goFoodlink))successBlock anderrorBlock:(void(^)())errorBlock{
    
    [[IMGNetWork sharedManager] GET:@"app/detail/photos/instagram" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *array = [NSMutableArray array];
//        for (NSDictionary *dict in responseObject[@"qravedList"]) {
//            DetailSeeAllPhotoInstagramModel *InstagramModel   = [DetailSeeAllPhotoInstagramModel mj_objectWithKeyValues:dict];
//            [array addObject:InstagramModel];
//        }
        array = [DetailSeeAllPhotoInstagramModel mj_objectArrayWithKeyValuesArray:responseObject[@"instagramPhotoList"]];
        
        successBlock(array,[responseObject objectForKey:@"goFoodLink"]);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        errorBlock();
    }];
    
    
}
+(void)SellMenuBarInstagram:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array))successBlock anderrorBlock:(void(^)())errorBlock{

    [[IMGNetWork sharedManager] GET:@"app/restaurant/barmenu/list" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *array = [NSMutableArray array];
        for (NSDictionary *dict in responseObject[@"barMenuList"]) {
            DetailSeeMenuBarModel *InstagramModel   = [DetailSeeMenuBarModel mj_objectWithKeyValues:dict];
            [array addObject:InstagramModel];
        }
        successBlock(array);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
         errorBlock();
    }];


}
+(void)SeeMenuDelevery:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array))successBlock anderrorBlock:(void(^)())errorBlock{

    [[IMGNetWork sharedManager] GET:@"app/restaurant/deliverymenu/list" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSMutableArray *array = [NSMutableArray array];
        for (NSDictionary *dict in responseObject[@"deliveryMenuList"]) {
            DetailSeeMenuDeleveryModel *InstagramModel   = [DetailSeeMenuDeleveryModel mj_objectWithKeyValues:dict];
            [array addObject:InstagramModel];
        }
        successBlock(array);

        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        errorBlock();
        
    }];
    

}
+(void)SeeMenuRestaurant:(NSDictionary*)dic andSuccessBlock:(void(^)(NSMutableArray * array ,NSNumber *restaurantMenuCount, NSNumber *barMenuCount, NSNumber *deliveryMenuCount))successBlock anderrorBlock:(void(^)())errorBlock{

    [[IMGNetWork sharedManager] GET:@"app/restaurant/restaurantmenu/list" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSNumber * restaurantN;
        NSNumber * barN;
        NSNumber *deliveryN;
        restaurantN = responseObject[@"restaurantMenuCount"];
        barN = responseObject[@"barMenuCount"];
        deliveryN = responseObject[@"deliveryMenuCount"];
        
        NSMutableArray *array = [NSMutableArray array];
        for (NSDictionary *dict in responseObject[@"restaurantMenuList"]) {
            DetailSeeMenuRestaurantModel *InstagramModel   = [DetailSeeMenuRestaurantModel mj_objectWithKeyValues:dict];
            [array addObject:InstagramModel];
        }
        successBlock(array, restaurantN, barN, deliveryN);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         errorBlock();
        
    }];
    
}

+(void)suggestAnEditSubmit:(NSDictionary*)dic andSuccessBlock:(void(^)(NSDictionary * array))successBlock anderrorBlock:(void(^)())errorBlock{

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",QRAVED_WEB_SERVICE_SERVER,@"suggestion/add"]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSDictionary *json = dic;
    NSData *data = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:nil];
    request.HTTPBody = data;
    
    [[IMGNetWork sharedManager] requestWithPath:@"suggestion/add" withBody:data withSuccessBlock:^(id responseObject) {
        successBlock(responseObject);
        
    } withFailurBlock:^(NSError *error) {
        errorBlock();
        
    }];
    
//    [[IMGNetWork sharedManager] POST:@"suggestion/add" parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        
//        successBlock(responseObject);
//        
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        
//        errorBlock();
//        
//    }];


}



@end
