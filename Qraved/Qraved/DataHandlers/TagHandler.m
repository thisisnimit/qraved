//
//  TagHandler.m
//  Qraved
//
//  Created by Jeff on 10/8/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "TagHandler.h"
#import "DBManager.h"
#import "IMGTag.h"
#import "IMGArea.h"

@implementation TagHandler


-(void)getDatasFromServer
{
    
    [[IMGNetWork sharedManager] GET:@"tags" parameters:nil progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueue];
        [queue inDeferredTransaction:^(FMDatabase *db, BOOL *rollback) {
            [db executeUpdate:@"delete from IMGTag"];
            NSArray *array = responseObject;
            for (int i=0; i<array.count; i++) {
                NSDictionary *dict = [array objectAtIndex:i];
                IMGTag *tag = [[IMGTag alloc]init];
                [tag setValuesForKeysWithDictionary:dict];
                [[DBManager manager]insertModel:tag selectField:@"tagId" andSelectID:tag.tagId andFMDatabase:db];
            }
            NSLog(@"TagHandler update datas done");
        }];
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"AFHTTPRequestOperation erors when get datas:%@",error.localizedDescription);
    }];
    
}
- (void)getAreaDatasFromServerWithTypeId:(NSNumber *)typeId
{
//    NSDictionary *params = @{@"typeId":typeId};
    [[IMGNetWork sharedManager] GET:@"app/search/filter/area/list" parameters:nil progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueue];
        [queue inDeferredTransaction:^(FMDatabase *db, BOOL *rollback) {
            [db executeUpdate:@"delete from IMGArea"];
//            NSArray *array = [responseObject objectForKey:@"areaList"];
            NSArray *array=responseObject;
            for (int i=0; i<array.count; i++) {
                NSDictionary *dict = [array objectAtIndex:i];
                IMGArea *area = [[IMGArea alloc]init];
                [area setValuesForKeysWithDictionary:dict];
                area.tagId = [dict objectForKey:@"id"];
                area.tagTypeId = [NSNumber numberWithInt:1];
                area.type = @"Area";
                area.type = @"Area";
                [[DBManager manager]insertModel:area selectField:@"tagId" andSelectID:area.tagId andFMDatabase:db];
            }
            NSLog(@"TagHandler update datas done");
        }];
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"AFHTTPRequestOperation erors when get datas:%@",error.localizedDescription);
    }];
}

- (void)getDatasFromServerWithTagTypeId:(NSNumber *)tagTypeId  typeId:(NSNumber *)typeId
{
    NSDictionary *params = @{@"tagTypeId":tagTypeId};
    [[IMGNetWork sharedManager] GET:@"app/search/filter/tag/list" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueue];
        [queue inDeferredTransaction:^(FMDatabase *db, BOOL *rollback) {
            [db executeUpdate:[NSString stringWithFormat:@"delete from IMGTag where tagTypeId = %d",[tagTypeId intValue]]];
            NSArray *array = responseObject;
            for (int i=0; i<array.count; i++) {
                NSDictionary *dict = [array objectAtIndex:i];
                IMGTag *tag = [[IMGTag alloc]init];
                [tag setValuesForKeysWithDictionary:dict];
                [[DBManager manager]insertModel:tag selectField:@"tagId" andSelectID:tag.tagId andFMDatabase:db];
            }
            if (array.count == 0) {
                [db executeUpdate:[NSString stringWithFormat:@"delete from IMGTagUpdate where typeId = %d",[typeId intValue]]];
            }
            NSLog(@"TagHandler update datas done");
        }];
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"AFHTTPRequestOperation erors when get datas:%@",error.localizedDescription);
    }];

}


@end
