//
//  SuggestHandler.m
//  Qraved
//
//  Created by Lucky on 15/10/9.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import "SuggestHandler.h"
#import "Amplitude.h"
@implementation SuggestHandler
+ (void)suggestWithDic:(NSDictionary *)dataDic withSuccess:(void(^)(bool success))successblock withFail:(void(^)(bool fail,NSString* erroInfo))failblock
{
   
    [[IMGNetWork sharedManager] POST:@"suggestion/restaurant/add" parameters:dataDic progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
       

        successblock(YES);
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"suggest ---- :%@",error.localizedDescription);
       

        failblock(YES,error.localizedDescription);
        
    }];
}
+ (void)suggestJournalWithDic:(NSDictionary *)dataDic withSuccess:(void(^)(bool succes))successBlock withfail:(void(^)(bool fail,NSString* erroInfo))erroBlock
{
    [[IMGNetWork sharedManager] POST:@"suggestion/journal/add" parameters:dataDic progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        successBlock(YES);
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"suggestJournalWithDic ---- :%@",error.localizedDescription);
        
        erroBlock(YES,error.localizedDescription);

    }];

}
@end
