//
//  ListHandler.m
//  Qraved
//
//  Created by Lucky on 15/4/25.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "ListHandler.h"
#import "DBManager.h"
#import "IMGMyList.h"
#import "RestaurantHandler.h"
#import "NSString+Helper.h"
@implementation ListHandler


+ (void)getListWithUser:(IMGUser *)user andOffset:(int)offset andMax:(int)max andBlock:(void (^)(NSArray *dataArray,int listCount,BOOL hasData))block andFailedBlock:(void(^)())FailedBlock
{
    NSMutableArray *dataAry = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:user.userId forKey:@"userId"];
    [param setValue:user.token forKey:@"t"];
    [param setValue:[NSNumber numberWithInt:offset] forKey:@"offset"];
    [param setValue:[NSNumber numberWithInt:max] forKey:@"max"];
    
    __block int skip= max + offset;

    [[IMGNetWork sharedManager]POST:@"lists/v2/self" parameters:param progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSLog(@"responseObject = %@",responseObject);
     
         NSNumber *userListCount = [responseObject objectForKey:@"guideCount"];
         int listCount = [userListCount intValue];
         
         NSArray *restaurantListAry = [responseObject objectForKey:@"guideList"];
         
         BOOL hasData=YES;
         if(skip >= listCount){
             
             hasData = NO;
         }

         for (NSDictionary *dic in restaurantListAry)
         {
             
             IMGMyList *list = [[IMGMyList alloc] init];
             //list.listCount = [dic objectForKey:@"count"];
             list.listId = [dic objectForKey:@"id"];
             list.listImageUrls = [dic objectForKey:@"coverImage"];
             //list.listImageUrls = @"url";
             list.restaurantListItemCount = [dic objectForKey:@"restaurantListItemCount"];
             list.listName = [dic objectForKey:@"name"];
             list.listType = [dic objectForKey:@"listType"];
             list.listUserId = user.userId;
             //list.cityId = [dic objectForKey:@"cityId"];
             // [[DBManager manager] insertModel:list selectField:@"listId" andSelectID:list.listId];
             
             [dataAry addObject:list];
         }
         block(dataAry,listCount,hasData);
        
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"likeButtonTapped error: %@",error.localizedDescription);
         
         FailedBlock();
     }];
}

+ (void)getListWithUser:(IMGUser *)user restaurantId:(NSNumber*)restaurantId cityId:(NSNumber*)cityId offset:(NSInteger)offset max:(NSInteger)max andBlock:(void (^)(NSArray *dataArray,BOOL hasData))block
{
    NSDictionary * parameters = @{@"userId":user.userId,@"t":user.token,@"restaurantId":restaurantId,@"offset":[NSNumber numberWithInteger:offset],@"max":[NSNumber numberWithInteger:max],@"cityId":cityId};
     __block int skip= (int)max + (int)offset;
    [[IMGNetWork sharedManager]POST:@"liststoadd" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        NSLog(@"responseObject = %@",responseObject);
        NSNumber *userListCount = [responseObject objectForKey:@"restaurantListCount"];
        int listCount = [userListCount intValue];
        BOOL hasData=YES;
         if(skip >= listCount){
             
             hasData = NO;
         }
        NSArray *restaurantList = [responseObject objectForKey:@"restaurantList"];

        NSMutableArray *dataAry=[[NSMutableArray alloc] init];

        for (NSDictionary *dic in restaurantList){
             
            IMGMyList *list = [[IMGMyList alloc] init];
            list.listCount = [dic objectForKey:@"count"];
            list.listId = [dic objectForKey:@"id"];
            list.listImageUrls = [dic objectForKey:@"imageUrls"];
            list.listName = [dic objectForKey:@"name"];
            list.listType = [dic objectForKey:@"listType"];
            list.inList = [dic objectForKey:@"inList"];
            list.listUserId = user.userId;
            list.cityId = [dic objectForKey:@"cityId"];
            [dataAry addObject:list];
         }
         
        block(dataAry,hasData);
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"likeButtonTapped error: %@",error.localizedDescription);
     }];
}


+ (void)addListWithUser:(IMGUser *)user andListName:(NSString *)listName  andCityId:(NSNumber*)cityId andBlock:(void (^)(NSDictionary *))block  failure:(void(^)(NSString *exceptionMsg))failureBlock
{
    NSMutableDictionary * param = [NSMutableDictionary dictionaryWithDictionary:@{@"userId":user.userId,@"t":user.token,@"name":listName,@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"],@"cityId":cityId}];
//    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",QRAVED_WEB_SERVICE_SERVER,@"list/add/v2"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [param setValue:@"2" forKey:@"client"];
    [param setValue:QRAVED_WEB_SERVICE_VERSION forKey:@"v"];
    [param setValue:QRAVED_APIKEY forKey:@"appApiKey"];
    NSDictionary *json = param;
    NSData *data = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:nil];
    request.HTTPBody = data;
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (connectionError==nil) {
            NSError *error = nil;
            id responseObject = [NSJSONSerialization JSONObjectWithData:data
                                                            options:NSJSONReadingAllowFragments
                                                              error:&error];
            NSLog(@"%@",responseObject);
            if ([[responseObject objectForKey:@"status"] isEqualToString:@"succeed"])
            {
                NSDictionary *restaurantListDic = [[NSDictionary alloc] initWithObjectsAndKeys:[responseObject objectForKey:@"id"],@"id", nil];
                IMGMyList *mylist=[[IMGMyList alloc] init];
                mylist.listId=[restaurantListDic objectForKey:@"id"];
                mylist.listCount=[NSNumber numberWithInt:0];
                mylist.listType=[NSNumber numberWithInt:99];
                mylist.listName=listName;
                mylist.listUserId=user.userId;
                mylist.inList=[NSNumber numberWithBool:YES];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"profileNewListNotification" object:nil userInfo:@{@"mylist":mylist}];
                block(restaurantListDic);
            }else if ([responseObject objectForKey:@"exceptionmsg"])
            {
                NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
                if(![errorMessage isLogout]){
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Message" message:[NSString stringWithFormat:@"%@",[responseObject objectForKey:@"exceptionmsg"]] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil ];
                    [alertView show];
                    [[LoadingView sharedLoadingView]stopLoading];
                }
                failureBlock(errorMessage);
            }
            else
                block(nil);

        }else{
        }
    }];

}

+ (void)editListWithUser:(IMGUser *)user andListName:(NSString *)listName andListId:(NSNumber *)listId andBlock:(void (^)(BOOL status,NSString *exceptionmsg))block
{
    NSDictionary * parameters = @{@"userId":user.userId,@"t":user.token,@"name":listName,@"id":listId,@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]};
    [[IMGNetWork sharedManager]POST:@"list/edit" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSLog(@"responseObject = %@",responseObject);
         NSString *exceptionmsg=nil;
         if([[responseObject objectForKey:@"status"] isEqualToString:@"succeed"]){
            block(YES,nil);
         }else if((exceptionmsg=[responseObject objectForKey:@"exceptionmsg"])){
            block(NO,exceptionmsg);
         }
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"likeButtonTapped error: %@",error.localizedDescription);
         block(NO,error.localizedDescription);
     }];
}

+ (void)delListWithUser:(IMGUser *)user andListId:(NSNumber *)listId andBlock:(void (^)(BOOL,NSString *))block
{
    NSDictionary * parameters = @{@"userId":user.userId,@"t":user.token,@"id":listId,@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]};
    [[IMGNetWork sharedManager]POST:@"list/del" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSLog(@"responseObject = %@",responseObject);
         NSString *exceptionmsg=nil;
         if([[responseObject objectForKey:@"status"] isEqualToString:@"succeed"]){
            block(YES,nil);
         }else if((exceptionmsg=[responseObject objectForKey:@"exceptionmsg"])){
            block(NO,exceptionmsg);
         }else{
            block(NO,nil);
         }
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"likeButtonTapped error: %@",error.localizedDescription);
         block(NO,error.localizedDescription);
     }];
}

+ (void)listAddRestaurantWithUser:(IMGUser *)user andListId:(NSNumber *)listId andRestaurantId:(NSNumber *)restaurantId andBlock:(void (^)(BOOL succeed,id status))block
{
    NSDictionary * parameters = @{@"userId":user.userId,@"t":user.token,@"listId":listId,@"restaurantId":restaurantId,@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]};
    [[IMGNetWork sharedManager]POST:@"list/item/add" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSLog(@"responseObject = %@",responseObject);
         if([responseObject objectForKey:@"exceptionmsg"]){
            block(NO,[responseObject objectForKey:@"exceptionmsg"]);
         }else if([[responseObject objectForKey:@"status"] isEqualToString:@"succeed"]){
             block(YES,[NSNumber numberWithBool:YES]);
             [[NSNotificationCenter defaultCenter] postNotificationName:@"profileListAddRestaurantNotification" object:nil userInfo:@{@"listId":listId,@"added":[NSNumber numberWithBool:YES]}];
         }else if([[responseObject objectForKey:@"status"] isEqualToString:@"existed"]){
             block(YES,[NSNumber numberWithBool:NO]);
         }
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         block(NO,error.localizedDescription);
         NSLog(@"likeButtonTapped error: %@",error.localizedDescription);
     }];
}
+ (void)listAddRestaurantsWithUser:(IMGUser *)user andListIds:(NSArray *)listIds andRestaurantIds:(NSArray *)restaurantIds andBlock:(void (^)(BOOL succeed,id status))block{
    
    
    NSMutableDictionary * parameters = [NSMutableDictionary dictionary];
    [parameters setObject:user.userId forKey:@"userId"];
    [parameters setObject:listIds forKey:@"listIds"];
    [parameters setObject:restaurantIds forKey:@"restaurantIds"];
    [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    [parameters setObject:user.token forKey:@"t"];
    [parameters setObject:QRAVED_APIKEY forKey:@"appApiKey"];
    [parameters setObject:QRAVED_WEB_SERVICE_VERSION forKey:@"v"];
    [parameters setObject:QRAVED_WEB_SERVICE_CLIENT forKey:@"client"];
    NSData *data = [NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:nil];
    [[IMGNetWork sharedManager]requestWithPath:@"list/item/add/multiple" withBody:data withSuccessBlock:^(id responseObject) {
        NSLog( @"responseObject=%@",responseObject);
        if ([[responseObject objectForKey:@"status"] isEqualToString:@"succeed"]) {
            block(YES,responseObject);

        }else{
            block(NO,responseObject);
        }
       
    } withFailurBlock:^(NSError *error) {
        
    }];

}
+ (void)getListDetailWithUser:(IMGUser *)user andOffset:(int)offset andMax:(int)max andListId:(NSNumber *)listId andBlock:(void (^)(NSInteger total,NSArray *dataArray))block
{
    NSDictionary * parameters = @{@"userId":user.userId,@"t":user.token,@"listId":listId,@"max":[NSNumber numberWithInt:max],@"offset":[NSNumber numberWithInt:offset]};
    [[IMGNetWork sharedManager]POST:@"list/detail/v2" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSArray *tempArry = [responseObject objectForKey:@"restaurantList"];
         NSMutableArray *restaurantArr = [NSMutableArray array];
         for (NSDictionary *dic in tempArry) {
             IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
             [restaurant setValuesForKeysWithDictionary:dic];
             restaurant.ratingScore = [dic objectForKey:@"rating"];
             restaurant.address1 = [dic objectForKey:@"address"];
             restaurant.bookStatus = [dic objectForKey:@"status"];
             NSDictionary* yesterOpenDic=[dic objectForKey:@"yesterDayOpenTime"];
             restaurant.yesteropenDay=[yesterOpenDic objectForKey:@"openDay"];
             restaurant.yesteropenTime=[yesterOpenDic objectForKey:@"openTime"];
             restaurant.yesterclosedTime=[yesterOpenDic objectForKey:@"closedTime"];
             restaurant.saved = @1;
             restaurant.goFoodLink = [dic objectForKey:@"goFoodLink"];
             
             NSDictionary* nextOpenDay=[dic objectForKey:@"nextOpenDay"];
             restaurant.nextopenTime=[nextOpenDay objectForKey:@"openTime"];
             restaurant.nextclosedTime=[nextOpenDay objectForKey:@"closedTime"];
             restaurant.nextopenDay=[nextOpenDay objectForKey:@"openDay"];
             
             NSDictionary* secondOpenDic=[dic objectForKey:@"secondOpenDay"];
             restaurant.secondclosedTime=[secondOpenDic objectForKey:@"closedTime"];
             restaurant.secondopenTime=[secondOpenDic objectForKey:@"openTime"];
             restaurant.secondopenDay=[secondOpenDic objectForKey:@"openDay"];
             
             [restaurantArr addObject:restaurant];
         }

         block([[responseObject objectForKey:@"restaurantListItemCount"] integerValue], restaurantArr);
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"likeButtonTapped error: %@",error.localizedDescription);
     }];
}

+(void)update:(NSArray *)restaurantArray forRestaurant:(IMGRestaurant *)restaurant{
    NSLog(@"restaurantArray = %@",restaurantArray);
    
    if([restaurantArray count]>6){
        restaurant.restaurantId = [restaurantArray objectAtIndex:0];
        restaurant.state = [restaurantArray objectAtIndex:1];
        restaurant.bestOfferName=[restaurantArray objectAtIndex:3];
        restaurant.title = [restaurantArray objectAtIndex:4];
        restaurant.imageUrl = [restaurantArray objectAtIndex:5];
        restaurant.cuisineName = [restaurantArray objectAtIndex:6];
        restaurant.districtName = [restaurantArray objectAtIndex:7];
        restaurant.priceLevel = [restaurantArray objectAtIndex:8];
        restaurant.reviewCount = [restaurantArray objectAtIndex:9];
        restaurant.latitude = [restaurantArray objectAtIndex:10];
        restaurant.longitude = [restaurantArray objectAtIndex:11];
        restaurant.logoImage = [restaurantArray objectAtIndex:12];
        restaurant.ratingScore = [restaurantArray objectAtIndex:13];
        restaurant.dateCreated = [restaurantArray objectAtIndex:14];
        restaurant.note = [restaurantArray objectAtIndex:15];
        restaurant.ratingCount = [restaurantArray objectAtIndex:16];
        restaurant.needUpdate = @"";
    }
    /*
    if(!restaurant.restaurantId || [restaurant.restaurantId intValue]==0){
        [[DBManager manager]insertModel:restaurant];
        restaurant = [IMGRestaurant findById:restaurant.restaurantId];
    }else{
        [[DBManager manager] insertModel:restaurant update:YES selectKey:@"restaurantId" andSelectValue:restaurant.restaurantId];
    }
     */
}

+ (void)listRemoveWithUser:(IMGUser *)user andListId:(NSNumber *)listId andRestaurantId:(NSNumber *)restaurantId andBlock:(void (^)(void))block failure:(void(^)(NSString *exceptionMsg))failureBlock
{
    NSDictionary * parameters = @{@"userId":user.userId,@"t":user.token,@"listId":listId,@"restaurantId":restaurantId,@"latitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"],@"longitude":[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"]};
    [[IMGNetWork sharedManager]POST:@"list/item/remove" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSLog(@"responseObject = %@",responseObject);
        NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
         if(responseObject && errorMessage){
             failureBlock(errorMessage);
         }else{
             block();
//             [[NSNotificationCenter defaultCenter] postNotificationName:@"profileListAddRestaurantNotification" object:nil userInfo:@{@"listId":listId,@"added":[NSNumber numberWithBool:NO]}];
         }
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         failureBlock(error.localizedDescription);
         NSLog(@"likeButtonTapped error: %@",error.localizedDescription);
     }];
}

+ (void)removeRestaurantFromListWithRestaurantId:(NSNumber *)restaurantId andListId:(NSNumber *)listId andBlock:(void (^)(void))block failure:(void(^)(NSString *exceptionMsg))failureBlock{

    NSMutableDictionary * parameters = [NSMutableDictionary dictionary];
    [parameters setObject:[IMGUser currentUser].userId forKey:@"userId"];
   
    [parameters setObject:restaurantId forKey:@"restaurantId"];
    [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] forKey:@"latitude"];
    [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] forKey:@"longitude"];
    [parameters setObject:[IMGUser currentUser].token forKey:@"t"];
    [parameters setObject:QRAVED_APIKEY forKey:@"appApiKey"];
    [parameters setObject:QRAVED_WEB_SERVICE_VERSION forKey:@"v"];
    [parameters setObject:QRAVED_WEB_SERVICE_CLIENT forKey:@"client"];
    NSData *data = [NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:nil];
    [[IMGNetWork sharedManager]requestWithPath:@"list/item/removeall" withBody:data withSuccessBlock:^(id responseObject) {
        NSLog( @"responseObject=%@",responseObject);
        if ([[responseObject objectForKey:@"status"] isEqualToString:@"succeed"]) {
            block();
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"profileListAddRestaurantNotification" object:nil userInfo:@{@"listId":listId,@"added":[NSNumber numberWithBool:NO]}];
            
        }
        
    } withFailurBlock:^(NSError *error) {
        
    }];

}

///user/restaurant/inlist?userId=1&restaurantId=1
+ (void)listIsInList:(IMGUser *)user andRestaurantId:(NSNumber *)restaurantId andBlock:(void (^)(BOOL))block
{
    NSDictionary * parameters = @{@"userId":user.userId,@"restaurantId":restaurantId};
    [[IMGNetWork sharedManager]POST:@"user/restaurant/inlist" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSLog(@"responseObject = %@",responseObject);
         NSNumber *inList = [responseObject objectForKey:@"inList"];
         if ([inList longLongValue] == 1)
         {
             block(YES);
         }
         else
         {
             block(NO);
         }
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {

     }];
}

+(NSURLSessionTask*)recentlyViewRestaurants:(NSDictionary*)parameters block:(void(^)(NSNumber* total,NSArray *restaurants))block{
    [parameters setValue:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
    return [[IMGNetWork sharedManager]POST:@"restaurant/viewed/list/v2" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSLog(@"responseObject = %@",responseObject);
         NSArray *restaurantList = [responseObject objectForKey:@"restaurantList"];
         NSNumber *total = [responseObject objectForKey:@"count"];
         NSMutableArray *restaurants=[NSMutableArray arrayWithArray:[ListHandler componentRestaurants:restaurantList]];
         block(total,restaurants);
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {

     }];
}

+(NSURLSessionTask*)searchRestaurant:(NSString*)keyword withParams:(NSDictionary*)params andBlock:(void(^)(NSNumber* total,NSArray *retaurants))block{

    NSMutableDictionary *parameters=[NSMutableDictionary dictionaryWithDictionary:params];
    [parameters setObject:keyword forKey:@"keyword"];
    [parameters setObject:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"cityId"];
    NSString *latitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"];
    NSString *longitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"];
    [parameters setObject:latitude forKey:@"latitude"];
    [parameters setObject:longitude forKey:@"longitude"];
    [parameters setValue:@"popularity" forKey:@"sortby"];

    return [[IMGNetWork sharedManager] GET:@"app/search/v8" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        NSNumber *total=[responseObject objectForKey:@"numFound"];
        NSArray *restaurantList=[responseObject objectForKey:@"restaurantList"];
        NSMutableArray *restaurants=[NSMutableArray arrayWithArray:[ListHandler componentRestaurants:restaurantList]];
        block(total,restaurants);
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"getDishesFromServer erors when get datas:%@",error.localizedDescription);
    }];
}

+(NSURLSessionTask*)mylist_recentlyViewRestaurants:(NSDictionary*)parameters block:(void(^)(NSNumber* total,NSArray *restaurants))block{

    return [[IMGNetWork sharedManager]POST:@"restaurant/viewed/list/v2" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSLog(@"responseObject = %@",responseObject);
         NSArray *restaurantList = [responseObject objectForKey:@"restaurantList"];
         NSNumber *total = [responseObject objectForKey:@"count"];
         NSMutableArray *restaurants=[[NSMutableArray alloc] init];
         for(NSDictionary *restaurant in restaurantList){
            NSMutableDictionary *tmp=[[NSMutableDictionary alloc] init];
            [tmp setObject:[[restaurant objectForKey:@"title"] filterHtml] forKey:@"title"];
            [tmp setObject:[restaurant objectForKey:@"restaurantId"] forKey:@"restaurantId"];
            [tmp setObject:[restaurant objectForKey:@"seoKeyword"] forKey:@"seoKeyword"];
            [tmp setObject:[restaurant objectForKey:@"inList"] forKey:@"inList"];
             [tmp setObject:[restaurant objectForKey:@"imageUrl"] forKey:@"imageUrl"];
             [tmp setObject:[restaurant objectForKey:@"latitude"] forKey:@"latitude"];
             [tmp setObject:[restaurant objectForKey:@"longitude"] forKey:@"longitude"];
             [tmp setObject:[restaurant objectForKey:@"listName"] forKey:@"listName"];

//            NSEnumerator *enumerator=[[restaurant objectForKey:@"cuisineList"] reverseObjectEnumerator];
//            NSMutableArray *cuisines=[[NSMutableArray alloc] init];
//            for(NSDictionary *cuisine in [enumerator allObjects]){
//                [cuisines addObject:[cuisine objectForKey:@"name"]];
//            }
//            [cuisines addObject:[restaurant objectForKey:@"districtName"]];
//            [tmp setObject:[cuisines componentsJoinedByString:@" / "] forKey:@"cuisine"];
             [tmp setObject:[restaurant objectForKey:@"districtName"] forKey:@"cuisine"];
            [restaurants addObject:tmp];
         }
         block(total,restaurants);
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {

     }];
}


+(NSURLSessionTask*)mylist_searchRestaurant:(NSString*)keyword withParams:(NSDictionary*)params andBlock:(void(^)(NSNumber* total,NSArray *retaurants))block{

    NSMutableDictionary *parameters=[NSMutableDictionary dictionaryWithDictionary:params];
    [parameters setObject:keyword forKey:@"filterWord"];

    return [[IMGNetWork sharedManager] GET:@"search/filterrestauranttitle" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        NSNumber *total=[responseObject objectForKey:@"count"];
        NSArray *restaurantList=[responseObject objectForKey:@"restaurantList"];
        NSMutableArray *restaurants=[[NSMutableArray alloc] init];
        for(NSDictionary *restaurant in restaurantList){
            NSMutableDictionary *tmp=[[NSMutableDictionary alloc] init];
            [tmp setObject:[[restaurant objectForKey:@"title"] filterHtml] forKey:@"title"];
            [tmp setObject:[restaurant objectForKey:@"restaurantId"] forKey:@"restaurantId"];
            [tmp setObject:[restaurant objectForKey:@"seoKeyword"] forKey:@"seoKeyword"];
            [tmp setObject:[restaurant objectForKey:@"inList"] forKey:@"inList"];
            [tmp setObject:[restaurant objectForKey:@"imageUrl"] forKey:@"imageUrl"];
            [tmp setObject:[restaurant objectForKey:@"latitude"] forKey:@"latitude"];
            [tmp setObject:[restaurant objectForKey:@"longitude"] forKey:@"longitude"];
            //listName
            [tmp setObject:[restaurant objectForKey:@"listName"] forKey:@"listName"];
//            NSEnumerator *enumerator=[[restaurant objectForKey:@"cuisineList"] reverseObjectEnumerator];
//            NSMutableArray *cuisines=[[NSMutableArray alloc] init];
//            for(NSDictionary *cuisine in [enumerator allObjects]){
//                [cuisines addObject:[cuisine objectForKey:@"name"]];
//            }
//            [cuisines addObject:[restaurant objectForKey:@"districtName"]];
//            [tmp setObject:[cuisines componentsJoinedByString:@" / "] forKey:@"cuisine"];
            [tmp setObject:[restaurant objectForKey:@"districtName"] forKey:@"cuisine"];
            [restaurants addObject:tmp];
         }
         block(total,restaurants);
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"getDishesFromServer erors when get datas:%@",error.localizedDescription);
    }];
}

+(NSURLSessionTask*)recentlyViewBookedRestaurants:(NSDictionary*)parameters block:(void(^)(NSNumber* total,NSArray *restaurants))block{
    return [[IMGNetWork sharedManager]POST:@"app/booking/viewedResto" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSLog(@"responseObject = %@",responseObject);
         NSArray *restaurantList = [responseObject objectForKey:@"restaurantList"];
         NSString *exceptionmsg = [responseObject objectForKey:@"exceptionmsg"];
         if(exceptionmsg!=nil){
            block(0,nil);
            return;
         }
         NSNumber *total = [responseObject objectForKey:@"restaurantCount"];
         NSMutableArray *restaurants=[ListHandler componentRestaurants:restaurantList];
         block(total,restaurants);
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {

     }];
}

+(NSURLSessionTask*)searchBookedRestaurant:(NSString*)keyword withParams:(NSDictionary*)params andBlock:(void(^)(NSNumber* total,NSArray *retaurants))block{
    NSMutableDictionary *parameters=[NSMutableDictionary dictionaryWithDictionary:params];
    [parameters setObject:keyword forKey:@"filterWord"];
    return [[IMGNetWork sharedManager] GET:@"app/booking/keywordFilter" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        NSNumber *total=[responseObject objectForKey:@"restaurantCount"];
        NSArray *restaurantList=[responseObject objectForKey:@"restaurantList"];
        NSMutableArray *restaurants=[ListHandler componentRestaurants:restaurantList];
        block(total,restaurants);
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"getDishesFromServer erors when get datas:%@",error.localizedDescription);
    }];
}

+(NSMutableArray*)componentRestaurants:(NSArray*)restaurantList{
    NSMutableArray *restaurants=[[NSMutableArray alloc] init];
     for(NSDictionary *restDic in restaurantList){
        IMGRestaurant *restaurant=[[IMGRestaurant alloc] init];
        restaurant.restaurantId=[restDic objectForKey:@"restaurantId"];
        restaurant.title=[restDic objectForKey:@"title"];
        if([restDic objectForKey:@"restaurantTitle"]){
            restaurant.title=[restDic objectForKey:@"restaurantTitle"];
        }
         restaurant.landMarkList = [restDic objectForKey:@"landMarkList"];
         restaurant.area = [restDic objectForKey:@"area"];
         restaurant.area = [restaurant.area filterHtml];
        restaurant.seoKeyword=[restDic objectForKey:@"seoKeyword"];
         if ([[restDic objectForKey:@"cuisineList"] count]>0) {
             NSEnumerator *enumerator=[[restDic objectForKey:@"cuisineList"] reverseObjectEnumerator];
             NSMutableArray *cuisines=[[NSMutableArray alloc] init];
             for(NSDictionary *cuisine in [enumerator allObjects]){
                 [cuisines addObject:[cuisine objectForKey:@"name"]];
             }
             NSString *cuisineStr=@"";
             if(cuisines.count){
                 cuisineStr=[cuisines componentsJoinedByString:@" / "];
             }
             restaurant.cuisineName = cuisineStr;
         }else{
             restaurant.cuisineName=[restDic objectForKey:@"cuisineName"];
         }
        
        
        // [cuisines addObject:[restaurant objectForKey:@"districtName"]];
        restaurant.address1=[restDic objectForKey:@"address1"];
        restaurant.address2=[restDic objectForKey:@"address2"];
        restaurant.imageUrl=[restDic objectForKey:@"imageUrl"];
        restaurant.state=[restDic objectForKey:@"state"];
        restaurant.bookStatus=[restDic objectForKey:@"bookStatus"];
        restaurant.discountContent=[restDic objectForKey:@"discountContent"];
        restaurant.districtName = [restDic objectForKey:@"districtName"];
         restaurant.latitude = [restDic objectForKey:@"latitude"];
         restaurant.longitude = [restDic objectForKey:@"longitude"];
         restaurant.saved = [restDic objectForKey:@"inList"];
         restaurant.stateName = [restDic objectForKey:@"listName"];
        [restaurants addObject:restaurant];
     }
     return restaurants;
}


+(void)mylist:(NSNumber*)listId user:(IMGUser*)user restaurant:(NSNumber*)restaurantId note:(NSString*)noteText block:(void(^)(BOOL succeed))block{
    NSDictionary *parameters=@{@"userId":user.userId,@"listId":listId,@"restaurantId":restaurantId,@"reviewNote":noteText};
    [[IMGNetWork sharedManager] POST:@"list/item/addnote" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        NSLog(@"add note %@",responseObject);
        if([[responseObject objectForKey:@"status"] isEqualToString:@"success"]){
            block(YES);
        }else{
            block(NO);
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"addnote erors when get datas:%@",error.localizedDescription);
         block(NO);
    }];
}

+(void)getRestaurantSaved:(NSNumber*)restaurantId withUser:(IMGUser*)user andBlock:(void(^)(BOOL saved,NSInteger savedCount))block{
    NSDictionary *parameters=@{@"userId":user.userId,@"t":user.token,@"restaurantId":restaurantId};
    [[IMGNetWork sharedManager] GET:@"app/user/restaurantSaveInfo" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        NSLog(@"getRestaurantSaved %@",responseObject);
        BOOL saved=[[responseObject objectForKey:@"saved"] boolValue];
        NSInteger savedCount=[[responseObject objectForKey:@"savedCount"] integerValue];
        block(saved,savedCount);
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"restaurantSaveInfo error:%@",error.localizedDescription);
//         block(NO);
    }];
}

@end
