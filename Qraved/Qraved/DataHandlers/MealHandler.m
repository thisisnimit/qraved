//
//  MealHandler.m
//  Qraved
//
//  Created by Jeff on 10/23/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "MealHandler.h"
#import "DBManager.h"
#import "IMGMeal.h"

@implementation MealHandler

-(void)getDatasFromServer
{
    
    [[IMGNetWork sharedManager] GET:@"meal/list" parameters:nil progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        [[DBManager manager] deleteWithSql:@"delete from IMGMeal"];
        NSArray *array = responseObject;
        for (int i=0; i<array.count; i++) {
            NSDictionary *dict = [array objectAtIndex:i];
            IMGMeal *meal = [[IMGMeal alloc]init];
            [meal setValuesForKeysWithDictionary:dict];
            [[DBManager manager]insertModel:meal selectField:@"mealId" andSelectID:meal.mealId];
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"AFHTTPRequestOperation erors when get datas:%@",error.localizedDescription);
    }];
    
}

@end
