//
//  RestaurantHandler.h
//  Qraved
//
//  Created by Laura on 14-9-28.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataHandler.h"
#import "IMGRestaurant.h"
#import "IMGRestaurantLike.h"
#import "IMGRestaurantEventComment.h"
#import "IMGRestaurantEvent.h"
#import "IMGDishComment.h"

@interface RestaurantHandler : DataHandler
+(void)getAllEventsFromServer:(NSNumber*)restaurantId andBlock:(void(^)(NSArray *))block;
+(void)getEventsFromServer:(NSNumber*)restaurantId andBlock:(void(^)(NSArray *))block;
+(void)getDishesFromServer:(int)offset max:(int)max;
+(void)getDetailFromServer:(NSNumber*)restaurantId andBlock:(void (^)(IMGRestaurant * restaurant))block;
+(void)getRestaurantDetailFromServer:(NSNumber*)restaurantId andBlock:(void(^)(IMGRestaurant* restaurant))block;
+(void)getUserAllReviewsFromServer:(NSMutableDictionary*)params andBlock:(void(^)(BOOL status,NSUInteger total,NSArray *,NSDictionary *))block;
+(void)getReviewsFromServer:(NSMutableDictionary*)params andBlock:(void (^)(BOOL status,NSUInteger total,NSArray *,NSDictionary *)) block;
+(void)getAllReviews:(NSMutableDictionary*)params andBlock:(void (^)(NSArray *reviewList, IMGRestaurant *restaurant)) block failure:(void(^)())failureBlock;
+(void)getReviews:(NSMutableDictionary*)params andBlock:(void (^)(NSArray *reviewList)) block failure:(void(^)())failureBlock;
+(void)getInstagramReviews:(NSMutableDictionary*)params andBlock:(void (^)(NSArray *instagramReviewList)) block failure:(void(^)())failureBlock;
+(void)getPromoInfo:(NSDictionary *)params andBlock:(void(^)(IMGRestaurantEvent *restaurantEvent))block;
+(void)getDetailReviewsFromServer:(NSDictionary*)params andBlock:(void(^)(NSArray *reviewList,NSDictionary *,NSString *restaurantScore,NSNumber *reviewCount,NSString *ratingCounts,NSString *ratingCount))block;

+(void)getSectionsFromServer:(NSDictionary*)params andBlock:(void (^)(BOOL status,NSDictionary *,NSDictionary *)) block;
+(void)getMenusFromServer:(NSDictionary*)params andBlock:(void (^)(BOOL status,NSArray *)) block;

+(void)addOffer:(NSDictionary *)dic andRestaurantId:(NSNumber *)restaurantId date:(NSDate *)date;

+(void)saveRestaurantLike:(IMGRestaurantLike *)restaurantLike liked:(BOOL)liked;

+(IMGRestaurant *)parseJson2Entity:(NSDictionary *)restaurantDictionary  andBlock:(void (^)(IMGRestaurant * restaurant))block;
+(IMGRestaurant *)parseJson2Entity_v1:(NSDictionary *)restaurantDictionary  andBlock:(void (^)(IMGRestaurant * restaurant))block;

+(IMGRestaurant *)update:(NSArray *)restaurantArray forRestaurant:(IMGRestaurant *)restaurant;
+(IMGRestaurant *)searchUpdate:(NSArray *)restaurantArray forRestaurant:(IMGRestaurant *)restaurant;
+(NSArray *)updateForDetailViewController:(NSArray *)restaurantArray forRestaurant:(IMGRestaurant *)restaurant;

+ (IMGRestaurant *)parseJson2EntityByArray:(NSArray *)restaurantArray  andRestaurant:(IMGRestaurant *)restaurant andBlock:(void (^)(IMGRestaurant * restaurant))block;

+(NSArray *)getRestaurantLinkArray:(NSNumber *)restaurantId;
+ (IMGRestaurant *)updateRestaurant:(NSArray *)restaurantArray;

+ (void)addOffer:(NSDictionary *)dic andRestaurantId:(NSNumber *)restaurantId;
+ (void)getOfferFromServer:(NSNumber *)restaurantId andBookDate:(NSDate *)bookDate andBlock:(void (^)())block;
+ (void)getOffersFromServer:(NSString *)offerIdsStr andRestaurantId:(NSNumber *)restaurantId;
+(void)previousComment:(NSString *)commentIdStr targetId:(NSNumber*)articleId andBlock:(void (^)(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount))block;
+(void)previousCardComment:(NSString *)commentIdStr homeTimelineRestaurantUpdateId:(NSNumber*)homeTimelineRestaurantUpdateId andBlock:(void (^)(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount))block;
+(void)likeRestaurantEvent:(BOOL)liked restaurantEventId:(NSNumber*)restaurantEventId andBlock:(void (^)())block  failure:(void(^)(NSString *exceptionMsg))failureBlock;
+(void)likeRestaurantEventCard:(BOOL)liked homeTimelineRestaurantUpdateId:(NSNumber*)homeTimelineRestaurantUpdateId andBlock:(void (^)())block  failure:(void(^)(NSString *exceptionMsg))failureBlock;
+(void)postComment:(NSString *)commentStr restaurantEventId:(NSNumber*)restaurantEventId andBlock:(void (^)(IMGRestaurantEventComment *eventComment))block failure:(void(^)(NSString *errorReason))failureBlock;
+(void)postCardComment:(NSString *)commentStr restaurantId:(NSNumber*)restaurantId homeTimeLineId:(NSNumber *)homeTimeLineId andBlock:(void (^)(IMGRestaurantEventComment *eventComment))block failure:(void(^)(NSString *errorReason))failureBlock;
+(void)getRestaurantDishListFromService:(NSNumber *)restaurantId offset:(NSNumber*)offset andBlock:(void (^)(NSMutableArray *photoArray,NSNumber * dishCount,NSNumber *offsetBlock))block;
+(void)getRestaurantEventPhotoListFromService:(NSNumber *)restaurantId offset:(NSNumber*)offset andBlock:(void (^)(NSMutableArray *photoArray,NSNumber * count,NSNumber *offsetBlock))block;
+(NSArray *)updateForDetailViewControllerNow:(NSDictionary *)restaurantDic forRestaurant:(IMGRestaurant *)restaurant;
+ (void)geOfferFromServer:(NSNumber *)restaurantId andBookDate:(NSDate *)bookDate andBlock:(void (^)(NSMutableArray *restaurantOfferArray))block;
+(void)getRestaurantDetailByReviewIdFromServer:(NSNumber*)reviewId andBlock:(void(^)(IMGRestaurant* restaurant,NSNumber *reviewScore))block;
+(void)likeRestaurantOfferCard:(BOOL)liked OfferCardId:(NSNumber*)OfferCardId andBlock:(void (^)())block  failure:(void(^)(NSString *exceptionMsg))failureBlock;
+(void)postComment:(NSString *)commentStr restaurantOfferCardId:(NSNumber*)restaurantOfferCardId andBlock:(void (^)(IMGDishComment *dishComment))block failure:(void(^)(NSString *errorReason))failureBlock;
+(void)previousComment:(NSString *)commentIdStr offerComentId:(NSNumber*)offerComentId andBlock:(void (^)(NSArray *commentList,NSNumber *likeCount,NSNumber *commentCount))block;
@end
