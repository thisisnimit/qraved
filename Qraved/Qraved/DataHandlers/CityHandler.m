//
//  MealHandler.m
//  Qraved
//
//  Created by Jeff on 10/23/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "CityHandler.h"
#import "DBManager.h"
#import "IMGCity.h"

@implementation CityHandler

-(void)getDatasFromServer
{
    [[IMGNetWork sharedManager] GET:@"banner/cities" parameters:[[NSDictionary alloc]initWithObjectsAndKeys:@99,@"countryID", nil] progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueue];
        [queue inDeferredTransaction:^(FMDatabase *db, BOOL *rollback) {
            [db executeUpdate:@"delete from IMGCity"];
            NSArray *array = responseObject;
            for (int i=0; i<array.count; i++) {
                NSDictionary *dict = [array objectAtIndex:i];
                IMGCity *city = [[IMGCity alloc]init];
                city.countryId=[dict objectForKey:@"countryId"];
                city.cityId=[dict objectForKey:@"id"];
                city.latitude=[dict objectForKey:@"latitude"];
                city.longitude=[dict objectForKey:@"longitude"];
                city.name=[dict objectForKey:@"name"];
                city.maxLatitude=[dict objectForKey:@"maxLatitude"];
                city.maxLongitude=[dict objectForKey:@"maxLongitude"];
                city.minLatitude=[dict objectForKey:@"minLatitude"];
                city.minLongitude=[dict objectForKey:@"minLongitude"];
                city.status=[NSNumber numberWithInt:1];
                [[DBManager manager]insertModel:city andFMDatabase:db];
            }
            NSLog(@"CityHandler update datas done");
        }];
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"AFHTTPRequestOperation erors when get datas:%@",error.localizedDescription);
    }];
    
}

+(void)changeCity:(NSNumber*)cityId withUser:(NSNumber*)userId block:(void(^)(BOOL))block{
    NSAssert(cityId && userId,@"need userId and cityId");
    [[IMGNetWork sharedManager] GET:@"app/user/changecity" parameters:@{@"cityId":cityId,@"userId":userId} progress:nil success:^(NSURLSessionDataTask *operation,id responseObject){
        
    } failure:^(NSURLSessionDataTask *operation,NSError *error){

    }];
}
@end
