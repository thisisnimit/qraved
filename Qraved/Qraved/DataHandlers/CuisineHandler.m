//
//  CuisineHandler.m
//  Qraved
//
//  Created by Jeff on 10/8/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "CuisineHandler.h"
#import "DBManager.h"
#import "IMGCuisine.h"
#import "IMGRestaurantCuisine.h"

@implementation CuisineHandler


-(void)getDatasFromServer
{
    
    [[IMGNetWork sharedManager] GET:@"app/search/filter/cuisine/list" parameters:nil progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueue];
         [queue inDatabase:^(FMDatabase *db) {
             [db executeUpdate:@"delete from IMGCuisine"];
             NSArray *array = responseObject;
             for (int i=0; i<array.count; i++) {
                 NSDictionary *dict = [array objectAtIndex:i];
                 IMGCuisine *cuisine = [[IMGCuisine alloc]init];
                 [cuisine setValuesForKeysWithDictionary:dict];
                 [dataArray addObject:cuisine];
                 [[DBManager manager]insertModel:cuisine selectField:@"cuisineId" andSelectID:cuisine.cuisineId andFMDatabase:db];
             }
         }];
     }failure:^(NSURLSessionDataTask *operation, NSError *error) {
         NSLog(@"AFHTTPRequestOperation erors when get datas:%@",error.localizedDescription);
     }];
    
}

+(void)getRestaurantsFromServer{
    [[IMGNetWork sharedManager] GET:@"cuisine/restaurants" parameters:nil progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        [[DBManager manager] deleteWithSql:@"delete from IMGRestaurantCuisine"];
        NSArray *array = responseObject;
        for (int i=0; i<array.count; i++){
            NSDictionary *dict = [array objectAtIndex:i];
            IMGRestaurantCuisine *restaurantCuisine = [[IMGRestaurantCuisine alloc]init];
            restaurantCuisine.cuisineId=[dict objectForKey:@"cuisineId"];
            restaurantCuisine.restaurantId=[dict objectForKey:@"restauantId"];
            [[DBManager manager]insertRelation:restaurantCuisine selectField:@"cuisineId" andSelectID:restaurantCuisine.cuisineId relationField:@"restaurantId" andRelationId:restaurantCuisine.restaurantId];
        }
        NSLog(@"cuisine/restaurants");
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"AFHTTPRequestOperation erors when get datas:%@",error.localizedDescription);
    }];
    
    
}
@end
