//
//  ListHandler.h
//  Qraved
//
//  Created by Lucky on 15/4/25.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataHandler.h"
#import "IMGUser.h"
#import "IMGRestaurant.h"

typedef void(^completionHandler)(NSNumber *total,NSArray *restaurants);

@interface ListHandler : DataHandler

+ (void)getListWithUser:(IMGUser *)user andOffset:(int)offset andMax:(int)max andBlock:(void (^)(NSArray *dataArray,int listCount,BOOL hasData))block andFailedBlock:(void(^)())FailedBlock;
+ (void)addListWithUser:(IMGUser *)user andListName:(NSString *)listName andCityId:(NSNumber*)cityId andBlock:(void (^)(NSDictionary *dataDic))block  failure:(void(^)(NSString *exceptionMsg))failureBlock;
+ (void)editListWithUser:(IMGUser *)user andListName:(NSString *)listName andListId:(NSNumber *)listId andBlock:(void (^)(BOOL status,NSString *exceptionmsg))block;
+ (void)delListWithUser:(IMGUser *)user andListId:(NSNumber *)listId andBlock:(void (^)(BOOL status,NSString *exceptionmsg))block;

+ (void)getListDetailWithUser:(IMGUser *)user andOffset:(int)offset andMax:(int)max andListId:(NSNumber *)listId andBlock:(void (^)(NSInteger total,NSArray *dataArray))block;
+ (void)listAddRestaurantWithUser:(IMGUser *)user andListId:(NSNumber *)listId andRestaurantId:(NSNumber *)restaurantId andBlock:(void (^)(BOOL succeed,id status))block;
+ (void)listRemoveWithUser:(IMGUser *)user andListId:(NSNumber *)listId andRestaurantId:(NSNumber *)restaurantId andBlock:(void (^)(void))block failure:(void(^)(NSString *exceptionMsg))failureBlock;
+ (void)removeRestaurantFromListWithRestaurantId:(NSNumber *)restaurantId andListId:(NSNumber *)listId andBlock:(void (^)(void))block failure:(void(^)(NSString *exceptionMsg))failureBlock;

+(void)update:(NSArray *)restaurantArray forRestaurant:(IMGRestaurant *)restaurant;

+ (void)listIsInList:(IMGUser *)user andRestaurantId:(NSNumber *)restaurantId andBlock:(void (^)(BOOL isInList))block;
+ (void)listAddRestaurantsWithUser:(IMGUser *)user andListIds:(NSArray *)listIds andRestaurantIds:(NSArray *)restaurantIds andBlock:(void (^)(BOOL succeed,id status))block;


+(NSURLSessionTask*)recentlyViewRestaurants:(NSDictionary*)parameters block:(completionHandler) block;
+(NSURLSessionTask*)searchRestaurant:(NSString*)keyword withParams:(NSDictionary*)params andBlock:(completionHandler)block;
+(NSURLSessionTask*)mylist_recentlyViewRestaurants:(NSDictionary*)parameters block:(completionHandler) block;
+(NSURLSessionTask*)mylist_searchRestaurant:(NSString*)keyword withParams:(NSDictionary*)params andBlock:(completionHandler)block;

+ (void)getListWithUser:(IMGUser *)user restaurantId:(NSNumber*)restaurantId  cityId:(NSNumber*)cityId offset:(NSInteger)offset max:(NSInteger)max andBlock:(void (^)(NSArray *dataArray,BOOL hasData))block;
+(void)mylist:(NSNumber*)listId user:(IMGUser*)user restaurant:(NSNumber*)restaurantId note:(NSString*)noteText block:(void(^)(BOOL succeed))block;
+(void)getRestaurantSaved:(NSNumber*)restaurantId withUser:(IMGUser*)user andBlock:(void(^)(BOOL,NSInteger))block;

+(NSURLSessionTask*)recentlyViewBookedRestaurants:(NSDictionary*)parameters block:(completionHandler)block;
+(NSURLSessionTask*)searchBookedRestaurant:(NSString*)keyword withParams:(NSDictionary*)params andBlock:(completionHandler)block;
@end
