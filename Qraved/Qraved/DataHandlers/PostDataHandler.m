//
//  PostData.m
//  Qraved
//
//  Created by Shine Wang on 7/25/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "PostDataHandler.h"
//#import "AFHTTPRequestOperationManager.h"
#import "IMGUser.h"

@implementation PostDataHandler


- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
    }
    return self;
}


+(PostDataHandler *)sharedPostData
{
    static PostDataHandler *postData=nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        postData=[[PostDataHandler alloc]init];
    });
    return postData;
}
- (void)postMenuImage:(NSDictionary *)parameters andSuccessBlock:(void(^)())successBlock andFailedBlock:(void(^)(NSString *exceptionMsg))failedBlock
{
    [[IMGNetWork sharedManager] POST:@"app/menuphoto/userupload" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        NSString *errorMessage = [responseObject objectForKey:@"exceptionmsg"];
        if(responseObject && errorMessage){
            failedBlock(errorMessage);
        }else if(responseObject){
            successBlock(responseObject);
        }
    } failure:^(NSURLSessionDataTask *operation, NSError *error){
        failedBlock(error.localizedDescription);
    }];

}
-(void)postImage:(UIImage *)image andTitle:(NSString *)title andDescription:(NSString *)description andSuccessBlock:(void(^)())successBlock andFailedBlock:(void(^)())failedBlock
{
    if(image){
        NSLog(@"压缩之前。。。。%@",[NSDate date]);
        NSData *fileData = UIImageJPEGRepresentation(image,0.3);
        
        NSLog(@"压缩之后。。。。%@",[NSDate date]);
        NSNumber *userID = [NSNumber numberWithInt:1];
        NSString *postTitle =title;
        NSString *postDescription =description;
        NSDictionary* options = [[NSDictionary alloc] initWithObjectsAndKeys:userID, @"userID",
                                 postTitle, @"postTitle",
                                 postDescription, @"postDescription",nil];
        AFHTTPSessionManager *webmanager = [AFHTTPSessionManager manager];
        webmanager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];

        [webmanager POST:[NSString stringWithFormat:@"%@common/header/uploadImgByMobile",QRAVED_WEB_UPLOAD_PHOTO_SERVER] parameters:options constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
             [formData appendPartWithFileData:fileData name:@"up-image" fileName:[NSString stringWithFormat:@"%f.jpg",[[NSDate date]timeIntervalSince1970]] mimeType:@"multipart/form-data"];
        } progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull operation, id  _Nullable responseObject) {
            NSLog(@"after upload。。。。首==%@",[NSDate date]);
            NSString *message ;
            if (operation.error == nil &&[[responseObject objectForKey:@"uploaded"] intValue]) {
                NSDictionary *JSON =(NSDictionary*)responseObject;
                successBlock(JSON);
            }
            else {
                message = operation.error.localizedDescription;
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle: @"Add Post"
                                      message: message\
                                      delegate: nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
                [alert show];
            }

        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            failedBlock(error);

        }];
    }
}
-(void)postbackImage:(UIImage *)image andTitle:(NSString *)title andDescription:(NSString *)description andSuccessBlock:(void(^)())successBlock andFailedBlock:(void(^)())failedBlock{


    if(image){
        NSLog(@"压缩背景图之前。。。。%@",[NSDate date]);
        NSData *fileData = UIImageJPEGRepresentation(image,0.3);
        
        NSLog(@"压缩背景图之后。。。。%@",[NSDate date]);
        NSNumber *userID = [NSNumber numberWithInt:1];
        NSString *postTitle =title;
        NSString *postDescription =description;
        NSDictionary* options = [[NSDictionary alloc] initWithObjectsAndKeys:userID, @"userID",
                                 postTitle, @"postTitle",
                                 postDescription, @"postDescription",nil];
        AFHTTPSessionManager *webmanager = [AFHTTPSessionManager manager];
        webmanager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
        
//        NSLog(@"%@",[NSString stringWithFormat:@"%@common/header/uploadImgByMobile",QRAVED_WEB_UPLOAD_PHOTO_SERVER]);
        [webmanager POST:[NSString stringWithFormat:@"%@common/header/uploadImgByMobile",QRAVED_WEB_UPLOAD_PHOTO_SERVER] parameters:options constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            [formData appendPartWithFileData:fileData name:@"up-image" fileName:[NSString stringWithFormat:@"%f.jpg",[[NSDate date]timeIntervalSince1970]] mimeType:@"multipart/form-data"];
        } progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull operation, id  _Nullable responseObject) {
            NSLog(@"after upload。。。。 edit==%@",[NSDate date]);
            NSString *message ;
            if (operation.error == nil &&[[responseObject objectForKey:@"uploaded"] intValue]) {
                NSDictionary *JSON =(NSDictionary*)responseObject;
                successBlock(JSON);
            }
            else {
                message = operation.error.localizedDescription;
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle: @"Add Post"
                                      message: message\
                                      delegate: nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil];
                [alert show];
            }
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            failedBlock(error);
            
        }];
    }




}

+(void)uploadDishes:(NSString*)dishes andSuccessBlock:(void(^)())successBlock andFailedBlock:(void(^)())failedBlock{
    IMGUser *user=[IMGUser currentUser];
    NSMutableDictionary *params=[NSMutableDictionary dictionaryWithCapacity:3];
    [params setObject:user.userId forKey:@"userId"];
    [params setObject:user.token forKey:@"t"];
    [params setObject:dishes forKey:@"dishIds"];
    [[IMGNetWork sharedManager] POST:@"app/moderateReview/uploadDishes" parameters:params progress:nil success:^(NSURLSessionDataTask *operation, id responseObject){
        NSLog(@"%@",responseObject);
        successBlock(responseObject);
    } failure:^(NSURLSessionDataTask *operation, NSError *error){
        failedBlock();
    }];
}
+(void)batchupload:(NSMutableDictionary *)param andSuccessBlock:(void(^)(id responseObject))successBlock andFailedBlock:(void(^)())failedBlock{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",QRAVED_WEB_SERVICE_SERVER,@"dish/batchupload"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [param setValue:@"2" forKey:@"client"];
    [param setValue:QRAVED_WEB_SERVICE_VERSION forKey:@"v"];
    [param setValue:QRAVED_APIKEY forKey:@"appApiKey"];
    NSDictionary *json = param;
    NSData *data = [NSJSONSerialization dataWithJSONObject:json options:NSJSONWritingPrettyPrinted error:nil];
    request.HTTPBody = data;
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (connectionError==nil) {
            NSError *error = nil;
            id jsonObject = [NSJSONSerialization JSONObjectWithData:data
                                                            options:NSJSONReadingAllowFragments
                                                              error:&error];
            NSLog(@"%@",jsonObject);
            if (error == nil) {
                successBlock(jsonObject);
            }else{
                failedBlock();
            }
        }else{
            failedBlock();
        }
    }];
}
@end
