//
//  MenuPhotoHandler.h
//  Qraved
//
//  Created by Evan on 16/4/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IMGMenu.h"

@interface MenuPhotoHandler : NSObject

+ (void)getUserUploadMenuDataFromService:(NSDictionary *)parameters andBlock:(void (^)(NSMutableArray *menuList,NSNumber *offset,NSMutableArray *menuPhotoArray,NSNumber *menuPhotoCount,NSNumber *menuPhotoCountTypeRestant,NSNumber *menuPhotoCountTypeBar,NSNumber *menuPhotoCountTypeDelivery,IMGMenu *menu_))block;

+(void)getMenuGalleryDataFromService:(NSNumber *)restaurantId offset:(NSNumber*)offset andBlock:(void (^)(NSMutableArray *menuList,NSNumber *offset,NSMutableArray *menuPhotoArray,NSNumber *menuPhotoCount,NSNumber *menuPhotoCountTypeRestant,NSNumber *menuPhotoCountTypeBar,NSNumber *menuPhotoCountTypeDelivery,IMGMenu *menu_,NSNumber *offsetBlock))block;

@end
