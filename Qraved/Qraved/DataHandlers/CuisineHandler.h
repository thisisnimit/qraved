//
//  CuisineHandler.h
//  Qraved
//
//  Created by Jeff on 10/8/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataHandler.h"

@interface CuisineHandler : DataHandler

+(void)getRestaurantsFromServer;

@end
