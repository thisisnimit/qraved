//
//  EventHandler.m
//  Qraved
//
//  Created by Jeff on 10/8/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "EventHandler.h"
#import "DBManager.h"
#import "IMGEvent.h"
#import "IMGRestaurantEvent.h"

@implementation EventHandler


-(void)getDatasFromServer
{
    
    [[IMGNetWork sharedManager] GET:@"event/list" parameters:nil progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueue];
        [queue inDatabase:^(FMDatabase *db) {
            [db executeUpdate:@"delete from IMGEvent"];
            NSArray *array = responseObject;
            for (int i=0; i<array.count; i++) {
                NSDictionary *dict = [array objectAtIndex:i];
                IMGEvent *event = [[IMGEvent alloc]init];
                [event setValuesForKeysWithDictionary:dict];
                [[DBManager manager]insertModel:event selectField:@"eventId" andSelectID:event.eventId andFMDatabase:db];
            }
        }];
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"AFHTTPRequestOperation erors when get datas:%@",error.localizedDescription);
    }];
    
    
}

+(void)getRestaurantsFromServer{
    [[IMGNetWork sharedManager] GET:@"event/restaurants" parameters:nil progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        [[DBManager manager] deleteWithSql:@"delete from IMGRestaurantEvent"];
        NSArray *array = responseObject;
        for (int i=0; i<array.count; i++){
            NSDictionary *dict = [array objectAtIndex:i];
            IMGRestaurantEvent *restaurantEvent = [[IMGRestaurantEvent alloc]init];
            restaurantEvent.eventId=[dict objectForKey:@"eventId"];
            restaurantEvent.restaurantId=[dict objectForKey:@"restaurantId"];
            [[DBManager manager]insertRelation:restaurantEvent selectField:@"eventId" andSelectID:restaurantEvent.eventId relationField:@"restaurantId" andRelationId:restaurantEvent.restaurantId];
        }
        NSLog(@"event/restaurants");
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"AFHTTPRequestOperation erors when get datas:%@",error.localizedDescription);
    }];
    
    
}

@end
