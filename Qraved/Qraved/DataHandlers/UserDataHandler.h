//
//  UserData.h
//  Qraved
//
//  Created by Shine Wang on 7/15/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "FBUser.h"
#import "TWitterUser.h"
#import <Google/SignIn.h>

@interface UserDataHandler : NSObject
FOUNDATION_EXPORT NSString *const USER_LOGINTYPE_WITHFACEBOOK;
FOUNDATION_EXPORT NSString *const USER_LOGINTYPE_WITHSELF;
FOUNDATION_EXPORT NSString *const USER_LOGINTYPE_WITHTWITTER;

-(void)faceBookLogin:(NSArray *)permissions delegateWithBlock:(void (^)())block failedBlock:(void(^)())failedBlock;
-(void)faceBookConnect:(NSArray *)permissions delegateWithBlock:(void (^)())block failedBlock:(void(^)())failedBlock;
//-(void)faceBookConnect:(FBSession *)session state:(FBSessionState)state error:(NSError *)fbStateChangeError delegateWithBlock:(void (^)())block failedBlock:(void(^)())failedBlock;
//-(void)faceBookLogin:(FBSession *)session state:(FBSessionState)state error:(NSError *)fbStateChangeError delegateWithBlock:(void (^)())block failedBlock:(void(^)())failedBlock;
- (void)twitterConnect:(NSDictionary *)twitterResult withError:(NSError *)twitterOpenError withDelegateBlock: (void (^)())block failedBlock:(void(^)())failedBlock;
- (void)twitterLogin :(NSDictionary *)twitterResult withError:(NSError *)twitterOpenError withDelegateBlock: (void (^)())block failedBlock:(void(^)())failedBlock;
- (void) requestConnect:(NSDictionary *)parameterDictionary delegateWithBlock:(void (^)())block failedBlock:(void(^)())faildBlock;
- (void) requestLogin:(NSDictionary *)parameterDictionary delegateWithBlock:(void (^)())block failedBlock:(void(^)(NSString *errorMsg))faildBlock;
-(void)googleLogin:(GIDGoogleUser*)user error:(NSError*)error delegateWithBlock:(void (^)())block failedBlock:(void(^)())failedBlock;

- (NSDictionary *)getParameters;
- (void)emailLogin :(NSDictionary *)param withError:(NSError *)error withDelegateBlock: (void (^)())block  failBlock:(void (^)(NSString *errorMsg))Failblock;

- (void)isSignIn:(NSDictionary *)parameters withBlock:(void (^)())block failedBlock:(void(^)(NSString *errorMsg))failedBlock;
- (void)faceBookSignUp:(NSDictionary *)parameter withBlock:(void (^)())block failedBlock:(void(^)(NSString *errorMsg))failedBlock;
- (BOOL)isSignUp;
- (BOOL)isUpdate;
- (void)updateUserInfo:(NSDictionary *)parameters withBlock:(void (^)())block failedBlock:(void(^)())failedBlock;
+(void)logout;
+(void)personalizationOptionFetch:(NSDictionary *)parameters andSuccessBlock:(void (^)(id responseObjectLocal))successBlock andFailedBlock:(void(^)(NSString *errorMsg)) failedBlock;
+(void)userPersonalizationAdd:(NSDictionary *)parameters andSuccessBlock:(void (^)(id responseObjectLocal))successBlock andFailedBlock:(void(^)(NSString *errorMsg)) failedBlock;

+(void)loginFirst:(NSDictionary *)parameters andSuccessBlock:(void (^)(id responseObjectLocal))successBlock andFailedBlock:(void(^)(NSString *errorMsg)) failedBlock;
+(void)preferredLocationAdd:(NSDictionary *)parameters andSuccessBlock:(void (^)(id responseObjectLocal))successBlock andFailedBlock:(void(^)(NSString *errorMsg)) failedBlock;

+(void)deeplink:(NSDictionary *)parameters andSuccessBlock:(void (^)(id responseObject))successBlock andFailedBlock:(void(^)()) failedBlock;

@end
