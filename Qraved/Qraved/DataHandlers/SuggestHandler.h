//
//  SuggestHandler.h
//  Qraved
//
//  Created by Lucky on 15/10/9.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataHandler.h"

@interface SuggestHandler : DataHandler

+ (void)suggestWithDic:(NSDictionary *)dataDic withSuccess:(void(^)(bool success))successblock withFail:(void(^)(bool fail,NSString* erroInfo))failblock;
+ (void)suggestJournalWithDic:(NSDictionary *)dataDic withSuccess:(void(^)(bool succes))successBlock withfail:(void(^)(bool fail,NSString* erroInfo))erroBlock;
@end
