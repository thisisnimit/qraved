//
//  SettingHandler
//  Qraved
//
//  Created by Jeff on 10/8/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "SettingHandler.h"
#import "DBManager.h"
#import "IMGSetting.h"

@implementation SettingHandler

-(void)getDatasFromServer
{
    
    [[IMGNetWork sharedManager] GET:@"setting/list" parameters:nil progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueue];
         [queue inDatabase:^(FMDatabase *db) {
             [db executeUpdate:@"delete from IMGSetting"];
             NSArray *array = responseObject;
             for (int i=0; i<array.count; i++) {
                 NSDictionary *dict = [array objectAtIndex:i];
                 IMGSetting *setting = [[IMGSetting alloc]init];
                 [setting setValuesForKeysWithDictionary:dict];
                 [dataArray addObject:setting];
                 [[DBManager manager]insertModel:setting selectField:@"settingId" andSelectID:setting.settingId andFMDatabase:db];
             }
         }];
     }failure:^(NSURLSessionDataTask *operation, NSError *error) {
         NSLog(@"AFHTTPRequestOperation erors when get datas:%@",error.localizedDescription);
     }];
    
}


@end
