//
//  V2_CityModel.h
//  Qraved
//
//  Created by harry on 2017/6/14.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface V2_CityModel : NSObject

@property (nonatomic,retain) NSNumber *cityId;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *latitude;
@property (nonatomic,copy) NSString *longitude;


@end
