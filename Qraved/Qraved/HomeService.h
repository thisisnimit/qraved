//
//  HomeService.h
//  Qraved
//
//  Created by harry on 2017/7/12.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HomeService : NSObject

+ (void)getHomeDataWithParam:(NSDictionary *)params andBlock:(void(^)(id instagram,id location,id instagramImageArr,id locationImageArr))successBlock andError:(void(^)(NSError *error))errorBlock;
+(void)getInstagramPhotos:(NSDictionary *)params andBlock:(void(^)(id instagramPhotos))successBlock andError:(void(^)(NSError *error))errorBlock;
+(void)getLocation:(NSDictionary *)params andIsPreferred:(BOOL)isPreferred andBlock:(void(^)(id locationArr))successBlock andError:(void(^)(NSError *error))errorBlock;

+(void)getContribution:(NSDictionary *)params andBlock:(void(^)(id contributionArr))successBlock andError:(void(^)(NSError *error))errorBlock;

+(void)deleteErrorImageUrlerrorCode:(NSInteger )errorCode imageUrl:(NSString *)imageUrl mediaId:(NSString *)mediaId photoId:(NSString *)photoId celebrityPhotoId:(NSString *)celebrityPhotoId  andBlock:(void(^)(id contributionArr))successBlock andError:(void(^)(NSError *error))errorBlock;
+(void)getCategories:(NSDictionary *)params andBlock:(void(^)(id locationArr))successBlock andError:(void(^)(NSError *error))errorBlock;

+(void)getFoodTag:(NSDictionary *)params andBlock:(void(^)(id locationArr))successBlock andError:(void(^)(NSError *error))errorBlock;
+(void)getQuicklySearchFoodTag:(NSDictionary *)params andBlock:(void(^)(id foodtagArr))successBlock andError:(void(^)(NSError *error))errorBlock;
+(void)getRecentlyViewedRestaurant:(NSDictionary *)params andBlock:(void(^)(id restaurantArr))successBlock andError:(void(^)(NSError *error))errorBlock;
+ (void)getInstagramFilter:(NSDictionary *)params andBlock:(void(^)(NSArray *filterArr))successBlock andError:(void(^)(NSError *error))errorBlock;

+ (void)getInstagramHashtags:(NSDictionary *)params andBlock:(void(^)(NSArray *hashtagsArr))successBlock andError:(void(^)(NSError *error))errorBlock;

+ (void)getHomeLocation:(NSDictionary *)params andBlock:(void(^)(NSArray *locationArr))successBlock andError:(void(^)(NSError *error))errorBlock;

+ (void)getDeliveryBannerWithBlock:(void(^)(NSArray *bannerArr))successBlock andError:(void(^)())errorBlock;

+ (void)getDeliveryFoodTagWithBlock:(void(^)(NSArray *tagArr))successBlock andError:(void(^)())errorBlock;

+ (void)getDeliveryJournal:(NSDictionary *)params andBlock:(void(^)(NSArray *journalArr, NSString *journalTitle))successBlock andError:(void(^)())errorBlock;

@end
