//
//  V2_InstagramDistrictCell.m
//  Qraved
//
//  Created by harry on 2017/8/1.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_InstagramDistrictCell.h"

@implementation V2_InstagramDistrictCell
{
    UIButton *btnDistrict;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    if (btnDistrict) {
        [btnDistrict removeFromSuperview];
    }
    btnDistrict = [UIButton buttonWithType:UIButtonTypeCustom];
    btnDistrict.frame = self.bounds;
    btnDistrict.layer.masksToBounds = YES;
    btnDistrict.layer.cornerRadius = self.bounds.size.height/2;
    btnDistrict.titleLabel.font = [UIFont systemFontOfSize:12];
    btnDistrict.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    [self.contentView addSubview:btnDistrict];
    [btnDistrict addTarget:self action:@selector(chooseDistrict:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)chooseDistrict:(UIButton *)button{
    btnDistrict.backgroundColor = [UIColor colorWithHexString:@"DE2029"];
    [btnDistrict setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.model.selected = YES;
    if (self.chooseDistrict) {
        self.chooseDistrict(self.model);
    }
}

- (void)setModel:(V2_AreaModel *)model{
    _model = model;
    
    if ([model.name isEqualToString:@"My Location"]) {
        [btnDistrict setImage:[UIImage imageNamed:@"icn_mylocation_selected"] forState:UIControlStateSelected];
        [btnDistrict setImage:[UIImage imageNamed:@"icn_mylocation_idle"] forState:UIControlStateNormal];
        btnDistrict.imageEdgeInsets = UIEdgeInsetsMake(0, 95, 0, 10);
        btnDistrict.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 30);
    }else if ([model.name isEqualToString:@"All"]){
        
    }else{
        if ([model.landMarkTypeId isEqual:@1]) {
            [btnDistrict setImage:[UIImage imageNamed:@"ic_badge_mall_select.png"] forState:UIControlStateSelected];
            [btnDistrict setImage:[UIImage imageNamed:@"ic_badge_mall_idle.png"] forState:UIControlStateNormal];
            btnDistrict.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        }else if ([model.landMarkTypeId isEqual:@2]){
            [btnDistrict setImage:[UIImage imageNamed:@"ic_badge_hotel_idle_select.png"] forState:UIControlStateSelected];
            [btnDistrict setImage:[UIImage imageNamed:@"ic_badge_hotel_idle.png"] forState:UIControlStateNormal];
             btnDistrict.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        }
        
    }
    [btnDistrict setTitle:model.name forState:UIControlStateNormal];
    
    if (model.selected) {
        btnDistrict.selected = YES;
        btnDistrict.backgroundColor = [UIColor colorWithHexString:@"DE2029"];
        [btnDistrict setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }else{

        btnDistrict.selected = NO;
        btnDistrict.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
        [btnDistrict setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    }
}

@end
