//
//  BookHandler.h
//  Qraved
//
//  Created by apple on 17/4/10.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookHandler : NSObject

+(void)getMonthTimeWithParams:(NSDictionary*)params andBlock:(void(^)(NSArray *returnArray))block;
+(void)getOfferDateWithParams:(NSDictionary*)params andBlock:(void(^)(NSArray *thisMonthArr,NSArray *nextMonthArr))block;
+(void)getDayAndTimesWithParams:(NSDictionary*)params andBlock:(void(^)(NSArray *returnArray))block andErroBlock:(void(^)())erroblock;
+(void)confirmBookingInfomationWithParams:(NSDictionary*)params andBlock:(void(^)(id dic))block;
+(void)judgeVoucherCodeWithParams:(NSDictionary*)params andBlock:(void(^)(NSString *status,NSString *exceptionType))block andErrorBlock:(void(^)(NSString *errorStr))errorBlock;

@end
