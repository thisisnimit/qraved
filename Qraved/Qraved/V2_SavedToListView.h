//
//  V2_SavedToListView.h
//  Qraved
//
//  Created by harry on 2017/7/14.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface V2_SavedToListView : UIView

@property (nonatomic, strong)id controller;
@property (nonatomic, strong) IMGRestaurant *restaurant;
@property (nonatomic, copy) void(^refreshRestaurant)(BOOL liked,NSString *listName);
@property (nonatomic, copy) void(^changeButtonTapped)();
@end
