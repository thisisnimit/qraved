//
//  V2_ArticleModel.h
//  Qraved
//
//  Created by harry on 2017/6/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface V2_ArticleModel : NSObject

@property (nonatomic,copy) NSString *action;
@property (nonatomic,copy) NSString *link;
@property (nonatomic,copy) NSString *main_photo;
@property (nonatomic,copy) NSString *post_date;
@property (nonatomic,copy) NSNumber *view_count;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSNumber *articleId;

@end
