//
//  V2_GuideView.h
//  Qraved
//
//  Created by harry on 2017/6/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGDiningGuide.h"
@interface V2_GuideView : UIView

@property (nonatomic, copy) NSArray *guideArray;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) void(^seeAllGuide)();
@property (nonatomic, copy) void(^gotoDiningGuideDetail)(IMGDiningGuide *diningGuide);
@end
