//
//  V2_BannerModel.h
//  Qraved
//
//  Created by harry on 2017/7/28.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface V2_BannerModel : NSObject


@property (nonatomic, copy) NSDictionary *attributes;
@property (nonatomic, copy) NSString *bannerDescription;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *startDate;
@property (nonatomic, copy) NSString *endDate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *bannerId;
@property (nonatomic, copy) NSString *bannerImage;


@end
