//
//  V2_DistrictModel.h
//  Qraved
//
//  Created by harry on 2017/6/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface V2_DistrictModel : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSArray *districts;
@property (nonatomic, copy) NSNumber *districtId;

@end

@interface V2_AreaModel : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *areaId;
@property (nonatomic, copy) NSNumber *districtId;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSNumber *landMarkTypeId;
@property (nonatomic, copy) NSString *landMarkTypeName;
@property (nonatomic, assign) BOOL selected;

@end
