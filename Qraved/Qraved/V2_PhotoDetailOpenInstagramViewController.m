//
//  V2_PhotoDetailOpenInstagramViewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/8/31.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_PhotoDetailOpenInstagramViewController.h"
#import "LoadingView.h"
@interface V2_PhotoDetailOpenInstagramViewController ()<UIWebViewDelegate>

@end

@implementation V2_PhotoDetailOpenInstagramViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (!self.isFromWebSite) {
        self.title = @"Instagram";
    }
    
    [self initUI];
    [self addBackBtn];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [[LoadingView sharedLoadingView] stopLoading];;
}

- (void)initUI{

    UIWebView *webv = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight +20 - IMG_StatusBarAndNavigationBarHeight)];
    webv.backgroundColor = [UIColor whiteColor];
    webv.delegate = self;
    NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:self.instagram_link]];
    [webv loadRequest:request];
    [self.view addSubview:webv];
    
    
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    [[LoadingView sharedLoadingView] startLoading];
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [[LoadingView sharedLoadingView] stopLoading];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
}

@end
