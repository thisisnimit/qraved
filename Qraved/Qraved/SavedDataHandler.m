//
//  SavedDataHandler.m
//  Qraved
//
//  Created by apple on 2017/5/15.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "SavedDataHandler.h"

@implementation SavedDataHandler

+(void)getSavedPreviousRestaurantsWithPara:(NSDictionary *)parameter andBlock:(void(^)(NSMutableArray *restaurantsArr))block{
    
    [[IMGNetWork sharedManager] POST:@"restaurant/previous/unsaved" parameters:parameter progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSArray *reponserObjectArr = [responseObject objectForKey:@"restaurantList"];
        NSMutableArray *restaurantArr = [NSMutableArray array];
        for (NSDictionary *dic  in reponserObjectArr) {
            IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
            [restaurant setValuesForKeysWithDictionary:dic];
            restaurant.ratingScore = [dic objectForKey:@"rating"];
            NSString *dollar;
            switch(restaurant.priceLevel.intValue){
                case 1:dollar=@"Below 100K";break;
                case 2:dollar=@"100K - 200K";break;
                case 3:dollar=@"200K - 300K";break;
                case 4:dollar=@"Start from 300K";break;
            }
            restaurant.priceName = dollar;
            restaurant.goFoodLink = [dic objectForKey:@"goFoodLink"];
            [restaurantArr addObject:restaurant];
        }
        block(restaurantArr);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
    
}
+(void)getWantToGoListWithPara:(NSDictionary *)parameter andBlock:(void(^)(NSArray *restaurantsArr,NSNumber *restaurantListItemCount,NSNumber *listId))block andFailedBlock:(void(^)())FailedBlock{
    [[IMGNetWork sharedManager] POST:@"list/detail/want2go" parameters:parameter progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (responseObject != nil && [[parameter objectForKey:@"offset"] intValue] == 0) {
            [CommonMethod setJsonToFile:SAVED_CACHE andJson:responseObject];
        }
 
        NSNumber *restaurantListItemCount = [responseObject objectForKey:@"restaurantListItemCount"];
        NSNumber *listId = [responseObject objectForKey:@"listId"];
        block([CommonMethod getSavedRestaurant:responseObject],restaurantListItemCount,listId);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        FailedBlock();
    }];

}
@end
