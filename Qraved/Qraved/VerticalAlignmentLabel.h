//
//  VerticalAlignmentLabel.h
//  Qraved
//
//  Created by harry on 2017/12/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum
{
    VerticalAlignmentTop = 0, 
    VerticalAlignmentMiddle,
    VerticalAlignmentBottom,
} VerticalAlignment;

@interface VerticalAlignmentLabel : UILabel

//@private
//VerticalAlignment _verticalAlignment;
//}
//
@property (nonatomic) VerticalAlignment verticalAlignment;

@end
