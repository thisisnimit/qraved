//
//  V2_QuickSearchModel.h
//  Qraved
//
//  Created by harry on 2017/6/15.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface V2_QuickSearchModel : NSObject


@property (nonatomic,copy) NSString *display_name;
@property (nonatomic,copy) NSNumber *searchId;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSNumber *order;
@property (nonatomic,copy) NSString *seo_keyword;

@end
