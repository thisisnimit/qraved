//
// Created by Tek Yin on 5/24/17.
// Copyright (c) 2017 Qraved. All rights reserved.
//

#import "QRCheckmark.h"


@implementation QRCheckmark {

}

- (void)startAnimation {
    [self.layer removeAllAnimations];
    NSArray<CALayer *> *array = [self.layer.sublayers copy];
    for (CALayer       *lay in array) {
        [lay removeFromSuperlayer];
    }

    CAShapeLayer *mask = [[CAShapeLayer alloc] init];
    mask.frame = self.layer.bounds;
    mask.path  = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, 100, 100)].CGPath;


    CAShapeLayer *pathLayer = [CAShapeLayer layer];
    pathLayer.frame     = self.layer.bounds;
    pathLayer.path      = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, 100, 100)].CGPath;
    pathLayer.fillColor = [[UIColor redColor] CGColor];
    pathLayer.lineWidth = 1.0f;
    pathLayer.lineJoin  = kCALineJoinBevel;

    [self.layer addSublayer:pathLayer];

    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    pathAnimation.duration  = 0.2;
    pathAnimation.fromValue = @0.0f;
    pathAnimation.toValue   = @1.0f;
    pathAnimation.delegate  = self;
    [pathLayer addAnimation:pathAnimation forKey:@"scale"];


    [mask setFillRule:kCAFillRuleEvenOdd];
    self.layer.mask = mask;
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    CGPoint middle = CGPointMake(48, 70);

    UIBezierPath *path1 = [UIBezierPath bezierPath];
    [path1 moveToPoint:CGPointMake(middle.x - 48, middle.y - 48)];
    [path1 addLineToPoint:middle];
    [path1 addLineToPoint:CGPointMake(middle.x + 35, middle.y - 35)];

    CAShapeLayer *line1 = [CAShapeLayer layer];
    line1.frame       = self.bounds;
    line1.strokeColor = [[UIColor whiteColor] CGColor];
    line1.path        = path1.CGPath;
    line1.fillColor   = nil;
    line1.lineWidth   = 3.f;
    line1.lineJoin    = kCALineJoinRound;
    [self.layer addSublayer:line1];

    CABasicAnimation *anim1 = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    anim1.beginTime = CACurrentMediaTime() + 0.2;
    anim1.duration  = 0.2;
    anim1.fromValue = @0.0f;
    anim1.toValue   = @1.0f;
    anim1.fillMode  = kCAFillModeBackwards;
    [line1 addAnimation:anim1 forKey:@"anim1"];

    UIBezierPath *path2 = [UIBezierPath bezierPath];
    [path2 moveToPoint:CGPointMake(middle.x - 48, middle.y - 48)];
    [path2 addLineToPoint:CGPointMake(middle.x - 20, middle.y - 20)];

    CAShapeLayer *line2 = [CAShapeLayer layer];
    line2.frame       = self.bounds;
    line2.strokeColor = [[UIColor redColor] CGColor];
    line2.path        = path2.CGPath;
    line2.fillColor   = nil;
    line2.lineWidth   = 5.f;
    line2.lineJoin    = kCALineJoinRound;
    [self.layer addSublayer:line2];

    CABasicAnimation *anim2 = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    anim2.beginTime = CACurrentMediaTime() + 0.3;
    anim2.duration  = 0.1;
    anim2.fromValue = @0.0f;
    anim2.toValue   = @1.0f;
    anim2.fillMode  = kCAFillModeBackwards;
    [line2 addAnimation:anim2 forKey:@"anim2"];

}

- (void)reset {
    [self.layer removeAllAnimations];
    NSArray<CALayer *> *array = [self.layer.sublayers copy];
    for (CALayer       *lay in array) {
        [lay removeFromSuperlayer];
    }
}
@end