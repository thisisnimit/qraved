//
// Created by Tek Yin on 5/24/17.
// Copyright (c) 2017 Qraved. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface QRCheckmark : UIControl <CAAnimationDelegate>

@property(nonatomic, strong) NSMutableArray<CABasicAnimation *> *animations;

- (void)startAnimation;

- (void)reset;
@end