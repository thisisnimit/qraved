//
//  V2_MiniRDPView.m
//  Qraved
//
//  Created by harry on 2017/8/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_MiniRDPView.h"

@implementation V2_MiniRDPView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    self.backgroundColor = [UIColor darkGrayColor];
}

@end
