//
//  DataInitialHandler.m
//  Qraved
//
//  Created by Jeff on 10/8/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "DataInitialHandler.h"
#import "DBManager.h"
#import "RestaurantHandler.h"
#import "DistrictHandler.h"
#import "TagHandler.h"
#import "CuisineHandler.h"
#import "EventHandler.h"
#import "DiningGuideHandler.h"
#import "SettingHandler.h"
#import "MealHandler.h"
#import "IMGRewardTrigger.h"
#import "IMGRestaurant.h"
#import "IMGDistrict.h"
#import "IMGTag.h"
#import "IMGCuisine.h"
#import "IMGEvent.h"
#import "IMGMyList.h"
#import "IMGDiningGuide.h"
#import "IMGRestaurantCuisine.h"
#import "IMGRestaurantTag.h"
#import "IMGRestaurantDistrict.h"
#import "IMGRestaurantImage.h"
#import "IMGRestaurantEvent.h"
#import "IMGRestaurantDiningGuide.h"
#import "IMGRestaurantOffer.h"
#import "IMGReview.h"
#import "IMGMenu.h"
#import "IMGMediaComment.h"
#import "IMGRestaurantLike.h"
#import "IMGSetting.h"
#import "IMGMeal.h"
#import "IMGReservation.h"
#import "IMGDish.h"
#import "IMGUser.h"
#import "IMGThirdPartyUser.h"

#import "IMGSlot.h"
#import "IMGCity.h"
#import "IMGUserFavoriteRestaurant.h"
#import "IMGDiscoverEntity.h"
#import "IMGNotification.h"
#import "IMGRestaurantViewed.h"
#import "IMGOperatingState.h"
#import "IMGRestaurantLink.h"
#import "OperatingStateHandler.h"
#import "CityHandler.h"
#import "OfferHandler.h"
@implementation DataInitialHandler

-(void)initTables{
    [[DBManager manager] createTableWithClass:[IMGCity class]];
    [[DBManager manager] createTableWithClass:[IMGDistrict class]];
    [[DBManager manager] createTableWithClass:[IMGTag class]];
    [[DBManager manager] createTableWithClass:[IMGCuisine class]];
    [[DBManager manager] createTableWithClass:[IMGEvent class]];
    [[DBManager manager] createTableWithClass:[IMGRestaurant class]];
    [[DBManager manager] createTableWithClass:[IMGDiningGuide class]];
    [[DBManager manager] createTableWithClass:[IMGRestaurantTag class]];
    [[DBManager manager] createTableWithClass:[IMGRestaurantCuisine class]];
    [[DBManager manager] createTableWithClass:[IMGRestaurantDistrict class]];
    [[DBManager manager] createTableWithClass:[IMGRestaurantDiningGuide class]];
    [[DBManager manager] createTableWithClass:[IMGRestaurantEvent class]];
    [[DBManager manager] createTableWithClass:[IMGRestaurantOffer class]];
    [[DBManager manager] createTableWithClass:[IMGRestaurantImage class]];
    [[DBManager manager] createTableWithClass:[IMGRestaurantLike class]];
    [[DBManager manager] createTableWithClass:[IMGReview class]];
    [[DBManager manager] createTableWithClass:[IMGMenu class]];
    [[DBManager manager] createTableWithClass:[IMGMediaComment class]];
    [[DBManager manager] createTableWithClass:[IMGSetting class]];
    [[DBManager manager] createTableWithClass:[IMGMeal class]];
    [[DBManager manager] createTableWithClass:[IMGReservation class]];

    [[DBManager manager] createTableWithClass:[IMGDish class]];
    [[DBManager manager] createTableWithClass:[IMGUser class]];
    [[DBManager manager] createTableWithClass:[IMGThirdPartyUser class]];
    [[DBManager manager] createTableWithClass:[IMGSlot class]];
    [[DBManager manager] createTableWithClass:[IMGUserFavoriteRestaurant class]];
    [[DBManager manager] createTableWithClass:[IMGRewardTrigger class]];
    [[DBManager manager] createTableWithClass:[IMGDiscoverEntity class]];
    [[DBManager manager] createTableWithClass:[IMGNotification class]];
    [[DBManager manager] createTableWithClass:[IMGRestaurantViewed class]];
    [[DBManager manager] createTableWithClass:[IMGOperatingState class]];
    [[DBManager manager] createTableWithClass:[IMGRestaurantLink class]];
    [[DBManager manager] createTableWithClass:[IMGMyList class]];

}

-(void)initFromServer
{
    [self initTables];
    [[[DataHandler alloc] init] initFromServer];
    [[[OperatingStateHandler alloc]init]initFromServer];
    [[[CityHandler alloc]init]initFromServer];
    
//    [OfferHandler getOffersFromServer];
//    [[[MealHandler alloc]init]initFromServer];
//    [[[SettingHandler alloc]init]initFromServer];
//    [[[DiningGuideHandler alloc]init]initFromServer];
//    [[[DistrictHandler alloc]init]initFromServer];
//    [[[TagHandler alloc]init]initFromServer];
//    [[[CuisineHandler alloc]init]initFromServer];
//    [[[EventHandler alloc]init]initFromServer];
//    [[[RestaurantHandler alloc]init]initFromServer];
//    [RestaurantHandler getDishesFromServer:0 max:20];
    
//    [EventHandler  getRestaurantsFromServer];

}
@end
