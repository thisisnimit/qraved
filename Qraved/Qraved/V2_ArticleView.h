//
//  V2_ArticleView.h
//  Qraved
//
//  Created by harry on 2017/6/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGMediaComment.h"
@interface V2_ArticleView : UIView

@property (nonatomic, copy) NSArray *articleArray;
//@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) void(^seeAllJournal)();
@property (nonatomic, copy) void(^gotoJournalDetail)(IMGMediaComment *journal);

@end
