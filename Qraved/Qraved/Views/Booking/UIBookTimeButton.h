//
//  UIBookTimeButton.h
//  Qraved
//
//  Created by Jeff on 10/22/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBookTimeButton : UIButton

@property(nonatomic,retain) NSNumber *pax;
@property(nonatomic,retain) NSDate *bookDate;
@property(nonatomic,retain) NSString *bookTime;

@end
