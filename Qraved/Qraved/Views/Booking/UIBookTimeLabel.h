//
//  UIBookTimeLabel.h
//  Qraved
//
//  Created by Jeff on 10/23/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBookTimeLabel : UILabel

@property(nonatomic,retain) NSNumber *pax;
@property(nonatomic,retain) NSDate *bookDate;
@property(nonatomic,retain) NSString *bookTime;

@end
