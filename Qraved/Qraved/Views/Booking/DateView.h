//
//  DateView.h
//  Qraved
//
//  Created by Laura on 14-8-14.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"
#import "IMGRestaurantOffer.h"

@interface DateView : UIView
{
    NSCalendar*calendar;
    
    NSDate *currentDate;
    
    NSDate *currentStartDate;
    long currentYear;
    long currentMonth;
    long currentWeekDay;
    long currentStartWeekDay;
    NSRange currentDays;
    long currentDay;
    
    NSDate *nextStartDate;
    long nextYear;
    long nextMonth;
    long nextStartWeekDay;
    NSRange nextDays;
}
@property (nonatomic,assign)CGFloat heightOfView;

-(id)initWithRestaurant: (IMGRestaurant *)pRestaurant andTimeArray:(NSArray *)timeArray andOfferArray:(NSArray *)offerArray andOffer:(IMGRestaurantOffer *)tmpRestaurantOffer andCurrentOfferAvailableWeekDayArray:(NSArray *)tmpCurrentOfferAvailableWeekDayArray andOtherOfferAvailableWeekDayArray:(NSArray *)tmpOtherOfferAvailableWeekDayArray andPax:(NSNumber *)tmpPax andSlotTimeBookedArray:(NSArray *)tmpSlotTimeBookedArray andOfferTimeBookedArray:(NSArray *)tmpOfferTimeBookedArray andThisMonthArr:(NSArray *)thisCalendarMonthArr andNextMonthArr:(NSArray *)nextCalendarMonthArr;
@end
