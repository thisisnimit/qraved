//
//  DateView.m
//  Qraved
//
//  Created by Laura on 14-8-14.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "DateView.h"
#import "Label.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import "NSDate+Helper.h"
#import "UIColor+Helper.h"
#import "BookUtil.h"
#import "IMGUser.h"
#import "AppDelegate.h"
@implementation DateView
{
    IMGRestaurant *restaurant;
    IMGRestaurantOffer *restaurantOffer;
    NSArray *regularTimeArray;
    NSArray *restaurantOfferArray;
    
    NSArray *currentOfferAvailableWeekDayArray;
    NSArray *otherOfferAvailableWeekDayArray;
    
    NSArray *slotTimeBookedArray;
    NSArray *offerTimeBookedArray;
    
    NSNumber *pax;
    NSMutableArray *calendarCurrentOfferDateArrM;
    NSArray *currentMonthArr;
    NSArray *nextMonthArr;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
    }
    return self;
}

-(id)initWithRestaurant: (IMGRestaurant *)pRestaurant andTimeArray:(NSArray *)timeArray andOfferArray:(NSArray *)offerArray andOffer:(IMGRestaurantOffer *)tmpRestaurantOffer andCurrentOfferAvailableWeekDayArray:(NSArray *)tmpCurrentOfferAvailableWeekDayArray andOtherOfferAvailableWeekDayArray:(NSArray *)tmpOtherOfferAvailableWeekDayArray andPax:(NSNumber *)tmpPax andSlotTimeBookedArray:(NSArray *)tmpSlotTimeBookedArray andOfferTimeBookedArray:(NSArray *)tmpOfferTimeBookedArray andThisMonthArr:(NSArray *)thisCalendarMonthArr andNextMonthArr:(NSArray *)nextCalendarMonthArr{
    
    self = [super init];
    if(self){
        restaurant=pRestaurant;
        regularTimeArray=timeArray;
        restaurantOfferArray=offerArray;
        restaurantOffer=tmpRestaurantOffer;
        currentOfferAvailableWeekDayArray=tmpCurrentOfferAvailableWeekDayArray;
        otherOfferAvailableWeekDayArray=tmpOtherOfferAvailableWeekDayArray;
        slotTimeBookedArray=tmpSlotTimeBookedArray;
        offerTimeBookedArray=tmpOfferTimeBookedArray;
        if (thisCalendarMonthArr.count)
        {
            currentMonthArr = [[NSArray alloc] initWithArray:thisCalendarMonthArr];
        }
        else
            currentMonthArr = [[NSArray alloc] init];
        
        if (nextCalendarMonthArr.count)
        {
            nextMonthArr = [[NSArray alloc] initWithArray:nextCalendarMonthArr];
        }
        else
            nextMonthArr = [[NSArray alloc] init];
        
        pax=tmpPax;
        [self loadMainView];
    }
    return self;
}
- (void)getCalendarCurrentOfferDate
{
    //http://54.251.100.162:9033/date/bookable?offerId=768&peopleCount=2&restaurantID=570&endDate=2015-8-1&startDate=2015-6-12
    
    calendarCurrentOfferDateArrM = [[NSMutableArray alloc] init];
    NSDictionary * parameters = @{@"offerId":restaurantOffer.offerId,@"peopleCount":pax,@"restaurantID":restaurant.restaurantId};
    [[IMGNetWork sharedManager]POST:@"date/bookable" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSLog(@"responseObject = %@",responseObject);
         currentMonthArr = [responseObject objectForKey:@"thisMonth"];
         nextMonthArr = [responseObject objectForKey:@"nextmonth"];
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error)
     {
         NSLog(@"getCalendar error: %@",error.localizedDescription);
     }];
}
-(void)loadMainView{
    currentDate = [NSDate date];
    calendar = [NSCalendar currentCalendar];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"LLLL"];
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
    [dateFormatter1 setDateFormat:@"YYYY-MM-dd"];
    NSDateFormatter *dateFormatter2 = [[NSDateFormatter alloc]init];
    [dateFormatter2 setDateFormat:@"LLLL"];
    
    NSString *currentDateStr = [dateFormatter stringFromDate:currentDate];
    Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 50) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14] andTextColor:[UIColor redColor] andTextLines:1];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.text = [currentDateStr uppercaseString];
    [self addSubview:titleLabel];
    
    NSDateComponents*comps1;
    comps1 = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth |NSCalendarUnitDay | NSCalendarUnitWeekday) fromDate:currentDate];
    //获得当天是星期几
    currentWeekDay = [comps1 weekday]; // 星期几（注意，周日是“1”，周一是“2”。。。。）
    
    //获得当前年份月份
    currentYear = [comps1 year];
    currentMonth = [comps1 month];
    currentDay = [comps1 day];
    
    //获得当前月有多少天
    currentDays = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:currentDate];
    
    //获得当前月第一天
    NSString *currentStartDateStr = [NSString stringWithFormat:@"%ld-%ld-01",currentYear,currentMonth];
    currentStartDate = [dateFormatter1 dateFromString:currentStartDateStr];
    
    
    //获得当前月第一天是星期几
    NSDateComponents*comps2=[calendar components:(NSCalendarUnitWeekday) fromDate:currentStartDate];
    currentStartWeekDay = [comps2 weekday];
    

    if(currentStartWeekDay-1<1){
        currentStartWeekDay = 7;
    }else{
        currentStartWeekDay = currentStartWeekDay-1;
    }
    if(currentWeekDay-1<1){
        currentWeekDay = 7;
    }else{
        currentWeekDay = currentWeekDay-1;
    }
    
    NSArray *customWeeks = @[@"M",@"T",@"W",@"T",@"F",@"S",@"S"];
    titleLabel.frame = CGRectMake(13+42*((currentStartWeekDay-1)%7)*[AppDelegate ShareApp].autoSizeScaleX, 0, DeviceWidth, 30);
    for (int i=0; i<customWeeks.count; i++) {
        Label *label = [[Label alloc]initWithFrame:CGRectMake(13+42*i*[AppDelegate ShareApp].autoSizeScaleX, titleLabel.endPointY, 42, 42) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15] andTextColor:[UIColor color333333] andTextLines:1];
        label.text = [customWeeks objectAtIndex:i];
        label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:label];
    }
    NSDate *tmpThisMonthDate;
    int thisMonthDateCount=1;
    for (NSInteger i=currentStartWeekDay; i<currentDays.length+currentStartWeekDay; i++)
    {
        NSString *tmpNextDateStr = [NSString stringWithFormat:@"%ld-%ld-%d",currentYear,(long)currentMonth,thisMonthDateCount++];
        tmpThisMonthDate = [dateFormatter1 dateFromString:tmpNextDateStr];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        
        if(i<currentDay+currentStartWeekDay-1) {
            button.enabled = NO;
            [button setTitleColor:[UIColor colorCCCCCC] forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];//CalendarWhite
            
        }else if(i==currentDay+currentStartWeekDay-1) {
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageNamed:@"CalendarToday"] forState:UIControlStateNormal];
          if([BookUtil bookableFromRegular:regularTimeArray]){
                button.tag=3;
                [button addTarget:self action:@selector(currentMonthButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
              if([BookUtil bookableFromOffer:restaurantOffer withPax:pax andSlotTimeBookedArray:slotTimeBookedArray andOfferTimeBookedArray:regularTimeArray atDate:tmpThisMonthDate restaurant:restaurant]){
                  button.tag = 1;
                  [button addTarget:self action:@selector(currentMonthButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
              }else if([BookUtil bookableNotFromOffer:restaurantOffer inOfferArray:restaurantOfferArray withPax:pax andSlotTimeBookedArray:slotTimeBookedArray andOfferTimeBookedArray:offerTimeBookedArray atDate:tmpThisMonthDate restaurant:restaurant]){
                  button.tag=2;
                  [button addTarget:self action:@selector(currentMonthButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
              }
            }
        }

        else if([self isCalendarCurrentOfferWithCurrentDay:[NSNumber numberWithInteger:i-currentStartWeekDay+1] andCalendarsArr:currentMonthArr]){
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageNamed:@"CalendarCurrentOffer"] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(currentMonthButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            button.tag = 1;
            
        }else if([BookUtil bookableNotFromOffer:restaurantOffer inOfferArray:restaurantOfferArray withPax:pax andSlotTimeBookedArray:slotTimeBookedArray andOfferTimeBookedArray:offerTimeBookedArray atDate:tmpThisMonthDate restaurant:restaurant]){
            [button setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageNamed:@"white_point1"] forState:UIControlStateNormal];
            [button addTarget:self action:@selector(currentMonthButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            button.tag = 2;
        }else if([BookUtil bookableFromRegular:regularTimeArray withPax:pax andSlotTimeBookedArray:slotTimeBookedArray atDate:tmpThisMonthDate restaurant:restaurant]){
            [button setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
            [button setBackgroundColor:[UIColor whiteColor]];
            [button addTarget:self action:@selector(currentMonthButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            button.tag = 3;
        }else{
            button.enabled = NO;
            [button setTitleColor:[UIColor colorCCCCCC] forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal]; //CalendarWhite
        }
  
        [button setTitle:[NSString stringWithFormat:@"%ld",i-currentStartWeekDay+1] forState:UIControlStateNormal];
        button.frame = CGRectMake(13+42*((i-1)%7)*[AppDelegate ShareApp].autoSizeScaleX, (titleLabel.endPointY+40)+65*((i-1)/7), 42, 42);
        [self addSubview:button];
        
        
        
        UIView *linevIEW = [[UIView alloc] initWithFrame:CGRectMake(13+42*((i-1)%7)*[AppDelegate ShareApp].autoSizeScaleX, (titleLabel.endPointY+40)+63*((i-1)/7), 50, 1)];
        linevIEW.backgroundColor = [UIColor colorWithRed:228/255.0f green:228/255.0f blue:228/255.0f alpha:1.0f];
        [self addSubview:linevIEW];
        
        self.heightOfView = button.endPointY;
    }
    
 
    if (regularTimeArray.count>0) {
        NSString *monthString ;
        for (NSInteger i=0; i<regularTimeArray.count; i++) {
            if ([[regularTimeArray[regularTimeArray.count-i-1] objectForKey:@"bookable"] boolValue]) {
                NSString *bookDateString =  [regularTimeArray[regularTimeArray.count-i-1] objectForKey:@"bookDate"];
                NSArray *tempArry =[bookDateString componentsSeparatedByString:@"/"];
              monthString = tempArry[1];
                break;
            }
        }
      
        
        NSDateFormatter *dateFormatter1=[[NSDateFormatter alloc]init];
        [dateFormatter1 setDateFormat:@"yyyy/MM/dd"];
        NSDate *startDate=[NSDate date];
        NSString *currentMonthString = [[dateFormatter1 stringFromDate:startDate] componentsSeparatedByString:@"/"][1];
        NSLog(@"monthString====%@,currentMonthString===%@",monthString,currentMonthString);
        int calendarCount;
        if (monthString.length>0) {
            calendarCount = [monthString intValue] - [currentMonthString intValue];

        }else{
            calendarCount=1;
        }
        if (calendarCount<0) {
            calendarCount = calendarCount+12;
        }
        if (calendarCount>0) {
            for (int i =0; i<calendarCount; i++) {
                //获得下一个月开始日期
                NSDateComponents *comps3 = [[NSDateComponents alloc] init];
                [comps3 setYear:0];
                [comps3 setMonth:1];
                [comps3 setDay:0];
                nextStartDate = [calendar dateByAddingComponents:comps3 toDate:currentStartDate options:0];
                currentStartDate = nextStartDate;
                NSDateComponents *comps4;
                comps4 = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitWeekday fromDate:nextStartDate];
                nextYear = comps4.year;
                nextMonth = comps4.month;
                
                
                //获得下一个月第一天是星期几
                nextStartWeekDay = [comps4 weekday];
                
                //获得下一个月有多少天
                nextDays = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:nextStartDate];
                if (nextStartWeekDay-1<1) {
                    nextStartWeekDay = 7;
                }
                else
                {
                    nextStartWeekDay = nextStartWeekDay-1;
                }
                
                
                
                
                NSString *NextDateStr = [dateFormatter stringFromDate:nextStartDate];
                Label *titleLabel2 = [[Label alloc]initWithFrame:CGRectMake(13+42*((nextStartWeekDay-1)%7)*[AppDelegate ShareApp].autoSizeScaleX, self.heightOfView+30, DeviceWidth, 20) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14] andTextColor:[UIColor redColor] andTextLines:1];
                titleLabel2.textAlignment = NSTextAlignmentLeft;
                titleLabel2.text = [NextDateStr uppercaseString];
                [self addSubview:titleLabel2];
                
                CGFloat startY = titleLabel2.endPointY+10;
                for (int i=0; i<customWeeks.count; i++) {
                    Label *label = [[Label alloc]initWithFrame:CGRectMake(13+42*i*[AppDelegate ShareApp].autoSizeScaleX, startY, 42, 42) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15] andTextColor:[UIColor color333333] andTextLines:1];
                    label.text = [customWeeks objectAtIndex:i];
                    label.textAlignment = NSTextAlignmentCenter;
                    [self addSubview:label];
                }
                
                NSDate *tmpNextMonthDate;
                int nextMonthDateCount=1;
                for (NSInteger j=nextStartWeekDay; j<nextDays.length+nextStartWeekDay; j++) {
                    NSString *tmpNextDateStr = [NSString stringWithFormat:@"%ld-%ld-%d",nextYear,(long)nextMonth,nextMonthDateCount++];
                    tmpNextMonthDate = [dateFormatter1 dateFromString:tmpNextDateStr];
                    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                    
                    
                    if([self isCalendarCurrentOfferWithCurrentDay:[NSNumber numberWithInteger:i-nextStartWeekDay+1] andCalendarsArr:nextMonthArr]){
                        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        [button setBackgroundImage:[UIImage imageNamed:@"CalendarCurrentOffer"] forState:UIControlStateNormal];//quan hong//CalendarCurrentOffer
                        button.tag = 1;
                        [button addTarget:self action:@selector(nextMonthButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                        
                    }
                    else if([BookUtil bookableNotFromOffer:restaurantOffer inOfferArray:restaurantOfferArray withPax:pax andSlotTimeBookedArray:slotTimeBookedArray andOfferTimeBookedArray:offerTimeBookedArray atDate:tmpNextMonthDate restaurant:restaurant])
                        
                    {
                        [button setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
                        [button setBackgroundImage:[UIImage imageNamed:@"white_point1"] forState:UIControlStateNormal];//dian
                        button.tag = 2;
                        [button addTarget:self action:@selector(nextMonthButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                    }else if([BookUtil bookableFromRegular:regularTimeArray withPax:pax andSlotTimeBookedArray:slotTimeBookedArray atDate:tmpNextMonthDate restaurant:restaurant]){
                        [button setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
                        [button setBackgroundColor:[UIColor whiteColor]];
                        button.tag = 3;
                        [button addTarget:self action:@selector(nextMonthButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
                    }else{
                        button.enabled = NO;
                        [button setTitleColor:[UIColor colorCCCCCC] forState:UIControlStateNormal];
                        [button setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal]; //heng xian   CalendarWhite
                    }
                    [button setTitle:[NSString stringWithFormat:@"%ld",j-nextStartWeekDay+1] forState:UIControlStateNormal];
                    
                    button.frame = CGRectMake(13+42*((j-1)%7)*[AppDelegate ShareApp].autoSizeScaleX, (startY+40)+60*((j-1)/7), 41, 42);
                    
                    
                    [self addSubview:button];
                    
                    
                    UIView *linevIEW = [[UIView alloc] initWithFrame:CGRectMake(13+42*((j-1)%7)*[AppDelegate ShareApp].autoSizeScaleX, (startY+40)+55*((j-1)/7), 50, 1)];
                    linevIEW.backgroundColor = [UIColor colorWithRed:228/255.0f green:228/255.0f blue:228/255.0f alpha:1.0f];
                    [self addSubview:linevIEW];
                    
                    self.heightOfView = button.endPointY;
                }
            }
        }
    }
//    //获得下一个月开始日期
//    NSDateComponents *comps5 = [[NSDateComponents alloc] init];
//    [comps5 setYear:0];
//    [comps5 setMonth:1];
//    [comps5 setDay:0];
//    nextStartDate = [calendar dateByAddingComponents:comps5 toDate:nextStartDate options:0];
//    
//    NSDateComponents *comps6;
//    comps6 = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitWeekday fromDate:nextStartDate];
//    nextYear = comps6.year;
//    nextMonth = comps6.month;
//    
//    
//    //获得下一个月第一天是星期几
//    nextStartWeekDay = [comps6 weekday];
//    
//    //获得下一个月有多少天
//    nextDays = [calendar rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitMonth forDate:nextStartDate];
//    if (nextStartWeekDay-1<1) {
//        nextStartWeekDay = 7;
//    }
//    else
//    {
//        nextStartWeekDay = nextStartWeekDay-1;
//    }
//    
//    
//    
//    
//    NSString *NextDateStr1 = [dateFormatter stringFromDate:nextStartDate];
//    Label *titleLabel3 = [[Label alloc]initWithFrame:CGRectMake(0, self.heightOfView+50, DeviceWidth, 20) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:18] andTextColor:[UIColor blackColor] andTextLines:1];
//    titleLabel3.textAlignment = NSTextAlignmentCenter;
//    titleLabel3.text = NextDateStr1;
//    [self addSubview:titleLabel3];
//    
//    CGFloat startY1 = titleLabel3.endPointY+10;
//    for (int i=0; i<customWeeks.count; i++) {
//        Label *label = [[Label alloc]initWithFrame:CGRectMake(13+42*i*[AppDelegate ShareApp].autoSizeScaleX, startY, 42, 42) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15] andTextColor:[UIColor blackColor] andTextLines:1];
//        label.text = [customWeeks objectAtIndex:i];
//        label.textAlignment = NSTextAlignmentCenter;
//        [self addSubview:label];
//    }
//    
//    NSDate *tmpNextMonthDate1;
//    int nextMonthDateCount1=1;
//    for (NSInteger i=nextStartWeekDay; i<nextDays.length+nextStartWeekDay; i++) {
//        NSString *tmpNextDateStr = [NSString stringWithFormat:@"%ld-%ld-%d",nextYear,(long)nextMonth,nextMonthDateCount1++];
//        tmpNextMonthDate1 = [dateFormatter1 dateFromString:tmpNextDateStr];
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//        
//        
//        if([self isCalendarCurrentOfferWithCurrentDay:[NSNumber numberWithInteger:i-nextStartWeekDay+1] andCalendarsArr:nextMonthArr]){
//            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//            [button setBackgroundImage:[UIImage imageNamed:@"CalendarCurrentOffer"] forState:UIControlStateNormal];
//            button.tag = 1;
//            [button addTarget:self action:@selector(nextMonthButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//            
//        }
//        else if([BookUtil bookableNotFromOffer:restaurantOffer inOfferArray:restaurantOfferArray withPax:pax andSlotTimeBookedArray:slotTimeBookedArray andOfferTimeBookedArray:offerTimeBookedArray atDate:tmpNextMonthDate restaurant:restaurant])
//            
//        {
//            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//            [button setBackgroundImage:[UIImage imageNamed:@"CalendarOtherOffer"] forState:UIControlStateNormal];
//            button.tag = 2;
//            [button addTarget:self action:@selector(nextMonthButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//        }else if([BookUtil bookableFromRegular:regularTimeArray withPax:pax andSlotTimeBookedArray:slotTimeBookedArray atDate:tmpNextMonthDate restaurant:restaurant]){
//            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//            [button setBackgroundColor:[UIColor whiteColor]];
//            button.tag = 3;
//            [button addTarget:self action:@selector(nextMonthButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//        }else{
//            button.enabled = NO;
//            [button setTitleColor:[UIColor colorCCCCCC] forState:UIControlStateNormal];
//            [button setBackgroundImage:[UIImage imageNamed:@"CalendarWhite"] forState:UIControlStateNormal];
//        }
//        [button setTitle:[NSString stringWithFormat:@"%ld",i-nextStartWeekDay+1] forState:UIControlStateNormal];
//        
//        button.frame = CGRectMake(13+42*((i-1)%7)*[AppDelegate ShareApp].autoSizeScaleX, (startY1+40)+42*((i-1)/7), 41, 42);
//        
//        [self addSubview:button];
//        
//        self.heightOfView = button.endPointY;
//    }

}
- (BOOL)isCalendarCurrentOfferWithCurrentDay:(NSNumber *)day andCalendarsArr:(NSArray *)currentArr
{
    for (NSNumber *tempDay in currentArr)
    {
        if ([tempDay intValue] == [day intValue])
        {
            return YES;
        }
    }
    return NO;
}
-(void)currentMonthButtonClicked:(UIButton*)button
{
    NSDate *selectDate;

        int day = [button.titleLabel.text intValue];
        NSString *str = [NSString stringWithFormat:@"%ld-%ld-%d",(long)currentYear,(long)currentMonth,day];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"YYYY-MM-dd"];
        selectDate = [formatter dateFromString:str];

    NSMutableDictionary *dateSelectedDictionary = [[NSMutableDictionary alloc]init];
    [dateSelectedDictionary setObject:selectDate forKey:@"date"];
    [dateSelectedDictionary setObject:[NSString stringWithFormat:@"%ld",(long)button.tag] forKey:@"whichDateTypeSelected"];
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:@"SelectedDate" object:dateSelectedDictionary];
}

-(void)nextMonthButtonClicked:(UIButton*)button
{

    NSDate *selectDate;

        int day = [button.titleLabel.text intValue];
        NSString *str = [NSString stringWithFormat:@"%ld-%ld-%d",(long)nextYear,(long)nextMonth,day];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"YYYY-MM-dd"];
        selectDate = [formatter dateFromString:str];

    NSMutableDictionary *dateSelectedDictionary = [[NSMutableDictionary alloc]init];
    [dateSelectedDictionary setObject:selectDate forKey:@"date"];
    [dateSelectedDictionary setObject:[NSString stringWithFormat:@"%ld",(long)button.tag] forKey:@"whichDateTypeSelected"];
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:@"SelectedDate" object:dateSelectedDictionary];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
