//
//  PlaceholderView.m
//  Qraved
//
//  Created by harry on 2018/3/13.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "PlaceholderView.h"
#define TOP_SCROLL_HEIGHT 180
@implementation PlaceholderView
{
    UIImageView *vimgRestaurant;
    UILabel *lblRestaurantTitle;
    DLStarRatingControl *ratingControl;
    UILabel *lblDistance;
    UILabel *lblInformation;
    UILabel *lblTime;
    UILabel *lblWellKnownFor;
}
- (instancetype)init{
    self = [super init];
    if (self) {
        [self initUI];
    }
    return self;
}

- (void)initUI{
    
    vimgRestaurant = [UIImageView new];
    vimgRestaurant.contentMode = UIViewContentModeScaleAspectFill;
    vimgRestaurant.clipsToBounds = YES;
    
    lblRestaurantTitle = [UILabel new];
    [lblRestaurantTitle setFont:[UIFont boldSystemFontOfSize:24]];
    [lblRestaurantTitle setTextColor:[UIColor color333333]];
    
    ratingControl=[[DLStarRatingControl alloc]initWithFrame:CGRectMake(LEFTLEFTSET, lblRestaurantTitle.bottom, 83, 15) andStars:5 andStarWidth:15 isFractional:YES spaceWidth:2];
    ratingControl.userInteractionEnabled=NO;
    
    lblDistance = [UILabel new];
    lblDistance.font = [UIFont systemFontOfSize:12];
    lblDistance.textColor = [UIColor color999999];
    
    lblInformation = [UILabel new];
    lblInformation.font = [UIFont systemFontOfSize:12];
    lblInformation.textColor = [UIColor color999999];
    
    
    [self sd_addSubviews:@[vimgRestaurant, lblRestaurantTitle, ratingControl, lblDistance, lblInformation]];
    vimgRestaurant.sd_layout
    .topSpaceToView(self, 0)
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .heightIs(TOP_SCROLL_HEIGHT*[AppDelegate ShareApp].autoSizeScaleY);
    
    lblRestaurantTitle.sd_layout
    .topSpaceToView(vimgRestaurant, 5)
    .leftSpaceToView(self, 15)
    .rightSpaceToView(self, 60)
    .autoHeightRatio(0);
    
    [lblRestaurantTitle setMaxNumberOfLinesToShow:2];
   
    ratingControl.sd_layout
    .topSpaceToView(lblRestaurantTitle, 7)
    .leftEqualToView(lblRestaurantTitle)
    .widthIs(83)
    .heightIs(15);
    
    lblDistance.sd_layout
    .leftSpaceToView(ratingControl, 3)
    .rightSpaceToView(self, 60)
    .topEqualToView(ratingControl)
    .heightIs(14);
    
    lblInformation.sd_layout
    .topSpaceToView(ratingControl, 7)
    .leftEqualToView(ratingControl)
    .rightSpaceToView(self, 60)
    .heightIs(14);
    
}

- (void)setRestaurant:(IMGRestaurant *)restaurant{
    _restaurant = restaurant;
    
    [vimgRestaurant sd_setImageWithURL:[NSURL URLWithString:[restaurant.logoImage returnCurrentImageString]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
    
    lblRestaurantTitle.text = restaurant.title;
    
    ratingControl.rating= [[NSNumber numberWithDouble:[restaurant.ratingScore doubleValue]] getRatingNumber];
    
    if ( [restaurant.distance isEqualToString:@""]) {
        
        lblDistance.text = [NSString stringWithFormat:@"(%@)",restaurant.ratingCount];
    }else{
        if ( [restaurant.ratingCount isEqualToNumber:@0]) {
            
            lblDistance.text = [NSString stringWithFormat:@"%@",restaurant.distance];
        }else{
            
            lblDistance.text = [NSString stringWithFormat:@"(%@) • %@",restaurant.ratingCount,restaurant.distance];
        }
    }
    
    NSString *dollar=nil;
    if (restaurant.priceLevel != nil) {
        switch(restaurant.priceLevel.intValue){
            case 1:dollar=@"Below 100K";break;
            case 2:dollar=@"100K - 200K";break;
            case 3:dollar=@"200K - 300K";break;
            case 4:dollar=@"Start from 300K";break;
        }
    }else{
        dollar = restaurant.priceName;
    }
    
    
    NSString *cuisineAndDistrictText;
    if (restaurant.landMarkList.count >0) {
        cuisineAndDistrictText=[NSString stringWithFormat:@"%@ • %@ • %@",restaurant.cuisineName,restaurant.landMarkList[0][@"name"],dollar];
    }else{
        cuisineAndDistrictText=[NSString stringWithFormat:@"%@ • %@ • %@",restaurant.cuisineName,restaurant.districtName,dollar];
    }
    
    //lblInformation.text=cuisineAndDistrictText;
    
    [self setupAutoHeightWithBottomView:lblInformation bottomMargin:10];
}

@end
