//
//  DetailViewInformationView.h
//  Qraved
//
//  Created by apple on 17/3/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"

@protocol DetailViewInformationViewDelegate <NSObject>

-(void)btnClick:(UIButton*)sender;

@end

@interface DetailViewInformationView : UIView

@property (nonatomic,weak)id<DetailViewInformationViewDelegate> delegate;
-(instancetype)initWithRestaurant:(IMGRestaurant *)restaurant andShowClaim:(NSNumber*)showClaim;
@end
