//
//  UIAlertController+IMGAlertController.m
//  Qraved
//
//  Created by harry on 2018/3/21.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "UIAlertController+IMGAlertController.h"

@implementation UIAlertController (IMGAlertController)
+ (void)showAlertCntrollerWithViewController:(UIViewController*)viewController
                        alertControllerStyle:(UIAlertControllerStyle)alertControllerStyle
                                       title:(NSString*)title
                                     message:(NSString*)message
                           cancelButtonTitle:(NSString *)cancelBtnTitle
                      destructiveButtonTitle:(NSString *)destructiveBtnTitle
                               CallBackBlock:(CallBackBlock)block{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:alertControllerStyle];
    
    if (destructiveBtnTitle.length) {
        UIAlertAction * destructiveAction = [UIAlertAction actionWithTitle:destructiveBtnTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            block(1);
        }];
        [alertController addAction:destructiveAction];
    }
    
    if (cancelBtnTitle.length) {
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelBtnTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];
        [alertController addAction:cancelAction];
    }
    [viewController presentViewController:alertController animated:YES completion:nil];
}

+ (void)showFunctionAlertCntrollerWithViewController:(UIViewController*)viewController
                                alertControllerStyle:(UIAlertControllerStyle)alertControllerStyle
                                               title:(NSString*)title
                                             message:(NSString*)message
                                   cancelButtonTitle:(NSString *)cancelBtnTitle
                              destructiveButtonTitle:(NSString *)destructiveBtnTitle
                                       CallBackBlock:(CallBackBlock)block{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:alertControllerStyle];
    
    if (cancelBtnTitle.length) {
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancelBtnTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }];
        [alertController addAction:cancelAction];
    }
    
    if (destructiveBtnTitle.length) {
        UIAlertAction * destructiveAction = [UIAlertAction actionWithTitle:destructiveBtnTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            block(1);
        }];
        [alertController addAction:destructiveAction];
    }
    
    [viewController presentViewController:alertController animated:YES completion:nil];
}

@end
