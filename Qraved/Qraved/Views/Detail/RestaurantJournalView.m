//
//  RestaurantJournalView.m
//  Qraved
//
//  Created by System Administrator on 10/14/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "RestaurantJournalView.h"
#import "RestaurantJournalViewCell.h"
#import "DetailDataHandler.h"
#import "IMGMediaComment.h"
#import "UIConstants.h"
#import "MoreArticleViewController.h"
#import "NSString+Helper.h"
#import "UIDevice+Util.h"

@implementation RestaurantJournalView

@synthesize hasData;

-(instancetype)initWithFrame:(CGRect)frame withRestaurantId:(NSNumber*)restaurantId{
	if(self=[super initWithFrame:frame]){
		self.restaurantId=restaurantId;
		hasData=YES;
		self.journals=[[NSMutableArray alloc] init];
		self.tableView=[[UITableView alloc] initWithFrame:CGRectMake(0,0,frame.size.width,frame.size.height)];
		self.tableView.delegate=self;
		self.tableView.dataSource=self;
		self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
		[self addSubview:self.tableView];
		[self addObserver:self forKeyPath:@"journals" options:NSKeyValueObservingOptionNew context:nil];
	}
	return self;
}

-(void)setRefreshViewFrame{
	if(hasData){
		if(self.refreshView==nil){
			self.refreshView=[[EGORefreshTableFooterView alloc] initWithFrame:CGRectMake(0,DeviceHeight,DeviceWidth,66)];
			self.refreshView.backgroundColor=[UIColor clearColor];
			self.refreshView.delegate=self;
			[self.tableView addSubview:self.refreshView];
		}
		if(self.tableView.contentSize.height>self.tableView.frame.size.height){
			self.refreshView.frame=CGRectMake(0,self.tableView.contentSize.height+10,DeviceWidth,66);
		}else{
			self.refreshView.frame=CGRectMake(0,self.tableView.frame.size.height+10,DeviceWidth,66);
		}
	}else if(self.refreshView){
		[self.refreshView removeFromSuperview];
		self.refreshView=nil;
	}
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexpath{
	IMGMediaComment *journal=[self.journals objectAtIndex:indexpath.row];
	UIFont *titleFont=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:13];
	CGSize size = [journal.title sizeWithFont:titleFont constrainedToSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 100)];
    float height = 0.0;
    if ([UIDevice isIphone6Plus]) {
        height = 30.0;
    }
    if ([UIDevice isIphone6]) {
        height = 20.0;
    }
	return 155+size.height-titleFont.lineHeight+height;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
	return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return self.journals.count;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexpath{
	RestaurantJournalViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"restaurant_journal"];
	if(cell==nil){
		cell=[[RestaurantJournalViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"restaurant_journal"];
	}
	cell.delegate=self.delegate;
	cell.tag=indexpath.row;
	IMGMediaComment *journal=[self.journals objectAtIndex:indexpath.row];
	cell.title=journal.journalTitle;
	cell.imageUrl=journal.journalImageUrl;
	cell.contents=journal.contents;
	cell.link=journal.link;
	[cell callLayoutSubviews];
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexpath{
    [tableView deselectRowAtIndexPath:indexpath animated:NO];
    if(self.delegate && [self.delegate respondsToSelector:@selector(gotoJournal:inView:)]){
		IMGMediaComment *journal=[self.journals objectAtIndex:indexpath.row];
		if (journal!=nil){
			[self.delegate gotoJournal:journal inView:[tableView cellForRowAtIndexPath:indexpath]];	
		}	
	}
}

-(void)goToRestaurant:(UITapGestureRecognizer*)tap{
	
}

- (void)egoRefreshTableFooterDidTriggerRefresh:(EGORefreshTableFooterView*)view{
	if(self.delegate && [self.delegate respondsToSelector:@selector(loadRestaurantJournals:)]){
		[self.delegate loadRestaurantJournals:^(BOOL _hasData){
			hasData=_hasData;
			[self setRefreshViewFrame];
        }];
	}
}

-(void)scrollViewDidScroll:(UITableView*)tableView{
	[self.refreshView egoRefreshScrollViewDidScroll:tableView];
}

-(void)scrollViewDidEndDragging:(UITableView*)tableView willDecelerate:(BOOL)decelerate{
	if(self.tableView.contentOffset.y>self.tableView.contentSize.height-self.tableView.frame.size.height+66){
		self.refreshView.frame=CGRectMake(0,self.tableView.contentSize.height,DeviceWidth,66);
		[self.refreshView egoRefreshScrollViewDidEndDragging:tableView];
	}
}

-(void)observeValueForKeyPath:(NSString*)keypath ofObject:(id)objc change:(NSDictionary*)change context:(void*)context{
	if([keypath isEqualToString:@"journals"]){
		[self.tableView reloadData];
	}
}

-(void)dealloc{
	[self removeObserver:self forKeyPath:@"journals" context:nil];
}


@end
