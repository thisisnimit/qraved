//
//  ReviewRatingHeaderView.m
//  Qraved
//
//  Created by harry on 2017/11/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "ReviewRatingHeaderView.h"

@implementation ReviewRatingHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //[self createUI];
    }
    return self;
}

- (void)setRestaurant:(IMGRestaurant *)restaurant{
    _restaurant = restaurant;
    Label *titleLabel=[[Label alloc] initWithFrame:CGRectZero andTextFont:[UIFont boldSystemFontOfSize:14] andTextColor:[UIColor color333333] andTextLines:1];
    [titleLabel setText:L(@"Reviews")];
    [titleLabel setFrame:CGRectMake(LEFTLEFTSET, 16, titleLabel.expectedWidth, titleLabel.font.lineHeight)];
    [self addSubview:titleLabel];

    
    CGFloat currentPointY = titleLabel.endPointY;
    
    UILabel *scoreLabel = [[UILabel alloc] init];
    
    [scoreLabel setFont:[UIFont systemFontOfSize:50]];
    float ratingScore = [restaurant.ratingScore floatValue];
    scoreLabel.text = [NSString stringWithFormat:@"%.1f",ratingScore/2];
//    [scoreBtnTop setTitle:scoreLabel.text forState:UIControlStateNormal];
    scoreLabel.frame = CGRectMake(DeviceWidth-LEFTLEFTSET*2 - scoreLabel.expectedWidth, currentPointY+5, scoreLabel.expectedWidth, scoreLabel.font.lineHeight);
    scoreLabel.textColor = [UIColor colorWithRed:241/255.0f green:92/255.0f blue:79/255.0f alpha:1.0f];
    [self addSubview:scoreLabel];
    
    float overallScoreOffset = 0.0;
    float yellowLabelOffset = 0.0;
    if ([UIDevice isIphone6Plus]) {
        overallScoreOffset = 40.0;
        yellowLabelOffset = 30.0;
    }
    if ([UIDevice isIphone6]) {
        overallScoreOffset = 30.0;
        yellowLabelOffset = 20.0;
    }
    
    DLStarRatingControl *overallScore=[[DLStarRatingControl alloc]initWithFrame:CGRectMake(DeviceWidth - 100 - LEFTLEFTSET, scoreLabel.endPointY + 4, 100, 15) andStars:5 andStarWidth:15 isFractional:YES spaceWidth:5];
    overallScore.userInteractionEnabled=NO;
    overallScore.rating= [[NSNumber numberWithDouble:[restaurant.ratingScore doubleValue]] getRatingNumber];
    [self addSubview:overallScore];
    
    NSNumber *ratingCounts = restaurant.ratingCount;
    
    UILabel *reviewsStrLabel = [[UILabel alloc] init];
    if (ratingCounts) {
        reviewsStrLabel.text = [NSString stringWithFormat:@"%@ ",([ratingCounts intValue] >1)?L(@"Reviews"):L(@"Review")];
    }
    reviewsStrLabel.font = [UIFont systemFontOfSize:12];
    reviewsStrLabel.textColor = [UIColor color999999];
    reviewsStrLabel.frame = CGRectMake(DeviceWidth - LEFTLEFTSET*2 -  reviewsStrLabel.expectedWidth, overallScore.endPointY+4, reviewsStrLabel.expectedWidth, reviewsStrLabel.font.lineHeight);
    [self addSubview:reviewsStrLabel];
    
    NSArray *reviewListArr = [restaurant.ratingCounts componentsSeparatedByString:@","];
    float max = 0;
    for (NSString *str in reviewListArr)
    {
        if (max < [str floatValue])
        {
            max = [str floatValue];
        }
    }
    if (max == 0)
    {
        reviewListArr = [[NSArray alloc] init];
    }
    float AVG = 10/max*8;
    
    for (int i = 0; i<reviewListArr.count; i++)
    {
        NSArray *colorArray = @[[UIColor colorWithRed:226/255.0f green:51/255.0f blue:53/255.0f alpha:1.0f],[UIColor colorWithRed:241/255.0f green:92/255.0f blue:79/255.0f alpha:1.0f],[UIColor colorWithRed:255/255.0f green:146/255.0f blue:46/255.0f alpha:1.0f],[UIColor colorWithRed:254/255.0f green:192/255.0f blue:17/255.0f alpha:1.0f],[UIColor colorWithRed:242/255.0f green:189/255.0f blue:121/255.0f alpha:1.0f],];
        NSNumber *newRating = [NSNumber numberWithFloat:([[reviewListArr objectAtIndex:reviewListArr.count-i-1] floatValue] * AVG)];
        
        UIView *backLabel = [[UIView alloc] init];
        backLabel.frame = CGRectMake(LEFTLEFTSET +26, titleLabel.endPointY+15  + 20*i, 150, 15);
        backLabel.backgroundColor = [UIColor colorWithRed:229/255.0f green:230/255.0f blue:232/255.0f alpha:1.0f];;
        [self addSubview:backLabel];
        
        UILabel *yellowLabel = [[UILabel alloc] init];
        yellowLabel.frame = CGRectMake(LEFTLEFTSET +26, titleLabel.endPointY+15  + 20*i, [newRating intValue], 15);
        yellowLabel.alpha = (reviewListArr.count - i)*0.2;
        yellowLabel.backgroundColor = colorArray[i];
        [self addSubview:yellowLabel];
        
        UILabel *numLabel = [[UILabel alloc] init];
        numLabel.frame = CGRectMake(LEFTLEFTSET, yellowLabel.frame.origin.y, 10, 15);
        numLabel.text = [NSString stringWithFormat:@"%d",5-i];
        numLabel.textColor = [UIColor color999999];
        numLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:numLabel];
        
        UIImageView * starImage = [UIImageView new];
        starImage.image = [UIImage imageNamed:@"ic_star_empty"];
        starImage.frame = CGRectMake(numLabel.right +2, yellowLabel.frame.origin.y, 12, 12);
        [self addSubview:starImage];
        
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:backLabel.bounds byRoundingCorners:UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii:CGSizeMake(7.5, 7.5)];
        CAShapeLayer *layer = [[CAShapeLayer alloc]init];
        layer.frame = backLabel.bounds;
        layer.path = maskPath.CGPath;
        
        backLabel.layer.mask = layer;
        
        
        UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:yellowLabel.bounds byRoundingCorners:UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii:CGSizeMake(7.5, 7.5)];
        CAShapeLayer *layer1 = [[CAShapeLayer alloc]init];
        layer1.frame = yellowLabel.bounds;
        layer1.path = maskPath1.CGPath;
        
        yellowLabel.layer.mask = layer1;
    }
    
    UILabel *reviewsCountLable = [[UILabel alloc] init];
    reviewsCountLable.text = [NSString stringWithFormat:@"%@",[ratingCounts intValue] == 0?@"1":ratingCounts];
    reviewsCountLable.font = [UIFont systemFontOfSize:15];
    reviewsCountLable.textColor = [UIColor color999999];
    reviewsCountLable.frame = CGRectMake(reviewsStrLabel.frame.origin.x - reviewsCountLable.expectedWidth - 3, reviewsStrLabel.frame.origin.y, reviewsStrLabel.expectedWidth, reviewsStrLabel.font.lineHeight);
    [self addSubview:reviewsCountLable];
    
}


@end
