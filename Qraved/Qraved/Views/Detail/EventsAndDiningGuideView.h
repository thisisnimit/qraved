//
//  EventsAndDiningGuideView.h
//  Qraved
//
//  Created by System Administrator on 1/13/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "EGORefreshTableFooterView.h"

@class IMGRestaurant;
@class EventsAndDiningViewController;

@interface EventsAndDiningGuideView : UIView<UITableViewDelegate,UITableViewDataSource,EGORefreshTableFooterDelegate>

@property(nonatomic,strong) IMGRestaurant *restaurant;
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) EGORefreshTableFooterView *refreshView;
@property(nonatomic,strong) NSMutableArray *events;
@property(nonatomic,strong) NSMutableArray *diningGuides;
@property(nonatomic,assign) BOOL hasData;
@property(nonatomic,assign) BOOL isLoading;
@property(nonatomic,weak) EventsAndDiningViewController *delegate;


@end
