//
//  PopupView.h
//  Qraved
//
//  Created by Lucky on 15/4/23.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

typedef void (^requestCallback)(BOOL status);

#import <UIKit/UIKit.h>
#import "IMGUser.h"
@interface PopupView : UIView<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (nonatomic, strong)UIView *innerView;
@property (nonatomic, weak)UIViewController *parentVC;
@property (nonatomic,copy) requestCallback requestCallback;


+ (instancetype)defaultPopupView;
- (id)initWithUser:(IMGUser *)user andRestaurantId:(NSNumber *)restaurantId requestCallback:(void(^)(BOOL))callback;

- (id)initWithTutorialViewFrame:(CGRect)frame andText:(NSString *)text andIsNext:(BOOL)isNext;

@end
