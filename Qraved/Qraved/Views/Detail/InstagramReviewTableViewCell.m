//
//  InstagramReviewTableViewCell.m
//  Qraved
//
//  Created by harry on 2017/11/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "InstagramReviewTableViewCell.h"
#import "IMGDish.h"
@interface InstagramReviewTableViewCell()<UIWebViewDelegate>
@end

@implementation InstagramReviewTableViewCell
{
    UIButton *btnUserPhoto;
    UILabel *lblName;
    UILabel *lblDes;
    UILabel *lblTime;
    UIWebView *webCaption;
    UIView *photosView;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    btnUserPhoto = [UIButton buttonWithType:UIButtonTypeCustom];
    btnUserPhoto.frame = CGRectMake(LEFTLEFTSET, TOPOFFSET, 40, 40);
    btnUserPhoto.layer.masksToBounds = YES;
    btnUserPhoto.layer.cornerRadius = 20;
    [btnUserPhoto addTarget:self action:@selector(controlTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:btnUserPhoto];
    
    lblName = [[UILabel alloc]init];
    lblName.textColor = [UIColor color333333];
    lblName.font = [UIFont systemFontOfSize:15];
    
    lblName.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap_nameLabel = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(controlTapped)];
    [lblName addGestureRecognizer:tap_nameLabel];
    
    [self.contentView addSubview:lblName];
    
    lblDes = [[UILabel alloc]init];
    lblDes.font = [UIFont systemFontOfSize:12];
    lblDes.textColor = [UIColor color999999];
    lblDes.alpha = 0.7f;
    [self.contentView addSubview:lblDes];
    
    lblTime = [[UILabel alloc]init];
    lblTime.textColor = [UIColor color999999];
    lblTime.alpha = 0.7f;
    lblTime.font = [UIFont systemFontOfSize:12];
    lblTime.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:lblTime];
    
    webCaption = [[UIWebView alloc] initWithFrame:CGRectMake(LEFTLEFTSET, lblDes.endPointY+5, DeviceWidth - LEFTLEFTSET*2, 40)];
    webCaption.delegate = self;
    webCaption.scrollView.bounces = NO;
    webCaption.backgroundColor=[UIColor whiteColor];
    webCaption.scrollView.showsHorizontalScrollIndicator = NO;
    webCaption.scrollView.scrollEnabled = YES;
    webCaption.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.contentView addSubview:webCaption];
}

- (void)setReview:(IMGReview *)review{
    _review = review;
    
    NSURL *urlString;
    review.userPicture = [review.userPicture stringByReplacingPercentEscapesUsingEncoding:
                          NSUTF8StringEncoding];
    
    if ([review.userPicture hasPrefix:@"http"])
    {
        urlString = [NSURL URLWithString:review.userPicture];
    }
    else
    {
        urlString = [NSURL URLWithString:[review.userPicture returnFullImageUrl]];
    }
    [btnUserPhoto sd_setImageWithURL:urlString forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"headDefault.jpg"] completed:nil];
    lblName.text = [NSString stringWithFormat:@"%@",review.userName];
    
    lblName.frame = CGRectMake(btnUserPhoto.frame.size.width+20,TOPOFFSET, lblName.expectedWidth, lblName.font.lineHeight);
    
    lblDes.text = @"in Instagram";
    lblDes.frame = CGRectMake(btnUserPhoto.frame.size.width+20, lblName.endPointY+3, 100, 14);
    lblTime.text = [Date getTimeInteval_v10:[review.timeCreated longLongValue]/1000];
    lblTime.frame = CGRectMake(DeviceWidth - 150 - 15, lblName.endPointY+3, 150, 14);
    
    NSString *captionStr = [NSString returnStringWithTag:review.caption];
    float contentLabelHeight = [self calculateCommentTextHeight:review.caption];;
    webCaption.frame=CGRectMake(LEFTLEFTSET, lblDes.endPointY+15, DeviceWidth - LEFTLEFTSET*2, contentLabelHeight);
    
    NSString *summarizeStr = [NSString stringWithFormat:@"<html> <head> <meta charset=\"UTF-8\">"
                              "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">"
                              "<style type=\"text/css\">"
                              "html {"
                              "    -webkit-text-size-adjust: none; /* Never autoresize text */"
                              "}"
                              "body{font-family:\"%@\";font-size: 12px; color:%@; padding:0; margin:0; width:%fpx; line-height: 15px;}"
                              "</style>"
                              "</head>"
                              "<body><div id=\"content\">%@</div></body>"
                              "</html>",DEFAULT_FONT_NAME,@"#999999",DeviceWidth - LEFTLEFTSET*2,captionStr];
    
    summarizeStr = [summarizeStr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
    [webCaption loadHTMLString:summarizeStr baseURL:nil];
    [webCaption layoutSubviews];
    
    if (photosView) {
        [photosView removeFromSuperview];
    }
    photosView = [[UIView alloc] init];
    photosView.frame = CGRectMake(LEFTLEFTSET, webCaption.endPointY+5, DeviceWidth - 2*LEFTLEFTSET, 0);
    
    
    CGFloat dishImageWidth;
    CGFloat dishImageEndX;
    int viewFrameY = 0;
    int viewFrameX = 0;
    for (int i = 0; i<review.imageArray.count; i++)
    {
        IMGDish *dish=(IMGDish*)[review.imageArray objectAtIndex:i];
        UIImageView *dishImageView = [[UIImageView alloc]init];
        dishImageWidth = (photosView.frame.size.width-10)/3;
        dishImageView.userInteractionEnabled=YES;
        dishImageView.contentMode = UIViewContentModeScaleAspectFill;
        dishImageView.clipsToBounds = YES;
        if (viewFrameX == 3)
        {
            viewFrameY = dishImageWidth + 5 + viewFrameY;
            viewFrameX = 0;
        }
        
        dishImageView.frame = CGRectMake((dishImageWidth+5)*viewFrameX, viewFrameY, dishImageWidth, dishImageWidth);
        viewFrameX++;
        dishImageView.tag = i;
        [photosView addSubview:dishImageView];
        
        
        if (review.imageArray.count == 1) {
            
            dishImageView.frame = CGRectMake(0, viewFrameY, DeviceWidth, DeviceWidth/4*3);
        }
        
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagePressed:)];
        [dishImageView addGestureRecognizer:tap];
        
        dishImageEndX = dishImageView.endPointX;
        
        if (i == 5 && review.imageArray.count > 6 )
        {
            dishImageView.backgroundColor = [UIColor colorWithRed:222/250.0 green:222/250.0 blue:222/250.0 alpha:1];
            UILabel *otherCountLabel = [[UILabel alloc] init];
            otherCountLabel.tag=i;
            otherCountLabel.userInteractionEnabled=YES;
            otherCountLabel.text = [NSString stringWithFormat:@"+%lu",review.imageArray.count - 5];
            otherCountLabel.font = [UIFont systemFontOfSize:17];
            otherCountLabel.textColor = [UIColor color333333];
            otherCountLabel.frame = CGRectMake(dishImageView.frame.size.width/2 - 20, dishImageView.frame.size.height/2 - 20, 40, 40);
            [otherCountLabel addGestureRecognizer:tap];
            [dishImageView addSubview:otherCountLabel];
            break;
        }
        else
        {
            NSString *urlString = dish.lowResolutionImage;
            __weak typeof(dishImageView) weakRestaurantImageView = dishImageView;
            
            [dishImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [UIView animateWithDuration:1.0 animations:^{
                    [weakRestaurantImageView setAlpha:1.0];
                }];
            }];
        }
        
        
        photosView.frame = CGRectMake(LEFTLEFTSET, webCaption.endPointY+5, DeviceWidth - 2*LEFTLEFTSET, dishImageView.endPointY);
        if (review.imageArray.count == 1) {
            photosView.frame = CGRectMake(0, webCaption.endPointY+5, DeviceWidth, dishImageView.endPointY);
            
        }
    }
    
    [self.contentView addSubview:photosView];
    
    self.currentPointY = photosView.endPointY+10;
    
}

- (void)controlTapped{
    NSString *urlStr = @"https://www.instagram.com/";
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",urlStr,self.review.userName]];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)imagePressed:(UITapGestureRecognizer *)tap{
    if([self.delegate respondsToSelector:@selector(instagramReviewImagePressed:withTag:)]){
        [self.delegate instagramReviewImagePressed:self.review withTag:(int)tap.view.tag];
    }

}

#pragma mark - webview delegate
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none';"];
    
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitTouchCallout='none';"];
    UIScrollView *tempView = (UIScrollView *)[webCaption.subviews objectAtIndex:0];
    
    tempView.scrollEnabled = NO;
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if (navigationType==UIWebViewNavigationTypeLinkClicked) {
        NSURL *url = [NSURL URLWithString:self.review.link];
        [[UIApplication sharedApplication] openURL:url];
        return NO;
    }
    
    return YES;
}

-(CGFloat)calculateCommentTextHeight:(NSString*)commentStr_{
    
    commentStr_=[commentStr_ stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12]};
    
    CGSize textSize = [commentStr_ boundingRectWithSize:CGSizeMake(DeviceWidth-30, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    
    float height = ceil(textSize.height + 1.5f);
    
    NSLog(@"height = %@",commentStr_);
    
    return height+15;
}


@end
