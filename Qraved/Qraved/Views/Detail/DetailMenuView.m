//
//  DetailMenuView.m
//  Qraved
//
//  Created by apple on 17/3/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "DetailMenuView.h"
#import "IMGMenu.h"
#import "Label.h"
#import "UIConstants.h"
#import "UIImageView+WebCache.h"
#import "UIColor+Helper.h"
#import "UILabel+Helper.h"
#import "UIImage+Resize.h"
#import "UIView+Helper.h"
#import "UIColor+Hex.h"

#define CONTENT_OFFSET 5
#define TOP_SCROLL_HEIGHT 180
#define NAV_HEIGHT 35
#define mapViewHeight 100
#define SHADOWHEIGHT 8
#define CELLOFFSET 6
#define SCORLLVIEW_Y 23
#define TITLE_MARGIN 20
#define BTN_HEIGHT 35


@implementation DetailMenuView
-(instancetype)initWithPhtosArray:(NSArray*)photoArray andRestaurant:(IMGRestaurant*)restaurant{
    if (self = [super init]) {
        self.menuPhotoArray = [NSMutableArray array];
        Label *titleLabel=[[Label alloc] initWithFrame:CGRectZero andTextFont:[UIFont boldSystemFontOfSize:14] andTextColor:[UIColor color222222] andTextLines:1];
        [titleLabel setText:L(@"Menu")];
        [titleLabel setFrame:CGRectMake(LEFTLEFTSET, TITLE_MARGIN, titleLabel.expectedWidth, titleLabel.font.lineHeight)];
        [self addSubview:titleLabel];
        
        UIScrollView *photosScrollView = [[UIScrollView alloc] init];
        photosScrollView.scrollsToTop = NO;
        [self addSubview:photosScrollView];
        CGFloat currentPointY = titleLabel.endPointY;
        
        CGFloat dishImageWidth;
        CGFloat dishImageEndX;
        for (NSDictionary *dic in photoArray)
        {
            IMGMenu *menu = [[IMGMenu alloc] init];
            [menu setValuesForKeysWithDictionary:dic];
            menu.imageUrl = [dic objectForKey:@"imageUrl"];
            menu.createTime = [dic objectForKey:@"createTime"];
            menu.menuId = [dic objectForKey:@"id"];
            menu.type = [dic objectForKey:@"type"];
            NSDictionary *photoCredi=[dic objectForKey:@"photoCredit"];
            menu.resSourceTitle=[photoCredi objectForKey:@"restaurantTitle"];
            [self.menuPhotoArray addObject:menu];
        }
        if (!self.menuPhotoArray.count) {
            UIView *uploadView = [[UIView alloc] init];
            uploadView.backgroundColor = [UIColor colorDDDDDD];
            dishImageWidth = (DeviceWidth-LEFTLEFTSET-40)/3;
            dishImageEndX = dishImageWidth;
            uploadView.frame = CGRectMake(LEFTLEFTSET,0 , dishImageWidth,dishImageWidth);
            [photosScrollView addSubview:uploadView];
            
            UIImageView *uploadImageView = [[UIImageView alloc] initWithFrame:CGRectMake((dishImageWidth-32)/2, dishImageWidth/3-10, 32, 29)];
            [uploadImageView setImage:[UIImage imageNamed:@"camera"]];
            [uploadView addSubview:uploadImageView];
            UILabel *uploadLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, dishImageWidth/3*2-8, dishImageWidth, 20)];
            uploadLabel.text = @"Upload Menu";
            uploadLabel.textAlignment = NSTextAlignmentCenter;
            uploadLabel.font = [UIFont systemFontOfSize:12];
            [uploadView addSubview:uploadLabel];
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadImageViewTap)];
            [uploadView addGestureRecognizer:tap];
            
        }
        for (int i = 0 ; i<self.menuPhotoArray.count; i++)
        {
            UIImageView *dishImageView = [[UIImageView alloc]init];
            dishImageWidth = (DeviceWidth-LEFTLEFTSET-40)/3;
            dishImageView.frame = CGRectMake((dishImageWidth+10)*i+LEFTLEFTSET, 0, dishImageWidth, dishImageWidth);
            dishImageView.tag = i;
            UITapGestureRecognizer *Tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(menuPhotoimagesPressed:)];
            [Tap setNumberOfTapsRequired:1];
            [Tap setNumberOfTouchesRequired:1];
            dishImageView.userInteractionEnabled=YES;
            [dishImageView addGestureRecognizer:Tap];
            IMGMenu *menu=[self.menuPhotoArray objectAtIndex:i];
            [photosScrollView addSubview:dishImageView];
            dishImageEndX = dishImageView.endPointX;
            
            NSString *urlString = [menu.imageUrl returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution];
            __weak typeof(dishImageView) weakRestaurantImageView = dishImageView;
            UIImage *placehoderImage = [[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(dishImageWidth, dishImageWidth)];
            [dishImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:placehoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                image = [image imageByScalingAndCroppingForSize:CGSizeMake(dishImageWidth, dishImageWidth)];
                [weakRestaurantImageView setImage:image];
            }];
            if (i==9&&[restaurant.menuCount intValue]>10) {
                [dishImageView removeFromSuperview];
                UILabel *restImagesCount = [[UILabel alloc] init];
                restImagesCount.frame = CGRectMake(((DeviceWidth-LEFTLEFTSET-40)/3.0+10)*9+LEFTLEFTSET, -1, (DeviceWidth-LEFTLEFTSET-40)/3.0, (DeviceWidth-LEFTLEFTSET-40)/3.0);
                restImagesCount.text = [NSString stringWithFormat:@"+%d",[restaurant.menuCount intValue]-9];
                restImagesCount.font = [UIFont systemFontOfSize:25];
                restImagesCount.textAlignment = NSTextAlignmentCenter;
                restImagesCount.backgroundColor = [UIColor colorWithRed:237/255.0 green:237/255.0 blue:237/255.0 alpha:1];
                
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(restMenuPhotoCountClick:)];
                [restImagesCount addGestureRecognizer:tap];
                restImagesCount.userInteractionEnabled = YES;
                [photosScrollView addSubview:restImagesCount];
                i=11;
            }
            
        }
        
        photosScrollView.contentSize = CGSizeMake(dishImageEndX+3, 0);
        photosScrollView.bounces = NO;
        photosScrollView.showsHorizontalScrollIndicator = NO;
        photosScrollView.frame = CGRectMake(0, titleLabel.endPointY+SHADOWHEIGHT, DeviceWidth, dishImageWidth+10);
        CGFloat menuViewendY = photosScrollView.endPointY;
        if (self.menuPhotoArray.count) {
            UILabel *tempLable = [[UILabel alloc] init];
            tempLable.text = L(@"See All");
            tempLable.font = [UIFont systemFontOfSize:12];
            //    tempLable.frame = CGRectMake(DeviceWidth-tempLable.expectedWidth, currentPointY+25, tempLable.expectedWidth, tempLable.font.lineHeight);
            
            UIButton *seeAllBtn = [UIButton buttonWithType:UIButtonTypeCustom];
           
            [seeAllBtn setTitle:[NSString stringWithFormat:L(@"See All")] forState:UIControlStateNormal];
            seeAllBtn.frame = CGRectMake(DeviceWidth-tempLable.expectedWidth-LEFTLEFTSET*2 - 1
                                         , 15, tempLable.expectedWidth, tempLable.font.lineHeight);
            [seeAllBtn setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
            seeAllBtn.titleLabel.font = [UIFont systemFontOfSize:12];
            [seeAllBtn addTarget:self action:@selector(gotosMenu:) forControlEvents:UIControlEventTouchUpInside];
            
            [self addSubview:seeAllBtn];
            
            UIImageView *ArrowImage = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth - 20, 0, 6, 10)];
            ArrowImage.centerY = seeAllBtn.centerY;
//            ArrowImage.backgroundColor = [UIColor greenColor];
            ArrowImage.image = [UIImage imageNamed:@"ic_home_arrow"];
            [self addSubview:ArrowImage];
            
            currentPointY=seeAllBtn.endPointY;
            
            menuViewendY = photosScrollView.endPointY +LEFTLEFTSET;
        }
         UIImageView *lineImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(0,menuViewendY, DeviceWidth, 1)];
         lineImage3.image = [UIImage imageNamed:@"CutOffRule"];
         [self addSubview:lineImage3];
        self.frame = CGRectMake(0, 0, DeviceWidth, menuViewendY+1);

    }
    return self;
}
-(void)uploadImageViewTap{
    if ([self.delegate respondsToSelector:@selector(uploadImageViewPressed)]) {
        [self.delegate uploadImageViewPressed];
    }
}
-(void)menuPhotoimagesPressed:(UITapGestureRecognizer*)sender{
    if ([self.delegate respondsToSelector:@selector(menuPhotoimagePressed:)]) {
        [self.delegate menuPhotoimagePressed:sender];
    }
}
-(void)restMenuPhotoCountClick:(UITapGestureRecognizer *)sender{
    if ([self.delegate respondsToSelector:@selector(restMenuPhotosCountClick:)]) {
        [self.delegate restMenuPhotosCountClick:sender];
    }
}
-(void)gotosMenu:(UIButton*)sender{
    if ([self.delegate respondsToSelector:@selector(gotoMenu:)]) {
        [self.delegate gotoMenu:sender];
    }
}
@end
