//
//  DetailBottomButtonView.m
//  Qraved
//
//  Created by harry on 2018/2/26.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "DetailBottomButtonView.h"

@implementation DetailBottomButtonView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithRed:250/255.0 green:250/255.0 blue:250/255.0 alpha:1.0f];
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOpacity = 0.3;
        self.layer.shadowOffset = CGSizeMake(0, 3);
        self.layer.shadowRadius = 5;
        
        //[self createUIWithRestaurant:restaurant];
    }
    return self;
}

- (void)createUIWithRestaurant:(IMGRestaurant *)restaurant{
    if ([Tools isBlankString:restaurant.goFoodLink]) {
        if ([restaurant.bookStatus intValue]==1 && ([restaurant.state intValue]==1 || [restaurant.state intValue]==6)) {
            if (restaurant.phoneNumber.length>5) {
                //book , call
                CGFloat buttonWidth = (DeviceWidth - 40)/2;
                
                FunctionButton *callButton = [self createCallButton];
                callButton.frame = CGRectMake(LEFTLEFTSET, LEFTLEFTSET, buttonWidth, 42);
                [self addSubview:callButton];
                
                FunctionButton *bookButton = [self createBookButton];
                bookButton.frame = CGRectMake(LEFTLEFTSET + buttonWidth + 10, LEFTLEFTSET, buttonWidth, 42);
                bookButton.backgroundColor = [UIColor redButtonColor];
                [bookButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [self addSubview:bookButton];
                
            }else{
                //book and map
                CGFloat buttonWidth = (DeviceWidth - 40)/2;
                FunctionButton *bookButton = [self createBookButton];
                bookButton.frame = CGRectMake(LEFTLEFTSET, LEFTLEFTSET, buttonWidth, 42);
                [self addSubview:bookButton];
                
                FunctionButton *mapButton = [self createMapButton];
                mapButton.frame = CGRectMake(LEFTLEFTSET + buttonWidth + 10, LEFTLEFTSET, buttonWidth, 42);
                mapButton.backgroundColor = [UIColor redButtonColor];
                [mapButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [self addSubview:mapButton];
            }
                
        }else{
            if (restaurant.phoneNumber.length>5) {
                //call and map
                CGFloat buttonWidth = (DeviceWidth - 40)/2;
                FunctionButton *callButton = [self createCallButton];
                callButton.frame = CGRectMake(LEFTLEFTSET, LEFTLEFTSET, buttonWidth, 42);
                [self addSubview:callButton];
                
                FunctionButton *mapButton = [self createMapButton];
                mapButton.frame = CGRectMake(LEFTLEFTSET + buttonWidth + 10, LEFTLEFTSET, buttonWidth, 42);
                mapButton.backgroundColor = [UIColor redButtonColor];
                [mapButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [self addSubview:mapButton];
            }else{
                // map
                CGFloat buttonWidth = DeviceWidth - 30;
                FunctionButton *mapButton = [self createMapButton];
                mapButton.backgroundColor = [UIColor redButtonColor];
                [mapButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                mapButton.frame = CGRectMake(LEFTLEFTSET, LEFTLEFTSET, buttonWidth, 42);
                [self addSubview:mapButton];
            }
        }
        
    }else{
        if ([restaurant.bookStatus intValue]==1 && ([restaurant.state intValue]==1 || [restaurant.state intValue]==6)) {
            
            if (restaurant.phoneNumber.length>5) {
                //book , call and goFood
                CGFloat goFoodWidth = 137;
                CGFloat buttonWidth = (DeviceWidth - 50 - 137)/2;
                FunctionButton *callButton = [self createCallButton];
                callButton.frame = CGRectMake(LEFTLEFTSET, LEFTLEFTSET, buttonWidth, 42);
                [self addSubview:callButton];
                
                FunctionButton *bookButton = [self createBookButton];
                bookButton.frame = CGRectMake(LEFTLEFTSET + buttonWidth + 10, LEFTLEFTSET, buttonWidth, 42);
                [self addSubview:bookButton];
                
                FunctionButton *goFoodButton = [self createGoFoodButton];
                goFoodButton.frame = CGRectMake(LEFTLEFTSET + (buttonWidth + 10)*2, LEFTLEFTSET, goFoodWidth, 42);
                [self addSubview:goFoodButton];
                
            }else{
                //book and goFood
                CGFloat buttonWidth = (DeviceWidth - 40)/2;
                FunctionButton *bookButton = [self createBookButton];
                bookButton.frame = CGRectMake(LEFTLEFTSET, LEFTLEFTSET, buttonWidth, 42);
                [self addSubview:bookButton];
                
                FunctionButton *goFoodButton = [self createGoFoodButton];
                goFoodButton.frame = CGRectMake(LEFTLEFTSET + buttonWidth + 10, LEFTLEFTSET, buttonWidth, 42);
                [self addSubview:goFoodButton];
            }
        }else{
            if (restaurant.phoneNumber.length>5) {
                //call and goFood
                CGFloat buttonWidth = (DeviceWidth - 40)/2;
                FunctionButton *callButton = [self createCallButton];
                callButton.frame = CGRectMake(LEFTLEFTSET, LEFTLEFTSET, buttonWidth, 42);
                [self addSubview:callButton];
                
                FunctionButton *goFoodButton = [self createGoFoodButton];
                goFoodButton.frame = CGRectMake(LEFTLEFTSET + buttonWidth + 10, LEFTLEFTSET, buttonWidth, 42);
                [self addSubview:goFoodButton];
            }else{
                // goFood and map
                CGFloat buttonWidth = (DeviceWidth - 40)/2;
                FunctionButton *mapButton = [self createMapButton];
                mapButton.frame = CGRectMake(LEFTLEFTSET, LEFTLEFTSET, buttonWidth, 42);
                [self addSubview:mapButton];
                
                FunctionButton *goFoodButton = [self createGoFoodButton];;
                goFoodButton.frame = CGRectMake(LEFTLEFTSET + buttonWidth + 10, LEFTLEFTSET, buttonWidth, 42);
                [self addSubview:goFoodButton];
            }
        }
    }
}

- (FunctionButton *)createCallButton{
    FunctionButton *callButton = [FunctionButton buttonWithType:UIButtonTypeCustom andTitle:@"Call" andBGColor:[UIColor grayButtonColor] andTitleColor:[UIColor color333333]];
    [callButton addTarget:self action:@selector(callButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    return callButton;
}

- (FunctionButton *)createBookButton{
    FunctionButton *bookButton = [FunctionButton buttonWithType:UIButtonTypeCustom andTitle:@"Book" andBGColor:[UIColor grayButtonColor] andTitleColor:[UIColor color333333]];
    [bookButton addTarget:self action:@selector(bookButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    return bookButton;
}

- (FunctionButton *)createMapButton{
    FunctionButton *mapButton = [FunctionButton buttonWithType:UIButtonTypeCustom andTitle:@"Map" andBGColor:[UIColor grayButtonColor] andTitleColor:[UIColor color333333]];
    [mapButton addTarget:self action:@selector(mapButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    return mapButton;
}

- (FunctionButton *)createGoFoodButton{
    FunctionButton *goFoodButton = [FunctionButton buttonWithType:UIButtonTypeCustom];
    [goFoodButton addTarget:self action:@selector(goFoodButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    return goFoodButton;
}

- (void)callButtonTapped{
    if (self.delegate && [self.delegate respondsToSelector:@selector(callButtonTapped)]) {
        [self.delegate callButtonTapped];
    }
}

- (void)bookButtonTapped{
    if (self.delegate && [self.delegate respondsToSelector:@selector(bookButtonTapped)]) {
        [self.delegate bookButtonTapped];
    }
}

- (void)mapButtonTapped{
    if (self.delegate && [self.delegate respondsToSelector:@selector(mapButtonTapped)]) {
        [self.delegate mapButtonTapped];
    }
}

- (void)goFoodButtonTapped{
    if (self.delegate && [self.delegate respondsToSelector:@selector(goFoodButtonTapped)]) {
        [self.delegate goFoodButtonTapped];
    }
}


@end
