//
//  DetailSeeMenDeliveryViewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/11.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "DetailSeeMenDeliveryViewController.h"
#import "DetailSeeMenuDeleveryModel.h"
#import "DetailDataHandler.h"
#import "IMGMenu.h"
#import "MenuPhotoViewColltroller.h"
@interface DetailSeeMenDeliveryViewController ()<UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    UICollectionView *DeleveryCollectionView;
    DetailSeeMenuDeleveryModel *DeleveryModel;
    IMGRestaurant *restaurant;
    int offset;
}
@property (nonatomic, strong)  NSMutableArray  *dataArray;

@end

@implementation DetailSeeMenDeliveryViewController

-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant{
    self = [super init];
    if (self != nil) {
        restaurant = paramRestaurant;
    }
    return self;
}
-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant andIsSelfUpload:(BOOL)isSelfUpload{
    self = [super init];
    if (self != nil) {
        restaurant = paramRestaurant;
        self.isSelfUpload = isSelfUpload;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initUI];
    [self requestData];
    offset = 0;
    _dataArray = [NSMutableArray array];
    DeleveryCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
}
- (void)requestData{
    
    [self initData];
}
- (void)loadMoreData{
    [self initData];
}
- (void)initData{
    
    NSDictionary *dict=@{@"restaurantId":[NSString stringWithFormat:@"%@",restaurant.restaurantId],@"max":@"15",@"offset":[NSString stringWithFormat:@"%d",offset]};
    
    [DetailDataHandler SeeMenuDelevery:dict andSuccessBlock:^(NSMutableArray *array) {
        [DeleveryCollectionView.mj_footer endRefreshing];
        if ([array count] >0) {
            offset += 20;
        }
        [_dataArray addObjectsFromArray:array];
        [DeleveryCollectionView reloadData];
        
        
    } anderrorBlock:^{
        [DeleveryCollectionView.mj_footer endRefreshing];
        
    }];
    
}
- (void)initUI{
    
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    CGFloat width = (DeviceWidth-4)/3;
    layout.itemSize = CGSizeMake(width, width);
    layout.minimumLineSpacing = 1;
    layout.minimumInteritemSpacing = 1;
    //    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    
    DeleveryCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,40, DeviceWidth, DeviceHeight - 84) collectionViewLayout:layout];
    DeleveryCollectionView.backgroundColor = [UIColor whiteColor];
    DeleveryCollectionView.delegate = self;
    DeleveryCollectionView.dataSource = self;
    DeleveryCollectionView.showsHorizontalScrollIndicator = NO;
    DeleveryCollectionView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:DeleveryCollectionView];
    
    
    [DeleveryCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"photoCell"];
    
}

#pragma mark -- collection  delegate
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    
    for (UIView *subView in cell.contentView.subviews) {
        [subView removeFromSuperview];
    }
    DeleveryModel = [_dataArray objectAtIndex:indexPath.row];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.contentView.bounds];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[DeleveryModel.imageUrl returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        //image animation
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        if ([manager diskImageExistsForURL:[NSURL URLWithString:[DeleveryModel.imageUrl returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]]]) {
            NSLog(@"dont loading animation");
        }else {
            imageView.alpha = 0.0;
            [UIView transitionWithView:imageView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                imageView.image = image;
                imageView.alpha = 1.0;
            } completion:NULL];
            
        }
    }];
    [cell.contentView addSubview:imageView];
    
    return cell;
    
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return _dataArray.count;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    NSInteger tag = indexPath.row;
    //    NSMutableArray *images=[[NSMutableArray alloc] init];
    NSMutableArray *photoImageUrl = [[NSMutableArray alloc] init];
    for (DetailSeeMenuDeleveryModel *deleveryModel in _dataArray) {
        IMGMenu *menu_ = [[IMGMenu alloc] init];
        //        [menu_ setValuesForKeysWithDictionary:dict];
        menu_.imageUrl = deleveryModel.imageUrl;//[dict objectForKey:@"imageUrl"];
        menu_.menuId = deleveryModel.myid;
        menu_.createTimeLong = (long long)deleveryModel.createTime;
        [photoImageUrl addObject:menu_];
        
    }
    
    
    
    MenuPhotoViewColltroller *menuPhotoViewColltroller = [[MenuPhotoViewColltroller alloc]initWithPhotosArray:photoImageUrl andPhotoTag:tag andRestaurant:restaurant andFromDetail:YES];
    menuPhotoViewColltroller.isUseWebP = self.isUseWebP;
    menuPhotoViewColltroller.menuPhotoCount = [NSNumber numberWithInteger:_dataArray.count];
    menuPhotoViewColltroller.amplitudeType = @"Restaurant detail page - menu list";
    [self.navigationController pushViewController:menuPhotoViewColltroller animated:YES];

}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"%f",DeleveryCollectionView.contentOffset.y);
    if (DeleveryCollectionView.contentOffset.y <= 0) {
        DeleveryCollectionView.bounces = NO;
        
        NSLog(@"jinzhixiala");
    }
    else
        if (DeleveryCollectionView.contentOffset.y >= 0){
            DeleveryCollectionView.bounces = YES;
            NSLog(@"allow shangla");
            
        }
}

@end
