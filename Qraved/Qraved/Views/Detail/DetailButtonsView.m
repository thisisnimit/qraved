//
//  DetailButtonsView.m
//  Qraved
//
//  Created by apple on 17/3/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "DetailButtonsView.h"
#import "UIColor+Hex.h"
#import "UIConstants.h"

@implementation DetailButtonsView
-(instancetype)init{
    if(self = [super init]){
        NSArray *btnNameAry = @[/*L(@"Call"),L(@"Favorite"),*/L(@"Save"),L(@"Add Review"),L(@"Add Photo")];
        NSArray *btnImageNameAry = [[NSArray alloc] initWithObjects:/*@"callGreenImage",@"noSaveImage",*/@"ic_heart_idle_rdp",@"ic_review",@"ic_camera_alt_rdp", nil];//ic_camera_alt
        
        float btnWidth = DeviceWidth/3;
        int btnCount = 3;
//        float btnGap = ((DeviceWidth - LEFTLEFTSET*2)-(btnWidth*btnCount))/(btnCount-1);
        
        for (int i = 0; i<btnCount; i++)
        {
           float btnx = i * btnWidth;
            
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.backgroundColor = [UIColor whiteColor/*colorWithRed:arc4random_uniform(255)/255.0 green:arc4random_uniform(255)/255.0 blue:arc4random_uniform(255)/255.0 alpha:1*/];
            btn.frame = CGRectMake(btnx, 0, btnWidth - 0.5, 80);
            btn.tag = 1000+i;
            
            UIImageView *imgView = [[UIImageView alloc] init];
            if (i==0) {
                imgView.frame = CGRectMake((btnWidth-23)/2, 2, 23, 20);
            }else if (i==1){
                imgView.frame = CGRectMake((btnWidth-24)/2, 0, 24, 24);
            }else if (i==2){
                imgView.frame = CGRectMake((btnWidth-20)/2, 2, 20, 19);
            }
            imgView.image = [UIImage imageNamed:[btnImageNameAry objectAtIndex:i]];
            [btn addSubview:imgView];
            //imgView.backgroundColor = [UIColor greenColor];
            imgView.tag = 2000+i;
            
            UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 32, btnWidth, 16)];
            lblTitle.textAlignment = NSTextAlignmentCenter;
            //lblTitle.backgroundColor = [UIColor blueColor];
            lblTitle.text = [btnNameAry objectAtIndex:i];
            lblTitle.font = [UIFont systemFontOfSize:12];
            lblTitle.textColor = [UIColor colorRed];
            [btn addSubview:lblTitle];

            [self addSubview:btn];
            [btn addTarget:self action:@selector(btnsClick:) forControlEvents:UIControlEventTouchUpInside];
            
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(btn.right, 10, 0.5, 60)];
            lineView.backgroundColor = [UIColor colorWithRed:239/255.0f green:239/255.0f blue:239/255.0f alpha:1.0f];
            [self addSubview:lineView];
        }
        
    }
    return self;
}

-(void)btnsClick:(UIButton*)sender{
    if ([self.delegate respondsToSelector:@selector(btnClick:)]) {
        [self.delegate btnClick:sender];
    }

}
@end
