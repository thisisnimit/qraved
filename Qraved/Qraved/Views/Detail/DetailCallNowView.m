//
//  DetailCallNowView.m
//  Qraved
//
//  Created by apple on 17/3/23.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "DetailCallNowView.h"
#import "UIDevice+Util.h"
#import "UIConstants.h"


@implementation DetailCallNowView

-(instancetype)initWithRestaurant:(IMGRestaurant*)restaurant{
    if (self = [super init]) {
        self.frame = CGRectMake(50, DeviceHeight-62+[UIDevice heightDifference], DeviceWidth - 100, 40);
        self.backgroundColor = [UIColor colorWithRed:250/255.0f green:250/255.0f blue:250/255.0f alpha:1.0f];//[UIColor colorWithPatternImage:[UIImage imageNamed:@"SitDownTimeBg"]];
        self.layer.cornerRadius = 20;
        self.layer.masksToBounds = YES;
        self.layer.borderWidth = 0.5;
        self.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//        [button setBackgroundImage:[UIImage imageNamed:@"CallNowButton"] forState:UIControlStateNormal];
//        button.frame = CGRectMake(7, 7, DeviceWidth-14, 50);
//        [button addTarget:self action:@selector(telephoneButtonTapped) forControlEvents:UIControlEventTouchUpInside];
//        UILabel *callLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth-14, 25)];
//        callLabel.text = L(@"Call");
//        callLabel.backgroundColor = [UIColor clearColor];
//        callLabel.font =[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:15];
//        callLabel.textColor = [UIColor whiteColor];
//        callLabel.textAlignment = NSTextAlignmentCenter;
//        [button addSubview:callLabel];
//        
//        UILabel *phoneNumberLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 25, DeviceWidth-14, 25)];
//        phoneNumberLabel.text = restaurant.phoneNumber;
//        phoneNumberLabel.backgroundColor = [UIColor clearColor];
//        phoneNumberLabel.font =[UIFont fontWithName:FONT_GOTHAM_LIGHT size:15];
//        phoneNumberLabel.textColor = [UIColor whiteColor];
//        phoneNumberLabel.textAlignment = NSTextAlignmentCenter;
//        [button addSubview:phoneNumberLabel];
//        [self addSubview:button];
        
        
//        bookButtonView = [[UIView alloc]initWithFrame:CGRectMake(20, DeviceHeight-60+[UIDevice heightDifference], DeviceWidth - 40, 40)];
//        //    bookButtonView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"SitDownTimeBg"]];
//        bookButtonView.backgroundColor = [UIColor whiteColor];
//        bookButtonView.layer.cornerRadius = bookButtonView.height/2;
//        bookButtonView.layer.masksToBounds = YES;
//        bookButtonView.layer.borderWidth = 0.5;
//        bookButtonView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        [self.view addSubview:bookButtonView];
        
        UIButton *btnCall = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCall.frame = CGRectMake(0, 0, self.width/2, 40);
        [btnCall setTitle:[@"Call" uppercaseString] forState:UIControlStateNormal];
        [btnCall setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
        btnCall.titleLabel.font = [UIFont boldSystemFontOfSize:15];
        [btnCall setImage:[UIImage imageNamed:@"ic_phone@2x"] forState:UIControlStateNormal];
        [btnCall setImageEdgeInsets:UIEdgeInsetsMake(0, 80, 0, 0)];
        [btnCall setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 40)];
        [btnCall addTarget:self action:@selector(telephoneButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btnCall];
        
        UIView *callView = [[UIView alloc] initWithFrame:CGRectMake(btnCall.right, 5, 0.5, 30)];
        callView.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:callView];
        
        UIButton *btnMap = [UIButton buttonWithType:UIButtonTypeCustom];
        btnMap.frame = CGRectMake(btnCall.right, 0, self.width/2, 40);
        [btnMap setTitle:[@"Map" uppercaseString] forState:UIControlStateNormal];
        [btnMap setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
        btnMap.titleLabel.font = [UIFont boldSystemFontOfSize:15];
        [btnMap setImage:[UIImage imageNamed:@"ic_map@2x"] forState:UIControlStateNormal];
        [btnMap setImageEdgeInsets:UIEdgeInsetsMake(0, 80, 0, 0)];
        [btnMap setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 40)];
        [btnMap addTarget:self action:@selector(gotoMap) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btnMap];
        
//        UIView *mapV = [[UIView alloc] initWithFrame:CGRectMake(btnMap.right, 5, 0.5, 30)];
//        mapV.backgroundColor = [UIColor lightGrayColor];
//        [self addSubview:mapV];
//        
//        UIButton *btnBookNow = [UIButton buttonWithType:UIButtonTypeCustom];
//        btnBookNow.frame = CGRectMake(btnMap.right, 0, bookButtonView.width/3, 40);
//        [btnBookNow setTitle:@"Book Now" forState:UIControlStateNormal];
//        [btnBookNow setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        btnBookNow.titleLabel.font = [UIFont systemFontOfSize:15];
//        [btnBookNow setImage:[UIImage imageNamed:@"ic_booknow@2x"] forState:UIControlStateNormal];
//        [btnBookNow setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
//        [btnBookNow addTarget:self action:@selector(bookButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
//        [bookButtonView addSubview:btnBookNow];
//
        
        
        
    }
    return self;
}
-(void)telephoneButtonTapped{
    if ([self.delegate respondsToSelector:@selector(telephoneButtonTapped2)]) {
        [self.delegate telephoneButtonTapped2];
    }
}
- (void)gotoMap{
    if ([self.delegate respondsToSelector:@selector(gotoMap)]) {
        [self.delegate mapButtonTapped2];
    }

}
@end
