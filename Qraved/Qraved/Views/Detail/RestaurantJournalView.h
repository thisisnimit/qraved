//
//  RestaurantJournalView.h
//  Qraved
//
//  Created by System Administrator on 9/24/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "EGORefreshTableFooterView.h"

@class MoreArticleViewController;

@interface RestaurantJournalView : UIView<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,EGORefreshTableFooterDelegate>

@property(nonatomic,strong) NSNumber *restaurantId;

@property(nonatomic,strong) NSMutableArray *journals;

@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) EGORefreshTableFooterView *refreshView;

@property(nonatomic,weak) MoreArticleViewController *delegate;

@property(nonatomic,assign) BOOL isLoading;
@property(nonatomic,assign) BOOL hasData;

-(instancetype)initWithFrame:(CGRect)frame withRestaurantId:(NSNumber*)restaurantId;

//-(void)addRefreshView;
-(void)setRefreshViewFrame;

@end


