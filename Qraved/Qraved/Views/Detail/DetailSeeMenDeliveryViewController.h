//
//  DetailSeeMenDeliveryViewController.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/11.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"

@interface DetailSeeMenDeliveryViewController : BaseViewController


@property (nonatomic,assign)BOOL isSelfUpload;
@property (nonatomic,assign) BOOL isUseWebP;
-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant;
-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant andIsSelfUpload:(BOOL)isSelfUpload;
@end
