//
//  ReviewRatingHeaderView.h
//  Qraved
//
//  Created by harry on 2017/11/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewRatingHeaderView : UIView

@property (nonatomic, strong) IMGRestaurant *restaurant;

@end
