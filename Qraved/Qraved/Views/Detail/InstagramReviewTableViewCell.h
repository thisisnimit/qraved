//
//  InstagramReviewTableViewCell.h
//  Qraved
//
//  Created by harry on 2017/11/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGReview.h"

@protocol InstagramReviewTableViewCellDelegate <NSObject>
@optional
- (void)instagramReviewImagePressed:(IMGReview*)review withTag:(int)tag;

@end

@interface InstagramReviewTableViewCell : UITableViewCell
@property (nonatomic, strong) IMGReview *review;
@property (nonatomic,assign) float currentPointY;
@property (nonatomic, assign) id<InstagramReviewTableViewCellDelegate>delegate;

@end
