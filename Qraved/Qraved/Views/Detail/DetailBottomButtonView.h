//
//  DetailBottomButtonView.h
//  Qraved
//
//  Created by harry on 2018/2/26.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DetailBottomButtonViewDelegate <NSObject>

-(void)callButtonTapped;
-(void)mapButtonTapped;
-(void)bookButtonTapped;
-(void)goFoodButtonTapped;

@end

@interface DetailBottomButtonView : UIView

@property(nonatomic,weak) id<DetailBottomButtonViewDelegate> delegate;

//- (instancetype)initWithRestaurant:(IMGRestaurant *)restaurant;
- (void)createUIWithRestaurant:(IMGRestaurant *)restaurant;

@end
