//
//  DetaiPhotosView.m
//  Qraved
//
//  Created by apple on 17/3/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "DetaiPhotosView.h"
#import "IMGDish.h"
#import "Label.h"
#import "UIConstants.h"
#import "UIImageView+WebCache.h"
#import "UIColor+Helper.h"
#import "UILabel+Helper.h"
#import "UIImage+Resize.h"
#import "UIView+Helper.h"
#import "UIColor+Hex.h"


#define CONTENT_OFFSET 5
#define TOP_SCROLL_HEIGHT 180
#define NAV_HEIGHT 35
#define mapViewHeight 100
#define SHADOWHEIGHT 8
#define CELLOFFSET 6
#define SCORLLVIEW_Y 23
#define TITLE_MARGIN 15
#define BTN_HEIGHT 35

@implementation DetaiPhotosView

-(instancetype)initWithPhtosArray:(NSArray*)photoArray andRestaurant:(IMGRestaurant*)restaurant{
    if (self = [super init]) {
        if (photoArray.count==0)
        {
            return self;
        }
        
        
        self.backgroundColor = [UIColor clearColor];
        
        Label *titleLabel=[[Label alloc] initWithFrame:CGRectZero andTextFont:[UIFont boldSystemFontOfSize:14] andTextColor:[UIColor color222222] andTextLines:1];
        [titleLabel setText:L(@"Photos")];
        [titleLabel setFrame:CGRectMake(LEFTLEFTSET, TITLE_MARGIN, titleLabel.expectedWidth, titleLabel.font.lineHeight)];
        [self addSubview:titleLabel];
        
        UIScrollView *photosScrollView = [[UIScrollView alloc] init];
        photosScrollView.scrollsToTop = NO;
        [self addSubview:photosScrollView];
        
        CGFloat dishImageWidth;
        CGFloat dishImageEndX;
        
        for (int i = 0 ; i<photoArray.count; i++)
        {
            UIImageView *dishImageView = [[UIImageView alloc]init];
            dishImageView.contentMode = UIViewContentModeCenter;
            dishImageWidth = (DeviceWidth-LEFTLEFTSET-40)/3;
            dishImageView.frame = CGRectMake((dishImageWidth+10)*i+LEFTLEFTSET, 0, dishImageWidth, dishImageWidth);
            dishImageView.tag = i;
            UITapGestureRecognizer *Tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTap:)];
            [Tap setNumberOfTapsRequired:1];
            [Tap setNumberOfTouchesRequired:1];
            dishImageView.userInteractionEnabled=YES;
            [dishImageView addGestureRecognizer:Tap];
            IMGDish *dish=[photoArray objectAtIndex:i];
            [photosScrollView addSubview:dishImageView];
            dishImageEndX = dishImageView.endPointX;
            
            NSString *urlString = [dish.imageUrl returnFullWebPImageUrlWithWidth:dishImageWidth];
            __weak typeof(dishImageView) weakRestaurantImageView = dishImageView;
            if ([dish.type intValue]==2) {
               
                if (dish.lowResolutionImage.length>0) {
                    urlString = dish.lowResolutionImage;
                }else if (dish.thumbnailImage.length>0){
                    urlString = dish.thumbnailImage;
                }else if (dish.standardResolutionImage.length>0){
                    urlString = dish.standardResolutionImage;
                }else{
                    urlString = dish.imageUrl;
                }
                
                UIImageView *instagramLogoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(dishImageView.frame.size.width/6*5-5, dishImageView.frame.size.width/6*5-5, dishImageView.frame.size.width/6, dishImageView.frame.size.width/6)];
                instagramLogoImageView.image = [UIImage imageNamed:@"ins2.png"];
                [dishImageView addSubview:instagramLogoImageView];
            }
            
            UIImage *placehoderImage = [[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(dishImageWidth, dishImageWidth)];
            [dishImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:placehoderImage options:SDWebImageRetryFailed|SDWebImageAllowInvalidSSLCertificates completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (image) {
                    image = [image imageByScalingAndCroppingForSize:CGSizeMake(dishImageWidth, dishImageWidth)];
                    [weakRestaurantImageView setImage:image];
                    
                }
                
            }];
            if (i==9&&[restaurant.dishCount intValue]>10) {
                [dishImageView removeFromSuperview];
                UILabel *restImagesCount = [[UILabel alloc] init];
                restImagesCount.frame = CGRectMake(((DeviceWidth-LEFTLEFTSET-40)/3.0+10)*9+LEFTLEFTSET, -1, (DeviceWidth-LEFTLEFTSET-40)/3.0, (DeviceWidth-LEFTLEFTSET-40)/3.0);
                restImagesCount.text = [NSString stringWithFormat:@"+%d",[restaurant.disCount intValue]-9];
                restImagesCount.font = [UIFont systemFontOfSize:25];
                restImagesCount.textAlignment = NSTextAlignmentCenter;
                restImagesCount.backgroundColor = [UIColor colorWithRed:237/255.0 green:237/255.0 blue:237/255.0 alpha:1];
                [photosScrollView addSubview:restImagesCount];
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(restDishPhotoCountClick:)];
                [restImagesCount addGestureRecognizer:tap];
                restImagesCount.userInteractionEnabled = YES;
                i=11;
            }
        }
        
        photosScrollView.contentSize = CGSizeMake(dishImageEndX+3, 0);
        photosScrollView.bounces = NO;
        photosScrollView.showsHorizontalScrollIndicator = NO;
        photosScrollView.frame = CGRectMake(0, titleLabel.endPointY+SHADOWHEIGHT, DeviceWidth, dishImageWidth+10);
        
        
        UILabel *tempLable = [[UILabel alloc] init];
        tempLable.text = L(@"See All");
        tempLable.font = [UIFont systemFontOfSize:12];
        
        UIButton *seeAllBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [seeAllBtn setTitle:[NSString stringWithFormat:L(tempLable.text)] forState:UIControlStateNormal];
        seeAllBtn.frame = CGRectMake(DeviceWidth-tempLable.expectedWidth-LEFTLEFTSET*2 - 1
                                     , 15, tempLable.expectedWidth, tempLable.font.lineHeight);
        [seeAllBtn setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
        seeAllBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        seeAllBtn.tag = 1;
        [seeAllBtn addTarget:self action:@selector(gotoPhoto:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:seeAllBtn];
        
        UIImageView *ArrowImage = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth - 20, 0, 6, 10)];
        ArrowImage.centerY = seeAllBtn.centerY;
//        ArrowImage.backgroundColor = [UIColor greenColor];
        ArrowImage.image = [UIImage imageNamed:@"ic_home_arrow"];
        [self addSubview:ArrowImage];
        
//        UIButton *photoImageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        photoImageBtn.frame = CGRectMake(seeAllBtn.frame.origin.x - 40, seeAllBtn.frame.origin.y+1, 13, 12);
//        [photoImageBtn setBackgroundImage:[UIImage imageNamed:@"photoImage"] forState:UIControlStateNormal];
//        [photoImageBtn addTarget:self action:@selector(uploadPhotos) forControlEvents:UIControlEventTouchUpInside];
//        [self addSubview:photoImageBtn];
//        
//        UILabel *textLabel = [[UILabel alloc] init];
//        textLabel.text = @"|";
//        textLabel.font = [UIFont systemFontOfSize:12];
//        textLabel.frame = CGRectMake(photoImageBtn.frame.origin.x+25, photoImageBtn.frame.origin.y-2, textLabel.expectedWidth, textLabel.font.lineHeight);
//        [self addSubview:textLabel];
        self.frame = CGRectMake(0, 0, DeviceWidth, photosScrollView.endPointY+15);
        UIImageView *lineImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(0,  photosScrollView.endPointY+15, DeviceWidth, 1)];
        lineImage3.image = [UIImage imageNamed:@"CutOffRule"];
        [self addSubview:lineImage3];

    }
    
    

    return self;

}
-(void)imageTap:(UITapGestureRecognizer*)sender{
    if ([self.delegate respondsToSelector:@selector(imagePressed:)]) {
        [self.delegate imagePressed:sender];
    }
}
-(void)uploadPhotos{
    if ([self.delegate respondsToSelector:@selector(uploadPhoto)]) {
        [self.delegate uploadPhoto];
    }
}
-(void)gotoPhoto:(UIButton*)sender{
    if ([self.delegate respondsToSelector:@selector(gotoPhotos:)]) {
        [self.delegate gotoPhotos:sender];
    }
}
-(void)restDishPhotoCountClick:(UITapGestureRecognizer*)sender{
    if ([self.delegate respondsToSelector:@selector(restDishPhotosCountClick:)]) {
        [self.delegate restDishPhotosCountClick:sender];
    }
}
@end
