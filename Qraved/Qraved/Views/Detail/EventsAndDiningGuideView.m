//
//  EventsAndDiningGuideView.m
//  Qraved
//
//  Created by System Administrator on 10/14/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "EventsAndDiningGuideView.h"
#import "EventsAndDiningGuideViewCell.h"
#import "EventsAndDiningViewController.h"
#import "IMGMyList.h"
#import "IMGRestaurant.h"
#import "UIConstants.h"
#import "IMGDiningGuide.h"
#import "IMGEvent.h"
#import "UIDevice+Util.h"
#define HEADER_HEIGHT 50

@implementation EventsAndDiningGuideView{
	UIView *headerView;
}

@synthesize hasData;

-(instancetype)initWithFrame:(CGRect)frame{
	if(self=[super initWithFrame:frame]){
		hasData=YES;
		self.events=[[NSMutableArray alloc] init];
		self.diningGuides=[[NSMutableArray alloc] init];
		[self setHeader];
		self.tableView=[[UITableView alloc] initWithFrame:CGRectMake(0,0,frame.size.width,frame.size.height-44)];
		self.tableView.delegate=self;
		self.tableView.dataSource=self;
		self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
		self.tableView.tableHeaderView=headerView;
		[self addSubview:self.tableView];
	}
	return self;
}

-(void)setRefreshViewFrame{
	if(hasData){
		if(self.refreshView==nil){
			self.refreshView=[[EGORefreshTableFooterView alloc] initWithFrame:CGRectMake(0,DeviceHeight,DeviceWidth,66)];
			self.refreshView.backgroundColor=[UIColor clearColor];
			self.refreshView.delegate=self;
			[self.tableView addSubview:self.refreshView];
		}
		if(self.tableView.contentSize.height>self.tableView.frame.size.height){
			self.refreshView.frame=CGRectMake(0,self.tableView.contentSize.height+10,DeviceWidth,66);
		}else{
			self.refreshView.frame=CGRectMake(0,self.tableView.frame.size.height+10,DeviceWidth,66);
		}
	}else if(self.refreshView){
		[self.refreshView removeFromSuperview];
		self.refreshView=nil;
	}
}

-(void)setHeader{
	headerView=[[UIView alloc] initWithFrame:CGRectMake(0,0,DeviceWidth-LEFTLEFTSET*2,20)];
	UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET,2,DeviceWidth-LEFTLEFTSET*2,20)];
	titleLabel.text=L(@"DINING GUIDE");
	titleLabel.alpha = 0.6;
	titleLabel.font = [UIFont systemFontOfSize:10];
	UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 20, DeviceWidth, 1)];
    lineImage.image = [UIImage imageNamed:@"CutOffRule"];
    [headerView addSubview:titleLabel];
    [headerView addSubview:lineImage];
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexpath{
	return DeviceWidth/4+8;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
	return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	if(section==0){
		return self.events.count;
	}else{
		return self.diningGuides.count;
	}
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexpath{
	EventsAndDiningGuideViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"event_dinning_cell"];
	if(cell==nil){
		cell=[[EventsAndDiningGuideViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"event_dinning_cell"];
	}
	if(indexpath.section==0){
		cell.title=[(IMGEvent*)[self.events objectAtIndex:indexpath.row] name];
		cell.image=[(IMGEvent*)[self.events objectAtIndex:indexpath.row] appBanner];
		cell.isEvent=YES;
	}else{
		cell.title=[(IMGDiningGuide*)[self.diningGuides objectAtIndex:indexpath.row] pageName];
		cell.image=[(IMGDiningGuide*)[self.diningGuides objectAtIndex:indexpath.row] headerImage];
		cell.isEvent=NO;
	}
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexpath{
    [tableView deselectRowAtIndexPath:indexpath animated:NO];
    if(indexpath.section==0){
    	if(self.delegate && [self.delegate respondsToSelector:@selector(goToEvent:)]){
			[self.delegate goToEvent:(IMGEvent*)[self.events objectAtIndex:indexpath.row]];
		}
    }else{
    	if(self.delegate && [self.delegate respondsToSelector:@selector(goToDinningGuide:)]){
			[self.delegate goToDinningGuide:(IMGDiningGuide*)[self.diningGuides objectAtIndex:indexpath.row]];
		}
    }
}

- (void)egoRefreshTableFooterDidTriggerRefresh:(EGORefreshTableFooterView*)view{
//	if(self.delegate && [self.delegate respondsToSelector:@selector(loadEventAndDinningGuide:)]){
//		[self.delegate loadEventAndDinningGuide:^(BOOL _hasData){
//			hasData=_hasData;
//			[self setRefreshViewFrame];
//        }];
//	}
}

-(void)scrollViewDidScroll:(UITableView*)tableView{
	[self.refreshView egoRefreshScrollViewDidScroll:tableView];
}

-(void)scrollViewDidEndDragging:(UITableView*)tableView willDecelerate:(BOOL)decelerate{
	if(self.tableView.contentOffset.y>self.tableView.contentSize.height-self.tableView.frame.size.height+66){
		self.refreshView.frame=CGRectMake(0,self.tableView.contentSize.height,DeviceWidth,66);
		[self.refreshView egoRefreshScrollViewDidEndDragging:tableView];
	}
}

-(void)dealloc{
	
}


@end
