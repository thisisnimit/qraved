//
//  DetailCouponView.m
//  Qraved
//
//  Created by harry on 2018/1/30.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "DetailCouponView.h"

@implementation DetailCouponView
{
    UIScrollView *couponScroll;
    UIPageControl *pageControl;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    
    self.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    
    couponScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceWidth*.24)];
    couponScroll.delegate = self;
    couponScroll.pagingEnabled = YES;
    [self addSubview:couponScroll];
    
    pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, DeviceWidth*.24 - 10, DeviceWidth, 5)];
    pageControl.centerX = self.centerX;
    pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    pageControl.currentPageIndicatorTintColor = [UIColor colorWithHexString:@"D20000"];
    [pageControl setCurrentPage:0];
    [self addSubview:pageControl];
}

- (void)setCouponArray:(NSArray *)couponArray{
    _couponArray = couponArray;
    
    if (couponArray.count > 1) {
        pageControl.numberOfPages = couponArray.count;
    }else{
        pageControl.hidden = YES;
    }
    
    for (int i = 0; i < couponArray.count; i ++) {
        
        NSDictionary *couponDic = [couponArray objectAtIndex:i];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth * i, 0, DeviceWidth, DeviceWidth*.24)];
        imageView.tag = i;
        imageView.userInteractionEnabled = YES;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        
        [imageView sd_setImageWithURL:[NSURL URLWithString:[[couponDic objectForKey:@"bannerUrl"] returnCurrentImageString]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        
        [couponScroll addSubview:imageView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
        [imageView addGestureRecognizer:tap];
    }
    [couponScroll setContentSize:CGSizeMake(DeviceWidth * couponArray.count, 0)];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    int index =  scrollView.contentOffset.x / DeviceWidth;
    pageControl.currentPage = index;
}

- (void)imageTapped:(UITapGestureRecognizer *)tap{
    if (self.couponImageTapped) {
        self.couponImageTapped();
    }
}

@end
