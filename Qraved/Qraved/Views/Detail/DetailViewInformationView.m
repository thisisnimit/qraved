//
//  DetailViewInformationView.m
//  Qraved
//
//  Created by apple on 17/3/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "DetailViewInformationView.h"
#import "Label.h"
#import "UIColor+Helper.h"
#import "UILabel+Helper.h"
#import "UIView+Helper.h"

#define navigationHeight  (44+[UIDevice heightDifference])
#define REFRESH_HEADER_HEIGHT 52.0f
#define CONTENT_OFFSET 5
#define TOP_SCROLL_HEIGHT 180
#define NAV_HEIGHT 35
#define mapViewHeight 100
#define SHADOWHEIGHT 8
#define CELLOFFSET 6
#define SCORLLVIEW_Y 23
#define TITLE_MARGIN 15
#define BTN_HEIGHT 35


@implementation DetailViewInformationView
-(instancetype)initWithRestaurant:(IMGRestaurant *)restaurant andShowClaim:(NSNumber*)showClaim{
    if (self = [super init]) {
        self.backgroundColor = [UIColor clearColor];
        
        Label *titleLabel=[[Label alloc] initWithFrame:CGRectZero andTextFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:20] andTextColor:[UIColor color222222] andTextLines:1];
        [titleLabel setText:L(@"Information")];
        [titleLabel setFrame:CGRectMake(LEFTLEFTSET, TITLE_MARGIN, titleLabel.expectedWidth, 0)];
        titleLabel.backgroundColor = [UIColor greenColor];
        [self addSubview:titleLabel];
        
        
        CGFloat currentPointY = titleLabel.endPointY;
        
        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, currentPointY+5, DeviceWidth, 1)];
        lineImage.image = [UIImage imageNamed:@"CutOffRule"];
        currentPointY = lineImage.endPointY;
        [self addSubview:lineImage];
        
        UIButton *instagramBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        instagramBtn.frame = CGRectMake(0, currentPointY +5, DeviceWidth, 30);
        instagramBtn.backgroundColor = [UIColor whiteColor];
        instagramBtn.tag = 100000;
        
        [instagramBtn addTarget:self action:@selector(btnsClick:) forControlEvents:UIControlEventTouchUpInside];
        UILabel *instagramlabel = [[UILabel alloc] init];
        instagramlabel.text = @"Instagram Places";//btnNameStr;
        instagramlabel.textColor = [UIColor color222222];
        instagramlabel.frame = CGRectMake(50, 0, DeviceWidth-50 - 25, 30);
        instagramlabel.font = [UIFont systemFontOfSize:14];
        [instagramBtn addSubview:instagramlabel];
        
        UIImageView *instagramImage = [[UIImageView alloc]initWithFrame:CGRectMake(15, 7, 20, 20)];
        instagramImage.image = [UIImage imageNamed:@"instagram"];
        [instagramBtn addSubview:instagramImage];

        
        UIImageView *instagramImag = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth - 25, 7, 15, 15)];
        instagramImag.image = [UIImage imageNamed:@"arrow_right"];
        [instagramBtn addSubview:instagramImag];
        
        
        currentPointY = instagramBtn.endPointY +5;
        [self addSubview:instagramBtn];
        
        
        
//        UIView *lineone = [[UIView alloc] initWithFrame:CGRectMake(50, currentPointY, DeviceWidth - 50, 1)];
//        lineone.backgroundColor = [UIColor colorWithRed:239/255.0f green:239/255.0f blue:239/255.0f alpha:1.0f];
//        [self addSubview:lineone];
//        currentPointY = lineone.endPointY +5;
        
        NSMutableArray *btnNamesArr = [[NSMutableArray alloc] init];
        if (restaurant.phoneNumber.length >1)
        {
            NSMutableString *phoneNumber = [[NSMutableString alloc]initWithString:restaurant.phoneNumber];
            for (int i=(int)phoneNumber.length-1; i>-1; i--) {
                char c = [phoneNumber characterAtIndex:i];
                if ([[NSString stringWithFormat:@"%c",c] isEqualToString:@"+"] || [[NSString stringWithFormat:@"%c",c] isEqualToString:@"("] || [[NSString stringWithFormat:@"%c",c] isEqualToString:@")"] || [[NSString stringWithFormat:@"%c",c] isEqualToString:@" "]) {
                    [phoneNumber deleteCharactersInRange:NSMakeRange(i, 1)];
                }
            }
            if (restaurant.phoneNumber.length == 0 || [phoneNumber integerValue] == 0) {
                
            }else{
                restaurant.phoneNumber = [restaurant.phoneNumber filterHtml];
                NSString *str = [[NSString alloc] initWithFormat:@"100%@",restaurant.phoneNumber];
                [btnNamesArr addObject:str];
            }
        }
        NSString *str;
        if([restaurant.td intValue]==0){
            str = L(@"101Today: Closed");
        }else{
            str = [[NSString alloc] initWithFormat:L(@"101Today: %@ - %@"),restaurant.todayTimeOpen,restaurant.todayTimeClosed];
            if ([restaurant.todayTimeOpen isEqualToString:@"00:00"]&&([restaurant.todayTimeClosed isEqualToString:@"00:00"]||[restaurant.todayTimeClosed isEqualToString:@"24:00"])) {
                str=[NSString stringWithFormat:L(@"101Today: 24 Hours Open")];
            }
            
            
        }
        
        [btnNamesArr addObject:str];
        
        [btnNamesArr addObject:L(@"103More info")];
        [btnNamesArr addObject:L(@"104Suggest an Edit")];
        if ([showClaim intValue] == 1) {
            [btnNamesArr addObject:L(@"105Claim your Restaurant")];
        }
        for (NSString *str in btnNamesArr)
        {
            NSRange range;
            range.location = 0;
            range.length = 3;
            NSString *btnTagStr = [str substringWithRange:range];
            NSString *btnNameStr = [str substringFromIndex:3];
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake(0, currentPointY+10, DeviceWidth, 30);
            btn.tag = [btnTagStr intValue];
            [btn addTarget:self action:@selector(btnsClick:) forControlEvents:UIControlEventTouchUpInside];
            UILabel *label = [[UILabel alloc] init];
            label.text = btnNameStr;
            label.textColor = [UIColor color222222];
            label.frame = CGRectMake(50, 0, DeviceWidth-50 - 25, 30);
            label.font = [UIFont systemFontOfSize:14];
            [btn addSubview:label];
            
            UIImageView *btnImage = [[UIImageView alloc]initWithFrame:CGRectMake(15, 7, 20, 20)];
            btnImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"btnImage%@",btnTagStr]];
            [btn addSubview:btnImage];
            [self addSubview:btn];
            
            UIImageView *btnImag = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth - 25, 7, 15, 15)];
            btnImag.image = [UIImage imageNamed:@"arrow_right"];
            [btn addSubview:btnImag];
            [self addSubview:btn];
            
            UIView *lineone = [[UIView alloc] initWithFrame:CGRectMake(50, currentPointY, DeviceWidth - 50, 1)];
            lineone.backgroundColor = [UIColor colorWithRed:239/255.0f green:239/255.0f blue:239/255.0f alpha:1.0f];
            [self addSubview:lineone];
            
            currentPointY = currentPointY + 50;
            
            
        }
        
        UIImageView *lineImage2 = [[UIImageView alloc]initWithFrame:CGRectMake(0, currentPointY+5, DeviceWidth, 1)];
        lineImage2.image = [UIImage imageNamed:@"CutOffRule"];
        currentPointY = lineImage2.endPointY;
        [self addSubview:lineImage2];
            self.frame = CGRectMake(0,0, DeviceWidth, currentPointY);

    }

    return self;
}
-(void)btnsClick:(UIButton*)sender{
    if ([self.delegate respondsToSelector:@selector(btnClick:)]) {
        [self.delegate btnClick:sender];
    }
}

@end
