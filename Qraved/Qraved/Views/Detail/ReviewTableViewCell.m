//
//  ReviewTableViewCell.m
//  Qraved
//
//  Created by harry on 2017/11/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "ReviewTableViewCell.h"
#import "ReviewView.h"
#import "IMGDish.h"
#import "V2_LikeCommentShareView.h"
#import "DLStarRatingControl.h"
#import "LoadingView.h"
#import "HomeUtil.h"
#import "LikeView.h"
#import "ReviewHandler.h"
#import "ProfileViewController.h"
#import "V2_CommentListViewController.h"

#define CONTENTLABELWIDTH DeviceWidth-LEFTMARGIN*2

#define LINK_TEXT @"<a style=\"text-decoration:none;color:#09BFD3;font-family:'OpenSans'\" href=\"http://www.qraved.com/\">...read more</a>"
@interface ReviewTableViewCell()<UIWebViewDelegate,V2_LikeCommentShareViewDelegete,LikeListViewDelegate>
@end

@implementation ReviewTableViewCell
{
    UIButton *btnUserPhoto;
    UIButton *btnEdit;
    UILabel *lblName;
    UILabel *lblDes;
    UILabel *lblTime;
    UIWebView *webCaption;
    V2_LikeCommentShareView *lCSView;
    UIView *photosView;
    DLStarRatingControl *ratingScore;
    UILabel *lblReviewTitle;
    
    NSMutableArray* likelistArr;
    LikeView* likepopView;
    UIImageView *imgJournal;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}
- (void)createUI{
    btnUserPhoto = [UIButton buttonWithType:UIButtonTypeCustom];
    btnUserPhoto.frame = CGRectMake(LEFTLEFTSET, TOPOFFSET, 40, 40);
    btnUserPhoto.layer.masksToBounds = YES;
    btnUserPhoto.layer.cornerRadius = 20;
    [btnUserPhoto addTarget:self action:@selector(controlTapped) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:btnUserPhoto];
    
    btnEdit = [UIButton buttonWithType:UIButtonTypeCustom];
    btnEdit = [UIButton buttonWithType:UIButtonTypeCustom];
    btnEdit.frame = CGRectMake(DeviceWidth - 25, TOPOFFSET - 3, 25, 50);
    [btnEdit setImage:[UIImage imageNamed:@"listMoreVertical"] forState:UIControlStateNormal];
    [self.contentView addSubview:btnEdit];
    
    [btnEdit addTarget:self action:@selector(moreBtnClcik) forControlEvents:UIControlEventTouchUpInside];
    
    lblName = [[UILabel alloc]init];
    lblName.textColor = [UIColor color333333];
    lblName.font = [UIFont systemFontOfSize:15];
    
    lblName.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap_nameLabel = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(controlTapped)];
    [lblName addGestureRecognizer:tap_nameLabel];
    
    [self.contentView addSubview:lblName];
    
    lblDes = [[UILabel alloc]init];
    lblDes.font = [UIFont systemFontOfSize:12];
    lblDes.textColor = [UIColor color999999];
    lblDes.alpha = 0.7f;
    [self.contentView addSubview:lblDes];
    
    lblTime = [[UILabel alloc]init];
    lblTime.textColor = [UIColor color999999];
    lblTime.alpha = 0.7f;
    lblTime.font = [UIFont systemFontOfSize:12];
    lblTime.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:lblTime];
    
    
    ratingScore=[[DLStarRatingControl alloc]initWithFrame:CGRectMake(LEFTLEFTSET, btnUserPhoto.endPointY+10, 75, 15) andStars:5 andStarWidth:13 isFractional:NO spaceWidth:2];
    ratingScore.userInteractionEnabled=NO;
    [self.contentView addSubview:ratingScore];
    
    imgJournal = [[UIImageView alloc] init];
    imgJournal.image = [UIImage imageNamed:@"ic_home_qraved_logo"];
    [self.contentView addSubview:imgJournal];
    imgJournal.hidden = YES;
    
    lblReviewTitle = [[UILabel alloc] init];
    lblReviewTitle.font = [UIFont systemFontOfSize:15];
    lblReviewTitle.textColor = [UIColor color333333];
    lblReviewTitle.numberOfLines = 0;
    lblReviewTitle.lineBreakMode=NSLineBreakByWordWrapping;
    [self.contentView addSubview:lblReviewTitle];
    
    
    webCaption = [[UIWebView alloc] initWithFrame:CGRectMake(LEFTLEFTSET, lblReviewTitle.endPointY+5, DeviceWidth - LEFTLEFTSET*2, 40)];
    webCaption.tag=711;
    webCaption.delegate = self;
    webCaption.scrollView.bounces = NO;
    webCaption.backgroundColor=[UIColor whiteColor];
    webCaption.scrollView.showsHorizontalScrollIndicator = NO;
    webCaption.scrollView.scrollEnabled = YES;
    webCaption.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.contentView addSubview:webCaption];
    
    
    lCSView = [[V2_LikeCommentShareView alloc] init];
    lCSView.backgroundColor = [UIColor whiteColor];
    lCSView.delegate = self;
    [self.contentView addSubview:lCSView];
    
    UIButton *btnLike = [UIButton buttonWithType:UIButtonTypeCustom];
    btnLike.frame = CGRectMake(DeviceWidth/3-60, 0, 60, 50);
    [lCSView addSubview:btnLike];
    
    [btnLike addTarget:self action:@selector(likeCountTapped:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setReview:(IMGReview *)review{
    _review = review;
    
    
    NSURL *urlString;
    self.review.avatar = [self.review.avatar stringByReplacingPercentEscapesUsingEncoding:
                          NSUTF8StringEncoding];
    
    if ([self.review.avatar hasPrefix:@"http"])
    {
        urlString = [NSURL URLWithString:self.review.avatar];
    }
    else
    {
        urlString = [NSURL URLWithString:[self.review.avatar returnFullImageUrl]];
    }
    
   // __weak typeof(btnUserPhoto)weakBtnUserPhoto = btnUserPhoto;
    [btnUserPhoto sd_setImageWithURL:urlString forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"headDefault.jpg"] completed:nil];
    
    lblName.text = [NSString stringWithFormat:@"%@",self.review.fullName];
    lblName.frame = CGRectMake(btnUserPhoto.frame.size.width+20,TOPOFFSET, lblName.expectedWidth, lblName.font.lineHeight);
    
    if ([review.userDishCount intValue]!=0&&[review.userReviewCount intValue]!=0) {
        [lblDes setText:[NSString stringWithFormat:@"%@ %@ • %@ %@",review.userReviewCount,[review.userReviewCount intValue] > 1 ? @"reviews":@"review",review.userDishCount,[review.userDishCount intValue] > 1 ? @"photos":@"photo"]];
    }else if ([review.userDishCount intValue]==0||[review.userReviewCount intValue]==0){
        if ([review.userReviewCount intValue]==0&&[review.userDishCount intValue]!=0) {
            [lblDes setText:[NSString stringWithFormat:@"%@ %@",review.userDishCount,[review.userDishCount intValue] > 1 ? @"photos":@"photo"]];
        }
        if ([review.userDishCount intValue]==0&&[review.userReviewCount intValue]!=0) {
            
            [lblDes setText:[NSString stringWithFormat:@"%@ %@",review.userReviewCount,[review.userReviewCount intValue] > 1 ? @"reviews":@"review"]];
        }
    }
    
    ratingScore.rating= [[NSNumber numberWithDouble:[self.review.score doubleValue]] getRatingNumber];
    
    lblDes.frame = CGRectMake(lblName.frame.origin.x,lblName.endPointY, lblDes.expectedWidth, lblDes.font.lineHeight);
    
    lblTime.text = [Date getTimeInteval_v10:[review.timeCreated longLongValue]/1000];
    lblTime.frame = CGRectMake(DeviceWidth - 160 - 15, btnUserPhoto.endPointY + 10, 160, 14);
    ratingScore.frame = CGRectMake(LEFTLEFTSET, btnUserPhoto.endPointY+10, 75, 15);
    if (self.review.title.length > 0){
        
        CGFloat titleX = 0.0f;
        if (![self.review.journalId isEqual:@0]) {
            imgJournal.frame = CGRectMake(LEFTLEFTSET, ratingScore.endPointY + 10, 16, 16);
            imgJournal.hidden = NO;
            titleX = LEFTLEFTSET + 20;
            lblReviewTitle.userInteractionEnabled = YES;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoJournal)];
            [lblReviewTitle addGestureRecognizer:tap];
        }else{
            imgJournal.hidden = YES;
            titleX = LEFTLEFTSET;
        }
        
        lblReviewTitle.text = [self.review.title filterHtml];
        lblReviewTitle.frame = CGRectMake(titleX, ratingScore.endPointY+10, DeviceWidth-LEFTLEFTSET-titleX, 1000);
        [lblReviewTitle sizeToFit];
    }else{
        lblReviewTitle.frame = CGRectMake(LEFTLEFTSET, ratingScore.endPointY+10, DeviceWidth-2*LEFTLEFTSET, 0);
    }
    
    NSString *summarize=review.summarize;
    NSDictionary *strBackDic = [self getIsNeedReadMoreForOverLines:summarize];
    float contentLabelHeight;
    if([strBackDic[@"isNeedLoadMore"] boolValue]&&!review.isReadMore){
        summarize= strBackDic[@"finalStr"];
    }else{
        if(!review.isReadMore && summarize.length>300){
            summarize=[NSString stringWithFormat:@"%@%@",[summarize substringToIndex:300],LINK_TEXT];
        }
    }
    contentLabelHeight = [self calculateCommentTextHeight:summarize];
//
    webCaption.frame=CGRectMake(LEFTLEFTSET, lblReviewTitle.endPointY+5, DeviceWidth - LEFTLEFTSET*2, contentLabelHeight);
    NSString *summarizeStr = [NSString stringWithFormat:@"<html> <head> <meta charset=\"UTF-8\">"
                              "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">"
                              "<style type=\"text/css\">"
                              "html {"
                              "    -webkit-text-size-adjust: none; /* Never autoresize text */"
                              "}"
                              "body{font-family:\"%@\";font-size: 12px; color:%@; padding:0; margin:0; width:%fpx; line-height: 15px;}"
                              "</style>"
                              "</head>"
                              "<body><div id=\"content\">%@</div></body>"
                              "</html>",DEFAULT_FONT_NAME,@"#999999",DeviceWidth - LEFTLEFTSET*2,summarize];
    
    summarizeStr = [summarizeStr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
    [webCaption loadHTMLString:summarizeStr baseURL:nil];
    
    
    [webCaption layoutSubviews];
    
    
    BOOL isOtherUser;
    IMGUser *user = [IMGUser currentUser];
    if ([review.userId intValue] == [user.userId intValue]){
        isOtherUser = NO;
    }else{
        isOtherUser = YES;
    }
    
    if (isOtherUser)
    {
        btnEdit.hidden = YES;
    }else{
        btnEdit.hidden = NO;
    }
    
    if (photosView) {
        [photosView removeFromSuperview];
    }
    photosView = [[UIView alloc] init];
    photosView.frame = CGRectMake(LEFTLEFTSET, webCaption.endPointY+5, DeviceWidth - 2*LEFTLEFTSET, 0);
    
    
    CGFloat dishImageWidth;
    CGFloat dishImageEndX;
    int viewFrameY = 0;
    int viewFrameX = 0;
    for (int i = 0; i<review.imageArray.count; i++)
    {
        IMGDish *dish=(IMGDish*)[review.imageArray objectAtIndex:i];
        UIImageView *dishImageView = [[UIImageView alloc]init];
        dishImageWidth = (photosView.frame.size.width-10)/3;
        dishImageView.userInteractionEnabled=YES;
        dishImageView.contentMode = UIViewContentModeScaleAspectFill;
        dishImageView.clipsToBounds = YES;
        if (viewFrameX == 3)
        {
            viewFrameY = dishImageWidth + 5 + viewFrameY;
            viewFrameX = 0;
        }

        dishImageView.frame = CGRectMake((dishImageWidth+5)*viewFrameX, viewFrameY, dishImageWidth, dishImageWidth);
        viewFrameX++;
        dishImageView.tag = i;
        [photosView addSubview:dishImageView];


        if (review.imageArray.count == 1) {

            dishImageView.frame = CGRectMake(0, viewFrameY, DeviceWidth, DeviceWidth/4*3);
        }

        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagePressed:)];
        [dishImageView addGestureRecognizer:tap];

        dishImageEndX = dishImageView.endPointX;

        if (i == 5 && review.imageArray.count > 6 )
        {
            dishImageView.backgroundColor = [UIColor colorWithRed:222/250.0 green:222/250.0 blue:222/250.0 alpha:1];
            UILabel *otherCountLabel = [[UILabel alloc] init];
            otherCountLabel.tag=i;
            otherCountLabel.userInteractionEnabled=YES;
            otherCountLabel.text = [NSString stringWithFormat:@"+%lu",review.imageArray.count - 5];
            otherCountLabel.font = [UIFont systemFontOfSize:17];
            otherCountLabel.textColor = [UIColor color333333];
            otherCountLabel.frame = CGRectMake(dishImageView.frame.size.width/2 - 20, dishImageView.frame.size.height/2 - 20, 40, 40);
            [otherCountLabel addGestureRecognizer:tap];
            [dishImageView addSubview:otherCountLabel];
            break;
        }
        else
        {
            NSString *urlString = [dish.imageUrl returnFullImageUrl];
            __weak typeof(dishImageView) weakRestaurantImageView = dishImageView;

            [dishImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [UIView animateWithDuration:1.0 animations:^{
                    [weakRestaurantImageView setAlpha:1.0];
                }];
            }];
        }

    
        photosView.frame = CGRectMake(LEFTLEFTSET, webCaption.endPointY+5, DeviceWidth - 2*LEFTLEFTSET, dishImageView.endPointY);
        if (review.imageArray.count == 1) {
            photosView.frame = CGRectMake(0, webCaption.endPointY+5, DeviceWidth, dishImageView.endPointY);

        }
    }

    [self.contentView addSubview:photosView];
    
    lCSView.frame = CGRectMake(0, photosView.endPointY+5, DeviceWidth, 40);
    [lCSView setCommentCount:[review.commentCount intValue] likeCount:[review.likeCount intValue] liked:review.isLike];
    
    self.currentPointY = lCSView.endPointY+10;
}


- (void)likeCountTapped:(UIButton *)button{
    [[Amplitude instance] logEvent:@" RC - View People Who Likes"];
    
    if (likepopView) {
        [likepopView removeFromSuperview];
    }
    
    [[LoadingView sharedLoadingView]startLoading];
    
    [HomeUtil getLikeListFromServiceWithReviewId:self.review.reviewId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
        likelistArr=[NSMutableArray arrayWithArray:likeListArray];
        likepopView.likecount=likeviewcount;
        
        [[LoadingView sharedLoadingView]stopLoading];
        likepopView.likelistArray=likelistArr;
        
    }];
    
    likepopView=[[LikeView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    
    likepopView.delegate=self;
    UIResponder *responder = [self nextResponder];
    while (responder){
        if ([responder isKindOfClass:[UINavigationController class]]){
            UINavigationController* vc=(UINavigationController*)responder;
            [[vc.viewControllers lastObject].view addSubview:likepopView];
            return;
        }
        responder = [responder nextResponder];
    }
}

-(void)returnLikeCount:(NSInteger)count
{
    
}

-(void)LikeviewLoadMoreData:(NSInteger)offset{
    
    [HomeUtil getLikeListFromServiceWithReviewId:self.review.reviewId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset]andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
        likepopView.likecount=likeviewcount;
        [likelistArr addObjectsFromArray:likeListArray];
        [likepopView.likeUserTableView reloadData];
    }];
    
}

- (void)likeCommentShareView:(UIView *)likeCommentShareView likeButtonTapped:(UIButton *)button{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
    [[LoadingView sharedLoadingView] startLoading];
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:self.review.reviewId forKey:@"Review_ID"];
    [eventProperties setValue:self.review.userId forKey:@"ReviewerUser_ID"];
    [eventProperties setValue:self.review.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:@"Restaurant detail page" forKey:@"Location"];
    [ReviewHandler likeReview:self.review.isLike reviewId:self.review.reviewId andBlock:^{
        if (!self.review.isLike) {
            [[Amplitude instance] logEvent:@"UC - UnLike Review Succeed" withEventProperties:eventProperties];
        }else{
            [[Amplitude instance] logEvent:@"UC - Like Review Succeed" withEventProperties:eventProperties];
        }
        
        
    } failure:^(NSString *exceptionMsg){
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
    }];
    
    self.review.isLike=!self.review.isLike;
    if(self.review.isLike){
        self.review.likeCount=[NSNumber numberWithInt:[self.review.likeCount intValue]+1];
        
        [[Amplitude instance] logEvent:@"UC - Like Review Initiate" withEventProperties:eventProperties];
        
    }else{
        self.review.likeCount=[NSNumber numberWithInt:[self.review.likeCount intValue]-1];
        
        [[Amplitude instance] logEvent:@"UC - UnLike Review Initiate" withEventProperties:eventProperties];
    }
    
    [lCSView setCommentCount:[self.review.commentCount intValue] likeCount:[self.review.likeCount intValue] liked:self.review.isLike];
    
    [[LoadingView sharedLoadingView] stopLoading];
    
    
}
- (void)likeCommentShareView:(UIView *)likeCommentShareView shareButtonTapped:(UIButton *)button{
    if (self.delegate) {
        [self.delegate shareButtonTapped:self.review shareButton:button];
    }
    
}
- (void)likeCommentShareView:(UIView *)likeCommentShareView commentButtonTapped:(UIButton *)button{
    
    V2_CommentListViewController *commentListVC = [[V2_CommentListViewController alloc] initWithTypeID:14 andModel:self.review];
    [commentListVC setRefreshComment:^{
        int commentCount = [self.review.commentCount intValue];
        commentCount ++;
        self.review.commentCount = [NSNumber numberWithInt:commentCount];
        [lCSView setCommentCount:[self.review.commentCount intValue] likeCount:[self.review.likeCount intValue] liked:self.review.isLike];
    }];
    
    [self.navController pushViewController:commentListVC animated:YES];
    
}

- (void)controlTapped
{
    if (self.delegate)
    {
        IMGUser *user = [[IMGUser alloc] init];
        user.userName = [NSString stringWithString:self.review.fullName];
        user.userId = [NSNumber numberWithInteger:[self.review.userId integerValue]];
        user.token = @"";
        user.avatar=self.review.avatar;
        [self.delegate reviewUserNameOrImageTapped:user];
    }
}
-(void)imagePressed:(UIGestureRecognizer*)gesture{
    if([self.delegate respondsToSelector:@selector(reviewImagePressed:withTag:)]){
        [self.delegate reviewImagePressed:self.review withTag:(int)gesture.view.tag];
    }
}

- (void)moreBtnClcik
{
    if([self.delegate respondsToSelector:@selector(userReviewMoreBtnClickWithIMGUserReview:)]){
        IMGUserReview *userReview = [[IMGUserReview alloc] init];
        userReview.reviewId = self.review.reviewId;
        userReview.restaurantId = self.review.restaurantId;
        userReview.reviewScore = self.review.score;
        userReview.reviewTimeCreated = self.review.timeCreated;
        userReview.userId = [NSNumber numberWithInt:[self.review.userId intValue]];
        //        userReview.restaurantPriceLevel = self.review.;
        userReview.reviewTitle = [self.review.title filterHtml];
        userReview.reviewSummarize = self.review.summarize;
        userReview.restaurantTitle = self.review.restaurantTitle;
        userReview.userFullName = self.review.fullName;
        userReview.userAvatar = self.review.avatar;
        userReview.dishListArrM = [NSMutableArray arrayWithArray:self.review.imageArray];
        [self.delegate userReviewMoreBtnClickWithIMGUserReview:userReview];
    }
}

- (void)gotoJournal{
    if (self.delegate && [self.delegate respondsToSelector:@selector(gotoJournalDetail:)]) {
        [self.delegate gotoJournalDetail:self.review.journalId];
    }
}

#pragma mark - webview delegate
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    UIScrollView *tempView = (UIScrollView *)[webCaption.subviews objectAtIndex:0];
    
    tempView.scrollEnabled = NO;
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if (navigationType==UIWebViewNavigationTypeLinkClicked) {
        self.review.isReadMore = !self.review.isReadMore;
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(reviewMsgReadMoreTapped:)]) {
            [self.delegate reviewMsgReadMoreTapped:self.indexPath];
        }
        
        return NO;
    }
    
    return YES;
}


-(CGFloat)calculateCommentTextHeight:(NSString*)commentStr_{
    
    commentStr_=[commentStr_ stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    commentStr_=[commentStr_ stringByReplacingOccurrencesOfString:LINK_TEXT withString:@"...read more"];
    
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12]};
    
    CGSize textSize = [commentStr_ boundingRectWithSize:CGSizeMake(DeviceWidth-30, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    
    float height = ceil(textSize.height + 1.5f);
    
    NSLog(@"height = %@",commentStr_);
    
    return height+5;
}

-(NSDictionary *)getIsNeedReadMoreForOverLines:(NSString*)commentStr_{
    
    UILabel *lblTempStr = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth-30, MAXFLOAT)];
    lblTempStr.text = commentStr_;
    lblTempStr.font = [UIFont systemFontOfSize:12];
    
    
    NSString *text = [lblTempStr text];
    UIFont   *font = [lblTempStr font];
    CGRect    rect = [lblTempStr frame];
    
    
    CTFontRef myFont = CTFontCreateWithName((CFStringRef)font.fontName,
                                            font.pointSize,
                                            NULL);
    
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:text];
    [attStr addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)myFont range:NSMakeRange(0, attStr.length)];
    
    CTFramesetterRef frameSetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)attStr);
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, CGRectMake(0,0,rect.size.width,100000));
    
    CTFrameRef frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, 0), path, NULL);
    
    NSArray *lines = (__bridge NSArray *)CTFrameGetLines(frame);
    
    NSMutableArray *linesArray = [[NSMutableArray alloc]init];
    
    NSMutableDictionary *strBackDic = [[NSMutableDictionary alloc]init];
    
    if(lines.count<10+1){
        [strBackDic setObject:@NO forKey:@"isNeedLoadMore"];
    }else{
        [strBackDic setObject:@YES forKey:@"isNeedLoadMore"];
        for (id line in lines)
        {
            CTLineRef lineRef = (__bridge CTLineRef)line;
            CFRange lineRange = CTLineGetStringRange(lineRef);
            NSRange range = NSMakeRange(lineRange.location, lineRange.length);
            
            NSString *lineString = [text substringWithRange:range];
            
            //            CFRelease(lineRef);
            [linesArray addObject:lineString];
            
        }
        
        //start range work for 4th line
        NSMutableAttributedString *attStr4Th = [[NSMutableAttributedString alloc] initWithString:linesArray[10-1]];
        [attStr4Th addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)myFont range:NSMakeRange(0, attStr4Th.length)];
        CTFramesetterRef frameSetter4Th = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)attStr4Th);
        
        CGMutablePathRef path4Th = CGPathCreateMutable();
        CGPathAddRect(path4Th, NULL, CGRectMake(0,0,DeviceWidth-100,MAXFLOAT));
        
        CTFrameRef frame4Th = CTFramesetterCreateFrame(frameSetter4Th, CFRangeMake(0, 0), path4Th, NULL);
        
        NSArray *lines4Th = (__bridge NSArray *)CTFrameGetLines(frame4Th);
        CTLineRef lineRef4Th = (__bridge CTLineRef )[lines4Th firstObject];
        CFRange lineRange4Th = CTLineGetStringRange(lineRef4Th);
        NSRange range4Th = NSMakeRange(lineRange4Th.location, lineRange4Th.length);
        NSString *lineString4Th = [linesArray[10-1] substringWithRange:range4Th];
        
        NSString *finalStr = @"";
        for(int i=0; i<10-1; i++){
            finalStr = [finalStr stringByAppendingString:linesArray[i]];
        }
        lineString4Th=[lineString4Th stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        finalStr = [finalStr stringByAppendingString:lineString4Th];
        //        finalStr = [finalStr stringByAppendingString:@"<a href=\"http://www.qraved.com/\">...read more</a>"];
        
        finalStr = [finalStr stringByAppendingString:LINK_TEXT];
        
        
        finalStr=[finalStr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
        [strBackDic setObject:finalStr forKey:@"finalStr"];
        
        CFRelease(frame4Th);
        CFRelease(frameSetter4Th);
        CFRelease(path4Th);
        //        CFRelease(lineRef4Th);
        
    }
    
    CFRelease(frame);
    CFRelease(myFont);
    CFRelease(frameSetter);
    CFRelease(path);
    return strBackDic;
}


@end
