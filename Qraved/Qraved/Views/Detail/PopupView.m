

#import "PopupView.h"
#import "UIViewController+LewPopupViewController.h"
#import "LewPopupViewAnimationFade.h"
#import "LewPopupViewAnimationSlide.h"
#import "LewPopupViewAnimationSpring.h"
#import "LewPopupViewAnimationDrop.h"

#import "UIView+Helper.h"
#import "UILabel+Helper.h"
#import "PopupTableViewCell.h"

#import "UIColor+Hex.h"
#import "ListHandler.h"
#import "IMGMyList.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"

#import "LoadingImageView.h"
#import "LoadingView.h"
#import "UILabel+Helper.h"
#import "UIColor+Helper.h"

#define LEFTSET 10
#define RESTAURANT_IMAGE_SIZE 33


@implementation PopupView
{
    UITableView *_tableView;
    CGFloat _current;
    NSMutableArray *_dataAry;
    UIScrollView *_scrollView;
    IMGUser *_user;
    NSString *_newListName;
    NSNumber *_restaurantId;
    UITextField *_textField;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        //[[NSBundle mainBundle] loadNibNamed:[[self class] description] owner:self options:nil];
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 10.0;
        self.layer.borderWidth = 0;
        self.backgroundColor = [UIColor whiteColor];
        self.frame = frame;
    }
    return self;
}

- (id)initWithTutorialViewFrame:(CGRect)frame andText:(NSString *)text andIsNext:(BOOL)isNext
{
    self = [super initWithFrame:CGRectMake(0, 0, DeviceWidth-30, DeviceWidth-30)];
    if (self) {

        
        UIControl *backgroundView = [[UIControl alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
        [self addSubview:backgroundView];
        
        UIView *tutorialView = [[UIView alloc] initWithFrame:frame];
        tutorialView.layer.masksToBounds = YES;
        tutorialView.layer.cornerRadius = 10.0;
        tutorialView.layer.borderWidth = 0;
        tutorialView.backgroundColor = [UIColor whiteColor];
        [self addSubview:tutorialView];
        
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.text = text;
        titleLabel.font = [UIFont systemFontOfSize:15];
        CGSize size = [titleLabel sizeWithWidth:frame.size.width - LEFTSET*2];
        titleLabel.frame = CGRectMake(LEFTSET, 10, size.width, size.height);
        [tutorialView addSubview:titleLabel];
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.numberOfLines = 0;
        tutorialView.frame = CGRectMake(tutorialView.frame.origin.x, tutorialView.frame.origin.y, titleLabel.endPointX + 10, titleLabel.endPointY + 10);


        if (isNext)
        {
            UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            [nextBtn setTitle:L(@"Next") forState:UIControlStateNormal];
            [nextBtn setTitleColor:[UIColor colorRed] forState:UIControlStateNormal];
             nextBtn.frame = CGRectMake(tutorialView.frame.size.width - 10 - nextBtn.titleLabel.expectedWidth-5, titleLabel.endPointY + 20, nextBtn.titleLabel.expectedWidth, 20);
            [tutorialView addSubview:nextBtn];
            [nextBtn addTarget:self action:@selector(nextBtnClick) forControlEvents:UIControlEventTouchUpInside];
            
            tutorialView.userInteractionEnabled = YES;
            tutorialView.frame = CGRectMake(tutorialView.frame.origin.x, tutorialView.frame.origin.y, titleLabel.endPointX + 10, nextBtn.endPointY + 10);
        }
        
        
    }
    return self;
}

- (void)nextBtnClick
{
    [_parentVC lew_dismissPopupView];
}

- (void)addTextLabelWithText:(NSString *)text
{

}

-(id)initWithUser:(IMGUser *)user andRestaurantId:(NSNumber *)restaurantId requestCallback:(void(^)(BOOL))callback
{
    if(self= [super initWithFrame:CGRectMake(0, 0, DeviceWidth-30, DeviceWidth-30)]){
        _user = user;
        _restaurantId = restaurantId;
        _requestCallback=callback;
        _dataAry = [[NSMutableArray alloc] init];
        _newListName = [[NSString alloc] init];
        _innerView.frame = CGRectMake(15, 0, DeviceWidth-30, DeviceWidth-30);
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 10.0;
        self.layer.borderWidth = 1.0;
        self.backgroundColor = [UIColor whiteColor];
        [self addTitle];
        [self addEixtBtn];
        [self addCreatBtnAndTextFieldView];
        [self addTableView];
        [self addSubview:_innerView];
        [ListHandler getListWithUser:_user andOffset:0 andMax:150 andBlock:^(NSArray *dataArray,int listCount,BOOL hasData) {
            
            _dataAry = [NSMutableArray arrayWithArray:dataArray];
            if (_dataAry.count)
            {
                [_dataAry removeObjectAtIndex:0];
            }
            [_tableView reloadData];
        }andFailedBlock:^{
             
             
         }];
        //[ListHandler delListWithUser:user AndListId:[NSNumber numberWithInt:131219]];
        
    }
    return self;
}

- (void)addEixtBtn
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"×" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:24];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn.frame = CGRectMake(self.frame.size.width-30, 7, 20, 20);
    btn.tag = 100;
    [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];

}
- (void)addCreatBtnAndTextFieldView
{
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, _current+17, self.frame.size.width, 18)];
    [self addSubview:_scrollView];
    _scrollView.contentSize = CGSizeMake(self.frame.size.width*2, 0);
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.pagingEnabled = YES;
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:L(@"+  Create new list") forState:UIControlStateNormal];
    btn.frame = CGRectMake(10, 0, self.frame.size.width-10, 17);
    btn.titleLabel.font = [UIFont systemFontOfSize:16];
    btn.titleLabel.textAlignment = NSTextAlignmentLeft;
    btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    [btn setTitleColor:[UIColor colorWithRed:174/255.0 green:0 blue:0 alpha:1] forState:UIControlStateNormal];
    btn.tag = 101;
    [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [_scrollView addSubview:btn];
    _current = _scrollView.endPointY;
    
    _textField = [[UITextField alloc] init];
    _textField.borderStyle = UITextBorderStyleNone;
    _textField.frame = CGRectMake(_scrollView.frame.size.width + 40, 0, self.frame.size.width-90, 17);
    _textField.returnKeyType = UIReturnKeyDone;
    _textField.font = [UIFont systemFontOfSize:13];
    _textField.delegate = self;
    [_scrollView addSubview:_textField];
    
    UILabel * lineLabel = [[UILabel alloc] init];
    lineLabel.backgroundColor = [UIColor grayColor];
    lineLabel.frame = CGRectMake(_textField.frame.origin.x, _textField.endPointY, _textField.frame.size.width, 1);
    lineLabel.alpha = 0.5f;
    [_scrollView addSubview:lineLabel];
    
    UIButton *submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    submitBtn.frame = CGRectMake(_textField.endPointX + 10, _textField.frame.origin.y, _textField.frame.size.height, _textField.frame.size.height);
    [submitBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [submitBtn setImage:[UIImage imageNamed:@"listAdd"] forState:UIControlStateNormal];
    submitBtn.tag = 102;
    [_scrollView addSubview:submitBtn];
}
- (void)btnClick:(UIButton *)btn
{
    switch (btn.tag)
    {
        case 100:
        {
            [_parentVC lew_dismissPopupViewWithanimation:nil];
        }
            break;
        case 101:
        {
            [_scrollView setContentOffset:CGPointMake(self.frame.size.width, 0) animated:YES];
            [_textField becomeFirstResponder];
        }
            break;
        case 102:
        {
            [self addListClick];
        }
            break;
        default:
            break;
    }
}
- (void)addListClick
{
    if (_newListName.length == 0)
    {
        [_textField resignFirstResponder];
        return;
    }

    
    if (![self isValidateText:_textField.text] || [_textField.text isBlankString])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:L(@"Message") message:L(@"Sorry, we are not able to process your list") delegate:nil cancelButtonTitle:L(@"OK") otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    
    [[LoadingView sharedLoadingView]startLoading];
    [ListHandler addListWithUser:_user andListName:_newListName andCityId:[[NSUserDefaults standardUserDefaults] objectForKey:@"listCityId"] andBlock:^(NSDictionary *dataDic) {
        
    
        /*
        [ListHandler getListWithUser:_user andBlock:^(NSArray *dataArray) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Success!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alertView show];
            _textField.text = @"";
            _newListName = @"";
            [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
            [_textField resignFirstResponder];
            _dataAry = [NSMutableArray arrayWithArray:dataArray];
            if (_dataAry.count)
            {
                [_dataAry removeObjectAtIndex:0];
            }                    [_tableView reloadData];
            [[LoadingView sharedLoadingView] stopLoading];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDataFromNotification) name:@"MyListCollectionViewReloadData" object:nil];
            
            
        }];
         */
        
        
        if (dataDic)
        {
            _textField.text = @"";
            _newListName = @"";
            [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
            [_textField resignFirstResponder];
            
            [ListHandler listAddRestaurantWithUser:_user andListId:[dataDic objectForKey:@"id"] andRestaurantId:_restaurantId andBlock:^(BOOL succeed,id status){
                
            }];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"MyListCollectionViewReloadData" object:nil];
            
            [_parentVC lew_dismissPopupView];
        }
        else
        {
            
        }
        
        [[LoadingView sharedLoadingView] stopLoading];


        
    }failure:^(NSString *exceptionMsg){
        
    }];
}



- (void)addTableView
{
    _tableView = [[UITableView alloc] init];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.frame = CGRectMake(0, _current+10, self.frame.size.width, self.frame.size.height-_current-10);
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:_tableView];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"PopupCell";
    PopupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(!cell){
        cell = [[[NSBundle mainBundle] loadNibNamed:@"PopupTableViewCell" owner:self options:nil] lastObject];
        IMGMyList *list = [_dataAry objectAtIndex:indexPath.row];
        cell.popupLabel.text = list.listName;
        // cell.popupImageView.image;
        /*
        if (list.listImageUrls.length >1 )
        {
            NSArray *urlArr = [list.listImageUrls componentsSeparatedByString:@","];
            NSLog(@"url = %@",[urlArr objectAtIndex:0]);
            NSString *urlString = [[urlArr objectAtIndex:0] returnFullImageUrlWithWidth:RESTAURANT_IMAGE_SIZE];
            __weak typeof(cell.popupImageView) weakRestaurantImageView = cell.popupImageView;
            [cell.popupImageView setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                [UIView animateWithDuration:1.0 animations:^{
                    [weakRestaurantImageView setAlpha:1.0];
                }];
            }];
        }
         */
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    IMGMyList *list = [_dataAry objectAtIndex:indexPath.row];
    
    [ListHandler listAddRestaurantWithUser:_user andListId:list.listId andRestaurantId:_restaurantId andBlock:^(BOOL succeed,id status){
        //self
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDataFromNotification) name:@"MyListCollectionViewReloadData" object:nil];
        if(succeed){
            if(self.requestCallback){
                self.requestCallback([status boolValue]);
            }
        }
        
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"MyListCollectionViewReloadData" object:nil];
    }];
    [_parentVC lew_dismissPopupViewWithanimation:[LewPopupViewAnimationSpring new]];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataAry.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (void)addTitle
{
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"ADD TO LISTS";
    titleLabel.font = [UIFont systemFontOfSize:15];
    titleLabel.frame = CGRectMake(0, 10, self.frame.size.width, titleLabel.font.lineHeight);
    [self addSubview:titleLabel];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    UILabel * lineLabel = [[UILabel alloc] init];
    lineLabel.backgroundColor = [UIColor grayColor];
    lineLabel.frame = CGRectMake(10, titleLabel.endPointY+10, self.frame.size.width-20, 1);
    [self addSubview:lineLabel];
    
    _current = lineLabel.endPointY;
}

+ (instancetype)defaultPopupView{
    return [[PopupView alloc]initWithFrame:CGRectMake(15, 0, DeviceWidth-30, DeviceWidth-30)];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self addListClick];
    //[_textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
{  //string就是此时输入的那个字符textField就是此时正在输入的那个输入框返回YES就是可以改变输入框的值NO相反
    
    if ([string isEqualToString:@"\n"])  //按会车可以改变
    {
        return YES;
    }
    
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    
    
    _newListName = toBeString;
    
    if ([toBeString length] > 65) { //如果输入框内容大于20则弹出警告
        textField.text = [toBeString substringToIndex:65];
        return NO;
    }
    
    return YES;
}
-(BOOL)isValidateText:(NSString *)text {
    NSString *regex = @"[a-zA-Z0-9\\s]*";
    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [test evaluateWithObject:text];
}
//
//- (IBAction)dismissAction:(id)sender{
//    [_parentVC lew_dismissPopupView];
//}
//
//- (IBAction)dismissViewFadeAction:(id)sender{
//    [_parentVC lew_dismissPopupViewWithanimation:[LewPopupViewAnimationFade new]];
//}
//
//- (IBAction)dismissViewSlideAction:(id)sender{
//    LewPopupViewAnimationSlide *animation = [[LewPopupViewAnimationSlide alloc]init];
//    animation.type = LewPopupViewAnimationSlideTypeTopBottom;
//    [_parentVC lew_dismissPopupViewWithanimation:animation];
//}
//
//- (IBAction)dismissViewSpringAction:(id)sender{
//    [_parentVC lew_dismissPopupViewWithanimation:[LewPopupViewAnimationSpring new]];
//}
//
//- (IBAction)dismissViewDropAction:(id)sender{
//    [_parentVC lew_dismissPopupViewWithanimation:[LewPopupViewAnimationDrop new]];
//}
@end
