//
//  PlaceholderView.h
//  Qraved
//
//  Created by harry on 2018/3/13.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaceholderView : UIView

@property (nonatomic, strong) IMGRestaurant *restaurant;

@end
