//
//  DetailSeeAllPhotoInstagramModel.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/11.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DetailSeeAllPhotoInstagramModel : NSObject

//@property (nonatomic, strong)  NSNumber  *commentCount;
//@property (nonatomic, strong)  NSString  *createTime;
//@property (nonatomic, strong)  NSString  *creator;
//@property (nonatomic, strong)  NSString  *mydescription;
//@property (nonatomic, strong)  NSNumber  *myid;
//@property (nonatomic, strong)  NSString  *imageUrl;
//@property (nonatomic, strong)  NSNumber  *isLike;
//@property (nonatomic, strong)  NSNumber  *likeCount;
//@property (nonatomic, strong)  NSDictionary  *photoCredit;
///*class photoCredit  photoCreditType  photoCreditUrl restaurantId  userAvatar userId userPhotoCount userReviewCount*/
//@property (nonatomic, strong)  NSNumber  *restaurantId;
//@property (nonatomic, strong)  NSString  *title;

@property (nonatomic, strong)  NSString  *approvedDate;
@property (nonatomic, strong)  NSString  *myClass;
@property (nonatomic, strong)  NSNumber  *myid;
@property (nonatomic, strong)  NSString  *instagramLink;
@property (nonatomic, strong)  NSString  *instagramUserFullName;
@property (nonatomic, strong)  NSString  *instagramUserId;
@property (nonatomic, strong)  NSString  *instagramUserName;
@property (nonatomic, strong)  NSString  *instagramUserProfilePicture;
@property (nonatomic, strong)  NSString  *lowResolutionImage;
@property (nonatomic, strong)  NSNumber  *lowResolutionImageHeight;
@property (nonatomic, strong)  NSNumber  *lowResolutionImageWidth;
@property (nonatomic, strong)  NSNumber  *restaurantId;
@property (nonatomic, strong)  NSString  *restaurantName;
@property (nonatomic, strong)  NSString  *restaurantSeoKeyword;
@property (nonatomic, strong)  NSString  *standardResolutionImage;
@property (nonatomic, strong)  NSNumber  *standardResolutionImageHeight;
@property (nonatomic, strong)  NSNumber  *standardResolutionImageWidth;
@property (nonatomic, strong)  NSString  *thumbnailImage;
@property (nonatomic, strong)  NSNumber  *thumbnailImageHeight;
@property (nonatomic, strong)  NSNumber  *thumbnailImageWidth;
@property (nonatomic, strong)  NSNumber  *type;

@end
