//
//  UIAlertController+IMGAlertController.h
//  Qraved
//
//  Created by harry on 2018/3/21.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^CallBackBlock)(NSInteger btnIndex);
@interface UIAlertController (IMGAlertController)

+ (void)showAlertCntrollerWithViewController:(UIViewController*)viewController
                        alertControllerStyle:(UIAlertControllerStyle)alertControllerStyle
                                       title:(NSString*)title
                                     message:(NSString*)message
                           cancelButtonTitle:(NSString *)cancelBtnTitle
                      destructiveButtonTitle:(NSString *)destructiveBtnTitle
                               CallBackBlock:(CallBackBlock)block;

+ (void)showFunctionAlertCntrollerWithViewController:(UIViewController*)viewController
                        alertControllerStyle:(UIAlertControllerStyle)alertControllerStyle
                                       title:(NSString*)title
                                     message:(NSString*)message
                           cancelButtonTitle:(NSString *)cancelBtnTitle
                      destructiveButtonTitle:(NSString *)destructiveBtnTitle
                               CallBackBlock:(CallBackBlock)block;
@end
