//
//  DetailButtonsView.h
//  Qraved
//
//  Created by apple on 17/3/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DetailButtonsViewDelegate <NSObject>

-(void)btnClick:(UIButton*)button;

@end

@interface DetailButtonsView : UIView
@property(nonatomic,strong)UIButton *saveBtn;
@property(nonatomic,strong)UIButton *likeBtn;
@property(nonatomic,weak)id<DetailButtonsViewDelegate> delegate;
@end
