//
//  RestaurantJournalViewCell.h
//  Qraved
//
//  Created by System Administrator on 12/30/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

@class MoreArticleViewController;

@interface RestaurantJournalViewCell : UITableViewCell

@property(nonatomic,copy) NSString *title;
@property(nonatomic,copy) NSString *contents;
@property(nonatomic,copy) NSString *imageUrl;
@property(nonatomic,copy) NSString *link;
@property(nonatomic,weak) MoreArticleViewController *delegate;

-(void)callLayoutSubviews;

@end
