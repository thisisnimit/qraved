//
//  PopupTableViewCell.h
//  Qraved
//
//  Created by Lucky on 15/4/23.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PopupTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *popupImageView;

@property (weak, nonatomic) IBOutlet UILabel *popupLabel;

@end
