//
//  DetailSeeAllPhotoQravedModel.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/11.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DetailSeeAllPhotoQravedModel : NSObject

@property (nonatomic, strong)  NSNumber  *commentCount;
@property (nonatomic, strong)  NSString  *createTime;
@property (nonatomic, strong)  NSString  *creator;
@property (nonatomic, strong)  NSString  *mydescription;
@property (nonatomic, strong)  NSNumber  *myid;
@property (nonatomic, strong)  NSString  *imageUrl;
@property (nonatomic, strong)  NSNumber  *isLike;
@property (nonatomic, strong)  NSNumber  *likeCount;
@property (nonatomic, strong)  NSDictionary  *photoCredit;
/*class photoCredit  photoCreditType  photoCreditUrl restaurantId  userAvatar userId userPhotoCount userReviewCount*/
@property (nonatomic, strong)  NSNumber  *restaurantId;
@property (nonatomic, strong)  NSString  *title;


@end
