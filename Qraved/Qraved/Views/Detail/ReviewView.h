//
//  ReviewView.h
//  Qraved
//
//  Created by Sean Liao on 9/11/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGReview.h"
#import "LikeView.h"

@class ReviewView,IMGUserReview;

@protocol reviewDelegate <NSObject>
@optional
-(void)reviewImagePressed:(ReviewView*)review images:(NSArray*)images withTag:(int)tag;
-(void)reviewUserNameOrImageTapped:(id)sender;
-(void)reviewViewClick:(IMGReview *)review;
- (void)userReviewMoreBtnClickWithIMGUserReview:(IMGUserReview*)review;
- (void)reviewViewMsgClick:(ReviewView *)reviewView readMoreTapped:(id)review isReadMore:(BOOL)isReadMore;
-(void)previousLableTap:(IMGReview*)review_;
- (void)shareButtonTapped:(IMGReview *)reviewView shareButton:(UIButton *)button;
- (void)gotoJournalDetail:(NSNumber *)journalId;

@end

@interface ReviewView : UIView<LikeListViewDelegate>

@property(nonatomic, retain) IMGReview *review;
@property(nonatomic, retain) NSMutableArray *reviewImages; 
@property(nonatomic,weak)id<reviewDelegate>delegate;
@property(nonatomic,assign)CGFloat reviewHeight;
@property(nonatomic,assign)BOOL ATReviewsVC;
@property (nonatomic, strong) id controller;
-(id)initWithReview:(IMGReview *)review andDish:dishArray withDelegate:(id<reviewDelegate>)delegate fromDetail:(BOOL)isDetail andIsOtherUser:(BOOL)isOtherUser andIsreadMore:(BOOL)isReadMore andIsFromUserallReviewPage:(BOOL)isFromUserallReviewPage andIsNeedReadMore:(BOOL)isNeedReadMore;

-(NSArray*)getReviewImages:(NSUInteger)reviewId;

@end
