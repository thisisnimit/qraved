//
//  CuisineView.m
//  Qraved
//
//  Created by Admin on 8/5/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "CuisineView.h"
#import "AppDelegate.h"
#import "UIImageView+Cache.h"
#import "Label.h"

@interface CuisineView ()

@property (nonatomic ,weak) UIButton *touchBtn;
@property (nonatomic ,strong) UIImageView *cuisineImage;
@property (nonatomic ,strong) UIImageView *selecteView;

@end

@implementation CuisineView

@synthesize cuisineID = _cuisineID;
@synthesize cuisineName = _cuisineName;
@synthesize dishID = _dishID;
@synthesize imgUrl = _imgUrl;
@synthesize isQraved;
@synthesize fromHome;


- (id)initWithFrame:(CGRect)frame andInfoArray:(NSArray *)cuisineArray
{
    return [self initWithFrame:frame andInfoArray:cuisineArray isFromHome:NO];
}

- (id)initWithFrame:(CGRect)frame andInfoArray:(NSArray *)cuisineArray  isFromHome:(BOOL)isFromHome
{
    self = [super initWithFrame:frame];
    if (self) {
        self.fromHome = isFromHome;
        self.cuisineID = [cuisineArray objectAtIndex:0];
        self.cuisineName = [cuisineArray objectAtIndex:1];
        self.dishID = [cuisineArray objectAtIndex:4];
        self.imgUrl=[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:[cuisineArray objectAtIndex:3]];
        if([[cuisineArray objectAtIndex:5] isEqualToString:@"1"]){
            self.isQraved = YES;
        }else{
            self.isQraved = NO;
        }
        
        [self setCuisineView];
    }
    return self;
}

-(void)setCuisineView {
    self.cuisineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    self.cuisineImage.image = [UIImage imageNamed:@"placeholder.png"];
    [self addSubview:self.cuisineImage];
    
    UIView *titleBackground = [[UIView alloc]initWithFrame:CGRectMake(0, self.frame.size.height - 20, self.frame.size.width, 20)];
    titleBackground.backgroundColor = [UIColor blackColor];
    titleBackground.alpha = 0.5;
    [self addSubview:titleBackground];
    
    UILabel *titleLabel = [[Label alloc] initCuisineTitleLabel];
    titleLabel.frame = CGRectMake(3, self.frame.size.height - 18, self.frame.size.width, 16);
    //titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text = self.cuisineName;
    //titleLabel.textColor = [UIColor whiteColor];
    [self addSubview:titleLabel];
    
    self.touchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.touchBtn.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    self.touchBtn.backgroundColor = [UIColor clearColor];
    [self addSubview:self.touchBtn];
    [self.touchBtn addTarget:self action:@selector(btnTouched:) forControlEvents:UIControlEventTouchDown];
    if(!fromHome){
        if (isQraved) {
//            self.selecteView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"qrave_btn_press"]];
            self.touchBtn.selected = YES;
        }else{
//            self.selecteView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"qrave_btn"]];
            self.touchBtn.selected = NO;
        }
        self.selecteView.frame = CGRectMake(self.frame.size.width - 29, 4, 25, 25);
        [self addSubview:self.selecteView];
    }
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",self.imgUrl,@"&width=320"];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self.cuisineImage setImageUrl:urlString];
    });
}


-(void)unselectedPostToServer {
    if([AppDelegate ShareApp].isLogin==NO){
        [[AppDelegate ShareApp] goToLoginController];
    }
    else
    {
    NSString *userId =  [User sharedUser].userID;
    NSString *Facebktoken = [User sharedUser].token;
    NSString *urlString = [NSString stringWithFormat:@"%@unqrave?userID=%@&t=%@&dishID=%@",QRAVED_WEB_SERVICE_SERVER,userId,Facebktoken,self.dishID];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];
    [request setHTTPMethod:@"POST"];
    [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    }
//    NSString *str1 = [[NSString alloc]initWithData:received encoding:NSUTF8StringEncoding];

}

-(void)selectedPostToServer {
    if([AppDelegate ShareApp].isLogin==NO){
        [[AppDelegate ShareApp] goToLoginController];
    }
    else
    {
    NSString *userId =  [User sharedUser].userID;
    NSString *Facebktoken = [User sharedUser].token;
    NSString *urlString = [NSString stringWithFormat:@"%@qrave?userID=%@&t=%@&dishID=%@",QRAVED_WEB_SERVICE_SERVER,userId,Facebktoken,self.dishID];
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];
    [request setHTTPMethod:@"POST"];
    [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    }
//    NSString *str1 = [[NSString alloc]initWithData:received encoding:NSUTF8StringEncoding];

}

-(void)btnTouched:(UIButton *)sender {
    if (!fromHome) {
        BOOL isSelected = sender.selected;
        if (isSelected == YES) {
            self.selecteView.frame = CGRectMake(self.frame.size.width - 29, 4,25, 25);
            self.selecteView.image = [UIImage imageNamed:@"qrave_btn"];
            sender.selected = NO;
            
            [NSThread detachNewThreadSelector:@selector(unselectedPostToServer) toTarget:self withObject:nil];
        } else {
            self.selecteView.frame = CGRectMake(self.frame.size.width - 29, 4,25, 25);
            self.selecteView.image = [UIImage imageNamed:@"qrave_btn_press.png"];
            sender.selected = YES;
            
            [NSThread detachNewThreadSelector:@selector(selectedPostToServer) toTarget:self withObject:nil];
        }
    } else {
        NSDictionary *tag = [[NSDictionary alloc] initWithObjectsAndKeys:self.cuisineID,@"id",self.cuisineName,@"name", nil];
        NSMutableDictionary *tagDictionary = [[NSMutableDictionary alloc] init];
        NSArray *tagArray = [[NSArray alloc] initWithObjects: tag,nil];
        [tagDictionary setObject:tagArray forKey:@"Cuisine"];

    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
