//
//  EventsAndDiningGuideViewCell.h
//  Qraved
//
//  Created by System Administrator on 1/13/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsAndDiningGuideViewCell : UITableViewCell

@property(nonatomic,copy) NSString *title;
@property(nonatomic,copy) NSString *image;
@property(nonatomic,assign,setter=isEvent:) BOOL isEvent;

@end
