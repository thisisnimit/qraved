//
//  DetailReviewView.m
//  Qraved
//
//  Created by apple on 17/3/23.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "DetailReviewView.h"
#import "Label.h"
#import "UIConstants.h"
#import "UIColor+Helper.h"
#import "UILabel+Helper.h"
#import "UIView+Helper.h"
#import "UIColor+Hex.h"
#import "UIDevice+Util.h"
#import "DLStarRatingControl.h"
#import "NSNumber+Helper.h"
#import "IMGUser.h"
#import "IMGReview.h"
#import "IMGReviewComment.h"
#import "IMGDish.h"
#import "LoadingView.h"
#import "ReviewHandler.h"
#import "LikeView.h"
#import "HomeUtil.h"
#import "ReviewInstagramView.h"
@interface DetailReviewView ()<LikeListViewDelegate>{
    LikeView* likepopView;
    NSMutableArray* likelistArr;
    IMGReview *currentReview;
}

@end

@implementation DetailReviewView

-(instancetype)initWithRestaurant:(IMGRestaurant*)restaurant andNewReviewArrM:(NSMutableArray*)newReviewArrM andReviewDataArr:(NSMutableArray*)reviewDataArr andDishdic:(NSMutableDictionary*)dishDic andCommentReadMore:(NSMutableArray*)commentReadMore andReveiwCardIsReadMore:(NSMutableDictionary *)reveiwCardIsReadMore andScoreBtnTop:(UIButton *)scoreBtnTop andReviewedUserCount:(NSNumber*)reviewedUserCount andDelegate:(id)delegate andController:(id)controller{
    if (self = [super init]) {
       
        self.reviewHeight = [[NSMutableArray alloc]init];
        
        self.backgroundColor = [UIColor clearColor];
        
        Label *titleLabel=[[Label alloc] initWithFrame:CGRectZero andTextFont:[UIFont boldSystemFontOfSize:14] andTextColor:[UIColor color333333] 
                                          andTextLines:1];
        [titleLabel setText:L(@"Reviews")];
        [titleLabel setFrame:CGRectMake(LEFTLEFTSET, 16, titleLabel.expectedWidth, titleLabel.font.lineHeight)];
        [self addSubview:titleLabel];
        
        UIButton *btnSeeAll = [UIButton buttonWithType:UIButtonTypeCustom];
        btnSeeAll.frame = CGRectMake(DeviceWidth - 55 - 16, 16, 40, 20);
        [btnSeeAll setTitle:@"See All" forState:UIControlStateNormal];
        [btnSeeAll setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
        btnSeeAll.titleLabel.font = [UIFont systemFontOfSize:12];
        [btnSeeAll addTarget:self action:@selector(gotoReviews:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btnSeeAll];
        
        UIImageView *ArrowImage = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth - 20, 0, 6, 10)];
        ArrowImage.centerY = btnSeeAll.centerY;
        ArrowImage.image = [UIImage imageNamed:@"ic_home_arrow"];
        [self addSubview:ArrowImage];

        
        CGFloat currentPointY = titleLabel.endPointY;
        
        UILabel *scoreLabel = [[UILabel alloc] init];
        
        [scoreLabel setFont:[UIFont systemFontOfSize:50]];
        float ratingScore = [restaurant.ratingScore floatValue];
        scoreLabel.text = [NSString stringWithFormat:@"%.1f",ratingScore/2];
        [scoreBtnTop setTitle:scoreLabel.text forState:UIControlStateNormal];
        scoreLabel.frame = CGRectMake(DeviceWidth-LEFTLEFTSET*2 - scoreLabel.expectedWidth, currentPointY+5, scoreLabel.expectedWidth, scoreLabel.font.lineHeight);
        scoreLabel.textColor = [UIColor colorWithRed:241/255.0f green:92/255.0f blue:79/255.0f alpha:1.0f];
        [self addSubview:scoreLabel];
        
        float overallScoreOffset = 0.0;
        float yellowLabelOffset = 0.0;
        if ([UIDevice isIphone6Plus]) {
            overallScoreOffset = 40.0;
            yellowLabelOffset = 30.0;
        }
        if ([UIDevice isIphone6]) {
            overallScoreOffset = 30.0;
            yellowLabelOffset = 20.0;
        }
        
        DLStarRatingControl *overallScore=[[DLStarRatingControl alloc]initWithFrame:CGRectMake(DeviceWidth - 100 - LEFTLEFTSET, scoreLabel.endPointY + 4, 100, 15) andStars:5 andStarWidth:15 isFractional:YES spaceWidth:5];
        overallScore.userInteractionEnabled=NO;
        overallScore.rating= [[NSNumber numberWithDouble:[restaurant.ratingScore doubleValue]] getRatingNumber];
        [self addSubview:overallScore];
        
        NSNumber *ratingCounts = restaurant.ratingCount;
        
        UILabel *reviewsStrLabel = [[UILabel alloc] init];
        reviewsStrLabel.text = [NSString stringWithFormat:@"%@ ",([ratingCounts intValue] >1)?L(@"Reviews"):L(@"Review")];
        reviewsStrLabel.font = [UIFont systemFontOfSize:12];
        reviewsStrLabel.textColor = [UIColor color999999];
        reviewsStrLabel.frame = CGRectMake(DeviceWidth - LEFTLEFTSET*2 -  reviewsStrLabel.expectedWidth, overallScore.endPointY+4, reviewsStrLabel.expectedWidth, reviewsStrLabel.font.lineHeight);
        [self addSubview:reviewsStrLabel];
        
        NSArray *reviewListArr = [restaurant.ratingCounts componentsSeparatedByString:@","];
        float max = 0;
        for (NSString *str in reviewListArr)
        {
            if (max < [str floatValue])
            {
                max = [str floatValue];
            }
        }
        if (max == 0)
        {
            reviewListArr = [[NSArray alloc] init];
        }
        float AVG = 10/max*8;
        
        for (int i = 0; i<reviewListArr.count; i++)
        {
            NSArray *colorArray = @[[UIColor colorWithRed:226/255.0f green:51/255.0f blue:53/255.0f alpha:1.0f],[UIColor colorWithRed:241/255.0f green:92/255.0f blue:79/255.0f alpha:1.0f],[UIColor colorWithRed:255/255.0f green:146/255.0f blue:46/255.0f alpha:1.0f],[UIColor colorWithRed:254/255.0f green:192/255.0f blue:17/255.0f alpha:1.0f],[UIColor colorWithRed:242/255.0f green:189/255.0f blue:121/255.0f alpha:1.0f],];
            NSNumber *newRating = [NSNumber numberWithFloat:([[reviewListArr objectAtIndex:reviewListArr.count-i-1] floatValue] * AVG)];
            
            UIView *backLabel = [[UIView alloc] init];
            backLabel.frame = CGRectMake(LEFTLEFTSET +26, titleLabel.endPointY+15  + 20*i, 150, 15);
            backLabel.backgroundColor = [UIColor colorWithRed:229/255.0f green:230/255.0f blue:232/255.0f alpha:1.0f];;
            [self addSubview:backLabel];
            
            UILabel *yellowLabel = [[UILabel alloc] init];
            yellowLabel.frame = CGRectMake(LEFTLEFTSET +26, titleLabel.endPointY+15  + 20*i, [newRating intValue], 15);
            yellowLabel.alpha = (reviewListArr.count - i)*0.2;
            yellowLabel.backgroundColor = colorArray[i];
            [self addSubview:yellowLabel];
            
            UILabel *numLabel = [[UILabel alloc] init];
            numLabel.frame = CGRectMake(LEFTLEFTSET, yellowLabel.frame.origin.y, 10, 15);
            numLabel.text = [NSString stringWithFormat:@"%d",5-i];
            numLabel.textColor = [UIColor color999999];
            numLabel.font = [UIFont systemFontOfSize:14];
            [self addSubview:numLabel];
            
            UIImageView * starImage = [UIImageView new];
            starImage.image = [UIImage imageNamed:@"ic_star_empty"];
            starImage.frame = CGRectMake(numLabel.right +2, yellowLabel.frame.origin.y, 12, 12);
            [self addSubview:starImage];
            
            UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:backLabel.bounds byRoundingCorners:UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii:CGSizeMake(7.5, 7.5)];
            CAShapeLayer *layer = [[CAShapeLayer alloc]init];
            layer.frame = backLabel.bounds;
            layer.path = maskPath.CGPath;
            
            backLabel.layer.mask = layer;
            
            
            UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:yellowLabel.bounds byRoundingCorners:UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii:CGSizeMake(7.5, 7.5)];
            CAShapeLayer *layer1 = [[CAShapeLayer alloc]init];
            layer1.frame = yellowLabel.bounds;
            layer1.path = maskPath1.CGPath;
            
            yellowLabel.layer.mask = layer1;
        }
        
        UILabel *reviewsCountLable = [[UILabel alloc] init];
        reviewsCountLable.text = [NSString stringWithFormat:@"%@",[ratingCounts intValue] == 0?@"1":ratingCounts];
        reviewsCountLable.font = [UIFont systemFontOfSize:15];
        reviewsCountLable.textColor = [UIColor color999999];
        reviewsCountLable.frame = CGRectMake(reviewsStrLabel.frame.origin.x - reviewsCountLable.expectedWidth - 3, reviewsStrLabel.frame.origin.y, reviewsStrLabel.expectedWidth, reviewsStrLabel.font.lineHeight);
        [self addSubview:reviewsCountLable];
        
        currentPointY = reviewsStrLabel.endPointY +22;
        
        IMGUser * user =[IMGUser currentUser];
        
        self.reviewsArray = [NSMutableArray array];
        
        for (id dic in reviewDataArr)
        {
            if ([dic isKindOfClass:[NSDictionary class]]) {
                if ([[dic objectForKey:@"id"] isKindOfClass:[NSNull class]]) {
                    break;
                }
                IMGReview *review = [[IMGReview alloc] init];
                [review setValuesForKeysWithDictionary:dic];
                if ([[dic objectForKey:@"reviewType"] isEqual:@1]) {
                    review.restaurantTitle = restaurant.title;
                    review.summarize = [review.summarize stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    review.userDishCount = dic[@"userPhotoCount"];
                    review.commentList = [IMGReviewComment commentListFromArray:dic[@"commentList"] andIsVendor:NO andRestaurant:restaurant];
                    
                    NSMutableArray *dishArray=[[NSMutableArray alloc] init];
                    for(id dishdic in [dic objectForKey:@"dishList"]){
                        IMGDish *dish=[[IMGDish alloc] init];
                        [dish setValuesForKeysWithDictionary:dishdic];
                        dish.descriptionStr=[dishdic objectForKey:@"description"];
                        [dish updatePhotoCredit:[dishdic objectForKey:@"photoCredit"]];
                        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
                        [formatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
                        NSDate *createTime=[formatter dateFromString:[NSString stringWithFormat:@"%@",dish.createTime]];
                        dish.createTime=[NSNumber numberWithDouble:createTime.timeIntervalSince1970];
                        [dishArray addObject:dish];
                        [dishDic setObject:dishArray forKey:[dic objectForKey:@"id"]];
                    }
                }else{
                    review.reviewId = [dic objectForKey:@"postId"];
                    review.timeCreated = [dic objectForKey:@"createTime"];
                    NSMutableArray *dishArray=[[NSMutableArray alloc] init];
                    for(id dishdic in [dic objectForKey:@"photoList"]){
                        IMGDish *dish=[[IMGDish alloc] init];
                        [dish setValuesForKeysWithDictionary:dishdic];
                        dish.dishId = [dishdic objectForKey:@"instagramPhotoId"];
                        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
                        [formatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
                        NSDate *createTime=[formatter dateFromString:[NSString stringWithFormat:@"%@",dish.createTime]];
                        dish.createTime=[NSNumber numberWithDouble:createTime.timeIntervalSince1970];
                        dish.instagramLink = [dic objectForKey:@"link"];
                        [dishArray addObject:dish];
                        [dishDic setObject:dishArray forKey:[dic objectForKey:@"postId"]];
                    }
                }
                
                [self.reviewsArray addObject:review];
               
            }
            
        }
        for (int i = 0; i < self.reviewsArray.count; i ++) {
            IMGReview *review = [self.reviewsArray objectAtIndex:i];
            BOOL isOtherUser = NO;
            if ([review.reviewType isEqual:@1]) {
                if ([review.userId intValue] == [user.userId intValue]){
                    isOtherUser = NO;
                }else{
                    isOtherUser = YES;
                }
                
                ReviewView *reviewView = [[ReviewView alloc] initWithReview:review andDish:[dishDic objectForKey:review.reviewId] withDelegate:delegate fromDetail:YES andIsOtherUser:isOtherUser andIsreadMore:(BOOL)[reveiwCardIsReadMore objectForKey:review.targetId] andIsFromUserallReviewPage:NO andIsNeedReadMore:YES];
                reviewView.controller = controller;
                reviewView.frame = CGRectMake(0, currentPointY,DeviceWidth,reviewView.reviewHeight);
                [self addSubview:reviewView];
                [self setFrame:CGRectMake(0, currentPointY+6, DeviceWidth, reviewView.endPointY+15)];
                
                currentPointY = reviewView.endPointY;
                
            }else{
                ReviewInstagramView *reviewInstagramView = [[ReviewInstagramView alloc] initWithReview:review andDish:[dishDic objectForKey:review.reviewId] withDelegate:delegate];
                reviewInstagramView.frame = CGRectMake(0, currentPointY, DeviceWidth, reviewInstagramView.reviewHeight);
                [self addSubview:reviewInstagramView];
                [self setFrame:CGRectMake(0, currentPointY+6, DeviceWidth, reviewInstagramView.endPointY+15)];
                
                currentPointY = reviewInstagramView.endPointY;
            }
        }
        
    
        if ([reviewedUserCount intValue] > 2)
        {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = CGRectMake(0, currentPointY, DeviceWidth, 40);
            btn.backgroundColor = [UIColor clearColor];
            
            UILabel *btnLabel = [[UILabel alloc] init];
            btnLabel.text = L(@"See all Reviews  >");
            btnLabel.textColor = [UIColor colorWithHexString:@"#333333"];
            btnLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12];
            btnLabel.frame = CGRectMake(DeviceWidth-15-btnLabel.expectedWidth, 15, btnLabel.expectedWidth, btnLabel.font.lineHeight);
            [btn addTarget:self action:@selector(gotoReviews:) forControlEvents:UIControlEventTouchUpInside];
            currentPointY = btn.endPointY;
        }
        
        self.currentPointY = currentPointY;
    }
    return self;
}
-(void)gotoReviews:(id)sender{
    if ([self.delegate respondsToSelector:@selector(gotoReview:)]) {
        [self.delegate gotoReview:sender];
    }
}

@end
