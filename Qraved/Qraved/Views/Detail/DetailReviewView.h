//
//  DetailReviewView.h
//  Qraved
//
//  Created by apple on 17/3/23.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"
#import "ReviewView.h"

@protocol DetailReviewViewDelegate <NSObject>

-(void)gotoReview:(id)sender;


@end
@interface DetailReviewView : UIView

@property (nonatomic,assign) float currentPointY;

@property (nonatomic,strong)NSMutableArray* reviewsArray;
@property (nonatomic,strong)NSMutableArray* reviewHeight;
@property (nonatomic,weak)id<DetailReviewViewDelegate>delegate;


-(instancetype)initWithRestaurant:(IMGRestaurant*)restaurant andNewReviewArrM:(NSMutableArray*)newReviewArrM andReviewDataArr:(NSMutableArray*)reviewDataArr andDishdic:(NSMutableDictionary*)dishDic andCommentReadMore:(NSMutableArray*)commentReadMore andReveiwCardIsReadMore:(NSMutableDictionary *)reveiwCardIsReadMore andScoreBtnTop:(UIButton *)scoreBtnTop andReviewedUserCount:(NSNumber*)reviewedUserCount andDelegate:(id)delegate andController:(id)controller;

@end
