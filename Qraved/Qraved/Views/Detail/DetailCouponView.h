//
//  DetailCouponView.h
//  Qraved
//
//  Created by harry on 2018/1/30.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CouponModel.h"

@interface DetailCouponView : UIView<UIScrollViewDelegate>

@property (nonatomic, strong) NSArray *couponArray;
@property (nonatomic, copy) void (^couponImageTapped)();

@end
