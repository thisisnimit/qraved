//
//  ReviewInstagramView.h
//  Qraved
//
//  Created by harry on 2017/11/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGReview.h"
@class ReviewInstagramView;

@protocol ReviewInstagramViewDelegate <NSObject>
@optional
-(void)instagramImagePressed:(IMGReview*)review images:(NSArray*)images withTag:(int)tag;

@end

@interface ReviewInstagramView : UIView
@property(nonatomic,assign)CGFloat reviewHeight;
@property (nonatomic, assign) id <ReviewInstagramViewDelegate>delegate;

-(id)initWithReview:(IMGReview *)review andDish:(NSArray *)dishArray withDelegate:(id<ReviewInstagramViewDelegate>)delegate;

@end
