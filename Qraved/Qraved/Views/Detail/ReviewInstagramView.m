//
//  ReviewInstagramView.m
//  Qraved
//
//  Created by harry on 2017/11/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "ReviewInstagramView.h"
#import "IMGDish.h"

@interface ReviewInstagramView()<UIWebViewDelegate>
@property (nonatomic, strong) IMGReview *review;
@end

@implementation ReviewInstagramView
{
    UIWebView *reviewSummarizeLabel;
    NSArray *reviewImages;
}
-(id)initWithReview:(IMGReview *)review andDish:(NSArray *)dishArray withDelegate:(id<ReviewInstagramViewDelegate>)delegate{
    self = [super init];
    if (self) {
        self.review = review;
        self.delegate=delegate;
        reviewImages = [NSArray arrayWithArray:dishArray];
        CGFloat currentPointY= 0.0;
        UIButton *avatarLinkButton=[UIButton buttonWithType:UIButtonTypeCustom];
        avatarLinkButton.frame = CGRectMake(LEFTLEFTSET, TOPOFFSET, 40, 40);
        [avatarLinkButton addTarget:self action:@selector(controlTapped) forControlEvents:UIControlEventTouchUpInside];
        
        NSURL *urlString;
        review.userPicture = [review.userPicture stringByReplacingPercentEscapesUsingEncoding:
                              NSUTF8StringEncoding];
        
        if ([review.userPicture hasPrefix:@"http"])
        {
            urlString = [NSURL URLWithString:review.userPicture];
        }
        else
        {
            urlString = [NSURL URLWithString:[review.userPicture returnFullImageUrl]];
        }
        
        __weak typeof(avatarLinkButton) weakAvatarLinkButton = avatarLinkButton;
        [avatarLinkButton sd_setImageWithURL:urlString forState:UIControlStateNormal placeholderImage:[[UIImage imageNamed:@"headDefault.jpg"] cutCircleImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image.size.height>0&&image.size.width>0) {
                image = [image cutCircleImage];
                [weakAvatarLinkButton setImage:image forState:UIControlStateNormal];
            }
        }];
        [self addSubview:avatarLinkButton];
        
        currentPointY = avatarLinkButton.frame.origin.y;
        
        
        UILabel *nameLabel = [[UILabel alloc]init];
        nameLabel.text = [NSString stringWithFormat:@"%@",review.userName];
        nameLabel.textColor = [UIColor color333333];
        nameLabel.font = [UIFont systemFontOfSize:15];
        nameLabel.frame = CGRectMake(avatarLinkButton.frame.size.width+20,TOPOFFSET, nameLabel.expectedWidth, nameLabel.font.lineHeight);
        [self addSubview:nameLabel];
        
        nameLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap_nameLabel = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(controlTapped)];
        [nameLabel addGestureRecognizer:tap_nameLabel];
        
        UILabel *lblInstagram = [[UILabel alloc]initWithFrame:CGRectMake(avatarLinkButton.frame.size.width+20, nameLabel.endPointY+3, 100, 14)];
        lblInstagram.text = @"in Instagram";
        lblInstagram.font = [UIFont systemFontOfSize:12];
        lblInstagram.textColor = [UIColor color999999];
        lblInstagram.alpha = 0.7f;
        [self addSubview:lblInstagram];
        
        
        UILabel *lblTime = [[Label alloc]init];
        lblTime.text = [Date getTimeInteval_v10:[review.timeCreated longLongValue]/1000];
        lblTime.textColor = [UIColor color999999];
        lblTime.alpha = 0.7f;
        lblTime.font = [UIFont systemFontOfSize:12];
        lblTime.frame = CGRectMake(DeviceWidth - 150 - 15, nameLabel.endPointY+3, 150, 14);
        lblTime.textAlignment = NSTextAlignmentRight;
        [self addSubview:lblTime];
        
        
        reviewSummarizeLabel = [[UIWebView alloc] initWithFrame:CGRectMake(LEFTLEFTSET, lblInstagram.endPointY+15, DeviceWidth - LEFTLEFTSET*2, 40)];
        reviewSummarizeLabel.tag=711;
        reviewSummarizeLabel.delegate = self;
        reviewSummarizeLabel.scrollView.bounces = NO;
        reviewSummarizeLabel.backgroundColor=[UIColor whiteColor];
        reviewSummarizeLabel.scrollView.showsHorizontalScrollIndicator = NO;
        reviewSummarizeLabel.scrollView.scrollEnabled = YES;
        reviewSummarizeLabel.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
         NSString *captionStr = [NSString returnStringWithTag:review.caption];
        float contentLabelHeight = [ReviewInstagramView calculateCommentTextHeight:review.caption];;
        reviewSummarizeLabel.frame=CGRectMake(LEFTLEFTSET, lblInstagram.endPointY+15, DeviceWidth - LEFTLEFTSET*2, contentLabelHeight);
        
        NSString *summarizeStr = [NSString stringWithFormat:@"<html> <head> <meta charset=\"UTF-8\">"
                                  "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">"
                                  "<style type=\"text/css\">"
                                  "html {"
                                  "    -webkit-text-size-adjust: none; /* Never autoresize text */"
                                  "}"
                                  "body{font-family:\"%@\";font-size: 12px; color:%@; padding:0; margin:0; width:%fpx; line-height: 15px;}"
                                  "</style>"
                                  "</head>"
                                  "<body><div id=\"content\">%@</div></body>"
                                  "</html>",DEFAULT_FONT_NAME,@"#999999",DeviceWidth - LEFTLEFTSET*2,captionStr];
        
        summarizeStr = [summarizeStr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
        [reviewSummarizeLabel loadHTMLString:summarizeStr baseURL:nil];
        
        
        [reviewSummarizeLabel layoutSubviews];
        [self addSubview:reviewSummarizeLabel];
        currentPointY = reviewSummarizeLabel.endPointY;
        
        
        UIView *photosView = [[UIView alloc] init];
        
        photosView.frame = CGRectMake(LEFTLEFTSET, currentPointY + 5, DeviceWidth - 2*LEFTLEFTSET, 0);
        
        CGFloat dishImageWidth;
        CGFloat dishImageEndX;
        int viewFrameY = 0;
        int viewFrameX = 0;
        for (int i = 0; i<dishArray.count; i++)
        {
            IMGDish *dish=(IMGDish*)[dishArray objectAtIndex:i];
            UIImageView *dishImageView = [[UIImageView alloc]init];
            dishImageWidth = (photosView.frame.size.width-10)/3;
            dishImageView.userInteractionEnabled=YES;
            dishImageView.contentMode = UIViewContentModeScaleAspectFill;
            dishImageView.clipsToBounds = YES;
            if (viewFrameX == 3)
            {
                viewFrameY = dishImageWidth + 5 + viewFrameY;
                viewFrameX = 0;
            }
            
            dishImageView.frame = CGRectMake((dishImageWidth+5)*viewFrameX, viewFrameY, dishImageWidth, dishImageWidth);
            viewFrameX++;
            dishImageView.tag = i;
            [photosView addSubview:dishImageView];
            
            
            if (dishArray.count == 1) {
                
                dishImageView.frame = CGRectMake(0, viewFrameY, DeviceWidth, DeviceWidth/4*3);
            }
            
            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagePressed:)];
            [dishImageView addGestureRecognizer:tap];
            
            dishImageEndX = dishImageView.endPointX;
            // && isDetail
            if (i == 5 && dishArray.count > 6)
            {
                dishImageView.backgroundColor = [UIColor colorWithRed:222/250.0 green:222/250.0 blue:222/250.0 alpha:1];
                UILabel *otherCountLabel = [[UILabel alloc] init];
                otherCountLabel.tag=i;
                otherCountLabel.userInteractionEnabled=YES;
                otherCountLabel.text = [NSString stringWithFormat:@"+%lu",dishArray.count - 5];
                otherCountLabel.font = [UIFont systemFontOfSize:17];
                otherCountLabel.textColor = [UIColor color333333];
                otherCountLabel.frame = CGRectMake(dishImageView.frame.size.width/2 - 20, dishImageView.frame.size.height/2 - 20, 40, 40);
                [otherCountLabel addGestureRecognizer:tap];
                [dishImageView addSubview:otherCountLabel];
                break;
            }
            else
            {
                NSString *urlString = dish.lowResolutionImage;
                __weak typeof(dishImageView) weakRestaurantImageView = dishImageView;
                
                [dishImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    [UIView animateWithDuration:1.0 animations:^{
                        [weakRestaurantImageView setAlpha:1.0];
                    }];
                }];
            }
            
            
            photosView.frame = CGRectMake(LEFTLEFTSET, currentPointY + 5, DeviceWidth - 2*LEFTLEFTSET, dishImageView.endPointY);
            if (dishArray.count == 1) {
                photosView.frame = CGRectMake(0, currentPointY + 5, DeviceWidth, dishImageView.endPointY);
                
            }
        }
        
        
        [self addSubview:photosView];
        currentPointY = photosView.endPointY;
        self.reviewHeight = currentPointY;
    }
    
    return self;
}

- (void)controlTapped{
    NSString *urlStr = @"https://www.instagram.com/";
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",urlStr,self.review.userName]];
    [[UIApplication sharedApplication] openURL:url];
}

-(void)imagePressed:(UIGestureRecognizer*)gesture{
    if([self.delegate respondsToSelector:@selector(reviewImagePressed:images:withTag:)]){
        [self.delegate instagramImagePressed:self.review images:reviewImages withTag:(int)gesture.view.tag];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none';"];
    [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitTouchCallout='none';"];
    UIScrollView *tempView = (UIScrollView *)[reviewSummarizeLabel.subviews objectAtIndex:0];
    
    tempView.scrollEnabled = NO; 
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if (navigationType==UIWebViewNavigationTypeLinkClicked) {
        NSURL *url = [NSURL URLWithString:self.review.link];
        [[UIApplication sharedApplication] openURL:url];
        return NO;
    }
    
    return YES;
}

+(CGFloat)calculateCommentTextHeight:(NSString*)commentStr_{
    
    commentStr_=[commentStr_ stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12]};
    
    CGSize textSize = [commentStr_ boundingRectWithSize:CGSizeMake(DeviceWidth-30, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    
    float height = ceil(textSize.height + 1.5f);
    
    NSLog(@"height = %@",commentStr_);
    
    return height+15;
}


@end
