//
//  DetailSeeMenuViewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/11.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "DetailSeeMenuViewController.h"
#import "DetailSeeMenuRestaurantViewController.h"
#import "DetailSeeMenuBarViewController.h"
#import "DetailSeeMenDeliveryViewController.h"
#import "DetailDataHandler.h"
CGFloat const TitlesViewH = 40;

CGFloat const TitlesViewY = 64;

@interface DetailSeeMenuViewController ()<UIScrollViewDelegate>
{
    IMGRestaurant *restaurant;
    NSMutableArray *restaurantPhotoArray;
    NSNumber    *  RestaurantMenuCount;
    NSNumber *BarMenuCount;
    NSNumber     *DeliveryMenuCount;
    int offset;
}
/**标签栏底部的红色指示器 */
@property  (nonatomic, weak)  UIView       *indicatorView;
/**当前选中的按钮 */
@property  (nonatomic, weak)  UIButton     *selectedButton;
/**顶部所有标签 */
@property  (nonatomic,  weak) UIView       *titlesView;
/**底部所有内容*/
@property  (nonatomic, weak)  UIScrollView *contentView;

@end

@implementation DetailSeeMenuViewController
-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant andIsSelfUpload:(BOOL)isSelfUpload{
    self = [super init];
    if (self != nil) {
        restaurant = paramRestaurant;
        self.isSelfUpload = isSelfUpload;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=NO;
    [super viewWillAppear:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Menu";
    
//    [self initData];
    [self setNavi];
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self addBackBtn];
    UIBarButtonItem *searchBtn=[[UIBarButtonItem alloc] initWithTitle:L(@"Save") style:UIBarButtonItemStylePlain target:self action:@selector(SaveButtonClick:)];
    searchBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=searchBtn;

    [self initData];
    
    offset = 0;
}
-(void)initData{

    NSDictionary *dict=@{@"restaurantId":[NSString stringWithFormat:@"%@",restaurant.restaurantId],@"max":@"15",@"offset":[NSString stringWithFormat:@"%d",offset]};
    [DetailDataHandler SeeMenuRestaurant:dict andSuccessBlock:^(NSMutableArray *array,NSNumber *restaurantMenuCount, NSNumber *barMenuCount, NSNumber *deliveryMenuCount) {
        RestaurantMenuCount = restaurantMenuCount;
        BarMenuCount = barMenuCount;
        DeliveryMenuCount = deliveryMenuCount;
        
        //初始化子控制器
        [self setupChildVces];
        //设置顶部标签栏
        [self setupTitleView];
        //底部的 scrollview
        [self setupContentview];

        
    } anderrorBlock:^{
        
        
    }];

}
#pragma mark - 初始化子控制器
- (void)setupChildVces{
    
    DetailSeeMenuRestaurantViewController *DetailSeeMenuRestaurantV = [[DetailSeeMenuRestaurantViewController alloc] initWithRestaurant:restaurant andIsSelfUpload:self.isSelfUpload];
//    DetailSeeMenuRestaurantV.isSelfUpload = self.isSelfUpload;
    [self addChildViewController:DetailSeeMenuRestaurantV];
 
    
    DetailSeeMenuBarViewController *DetailSeeMenuBarV = [[DetailSeeMenuBarViewController alloc] initWithRestaurant:restaurant andIsSelfUpload:self.isSelfUpload];
    [self addChildViewController:DetailSeeMenuBarV];
    
    DetailSeeMenDeliveryViewController *DetailSeeMenDeliveryV = [[DetailSeeMenDeliveryViewController alloc] initWithRestaurant:restaurant andIsSelfUpload:self.isSelfUpload];
    [self addChildViewController:DetailSeeMenDeliveryV];
    
    
}

#pragma mark -  //设置顶部标签栏
- (void)setupTitleView{
    
    //添加标签栏整体
    UIView *titlesView = [[UIView alloc] init];
    titlesView.backgroundColor = [UIColor whiteColor];//[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5];
    titlesView.width = self.view.width;
    titlesView.height = TitlesViewH;
    titlesView.y = 0;
    [self.view addSubview:titlesView];
    self.titlesView = titlesView;
    
    //底部红色的指示器
    UIView *indicatorView = [[UIView alloc] init];
    indicatorView.backgroundColor = [UIColor redColor];
    indicatorView.height = 2;
    indicatorView.tag = -1;
    indicatorView.y = titlesView.height - indicatorView.height;
    
    self.indicatorView = indicatorView;
    
    //内部子标签
    
    CGFloat width = titlesView.width / self.childViewControllers.count;
    CGFloat height = titlesView.height;
    for (NSInteger i = 0; i < self.childViewControllers.count; i ++) {
        UIButton *button = [[UIButton alloc] init];
        button.tag = i;
        button.height = height;
        button.width = width ;
        button.x = i * width;
        UIViewController *vc = self.childViewControllers[i];
        //        [button setTitle:vc.title forState:UIControlStateNormal];
        if (i == 0) {
            [button setTitle:[NSString stringWithFormat:@"Restaurant(%@)",RestaurantMenuCount] forState:UIControlStateNormal];
        }else if (i == 1){
            [button setTitle:[NSString stringWithFormat:@"Bar(%@)",BarMenuCount] forState:UIControlStateNormal];
        }else{
            [button setTitle:[NSString stringWithFormat:@"Delivery(%@)",DeliveryMenuCount] forState:UIControlStateNormal];
        }
        //强制布局(强制更新子空间的 frame)
        //        [button layoutIfNeeded];
        [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor redColor] forState:UIControlStateDisabled];
        button.titleLabel.font = [UIFont systemFontOfSize:14];
        [button addTarget:self action:@selector(titleClick:) forControlEvents:UIControlEventTouchUpInside];
        [titlesView addSubview:button];
        
        //默认点击第一个按钮
        if (i == 0) {
            button.enabled = NO;
            self.selectedButton = button;
            //让按钮内部的 lebel 根据文字内容来计算尺寸
            [button.titleLabel sizeToFit];
            self.indicatorView.width = button.titleLabel.width;
            self.indicatorView.centerX = button.centerX;
        }
        
    }
    
    [titlesView addSubview:indicatorView];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor colorCCCCCC];
    [titlesView addSubview:lineView];
    lineView.sd_layout
    .bottomSpaceToView(titlesView, 0)
    .leftSpaceToView(titlesView, 0)
    .widthIs(DeviceWidth)
    .heightIs(0.8);
    
}
#pragma mark - <UIScrollViewDelegate>
//滚动动画结束后调用这个方法
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    
    //当前索引
    NSInteger index =  scrollView.contentOffset.x / scrollView.width;
    
    //取出子控制器
    UITableViewController *vc = self.childViewControllers[index];
    vc.view.x = scrollView.contentOffset.x;
    vc.view.y = 0;
    //设置控制器 view 的 height 值为整个屏幕的高度(默认是比屏幕高度少个20)
    vc.view.height =scrollView.height;
    
    
    [scrollView addSubview:vc.view];
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [self scrollViewDidEndScrollingAnimation:scrollView];
    //点击按钮
    NSInteger index = scrollView.contentOffset.x / scrollView.width;
    [self titleClick:self.titlesView.subviews[index]];
}

#pragma mark -底部setupscrollview
- (void)setupContentview{
    
    //不要自动调整 inset
    self.automaticallyAdjustsScrollViewInsets = NO;
    UIScrollView *contentView = [[UIScrollView alloc] init];
    contentView.frame = self.view.bounds;
    contentView.delegate = self;
    contentView.pagingEnabled = YES;
    //    //设置内边距
    //    CGFloat bottom = self.tabBarController.tabBar.height;
    //    CGFloat top = CGRectGetMaxY(self.titleView.frame);
    //    contentView.contentInset = UIEdgeInsetsMake(top, 0, bottom, 0);
    [self.view insertSubview:contentView atIndex:0];
    contentView.contentSize = CGSizeMake(contentView.width * self.childViewControllers.count,0 );
    self.contentView = contentView;
    
    //添加第一个控制器
    [self scrollViewDidEndScrollingAnimation:contentView];
}
- (void)titleClick:(UIButton *)button{
    
    
    //修改按钮状态
    self.selectedButton.enabled = YES;
    button.enabled = NO;
    self.selectedButton = button;
    [UIView animateWithDuration:0.25 animations:^{
        
        self.indicatorView.width = button.titleLabel.width;
        self.indicatorView.centerX = button.centerX;
    }];
    //滚动
    CGPoint offset = self.contentView.contentOffset;
    offset.x =  button.tag * self.contentView.width;
    [self.contentView setContentOffset:offset animated:YES];
    
}

-(void)setNavi{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor color333333],
                                                                    NSFontAttributeName:[UIFont boldSystemFontOfSize:17]};
    //    [self setLeftItemWithIcon:[UIImage imageNamed:@"back_black.png"] title:nil selector:@selector(backAction:)];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
