//
//  DetailSeeMenuUserIDViewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/11.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "DetailSeeMenuUserIDViewController.h"
#import "MenuPhotoGalleryHandler.h"
#import "NavigationBarButtonItem.h"
#import "MenuPhotoViewColltroller.h"
@interface DetailSeeMenuUserIDViewController ()<UIScrollViewDelegate>
{
    IMGRestaurant *restaurant;
    UICollectionView *allCollectionView;
    NSInteger currentCount;
    UIScrollView *scrollView;
    CGFloat currentHeight;
    float leftCurrentHeight;
    float rightCurrentHeight;
    
    float startPointY;
    
    float currentY;
    NSMutableArray *menuPhotoArray;
    NSMutableArray *menuPhotoList;
    NSMutableArray *photosArray;
    
    int menuType;
    NSMutableArray *restaurantMenu;
    NSMutableArray *barMenu;
    NSMutableArray *deliveryMenu;
    NSArray *lastPageMenuPhotoArray;
    IMGMenu *menu;
    int serveCout;
    int offset;
}
@property (nonatomic, retain) UIScrollView *mScrollView;
@end

@implementation DetailSeeMenuUserIDViewController

-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant{
    self = [super init];
    if (self != nil) {
        restaurant = paramRestaurant;
    }
    return self;
}
-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant andIsSelfUpload:(BOOL)isSelfUpload{
    self = [super init];
    if (self != nil) {
        restaurant = paramRestaurant;
        self.isSelfUpload = isSelfUpload;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=NO;
    [super viewWillAppear:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"Restaurant Menu list page";
    offset = 0;
    NavigationBarButtonItem *navigationBarLeftButton = [[NavigationBarButtonItem alloc ]initWithImage:[UIImage imageNamed:NavigationBackImage] andTitle:@"  MENU" target:self action:@selector(goToBackViewController) width:70.0f offset:-[UIDevice heightDifference]+15];
    self.navigationItem.leftBarButtonItem = navigationBarLeftButton;
    
    leftCurrentHeight = 5;
    rightCurrentHeight = 5;
    
    currentCount=0;
    
    IMGUser *user = [IMGUser currentUser];
    
    [self  addScrollView];
    
    [self getSelfDatafromeServer:restaurant.restaurantId userId:user.userId offset:offset];
}


- (void)getSelfDatafromeServer:(NSNumber*)restaurantId userId:(NSNumber*)userId offset:(int )offset_{
    serveCout=1;
    menuPhotoArray = [[NSMutableArray alloc] init];
    restaurantMenu = [[NSMutableArray alloc] init];
    barMenu = [[NSMutableArray alloc] init];
    deliveryMenu = [[NSMutableArray alloc] init];
    lastPageMenuPhotoArray = [[NSArray alloc] init];
    [MenuPhotoGalleryHandler getSelfMenuGalleryDataFromService:restaurantId userId:userId  offset:offset_ andBlock:^(NSArray *menuList, NSNumber *menuPhotoCount, NSNumber *menuPhotoCountTypeRestant, NSNumber *menuPhotoCountTypeBar, NSNumber *menuPhotoCountTypeDelivery, IMGMenu *menu_) {
        menu = menu_;
        menuPhotoList = [NSMutableArray arrayWithArray:menuList];
        lastPageMenuPhotoArray = [NSArray arrayWithArray:menuPhotoList];
        [self getAllImageUrl];
        [self setPhotoUI];
        [self addRestaurantMenuSelf];
    }];
    
    
}

-(void)setPhotoUI{
    for (NSDictionary *dic in menuPhotoList) {
        IMGMenu *menu_ = [[IMGMenu alloc] init];
        [menu_ setValuesForKeysWithDictionary:dic];
        menu_.imageUrl = [dic objectForKey:@"imageUrl"];
        menu_.type = [dic objectForKey:@"type"];
        [menuPhotoArray addObject:menu_];
    }
    for (int i = 0; i<menuPhotoArray.count; i++) {
        IMGMenu * menu_ = menuPhotoArray[i];
        if ([menu_.type intValue]==1||[menu_.type intValue]==0)
        {
            [restaurantMenu addObject:menu_];
        }else if ([menu_.type intValue] == 2)
        {
            [barMenu addObject:menu_];
        }else{
            [deliveryMenu addObject:menu_];
        }
    }
    [menuPhotoArray removeAllObjects];
}
- (void)getAllImageUrl{
    NSMutableArray *photoImageUrl = [[NSMutableArray alloc] init];
    for (NSDictionary *dict in menuPhotoList) {
        IMGMenu *menu_ = [[IMGMenu alloc] init];
        [menu_ setValuesForKeysWithDictionary:dict];
        menu_.imageUrl = [dict objectForKey:@"imageUrl"];
        menu_.menuId = [dict objectForKey:@"id"];
        [photoImageUrl addObject:menu_];
        
    }
    photosArray = [[NSMutableArray alloc] initWithArray:photoImageUrl];
    
}
-(void)addScrollView
{
    if (self.mScrollView == nil) {
        self.mScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-44)];
        self.mScrollView.backgroundColor = [UIColor blackColor];
        self.mScrollView.showsVerticalScrollIndicator = NO;
        //        self.mScrollView.delegate = self;
        self.mScrollView.contentSize = CGSizeZero;
        self.mScrollView.delegate = self;
        currentY = 0;
        [self.view addSubview:self.mScrollView];
    }
}
-(void)addRestaurantMenuSelf{
    currentY=10;
    if(restaurantMenu.count==0) return;
    
    
    Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, currentY, DeviceWidth/2, 30) andTextFont:[UIFont fontWithName:FONT_GOTHAM_LIGHT size:12] andTextColor:[UIColor whiteColor] andTextLines:1];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.text = [NSString stringWithFormat:@"Restaurant (%@)",[NSString stringWithFormat:@"%d%@",[menu.menuPhotoCount intValue],([menu.menuPhotoCount intValue]>1)?@"":@""/*@"photos":@"photo"*/]];
    [self.mScrollView addSubview:titleLabel];
    
//    Label *numlabel = [[Label alloc]initWithFrame:CGRectMake(DeviceWidth-15-60, currentY, 60, 35) andTextFont:[UIFont fontWithName:FONT_GOTHAM_LIGHT size:12] andTextColor:[UIColor whiteColor] andTextLines:1];
//    numlabel.textAlignment = NSTextAlignmentLeft;
//    numlabel.text = [NSString stringWithFormat:@"%d %@",[menu.menuPhotoCount intValue],([menu.menuPhotoCount intValue]>1)?@"photos":@"photo"];
//    
//    [self.mScrollView addSubview:numlabel];
//    
    
    for (int i=0; i<restaurantMenu.count; i++) {
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(6+(3.5+(DeviceWidth-19)/3)*(i%3), titleLabel.endPointY+((DeviceWidth-19)/3+3)*(i/3), (DeviceWidth-19)/3, (DeviceWidth-19)/3)];
        imageView.tag =i;
        UITapGestureRecognizer *Tap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagePressed:)];
        [Tap setNumberOfTapsRequired:1];
        [Tap setNumberOfTouchesRequired:1];
        imageView.userInteractionEnabled=YES;
        [imageView addGestureRecognizer:Tap];
        NSString *url;
        if (self.isUseWebP) {
            url = [((IMGMenu *)[restaurantMenu objectAtIndex:i]).imageUrl returnFullWebPImageUrlWithWidth:(DeviceWidth-19)/3];
        }else{
            url = [((IMGMenu *)[restaurantMenu objectAtIndex:i]).imageUrl returnFullImageUrlWithWidth:(DeviceWidth-19)/3];
        }
        UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake( (DeviceWidth-19)/3, (DeviceWidth-19)/3)];
        __weak typeof(imageView) weakImageView = imageView;
        [imageView sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            image = [image imageByScalingAndCroppingForSize:CGSizeMake( (DeviceWidth-19)/3,(DeviceWidth-19)/3)];
            
            [weakImageView setImage:image];
            
        }];
        
        //        [imageView setImageWithURL:[NSURL URLWithString:url] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] sizeFitWidth:YES];
        [self.mScrollView addSubview:imageView];
        currentY = imageView.endPointY;
        
    }
    
    startPointY=currentY;
    
    self.mScrollView.contentSize = CGSizeMake(DeviceWidth, currentY);
    [restaurantMenu removeAllObjects];
}
-(void)imagePressed:(UIGestureRecognizer*)gesture{
    NSInteger tag = gesture.view.tag;
    //    NSMutableArray *images=[[NSMutableArray alloc] init];
    
    MenuPhotoViewColltroller *menuPhotoViewColltroller = [[MenuPhotoViewColltroller alloc]initWithPhotosArray:photosArray andPhotoTag:tag andRestaurant:restaurant andFromDetail:YES];
    menuPhotoViewColltroller.isUseWebP = self.isUseWebP;
    menuPhotoViewColltroller.menuPhotoCount = menu.menuPhotoCount;
    menuPhotoViewColltroller.amplitudeType = @"Restaurant detail page - menu list";
    [self.navigationController pushViewController:menuPhotoViewColltroller animated:YES];
    
    NSLog(@"图片被点击了%ld",(long)tag);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
