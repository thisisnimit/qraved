//
//  ReviewTableViewCell.h
//  Qraved
//
//  Created by harry on 2017/11/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGReview.h"

@class ReviewTableViewCell;

@protocol ReviewTableViewCellDelegate <NSObject>
@optional
- (void)reviewImagePressed:(IMGReview*)review withTag:(int)tag;
- (void)reviewUserNameOrImageTapped:(id)sender;
- (void)userReviewMoreBtnClickWithIMGUserReview:(IMGUserReview*)review;
- (void)reviewMsgReadMoreTapped:(NSIndexPath *)indexPath;
- (void)shareButtonTapped:(IMGReview *)reviewView shareButton:(UIButton *)button;
- (void)gotoJournalDetail:(NSNumber *)journalId;
@end

@interface ReviewTableViewCell : UITableViewCell

@property (nonatomic, assign) id <ReviewTableViewCellDelegate>delegate;
@property (nonatomic, strong) IMGReview *review;
@property (nonatomic, strong) id navController;
@property (nonatomic, strong) id controller;
@property (nonatomic,assign) float currentPointY;
@property (nonatomic, strong) NSIndexPath *indexPath;

@end
