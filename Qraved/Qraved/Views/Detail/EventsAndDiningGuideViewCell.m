//
//  EventsAndDiningGuideViewCell.m
//  Qraved
//
//  Created by System Administrator on 10/14/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "EventsAndDiningGuideViewCell.h"
#import "UIConstants.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "UIImageView+Helper.h"
#import "UIImageView+WebCache.h"
#import "UILabel+Helper.h"
#import "Date.h"
#import "UIImage+Resize.h"
#define RESTAURANT_IMAGE_SIZE 70
#define MORE_BUTTON_SIZE 10
#define CELL_RATING_STAR_WIDTH 10

@implementation EventsAndDiningGuideViewCell{
	
	UILabel *titleLabel;
	UIImageView *imageView;
	UILabel *typeLabel;
	UIImage *placeHolderImage;
}

@synthesize title,image,isEvent;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier{

	if(self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]){
		self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth/2, DeviceWidth/4+7)];
        imageView.userInteractionEnabled = YES;
        
        placeHolderImage = [[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth, DeviceWidth/4+47)];
        
        UIFont *typeFont=[UIFont systemFontOfSize:9];
        
        CGSize size1 = [L(@"DINING GUIDE") sizeWithFont:typeFont];
        typeLabel = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth/2+LEFTLEFTSET, 5,DeviceWidth/2-LEFTLEFTSET,size1.height)];
        typeLabel.font=typeFont;
        typeLabel.textColor=[UIColor color222222];
        typeLabel.numberOfLines=1;

        UIFont *titleFont=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR  size:13];
       	titleLabel=[[UILabel alloc] init];
        titleLabel.font=titleFont;
        titleLabel.numberOfLines=4;
        titleLabel.lineBreakMode=NSLineBreakByWordWrapping;

        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, imageView.endPointY, DeviceWidth, 1)];
        lineImage.image = [UIImage imageNamed:@"CutOffRule"];
        
        [self addSubview:imageView];
        [self addSubview:typeLabel];
        [self addSubview:titleLabel];
        [self addSubview:lineImage];

		return  self;
	}
    return self;
}

-(void)setTitle:(NSString*)_title{
	title=[_title copy];

	CGSize titleSize=[title sizeWithFont:titleLabel.font constrainedToSize:CGSizeMake(DeviceWidth/2-LEFTLEFTSET,1000) lineBreakMode:NSLineBreakByWordWrapping];
	titleLabel.frame=CGRectMake(DeviceWidth/2+LEFTLEFTSET,typeLabel.endPointY+5,DeviceWidth/2-LEFTLEFTSET,titleSize.height);
	titleLabel.text=title;
}

-(void)setImage:(NSString*)_image{
	image=[_image copy];
    __weak typeof(imageView) weakImageView = imageView;
    UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth/2, DeviceWidth/4+7)];
    
//	[imageView setImageWithURL:[NSURL URLWithString:[_image returnFullImageUrlWithWidth:DeviceWidth/2]] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//        image = [image imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth/2, DeviceWidth/4+7)];
//        
//        [weakImageView setImage:image];
//
//    }];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[_image returnFullImageUrlWithWidth:DeviceWidth/2]] placeholderImage:placeHoderImage completed:^(UIImage *images, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        images = [images imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth/2, DeviceWidth/4+7)];
        
                [weakImageView setImage:images];

    }];
}

-(void)isEvent:(BOOL)_isEvent{
	isEvent=_isEvent;
	if(isEvent){
		typeLabel.text=L(@"EVENT");
	}else{
		typeLabel.text=L(@"DINING GUIDE");
	}
}

-(void)dealloc{

}

@end
