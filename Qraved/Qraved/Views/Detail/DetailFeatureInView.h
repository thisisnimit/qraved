//
//  DetailFeatureInView.h
//  Qraved
//
//  Created by apple on 17/3/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"
#import "IMGMediaComment.h"
@protocol DetailFeatureInViewDelegate <NSObject>

-(void)eventsImagePressed:(UITapGestureRecognizer*)sender;
-(void)diningGuideImagePressed:(IMGDiningGuide *)diningGuide;
-(void)gotoEvents;
-(void)gotoJournal:(IMGMediaComment*)journal;
-(void)gotoArticle;
@end

@interface DetailFeatureInView : UIView<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic,strong)NSMutableArray *diningGuideArray;
@property (nonatomic,strong)NSMutableArray *eventArray;
@property (nonatomic,strong)NSMutableArray *mediaCommentArray;
@property (nonatomic,weak)id<DetailFeatureInViewDelegate> delegate;
@property (nonatomic, strong)  IMGRestaurant  *restaurant;

@property (nonatomic, strong) UICollectionView* photoCollectionView;
@property (nonatomic, strong) UICollectionView* journalCollectionView;

-(instancetype)initWithDiningGuideDataArr:(NSArray*)diningGuideDataArr  andEventArray:(NSArray *)eventDataArr andJournalDataArr:(NSArray*)journalDataArr andRestaurant:(IMGRestaurant*)restaurant;

@end
