//
//  DetailCallNowView.h
//  Qraved
//
//  Created by apple on 17/3/23.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"

@protocol DetailCallNowViewDelegate <NSObject>

-(void)telephoneButtonTapped2;
-(void)mapButtonTapped2;

@end
@interface DetailCallNowView : UIView

@property(nonatomic,weak) id<DetailCallNowViewDelegate> delegate;

-(instancetype)initWithRestaurant:(IMGRestaurant*)restaurant;

@end
