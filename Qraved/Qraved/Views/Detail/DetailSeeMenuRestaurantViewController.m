//
//  DetailSeeMenuRestaurantViewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/11.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "DetailSeeMenuRestaurantViewController.h"
#import "NavigationBarButtonItem.h"
#import "MenuPhotoGalleryHandler.h"
#import "MenuPhotoViewColltroller.h"
#import "DetailDataHandler.h"
#import "DetailSeeMenuRestaurantModel.h"
#define qraveWidth 305.0/2
#define IMAGE_WIDTH 60
@interface DetailSeeMenuRestaurantViewController ()<UICollectionViewDelegate,UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    NSInteger currentCount;
    UIScrollView *scrollView;
    CGFloat currentHeight;
    float leftCurrentHeight;
    float rightCurrentHeight;
    
    float startPointY;
    
    float currentY;
    NSMutableArray *menuPhotoArray;
    NSMutableArray *menuPhotoList;
    IMGRestaurant *restaurant;
    NSMutableArray *photosArray;
    
    int menuType;
    NSMutableArray *restaurantMenu;
    NSMutableArray *barMenu;
    NSMutableArray *deliveryMenu;
    NSArray *lastPageMenuPhotoArray;
    IMGMenu *menu;
    int serveCout;
    int offset;
    UICollectionView *allCollectionView;
    NSMutableArray *restaurantPhotoArray;
    DetailSeeMenuRestaurantModel *RestaurantModel;
}
@property (nonatomic, strong)  NSMutableArray  *dataArray;
@end

@implementation DetailSeeMenuRestaurantViewController

-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant{
    self = [super init];
    if (self != nil) {
        restaurant = paramRestaurant;
    }
    return self;
}
-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant andIsSelfUpload:(BOOL)isSelfUpload{
    self = [super init];
    if (self != nil) {
        restaurant = paramRestaurant;
        self.isSelfUpload = isSelfUpload;
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=NO;
    [super viewWillAppear:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"Restaurant Menu list page";
    offset = 0;
    NavigationBarButtonItem *navigationBarLeftButton = [[NavigationBarButtonItem alloc ]initWithImage:[UIImage imageNamed:NavigationBackImage] andTitle:@"  MENU" target:self action:@selector(goToBackViewController) width:70.0f offset:-[UIDevice heightDifference]+15];
    self.navigationItem.leftBarButtonItem = navigationBarLeftButton;
    
    leftCurrentHeight = 5;
    rightCurrentHeight = 5;
    
    currentCount=0;
    
   
    
//    [self  addScrollView];
     [self initUI];
    
    [self requestData];
        _dataArray = [NSMutableArray array];

    allCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
}
- (void)requestData{

    [self initData];
}
- (void)initData{

        [self getDishesFromServer:restaurant.restaurantId];

}
- (void)loadMoreData{

     [self initData];
}
- (void)getDishesFromServer:(NSNumber*)restaurantId
{
    NSDictionary *dict=@{@"restaurantId":[NSString stringWithFormat:@"%@",restaurant.restaurantId],@"max":@"15",@"offset":[NSString stringWithFormat:@"%d",offset]};
    [DetailDataHandler SeeMenuRestaurant:dict andSuccessBlock:^(NSMutableArray *array,NSNumber *restaurantMenuCount, NSNumber *barMenuCount, NSNumber *deliveryMenuCount) {
        [allCollectionView.mj_footer endRefreshing];
        [self loadImageUrl:array];
        if ([array count] >0) {
            offset += 20;
        }
        [_dataArray addObjectsFromArray:array];
        [allCollectionView reloadData];
        
        
        
    } anderrorBlock:^{
        
         [allCollectionView.mj_footer endRefreshing];
    }];



}

- (void)initUI{
    
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    CGFloat width = (DeviceWidth-4)/3;
    layout.itemSize = CGSizeMake(width, width);
    layout.minimumLineSpacing = 1;
    layout.minimumInteritemSpacing = 1;
    //    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    
    allCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,40, DeviceWidth, DeviceHeight - 84) collectionViewLayout:layout];
    allCollectionView.backgroundColor = [UIColor whiteColor];
    allCollectionView.delegate = self;
    allCollectionView.dataSource = self;
    allCollectionView.showsHorizontalScrollIndicator = NO;
    allCollectionView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:allCollectionView];
    
    
    [allCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"photoCell"];
    
}


#pragma mark -- collection  delegate
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
        
        for (UIView *subView in cell.contentView.subviews) {
            [subView removeFromSuperview];
        }
            RestaurantModel = [_dataArray objectAtIndex:indexPath.row];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.contentView.bounds];
        [imageView sd_setImageWithURL:[NSURL URLWithString:[RestaurantModel.imageUrl returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            //image animation
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            if ([manager diskImageExistsForURL:[NSURL URLWithString:[RestaurantModel.imageUrl returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]]]) {
                NSLog(@"dont loading animation");
            }else {
                imageView.alpha = 0.0;
                [UIView transitionWithView:imageView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                    imageView.image = image;
                    imageView.alpha = 1.0;
                } completion:NULL];
                
            }

            
        }];
        [cell.contentView addSubview:imageView];
        
        return cell;

}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
        return _dataArray.count;
   
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger tag = indexPath.row;
    //    NSMutableArray *images=[[NSMutableArray alloc] init];
    NSMutableArray *photoImageUrl = [[NSMutableArray alloc] init];
    for (DetailSeeMenuRestaurantModel *restaurantModel in _dataArray) {
        IMGMenu *menu_ = [[IMGMenu alloc] init];
        //        [menu_ setValuesForKeysWithDictionary:dict];
        menu_.imageUrl = restaurantModel.imageUrl;//[dict objectForKey:@"imageUrl"];
        menu_.menuId = restaurantModel.myid;
        menu_.createTimeLong = (long long)restaurantModel.createTime;
        [photoImageUrl addObject:menu_];
        
    }
    
    
    
    MenuPhotoViewColltroller *menuPhotoViewColltroller = [[MenuPhotoViewColltroller alloc]initWithPhotosArray:photoImageUrl andPhotoTag:tag andRestaurant:restaurant andFromDetail:YES];
    menuPhotoViewColltroller.isUseWebP = self.isUseWebP;
    menuPhotoViewColltroller.menuPhotoCount = [NSNumber numberWithInteger:_dataArray.count];
    menuPhotoViewColltroller.amplitudeType = @"Restaurant detail page - menu list";
    [self.navigationController pushViewController:menuPhotoViewColltroller animated:YES];
    
}

- (void)loadImageUrl:(NSMutableArray *)imageArr{
     imageArr = (NSMutableArray *)[[imageArr reverseObjectEnumerator] allObjects];
    for (DetailSeeMenuRestaurantModel *model in imageArr) {
        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[model.imageUrl returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            
        }];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"%f",allCollectionView.contentOffset.y);
    if (allCollectionView.contentOffset.y <= 0) {
        allCollectionView.bounces = NO;
        
        NSLog(@"jinzhixiala");
    }
    else
        if (allCollectionView.contentOffset.y >= 0){
            allCollectionView.bounces = YES;
            NSLog(@"allow shangla");
            
        }
}

@end
