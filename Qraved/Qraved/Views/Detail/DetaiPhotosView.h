//
//  DetaiPhotosView.h
//  Qraved
//
//  Created by apple on 17/3/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"

@protocol DetaiPhotosViewDelegate <NSObject>

-(void)imagePressed:(UIGestureRecognizer *)gesture;
-(void)uploadPhoto;
-(void)gotoPhotos:(id)btnOrTap;
-(void)restDishPhotosCountClick:(UIGestureRecognizer *)gesture;
@end

@interface DetaiPhotosView : UIView

@property(nonatomic,weak) id<DetaiPhotosViewDelegate> delegate;
-(instancetype)initWithPhtosArray:(NSArray*)photoArray andRestaurant:(IMGRestaurant*)restaurant;

@end
