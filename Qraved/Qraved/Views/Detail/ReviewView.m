//
//  ReviewView.m
//  Qraved
//
//  Created by Sean Liao on 9/11/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "ReviewView.h"
#import "AppDelegate.h"
#import "ReviewHandler.h"
#import "ProfileViewController.h"
#import "V2_CommentListViewController.h"
#import "Label.h"
 
#import "Tools.h"
#import "Date.h"
#import "UIConstants.h"
#import "UserComment.h"
#import "UIButton+LinkButton.h"
#import "ReviewViewController.h"
#import "UILabel+Helper.h"
#import "UIImageView+WebCache.h"
#import "NSString+Helper.h"
#import "UIView+Helper.h"
#import "NSNumber+Helper.h"
#import "DLStarRatingControl.h"
#import "UIButton+WebCache.h"
#import "PhotosDetailViewController.h"

#import "IMGReview.h"
#import "IMGDish.h"
#import "UIColor+Helper.h"
#import "UIColor+Hex.h"
#import "NSString+Helper.h"
#import "LoadingView.h"
#import "HomeUtil.h"
#import "LikeView.h"

#define CONTENT_WIDTH 246
#define CELL_CONTENT_OFFSET 8
#define IMAGE_HEIGHT 60
#define IMAGE_WIDTH 60
#define AVATAR_IMAGE_SIZE 20    //user,coment avatar
#define COMMENT_TEXT_WIDTH 113

#define LEFTMARGIN 15
#define TEXTLABELTOPMARGIN 35
#define RIGHTMARGIN 30
#define TEXTLABELLEFTMARGIN 11+PROFILEIMAGEVIEWSIZE+8
#define PROFILEIMAGEVIEWSIZE 20

#define CONTENTLABELWIDTH DeviceWidth-LEFTMARGIN*2
#define LINK_TEXT @"<a style=\"text-decoration:none;color:#09BFD3;font-family:'OpenSans'\" href=\"http://www.qraved.com/\">...read more</a>"

@interface ReviewView()<UIWebViewDelegate,V2_LikeCommentShareViewDelegete>
{
    UIButton *backButton;
    UILabel *timeLabel;
    UILabel *scoreLabel;
    UIWebView *reviewSummarizeLabel;
    BOOL readMore;

    
    UIView *photosView;
    UILabel *likeLabel;
    UILabel *commentLabel;
    NSMutableArray* likelistArr;
    LikeView* likepopView;
    UILabel *userAllReviewSummarizeLabel;
    UIControl *control;
    DLStarRatingControl *ratingScore;
    UILabel *titleLabel;
    CGFloat ifY;
    V2_LikeCommentShareView *lCSView;
}
//-(CGSize)getLabelSize:(NSString *)string withFont:(UIFont *)font;

@end

@implementation ReviewView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initWithReview:(IMGReview *)review andDish:(NSArray*)dishArray withDelegate:(id<reviewDelegate>)delegate fromDetail:(BOOL)isDetail andIsOtherUser:(BOOL)isOtherUser andIsreadMore:(BOOL)isReadMore andIsFromUserallReviewPage:(BOOL)isFromUserallReviewPage andIsNeedReadMore:(BOOL)isNeedReadMore
{
    self=[super init];
    
    if(self) {
        self.reviewImages=[dishArray mutableCopy];

        self.delegate=delegate;
        
        CGFloat currentPointY;

        self.review = review;
        readMore = isReadMore;
        
        UITapGestureRecognizer *tap_1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(reviewViewPressed)];
        [self addGestureRecognizer:tap_1];

        UIButton *avatarLinkButton=[UIButton buttonWithType:UIButtonTypeCustom];
        avatarLinkButton.frame = CGRectMake(LEFTLEFTSET, TOPOFFSET, 40, 40);
        [avatarLinkButton addTarget:self action:@selector(controlClick) forControlEvents:UIControlEventTouchUpInside];

        
        
//        avatarLinkButton.clipsToBounds = YES;
//        avatarLinkButton.layer.cornerRadius = 40/2;
        NSURL *urlString;
        self.review.avatar = [self.review.avatar stringByReplacingPercentEscapesUsingEncoding:
                       NSUTF8StringEncoding];

        if ([self.review.avatar hasPrefix:@"http"])
        {
            urlString = [NSURL URLWithString:self.review.avatar];
        }
        else
        {
            urlString = [NSURL URLWithString:[self.review.avatar returnFullImageUrl]];
        }
        
        __weak UIButton *weakAvatarLinkButton = avatarLinkButton;
        [avatarLinkButton sd_setImageWithURL:urlString forState:UIControlStateNormal placeholderImage:[[UIImage imageNamed:@"headDefault.jpg"] cutCircleImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image.size.height>0&&image.size.width>0) {
                                image = [image cutCircleImage];
                                [weakAvatarLinkButton setImage:image forState:UIControlStateNormal];
                            }
        }];
        [self addSubview:avatarLinkButton];
        
        currentPointY = avatarLinkButton.frame.origin.y;
        
        UILabel *nameLabel = [[UILabel alloc]init];
        
        nameLabel.text = [NSString stringWithFormat:@"%@",self.review.fullName];
        nameLabel.textColor = [UIColor color333333];
        nameLabel.font = [UIFont systemFontOfSize:15];
        nameLabel.frame = CGRectMake(avatarLinkButton.frame.size.width+20,TOPOFFSET, nameLabel.expectedWidth, nameLabel.font.lineHeight);
        nameLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap_nameLabel = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(controlClick)];
        [nameLabel addGestureRecognizer:tap_nameLabel];

        [self addSubview:nameLabel];

        
        UILabel *reviewAndPhotoCountLabel = [[UILabel alloc]init];
        reviewAndPhotoCountLabel.font = [UIFont systemFontOfSize:12];
        reviewAndPhotoCountLabel.textColor = [UIColor color999999];
        reviewAndPhotoCountLabel.alpha = 0.7f;
        [self addSubview:reviewAndPhotoCountLabel];
        
        if ([review.userDishCount intValue]!=0&&[review.userReviewCount intValue]!=0) {
        
        
        [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@ • %@ %@",review.userReviewCount,[review.userReviewCount intValue] > 1 ? @"reviews":@"review",review.userDishCount,[review.userDishCount intValue] > 1 ? @"photos":@"photo"]];
        reviewAndPhotoCountLabel.frame = CGRectMake(nameLabel.frame.origin.x,nameLabel.endPointY, reviewAndPhotoCountLabel.expectedWidth, reviewAndPhotoCountLabel.font.lineHeight);
        }else if ([review.userDishCount intValue]==0||[review.userReviewCount intValue]==0){
            if ([review.userReviewCount intValue]==0&&[review.userDishCount intValue]!=0) {
                
                [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@",review.userDishCount,[review.userDishCount intValue] > 1 ? @"photos":@"photo"]];
                reviewAndPhotoCountLabel.frame = CGRectMake(nameLabel.frame.origin.x, nameLabel.endPointY+10, reviewAndPhotoCountLabel.expectedWidth, reviewAndPhotoCountLabel.font.lineHeight);
                
                
                
            }
            if ([review.userDishCount intValue]==0&&[review.userReviewCount intValue]!=0) {
                
                [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@",review.userReviewCount,[review.userReviewCount intValue] > 1 ? @"reviews":@"review"]];
                reviewAndPhotoCountLabel.frame = CGRectMake(nameLabel.frame.origin.x, nameLabel.endPointY+10, reviewAndPhotoCountLabel.expectedWidth, reviewAndPhotoCountLabel.font.lineHeight);
            }
        }

        //时间
        timeLabel = [[Label alloc]init];
        timeLabel.text = [Date getTimeInteval_v10:[review.timeCreated longLongValue]/1000];
        timeLabel.textColor = [UIColor color999999];
        timeLabel.alpha = 0.7f;
        timeLabel.font = [UIFont systemFontOfSize:12];
        timeLabel.frame = CGRectMake(DeviceWidth - 240 - 15, avatarLinkButton.endPointY + 10, 240, 14);
        timeLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:timeLabel];
        if (isFromUserallReviewPage) {
            avatarLinkButton.hidden = YES;
            nameLabel.hidden = YES;
            reviewAndPhotoCountLabel.hidden = YES;
            timeLabel.frame = CGRectMake(LEFTLEFTSET,TOPOFFSET, 240, 14);
        }
        
        ratingScore=[[DLStarRatingControl alloc]initWithFrame:CGRectMake(LEFTLEFTSET, avatarLinkButton.endPointY+10, 73, 15) andStars:5 andStarWidth:13 isFractional:NO spaceWidth:2];
        ratingScore.userInteractionEnabled=NO;
        ratingScore.rating= [[NSNumber numberWithDouble:[self.review.score doubleValue]] getRatingNumber];
        [self addSubview:ratingScore];
        
        
        if (self.review.title.length > 0){
            
            CGFloat titleX = 0.0f;
            if (![review.journalId isEqual:@0]) {
                UIImageView *imgJournal = [[UIImageView alloc] initWithFrame:CGRectMake(LEFTLEFTSET, ratingScore.endPointY+10, 16, 16)];
                imgJournal.image = [UIImage imageNamed:@"ic_home_qraved_logo"];
                [self addSubview:imgJournal];
                titleX = LEFTLEFTSET + 20;
            }else{
                titleX = LEFTLEFTSET;
            }
        
            titleLabel = [[UILabel alloc] init];
            titleLabel.font = [UIFont systemFontOfSize:15];
            titleLabel.textColor = [UIColor color333333];
            titleLabel.numberOfLines = 0;
            titleLabel.text = [self.review.title filterHtml];
            titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
            titleLabel.frame = CGRectMake(titleX, ratingScore.endPointY+10, DeviceWidth-titleX-LEFTLEFTSET, 1000);
            [titleLabel sizeToFit];
            [self addSubview:titleLabel];
            
            if (![review.journalId isEqual:@0]) {
                titleLabel.userInteractionEnabled = YES;
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoJournal)];
                [titleLabel addGestureRecognizer:tap];
            }
            
            ifY = titleLabel.endPointY +5;
        }else{
        
            ifY = ratingScore.endPointY +5;
        }
        

        CGFloat reviewSummarizeLableEndY;
        if (isNeedReadMore) {
            reviewSummarizeLabel = [[UIWebView alloc] initWithFrame:CGRectMake(LEFTLEFTSET, ifY, DeviceWidth - LEFTLEFTSET*2, 40)];
            reviewSummarizeLabel.tag=711;
            reviewSummarizeLabel.delegate = self;
            reviewSummarizeLabel.scrollView.bounces = NO;
            reviewSummarizeLabel.backgroundColor=[UIColor whiteColor];
            reviewSummarizeLabel.scrollView.showsHorizontalScrollIndicator = NO;
            reviewSummarizeLabel.scrollView.scrollEnabled = YES;
            reviewSummarizeLabel.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
            NSString *summarize=review.summarize;
            NSDictionary *strBackDic = [ReviewView getIsNeedReadMoreForOverLines:summarize];
            float contentLabelHeight;
            if([strBackDic[@"isNeedLoadMore"] boolValue]&&!readMore){
                summarize= strBackDic[@"finalStr"];
            }else{
//                summarize=[summarize stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
                if(!readMore && summarize.length>300){
                    //            commentStr=[NSString stringWithFormat:@"%@<a href=\"http://www.qraved.com/\">...read more</a>",[commentStr substringToIndex:COMMENTTEXTLENGTH]];
                    
                    summarize=[NSString stringWithFormat:@"%@%@",[summarize substringToIndex:300],LINK_TEXT];
                }
            }
            contentLabelHeight = [ReviewView calculateCommentTextHeight:summarize];
            
            reviewSummarizeLabel.frame=CGRectMake(LEFTLEFTSET, ifY, DeviceWidth - LEFTLEFTSET*2, contentLabelHeight);
            NSString *summarizeStr = [NSString stringWithFormat:@"<html> <head> <meta charset=\"UTF-8\">"
                                      "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">"
                                      "<style type=\"text/css\">"
                                      "html {"
                                      "    -webkit-text-size-adjust: none; /* Never autoresize text */"
                                      "}"
                                      "body{font-family:\"%@\";font-size: 12px; color:%@; padding:0; margin:0; width:%fpx; line-height: 15px;}"
                                      "</style>"
                                      "</head>"
                                      "<body><div id=\"content\">%@</div></body>"
                                      "</html>",DEFAULT_FONT_NAME,@"#999999",DeviceWidth - LEFTLEFTSET*2,summarize];
            
            summarizeStr = [summarizeStr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
            [reviewSummarizeLabel loadHTMLString:summarizeStr baseURL:nil];
            
            
            [reviewSummarizeLabel layoutSubviews];
            [self addSubview:reviewSummarizeLabel];
            reviewSummarizeLableEndY = reviewSummarizeLabel.endPointY;
        }else{
            userAllReviewSummarizeLabel = [[UILabel alloc] init];
            userAllReviewSummarizeLabel.text = review.summarize;
            float contentLabelHeight = [review.summarize sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 10000)].height;
            userAllReviewSummarizeLabel.frame = CGRectMake(LEFTLEFTSET, ifY, DeviceWidth-2*LEFTLEFTSET, contentLabelHeight);
            userAllReviewSummarizeLabel.numberOfLines = 0;
            userAllReviewSummarizeLabel.font = [UIFont systemFontOfSize:12];
            userAllReviewSummarizeLabel.alpha=0.3;

            [self addSubview:userAllReviewSummarizeLabel];
            reviewSummarizeLableEndY =userAllReviewSummarizeLabel.endPointY;
        }
        

        
        photosView = [[UIView alloc] init];
        photosView.frame = CGRectMake(LEFTLEFTSET, reviewSummarizeLableEndY + 5, DeviceWidth - 2*LEFTLEFTSET, 0);
        
        CGFloat dishImageWidth;
        CGFloat dishImageEndX;
        int viewFrameY = 0;
        int viewFrameX = 0;
        for (int i = 0; i<dishArray.count; i++)
        {
            IMGDish *dish=(IMGDish*)[dishArray objectAtIndex:i];
            UIImageView *dishImageView = [[UIImageView alloc]init];
            dishImageWidth = (photosView.frame.size.width-10)/3;
            dishImageView.userInteractionEnabled=YES;
            dishImageView.contentMode = UIViewContentModeScaleAspectFill;
            dishImageView.clipsToBounds = YES;
            if (viewFrameX == 3)
            {
                viewFrameY = dishImageWidth + 5 + viewFrameY;
                viewFrameX = 0;
            }
            
            dishImageView.frame = CGRectMake((dishImageWidth+5)*viewFrameX, viewFrameY, dishImageWidth, dishImageWidth);
            viewFrameX++;
            dishImageView.tag = i;
            [photosView addSubview:dishImageView];
            
            
            if (dishArray.count == 1) {
                
                dishImageView.frame = CGRectMake(0, viewFrameY, DeviceWidth, DeviceWidth/4*3);
            }
            
            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagePressed:)];
            [dishImageView addGestureRecognizer:tap];
            
            dishImageEndX = dishImageView.endPointX;
            
            if (i == 5 && dishArray.count > 6 && isDetail)
            {
                dishImageView.backgroundColor = [UIColor colorWithRed:222/250.0 green:222/250.0 blue:222/250.0 alpha:1];
                UILabel *otherCountLabel = [[UILabel alloc] init];
                otherCountLabel.tag=i;
                otherCountLabel.userInteractionEnabled=YES;
                otherCountLabel.text = [NSString stringWithFormat:@"+%lu",dishArray.count - 5];
                otherCountLabel.font = [UIFont systemFontOfSize:17];
                otherCountLabel.textColor = [UIColor color333333];
                otherCountLabel.frame = CGRectMake(dishImageView.frame.size.width/2 - 20, dishImageView.frame.size.height/2 - 20, 40, 40);
                [otherCountLabel addGestureRecognizer:tap];
                [dishImageView addSubview:otherCountLabel];
                break;
            }
            else
            {
                NSString *urlString = [dish.imageUrl returnFullImageUrl];
                __weak typeof(dishImageView) weakRestaurantImageView = dishImageView;

                [dishImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    [UIView animateWithDuration:1.0 animations:^{
                                                [weakRestaurantImageView setAlpha:1.0];
                                            }];
                }];
            }
            
            
            photosView.frame = CGRectMake(LEFTLEFTSET, reviewSummarizeLableEndY + 5, DeviceWidth - 2*LEFTLEFTSET, dishImageView.endPointY);
            if (dishArray.count == 1) {
             photosView.frame = CGRectMake(0, reviewSummarizeLableEndY + 5, DeviceWidth, dishImageView.endPointY);
            
            }
        }
        
        
        [self addSubview:photosView];

        float currentPoint= photosView.endPointY;
        
        lCSView = [[V2_LikeCommentShareView alloc] initWithFrame:CGRectMake(0, currentPoint, DeviceWidth, 40)];
        lCSView.backgroundColor = [UIColor whiteColor];
        lCSView.delegate = self;
        [lCSView setCommentCount:[review.commentCount intValue] likeCount:[review.likeCount intValue] liked:review.isLike];
        [self addSubview:lCSView];
        
        currentPoint = lCSView.endPointY+10;
        
        UIButton *btnLike = [UIButton buttonWithType:UIButtonTypeCustom];
        btnLike.frame = CGRectMake(DeviceWidth/3-60, 0, 60, 50);
        [lCSView addSubview:btnLike];
        
        [btnLike addTarget:self action:@selector(likeCountTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        self.reviewHeight = currentPoint;
        if ([review.siblingCountByUser intValue]>0) {
            UILabel *previousLabel = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth/2-LEFTMARGIN, photosView.endPointY-5, DeviceWidth/2, 20)];
            previousLabel.text = @"See Previous Reviews";
            
            previousLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:11];

            previousLabel.userInteractionEnabled = YES;
            [previousLabel setTextAlignment:NSTextAlignmentRight];
            [self addSubview:previousLabel];
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(previousLabelClicked)];
            [previousLabel addGestureRecognizer:tap];

        }


        //moreBtn
        UIButton *moreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        moreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        moreBtn.frame = CGRectMake(DeviceWidth - 25, TOPOFFSET - 3, 25, 50);
        [moreBtn setImage:[UIImage imageNamed:@"listMoreVertical"] forState:UIControlStateNormal];
        [self addSubview:moreBtn];
        if (isOtherUser)
        {
            moreBtn.hidden = YES;
        }
        [moreBtn addTarget:self action:@selector(moreBtnClcik) forControlEvents:UIControlEventTouchUpInside];

    }
    return self;
}
- (void)likeCountTapped:(UIButton *)button{
    [[Amplitude instance] logEvent:@" RC - View People Who Likes"];
    
    if (likepopView) {
        [likepopView removeFromSuperview];
    }
    
    [[LoadingView sharedLoadingView]startLoading];
    
    [HomeUtil getLikeListFromServiceWithReviewId:self.review.reviewId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
        likelistArr=[NSMutableArray arrayWithArray:likeListArray];
        likepopView.likecount=likeviewcount;
        
        [[LoadingView sharedLoadingView]stopLoading];
        likepopView.likelistArray=likelistArr;
        
    }];
    
    likepopView=[[LikeView alloc]initWithFrame:CGRectMake(0, 63, DeviceWidth, DeviceHeight)];
    
    likepopView.delegate=self;
    UIResponder *responder = [self nextResponder];
    while (responder){
        if ([responder isKindOfClass:[UINavigationController class]]){
            UINavigationController* vc=(UINavigationController*)responder;
            [[vc.viewControllers lastObject].view addSubview:likepopView];
            return;
        }
        
        responder = [responder nextResponder];
        
    }
    
}
-(void)returnLikeCount:(NSInteger)count
{
    
}

-(void)LikeviewLoadMoreData:(NSInteger)offset{
    
    [HomeUtil getLikeListFromServiceWithReviewId:self.review.reviewId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset]andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
        likepopView.likecount=likeviewcount;
        [likelistArr addObjectsFromArray:likeListArray];
        [likepopView.likeUserTableView reloadData];
    }];
    
}

- (void)likeCommentShareView:(UIView *)likeCommentShareView likeButtonTapped:(UIButton *)button{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
    [[LoadingView sharedLoadingView] startLoading];
//    IMGReview *review=[self.reviewsArray objectAtIndex:likeCommentShareView.tag];
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:self.review.reviewId forKey:@"Review_ID"];
    [eventProperties setValue:self.review.userId forKey:@"ReviewerUser_ID"];
    [eventProperties setValue:self.review.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:@"Restaurant detail page - review list" forKey:@"Location"];
    
    [ReviewHandler likeReview:self.review.isLike reviewId:self.review.reviewId andBlock:^{
        if (!self.review.isLike) {
            [[Amplitude instance] logEvent:@"UC - UnLike Review Succeed" withEventProperties:eventProperties];
        }else{
            [[Amplitude instance] logEvent:@"UC - Like Review Succeed" withEventProperties:eventProperties];
        }
        
        
    } failure:^(NSString *exceptionMsg){
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
    }];
    
    self.review.isLike=!self.review.isLike;
    if(self.review.isLike){
        self.review.likeCount=[NSNumber numberWithInt:[self.review.likeCount intValue]+1];
        
        [[Amplitude instance] logEvent:@"UC - Like Review Initiate" withEventProperties:eventProperties];
        
    }else{
        self.review.likeCount=[NSNumber numberWithInt:[self.review.likeCount intValue]-1];
        
        [[Amplitude instance] logEvent:@"UC - UnLike Review Initiate" withEventProperties:eventProperties];
    }
    
    [lCSView setCommentCount:[self.review.commentCount intValue] likeCount:[self.review.likeCount intValue] liked:self.review.isLike];
  
    [[LoadingView sharedLoadingView] stopLoading];
    
    
}
- (void)likeCommentShareView:(UIView *)likeCommentShareView shareButtonTapped:(UIButton *)button{
    if (self.delegate) {
        [self.delegate shareButtonTapped:self.review shareButton:button];
    }
    
}
- (void)likeCommentShareView:(UIView *)likeCommentShareView commentButtonTapped:(UIButton *)button{
    
    [IMGAmplitudeUtil trackReviewViewWithName:@"UC - Comment Review Initiate" andRestaurantId:self.review.restaurantId andReviewId:self.review.reviewId andReviewUserId:self.review.userId andOrigin:nil andLocation:@"Restaurant detail page - review list" andChannel:nil];
    
    V2_CommentListViewController *commentListVC = [[V2_CommentListViewController alloc] initWithTypeID:14 andModel:self.review];
    [commentListVC setRefreshComment:^{
        int commentCount = [self.review.commentCount intValue];
        commentCount ++;
        
        [IMGAmplitudeUtil trackReviewViewWithName:@"UC - Comment Review Succeed" andRestaurantId:self.review.restaurantId andReviewId:self.review.reviewId andReviewUserId:self.review.userId andOrigin:nil andLocation:@"Restaurant detail page - review list" andChannel:nil];
        
        self.review.commentCount = [NSNumber numberWithInt:commentCount];
        [lCSView setCommentCount:[self.review.commentCount intValue] likeCount:[self.review.likeCount intValue] liked:self.review.isLike];
    }];
    
    [self.controller pushViewController:commentListVC animated:YES];
    
}

-(void)reviewViewPressed
{
    if([self.delegate respondsToSelector:@selector(reviewViewClick:)])
    {
        [self.delegate reviewViewClick:self.review];
    }
}
- (void)controlClick
{

    if (self.delegate)
    {
            IMGUser *user = [[IMGUser alloc] init];
            user.userName = [NSString stringWithString:self.review.fullName];
            user.userId = [NSNumber numberWithInteger:[self.review.userId integerValue]];
            user.token = @"";
            user.avatar=self.review.avatar;
            [self.delegate reviewUserNameOrImageTapped:user];
    }
}
-(void)imagePressed:(UIGestureRecognizer*)gesture{
    if([self.delegate respondsToSelector:@selector(reviewImagePressed:images:withTag:)]){
        [self.delegate reviewImagePressed:self images:self.reviewImages withTag:(int)gesture.view.tag];
    }
    
}   

-(NSArray*)getReviewImages:(NSUInteger)reviewId{
    self.reviewImages = [[NSMutableArray alloc] initWithCapacity:2];
    [[DBManager manager] selectWithSql:[NSString stringWithFormat:@"SELECT * FROM IMGDish WHERE reviewId = '%lu' order by dishId desc;",(unsigned long)reviewId] successBlock:^(FMResultSet *resultSet){
        while([resultSet next]){
            IMGDish *dish=[[IMGDish alloc] init];
            [dish setValueWithResultSet:resultSet];
            [self.reviewImages addObject:dish];
        }
        [resultSet close];
    }failureBlock:^(NSError *error) {
        NSLog(@"error when get datas:%@",error.description);
    }];
    return self.reviewImages;
}

-(void)avatarLink:(UIButton *)button
{
}

-(void)commentAvatarLink:(UIButton *)button
{
    ProfileViewController *profileViewController=[[ProfileViewController alloc] init];
    
    [((UIViewController *)self.delegate).navigationController pushViewController:profileViewController animated:YES];
}
-(void)hideBackButton
{
    [backButton removeFromSuperview];
}
- (void)moreBtnClcik
{
    if([self.delegate respondsToSelector:@selector(userReviewMoreBtnClickWithIMGUserReview:)]){
        IMGUserReview *userReview = [[IMGUserReview alloc] init];
        userReview.reviewId = self.review.reviewId;
        userReview.restaurantId = self.review.restaurantId;
        userReview.reviewScore = self.review.score;
        userReview.reviewTimeCreated = self.review.timeCreated;
        userReview.userId = [NSNumber numberWithInt:[self.review.userId intValue]];
//        userReview.restaurantPriceLevel = self.review.;
        userReview.reviewTitle = [self.review.title filterHtml];
        userReview.reviewSummarize = self.review.summarize;
        userReview.restaurantTitle = self.review.restaurantTitle;
        userReview.userFullName = self.review.fullName;
        userReview.userAvatar = self.review.avatar;
        userReview.dishListArrM = [NSMutableArray arrayWithArray:self.reviewImages];
        [self.delegate userReviewMoreBtnClickWithIMGUserReview:userReview];
    }
}
+(NSDictionary *)getIsNeedReadMoreForOverLines:(NSString*)commentStr_{
    
    UILabel *lblTempStr = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, CONTENTLABELWIDTH, MAXFLOAT)];
    lblTempStr.text = commentStr_;
    lblTempStr.font = [UIFont systemFontOfSize:12];
    
    
    NSString *text = [lblTempStr text];
    UIFont   *font = [lblTempStr font];
    CGRect    rect = [lblTempStr frame];
    
    
    CTFontRef myFont = CTFontCreateWithName((CFStringRef)font.fontName,
                                            font.pointSize,
                                            NULL);
    
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:text];
    [attStr addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)myFont range:NSMakeRange(0, attStr.length)];
    
    CTFramesetterRef frameSetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)attStr);
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, CGRectMake(0,0,rect.size.width,100000));
    
    CTFrameRef frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, 0), path, NULL);
    
    NSArray *lines = (__bridge NSArray *)CTFrameGetLines(frame);
    
    NSMutableArray *linesArray = [[NSMutableArray alloc]init];
    
    NSMutableDictionary *strBackDic = [[NSMutableDictionary alloc]init];
    
    if(lines.count<10+1){
        [strBackDic setObject:@NO forKey:@"isNeedLoadMore"];
    }else{
        [strBackDic setObject:@YES forKey:@"isNeedLoadMore"];
        for (id line in lines)
        {
            CTLineRef lineRef = (__bridge CTLineRef)line;
            CFRange lineRange = CTLineGetStringRange(lineRef);
            NSRange range = NSMakeRange(lineRange.location, lineRange.length);
            
            NSString *lineString = [text substringWithRange:range];
            
            //            CFRelease(lineRef);
            [linesArray addObject:lineString];
            
        }
        
        //start range work for 4th line
        NSMutableAttributedString *attStr4Th = [[NSMutableAttributedString alloc] initWithString:linesArray[10-1]];
        [attStr4Th addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)myFont range:NSMakeRange(0, attStr4Th.length)];
        CTFramesetterRef frameSetter4Th = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)attStr4Th);
        
        CGMutablePathRef path4Th = CGPathCreateMutable();
        CGPathAddRect(path4Th, NULL, CGRectMake(0,0,CONTENTLABELWIDTH-70,MAXFLOAT));
        
        CTFrameRef frame4Th = CTFramesetterCreateFrame(frameSetter4Th, CFRangeMake(0, 0), path4Th, NULL);
        
        NSArray *lines4Th = (__bridge NSArray *)CTFrameGetLines(frame4Th);
        CTLineRef lineRef4Th = (__bridge CTLineRef )[lines4Th firstObject];
        CFRange lineRange4Th = CTLineGetStringRange(lineRef4Th);
        NSRange range4Th = NSMakeRange(lineRange4Th.location, lineRange4Th.length);
        NSString *lineString4Th = [linesArray[10-1] substringWithRange:range4Th];
        
        NSString *finalStr = @"";
        for(int i=0; i<10-1; i++){
            finalStr = [finalStr stringByAppendingString:linesArray[i]];
        }
        lineString4Th=[lineString4Th stringByReplacingOccurrencesOfString:@"\n" withString:@""];

        finalStr = [finalStr stringByAppendingString:lineString4Th];
        //        finalStr = [finalStr stringByAppendingString:@"<a href=\"http://www.qraved.com/\">...read more</a>"];
        
        finalStr = [finalStr stringByAppendingString:LINK_TEXT];
        
        
        finalStr=[finalStr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
        [strBackDic setObject:finalStr forKey:@"finalStr"];
        
        CFRelease(frame4Th);
        CFRelease(frameSetter4Th);
        CFRelease(path4Th);
        //        CFRelease(lineRef4Th);
        
    }
    
    CFRelease(frame);
    CFRelease(myFont);
    CFRelease(frameSetter);
    CFRelease(path);
    return strBackDic;
}
+(CGFloat)calculateCommentTextHeight:(NSString*)commentStr_{
  
    commentStr_=[commentStr_ stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    commentStr_=[commentStr_ stringByReplacingOccurrencesOfString:LINK_TEXT withString:@"...read more"];
    
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12]};
    
    CGSize textSize = [commentStr_ boundingRectWithSize:CGSizeMake(CONTENTLABELWIDTH, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    
    float height = ceil(textSize.height + 1.5f);
    
    NSLog(@"height = %@",commentStr_);
    
    return height;
}

-(void)previousLabelClicked{
    if (self.delegate&&[self.delegate respondsToSelector:@selector(previousLableTap:)]) {
        [self.delegate previousLableTap:self.review];
    }
}


#pragma mark - webview delegate

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    CGFloat htmlHeight = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] floatValue];
    
    webView.frame = CGRectMake(LEFTLEFTSET, ifY, DeviceWidth - LEFTLEFTSET*2, htmlHeight);
    if (control) {
        [control removeFromSuperview];
    }
    control=[[UIControl alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth - LEFTLEFTSET*2, htmlHeight-18)];
    [webView addSubview:control];

    UIScrollView *tempView = (UIScrollView *)[reviewSummarizeLabel.subviews objectAtIndex:0];
    
    tempView.scrollEnabled = NO;
    
    
    
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if (navigationType==UIWebViewNavigationTypeLinkClicked) {
        readMore = !readMore;
        
        if (self.delegate) {
            [self.delegate reviewViewMsgClick:self readMoreTapped:self.review isReadMore:readMore];
        }
        
        return NO;
    }
    
    return YES;
}

- (void)gotoJournal{
    if (self.delegate && [self.delegate respondsToSelector:@selector(gotoJournalDetail:)]) {
        [self.delegate gotoJournalDetail:self.review.journalId];
    }
}


@end
