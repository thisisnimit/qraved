//
//  DetailFeatureInView.m
//  Qraved
//
//  Created by apple on 17/3/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "DetailFeatureInView.h"
#import "IMGDiningGuide.h"
#import "IMGEvent.h"
#import "UIConstants.h"
#import "UIImage+Resize.h"
#import "IMGMediaComment.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "UIImageView+WebCache.h"
#import "Label.h"
#import "myLabel.h"
#import "V2_GuideCell.h"

@implementation DetailFeatureInView
{
    IMGDiningGuide  *diningGuide;
    IMGMediaComment *journal;
    NSMutableString *titleAndDescription;
}
-(UIView*)initWithDiningGuideDataArr:(NSArray*)diningGuideDataArr  andEventArray:(NSArray *)eventDataArr andJournalDataArr:(NSArray*)journalDataArr andRestaurant:(IMGRestaurant*)restaurant{
    
    NSLog(@"%@",diningGuideDataArr);
//     NSLog(@"%@",journalDataArr);
    if (self = [super init]) {
        float lastEndPointY=0.0;
        float journalStartY = 0.0;
       _diningGuideArray = [[NSMutableArray alloc]initWithCapacity:0];
        for (NSDictionary *dic in diningGuideDataArr)
        {
            diningGuide=[[IMGDiningGuide alloc] init];
            [diningGuide setValuesForKeysWithDictionary:dic];
            [_diningGuideArray addObject:diningGuide];
        }
        
        _eventArray = [[NSMutableArray alloc]initWithCapacity:0];
        for (NSDictionary *dic in eventDataArr){
            IMGEvent  *event=[[IMGEvent alloc]init];
            [event setValuesForKeysWithDictionary:dic];
            [_eventArray addObject:event];
        }
        
        self.backgroundColor = [UIColor whiteColor];
        
        _mediaCommentArray = [[NSMutableArray alloc]initWithCapacity:0];
        //journal
        for (int i = 0; i < journalDataArr.count; ++i){
            NSDictionary *sub=journalDataArr[i];
            journal = [[IMGMediaComment alloc]init];
            journal.title = [sub objectForKey:@"journalTitle"];
            journal.imageUrl = [sub objectForKey:@"journalImageUrl"];
            journal.journalTitle = journal.title;
            journal.journalImageUrl = journal.imageUrl;
            journal.link = [sub objectForKey:@"journalWebsiteUrl"];
            journal.mediaCommentId = [sub objectForKey:@"journalId"];
            journal.contents = [sub objectForKey:@"journalWebsiteName"];
            [_mediaCommentArray addObject:journal];
        }
        
        if([_diningGuideArray count]>0 || [_eventArray count]>0 || [_mediaCommentArray count] > 0){
            if (_mediaCommentArray.count > 0) {
  
                UILabel *articlesLabel = [[UILabel alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 15, DeviceWidth-2*LEFTLEFTSET, 20)];
                articlesLabel.text = L(@"Featured in Journal");
                articlesLabel.font = [UIFont boldSystemFontOfSize:14];
                articlesLabel.textColor = [UIColor color222222];
                [self addSubview:articlesLabel];
                
            
                    UIButton *reviewBackground=[UIButton buttonWithType:UIButtonTypeCustom];
                    [reviewBackground setFrame:CGRectMake(DeviceWidth - LEFTLEFTSET - 80, articlesLabel.frame.origin.y, 80, 40)];
                    [reviewBackground addTarget:self action:@selector(gotoArticles) forControlEvents:UIControlEventTouchUpInside];
                    reviewBackground.centerY = articlesLabel.centerY;
                    [self addSubview:reviewBackground];
                    [reviewBackground setTitle:L(@"See all") forState:UIControlStateNormal];
                    reviewBackground.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
                    reviewBackground.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
                    reviewBackground.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12];
                    lastEndPointY = reviewBackground.endPointY+5;
                    [reviewBackground setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
               
                
//                UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, articlesLabel.endPointY+5, DeviceWidth, 1)];
//                lineImage.image = [UIImage imageNamed:@"CutOffRule"];
//                [self addSubview:lineImage];
                
                lastEndPointY = articlesLabel.endPointY+15;
                journalStartY = lastEndPointY;
                BOOL morArticle = NO;
                if ([restaurant.journalCount intValue]>2) {
                    morArticle = YES;
                }
                int articleCount = morArticle?2:(int)_mediaCommentArray.count;
                
                UICollectionViewFlowLayout *layout1  = [[UICollectionViewFlowLayout alloc] init];
                layout1.scrollDirection = UICollectionViewScrollDirectionHorizontal;
                layout1.itemSize = CGSizeMake(124.5, 165);
                layout1.minimumLineSpacing = 8;
                layout1.minimumInteritemSpacing = 1;
                layout1.sectionInset = UIEdgeInsetsMake(0, 15, 0, 0);
                
                _journalCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,journalStartY, DeviceWidth, 165) collectionViewLayout:layout1];
                _journalCollectionView.backgroundColor = [UIColor whiteColor];
                _journalCollectionView.delegate = self;
                _journalCollectionView.dataSource = self;
                _journalCollectionView.showsHorizontalScrollIndicator = NO;
                [self addSubview:_journalCollectionView];
                
                
                lastEndPointY = _journalCollectionView.endPointY+1;
                
                if (journal.contents) {
                    
                    titleAndDescription = [NSMutableString stringWithString:journal.contents];
                }
                if(journal.link){
                    [titleAndDescription appendFormat:@", %@",journal.link];
                }
            }else{
            
                lastEndPointY = 15;
            }
           

            if (_diningGuideArray.count > 0) {
                lastEndPointY += 15;
                
                UILabel *featuredInLabel = [[UILabel alloc]initWithFrame:CGRectMake(LEFTLEFTSET, lastEndPointY, DeviceWidth-2*LEFTLEFTSET, 40)];
                featuredInLabel.text = L(@"Featured in Guide");
                featuredInLabel.font = [UIFont boldSystemFontOfSize:14];
                featuredInLabel.textColor = [UIColor color222222];
                [self addSubview:featuredInLabel];
                
                if ([restaurant.eventCount intValue] + [restaurant.diningGuideCount intValue] >2)
                {
                    UIButton *reviewBackground=[UIButton buttonWithType:UIButtonTypeCustom];
                    [reviewBackground setFrame:CGRectMake(DeviceWidth - LEFTLEFTSET - 80, featuredInLabel.frame.origin.y, 80, 40)];
                    [reviewBackground addTarget:self action:@selector(gotoEvent) forControlEvents:UIControlEventTouchUpInside];
                    reviewBackground.centerY = featuredInLabel.centerY;
                    [self addSubview:reviewBackground];
                    [reviewBackground setTitle:L(@"See all") forState:UIControlStateNormal];
                    reviewBackground.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
                    reviewBackground.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
                    reviewBackground.titleLabel.font= [UIFont systemFontOfSize:12];
                    lastEndPointY = reviewBackground.endPointY+5;
                    [reviewBackground setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
                }
                
                //            UILabel *eventsLabel;
//                if([_diningGuideArray count]>0 || [_eventArray count]>0){;
//                    
//                    UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, featuredInLabel.endPointY, DeviceWidth, 1)];
//                    lineImage.image = [UIImage imageNamed:@"CutOffRule"];
//                    [self addSubview:lineImage];
//                    lastEndPointY = lineImage.endPointY;
//                }else{
                    lastEndPointY = featuredInLabel.endPointY;
//                }
                
                
                
                UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
                layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
                layout.itemSize = CGSizeMake(124.5, 124.5);
                layout.minimumLineSpacing = 8;
                layout.minimumInteritemSpacing = 1;
                layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 0);
                
                _photoCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,lastEndPointY, DeviceWidth, 124.5) collectionViewLayout:layout];
                _photoCollectionView.backgroundColor = [UIColor whiteColor];
                _photoCollectionView.delegate = self;
                _photoCollectionView.dataSource = self;
                _photoCollectionView.showsHorizontalScrollIndicator = NO;
                [self addSubview:_photoCollectionView];
                
                [_photoCollectionView registerNib:[UINib nibWithNibName:@"V2_GuideCell" bundle:nil] forCellWithReuseIdentifier:@"guideCell"];
                
                lastEndPointY = _photoCollectionView.endPointY+1;
                
            }else{
                
                lastEndPointY = _journalCollectionView.endPointY+1;
            }
            
            
            
        }else{
        
            lastEndPointY = 15;
        }
        
        
        self.frame = CGRectMake(0, 0, DeviceWidth, lastEndPointY);

    }
    return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == _photoCollectionView) {
        if (_diningGuideArray.count >0) {
            
            return _diningGuideArray.count;
        }
    }else{
        if (_mediaCommentArray.count >0) {
            
            return _mediaCommentArray.count;
        }
    }
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == _photoCollectionView) {
        V2_GuideCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"guideCell" forIndexPath:indexPath];
        if (_diningGuideArray.count>0) {
            diningGuide =  [_diningGuideArray objectAtIndex:indexPath.row];
            cell.diningGuideModel = diningGuide;
        }
 
        return cell;

    }else{
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath item]];
        [_journalCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:CellIdentifier];
        
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
        for (UIView *view in [cell.contentView subviews]) {
            [view removeFromSuperview];
        }
        
        //    IMGPhotoModel *model = [self.summary.topPhotoList objectAtIndex:indexPath.row];
     journal = [_mediaCommentArray objectAtIndex:indexPath.row];
       
        UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 124.5, 85)];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        [imageView sd_setImageWithURL:[NSURL URLWithString:[journal.imageUrl returnFullImageUrlWithWidth:124.5 andHeight:85]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
        [cell.contentView addSubview:imageView];
        
        Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(0, imageView.bottom, 124.5, 80) andTextFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR  size:13] andTextColor:[UIColor color222222] andTextLines:4];
        titleLabel.verticalAlignment1 = VerticalAlignmentTop1;
        titleLabel.text = journal.journalTitle;//titleAndDescription;
        titleLabel.textAlignment = NSTextAlignmentLeft;
        [cell.contentView addSubview:titleLabel];
        
        return cell;

    
    
    }
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    NSInteger index = indexPath.row;
    
    if (collectionView == _photoCollectionView) {
        
        IMGDiningGuide *DiningGuide = [_diningGuideArray objectAtIndex:index];
        if ([self.delegate respondsToSelector:@selector(diningGuideImagePressed:)]) {
            [self.delegate diningGuideImagePressed:DiningGuide];
        }
    }else{
        IMGMediaComment *Journal = [_mediaCommentArray objectAtIndex:index];
        if ([self.delegate respondsToSelector:@selector(gotoJournal:)]) {
            [self.delegate gotoJournal:Journal];
        }
        
    }
    
}

-(void)eventImagePressed:(UITapGestureRecognizer *)sender{
    if ([self.delegate respondsToSelector:@selector(eventsImagePressed:)]) {
        [self.delegate eventsImagePressed:sender];
    }
}
//-(void)diningGuidesImagePressed:(UITapGestureRecognizer *)sender{
//    if ([self.delegate respondsToSelector:@selector(diningGuideImagePressed:)]) {
//        [self.delegate diningGuideImagePressed:sender];
//    }
//}
-(void)gotoEvent{
    if ([self.delegate respondsToSelector:@selector(gotoEvents)]) {
        [self.delegate gotoEvents];
    }
}
-(void)gotoArticles{
    if ([self.delegate respondsToSelector:@selector(gotoArticle)]) {
        [self.delegate gotoArticle];
    }
}
//-(void)gotosJournal:(UITapGestureRecognizer*)sender{
//    if ([self.delegate respondsToSelector:@selector(gotoJournal:)]) {
//        [self.delegate gotoJournal:sender];
//    }
//}

@end
