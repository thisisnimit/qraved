//
//  DetailInformationView.h
//  Qraved
//
//  Created by harry on 2017/12/1.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailInformationView : UIView


@property (nonatomic, strong) IMGRestaurant *restaurant;

@end
