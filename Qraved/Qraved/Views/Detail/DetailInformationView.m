//
//  DetailInformationView.m
//  Qraved
//
//  Created by harry on 2017/12/1.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "DetailInformationView.h"

@implementation DetailInformationView
{
    UILabel *lblRestaurantTitle;
    DLStarRatingControl *ratingControl;
    UILabel *lblDistance;
    UILabel *lblInformation;
    UILabel *lblTime;
    UILabel *lblWellKnownFor;
    
    UIImageView *logoImageView;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    
    lblRestaurantTitle = [UILabel new];
    [lblRestaurantTitle setFont:[UIFont boldSystemFontOfSize:24]];
    [lblRestaurantTitle setTextColor:[UIColor color333333]];
    
    logoImageView = [UIImageView new];
    logoImageView.contentMode = UIViewContentModeScaleAspectFill;
    logoImageView.clipsToBounds = YES;
  
    ratingControl=[[DLStarRatingControl alloc]initWithFrame:CGRectMake(LEFTLEFTSET, lblRestaurantTitle.bottom, 83, 15) andStars:5 andStarWidth:15 isFractional:YES spaceWidth:2];
    ratingControl.userInteractionEnabled=NO;
    
    lblDistance = [UILabel new];
    lblDistance.font = [UIFont systemFontOfSize:12];
    lblDistance.textColor = [UIColor color999999];
    
    lblInformation = [UILabel new];
    lblInformation.font = [UIFont systemFontOfSize:12];
    lblInformation.textColor = [UIColor color999999];

    lblTime = [UILabel new];
    lblTime.font = [UIFont systemFontOfSize:12];
    lblTime.textColor = [UIColor color999999];
    
    lblWellKnownFor = [UILabel new];
    lblWellKnownFor.font = [UIFont systemFontOfSize:12];
    lblWellKnownFor.textColor = [UIColor color999999];
    
    [self sd_addSubviews:@[lblRestaurantTitle, logoImageView,ratingControl,lblDistance,lblInformation,lblTime,lblWellKnownFor]];
    
    lblRestaurantTitle.sd_layout
    .topSpaceToView(self, 5)
    .leftSpaceToView(self, 15)
    .rightSpaceToView(self, 60)
    .autoHeightRatio(0);
    
    [lblRestaurantTitle setMaxNumberOfLinesToShow:2];
    
    logoImageView.sd_layout
    .topSpaceToView(self, 15)
    .rightSpaceToView(self, 15)
    .widthIs(45)
    .heightIs(45);
    
    logoImageView.layer.masksToBounds = YES;
    logoImageView.layer.cornerRadius = 22.5;
    
    ratingControl.sd_layout
    .topSpaceToView(lblRestaurantTitle, 7)
    .leftEqualToView(lblRestaurantTitle)
    .widthIs(83)
    .heightIs(15);
    
    lblDistance.sd_layout
    .leftSpaceToView(ratingControl, 3)
    .rightSpaceToView(self, 60)
    .topEqualToView(ratingControl)
    .heightIs(14);
    
    lblInformation.sd_layout
    .topSpaceToView(ratingControl, 7)
    .leftEqualToView(ratingControl)
    .rightSpaceToView(self, 60)
    .heightIs(14);
    
    lblTime.sd_layout
    .topSpaceToView(lblInformation, 7)
    .leftEqualToView(lblInformation)
    .rightSpaceToView(self, 60)
    .heightIs(14);
    
    lblWellKnownFor.sd_layout
    .topSpaceToView(lblTime, 7)
    .leftEqualToView(lblTime)
    .rightSpaceToView(self, 15)
    .heightIs(14);
}

- (void)setRestaurant:(IMGRestaurant *)restaurant{
    _restaurant = restaurant;
    
    lblRestaurantTitle.text = restaurant.title;
    
    if (![Tools isBlankString:restaurant.brandLogo]) {
        logoImageView.hidden = NO;
        [logoImageView sd_setImageWithURL:[NSURL URLWithString:[restaurant.brandLogo returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:nil];
    }else{
        logoImageView.hidden = YES;
    }
    
    ratingControl.rating= [[NSNumber numberWithDouble:[restaurant.ratingScore doubleValue]] getRatingNumber];
    
    NSString *distance = [restaurant distance];
    NSString *ratingStr = @"";
    if (restaurant.ratingCount != nil && ![restaurant.ratingCount isEqual:@0]) {
        ratingStr = [NSString stringWithFormat:@"(%@) •",restaurant.ratingCount];
    }
    
    if (![distance isEqualToString:@""]) {
        lblDistance.text = [NSString stringWithFormat:@"%@ %@",ratingStr, distance];
    }else{
        lblDistance.text = ratingStr;
    }

    
    NSString *dollar=nil;
    switch(restaurant.priceLevel.intValue){
        case 1:dollar=@"Below 100K";break;
        case 2:dollar=@"100K - 200K";break;
        case 3:dollar=@"200K - 300K";break;
        case 4:dollar=@"Start from 300K";break;
    }
 
    NSString *cuisineAndDistrictText;
    if (restaurant.landMarkList.count >0) {
        cuisineAndDistrictText=[NSString stringWithFormat:@"%@ • %@ • %@",restaurant.cuisineName,restaurant.landMarkList[0][@"name"],dollar];
    }else{
        cuisineAndDistrictText=[NSString stringWithFormat:@"%@ • %@ • %@",restaurant.cuisineName,restaurant.districtName,dollar];
    }
    
    lblInformation.text=cuisineAndDistrictText;
    
    
    NSString *status;
    NSString *endStatus;
    NSInteger weekNum=[self returnTodayIsWeekDay];
    NSString* weekDay=[self returnWeekDayWithNumber:weekNum];
    
    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"HH:mm"];
    NSString *  locationString=[dateformatter stringFromDate:senddate];
    NSString* hours=[locationString substringToIndex:2];
    NSRange rang={3,2};
    NSString* min=[locationString substringWithRange:rang];
    int curenttime=[hours intValue]*3600+[min intValue]*60;
    
    if ([restaurant.state intValue]==6||[restaurant.state intValue]==1) {
        
        NSString *closedTime = restaurant.nextclosedTime;
        if ([weekDay isEqualToString:restaurant.nextopenDay]) {
            if ([restaurant.nextopenTime isEqualToString:@"00:00"]&&[restaurant.nextclosedTime isEqualToString:@"00:00"]) {
                closedTime = @"24:00";
                
            }
            NSString *openHour = [restaurant.nextopenTime substringToIndex:2];
            NSString *openMin = [restaurant.nextopenTime substringWithRange:rang];
            NSString *closeHour = [closedTime substringToIndex:2];
            NSString *closeMin = [closedTime substringWithRange:rang];
            
            int openTime = [openHour intValue]*3600+[openMin intValue]*60;
            int closeTime = [closeHour intValue]*3600+[closeMin intValue]*60;
            if (openTime < closeTime) {
                if (openTime <= curenttime && curenttime <= closeTime) {
                    status = @"open";
                    endStatus = @"now";
                }else if (curenttime < openTime){
                    status = @"closed";
                    endStatus = [NSString stringWithFormat:@"• Open at %@",restaurant.nextopenTime];
                }else if (curenttime > closeTime){
                    status = @"closed";
                    endStatus = [NSString stringWithFormat:@"• Open on %@",[restaurant.secondopenDay uppercaseString]];
                }
            }else if (openTime >closeTime){
                if (openTime<=curenttime) {
                    status = @"open";
                    endStatus = @"now";
                }else{
                    status = @"closed";
                    endStatus = [NSString stringWithFormat:@"• Open at %@",restaurant.nextopenTime];
                    
                }
            }else{
                status = @"open";
                endStatus = @"now";
            }
            
        }else{
            status = @"closed";
            endStatus = [NSString stringWithFormat:@"• Open on %@",[restaurant.nextopenDay uppercaseString]];
        }
        
        NSString* lastWeekDay=[self returnWeekDayWithNumber:weekNum-1];
        
        long lastCloseTime;
        if (restaurant.yesteropenDay != nil && [restaurant.yesteropenDay isEqualToString:lastWeekDay]) {
            lastCloseTime = [restaurant.yesterclosedTime longLongValue];
            
            if (curenttime<=lastCloseTime) {
                status = @"open";
                endStatus = @"now";
            }
        }
        
        if ([restaurant.nextopenTime isEqualToString:@""] || [restaurant.nextclosedTime isEqualToString:@""]) {
            status = @"Closed";
            endStatus = @"";
        }
    }else{
        status = @"Closed";
        endStatus = @"";
    }
    
    if ([status isEqualToString:@"open"]) {
        lblTime.textColor = [UIColor colorWithRed:70/255.0f green:163/255.0f blue:131/255.0f alpha:1.0f];
    }else{
        lblTime.textColor = [UIColor color999999];
    }

     lblTime.text = [NSString stringWithFormat:@"%@ %@",[status capitalizedString],[endStatus stringByReplacingOccurrencesOfString:@"00:00" withString:@"24:00"]];
    
    lblWellKnownFor.text = restaurant.wellKnownFor>0?[NSString stringWithFormat:@"%@",restaurant.wellKnownFor]:@"";
    
    if ([Tools isBlankString:restaurant.wellKnownFor]) {
        
        lblWellKnownFor.sd_layout
        .heightIs(0);
    }else{
        
        lblWellKnownFor.sd_layout
        .heightIs(14);
    }
}

-(NSString*)returnWeekDayWithNumber:(NSInteger)number
{
    
    switch (number) {
        case 1:
            return [NSString stringWithFormat:@"Sun"];
            break;
        case 2:
            return [NSString stringWithFormat:@"Mon"];
            break;
        case 3:
            return [NSString stringWithFormat:@"Tue"];
            break;
        case 4:
            return [NSString stringWithFormat:@"Wed"];
            break;
        case 5:
            return [NSString stringWithFormat:@"Thu"];
            break;
        case 6:
            return [NSString stringWithFormat:@"Fri"];
            break;
        case 7:
            return [NSString stringWithFormat:@"Sat"];
            break;
        default:
            break;
    }
    
    
    return nil;
    
    
}

-(NSInteger)returnTodayIsWeekDay{
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *now = [NSDate date];;
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    comps = [calendar components:unitFlags fromDate:now];
    NSInteger weekDay=[comps weekday];
    NSLog(@"today is %ld day ",(long)weekDay);
    return weekDay;
    
}

@end
