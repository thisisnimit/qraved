//
//  DetailMenuView.h
//  Qraved
//
//  Created by apple on 17/3/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"

@protocol DetailMenuViewDelegate <NSObject>

-(void)uploadImageViewPressed;
-(void)menuPhotoimagePressed:(UITapGestureRecognizer*)sender;
-(void)restMenuPhotosCountClick:(UITapGestureRecognizer*)sender;
-(void)gotoMenu:(UIButton*)sender;


@end

@interface DetailMenuView : UIView

@property (nonatomic,strong)NSMutableArray *menuPhotoArray;
@property (nonatomic,weak)id<DetailMenuViewDelegate> delegate;
-(instancetype)initWithPhtosArray:(NSArray*)photoArray andRestaurant:(IMGRestaurant*)restaurant;

@end
