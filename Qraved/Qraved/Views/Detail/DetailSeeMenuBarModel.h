//
//  DetailSeeMenuBarModel.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/11.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DetailSeeMenuBarModel : NSObject

@property (nonatomic, strong)  NSNumber  *myid;
@property (nonatomic, strong)  NSNumber  *sectionId;
@property (nonatomic, strong)  NSString  *sectionName;
@property (nonatomic, strong)  NSString  *imageUrl;
@property (nonatomic, strong)  NSNumber  *sortOrder;
@property (nonatomic, strong)  NSNumber  *chain;
@property (nonatomic, strong)  NSString  *sourceType;
@property (nonatomic, strong)  NSString  *sourceValue;
@property (nonatomic, strong)  NSString  *sourceUrl;
@property (nonatomic, strong)  NSDictionary  *photoCredit;
/*class  restaurantCityName   restaurantSearchTitle  restaurantSeo  restaurantTitle  userAvatar  userName*/
@property (nonatomic, strong)  NSNumber  *userId;
@property (nonatomic, strong)  NSNumber  *createTime;
@property (nonatomic, strong)  NSNumber  *type;
@property (nonatomic, strong)  NSString  *sourceRestaurantSearchTitle;
@property (nonatomic, strong)  NSString  *sourceUserName;
@property (nonatomic, strong)  NSString  *altText;

@end
