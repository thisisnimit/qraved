//
//  Qraved
//
//  Created by Shine Wang on 7/19/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "RestaurantTableViewCell.h"
#import "Label.h"
#import "Constants.h"
#import "Utility.h"
//#import "UIImageView+Cache.h"
#import "UIImageView+WebCache.h"
#import "NSString+Helper.h"
#import "District.h"
#import "Tools.h"
#import "DLStarRatingControl.h"
#import "UIImage+Resize.h"
#import "UILabel+Helper.h"
#import "UIView+Helper.h"
#import "NSNumber+Helper.h"
@implementation RestaurantTableViewCell
int const CELL_WIDTH = 508;
int const CELL_HEIGHT = 70;
int const CELL_START_POSITION_X = 142;
int const CELL_PADDING_LEFT = 15;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
}
-(void)move:(UISwipeGestureRecognizer*)gesture
{
    if(gesture.direction == UISwipeGestureRecognizerDirectionRight)
    {
        [self moveRightAnimate];
    }
    if(gesture.direction == UISwipeGestureRecognizerDirectionLeft)
    {
        [self moveLeftAnimate];
    }
    
}
- (void)moveLeft {
    [self.subView setFrame:CGRectMake(0, 0, CELL_WIDTH, CELL_HEIGHT)];
//    [self.controller cellMoveLeft:self];
}
- (void)moveRight{
    [self.subView setFrame:CGRectMake(125, 0, CELL_WIDTH, CELL_HEIGHT)];
//    [self.controller cellMoveRight:self];
}

-(void)moveRightAnimate {
    [UIView animateWithDuration:0.25 animations:^(){
        
        [self moveRight];
    }];
}

-(void)moveLeftAnimate {
    [UIView animateWithDuration:0.25 animations:^(){
        
        [self moveLeft];
    }];
}

-(void)configByRestaurant:(Restaurant *)restaurant witheventLogo:(NSString*)eventLogo
{
    [self bringSubviewToFront:self.subView];
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    UISwipeGestureRecognizer *rightSwipeGesture=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(move:)];
    [rightSwipeGesture setDirection:UISwipeGestureRecognizerDirectionRight];
    [self addGestureRecognizer:rightSwipeGesture];
    
    UISwipeGestureRecognizer *leftSwipeGesture=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(move:)];
    [leftSwipeGesture setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self addGestureRecognizer:leftSwipeGesture];
    
    UILabel *titleLabel=[[UILabel alloc] init];
    [titleLabel configSearchRestaurantTitle];
    [titleLabel setText:restaurant.title];
    
    [titleLabel setFrame:CGRectMake(94, 5, 176,titleLabel.font.lineHeight)];
    if([restaurant.discount integerValue]==0&&(!restaurant.eventLogo||[restaurant.eventLogo isEqualToString:@""]||restaurant.eventLogo.length<1))
    {
        [titleLabel setFrame:CGRectMake(94, 5,216,titleLabel.font.lineHeight)];
    }
    [self.subView addSubview: titleLabel];
    
    //Cuisine
    NSMutableString *cuisineString=[[NSMutableString alloc]init];
    for(Cuisine *cuisine in restaurant.cuisineList)
    {
        [cuisineString appendFormat:@"%@ ",cuisine.name];
    }
    
    NSMutableString *scoreString=[[NSMutableString alloc]init];
    for(int i=0;i<[restaurant.score intValue];i++)
    {
        [scoreString appendFormat:@"$"];
    }
    
    //append score
    if(scoreString)
    {
        [cuisineString appendFormat:@"/ %@ ",scoreString];
    }
    
    //append district
    if(restaurant.location.district.name!=nil)
    {
        [cuisineString appendFormat:@"/ %@ ", restaurant.location.district.name];
    }
    
    //append distance
    NSString *distancString=[restaurant getDistanceInfoFromMe];
    if(distancString && ![distancString isEqualToString:@"0.0km"])
    {
        [cuisineString appendFormat:@"/ %@ ", distancString];
    }
    
    UILabel *titleCaptionLabel = [Label setTitleCaption:self.cuisineLabel text:[NSString stringWithFormat:@"%@", cuisineString]];
    [titleCaptionLabel setFont:[UIFont fontWithName:DEFAULT_FONT_NAME size:11]];
    //    UILabel *titleCaptionLabel = [Label setTitleCaption:self.cuisineLabel text:[NSString stringWithFormat:@"%@", @"dfdfdfdfdfdfdfdfdfdfddfdfdfdfdfdfdfdfdffdfdfdfdf"]];
    CGSize cuisineSize=[titleCaptionLabel sizeWithWidth:176];
    [titleCaptionLabel setFrame:CGRectMake(94, titleLabel.endPointY+5, 176, cuisineSize.height)];
    [self.subView addSubview: titleCaptionLabel];
    
    if(![Tools isBlankString:restaurant.imageUrl])
    {
        [self.restaurantImage setImage:[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(69, 69)]];
        
        [self.restaurantImage setImageUrl:restaurant.imageUrl andWidth:69];
    }
    else
    {
        [self.restaurantImage setImage:[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(69, 69)]];
    }
    
    
    //    DLStarRatingControl *ratingScore=[[DLStarRatingControl alloc]initWithFrame:CGRectMake([Tools viewEndPointX:self.restaurantImage]+CELL_CONTENT_OFFSET, [Tools viewEndPointY:titleCaptionLabel]+CELL_CONTENT_OFFSET, 60, 12) andStars:5 andStarWidth:12 isFractional:NO];
    
    //    CGFloat ratingY=titleCaptionLabel.frame.size.height>titleCaptionLabel.font.lineHeight?[Tools viewEndPointY:titleCaptionLabel]+2:[Tools viewEndPointY:titleCaptionLabel]+10;
    DLStarRatingControl *ratingScore=[[DLStarRatingControl alloc]initWithFrame:CGRectMake(94, self.frame.size.height-RATING_STAR_WIDTH-10, RATING_STAR_WIDTH*5, RATING_STAR_WIDTH) andStars:5 andStarWidth:RATING_STAR_WIDTH isFractional:YES];
    ratingScore.rating= [[NSNumber numberWithDouble:[restaurant.ratingScore doubleValue]] getRatingNumber];
    ratingScore.userInteractionEnabled=NO;
    [self.subView addSubview:ratingScore];
    
    float eatjtkY =2;
    if(![restaurant.discount integerValue]==0)
    {
        [self.discountLabel setText:[NSString stringWithFormat:@"%@%@",restaurant.discount,@"%"]];
        [self.discountView setHidden:NO];
        eatjtkY = eatjtkY + 38;
    }
    else
    {
        [self.discountView setHidden:YES];
    }
//    restaurant.eventLogo&& restaurant.eventLogo!=(NSString*)[NSNull null] && [[restaurant.eventLogo stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]!=0
    if (eventLogo) {
        if (eventLogo.length>1) {
            self.eatjtkImg.hidden = NO;
            self.eatjtkImg.frame = CGRectMake(279, eatjtkY, 27, 20);
            
            NSString *urlString = [eventLogo returnFullImageUrlWithWidth:20];
            if (urlString && ![urlString isEqualToString:[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:@"&width=640.000000"]]) {
                [self.eatjtkImg setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) { }];
            }
        }
        else
        {
            self.eatjtkImg.hidden = YES;
        }
    }else{
        self.eatjtkImg.hidden = YES;
    }
    
    self.qravesCountLabel = [Label setStat:self.qravesCountLabel text:[NSString stringWithFormat:@"%@", restaurant.qravesCount]];
    [self.subView addSubview: self.qravesCountLabel];
    
    self.reviewCountLabel = [Label setStat:self.reviewCountLabel text:[NSString stringWithFormat:@"%@", restaurant.reviewCount]];
    [self.subView addSubview: self.reviewCountLabel];
}

@end
