//
//  RestaurantJournalViewCell.m
//  Qraved
//
//  Created by System Administrator on 12/30/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import "RestaurantJournalViewCell.h"
#import "Label.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"
#import "UIConstants.h"
#import "MoreArticleViewController.h"
#import "UIImage+Resize.h"

@implementation RestaurantJournalViewCell{
	Label *titleLabel;
	UIImageView *imageView;
	TYAttributedLabel *contentLabel;
	UIImage *placeHolderImage;
	UIFont *titleFont;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
	if(self=[super initWithStyle:style reuseIdentifier:identifier]){
        self.selectionStyle=UITableViewCellSelectionStyleNone;
		titleFont=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:13];
		titleLabel=[[Label alloc] initWithFrame:CGRectZero andTextFont:titleFont andTextColor:[UIColor color222222] andTextLines:0];
		imageView=[[UIImageView alloc] init];
		contentLabel=[[TYAttributedLabel alloc] init];
//        contentLabel.backgroundColor = [UIColor purpleColor];
		[self addSubview:titleLabel];
		[self addSubview:imageView];
		[self addSubview:contentLabel];
        placeHolderImage = [UIImage imageNamed:DEFAULT_IMAGE_STRING2];
	}
	return self;
}

-(void)callLayoutSubviews{

	imageView.frame=CGRectMake(LEFTLEFTSET,  0, DeviceWidth-2*LEFTLEFTSET, DeviceWidth/3);
    __weak typeof(imageView) weakImage = imageView;
    UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, DeviceWidth/3)];

//    [imageView setImageWithURL:[NSURL URLWithString:[self.imageUrl returnFullImageUrlWithWidth:DeviceWidth-2*LEFTLEFTSET]] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//        image = [image imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, DeviceWidth/3)];
//        
//        [weakImage setImage:image];
//
//    }];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[self.imageUrl returnFullImageUrlWithWidth:DeviceWidth-2*LEFTLEFTSET]] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        image = [image imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, DeviceWidth/3)];
                [weakImage setImage:image];
    }];
    imageView.userInteractionEnabled = YES;
    
    CGSize size = [self.title sizeWithFont:titleFont constrainedToSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 100)];
	titleLabel.frame =CGRectMake(LEFTLEFTSET, imageView.endPointY+5, DeviceWidth-2*LEFTLEFTSET, size.height);
    titleLabel.text = self.title;
    titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    
    NSMutableString *titleAndDescription = [NSMutableString stringWithString:self.contents] ;
    if(self.link){
    	[titleAndDescription appendFormat:@", %@",self.link];	
    }
    CGSize titleSize = [titleAndDescription sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_ITALIC size:14] constrainedToSize:CGSizeMake(DeviceWidth-30, 100) lineBreakMode:NSLineBreakByWordWrapping];
    contentLabel.frame =CGRectMake(15, titleLabel.endPointY+5, DeviceWidth-30, titleSize.height);
    contentLabel.text = titleAndDescription;
    contentLabel.textColor = [UIColor color333333];
    contentLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14];
    
    NSMutableAttributedString *AttributedString = [[NSMutableAttributedString alloc] initWithString:titleAndDescription];
    NSRange range = [titleAndDescription rangeOfString:[NSString stringWithFormat:@"%@",self.link]];
    [AttributedString addAttributes:@{NSForegroundColorAttributeName:[UIColor color6F6F6F],NSFontAttributeName:[UIFont fontWithName:FONT_OPEN_SANS_ITALIC size:12]} range:NSMakeRange(range.location, range.length)];
    contentLabel.attributedText = AttributedString;
    contentLabel.verticalAlignment = TYVerticalAlignmentCenter;
}

@end
