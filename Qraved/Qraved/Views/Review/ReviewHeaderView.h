//
//  ReviewHeaderView.h
//  Qraved
//
//  Created by Laura on 14-9-19.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewHeaderView : UIImageView
@property (nonatomic,retain)UIButton *closeButton;
@property (nonatomic,retain)UIButton *postButton;


- (id)initWithTitle:(NSString*)title andIsNeedWriteReview:(BOOL)isNeedWriteReview;
@end
