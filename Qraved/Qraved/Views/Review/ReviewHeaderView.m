//
//  ReviewHeaderView.m
//  Qraved
//
//  Created by Laura on 14-9-19.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#define navigationHeight  (44+[UIDevice heightDifference])

#import "ReviewHeaderView.h"

#import "UIConstants.h"

#import "UIDevice+Util.h"
#import "Label.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"

@implementation ReviewHeaderView

- (id)initWithTitle:(NSString *)title andIsNeedWriteReview:(BOOL)isNeedWriteReview
{
    self= [super init];
    if(self){
        self.frame  =CGRectMake(0, [UIDevice heightDifference], DeviceWidth, 45);
        self.backgroundColor = [UIColor whiteColor];
        self.userInteractionEnabled = YES;
        self.image =[UIImage imageNamed:@"TopBar"];
        
        if (isNeedWriteReview)
        {
            _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [_closeButton setImage:[UIImage imageNamed:@"blackBack"] forState:UIControlStateNormal];
            //[_closeButton setImage:[UIImage imageNamed:@"blackBack"] forState:UIControlStateNormal];
            //_closeButton.frame = CGRectMake(LEFTLEFTSET, 10, 11, 22);
            _closeButton.frame = CGRectMake(-50, navigationHeight-65, 140,48);

            
            Label *headeLabel = [[Label alloc]initWithFrame:CGRectMake(50, 0, DeviceWidth-105, 45) andTextFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:16] andTextColor:[UIColor color222222] andTextLines:1];
            headeLabel.textAlignment = NSTextAlignmentCenter;
            headeLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            headeLabel.text = title;
            [self addSubview:headeLabel];
            
            [self insertSubview:_closeButton aboveSubview:headeLabel];
            
            _postButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [_postButton setTitle:L(@"Post") forState:UIControlStateNormal];
            _postButton.titleLabel.font = [UIFont systemFontOfSize:13];
            [_postButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            _postButton.frame = CGRectMake(DeviceWidth - 105, 0, 140, 44);
            [self insertSubview:_postButton aboveSubview:headeLabel];

        }
        else
        {
            Label *headeLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 0, DeviceWidth-LEFTLEFTSET-40, 45) andTextFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:16] andTextColor:[UIColor color222222] andTextLines:1];
            headeLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            headeLabel.text = title;
            [self addSubview:headeLabel];
            
            _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [_closeButton setImage:[UIImage imageNamed:@"ReviewClosed"] forState:UIControlStateNormal];
            _closeButton.frame = CGRectMake(DeviceWidth - 40, 0, 40, 44);
            [self insertSubview:_closeButton aboveSubview:headeLabel];
        }
        
        UIImageView *lineImageView1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, self.endPointY, DeviceWidth, 1)];
        lineImageView1.backgroundColor = [UIColor colorE4E4E4];
        [self addSubview:lineImageView1];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
