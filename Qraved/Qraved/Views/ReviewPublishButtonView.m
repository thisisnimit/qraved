//
//  ReviewPublishButtonView.m
//  Qraved
//
//  Created by apple on 17/4/1.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "ReviewPublishButtonView.h"
#import "UIConstants.h"

@implementation ReviewPublishButtonView

-(instancetype)init{
    if(self = [super init]){
        self.backgroundColor = [UIColor colorWithRed:241/255.0 green:241/255.0 blue:241/255.0 alpha:1];
        self.frame = CGRectMake(0, DeviceHeight-19-64, DeviceWidth, 39);
        //    if (_photosArrM.count>0&&[UIDevice isIphone5]) {
        //        buttonBtnsView.frame = CGRectMake(0, _tableView.endPointY-20, DeviceWidth, DeviceHeight - _tableView.endPointY + 40);
        //    }
        UIButton *buttonAddPhotoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonAddPhotoBtn setImage:[UIImage imageNamed:@"upload_review_photo"] forState:UIControlStateNormal];
        buttonAddPhotoBtn.frame = CGRectMake(22, self.frame.size.height - 27, 22, 18);
        [self addSubview:buttonAddPhotoBtn];
        [buttonAddPhotoBtn addTarget:self action:@selector(addPhotoClicks) forControlEvents:UIControlEventTouchUpInside];
        
        UILabel* addPhotoLabel=[[UILabel alloc]initWithFrame:CGRectMake(buttonAddPhotoBtn.endPointX+5, buttonAddPhotoBtn.startPointY, 150, 18)];
        addPhotoLabel.text=@"Upload Photo";
        addPhotoLabel.font=[UIFont systemFontOfSize:16];
        UIControl* addphoto=[[UIControl alloc]init];
        [addphoto addTarget:self action:@selector(addPhotoClicks) forControlEvents:UIControlEventTouchUpInside];
        addphoto.frame=addPhotoLabel.frame;
        
        [self addSubview:addphoto];
        [self addSubview:addPhotoLabel];
        
        _buttonCharNoticeLabel=[[UILabel alloc] initWithFrame:CGRectMake(50,self.frame.size.height-27,DeviceWidth-50-LEFTLEFTSET,18)];
        _buttonCharNoticeLabel.textColor=[UIColor grayColor];
        _buttonCharNoticeLabel.font=[UIFont systemFontOfSize:12];
        _buttonCharNoticeLabel.textAlignment=NSTextAlignmentRight;
        [self addSubview:_buttonCharNoticeLabel];

    
    
    
    }
    return self;
}
-(void)addPhotoClicks{
    if ([self.delegate respondsToSelector:@selector(addPhotoClick)]) {
        [self.delegate addPhotoClick];
    }
}
@end
