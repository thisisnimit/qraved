//
//  JournalImageBtn.m
//  Qraved
//
//  Created by System Administrator on 9/7/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "JournalImageBtn.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "UILabel+Helper.h"
#import "AppDelegate.h"

@implementation JournalImageBtn

-(instancetype)initWithFrame:(CGRect)frame image:(NSString*)name text:(NSString*)text margin:(NSInteger)margin target:(id)target action:(SEL)selector{
	if(self=[super initWithFrame:frame]){
		self.imageName=name;
		[self setImage:[UIImage imageNamed:name]];
		self.imageSize=self.image.size;
		self.text=text;
		self.margin=margin;
		self.userInteractionEnabled=YES;
		UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:target action:selector];
		tap.numberOfTapsRequired=1;
		tap.numberOfTouchesRequired=1;
		[self addGestureRecognizer:tap];
		[self drawContent];
	}	
	return self;
}

-(instancetype)initWithFrame:(CGRect)frame image:(NSString*)name size:(CGSize)size text:(NSString*)text margin:(NSInteger)margin target:(id)target action:(SEL)selector{
	if(self=[super initWithFrame:frame]){
		self.imageName=name;
		self.image=[UIImage imageNamed:name];
		self.imageSize=size;
		self.text=text;
		self.margin=margin;
		self.userInteractionEnabled=YES;
		UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:target action:selector];
		tap.numberOfTapsRequired=1;
		tap.numberOfTouchesRequired=1;
		[self addGestureRecognizer:tap];
		[self drawContent];
	}	
	return self;
}

-(void)drawContent{
	self.font=[UIFont fontWithName:@"OpenSans" size:12];
	_button=[[UIButton alloc] initWithFrame:CGRectMake(0+_padding_left,(self.frame.size.height-_imageSize.height)/2,_imageSize.width,_imageSize.height)];
	[_button setImage:_image forState:UIControlStateNormal];
	_button.userInteractionEnabled=NO;
	[self addSubview:_button];

	CGFloat margin=_padding_left+_padding_right;

	if(_text && _text.length){
		_label=[[UILabel alloc] initWithFrame:CGRectMake(_button.endPointX+2,0,DeviceWidth-LEFTLEFTSET*2,self.frame.size.height)];
		_label.text=_text;
		_label.textColor=[UIColor color6B6B6B];
		_label.font=self.font;
		_label.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
		_label.frame=CGRectMake(_button.endPointX+self.margin,0,_label.expectedWidth,self.frame.size.height);
		_label.userInteractionEnabled=YES;
		[self addSubview:_label];
		margin+=_label.frame.size.width+self.margin;
	}
	
	self.backgroundColor=[UIColor colorEEEEEE];
	self.layer.cornerRadius=10.0f;
	[self addObserver:self forKeyPath:@"text" options:NSKeyValueObservingOptionNew context:nil];
	[self addObserver:self forKeyPath:@"font" options:NSKeyValueObservingOptionNew context:nil];
	[self addObserver:self forKeyPath:@"imageName" options:NSKeyValueObservingOptionNew context:nil];
	[self addObserver:self forKeyPath:@"padding_left" options:NSKeyValueObservingOptionNew context:nil];
	[self addObserver:self forKeyPath:@"padding_right" options:NSKeyValueObservingOptionNew context:nil];

	self.frame=CGRectMake(self.frame.origin.x,self.frame.origin.y,_button.frame.size.width+margin,self.frame.size.height);
}

-(void)observeValueForKeyPath:(NSString*)keypath ofObject:(id)object change:(NSDictionary*)change context:(void*)context{
	if([keypath isEqualToString:@"text"]){
		_label.text=[change objectForKey:@"new"];
	}else if([keypath isEqualToString:@"font"]){
		_label.font=[change objectForKey:@"new"];
	}else if([keypath isEqualToString:@"imageName"]){
		[self.button setImage:[UIImage imageNamed:[change objectForKey:@"new"]] forState:UIControlStateNormal];
	}else if([keypath isEqualToString:@"padding_left"] || [keypath isEqualToString:@"padding_right"]){
        _button.frame=CGRectMake(_padding_left,(self.frame.size.height-_imageSize.height)/2,_imageSize.width,_imageSize.height);
	}
	CGFloat margin=_padding_left+_padding_right;
	if(_label){
		_label.frame=CGRectMake(_button.endPointX+2,0,DeviceWidth-LEFTLEFTSET*2,self.frame.size.height);
		_label.frame=CGRectMake(_button.endPointX+2,0,_label.expectedWidth,self.frame.size.height);
		margin+=_label.frame.size.width+2;
	}
    self.frame=CGRectMake(self.frame.origin.x,self.frame.origin.y,_button.frame.size.width+margin,self.frame.size.height);
}

-(void)dealloc{
	[self removeObserver:self forKeyPath:@"text"];
	[self removeObserver:self forKeyPath:@"font"];
	[self removeObserver:self forKeyPath:@"imageName"];
	[self removeObserver:self forKeyPath:@"padding_left"];
	[self removeObserver:self forKeyPath:@"padding_right"];
}

@end
