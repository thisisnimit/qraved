//
//  ProfileUpcomingViewCell.h
//  Qraved
//
//  Created by System Administrator on 10/20/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProfileUpcomingViewCell : UICollectionViewCell

@property(nonatomic,strong) NSDictionary *upcomingBook;

@end
