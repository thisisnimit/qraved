//
//  PersonalSummaryTopReviewTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/6/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGSummary.h"

@interface PersonalSummaryTopReviewTableViewCell : UITableViewCell

@property (nonatomic,strong) IMGSummary *summary;

@property (nonatomic,assign) CGFloat cellHeight;
@end
