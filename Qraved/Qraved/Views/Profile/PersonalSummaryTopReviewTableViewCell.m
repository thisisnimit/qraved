//
//  PersonalSummaryTopReviewTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/6/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "PersonalSummaryTopReviewTableViewCell.h"
#import "ReviewHandler.h"
#import "HomeUtil.h"
#import "IMGJourneyReview.h"
#import "DLStarRatingControl.h"

#define LINK_TEXT @"<a style=\"text-decoration:none;color:#3A8CE2;font-family:'OpenSans'\" href=\"http://www.qraved.com/\">...read more</a>"
#define LEFTMARGIN 15
#define TEXTLABELTOPMARGIN 35
#define RIGHTMARGIN 30
#define TEXTLABELLEFTMARGIN 11+PROFILEIMAGEVIEWSIZE+8
#define PROFILEIMAGEVIEWSIZE 20
#define CONTENTLABELWIDTH DeviceWidth-LEFTMARGIN-PROFILEIMAGEVIEWSIZE-RIGHTMARGIN

@implementation PersonalSummaryTopReviewTableViewCell
{
    UIView *currentView;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setSummary:(IMGSummary *)summary{
    _summary = summary;
    
    UILabel * titleLabel = [UILabel new];
    titleLabel.text = @"Top Reviews";
    titleLabel.textColor = [UIColor color333333];
    titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [self.contentView addSubview:titleLabel];
    
    titleLabel.sd_layout
    .topSpaceToView(self.contentView, 40)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(17);
    
    currentView = titleLabel;
    for (PersonalSummaryTopReviewModel *review in summary.topReviews) {
        UIView *reviewView = [UIView new];
        [self.contentView addSubview:reviewView];
        
        UIImageView *vimgReview = [UIImageView new];
        vimgReview.contentMode = UIViewContentModeScaleAspectFill;
        vimgReview.clipsToBounds = YES;
        
        if (![Tools isBlankString:review.imageUrl]) {
            [vimgReview sd_setImageWithURL:[NSURL URLWithString:[review.imageUrl returnCurrentImageString]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
        }else{
            [vimgReview sd_setImageWithURL:[NSURL URLWithString:[[review.mini1Restaurant objectForKey:@"imageUrl"]  returnCurrentImageString]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
        }
        
        UILabel *lblName = [UILabel new];
        lblName.font = [UIFont systemFontOfSize:14];
        lblName.textColor = [UIColor color333333];
        lblName.text = review.targetTitle;
        
        UILabel *lblAddress = [UILabel new];
        lblAddress.font = [UIFont systemFontOfSize:12];
        lblAddress.textColor = [UIColor color999999];
        NSString *cuisineStr = @"";
        NSString *districtStr = @"";
        if (review.mini1Restaurant != nil) {
            NSArray *cuisineArr = [review.mini1Restaurant objectForKey:@"cuisineList"];
            NSDictionary *districtDic = [[review.mini1Restaurant objectForKey:@"location"] objectForKey:@"district"];
            
            if (cuisineArr.count > 0) {
                cuisineStr = [[cuisineArr objectAtIndex:0] objectForKey:@"name"];
            }
            districtStr = [districtDic objectForKey:@"name"] ? [districtDic objectForKey:@"name"] : @"";
        }
        
        lblAddress.text = [NSString stringWithFormat:@"%@ • %@",cuisineStr, districtStr];
        
        DLStarRatingControl *ratingControl = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(LEFTLEFTSET, 0, 73, 13) andStars:5 andStarWidth:12 isFractional:YES spaceWidth:2];
        [ratingControl updateRating:[NSNumber numberWithDouble:[review.score doubleValue]]];
        
        UILabel *lblDate = [UILabel new];
        lblDate.font = [UIFont systemFontOfSize:12];
        lblDate.textColor = [UIColor color999999];
        lblDate.textAlignment = NSTextAlignmentRight;
        lblDate.text = ![Tools isBlankString:review.createTimeStr]?[NSString stringWithFormat:@"%@",[Date dateTimeChangeDateFormart:review.createTimeStr]]:@"";
        
        UILabel *lblDes = [UILabel new];
        lblDes.font = [UIFont systemFontOfSize:12];
        lblDes.textColor = [UIColor color999999];
        lblDes.numberOfLines = 0;
        lblDes.text = review.summarize;
        
        [reviewView sd_addSubviews:@[vimgReview, lblName, lblAddress, ratingControl, lblDate, lblDes]];
        
        reviewView.sd_layout
        .topSpaceToView(currentView, 15)
        .leftSpaceToView(self.contentView, 0)
        .rightSpaceToView(self.contentView, 0)
        .autoHeightRatio(0);
        
        vimgReview.sd_layout
        .topSpaceToView(reviewView, 0)
        .leftSpaceToView(reviewView, 15)
        .widthIs(40)
        .heightIs(40);
        
        lblName.sd_layout
        .topSpaceToView(reviewView, 0)
        .leftSpaceToView(vimgReview, 5)
        .rightSpaceToView(reviewView, 15)
        .heightIs(16);
        
        lblAddress.sd_layout
        .topSpaceToView(lblName, 4)
        .leftEqualToView(lblName)
        .rightEqualToView(lblName)
        .heightIs(14);
        
        ratingControl.sd_layout
        .topSpaceToView(vimgReview, 10)
        .leftEqualToView(vimgReview)
        .widthIs(73)
        .heightIs(13);
        
        lblDate.sd_layout
        .topSpaceToView(vimgReview, 10)
        .rightSpaceToView(reviewView, 15)
        .leftSpaceToView(ratingControl, 15)
        .heightIs(14);
        
        lblDes.sd_layout
        .topSpaceToView(ratingControl, 10)
        .leftSpaceToView(reviewView, 15)
        .rightSpaceToView(reviewView, 15)
        .autoHeightRatio(0);
        
        [reviewView setupAutoHeightWithBottomView:lblDes bottomMargin:0];
        currentView = reviewView;
    }
    [self setupAutoHeightWithBottomView:currentView bottomMargin:20];
}




@end
