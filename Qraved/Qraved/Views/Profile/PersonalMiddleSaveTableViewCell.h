//
//  PersonalMiddleSaveTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/6/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGJourneySave.h"

@protocol PersonalMiddleSaveTableViewCellDelegate <NSObject>

//头像
- (void)saveIconImageViewTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path save:(IMGJourneySave *)model;
//write review
- (void)didwriteClickEditBtn:(IMGRestaurant *)restaurant andReviewId:(NSNumber*)reviewId indexPath:(NSIndexPath *)indexPath;
@end

@interface PersonalMiddleSaveTableViewCell : UITableViewCell

@property (nonatomic, strong)  UILabel  *lblSave;
@property (nonatomic, strong)  UILabel  *lblDate;
@property (nonatomic, strong)  UIButton  *iconImageView;
@property (nonatomic, strong)  UILabel  *lbluserName;
@property (nonatomic, strong)  UILabel  *lblTitle;
@property (nonatomic, strong)  UIButton  *btnwhiteReview;

@property (nonatomic, strong)  UILabel  *lbldetail;

@property (nonatomic, strong)  IMGJourneySave  *model;

@property (nonatomic, weak)  id<PersonalMiddleSaveTableViewCellDelegate> Delegate;
@property(nonatomic,strong) NSIndexPath *fromCellIndexPath;

@end
