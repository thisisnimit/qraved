//
//  AccountView.m
//  Qraved
//
//  Created by System Administrator on 10/19/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "AccountView.h"
#import "UIConstants.h"
#import "UIView+Helper.h"

@implementation AccountView

-(instancetype)initWithFrame:(CGRect)frame withOtherUser:(BOOL)isOtherUser{
	if([super initWithFrame:frame]){
        self.isOtherUser = isOtherUser;
		[self setAccount];
	}
	return self;
}

-(void)setAccount{
    
	NSDictionary *reward=@{@"image":@"profileReward",@"name":L(@"Rewards")};
    NSDictionary *feedback=@{@"image":@"myprofile_account_feedback",@"name":L(@"Send Feedback")};

	NSDictionary *setting=@{@"image":@"profileSetting",@"name":L(@"Settings")};

    NSArray *menus;

    menus = @[feedback,reward, setting];

	UIView *container=nil;
	UIImageView *imageView=nil;
	UILabel *name=nil;
	UITapGestureRecognizer *tap=nil;

	CGFloat margin=10;
	CGFloat currentY=0;
	CGFloat height=50;
	int i=0;

	for(NSDictionary *menu in menus){

		container=[[UIView alloc] initWithFrame:CGRectMake(0,currentY,DeviceWidth,height)];
		container.userInteractionEnabled=YES;
		container.tag=i++;

		tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(redirect:)];
		[container addGestureRecognizer:tap];

		imageView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:[menu objectForKey:@"image"]]];
		imageView.frame=CGRectMake(LEFTLEFTSET,(height-16)/2,16,16);

		name=[[UILabel alloc] initWithFrame:CGRectMake(imageView.endPointX+10,margin,DeviceWidth-LEFTLEFTSET*2-50,30)];
		name.text=[menu objectForKey:@"name"];

		UIImageView *lineImage = [[UIImageView alloc] initWithFrame:CGRectMake(imageView.endPointX+10, name.endPointY+margin, DeviceWidth-imageView.endPointX, 1)];
	    lineImage.image = [UIImage imageNamed:@"CutOffRule"];
	    
		[container addSubview:imageView];
		[container addSubview:name];
		[container addSubview:lineImage];
		[self addSubview:container];

		currentY+=height;
	}
}

-(void)redirect:(UITapGestureRecognizer*)tap{
	if(self.delegate && [self.delegate respondsToSelector:@selector(menuClick:)]){
		[self.delegate menuClick:tap.view.tag];
	}
}

@end
