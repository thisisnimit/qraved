//
//  PersonalMiddleAddPictureTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/6/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "PersonalMiddleAddPictureTableViewCell.h"

@implementation PersonalMiddleAddPictureTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}
- (void)initUI{
    
    _iconImageView = [UIImageView new];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(iconImageViewClick:)];
    [_iconImageView addGestureRecognizer:tap];
    
    _lblUserName = [UILabel new];
    _lblUserName.text = @"Pepenero";
    _lblUserName.textColor = [UIColor color333333];
    _lblUserName.font = [UIFont systemFontOfSize:14];
    
    _lblTittle = [UILabel new];
    _lblTittle.text = @"Western • Senopati";
    _lblTittle.textColor = [UIColor lightGrayColor];
    _lblTittle.font = [UIFont systemFontOfSize:12];
    
    _btnEdit = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnEdit setTitle:@"Edit" forState:UIControlStateNormal];
    [_btnEdit setTitleColor:[UIColor colortextBlue] forState:UIControlStateNormal];
    _btnEdit.titleLabel.font = [UIFont systemFontOfSize:12];
    [_btnEdit addTarget:self action:@selector(btnEditClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _starR = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(15, 0, 73, 13) andStars:5 andStarWidth:12 isFractional:YES spaceWidth:2];
    _starR.centerY = _lbldate.centerY;
    [_starR setHighlightedStar:[UIImage imageNamed:@"starImage"]];
    [_starR updateRating:[NSNumber numberWithDouble: 4]];//[summary.ratingSum doubleValue]/[summary.ratingCount doubleValue]
    [self.contentView addSubview:_starR];
    
    _lbldate = [UILabel new];
    _lbldate.text = @"Jan 12, 2017";
    _lbldate.textColor = [UIColor color999999];
    _lbldate.font = [UIFont systemFontOfSize:12];
    
    _lblDetail = [UILabel new];
    _lblDetail.text = @"Nice Food This is Title. H Gourmet and Vibes takes up the challenge of introducing The Big Apple’s flavor groove to The Big Durian. And then for… read more";
    _lblDetail.textColor = [UIColor lightGrayColor];
    _lblDetail.font = [UIFont systemFontOfSize:12];
    
    _btnAddPicture = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnAddPicture setImage:[UIImage imageNamed:@"addphotobtn@2x"] forState:UIControlStateNormal];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    
    _bottomView = [UIView new];
    _bottomView.backgroundColor = [UIColor whiteColor];
    
    _btnLikes = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnLikes setTitle:@"123 Likes" forState:UIControlStateNormal];
    [_btnLikes setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
    [_btnLikes setImage:[UIImage imageNamed:@"like_btn"] forState:UIControlStateNormal];
    _btnLikes.titleLabel.font = [UIFont systemFontOfSize:12];
    _btnLikes.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    [_btnLikes addTarget:self action:@selector(btnLikesClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _btnComments = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnComments setTitle:@"123 Comments" forState:UIControlStateNormal];
    [_btnComments setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
    [_btnComments setImage:[UIImage imageNamed:@"comment_btn"] forState:UIControlStateNormal];
    _btnComments.titleLabel.font = [UIFont systemFontOfSize:12];
    _btnComments.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    [_btnComments addTarget:self action:@selector(btnCommentsClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _btnShare = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnShare setTitle:@"123 Share" forState:UIControlStateNormal];
    [_btnShare setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
    [_btnShare setImage:[UIImage imageNamed:@"share_btn"] forState:UIControlStateNormal];
    _btnShare.titleLabel.font = [UIFont systemFontOfSize:12];
    _btnShare.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    [_btnShare addTarget:self action:@selector(btnShareClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [_bottomView sd_addSubviews:@[_btnLikes, _btnComments, _btnShare]];
    [self.contentView sd_addSubviews:@[_iconImageView, _lblUserName, _lblTittle, _btnEdit, _lbldate, _lblDetail, _btnAddPicture,  lineView, _bottomView]];
    
    _iconImageView.sd_layout
    .topSpaceToView(self.contentView, 15)
    .leftSpaceToView(self.contentView, 15)
    .heightIs(40)
    .widthIs(40);
    
    _lblUserName.sd_layout
    .topSpaceToView(self.contentView, 20)
    .leftSpaceToView(_iconImageView, 15)
    .heightIs(20);
    [_lblUserName setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 200];
    
    _lblTittle.sd_layout
    .topSpaceToView(_lblUserName, 0)
    .leftEqualToView(_lblUserName)
    .heightIs(20);
    [_lblTittle setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 200];
    
    _btnEdit.sd_layout
    .rightSpaceToView(self.contentView, 15)
    .centerYEqualToView(_iconImageView)
    .heightIs(20)
    .widthIs(25);
    
    _lbldate.sd_layout
    .topSpaceToView(_iconImageView, 10)
    .rightSpaceToView(self.contentView, 15)
    .autoHeightRatio(0);
    [_lbldate setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
    _starR.sd_layout
    .topSpaceToView(_iconImageView, 10)
    .leftSpaceToView(self.contentView , 15)
    .widthIs(73)
    .heightIs(13);
    
    _lblDetail.sd_layout
    .topSpaceToView(_starR, 10)
    .leftSpaceToView(self.contentView, 15)
    .autoHeightRatio(0);
    [_lblDetail setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 30];
    
    _btnAddPicture.sd_layout
    .topSpaceToView(_lblDetail, 10)
    .widthIs(92)
    .heightIs(20)
    .centerXEqualToView(_lblDetail);
    
    lineView.sd_layout
    .topSpaceToView(_btnAddPicture, 10)
    .leftSpaceToView(self.contentView, 15)
    .widthIs(DeviceWidth - 30)
    .heightIs(1);
    
    _bottomView.sd_layout
    .topSpaceToView(lineView, 0)
    .leftSpaceToView(self.contentView, 0)
    .widthIs(DeviceWidth)
    .heightIs(40);
    
    _btnLikes.sd_layout
    .topSpaceToView(_bottomView, 0)
    .leftSpaceToView(_bottomView, 0)
    .widthIs(DeviceWidth/3)
    .heightIs(40);
    
    _btnComments.sd_layout
    .topSpaceToView(_bottomView, 0)
    .leftSpaceToView(_btnLikes, 0)
    .widthIs(DeviceWidth/3)
    .heightIs(40);
    
    _btnShare.sd_layout
    .topSpaceToView(_bottomView, 0)
    .leftSpaceToView(_btnComments, 0)
    .widthIs(DeviceWidth/3)
    .heightIs(40);
    
    
//    [self.contentView setupAutoHeightWithBottomView:_bottomView bottomMargin:10];
    
}
-(void)iconImageViewClick:(UITapGestureRecognizer *)iconImageView{
    
    NSLog(@"iconImageView");
}

- (void)btnEditClick:(UIButton *)btnEdit{
    
    NSLog(@"edit");
}
- (void)btnLikesClick:(UIButton *)btnLikes{
    
    NSLog(@"like");
}
- (void)btnCommentsClick:(UIButton *)btnComments{
    
    NSLog(@"comments");
}
- (void)btnShareClick:(UIButton *)btnShare{
    
    NSLog(@"share");
}

@end
