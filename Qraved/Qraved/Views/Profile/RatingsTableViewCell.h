//
//  RatingsTableViewCell.h
//  Qraved
//
//  Created by Gary.yao on 2017/6/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGSummary.h"

@interface RatingsTableViewCell : UITableViewCell


- (void)createUIWithModel:(IMGSummary*)summary;
@end
