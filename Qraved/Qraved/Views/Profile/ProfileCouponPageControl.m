//
//  ProfileCouponPageControl.m
//  Qraved
//
//  Created by harry on 2018/1/31.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "ProfileCouponPageControl.h"

@implementation ProfileCouponPageControl

- (void) setCurrentPage:(NSInteger)page {
    
    [super setCurrentPage:page];
    
    for (NSUInteger subviewIndex = 0; subviewIndex < [self.subviews count]; subviewIndex++) {
        
        if (subviewIndex == page)
            
        {
            UIImageView* subview = [self.subviews objectAtIndex:subviewIndex];
            
            CGSize size = CGSizeMake(9, 9);
            
            [subview setFrame:CGRectMake(subview.frame.origin.x, subview.frame.origin.y,
                                         
                                         size.width,size.height)];
        }else{
            UIImageView* subview = [self.subviews objectAtIndex:subviewIndex];
            
            CGSize size = CGSizeMake(7, 7);
            
            [subview setFrame:CGRectMake(subview.frame.origin.x, subview.frame.origin.y,
                                         
                                         size.width,size.height)];
        }
    }
}

@end
