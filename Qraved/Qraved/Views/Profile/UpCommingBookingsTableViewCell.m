//
//  UpCommingBookingsTableViewCell.m
//  Qraved
//
//  Created by Laura on 14-9-10.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "UpCommingBookingsTableViewCell.h"
#import "UIImageView+Helper.h"
#import "NSString+Helper.h"
#import "BookUtil.h"

@implementation UpCommingBookingsTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        _titleLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 0, DeviceWidth-2*LEFTLEFTSET, NAVIGATION_BAR_HEIGHT) andTextFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:20] andTextColor:[UIColor color222222] andTextLines:1];
        [self.contentView addSubview:_titleLabel];
        _titleLabel.lineBreakMode=NSLineBreakByTruncatingTail;
        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, _titleLabel.endPointY, DeviceWidth, 1)];
        lineImage.backgroundColor = [UIColor colorEDEDED];
        [self.contentView addSubview:lineImage];

        _personLabel = [[Label alloc] initWithFrame:CGRectMake(0, _titleLabel.endPointY, (DeviceWidth/3), 39) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13] andTextColor:[UIColor blackColor] andTextLines:1];
        _personLabel.backgroundColor = [UIColor clearColor];
        _personLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_personLabel];
        
        UIImageView *lineImage1 = [[UIImageView alloc]initWithFrame:CGRectMake((DeviceWidth/3), _titleLabel.endPointY, 1, 39)];
        lineImage1.backgroundColor = [UIColor colorEDEDED];
        [self.contentView addSubview:lineImage1];

        _dateLabel = [[Label alloc] initWithFrame:CGRectMake((DeviceWidth/3)+1,_titleLabel.endPointY,(DeviceWidth/3),39) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13] andTextColor:[UIColor blackColor] andTextLines:1];
        _dateLabel.backgroundColor = [UIColor clearColor];
        _dateLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_dateLabel];

        UIImageView *lineImage2 = [[UIImageView alloc]initWithFrame:CGRectMake((DeviceWidth/3)*2, _titleLabel.endPointY, 1, 39)];
        lineImage2.backgroundColor = [UIColor colorEDEDED];
        [self.contentView addSubview:lineImage2];
        
           
        _timeLabel = [[Label alloc] initWithFrame:CGRectMake((DeviceWidth/3)*2+1, _titleLabel.endPointY, (DeviceWidth/3), 39) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13] andTextColor:[UIColor blackColor] andTextLines:1];
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_timeLabel];
        
        UIImageView *lineImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, _titleLabel.endPointY+39, DeviceWidth, 1)];
        lineImage3.backgroundColor = [UIColor colorEDEDED];
        [self.contentView addSubview:lineImage3];

        CGFloat currentY=_titleLabel.endPointY+40;

        _offerBox=[[UIView alloc] initWithFrame:CGRectMake(0,currentY,DeviceWidth,41)];

        self.discountLabel = [UIButton buttonWithType:UIButtonTypeCustom];
        self.discountLabel.frame = CGRectMake(LEFTLEFTSET, 0, DeviceWidth-2*LEFTLEFTSET, 40);
        self.discountLabel.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13];
        [self.discountLabel setTitleColor:[UIColor colorC2060A] forState:UIControlStateNormal];
        [_offerBox addSubview:self.discountLabel];

        UIImageView *lineImage_4 = [[UIImageView alloc]initWithFrame:CGRectMake(0, self.discountLabel.endPointY+1, DeviceWidth, 1)];
        lineImage_4.backgroundColor = [UIColor colorEDEDED];
        [_offerBox addSubview:lineImage_4];

        [self.contentView addSubview:_offerBox];

        currentY+=41;

        _addressLabel = [[Label alloc] initWithFrame:CGRectMake(LEFTLEFTSET, currentY+10, DeviceWidth-LEFTLEFTSET*2, 39) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13] andTextColor:[UIColor blackColor] andTextLines:1];
        _addressLabel.textAlignment = NSTextAlignmentCenter;
        _addressLabel.lineBreakMode=NSLineBreakByWordWrapping;
        _addressLabel.numberOfLines=0;
        [self.contentView addSubview:_addressLabel];

        currentY=_addressLabel.endPointY+10;

        _bottomBox=[[UIView alloc] initWithFrame:CGRectMake(0,currentY,DeviceWidth,100)];

        UIImageView *lineImage_5 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, DeviceWidth, 1)];
        lineImage_5.backgroundColor = [UIColor colorEDEDED];
        [_bottomBox addSubview:lineImage_5];

        _voucherLabel = [[Label alloc] initWithFrame:CGRectMake(0, 15, (DeviceWidth/3), 12) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:7] andTextColor:[UIColor blackColor] andTextLines:1];
        _voucherLabel.textAlignment = NSTextAlignmentCenter;
        _voucherLabel.textColor=[UIColor color999999];
        _voucherLabel.text=L(@"VOUCHER");
        [_bottomBox addSubview:_voucherLabel];
        _voucherValueLabel = [[Label alloc] initWithFrame:CGRectMake(0, 20, (DeviceWidth/3), 35) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:12] andTextColor:[UIColor blackColor] andTextLines:1];
        _voucherValueLabel.textAlignment = NSTextAlignmentCenter;
        [_bottomBox addSubview:_voucherValueLabel];
        
        _codeLabel = [[Label alloc] initWithFrame:CGRectMake((DeviceWidth/3), 15, (DeviceWidth/3), 12) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:7] andTextColor:[UIColor blackColor] andTextLines:1];
        _codeLabel.textAlignment = NSTextAlignmentCenter;
        _codeLabel.textColor=[UIColor color999999];
        _codeLabel.text=L(@"BOOK CODE");
        [_bottomBox addSubview:_codeLabel];
        _codeValueLabel = [[Label alloc] initWithFrame:CGRectMake((DeviceWidth/3), 20, (DeviceWidth/3), 35) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:12] andTextColor:[UIColor blackColor] andTextLines:1];
        _codeValueLabel.textAlignment = NSTextAlignmentCenter;
        [_bottomBox addSubview:_codeValueLabel];

        _statusLabel = [[Label alloc] initWithFrame:CGRectMake((DeviceWidth/3)*2, 15, (DeviceWidth/3), 12) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:7] andTextColor:[UIColor blackColor] andTextLines:1];
        _statusLabel.textAlignment = NSTextAlignmentCenter;
        _statusLabel.textColor=[UIColor color999999];
        _statusLabel.text=L(@"STATUS");
        [_bottomBox addSubview:_statusLabel];
        _statusValueLabel = [[Label alloc] initWithFrame:CGRectMake((DeviceWidth/3)*2, 20, (DeviceWidth/3), 35) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:12] andTextColor:[UIColor blackColor] andTextLines:1];
        _statusValueLabel.textAlignment = NSTextAlignmentCenter;
        [_bottomBox addSubview:_statusValueLabel];

        CGFloat bottomCurrentY=60;

        NSArray *imageArray = @[@"phone",@"locationBlack",@"calendar"];
        
        UIImageView *lineImage4 = [[UIImageView alloc]initWithFrame:CGRectMake(0, bottomCurrentY, DeviceWidth, 1)];
        lineImage4.backgroundColor = [UIColor colorCCCCCC];
        [_bottomBox addSubview:lineImage4];
        
        UIImageView *lineImage5 = [[UIImageView alloc]initWithFrame:CGRectMake(0, bottomCurrentY+53, DeviceWidth, 1)];
        lineImage5.backgroundColor = [UIColor colorCCCCCC];
        [_bottomBox addSubview:lineImage5];
        
        
        self.phoneButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.phoneButton.frame = CGRectMake(0, bottomCurrentY+1, DeviceWidth/4-1, 52);
        UIImage *image = [UIImage imageNamed:[imageArray objectAtIndex:0]];
        UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET*2, LEFTLEFTSET, image.size.width, image.size.height)];
        iconImage.image = image;
        [self.phoneButton addSubview:iconImage];
        
        [self.phoneButton setBackgroundColor:[UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1]];
        
        [_bottomBox addSubview:self.phoneButton];
        
        UIImageView *lineImage10 = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth/4-1, bottomCurrentY, 1, 53)];
        lineImage10.backgroundColor = [UIColor colorCCCCCC];
        [_bottomBox addSubview:lineImage10];
        
        self.shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.shareButton.frame = CGRectMake(DeviceWidth/4, bottomCurrentY+1, DeviceWidth/4-1, 52);
        UIImage *image1 = [UIImage imageNamed:[imageArray objectAtIndex:1]];
        UIImageView *iconImage1 = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET*2, LEFTLEFTSET, image1.size.width, image1.size.height)];
        iconImage1.image = image1;
        [self.shareButton addSubview:iconImage1];
        
        [self.shareButton setBackgroundColor:[UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1]];
        
        [_bottomBox addSubview:self.shareButton];
        
        UIImageView* lineImage11 = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth/2-1, bottomCurrentY, 1, 53)];
        lineImage11.backgroundColor = [UIColor colorCCCCCC];
        [_bottomBox addSubview:lineImage11];
        
        self.calandarButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.calandarButton.frame = CGRectMake(DeviceWidth/2, bottomCurrentY+1, DeviceWidth/4-1, 52);
        UIImage *image2 = [UIImage imageNamed:[imageArray objectAtIndex:2]];
        UIImageView *iconImage2 = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET*2, LEFTLEFTSET, image2.size.width, image2.size.height)];
        iconImage2.image = image2;
        [self.calandarButton addSubview:iconImage2];
        
        [self.calandarButton setBackgroundColor:[UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1]];
        
        [_bottomBox addSubview:self.calandarButton];
        
        UIImageView*lineImage12 = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth/4*3-1, bottomCurrentY, 1, 53)];
        lineImage12.backgroundColor = [UIColor colorCCCCCC];
        [_bottomBox addSubview:lineImage12];
        
        self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.cancelButton.frame = CGRectMake(DeviceWidth/4*3, bottomCurrentY+1, DeviceWidth/4-1, 52);
        [self.cancelButton setTitle:L(@"Cancel") forState:UIControlStateNormal];
        self.cancelButton.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16];
        [self.cancelButton setTitleColor:[UIColor colorC2060A] forState:UIControlStateNormal];
        [self.cancelButton setBackgroundColor:[UIColor colorWithRed:244/255.0 green:244/255.0 blue:244/255.0 alpha:1]];
        
        [_bottomBox addSubview:self.cancelButton];

        UIImageView *shadowImage = [[UIImageView alloc] initShadowImageViewWithShadowOriginY_v2:self.cancelButton.endPointY andHeight:10];
        [_bottomBox addSubview:shadowImage];
        
        [self.contentView addSubview:_bottomBox];
    }
    return self;
}

-(void)initializeCellWithReservation:(IMGReservation *)reservation
{
    _titleLabel.text = reservation.restaurantName;

    NSString *bookDate = [BookUtil dateStringWithFormat:@"EEE, dd MMM" forDate:[BookUtil dateFromString:reservation.bookDate withFormat:@"yyyy-MM-dd"]];
    NSString *bookDate1 = [BookUtil dateStringWithFormat:@"dd MMM" forDate:[BookUtil dateFromString:reservation.bookDate withFormat:@"yyyy-MM-dd"]];
    NSMutableAttributedString *attStr;
    if (bookDate) {
        attStr = [[NSMutableAttributedString alloc]initWithString:bookDate];
    }
    NSRange range = [bookDate rangeOfString:bookDate1];
    NSRange range1 = [bookDate rangeOfString:bookDate];
    CTFontRef font = CTFontCreateWithName((CFStringRef)[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13].fontName, 13, NULL);
    CTFontRef font1 = CTFontCreateWithName((CFStringRef)[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13].fontName, 13, NULL);
    [attStr addAttribute:(id)kCTFontAttributeName value:(__bridge id)font range:range1];
    [attStr addAttribute:(id)kCTFontAttributeName value:(__bridge id)font1 range:range];
    [attStr addAttribute:(id)kCTForegroundColorAttributeName value:(id)[UIColor blackColor].CGColor range:range1];
    
//    CGSize maxSize = [bookDate sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13]];
//    _dateLabel.frame =CGRectMake((DeviceWidth/3)+(_dateLabel.frame.size.width-maxSize.width)/2 ,_titleLabel.endPointY + 39/2-maxSize.height/2, DeviceWidth/3, maxSize.height+39/2);
//    
//    [_dateLabel setAttributedString:attStr];
     _dateLabel.text = [attStr string];
    _timeLabel.text = reservation.bookTime;

    NSString *peopleCountString = [NSString stringWithFormat:L(@"%@ people"),reservation.party];
    NSMutableAttributedString *attStr1 = [[NSMutableAttributedString alloc]initWithString:peopleCountString];
    NSRange range2 = [peopleCountString rangeOfString:[NSString stringWithFormat:@"%@",reservation.party]];
    NSRange range3 = [peopleCountString rangeOfString:peopleCountString];
    
    [attStr1 addAttribute:(id)kCTFontAttributeName value:CFBridgingRelease(font) range:range3];
    [attStr1 addAttribute:(id)kCTFontAttributeName value:CFBridgingRelease(font1) range:range2];
    [attStr1 addAttribute:(id)kCTForegroundColorAttributeName value:(id)[UIColor blackColor].CGColor range:range3];
    
//    CGSize maxSize2 = [peopleCountString sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13]];
//    _personLabel.frame = CGRectMake((_personLabel.frame.size.width-maxSize2.width)/2,_titleLabel.endPointY + 39/2-maxSize2.height/2, DeviceWidth/3, maxSize2.height+39/2);
//    [_personLabel setAttributedString:attStr1];
    _personLabel.text = [attStr1 string];
    
    NSString *statusString=@"Unknown";
    if([reservation.status isEqualToString: @"1"]){
        statusString= L(@"Pending");
    }else if([reservation.status isEqualToString: @"2"]||[reservation.status isEqualToString: @"11"]){
        statusString= L(@"Confirmed");
    }else if([reservation.status isEqualToString: @"4"]){
        statusString= L(@"Completed");
    }else if([reservation.status isEqualToString: @"5"]){
        statusString= L(@"No Show");
    }else{
        statusString= L(@"Cancelled");
    }

    CGSize addressSize= [reservation.address sizeWithFont:_addressLabel.font  constrainedToSize:CGSizeMake(DeviceWidth-LEFTLEFTSET*2, 10000) lineBreakMode:NSLineBreakByWordWrapping];
    _addressLabel.text=[reservation.address removeHTML];
    _voucherValueLabel.text=reservation.voucherCode;
    _codeValueLabel.text=reservation.code;
    _statusValueLabel.text=statusString;

    if(reservation.offerName!=nil && reservation.offerName.length){
        [self.discountLabel setTitle:reservation.offerName forState:UIControlStateNormal];
        _addressLabel.frame=CGRectMake(LEFTLEFTSET,_offerBox.endPointY+10,DeviceWidth-LEFTLEFTSET*2,addressSize.height);
        _bottomBox.frame=CGRectMake(0,_addressLabel.endPointY,DeviceWidth,100);
        _offerBox.hidden=NO;
    }else{
        _offerBox.hidden=YES;
        _addressLabel.frame=CGRectMake(LEFTLEFTSET,_titleLabel.endPointY+50,DeviceWidth-LEFTLEFTSET*2,addressSize.height);
        _bottomBox.frame=CGRectMake(0,_addressLabel.endPointY,DeviceWidth,100);
    }
}
- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
