//
//  PersonalMiddleRatedTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/6/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGJourneyReview.h"
#import "DLStarRatingControl.h"

@protocol PersonalMiddleRatedTableViewCellDelegate <NSObject>

//头像
- (void)RatedIconImageViewTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path review:(IMGJourneyReview *)model;


//write review
- (void)didClickEditBtn:(IMGRestaurant *)restaurant andReviewId:(NSNumber*)reviewId indexPath:(NSIndexPath *)indexPath;

@end

@interface PersonalMiddleRatedTableViewCell : UITableViewCell

@property (nonatomic, strong)  UILabel  *lblRated;
@property (nonatomic, strong)  UILabel  *lblDate;
@property (nonatomic, strong)  UIButton  *iconImageView;
@property (nonatomic, strong)  UILabel  *lbluserName;
@property (nonatomic, strong)  UILabel  *lblTitle;
@property (nonatomic, strong)  UIButton  *btnwhiteReview;

@property (nonatomic, strong)  DLStarRatingControl * starR;
@property (nonatomic, strong)  UILabel  *lblGood;
@property (nonatomic, strong)  IMGJourneyReview  *model;

@property (nonatomic, weak)  id<PersonalMiddleRatedTableViewCellDelegate> Delegate;
@property(nonatomic,strong) NSIndexPath *fromCellIndexPath;

@end
