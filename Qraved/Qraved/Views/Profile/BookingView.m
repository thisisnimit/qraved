//
//  MyListRestaurantsView.m
//  Qraved
//
//  Created by System Administrator on 10/14/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "BookingView.h"
#import "UpCommingBookingsTableViewCell.h"
#import "PreviousBookingsTableViewCell.h"
#import "UIConstants.h"
#import "NSString+Helper.h"

@implementation BookingView

#define HEADER_VIEW_HEIGHT 50

@synthesize hasHistory;

-(instancetype)initWithFrame:(CGRect)frame{
	if(self=[super initWithFrame:frame]){
		hasHistory=YES;
		self.upcomingBookings=[[NSMutableArray alloc] init];
		self.pastBookings=[[NSMutableArray alloc] init];
		self.tableView=[[UITableView alloc] initWithFrame:CGRectMake(0,0,frame.size.width,frame.size.height)];
		self.tableView.delegate=self;
		self.tableView.dataSource=self;
		self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
		[self addSubview:self.tableView];
		[self addObserver:self forKeyPath:@"upcomingBookings" options:NSKeyValueObservingOptionNew context:nil];
		[self addObserver:self forKeyPath:@"pastBookings" options:NSKeyValueObservingOptionNew context:nil];
	}
	return self;
}

-(void)setRefreshViewFrame{
	if(hasHistory){
		if(self.refreshView==nil){
			self.refreshView=[[EGORefreshTableFooterView alloc] initWithFrame:CGRectMake(0,DeviceHeight,DeviceWidth,66)];
			self.refreshView.backgroundColor=[UIColor clearColor];
			self.refreshView.delegate=self;
			[self.tableView addSubview:self.refreshView];
		}
		if(self.tableView.contentSize.height>self.tableView.frame.size.height){
			self.refreshView.frame=CGRectMake(0,self.tableView.contentSize.height+10,DeviceWidth,66);
		}else{
			self.refreshView.frame=CGRectMake(0,self.tableView.frame.size.height+10,DeviceWidth,66);
		}
	}else{
		[self.refreshView removeFromSuperview];
		self.refreshView=nil;
	}
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexpath{
	CGFloat height=163;
	if(indexpath.section==0){
		IMGReservation *reservation=[self.upcomingBookings objectAtIndex:indexpath.row];
		CGSize addressSize=[reservation.address sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13] constrainedToSize:CGSizeMake(DeviceWidth-LEFTLEFTSET*2,1000) lineBreakMode:NSLineBreakByWordWrapping];
		height+=addressSize.height;
		if(reservation.offerName!=nil && reservation.offerName.length){
			height+=41;
		}
		height+=55;
	}else if(indexpath.section==1){
		IMGReservation *reservation=[self.pastBookings objectAtIndex:indexpath.row];
		CGSize addressSize=[reservation.address sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13] constrainedToSize:CGSizeMake(DeviceWidth-LEFTLEFTSET*2,1000) lineBreakMode:NSLineBreakByWordWrapping];
		height+=addressSize.height;
		if(reservation.offerName!=nil && reservation.offerName.length){
			height+=41;
		}
	}
	return height;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
	return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	if(section==0){
		return self.upcomingBookings.count;	
	}else{
		return self.pastBookings.count;	
	}
}

-(UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section{
	if(section==0 && self.upcomingBookings.count==0){
		return  nil;
	}else if(section==1 && self.pastBookings.count==0){
		return nil;
	}
	UIView *header=[[UIView alloc] initWithFrame:CGRectMake(0,0,self.frame.size.width,HEADER_VIEW_HEIGHT)];
	header.backgroundColor=[UIColor colorEDEDED];
	UILabel *title=[[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET,0,header.frame.size.width,header.frame.size.height)];
	title.font=[UIFont systemFontOfSize:16];
	[header addSubview:title];
	if(section==0){
		title.text=L(@"Upcoming Bookings");
	}else{
		title.text=L(@"Past Bookings");
	}
	return header;
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section{
	if(section==0 && self.upcomingBookings.count){
		return HEADER_VIEW_HEIGHT;
	}else if(section==1 && self.pastBookings.count){
		return HEADER_VIEW_HEIGHT;	
	}
	return 0;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexpath{
	if(indexpath.section==0){
		static NSString *upcomingIdentifier = @"UpcomingBookingsCell";
		UpCommingBookingsTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:upcomingIdentifier];
		if(cell==nil){
			cell=[[UpCommingBookingsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:upcomingIdentifier];
		}
		IMGReservation *reservation=[self.upcomingBookings objectAtIndex:indexpath.row];
		[cell initializeCellWithReservation:reservation];
		cell.titleLabel.tag=indexpath.row;
		UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self.delegate action:@selector(goToRestaurant_v1:)];
		[cell.titleLabel addGestureRecognizer:tap];

        cell.detailButton.tag = indexpath.row;
        [cell.detailButton addTarget:self.delegate action:@selector(upcomingReservationDetailButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.phoneButton.tag = indexpath.row;
        [cell.phoneButton addTarget:self.delegate action:@selector(telephoneButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        cell.shareButton.tag = indexpath.row;
        [cell.shareButton addTarget:self.delegate action:@selector(gotoMap:) forControlEvents:UIControlEventTouchUpInside];
        cell.calandarButton.tag = indexpath.row;
        [cell.calandarButton addTarget:self.delegate action:@selector(addToCalendar:) forControlEvents:UIControlEventTouchUpInside];
        cell.cancelButton.tag = indexpath.row;
        [cell.cancelButton addTarget:self.delegate action:@selector(cancelBooking:) forControlEvents:UIControlEventTouchUpInside];
        if (reservation.offerName) {
           [cell.discountLabel setTitle:reservation.offerName forState:UIControlStateNormal];
           // [cell.discountLabel addTarget:self.delegate action:@selector(offerButtonClick) forControlEvents:UIControlEventTouchUpInside];
        }else{
            [cell.discountLabel setTitle:L(@"No offer") forState:UIControlStateNormal];
        }
        
        cell.backgroundColor = [UIColor defaultColor];
        return cell;
	}else{
		static NSString *pastIdentifier = @"PreviousBookingsCell";
        PreviousBookingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:pastIdentifier];
        if(cell == nil) {
            cell = [[PreviousBookingsTableViewCell  alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:pastIdentifier];
            
        }
        IMGReservation *reservation = [self.pastBookings objectAtIndex:indexpath.row];
        [cell initializeCellWithReservation:reservation];
        cell.titleLabel.tag=indexpath.row;
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self.delegate action:@selector(goToRestaurant_v2:)];
		[cell.titleLabel addGestureRecognizer:tap];
        cell.detailButton.tag = indexpath.row;
        [cell.detailButton addTarget:self.delegate action:@selector(historyReservationDetailButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        if (reservation.offerName) {
           [cell.discountLabel setTitle:[reservation.offerName removeHTML] forState:UIControlStateNormal];
           // [cell.discountLabel addTarget:self.delegate action:@selector(offerButtonClick) forControlEvents:UIControlEventTouchUpInside];
        }else{
            [cell.discountLabel setTitle:L(@"No offer") forState:UIControlStateNormal];
        }
        cell.backgroundColor = [UIColor defaultColor];
        return cell;
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexpath{
    // [tableView deselectRowAtIndexPath:indexpath animated:NO];
    // if(self.delegate && [self.delegate respondsToSelector:@selector(goToBookingDetail:)]){
	// 	[self.delegate goToBookingDetail:indexpath];
	// }
}

- (void)egoRefreshTableFooterDidTriggerRefresh:(EGORefreshTableFooterView*)view{
	if(self.delegate && [self.delegate respondsToSelector:@selector(loadHistoryBookings:)]){
		[self.delegate loadHistoryBookings:^(BOOL hasData){
			hasHistory=hasData;
			[self.refreshView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
			[self setRefreshViewFrame];
		}];
    }
}

-(void)scrollViewDidScroll:(UITableView*)tableView{
	[self.refreshView egoRefreshScrollViewDidScroll:tableView];
	if(self.upcomingBookings.count || self.pastBookings.count){
		if(tableView.contentOffset.y<=HEADER_VIEW_HEIGHT && tableView.contentOffset.y>0){
			tableView.contentInset=UIEdgeInsetsMake(-tableView.contentOffset.y,0,0,0);
		}else if(tableView.contentOffset.y>HEADER_VIEW_HEIGHT){
			tableView.contentInset=UIEdgeInsetsMake(-HEADER_VIEW_HEIGHT,0,0,0);
		}
	}
}

-(void)scrollViewDidEndDragging:(UITableView*)tableView willDecelerate:(BOOL)decelerate{
	if(self.tableView.contentOffset.y>self.tableView.contentSize.height-self.tableView.frame.size.height+66){
		self.refreshView.frame=CGRectMake(0,self.tableView.contentSize.height,DeviceWidth,66);
		[self.refreshView egoRefreshScrollViewDidEndDragging:tableView];
	}
}

-(void)observeValueForKeyPath:(NSString*)keypath ofObject:(id)objc change:(NSDictionary*)change context:(void*)context{
	if([keypath isEqualToString:@"upcomingBookings"]){
		[self.tableView reloadData];
	}else if([keypath isEqualToString:@"pastBookings"]){
		[self.tableView reloadData];
	}
}

-(void)dealloc{
	[self removeObserver:self forKeyPath:@"upcomingBookings" context:nil];
	[self removeObserver:self forKeyPath:@"pastBookings" context:nil];
}


@end
