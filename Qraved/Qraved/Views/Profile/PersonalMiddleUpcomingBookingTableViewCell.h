//
//  PersonalMiddleUpcomingBookingTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/6/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGJourneyBook.h"
#import "CustomIOS7AlertView.h"
#import "LoadingView.h"

@protocol PersonalMiddleUpcomingBookingTableViewCellDelegate <NSObject>

- (void)delegateRefresh;
//- (void)delegateCancel:(UIButton *)sender;
- (void)UpcomingBookingViewMapTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path book:(IMGJourneyBook *)model;

//call
- (void)UpcomingBookingHomeReviewTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path book:(IMGJourneyBook *)model;
//reminder
- (void)UpcomingBookingReminderTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path book:(IMGJourneyBook *)model;

- (void)UpcomingBookingIconImageViewTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path book:(IMGJourneyBook *)model;
@end

@interface PersonalMiddleUpcomingBookingTableViewCell : UITableViewCell

@property (nonatomic, strong)  UILabel  *lblUpcomingBooking;
@property (nonatomic, strong)  UILabel  *lblDate;
@property (nonatomic, strong)  UIButton  *iconImageView;
@property (nonatomic, strong)  UILabel  *lbluserName;
@property (nonatomic, strong)  UILabel  *lblTitle;
@property (nonatomic, strong)  UIButton  *btnwhiteReview;
@property (nonatomic, strong)  UILabel  *lblPax;
@property (nonatomic, strong)  UILabel  *lbldetailDate;

@property (nonatomic, strong)  UIButton  *btnCall;
@property (nonatomic, strong)  UIButton  *btnViewMap;
@property (nonatomic, strong)  UIButton  *btnReminder;
@property (nonatomic, strong)  UIView *bottomView ;

@property (nonatomic, strong)  IMGJourneyBook  *model;

@property (nonatomic, weak)  id<PersonalMiddleUpcomingBookingTableViewCellDelegate> Delegate;
@property(nonatomic,strong) NSIndexPath *fromCellIndexPath;

@end

