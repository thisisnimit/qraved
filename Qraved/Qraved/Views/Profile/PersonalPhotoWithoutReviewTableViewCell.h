//
//  PersonalPhotoWithoutReviewTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/6/28.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGJourneyPhoto.h"
#import "IMGUploadPhoto.h"


@protocol PhotoWithHomeReviewTableViewCellDelegate <NSObject>
@optional

//share btn
- (void)homeCardTableViewCell:(UITableViewCell*)cell shareButtonTapped:(UIButton*)button entity:(id)entity;

//头像
- (void)PhotoWithReviewIconImageViewTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path uploadPhoto:(IMGJourneyPhoto *)model;

//write review
- (void)PhotoWithoutReviewEditBtn:(IMGRestaurant *)restaurant andReviewId:(NSNumber*)reviewId indexPath:(NSIndexPath *)indexPath array:(NSMutableArray *)array;


//comment
- (void)goToCardDetailPageWithType:(int)type andData:(NSDictionary *)dataDic andCellIndexPath:(NSIndexPath*)cellIndexPath;

@end


@interface PersonalPhotoWithoutReviewTableViewCell : UITableViewCell
{
    UIView *lineView;
}
//@property (nonatomic, strong)  UIImageView  *iconImageView;
@property (nonatomic, strong)  UIButton  *iconImageView;
@property (nonatomic, strong)  UILabel  *lblUserName;
@property (nonatomic, strong)  UILabel  *lblTittle;

@property (nonatomic, strong)  UIButton  *btnEdit;
@property (nonatomic, strong)  UILabel  *lbldate;


@property (nonatomic, strong)  UIButton  *btnLikes;
@property (nonatomic, strong)  UIButton  *btnComments;
@property (nonatomic, strong)  UIButton  *btnShare;

@property (nonatomic, strong)  IMGJourneyPhoto  *model;

@property (nonatomic, strong)  UIView *bottomView ;

@property (nonatomic, strong)  UIButton  *btnwhiteReview;



//一张图片
@property (nonatomic, strong)  UIImageView  *detailImageView;
//两张图片

@property (nonatomic, strong)  UIImageView  *detailImageOne;

//三张图片
@property (nonatomic, strong)  UIImageView  *detailImageTwo;
//四张图片
@property (nonatomic, strong)  UIImageView  *detailImageThree;

//五张图片
@property (nonatomic, strong)  UIImageView  *detailImageFour;

//多张图片(最后一张的蒙版)
@property (nonatomic, strong)  UIButton *lblMorePicture;

@property (nonatomic,assign) CGFloat cellHeight;


@property (nonatomic,weak) id<PhotoWithHomeReviewTableViewCellDelegate>homeReviewDelegate;
@property(nonatomic,strong) NSIndexPath *fromCellIndexPath;

@end
