//
//  PersonalMiddleCancelBookingTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/6/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGJourneyBook.h"

@protocol PersonalMiddleCancelBookingTableViewCellDelegate <NSObject>

// touxiang
- (void)CancelBookingViewMapTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path book:(IMGJourneyBook *)model;

@end

@interface PersonalMiddleCancelBookingTableViewCell : UITableViewCell
{
    //UIView *lineViewA;
}
@property (nonatomic, strong)  UILabel  *lblUpcomingBooking;
@property (nonatomic, strong)  UILabel  *lblDate;
@property (nonatomic, strong)  UIButton  *iconImageView;
@property (nonatomic, strong)  UILabel  *lbluserName;
@property (nonatomic, strong)  UILabel  *lblTitle;
//@property (nonatomic, strong)  UIButton  *btnwhiteReview;
@property (nonatomic, strong)  UILabel  *lblPax;
@property (nonatomic, strong)  UILabel  *lbldetailDate;

//@property (nonatomic, strong)  UIButton  *btnCall;
//@property (nonatomic, strong)  UIButton  *btnViewMap;
//@property (nonatomic, strong)  UIButton  *btnReminder;


@property (nonatomic, strong)  UIView *bottomView ;

@property (nonatomic, strong)  IMGJourneyBook  *model;

@property (nonatomic, weak)  id<PersonalMiddleCancelBookingTableViewCellDelegate> Delegate;
@property(nonatomic,strong) NSIndexPath *fromCellIndexPath;

@end
