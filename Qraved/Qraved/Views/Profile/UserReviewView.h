//
//  UserReviewView.h
//  Qraved
//
//  Created by Lucky on 15/10/22.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGUserReview.h"
#import "IMGRestaurant.h"

@class UserReviewView;

@protocol userReviewDelegate <NSObject>
@optional

-(void)userReviewImagePressed:(UserReviewView*)review images:(NSArray*)images withTag:(NSInteger)tag andRestaurant:(IMGRestaurant *)restaurant;

-(void)userReviewViewClickWithRestaurant:(IMGRestaurant *)restaurant;

- (void)userReviewRestaurantClickWithRestaurant:(IMGRestaurant *)restaurant;

- (void)userReviewMoreBtnClickWithIMGUserReview:(IMGUserReview*)review;

- (void)userReviewWriteReviewWithIMGUserReview:(IMGUserReview*)review;

@end

@interface UserReviewView : UIView



@property(nonatomic, retain) IMGUserReview *review;
@property(nonatomic, retain) NSMutableArray *reviewImages;
@property(nonatomic, assign)CGFloat reviewHeight;
@property (nonatomic,assign) BOOL isOtherUser;


@property(nonatomic,weak)id<userReviewDelegate>delegate;


-(id)initWithReview:(IMGUserReview *)userReview andDish:(NSArray*)dishArray andIsFromReviewViewController:(BOOL)isFromReviewViewController andIsOtherUser:(BOOL)isOtherUser;

@end
