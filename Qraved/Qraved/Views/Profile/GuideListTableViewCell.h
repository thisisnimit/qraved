//
//  GuideListTableViewCell.h
//  Qraved
//
//  Created by Gary.yao on 2017/6/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGGuideModel.h"
@interface GuideListTableViewCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, copy) void (^listDetail)(IMGGuideModel *guideModel);

- (void)createUIWithArray:(NSArray*)array andName:(NSString*)name;

@end
