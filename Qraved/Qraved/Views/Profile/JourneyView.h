//
//  JourneyView.h
//  Qraved
//
//  Created by imaginato on 16/9/8.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JNBaseTableViewCell.h"
#import "PullTableView.h"
#import "JNReservationTableViewCell.h"
#import "JNUploadPhotoTableViewCell.h"
#import "IMGUser.h"
#import "ProfileBookingViewController.h"
@protocol JourneyViewDelegate <NSObject>

- (void)didClickHotKeyBtnWithTag:(int)tag;
- (void)didClickJNCellRestTitleLableWithRestId:(NSNumber *)restaurantId;
- (void)didClickJNCellEditBtn:(IMGRestaurant *)restaurant andReviewId:(NSNumber*)reviewID indexPath:(NSIndexPath *)indexPath;
- (void)didClickUploadPhotoJNCellEditBtn:(IMGJNUploadPhoto *)_uploadPhoto indexPath:(NSIndexPath *)indexPath;
- (void)didClickJNCellPhotoWithPhotoTag:(NSInteger)photoTag andDishArr:(NSArray *)dishArr;
-(void)reservationMapClick:(IMGRestaurant*)resturant with:(IMGJNReservation*)reservation;
-(void)journeyScrollNavigationController:(UIScrollView *)scrollView;
-(void)cancelUpcomingBooking:(NSInteger)reservationId;
@end

@interface JourneyView : UIView<UITableViewDelegate,UITableViewDataSource,JNBaseTableViewCellDelegate,PullTableViewDelegate,JourneyReserrationMapClickDelegate,JourneyUploadDelegate,ProfileBookingViewControllerDelegate>
//-(void)journeyReservationCancel:(IMGJNReservation*)reservation;


@property (nonatomic,strong) NSNumber *filterType;
@property (nonatomic,assign)    id <JourneyViewDelegate> delegate;
@property(nonatomic,assign)BOOL isOtherUser;
@property(nonatomic,assign)BOOL isHiddenHotkey;
@property(nonatomic,retain)IMGUser* OtherUser;
@property(nonatomic,strong)PullTableView *tableView;
-(void)setScrollToTop:(BOOL)isScrollToTop;
- (void)reloadData;
- (void)reloadDataScrollToTop;
-(instancetype)initWithFrame:(CGRect)frame withOtherUser:(IMGUser*)otherUser;
@end
