//
//  ProfileJourneyPhotosView.h
//  Qraved
//
//  Created by harry on 2017/12/7.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileJourneyPhotosView : UIView


@property (nonatomic, strong)NSArray *photoArray;

+(float)cardPhotoHeightWithPhotoCout:(NSInteger)count;

@end
