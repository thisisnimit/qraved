//
//  CouponTableViewCell.h
//  Qraved
//
//  Created by harry on 2018/1/31.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CouponModel.h"
@interface CouponTableViewCell : UITableViewCell

- (void)createUIWithModel:(NSArray*)couponArray;

@property (nonatomic, copy) void (^couponViewTapped)(CouponModel *model);

@end
