//
//  TopPhotoTableViewCell.h
//  Qraved
//
//  Created by Gary.yao on 2017/6/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGSummary.h"
@interface TopPhotoTableViewCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>


@property (nonatomic,strong) IMGSummary *summary;
@property (nonatomic, copy) void (^goToPhotos)();
@property (nonatomic, copy) void (^photoClick)(NSArray *photoArr,NSInteger index);


- (void)createUIWithModel:(IMGSummary*)summary;
@end
