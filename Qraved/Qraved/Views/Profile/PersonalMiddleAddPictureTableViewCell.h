//
//  PersonalMiddleAddPictureTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/6/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DLStarRatingControl.h"

@interface PersonalMiddleAddPictureTableViewCell : UITableViewCell

@property (nonatomic, strong)  UIImageView  *iconImageView;
@property (nonatomic, strong)  UILabel  *lblUserName;
@property (nonatomic, strong)  UILabel  *lblTittle;

@property (nonatomic, strong)  UIButton  *btnEdit;
@property (nonatomic, strong)  DLStarRatingControl * starR;
@property (nonatomic, strong)  UILabel  *lbldate;
@property (nonatomic, strong)  UILabel  *lblDetail;

@property (nonatomic, strong)  UIButton  *btnAddPicture;

@property (nonatomic, strong)  UIButton  *btnLikes;
@property (nonatomic, strong)  UIButton  *btnComments;
@property (nonatomic, strong)  UIButton  *btnShare;


@property (nonatomic, strong)  UIView *bottomView ;

@end
