//
//  MostTableViewCell.h
//  Qraved
//
//  Created by Gary.yao on 2017/6/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGSummary.h"
@interface MostTableViewCell : UITableViewCell


- (void)createUIWithModel:(IMGSummary*)summary andIndex:(NSInteger)index;

@end
