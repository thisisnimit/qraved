//
//  UpcomingBookingTableViewCell.m
//  Qraved
//
//  Created by Gary.yao on 2017/6/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "UpcomingBookingTableViewCell.h"
#import "UIColor+Hex.h"
#import "SendCall.h"

@implementation UpcomingBookingTableViewCell
{

    UIScrollView * bookScrollView;
    
    UIPageControl *pgCtr;
    
    NSArray * bookArr;
    
    NSNumber *currentRestaurantId;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUIWithModel:(NSArray*)bookingArr{

    bookArr = bookingArr;
    bookScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(15, 15, DeviceWidth-30, 179)];
    bookScrollView.showsHorizontalScrollIndicator = NO;
    bookScrollView.showsVerticalScrollIndicator = NO;
    bookScrollView.delegate=self;
    bookScrollView.autoresizingMask=UIViewAutoresizingNone;
    [bookScrollView setPagingEnabled:YES];
    bookScrollView.layer.borderWidth = 1;
    bookScrollView.layer.borderColor = [UIColor colorWithHexString:@"#E5E6E8"].CGColor;
    [self.contentView addSubview:bookScrollView];
    
    
    [self setScrollView];
    
    pgCtr=[[UIPageControl alloc] initWithFrame:CGRectMake(15,bookScrollView.endPointY+10,DeviceWidth-30,7)];
    [pgCtr setBackgroundColor:[UIColor clearColor]];
    pgCtr.numberOfPages=bookingArr.count;
    pgCtr.pageIndicatorTintColor=[UIColor colorWithHexString:@"#DBDBDB"];
    pgCtr.currentPageIndicatorTintColor=[UIColor colorWithHexString:@"#D20000"];
    pgCtr.autoresizingMask=UIViewAutoresizingNone;
    [self.contentView addSubview:pgCtr];
    
    
    UILabel * titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 30, DeviceWidth-60, 17)];
    titleLabel.font = [UIFont boldSystemFontOfSize:14];
    titleLabel.textColor = [UIColor color333333];
    titleLabel.text = @"Upcoming Booking";
    [self.contentView addSubview:titleLabel];
    
    
    
}

- (void)setScrollView{

    
    for (int i = 0; i<bookArr.count; i++) {
        IMGReservation * reservation = bookArr[i];
        UIView * bookView = [UIView new];
        [bookScrollView addSubview:bookView];
        bookView.sd_layout
        .topSpaceToView(bookScrollView, 0)
        .leftSpaceToView(bookScrollView, i*bookScrollView.frame.size.width)
        .widthIs(bookScrollView.frame.size.width)
        .bottomSpaceToView(bookScrollView, 0);
        
        
        UIImageView * iconImage = [UIImageView new];
        [iconImage sd_setImageWithURL:[NSURL URLWithString:[reservation.restaurantImage returnFullImageUrl]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
        [bookView addSubview:iconImage];
        iconImage.sd_layout
        .topSpaceToView(bookView, 41)
        .leftSpaceToView(bookView, 15)
        .widthIs(40)
        .heightIs(40);
        
        UILabel * nameLabel = [UILabel new];
        nameLabel.text = reservation.restaurantTitle;
        nameLabel.textColor = [UIColor color333333];
        nameLabel.font = [UIFont systemFontOfSize:14];
        [bookView addSubview:nameLabel];
        
        nameLabel.sd_layout
        .topSpaceToView(bookView, 47)
        .leftSpaceToView(iconImage, 10)
        .rightSpaceToView(bookView, 50)
        .heightIs(16);
        
        UILabel * areaLabel = [UILabel new];
        areaLabel.text = reservation.restaurantCuisine;
        areaLabel.font = [UIFont systemFontOfSize:12];
        areaLabel.textColor = [UIColor color999999];
        [bookView addSubview:areaLabel];
        
        areaLabel.sd_layout
        .topSpaceToView(nameLabel, 2)
        .leftSpaceToView(iconImage, 10)
        .rightSpaceToView(bookView, 50)
        .heightIs(14);
        
        UIButton *btnClick = [UIButton new];
        btnClick.tag = 100+i;
        [bookView addSubview:btnClick];
        
        [btnClick addTarget:self action:@selector(gotoRDP:) forControlEvents:UIControlEventTouchUpInside];
        
        btnClick.sd_layout
        .topSpaceToView(bookView, 45)
        .leftSpaceToView(bookView, 0)
        .rightSpaceToView(bookView, 0)
        .heightIs(45);
        
        UILabel * desLabel = [UILabel new];
        desLabel.font = [UIFont systemFontOfSize:14];
        desLabel.text = [NSString stringWithFormat:@"Pax for %@ people\n%@ %@",reservation.bookPax,reservation.bookDate,reservation.bookTime];
        desLabel.numberOfLines = 2;
        desLabel.tag = 50+i;
        desLabel.textColor = [UIColor color333333];
        desLabel.userInteractionEnabled = YES;
        [bookView addSubview:desLabel];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bookClick:)];
        [desLabel addGestureRecognizer:tap];
        
        desLabel.sd_layout
        .topSpaceToView(iconImage, 10)
        .leftSpaceToView(bookView, 15)
        .rightSpaceToView(bookView, 15)
        .heightIs(36);
        
        
        UIView *lineView = [UIView new];
        lineView.backgroundColor = [UIColor colorWithHexString:@"#E5E6E8"];
        [bookView addSubview:lineView];
        
        lineView.sd_layout
        .topSpaceToView(desLabel, 5)
        .leftSpaceToView(bookView, 0)
        .rightSpaceToView(bookView, 0)
        .heightIs(1);
        
        UIButton *btnPhone = [UIButton buttonWithType:UIButtonTypeCustom];
        //    btnPhone.backgroundColor = [UIColor redColor];
        [btnPhone addTarget:self action:@selector(telephoneButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [btnPhone setTitle:@"Call" forState:UIControlStateNormal];
        btnPhone.titleLabel.font = [UIFont systemFontOfSize:12];
        [btnPhone setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [btnPhone setImage:[UIImage imageNamed:@"Shapephone"] forState:UIControlStateNormal];
        [btnPhone setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
        btnPhone.tag = 30+i;
        
        UIButton *btnMap = [UIButton buttonWithType:UIButtonTypeCustom];
        //    btnMap.backgroundColor = [UIColor brownColor];
        [btnMap addTarget:self action:@selector(gotoMap:) forControlEvents:UIControlEventTouchUpInside];
        [btnMap setTitle:@"View Map" forState:UIControlStateNormal];
        btnMap.titleLabel.font = [UIFont systemFontOfSize:12];
        [btnMap setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [btnMap setImage:[UIImage imageNamed:@"ic_home_map"] forState:UIControlStateNormal];
        [btnMap setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
        btnMap.tag = 200 +i;
        
        UIButton *btnReminder = [UIButton buttonWithType:UIButtonTypeCustom];
        //    btnReminder.backgroundColor = [UIColor blueColor];
        [btnReminder addTarget:self action:@selector(addToCalendar:) forControlEvents:UIControlEventTouchUpInside];
        [btnReminder setTitle:@"Reminder" forState:UIControlStateNormal];
        btnReminder.titleLabel.font = [UIFont systemFontOfSize:12];
        [btnReminder setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [btnReminder setImage:[UIImage imageNamed:@"Shapebtn"] forState:UIControlStateNormal];
        [btnReminder setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
        btnReminder.tag = 300+i;
        [bookView sd_addSubviews:@[btnPhone,btnMap,btnReminder]];
        
        btnPhone.sd_layout
        .topSpaceToView(lineView, 0)
        .leftSpaceToView(bookView, 0)
        .widthIs((DeviceWidth-30)/3)
        .heightIs(40);
        
        btnMap.sd_layout
        .topSpaceToView(lineView, 0)
        .rightSpaceToView(bookView, (DeviceWidth-30)/3)
        .widthIs((DeviceWidth-30)/3)
        .heightIs(40);
        
        btnReminder.sd_layout
        .topSpaceToView(lineView, 0)
        .rightSpaceToView(bookView, 0)
        .widthIs((DeviceWidth-30)/3)
        .heightIs(40);
        
//        UIButton * moreBtn = [UIButton new];
//        [moreBtn setImage:[UIImage imageNamed:@"icon_more"] forState:UIControlStateNormal];
//        [bookView addSubview:moreBtn];
//
//        moreBtn.sd_layout
//        .topSpaceToView(bookView, 52)
//        .rightSpaceToView(bookView, 15)
//        .widthIs(24)
//        .heightIs(24);
        
    }
    
     [bookScrollView setContentSize:CGSizeMake(bookScrollView.frame.size.width*bookArr.count, 0)];
}


- (void)scrollViewDidScroll:(UIScrollView *)sender {
    
    CGFloat pageWidth = bookScrollView.frame.size.width;
    int page = floor((bookScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    pgCtr.currentPage = page;
}

- (void)bookClick:(UITapGestureRecognizer *)tap{
    UIView *view = tap.view;
    IMGReservation * reservation = bookArr[view.tag - 50];
    IMGRestaurant *model = [[IMGRestaurant alloc] init];
    model.restaurantId = reservation.restaurantId;
    model.cuisineName = reservation.restaurantCuisine;
    model.title = reservation.restaurantName;
    model.imageUrl = reservation.restaurantImage;
    model.latitude = reservation.restaurantLatitude;
    model.longitude = reservation.restaurantLongitude;
    model.phoneNumber = reservation.restaurantPhone;
    model.districtName = reservation.restaurantDistrict;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(gotoBookDetail: andRestaurant:)]) {
        [self.delegate gotoBookDetail:reservation andRestaurant:model];
    }
    
}

- (void)gotoRDP:(UIButton *)button{
    IMGReservation * reservation = bookArr[button.tag - 100];
    if (self.delegate && [self.delegate respondsToSelector:@selector(gotoRDP:)]) {
        [self.delegate gotoRDP:reservation.restaurantId];
    }
}

- (void)telephoneButtonTapped:(UIButton *)button{
    IMGUser *user = [IMGUser currentUser];
    NSNumber *userId = [[NSNumber alloc] init];
    if (user.userId != nil)
    {
        userId = user.userId;
    }
    else{
        userId = [NSNumber numberWithInt:0];
    }
    
    
    IMGReservation * reservation = bookArr[button.tag - 30];
//    [TrackHandler trackWithUserId:userId andRestaurantId:restaurant.restaurantId andContent:[NSDictionary dictionaryWithObject:@"0" forKey:@"type"]];
    
    [IMGAmplitudeUtil trackBookWithName:@"CL - Call Restaurant CTA" andRestaurantId:reservation.restaurantId andReservationId:nil andLocation:@"User Profile Page - Summary" andChannel:nil];
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"" message:reservation.phone delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
    alertView.tag=1;
    [alertView show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==1){
        if(buttonIndex==0){
            [alertView dismissWithClickedButtonIndex:0 animated:NO];
        }else if (buttonIndex==1){
            
            [SendCall sendCall:alertView.message];
        }
    }
}

- (void)gotoMap:(UIButton *)button{
    IMGReservation * reservation = bookArr[button.tag - 200];
    IMGRestaurant *model = [[IMGRestaurant alloc] init];
    model.restaurantId = reservation.restaurantId;
    model.cuisineName = reservation.restaurantCuisine;
    model.title = reservation.restaurantName;
    model.imageUrl = reservation.restaurantImage;
    model.latitude = reservation.restaurantLatitude;
    model.longitude = reservation.restaurantLongitude;
    model.phoneNumber = reservation.restaurantPhone;
    model.districtName = reservation.restaurantDistrict;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(map:)]) {
        [self.delegate map:model];
    }
}

- (void)addToCalendar:(UIButton *)button{
    IMGReservation * reservation = bookArr[button.tag - 300];
    IMGRestaurant *model = [[IMGRestaurant alloc] init];
    model.restaurantId = reservation.restaurantId;
    model.cuisineName = reservation.restaurantCuisine;
    model.title = reservation.restaurantName;
    model.imageUrl = reservation.restaurantImage;
    model.latitude = reservation.restaurantLatitude;
    model.longitude = reservation.restaurantLongitude;
    model.phoneNumber = reservation.restaurantPhone;
    model.districtName = reservation.restaurantDistrict;
    model.address1 = reservation.address;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(reminder:andBook:)]) {
        [self.delegate reminder:model andBook:reservation];
    }
}

@end
