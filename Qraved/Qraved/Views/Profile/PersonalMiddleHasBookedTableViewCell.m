
//
//  PersonalMiddleHasBookedTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/6/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "PersonalMiddleHasBookedTableViewCell.h"

@implementation PersonalMiddleHasBookedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}
- (void)initUI{
    
    _lblHasBooked = [UILabel new];
//    _lblHasBooked.text = @"Has Booked";
    _lblHasBooked.textColor = [UIColor color333333];
    _lblHasBooked.font = [UIFont systemFontOfSize:14];
    
    _lblDate = [UILabel new];
//    _lblDate.text = @"Jan 12, 2017";
    _lblDate.textColor = [UIColor color999999];
    _lblDate.font = [UIFont systemFontOfSize:12];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    
    _iconImageView = [UIButton buttonWithType:UIButtonTypeCustom];
    [_iconImageView addTarget:self action:@selector(iconImageViewClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _lbluserName = [UILabel new];
//    _lbluserName.text = @"Pepenero";
    _lbluserName.textColor = [UIColor color333333];
    _lbluserName.font = [UIFont systemFontOfSize:14];
    
    _lblTitle = [UILabel new];
//    _lblTitle.text = @"Western • Senopati";
    _lblTitle.textColor = [UIColor color999999];
    _lblTitle.font = [UIFont systemFontOfSize:14];
    
    _btnwhiteReview = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnwhiteReview setImage:[UIImage imageNamed:@"whiteReviewbtn"] forState:UIControlStateNormal];
    
    _lblPax = [UILabel new];
//    _lblPax.text = @"Pax for 2 people";
    _lblPax.textColor = [UIColor color333333];
    _lblPax.font = [UIFont systemFontOfSize:14];
    
    _lbldetailDate = [UILabel new];
//    _lbldetailDate.text = @"Friday, Jan 27 2017 05.00 AM";
    _lbldetailDate.textColor = [UIColor color333333];
    _lbldetailDate.font = [UIFont systemFontOfSize:14];
    
    
    [self.contentView sd_addSubviews:@[_lblHasBooked, _lblDate, lineView, _iconImageView, _lbluserName, _lblTitle, _btnwhiteReview, _lblPax, _lbldetailDate]];
    
    _lblHasBooked.sd_layout
    .topSpaceToView(self.contentView, 10)
    .leftSpaceToView(self.contentView, 15)
    .heightIs(20);
    [_lblHasBooked setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
    _lblDate.sd_layout
    .topEqualToView(_lblHasBooked)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(20);
    [_lblDate setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
    lineView.sd_layout
    .topSpaceToView(_lblHasBooked, 10)
    .leftSpaceToView(self.contentView, 15)
    .widthIs(DeviceWidth - 30)
    .heightIs(1);
    
    _iconImageView.sd_layout
    .topSpaceToView(lineView, 10)
    .leftEqualToView(_lblHasBooked)
    .widthIs(40)
    .heightIs(40);
    
    _lbluserName.sd_layout
    .topSpaceToView(lineView, 15)
    .leftSpaceToView(_iconImageView, 5)
    .heightIs(20);
    [_lbluserName setSingleLineAutoResizeWithMaxWidth: DeviceWidth - 200];
    
    _lblTitle.sd_layout
    .topSpaceToView(_lbluserName, 5)
    .leftEqualToView(_lbluserName)
    .heightIs(20);
    [_lblTitle setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 200];
    
    _btnwhiteReview.sd_layout
    .centerYEqualToView(_iconImageView)
    .rightSpaceToView(self.contentView, 15)
    .widthIs(106)
    .heightIs(20);
    
    _lblPax.sd_layout
    .topSpaceToView(_iconImageView,10)
    .leftEqualToView(_iconImageView)
    .heightIs(20);
    [_lblPax setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
    _lbldetailDate.sd_layout
    .topSpaceToView(_lblPax, 5)
    .leftEqualToView(_lblPax)
    .heightIs(20);
    [_lbldetailDate setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
    
    
}
- (void)setModel:(IMGJourneyBook *)model{

    _model = model;
    
    IMGUser *user = [IMGUser currentUser];
    //    NSLog(@"%@========%@",model.userId, user.userId);
    if (![model.myID isEqualToNumber:user.userId]) {
        _btnwhiteReview.hidden = YES;
    }
  
    if (model.restaurantImage) {
        [_iconImageView sd_setImageWithURL:[NSURL URLWithString:[model.restaurantImage returnFullImageUrl]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            //image animation
            SDWebImageManager *manager = [SDWebImageManager sharedManager];
            if ([manager diskImageExistsForURL:[NSURL URLWithString:[model.restaurantImage returnFullImageUrl]]]) {
                NSLog(@"dont loading animation");
            }else {
                _iconImageView.alpha = 0.0;
                [UIView transitionWithView:_iconImageView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                    
                    [_iconImageView setImage:image forState:UIControlStateNormal];
                    _iconImageView.alpha = 1.0;
                } completion:NULL];
                
            }

        }];
    }
    _lblHasBooked.text = @"Has Booked";
    _lblDate.text = model.bookDate ? [Date dateFormart:model.bookDate]:@"";
    _lbluserName.text = model.restaurantTitle ?model.restaurantTitle :@"";
    _lblTitle.text = [NSString stringWithFormat:@"%@ • %@",model.restaurantCuisine, model.restaurantDistrict] ? [NSString stringWithFormat:@"%@ • %@",model.restaurantCuisine, model.restaurantDistrict] :@"";
    
    _lblPax.text = model.bookPax ? [NSString stringWithFormat:@"Pax for %@ people",model.bookPax] : @"";
    
    _lbldetailDate.text = model.bookTime ?[NSString stringWithFormat:@"%@ %@",[Date dateFormart:model.bookDate],[Date timeFormart:model.bookTime]]: @"";

    
//    [self.contentView setupAutoHeightWithBottomView:_lbldetailDate bottomMargin:10];
}
- (void)iconImageViewClick:(UIButton *)iconImageViewClick{

    NSLog(@"touxiang");
    if (self.Delegate) {
        [self.Delegate HasBookedIconImageViewTableCell:self indexPath:self.fromCellIndexPath book:self.model];
    }
}
@end
