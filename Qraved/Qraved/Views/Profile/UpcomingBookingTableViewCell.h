//
//  UpcomingBookingTableViewCell.h
//  Qraved
//
//  Created by Gary.yao on 2017/6/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGSummary.h"
#import "IMGReservation.h"
@protocol UpcomingBookingTableViewCellDelegate <NSObject>

- (void)gotoRDP:(NSNumber *)restaurantId;

- (void)gotoBookDetail:(IMGReservation *)book andRestaurant:(IMGRestaurant *)restaurant;

- (void)map:(IMGRestaurant *)restaurant;

- (void)reminder:(IMGRestaurant *)restaurant andBook:(IMGReservation *)book;

@end

@interface UpcomingBookingTableViewCell : UITableViewCell<UIScrollViewDelegate>

@property (nonatomic, weak) id<UpcomingBookingTableViewCellDelegate>delegate;
- (void)createUIWithModel:(NSArray*)bookingArr;

@end
