//
//  PersonalMiddleHasBookedTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/6/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGJourneyBook.h"

@protocol PersonalMiddleHasBookedTableViewCellDelegate <NSObject>

//头像
- (void)HasBookedIconImageViewTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path book:(IMGJourneyBook *)model;

@end

@interface PersonalMiddleHasBookedTableViewCell : UITableViewCell

@property (nonatomic, strong)  UILabel  *lblHasBooked;
@property (nonatomic, strong)  UILabel  *lblDate;
@property (nonatomic, strong)  UIButton  *iconImageView;
@property (nonatomic, strong)  UILabel  *lbluserName;
@property (nonatomic, strong)  UILabel  *lblTitle;
@property (nonatomic, strong)  UIButton  *btnwhiteReview;
@property (nonatomic, strong)  UILabel  *lblPax;
@property (nonatomic, strong)  UILabel  *lbldetailDate;

@property (nonatomic, strong)  IMGJourneyBook  *model;

@property (nonatomic, weak)  id<PersonalMiddleHasBookedTableViewCellDelegate> Delegate;
@property(nonatomic,strong) NSIndexPath *fromCellIndexPath;
@end
