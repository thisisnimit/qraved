//
//  ProfileSegument.m
//  Qraved
//
//  Created by Olaf on 14-9-5.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "ProfileSegument.h"
#import "ProfileViewController.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "LoadingView.h"
#import "UIButton+LinkButton.h"
 
#import "UIViewController+Helper.h"
#import "NavigationBarButtonItem.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"
#import "UILabel+Helper.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "Label.h"
#import <CoreText/CoreText.h>
#import "BookingDetailContentController.h"
#import "DLStarRatingControl.h"
#import "NSNumber+Helper.h"
#import "PointRewardsViewController.h"
#import "DetailViewController.h"
#import "UIImage+Helper.h"
#import "TYAttributedLabel.h"

@interface ProfileSegument()
    @property (nonatomic,strong) HMSegmentedControl * hmSegmentedControl;
@property (nonatomic,assign)BOOL isSelf;
@end

@implementation ProfileSegument

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.isSelf = YES;
        if(self.isSelf)
        {
            _hmSegmentedControl=[[HMSegmentedControl alloc]initWithSectionImages:@[[UIImage imageNamed:@"NoSelectBar"],[UIImage imageNamed:@"NoSelectBar"],[UIImage imageNamed:@"NoSelectBar"]] sectionSelectedImages:@[[UIImage imageNamed:@"SelectedBar"],[UIImage imageNamed:@"SelectedBar"],[UIImage imageNamed:@"SelectedBar"]]];
        }
        else
        {
            _hmSegmentedControl=[[HMSegmentedControl alloc]initWithSectionImages:@[[UIImage imageNamed:@"NoSelectBar"],[UIImage imageNamed:@"NoSelectBar"]] sectionSelectedImages:@[[UIImage imageNamed:@"SelectedBar"],[UIImage imageNamed:@"SelectedBar"]]];
        }
        _hmSegmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;

        self.frame= CGRectMake(0, 0, DeviceWidth, NAVIGATION_BAR_HEIGHT);
        self.backgroundColor = [UIColor colorEEEEEE];
        
        UISwipeGestureRecognizer *swipGesture = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(gestureStart:)];
        swipGesture.direction = UISwipeGestureRecognizerDirectionDown;
        [self addGestureRecognizer:swipGesture];
        
        UISwipeGestureRecognizer *swipGesture1 = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(gestureStart:)];
        swipGesture1.direction = UISwipeGestureRecognizerDirectionUp;
        [self addGestureRecognizer:swipGesture1];
        
        
        [_hmSegmentedControl setFrame:CGRectMake(0, 0, DeviceWidth, NAVIGATION_BAR_HEIGHT)];
        [_hmSegmentedControl addTarget:self action:@selector(indexChanged:) forControlEvents:UIControlEventValueChanged];
        [_hmSegmentedControl setSelectionIndicatorHeight:0];
        [self addSubview:_hmSegmentedControl];
        
        
        NSArray *titleArr = @[[NSString stringWithFormat:L(@"%d bookings"),6],[NSString stringWithFormat:L(@"My List")],[NSString stringWithFormat:L(@"%d reviews"),3]];
        
        
        UIFont *font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
        
        for (int i=0; i<titleArr.count; i++) {
            NSString *str = [titleArr objectAtIndex:i];
            CGSize expectSize = [str sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] constrainedToSize:CGSizeMake(DeviceWidth/3, NAVIGATION_BAR_HEIGHT)];
            
            NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc]initWithString:str];
            
            NSArray * attArr =[str componentsSeparatedByString:@" "];
            
            NSRange range1=[str rangeOfString:[attArr objectAtIndex:0]];
            NSRange range2=[str rangeOfString:[attArr objectAtIndex:1]];
            
            [attStr addAttribute:NSFontAttributeName value:font range:[str rangeOfString:str]];
            [attStr addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:range1];
            [attStr addAttribute:NSForegroundColorAttributeName value:[UIColor color111111] range:range2];
            
            
            TYAttributedLabel *titleLabel = [[TYAttributedLabel alloc]initWithFrame:CGRectMake(DeviceWidth/6-expectSize.width/2+DeviceWidth/3*i, NAVIGATION_BAR_HEIGHT/2-expectSize.height/2, expectSize.width, NAVIGATION_BAR_HEIGHT) ];
            
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.text=[attStr string];
            titleLabel.userInteractionEnabled=YES;
            titleLabel.tag = 500+i;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(indexLabelChanged:)];
            [tap setNumberOfTapsRequired:1];
            [tap setNumberOfTouchesRequired:1];
            
            [titleLabel addGestureRecognizer:tap];
            
            [self addSubview:titleLabel];
        }
        
        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, self.frame.size.height-1, DeviceWidth, 1)];
        lineImage.backgroundColor = [UIColor colorDDDDDD];
        [self addSubview:lineImage];
        

    }
    return self;
}


@end
