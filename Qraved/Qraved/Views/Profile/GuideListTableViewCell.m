//
//  GuideListTableViewCell.m
//  Qraved
//
//  Created by Gary.yao on 2017/6/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "GuideListTableViewCell.h"
#import "V2_GuideCell.h"
#import "IMGGuideModel.h"
@implementation GuideListTableViewCell
{

    UICollectionView *guideCollectionView;
    NSArray *guideArray;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUIWithArray:(NSArray*)array andName:(NSString*)name{

    guideArray = array;
    UILabel * titleLabel = [UILabel new];
    titleLabel.text = [NSString stringWithFormat:@"%@'s List",name];
    titleLabel.textColor = [UIColor color333333];
    titleLabel.font = [UIFont systemFontOfSize:14];
    [self.contentView addSubview:titleLabel];
    
    titleLabel.sd_layout
    .topSpaceToView(self.contentView, 15)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(17);
    
    
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(135, 135);
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 10;
    layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 0);
    
    guideCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,42, DeviceWidth, 135) collectionViewLayout:layout];
    guideCollectionView.backgroundColor = [UIColor whiteColor];
    guideCollectionView.delegate = self;
    guideCollectionView.dataSource = self;
    guideCollectionView.showsHorizontalScrollIndicator = NO;
    [self.contentView addSubview:guideCollectionView];
    
    
    [guideCollectionView registerNib:[UINib nibWithNibName:@"V2_GuideCell" bundle:nil] forCellWithReuseIdentifier:@"guideCell"];
    
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return guideArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    V2_GuideCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"guideCell" forIndexPath:indexPath];
    cell.guideIden.hidden = YES;
    IMGGuideModel *model = [guideArray objectAtIndex:indexPath.row];
    cell.guideModel = model;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    IMGGuideModel *model = [guideArray objectAtIndex:indexPath.row];
    if (self.listDetail) {
        self.listDetail(model);
    }
}

@end
