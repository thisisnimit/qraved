//
//  TopPhotoTableViewCell.m
//  Qraved
//
//  Created by Gary.yao on 2017/6/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "TopPhotoTableViewCell.h"
#import "IMGPhotoModel.h"
@implementation TopPhotoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUIWithModel:(IMGSummary *)summary{
    
    _summary = summary;

    UILabel * titleLabel = [UILabel new];
    titleLabel.text = @"Top Photos";
    titleLabel.textColor = [UIColor color333333];
    titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [self.contentView addSubview:titleLabel];
    
    titleLabel.sd_layout
    .topSpaceToView(self.contentView, 40)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(17);
    
    
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(124.5, 124.5);
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 1;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    UICollectionView* photoCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(15,72, DeviceWidth-15, 124.5) collectionViewLayout:layout];
    photoCollectionView.backgroundColor = [UIColor whiteColor];
    photoCollectionView.delegate = self;
    photoCollectionView.dataSource = self;
    photoCollectionView.showsHorizontalScrollIndicator = NO;
    [self.contentView addSubview:photoCollectionView];
    

    [photoCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"photoCell"];
    
    UIButton * button = [UIButton new];
    [button setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
    [button setTitle:@"See All" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:12];
    button.userInteractionEnabled = YES;
    button.enabled = YES;
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(buttonClick)];
    [button addGestureRecognizer:tap];
    [self.contentView insertSubview:button atIndex:999];
    
    button.sd_layout
    .topSpaceToView(self.contentView, 40)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(18)
    .widthIs(44);
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.summary.topPhotoList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    IMGPhotoModel *model = [self.summary.topPhotoList objectAtIndex:indexPath.row];
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 124.5, 124.5)];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[model.imageUrl returnFullImageUrl]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        //image animation
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        if ([manager diskImageExistsForURL:[NSURL URLWithString:[model.imageUrl returnFullImageUrl]]]) {
            NSLog(@"dont loading animation");
        }else {
            imageView.alpha = 0.0;
            [UIView transitionWithView:imageView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                
                imageView.image = image;
                imageView.alpha = 1.0;
            } completion:NULL];
            
        }

    }];
    [cell.contentView addSubview:imageView];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.photoClick) {
        self.photoClick(self.summary.topPhotoList,indexPath.row);
    }
}


-(void)buttonClick{

    if (self.goToPhotos!=nil) {
        self.goToPhotos();
    }
}

@end
