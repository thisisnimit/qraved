//
//  PersonalMiddleOnePictureTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/6/23.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "PersonalMiddleOnePictureTableViewCell.h"
#import "ReviewHandler.h"
#import "HomeUtil.h"
#import "ProfileJourneyPhotosView.h"

#define LINK_TEXT @"<a style=\"text-decoration:none;color:#3A8CE2;font-family:'OpenSans'\" href=\"http://www.qraved.com/\">...read more</a>"
#define LEFTMARGIN 15
#define TEXTLABELTOPMARGIN 35
#define RIGHTMARGIN 30
#define TEXTLABELLEFTMARGIN 11+PROFILEIMAGEVIEWSIZE+8
#define PROFILEIMAGEVIEWSIZE 20
#define CONTENTLABELWIDTH DeviceWidth-LEFTMARGIN-PROFILEIMAGEVIEWSIZE-RIGHTMARGIN
@implementation PersonalMiddleOnePictureTableViewCell
{
    ProfileJourneyPhotosView *photoView;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}
- (void)initUI{
    
    _iconImageView = [UIButton buttonWithType:UIButtonTypeCustom];
    [_iconImageView addTarget:self action:@selector(gotoRDP) forControlEvents:UIControlEventTouchUpInside];
    _iconImageView.frame = CGRectMake(15, 15, 40, 40);
    [self.contentView addSubview:_iconImageView];
    
    _lblUserName = [UILabel new];
//    _lblUserName.text = @"Pepenero";
    _lblUserName.textColor = [UIColor color333333];
    _lblUserName.font = [UIFont systemFontOfSize:14];
    _lblUserName.frame = CGRectMake(_iconImageView.endPointX+15, 20, DeviceWidth-200, 20);
    [self.contentView addSubview:_lblUserName];
    _lblUserName.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoRDP)];
    [_lblUserName addGestureRecognizer:tap];
    
    _lblTittle = [UILabel new];
//    _lblTittle.text = @"Western • Senopati";
    _lblTittle.textColor = [UIColor color999999];
    _lblTittle.font = [UIFont systemFontOfSize:12];
    _lblTittle.frame = CGRectMake(_lblUserName.startPointX, _lblUserName.endPointY, DeviceWidth-200, 20);
    [self.contentView addSubview:_lblTittle];
    _lblTittle.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *titleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoRDP)];
    [_lblTittle addGestureRecognizer:titleTap];

    
    _btnEdit = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnEdit setTitle:@"Edit" forState:UIControlStateNormal];
    [_btnEdit setTitleColor:[UIColor colortextBlue] forState:UIControlStateNormal];
    _btnEdit.titleLabel.font = [UIFont systemFontOfSize:12];
    [_btnEdit addTarget:self action:@selector(btnEditClick:) forControlEvents:UIControlEventTouchUpInside];
    _btnEdit.frame = CGRectMake(DeviceWidth-40, _iconImageView.centerY, 25, 20);
    [self.contentView addSubview:_btnEdit];
    _starR = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(15, _iconImageView.endPointY+10, 73, 13) andStars:5 andStarWidth:12 isFractional:YES spaceWidth:2];
   
    [_starR setHighlightedStar:[UIImage imageNamed:@"starImage"]];
    [_starR updateRating:[NSNumber numberWithDouble: 4]];//[summary.ratingSum doubleValue]/[summary.ratingCount doubleValue]
    [self.contentView addSubview:_starR];
    
    _lbldate = [UILabel new];
//    _lbldate.text = @"Jan 12, 2017";
    _lbldate.textColor = [UIColor color999999];
    _lbldate.font = [UIFont systemFontOfSize:12];
    _lbldate.textAlignment = NSTextAlignmentRight;
    _lbldate.frame = CGRectMake(DeviceWidth-215,_iconImageView.endPointY+10 , 200, 20);
    [self.contentView addSubview:_lbldate];
    
    _lblDetailTitle = [UILabel new];
//    _lblDetailTitle.text = @"Nice Food This is Title.";
    _lblDetailTitle.textColor = [UIColor color999999];
    _lblDetailTitle.font = [UIFont systemFontOfSize:12];
    _lblDetailTitle.frame = CGRectMake(15, _lbldate.endPointY+10, DeviceWidth-30, 20);
    [self.contentView addSubview:_lblDetailTitle];
    
    
    _lblDetail = [UIWebView new];
    _lblDetail.delegate = self;
    _lblDetail.backgroundColor=[UIColor whiteColor];
    _lblDetail.scrollView.showsHorizontalScrollIndicator = NO;
    [self.contentView addSubview:_lblDetail];
  
}

//- (void)resetImageView{
//    if (_detailImageView) {
//        [_detailImageView removeFromSuperview];
//    }
//    if (_detailImageOne) {
//        [_detailImageOne removeFromSuperview];
//    }
//    if (_detailImageTwo) {
//        [_detailImageTwo removeFromSuperview];
//    }
//    if (_detailImageThree) {
//        [_detailImageThree removeFromSuperview];
//    }
//    if (_detailImageFour) {
//        [_detailImageFour removeFromSuperview];
//    }
//    if (_lblMorePicture) {
//        [_lblMorePicture removeFromSuperview];
//    }
//}
//
//- (void)initImage{
//
//    if (_btnAddPicture) {
//        [_btnAddPicture removeFromSuperview];
//    }
//    [self resetImageView];
//
//    _btnAddPicture = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_btnAddPicture setImage:[UIImage imageNamed:@"addphotobtn@2x"] forState:UIControlStateNormal];
//    _btnAddPicture.frame = CGRectMake(0, _lblDetail.endPointY+10, DeviceWidth, 20);
//     [_btnAddPicture addTarget:self action:@selector(AddPictureClick:) forControlEvents:UIControlEventTouchUpInside];
//    [self.contentView addSubview:_btnAddPicture];
//}
//
//- (void)initOneImage{
//
//    [self resetImageView];
//
//    _detailImageView = [UIImageView new];
//    _detailImageView.frame = CGRectMake(0, _lblDetail.endPointY+10, DeviceWidth, DeviceWidth);
//    [self.contentView addSubview:_detailImageView];
//
//}
//- (void)initTwoImage{
//
//    [self resetImageView];
//
//    _detailImageView = [UIImageView new];
//    _detailImageView.frame = CGRectMake(0, _lblDetail.endPointY+10, (DeviceWidth - 5)/2, (DeviceWidth - 5)/2);
//    [self.contentView addSubview:_detailImageView];
//
//    _detailImageOne = [UIImageView new];
//    _detailImageOne.frame = CGRectMake(_detailImageView.endPointX+5, _lblDetail.endPointY+10, (DeviceWidth - 5)/2, (DeviceWidth - 5)/2);
//    [self.contentView addSubview:_detailImageOne];
//}
//
//- (void)initThreeImage{
//
//    [self resetImageView];
//    _detailImageView = [UIImageView new];
//    _detailImageView.frame = CGRectMake(0, _lblDetail.endPointY+10, DeviceWidth, (DeviceWidth - 5)/2);
//    [self.contentView addSubview:_detailImageView];
//
//    _detailImageOne = [UIImageView new];
//    _detailImageOne.frame = CGRectMake(0, _detailImageView.endPointY+5, (DeviceWidth - 5)/2, (DeviceWidth - 5)/2);
//    [self.contentView addSubview:_detailImageOne];
//
//    _detailImageTwo = [UIImageView new];
//    _detailImageTwo.frame = CGRectMake(_detailImageOne.endPointX+5, _detailImageView.endPointY+5, (DeviceWidth - 5)/2,(DeviceWidth - 5)/2);
//    [self.contentView addSubview:_detailImageTwo];
//
//}
//
//- (void)initFourImage{
//
//    [self resetImageView];
//
//    _detailImageView = [UIImageView new];
//    _detailImageView.frame = CGRectMake(0, _lblDetail.endPointY+10, (DeviceWidth - 5)/2, (DeviceWidth - 5)/2);
//    [self.contentView addSubview:_detailImageView];
//
//    _detailImageOne = [UIImageView new];
//    _detailImageOne.frame = CGRectMake(_detailImageView.endPointX+5, _lblDetail.endPointY+10, (DeviceWidth - 5)/2, (DeviceWidth - 5)/2);
//    [self.contentView addSubview:_detailImageOne];
//
//    _detailImageTwo = [UIImageView new];
//    _detailImageTwo.frame = CGRectMake(0, _detailImageView.endPointY+5, (DeviceWidth - 5)/2, (DeviceWidth - 5)/2);
//    [self.contentView addSubview:_detailImageTwo];
//
//    _detailImageThree = [UIImageView new];
//    _detailImageThree.frame = CGRectMake(_detailImageTwo.endPointX+5, _detailImageView.endPointY+5, (DeviceWidth - 5)/2,(DeviceWidth - 5)/2);
//    [self.contentView addSubview:_detailImageThree];
//
//}
//
//- (void)initFiveImage{
//
//    [self resetImageView];
//
//    _detailImageView = [UIImageView new];
//    _detailImageView.frame = CGRectMake(0, _lblDetail.endPointY+10, (DeviceWidth - 5)/2, (DeviceWidth - 5)/2);
//    [self.contentView addSubview:_detailImageView];
//
//    _detailImageOne = [UIImageView new];
//    _detailImageOne.frame = CGRectMake(_detailImageView.endPointX+5, _lblDetail.endPointY+10, (DeviceWidth - 5)/2, (DeviceWidth - 5)/2);
//    [self.contentView addSubview:_detailImageOne];
//
//    _detailImageTwo = [UIImageView new];
//    _detailImageTwo.frame = CGRectMake(0, _detailImageOne.endPointY+5, (DeviceWidth - 10)/3, (DeviceWidth - 10)/3);
//    [self.contentView addSubview:_detailImageTwo];
//
//    _detailImageThree = [UIImageView new];
//    _detailImageThree.frame = CGRectMake(_detailImageTwo.endPointX+5, _detailImageOne.endPointY+5, (DeviceWidth - 10)/3, (DeviceWidth - 10)/3);
//    [self.contentView addSubview:_detailImageThree];
//
//    _detailImageFour = [UIImageView new];
//    _detailImageFour.frame = CGRectMake(_detailImageThree.endPointX+5, _detailImageOne.endPointY+5, (DeviceWidth - 10)/3, (DeviceWidth - 10)/3);
//    [self.contentView addSubview:_detailImageFour];
//
//}
//
//- (void)initMoreImage{
//
//    [self resetImageView];
//    _detailImageView = [UIImageView new];
//    _detailImageView.frame = CGRectMake(0, _lblDetail.endPointY+10, (DeviceWidth - 5)/2, (DeviceWidth - 5)/2);
//    [self.contentView addSubview:_detailImageView];
//
//    _detailImageOne = [UIImageView new];
//    _detailImageOne.frame = CGRectMake(_detailImageView.endPointX+5, _lblDetail.endPointY+10, (DeviceWidth - 5)/2, (DeviceWidth - 5)/2);
//    [self.contentView addSubview:_detailImageOne];
//
//    _detailImageTwo = [UIImageView new];
//    _detailImageTwo.frame = CGRectMake(0, _detailImageOne.endPointY+5, (DeviceWidth - 10)/3, (DeviceWidth - 10)/3);
//    [self.contentView addSubview:_detailImageTwo];
//
//
//    _detailImageThree = [UIImageView new];
//    _detailImageThree.frame = CGRectMake(_detailImageTwo.endPointX+5, _detailImageOne.endPointY+5, (DeviceWidth - 10)/3, (DeviceWidth - 10)/3);
//    [self.contentView addSubview:_detailImageThree];
//
//    _detailImageFour = [UIImageView new];
//    _detailImageFour.frame = CGRectMake(_detailImageThree.endPointX+5, _detailImageOne.endPointY+5, (DeviceWidth - 10)/3, (DeviceWidth - 10)/3);
//    [self.contentView addSubview:_detailImageFour];
//
//    _lblMorePicture = [UIButton buttonWithType:UIButtonTypeCustom];
//    _lblMorePicture.backgroundColor = [UIColor colorWithRed:153/255.0f green:153/255.0f blue:153/255.0f alpha:0.5];
//    [_lblMorePicture setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    _lblMorePicture.titleLabel.font = [UIFont systemFontOfSize:24];
//    [_lblMorePicture addTarget:self action:@selector(lblMorePictureClick:) forControlEvents:UIControlEventTouchUpInside];
//    _lblMorePicture.frame = CGRectMake(_detailImageThree.endPointX+5, _detailImageOne.endPointY+5, (DeviceWidth - 10)/3, (DeviceWidth - 10)/3);
//    [self.contentView addSubview:_lblMorePicture];
//
//}


- (void)gotoRDP{
    NSLog(@"touxiang");
    if (self.homeReviewDelegate) {
        [self.homeReviewDelegate OnePictureIconImageViewTableCell:self indexPath:self.fromCellIndexPath review:self.model];
    }
  
}
- (void)btnEditClick:(UIButton *)btnEdit{
    
    NSLog(@"edit");
    
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    restaurant.restaurantId = _model.restaurantId;
    restaurant.priceLevel = _model.restaurantPriceLevel;
    restaurant.cuisineName = _model.restaurantCuisine;
    restaurant.districtName = _model.restaurantDistrict;
    restaurant.cityName = _model.restaurantCityName;
    restaurant.title = _model.restaurantTitle;
    restaurant.seoKeyword = _model.restaurantSeoKeywords;
    restaurant.imageUrl = _model.restaurantDefaultImageUrl;
    
    if (self.homeReviewDelegate && [self.homeReviewDelegate respondsToSelector:@selector(OnePictureEditBtn:andReviewId:indexPath:)]) {
        [self.homeReviewDelegate OnePictureEditBtn:restaurant andReviewId:_model.reviewId indexPath:self.fromCellIndexPath];
    }
}
- (void)AddPictureClick:(UIButton *)AddPicture{
    NSLog(@"add photo");
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    restaurant.restaurantId = _model.restaurantId;
    restaurant.priceLevel = _model.restaurantPriceLevel;
    restaurant.cuisineName = _model.restaurantCuisine;
    restaurant.districtName = _model.restaurantDistrict;
    restaurant.cityName = _model.restaurantCityName;
    restaurant.title = _model.restaurantTitle;
    restaurant.seoKeyword = _model.restaurantSeoKeywords;
    restaurant.imageUrl = _model.restaurantDefaultImageUrl;
    
    if (self.homeReviewDelegate && [self.homeReviewDelegate respondsToSelector:@selector(OnePictureEditBtn:andReviewId:indexPath:)]) {
        [self.homeReviewDelegate OnePictureEditBtn:restaurant andReviewId:_model.reviewId indexPath:self.fromCellIndexPath];
    }
}


-(void)lblMorePictureClick:(UIButton *)lblMorePictureClick{
    
    NSLog(@"duozhangtupian");
   
}
- (void)setModel:(IMGJourneyReview *)model{
    
    _model = model;
    
    IMGUser *user = [IMGUser currentUser];
    if (![model.userId isEqualToNumber:user.userId]) {
        _btnEdit.hidden = YES;
        _btnAddPicture.hidden = YES;
    }
   
    [_iconImageView sd_setImageWithURL:[NSURL URLWithString:[model.restaurantDefaultImageUrl returnFullImageUrl]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
   
    _lbldate.text = [Date ConvertStrToTime:[NSString stringWithFormat:@"%@",model.reviewTimeCreated]] ?[Date ConvertStrToTime:[NSString stringWithFormat:@"%@",model.reviewTimeCreated]]:@"";

    _lblUserName.text = model.restaurantTitle;
    _lblTittle.text = [NSString stringWithFormat:@"%@ • %@",model.restaurantCuisine, model.restaurantDistrict];
    [_starR updateRating:[NSNumber numberWithDouble:[model.reviewScore doubleValue]]];
    
    BOOL isReadMore = model.isReadMore;
    readMore = isReadMore;
    
    _lblDetailTitle.text = model.reviewTitle ? model.reviewTitle :@"";
    
    NSString *summarize=[NSString stringWithFormat:@"%@",model.reviewSummarize];
    NSDictionary *strBackDic = [PersonalMiddleOnePictureTableViewCell getIsNeedReadMoreForOverLines:summarize];
    
    if([strBackDic[@"isNeedLoadMore"] boolValue]&&!readMore){
        summarize= strBackDic[@"finalStr"];
    }else{
        summarize=[summarize stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
        if(!readMore && [self getStringLengthWithString:summarize]>300){
            
            if ([PersonalMiddleOnePictureTableViewCell stringContainsEmoji:[summarize substringWithRange:NSMakeRange(299,2)]]){
                summarize=[NSString stringWithFormat:@"%@%@",[summarize substringToIndex:299] ,LINK_TEXT];
            }else{
                summarize=[NSString stringWithFormat:@"%@%@",[summarize substringToIndex:300],LINK_TEXT];
            }
            
        }
    }
    CGFloat height = [self calculateCommentTextHeight:summarize];
    //_lblDetail.frame = CGRectMake(15, _lblDetailTitle.endPointY+10, DeviceWidth-30, height);
    
    if ([model.reviewSummarize isEqualToString:@""]) {
        height = 0;
    }
//    NSLog(@"*****%f",_lblDetail.endPointY);
    NSString *summarizeStr = [NSString stringWithFormat:@"<html> <head> <meta charset=\"UTF-8\">"
                              "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">"
                              "<style type=\"text/css\">"
                              "html {"
                              "    -webkit-text-size-adjust: none; /* Never autoresize text */"
                              "}"
                              "body{font-family:\"%@\";font-size: 12px; color:%@; padding:0; margin:0; width:%fpx; line-height: 15px;}"
                              "</style>"
                              "</head>"
                              "<body><div id=\"content\">%@</div></body>"
                              "</html>",DEFAULT_FONT_NAME,@"#929da7",DeviceWidth - LEFTLEFTSET*2,summarize];
    
    summarizeStr = [summarizeStr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
    [_lblDetail loadHTMLString:summarizeStr baseURL:nil];

    
    

    CGRect rect = _lblDetailTitle.frame;
    if ([model.reviewTitle isEqualToString:@""]) {
        rect.size.height = 0;
    }else{
        rect.size.height = 20;
    }
    
    
    _lblDetailTitle.frame = rect;
    _lblDetail.frame = CGRectMake(15, _lblDetailTitle.endPointY, DeviceWidth-30, height);
 
    if (photoView) {
        [photoView removeFromSuperview];
    }
    
    photoView = [[ProfileJourneyPhotosView alloc] initWithFrame:CGRectMake(0, _lblDetail.endPointY+10, DeviceWidth, [ProfileJourneyPhotosView cardPhotoHeightWithPhotoCout:model.dishList.count])];
    photoView.photoArray = model.dishList;
    [self.contentView addSubview:photoView];
    
    _cellHeight = photoView.endPointY;
    
//    NSLog(@"height:%f",_bottomView.endPointY);
}
+(NSDictionary *)getIsNeedReadMoreForOverLines:(NSString*)commentStr_{
    
    UILabel *lblTempStr = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, CONTENTLABELWIDTH, MAXFLOAT)];
    lblTempStr.text = commentStr_;
    lblTempStr.font = [UIFont systemFontOfSize:12];
    
    
    NSString *text = [lblTempStr text];
    UIFont   *font = [lblTempStr font];
    CGRect    rect = [lblTempStr frame];
    
    
    CTFontRef myFont = CTFontCreateWithName((CFStringRef)font.fontName,
                                            font.pointSize,
                                            NULL);
    
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:text];
    [attStr addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)myFont range:NSMakeRange(0, attStr.length)];
    
    CTFramesetterRef frameSetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)attStr);
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, CGRectMake(0,0,rect.size.width,100000));
    
    CTFrameRef frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, 0), path, NULL);
    
    NSArray *lines = (__bridge NSArray *)CTFrameGetLines(frame);
    
    NSMutableArray *linesArray = [[NSMutableArray alloc]init];
    
    NSMutableDictionary *strBackDic = [[NSMutableDictionary alloc]init];
    
    if(lines.count<10+1){
        [strBackDic setObject:@NO forKey:@"isNeedLoadMore"];
    }else{
        [strBackDic setObject:@YES forKey:@"isNeedLoadMore"];
        for (id line in lines)
        {
            CTLineRef lineRef = (__bridge CTLineRef)line;
            CFRange lineRange = CTLineGetStringRange(lineRef);
            NSRange range = NSMakeRange(lineRange.location, lineRange.length);
            
            NSString *lineString = [text substringWithRange:range];
            
            //            CFRelease(lineRef);
            [linesArray addObject:lineString];
            
        }
        
        //start range work for 4th line
        NSMutableAttributedString *attStr4Th = [[NSMutableAttributedString alloc] initWithString:linesArray[10-1]];
        [attStr4Th addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)myFont range:NSMakeRange(0, attStr4Th.length)];
        CTFramesetterRef frameSetter4Th = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)attStr4Th);
        
        CGMutablePathRef path4Th = CGPathCreateMutable();
        CGPathAddRect(path4Th, NULL, CGRectMake(0,0,CONTENTLABELWIDTH-70,MAXFLOAT));
        
        CTFrameRef frame4Th = CTFramesetterCreateFrame(frameSetter4Th, CFRangeMake(0, 0), path4Th, NULL);
        
        NSArray *lines4Th = (__bridge NSArray *)CTFrameGetLines(frame4Th);
        CTLineRef lineRef4Th = (__bridge CTLineRef )[lines4Th firstObject];
        CFRange lineRange4Th = CTLineGetStringRange(lineRef4Th);
        NSRange range4Th = NSMakeRange(lineRange4Th.location, lineRange4Th.length);
        NSString *lineString4Th = [linesArray[10-1] substringWithRange:range4Th];
        
        NSString *finalStr = @"";
        for(int i=0; i<10-1; i++){
            finalStr = [finalStr stringByAppendingString:linesArray[i]];
        }
        lineString4Th=[lineString4Th stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        finalStr = [finalStr stringByAppendingString:lineString4Th];
        //        finalStr = [finalStr stringByAppendingString:@"<a href=\"http://www.qraved.com/\">...read more</a>"];
        
        finalStr = [finalStr stringByAppendingString:LINK_TEXT];
        
        
        finalStr=[finalStr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
        [strBackDic setObject:finalStr forKey:@"finalStr"];
        
        CFRelease(frame4Th);
        CFRelease(frameSetter4Th);
        CFRelease(path4Th);
        //        CFRelease(lineRef4Th);
        
    }
    
    CFRelease(frame);
    CFRelease(myFont);
    CFRelease(frameSetter);
    CFRelease(path);
    return strBackDic;
}

- (NSInteger)getStringLengthWithString:(NSString *)string
{
    __block NSInteger stringLength = 0;
    
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length])
                               options:NSStringEnumerationByComposedCharacterSequences
                            usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop)
     {
         const unichar hs = [substring characterAtIndex:0];
         if (0xd800 <= hs && hs <= 0xdbff)
         {
             if (substring.length > 1)
             {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f)
                 {
                     stringLength += 1;
                 }
                 else
                 {
                     stringLength += 1;
                 }
             }
             else
             {
                 stringLength += 1;
             }
         } else if (substring.length > 1)
         {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3)
             {
                 stringLength += 1;
             }
             else
             {
                 stringLength += 1;
             }
         } else {
             if (0x2100 <= hs && hs <= 0x27ff)
             {
                 stringLength += 1;
             }
             else if (0x2B05 <= hs && hs <= 0x2b07)
             {
                 stringLength += 1;
             }
             else if (0x2934 <= hs && hs <= 0x2935)
             {
                 stringLength += 1;
             }
             else if (0x3297 <= hs && hs <= 0x3299)
             {
                 stringLength += 1;
             }
             else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50)
             {
                 stringLength += 1;
             }
             else
             {
                 stringLength += 1;
             }
         }
     }];
    
    return stringLength;
}
+ (BOOL)stringContainsEmoji:(NSString *)string
{
    __block BOOL returnValue = NO;
    
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length])
                               options:NSStringEnumerationByComposedCharacterSequences
                            usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                                const unichar hs = [substring characterAtIndex:0];
                                if (0xd800 <= hs && hs <= 0xdbff) {
                                    if (substring.length > 1) {
                                        const unichar ls = [substring characterAtIndex:1];
                                        const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                                        if (0x1d000 <= uc && uc <= 0x1f77f) {
                                            returnValue = YES;
                                        }
                                    }
                                } else if (substring.length > 1) {
                                    const unichar ls = [substring characterAtIndex:1];
                                    if (ls == 0x20e3) {
                                        returnValue = YES;
                                    }
                                } else {
                                    if (0x2100 <= hs && hs <= 0x27ff) {
                                        returnValue = YES;
                                    } else if (0x2B05 <= hs && hs <= 0x2b07) {
                                        returnValue = YES;
                                    } else if (0x2934 <= hs && hs <= 0x2935) {
                                        returnValue = YES;
                                    } else if (0x3297 <= hs && hs <= 0x3299) {
                                        returnValue = YES;
                                    } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
                                        returnValue = YES;
                                    }
                                }
                            }];
    
    return returnValue;
}
-(CGFloat)calculateCommentTextHeight:(NSString*)commentStr_{
    
    commentStr_=[commentStr_ stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    commentStr_=[commentStr_ stringByReplacingOccurrencesOfString:LINK_TEXT withString:@"...read more"];
    
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12]};
    
    CGSize textSize = [commentStr_ boundingRectWithSize:CGSizeMake(DeviceWidth - LEFTLEFTSET*2, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    
    float height = ceil(textSize.height + 1.5f);
    
    return height;
}
#pragma mark - webview delegate

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    CGFloat htmlHeight = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] floatValue];
    //    webView.sd_layout
    //    .heightIs(htmlHeight);
    
    UIScrollView *tempView = (UIScrollView *)[_lblDetail.subviews objectAtIndex:0];
    tempView.scrollEnabled = NO;
    
    
    
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if (navigationType==UIWebViewNavigationTypeLinkClicked) {
        readMore = !readMore;
        
        if (self.homeReviewDelegate) {
            [self.homeReviewDelegate HomeReviewTableCell:self readMoreTapped:_model indexPath:self.fromCellIndexPath];
        }
        
        return NO;
    }
    
    return YES;
    
    
}
//- (void) dealloc
//{
//    if(_lblDetail){
//        [_lblDetail stopLoading];
//        _lblDetail.delegate = nil;
//    }
//}
@end
