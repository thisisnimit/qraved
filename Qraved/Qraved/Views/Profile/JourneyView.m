//
//  JourneyView.m
//  Qraved
//
//  Created by imaginato on 16/9/8.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "JourneyView.h"
#import "JNReviewTableViewCell.h"
#import "JourneyHandler.h"
#import "IMGUser.h"
#import "UITableView+SDAutoTableViewCellHeight.h"
#import "UIView+SDAutoLayout.h"
#import "JNUploadPhotoTableViewCell.h"
#import "JNReservationTableViewCell.h"
#import "Label.h"
#import "IMGDish.h"
#import "CustomIOS7AlertView.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "AppDelegate.h"
#import "IMGUser.h"
#import "Date.h"
#import "UITableView+SDAutoTableViewCellHeight.h"


@implementation JourneyView
{
    NSMutableArray *dataArr;
    UIView *hotKeyView;
    int offset;
    int max;
    IMGUser *user;
    BOOL isOtherUser;
    BOOL isNeedReadmore;
    BOOL loadSuccess;
}
-(void)setScrollToTop:(BOOL)isScrollToTop{
    _tableView.scrollsToTop = isScrollToTop;
}
-(instancetype)initWithFrame:(CGRect)frame withOtherUser:(IMGUser*)otherUser{

    if ([super initWithFrame:frame]) {
        self.OtherUser=otherUser;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ReloadReviewsView_v1WithUpdateUserReview:) name:@"ReloadReviewsView_v1WithUpdateUserReview" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadJourneyUploadPhoto:) name:@"reloadJourneyUploadPhoto" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadJourneyBook:) name:@"reloadJourneyBook" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadDishIdAndDescription:) name:@"reloadDishIdAndDescription" object:nil];
        
        if ( (otherUser!=nil) && (![[IMGUser currentUser].userId isEqualToNumber:otherUser.userId])) {
            isOtherUser=YES;
        }
        
        dataArr = [[NSMutableArray alloc] init];
        max = 10;
        self.filterType = [NSNumber numberWithInt:0];
        
        _tableView =[[PullTableView alloc]initWithFrame:self.bounds];
        _tableView.isFromJourney = YES;
        _tableView.delegate=self;
        _tableView.dataSource=self;
        _tableView.pullDelegate = self;
        [_tableView setLoadMoreViewWithBackGroundColor:[UIColor clearColor]];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        CGRect frame  = _tableView.frame;
        frame.size.height = frame.size.height + 44;
        _tableView.frame = frame;
        
        [self addSubview:_tableView];
        if (isOtherUser) {
            user=otherUser;
        }else{
            user = [IMGUser currentUser];
            if(self.isHiddenHotkey) {
                [self addHotKeyButton];
            }
        }

        [self initData];
        NSDictionary *params = @{@"offset":[NSNumber numberWithInt:0],@"max":[NSNumber numberWithInt:10],@"filterType":[NSNumber numberWithInt:0],@"userId":user.userId,@"t":user.token};
        if (isOtherUser) {
            [JourneyHandler getOtherJourneyList:params andBlock:^(NSArray *journeyDataArr) {
                dataArr = [NSMutableArray arrayWithArray:journeyDataArr];
                [_tableView reloadDataWithExistedHeightCache];
                
            }];
        }else{
            [JourneyHandler getJourneyList:params andBlock:^(NSArray *journeyDataArr) {
                dataArr = [NSMutableArray arrayWithArray:journeyDataArr];
                [_tableView reloadDataWithExistedHeightCache];
            }];
        }}
    return self;
}
- (void)reloadData
{
    offset = 0;
    [[LoadingView sharedLoadingView] startLoading];
    [self initData];
}
- (void)reloadDataScrollToTop
{
    if (dataArr.count != 0) {
        NSIndexPath *ip=[NSIndexPath indexPathForRow:0 inSection:0];
        [_tableView selectRowAtIndexPath:ip animated:NO scrollPosition:UITableViewScrollPositionBottom];
    }
    [self reloadData];
}
- (void)initData
{
//    IMGUser *user = [IMGUser currentUser];
    NSDictionary *params = @{@"offset":[NSNumber numberWithInt:offset],@"max":[NSNumber numberWithInt:max],@"filterType":self.filterType,@"userId":user.userId,@"t":user.token};
    if (isOtherUser) {
        [JourneyHandler getOtherJourneyList:params andBlock:^(NSArray *journeyDataArr) {
            if (journeyDataArr.count<max) {
                loadSuccess=YES;
                _tableView.loadMoreView.isEndLoadMore = YES;
                [_tableView.loadMoreView removeFromSuperview];
            }else{
                if (_tableView.loadMoreView.isEndLoadMore) {
                    _tableView.loadMoreView.isEndLoadMore = NO;
                    [_tableView setLoadMoreViewWithBackGroundColor:[UIColor clearColor]];
                }
            }
            if (offset == 0)
            {
                dataArr = nil;
            }
            
            if (dataArr)
            {
                [dataArr addObjectsFromArray:journeyDataArr];
            }
            else
            {
                dataArr = [[NSMutableArray alloc] initWithArray:journeyDataArr];
            }
            [_tableView reloadDataWithExistedHeightCache];
            _tableView.pullTableIsLoadingMore = NO;
            [[LoadingView sharedLoadingView] stopLoading];
        }];
    }else{
        [JourneyHandler getJourneyList:params andBlock:^(NSArray *journeyDataArr) {
            if (journeyDataArr.count<max) {
                loadSuccess=YES;
                _tableView.loadMoreView.isEndLoadMore = YES;
                [_tableView.loadMoreView removeFromSuperview];
            }else{
                if (_tableView.loadMoreView.isEndLoadMore) {
                    _tableView.loadMoreView.isEndLoadMore = NO;
                    [_tableView setLoadMoreViewWithBackGroundColor:[UIColor clearColor]];
                }
            }
            if (offset == 0)
            {
                dataArr = nil;
            }
            
            if (dataArr)
            {
                [dataArr addObjectsFromArray:journeyDataArr];
            }
            else
            {
                dataArr = [[NSMutableArray alloc] initWithArray:journeyDataArr];
            }
            [_tableView reloadDataWithExistedHeightCache];
            _tableView.pullTableIsLoadingMore = NO;
            [[LoadingView sharedLoadingView] stopLoading];
        }];
    }
}


- (void)loadData{
    
    if(offset<dataArr.count)
    {
        offset+=max;
        
        [self initData];
    }else{
        _tableView.pullTableIsLoadingMore = YES;
    }
}


#pragma mark pullTableDelegate



- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    [self performSelector:@selector(loadData) withObject:nil afterDelay:1.0f];
}

- (void)addHotKeyButton
{
    UIButton *hotKeyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    hotKeyBtn.frame = CGRectMake(DeviceWidth - 65, DeviceHeight - 115 - 44, 50, 50);
    [hotKeyBtn setImage:[UIImage imageNamed:@"hotKey"] forState:UIControlStateNormal];
    [self addSubview:hotKeyBtn];
    [hotKeyBtn addTarget:self action:@selector(addHotKeyView) forControlEvents:UIControlEventTouchUpInside];
}


- (void)addHotKeyView
{
    if (hotKeyView)
    {
        [self addSubview:hotKeyView];
    }
    else
    {
        hotKeyView = [[UIView alloc] init];
        hotKeyView.frame = self.bounds;
        hotKeyView.backgroundColor = [UIColor clearColor];
        [self addSubview:hotKeyView];
        
        UIView *backGroundView = [[UIView alloc] init];
        backGroundView.frame = CGRectMake(0, 0, DeviceWidth, DeviceHeight);
        [hotKeyView addSubview:backGroundView];
        backGroundView.backgroundColor = [UIColor blackColor];
        backGroundView.alpha = 0.3f;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(backGroundViewSelected:)];
        [backGroundView addGestureRecognizer:tapGesture];
        
        
        UIView *buttonView = [[UIView alloc] init];
        buttonView.backgroundColor = [UIColor whiteColor];
        [hotKeyView addSubview:buttonView];
        buttonView.frame = CGRectMake(0, DeviceHeight - 150 - 44, DeviceWidth, 150);
        
        
        NSArray *buttonsArray = @[L(@"Upload Photo"),L(@"Write Review"),L(@"Booking Now")];
        NSArray *imagesArray = @[@"hotkeyphoto",@"hotkeyreview",@"profileBook",@"hotkeysave",@"hotkeyplace"];
        CGFloat hotkeyWidth=(DeviceWidth)/buttonsArray.count;
        for (int i = 0; i<buttonsArray.count; i++)
        {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(hotkeyWidth*i,25, hotkeyWidth, 50);
            button.tag = 500+i;
            [button addTarget:self action:@selector(bottomButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake((hotkeyWidth-47)/2, 25, 47, 40) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12] andTextColor:[UIColor color333333] andTextLines:1];
            titleLabel.tag = 500+i;
            titleLabel.alpha = 0.8;
            titleLabel.textAlignment = NSTextAlignmentCenter;
            [button addSubview:titleLabel];
            titleLabel.numberOfLines = 0;
            titleLabel.text = [buttonsArray objectAtIndex:i];
            
            UITapGestureRecognizer *tapGeature = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(bottomButtonLabelTapped:)];
            [titleLabel addGestureRecognizer:tapGeature];
            
            UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake((hotkeyWidth-20)/2, 0, 20, 20)];
            iconImage.image = [UIImage imageNamed:[imagesArray objectAtIndex:i]];
            [button addSubview:iconImage];
            
            [buttonView addSubview:button];
        }
    }
    
//    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"isFirstHomeTutorialPage"])
//        //    if (1)
//    {
//        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"isFirstHomeTutorialPage"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        tutorialStrArr = [[NSArray alloc] initWithObjects:@"Yuk tulis review restoran yang pernah dikunjungi",@"Pamerkan fotomu ke foodies lainnya",@"Buat daftar restoran favoritmu!",@"Kasih tau kita tempat makan baru yang kamu tau",@"Cape jadi waiting list? Booking aja yuk!", nil];
//        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
//        IMGUser *user = [IMGUser currentUser];
//        if ([user.userId intValue]) {
//            [eventProperties setValue:user.userId forKey:@"UserID"];
//            [eventProperties setValue:user.userName forKey:@"UserName"];
//        }
//        [eventProperties setValue:@"hotKeyFirst" forKey:@"HotKey"];
//        [[Amplitude instance] logEvent:@"AT - Hot Key Continue" withEventProperties:eventProperties];
//        
//        [self addTutorialViewWithCurrentTag:0];
//    }
    
}

-(void)bottomButtonTapped:(UIButton*)button
{
    [self getRestaurantArrayForNextView:(int)button.tag-500];
}

-(void)bottomButtonLabelTapped:(UITapGestureRecognizer *)gesture
{
    [self getRestaurantArrayForNextView:(int)gesture.view.tag-500];
}

-(void)getRestaurantArrayForNextView:(int)tag{
    
    [self backGroundViewSelected:nil];
    
    [self.delegate didClickHotKeyBtnWithTag:tag];
        
}

- (void)backGroundViewSelected:(UITapGestureRecognizer *)tap
{
    [hotKeyView removeFromSuperview];
}


#pragma --Mark TanleViewDelgate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (dataArr.count==0&&loadSuccess) {
        return 1;
    }
    return dataArr.count;

}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    
    if (dataArr.count==0&&loadSuccess) {
        
        UITableViewCell* noJourneyCell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        UIView* line=[[UIView alloc]initWithFrame:CGRectMake(20, 0, 1, tableView.bounds.size.height)];
        line.backgroundColor=[UIColor grayColor];
        line.alpha=0.4f;
        [noJourneyCell.contentView addSubview:line];
        UIImageView* imageview=[[UIImageView alloc]initWithFrame:CGRectMake(8, 15, 25, 25)];
        imageview.image=[UIImage imageNamed:@"history_qraved"];
        [noJourneyCell.contentView addSubview:imageview];
        UILabel* lable=[[UILabel alloc]initWithFrame:CGRectMake(15+imageview.right, 18, noJourneyCell.frame.size.width, 20)];
        lable.font = [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:12];
        lable.textColor = [UIColor grayColor];
        NSString* timeSp = [Date getTimeInteval_v2:user.dateCreated];
        lable.text=[NSString stringWithFormat:@"Started using Qraved %@",timeSp];
        [noJourneyCell.contentView addSubview:lable];
        tableView.userInteractionEnabled=NO;
        return noJourneyCell;
    }else{
        tableView.userInteractionEnabled=YES;
        IMGJourney *journey = [dataArr objectAtIndex:indexPath.row];
        BOOL isLastCell = ( (dataArr.count - 1) == indexPath.row );
        switch ([journey.type intValue])
        {
            case 1: case 4:
            {
                JNReviewTableViewCell* cell=[tableView dequeueReusableCellWithIdentifier:@"reviewCell"];
                if (!cell) {
                    cell=[[JNReviewTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reviewCell"];
                }
                cell.JNDelegate = self;
                cell.isOther=isOtherUser;
                cell.review = [dataArr objectAtIndex:indexPath.row];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.indexPath = indexPath;
                cell.bottomLineView.hidden = isLastCell;
                return cell;
            }
                break;
            case 2:
            {
                JNReservationTableViewCell* cell=[tableView dequeueReusableCellWithIdentifier:@"reservationCell"];
                if (!cell) {
                    cell=[[JNReservationTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reservationCell"];
                    cell.delegate=self;
                    cell.JNDelegate = self;
                    
                }
                cell.isOtherUser=isOtherUser;
                cell.reservation = [dataArr objectAtIndex:indexPath.row];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.indexPath = indexPath;
                cell.bottomLineView.hidden = isLastCell;
                return cell;
                
            }
                break;
                
            case 3:
            {
                JNUploadPhotoTableViewCell* cell=[tableView dequeueReusableCellWithIdentifier:@"uploadPhotoCell"];
                if (!cell) {
                    cell=[[JNUploadPhotoTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"uploadPhotoCell"];
                    
                }
                cell.isOtherUser=isOtherUser;
                cell.JNDelegate = self;
                cell.delegate=self;
                cell.uploadPhoto = [dataArr objectAtIndex:indexPath.row];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.indexPath = indexPath;
                cell.bottomLineView.hidden = isLastCell;
                return cell;
            }
                break;
                
            default:
                break;
        }
    }
    return 0;
}

- (void)didClickUploadPhotoEditBtn:(IMGJNUploadPhoto *)_uploadPhoto indexPath:(NSIndexPath *)indexPath{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didClickUploadPhotoJNCellEditBtn:indexPath:)]) {
        [self.delegate didClickUploadPhotoJNCellEditBtn:_uploadPhoto indexPath:indexPath];
    }
}
- (void)didClickPhotoWithTag:(NSInteger)photoTag andDishArr:(NSArray *)dishArr
{
    [self.delegate didClickJNCellPhotoWithPhotoTag:photoTag andDishArr:dishArr];
}
-(void)journeyReserationCancelClick:(IMGJNReservation *)reservation{
    
    UIView *popUpView = [[UIView alloc]init];
    
    NSString *offerTitleStr = @"Are you sure want to cancel your booking?";
    
    CGSize maxSize = [offerTitleStr sizeWithFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
    Label *seeMenuTitle = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 64/2-(maxSize.height+4)/2, maxSize.width, maxSize.height+4) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] andTextColor:[UIColor color333333] andTextLines:0];
    seeMenuTitle.text = offerTitleStr;
    seeMenuTitle.textAlignment = NSTextAlignmentCenter;
    [popUpView addSubview:seeMenuTitle];
    
    NSArray *menusArr = @[@"We appreciate the gesture. This means other guests can get a seat if you can't attend."];
    float startY = seeMenuTitle.endPointY ;
    for (int i=0; i<menusArr.count; i++) {
        CGSize maxSize1 = [[menusArr objectAtIndex:i] sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
        
        Label *seeMenuLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, startY, DeviceWidth-4*LEFTLEFTSET, maxSize1.height+20) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15] andTextColor:[UIColor color333333] andTextLines:0];
        seeMenuLabel.textAlignment = NSTextAlignmentCenter;
        seeMenuLabel.text = [menusArr objectAtIndex:i];
        seeMenuLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [popUpView addSubview:seeMenuLabel];
        startY = seeMenuLabel.endPointY;
    }
    popUpView.frame = CGRectMake(0, 0, DeviceWidth-2*LEFTLEFTSET, startY);
    
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc]init];
    alertView.buttonTitles = @[@"No, keep it",@"Yes"];
    [alertView setContainerView:popUpView];
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, NSInteger buttonIndex) {
        if(buttonIndex==1){
            [[LoadingView sharedLoadingView] startLoading];
            //            IMGUser * user = [IMGUser currentUser];
            if(user.userId==nil){
                //lead to login
                return;
            }
            if(user.token==nil){
                //lead to login
                return;
            }
            NSString *newStatus=@"13";
            if([[NSString stringWithFormat:@"%@",reservation.status] isEqualToString:@"11"]){
                newStatus=@"14";
            }else if([[NSString stringWithFormat:@"%@",reservation.status] isEqualToString:@"2"]){
                newStatus=@"3";
            }
            NSDictionary * parameters = @{@"userID":user.userId,@"t":user.token,@"id":reservation.reservationId,@"status":newStatus};
                            [[IMGNetWork sharedManager]POST:@"reseravtion/status/update" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
            
                                NSString *exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
                                if([exceptionMsg isLogout]){
                                    [[LoadingView sharedLoadingView] stopLoading];
                                    [[IMGUser currentUser]logOut];
                                    [[AppDelegate ShareApp] goToLoginController];
                                    return;
                                }
            
                                NSString *returnStatusString = [responseObject objectForKey:@"status"];
                                if([returnStatusString isEqualToString:@"succeed"]){
                                    [[DBManager manager]selectWithSql:[NSString stringWithFormat:@"select * from IMGRestaurant where restaurantId=%@ limit 1",reservation.restaurantId] successBlock:^(FMResultSet *resultSet) {
                                        if ([resultSet next]) {
                                            IMGRestaurant *restaurant = [[IMGRestaurant alloc]init];
                                            [restaurant setValueWithResultSet:resultSet];
                                            [[NSUserDefaults standardUserDefaults]setObject:[NSDate date] forKey:@"dateCancel"];
                                            //                            [MixpanelHelper trackCancelBookingWithRestaurant:restaurant andReservation:reservation];
                                        }
                                    } failureBlock:^(NSError *error) {
            
                                    }];
//                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"cancelUpcomingBookingNotification" object:nil userInfo:@{@"reservationId":reservation.reservationId}];
                                    [self cancelUpcomingBooking:[reservation.reservationId integerValue]];
                                    [self.delegate cancelUpcomingBooking:[reservation.reservationId integerValue]];
            
                                }
                                [[LoadingView sharedLoadingView]stopLoading];
                            } failure:^(NSURLSessionDataTask *operation, NSError *error) {
                                NSLog(@"MainMenuViewController.request cancel book.error: %@",error.localizedDescription);
                                [[LoadingView sharedLoadingView]stopLoading];
                            }];
                   }
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    [alertView show];
    
}
-(void)cancelUpcomingBooking:(NSInteger)reservationId{
    NSLog(@"%s",__func__);
    for (int i = 0; i<dataArr.count; i++) {
        id item = dataArr[i];
        if ([item isKindOfClass:[IMGJNReservation class]]) {
            IMGJNReservation *reservation = (IMGJNReservation *)item;
            if([reservation.reservationId intValue]==reservationId){
                reservation.status = @13;
                [dataArr replaceObjectAtIndex:i withObject:reservation];

                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                return;
            }
        }

    }
}
- (void)didClickRestTitleLableWithRestId:(NSNumber *)restaurantId
{
    [self.delegate didClickJNCellRestTitleLableWithRestId:restaurantId];
}
-(void)didClickEditBtn:(IMGRestaurant *)restaurant andReviewId:(NSNumber *)reviewId indexPath:(NSIndexPath *)indexPath{
    if (self.delegate &&[self.delegate respondsToSelector:@selector(didClickJNCellEditBtn:andReviewId:indexPath:)]) {
        [self.delegate didClickJNCellEditBtn:restaurant andReviewId:reviewId indexPath:indexPath];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    if (dataArr.count==0) {
        return tableView.bounds.size.height;
    }else{
    
    
        IMGJourney *journey = [dataArr objectAtIndex:indexPath.row];
        BOOL isLastCell = ( (dataArr.count - 1) == indexPath.row );
        switch ([journey.type intValue])
        {
            case 1: case 4:
            {
                return [tableView cellHeightForIndexPath:indexPath model:dataArr[indexPath.row] keyPath:@"review" cellClass:[JNReviewTableViewCell class] contentViewWidth:[UIScreen mainScreen].bounds.size.width]+(isLastCell?50.0:0.0);
            }
                break;
            case 2:
            {
                return [tableView cellHeightForIndexPath:indexPath model:dataArr[indexPath.row] keyPath:@"reservation" cellClass:[JNReservationTableViewCell class] contentViewWidth:[UIScreen mainScreen].bounds.size.width]+(isLastCell?50.0:0.0);
            }
                break;
                
            case 3:
            {
                return [tableView cellHeightForIndexPath:indexPath model:dataArr[indexPath.row] keyPath:@"uploadPhoto" cellClass:[JNUploadPhotoTableViewCell class] contentViewWidth:[UIScreen mainScreen].bounds.size.width]+(isLastCell?50.0:0.0);
            }
                break;
                
            default:
                break;
        }
    }
    return 0;

}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ReloadReviewsView_v1WithUpdateUserReview" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"reloadJourneyUploadPhoto" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"reloadJourneyBook" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"reloadDishIdAndDescription" object:nil];

}
-(void)journeyUploadPhotoCancel:(UITableViewCell *)uploadPhotoCell
{
    NSIndexPath *index = [_tableView indexPathForCell:uploadPhotoCell];
    NSInteger cellRow=   index.row;
    [dataArr removeObjectAtIndex:cellRow];
    if (dataArr.count != 0) {
        [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:index] withRowAnimation:UITableViewRowAnimationFade];
    }
    [_tableView reloadDataWithExistedHeightCache];
}
-(void)JourneyReserrationMapClick:(IMGRestaurant *)restaurant  withReservation:(IMGJNReservation*)reservation{
    
    if (self.delegate) {
        [self.delegate reservationMapClick:restaurant with:reservation];
    }
    
}
-(void)reloadDishIdAndDescription:(NSNotification *)notification{
    NSMutableArray *dishIdAndDescription = [notification.userInfo objectForKey:@"dishIdAndDescription"];
    NSIndexPath *indexPath = [notification.userInfo objectForKey:@"indexPath"];
    IMGJNUploadPhoto *uploadPhoto = [dataArr objectAtIndex:indexPath.row];
    for (IMGDish *item in uploadPhoto.dishArr) {
        for (NSMutableDictionary *dishDic in dishIdAndDescription) {
            if ([[dishDic objectForKey:@"dishId"] intValue] == [item.dishId intValue]) {
                item.descriptionStr = [dishDic objectForKey:@"description"];
            }
        }
    }
}
-(void)reloadJourneyBook:(NSNotification *)notification{
    [self reloadData];
}
-(void)reloadJourneyUploadPhoto:(NSNotification *)notification{
    NSIndexPath *indexPath = [notification.userInfo objectForKey:@"indexPath"];
    NSArray *dishList = [notification.userInfo objectForKey:@"responseObject"];
    NSArray *delDishIdArr = [notification.userInfo objectForKey:@"delDishIdArr"];
    if ( (indexPath == nil) || (dishList == nil) ) {
        [self reloadData];
        return;
    }
    IMGJNUploadPhoto *uploadPhoto = [dataArr objectAtIndex:indexPath.row];
    
    NSMutableArray *newAddArr = [[NSMutableArray alloc] init];
    if (dishList!=nil) {
        for (NSDictionary *item in dishList){
            NSDictionary *dic = [item objectForKey:@"dishMap"];
            
            IMGDish *dish = [[IMGDish alloc] init];
            dish.dishId = [dic objectForKey:@"id"];
            dish.imageUrl = [dic objectForKey:@"imageUrl"];
            dish.title = [dic objectForKey:@"title"];
            dish.descriptionStr = [dic objectForKey:@"description"];
            dish.createTime = [dic objectForKey:@"createTimeStr"];
            NSDictionary *photoCreditDic = [dic objectForKey:@"photoCredit"];
            dish.userName = [photoCreditDic objectForKey:@"photoCredit"];
            dish.photoCreditTypeDic = [photoCreditDic objectForKey:@"photoCreditType"];
            dish.restaurantId = [photoCreditDic objectForKey:@"restaurantId"];
            dish.userAvatarDic = [photoCreditDic objectForKey:@"userAvatar"];
            dish.userId = [photoCreditDic objectForKey:@"userId"];
            dish.userPhotoCountDic = [photoCreditDic objectForKey:@"userPhotoCount"];
            dish.userReviewCountDic = [photoCreditDic objectForKey:@"userReviewCount"];
            [newAddArr addObject:dish];
        }
    }
    
    NSMutableArray *oldAllDishArr = [[NSMutableArray alloc] init];
    for (IMGDish *item in uploadPhoto.dishArr) {
        if ((delDishIdArr == nil) || (![delDishIdArr containsObject:[NSString stringWithFormat:@"%@",item.dishId]])) {
            [oldAllDishArr addObject:item];
        }
    }

    [oldAllDishArr addObjectsFromArray:newAddArr];
    uploadPhoto.dishArr = oldAllDishArr;
    uploadPhoto.isother=isOtherUser;
    [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:NO];
    [_tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:NO];
}

- (void)ReloadReviewsView_v1WithUpdateUserReview:(NSNotification *)notification{
    NSDictionary *newReviewDic = [notification.userInfo objectForKey:@"newReview"];
    NSIndexPath *indexPath = [notification.userInfo objectForKey:@"indexPath"];
    if ( (indexPath == nil) || (newReviewDic == nil ) ) {
        [self reloadData];
        return;
    }
    IMGJNReview *jnReview = [dataArr objectAtIndex:indexPath.row];
    jnReview.reviewScore = [newReviewDic objectForKey:@"score"];
    jnReview.reviewTitle = [newReviewDic objectForKey:@"title"];
    jnReview.reviewSummarize = [newReviewDic objectForKey:@"summarize"];

    NSArray *dishList = [newReviewDic objectForKey:@"dishList"];
    NSMutableArray *dishArrM = [[NSMutableArray alloc] init];
    for (NSDictionary *dic in dishList){
        IMGDish *dish = [[IMGDish alloc] init];
        dish.dishId = [dic objectForKey:@"id"];
        dish.imageUrl = [dic objectForKey:@"imageUrl"];
        dish.title = [dic objectForKey:@"title"];
        dish.descriptionStr = [dic objectForKey:@"description"];
        dish.createTime = [dic objectForKey:@"createTimeStr"];
        NSDictionary *photoCreditDic = [dic objectForKey:@"photoCredit"];
        dish.userName = [photoCreditDic objectForKey:@"photoCredit"];
        dish.photoCreditTypeDic = [photoCreditDic objectForKey:@"photoCreditType"];
        dish.restaurantId = [photoCreditDic objectForKey:@"restaurantId"];
        dish.userAvatarDic = [photoCreditDic objectForKey:@"userAvatar"];
        dish.userId = [photoCreditDic objectForKey:@"userId"];
        dish.userPhotoCountDic = [photoCreditDic objectForKey:@"userPhotoCount"];
        dish.userReviewCountDic = [photoCreditDic objectForKey:@"userReviewCount"];
        [dishArrM addObject:dish];
    }
    jnReview.dishArr = [NSArray arrayWithArray:dishArrM];

    [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath,nil] withRowAnimation:NO];
    [_tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionNone animated:NO];
}
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    if (self.delegate && [self.delegate respondsToSelector:@selector(journeyScrollNavigationController:)]) {
//        [self.delegate journeyScrollNavigationController:scrollView];
//    }
//}


@end
