//
//  JourneyCouponTableViewCell.h
//  Qraved
//
//  Created by harry on 2018/2/1.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CouponModel.h"

@interface JourneyCouponTableViewCell : UITableViewCell

@property (nonatomic, strong)CouponModel *model;

@end
