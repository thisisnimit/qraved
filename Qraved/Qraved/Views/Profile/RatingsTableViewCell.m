//
//  RatingsTableViewCell.m
//  Qraved
//
//  Created by Gary.yao on 2017/6/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "RatingsTableViewCell.h"
#import "FUProgressView.h"
#import "DLStarRatingControl.h"

@implementation RatingsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUIWithModel:(IMGSummary *)summary{
    
    UILabel * titleLabel = [UILabel new];
    if (summary.ratings.count >0 ) {
        titleLabel.text = @"Ratings";
    }
    titleLabel.textColor = [UIColor color333333];
    titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [self.contentView addSubview:titleLabel];
    
    titleLabel.sd_layout
    .topSpaceToView(self.contentView, 5)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(17);
    
    UIView * currentView = titleLabel;
    
     NSArray *colorArray = @[[UIColor colorWithRed:242/255.0f green:189/255.0f blue:121/255.0f alpha:1.0f], [UIColor colorWithRed:254/255.0f green:192/255.0f blue:17/255.0f alpha:1.0f], [UIColor colorWithRed:255/255.0f green:146/255.0f blue:46/255.0f alpha:1.0f], [UIColor colorWithRed:241/255.0f green:92/255.0f blue:79/255.0f alpha:1.0f],[UIColor colorWithRed:226/255.0f green:51/255.0f blue:53/255.0f alpha:1.0f]];
    if (summary!=nil) {
        for (int i = 5; i>0; i--) {
            NSDictionary * dict = summary.ratings[i-1];
            NSNumber * count = dict[@"count"];
            UILabel * label = [UILabel new];
            label.text = [NSString stringWithFormat:@"%d",i];
            label.font = [UIFont systemFontOfSize:14];
            [self.contentView addSubview:label];
            
            label.sd_layout
            .topSpaceToView(currentView, 5+(i==5?13:0))
            .leftSpaceToView(self.contentView,15)
            .widthIs(9)
            .heightIs(12);
            
            UIImageView * starImage = [UIImageView new];
            starImage.image = [UIImage imageNamed:@"ic_star_empty"];
            [self.contentView addSubview:starImage];
            
            starImage.sd_layout
            .topSpaceToView(currentView, 5+(i==5?13:0))
            .leftSpaceToView(label,3)
            .widthIs(12)
            .heightIs(12);
            FUProgressView * progress = [[FUProgressView alloc] initWithFrame:CGRectMake(44, 40+(5-i)*17, 160, 12)];
            progress.contentView.backgroundColor = colorArray[i - 1];
            if ([summary.ratingCount intValue] >0) {
                progress.percent =  [count doubleValue]/[summary.ratingCount doubleValue] *100;
            }

            UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:progress.bounds byRoundingCorners:UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii:CGSizeMake(6, 6)];
            CAShapeLayer *layer = [[CAShapeLayer alloc]init];
            layer.frame = progress.bounds;
            layer.path = maskPath.CGPath;
            
            progress.layer.mask = layer;
            
            [self.contentView addSubview:progress];
            
            
    //        progress.sd_layout
    //        .topSpaceToView(currentView, 5+(i==5?13:0))
    //        .leftSpaceToView(starImage,5)
    //        .widthIs(160)
    //        .heightIs(12);
    //        [self.contentView layoutIfNeeded];
            
            currentView = label;

        }
        
        
        UILabel * ratingLabel = [UILabel new];
        if ([summary.ratingCount intValue]>0) {
            ratingLabel.text = [NSString stringWithFormat:@"%.1f",([summary.ratingSum doubleValue]/[summary.ratingCount doubleValue])/2];
        }else{
            ratingLabel.text = @"0";
        }
        
        ratingLabel.font = [UIFont systemFontOfSize:50];
        ratingLabel.textColor = [UIColor redColor];
        ratingLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:ratingLabel];
        
        ratingLabel.sd_layout
        .topSpaceToView(titleLabel, 21)
        .rightSpaceToView(self.contentView, 15)
        .widthIs(100)
        .heightIs(50);
        
        DLStarRatingControl * starR = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(DeviceWidth-18-68, 96, 68, 12) andStars:5 andStarWidth:12 isFractional:YES spaceWidth:2];
        [starR setHighlightedStar:[UIImage imageNamed:@"starImage"]];
        [starR updateRating:[NSNumber numberWithDouble: [summary.ratingSum doubleValue]/[summary.ratingCount doubleValue]]];
        [self.contentView addSubview:starR];
        starR.sd_layout
        .centerXEqualToView(ratingLabel)
        .widthIs(68)
        .heightIs(12);
        
        UILabel * reviewLabel = [UILabel new];
        reviewLabel.text = [NSString stringWithFormat:@"%@ reviews",summary.ratingCount];
        reviewLabel.font = [UIFont systemFontOfSize:12];
        reviewLabel.textAlignment = NSTextAlignmentCenter;
        reviewLabel.textColor = [UIColor color999999];
        [self.contentView addSubview:reviewLabel];
        
        reviewLabel.sd_layout
        .bottomSpaceToView(self.contentView, 1)
//        .rightSpaceToView(self.contentView, 15)
        .centerXEqualToView(ratingLabel)
        .heightIs(15);
        [reviewLabel setSingleLineAutoResizeWithMaxWidth:150];
    }
}

@end
