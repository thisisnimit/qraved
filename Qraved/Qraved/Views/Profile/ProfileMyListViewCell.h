//
//  ProfileMyListViewCell.h
//  Qraved
//
//  Created by System Administrator on 10/19/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "ProfileDelegate.h"
#import "IMGMyList.h"

@interface ProfileMyListViewCell : UICollectionViewCell

@property(nonatomic,strong) IMGMyList *mylist;
@property(nonatomic,weak) NSObject<ProfileDelegate> *delegate;
@property(nonatomic,assign) BOOL isOtherUser;
@end
