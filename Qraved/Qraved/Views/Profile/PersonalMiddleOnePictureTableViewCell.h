//
//  PersonalMiddleOnePictureTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/6/23.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLStarRatingControl.h"
#import "LikeView.h"
#import "IMGJourneyReview.h"

@protocol HomeReviewTableViewCellDelegate <NSObject>
@optional

- (void)homeCardTableViewCell:(UITableViewCell*)cell likeButtonTapped:(UIButton*)button entity:(id)entity;

//share btn
- (void)homeCardTableViewCell:(UITableViewCell*)cell shareButtonTapped:(UIButton*)button entity:(id)entity;

//read more
- (void)HomeReviewTableCell:(UITableViewCell *)tableViewCell readMoreTapped:(id)review indexPath:(NSIndexPath*)path;


- (void)OnePictureIconImageViewTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path review:(IMGJourneyReview *)model;

//write review
- (void)OnePictureEditBtn:(IMGRestaurant *)restaurant andReviewId:(NSNumber*)reviewId indexPath:(NSIndexPath *)indexPath;


//comment
- (void)goToCardDetailPageWithType:(int)type andData:(NSDictionary *)dataDic andCellIndexPath:(NSIndexPath*)cellIndexPath;
@end


@interface PersonalMiddleOnePictureTableViewCell : UITableViewCell<UIWebViewDelegate>
{
    BOOL readMore;;
}
@property (nonatomic, strong)  UIButton  *iconImageView;
@property (nonatomic, strong)  UILabel  *lblUserName;
@property (nonatomic, strong)  UILabel  *lblTittle;

@property (nonatomic, strong)  UIButton  *btnEdit;
@property (nonatomic, strong)  DLStarRatingControl * starR;
@property (nonatomic, strong)  UILabel  *lbldate;
@property (nonatomic, strong)  UILabel  *lblDetailTitle;
@property (nonatomic, strong)  UIWebView  *lblDetail;
@property (nonatomic, strong)  LikeView* likepopView;

@property (nonatomic, strong)  NSMutableArray* likelistArr;

//@property (nonatomic, strong)  UIButton  *btnLikes;
//@property (nonatomic, strong)  UIButton  *btnComments;
//@property (nonatomic, strong)  UIButton  *btnShare;

@property (nonatomic, strong)  IMGJourneyReview  *model;

//@property (nonatomic, strong)  UIView *bottomView ;


@property (nonatomic, strong)  UIImageView  *detailImageView;


@property (nonatomic, strong)  UIImageView  *detailImageOne;


@property (nonatomic, strong)  UIImageView  *detailImageTwo;

@property (nonatomic, strong)  UIImageView  *detailImageThree;


@property (nonatomic, strong)  UIImageView  *detailImageFour;


@property (nonatomic, strong)  UIButton *lblMorePicture;

//@property (nonatomic, strong) V2_LikeCommentShareView *likeCommentShareView;

@property (nonatomic, strong)  UIButton  *btnwhiteReview;
@property (nonatomic, strong)  UILabel  *lblGood;



@property (nonatomic, strong)  UIButton  *btnAddPicture;


@property (nonatomic,assign) CGFloat cellHeight;


@property (nonatomic,weak) id<HomeReviewTableViewCellDelegate>homeReviewDelegate;

@property(nonatomic,strong) NSIndexPath *fromCellIndexPath;
@property (nonatomic, strong)  V2_LikeCommentShareView *LikeCommentShareView;

@end
