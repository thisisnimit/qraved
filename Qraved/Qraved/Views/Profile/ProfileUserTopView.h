//
//  ProfileUserTopView.h
//  Qraved
//
//  Created by Olaf on 14-9-5.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void (^buttonClickBlock)(void);
@interface ProfileUserTopView : UIView
@property (nonatomic,strong) UIImageView * pointImageView;
@property (nonatomic,strong) UIImageView * photoImageView;
@property (nonatomic,strong) UIImage * photoImage;

@property (nonatomic,strong) UILabel * nameLabel;

@property (nonatomic,strong) UIImageView * pointBackImageView;

@property (nonatomic,assign) NSInteger points;

@property (nonatomic,copy) NSString * name;
@property (nonatomic,copy) NSString * descript;

@property (nonatomic,strong) buttonClickBlock pointsButtonClick;

@end
