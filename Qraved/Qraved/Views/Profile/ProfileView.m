//
//  ProfileView.m
//  Qraved
//
//  Created by System Administrator on 10/19/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "ProfileView.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "UIView+Helper.h"
#import "UIColor+Hex.h"
#import "IMGUser.h"
#import "IMGMyList.h"
#import "IMGReservation.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+Helper.h"
#import "UIConstants.h"
#import "ProfileUpcomingViewCell.h"
#import "ProfileMyListViewCell.h"
#import "Amplitude.h"
#import "AppDelegate.h"
#import "DLStarRatingControl.h"

#define PROFILE_HEIGHT 105

@implementation ProfileView{
    IMGUser *user;
    CGFloat currentY;
    UILabel *mylistTitle;
    BOOL loading;
    NSMutableArray *draftArry;
    NSMutableArray *uploadPhottDraftArry;
}

@synthesize hasData;

-(void)resetCurrentUser{
    user=[IMGUser currentUser];
}
-(instancetype)initWithFrame:(CGRect)frame andIsOtherUser:(BOOL)isOtherUser andOtherUser:(IMGUser *)otherUser{
    if(self=[super initWithFrame:frame]){
        if (isOtherUser) {
            draftArry = [NSMutableArray array];
            uploadPhottDraftArry = [NSMutableArray array];

        }else{
            draftArry = [[NSUserDefaults standardUserDefaults] objectForKey:@"reviewDrafts"];
            uploadPhottDraftArry = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"uploadPhotoDraft"]];
        }
        hasData=YES;

        self.backgroundColor=[UIColor whiteColor];
        self.isOtherUser = isOtherUser;
        if (self.isOtherUser)
        {
            user = otherUser;
        }
        else
        {
            user=[IMGUser currentUser];
        }
        UICollectionViewFlowLayout *flowLayout=[[UICollectionViewFlowLayout alloc] init];
        self.collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height + 44) collectionViewLayout:flowLayout];
        self.collectionView.backgroundColor=[UIColor whiteColor];
        self.collectionView.delegate=self;
        self.collectionView.dataSource=self;
        self.collectionView.scrollsToTop = NO;
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"profile_cell"];
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"draft_cell"];
        
        [self.collectionView registerClass:[ProfileUpcomingViewCell class] forCellWithReuseIdentifier:@"booking_cell"];
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"add_mylist_cell"];
        [self.collectionView registerClass:[ProfileMyListViewCell class] forCellWithReuseIdentifier:@"mylist_cell"];
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"title_cell"];
        [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"shadow_cell"];
        [self addSubview:self.collectionView];
        
        [self addObserver:self forKeyPath:@"mylists" options:NSKeyValueObservingOptionNew context:nil];
        [self addObserver:self forKeyPath:@"upcomingBooks" options:NSKeyValueObservingOptionNew context:nil];
    }
    return self;
}
-(void)addDraft:(UICollectionViewCell*)cell{
    UILabel *myDraftLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, LEFTLEFTSET, DeviceWidth-2*LEFTLEFTSET, 20)];
    myDraftLabel.text = @"My Draft";
    if (draftArry.count+uploadPhottDraftArry.count>0) {
        [cell addSubview:myDraftLabel];

    }
    if (draftArry.count+uploadPhottDraftArry.count>2) {
        UIButton *seeMoreDraftButton = [[UIButton alloc] initWithFrame:CGRectMake(DeviceWidth-135, 253, 120, 20)];
        [seeMoreDraftButton setTitle:@"See More Draft >" forState:UIControlStateNormal];
        [seeMoreDraftButton setTitleColor:[UIColor colorWithRed:150/255.0 green:150/255.0 blue:150/255.0 alpha:1] forState:UIControlStateNormal];
        seeMoreDraftButton.titleLabel.textAlignment = NSTextAlignmentRight;
        seeMoreDraftButton.titleLabel.font = [UIFont systemFontOfSize:15];
//        [seeMoreDraftButton.titleLabel setTextColor:];
        [seeMoreDraftButton addTarget:self action:@selector(clickedSeeMore) forControlEvents:UIControlEventTouchUpInside];
        cell.contentView.userInteractionEnabled = YES;
        [cell.contentView addSubview:seeMoreDraftButton];
    }
    for (int i = 0; i<draftArry.count+uploadPhottDraftArry.count; i++) {
        if (draftArry.count>i) {
            UIView *drftView = [[UIView alloc] initWithFrame:CGRectMake(0,myDraftLabel.endPointY+ 45+i*85, DeviceWidth, 90)];
            drftView.tag = 1000+i;
            drftView.userInteractionEnabled = YES;
            drftView.backgroundColor = [UIColor whiteColor];
            [cell addSubview:drftView];
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reviewDraftCellCliked:)];
            [drftView addGestureRecognizer:tap];
            NSString *restaurantImageUrl = [draftArry[i] objectForKey:@"restaurantImageUrl"];
            NSURL *imageUrl ;
            if ([restaurantImageUrl hasPrefix:@"http://"]||[restaurantImageUrl hasPrefix:@"https://"])
            {
                imageUrl =  [NSURL URLWithString:restaurantImageUrl];
            }
            else
            {
                imageUrl = [NSURL URLWithString:[restaurantImageUrl returnFullImageUrl]];
                
            }
            NSString *restaurantName = [draftArry[i] objectForKey:@"restaurantTitle"];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(LEFTLEFTSET,0, 80, 80)];
            [drftView addSubview:imageView];
            [imageView sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
            UILabel *restaurantNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(imageView.endPointX+LEFTLEFTSET, imageView.startPointY, DeviceWidth-imageView.endPointX-2*LEFTLEFTSET, 20)];
            restaurantNameLabel.text = restaurantName;
            [restaurantNameLabel setTextColor:[UIColor colorRed]];
            [drftView addSubview:restaurantNameLabel];
            UILabel *districLabel = [[UILabel alloc] initWithFrame:CGRectMake(restaurantNameLabel.startPointX, restaurantNameLabel.endPointY, restaurantNameLabel.frame.size.width-20, 20)];
            districLabel.font = [UIFont systemFontOfSize:15];
            districLabel.text = [draftArry[i] objectForKey:@"restaurantArea"];
            [drftView addSubview:districLabel];
            DLStarRatingControl *star = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(districLabel.startPointX, districLabel.endPointY, 80, 20) andStars:5 andStarWidth:15 isFractional:YES];
            int starCount = [[draftArry[i] objectForKey:@"restaurantRatingCount"] intValue]/2.0;
            [star setRating:starCount];
            star.userInteractionEnabled = NO;
            [drftView addSubview:star];
            UILabel *starLabel = [[UILabel alloc] initWithFrame:CGRectMake(star.endPointX-2, star.startPointY-3, 50, 20)];
            [starLabel setFont:[UIFont systemFontOfSize:12]];
            starLabel.text = [NSString stringWithFormat:@"(%d/5)",starCount];
            starLabel.textColor= [UIColor colorWithRed:253/255.0 green:181/255.0 blue:10/255.0 alpha:1];
            [drftView addSubview:starLabel];
            UILabel *reviewDraftLabel = [[UILabel alloc] initWithFrame:CGRectMake(restaurantNameLabel.startPointX, starLabel.endPointY, restaurantNameLabel.frame.size.width, 20)];
            reviewDraftLabel.font = [UIFont systemFontOfSize:15];
            reviewDraftLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"reviewContent%@",[draftArry[i] objectForKey:@"restaurantId"]]];
            [drftView addSubview:reviewDraftLabel];
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, imageView.endPointY+3, DeviceWidth, 1)];
            lineView.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:248/255.0 alpha:1];
            [drftView addSubview:lineView];
            UIImageView *arrowRight = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-20-LEFTLEFTSET, districLabel.startPointY+5, 20, 20)];
            arrowRight.image = [UIImage imageNamed:@"ArrowRight.png"];
            [drftView addSubview:arrowRight];
            if (i==1) {
                return;
            }

        }else{
            UIView *drftView = [[UIView alloc] initWithFrame:CGRectMake(0,myDraftLabel.endPointY+ 45+ i*85, DeviceWidth, 90)];
            drftView.tag = 1000+i-draftArry.count;
            drftView.userInteractionEnabled = YES;
            drftView.backgroundColor = [UIColor whiteColor];
            [cell addSubview:drftView];
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadPhotoDraft:)];
            [drftView addGestureRecognizer:tap];
            NSString *restaurantImageUrl = [uploadPhottDraftArry[i-draftArry.count] objectForKey:@"restaurantImageUrl"];
            NSURL *imageUrl ;
            if ([restaurantImageUrl hasPrefix:@"http://"]||[restaurantImageUrl hasPrefix:@"https://"])
            {
                imageUrl =  [NSURL URLWithString:restaurantImageUrl];
            }
            else
            {
                imageUrl = [NSURL URLWithString:[restaurantImageUrl returnFullImageUrl]];
                
            }
            NSString *restaurantName = [uploadPhottDraftArry[i-draftArry.count]  objectForKey:@"restaurantTitle"];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(LEFTLEFTSET,0, 80, 80)];
            [drftView addSubview:imageView];
            [imageView sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
            UILabel *restaurantNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(imageView.endPointX+LEFTLEFTSET, imageView.startPointY, DeviceWidth-imageView.endPointX-2*LEFTLEFTSET, 20)];
            restaurantNameLabel.text = restaurantName;
            [restaurantNameLabel setTextColor:[UIColor colorRed]];
            [drftView addSubview:restaurantNameLabel];
            UILabel *districLabel = [[UILabel alloc] initWithFrame:CGRectMake(restaurantNameLabel.startPointX, restaurantNameLabel.endPointY, restaurantNameLabel.frame.size.width-20, 20)];
            districLabel.font = [UIFont systemFontOfSize:15];
            districLabel.text = [uploadPhottDraftArry[i-draftArry.count]  objectForKey:@"restaurantArea"];
            [drftView addSubview:districLabel];
            DLStarRatingControl *star = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(districLabel.startPointX, districLabel.endPointY, 80, 20) andStars:5 andStarWidth:15 isFractional:YES];
            int starCount = [[uploadPhottDraftArry[i-draftArry.count]  objectForKey:@"restaurantRatingCount"] intValue]/2.0;
            [star setRating:starCount];
            star.userInteractionEnabled = NO;
            [drftView addSubview:star];
            UILabel *starLabel = [[UILabel alloc] initWithFrame:CGRectMake(star.endPointX-2, star.startPointY-3, 50, 20)];
            [starLabel setFont:[UIFont systemFontOfSize:12]];
            starLabel.text = [NSString stringWithFormat:@"(%d/5)",starCount];
            starLabel.textColor= [UIColor colorWithRed:253/255.0 green:181/255.0 blue:10/255.0 alpha:1];
            [drftView addSubview:starLabel];
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, imageView.endPointY+3, DeviceWidth, 1)];
            lineView.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:248/255.0 alpha:1];
            [drftView addSubview:lineView];
            UIImageView *arrowRight = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-20-LEFTLEFTSET, districLabel.startPointY+5, 20, 20)];
            arrowRight.image = [UIImage imageNamed:@"ArrowRight.png"];
            [drftView addSubview:arrowRight];
            if (i==1) {
                return;
            }

        }
        
    }
    
}
-(void)reviewDraftCellCliked:(id)sender{
    UITapGestureRecognizer *tap= (UITapGestureRecognizer*)sender;
    if (tap.view.tag==1000) {
        if (self.delegate&&[self.delegate respondsToSelector:@selector(reviewDraftClike:)]) {
            IMGRestaurant *restautant = [[IMGRestaurant alloc] init];
            restautant.restaurantId = [draftArry [0] objectForKey:@"restaurantId"];
            restautant.title = [draftArry[0] objectForKey:@"restaurantTitle"];
            restautant.districtName = [draftArry[0] objectForKey:@"restaurantArea"];
            [self.delegate reviewDraftClike:restautant];
        }

    }
    if (tap.view.tag==1001) {
        if (self.delegate&&[self.delegate respondsToSelector:@selector(reviewDraftClike:)]) {
            IMGRestaurant *restautant = [[IMGRestaurant alloc] init];
            restautant.restaurantId = [draftArry [1] objectForKey:@"restaurantId"];
            restautant.title = [draftArry[1] objectForKey:@"restaurantTitle"];
            restautant.districtName = [draftArry[1] objectForKey:@"restaurantArea"];
            [self.delegate reviewDraftClike:restautant];
        }
        
    }
}
-(void)uploadPhotoDraft:(id)sender{
    UITapGestureRecognizer *tap= (UITapGestureRecognizer*)sender;
    if (tap.view.tag==1000) {
        if (self.delegate&&[self.delegate respondsToSelector:@selector(uploadDraftCliked:)]) {
            IMGRestaurant *restautant = [[IMGRestaurant alloc] init];
            restautant.restaurantId = [uploadPhottDraftArry [0] objectForKey:@"restaurantId"];
            restautant.title = [uploadPhottDraftArry[0] objectForKey:@"restaurantTitle"];
            restautant.districtName = [uploadPhottDraftArry[0] objectForKey:@"restaurantArea"];
            [self.delegate uploadDraftCliked:restautant];
        }
        
    }
    if (tap.view.tag==1001) {
        if (self.delegate&&[self.delegate respondsToSelector:@selector(uploadDraftCliked:)]) {
            IMGRestaurant *restautant = [[IMGRestaurant alloc] init];
            restautant.restaurantId = [uploadPhottDraftArry[1] objectForKey:@"restaurantId"];
            restautant.title = [uploadPhottDraftArry[1] objectForKey:@"restaurantTitle"];
            restautant.districtName = [uploadPhottDraftArry[1] objectForKey:@"restaurantArea"];
            [self.delegate uploadDraftCliked:restautant];
        }
        
    }

}
-(void)clickedSeeMore{
    if (self.delegate&&[self.delegate respondsToSelector:@selector(seeMoreDraftClicked)]) {
        [self.delegate seeMoreDraftClicked];
    }
}

-(void)addProfile:(UICollectionViewCell*)cell{
    UIView *profileView=[[UIView alloc] initWithFrame:CGRectMake(0,0,DeviceWidth,90)];
    profileView.backgroundColor=[UIColor colorWithHexString:@"#2b2b2b"];
    _photoImage=[[UIImageView alloc] initWithFrame:CGRectMake(14,15,60,60)];
    [profileView addSubview:_photoImage];
    
    NSURL *avatar;
    user.avatar = [user.avatar stringByReplacingPercentEscapesUsingEncoding:
                   NSUTF8StringEncoding];
    if ([user.avatar isEqualToString:DEFAULTPROFILEIMAGEURL]) {
        avatar = [NSURL URLWithString:@""];
    }else if ([user.avatar hasPrefix:@"http"]){
        avatar = [NSURL URLWithString:user.avatar];
    }else{
        avatar = [NSURL URLWithString:[user.avatar returnFullImageUrl]];
    }
    NSString *path_sandox = NSHomeDirectory();
    //设置一个图片的存储路径
    NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/avastar.png"];
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    if (image&&[user.userId intValue] == [[IMGUser currentUser].userId intValue]) {
        self.avatarImage =image;
        _photoImage.image = image;
    }else{

        [_photoImage sd_setImageWithURL:avatar placeholderImage:[UIImage imageNamed:@"headDefault.jpg"]];
        self.avatarImage =_photoImage.image;
        
    }
    
    _photoImage.layer.masksToBounds=YES;
    _photoImage.layer.borderWidth = 1.5;
    _photoImage.layer.borderColor = [UIColor whiteColor].CGColor;
    _photoImage.layer.cornerRadius = 30.4;
    _photoImage.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoImageCliked)];
    [_photoImage addGestureRecognizer:tap];
    [cell addSubview:profileView];
    
    
    self.name=[[UILabel alloc] initWithFrame:CGRectMake(_photoImage.endPointX+13,(90-30)/2,DeviceWidth-_photoImage.endPointX-13,30)];
    user.userName = [user.userName filterHtml];
    if (self.isOtherUser)
    {
        self.name.text=[NSString stringWithFormat:@"%@",user.userName];
    }
    else
    {
        self.name.text=[NSString stringWithFormat:@"%@ %@",[user.firstName filterHtml],[user.lastName filterHtml]];
    }
    self.name.textColor=[UIColor whiteColor];
    if (!_isOtherUser) {
        UIImageView *editImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"myprofile_edit_icon.png"]];
        editImage.frame = CGRectMake(DeviceWidth-LEFTLEFTSET-20, cell.bounds.size.height/2-10, 20, 20);
        editImage.userInteractionEnabled=YES;
        editImage.contentMode = UIViewContentModeScaleAspectFit;
        [cell addSubview:editImage];
        UIControl *control = [[UIControl alloc] initWithFrame:CGRectMake(DeviceWidth-LEFTLEFTSET-30, cell.bounds.size.height/2-15, 30, 30)];
        [control addTarget:self action:@selector(editImageClicked) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:control];
        
    }
    [cell addSubview:self.name];
}
-(void)editImageClicked{
    [self.delegate editClicked];
    
}
-(void)photoImageCliked{
    [self.delegate avatarClicked];
    
}
-(void)addRefreshView{
    self.refreshView=[[EGORefreshTableFooterView alloc] initWithFrame:CGRectMake(0,self.collectionView.contentSize.height+10,self.collectionView.frame.size.width,66)];
    self.refreshView.delegate=self;
    self.refreshView.backgroundColor=[UIColor whiteColor];
    [self.collectionView addSubview:self.refreshView];
}

-(void)resetRefreshViewFrame{
    if(self.collectionView.contentSize.height>self.collectionView.frame.size.height){
        self.refreshView.frame=CGRectMake(0,self.collectionView.contentSize.height+10,self.collectionView.frame.size.width,66);
    }else{
        self.refreshView.frame=CGRectMake(0,DeviceHeight+10,self.collectionView.frame.size.width,66);
    }
}

-(NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section{
    if (section==1) {
        return 1;
    }else if(section==2){
        return self.upcomingBooks.count?1:0;
    }else if(section==3){
        return self.upcomingBooks.count;
    }else if(section==4){
        return self.upcomingBooks.count?1:0;
    }else if(section==6){
        if (self.isOtherUser)
        {
            return self.mylists.count;
        }
        else
        {
            return self.mylists.count+1;
        }
    }
    return 1;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView*)collectionView{
    return 7;
}

-(UICollectionViewCell*)collectionView:(UICollectionView*)collectionView cellForItemAtIndexPath:(NSIndexPath*)indexpath{
    if(indexpath.section==0){
        UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"profile_cell" forIndexPath:indexpath];
        [self addProfile:cell];
        return cell;
    }else if(indexpath.section==1){
        UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"draft_cell" forIndexPath:indexpath];
        [self addDraft:cell];
        return cell;
        
    }else if(indexpath.section==2){
        UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"title_cell" forIndexPath:indexpath];
        UILabel *title=[[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET,10,DeviceWidth,40)];
        title.text=L(@"Upcoming Bookings");
        for(UIView *view in [cell.contentView subviews]){
            [view removeFromSuperview];
        }
        [cell.contentView addSubview:title];
        return cell;
    }else if(indexpath.section==3){
        ProfileUpcomingViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"booking_cell" forIndexPath:indexpath];
        cell.upcomingBook=[self.upcomingBooks objectAtIndex:indexpath.row];
        return cell;
    }else if(indexpath.section==4){
        UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"shadow_cell" forIndexPath:indexpath];
        UIImageView *lineImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 10)];
        lineImage.image = [UIImage imageNamed:@"CutOffRule"];
        [cell.contentView addSubview:lineImage];
        return cell;
    }else if(indexpath.section==5){
        UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"title_cell" forIndexPath:indexpath];
        UILabel *title=[[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET,20,DeviceWidth,40)];
        title.text=L(@"My List");
        for(UIView *view in [cell.contentView subviews]){
            [view removeFromSuperview];
        }
        [cell.contentView addSubview:title];
        return cell;
    }else if(indexpath.section==6){
        if(indexpath.row==0 && !self.isOtherUser){
            UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"add_mylist_cell" forIndexPath:indexpath];
            CGFloat newListHeight=18;
            CGFloat margin=10;
            UIImage *plus=[UIImage imageNamed:@"add_profile"];
            UIImageView *imageView=[[UIImageView alloc] initWithImage:plus];
            imageView.frame=CGRectMake((cell.frame.size.width-plus.size.width)/2,(cell.frame.size.height-plus.size.height-margin-newListHeight)/2,plus.size.width,plus.size.height);
            cell.layer.borderWidth=1;
            cell.layer.borderColor=[UIColor colorCCCCCC].CGColor;
            [cell addSubview:imageView];
            UILabel *newList=[[UILabel alloc] init];
            newList.text=@"New List";
            newList.font=[UIFont systemFontOfSize:14];
            newList.textColor=[UIColor colorCCCCCC];
            CGSize size = [newList.text sizeWithFont:newList.font constrainedToSize:CGSizeMake(newList.frame.size.width, 10000) lineBreakMode:NSLineBreakByWordWrapping];
            newList.frame=CGRectMake((cell.frame.size.width-size.width)/2,imageView.endPointY+margin,size.width,newListHeight);
            [cell addSubview:newList];
            return cell;
        }else{
            ProfileMyListViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"mylist_cell" forIndexPath:indexpath];
            cell.tag=indexpath.row;
            cell.isOtherUser = self.isOtherUser;
            cell.delegate=self.delegate;
            if (self.isOtherUser)
            {
                cell.mylist=[self.mylists objectAtIndex:indexpath.row];
            }
            else
            {
                cell.mylist=[self.mylists objectAtIndex:indexpath.row-1];
            }
            return cell;
        }
    }
    return nil;
}


-(CGSize)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewFlowLayout*)layout sizeForItemAtIndexPath:(NSIndexPath*)indexpath{
    if(indexpath.section==0){
        return CGSizeMake(DeviceWidth,90);
    }else if(indexpath.section==1){
        CGFloat height;
        if (_isOtherUser) {
            height=0.1;
        }else{
            if (draftArry.count+uploadPhottDraftArry.count==0){
                height=0.1;
            }if (draftArry.count+uploadPhottDraftArry.count==1) {
                height=150;
            }if (draftArry.count+uploadPhottDraftArry.count>1) {
                height=240;
            }if (draftArry.count+uploadPhottDraftArry.count>2) {
                height = 270;
            }
        }
        return CGSizeMake(DeviceWidth,height);
    }else if(indexpath.section==2){
        return CGSizeMake(DeviceWidth,50);
    }else if(indexpath.section==3){
        UIFont *font=[UIFont systemFontOfSize:20];
        // IMGReservation *reservation=
        CGSize size=[[[self.upcomingBooks objectAtIndex:indexpath.row] restaurantName] sizeWithFont:font constrainedToSize:CGSizeMake(DeviceWidth-LEFTLEFTSET*2-80,1000) lineBreakMode:NSLineBreakByWordWrapping];
        return CGSizeMake(DeviceWidth-LEFTLEFTSET*2,110+size.height/font.lineHeight);
    }else if(indexpath.section==4){
        return CGSizeMake(DeviceWidth,10);
    }else if(indexpath.section==5){
        return CGSizeMake(DeviceWidth,80);
    }else{
        return CGSizeMake((DeviceWidth-6*4)/2, (DeviceWidth-6*4)/2);
    }
}

-(UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewFlowLayout*)layout insetForSectionAtIndex:(NSInteger)section{
    if(section==3){
        return UIEdgeInsetsMake(10, LEFTLEFTSET, 10, LEFTLEFTSET);
    }else if(section==6){
        return UIEdgeInsetsMake(10, 6, 10,  6);
    }
    return UIEdgeInsetsZero;
}

-(void)collectionView:(UICollectionView*)collectionView didSelectItemAtIndexPath:(NSIndexPath*)indexpath{

    if(indexpath.section==3){
        if(self.delegate && [self.delegate respondsToSelector:@selector(upcomingClick:)]){
            [self.delegate upcomingClick:indexpath.row];
        }
    }else if(indexpath.section==6){
        if(self.delegate && [self.delegate respondsToSelector:@selector(mylistClick:)]){
            [self.delegate mylistClick:indexpath.row];
        }
    }
}

-(void)scrollViewDidScroll:(UICollectionView*)collectionView{
    if (collectionView.contentOffset.y <= 0) {
        collectionView.contentOffset = CGPointMake(0, 0);
        return;
    }
    [self.refreshView egoRefreshScrollViewDidScroll:collectionView];
    [self resetRefreshViewFrame];
    if (self.delegate && [self.delegate respondsToSelector:@selector(profileScrollNavigationController:)]) {
        [self.delegate profileScrollNavigationController:collectionView];
    }
    
}

-(void)scrollViewDidEndDragging:(UICollectionView*)collectionView willDecelerate:(BOOL)decelerate{
    if(collectionView.contentOffset.y>collectionView.contentSize.height-collectionView.frame.size.height+66){
        [self.refreshView egoRefreshScrollViewDidEndDragging:collectionView];
        [self resetRefreshViewFrame];
        return;
    }
}

- (void)egoRefreshTableFooterDidTriggerRefresh:(EGORefreshTableFooterView*)view{
    loading=YES;
    __weak EGORefreshTableFooterView *weakRefreshView=view;
    if(self.delegate && [self.delegate respondsToSelector:@selector(loadMyList:)] && hasData){
        [self.delegate loadMyList:^(BOOL _hasData){
            loading=NO;
            hasData=_hasData;
            [weakRefreshView egoRefreshScrollViewDataSourceDidFinishedLoading:self.collectionView];
        }];
    }
}

- (BOOL)egoRefreshTableFooterDataSourceIsLoading:(EGORefreshTableFooterView*)view{
    return loading;
}

-(void)observeValueForKeyPath:(NSString*)keypath ofObject:(id)objc change:(NSDictionary*)change context:(void*)context{
    if([keypath isEqualToString:@"mylists"]){
        // [self.collectionView reloadSections:[[NSIndexSet alloc] initWithIndex:5]];
    }else if([keypath isEqualToString:@"upcomingBooks"]){
        // if([[change objectForKey:@"new"] count]){
        // 	[self.collectionView reloadSections:[[NSIndexSet alloc] initWithIndex:1]];
        // 	[self.collectionView reloadSections:[[NSIndexSet alloc] initWithIndex:2]];			
        // }
        
    }
    [self.collectionView reloadData];
    [self resetRefreshViewFrame];
}


-(void)dealloc{
    [self removeObserver:self forKeyPath:@"mylists" context:nil];
    [self removeObserver:self forKeyPath:@"upcomingBooks" context:nil];
}
@end
