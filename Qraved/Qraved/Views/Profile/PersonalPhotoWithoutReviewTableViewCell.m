//
//  PersonalPhotoWithoutReviewTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/6/28.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "PersonalPhotoWithoutReviewTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ProfileJourneyPhotosView.h"
@implementation PersonalPhotoWithoutReviewTableViewCell
{
    ProfileJourneyPhotosView *photoView;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
      
       
        [self initUI];
    }
    return self;
}
- (void)initUI{
    
    _iconImageView = [UIButton buttonWithType: UIButtonTypeCustom];
    [_iconImageView addTarget:self action:@selector(iconImageViewClick:) forControlEvents:UIControlEventTouchUpInside];
    _iconImageView.frame = CGRectMake(15, 15, 40, 40);
    [self.contentView addSubview:_iconImageView];
    
    _lblUserName = [UILabel new];
    _lblUserName.text = @"Pepenero";
    _lblUserName.textColor = [UIColor color333333];
    _lblUserName.font = [UIFont systemFontOfSize:14];
    _lblUserName.frame = CGRectMake(_iconImageView.endPointX+15, 20, DeviceWidth-200, 20);
    [self.contentView addSubview:_lblUserName];
    
    _lblTittle = [UILabel new];
    _lblTittle.text = @"Western • Senopati";
    _lblTittle.textColor = [UIColor color999999];
    _lblTittle.font = [UIFont systemFontOfSize:12];
    _lblTittle.frame = CGRectMake(_lblUserName.startPointX, _lblUserName.endPointY, DeviceWidth-200, 20);
    [self.contentView addSubview:_lblTittle];
    
    _btnEdit = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnEdit setTitle:@"Edit" forState:UIControlStateNormal];
    [_btnEdit setTitleColor:[UIColor colortextBlue] forState:UIControlStateNormal];
    _btnEdit.titleLabel.font = [UIFont systemFontOfSize:12];
    [_btnEdit addTarget:self action:@selector(btnEditClick:) forControlEvents:UIControlEventTouchUpInside];
    _btnEdit.frame = CGRectMake(DeviceWidth-40, _iconImageView.centerY, 25, 20);
    [self.contentView addSubview:_btnEdit];
    
    _lbldate = [UILabel new];
    _lbldate.text = @"Jan 12, 2017";
    _lbldate.textColor = [UIColor color999999];
    _lbldate.font = [UIFont systemFontOfSize:12];
     _lbldate.frame = CGRectMake(DeviceWidth-215,_iconImageView.endPointY+10 , 200, 20);
    _lbldate.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_lbldate];
    
    
}



- (void)setModel:(IMGJourneyPhoto *)model{

    _model = model;
//    NSLog(@"%@",model.userAvatar);
    
    IMGUser *user = [IMGUser currentUser];
    //    NSLog(@"%@========%@",model.userId, user.userId);
    if (![model.userId isEqualToNumber:user.userId]) {
        _btnEdit.hidden = YES;
    }
    
    [_iconImageView sd_setImageWithURL:[NSURL URLWithString:[model.restaurantImageUrl returnFullImageUrl]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:nil];
    
    _lblUserName.text = model.restaurantTitle;
    _lblTittle.text = [NSString stringWithFormat:@"%@ • %@",model.restaurantCuisine, model.restaurantDistrict];
 
     _lbldate.text = model.uploadedPhotoTimeCreated ?[Date ConvertStrToTime:[NSString stringWithFormat:@"%@",model.uploadedPhotoTimeCreated]]:@"";
    if (photoView) {
        [photoView removeFromSuperview];
    }
    
    photoView = [[ProfileJourneyPhotosView alloc] initWithFrame:CGRectMake(0, _lbldate.endPointY+10, DeviceWidth, [ProfileJourneyPhotosView cardPhotoHeightWithPhotoCout:model.dishList.count])];
    photoView.photoArray = model.dishList;
    [self.contentView addSubview:photoView];
    
    _cellHeight = photoView.endPointY;
}
-(void)iconImageViewClick:(UIButton *)iconImageView{
    
    NSLog(@"touxinag");
    if (self.homeReviewDelegate) {
        [self.homeReviewDelegate PhotoWithReviewIconImageViewTableCell:self indexPath:self.fromCellIndexPath uploadPhoto:self.model];
    }
}

- (void)btnEditClick:(UIButton *)btnEdit{
   
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    restaurant.restaurantId = _model.restaurantId;
    restaurant.priceLevel = _model.restaurantPriceLevel;
    restaurant.cuisineName = _model.restaurantCuisine;
    restaurant.districtName = _model.restaurantDistrict;
    restaurant.cityName = _model.restaurantCityName;
    restaurant.title = _model.restaurantTitle;
    restaurant.seoKeyword = _model.restaurantSeoKeywords;
    restaurant.imageUrl = _model.userAvatar;
    
    NSMutableArray *_photosArrM = [[NSMutableArray alloc] init];
    for (NSDictionary *dish in _model.dishList) {
        IMGUploadPhoto *photo = [[IMGUploadPhoto alloc] init];
        photo.isOldPhoto = YES;
        photo.dishId = dish[@"id"];
        photo.imageUrl = dish[@"imageUrl"];
        photo.descriptionStr = dish[@"description"];
        [_photosArrM addObject:photo];
    }
    
    
    if (self.homeReviewDelegate && [self.homeReviewDelegate respondsToSelector:@selector(PhotoWithoutReviewEditBtn:andReviewId:indexPath:array:)]) {
        [self.homeReviewDelegate PhotoWithoutReviewEditBtn:restaurant andReviewId:_model.restaurantId indexPath:self.fromCellIndexPath array:_photosArrM];
    }
    
    
   
}
- (void)btnLikesClick:(UIButton *)btnLikes{
    
    NSLog(@"like");
}
- (void)btnCommentsClick:(UIButton *)btnComments{
    
    NSLog(@"comments");
    NSDictionary *dataDic = [[NSDictionary alloc] initWithObjectsAndKeys:_model,@"review", nil];
    
    [self.homeReviewDelegate goToCardDetailPageWithType:17 andData:dataDic andCellIndexPath:self.fromCellIndexPath];
    
}
- (void)btnShareClick:(UIButton *)btnShare{
    NSLog(@"share");
    if(self.homeReviewDelegate){
        [self.homeReviewDelegate homeCardTableViewCell:self shareButtonTapped:btnShare entity:_model];
    }
}
- (void)lblMorePictureClick:(UIButton *)lblMorePictureClick{

    NSLog(@"duozhangtupian");
}
@end
