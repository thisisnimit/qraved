//
//  MostTableViewCell.m
//  Qraved
//
//  Created by Gary.yao on 2017/6/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "MostTableViewCell.h"
#import "FUProgressView.h"
@implementation MostTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUIWithModel:(IMGSummary*)summary andIndex:(NSInteger)index{

    UILabel * titleLabel = [UILabel new];
    titleLabel.textColor = [UIColor color333333];
    titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [self.contentView addSubview:titleLabel];
    
    if (index == 3) {
        titleLabel.text = @"Most Reviewed Cuisines";
    }else{
        
        titleLabel.text = @"Most Reviewed Locations";
    
    }
    
    titleLabel.sd_layout
    .topSpaceToView(self.contentView, 40)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(17);
    
    UIView * curentView = titleLabel;
    NSMutableArray *Array = [NSMutableArray array];
    
    if (summary!=nil) {
        NSArray * array;
        if(index == 3) {
            array = summary.cuisineReviewList;
           
            for (NSDictionary *dict in array) {
                
                [Array addObject:dict[@"reviewCount"]];
            }
            
        }else{
            array = summary.districtReviewList;
            for (NSDictionary *dict in array) {
                
                [Array addObject:dict[@"reviewCount"]];
            }
        }
        
        for (int i = 0; i < array.count; i++) {
            
            NSDictionary * dict = array[i];
            NSNumber *number = dict[@"reviewCount"];
            UILabel * nameLabel = [UILabel new];
            nameLabel.text = dict[@"name"];
            nameLabel.textColor = [UIColor color999999];
            nameLabel.font = [UIFont boldSystemFontOfSize:14];
            [self.contentView addSubview:nameLabel];
            
            nameLabel.sd_layout
            .topSpaceToView(curentView, 5+(i==0?10:0))
            .leftSpaceToView(self.contentView, 15)
            .widthIs(120)
            .heightIs(12);
            
            FUProgressView * progress = [[FUProgressView alloc] initWithFrame:CGRectMake(DeviceWidth-160-15, 72+i*17, 160, 12)];
            [progress setBgColor:[UIColor colortextBlue]];
            
            double totleValue = 0.0;
            for (int i = 0; i < Array.count; i ++) {
                totleValue += [[Array objectAtIndex:i] doubleValue];
            }
            progress.percent =  ([number doubleValue]/totleValue)*100;
            UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:progress.bounds byRoundingCorners:UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii:CGSizeMake(6, 6)];
            CAShapeLayer *layer = [[CAShapeLayer alloc]init];
            layer.frame = progress.bounds;
            layer.path = maskPath.CGPath;
            
            progress.layer.mask = layer;
            
            [self.contentView addSubview:progress];
            
            UILabel * numLabel = [UILabel new];
            numLabel.text = [NSString stringWithFormat:@"%@",dict[@"reviewCount"]];
            numLabel.textColor = [UIColor color999999];
            numLabel.font = [UIFont systemFontOfSize:14];
            numLabel.textAlignment = NSTextAlignmentRight;
            [self.contentView addSubview:numLabel];
            
            numLabel.sd_layout
            .topSpaceToView(curentView, 5+(i==0?10:0))
            .rightSpaceToView(progress, 5)
            .widthIs(100)
            .heightIs(12);
            
            curentView = nameLabel;
        }

    }
    
    
}

@end
