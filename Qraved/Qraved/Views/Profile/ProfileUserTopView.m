//
//  ProfileUserTopView.m
//  Qraved
//
//  Created by Olaf on 14-9-5.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "ProfileUserTopView.h"
#import "HMSegmentedControl.h"
#import "AppDelegate.h"
#import "LoadingView.h"
#import "UIButton+LinkButton.h"
 
#import "UIViewController+Helper.h"
#import "NavigationBarButtonItem.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"
#import "UILabel+Helper.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "Label.h"
#import <CoreText/CoreText.h>
#import "BookingDetailContentController.h"
#import "DLStarRatingControl.h"
#import "NSNumber+Helper.h"
#import "PointRewardsViewController.h"

#import "UIImage+Helper.h"
#import "TYAttributedLabel.h"

#define AVATAR_WIDTH 60
#define AVATAR_HEIGHT 60
#define AVATAR_OFFSET_X 10
#define AVATAR_OFFSET_Y 10
#define CONTENT_OFFSET 5
#define BANNER_HEIGHT 80
#define NAV_HEIGHT 42
#define POINTS_LABEL_WIDTH 60
@interface ProfileUserTopView ()

@property (nonatomic,copy) NSString * pointsString;

@property (nonatomic,strong) TYAttributedLabel * pointsLabel;
@property (nonatomic,strong) UILabel * descriptionLabel;
@end

@implementation ProfileUserTopView


@synthesize photoImageView;
@synthesize photoImage;
@synthesize nameLabel=_nameLabel;
@synthesize pointBackImageView;

@synthesize pointImageView;

@synthesize points;
@synthesize descript=_descript;

-(void)setPhotoImage:(UIImage *)newPhotoImage{
    photoImageView.image=newPhotoImage;
}
-(void)setPoints:(NSInteger)newPoints{
    points=newPoints;
    if(points<=1){
        self.pointsString = [NSString stringWithFormat:@"%ld point",(long)points];
    }else{
        self.pointsString = [NSString stringWithFormat:@"%ld points",(long)points];
    }
}
-(void)setName:(NSString *)name{
    CGSize size = [name sizeWithFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18]];
    _nameLabel.frame = CGRectMake(DeviceWidth/2-size.width/2, photoImageView.endPointY+15, size.width, 20);
    _name = name;
    _nameLabel.text=name;
}
//-(void)setPointsString:(NSString *)pointsString{
//    _pointsString=pointsString;
//    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc]initWithString:pointsString];
//    
//    NSArray * attArr=[pointsString componentsSeparatedByString:@" "];
//    NSRange range1 = [pointsString rangeOfString:[attArr objectAtIndex:0]];
//    NSRange range2 = [pointsString rangeOfString:[attArr objectAtIndex:1]];
//    
//    UIFont * font2 =[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
//    UIFont * font1 =[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13];
//    [attStr addAttribute:NSFontAttributeName value:font1 range:range1];
//    [attStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorFFC000] range:range1];
//    [attStr addAttribute:NSFontAttributeName value:font2 range:range2];
//    [attStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:range2];
//    
//    
//
//    [_pointsLabel setAttributedString:attStr];
//    
//    CGSize expectSize = [_pointsString sizeWithFont:font1];
//    _pointsLabel.frame=CGRectMake(DeviceWidth/2-expectSize.width/2, pointBackImageView.endPointY+15, expectSize.width, expectSize.height);
//}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
       
        self.backgroundColor = [UIColor color434343];
        photoImageView = [[UIImageView alloc]initWithFrame:CGRectMake(124, 33, 72, 72)];
        photoImageView.image = photoImage;
        photoImageView.layer.masksToBounds = YES;
        photoImageView.layer.borderWidth = 2.5;
        photoImageView.layer.borderColor = [UIColor whiteColor].CGColor;
        photoImageView.layer.cornerRadius = 36;
        [self addSubview:photoImageView];
        
        CGSize size = [_name sizeWithFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18]];
        _nameLabel = [[Label alloc]initWithFrame:CGRectMake(DeviceWidth/2-size.width/2, photoImageView.endPointY+15, size.width, 20) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] andTextColor:[UIColor whiteColor] andTextLines:1];
        _nameLabel.text = _name;
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_nameLabel];
        
        pointBackImageView = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET, _nameLabel.endPointY+15, DeviceWidth-2*LEFTLEFTSET, 10)];
        pointBackImageView.image = [UIImage imageNamed:@"ProgressBar"];
        [self addSubview:pointBackImageView];
        
        pointImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, (DeviceWidth-2*LEFTLEFTSET)*(0.0/1000.0), 10)];
        pointImageView.backgroundColor = [UIColor colorFFC000];
        pointImageView.layer.masksToBounds = YES;
        pointImageView.layer.cornerRadius = 5;
        [pointBackImageView addSubview:pointImageView];
        
        _pointsLabel = [[TYAttributedLabel alloc]initWithFrame:CGRectZero];
        _pointsLabel.backgroundColor = [UIColor clearColor];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(buttonClick:)];
        [_pointsLabel addGestureRecognizer:tap];
        [self addSubview:_pointsLabel];
        
        _descriptionLabel = [[UILabel alloc]initWithFrame:CGRectZero];
        _descriptionLabel.backgroundColor=[UIColor clearColor];
        _descriptionLabel.alpha = 0.5;
        _descriptionLabel.textAlignment = NSTextAlignmentCenter;
        _descriptionLabel.font=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
        _descriptionLabel.textColor=[UIColor whiteColor];
        _descriptionLabel.textAlignment=NSTextAlignmentCenter;
        
        [_descriptionLabel setNumberOfLines:2];
        [_descriptionLabel setLineBreakMode:NSLineBreakByWordWrapping];
        
         [self addSubview:_descriptionLabel];
    
    }
    return self;
}
-(void)setDescript:(NSString *)descript{
    _descript=descript;
    
    CGSize desSize = [_descript sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] constrainedToSize:CGSizeMake(DeviceWidth-8*LEFTLEFTSET, 100)];
    _descriptionLabel.text=_descript;
    _descriptionLabel.frame=CGRectMake(60, 200, 200, desSize.height);
   
}
-(void)buttonClick:(UIGestureRecognizer *)gesture{
    if(_pointsButtonClick){
        _pointsButtonClick();
    }
}
@end
