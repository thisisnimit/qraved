//
//  JourneyCouponTableViewCell.m
//  Qraved
//
//  Created by harry on 2018/2/1.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "JourneyCouponTableViewCell.h"

@implementation JourneyCouponTableViewCell
{
    UILabel *lblTitle;
    UIImageView *couponImageView;
    UILabel *lblCouponTitle;
    UILabel *lblDes;
    UILabel *lblRemaining;
    UILabel *lblDate;
    UILabel *lblState;
    UIView *bottomView;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    lblTitle = [UILabel new];
    lblTitle.font = [UIFont boldSystemFontOfSize:14];
    lblTitle.textColor = [UIColor color333333];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    
    couponImageView = [UIImageView new];
    couponImageView.contentMode = UIViewContentModeScaleAspectFill;
    couponImageView.clipsToBounds = YES;
    
    lblCouponTitle = [UILabel new];
    lblCouponTitle.font = [UIFont boldSystemFontOfSize:14];
    lblCouponTitle.textColor = [UIColor color333333];
    
    lblDes = [UILabel new];
    lblDes.font = [UIFont boldSystemFontOfSize:14];
    lblDes.textColor = [UIColor color999999];
    
    lblRemaining = [UILabel new];
    lblRemaining.font = [UIFont systemFontOfSize:10];
    lblRemaining.textColor = [UIColor color333333];
    
    lblDate = [UILabel new];
    lblDate.font = [UIFont systemFontOfSize:10];
    lblDate.textColor = [UIColor color999999];
    
    lblState = [UILabel new];
    lblState.font = [UIFont systemFontOfSize:12];
    
    bottomView = [UIView new];
    bottomView.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    
    [self.contentView sd_addSubviews:@[lblTitle, lineView, couponImageView, lblCouponTitle, lblDes, lblRemaining, lblDate, lblState, bottomView]];
    
    lblTitle.sd_layout
    .topSpaceToView(self.contentView, 12)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(16);
    
    lineView.sd_layout
    .topSpaceToView(lblTitle, 12)
    .leftEqualToView(lblTitle)
    .rightEqualToView(lblTitle)
    .heightIs(1);
    
    couponImageView.sd_layout
    .topSpaceToView(lineView, 15)
    .leftEqualToView(lineView)
    .widthIs(104)
    .heightIs(104);
    
    lblCouponTitle.sd_layout
    .topEqualToView(couponImageView)
    .leftSpaceToView(couponImageView, 15)
    .rightEqualToView(lineView)
    .heightIs(16);
    
    lblDes.sd_layout
    .topSpaceToView(lblCouponTitle, 5)
    .leftEqualToView(lblCouponTitle)
    .rightEqualToView(lblCouponTitle)
    .autoHeightRatio(0);
    [lblDes setMaxNumberOfLinesToShow:2];
    
    lblRemaining.sd_layout
    .topSpaceToView(lblDes, 6)
    .leftEqualToView(lblDes)
    .rightEqualToView(lblDes)
    .heightIs(12);
    
    lblDate.sd_layout
    .topSpaceToView(lblRemaining, 5)
    .leftEqualToView(lblRemaining)
    .rightEqualToView(lblRemaining)
    .heightIs(12);
    
    lblState.sd_layout
    .topSpaceToView(lblDate, 12)
    .leftEqualToView(lblDate)
    .rightEqualToView(lblDate)
    .heightIs(14);
    
    bottomView.sd_layout
    .topSpaceToView(couponImageView, 33)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(1);
}

- (void)setModel:(CouponModel *)model{
    _model = model;
    
    if ([model.default_image_url hasPrefix:@"http"]) {
        [couponImageView sd_setImageWithURL:[NSURL URLWithString:model.default_image_url] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
    }else{
        [couponImageView sd_setImageWithURL:[NSURL URLWithString:[model.default_image_url returnFullImageUrl]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
    }
    
    lblCouponTitle.text = model.name;
    lblDes.text = model.couponDescription;
    if (![Tools isBlankString:model.remaining_time]) {
        lblRemaining.text = [NSString stringWithFormat:@"Remaining time: %@",model.remaining_time];
    }
    lblDate.text = [NSString stringWithFormat:@"%@ - %@",[Date dateFormatForCoupon:model.startDate],[Date dateFormatForCoupon:model.endDate]];
    
    if ([model.redeemed isEqual:@0]) {
        lblTitle.text = @"Saved Coupon";
        
        if (![Tools isBlankString:model.status_text]) {
            lblState.text = model.status_text;
            lblState.textColor = [UIColor colorWithHexString:@"D20000"];
        }else{
            if ([model.type isEqual:@1]) {
                lblState.text = @"Coupon is ready to be used";
                lblState.textColor = [UIColor colorWithHexString:@"09BFD3"];
            }else{
                lblState.text = @"";
            }
        }
        
    }else{
        lblTitle.text = @"Redeemed Coupon";
        lblState.textColor = [UIColor colorWithHexString:@"D20000"];
        lblState.text = @"You have already used this coupon";
    }
    [self setupAutoHeightWithBottomView:bottomView bottomMargin:0];
}

@end
