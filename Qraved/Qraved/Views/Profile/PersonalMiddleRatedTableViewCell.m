//
//  PersonalMiddleRatedTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/6/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "PersonalMiddleRatedTableViewCell.h"
#define LINK_TEXT @"<a style=\"text-decoration:none;color:#3A8CE2;font-family:'OpenSans'\" href=\"http://www.qraved.com/\">...read more</a>"

@implementation PersonalMiddleRatedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}
- (void)initUI{
    
    _lblRated = [UILabel new];
    _lblRated.text = @"Rated";
    _lblRated.textColor = [UIColor color333333];
    _lblRated.font = [UIFont systemFontOfSize:14];
    
    _lblDate = [UILabel new];
//    _lblDate.text = @"Jan 12, 2017";
    _lblDate.textColor = [UIColor color999999];
    _lblDate.font = [UIFont systemFontOfSize:12];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    
    _iconImageView = [UIButton buttonWithType:UIButtonTypeCustom];
    _iconImageView.backgroundColor = [UIColor whiteColor];
    [_iconImageView addTarget:self action:@selector(gotoRDP) forControlEvents:UIControlEventTouchUpInside];
    
    _lbluserName = [UILabel new];
//    _lbluserName.text = @"Pepenero";
    _lbluserName.textColor = [UIColor color333333];
    _lbluserName.font = [UIFont systemFontOfSize:14];
    _lbluserName.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoRDP)];
    [_lbluserName addGestureRecognizer:tap];
    
    _lblTitle = [UILabel new];
//    _lblTitle.text = @"Western • Senopati";
    _lblTitle.textColor = [UIColor color999999];
    _lblTitle.font = [UIFont systemFontOfSize:14];
    _lblTitle.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *titleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoRDP)];
    [_lblTitle addGestureRecognizer:titleTap];
    
    _btnwhiteReview = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnwhiteReview setImage:[UIImage imageNamed:@"whiteReviewbtn"] forState:UIControlStateNormal];
    [_btnwhiteReview addTarget:self action:@selector(didClickEditBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _starR = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(0, 0, 0, 0) andStars:5 andStarWidth:12 isFractional:YES spaceWidth:2];
//    _starR.centerY = _lbldate.centerY;
    [_starR setHighlightedStar:[UIImage imageNamed:@"starImage"]];
    [_starR updateRating:[NSNumber numberWithDouble: 4]];//[summary.ratingSum doubleValue]/[summary.ratingCount doubleValue]

    
    _lblGood = [UILabel new];
    _lblGood.text = @"Good";
    _lblGood.textColor = [UIColor color999999];
    _lblGood.font = [UIFont systemFontOfSize:14];
    
    [self.contentView sd_addSubviews:@[_lblRated, _lblDate, lineView, _iconImageView, _lbluserName, _lblTitle, _btnwhiteReview, _starR, _lblGood]];
    
    _lblRated.sd_layout
    .topSpaceToView(self.contentView, 10)
    .leftSpaceToView(self.contentView, 15)
    .heightIs(20);
    [_lblRated setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
    _lblDate.sd_layout
    .topEqualToView(_lblRated)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(20);
    [_lblDate setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
    lineView.sd_layout
    .topSpaceToView(_lblRated, 10)
    .leftSpaceToView(self.contentView, 15)
    .widthIs(DeviceWidth - 30)
    .heightIs(1);
    
    _iconImageView.sd_layout
    .topSpaceToView(lineView, 10)
    .leftEqualToView(_lblRated)
    .widthIs(40)
    .heightIs(40);
    
    _lbluserName.sd_layout
    .topSpaceToView(lineView, 15)
    .leftSpaceToView(_iconImageView, 5)
    .heightIs(20);
    [_lbluserName setSingleLineAutoResizeWithMaxWidth: DeviceWidth - 200];
    
    _lblTitle.sd_layout
    .topSpaceToView(_lbluserName, 0)
    .leftEqualToView(_lbluserName)
    .heightIs(20);
    [_lblTitle setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 200];
    
    _btnwhiteReview.sd_layout
    .centerYEqualToView(_iconImageView)
    .rightSpaceToView(self.contentView, 15)
    .widthIs(106)
    .heightIs(20);
    
    _starR.sd_layout
    .topSpaceToView(_iconImageView, 10)
    .leftSpaceToView(self.contentView , 15)
    .widthIs(73)
    .heightIs(13);
    
    _lblGood.sd_layout
    .centerYEqualToView(_starR)
    .leftSpaceToView(_starR, 5)
    .heightIs(20);
    [_lblGood setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
//   [self.contentView setupAutoHeightWithBottomView:_starR bottomMargin:10];

   
    
}

- (void)setModel:(IMGJourneyReview *)model{
    _model = model;
    
    IMGUser *user = [IMGUser currentUser];
    //    NSLog(@"%@========%@",model.userId, user.userId);
    if (![model.userId isEqualToNumber:user.userId]) {
        _btnwhiteReview.hidden = YES;
    }
    
     [_iconImageView sd_setImageWithURL:[NSURL URLWithString:[model.restaurantDefaultImageUrl returnFullImageUrl]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
    
//    _lblRated.text = @"Rate";
    _lblDate.text = [Date ConvertStrToTime:[NSString stringWithFormat:@"%@",model.reviewTimeCreated]] ?[Date ConvertStrToTime:[NSString stringWithFormat:@"%@",model.reviewTimeCreated]]:@"";
    _lbluserName.text = model.restaurantTitle ?model.restaurantTitle :@"";
    _lblTitle.text = [NSString stringWithFormat:@"%@ • %@",model.restaurantCuisine, model.restaurantDistrict] ? [NSString stringWithFormat:@"%@ • %@",model.restaurantCuisine, model.restaurantDistrict] :@"";
    [_starR updateRating:[NSNumber numberWithDouble:[model.reviewScore doubleValue]]];
//     _lblGood.text = @"Good";
   
}
- (void)gotoRDP{

    NSLog(@"touxiang ");
    if (self.Delegate) {
        [self.Delegate RatedIconImageViewTableCell:self indexPath:self.fromCellIndexPath review:self.model];
    }
    
}
- (void)didClickEditBtnClick:(UIButton *)didClickEditBtn{

    NSLog(@"write review");
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    restaurant.restaurantId = _model.restaurantId;
    restaurant.priceLevel = _model.restaurantPriceLevel;
    restaurant.cuisineName = _model.restaurantCuisine;
    restaurant.districtName = _model.restaurantDistrict;
    restaurant.cityName = _model.restaurantCityName;
    restaurant.title = _model.restaurantTitle;
    restaurant.seoKeyword = _model.restaurantSeoKeywords;
    restaurant.imageUrl = _model.restaurantDefaultImageUrl;
    
    if (self.Delegate && [self.Delegate respondsToSelector:@selector(didClickEditBtn:andReviewId:indexPath:)]) {
        [self.Delegate didClickEditBtn:restaurant andReviewId:_model.reviewId indexPath:self.fromCellIndexPath];
    }
    
    
}
@end
