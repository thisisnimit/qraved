//
//  ProfileJourneyPhotosView.m
//  Qraved
//
//  Created by harry on 2017/12/7.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "ProfileJourneyPhotosView.h"

@implementation ProfileJourneyPhotosView


- (void)setPhotoArray:(NSArray *)photoArray{
    _photoArray = photoArray;
    
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    switch (photoArray.count)
    {
        case 0:{

        }
            break;
        case 1:
        {
            
            NSDictionary *photoDic = [photoArray objectAtIndex:0];
            UIImageView *imageView = [UIImageView new];
            imageView.tag = 0;
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            imageView.clipsToBounds = YES;
//            UITapGestureRecognizer *imageViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClicked:)];
//            imageView.userInteractionEnabled = YES;
//            [imageView addGestureRecognizer:imageViewTapGesture];
        
            [self addSubview:imageView];
            
            
            __weak typeof(imageView) weakImageView = imageView;
            [imageView sd_setImageWithURL:[NSURL URLWithString:[[photoDic objectForKey:@"imageUrl"] returnFullImageUrlWithWidth:DeviceWidth andHeight:DeviceWidth*.75]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [UIView animateWithDuration:1.0 animations:^{
                    [weakImageView setAlpha:1.0];
                }];
            }];
            
            imageView.sd_layout
            .topSpaceToView(self, 0)
            .leftSpaceToView(self, 0)
            .rightSpaceToView(self, 0)
            .heightIs(DeviceWidth*.75);
            
            [self setupAutoHeightWithBottomView:imageView bottomMargin:0];
        }
            break;
        case 2:
        {
            UIView *bgView = [UIView new];
            [self addSubview:bgView];
            CGFloat imageSize = (DeviceWidth-4)/2;
            
            NSMutableArray *tempArr = [NSMutableArray array];
            
            for (int i = 0; i<photoArray.count; i++)
            {
                UIImageView *imageView = [UIImageView new];
                [bgView addSubview:imageView];
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                imageView.clipsToBounds = YES;
                imageView.tag = i;
//                UITapGestureRecognizer *imageViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClicked:)];
//                imageView.userInteractionEnabled = YES;
//                [imageView addGestureRecognizer:imageViewTapGesture];
                
                 NSDictionary *photoDic = [photoArray objectAtIndex:i];
                [self setImagUrl:[photoDic objectForKey:@"imageUrl"] imageWidth:imageSize imageHeight:imageSize imageView:imageView];
                
                imageView.sd_layout
                .heightIs(imageSize);
                
                [tempArr addObject:imageView];
            }
            
            [bgView setupAutoMarginFlowItems:tempArr withPerRowItemsCount:2 itemWidth:imageSize verticalMargin:0 verticalEdgeInset:0 horizontalEdgeInset:0];
            
            bgView.sd_layout
            .topSpaceToView(self, 0)
            .leftSpaceToView(self, 0)
            .rightSpaceToView(self, 0);
            
            [self setupAutoHeightWithBottomView:bgView bottomMargin:0];
        }
            break;
            
        case 3:
        {
            
            UIImageView *topImageView = [UIImageView new];
            topImageView.contentMode = UIViewContentModeScaleAspectFill;
            topImageView.clipsToBounds = YES;
            topImageView.tag = 0;
//            UITapGestureRecognizer *imageViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClicked:)];
//            topImageView.userInteractionEnabled = YES;
//            [topImageView addGestureRecognizer:imageViewTapGesture];
            [self addSubview:topImageView];
            NSDictionary *photoDic = [photoArray objectAtIndex:0];
            [self setImagUrl:[photoDic objectForKey:@"imageUrl"] imageWidth:DeviceWidth imageHeight:DeviceWidth/2 imageView:topImageView];
            topImageView.sd_layout
            .topSpaceToView(self, 0)
            .leftSpaceToView(self, 0)
            .rightSpaceToView(self, 0)
            .heightIs(DeviceWidth/2);
            
            UIView *bottomView = [UIView new];
            [self addSubview:bottomView];
            NSMutableArray *tempArr = [NSMutableArray array];
            for (int i = 1; i<photoArray.count; i++)
            {
                UIImageView *imageView = [UIImageView new];
                [bottomView addSubview:imageView];
                
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                imageView.clipsToBounds = YES;
                imageView.tag = i;
//                UITapGestureRecognizer *imageViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClicked:)];
//                imageView.userInteractionEnabled = YES;
//                [imageView addGestureRecognizer:imageViewTapGesture];
                
                
                NSDictionary *dic = [photoArray objectAtIndex:i];
                [self setImagUrl:[dic objectForKey:@"imageUrl"] imageWidth:(DeviceWidth-4)/2 imageHeight:(DeviceWidth-4)/2 imageView:imageView];
                
                imageView.sd_layout
                .heightIs((DeviceWidth-4)/2);
                
                [tempArr addObject:imageView];
            }
            
            [bottomView setupAutoMarginFlowItems:tempArr withPerRowItemsCount:2 itemWidth:(DeviceWidth-4)/2 verticalMargin:0 verticalEdgeInset:0 horizontalEdgeInset:0];
            
            bottomView.sd_layout
            .topSpaceToView(topImageView, 4)
            .leftEqualToView(topImageView)
            .rightEqualToView(topImageView);
            
            [self setupAutoHeightWithBottomView:bottomView bottomMargin:0];
            
            
        }
            break;
            
        case 4:
        {
            
            UIView *bgView = [UIView new];
            [self addSubview:bgView];
            CGFloat imageSize = (DeviceWidth-4)/2;
            
            NSMutableArray *tempArr = [NSMutableArray array];
            for (int i = 0; i<photoArray.count; i++)
            {
                UIImageView *imageView = [[UIImageView alloc] init];
                
                [bgView addSubview:imageView];
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                imageView.clipsToBounds = YES;
                imageView.tag = i;
//                UITapGestureRecognizer *imageViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClicked:)];
//                imageView.userInteractionEnabled = YES;
//                [imageView addGestureRecognizer:imageViewTapGesture];
                
                NSDictionary *dic = [photoArray objectAtIndex:i];
                
                [self setImagUrl:[dic objectForKey:@"imageUrl"] imageWidth:imageSize imageHeight:imageSize imageView:imageView];
                
                imageView.sd_layout
                .heightIs(imageSize);
                
                [tempArr addObject:imageView];
            }
            
            [bgView setupAutoMarginFlowItems:tempArr withPerRowItemsCount:2 itemWidth:imageSize verticalMargin:4 verticalEdgeInset:0 horizontalEdgeInset:0];
            
            bgView.sd_layout
            .topSpaceToView(self, 0)
            .leftSpaceToView(self, 0)
            .rightSpaceToView(self, 0);
            
            [self setupAutoHeightWithBottomView:bgView bottomMargin:0];
        }
            break;
            
        default:
        {
            
            UIView *topView = [UIView new];
            [self addSubview:topView];
            
            NSMutableArray *topArr = [NSMutableArray array];
            for (int i = 0; i < 2; i ++) {
                UIImageView *imageView = [UIImageView new];
                [topView addSubview:imageView];
                
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                imageView.clipsToBounds = YES;
                imageView.tag = i;
//                UITapGestureRecognizer *imageViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClicked:)];
//                imageView.userInteractionEnabled = YES;
//                [imageView addGestureRecognizer:imageViewTapGesture];
                
                NSDictionary *dic = [photoArray objectAtIndex:i];
                
                [self setImagUrl:[dic objectForKey:@"imageUrl"] imageWidth:(DeviceWidth-4)/2 imageHeight:(DeviceWidth-4)/2 imageView:imageView];
                
                imageView.sd_layout
                .heightIs((DeviceWidth-4)/2);
                
                [topArr addObject:imageView];
            }
            
            [topView setupAutoMarginFlowItems:topArr withPerRowItemsCount:2 itemWidth:(DeviceWidth-4)/2 verticalMargin:0 verticalEdgeInset:0 horizontalEdgeInset:0];
            
            topView.sd_layout
            .topSpaceToView(self, 0)
            .leftSpaceToView(self, 0)
            .rightSpaceToView(self, 0);
            
            UIView *bottomView = [UIView new];
            [self addSubview:bottomView];
            
            NSMutableArray *bottomArr = [NSMutableArray array];
            for (int i = 2; i < 5; i ++) {
                UIImageView *imageView = [UIImageView new];
                [bottomView addSubview:imageView];
                
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                imageView.clipsToBounds = YES;
                imageView.tag = i;
//                UITapGestureRecognizer *imageViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClicked:)];
//                imageView.userInteractionEnabled = YES;
//                [imageView addGestureRecognizer:imageViewTapGesture];
                
                NSDictionary *dic = [photoArray objectAtIndex:i];
                
                CGFloat imageSize = (DeviceWidth-8)/3;
                [self setImagUrl:[dic objectForKey:@"imageUrl"] imageWidth:imageSize imageHeight:imageSize imageView:imageView];
                
                
                if (i==4 && photoArray.count-5 > 0) {
                    UIView *fillView = [UIView new];
                    fillView.backgroundColor = [UIColor blackColor];
                    fillView.alpha = 0.4;
                    [imageView addSubview:fillView];
                    
                    UILabel *lblNum = [UILabel new];
                    lblNum.font = [UIFont systemFontOfSize:24];
                    lblNum.textColor = [UIColor whiteColor];
                    lblNum.text = [NSString stringWithFormat:@"+%ld",photoArray.count-5];
                    lblNum.textAlignment = NSTextAlignmentCenter;
                    [imageView addSubview:lblNum];
                    
                    fillView.sd_layout
                    .topSpaceToView(imageView, 0)
                    .leftSpaceToView(imageView, 0)
                    .rightSpaceToView(imageView, 0)
                    .bottomSpaceToView(imageView, 0);
                    
                    lblNum.sd_layout
                    .centerXEqualToView(imageView)
                    .centerYEqualToView(imageView)
                    .heightIs(28)
                    .widthIs(80);
                }
                
                imageView.sd_layout
                .heightIs(imageSize);
                
                [bottomArr addObject:imageView];
                
            }
            
            [bottomView setupAutoMarginFlowItems:bottomArr withPerRowItemsCount:3 itemWidth:(DeviceWidth-8)/3 verticalMargin:0 verticalEdgeInset:0 horizontalEdgeInset:0];
            bottomView.sd_layout
            .topSpaceToView(topView, 4)
            .leftEqualToView(topView)
            .rightEqualToView(topView);
            
            [self setupAutoHeightWithBottomView:bottomView bottomMargin:0];
            
            
        }
            break;
    }
    
}


- (void)setImagUrl:(NSString *)imageUrl imageWidth:(CGFloat)imageWidth imageHeight:(CGFloat)imageHeight imageView:(UIImageView *)imageView{
    __weak typeof(imageView) weakImageView = imageView;
    
    [imageView sd_setImageWithURL:[NSURL URLWithString:[imageUrl returnFullImageUrlWithWidth:imageWidth andHeight:imageHeight]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [UIView animateWithDuration:1.0 animations:^{
            [weakImageView setAlpha:1.0];
        }];
    }];
}

+(float)cardPhotoHeightWithPhotoCout:(NSInteger)count{
    switch (count) {
        case 0:
            return 0;
            break;
        case 1:
            return DeviceWidth*.75;
            break;
        case 2:
            return (DeviceWidth-4)/2;
            break;
        case 3:
            return (DeviceWidth-4)/2 + DeviceWidth/2 + 4;
            break;
        case 4:
            return DeviceWidth;
            break;
        default:
            return (DeviceWidth-8)/3+4+(DeviceWidth-4)/2;
            break;
    }
//    if (count>0) {
//        return (DeviceWidth - LEFTLEFTSET*2-2)/3.0*2;
//    }else{
//        return 0;
//    }
}

@end
