//
//  RestaurantListTableViewCell.h
//  Qraved
//
//  Created by Laura on 14-8-12.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RestaurantListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *restaurantTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *restaurantInfoLabel;
@property (weak, nonatomic) IBOutlet UIView *starImageView;
@property (weak, nonatomic) IBOutlet UILabel *reviewCountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *restaurantImage;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *diatanceImage;


@end
