//
//  PreviousBookingsTableViewCell.m
//  Qraved
//
//  Created by Laura on 14-9-10.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "PreviousBookingsTableViewCell.h"

#import <CoreText/CoreText.h>


#import "UIColor+Helper.h"
#import "UIImage+Helper.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "UIDevice+Util.h"
#import "UIImageView+Helper.h"
#import "BookUtil.h"


@implementation PreviousBookingsTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        _titleLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 0, DeviceWidth-2*LEFTLEFTSET, NAVIGATION_BAR_HEIGHT) andTextFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:20] andTextColor:[UIColor color222222] andTextLines:1];
        [self.contentView addSubview:_titleLabel];
        _titleLabel.lineBreakMode=NSLineBreakByTruncatingTail;
        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, _titleLabel.endPointY, DeviceWidth, 1)];
        lineImage.backgroundColor = [UIColor colorEDEDED];
        [self.contentView addSubview:lineImage];

        _personLabel = [[TYAttributedLabel alloc] initWithFrame:CGRectMake(0, _titleLabel.endPointY, (DeviceWidth/3), 39)];
        _personLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_personLabel];
        
        UIImageView *lineImage1 = [[UIImageView alloc]initWithFrame:CGRectMake((DeviceWidth/3), _titleLabel.endPointY, 1, 39)];
        lineImage1.backgroundColor = [UIColor colorEDEDED];
        [self.contentView addSubview:lineImage1];

        _dateLabel = [[TYAttributedLabel alloc] initWithFrame:CGRectMake((DeviceWidth/3)+1,_titleLabel.endPointY,(DeviceWidth/3),39)];
        _dateLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_dateLabel];

        UIImageView *lineImage2 = [[UIImageView alloc]initWithFrame:CGRectMake((DeviceWidth/3)*2, _titleLabel.endPointY, 1, 39)];
        lineImage2.backgroundColor = [UIColor colorEDEDED];
        [self.contentView addSubview:lineImage2];
        
           
        _timeLabel = [[Label alloc] initWithFrame:CGRectMake((DeviceWidth/3)*2+1, _titleLabel.endPointY, (DeviceWidth/3), 39) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13] andTextColor:[UIColor blackColor] andTextLines:1];
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_timeLabel];
        
        UIImageView *lineImage3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, _titleLabel.endPointY+39, DeviceWidth, 1)];
        lineImage3.backgroundColor = [UIColor colorEDEDED];
        [self.contentView addSubview:lineImage3];

        CGFloat currentY=_titleLabel.endPointY+40;

        _offerBox=[[UIView alloc] initWithFrame:CGRectMake(0,currentY,DeviceWidth,41)];

        self.discountLabel = [UIButton buttonWithType:UIButtonTypeCustom];
        self.discountLabel.frame = CGRectMake(LEFTLEFTSET, 0, DeviceWidth-2*LEFTLEFTSET, 40);
        self.discountLabel.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13];
        [self.discountLabel setTitleColor:[UIColor colorC2060A] forState:UIControlStateNormal];
        [_offerBox addSubview:self.discountLabel];

        UIImageView *lineImage_4 = [[UIImageView alloc]initWithFrame:CGRectMake(0, self.discountLabel.endPointY+1, DeviceWidth, 1)];
        lineImage_4.backgroundColor = [UIColor colorEDEDED];
        [_offerBox addSubview:lineImage_4];

        [self.contentView addSubview:_offerBox];

        currentY+=41;

        _addressLabel = [[Label alloc] initWithFrame:CGRectMake(LEFTLEFTSET, currentY+10, DeviceWidth-LEFTLEFTSET*2, 39) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13] andTextColor:[UIColor blackColor] andTextLines:1];
        _addressLabel.textAlignment = NSTextAlignmentCenter;
        _addressLabel.lineBreakMode=NSLineBreakByWordWrapping;
        _addressLabel.numberOfLines=0;
        [self.contentView addSubview:_addressLabel];

        currentY=_addressLabel.endPointY+10;

        _bottomBox=[[UIView alloc] initWithFrame:CGRectMake(0,currentY,DeviceWidth,100)];

        UIImageView *lineImage_5 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 10, DeviceWidth, 1)];
        lineImage_5.backgroundColor = [UIColor colorEDEDED];
        [_bottomBox addSubview:lineImage_5];

        _voucherLabel = [[Label alloc] initWithFrame:CGRectMake(0, 15, (DeviceWidth/3), 12) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:7] andTextColor:[UIColor blackColor] andTextLines:1];
        _voucherLabel.textAlignment = NSTextAlignmentCenter;
        _voucherLabel.textColor=[UIColor color999999];
        _voucherLabel.text=L(@"VOUCHER");
        [_bottomBox addSubview:_voucherLabel];
        _voucherValueLabel = [[Label alloc] initWithFrame:CGRectMake(0, 20, (DeviceWidth/3), 35) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:12] andTextColor:[UIColor blackColor] andTextLines:1];
        _voucherValueLabel.textAlignment = NSTextAlignmentCenter;
        [_bottomBox addSubview:_voucherValueLabel];
        
        _codeLabel = [[Label alloc] initWithFrame:CGRectMake((DeviceWidth/3), 15, (DeviceWidth/3), 12) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:7] andTextColor:[UIColor blackColor] andTextLines:1];
        _codeLabel.textAlignment = NSTextAlignmentCenter;
        _codeLabel.textColor=[UIColor color999999];
        _codeLabel.text=L(@"BOOK CODE");
        [_bottomBox addSubview:_codeLabel];
        _codeValueLabel = [[Label alloc] initWithFrame:CGRectMake((DeviceWidth/3), 20, (DeviceWidth/3), 35) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:12] andTextColor:[UIColor blackColor] andTextLines:1];
        _codeValueLabel.textAlignment = NSTextAlignmentCenter;
        [_bottomBox addSubview:_codeValueLabel];

        _statusLabel = [[Label alloc] initWithFrame:CGRectMake((DeviceWidth/3)*2, 15, (DeviceWidth/3), 12) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:7] andTextColor:[UIColor blackColor] andTextLines:1];
        _statusLabel.textAlignment = NSTextAlignmentCenter;
        _statusLabel.textColor=[UIColor color999999];
        _statusLabel.text=L(@"STATUS");
        [_bottomBox addSubview:_statusLabel];
        _statusValueLabel = [[Label alloc] initWithFrame:CGRectMake((DeviceWidth/3)*2, 20, (DeviceWidth/3), 35) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:12] andTextColor:[UIColor blackColor] andTextLines:1];
        _statusValueLabel.textAlignment = NSTextAlignmentCenter;
        [_bottomBox addSubview:_statusValueLabel];

        UIImageView *shadowImage = [[UIImageView alloc] initShadowImageViewWithShadowOriginY_v2:_statusValueLabel.endPointY andHeight:10];
        [_bottomBox addSubview:shadowImage];
        
        [self.contentView addSubview:_bottomBox];
    }
    return self;
}

-(void)initializeCellWithReservation:(IMGReservation *)reservation
{
    
    _titleLabel.text = reservation.restaurantName;

    NSString *bookDate = [BookUtil dateStringWithFormat:@"EEE, dd MMM" forDate:[BookUtil dateFromString:reservation.bookDate withFormat:@"yyyy-MM-dd"]];
    NSString *bookDate1 = [BookUtil dateStringWithFormat:@"dd MMM" forDate:[BookUtil dateFromString:reservation.bookDate withFormat:@"yyyy-MM-dd"]];
    NSMutableAttributedString *attStr;
    if (bookDate) {
        attStr = [[NSMutableAttributedString alloc]initWithString:bookDate];
    }
    _dateLabel.frame =CGRectMake(_personLabel.right ,_titleLabel.endPointY , DeviceWidth/3, 39);
    _dateLabel.text = bookDate;
    _dateLabel.textColor = [UIColor blackColor];
    _dateLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    
    NSMutableAttributedString *AttributedString = [[NSMutableAttributedString alloc] initWithString:bookDate];
    NSRange range = [bookDate rangeOfString:bookDate1];
    [AttributedString addAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13]} range:NSMakeRange(range.location, range.length)];
    _dateLabel.attributedText = AttributedString;
    _dateLabel.verticalAlignment = TYVerticalAlignmentCenter;
    _dateLabel.textAlignment = kCTTextAlignmentCenter;

    _timeLabel.text = reservation.bookTime;

    NSString *peopleCountString = [NSString stringWithFormat:L(@"%@ people"),reservation.party];
    _personLabel.frame = CGRectMake(0,_titleLabel.endPointY, DeviceWidth/3, 39);
    _personLabel.text = peopleCountString;
    _personLabel.textColor = [UIColor blackColor];
    _personLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    
    NSMutableAttributedString *AttributedString1 = [[NSMutableAttributedString alloc] initWithString:peopleCountString];
    NSRange range1 = [bookDate rangeOfString:[NSString stringWithFormat:@"%@",reservation.party]];
    [AttributedString1 addAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13]} range:NSMakeRange(range1.location, range1.length)];
    _personLabel.attributedText = AttributedString1;
    _personLabel.verticalAlignment = TYVerticalAlignmentCenter;
    _personLabel.textAlignment = kCTTextAlignmentCenter;
    

    NSString *statusString=@"Unknown";
    if([reservation.status isEqualToString: @"1"]){
        statusString= L(@"Pending");
    }else if([reservation.status isEqualToString: @"2"]||[reservation.status isEqualToString: @"11"]){
        statusString= L(@"Confirmed");
    }else if([reservation.status isEqualToString: @"4"]){
        statusString= L(@"Completed");
    }else if([reservation.status isEqualToString: @"5"]){
        statusString= L(@"No Show");
    }else{
        statusString= L(@"Cancelled");
    }

    CGSize addressSize= [reservation.address sizeWithFont:_addressLabel.font  constrainedToSize:CGSizeMake(DeviceWidth-LEFTLEFTSET*2, 10000) lineBreakMode:NSLineBreakByWordWrapping];
    _addressLabel.text=[reservation.address removeHTML];
    _voucherValueLabel.text=reservation.voucherCode;
    _codeValueLabel.text=reservation.code;
    _statusValueLabel.text=statusString;

    if(reservation.offerName!=nil && reservation.offerName.length){
        [self.discountLabel setTitle:[reservation.offerName removeHTML] forState:UIControlStateNormal];
        _addressLabel.frame=CGRectMake(LEFTLEFTSET,_offerBox.endPointY+10,DeviceWidth-LEFTLEFTSET*2,addressSize.height);
        _bottomBox.frame=CGRectMake(0,_addressLabel.endPointY,DeviceWidth,100);
        _offerBox.hidden=NO;
    }else{
        _offerBox.hidden=YES;
        _addressLabel.frame=CGRectMake(LEFTLEFTSET,_titleLabel.endPointY+50,DeviceWidth-LEFTLEFTSET*2,addressSize.height);
        _bottomBox.frame=CGRectMake(0,_addressLabel.endPointY,DeviceWidth,100);
    }
}


- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
