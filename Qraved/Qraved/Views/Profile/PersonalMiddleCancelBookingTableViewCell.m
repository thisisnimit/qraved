//
//  PersonalMiddleCancelBookingTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/6/29.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "PersonalMiddleCancelBookingTableViewCell.h"

@implementation PersonalMiddleCancelBookingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}
- (void)initUI{
    
    _lblUpcomingBooking = [UILabel new];
    
    _lblUpcomingBooking.textColor = [UIColor color999999];
    _lblUpcomingBooking.font = [UIFont systemFontOfSize:14];
    
    _lblDate = [UILabel new];
    _lblDate.textColor = [UIColor color999999];
    _lblDate.font = [UIFont systemFontOfSize:12];
    
//    UIView *lineView = [UIView new];
//    lineView.backgroundColor = [UIColor colorLineGray];
    
    _iconImageView = [UIButton buttonWithType:UIButtonTypeCustom];
    _iconImageView.backgroundColor = [UIColor whiteColor];
    [_iconImageView addTarget:self action:@selector(iconImageViewClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _lbluserName = [UILabel new];
    _lbluserName.textColor = [UIColor color333333];
    _lbluserName.font = [UIFont systemFontOfSize:14];
    
    _lblTitle = [UILabel new];
    _lblTitle.textColor = [UIColor color999999];
    _lblTitle.font = [UIFont systemFontOfSize:12];
    
//    _btnwhiteReview = [UIButton buttonWithType:UIButtonTypeCustom];
//    [_btnwhiteReview setTitle:@"Cancel" forState:UIControlStateNormal];
//    [_btnwhiteReview setTitleColor:[UIColor colortextBlue] forState:UIControlStateNormal];
//    _btnwhiteReview.titleLabel.font = [UIFont systemFontOfSize:14];
    
    _lblPax = [UILabel new];
    _lblPax.textColor = [UIColor color333333];
    _lblPax.font = [UIFont systemFontOfSize:14];
    
    _lbldetailDate = [UILabel new];
    //    _lbldetailDate.text = @"Friday, Jan 27 2017 05.00 AM";
    _lbldetailDate.textColor = [UIColor color333333];
    _lbldetailDate.font = [UIFont systemFontOfSize:14];
    
//    lineViewA = [UIView new];
//    lineViewA.backgroundColor = [UIColor colorLineGray];

    [self.contentView sd_addSubviews:@[_lblUpcomingBooking, _lblDate, _iconImageView, _lbluserName, _lblTitle, _lblPax, _lbldetailDate]];
    
    _lblDate.sd_layout
    .topSpaceToView(self.contentView, 60)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(14);
    [_lblDate setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
    
    
//    lineView.sd_layout
//    .topSpaceToView(_lblUpcomingBooking, 10)
//    .leftSpaceToView(self.contentView, 15)
//    .widthIs(DeviceWidth - 30)
//    .heightIs(0.5);
    
    _iconImageView.sd_layout
    .topSpaceToView(self.contentView, 15)
    .leftSpaceToView(self.contentView, 15)
    .widthIs(40)
    .heightIs(40);
    
    _lbluserName.sd_layout
    .topSpaceToView(self.contentView, 18)
    .leftSpaceToView(_iconImageView, 10)
    .heightIs(16);
    [_lbluserName setSingleLineAutoResizeWithMaxWidth: DeviceWidth - 100];
    
    _lblTitle.sd_layout
    .topSpaceToView(_lbluserName, 3)
    .leftEqualToView(_lbluserName)
    .heightIs(14);
    [_lblTitle setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 100];

    _lblPax.sd_layout
    .topSpaceToView(_iconImageView,30)
    .leftEqualToView(_iconImageView)
    .heightIs(16);
    [_lblPax setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 100];
    
    _lbldetailDate.sd_layout
    .topSpaceToView(_lblPax, 5)
    .leftEqualToView(_lblPax)
    .heightIs(16);
    [_lbldetailDate setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 100];
    
    
    _lblUpcomingBooking.sd_layout
    .topSpaceToView(_lblPax, 5)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(16);
    [_lblUpcomingBooking setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
//    lineViewA.sd_layout
//    .topSpaceToView(_lbldetailDate, 10)
//    .leftSpaceToView(self.contentView, 15)
//    .widthIs(DeviceWidth - 30)
//    .heightIs(0.5);
//
    
}
- (void)setModel:(IMGJourneyBook *)model{
    
    _model = model;
   
    [_iconImageView sd_setImageWithURL:[NSURL URLWithString:[model.restaurantImage returnFullImageUrl]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:nil];
    
    _lblUpcomingBooking.text = @"cancelled";
    _lblDate.text = model.bookDate ? [Date getTimeInteval:model.dateCreated]:@"";
    _lbluserName.text = model.restaurantTitle ?model.restaurantTitle :@"";
    _lblTitle.text = [NSString stringWithFormat:@"%@ • %@",model.restaurantCuisine, model.restaurantDistrict] ? [NSString stringWithFormat:@"%@ • %@",model.restaurantCuisine, model.restaurantDistrict] :@"";
    
    _lblPax.text = model.bookPax ? [NSString stringWithFormat:@"Pax for %@ people",model.bookPax] : @"";
    
    _lbldetailDate.text = model.bookTime ?[NSString stringWithFormat:@"%@ %@",[Date timeFormart:model.bookTime],[Date dateFormart:model.bookDate]]: @"";
    
//    [self.contentView setupAutoHeightWithBottomView:lineViewA bottomMargin:10];
}
- (void)iconImageViewClick:(UIButton *)iconImageViewClick{

    NSLog(@"touxiang");
    if (self.Delegate) {
        [self.Delegate CancelBookingViewMapTableCell:self indexPath:self.fromCellIndexPath book:self.model];
    }
}
@end
