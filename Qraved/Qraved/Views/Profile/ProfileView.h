//
//  ProfileView.h
//  Qraved
//
//  Created by System Administrator on 10/19/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EGORefreshTableFooterView.h"
#import "ProfileDelegate.h"
#import "IMGUser.h"

@interface ProfileView : UIView<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,EGORefreshTableFooterDelegate>

@property(nonatomic,strong) UICollectionView *collectionView;
@property(nonatomic,strong) NSMutableArray *mylists;
@property(nonatomic,strong) NSMutableArray *upcomingBooks;
@property(nonatomic,strong) EGORefreshTableFooterView *refreshView;
@property(nonatomic,weak) NSObject<ProfileDelegate> *delegate;
@property(nonatomic,assign) BOOL hasData;
@property(nonatomic,assign) BOOL isOtherUser;
@property (nonatomic,strong) UIImageView *photoImage;
@property(nonatomic,strong)UIImage *avatarImage;
@property(nonatomic,strong)UILabel *name;


-(instancetype)initWithFrame:(CGRect)frame andIsOtherUser:(BOOL)isOtherUser andOtherUser:(IMGUser *)otherUser;
//-(void)setUpComingBooks;
-(void)addRefreshView;
-(void)resetRefreshViewFrame;
-(void)resetCurrentUser;
@end
