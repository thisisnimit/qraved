//
//  BookingView.h
//  Qraved
//
//  Created by System Administrator on 10/21/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "EGORefreshTableFooterView.h"
#import "ProfileDelegate.h"

@interface BookingView : UIView<UITableViewDelegate,UITableViewDataSource,EGORefreshTableFooterDelegate>

@property(nonatomic,strong) NSMutableArray *upcomingBookings;
@property(nonatomic,strong) NSMutableArray *pastBookings;
@property(nonatomic,assign) BOOL hasHistory;
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) EGORefreshTableFooterView *refreshView;
@property(nonatomic,strong) NSObject<ProfileDelegate> *delegate;


-(void)setRefreshViewFrame;

@end
