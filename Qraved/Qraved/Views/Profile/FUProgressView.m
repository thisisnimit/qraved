//
//  FUProgressView.m
//  fundsUP
//
//  Created by DeliveLee on 2017/5/12.
//  Copyright © 2017年 mediaBunker. All rights reserved.
//

#import "FUProgressView.h"

@interface FUProgressView (){
    UIView *contentView;
}
@end

@implementation FUProgressView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}

-(void)initUI{

    self.backgroundColor = [UIColor colorWithHexString:@"#E5E6E8"];
    contentView = [UIView new];
    contentView.backgroundColor = [UIColor redColor];
    [self addSubview:contentView];
    contentView.sd_layout
    .leftEqualToView(self)
    .topEqualToView(self)
    .bottomEqualToView(self)
    .widthIs(0);
}

-(void)setPercent:(CGFloat)percent{
    
    contentView.sd_layout.widthIs(160 * (percent / 100));
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 160 * (percent / 100), 12) byRoundingCorners:UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii:CGSizeMake(6, 6)];
    CAShapeLayer *layer = [[CAShapeLayer alloc]init];
    layer.frame = CGRectMake(0, 0, 160 * (percent / 100), 12);
    layer.path = maskPath.CGPath;
    contentView.layer.mask = layer;
    [contentView updateLayout];

    _percent = percent;
    
}

- (void)setBgColor:(UIColor *)bgColor
{

    contentView.backgroundColor = bgColor;
    
}

@end
