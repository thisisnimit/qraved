//
//  AccountView.h
//  Qraved
//
//  Created by System Administrator on 10/19/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProfileDelegate.h"

@interface AccountView : UIView

@property(nonatomic,weak) NSObject<ProfileDelegate> *delegate;
@property(nonatomic,assign) BOOL isOtherUser;

-(instancetype)initWithFrame:(CGRect)frame withOtherUser:(BOOL)isOtherUser;
@end
