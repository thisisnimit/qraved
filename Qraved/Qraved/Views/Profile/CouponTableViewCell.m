//
//  CouponTableViewCell.m
//  Qraved
//
//  Created by harry on 2018/1/31.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "CouponTableViewCell.h"
#import "Date.h"
#import "ProfileCouponPageControl.h"
@interface CouponTableViewCell ()<UIScrollViewDelegate>{
    UIScrollView *couponScroll;
    UILabel *lblTitle;
    UIPageControl *pageControl;
    NSArray *couponArr;
}
@end

@implementation CouponTableViewCell

- (void)createUIWithModel:(NSArray*)couponArray{
    
    couponArr = couponArray;
    UILabel * titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 20, DeviceWidth-60, 16)];
    titleLabel.font = [UIFont boldSystemFontOfSize:14];
    titleLabel.textColor = [UIColor color333333];
    titleLabel.text = @"Saved Coupon";
    [self.contentView addSubview:titleLabel];
    
    couponScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(15, titleLabel.endPointY + 12, DeviceWidth-30, 144)];
    couponScroll.showsHorizontalScrollIndicator = NO;
    couponScroll.showsVerticalScrollIndicator = NO;
    couponScroll.delegate=self;
    couponScroll.autoresizingMask=UIViewAutoresizingNone;
    [couponScroll setPagingEnabled:YES];
    couponScroll.layer.borderWidth = 1;
    couponScroll.layer.borderColor = [UIColor colorWithHexString:@"#EAEAEA"].CGColor;
    [self.contentView addSubview:couponScroll];
    
    [couponScroll setContentSize:CGSizeMake((DeviceWidth-30) * couponArray.count, 0)];
    [self setScrollView:couponArray];
    
    pageControl=[[UIPageControl alloc] initWithFrame:CGRectMake(15,couponScroll.endPointY+10,DeviceWidth - 30,10)];
//    [pageControl setValue:[UIImage imageNamed:@"ic_current_dot"] forKeyPath:@"_currentPageImage"];
//    [pageControl setValue:[UIImage imageNamed:@"ic_normal_dot"] forKeyPath:@"_pageImage"];
    pageControl.pageIndicatorTintColor=[UIColor colorWithHexString:@"#DBDBDB"];
    pageControl.currentPageIndicatorTintColor=[UIColor colorWithHexString:@"#D20000"];

    [self.contentView addSubview:pageControl];
    if (couponArray.count>1) {
        pageControl.numberOfPages = couponArray.count;
        pageControl.currentPage = 0;
    }
}

- (void)setScrollView:(NSArray *)couponArr{
    
    pageControl.numberOfPages = couponArr.count;
    for (int i = 0; i < couponArr.count; i ++) {
        CouponModel *model = [couponArr objectAtIndex:i];
        
        UIView *couponView = [self createCouponView:model];
        couponView.tag = i;
        couponView.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(couponViewTapped:)];
        [couponView addGestureRecognizer:tap];
        
        couponView.frame = CGRectMake((DeviceWidth-30) * i, 0, DeviceWidth-30, 144);
        [couponScroll addSubview:couponView];
    }
}

- (UIView *)createCouponView:(CouponModel *)model{
    UIView *view = [UIView new];
    view.backgroundColor = [UIColor whiteColor];
    
    
    UIImageView *couponImageView = [UIImageView new];
    couponImageView.contentMode = UIViewContentModeScaleAspectFill;
    couponImageView.clipsToBounds = YES;
    if ([model.default_image_url hasPrefix:@"http"]) {
        [couponImageView sd_setImageWithURL:[NSURL URLWithString:model.default_image_url] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
    }else{
        [couponImageView sd_setImageWithURL:[NSURL URLWithString:[model.default_image_url returnFullImageUrl]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
    }
    
    UILabel *lblCouponTitle = [UILabel new];
    lblCouponTitle.font = [UIFont boldSystemFontOfSize:14];
    lblCouponTitle.textColor = [UIColor color333333];
    lblCouponTitle.text = model.name;
    
    UILabel *lblDes = [UILabel new];
    lblDes.font = [UIFont boldSystemFontOfSize:14];
    lblDes.textColor = [UIColor color999999];
    lblDes.text = model.couponDescription;
    
    UILabel *lblRemaining = [UILabel new];
    lblRemaining.font = [UIFont systemFontOfSize:10];
    lblRemaining.textColor = [UIColor color333333];
    if (![Tools isBlankString:model.remaining_time]) {
        lblRemaining.text = [NSString stringWithFormat:@"Remaining time: %@",model.remaining_time];
    }
    
    UILabel *lblDate = [UILabel new];
    lblDate.font = [UIFont systemFontOfSize:10];
    lblDate.textColor = [UIColor color999999];
    lblDate.text = [NSString stringWithFormat:@"%@ - %@",[Date dateFormatForCoupon:model.startDate],[Date dateFormatForCoupon:model.endDate]];
    
    UILabel *lblStatus = [UILabel new];
    lblStatus.font = [UIFont systemFontOfSize:12];
    
    if ([Date compareCurrentTimeWithTime:model.startDate] == 1) {
        lblStatus.text = @"Coupon has not yet started";
        lblStatus.textColor = [UIColor colorWithHexString:@"D20000"];
    }else{
        if ([model.type isEqual:@1]) {
            lblStatus.text = @"Coupon is ready to be used";
            lblStatus.textColor = [UIColor colorWithHexString:@"09BFD3"];
        }else{
            lblStatus.text = @"";
        }
    }
    [view sd_addSubviews:@[couponImageView, lblCouponTitle, lblDes, lblRemaining, lblDate ,lblStatus]];
    
    couponImageView.sd_layout
    .topSpaceToView(view, 15)
    .leftSpaceToView(view, 15)
    .widthIs(104)
    .heightIs(104);
    
    lblCouponTitle.sd_layout
    .topSpaceToView(view, 15)
    .leftSpaceToView(couponImageView, 15)
    .rightSpaceToView(view, 15)
    .heightIs(16);
    
    lblDes.sd_layout
    .topSpaceToView(lblCouponTitle, [Tools isBlankString:model.couponDescription]?0:5)
    .leftEqualToView(lblCouponTitle)
    .rightEqualToView(lblCouponTitle)
    .autoHeightRatio(0);
    
    [lblDes setMaxNumberOfLinesToShow:2];
    
    lblRemaining.sd_layout
    .topSpaceToView(lblDes, [Tools isBlankString:model.remaining_time]?0:6)
    .leftEqualToView(lblDes)
    .rightEqualToView(lblDes)
    .heightIs([Tools isBlankString:model.remaining_time]?0:12);
    
    lblDate.sd_layout
    .topSpaceToView(lblRemaining, 5)
    .leftEqualToView(lblRemaining)
    .rightEqualToView(lblRemaining)
    .heightIs(12);
    
    lblStatus.sd_layout
    .topSpaceToView(lblDate, 10)
    .leftEqualToView(lblDate)
    .rightEqualToView(lblDate)
    .heightIs(14);
    
    return view;
}

- (void)couponViewTapped:(UITapGestureRecognizer *)tap{
    CouponModel *model = [couponArr objectAtIndex:tap.view.tag];
    if (self.couponViewTapped) {
        self.couponViewTapped(model);
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    int index =  scrollView.contentOffset.x / (DeviceWidth-30);
    pageControl.currentPage = index;
}

@end
