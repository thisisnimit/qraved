
//
//  PersonalMiddleSaveTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/6/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "PersonalMiddleSaveTableViewCell.h"

@implementation PersonalMiddleSaveTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}
- (void)initUI{
    
    _lblSave = [UILabel new];
    _lblSave.text = @"Save";
    _lblSave.textColor = [UIColor color333333];
    _lblSave.font = [UIFont boldSystemFontOfSize:14];
    
    _lblDate = [UILabel new];
//    _lblDate.text = @"Jan 12, 2017";
    _lblDate.textColor = [UIColor color999999];
    _lblDate.font = [UIFont systemFontOfSize:12];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    
    _iconImageView = [UIButton buttonWithType:UIButtonTypeCustom];
    _iconImageView.backgroundColor = [UIColor whiteColor];
    [_iconImageView addTarget:self action:@selector(gotoRDP) forControlEvents:UIControlEventTouchUpInside];
    
    _lbluserName = [UILabel new];
//    _lbluserName.text = @"Pepenero";
    _lbluserName.textColor = [UIColor color333333];
    _lbluserName.font = [UIFont systemFontOfSize:14];
    _lbluserName.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoRDP)];
    [_lbluserName addGestureRecognizer:tap];
    
    _lblTitle = [UILabel new];
//    _lblTitle.text = @"Western • Senopati";
    _lblTitle.textColor = [UIColor color999999];
    _lblTitle.font = [UIFont systemFontOfSize:14];
    _lblTitle.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *titleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoRDP)];
    [_lblTitle addGestureRecognizer:titleTap];
    
    _btnwhiteReview = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnwhiteReview setImage:[UIImage imageNamed:@"whiteReviewbtn"] forState:UIControlStateNormal];
    [_btnwhiteReview addTarget:self action:@selector(btnwhiteReviewClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _lbldetail = [UILabel new];
    _lbldetail.text = @"Has been saved to want to go";
    _lbldetail.textColor = [UIColor color333333];
    _lbldetail.font = [UIFont systemFontOfSize:14];
    
    
    [self.contentView sd_addSubviews:@[_lblSave, _lblDate, lineView, _iconImageView, _lbluserName, _lblTitle, _btnwhiteReview, _lbldetail]];
    
    _lblSave.sd_layout
    .topSpaceToView(self.contentView, 10)
    .leftSpaceToView(self.contentView, 15)
    .heightIs(20);
    [_lblSave setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
    _lblDate.sd_layout
    .topEqualToView(_lblSave)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(20);
    [_lblDate setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
    lineView.sd_layout
    .topSpaceToView(_lblSave, 10)
    .leftSpaceToView(self.contentView, 15)
    .widthIs(DeviceWidth - 30)
    .heightIs(1);
    
    _iconImageView.sd_layout
    .topSpaceToView(lineView, 10)
    .leftEqualToView(_lblSave)
    .widthIs(40)
    .heightIs(40);
    
    _lbluserName.sd_layout
    .topSpaceToView(lineView, 15)
    .leftSpaceToView(_iconImageView, 5)
    .heightIs(20);
    [_lbluserName setSingleLineAutoResizeWithMaxWidth: DeviceWidth - 200];
    
    _lblTitle.sd_layout
    .topSpaceToView(_lbluserName, 5)
    .leftEqualToView(_lbluserName)
    .heightIs(20);
    [_lblTitle setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 200];
    
    _btnwhiteReview.sd_layout
    .centerYEqualToView(_iconImageView)
    .rightSpaceToView(self.contentView, 15)
    .widthIs(106)
    .heightIs(20);
    
    _lbldetail.sd_layout
    .topSpaceToView(_iconImageView,10)
    .leftEqualToView(_iconImageView)
    .heightIs(20);
    [_lbldetail setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
//    [self.contentView setupAutoHeightWithBottomView:_lbldetail bottomMargin:10];
    
    
}
- (void)setModel:(IMGJourneySave *)model{

    _model = model;
    IMGUser *user = [IMGUser currentUser];
//    NSLog(@"%@========%@",model.userId, user.userId);
    if (![model.userId isEqualToNumber:user.userId]) {
        _btnwhiteReview.hidden = YES;
    }
    [_iconImageView sd_setImageWithURL:[NSURL URLWithString:[model.restaurantDefaultImageUrl returnFullImageUrl]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:nil];
    
    _lblDate.text = model.savedTime ?[Date ConvertStrToTime:[NSString stringWithFormat:@"%@",model.savedTime]]:@"";
    _lbluserName.text = model.restaurantTitle ?model.restaurantTitle :@"";
    _lblTitle.text = [NSString stringWithFormat:@"%@ • %@",model.restaurantCuisine, model.restaurantDistrict] ? [NSString stringWithFormat:@"%@ • %@",model.restaurantCuisine, model.restaurantDistrict] :@"";
    _lbldetail.text = [NSString stringWithFormat:@"Has been saved to %@",model.listName];
    
    
    
}
- (void)btnwhiteReviewClick:(UIButton *)btnwhiteReview{

    NSLog(@"saved");
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    restaurant.restaurantId = _model.restaurantId;
    restaurant.priceLevel = _model.restaurantPriceLevel;
    restaurant.cuisineName = _model.restaurantCuisine;
    restaurant.districtName = _model.restaurantDistrict;
    restaurant.cityName = _model.restaurantCityName;
    restaurant.title = _model.restaurantTitle;
    restaurant.seoKeyword = _model.restaurantSeoKeywords;
//    restaurant.imageUrl = _model.restaurantDefaultImageUrl;
    
    if (self.Delegate && [self.Delegate respondsToSelector:@selector(didwriteClickEditBtn:andReviewId:indexPath:)]) {
        [self.Delegate didwriteClickEditBtn:restaurant andReviewId:_model.restaurantId indexPath:self.fromCellIndexPath];
    }

}
- (void)gotoRDP{

    NSLog(@"touxiang");
    if (self.Delegate) {
        [self.Delegate saveIconImageViewTableCell:self indexPath:self.fromCellIndexPath save:self.model];
    }
    
}
@end
