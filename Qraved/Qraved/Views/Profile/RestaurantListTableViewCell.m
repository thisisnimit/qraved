//
//  RestaurantListTableViewCell.m
//  Qraved
//
//  Created by Laura on 14-8-12.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "RestaurantListTableViewCell.h"
#import "UIConstants.h"
#import "UIColor+Helper.h"
@implementation RestaurantListTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
    self.restaurantImage.layer.cornerRadius = 4;
    self.restaurantImage.layer.masksToBounds = YES;
    
    self.restaurantTitleLabel.font = [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:18];
    
    self.restaurantInfoLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    
    self.starImageView.layer.masksToBounds=YES;
    self.starImageView.backgroundColor = [UIColor clearColor];
    self.reviewCountLabel.textColor = [UIColor colorFFC000];
    self.reviewCountLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:10];
    self.distanceLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:10];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
