//
//  UserReviewView.m
//  Qraved
//
//  Created by Lucky on 15/10/22.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import "UserReviewView.h"
#import "UIConstants.h"
#import "UIColor+Helper.h"
#import "UIColor+Hex.h"
#import "NSString+Helper.h"
#import "UIView+Helper.h"
#import "UIDevice+Util.h"
#import "UIButton+WebCache.h"
#import "UILabel+Helper.h"
#import "DLStarRatingControl.h"
#import "NSNumber+Helper.h"
#import "Label.h"
#import "IMGDish.h"
#import "Date.h"
#import "UIImageView+WebCache.h"

@implementation UserReviewView
-(id)initWithReview:(IMGUserReview *)userReview andDish:(NSArray*)dishArray andIsFromReviewViewController:(BOOL)isFromReviewViewController andIsOtherUser:(BOOL)isOtherUser
{
    self = [super init];
    if (self) {
        self.isOtherUser = isOtherUser;
        self.review = userReview;

        dishArray = (NSArray *)self.review.dishListArrM;
        CGFloat currentPointY = 0 + 15;

        //添加review点击
        UITapGestureRecognizer *tap_1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(reviewViewPressed)];
        [self addGestureRecognizer:tap_1];
        
        
        UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,currentPointY,DeviceWidth-LEFTLEFTSET*2-44,200)];
        titleLabel.text=self.review.restaurantTitle;
        titleLabel.numberOfLines = 0;
        titleLabel.frame=CGRectMake(0,currentPointY,DeviceWidth-LEFTLEFTSET*2-44,titleLabel.expectedHeight);
        titleLabel.font=[UIFont fontWithName:FONT_OPEN_SANS_BOLD size:18];
        titleLabel.textColor=[UIColor colorWithHexString:@"#000000"];
//        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewDetail:)];
//        tap.numberOfTapsRequired=1;
//        tap.numberOfTouchesRequired=1;
//        [titleLabel addGestureRecognizer:tap];
        titleLabel.userInteractionEnabled=YES;
        CGSize titleLabelsize = [titleLabel.text sizeWithFont:titleLabel.font constrainedToSize:CGSizeMake(titleLabel.frame.size.width, 10000) lineBreakMode:NSLineBreakByWordWrapping];
        titleLabel.frame = CGRectMake(LEFTLEFTSET, titleLabel.frame.origin.y, titleLabelsize.width, titleLabelsize.height);
        currentPointY = titleLabel.endPointY;

        NSString *dollar=nil;
        switch(self.review.restaurantPriceLevel.intValue){
            case 1:dollar=@"Below 100K";break;
            case 2:dollar=@"100K - 200K";break;
            case 3:dollar=@"200K - 300K";break;
            case 4:dollar=@"Start from 300K";break;
        }
        UIFont *font=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
        NSString *cuisineAndDistrictText=[NSString stringWithFormat:@"%@ / %@ / %@",self.review.restaurantCuisine,self.review.restaurantDistrict,dollar];
        CGSize size=[cuisineAndDistrictText sizeWithFont:font constrainedToSize:CGSizeMake(DeviceWidth-18*2-70-5,1000) lineBreakMode:NSLineBreakByWordWrapping];
        
        UILabel *cuisineAndDistrictLabel=[[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET,titleLabel.endPointY, DeviceWidth-LEFTLEFTSET*2, size.height)];
        cuisineAndDistrictLabel.numberOfLines=2;
        cuisineAndDistrictLabel.lineBreakMode=NSLineBreakByTruncatingTail;
        cuisineAndDistrictLabel.font=font;
        cuisineAndDistrictLabel.textColor=[UIColor color222222];
        cuisineAndDistrictLabel.text=cuisineAndDistrictText;
        
        currentPointY=cuisineAndDistrictLabel.endPointY;
        
        UIView *restaurantView = [[UIView alloc] init];
        restaurantView.frame = CGRectMake(0, 0, DeviceWidth, currentPointY);
        if (!isFromReviewViewController)
        {
            [self addSubview:restaurantView];
        }
        else
        {
            currentPointY = 0 + 10;
        }
        [restaurantView addSubview:titleLabel];
        [restaurantView addSubview:cuisineAndDistrictLabel];
        
        UITapGestureRecognizer *restaurantViewTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(restaurantViewClick)];
        [restaurantView addGestureRecognizer:restaurantViewTap];
        
        
        UIButton *avatarLinkButton=[UIButton buttonWithType:UIButtonTypeCustom];
        //        [avatarLinkButton addTarget:self action:@selector(avatarLink:) forControlEvents:UIControlEventTouchUpInside];
        [avatarLinkButton setFrame:CGRectMake(LEFTLEFTSET, currentPointY+10, MIDDLE_AVATAR_IMAGE_WIDTH, MIDDLE_AVATAR_IMAGE_HEIGHT)];
        avatarLinkButton.clipsToBounds = YES;
        avatarLinkButton.layer.cornerRadius = MIDDLE_AVATAR_IMAGE_WIDTH/2;
        NSURL *urlString;
        self.review.userAvatar = [self.review.userAvatar stringByReplacingPercentEscapesUsingEncoding:
                       NSUTF8StringEncoding];

        if ([self.review.userAvatar hasPrefix:@"http"])
        {
            urlString = [NSURL URLWithString:self.review.userAvatar];
        }
        else
        {
            urlString = [NSURL URLWithString:[self.review.userAvatar returnFullImageUrl]];
        }
        
        [avatarLinkButton sd_setImageWithURL:urlString forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@"headDefault.jpg"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
        [self addSubview:avatarLinkButton];
        
        
        UIButton *moreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        moreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        moreBtn.frame = CGRectMake(DeviceWidth - 25, avatarLinkButton.frame.origin.y - 3, 25, 50);
        [moreBtn setImage:[UIImage imageNamed:@"listMoreVertical"] forState:UIControlStateNormal];
        [self addSubview:moreBtn];
        if (self.isOtherUser)
        {
            moreBtn.hidden = YES;
        }
        [moreBtn addTarget:self action:@selector(moreBtnClcik) forControlEvents:UIControlEventTouchUpInside];

        currentPointY = avatarLinkButton.frame.origin.y;
        
        if (self.review.reviewTitle.length > 0)
        {
            UILabel *titleLabel = [[UILabel alloc] init];
            titleLabel.font = [UIFont systemFontOfSize:12];
            titleLabel.textColor = [UIColor colorWithHexString:@"#ab0009"];
            titleLabel.numberOfLines = 0;
            titleLabel.text = [self.review.reviewTitle filterHtml];
            titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
//            titleLabel.frame = CGRectMake(LEFTLEFTSET+MIDDLE_AVATAR_IMAGE_WIDTH+LEFTLEFTSET, currentPointY+3, DeviceWidth-[avatarLinkButton endPointX]-LEFTLEFTSET*2, 2000);
            titleLabel.frame = CGRectMake(LEFTLEFTSET+35+LEFTLEFTSET, currentPointY+1, DeviceWidth-[avatarLinkButton endPointX]-LEFTLEFTSET*2, titleLabel.expectedHeight);
            [self addSubview:titleLabel];
            currentPointY = titleLabel.endPointY;
        }
        
        
        UILabel *scoreLabel = [[UILabel alloc] init];
        float score = [self.review.reviewScore floatValue];
        scoreLabel.text = [NSString stringWithFormat:@"%.1f",score/2];
        scoreLabel.font = [UIFont systemFontOfSize:12.5];

        scoreLabel.textColor = [UIColor colorFFC000];
        scoreLabel.frame = CGRectMake(LEFTLEFTSET+35+LEFTLEFTSET, currentPointY+3, scoreLabel.expectedWidth, scoreLabel.font.lineHeight);
        [self addSubview:scoreLabel];
        
        DLStarRatingControl *ratingScore=[[DLStarRatingControl alloc]initWithFrame:CGRectMake(scoreLabel.endPointX+5, scoreLabel.frame.origin.y, 100, 12) andStars:5 andStarWidth:12 isFractional:YES spaceWidth:3];
        ratingScore.userInteractionEnabled=NO;
        ratingScore.rating= [[NSNumber numberWithDouble:[self.review.reviewScore doubleValue]] getRatingNumber];
        [self addSubview:ratingScore];
        CGSize desSize = [self.review.reviewSummarize sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] constrainedToSize:CGSizeMake(DeviceWidth-avatarLinkButton.endPointX-20-LEFTLEFTSET, 100) lineBreakMode:NSLineBreakByTruncatingTail];
        
        
        Label *timeLabel = [[Label alloc]initWithFrame:CGRectMake(DeviceWidth-LEFTLEFTSET-(DeviceWidth-avatarLinkButton.endPointX-20-LEFTLEFTSET)-4, scoreLabel.frame.origin.y, DeviceWidth-avatarLinkButton.endPointX-20-LEFTLEFTSET, 15) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:11] andTextColor:[UIColor colorAAAAAA] andTextLines:1];

        timeLabel.text = [Date getTimeInteval_v4:[userReview.reviewTimeCreated longLongValue]/1000];
        timeLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:timeLabel];
        
        
        Label *desLabel = [[Label alloc]initWithFrame:CGRectMake(scoreLabel.frame.origin.x, timeLabel.endPointY +5, DeviceWidth-LEFTLEFTSET*2, desSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor blackColor] andTextLines:0];
        desLabel.text = self.review.reviewSummarize;
        desLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        desLabel.numberOfLines = 0;
        size = [desLabel.text sizeWithFont:desLabel.font constrainedToSize:CGSizeMake(DeviceWidth-LEFTLEFTSET*3-avatarLinkButton.frame.size.width, 10000) lineBreakMode:NSLineBreakByCharWrapping];
        
        UILabel *tempLabel = [[UILabel alloc] init];
        tempLabel.text = @"TEXTTEXTTEXTTEXTTEXTTEXTTEXTTEXTTEXTTEXTTEXTTEXTTEXTTEX";
        tempLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        tempLabel.numberOfLines = 0;
        tempLabel.frame = desLabel.frame;
//        CGSize tempSize = [tempLabel.text sizeWithFont:tempLabel.font constrainedToSize:CGSizeMake(DeviceWidth-avatarLinkButton.endPointX-20-LEFTLEFTSET, 10000) lineBreakMode:NSLineBreakByCharWrapping];
//        
//        tempLabel.backgroundColor = [UIColor redColor];
        //[self addSubview:tempLabel];
        
//        if (size.height > tempSize.height)
//        {
//            size.height = tempSize.height;
//        }
        float i=0;
        if ([UIDevice isIphone6Plus]) {
            i=20;
        }
        if ([UIDevice isIphone6]) {
            i=20;
        }
        desLabel.frame = CGRectMake(desLabel.frame.origin.x, desLabel.frame.origin.y, size.width, size.height+i);
        

        [self addSubview:desLabel];
        
        UIView *photosView = [[UIView alloc] init];
        photosView.frame = CGRectMake(desLabel.frame.origin.x, desLabel.endPointY + 17, DeviceWidth - desLabel.frame.origin.x - LEFTLEFTSET, 0);
        
        CGFloat dishImageWidth;
        CGFloat dishImageEndX;
        int viewFrameY = 0;
        int viewFrameX = 0;
        for (int i = 0; i<dishArray.count; i++)
        {
            IMGDish *dish=(IMGDish*)[dishArray objectAtIndex:i];
            UIImageView *dishImageView = [[UIImageView alloc]init];
            dishImageWidth = (photosView.frame.size.width-10)/3;
            dishImageView.userInteractionEnabled=YES;
            if (viewFrameX == 3)
            {
                viewFrameY = dishImageWidth + 5 + viewFrameY;
                viewFrameX = 0;
            }
            
            dishImageView.frame = CGRectMake((dishImageWidth+5)*viewFrameX, viewFrameY, dishImageWidth, dishImageWidth);
            viewFrameX++;
            dishImageView.tag = i;
            [photosView addSubview:dishImageView];
            
            UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imagePressed:)];
            [dishImageView addGestureRecognizer:tap];
            
            dishImageEndX = dishImageView.endPointX;
            
            if (i == 5 && dishArray.count > 6)
            {
                dishImageView.backgroundColor = [UIColor colorWithRed:222/250.0 green:222/250.0 blue:222/250.0 alpha:1];
                UILabel *otherCountLabel = [[UILabel alloc] init];
                otherCountLabel.tag=i;
                otherCountLabel.userInteractionEnabled=YES;
                otherCountLabel.text = [NSString stringWithFormat:@"+%lu",dishArray.count - 5];
                otherCountLabel.textColor = [UIColor blackColor];
                otherCountLabel.frame = CGRectMake(dishImageView.frame.size.width/2 - 13, dishImageView.frame.size.height/2 - 11, 26, 22);
                [otherCountLabel addGestureRecognizer:tap];
                [dishImageView addSubview:otherCountLabel];
                break;
            }
            else
            {
                NSString *urlString = [dish.imageUrl returnFullImageUrlWithWidth:(DeviceWidth-19)/3];
                __weak typeof(dishImageView) weakRestaurantImageView = dishImageView;
//                [dishImageView setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//                    [UIView animateWithDuration:1.0 animations:^{
//                        [weakRestaurantImageView setAlpha:1.0];
//                    }];
//                }];
                [dishImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    [UIView animateWithDuration:1.0 animations:^{
                                                [weakRestaurantImageView setAlpha:1.0];
                                            }];
                }];
            }
            
            photosView.frame = CGRectMake(desLabel.frame.origin.x, desLabel.endPointY + 5, DeviceWidth - desLabel.frame.origin.x - LEFTLEFTSET, dishImageView.endPointY);
        }
        [self addSubview:photosView];
        
        Label *nameLabel=[[Label alloc]initWithFrame:CGRectMake(DeviceWidth-LEFTLEFTSET-(DeviceWidth-avatarLinkButton.endPointX+10)-4, photosView.endPointY+17, DeviceWidth-avatarLinkButton.endPointX+10, NAME_LABEL_HEIGHT) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:15] andTextColor:[UIColor blackColor] andTextLines:1];
        nameLabel.text = [NSString stringWithFormat:@"- %@",[self.review.userFullName filterHtml]];
        nameLabel.textColor = [UIColor colorWithHexString:@"#acacac"];
        nameLabel.font = [UIFont systemFontOfSize:12];
        nameLabel.frame = CGRectMake(DeviceWidth-LEFTLEFTSET-nameLabel.expectedWidth, photosView
                                     .endPointY+5, nameLabel.expectedWidth, nameLabel.font.lineHeight);
        [self addSubview:nameLabel];
        self.reviewHeight = nameLabel.endPointY+15;

        if (self.review.reviewSummarize.length == 0 && self.review.title.length == 0 && !isFromReviewViewController && !isOtherUser)
        {
            UIView *writeReviewView = [[UIView alloc] init];
            writeReviewView.frame = CGRectMake(LEFTLEFTSET, nameLabel.endPointY + 8, DeviceWidth - LEFTLEFTSET*2, 40);
            [self addSubview:writeReviewView];
            
            UILabel *lineLabel = [[UILabel alloc] init];
            lineLabel.frame = CGRectMake(0, 0, DeviceWidth - LEFTLEFTSET*2, 1);
            lineLabel.backgroundColor = [UIColor grayColor];
            lineLabel.alpha = 0.3f;
            [writeReviewView addSubview:lineLabel];
            
            UIImageView *writeImageView = [[UIImageView alloc] init];
            writeImageView.frame = CGRectMake(0, 7, 13, 13);
            writeImageView.image = [UIImage imageNamed:@"writereview"];
            [writeReviewView addSubview:writeImageView];
            
            UILabel *writeLabel = [[UILabel alloc] init];
            writeLabel.text = L(@"Write Review");
            writeLabel.frame = CGRectMake(writeImageView.endPointX + 7, 5, 150, 15);
            writeLabel.font = [UIFont systemFontOfSize:13];
            writeLabel.textColor = [UIColor grayColor];
            writeLabel.alpha = 0.7f;
            writeLabel.textAlignment = NSTextAlignmentLeft;
            [writeReviewView addSubview:writeLabel];
            
            
            UITapGestureRecognizer *tap_1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(writeRview)];
            [writeReviewView addGestureRecognizer:tap_1];

            
            self.reviewHeight = writeReviewView.endPointY;
        }
        if (!isFromReviewViewController)
        {
            UILabel *lineLabel = [[UILabel alloc] init];
            lineLabel.frame = CGRectMake(LEFTLEFTSET, self.reviewHeight-1, DeviceWidth - LEFTLEFTSET*2, 1);
            lineLabel.backgroundColor = [UIColor grayColor];
            lineLabel.alpha = 0.3f;
            [self addSubview:lineLabel];

        }
        
    }

    return self;
}
- (void)writeRview
{
    if([self.delegate respondsToSelector:@selector(userReviewWriteReviewWithIMGUserReview:)]){
        [self.delegate userReviewWriteReviewWithIMGUserReview:self.review];
    }
}

-(void)reviewViewPressed
{
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    restaurant.restaurantId = self.review.restaurantId;
    restaurant.ratingScore = [NSString stringWithFormat:@"%@",self.review.reviewScore];
    restaurant.title=self.review.restaurantTitle;
    if([self.delegate respondsToSelector:@selector(userReviewViewClickWithRestaurant:)])
    {
        [self.delegate userReviewViewClickWithRestaurant:restaurant];
    }
}

-(void)imagePressed:(UIGestureRecognizer*)gesture{
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    restaurant.restaurantId = self.review.restaurantId;
    restaurant.ratingScore = [NSString stringWithFormat:@"%@",self.review.reviewScore];

    if([self.delegate respondsToSelector:@selector(userReviewImagePressed:images:withTag:andRestaurant:)]){
        [self.delegate userReviewImagePressed:self images:self.review.dishListArrM withTag:gesture.view.tag andRestaurant:restaurant];
    }
    
}
- (void)moreBtnClcik
{
    if([self.delegate respondsToSelector:@selector(userReviewMoreBtnClickWithIMGUserReview:)]){
        [self.delegate userReviewMoreBtnClickWithIMGUserReview:self.review];
    }
}
- (void)restaurantViewClick
{
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    restaurant.restaurantId = self.review.restaurantId;
    if([self.delegate respondsToSelector:@selector(userReviewRestaurantClickWithRestaurant:)]){
        [self.delegate userReviewRestaurantClickWithRestaurant:restaurant];
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
