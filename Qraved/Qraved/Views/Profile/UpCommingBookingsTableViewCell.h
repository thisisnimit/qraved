//
//  UpCommingBookingsTableViewCell.h
//  Qraved
//
//  Created by Laura on 14-9-10.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Label.h"
#import <CoreText/CoreText.h>
#import "UIConstants.h"
#import "Label.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"

#import "IMGReservation.h"

@interface UpCommingBookingsTableViewCell : UITableViewCell

@property (nonatomic,retain) UIButton *detailButton;
@property (nonatomic,retain) UIButton *phoneButton;
@property (nonatomic,retain) UIButton *shareButton;
@property (nonatomic,retain) UIButton *calandarButton;
@property (nonatomic,retain) UIButton *cancelButton;
@property (nonatomic,retain) UIButton *discountLabel;

@property (nonatomic,retain) Label *dateLabel;
@property (nonatomic,retain) Label * titleLabel;
@property (nonatomic,retain) Label *timeLabel;
@property (nonatomic,retain) Label *voucherLabel;
@property (nonatomic,retain) Label *codeLabel;
@property (nonatomic,retain) Label *statusLabel;
@property (nonatomic,retain) Label *voucherValueLabel;
@property (nonatomic,retain) Label *codeValueLabel;
@property (nonatomic,retain) Label *statusValueLabel;
@property (nonatomic,retain) Label *addressLabel;
@property (nonatomic,retain) Label *personLabel;

@property (nonatomic,strong) UIView *bottomBox;
@property (nonatomic,strong) UIView *offerBox;

-(void)initializeCellWithReservation:(IMGReservation *)reservation;

@end
