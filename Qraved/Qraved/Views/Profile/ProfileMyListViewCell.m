//
//  ProfileMyListViewCell.m
//  Qraved
//
//  Created by System Administrator on 10/19/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "ProfileMyListViewCell.h"
#import "UIImageView+WebCache.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "UIImage+Resize.h"
#import "UIView+Helper.h"

@implementation ProfileMyListViewCell{
	UIView *maskView;
	UIImageView *listImage;
	UIImageView *moreImage;
	UIImageView *iconImage;
	UILabel *countLabel;
	UILabel *nameLabel;
	UIFont *nameFont;
	UIView *moreBox;
	BOOL canEdit;

}

-(instancetype)initWithFrame:(CGRect)frame{
	if(self=[super initWithFrame:frame]){
		self.backgroundColor=[UIColor colorCCCCCC];
		self.layer.borderColor=[UIColor colorCCCCCC].CGColor;
		self.layer.borderWidth=1;
		maskView=[[UIView alloc] initWithFrame:CGRectMake(0,0,frame.size.width,frame.size.height)];
		maskView.backgroundColor=[UIColor blackColor];
		maskView.alpha=0.4;
		listImage=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,frame.size.width,frame.size.height)];
		// listImage.maskView=maskView;
	
		UIImage *listMore=[UIImage imageNamed:@"more_button"];
		moreBox=[[UIView alloc] initWithFrame:CGRectMake(frame.size.width-listMore.size.width-12,5,listMore.size.width+8,listMore.size.height+10)];
//		moreBox.backgroundColor=[[UIColor alloc] initWithRed:0 green:0 blue:0 alpha:0.6];
		moreBox.userInteractionEnabled=YES;
		moreBox.layer.zPosition=99;
		moreImage=[[UIImageView alloc] initWithFrame:CGRectMake(4,5,listMore.size.width,listMore.size.height)];
		[moreImage setImage:listMore];
		UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moreClick:)];
		[moreBox addGestureRecognizer:tap];
		
		UIView *countView=[[UIView alloc] initWithFrame:CGRectMake(5,frame.size.height-35,25,25)];
		countView.backgroundColor=[[UIColor alloc] initWithRed:0 green:0 blue:0 alpha:0.6];
		countView.layer.zPosition=99;
		countLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,0,25,25)];
		countLabel.textAlignment=NSTextAlignmentCenter;
		countLabel.font=[UIFont boldSystemFontOfSize:14];
		countLabel.textColor=[UIColor whiteColor];
		[countView addSubview:countLabel];
		nameLabel=[[UILabel alloc] init];
		nameLabel.layer.zPosition=99;
		nameLabel.textAlignment=NSTextAlignmentCenter;
		nameLabel.numberOfLines=3;
		nameFont=[UIFont boldSystemFontOfSize:18];
		nameLabel.font=nameFont;
		nameLabel.textColor=[UIColor whiteColor];
		[self addSubview:listImage];
		[self addSubview:maskView];
		[self addSubview:countView];
		[self addSubview:moreBox];
		[moreBox addSubview:moreImage];
		[self addSubview:nameLabel];

		[self addObserver:self forKeyPath:@"mylist" options:NSKeyValueObservingOptionNew context:nil];
	}
	return self;
}

-(void)observeValueForKeyPath:(NSString*)keypath ofObject:(id)objc change:(NSDictionary*)change context:(void*)context{
	if([keypath isEqualToString:@"mylist"]){
		IMGMyList *mylist=(IMGMyList*)[change objectForKey:@"new"];
		__weak UIImageView *weakListImage=listImage;
		NSArray *images=[mylist.listImageUrls componentsSeparatedByString:@","];
		if(mylist.listImageUrls && mylist.listImageUrls.length && images.count>0){
			[listImage setImageWithURL:[NSURL URLWithString:[[images objectAtIndex:0] returnFullImageUrlWithWidth:160]] placeholderImage:[[UIImage imageNamed:@"placeholder"] imageByScalingAndCroppingForSize:CGSizeMake(listImage.frame.size.width, listImage.frame.size.height)] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
                if (image) {
                    image = [image imageByScalingAndCroppingForSize:CGSizeMake(weakListImage.frame.size.width, weakListImage.frame.size.height)];
                    weakListImage.image = image;
                }
            }];
		}else{
            UIImage *placeholder = [UIImage imageNamed:@"placeholder"];
            listImage.image = [placeholder imageByScalingAndCroppingForSize:CGSizeMake(listImage.frame.size.width, listImage.frame.size.height)];
		}
        CGSize cellsize=self.frame.size;
        if (iconImage != nil) {
            [iconImage removeFromSuperview];
            iconImage = nil;
        }
		if([mylist.listName isEqualToString:@"Favorites"]){
			iconImage=[[UIImageView alloc] initWithFrame:CGRectMake((cellsize.width-16)/2,(cellsize.height/2-21),16,16)];
			moreBox.hidden=YES;
			moreImage.hidden=YES;
			[iconImage setImage:[UIImage imageNamed:@"LikeRed"]];
			[self addSubview:iconImage];
			CGSize size=[mylist.listName sizeWithFont:nameFont constrainedToSize:CGSizeMake(cellsize.width-20,1000)];
			nameLabel.frame=CGRectMake((cellsize.width-size.width)/2,iconImage.endPointY+10,size.width,size.height);
		}else{
			moreBox.hidden=NO;
			moreImage.hidden=NO;
			CGSize size=[mylist.listName sizeWithFont:nameFont constrainedToSize:CGSizeMake(cellsize.width-20,1000)];
			nameLabel.frame=CGRectMake((cellsize.width-size.width)/2,(cellsize.height-size.height)/2,size.width,size.height);
		}
        if (self.isOtherUser)
        {
            moreBox.hidden = YES;
        }
		nameLabel.text=mylist.listName;
		// listImage.tag=[mylist.listId intValue];

       	countLabel.text=[NSString stringWithFormat:@"%@",mylist.listCount];
        
	}
}

-(void)moreClick:(UITapGestureRecognizer*)tap{
	if(self.delegate && [self.delegate respondsToSelector:@selector(mylistMoreClick:)]){
		[self.delegate mylistMoreClick:tap.view.superview.tag-1];
	}
}

-(void)dealloc{
	[self removeObserver:self forKeyPath:@"mylist" context:nil];
}

@end
