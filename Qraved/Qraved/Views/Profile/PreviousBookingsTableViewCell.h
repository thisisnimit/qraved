//
//  PreviousBookingsTableViewCell.h
//  Qraved
//
//  Created by Laura on 14-9-10.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGReservation.h"
#import "Label.h"

@interface PreviousBookingsTableViewCell : UITableViewCell
@property (nonatomic,retain) UIButton *detailButton;
@property (nonatomic,retain) UIButton *discountLabel;
@property (nonatomic,retain) Label *titleLabel;
@property (nonatomic,retain) TYAttributedLabel *dateLabel;
@property (nonatomic,retain) Label *timeLabel;
@property (nonatomic,retain) Label *voucherLabel;
@property (nonatomic,retain) Label *codeLabel;
@property (nonatomic,retain) Label *statusLabel;
@property (nonatomic,retain) Label *voucherValueLabel;
@property (nonatomic,retain) Label *codeValueLabel;
@property (nonatomic,retain) Label *statusValueLabel;
@property (nonatomic,retain) Label *addressLabel;
@property (nonatomic,retain) TYAttributedLabel *personLabel;

@property (nonatomic,strong) UIView *bottomBox;
@property (nonatomic,strong) UIView *offerBox;

-(void)initializeCellWithReservation:(IMGReservation *)reservation;
@end
