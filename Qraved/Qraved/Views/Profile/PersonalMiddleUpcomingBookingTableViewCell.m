//
//  PersonalMiddleUpcomingBookingTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/6/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "PersonalMiddleUpcomingBookingTableViewCell.h"

@implementation PersonalMiddleUpcomingBookingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}
- (void)initUI{
    
    _lblUpcomingBooking = [UILabel new];
  
    _lblUpcomingBooking.textColor = [UIColor color333333];
    _lblUpcomingBooking.font = [UIFont systemFontOfSize:14];
    
    _lblDate = [UILabel new];
    _lblDate.textColor = [UIColor color999999];
    _lblDate.font = [UIFont systemFontOfSize:12];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    
    _iconImageView = [UIButton  buttonWithType:UIButtonTypeCustom];
    _iconImageView.backgroundColor = [UIColor whiteColor];
    [_iconImageView addTarget:self action:@selector(iconImageViewClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _lbluserName = [UILabel new];
    _lbluserName.textColor = [UIColor color333333];
    _lbluserName.font = [UIFont systemFontOfSize:14];
    
    _lblTitle = [UILabel new];
    _lblTitle.textColor = [UIColor color999999];
    _lblTitle.font = [UIFont systemFontOfSize:14];
    
    _btnwhiteReview = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnwhiteReview setTitle:@"Cancel" forState:UIControlStateNormal];
    [_btnwhiteReview setTitleColor:[UIColor colortextBlue] forState:UIControlStateNormal];
    _btnwhiteReview.titleLabel.font = [UIFont systemFontOfSize:14];
    [_btnwhiteReview addTarget:self action:@selector(btnCancelClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _lblPax = [UILabel new];
    _lblPax.textColor = [UIColor color333333];
    _lblPax.font = [UIFont systemFontOfSize:14];
    
    _lbldetailDate = [UILabel new];
//    _lbldetailDate.text = @"Friday, Jan 27 2017 05.00 AM";
    _lbldetailDate.textColor = [UIColor color333333];
    _lbldetailDate.font = [UIFont systemFontOfSize:14];
    
    UIView *lineViewA = [UIView new];
    lineViewA.backgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    
    _bottomView = [UIView new];
    _bottomView.backgroundColor = [UIColor whiteColor];
    
    _btnCall = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnCall setTitle:@"Call" forState:UIControlStateNormal];
    [_btnCall setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
    [_btnCall setImage:[UIImage imageNamed:@"callBtn"] forState:UIControlStateNormal];
    _btnCall.titleLabel.font = [UIFont systemFontOfSize:12];
    _btnCall.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    [_btnCall addTarget:self action:@selector(btnCallClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _btnViewMap = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnViewMap setTitle:@"View Map" forState:UIControlStateNormal];
    [_btnViewMap setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
    [_btnViewMap setImage:[UIImage imageNamed:@"ic_map_small"] forState:UIControlStateNormal];
    _btnViewMap.titleLabel.font = [UIFont systemFontOfSize:12];
    _btnViewMap.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    [_btnViewMap addTarget:self action:@selector(btnViewMapClick:) forControlEvents:UIControlEventTouchUpInside];
    
    _btnReminder = [UIButton buttonWithType:UIButtonTypeCustom];
    [_btnReminder setTitle:@"Reminder" forState:UIControlStateNormal];
    [_btnReminder setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
    [_btnReminder setImage:[UIImage imageNamed:@"reminder"] forState:UIControlStateNormal];
    _btnReminder.titleLabel.font = [UIFont systemFontOfSize:12];
    _btnReminder.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    [_btnReminder addTarget:self action:@selector(btnReminderClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [_bottomView sd_addSubviews:@[_btnCall, _btnViewMap, _btnReminder]];
    [self.contentView sd_addSubviews:@[_lblUpcomingBooking, _lblDate, lineView, _iconImageView, _lbluserName, _lblTitle, _btnwhiteReview, _lblPax, _lbldetailDate, lineViewA, _bottomView]];
    
    _lblUpcomingBooking.sd_layout
    .topSpaceToView(self.contentView, 10)
    .leftSpaceToView(self.contentView, 15)
    .heightIs(20);
    [_lblUpcomingBooking setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
    _lblDate.sd_layout
    .topEqualToView(_lblUpcomingBooking)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(20);
    [_lblDate setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
    lineView.sd_layout
    .topSpaceToView(_lblUpcomingBooking, 10)
    .leftSpaceToView(self.contentView, 15)
    .widthIs(DeviceWidth - 30)
    .heightIs(1);
    
    _iconImageView.sd_layout
    .topSpaceToView(lineView, 10)
    .leftEqualToView(_lblUpcomingBooking)
    .widthIs(40)
    .heightIs(40);
    
    _lbluserName.sd_layout
    .topSpaceToView(lineView, 15)
    .leftSpaceToView(_iconImageView, 5)
    .heightIs(20);
    [_lbluserName setSingleLineAutoResizeWithMaxWidth: DeviceWidth - 200];
    
    _lblTitle.sd_layout
    .topSpaceToView(_lbluserName, 5)
    .leftEqualToView(_lbluserName)
    .heightIs(20);
    [_lblTitle setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 200];
    
    _btnwhiteReview.sd_layout
    .centerYEqualToView(_iconImageView)
    .rightSpaceToView(self.contentView, 15)
    .widthIs(106)
    .heightIs(20);
    
    _lblPax.sd_layout
    .topSpaceToView(_iconImageView,10)
    .leftEqualToView(_iconImageView)
    .heightIs(20);
    [_lblPax setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 30];
    
    _lbldetailDate.sd_layout
    .topSpaceToView(_lblPax, 5)
    .leftEqualToView(_lblPax)
    .heightIs(20);
    [_lbldetailDate setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 30];
    
    lineViewA.sd_layout
    .topSpaceToView(_lbldetailDate, 10)
    .leftSpaceToView(self.contentView, 15)
    .widthIs(DeviceWidth - 30)
    .heightIs(1);
    
    _bottomView.sd_layout
    .topSpaceToView(lineViewA, 0)
    .leftSpaceToView(self.contentView, 0)
    .widthIs(DeviceWidth)
    .heightIs(40);
    
    _btnCall.sd_layout
    .topSpaceToView(_bottomView, 0)
    .leftSpaceToView(_bottomView, 0)
    .widthIs(DeviceWidth/3)
    .heightIs(40);
    
    _btnViewMap.sd_layout
    .topSpaceToView(_bottomView, 0)
    .leftSpaceToView(_btnCall, 0)
    .widthIs(DeviceWidth/3)
    .heightIs(40);
    
    _btnReminder.sd_layout
    .topSpaceToView(_bottomView, 0)
    .leftSpaceToView(_btnViewMap, 0)
    .widthIs(DeviceWidth/3)
    .heightIs(40);

    
//    [self.contentView setupAutoHeightWithBottomView:_bottomView bottomMargin:10];
   
    
}
- (void)setModel:(IMGJourneyBook *)model{
    
    _model = model;
  
    IMGUser *user = [IMGUser currentUser];
    //    NSLog(@"%@========%@",model.userId, user.userId);
    if (![model.myID isEqualToNumber:user.userId]) {
        _btnwhiteReview.hidden = YES;
    }
    [_iconImageView sd_setImageWithURL:[NSURL URLWithString:[model.restaurantImage returnFullImageUrl]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:nil];
    
      _lblUpcomingBooking.text = @"Upcoming Booking";
    _lblDate.text = model.bookDate ? [Date dateFormart:model.bookDate]:@"";
    _lbluserName.text = model.restaurantTitle ?model.restaurantTitle :@"";
    _lblTitle.text = [NSString stringWithFormat:@"%@ • %@",model.restaurantCuisine, model.restaurantDistrict] ? [NSString stringWithFormat:@"%@ • %@",model.restaurantCuisine, model.restaurantDistrict] :@"";
    
    _lblPax.text = model.bookPax ? [NSString stringWithFormat:@"Pax for %@ people",model.bookPax] : @"";
    
    _lbldetailDate.text = model.bookTime ?[NSString stringWithFormat:@"%@ %@",[Date dateFormart:model.bookDate],[Date timeFormart:model.bookTime]]: @"";
    
}
- (void)iconImageViewClick:(UIButton *)iconImageViewClick{

    NSLog(@"touxiang ");
    if (self.Delegate) {
        [self.Delegate UpcomingBookingIconImageViewTableCell:self indexPath:self.fromCellIndexPath book:self.model];
    }
    
}
//  NSLog(@"call");
- (void)btnCallClick :(UIButton *)btnCall{
 
    if (self.Delegate) {
        [self.Delegate UpcomingBookingHomeReviewTableCell:self indexPath:self.fromCellIndexPath book:self.model];
    }
    
}
//NSLog(@"viewmap");
- (void)btnViewMapClick :(UIButton *)btnViewMap{
    
    if (self.Delegate) {
        [self.Delegate UpcomingBookingViewMapTableCell:self indexPath:self.fromCellIndexPath book:self.model];
    }
    
}
// NSLog(@"reminder");
- (void)btnReminderClick :(UIButton *)btnReminder{
 
    if (self.Delegate) {
        [self.Delegate UpcomingBookingReminderTableCell:self indexPath:self.fromCellIndexPath book:self.model];
    }
    
}
- (void)btnCancelClick:(UIButton *)btnCancel{
    NSLog(@"btnCancelClick");

    
//    [[Amplitude instance] logEvent:@"TR - Reservation Cancel Initiate" withEventProperties:@{@"Reservation_ID":reservation.reservationId}];
    UIView *popUpView = [[UIView alloc]init];
    
    NSString *offerTitleStr = L(@"Are you sure want to cancel your booking?");
    
    CGSize maxSize = [offerTitleStr sizeWithFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
    Label *seeMenuTitle = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 64/2-(maxSize.height+4)/2, maxSize.width, maxSize.height+4) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] andTextColor:[UIColor color333333] andTextLines:0];
    seeMenuTitle.text = offerTitleStr;
    seeMenuTitle.textAlignment = NSTextAlignmentCenter;
    [popUpView addSubview:seeMenuTitle];
    
    NSArray *menusArr = @[L(@"We appreciate the gesture. This means other guests can get a seat if you can't attend.")];
    float startY = seeMenuTitle.endPointY;
    for (int i=0; i<menusArr.count; i++) {
        CGSize maxSize1 = [[menusArr objectAtIndex:i] sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
        
        Label *seeMenuLabel = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, startY, DeviceWidth-4*LEFTLEFTSET, maxSize1.height+20) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15] andTextColor:[UIColor color333333] andTextLines:0];
        seeMenuLabel.textAlignment = NSTextAlignmentCenter;
        seeMenuLabel.text = [menusArr objectAtIndex:i];
        seeMenuLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [popUpView addSubview:seeMenuLabel];
        startY = seeMenuLabel.endPointY;
    }
    popUpView.frame = CGRectMake(0, 0, DeviceWidth-2*LEFTLEFTSET, startY);
    
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc]init];
    alertView.buttonTitles = @[@"No, keep it",@"Yes"];
    [alertView setContainerView:popUpView];
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, NSInteger buttonIndex) {
        if(buttonIndex==1){
            IMGUser * user = [IMGUser currentUser];
            if(user.userId==nil){
                //lead to login
                return;
            }
            if(user.token==nil){
                //lead to login
                return;
            }
            NSString *newStatus=@"13";
            if([_model.status isEqualToString:@"11"]){
                newStatus=@"14";
            }else if([_model.status isEqualToString:@"2"]){
                newStatus=@"3";
            }
            [[LoadingView sharedLoadingView] startLoading];
            
            NSDictionary * parameters = @{@"userID":user.userId,@"t":user.token,@"id":_model.myID,@"status":newStatus};
            [[IMGNetWork sharedManager]POST:@"reseravtion/status/update" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
                
                NSString *exceptionMsg = [responseObject objectForKey:@"exceptionmsg"];
                if([exceptionMsg isLogout]){
                    [[LoadingView sharedLoadingView] stopLoading];
                    [[IMGUser currentUser]logOut];
                    [[AppDelegate ShareApp] goToLoginController];
                    return;
                }
                
                NSString *returnStatusString = [responseObject objectForKey:@"status"];
                if([returnStatusString isEqualToString:@"succeed"]){

                    [_btnwhiteReview setHidden:YES];

                }
                if (self.Delegate) {
                    [self.Delegate delegateRefresh];
                }
                [[LoadingView sharedLoadingView]stopLoading];
            } failure:^(NSURLSessionDataTask *operation, NSError *error) {
                NSLog(@"MainMenuViewController.request cancel book.error: %@",error.localizedDescription);
                [[LoadingView sharedLoadingView]stopLoading];
            }];
            
        }
        if (buttonIndex==0) {
            NSMutableDictionary* eventDic=[[NSMutableDictionary alloc]init];
            [eventDic setValue:_model.myID forKey:@"Reservation_ID"];
            [eventDic setValue:_model.restaurantId forKey:@"Restaurant_ID"];
            [eventDic setValue:@"User Profile Page - Journey" forKey:@"Location "];
            [[Amplitude instance] logEvent:@"TR – Reservation Cancel Initiate" withEventProperties:eventDic];
        }
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    [alertView show];

}
@end
