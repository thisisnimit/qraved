//
//  ProfileUpcomingViewCell.m
//  Qraved
//
//  Created by System Administrator on 10/20/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "ProfileUpcomingViewCell.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+Helper.h"
#import "NSString+Helper.h"
#import "UIColor+Hex.h"
#import "UIView+Helper.h"
#import "UIConstants.h"
#import "IMGReservation.h"

#define RESTAURANT_IMAGE_WIDTH 80

@implementation ProfileUpcomingViewCell{
	
	UILabel *titleLabel;
	UILabel *dateLabel;
	UILabel *countLabel;
	UIImageView *imageView;
	UIView *boxView;
	CGFloat image_width;
	NSDateFormatter *dateFormatter;
}

-(instancetype)initWithFrame:(CGRect)frame{
	if(self=[super initWithFrame:frame]){

		CGFloat margin_left=DeviceWidth-LEFTLEFTSET*2-RESTAURANT_IMAGE_WIDTH;

		UIImageView *lineImage= [[UIImageView alloc] initShadowImageViewWithShadowOriginY_v2:0 andHeight:1];

		titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,10,margin_left,40)];
		titleLabel.font=[UIFont systemFontOfSize:18];
		titleLabel.numberOfLines=0;
		titleLabel.lineBreakMode=NSLineBreakByWordWrapping;

		boxView=[[UIView alloc] initWithFrame:CGRectMake(0,titleLabel.endPointY+5,margin_left,60)];

		UIImageView *dateImage=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"profileDate"]];
		dateImage.frame=CGRectMake(0,6,16,16);

		dateLabel=[[UILabel alloc] initWithFrame:CGRectMake(20,5,margin_left,18)];
		dateLabel.font=[UIFont systemFontOfSize:12];

		UIImageView *peopleImage=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"profilePeople"]];
		peopleImage.frame=CGRectMake(0,30,16,16);

		countLabel=[[UILabel alloc] initWithFrame:CGRectMake(20,28,margin_left,18)];
		countLabel.font=[UIFont systemFontOfSize:12];
		
		imageView=[[UIImageView alloc] initWithFrame:CGRectMake(margin_left,(frame.size.height-RESTAURANT_IMAGE_WIDTH)/2+6,RESTAURANT_IMAGE_WIDTH,RESTAURANT_IMAGE_WIDTH)];
		imageView.layer.cornerRadius=8;
		imageView.layer.masksToBounds=YES;

		[self addSubview:lineImage];
		[self addSubview:titleLabel];
		[self addSubview:imageView];
		[boxView addSubview:dateLabel];
		[boxView addSubview:countLabel];
		[boxView addSubview:dateImage];
		[boxView addSubview:peopleImage];
		[self addSubview:boxView];

		dateFormatter=[[NSDateFormatter alloc] init];
		


		[self addObserver:self forKeyPath:@"upcomingBook" options:NSKeyValueObservingOptionNew context:nil];
	}
	return self;
}

-(void)observeValueForKeyPath:(NSString*)keypath ofObject:(id)objc change:(NSDictionary*)change context:(void*)context{
	if([keypath isEqualToString:@"upcomingBook"]){
		IMGReservation *reservation=[change objectForKey:@"new"];
		titleLabel.text=reservation.restaurantName;
		CGSize size=[reservation.restaurantName sizeWithFont:titleLabel.font constrainedToSize:CGSizeMake(DeviceWidth-LEFTLEFTSET*2-RESTAURANT_IMAGE_WIDTH,1000) lineBreakMode:NSLineBreakByWordWrapping];
		titleLabel.frame=CGRectMake(0,10,size.width,size.height);
		boxView.frame=CGRectMake(0,titleLabel.endPointY+5,size.width,60);
		dateFormatter.dateFormat=@"yyyy-MM-dd";
		NSDate *bookDate=[dateFormatter dateFromString:reservation.bookDate];
		dateFormatter.dateStyle=NSDateFormatterMediumStyle;

		dateLabel.text=[NSString stringWithFormat:@"%@ %@",reservation.bookTime,[dateFormatter stringFromDate:bookDate]];
		countLabel.text=[NSString stringWithFormat:@"%@ people",reservation.party];
        __weak typeof(imageView) weakImageView = imageView;
        UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake(RESTAURANT_IMAGE_WIDTH, RESTAURANT_IMAGE_WIDTH)];
//		[imageView setImageWithURL:[NSURL URLWithString:[reservation.restaurantImage returnFullImageUrlWithWidth:RESTAURANT_IMAGE_WIDTH]] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//            image = [image imageByScalingAndCroppingForSize:CGSizeMake(RESTAURANT_IMAGE_WIDTH, RESTAURANT_IMAGE_WIDTH)];
//            
//            [weakImageView setImage:image];
//
//        }];
        [imageView sd_setImageWithURL:[NSURL URLWithString:[reservation.restaurantImage returnFullImageUrlWithWidth:RESTAURANT_IMAGE_WIDTH]] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            image = [image imageByScalingAndCroppingForSize:CGSizeMake(RESTAURANT_IMAGE_WIDTH, RESTAURANT_IMAGE_WIDTH)];
            [weakImageView setImage:image];

        }];
	}
}

-(void)dealloc{
	[self removeObserver:self forKeyPath:@"upcomingBook" context:nil];
}

@end
