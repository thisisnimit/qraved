//
//  NewListView.h
//  Qraved
//
//  Created by System Administrator on 9/24/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NewListViewDelegate <NSObject>

-(void)changeCity;

@end

@interface NewListView : UIView

@property(nonatomic,strong) UITextField *field;
@property(nonatomic,strong) NSString *listName;
@property(nonatomic,strong) UILabel *cityLabel;
@property(weak,nonatomic) id<NewListViewDelegate> delegate;


-(instancetype)initWithFrame:(CGRect)frame withDelegate:(id<UITextFieldDelegate>)delegate;

@end
