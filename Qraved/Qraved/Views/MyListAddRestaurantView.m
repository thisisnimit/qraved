//
//  MyListAddRestaurantView.m
//  Qraved
//
//  Created by System Administrator on 9/24/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "MyListAddRestaurantView.h"
#import "MyListAddRestaurantCell.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "Amplitude.h"
@implementation MyListAddRestaurantView


-(instancetype)initWithFrame:(CGRect)frame andList:(NSNumber*)listId{
	if(self=[super initWithFrame:frame]){

		float width=frame.size.width;
		_listId=listId;
        
        
		_searchBar=[[UISearchBar alloc] initWithFrame:CGRectMake(0,10,width,44)];
		_searchBar.placeholder= L(@"Search restaurants by name");
		_searchBar.delegate=self;
		_searchBar.returnKeyType=UIReturnKeyDone;

        if ([UIDevice isLaterThanIos6]) {
            self.searchBar.searchBarStyle = UISearchBarStyleMinimal;
        }else{
            for(UIView *subview in self.searchBar.subviews){
                if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                    [subview removeFromSuperview];
                }
            }
        }

        self.lblViewed=[[UILabel alloc] initWithFrame:CGRectMake(0,_searchBar.endPointY+10,width,16)];
        self.lblViewed.text=L(@"Recently Viewed");
        self.lblViewed.font=[UIFont boldSystemFontOfSize:FONT_SIZE_14];
        self.lblViewed.textColor=[UIColor color333333];

        UIImageView *line2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.lblViewed.endPointY+5,width, 1)];
	    line2.image = [UIImage imageNamed:@"CutOffRule"];

        
	    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0,_searchBar.endPointY+32,width,frame.size.height-_searchBar.endPointY-32)];
	    _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
	    _tableView.delegate=self;
	    _tableView.dataSource=self;

	    [self addObserver:self forKeyPath:@"restaurants" options:NSKeyValueObservingOptionNew context:nil];

	    [self addSubview:_searchBar];
	    [self addSubview:self.lblViewed];
//	    [self addSubview:line1];
	    [self addSubview:line2];
	    [self addSubview:_tableView];
	}
    return self;
}


-(void)addRefreshView{
	self.refreshView=[[EGORefreshTableFooterView alloc] initWithFrame:CGRectMake(0,self.tableView.contentSize.height+20,self.tableView.frame.size.width,66)];
	self.refreshView.delegate=self;
	self.refreshView.backgroundColor=[UIColor whiteColor];
	[self.tableView addSubview:self.refreshView];
}

-(void)resetRefreshViewFrame{
	self.refreshView.frame=CGRectMake(0,self.tableView.contentSize.height+20,self.tableView.frame.size.width,66);
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexpath{
	return 64;
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section{
	return self.restaurants.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
	return 1;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexpath{
	MyListAddRestaurantCell *cell=[tableView dequeueReusableCellWithIdentifier:@"viewedcell"];
	[cell prepareForReuse];
	if(cell==nil){
		cell=[[MyListAddRestaurantCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"viewedcell"];
	}
//	cell.title=[[self.restaurants objectAtIndex:indexpath.row] objectForKey:@"title"];
//	cell.cuisine=[[self.restaurants objectAtIndex:indexpath.row] objectForKey:@"cuisine"];
	cell.inList=[[[self.restaurants objectAtIndex:indexpath.row] objectForKey:@"inList"] boolValue];
//    if (![[[self.restaurants objectAtIndex:indexpath.row] objectForKey:@"imageUrl"] isKindOfClass:[NSNull class]]) {
//        [cell setImageUrl:[[self.restaurants objectAtIndex:indexpath.row] objectForKey:@"imageUrl"]];
//    }else{
//        [cell setImageUrl:@""];
//    }
    NSLog(@"%@",self.restaurants);
    [cell setRestaurantWithDic:[self.restaurants objectAtIndex:indexpath.row]];
	return cell;
}

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexpath{
	NSMutableDictionary *restaurant=[[self.restaurants objectAtIndex:indexpath.row] mutableCopy];
	NSNumber *restaurantId=[restaurant objectForKey:@"restaurantId"];
	MyListAddRestaurantCell *cell=[tableView cellForRowAtIndexPath:indexpath];
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    if ([self.listName isEqualToString:@"Favorites"]) {
        [param setObject:@"Favorite" forKey:@"Type"];
    }else{
        [param setObject:@"Custom list" forKey:@"Type"];
    }
    [param setObject:@"List detail page" forKey:@"Location"];
    [param setObject:restaurantId forKey:@"Restaurant_ID"];
    [param setObject:self.listId forKey:@"List_ID"];
    
    if ([[restaurant objectForKey:@"inList"] intValue] == 1) {
        return;
    }
    if (cell.inList) {
        cell.inList = NO;
        [restaurant setObject:[NSNumber numberWithBool:NO] forKey:@"inList"];
        [self.restaurants replaceObjectAtIndex:indexpath.row withObject:restaurant];
    }else{
        cell.inList = YES;
        [restaurant setObject:[NSNumber numberWithInt:3] forKey:@"inList"];
        [self.restaurants replaceObjectAtIndex:indexpath.row withObject:restaurant];
    }
    
}

-(void)scrollViewDidScroll:(UITableView*)tableView{
	[self.refreshView egoRefreshScrollViewDidScroll:tableView];
    [self endEditing:YES];
}

- (void)scrollViewDidEndDragging:(UITableView*)tableView willDecelerate:(BOOL)decelerate{
	if(tableView.contentOffset.y>tableView.contentSize.height-tableView.frame.size.height+60){
		tableView.contentOffset=CGPointMake(0,tableView.contentSize.height-tableView.frame.size.height+66);
		[self resetRefreshViewFrame];
		[self.refreshView egoRefreshScrollViewDidEndDragging:tableView];
	}
}

-(void)egoRefreshTableFooterDidTriggerRefresh:(EGORefreshTableFooterView*)refreshView{
	if([self.delegate respondsToSelector:@selector(loadRestaurantData:block:)]){
		__block __weak EGORefreshTableFooterView *weakRefreshView=refreshView;
		[self.delegate loadRestaurantData:self.searchBar.text block:^(BOOL _hasData){
            if(!_hasData){
                self.refreshView.hidden=YES;
            }
			[weakRefreshView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
			self.refreshView.frame=CGRectMake(0,self.tableView.contentSize.height,self.tableView.frame.size.width,66);
		}];
	}
}

-(void)observeValueForKeyPath:(NSString*)keyPath ofObject:object change:(NSDictionary *)change context:(void*)context{
	if([keyPath isEqualToString:@"restaurants"]){
		[self.tableView reloadData];
	}
    // 修改放大镜Frame前, 移除观察者
    [_imgV removeObserver:self forKeyPath:@"frame"];
    // 修改Frame
    _imgV.frame = CGRectMake(8, 7.5, 13, 13);
    // 再次添加观察者
    [_imgV addObserver:self forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];

}

-(BOOL)textFieldShouldReturn:(UITextField*)field{
	[field resignFirstResponder];
	return NO;
}

-(void)dealloc{
	[self removeObserver:self forKeyPath:@"restaurants"];
    // 移除观察者
    [_imgV removeObserver:self forKeyPath:@"frame"];
}
#pragma mark -UISearchBarDelegate代理方法
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    if ([searchBar.text isEqualToString:searchBar.placeholder]) {
        // 无文本时, 显示placeholder
        searchBar.text = @"";
    }
    // 获取到UISearchBar中UITextField
    UITextField *searchField = [searchBar valueForKey:@"_searchField"];
    // 开始编辑要修改textColor颜色
    searchField.textColor = [UIColor blackColor];
    searchField.clearButtonMode = UITextFieldViewModeWhileEditing;
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self updateSeachBar];
}
#pragma mark -根据文本内容设置searchBar颜色及clearButtonMode
- (void)updateSeachBar
{
    if ([_searchBar.text isEqualToString:@""]) {// 文本内容为空时
        UITextField *searchField = [_searchBar valueForKey:@"_searchField"];
        // 修改textColor为placeholderColor
        searchField.textColor = [searchField valueForKeyPath:@"_placeholderLabel.textColor"];
        searchField.text = searchField.placeholder;
        // 去除右侧clearButton
        searchField.clearButtonMode = UITextFieldViewModeNever;
    }
}

@end
