//
//  MyListRestaurantsViewCell.m
//  Qraved
//
//  Created by System Administrator on 10/14/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "MyListRestaurantsViewCell.h"
#import "UIConstants.h"
#import "DLStarRatingControl.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "UIImageView+Helper.h"
#import "UIImageView+WebCache.h"
#import "UILabel+Helper.h"
#import "Date.h"

#define RESTAURANT_IMAGE_SIZE 70
#define MORE_BUTTON_SIZE 10
#define CELL_RATING_STAR_WIDTH 10

@implementation MyListRestaurantsViewCell{
	
	UILabel *titleLabel;
	UIImageView *imageView;
	DLStarRatingControl *rateView;
	UILabel *reviewLabel;
	UIImageView *reviewImage;
	UIImageView *celllineImageView;
	UIImageView *lineImageView;
	UIImageView *shadowImage;
	UILabel *dateLabel;
	UILabel *distanceLabel;
	UILabel *placeholder;
	UIButton *moreBtn;
	UIView *reviewBox;
	UIButton *reviewBtn;
	UITextView *noteView;

	CGFloat margin_top;
}

@synthesize restaurantTitle,rate,reviewCount,ratingCount,saveDate,distance,image,note,cuisizeDistrictLabel,cuisizeDistrict;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier{
	if(self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]){
		CGFloat width=self.frame.size.width;
		margin_top=10;
		titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,margin_top,width-RESTAURANT_IMAGE_SIZE-MORE_BUTTON_SIZE,200)];
		titleLabel.numberOfLines=0;
		titleLabel.font=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:18];
        titleLabel.textColor=[UIColor color222222];
        titleLabel.backgroundColor=[UIColor clearColor];

		cuisizeDistrictLabel=[[UILabel alloc] init];
		cuisizeDistrictLabel.font=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
		cuisizeDistrictLabel.numberOfLines=2;
		cuisizeDistrictLabel.lineBreakMode=NSLineBreakByTruncatingTail;

		rateView=[[DLStarRatingControl alloc] initWithFrame:CGRectMake(0, 22, 74, 10) andStars:5 andStarWidth:CELL_RATING_STAR_WIDTH isFractional:YES spaceWidth:5];

		reviewLabel=[[UILabel alloc] init];
		[reviewLabel setTextAlignment:NSTextAlignmentRight];
        [reviewLabel setFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:10]];
        [reviewLabel setTextColor:[UIColor colorFFC000]];
        [reviewLabel setBackgroundColor:[UIColor clearColor]];

        reviewImage = [[UIImageView alloc] init];

        dateLabel=[[UILabel alloc] init];
        dateLabel.font = [UIFont systemFontOfSize:11];

        celllineImageView = [[UIImageView alloc] init];
        [celllineImageView setImage:[UIImage imageNamed:@"search_line"]];

        reviewBox=[[UIView alloc] init];
        reviewBox.userInteractionEnabled=YES;
        reviewBox.backgroundColor=[UIColor clearColor];
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:nil];
        [reviewBox addGestureRecognizer:tap];

        reviewBtn=[[UIButton alloc] initWithFrame:CGRectMake(LEFTLEFTSET,15,16,16)];
        [reviewBtn setImage:[UIImage imageNamed:@"review"] forState:UIControlStateNormal];
        [reviewBox addSubview:reviewBtn];

        noteView=[[UITextView alloc] initWithFrame:CGRectMake(reviewBtn.endPointX+12,8,DeviceWidth-LEFTLEFTSET*2-16,60)];
        noteView.delegate=self;
        noteView.returnKeyType=UIReturnKeyDone;
        noteView.scrollsToTop = NO;
        [reviewBox addSubview:noteView];

        placeholder=[[UILabel alloc] initWithFrame:CGRectMake(reviewBtn.endPointX+16,17,100,14)];
        placeholder.text=L(@"Write Note");
        placeholder.textColor=[UIColor colorCCCCCC];
        placeholder.font=[UIFont systemFontOfSize:14];
        
        [reviewBox addSubview:placeholder];

        shadowImage = [[UIImageView alloc] initShadowImageViewWithShadowOriginY_v2:70 andHeight:10];
		[reviewBox addSubview:shadowImage];

        imageView=[[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-LEFTLEFTSET-RESTAURANT_IMAGE_SIZE-MORE_BUTTON_SIZE,margin_top,RESTAURANT_IMAGE_SIZE,RESTAURANT_IMAGE_SIZE)];
		[imageView.layer setMasksToBounds:YES];
        imageView.layer.borderColor = [UIColor whiteColor].CGColor;
        imageView.layer.cornerRadius = 5;
        imageView.layer.borderWidth = 0.3;
        [imageView setAlpha:0.0];

        distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth-LEFTLEFTSET-45,imageView.endPointY+5, 45, 20)];
        [distanceLabel setFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:10]];
        [distanceLabel setBackgroundColor:[UIColor clearColor]];

        lineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(distanceLabel.frame.origin.x, distanceLabel.endPointY , 48, 2)];
        [lineImageView setImage:[UIImage imageNamed:@"PlottingScaleGray"]];

        moreBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        moreBtn.userInteractionEnabled=YES;
        moreBtn.frame = CGRectMake(DeviceWidth - 20, imageView.frame.origin.y, 20, RESTAURANT_IMAGE_SIZE);
        [moreBtn setImage:[UIImage imageNamed:@"listMoreVertical"] forState:UIControlStateNormal];
        [moreBtn addTarget:self action:@selector(moreBtnClick) forControlEvents:UIControlEventTouchUpInside];

		[self addSubview:titleLabel];
		[self addSubview:imageView];
		[self addSubview:cuisizeDistrictLabel];
		[self addSubview:distanceLabel];
		[self addSubview:rateView];
		[self addSubview:reviewLabel];
		[self addSubview:reviewImage];
		[self addSubview:dateLabel];
		[self addSubview:lineImageView];
		[self addSubview:celllineImageView];
		[self addSubview:moreBtn];
        [self addSubview:reviewBox];

		[self addObserver:self  forKeyPath:@"restaurantTitle" options:NSKeyValueObservingOptionNew context:nil];
		[self addObserver:self  forKeyPath:@"cuisizeDistrict" options:NSKeyValueObservingOptionNew context:nil];
		[self addObserver:self  forKeyPath:@"rate" options:NSKeyValueObservingOptionNew context:nil];
		[self addObserver:self  forKeyPath:@"ratingCount" options:NSKeyValueObservingOptionNew context:nil];
		[self addObserver:self  forKeyPath:@"saveDate" options:NSKeyValueObservingOptionNew context:nil];
		[self addObserver:self  forKeyPath:@"distance" options:NSKeyValueObservingOptionNew context:nil];
		[self addObserver:self  forKeyPath:@"image" options:NSKeyValueObservingOptionNew context:nil];
		[self addObserver:self  forKeyPath:@"note" options:NSKeyValueObservingOptionNew context:nil];

		return  self;
	}
    return nil;
}

-(void)observeValueForKeyPath:(NSString*)keypath ofObject:(id)objc change:(NSDictionary*)change context:(void*)context{
	if([keypath isEqualToString:@"restaurantTitle"]){
		titleLabel.text=[change objectForKey:@"new"];
	}else if([keypath isEqualToString:@"rate"]){
		[rateView updateRating:[change objectForKey:@"new"]];
	}else if([keypath isEqualToString:@"distance"]){
        NSString *distanceStr=[NSString stringWithFormat:@"%@",[change objectForKey:@"new"]];
        distanceLabel.text = distanceStr;
        lineImageView.hidden = [@"" isEqualToString:distanceStr];
	}else if([keypath isEqualToString:@"ratingCount"]){
        reviewLabel.text=[change objectForKey:@"new"]==nil?@"0":[NSString stringWithFormat:@"%@",[change objectForKey:@"new"]];
		[reviewImage setImage:[UIImage imageNamed:@"reviewCount"]];
        if ([[change objectForKey:@"new"] isEqual:@0]) {
            reviewLabel.hidden=YES;
            reviewImage.hidden=YES;
        }else{
            reviewLabel.hidden=NO;
            reviewImage.hidden=NO;
        
        
        }
	}else if([keypath isEqualToString:@"saveDate"]){
		dateLabel.text=[Date getTimeInteval:[NSString stringWithFormat:@"%@",[change objectForKey:@"new"]]];
	}else if([keypath isEqualToString:@"cuisizeDistrict"]){
		cuisizeDistrictLabel.text=[change objectForKey:@"new"];
	}else if([keypath isEqualToString:@"note"]){
		noteView.tag=self.tag;
		NSString *noteText=[change objectForKey:@"new"];
		if(noteText!=nil && noteText.length){
			noteView.text=noteText;
			placeholder.hidden=YES;
		}else{
			noteView.text=@"";
			placeholder.hidden=NO;
		}
	}else if([keypath isEqualToString:@"image"]){
		NSString *urlString = [[NSString stringWithFormat:@"%@",[change objectForKey:@"new"]] returnFullImageUrlWithWidth:RESTAURANT_IMAGE_SIZE];
	    __weak typeof(imageView) weakImageView = imageView;
//	    [weakImageView setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//	        [UIView animateWithDuration:1.0 animations:^{
//	            [weakImageView setAlpha:1.0];
//	        }];
//	    }];
        [weakImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [UIView animateWithDuration:1.0 animations:^{
                	            [weakImageView setAlpha:1.0];
                	        }];
        }];
	}
	CGFloat leftWidth=DeviceWidth-LEFTLEFTSET-RESTAURANT_IMAGE_SIZE-MORE_BUTTON_SIZE;
	CGSize size = [titleLabel.text sizeWithFont:titleLabel.font constrainedToSize:CGSizeMake(self.frame.size.width-LEFTLEFTSET*2-RESTAURANT_IMAGE_SIZE-MORE_BUTTON_SIZE, 10000) lineBreakMode:NSLineBreakByWordWrapping];
    titleLabel.frame = CGRectMake(LEFTLEFTSET,margin_top, size.width, size.height);
    size=[cuisizeDistrictLabel.text sizeWithFont:cuisizeDistrictLabel.font constrainedToSize:CGSizeMake(leftWidth-LEFTLEFTSET,cuisizeDistrictLabel.font.lineHeight*2) lineBreakMode:NSLineBreakByTruncatingTail];
    cuisizeDistrictLabel.frame=CGRectMake(LEFTLEFTSET,titleLabel.endPointY+5,size.width,size.height);
    rateView.frame=CGRectMake(LEFTLEFTSET,cuisizeDistrictLabel.endPointY+2,74,10);
    reviewLabel.frame=CGRectMake(rateView.endPointX+11,rateView.frame.origin.y,18,CELL_RATING_STAR_WIDTH);
    reviewImage.frame=CGRectMake(reviewLabel.endPointX+3,rateView.frame.origin.y+2,8,8);
    dateLabel.frame=CGRectMake(LEFTLEFTSET,rateView.endPointY+8,dateLabel.expectedWidth,dateLabel.font.lineHeight);
    if(dateLabel.endPointY>lineImageView.endPointY){
    	celllineImageView.frame=CGRectMake(0, dateLabel.endPointY+5 , DeviceWidth, 2);	
    }else{
    	celllineImageView.frame=CGRectMake(0, lineImageView.endPointY+5 , DeviceWidth, 2);
    }
    reviewBox.frame=CGRectMake(0,celllineImageView.endPointY,DeviceWidth,78);

    if (self.isOtherUser)
    {
        [moreBtn setHidden:YES];
        [reviewBox setHidden:YES];
    }
}

-(void)moreBtnClick{
    if(self.delegate && [self.delegate respondsToSelector:@selector(moreBtnClick:)]){
		[self.delegate moreBtnClick:self.restaurant];
	}
}

-(void)textViewDidChange:(UITextView*)textView{
    if(textView.text.length){
        placeholder.hidden=YES;
    }else{
        placeholder.hidden=NO;
    }
}

-(BOOL)textView:(UITextView*)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text{
    if([text isEqualToString:@"\n"]){
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

-(void)textViewDidEndEditing:(UITextView*)textView{
	if(textView.text.length){
		NSInteger row=textView.tag;
        if(self.delegate && [self.delegate respondsToSelector:(@selector(writeNote:withRestaurant:atIndexPath:))]){
            NSIndexPath *indexpath=[NSIndexPath indexPathForRow:row inSection:0];
            [self.delegate writeNote:textView.text withRestaurant:self.restaurant atIndexPath:indexpath];
        }
	}
}

-(void)dealloc{
	[self removeObserver:self forKeyPath:@"restaurantTitle"];
	[self removeObserver:self forKeyPath:@"cuisizeDistrict"];
	[self removeObserver:self forKeyPath:@"rate"];
	[self removeObserver:self forKeyPath:@"ratingCount"];
	[self removeObserver:self forKeyPath:@"saveDate"];
	[self removeObserver:self forKeyPath:@"distance"];
	[self removeObserver:self forKeyPath:@"image"];
	[self removeObserver:self forKeyPath:@"note"];
}

@end
