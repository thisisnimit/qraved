//
//  RestaurantMapCell.h
//  Qraved
//
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"

@interface RestaurantMapCell : UIView

-(instancetype)initWithFrame:(CGRect)frame;
-(void)setRestaurant:(IMGRestaurant*)restaurantLocal andNumber:(NSInteger)numuber;

@end
