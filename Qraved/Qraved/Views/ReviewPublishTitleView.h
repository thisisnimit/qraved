//
//  ReviewPublishTitleView.h
//  Qraved
//
//  Created by apple on 17/4/1.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"

@interface ReviewPublishTitleView : UIView

-(instancetype)initWith:(IMGRestaurant*)restaurant;
@property (nonatomic, strong) IMGRestaurant *restaurant;
@property (nonatomic, assign) BOOL isSearch;
@property (nonatomic, copy) void(^gotoSearchRestaurant)();

@end
