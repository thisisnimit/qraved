//
//  ReviewPublishAddToListView.h
//  Qraved
//
//  Created by harry on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewPublishAddToListView : UIView

@property (nonatomic, strong)IMGRestaurant *currentRestaurant;

@property (nonatomic, copy) void (^addToListClick)(IMGRestaurant *addRestaurant);


@end
