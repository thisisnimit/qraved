//
//  LikeListTableViewCell.m
//  Qraved
//
//  Created by imaginato on 16/4/21.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "LikeListTableViewCell.h"
#import "UIView+Helper.h"
#import "UIConstants.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"
#import "Date.h"
#import "UIColor+Helper.h"
#import "IMGLikeList.h"
#define LEFTMARGIN 8
#define RIGHTMARGIN 30
#define PROFILEIMAGEVIEWSIZE 20
@implementation LikeListTableViewCell

{


    UIImageView *profileImageView;
    UILabel *userNameLabel;
    UILabel *timeLabel;
    UILabel *contentLabel;
    IMGLikeList* likeListObjict;
    UIControl *userNameLabelControl;
    UILabel* reviewsAndPhotos;

}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
    
    if(self=[super initWithStyle:style reuseIdentifier:identifier]){
            
            self.selectionStyle=UITableViewCellSelectionStyleNone;
            
            self.backgroundColor = [UIColor whiteColor];
            self.selectionStyle = UITableViewCellSelectionStyleNone;
            profileImageView = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTMARGIN, 28/2, 74/2, 74/2)];
            profileImageView.layer.cornerRadius=74/4;
            profileImageView.layer.borderColor=[UIColor clearColor].CGColor;
            profileImageView.layer.borderWidth=0.0f;
            profileImageView.clipsToBounds=YES;
            [self addSubview:profileImageView];
            
            UIControl *profileImageViewControl = [[UIControl alloc] init];
            [profileImageViewControl addTarget:self action:@selector(controlClick) forControlEvents:UIControlEventTouchUpInside];
            profileImageViewControl.frame = profileImageView.frame;
            [self addSubview:profileImageViewControl];
            
            
            userNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(profileImageView.endPointX+31/2, profileImageView.startPointY, DeviceWidth-profileImageView.endPointY-RIGHTMARGIN, 22)];
            userNameLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:14];
            userNameLabel.textColor=[[UIColor darkGrayColor] colorWithAlphaComponent:0.9];
            [self addSubview:userNameLabel];
            
            userNameLabelControl = [[UIControl alloc] init];
            [userNameLabelControl addTarget:self action:@selector(controlClick) forControlEvents:UIControlEventTouchUpInside];
            userNameLabelControl.frame = userNameLabel.frame;
            [self addSubview:userNameLabelControl];
            
            reviewsAndPhotos = [[UILabel alloc]initWithFrame:CGRectMake(userNameLabel.startPointX, userNameLabel.endPointY+2, userNameLabel.frame.size.width, 16)];
            reviewsAndPhotos.font=[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:11];
            reviewsAndPhotos.textColor=[[UIColor lightGrayColor] colorWithAlphaComponent:0.8];
            [self addSubview:reviewsAndPhotos];
            
            
       
        
    }
    return self;
}
-(void)setList:(IMGLikeList *)list
{


    _list=list;
    NSString *userName=@"";
    NSString *imageUrl=_list.imageUrl;
    NSString* reviewAndPhots=@"";
    likeListObjict=_list;
    userName = [likeListObjict.fullName filterHtml];
    
    
    if ([_list.reviewCount intValue]!=0&&[_list.photoCount intValue]!=0) {
        reviewAndPhots=[NSString stringWithFormat:@"%@ %@ - %@ %@",_list.reviewCount,[_list.reviewCount intValue] > 1 ? @"reviews":@"review",_list.photoCount,[_list.photoCount intValue] > 1 ? @"photos":@"photo"];
    }else
    {
    
        if ([_list.reviewCount intValue]==0&&[_list.photoCount intValue]!=0) {
            reviewAndPhots=[NSString stringWithFormat:@"%@ %@",_list.photoCount,[_list.photoCount intValue] > 1 ? @"photos":@"photo"];
        }
        if ([_list.photoCount intValue]==0&&[_list.reviewCount intValue]!=0) {
            reviewAndPhots=[NSString stringWithFormat:@"%@ %@",_list.reviewCount,[_list.reviewCount intValue] > 1 ? @"reviews":@"review"];
        }
    
    
    
    }
    userNameLabel.textColor = [UIColor color333333];
    NSURL *avatar;
    if ([imageUrl isEqualToString:DEFAULTPROFILEIMAGEURL]) {
        avatar = [NSURL URLWithString:@""];
    }else if ([imageUrl hasPrefix:@"http"]){
        avatar = [NSURL URLWithString:imageUrl];
    }else{
        avatar = [NSURL URLWithString:[imageUrl returnFullImageUrl]];
    }
    [profileImageView sd_setImageWithURL:avatar placeholderImage:[UIImage imageNamed:@"headDefault.jpg"]];
    
    userNameLabel.text=userName;
    reviewsAndPhotos.text=reviewAndPhots;
    


}
- (void)controlClick
{
    
    if (self.delegate)
    {
        
            IMGUser *user = [[IMGUser alloc] init];
            user.userName = [NSString stringWithString:likeListObjict.fullName];
            user.userId = likeListObjict.userId;
       
       user.avatar=_list.imageUrl;
        user.token = @"";
            [self.delegate likeListTableViewCellUserNameOrImageTapped:user];
        }
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
