//
//  LikeButton.h
//  Qraved
//
//  Created by Jeff on 3/3/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICommunityButton.h"

@interface UILikeButton : UICommunityButton

@property (nonatomic, assign) BOOL isFromPhotoViewer;
-(id)initInSuperView:(UIView*)parentView liked:(BOOL)liked_;
-(id)initInSuperView:(UIView*)parentView liked:(BOOL)isLike likeCount:(int)likeCount isFromPhotoViewer:(BOOL)isFromPhotoViewer;
-(void)updateLikedStatus;
@end
