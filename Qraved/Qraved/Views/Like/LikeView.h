//
//  LikeView.h
//  Qraved
//
//  Created by imaginato on 16/4/20.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LikeListTableViewCell.h"
#import "PullTableView.h"
@protocol LikeListViewDelegate
@optional

- (void)LikeListViewUserNameOrImageTapped:(id)sender;
-(void)LikeviewLoadMoreData:(NSInteger)offset;
-(void)returnLikeCount:(NSInteger)count;

@end


@interface LikeView : UIView<UITableViewDataSource,UITableViewDelegate,LikeListTableViewCellDelegate,PullTableViewDelegate>
@property(nonatomic,assign) id<LikeListViewDelegate>delegate;
@property(nonatomic,retain)NSArray* likelistArray;
@property(nonatomic,retain)PullTableView* likeUserTableView;
@property(nonatomic,retain)NSNumber* likecount;
@end
