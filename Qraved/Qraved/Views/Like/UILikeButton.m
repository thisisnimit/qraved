//
//  LikeButton.m
//  Qraved
//
//  Created by Jeff on 3/3/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "UILikeButton.h"
#import "UIView+Helper.h"

#define LIKEBUTTONTAPPEDTITLECOLOR       [UIColor colorWithRed:234/255.0f green:98/255.0f blue:98/255.0f alpha:1]

#define LIKEBUTTONTAPPEDICON   @"like_red1.png"
#define LIKEBUTTONUNTAPPEDICON   @"ic_like.png"

@implementation UILikeButton{
    BOOL liked;
}
-(id)initInSuperView:(UIView*)parentView liked:(BOOL)liked_{
    self = [super initInSuperView:parentView tag:0];
    if (self) {
        liked = liked_;
        [self.textLabel setText:L(@"Like")];
        [self.textLabel sizeToFit];
        self.iconView = [[UIImageView alloc]initWithFrame:CGRectMake((self.frame.size.width-COMMUNITYBUTTONICONSIZE-COMMUNITYBUTTONGAP-self.textLabel.frame.size.width)/2, (self.frame.size.height-COMMUNITYBUTTONICONSIZE)/2, COMMUNITYBUTTONICONSIZE, COMMUNITYBUTTONICONSIZE)];
        [self addSubview:self.iconView];
        self.textLabel.frame=CGRectMake(self.iconView.endPointX+COMMUNITYBUTTONGAP, (self.frame.size.height-self.textLabel.frame.size.height)/2, self.textLabel.frame.size.width, self.textLabel.frame.size.height);
        
        if(liked==0){
            [self.textLabel setTextColor:COMMUNITYBUTTONUNTAPPEDTITLECOLOR];
            self.iconView.image=[UIImage imageNamed:LIKEBUTTONUNTAPPEDICON];
        }else{
            [self.textLabel setTextColor:LIKEBUTTONTAPPEDTITLECOLOR];
            self.iconView.image=[UIImage imageNamed:LIKEBUTTONTAPPEDICON];
        }
        
        
    }
    return self;
}

-(id)initInSuperView:(UIView*)parentView liked:(BOOL)isLike likeCount:(int)likeCount isFromPhotoViewer:(BOOL)isFromPhotoViewer{
    self = [super initInSuperView:parentView tag:0];
    if (self) {
        liked = isLike;
        self.isFromPhotoViewer = isFromPhotoViewer;
        //[self.textLabel setText:L(@"Like")];
        
        if (likeCount>1) {
            [self.textLabel setText:[NSString stringWithFormat:@"%d Likes",likeCount]];
//            [likeButton setTitle:[NSString stringWithFormat:@"%@ Likes",likeCount] forState:UIControlStateNormal];
        }else if(likeCount==1){
            [self.textLabel setText:[NSString stringWithFormat:@"%d Like",likeCount]];
//            [likeButton setTitle:[NSString stringWithFormat:@"%@ Like",likeCount] forState:UIControlStateNormal];
        }else{
            [self.textLabel setText:L(@"Like")];
            //[likeButton setTitle:@"Like" forState:UIControlStateNormal];
        }

        [self.textLabel sizeToFit];
        self.iconView = [[UIImageView alloc]initWithFrame:CGRectMake((self.frame.size.width-COMMUNITYBUTTONICONSIZE-COMMUNITYBUTTONGAP-self.textLabel.frame.size.width)/2, (self.frame.size.height-COMMUNITYBUTTONICONSIZE)/2, COMMUNITYBUTTONICONSIZE, COMMUNITYBUTTONICONSIZE)];
        [self addSubview:self.iconView];
        self.textLabel.frame=CGRectMake(self.iconView.endPointX+COMMUNITYBUTTONGAP, (self.frame.size.height-self.textLabel.frame.size.height)/2, self.textLabel.frame.size.width, self.textLabel.frame.size.height);
        
        if(liked==0){
            [self.textLabel setTextColor:isFromPhotoViewer?[UIColor whiteColor]:COMMUNITYBUTTONUNTAPPEDTITLECOLOR];
            self.iconView.image=[UIImage imageNamed:isFromPhotoViewer?@"ic_like_white":LIKEBUTTONUNTAPPEDICON];
        }else{
            [self.textLabel setTextColor:LIKEBUTTONTAPPEDTITLECOLOR];
            self.iconView.image=[UIImage imageNamed:LIKEBUTTONTAPPEDICON];
        }
        
        
    }
    return self;
}

-(void)updateLikedStatus{
    liked=!liked;
    if(liked==NO){
        [self.textLabel setTextColor:self.isFromPhotoViewer?[UIColor whiteColor]:COMMUNITYBUTTONUNTAPPEDTITLECOLOR];
        self.iconView.image=[UIImage imageNamed:self.isFromPhotoViewer?@"ic_like_white":LIKEBUTTONUNTAPPEDICON];
    }else{
        [self.textLabel setTextColor:LIKEBUTTONTAPPEDTITLECOLOR];
        self.iconView.image=[UIImage imageNamed:LIKEBUTTONTAPPEDICON];
    }
}


@end
