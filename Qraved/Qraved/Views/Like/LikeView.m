//
//  LikeView.m
//  Qraved
//
//  Created by imaginato on 16/4/20.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "LikeView.h"
#import "UIView+Helper.h"
#import "OtherProfileViewController.h"
#import "LikeListTableViewCell.h"
#import "UIColor+Helper.h"
#import "LoadingView.h"
#import "IMGLikeList.h"
#import "PullTableView.h"
@implementation LikeView
{
    UILabel* likeNum;
    UILabel* like;
    UIView* footview;
    UIActivityIndicatorView* activeIndicarir;
    BOOL isHadNextData;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(instancetype)initWithFrame:(CGRect)frame
{


    if ([super initWithFrame:frame]) {
        isHadNextData=YES;
        [self creatGlobelView];
      
    }

    return self;
}
-(void)creatGlobelView
{

    UIView* BottomshadowView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    BottomshadowView.layer.borderWidth=0;
    BottomshadowView.layer.borderColor=[UIColor clearColor].CGColor;
    BottomshadowView.userInteractionEnabled=YES;
    UITapGestureRecognizer* tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelBtnAct)];
    [BottomshadowView addGestureRecognizer:tap];
    BottomshadowView.backgroundColor=[UIColor blackColor];
    BottomshadowView.alpha=0.3f;
    [self addSubview:BottomshadowView];

    UIView* topView=[[UIView alloc]initWithFrame:CGRectMake(15/2, (DeviceHeight)*1/11+28, self.bounds.size.width-15, 40)];
    topView.backgroundColor=[UIColor colorWithRed:246/255.0 green:247/255.0 blue:248/255.0 alpha:1];
    [self addSubview:topView];
    topView.layer.masksToBounds=YES;
    topView.layer.cornerRadius=4;
    topView.clipsToBounds=YES;
    topView.layer.borderWidth=1;
    topView.layer.borderColor=[UIColor colorWithRed:236/255.0 green:237/255.0 blue:240/255.0 alpha:1].CGColor;
    
    UILabel* tittleLable=[[UILabel alloc]initWithFrame:CGRectMake(15/2, 5, topView.bounds.size.width-50, 30)];
    tittleLable.backgroundColor=[UIColor clearColor];
    tittleLable.font=[UIFont systemFontOfSize:16];
        tittleLable.text=@"People who liked this ";
    [topView addSubview:tittleLable];
    
    UIButton* cancel=[[UIButton alloc]initWithFrame:CGRectMake(self.bounds.size.width-46, 18/2, 24, 24)];
    cancel.layer.zPosition=1000;
    [cancel setImage:[UIImage imageNamed:@"ClosedPopUp"] forState:UIControlStateNormal];
    [cancel addTarget:self action:@selector(cancelBtnAct) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:cancel];
    UIView* likeNumView=[[UIView alloc]initWithFrame:CGRectMake(15/2, topView.endPointY, self.frame.size.width-15, 34)];
    likeNumView.backgroundColor=[UIColor whiteColor];
    [self addSubview:likeNumView];
    
    likeNum=[[UILabel alloc]initWithFrame:CGRectMake(7.5, 12, 25, 18)];
    likeNum.backgroundColor=[UIColor whiteColor];
    likeNum.font=[UIFont boldSystemFontOfSize:16];
    likeNum.textAlignment=NSTextAlignmentCenter;
    [likeNumView addSubview:likeNum];
    like=[[UILabel alloc]initWithFrame:CGRectMake(likeNum.endPointX+2, 12, 100, 18)];
    like.backgroundColor=[UIColor clearColor];
    like.textAlignment=NSTextAlignmentLeft;
    like.font=[UIFont systemFontOfSize:15];
    [likeNumView addSubview:like];
    UIView* bottomCorner=[[UIView alloc]initWithFrame:CGRectMake(15/2, likeNumView.endPointY+10, self.frame.size.width-15, self.bounds.size.height*2/5+30)];
    bottomCorner.backgroundColor=[UIColor whiteColor];
    bottomCorner.layer.cornerRadius=4;
    [self addSubview:bottomCorner];
    _likeUserTableView=[[PullTableView alloc]initWithFrame:CGRectMake(15/2, likeNumView.endPointY, self.frame.size.width-15, self.bounds.size.height*2/5) ];
       _likeUserTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
   [_likeUserTableView setLoadMoreViewWithBackGroundColor:[UIColor clearColor]];
       _likeUserTableView.delegate=self;
    _likeUserTableView.dataSource=self;
    _likeUserTableView.pullDelegate = self;
    _likeUserTableView.isNotLoadLikeList=NO;

    footview=[[UIView alloc]initWithFrame:CGRectMake(0, 0, _likeUserTableView.frame.size.width, 30)];
  //  _likeUserTableView.tableFooterView=footview;
       [self addSubview:_likeUserTableView];

    UIView *leftLine = [[UIView alloc]initWithFrame:CGRectMake(15/2, topView.startPointY+3, 1, self.bounds.size.height*2/5-7)];
    leftLine.backgroundColor=[UIColor colorWithRed:246/255.0 green:247/255.0 blue:248/255.0 alpha:1];
       [self addSubview:leftLine];
    
    UIView *rightLine = [[UIView alloc]initWithFrame:CGRectMake(_likeUserTableView.endPointX-1, topView.startPointY+3, 1, self.bounds.size.height*2/5-7)];
    rightLine.backgroundColor=[UIColor colorWithRed:246/255.0 green:247/255.0 blue:248/255.0 alpha:1];
    [self addSubview:rightLine];
    UIView *bottomLine = [[UIView alloc]initWithFrame:CGRectMake(15/2+5, bottomCorner.endPointY, bottomCorner.bounds.size.width-8, 0.6)];
    bottomLine.backgroundColor=[UIColor colorWithRed:246/255.0 green:247/255.0 blue:248/255.0 alpha:1];
   // [self addSubview:bottomLine];
    activeIndicarir=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activeIndicarir.frame=CGRectMake(_likeUserTableView.bounds.size.width/2-15, 5, 20, 20);
  // [_likeUserTableView.tableFooterView addSubview:activeIndicarir];
    

}

-(void)setLikelistArray:(NSArray *)likelistArray
{

    _likelistArray=likelistArray;
    
      [_likeUserTableView reloadData];



}
-(void)setLikecount:(NSNumber *)likecount
{
    _likecount=likecount;
    _likeUserTableView.pullTableIsLoadingMore = NO;
    likeNum.text=[NSString stringWithFormat:@"%d",[_likecount intValue]];

     like.text=[NSString stringWithFormat:@"%@",[_likecount intValue]  > 1 ? @"Likes":@"Like"];
   
}
-(void)cancelBtnAct
{

    [self removeFromSuperview];
    if (self.delegate) {
        [self.delegate returnLikeCount:[_likecount intValue]];
    }

}
#pragma TabelViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _likelistArray.count;

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.row>=_likelistArray.count)
//    {
//        if (_likelistArray.count!=0) {
//            
//             return [self loadingCell];
//        }
//       
//
//        
//    }
//
        LikeListTableViewCell *likeListTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"CellID"];
    if (likeListTableViewCell == nil) {
        likeListTableViewCell = [[LikeListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellID"];
        
    }
    
   // [likeListTableViewCell updateLikeUI:_likelistArray];
    likeListTableViewCell.list=_likelistArray[indexPath.row];
    likeListTableViewCell.delegate = self;
    //    [UILineImageView initInParent:commentCell withFrame:CGRectMake(0, popReviewTableViewCell.startPointY, DeviceWidth, 1)];

    return likeListTableViewCell;
       
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    return 55;

}
-(void)loadData

{
    
    if (_likelistArray.count<[_likecount intValue]) {
      
    IMGLikeList* likeObjict=[_likelistArray lastObject];
    
    [self.delegate LikeviewLoadMoreData:[likeObjict.likeId floatValue] ];


    }else{
    
        _likeUserTableView.pullTableIsLoadingMore=NO ;
    
    
    }


}
- (void)pullTableViewDidTriggerLoadMore:(PullTableView *)pullTableView
{
    
    [self performSelector:@selector(loadData) withObject:nil afterDelay:1.0f];
    
}
//-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
//{
//
//    CGFloat offSetY=scrollView.contentOffset.y;
//    CGFloat contentSizeHeight=scrollView.contentSize.height;
//    if (_likelistArray.count<[_likecount floatValue]) {
//        
//      if (offSetY>contentSizeHeight-(self.bounds.size.height/2-50)+40) {
//
//        
//        if (self.delegate) {
//            [activeIndicarir startAnimating];
//
//
//            IMGLikeList* likeObjict=[_likelistArray lastObject];
//        
//            [self.delegate LikeviewLoadMoreData:[likeObjict.likeId floatValue] ];
//        }
//        
//        
//    }
//
//    }
// 
//}
-(void)likeListTableViewCellUserNameOrImageTapped:(id)sender
{


    IMGUser *user = (IMGUser *)sender;
    OtherProfileViewController *opvc = [[OtherProfileViewController alloc] init];
    opvc.otherUser = user;

    IMGUser *currentUser = [IMGUser currentUser];
    if ([currentUser.userId intValue] == [user.userId intValue])
    {
        opvc.isOtherUser = NO;
    }else
    {
        opvc.isOtherUser = YES;
    }
    
//响应者连遍历，找到对应的导航控制器
    UIResponder *responder = [self nextResponder];
    while (responder)
        
    {
        
        if ([responder isKindOfClass:[UINavigationController class]])
            
        {
                     UINavigationController* vc=(UINavigationController*)responder;
           [vc pushViewController:opvc animated:YES];

            //[vc presentViewController:opvc animated:NO completion:nil];
            return;
            
        }
        
        responder = [responder nextResponder];
        
    }
    
    
    
    
    
  }
//- (UITableViewCell *)loadingCell {
//    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
//                                                   reuseIdentifier:nil] ;
//    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//    
//    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]
//                                                  initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//    activityIndicator.frame = CGRectMake((self.bounds.size.width-15)/2, 5, 20, 20);
//    
//    [cell addSubview:activityIndicator];
//       
//    [activityIndicator startAnimating];
//    
//    cell.tag = 100;
//    
//    return cell;
//}
//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (_likelistArray.count<[_likecount intValue]) {
//
//    if (cell.tag == 100) {
//        
//        if (isHadNextData)
//        {
//            isHadNextData =NO;
//            
//            IMGLikeList* likeObjict=[_likelistArray lastObject];
//            
//            [self.delegate LikeviewLoadMoreData:[likeObjict.likeId floatValue] ];
//        }
//        
//        
//    }
//    }
//}
//

@end
