//
//  LikeListTableViewCell.h
//  Qraved
//
//  Created by imaginato on 16/4/21.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGJournalComment.h"
#import "IMGDishComment.h"
#import "IMGReviewComment.h"
#import "IMGRestaurantEventComment.h"
#import "IMGLikeList.h"
@protocol LikeListTableViewCellDelegate <NSObject>

- (void)likeListTableViewCellUserNameOrImageTapped:(id)sender;

@end
@interface LikeListTableViewCell : UITableViewCell
@property (nonatomic, assign) id<LikeListTableViewCellDelegate>delegate;
@property(nonatomic,retain)IMGLikeList* list;



@end
