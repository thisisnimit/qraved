//
//  CuisineAndDistrict.h
//  Qraved
//
//  Created by carl on 16/5/26.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CuisineAndDistrictDelegate <NSObject>
-(void)cuisineClick:(UITapGestureRecognizer*)tap;
-(void)districtClick:(UITapGestureRecognizer*)tap;
@end

@interface CuisineAndDistrict : UILabel

@property (nonatomic,strong) NSMutableArray *cRects;
@property (nonatomic,strong) NSMutableArray *dRects;
@property (nonatomic,strong) UIView *parentView;
@property(nonatomic,weak) id<CuisineAndDistrictDelegate> delegate;

@end
