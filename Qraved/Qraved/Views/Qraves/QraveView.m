//
//  QraveView.m
//  Qraved
//
//  Created by Admin on 8/9/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#define BigQraveViewTag 111

#import "QraveView.h"
#import "Consts.h"
#import "QraveViewController.h"
#import "Restaurant.h"
#import "CommentsViewController.h"
#import "DetailViewController.h"
#import "AppDelegate.h"
#import "Tools.h"
#import "LoadingView.h"
#import "UIImage+Resize.h"
@implementation QraveView

@synthesize infoDic;
@synthesize delegate;

#define imageWidth 305.0/2 
//#define imageHeight 305.0/2

#define exceptImageHeight 80.5


- (id)initWithFrame:(CGRect)frame andInfoDic:(NSDictionary *)dic
{
    self = [super initWithFrame:frame];
    if (self) {
        NSLog(@"Rect ====== %@", NSStringFromCGRect(frame));
        self.infoDic = dic;
        
        float imageHeight = self.frame.size.height - exceptImageHeight;
        
        self.backgroundColor = [UIColor whiteColor];
        UIButton *imgBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        imgBtn.frame = CGRectMake(1, 1, imageWidth-2, imageHeight-2);
        [imgBtn setBackgroundImage:[UIImage imageNamed:@"placeholder.png"] forState:UIControlStateNormal];
        [imgBtn setBackgroundImage:[UIImage imageNamed:@"placeholder.png"] forState:UIControlStateHighlighted];
        [imgBtn addTarget:self action:@selector(imgBtnTouched:) forControlEvents:UIControlEventTouchDown];
        [self addSubview:imgBtn];
    
        NSString *lastImgUrlStr = [dic objectForKey:@"imageUrl"];
        NSString *imgUrlStr = [NSString stringWithFormat:@"%@%@",QRAVED_WEB_IMAGE_SERVER,lastImgUrlStr];
        NSURL *url = [NSURL URLWithString:imgUrlStr];
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            NSData *data = [NSData dataWithContentsOfURL:url];
            if (data != nil) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIImage *img = [UIImage imageWithData:data];
//                    imgView.image = img;
                    img = [img resetImageSizeFromOImage];
                    [imgBtn setImage:img forState:UIControlStateNormal];
                    [imgBtn setImage:img forState:UIControlStateHighlighted];
                });
            } else {
                //                NSLog(@"error");
            }
        });
        
        UILabel *dishNameLab = [[UILabel alloc]initWithFrame:CGRectMake(0, imageHeight, imageWidth, 25)];
        dishNameLab.text = [dic objectForKey:@"description"];
        if ([[dic objectForKey:@"description"] isEqualToString:@"type description"]) {
            dishNameLab.text = @"";
        }
        dishNameLab.font = [UIFont systemFontOfSize:18];
        dishNameLab.backgroundColor = [UIColor clearColor];
        [self addSubview:dishNameLab];
        
        UILabel *restaurantNameLab = [[UILabel alloc]initWithFrame:CGRectMake(0, imageHeight+25, imageWidth, 20)];
        restaurantNameLab.text = [[NSString stringWithFormat:@"at %@",[dic objectForKey:@"restaurantName"]]stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
        restaurantNameLab.font = [UIFont systemFontOfSize:14];
        restaurantNameLab.textColor = [UIColor grayColor];
        restaurantNameLab.backgroundColor = [UIColor clearColor];
        [self addSubview:restaurantNameLab];
        
        UIView *lowBackView = [[UIView alloc]initWithFrame:CGRectMake(1, imageHeight+50, self.frame.size.width-2, 30)];
        lowBackView.backgroundColor = [UIColor colorWithRed:0.9529 green:0.9529 blue:0.9529 alpha:1.0];
        [self addSubview:lowBackView];
        
        UILabel *qravedCountLab = [[UILabel alloc]initWithFrame:CGRectMake(imageWidth-85, imageHeight+58, 30, 15)];
        qravedCountLab.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"qravedCount"]];
        qravedCountLab.backgroundColor = [UIColor clearColor];
        qravedCountLab.textColor = [UIColor grayColor];
        qravedCountLab.font = [UIFont systemFontOfSize:14];
        [self addSubview:qravedCountLab];
        
        UIImageView *qraveImg = [[UIImageView alloc]initWithFrame:CGRectMake(imageWidth-70, imageHeight+58, 15, 15)];
        [qraveImg setImage:[UIImage imageNamed:@"qrave.png"]];
        [self addSubview:qraveImg];
        
        UIImageView *xian = [[UIImageView alloc]initWithFrame:CGRectMake(imageWidth-48, imageHeight+54, 0.5, 22)];
        [xian setImage:[UIImage imageNamed:@"header-bg.png"]];
        [self addSubview:xian];
        
        UILabel *commentCountLab = [[UILabel alloc]initWithFrame:CGRectMake(imageWidth-40, imageHeight+58, 30, 15)];
        commentCountLab.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"commentCount"]];
        commentCountLab.backgroundColor = [UIColor clearColor];
        commentCountLab.textColor = [UIColor grayColor];
        commentCountLab.font = [UIFont systemFontOfSize:14];
        [self addSubview:commentCountLab];
        
        UIButton *reviewBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [reviewBtn setBackgroundImage:[UIImage imageNamed:@"comment.png"] forState:UIControlStateNormal];
        [reviewBtn setBackgroundImage:[UIImage imageNamed:@"comment.png"] forState:UIControlStateHighlighted];
        reviewBtn.frame = CGRectMake(imageWidth-25, imageHeight+58, 15, 15);
        [self addSubview:reviewBtn];
        [reviewBtn addTarget:self action:@selector(reviewBtnTouched:) forControlEvents:UIControlEventTouchDown];
        
        UIButton *qraveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        qraveBtn.frame = CGRectMake(0, imageHeight+58, 50, 15);
        [qraveBtn addTarget:self action:@selector(qraveBtnTouched:) forControlEvents:UIControlEventTouchUpInside];
        qraveBtn.adjustsImageWhenHighlighted = NO;
        [qraveBtn setImage:[UIImage imageNamed:@"qrave-it-icon.png"] forState:UIControlStateNormal];
        [self addSubview:qraveBtn];
        NSLog(@"dic ======== %@",dic);
    }
    return self;
}



/*
-(void)imgBtnTouched:(UIButton *)btn {
    CGRect rect = ((QraveViewController *)self.delegate).navigationController.navigationBar.frame;
    ((QraveViewController *)self.delegate).navigationController.navigationBar.frame = CGRectMake(0, 0, rect.size.width, rect.size.height);
    
    AppDelegate *app = (AppDelegate *)[UIApplication sharedApplication].delegate;
    Restaurant *restaurant = (Restaurant *)[NSEntityDescription insertNewObjectForEntityForName:@"Restaurant" inManagedObjectContext:app.managedObjectContext];
    restaurant.restaurantID = [NSNumber numberWithInteger:[[self.infoDic objectForKey:@"restaurantId"] intValue]];
    restaurant.title = [self.infoDic objectForKey:@"title"];
    restaurant.reviewCount = [self.infoDic objectForKey:@"commentCount"];
    DetailViewController *detailViewController=[[DetailViewController alloc]initWithRestaurantID:[NSString stringWithFormat:@"%@",restaurant.restaurantID] andRestaurantName:restaurant.title andSegmentedIndex:1];
    [((QraveViewController *)self.delegate).navigationController pushViewController:detailViewController animated:YES];
    
    [app setTabbarHidden];
}
 
 */

-(void)imgBtnTouched:(UIButton *)btn {
    CGRect rect = ((QraveViewController *)self.delegate).navigationController.navigationBar.frame;
    float fixY =0;
    if([[[UIDevice currentDevice] systemVersion] floatValue]>=7.0)
    {
        fixY = 20;
    }
    ((QraveViewController *)self.delegate).navigationController.navigationBar.frame = CGRectMake(0, fixY, rect.size.width, rect.size.height);
    
    for(UIView* view in ((QraveViewController *)self.delegate).navigationController.navigationBar.subviews)
    {
        if([view isKindOfClass:[UIImageView class]])
        {
            if(view.tag==200)
            {
            [view setHidden:YES];
            }
        }
        
        if([view isKindOfClass:[UIButton class]])
        {
            [view setHidden:YES];
        }
    }
    
    UIView *bigQraveView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-44)];
    [bigQraveView setTag:BigQraveViewTag];
    [bigQraveView setBackgroundColor:[UIColor blackColor]];

    backButton=[UIButton buttonWithType:UIButtonTypeCustom];
    [backButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIImage *backImage=[UIImage imageNamed:@"back.png"];
    [backButton setBackgroundImage:backImage forState:UIControlStateNormal];
    [backButton setFrame:CGRectMake(5, 0, backImage.size.width, backImage.size.height)];
    [((QraveViewController *)self.delegate).navigationController.navigationBar addSubview:backButton];
    
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-100)];
    
    NSString *lastImgUrlStr = [self.infoDic objectForKey:@"imageUrl"];
    NSString *imgUrlStr = [NSString stringWithFormat:@"%@%@",QRAVED_WEB_IMAGE_SERVER,lastImgUrlStr];

    [imageView setImage:[UIImage imageNamed:@"qrave.png"]];
    
    [[LoadingView sharedLoadingView] startLoading];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSURL *url=[NSURL URLWithString:imgUrlStr];
        NSData *imageData=[NSData dataWithContentsOfURL:url];
        
        if(imageData!=nil)
        {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *image=[UIImage imageWithData:imageData];
            CGFloat imageHeight=image.size.height;
            CGFloat imagewidth=image.size.width;
            
            CGFloat imageViewHeight=imageHeight*DeviceWidth/imagewidth;
            
            [imageView setFrame:CGRectMake(0, 44, DeviceWidth, imageViewHeight)];
            imageView.center=imageView.superview.center;
            [imageView setImage:image];
            
            [[LoadingView sharedLoadingView] stopLoading];
        });
        }
    });
    [bigQraveView addSubview:imageView];
    
    UIButton *dishBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [dishBtn setFrame:CGRectMake(0, [Tools viewEndPointY:imageView], DeviceWidth, 30)];
    [dishBtn setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.3]];
    [dishBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [dishBtn setTitle:[self.infoDic objectForKey:@"description"] forState:UIControlStateNormal];
    [bigQraveView addSubview:dishBtn];
    
    UIButton *restaurantBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [restaurantBtn setFrame:CGRectMake(0, [Tools viewEndPointY:dishBtn], DeviceWidth, 30)];
    [restaurantBtn addTarget:self action:@selector(detailLink:) forControlEvents:UIControlEventTouchUpInside];
    [restaurantBtn setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.3]];
    [restaurantBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [restaurantBtn setTitle:[self.infoDic objectForKey:@"restaurantName"] forState:UIControlStateNormal];
    [bigQraveView addSubview:restaurantBtn];
    
    CATransition *applicationLoadViewIn =[CATransition animation];
    [applicationLoadViewIn setDuration:0.8];
    [applicationLoadViewIn setType:kCATransitionFade];
    [applicationLoadViewIn setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [[((QraveViewController *)self.delegate).view layer]addAnimation:applicationLoadViewIn forKey:kCATransitionFade];
    [((QraveViewController *)self.delegate).view addSubview:bigQraveView];
//    [[AppDelegate ShareApp] setTabbarHidden];
    
    //    [detailViewController setSegmentIndex:1];
    
}

-(void)back:(UIButton *)button
{
    [[LoadingView sharedLoadingView] stopLoading];
    
    UIView *bigQravedView=[((QraveViewController *)self.delegate).view viewWithTag:BigQraveViewTag];
    [bigQravedView removeFromSuperview];
    
    [backButton removeFromSuperview];
    for(UIView* view in ((QraveViewController *)self.delegate).navigationController.navigationBar.subviews)
    {
        if([view isKindOfClass:[UIImageView class]])
        {
            if(view.tag==200)
            {
                [view setHidden:NO];
            }
        }
        
        if([view isKindOfClass:[UIButton class]])
        {
            [view setHidden:NO];
        }
    }

//    [[AppDelegate ShareApp] setTabbarAppear];
}

-(void)detailLink:(UIButton *)button
{
    
    DetailViewController *detailViewController=[[DetailViewController alloc]initWithRestaurantID:[NSString stringWithFormat:@"%@",[self.infoDic objectForKey:@"restaurantId"]] andRestaurantName:[self.infoDic objectForKey:@"title"] andSegmentedIndex:1];
    [((QraveViewController *)self.delegate).navigationController pushViewController:detailViewController animated:YES];
    
    [self back:nil];
}

-(void)reviewBtnTouched:(UIButton *)btn {
//    BOOL isSelected = btn.selected;
//    if (isSelected) {
//        CGRect rect = self.frame;
//        self.frame = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height-50);
//        [self.delegate reCalculateFrameShorter:self];
//        btn.selected = NO;
//    } else {
//        CGRect rect = self.frame;
//        self.frame = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height+50);
//        [self.delegate reCalculateFrameLonger:self];
//        btn.selected = YES;
//    }
    float fixY =0;
    if([[[UIDevice currentDevice] systemVersion] floatValue]>=7.0)
    {
        fixY = 20;
    }
    CGRect rect = ((QraveViewController *)self.delegate).navigationController.navigationBar.frame;
    ((QraveViewController *)self.delegate).navigationController.navigationBar.frame = CGRectMake(0, fixY, rect.size.width, rect.size.height);
    CommentsViewController *cmtCtl = [[CommentsViewController alloc]initWithDishId:[self.infoDic objectForKey:@"id"]];
    [((HomeViewController *)self.delegate).navigationController pushViewController:cmtCtl animated:YES];
}


-(void)qraveIt:(UIButton *)button{
    
    NSString *dishId = [self.infoDic objectForKey:@"id"];
    //    NSString *Facebktoken = [[NSUserDefaults standardUserDefaults]objectForKey:@"token"];
    User *currentUser=[User sharedUser];
    NSString *Facebktoken =currentUser.token;
    NSString *userId =currentUser.userID;
    NSString *urlString = [NSString stringWithFormat:@"%@qrave?userID=%@&t=%@&dishID=%@",QRAVED_WEB_SERVICE_SERVER,userId,Facebktoken,dishId];
    
    NSLog(@"urlString222 ========%@",urlString);
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];
    [request setHTTPMethod:@"POST"];
    NSData *received = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSDictionary *returnJson=[NSJSONSerialization JSONObjectWithData:received
                                                             options: NSJSONReadingMutableContainers
                                                               error:nil];
    NSString *exceptionMsg=[returnJson objectForKey:@"exceptionMsg"];
    if(exceptionMsg!=nil)
    {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:ALERT_TITLE message:exceptionMsg];
        [alertView addButtonWithTitle:@"OK"];
        [alertView show];
        
        [button setImage:[UIImage imageNamed:@"qrave-it-icon.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        [button setImage:[UIImage imageNamed:@"qraved-icon.png"] forState:UIControlStateNormal];
    }

}

-(void)unQraveIt:(UIButton *)button{
    NSString *dishId = [self.infoDic objectForKey:@"id"];
    User *currentUser=[User sharedUser];
    NSString *Facebktoken =currentUser.token;
    NSString *userId =currentUser.userID;
    NSString *urlString = [NSString stringWithFormat:@"%@unqrave?userID=%@&t=%@&dishID=%@",QRAVED_WEB_SERVICE_SERVER,userId,Facebktoken,dishId];
    
    NSLog(@"urlString222 ========%@",urlString);
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];
    [request setHTTPMethod:@"POST"];
    NSData *received = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSDictionary *returnJson=[NSJSONSerialization JSONObjectWithData:received
                                    options: NSJSONReadingMutableContainers
                                      error:nil];
    NSString *exceptionMsg=[returnJson objectForKey:@"exceptionMsg"];
    if(exceptionMsg!=nil)
    {
        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:ALERT_TITLE message:exceptionMsg];
        [alertView addButtonWithTitle:@"OK"];
        [alertView show];
        
        [button setImage:[UIImage imageNamed:@"qraved-icon.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        [button setImage:[UIImage imageNamed:@"qrave-it-icon.png"] forState:UIControlStateNormal];
    }
    
}

-(void)qraveBtnTouched:(UIButton *)btn {
    
    if([AppDelegate ShareApp].isLogin)
    {
        
        BOOL selected = btn.selected;
        if (selected) { //qraved
            btn.selected = NO;
            [NSThread detachNewThreadSelector:@selector(unQraveIt:) toTarget:self withObject:btn];
        } else { // unqraved
            btn.selected = YES;
            [NSThread detachNewThreadSelector:@selector(qraveIt:) toTarget:self withObject:btn];
        }
    }
    else
    {
        [[AppDelegate ShareApp] goToLoginController];
    }
}

-(void)drawRect:(CGRect)rect {
    float imageHeight = self.frame.size.height - exceptImageHeight;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetRGBStrokeColor(context, 0.5, 0.5, 0.5, 1.0);//线条颜色
    CGContextSetLineWidth(context, 1.0);
    CGContextAddRect(context, CGRectMake(0, 0, self.frame.size.width, self.frame.size.height));
    CGContextStrokePath(context);
    
//    CGContextSetLineWidth(context, 0.5);
    CGContextMoveToPoint(context, 1, imageHeight+50);
    CGContextAddLineToPoint(context, 318,imageHeight+50);
    CGContextStrokePath(context);
//
//    CGContextSetLineWidth(context, 2);
//    CGContextMoveToPoint(context, imageWidth-70, imageWidth+50);
//    CGContextAddLineToPoint(context, imageWidth-70,imageWidth+80);
//    CGContextStrokePath(context);
}



@end
