//
//  V2_BrandPreferenceTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/8/8.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_BrandPreferenceTableViewCell.h"
#import "V2_BrandPreferenceNewCollectionViewCell.h"
@implementation V2_BrandPreferenceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI:(NSArray *)array brandTitleArray:(NSArray *)brandTitleArray{
    _eatingArray = array;
    _brandTitleArray = brandTitleArray;
    self.normalArray = [NSMutableArray array];
  
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(63, 111);
    layout.minimumLineSpacing = 15;
    layout.minimumInteritemSpacing = 5;
    layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 0);
    
    _photoCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,0, DeviceWidth, 115) collectionViewLayout:layout];
    _photoCollectionView.backgroundColor = [UIColor whiteColor];
    _photoCollectionView.delegate = self;
    _photoCollectionView.dataSource = self;
    _photoCollectionView.showsHorizontalScrollIndicator = NO;
    _photoCollectionView.allowsMultipleSelection = YES;
    [self.contentView addSubview:_photoCollectionView];
    if (@available(iOS 11.0, *)) {
        _photoCollectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;//UIScrollView也适用
    }
    
//  [_photoCollectionView registerClass:[V2_BrandPreferenceNewCollectionViewCell class] forCellWithReuseIdentifier:@"CellIdentifier"];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
//    V2_BrandModelSuper *
    if (self.eatingArray.count > 0) {
        return self.eatingArray.count;
    }else{
    
        return 0;
    }
    
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.eatingArray.count > 0) {
        
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath item]];
        [collectionView registerClass:[V2_BrandPreferenceNewCollectionViewCell class] forCellWithReuseIdentifier:CellIdentifier];
        
        V2_BrandPreferenceNewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
        
        cell.model = [self.eatingArray objectAtIndex:indexPath.item];
        
        return cell;

    }else{
    
        return nil;
    }
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"%ld%ld",(long)[indexPath section], (long)[indexPath item]];
    NSLog(@"%ld",(long)[CellIdentifier integerValue]);
    V2_BrandPreferenceNewCollectionViewCell *cell = (V2_BrandPreferenceNewCollectionViewCell *)[_photoCollectionView cellForItemAtIndexPath:indexPath];
    cell.selectImageView.hidden = NO;
    cell.selectView.layer.borderColor = [UIColor colorWithHexString:@"#D20000"].CGColor;
    V2_BrandModelSuper *superMOdel = [self.brandTitleArray objectAtIndex:[self.section integerValue]];
    V2_BrandModel *modle = [superMOdel.brandList objectAtIndex:[CellIdentifier integerValue]];
    NSLog(@"=======+++%@",modle.objectId);
    modle.state = @"1";
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"%ld%ld",(long)[indexPath section], (long)[indexPath item]];
    NSLog(@"%ld",(long)[CellIdentifier integerValue]);
    V2_BrandPreferenceNewCollectionViewCell *cell = (V2_BrandPreferenceNewCollectionViewCell *)[_photoCollectionView cellForItemAtIndexPath:indexPath];
    cell.selectImageView.hidden = YES;
    cell.selectView.layer.borderColor = [UIColor colorWithHexString:@"#EAEAEA"].CGColor;
    V2_BrandModelSuper *superMOdel = [self.brandTitleArray objectAtIndex:[self.section integerValue]];
    V2_BrandModel *modle = [superMOdel.brandList objectAtIndex:[CellIdentifier integerValue]];
    NSLog(@"=======-----%@",modle.objectId);
    modle.state = @"0";
    
}


#pragma amrk - 按钮点击事件
- (void)chooseEat:(UIButton *)cellBtn{
    NSLog(@"+++++%ld",(long)cellBtn.tag);
    
}

@end
