//
//  RestaurantMapCell.m
//  Qraved
//
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "RestaurantMapCell.h"
#import "UIConstants.h"
#import "UIColor+Hex.h"
#import "UIView+Helper.h"
#import "DLStarRatingControl.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"
#import "UIColor+Helper.h"
#import "UIImage+Resize.h"
#import "UIDevice+Util.h"

#define LEFT_X 10

@implementation RestaurantMapCell{
    IMGRestaurant*restaurant;
    
    UIView *backgroundCellView;
    UILabel *titleLabel;
    UILabel *cuisineAndDistrictLabel;
    DLStarRatingControl *ratingScoreControl;
    UILabel *ratingLabel;
    UILabel *reviewCountLabel;
    UILabel *reviewLabel;
    UILabel *offerLabel;
//    UIView *line;
    UILabel *openTime;
    UIImageView *restaurantImageView;
    UIImageView *locationImageView;
    UILabel *distanceLabel;
    int nextOpenCurenttime;
    int nextCloseCurenttime;
    int secondOpenCurenttime;
    int secondCloseCurenttime;

}

-(instancetype)initWithFrame:(CGRect)frame{
    if(self=[super initWithFrame:frame]){
        self.backgroundColor = [UIColor clearColor];
        //
        backgroundCellView = [[UIView alloc] initWithFrame:self.bounds];
        [backgroundCellView.layer setBorderWidth:0.5];
        [backgroundCellView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [backgroundCellView.layer setCornerRadius:5.0];
        backgroundCellView.backgroundColor = [UIColor whiteColor];
        [self addSubview:backgroundCellView];
        //
        titleLabel = [[UILabel alloc] init];
        titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:17];
        titleLabel.numberOfLines = 1;
        titleLabel.backgroundColor=[UIColor clearColor];
        [backgroundCellView addSubview:titleLabel];
        //
        cuisineAndDistrictLabel = [[UILabel alloc] init];
        cuisineAndDistrictLabel.numberOfLines=1;
        cuisineAndDistrictLabel.font=[UIFont fontWithName:DEFAULT_FONT_NAME size:13];
        cuisineAndDistrictLabel.lineBreakMode=NSLineBreakByTruncatingTail;
        [backgroundCellView addSubview:cuisineAndDistrictLabel];
        //
        ratingScoreControl=[[DLStarRatingControl alloc] initWithFrame:CGRectMake(LEFT_X, cuisineAndDistrictLabel.endPointY+5, 74, 18) andStars:5 andStarWidth:13 isFractional:YES spaceWidth:1];
        ratingScoreControl.userInteractionEnabled=NO;
        [backgroundCellView addSubview:ratingScoreControl];
        //
        ratingLabel = [[UILabel alloc] init];
        [ratingLabel setTextAlignment:NSTextAlignmentLeft];
        ratingLabel.font=[UIFont boldSystemFontOfSize:10];
        ratingLabel.textColor= [UIColor colorWithRed:253/255.0 green:181/255.0 blue:10/255.0 alpha:1];

        [backgroundCellView addSubview:ratingLabel];

        reviewCountLabel = [[UILabel alloc] init];
        [reviewCountLabel setTextAlignment:NSTextAlignmentRight];
        reviewCountLabel.font=[UIFont boldSystemFontOfSize:10];
        [reviewCountLabel setTextColor:[UIColor colorWithRed:71/255.0 green:124/255.0 blue:230/255.0 alpha:1]];
        [reviewCountLabel setBackgroundColor:[UIColor clearColor]];
        [backgroundCellView addSubview:reviewCountLabel];
        
        reviewLabel = [[UILabel alloc] init];
        reviewLabel.font=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:10];
        reviewLabel.textColor=[UIColor colorWithRed:71/255.0 green:124/255.0 blue:230/255.0 alpha:1];
        reviewLabel.userInteractionEnabled=YES;
        [backgroundCellView addSubview:reviewLabel];
        offerLabel = [[UILabel alloc] init];
        offerLabel.font = reviewLabel.font;
        offerLabel.textColor = [UIColor colorRed];
        [backgroundCellView addSubview:offerLabel];
        UITapGestureRecognizer* tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(reviewTapGesture:)];
        tap.numberOfTapsRequired = 1;
        tap.numberOfTouchesRequired = 1;
        [reviewCountLabel addGestureRecognizer:tap];
        [reviewLabel addGestureRecognizer:tap];
        
        openTime = [[UILabel alloc] init];
        openTime.textAlignment=NSTextAlignmentLeft;
        openTime.font=[UIFont boldSystemFontOfSize:11];
        openTime.textColor=[UIColor grayColor];
        openTime.numberOfLines = 0;
        [backgroundCellView addSubview:openTime];
//        line=[[UIView alloc]init];
//        line.backgroundColor=[UIColor colorWithRed:71/255.0 green:124/255.0 blue:230/255.0 alpha:1];
//        [backgroundCellView addSubview:line];
        //
        restaurantImageView = [[UIImageView alloc] initWithFrame:CGRectMake(backgroundCellView.frame.size.width - 10 - 77, (backgroundCellView.frame.size.height - 77)/2, 77, 77)];
        restaurantImageView.userInteractionEnabled=YES;
        

        
        [restaurantImageView.layer setMasksToBounds:YES];
        restaurantImageView.layer.borderColor = [UIColor whiteColor].CGColor;
        restaurantImageView.layer.cornerRadius = 5;
        restaurantImageView.layer.borderWidth = 0.3;
        [backgroundCellView addSubview:restaurantImageView];
        locationImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"searchLocation_map.png"]];
        locationImageView.contentMode = UIViewContentModeScaleAspectFit;
        [backgroundCellView addSubview:locationImageView];
        distanceLabel = [[UILabel alloc] init];
        [distanceLabel setFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12]];
        [backgroundCellView addSubview:distanceLabel];
    }
    return self;
}


-(void)setRestaurant:(IMGRestaurant*)restaurantLocal andNumber:(NSInteger)numuber{
    
    restaurant = restaurantLocal;
    //
    titleLabel.frame = CGRectMake(LEFT_X, LEFT_X, self.frame.size.width -3*LEFT_X-70, 20);
    titleLabel.text=[NSString stringWithFormat:@"%zd. %@",numuber+1,restaurant.title];
 //   [titleLabel sizeToFit];
    titleLabel.textColor = ([restaurant.state intValue]== 3)?[UIColor colorWithHexString:@"#7f8691"]:[UIColor colorWithRed:196/255.0 green:0 blue:0 alpha:1];
    //
    cuisineAndDistrictLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.endPointY, self.frame.size.width -3*LEFT_X-78, 20);
    cuisineAndDistrictLabel.text = [NSString stringWithFormat:@"%@ / %@",restaurant.cuisineName,restaurant.priceLevel];
//    [cuisineAndDistrictLabel sizeToFit];
    cuisineAndDistrictLabel.textColor = ([restaurant.state intValue] == 3)?[UIColor grayColor]:[UIColor color333333];
    //
    ratingScoreControl.frame=CGRectMake(titleLabel.frame.origin.x, cuisineAndDistrictLabel.endPointY+5, 64, 18);
    
    //
    float score = [restaurant.ratingScore floatValue];
    int scoreInt =(int)roundf(score);
    if (scoreInt/2.0-scoreInt/2>0) {
        ratingLabel.text = [NSString stringWithFormat:@"(%.1f/5)",scoreInt/2.0];

    }else{
        ratingLabel.text = [NSString stringWithFormat:@"(%d/5)",scoreInt/2];

        
    }
    [ratingScoreControl updateRating:[NSNumber numberWithFloat:scoreInt]];

    ratingLabel.frame = CGRectMake(ratingScoreControl.endPointX, ratingScoreControl.frame.origin.y-4.5, 35, 20);
    if ([restaurant.ratingScore intValue]==0) {
        ratingLabel.frame = CGRectMake(ratingScoreControl.endPointX, ratingScoreControl.frame.origin.y-3, 0, 0);
    }

    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"HH:mm"];
    NSString *  locationString=[dateformatter stringFromDate:senddate];
    NSString* hours=[locationString substringToIndex:2];
    NSRange rang={3,2};
    NSString* min=[locationString substringWithRange:rang];
    int curenttime=[hours intValue]*3600+[min intValue]*60;
    NSLog(@" current   time  %d",curenttime);
    //nextDay
    if (![restaurant.nextopenTime isEqualToString:@""]&&![restaurant.nextclosedTime isEqualToString:@""]) {
        
        NSString* nextHours=[restaurant.nextopenTime substringToIndex:2];
        NSString* nextMin=[restaurant.nextopenTime substringWithRange:rang];
        nextOpenCurenttime=[nextHours intValue]*3600+[nextMin intValue]*60;
        
        if ([restaurant.nextclosedTime isEqualToString:@"00:00"]&&![restaurant.nextopenTime isEqualToString:@"00:00"]) {
            restaurant.nextclosedTime=@"24:00";
        }
        NSString* nextCloseHours=[restaurant.nextclosedTime substringToIndex:2];
        NSString* nextCloseMin=[restaurant.nextclosedTime substringWithRange:rang];
        nextCloseCurenttime=[nextCloseHours intValue]*3600+[nextCloseMin intValue]*60;
        if (nextOpenCurenttime>nextCloseCurenttime) {
            
            restaurant.nextclosedTime=@"24:00";
            NSString* nextCloseHours1=[restaurant.nextclosedTime substringToIndex:2];
            NSString* nextCloseMin1=[restaurant.nextclosedTime substringWithRange:rang];
            nextCloseCurenttime=[nextCloseHours1 intValue]*3600+[nextCloseMin1 intValue]*60;
        }
        
    }
    //secondDay
    if (![restaurant.secondopenTime isEqualToString:@""]&&![restaurant.secondclosedTime isEqualToString:@""]) {
        
        NSString* secondHours=[restaurant.secondopenTime substringToIndex:2];
        NSString* secondMin=[restaurant.secondopenTime substringWithRange:rang];
        secondOpenCurenttime=[secondHours intValue]*3600+[secondMin intValue]*60;
        NSLog(@"secondOpenCurenttime  =%d",secondOpenCurenttime);
        NSString* secondCloseHours=[restaurant.secondclosedTime substringToIndex:2];
        NSString* secondCloseMin=[restaurant.secondclosedTime substringWithRange:rang];
        secondCloseCurenttime=[secondCloseHours intValue]*3600+[secondCloseMin intValue]*60;
        NSLog(@" secondCloseCurenttime =%d",secondCloseCurenttime);
    }
    NSInteger weekNum=[self returnTodayIsWeekDay];
    NSString* weekDay=[self returnWeekDayWithNumber:weekNum];
    if([restaurant.state intValue]==3){
        cuisineAndDistrictLabel.textColor = [UIColor grayColor];
        distanceLabel.textColor = [UIColor grayColor];
        
        
        openTime.textAlignment=NSTextAlignmentLeft;
        openTime.font=[UIFont boldSystemFontOfSize:12];
        openTime.text = @"Permanently Closed";
        
        openTime.textColor=[UIColor colorWithHexString:@"#7f8691"];
        
        reviewLabel.hidden=YES;
        reviewCountLabel.hidden=YES;
     
    }else if ([restaurant.state intValue]==6||[restaurant.state intValue]==1) {
        
        
        if ([restaurant.nextopenTime isEqualToString:@"00:00"]&&[restaurant.nextclosedTime isEqualToString:@"00:00"]) {
            openTime.text=@"OPEN";
            openTime.textColor=[UIColor colorWithRed:32/255.0 green:162/255.0 blue:105/255.0 alpha:1];
            [self bottomView];
            return;
        }
        
        if ([restaurant.nextopenDay isEqualToString:@""]||[restaurant.nextopenTime isEqualToString:@""]||[restaurant.nextclosedTime isEqualToString:@""]) {
            openTime.text=@"Closed";

            
            openTime.textColor=[UIColor grayColor];
            
            return;
        }
        if ([weekDay isEqualToString:restaurant.nextopenDay]) {
            
            if (curenttime>nextOpenCurenttime&&curenttime<nextCloseCurenttime) {
                openTime.text=@"OPEN";
                
                openTime.textColor=[UIColor colorWithRed:32/255.0 green:162/255.0 blue:105/255.0 alpha:1];
            }else if (curenttime>nextCloseCurenttime){
                
                openTime.frame= CGRectMake(DeviceWidth-10-99, 12, 103, 22);
                openTime.font=[UIFont boldSystemFontOfSize:10];
                openTime.text=[NSString stringWithFormat:@"OPEN ON %@",[restaurant.secondopenDay uppercaseString]];
                openTime.textColor=[UIColor grayColor];
                
                
                
            }else
            {
                
                openTime.text=[NSString stringWithFormat:@"OPEN AT %@",restaurant.nextopenTime ];
                openTime.textColor=[UIColor grayColor];
                
            }
            
        }else{
            openTime.font=[UIFont boldSystemFontOfSize:9];
            openTime.text=[NSString stringWithFormat:@"OPEN ON %@",[restaurant.nextopenDay uppercaseString]];
            
            openTime.textColor=[UIColor grayColor];
        }
        
        if (restaurant.yesteropenDay&&![restaurant.yesteropenDay isEqualToString:@""]) {
            NSString* secondHours=[restaurant.yesterclosedTime substringToIndex:2];
            NSString* secondMin=[restaurant.yesterclosedTime substringWithRange:rang];
            int yesterCloseCurenttime=[secondHours intValue]*3600+[secondMin intValue]*60;
            if (curenttime<yesterCloseCurenttime) {
                openTime.text=@"OPEN";
                
                openTime.textColor=[UIColor colorWithRed:32/255.0 green:162/255.0 blue:105/255.0 alpha:1];
            }
            
        }
        
    }else{
        openTime.font=[UIFont boldSystemFontOfSize:10];
        openTime.textAlignment=NSTextAlignmentRight;
        openTime.text=restaurant.stateName;
        openTime.textColor=[UIColor grayColor];
        
        
    }

    [self bottomView];

}
-(void)bottomView{
    if (!([restaurant.state intValue]== 3)) {
        reviewCountLabel.frame=CGRectMake(ratingLabel.endPointX, ratingScoreControl.frame.origin.y, 25,15);
        reviewCountLabel.text = [NSString stringWithFormat:@"%@",restaurant.reviewCount];
        [reviewCountLabel sizeToFit];
        
        reviewLabel.frame=CGRectMake(reviewCountLabel.endPointX+3, reviewCountLabel.frame.origin.y, 90,12);
        reviewLabel.text = [NSString stringWithFormat:@"%@",[restaurant.reviewCount intValue]>1?@"reviews":@"review"];
        [reviewLabel sizeToFit];
        reviewLabel.center = CGPointMake(reviewLabel.center.x, reviewCountLabel.center.y);
        if ([restaurant.reviewCount intValue]==0) {
            reviewCountLabel.frame = CGRectZero;
            reviewLabel.frame = CGRectZero;
        }
    }
    //
    __weak typeof(restaurantImageView) weakRestaurantImageView = restaurantImageView;

    [restaurantImageView sd_setImageWithURL:[NSURL URLWithString:[restaurant.imageUrl returnFullImageUrl]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        image = [image imageByScalingAndCroppingForSize:CGSizeMake(77, 77)];
        
        [weakRestaurantImageView setImage:image];
    }];
    restaurantImageView.frame = CGRectMake(titleLabel.endPointX, titleLabel.startPointY, 77, 77);
    if (restaurant.bestOfferName.length>0) {
        offerLabel.text = restaurant.bestOfferName;
        offerLabel.frame = CGRectMake(ratingScoreControl.startPointX, ratingScoreControl.endPointY-5, titleLabel.frame.size.width, 20);
        locationImageView.frame = CGRectMake(ratingScoreControl.startPointX, offerLabel.endPointY+5, 10, 10);
        NSString *distanceStr = [restaurant distance];
        [distanceLabel setText:[NSString stringWithFormat:@"%@ .",distanceStr]];
        if (distanceStr.length==0) {
            distanceLabel.text = @". ";
            distanceLabel.frame = CGRectMake(locationImageView.startPointX+5, offerLabel.endPointY, 5, 20);
            openTime.frame = CGRectMake(distanceLabel.endPointX, offerLabel.endPointY, 103, 20);
            
            [locationImageView removeFromSuperview];
        }else{
            distanceLabel.frame = CGRectMake(locationImageView.endPointX+5, offerLabel.endPointY, 55, 20);
            openTime.frame = CGRectMake(distanceLabel.endPointX, offerLabel.endPointY, 103, 20);
        }
        
    }else{
        offerLabel.frame = CGRectZero;
        locationImageView.frame = CGRectMake(ratingScoreControl.startPointX, ratingScoreControl.endPointY+5, 10, 10);
        NSString *distanceStr = [restaurant distance];
        [distanceLabel setText:[NSString stringWithFormat:@"%@ .",distanceStr]];
        if (distanceStr.length==0) {
            distanceLabel.text = @". ";
            distanceLabel.frame = CGRectMake(locationImageView.startPointX+5, ratingScoreControl.endPointY, 5, 20);
            openTime.frame = CGRectMake(distanceLabel.endPointX, distanceLabel.startPointY, 103, 20);
            
            [locationImageView removeFromSuperview];
        }else{
            distanceLabel.frame = CGRectMake(locationImageView.endPointX+5, ratingScoreControl.endPointY, 55, 20);
            openTime.frame = CGRectMake(distanceLabel.endPointX, distanceLabel.startPointY, 103, 20);
        }
    }
    if ([UIDevice isIphone4Now]||[UIDevice isIphone5]) {
        openTime.frame = CGRectMake(openTime.startPointX, openTime.startPointY, 103, 20);

    }else{
        openTime.frame = CGRectMake(openTime.startPointX, openTime.startPointY, 120, 20);

    }

}
-(NSString*)returnWeekDayWithNumber:(NSInteger)number
    {
        
        switch (number) {
            case 1:
                return [NSString stringWithFormat:@"Sun"];
                break;
            case 2:
                return [NSString stringWithFormat:@"Mon"];
                break;
            case 3:
                return [NSString stringWithFormat:@"Tue"];
                break;
            case 4:
                return [NSString stringWithFormat:@"Wed"];
                break;
            case 5:
                return [NSString stringWithFormat:@"Thu"];
                break;
            case 6:
                return [NSString stringWithFormat:@"Fri"];
                break;
            case 7:
                return [NSString stringWithFormat:@"Sat"];
                break;
            default:
                break;
        }
        
        
        return nil;
        
        
    }

-(NSInteger)returnTodayIsWeekDay{
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDate *now = [NSDate date];;
        NSDateComponents *comps = [[NSDateComponents alloc] init];
        NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
        NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
        comps = [calendar components:unitFlags fromDate:now];
        NSInteger weekDay=[comps weekday];
        NSLog(@"today is %ld day ",(long)weekDay);
        return weekDay;
        
        
        
}

-(void)reviewTapGesture:(UITapGestureRecognizer*)tap{
    
}

@end
