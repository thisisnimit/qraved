//
//  GoFoodRestaurantTableViewCell.m
//  Qraved
//
//  Created by harry on 2018/4/8.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "GoFoodRestaurantTableViewCell.h"

@implementation GoFoodRestaurantTableViewCell
{
    UIImageView *imageView;
    UILabel *lblTitle;
    DLStarRatingControl *ratingScoreControl;
    UILabel *lblRating;
    UILabel *lblDes;
    
    UIButton *btnSave;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return  self;
}

- (void)createUI{
    imageView = [UIImageView new];
    
    lblTitle = [UILabel new];
    lblTitle.font = [UIFont systemFontOfSize:14];
    lblTitle.textColor = [UIColor color333333];
    
    ratingScoreControl=[[DLStarRatingControl alloc]initWithFrame:CGRectMake(0, 0, 73, 14) andStars:5 andStarWidth:13 isFractional:YES spaceWidth:2];
    ratingScoreControl.userInteractionEnabled=NO;
    
    lblRating = [UILabel new];
    lblRating.font = [UIFont systemFontOfSize:12];
    lblRating.textColor = [UIColor color999999];
    
    lblDes = [UILabel new];
    lblDes.font = [UIFont systemFontOfSize:12];
    lblDes.textColor = [UIColor color999999];
    
    btnSave = [UIButton new];
    [btnSave addTarget:self action:@selector(saveButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [self.contentView sd_addSubviews:@[imageView, lblTitle, ratingScoreControl, lblRating, lblDes, btnSave]];
    
    imageView.sd_layout
    .leftSpaceToView(self.contentView, 15)
    .topSpaceToView(self.contentView, 15)
    .widthIs(50)
    .heightIs(50);
    
    lblTitle.sd_layout
    .topEqualToView(imageView)
    .leftSpaceToView(imageView, 10)
    .rightSpaceToView(self.contentView, 65)
    .heightIs(16);
    
    ratingScoreControl.sd_layout
    .topSpaceToView(lblTitle, 4)
    .leftEqualToView(lblTitle)
    .widthIs(73)
    .heightIs(14);
    
    lblRating.sd_layout
    .topEqualToView(ratingScoreControl)
    .leftSpaceToView(ratingScoreControl, 3)
    .rightSpaceToView(self.contentView, 65)
    .heightIs(14);
    
    lblDes.sd_layout
    .topSpaceToView(ratingScoreControl, 6)
    .leftEqualToView(ratingScoreControl)
    .rightSpaceToView(self.contentView, 65)
    .heightIs(14);
    
    btnSave.sd_layout
    .rightSpaceToView(self.contentView, 15)
    .topSpaceToView(self.contentView, 30)
    .widthIs(25)
    .heightIs(25);
}

- (void)setRestaurant:(IMGRestaurant *)restaurant{
    _restaurant = restaurant;
    
    [imageView sd_setImageWithURL:[NSURL URLWithString:[restaurant.imageUrl returnCurrentImageString]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:nil];
    
    lblTitle.text = restaurant.title;
    
    float score = [restaurant.ratingScore floatValue];
    int scoreInt =(int)roundf(score);
    [ratingScoreControl updateRating:[NSNumber numberWithFloat:scoreInt]];
    
    NSString *ratingStr = @"";
    if (restaurant.ratingCount != nil && ![restaurant.ratingCount isEqual:@0]) {
        ratingStr = [NSString stringWithFormat:@"(%@)",restaurant.ratingCount];
    }
    lblRating.text = ratingStr;
    
    NSString *dollar;
    switch(restaurant.priceLevel.intValue){
        case 1:dollar=@"Below 100K";break;
        case 2:dollar=@"100K - 200K";break;
        case 3:dollar=@"200K - 300K";break;
        case 4:dollar=@"Start from 300K";break;
    }

    NSString *cuisineAndDistrictText;
    if ([restaurant.cuisineName isEqualToString:@""]) {
        cuisineAndDistrictText = [NSString stringWithFormat:@"%@ • %@",restaurant.districtName,dollar];
    }else{
        cuisineAndDistrictText = [NSString stringWithFormat:@"%@ • %@ • %@",_restaurant.cuisineName,restaurant.districtName,dollar];
    }
    
    lblDes.text=cuisineAndDistrictText;

    if ([restaurant.saved boolValue]) {
        [btnSave setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
    }else{
        [btnSave setImage:[UIImage imageNamed:@"ic_heart_home"] forState:UIControlStateNormal];
    }

}

- (void)saveButtonTapped{
    if (self.saveTapped) {
        self.saveTapped(self.restaurant, self.indexPath);
    }
}

- (void)freshSavedButton:(BOOL)saved{
    if (saved) {
        [btnSave setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
    }else{
        [btnSave setImage:[UIImage imageNamed:@"ic_heart_home"] forState:UIControlStateNormal];
    }
}

@end
