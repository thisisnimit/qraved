//
//  V2_BrandModelSuper.m
//  Qraved
//
//  Created by Adam.zhang on 2017/8/8.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_BrandModelSuper.h"

@implementation V2_BrandModelSuper

/* 数组中存储模型数据，需要说明数组中存储的模型数据类型 */
+(NSDictionary *)mj_objectClassInArray
{
    return @{@"brandList" : [V2_BrandModel class]};
}
@end
