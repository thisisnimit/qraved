//
//  HomeEngageOfferTableViewCell.h
//  Qraved
//
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeTableViewCell.h"
#import "IMGOfferCard.h"
#import "IMGRestaurant.h"

@protocol homeEngageOfferDelegate <NSObject>

@optional
-(void)gotoDetailpageWithResturants:(IMGRestaurant*)restaurant;
- (void)saveBtnClick:(BOOL)isSaved andRestaurant:(IMGRestaurant *)restaurant withSaveBlock:(void(^)(BOOL save))saveBlock;
-(void)gotoOfferDetailpageWithResturants:(IMGOfferCard*)restaurant withIndexPath:(NSIndexPath*)indexpath;

@end

@interface HomeEngageOfferTableViewCell : HomeTableViewCell
@property(nonatomic,weak)id<homeEngageOfferDelegate>homeOfferCellDelegate;
@property(nonatomic,strong) IMGOfferCard *offerModel;
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier;


@end
