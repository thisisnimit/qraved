//
//  HomeUploadPhotoTableViewCell.m
//  Qraved
//
//  Created by lucky on 16/3/7.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "HomeUploadPhotoTableViewCell.h"
#import "UIConstants.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"
#import "IMGRestaurant.h"
#import "UILabel+Helper.h"
#import "IMGDish.h"
#import "UIButton+WebCache.h"
#import "CXAHyperlinkLabel.h"
#import "NSString+CXAHyperlinkParser.h"
#import "IMGUser.h"
#import "UILikeCommentShareView.h"
#import "LoadingView.h"
#import "UICommentInputView.h"
#import "AppDelegate.h"
#import "UploadPhotoHandler.h"
#import "Date.h"
#import "LikeView.h"
#import "HomeUtil.h"
#import "IMGLikeList.h"


@implementation HomeUploadPhotoTableViewCell
{
    UIButton *avatarLinkButton;
    
    UILabel *lblName;

    UILabel *lblRestaurantName;
    UILabel *lblDistrictName;
    UILabel *reviewAndPhotoCountLabel;
    UILabel *timeLabel;
    
    UIButton *writeReviewBtn;
    
    UILabel *likeLabel;
    UILabel *commentLabel;
    
    CardPhotosView *cardPhotosView;
    V2_LikeCommentShareView *likeCSView;
    IMGDish *dish;
    UIView *separateView;
    
    NSArray *dishArrM;
    IMGUser *userM;
    NSAttributedString *titleAttributedString;
    
    NSMutableArray* likelistArr;
    LikeView* likepopView;
    UIButton *heartBtn;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier
{
    if(self=[super initWithStyle:style reuseIdentifier:identifier]){
        [self createUI];

    }
    return self;
}

- (void)createUI{
    
    heartBtn = [UIButton new];
    [heartBtn setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
    [heartBtn addTarget:self action:@selector(heartClick:) forControlEvents:UIControlEventTouchUpInside];
    
    lblRestaurantName = [UILabel new];
    lblRestaurantName.font = [UIFont systemFontOfSize:14];
    lblRestaurantName.textColor = [UIColor color333333];
    lblRestaurantName.userInteractionEnabled = YES;
    
    lblDistrictName = [UILabel new];
    lblDistrictName.font = [UIFont systemFontOfSize:14];
    lblDistrictName.textColor = [UIColor color999999];
    
    UIImageView *topLine = [UIImageView new];
    topLine.image = [UIImage imageNamed:@"search_line"];
    
    avatarLinkButton = [UIButton new];
    [avatarLinkButton addTarget:self action:@selector(gotoOtherProfileWithUser) forControlEvents:UIControlEventTouchUpInside];
    
    lblName = [UILabel new];
    lblName.font = [UIFont systemFontOfSize:14];
    lblName.textColor = [UIColor color333333];
    lblName.userInteractionEnabled = YES;
    UITapGestureRecognizer *nameTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoOtherProfileWithUser)];
    [lblName addGestureRecognizer:nameTap];
    
    
    reviewAndPhotoCountLabel = [UILabel new];
    reviewAndPhotoCountLabel.font = [UIFont systemFontOfSize:12];
    reviewAndPhotoCountLabel.textColor = [UIColor color999999];
    
    timeLabel = [UILabel new];
    timeLabel.font = [UIFont systemFontOfSize:12];
    timeLabel.textColor = [UIColor color999999];
    timeLabel.textAlignment = NSTextAlignmentRight;
    
    
    cardPhotosView = [[CardPhotosView alloc] init];
    cardPhotosView.homeTableViewCellDelegate=self.homeTableViewCellDelegate;
    cardPhotosView.cardPhotosViewDelegate = self;
    
    writeReviewBtn = [UIButton new];
    [writeReviewBtn setTitle:@"Write Review" forState:UIControlStateNormal];
    [writeReviewBtn setTitleColor:[UIColor colorWithHexString:@"F10F33"] forState:UIControlStateNormal];
    [writeReviewBtn setImage:[UIImage imageNamed:@"ic_write_review"] forState:UIControlStateNormal];
    writeReviewBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    writeReviewBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [writeReviewBtn addTarget:self action:@selector(writeReviewBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *bottomLine = [UIImageView new];
    bottomLine.image = [UIImage imageNamed:@"search_line"];
    
    likeCSView = [V2_LikeCommentShareView new];
    likeCSView.delegate = self;
    
    UIButton *btn = [UIButton new];
    [likeCSView addSubview:btn];
    
    separateView = [UIView new];
    separateView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    
    [self.contentView sd_addSubviews:@[heartBtn,lblRestaurantName,lblDistrictName,topLine,avatarLinkButton,lblName,reviewAndPhotoCountLabel,timeLabel,cardPhotosView,writeReviewBtn,bottomLine,likeCSView,separateView]];
    
    heartBtn.sd_layout
    .widthIs(40)
    .heightIs(34)
    .rightSpaceToView(self.contentView,15)
    .topSpaceToView(self.contentView,5);
    heartBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    
    
    
    lblRestaurantName.sd_layout
    .topSpaceToView(self.contentView, 0)
    .leftSpaceToView(self.contentView,15)
    .heightIs(44);
    
    [lblRestaurantName setSingleLineAutoResizeWithMaxWidth:DeviceWidth-55-80];
    
    lblDistrictName.sd_layout
    .topEqualToView(lblRestaurantName)
    .rightSpaceToView(heartBtn,5)
    .heightIs(44)
    .leftSpaceToView(lblRestaurantName,3);
    
    //[lblRestaurantName setSingleLineAutoResizeWithMaxWidth:DeviceWidth-60];
    
    UITapGestureRecognizer *restaurantTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(restaurantTap:)];
    [lblRestaurantName addGestureRecognizer:restaurantTap];
    
    
    
    topLine.sd_layout
    .topSpaceToView(lblRestaurantName, 0)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(1);
    
    avatarLinkButton.sd_layout
    .topSpaceToView(topLine, 10)
    .leftEqualToView(topLine)
    .widthIs(40)
    .heightIs(40);
    
    lblName.sd_layout
    .topEqualToView(avatarLinkButton)
    .leftSpaceToView(avatarLinkButton, 10)
    .heightIs(16);
    
    [lblName setSingleLineAutoResizeWithMaxWidth:DeviceWidth-80];
    
    reviewAndPhotoCountLabel.sd_layout
    .topSpaceToView(lblName, 5)
    .leftSpaceToView(avatarLinkButton, 10)
    .heightIs(14);
    
    [reviewAndPhotoCountLabel setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
    timeLabel.sd_layout
    .topEqualToView(reviewAndPhotoCountLabel)
    .rightSpaceToView(self.contentView, 15)
    .leftSpaceToView(reviewAndPhotoCountLabel, 10)
    .heightIs(14);
    
    cardPhotosView.sd_layout
    .topSpaceToView(avatarLinkButton, 15)
    .leftSpaceToView(self.contentView, 0)
    .rightSpaceToView(self.contentView, 0)
    .autoHeightRatio(0);
    
    writeReviewBtn.sd_layout
    .topSpaceToView(cardPhotosView, 10)
    .centerXEqualToView(cardPhotosView)
    .widthIs(110)
    .heightIs(18);
    
    bottomLine.sd_layout
    .topSpaceToView(writeReviewBtn, 15)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(1);
    
    likeCSView.sd_layout
    .topSpaceToView(bottomLine, 0)
    .leftSpaceToView(self.contentView, 0)
    .rightSpaceToView(self.contentView, 0)
    .autoHeightRatio(0);
    
    btn.sd_layout
    .topSpaceToView(likeCSView,0)
    .leftSpaceToView(likeCSView,DeviceWidth/3 - 60)
    .widthIs(60)
    .heightIs(50);
    [btn addTarget:self action:@selector(likeCountClick:) forControlEvents:UIControlEventTouchUpInside];
    
    separateView.sd_layout
    .topSpaceToView(likeCSView,0)
    .leftSpaceToView(self.contentView,0)
    .rightSpaceToView(self.contentView,0)
    .heightIs(20);

}

- (void)setUploadCard:(IMGUploadPhotoCard *)uploadCard{
    IMGRestaurant *restaurant = uploadCard.uploadCardRestaurant;
    NSMutableArray *dishArrM_ = uploadCard.dishArrM;
    IMGUser *user = uploadCard.uploadCardUser;

    dishArrM = dishArrM_;
    userM = user;
    _restaurant = restaurant;
    _restaurant.districtName = uploadCard.uploadCardUser.districtName;
    dish = [dishArrM firstObject];

    if ([restaurant.saved isEqual:@1]) {
        [heartBtn setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
    }else{
        [heartBtn setImage:[UIImage imageNamed:@"ic_heart_home"] forState:UIControlStateNormal];
    }
    
    user.avatar = [user.avatar stringByReplacingPercentEscapesUsingEncoding:
                   NSUTF8StringEncoding];
    
    NSURL *urlString ;
    if ([user.avatar hasPrefix:@"http"])
    {
        urlString = [NSURL URLWithString:user.avatar];
    }else if ( (user.avatar == nil) || [user.avatar isEqualToString:@""]){
        urlString = nil;
    }
    else
    {
        urlString = [NSURL URLWithString:[user.avatar returnFullImageUrl]];
    }
    
    //    urlString = [NSURL URLWithString:user.avatar];
    NSString *path_sandox = NSHomeDirectory();
    //设置一个图片的存储路径
    NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/avastar.png"];
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    image = [image cutCircleImage];
    if (image&&[userM.userId intValue]== [[IMGUser currentUser].userId intValue]) {
        [avatarLinkButton setImage:image forState:UIControlStateNormal];
    }else{
        __weak UIButton *weakAvatarLinkButton = avatarLinkButton;
        [avatarLinkButton setImageWithURL:urlString forState:UIControlStateNormal placeholderImage:[[UIImage imageNamed:@"headDefault.jpg"] cutCircleImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
            if (image.size.height>0&&image.size.width>0) {
                if (image.size.height>0&&image.size.width>0) {
                    image = [image cutCircleImage];
                    [weakAvatarLinkButton setImage:image forState:UIControlStateNormal];
                }
            }
          
        }];
    }
    
    lblRestaurantName.text = restaurant.title;
    lblDistrictName.text = restaurant.districtName;
    
    user.userName = [user.userName filterHtml];
    if (dishArrM.count>1) {
        lblName.text = [NSString stringWithFormat:@"%@ uploaded %ld photos",user.userName,dishArrM.count];
    }else{
        lblName.text = [NSString stringWithFormat:@"%@ uploaded a photo",user.userName];
    }
  
    if ([user.photoCount intValue]!=0&&[user.reviewCount intValue]!=0) {
        
        
        [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@ • %@ %@",user.photoCount,[user.photoCount intValue] > 1 ? @"photos":@"photo",user.reviewCount,[user.reviewCount intValue] > 1 ? @"reviews":@"review"]];

    }else if ([user.photoCount intValue]==0||[user.reviewCount intValue]==0){
        if ([user.reviewCount intValue]==0&&[user.photoCount intValue]!=0) {
            
            [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@",user.photoCount,[user.photoCount intValue] > 1 ? @"photos":@"photo"]];

        }
        if ([user.photoCount intValue]==0&&[user.reviewCount intValue]!=0) {
            
            [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@",user.reviewCount,[user.reviewCount intValue] > 1 ? @"reviews":@"review"]];
        }
        
    }
    
    if ((restaurant.timeline != nil) && ([restaurant.timeline longLongValue] != 0) ) {
        [timeLabel setText:[Date getTimeInteval_v4:[restaurant.timeline longLongValue]/1000]];
    }

    
    if (![dish.imageHeight floatValue]) {
        dish.imageHeight = [NSNumber numberWithFloat:ONE_PHOTO_HEIGHT];
    }
    
    if (dishArrM.count == 1)
    {
        cardPhotosView.cardPhotoHeight = [dish.imageHeight floatValue];
    }

    [cardPhotosView setPhotos:dishArrM];
    cardPhotosView.homeTableViewCellDelegate=self.homeTableViewCellDelegate;
    cardPhotosView.cardPhotosViewDelegate = self;
    
    IMGUser *currentUser = [IMGUser currentUser];
    if ( ([user.userId intValue] !=0) && ([currentUser.userId intValue] == [user.userId intValue]))
    {
        writeReviewBtn.hidden = NO;
        writeReviewBtn.sd_layout
        .heightIs(18);
    }
    else
    {
        writeReviewBtn.hidden = YES;
        writeReviewBtn.sd_layout
        .heightIs(0);
    }
    
    [writeReviewBtn updateLayout];

    if (dishArrM.count>1) {
        [likeCSView setCommentCount:[restaurant.commentCardCount intValue] likeCount:[restaurant.likeCardCount intValue] liked:restaurant.isLikeCard];
    }else{
        [likeCSView setCommentCount:[dish.commentCount intValue] likeCount:[dish.likeCount intValue] liked:dish.isLike];
    }
    
    
    [self setupAutoHeightWithBottomView:separateView bottomMargin:0];

}
#pragma mark - like list
- (void)likeCountClick:(UIButton *)btn{
    [[Amplitude instance] logEvent:@" RC - View People Who Likes"];
    [self userPhotoCardAmplitude];
    if (likepopView) {
        [likepopView removeFromSuperview];
    }
    
    [[LoadingView sharedLoadingView]startLoading];
    if (dishArrM.count>1) {
        
        [HomeUtil getLikeListMuchPhotoFromServiceWithmoderateReviewId:userM.moderateReviewId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray, NSNumber *likeviewcount) {
            likelistArr=[NSMutableArray arrayWithArray:likeListArray];
            likepopView.likelistArray=likelistArr;
            likepopView.likecount=likeviewcount;
            [[LoadingView sharedLoadingView]stopLoading];
            
        }];
        
    }else{
        
        [HomeUtil getLikeListFromServiceWithDishId:dish.dishId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
            
            
            likelistArr=[NSMutableArray arrayWithArray:likeListArray];
            likepopView.likelistArray=likelistArr;
            likepopView.likecount=likeviewcount;
            [[LoadingView sharedLoadingView]stopLoading];
        }];
    }
    likepopView=[[LikeView alloc]initWithFrame:CGRectMake(0, -20, DeviceWidth, DeviceHeight)];
    likepopView.delegate=self;
    //遍历找到本view的导航控制器
    UIResponder *responder = [self nextResponder];
    while (responder){
        
        if ([responder isKindOfClass:[UINavigationController class]]){
            UINavigationController* vc=(UINavigationController*)responder;
            [[vc.viewControllers lastObject].view addSubview:likepopView];
            likepopView.layer.zPosition=1000;
            return;
        }
        responder = [responder nextResponder];
    }

}

-(void)LikeviewLoadMoreData:(NSInteger)offset
{
    
    if (dishArrM.count>1) {
        
        [HomeUtil getLikeListMuchPhotoFromServiceWithmoderateReviewId:userM.moderateReviewId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset] andBlock:^(NSArray *likeListArray, NSNumber *likeviewcount) {
            [likelistArr addObjectsFromArray:likeListArray];
            likepopView.likecount=likeviewcount;
            [likepopView.likeUserTableView reloadData];
            
        }];
        
    }else
    {
        
        [HomeUtil getLikeListFromServiceWithDishId:dish.dishId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
            
            [likelistArr addObjectsFromArray:likeListArray];
            likepopView.likecount=likeviewcount;
            [likepopView.likeUserTableView reloadData];
            
        }];
        
    }
    
}
-(void)returnLikeCount:(NSInteger)count
{
    
}

- (void)gotoOtherProfileWithUser
{
    if ((userM.userId == nil)  || ([userM.userId longValue] ==0)) {
        return;
    }
    [self.homeTableViewCellDelegate gotoOtherProfileWithUser:userM];
    [self userPhotoCardAmplitude];
}

- (void)gotoCardDetailPage
{
    [self userPhotoCardAmplitude];
    
    if (dishArrM.count>1) {
        [self.homeUploadPhotoTableViewCellDelegate homeUploadPhotoTableViewCellImageClick:_restaurant andUser:userM andDishArr:dishArrM andIndex:0 andFromCellIndexPath:self.fromCellIndexPath];
        return;
    }
    if (self.homeTableViewCellDelegate)
    {
        NSDictionary *dataDic = [[NSDictionary alloc] initWithObjectsAndKeys:_restaurant,@"restaurant",dishArrM,@"dataArr",userM,@"user", nil];
        [self.homeTableViewCellDelegate goToCardDetailPageWithType:15 andData:dataDic andCellIndexPath:self.fromCellIndexPath];
    }
}

- (void)writeReviewBtnClick
{
    if (self.homeUploadPhotoTableViewCellDelegate)
    {
        [self.homeUploadPhotoTableViewCellDelegate writeReviewWithRestaurant:_restaurant cellIndexPath:self.fromCellIndexPath];
    }
}

- (void)likeCommentShareView:(UIView*)likeCommentShareView_ viewPreviousCommentsTapped:(UIButton*)button lastComment:(id)comment{
    
    [[Amplitude instance] logEvent:@"CL - View Previous Comments CTA" withEventProperties:@{@"Location":@"Restaurant event card on homepage"}];
    
    if (dishArrM.count>1) {
    [self.homeUploadPhotoTableViewCellDelegate homeUploadPhotoTableViewCellImageClick:_restaurant andUser:userM andDishArr:dishArrM andIndex:0 andFromCellIndexPath:self.fromCellIndexPath];
        return;
    }
    //goto CDP screen
    [self gotoCardDetailPage];
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView_ likeButtonTapped:(UIButton *)button{
    [self userPhotoCardAmplitude];
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
//    [[LoadingView sharedLoadingView] startLoading];
    //like service
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    //Photo_ID, UploaderUser_ID, Restaurant_ID, Location
    [eventProperties setValue:dish.dishId forKey:@"Photo_ID"];
    [eventProperties setValue:userM.moderateReviewId forKey:@"MultiPhoto_ID"];
    [eventProperties setValue:userM.userId forKey:@"UploaderUser_ID"];
    [eventProperties setValue:_restaurant.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:@"Photo card on homepage" forKey:@"Location"];

    if (dishArrM.count>1) {
        [UploadPhotoHandler likeCard: _restaurant.isLikeCard moderateReviewId:userM.moderateReviewId andBlock:^{
            if (!_restaurant.isLikeCard) {
                [[Amplitude instance] logEvent:@"UC - Unlike Multi Photo Succeed" withEventProperties:eventProperties];
            }else{
                [[Amplitude instance] logEvent:@"UC - Like multi photo Succeed" withEventProperties:eventProperties];
            }
        }failure:^(NSString *exceptionMsg){
            if([exceptionMsg isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
                [[AppDelegate ShareApp] goToLoginController];
            }
        }];
        _restaurant.isLikeCard = !_restaurant.isLikeCard;
        if(_restaurant.isLikeCard){
            _restaurant.likeCardCount = [NSNumber numberWithInt:[_restaurant.likeCardCount intValue]+1];
            [[Amplitude instance] logEvent:@"UC - Like multi photo Initiate" withEventProperties:eventProperties];
        }else{
            _restaurant.likeCardCount = [NSNumber numberWithInt:[_restaurant.likeCardCount intValue]-1];
            [[Amplitude instance] logEvent:@"UC - Unlike Multi Photo Initiate" withEventProperties:eventProperties];
        }
    }else{
        [UploadPhotoHandler likeDish:dish.isLike dishId:dish.dishId andBlock:^{
        if(self.homeTableViewCellDelegate){
            if (!dish.isLike) {
                [[Amplitude instance] logEvent:@"UC - UnLike Photo Succeed" withEventProperties:eventProperties];
            }else{
                [[Amplitude instance] logEvent:@"UC - Like Photo Succeed" withEventProperties:eventProperties];
            }
        }

        }failure:^(NSString *exceptionMsg){
            if([exceptionMsg isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
                [[AppDelegate ShareApp] goToLoginController];
            }
        }];
        dish.isLike = !dish.isLike;
        if(dish.isLike){
            dish.likeCount=[NSNumber numberWithInt:[dish.likeCount intValue]+1];
           
            [[Amplitude instance] logEvent:@"UC - Like Photo Initiate" withEventProperties:eventProperties];
        }else{
            dish.likeCount=[NSNumber numberWithInt:[dish.likeCount intValue]-1];
            
            [[Amplitude instance] logEvent:@"UC - UnLike Photo Initiate" withEventProperties:eventProperties];
        }}

    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self likeButtonTapped:button entity:dish];
    }

}
-(void)likeCommentShareView:(UIView *)likeCommentShareView shareButtonTapped:(UIButton *)button{
    [self userPhotoCardAmplitude];
    if (dishArrM.count>1) {
        if (self.homeTableViewCellDelegate) {
            [self.homeTableViewCellDelegate homeCardTableViewCell:self shareMutiPhotosCardButtonTapped:button entity:dish];
            return;
        }
    }
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self shareButtonTapped:button entity:dish];
    }
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView commentButtonTapped:(UIButton *)button{
    //home card cell都留空
    
    NSDictionary *dataDic = [[NSDictionary alloc] initWithObjectsAndKeys:_restaurant,@"restaurant",dishArrM,@"dataArr",userM,@"user", nil];
    [self.homeTableViewCellDelegate goToCardDetailPageWithType:15 andData:dataDic andCellIndexPath:self.fromCellIndexPath];
}


- (void)likeCommentShareView:(UIView*)likeCommentShareView_ commentInputView:(UIView *)commentInputView inputViewExpand:(UITextView *)inputTextView commentInputViewYOffset:(CGFloat)commentInputViewYOffset{
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self likeCommentShareView:likeCommentShareView_ commentInputView:commentInputView inputViewExpand:inputTextView commentInputViewYOffset:commentInputViewYOffset];
    }
}

- (void)likeCommentShareView:(UIView*)likeCommentShareView_ commentInputView:(UIView *)commentInputView postButtonTapped:(UIButton *)postButton commentInputText:(NSString *)text{
    
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    postButton.enabled = NO;
    if (dishArrM.count>1) {
        if (self.homeTableViewCellDelegate) {
            
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:dish.dishId forKey:@"Photo_ID"];
            [eventProperties setValue:dish.userId forKey:@"UploaderUser_ID"];
            [eventProperties setValue:_restaurant.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:@"Photo card on homepage" forKey:@"Location"];
            
            [UploadPhotoHandler postCommentCard:text moderateReviewId:userM.moderateReviewId restaurantId:_restaurant.restaurantId andBlock:^(IMGDishComment *dishComment) {
                postButton.enabled = YES;
                [eventProperties setValue:dishComment.commentId forKey:@"Comment_ID"];
                [[Amplitude instance] logEvent:@"UC - Comment Photo Succeed" withEventProperties:eventProperties];
                [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Photo Succeed" withValues:eventProperties];

                _restaurant.commentCardCount=[NSNumber numberWithInt:([_restaurant.commentCardCount intValue]+1)];
                if(_restaurant.commentCardList){
                    if([_restaurant.commentCardList isKindOfClass:[NSArray class]]){
                        _restaurant.commentCardList=[NSMutableArray arrayWithArray: _restaurant.commentCardList];
                    }
                    [_restaurant.commentCardList insertObject:dishComment atIndex:0];
                }else{
                    _restaurant.commentCardList = [[NSMutableArray alloc]initWithObjects:dishComment, nil];
                }
               
                [self.homeTableViewCellDelegate homeCardTableViewCell:self newCommentPosted:dishComment];

            } failure:^(NSString *errorReason){
                if([errorReason isLogout]){
                    [[LoadingView sharedLoadingView] stopLoading];
                    [[IMGUser currentUser]logOut];
                    [[AppDelegate ShareApp] goToLoginController];
                }
                [eventProperties setValue:errorReason forKey:@"Reason"];
                [[Amplitude instance] logEvent:@"UC – Comment Photo Failed" withEventProperties:eventProperties];
            }];
            [[Amplitude instance] logEvent:@"UC - Comment Photo Initiate" withEventProperties:eventProperties];
        }
    }
    else{
    if(self.homeTableViewCellDelegate){
        
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:dish.dishId forKey:@"Photo_ID"];
        [eventProperties setValue:dish.userId forKey:@"UploaderUser_ID"];
        [eventProperties setValue:dish.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:@"Photo card on homepage" forKey:@"Location"];
        
        [UploadPhotoHandler postComment:text dishId:dish.dishId restaurantId:_restaurant.restaurantId andBlock:^(IMGDishComment *dishComment) {
            postButton.enabled = YES;
            [eventProperties setValue:dishComment.commentId forKey:@"Comment_ID"];
            [[Amplitude instance] logEvent:@"UC - Comment Photo Succeed" withEventProperties:eventProperties];
            [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Photo Succeed" withValues:eventProperties];

            dish.commentCount=[NSNumber numberWithInt:([dish.commentCount intValue]+1)];
            if(dish.commentList){
                if([dish.commentList isKindOfClass:[NSArray class]]){
                    dish.commentList=[NSMutableArray arrayWithArray:dish.commentList];
                }
                [dish.commentList insertObject:dishComment atIndex:0];
            }else{
                dish.commentList = [[NSMutableArray alloc]initWithObjects:dishComment, nil];
            }
         
            [self.homeTableViewCellDelegate homeCardTableViewCell:self newCommentPosted:dishComment];
            
        } failure:^(NSString *errorReason){
            [eventProperties setValue:errorReason forKey:@"Reason"];
            [[Amplitude instance] logEvent:@"UC – Comment Photo Failed" withEventProperties:eventProperties];
        }];
        [[Amplitude instance] logEvent:@"UC – Comment Photo Initiate" withEventProperties:eventProperties];
        
    }
    }
    
}

- (void)likeCommentShareView:(UIView*)likeCommentShareView_ commentInputView:(UIView *)view inputViewDidBeginEditing:(UITextView *)textView{
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self likeCommentShareView:likeCommentShareView_ commentInputView:view inputViewDidBeginEditing:textView];
    }
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView commentCell:(UITableViewCell *)cell readMoreTapped:(id)comment offset:(CGFloat)offset{
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self commentCell:cell readMoreTapped:comment offset:offset];
    }
}

-(void)cardPhotosViewImageClick:(int)imageIndex{
    userM.userName = [userM.userName filterHtml];
    [self.homeUploadPhotoTableViewCellDelegate homeUploadPhotoTableViewCellImageClick:_restaurant andUser:userM andDishArr:dishArrM andIndex:imageIndex andFromCellIndexPath:self.fromCellIndexPath];
}
- (void)cardPhotosChangeHeight:(CGFloat)height
{
    if ([dish.imageHeight floatValue] != ONE_PHOTO_HEIGHT)
    {
        return;
    }
    if (height <= ONE_PHOTO_HEIGHT)
    {
        return;
    }

    dish.imageHeight = [NSNumber numberWithFloat:height];
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewReloadCell:self];
    }
}
- (void)attributedLabel:(TYAttributedLabel *)attributedLabel textStorageClicked:(id<TYTextStorageProtocol>)TextRun atPoint:(CGPoint)point{


    if ([TextRun isKindOfClass:[TYLinkTextStorage class]]) {
        
        NSString *linkStr = ((TYLinkTextStorage*)TextRun).linkData;
        
        if ([linkStr hasPrefix:@"user"]) {
            [self.homeTableViewCellDelegate gotoOtherProfileWithUser:userM];

        }else {
            if ( (_restaurant.restaurantId !=nil) && (![@(0) isEqualToNumber:_restaurant.restaurantId]) ) {
                [self.homeTableViewCellDelegate gotoRestaurantWithRestaurantId:_restaurant.restaurantId];
            }
            
        }
        [self userPhotoCardAmplitude];
    }

}

#pragma mark - click restaurant name
- (void)restaurantTap:(UITapGestureRecognizer *)tap{
    if ( (_restaurant.restaurantId !=nil) && (![@(0) isEqualToNumber:_restaurant.restaurantId]) ) {
        [self.homeTableViewCellDelegate gotoRestaurantWithRestaurantId:_restaurant.restaurantId];
    }
    [self userPhotoCardAmplitude];
}

- (void)heartClick:(UIButton *)btn{
    if (self.homeTableViewCellDelegate) {
        [self.homeTableViewCellDelegate homeCardTableViewReloadCell:self.fromCellIndexPath entity:_restaurant];
    }
}
-(void)userPhotoCardAmplitude{
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:_restaurant.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:userM.userId forKey:@"UploaderUser_ID"];
    [eventProperties setValue:@"Homepage" forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL - User Photo Card" withEventProperties:eventProperties];
}

@end
