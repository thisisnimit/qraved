//
//  HomeReviewTableViewCell.h
//  Qraved
//
//  Created by lucky on 16/3/1.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"
#import "IMGUser.h"
#import "IMGReview.h"
#import "HomeTableViewCell.h"
#import "IMGReviewComment.h"
#import "UILikeCommentShareView.h"
#import "CardPhotosView.h"
#import "IMGReviewCard.h"
#import "V2_LikeCommentShareView.h"
#import "TYAttributedLabel.h"
#define LEFTMARGIN 15
#define RIGHTMARGIN 30
#define PROFILEIMAGEVIEWSIZE 20

@protocol HomeReviewTableViewCellDelegate <NSObject>
@optional
- (void)uploadPhotoButtonClickWithRestaurant:(IMGRestaurant *)restaurant andReview:(IMGReview *)review;
- (void)HomeReviewTableViewCellImageClick:(IMGRestaurant *)restaurant andReview:(IMGReview *)review andDishArr:(NSArray *)dishArr andIndex:(int)index;
- (void)HomeReviewTableViewCellImageClick:(IMGRestaurant *)restaurant andReview:(IMGReview *)review andDishArr:(NSArray *)dishArr andIndex:(int)index andFromCellIndexPath:(NSIndexPath*)fromCellIndexPath;
- (void)HomeReviewTableCell:(UITableViewCell *)tableViewCell readMoreTapped:(id)review indexPath:(NSIndexPath*)path;
@end


@interface HomeReviewTableViewCell : HomeTableViewCell<LikeCommentShareDelegate,CardPhotosViewDelegate,TYAttributedLabelDelegate,V2_LikeCommentShareViewDelegete>

@property (nonatomic, strong) IMGReviewCard *reviewCard;
@property(nonatomic,strong) IMGRestaurant *restaurant;
@property (nonatomic,weak) id<HomeReviewTableViewCellDelegate>homeReviewDelegate;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier;


@end
