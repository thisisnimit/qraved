//
//  HomeTrendingRestTableViewCell.m
//  Qraved
//
//  Created by System Administrator on 2/24/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "HomeTrendingRestTableViewCell.h"
#import "DLStarRatingControl.h"
#import "IMGRestaurant.h"
#import "RestaurantActivityItemProvider.h"
#import "TwitterActivityItemProvider.h"
#import "SendCall.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "UIImage+Helper.h"
#import "UIConstants.h"
#import "OfferLabel.h"
#import "UIImageView+WebCache.h"
#import "UILabel+Helper.h"
#import "Amplitude.h"
#import "UIImage+Resize.h"
#import "Date.h"

#define RESTAURANT_IMAGE_HEIGHT 160

@implementation HomeTrendingRestTableViewCell{
	UILabel *headerLabel;
	UILabel *dateLabel;
	UILabel *titleLabel;
	UIImageView *restaurantImageView;
    UIButton *saveBtn;
	UILabel *cuisineAndDistrictLabel;
	DLStarRatingControl *ratingScoreControl;
	UILabel *ratingCount;
    UILabel *listLabel;
    UIView *btnView;
    UIView *separateView;
    UILabel *lineLabel;
    UIButton *heartBtn;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
	if(self=[super initWithStyle:style reuseIdentifier:identifier]){
 
        [self createUI];

	}
	return self;
}

- (void)createUI{
    headerLabel = [UILabel new];
    headerLabel.font = [UIFont systemFontOfSize:14];
    
    dateLabel = [UILabel new];
    dateLabel.font = [UIFont systemFontOfSize:12];
    dateLabel.textColor = [UIColor color999999];
    dateLabel.textAlignment = NSTextAlignmentRight;
    
    UIImageView *sepImageView = [UIImageView new];
    sepImageView.image = [UIImage imageNamed:@"search_line"];
    
    restaurantImageView = [UIImageView new];
    restaurantImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(restaurantImageViewTapped)];
    [restaurantImageView addGestureRecognizer:tap];
    
    heartBtn = [UIButton new];
    [heartBtn addTarget:self action:@selector(heartClick:) forControlEvents:UIControlEventTouchUpInside];
    [heartBtn setImage:[UIImage imageNamed:@"ic_heart_full"] forState:UIControlStateNormal];
    [restaurantImageView addSubview:heartBtn];
    
    titleLabel = [UILabel new];
    titleLabel.font=[UIFont systemFontOfSize:14];
    titleLabel.textColor=[UIColor color333333];
    titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
    
    //star level
    ratingScoreControl=[[DLStarRatingControl alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 36, 90, 14) andStars:5 andStarWidth:13 isFractional:YES spaceWidth:5];
    ratingScoreControl.userInteractionEnabled=NO;
    
    ratingCount = [UILabel new];
    ratingCount.font = [UIFont systemFontOfSize:12];
    ratingCount.textColor = [UIColor color999999];
    
    cuisineAndDistrictLabel = [UILabel new];
    cuisineAndDistrictLabel.numberOfLines=0;
    cuisineAndDistrictLabel.lineBreakMode=NSLineBreakByTruncatingTail;
    cuisineAndDistrictLabel.font = [UIFont systemFontOfSize:12];
    cuisineAndDistrictLabel.textColor = [UIColor color999999];
    
    UIImageView *bottomSepImageView = [UIImageView new];
    bottomSepImageView.image = [UIImage imageNamed:@"search_line"];
    
    btnView=[UIView new];
    btnView.userInteractionEnabled=YES;
    btnView.backgroundColor=[UIColor colorWithRed:251/255.0f green:252/255.0f blue:255/255.0f alpha:1];
    NSArray *titleArr = @[@"Call",@"View Map",@"Share"];
    NSArray *imageArr = @[@"ic_home_call",@"ic_home_map",@"ic_home_share"];
    
    NSMutableArray *tempArr = [NSMutableArray array];
    for (int i = 0; i < imageArr.count; i ++) {
        UIButton *btn = [UIButton new];
        [btnView addSubview:btn];
        
        btn.tag = i;
        btn.titleLabel.font = [UIFont systemFontOfSize:12];
        [btn setTitleColor:[UIColor color999999] forState:UIControlStateNormal];
        [btn setTitle:titleArr[i] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:imageArr[i]] forState:UIControlStateNormal];
        btn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        [btn addTarget:self action:@selector(btnClick_btn:) forControlEvents:UIControlEventTouchUpInside];
        
        btn.sd_layout
        .heightIs(50);
        [tempArr addObject:btn];
    }
    
    [btnView setupAutoMarginFlowItems:tempArr withPerRowItemsCount:3 itemWidth:DeviceWidth/3 verticalMargin:0 verticalEdgeInset:0 horizontalEdgeInset:0];
    
    
    separateView = [UIView new];
    separateView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    
    [self.contentView sd_addSubviews:@[headerLabel,dateLabel,sepImageView,restaurantImageView,titleLabel,ratingScoreControl,ratingCount,cuisineAndDistrictLabel,bottomSepImageView,btnView,separateView]];
    
    headerLabel.sd_layout
    .leftSpaceToView(self.contentView,15)
    .topSpaceToView(self.contentView,15)
    .widthIs(200)
    .heightIs(16);
    
    dateLabel.sd_layout
    .rightSpaceToView(self.contentView,15)
    .topSpaceToView(self.contentView,15)
    .leftSpaceToView(headerLabel,10)
    .heightIs(16);
    
    sepImageView.sd_layout
    .leftSpaceToView(self.contentView,15)
    .rightSpaceToView(self.contentView,15)
    .topSpaceToView(headerLabel,15)
    .heightIs(1);
    
    restaurantImageView.sd_layout
    .topSpaceToView(sepImageView,10)
    .leftEqualToView(sepImageView)
    .rightEqualToView(sepImageView)
    .heightIs((DeviceWidth-LEFTLEFTSET*2)/69*43);
    
    heartBtn.sd_layout
    .topSpaceToView(restaurantImageView,0)
    .rightSpaceToView(restaurantImageView,0)
    .widthIs(40)
    .heightIs(34);
    
    titleLabel.sd_layout
    .topSpaceToView(restaurantImageView,5)
    .leftEqualToView(restaurantImageView)
    .rightEqualToView(restaurantImageView)
    .autoHeightRatio(0);
    
    [titleLabel setMaxNumberOfLinesToShow:2];
    
    ratingScoreControl.sd_layout
    .leftEqualToView(titleLabel)
    .topSpaceToView(titleLabel,5)
    .widthIs(90)
    .heightIs(14);
    
    ratingCount.sd_layout
    .leftSpaceToView(ratingScoreControl,5)
    .topEqualToView(ratingScoreControl)
    .heightIs(14)
    .widthIs(DeviceWidth-LEFTLEFTSET*2-95);
    
    cuisineAndDistrictLabel.sd_layout
    .topSpaceToView(ratingScoreControl,5)
    .leftEqualToView(ratingScoreControl)
    .widthIs(DeviceWidth-LEFTLEFTSET*2)
    .heightIs(14);
    
    bottomSepImageView.sd_layout
    .topSpaceToView(cuisineAndDistrictLabel,20)
    .leftEqualToView(cuisineAndDistrictLabel)
    .rightEqualToView(cuisineAndDistrictLabel)
    .heightIs(1);
    
    btnView.sd_layout
    .topSpaceToView(bottomSepImageView,0)
    .leftSpaceToView(self.contentView,0)
    .rightSpaceToView(self.contentView,0)
    .heightIs(50);
    
    separateView.sd_layout
    .topSpaceToView(btnView,0)
    .leftEqualToView(btnView)
    .rightEqualToView(btnView)
    .heightIs(20);

}

-(void)setRestaurant:(IMGRestaurant *)restaurant {
    
    _restaurant=restaurant;
    
    if ( (restaurant.typeName == nil) || ([restaurant.typeName isEqualToString:@""])) {
        headerLabel.text = @"Trending Restaurant";
    }else{
        headerLabel.text = restaurant.typeName;
    }
    if ((restaurant.timeline == nil) ||([restaurant.timeline longLongValue] == 0)) {
        dateLabel.text = @"";
    }else{
        dateLabel.text=[Date getTimeInteval_v4:[restaurant.timeline longLongValue]/1000];
    }
    
    
    if ([restaurant.saved isEqual:@1]) {
        [heartBtn setImage:[UIImage imageNamed:@"ic_heart_full"] forState:UIControlStateNormal];
    }else{
        [heartBtn setImage:[UIImage imageNamed:@"ic_unheart_full"] forState:UIControlStateNormal];
    }

    NSString *restTitle=[_restaurant.title filterHtml];
    titleLabel.text=restTitle;
    [titleLabel updateLayout];
    
    NSString *cuisineAndDistrictText=[NSString stringWithFormat:@"%@ / %@ / %@",_restaurant.cuisineName,_restaurant.districtName,_restaurant.priceName];

    cuisineAndDistrictLabel.text=cuisineAndDistrictText;
    
    [ratingScoreControl updateRating:[NSNumber numberWithFloat:[restaurant.ratingScore floatValue]*2]];
    
    NSString *distance = [restaurant distance];
    if ([distance isEqualToString:@""]) {
        ratingCount.text = [NSString stringWithFormat:@"(%@)",_restaurant.reviewCount];
    }else{
        ratingCount.text = [NSString stringWithFormat:@"(%@) • %@",_restaurant.reviewCount,distance];
    }


    NSString *urlString = [_restaurant.imageUrl ImgURLStringWide:DeviceWidth-2*LEFTLEFTSET High:(DeviceWidth-LEFTLEFTSET*2)/69*43];
    NSLog(@"urlString = %@",urlString);
    
    __weak typeof(restaurantImageView) weakRestaurantImageView = restaurantImageView;
    UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, (DeviceWidth-LEFTLEFTSET*2)/69*43)];
    UIImage* cacheImg = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:urlString];
    if (cacheImg==nil) {
        [restaurantImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:placeHoderImage options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [[SDImageCache sharedImageCache] clearMemory];
             [[SDImageCache sharedImageCache] setValue:nil forKey:@"memCache"];
            image = [image imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, (DeviceWidth-LEFTLEFTSET*2)/69*43)];
            
            [weakRestaurantImageView setImage:image];
        }];
    }else{
        restaurantImageView.image = [cacheImg imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, (DeviceWidth-LEFTLEFTSET*2)/69*43)];
    }

    

    [self setupAutoHeightWithBottomView:separateView bottomMargin:0];
    
}
-(void)btnClick_btn:(UIButton*)button{
    [self btnClick:button.tag withButton:button];
}

-(void)btnClick:(NSInteger)index withButton:(UIView*)button{
    switch(index){
        case 0:{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:_restaurant.phoneNumber delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
            [alertView show];
            break;    
        }
        case 1:{
            [self.delegate mapBtnClick:_restaurant];
            break;
        }
        case 2:{
            NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@%@/%@",QRAVED_WEB_SERVER_OLD,[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT],_restaurant.seoKeyword]];
            NSString *shareContentString = [NSString stringWithFormat:@"%@ on Qraved. %@",_restaurant.title,url];
            // NSString *shareContentString=[NSString stringWithFormat:@"%@",url];
            RestaurantActivityItemProvider *provider=[[RestaurantActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
            provider.url=url;
            provider.title=shareContentString;
            provider.districtName = _restaurant.districtName;
            provider.restaurantTitle=_restaurant.title;
            provider.cuisineAreaPrice=[NSString stringWithFormat:@"%@ / %@ / %@",_restaurant.cuisineName,_restaurant.districtName,_restaurant.priceName];
            TwitterActivityItemProvider *twitterProvider=[[TwitterActivityItemProvider alloc] initWithPlaceholderItem:@""];
            twitterProvider.title=[NSString stringWithFormat:L(@"Look! It ’s %@ on #Qraved!"),_restaurant.title];
            UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterProvider] applicationActivities:nil];
            activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
            [activityCtl setValue:[NSString stringWithFormat:L(@"I Found %@ on Qraved!"),_restaurant.title] forKey:@"subject"];
            activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
                if (completed) {
                    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                    [eventProperties setValue:_restaurant.restaurantId forKey:@"Restaurant_ID"];
                    [eventProperties setValue:_restaurant.title forKey:@"RestaurantTitle"];
                    [eventProperties setValue:@"Restaurant trending card on homepage" forKey:@"Location"];
                    [eventProperties setValue:provider.activityType forKey:@"Channel"];

                    [[Amplitude instance] logEvent:@"SH - Share Restaurant" withEventProperties:eventProperties];
                }else{
                }
            };
            if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
                // iOS8
                activityCtl.popoverPresentationController.sourceView =
                button;
            }

            [self.delegate shareBtnClick:activityCtl];
            break;
        }

    }
}

-(void)saveBtnClick:(UIButton*)btn{
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:_restaurant.restaurantId forKey:@"RestaurantID"];
    [eventProperties setValue:_restaurant.title forKey:@"RestaurantTitle"];
    [eventProperties setValue:@"Homepage" forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL - Trending Restaurant Card" withEventProperties:eventProperties];
    
    if (self.homeTrendingRestTableViewCellDelegate && [self.homeTrendingRestTableViewCellDelegate respondsToSelector:@selector(homeTrendingSaveBtnClick: andRestaurant:)]) {
        [self.homeTrendingRestTableViewCellDelegate homeTrendingSaveBtnClick:btn.selected andRestaurant:_restaurant];
    }
}



-(void)alterView:(UIAlertView*)alertView clickButtonAtIndex:(NSInteger)index{
    if(alertView.tag==1){
        if(index==0){
            [alertView dismissWithClickedButtonIndex:0 animated:NO];
        }else if (index==1){
            [SendCall sendCall:alertView.message];
        }
    }
}

- (void)heartClick:(UIButton *)btn{
    if (self.delegate && [self.delegate respondsToSelector:@selector(homeCardTableViewReloadCell:entity:)]) {
        [self.delegate homeCardTableViewReloadCell:self.fromCellIndexPath entity:_restaurant];
    }
}

-(void)restaurantImageViewTapped{
    if (self.delegate && [self.delegate respondsToSelector:@selector(gotoRestaurantWithRestaurantId:)]) {
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:_restaurant.restaurantId forKey:@"RestaurantID"];
        [eventProperties setValue:_restaurant.title forKey:@"RestaurantTitle"];
        [eventProperties setValue:@"Homepage" forKey:@"Location"];
        [[Amplitude instance] logEvent:@"CL - Trending Restaurant Card" withEventProperties:eventProperties];
        [self.delegate gotoRestaurantWithRestaurantId:_restaurant.restaurantId];
        
    }
}

@end
