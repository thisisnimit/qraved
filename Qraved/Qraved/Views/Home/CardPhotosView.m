//
//  CardPhotosView.m
//  Qraved
//
//  Created by lucky on 16/3/4.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "CardPhotosView.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "UIDevice+Util.h"
#import "UIImageView+WebCache.h"
#import "IMGDish.h"
#import "SinglePhotoViewController.h"
#import "IMGRestaurantEvent.h"
#import "NSString+Helper.h"
#import "EntityKind.h"
#import "UIImage+Size.h"
#import "UIImage+Helper.h"
#import "Amplitude.h"
#import "UIImageView+Helper.h"

@interface CardPhotosView(){
    NSMutableArray *photosArr;
    IMGType imgType;
}

@end

@implementation CardPhotosView
//压缩图片
- (UIImage *)image:(UIImage*)image scaledToSize:(CGSize)newSize
{
    // Create a graphics image context
    UIGraphicsBeginImageContext(newSize);
    // Tell the old image to draw in this new context, with the desired
    // new size
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    // Get the new image from the context
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    // End the context
    UIGraphicsEndImageContext();
    // Return the new image.
    return newImage;
}
- (void)setPhotos:(NSArray *)_photosArr
{
    photosArr=[NSMutableArray arrayWithArray:_photosArr];
  UITapGestureRecognizer*tap=  [[UITapGestureRecognizer alloc] init];
    [tap addTarget:self action:@selector(imageViewClicked:)];
    [self addGestureRecognizer:tap];

    imgType = [EntityKind typeKindFromEntity:[photosArr firstObject]];
    
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    switch (photosArr.count)
    {
        case 0:{
            UIImageView *imageView = [[UIImageView alloc] init];
            imageView.frame = CGRectMake(0, 0, DeviceWidth - LEFTLEFTSET*2, 0);
            [imageView sd_setImageWithURL:nil placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
            [self addSubview:imageView];
            
             [self setupAutoHeightWithBottomView:imageView bottomMargin:0];
        }
            break;
        case 1:
        {

            IMGEntity *dish = [photosArr objectAtIndex:0];
            UIImageView *imageView = [UIImageView new];
            imageView.tag = 0;
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            imageView.clipsToBounds = YES;
            UITapGestureRecognizer *imageViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClicked:)];
            imageView.userInteractionEnabled = YES;
            [imageView addGestureRecognizer:imageViewTapGesture];
            
            [self addSubview:imageView];

            
            __weak typeof(imageView) weakImageView = imageView;
            UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth, self.cardPhotoHeight)];
    
             UIImage* cacheImg = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[[dish valueForKey:@"imageUrl"] returnFullImageUrlWithWidth:DeviceWidth]];
            if (cacheImg==nil) {
                [imageView sd_setImageWithURL:[NSURL URLWithString:[[dish valueForKey:@"imageUrl"] returnFullImageUrlWithWidth:DeviceWidth]] placeholderImage:placeHoderImage options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    [[SDImageCache sharedImageCache] clearMemory];
                     [[SDImageCache sharedImageCache] setValue:nil forKey:@"memCache"];
                    if (image) {
                        image = [image imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth, self.cardPhotoHeight)];
                        [weakImageView setImage:image];
                        
                    }
                    
                }];
            }else{
                imageView.image = [cacheImg imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth, self.cardPhotoHeight)];
            }
           

//            [imageView sd_setImageWithURL:[NSURL URLWithString:[[dish valueForKey:@"imageUrl"]returnFullCDNImageUrlWithWidth:DeviceWidth]] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                if (!image||error) {
//                    [imageView sd_setImageWithURL:[NSURL URLWithString:[[dish valueForKey:@"imageUrl"] returnFullImageUrlWithWidth:DeviceWidth]] placeholderImage:placeHoderImage options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                        if (image) {
//                            image = [image imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth, self.cardPhotoHeight)];
//                            [weakImageView setImage:image];
//                            
//                        }
//                        
//                    }];
//                    
//                }else{
//                    image = [image imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth, self.cardPhotoHeight)];
//                    [weakImageView setImage:image];
//                }
//                
//            }];
            
            imageView.sd_layout
            .topSpaceToView(self, 0)
            .leftSpaceToView(self, 0)
            .rightSpaceToView(self, 0)
            .heightIs(self.cardPhotoHeight);
            
            [self setupAutoHeightWithBottomView:imageView bottomMargin:0];
        }
            break;
        case 2:
        {
            UIView *bgView = [UIView new];
            [self addSubview:bgView];
            CGFloat imageSize = (DeviceWidth-4)/2;
            
            NSMutableArray *tempArr = [NSMutableArray array];
            
            for (int i = 0; i<photosArr.count; i++)
            {
                UIImageView *imageView = [UIImageView new];
                [bgView addSubview:imageView];
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                imageView.tag = i;
                UITapGestureRecognizer *imageViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClicked:)];
                imageView.userInteractionEnabled = YES;
                [imageView addGestureRecognizer:imageViewTapGesture];

                IMGEntity *dish = [photosArr objectAtIndex:i];
                [self setImagUrl:[dish valueForKey:@"imageUrl"] imageWidth:imageSize imageHeight:imageSize imageView:imageView];

                imageView.sd_layout
                .heightIs(imageSize);
                
                [tempArr addObject:imageView];
            }
            
            [bgView setupAutoMarginFlowItems:tempArr withPerRowItemsCount:2 itemWidth:imageSize verticalMargin:0 verticalEdgeInset:0 horizontalEdgeInset:0];
            
            bgView.sd_layout
            .topSpaceToView(self, 0)
            .leftSpaceToView(self, 0)
            .rightSpaceToView(self, 0);
            
            [self setupAutoHeightWithBottomView:bgView bottomMargin:0];
        }
            break;

        case 3:
        {
            
            UIImageView *topImageView = [UIImageView new];
            topImageView.contentMode = UIViewContentModeScaleAspectFill;
            topImageView.tag = 0;
            UITapGestureRecognizer *imageViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClicked:)];
            topImageView.userInteractionEnabled = YES;
            [topImageView addGestureRecognizer:imageViewTapGesture];
            [self addSubview:topImageView];
             IMGEntity *dish = [photosArr objectAtIndex:0];
            [self setImagUrl:dish.imageUrl imageWidth:DeviceWidth imageHeight:DeviceWidth/2 imageView:topImageView];
            topImageView.sd_layout
            .topSpaceToView(self, 0)
            .leftSpaceToView(self, 0)
            .rightSpaceToView(self, 0)
            .heightIs(DeviceWidth/2);

            UIView *bottomView = [UIView new];
            [self addSubview:bottomView];
            NSMutableArray *tempArr = [NSMutableArray array];
            for (int i = 1; i<photosArr.count; i++)
            {
                UIImageView *imageView = [UIImageView new];
                [bottomView addSubview:imageView];
                
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                imageView.tag = i;
               UITapGestureRecognizer *imageViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClicked:)];
                imageView.userInteractionEnabled = YES;
               [imageView addGestureRecognizer:imageViewTapGesture];
                
                
                IMGEntity *dish = [photosArr objectAtIndex:i];
                [self setImagUrl:dish.imageUrl imageWidth:(DeviceWidth-4)/2 imageHeight:(DeviceWidth-4)/2 imageView:imageView];
                
                imageView.sd_layout
                .heightIs((DeviceWidth-4)/2);
                
                [tempArr addObject:imageView];
            }
            
            [bottomView setupAutoMarginFlowItems:tempArr withPerRowItemsCount:2 itemWidth:(DeviceWidth-4)/2 verticalMargin:0 verticalEdgeInset:0 horizontalEdgeInset:0];
            
            bottomView.sd_layout
            .topSpaceToView(topImageView, 4)
            .leftEqualToView(topImageView)
            .rightEqualToView(topImageView);
            
            [self setupAutoHeightWithBottomView:bottomView bottomMargin:0];
            

        }
            break;

        case 4:
        {
            
            UIView *bgView = [UIView new];
            [self addSubview:bgView];
            CGFloat imageSize = (DeviceWidth-4)/2;
            
            NSMutableArray *tempArr = [NSMutableArray array];
            for (int i = 0; i<photosArr.count; i++)
            {
                UIImageView *imageView = [[UIImageView alloc] init];
                
                [bgView addSubview:imageView];
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                
                imageView.tag = i;
                UITapGestureRecognizer *imageViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClicked:)];
                imageView.userInteractionEnabled = YES;
                [imageView addGestureRecognizer:imageViewTapGesture];
                
                IMGEntity *dish = [photosArr objectAtIndex:i];
              
                [self setImagUrl:dish.imageUrl imageWidth:imageSize imageHeight:imageSize imageView:imageView];
                
                imageView.sd_layout
                .heightIs(imageSize);
                
                [tempArr addObject:imageView];
            }
            
            [bgView setupAutoMarginFlowItems:tempArr withPerRowItemsCount:2 itemWidth:imageSize verticalMargin:4 verticalEdgeInset:0 horizontalEdgeInset:0];
            
            bgView.sd_layout
            .topSpaceToView(self, 0)
            .leftSpaceToView(self, 0)
            .rightSpaceToView(self, 0);
            
            [self setupAutoHeightWithBottomView:bgView bottomMargin:0];
        }
            break;
            
        default:
        {
            
            UIView *topView = [UIView new];
            [self addSubview:topView];
            
            NSMutableArray *topArr = [NSMutableArray array];
            for (int i = 0; i < 2; i ++) {
                UIImageView *imageView = [UIImageView new];
                [topView addSubview:imageView];
                
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                
                imageView.tag = i;
                UITapGestureRecognizer *imageViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClicked:)];
                imageView.userInteractionEnabled = YES;
                [imageView addGestureRecognizer:imageViewTapGesture];
                
                IMGEntity *dish = [photosArr objectAtIndex:i];
                
                [self setImagUrl:dish.imageUrl imageWidth:(DeviceWidth-4)/2 imageHeight:(DeviceWidth-4)/2 imageView:imageView];
                
                imageView.sd_layout
                .heightIs((DeviceWidth-4)/2);
                
                [topArr addObject:imageView];
            }
            
            [topView setupAutoMarginFlowItems:topArr withPerRowItemsCount:2 itemWidth:(DeviceWidth-4)/2 verticalMargin:0 verticalEdgeInset:0 horizontalEdgeInset:0];
            
            topView.sd_layout
            .topSpaceToView(self, 0)
            .leftSpaceToView(self, 0)
            .rightSpaceToView(self, 0);
            
            UIView *bottomView = [UIView new];
            [self addSubview:bottomView];
            
            NSMutableArray *bottomArr = [NSMutableArray array];
            for (int i = 2; i < 5; i ++) {
                UIImageView *imageView = [UIImageView new];
                [bottomView addSubview:imageView];
                
                imageView.contentMode = UIViewContentModeScaleAspectFill;
                
                imageView.tag = i;
                UITapGestureRecognizer *imageViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClicked:)];
                imageView.userInteractionEnabled = YES;
                [imageView addGestureRecognizer:imageViewTapGesture];
                
                IMGEntity *dish = [photosArr objectAtIndex:i];
                
                CGFloat imageSize = (DeviceWidth-8)/3;
                [self setImagUrl:dish.imageUrl imageWidth:imageSize imageHeight:imageSize imageView:imageView];
                
                
                if (i==4 && photosArr.count-5 > 0) {
                    UIView *fillView = [UIView new];
                    fillView.backgroundColor = [UIColor blackColor];
                    fillView.alpha = 0.4;
                    [imageView addSubview:fillView];
                    
                    UILabel *lblNum = [UILabel new];
                    lblNum.font = [UIFont systemFontOfSize:24];
                    lblNum.textColor = [UIColor whiteColor];
                    lblNum.text = [NSString stringWithFormat:@"+%ld",photosArr.count-5];
                    lblNum.textAlignment = NSTextAlignmentCenter;
                    [imageView addSubview:lblNum];
                    
                    fillView.sd_layout
                    .topSpaceToView(imageView, 0)
                    .leftSpaceToView(imageView, 0)
                    .rightSpaceToView(imageView, 0)
                    .bottomSpaceToView(imageView, 0);
                    
                    lblNum.sd_layout
                    .centerXEqualToView(imageView)
                    .centerYEqualToView(imageView)
                    .heightIs(28)
                    .widthIs(80);
                }
                
                imageView.sd_layout
                .heightIs(imageSize);
                
                [bottomArr addObject:imageView];
                
            }
            
            [bottomView setupAutoMarginFlowItems:bottomArr withPerRowItemsCount:3 itemWidth:(DeviceWidth-8)/3 verticalMargin:0 verticalEdgeInset:0 horizontalEdgeInset:0];
            bottomView.sd_layout
            .topSpaceToView(topView, 4)
            .leftEqualToView(topView)
            .rightEqualToView(topView);
            
            [self setupAutoHeightWithBottomView:bottomView bottomMargin:0];
            

        }
            break;
    }

}

- (void)setImagUrl:(NSString *)imageUrl imageWidth:(CGFloat)imageWidth imageHeight:(CGFloat)imageHeight imageView:(UIImageView *)imageView{
    UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(imageWidth,imageHeight)];
    
    __weak typeof(imageView) weakImageView = imageView;
    
     UIImage* cacheImg = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:[imageUrl returnFullImageUrlWithWidth:imageWidth andHeight:imageHeight]];
    if (cacheImg==nil) {
        [imageView sd_setImageWithURL:[NSURL URLWithString:[imageUrl returnFullImageUrlWithWidth:imageWidth andHeight:imageHeight]] placeholderImage:placeHoderImage options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
             [[SDImageCache sharedImageCache] setValue:nil forKey:@"memCache"];
            [[SDImageCache sharedImageCache] clearMemory];
            [UIView animateWithDuration:1.0 animations:^{
                [weakImageView setAlpha:1.0];
            }];
            image = [image imageByScalingAndCroppingForSize:CGSizeMake(imageWidth,imageHeight)];
            [weakImageView setImage:image];
            
        }];

    }else{
        imageView.image = [cacheImg imageByScalingAndCroppingForSize:CGSizeMake(imageWidth,imageHeight)];
    }
    
    
//    [imageView sd_setImageWithURL:[NSURL URLWithString:[imageUrl returnFullCDNImageUrlWithWidth:imageWidth andHeight:imageHeight]] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//        if (!image||error) {
//            
//            [imageView sd_setImageWithURL:[NSURL URLWithString:[imageUrl returnFullImageUrlWithWidth:imageWidth andHeight:imageHeight]] placeholderImage:placeHoderImage options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//                [UIView animateWithDuration:1.0 animations:^{
//                    [weakImageView setAlpha:1.0];
//                }];
//                image = [image imageByScalingAndCroppingForSize:CGSizeMake(imageWidth,imageHeight)];
//                [weakImageView setImage:image];
//                
//            }];
//        }else{
//            
//            [UIView animateWithDuration:1.0 animations:^{
//                [weakImageView setAlpha:1.0];
//            }];
//            image = [image imageByScalingAndCroppingForSize:CGSizeMake(imageWidth,imageHeight)];
//            [weakImageView setImage:image];
//        }
//    }];

}

-(UIColor *) randomColor
{
    CGFloat hue = ( arc4random() % 256 / 256.0 ); //0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5; // 0.5 to 1.0,away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5; //0.5 to 1.0,away from black
    return [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
}
-(void)imageViewClicked:(UITapGestureRecognizer *)tapGR{
    if(photosArr.count>1){
        [self.cardPhotosViewDelegate cardPhotosViewImageClick:(int)tapGR.view.tag+1];
    }else if(photosArr.count==1){
         IMGEntity *dish=[photosArr objectAtIndex:tapGR.view.tag];
//       [self.homeTableViewCellDelegate viewFullImage:dish];
        if(self.homeTableViewCellDelegate && [self.homeTableViewCellDelegate respondsToSelector:@selector(viewFullImage:andRestaurant:andReview:)]){
            if (self.isFromPromo && ([dish isKindOfClass:[IMGRestaurantEvent class]]) ) {
                IMGRestaurantEvent *re = (IMGRestaurantEvent *)dish;
                if ([@"Restaurant" isEqualToString:re.photoCreditTypeDic]) {
                    [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Promo Image detail" andRestaurantId:nil andRestaurantTitle:nil andLocation:@"Timeline" andOrigin:nil andType:nil];
                }
            }
            [self.homeTableViewCellDelegate viewFullImage:dish andRestaurant:self.restaurant andReview:self.review];
        }else if(self.cardPhotosViewDelegate && [self.cardPhotosViewDelegate respondsToSelector:@selector(viewFullImage:andRestaurant:andReview:)]){
            [self.cardPhotosViewDelegate viewFullImage:dish andRestaurant:self.restaurant andReview:self.review];
        }
    }
    
}

+(float)cardPhotoHeightWithPhotoCout:(NSInteger)count{
    switch (count) {
        case 1:
            return 160;
        case 2:
            return (DeviceWidth - 4)/2.0;
            
        case 3:
            return (DeviceWidth-4)/2 + DeviceWidth/2 + 4;
        case 4:
            return DeviceWidth;
//        case 5:
//            return (DeviceWidth-8)/3+4+(DeviceWidth-4)/2;
//        case 6:
//            return (DeviceWidth - LEFTLEFTSET*2-2)/3.0*2;
//            
        default:
            return (DeviceWidth-8)/3+4+(DeviceWidth-4)/2;
            break;
    }
    if (count>0) {
        return (DeviceWidth - LEFTLEFTSET*2-2)/3.0*2;
    }else{
        return 0;
    }
}
- (void)changeHeight:(CGFloat)height
{
    [self.cardPhotosViewDelegate cardPhotosChangeHeight:height];
}
@end
