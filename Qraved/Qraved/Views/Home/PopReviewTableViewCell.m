//
//  PopReviewTableViewCell.m
//  Qraved
//
//  Created by DeliveLee on 16/3/13.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "PopReviewTableViewCell.h"
#import "UIView+Helper.h"
#import "UIConstants.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"
#import "Date.h"
#import "UIColor+Helper.h"

#define LEFTMARGIN 15
#define RIGHTMARGIN 30
#define PROFILEIMAGEVIEWSIZE 20

@implementation PopReviewTableViewCell{
    UIImageView *profileImageView;
    UILabel *userNameLabel;
    UILabel *timeLabel;
    UILabel *contentLabel;
    BOOL viewMore;
    
    UIControl *userNameLabelControl;
    id commentObject;

}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
    if(self=[super initWithStyle:style reuseIdentifier:identifier]){
        
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        profileImageView = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTMARGIN, 11, PROFILEIMAGEVIEWSIZE, PROFILEIMAGEVIEWSIZE)];
        profileImageView.layer.cornerRadius=PROFILEIMAGEVIEWSIZE/2;
        profileImageView.layer.borderColor=[UIColor clearColor].CGColor;
        profileImageView.layer.borderWidth=0.0f;
        [self addSubview:profileImageView];
        
        UIControl *profileImageViewControl = [[UIControl alloc] init];
        [profileImageViewControl addTarget:self action:@selector(controlClick) forControlEvents:UIControlEventTouchUpInside];
        profileImageViewControl.frame = profileImageView.frame;
        [self addSubview:profileImageViewControl];

        
        userNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(profileImageView.endPointX+8, 5, DeviceWidth-profileImageView.endPointY-RIGHTMARGIN, 16)];
        userNameLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:12];
        userNameLabel.textColor=[[UIColor darkGrayColor] colorWithAlphaComponent:0.9];
        [self addSubview:userNameLabel];
        
        userNameLabelControl = [[UIControl alloc] init];
        [userNameLabelControl addTarget:self action:@selector(controlClick) forControlEvents:UIControlEventTouchUpInside];
        userNameLabelControl.frame = userNameLabel.frame;
        [self addSubview:userNameLabelControl];

        
        timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(userNameLabel.startPointX, userNameLabel.endPointY, userNameLabel.frame.size.width, 12)];
        timeLabel.font=[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:10];
        timeLabel.textColor=[[UIColor lightGrayColor] colorWithAlphaComponent:0.8];
        [self addSubview:timeLabel];
        
        contentLabel=[[UILabel alloc]initWithFrame:CGRectMake(userNameLabel.startPointX, timeLabel.endPointY+2, userNameLabel.frame.size.width, 100)];
        contentLabel.font=[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:10];
        contentLabel.textColor=[[UIColor darkGrayColor] colorWithAlphaComponent:0.8];
        contentLabel.textAlignment=NSTextAlignmentLeft;
        contentLabel.numberOfLines=0;
        [self addSubview:contentLabel];
        
        
    }
    return self;
}

-(void)updateUI:(id)comment{
    NSString *userName=@"";
    NSNumber *createTime=[NSNumber numberWithLongLong:[[NSDate date]timeIntervalSince1970]];
    NSString *commentStr=@"";
    NSString *imageUrl=@"";
    
    commentObject = comment;
    
    if([comment isKindOfClass:[IMGJournalComment class]]){
        IMGJournalComment* journalComment=(IMGJournalComment*)comment;
        commentStr=journalComment.content;
        createTime=journalComment.createTime;
        if (!journalComment.isVendor && [journalComment.userType intValue] == 1)
        {
            userName = journalComment.restaurantName;
            userNameLabel.textColor = [UIColor colorRed];
            imageUrl = journalComment.restaurantImageUrl;
//            imageUrl = @"";
        }
        else
        {
            userName=journalComment.userName;
            imageUrl=journalComment.userImageUrl;
        }
    }else if([comment isKindOfClass:[IMGDishComment class]]){
        IMGDishComment* dishComment=(IMGDishComment*)comment;
        commentStr=dishComment.content;
        createTime=dishComment.createTime;
        if (!dishComment.isVendor && [dishComment.userType intValue] == 1)
        {
            userName = dishComment.restaurantName;
            userNameLabel.textColor = [UIColor colorRed];
            imageUrl = dishComment.restaurantImageUrl;
        }
        else
        {
            userName=dishComment.userName;
            imageUrl=dishComment.userImageUrl;
        }

    }else if([comment isKindOfClass:[IMGReviewComment class]]){
        IMGReviewComment* reviewComment=(IMGReviewComment*)comment;
        commentStr=reviewComment.content;
        createTime=reviewComment.createTime;
        if (!reviewComment.isVendor && [reviewComment.userType intValue] == 1)
        {
            userName = reviewComment.restaurantName;
            userNameLabel.textColor = [UIColor colorRed];

            imageUrl = reviewComment.restaurantImageUrl;
        }
        else
        {
            userName=reviewComment.userName;
            imageUrl=reviewComment.userImageUrl;
        }
    }else if([comment isKindOfClass:[IMGRestaurantEventComment class]]){
        IMGRestaurantEventComment* restaurantEventComment=(IMGRestaurantEventComment*)comment;
        commentStr=restaurantEventComment.content;
        createTime=restaurantEventComment.createTime;
        if (!restaurantEventComment.isVendor&&[restaurantEventComment.userType intValue] == 1)
        {
            userName=restaurantEventComment.restaurantName;
            userNameLabel.textColor = [UIColor colorRed];
            imageUrl = restaurantEventComment.restaurantImageUrl;
        }
        else
        {
        userName=restaurantEventComment.userName;
        imageUrl=restaurantEventComment.userImageUrl;
        }
    }
    
    
    NSURL *avatar;
    if ([imageUrl isEqualToString:DEFAULTPROFILEIMAGEURL]) {
        avatar = [NSURL URLWithString:@""];
    }else if ([imageUrl hasPrefix:@"https://"]){
        avatar = [NSURL URLWithString:imageUrl];
    }else{
        avatar = [NSURL URLWithString:[imageUrl returnFullImageUrl]];
    }
    [profileImageView setImageWithURL:avatar placeholderImage:[UIImage imageNamed:@"headDefault.jpg"]];
    
    userNameLabel.text=userName;
    
    timeLabel.text = [Date getTimeInteval_v4:[createTime longLongValue]/1000];
    
    if(commentStr.length>140){
        commentStr=[NSString stringWithFormat:@"%@...",[commentStr substringToIndex:140]];
    }
    contentLabel.text=commentStr;
    CGFloat height=[PopReviewTableViewCell calculatedCommentTextHeight:commentStr];
    contentLabel.frame=CGRectMake(contentLabel.startPointX, contentLabel.startPointY, contentLabel.frame.size.width, height);
}

- (void)controlClick
{
    if ([commentObject isKindOfClass:[IMGJournalComment class]])
    {
        return;
    }
    if (self.delegate)
    {
        IMGComment *comment = (IMGComment *)commentObject;
        if (!comment.isVendor && [comment.userType intValue] == 1)
        {
            IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
            restaurant.title = comment.restaurantName;
            restaurant.restaurantId = comment.restaurantId;
            [self.delegate popReviewTableViewCellUserNameOrImageTapped:restaurant];
        }
        else
        {
            IMGUser *user = [[IMGUser alloc] init];
            user.userName = [NSString stringWithString:comment.userName];
            user.userId = comment.userId;
            user.token = @"";
            [self.delegate popReviewTableViewCellUserNameOrImageTapped:user];
        }
    }
}


+(CGFloat)calculatedCommentTextHeight:(NSString*)comment{
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:10]};
    CGSize singleSize = [@"test" boundingRectWithSize:CGSizeMake(DeviceWidth-LEFTMARGIN-PROFILEIMAGEVIEWSIZE-RIGHTMARGIN, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    CGSize textSize = [comment boundingRectWithSize:CGSizeMake(DeviceWidth-LEFTMARGIN-PROFILEIMAGEVIEWSIZE-RIGHTMARGIN, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    NSInteger lines = textSize.height/singleSize.height;
    CGFloat height=0.0f;
    if(lines>3){
        height=3*singleSize.height;
    }else{
        height=textSize.height;
    }
    return height;
}

+(CGFloat)calculatedHeight:(id)comment{
    NSString *commentStr=@"";
    if([comment isKindOfClass:[IMGJournalComment class]]){
        IMGJournalComment *journalComment=(IMGJournalComment*)comment;
        commentStr=journalComment.content;
    }else if([comment isKindOfClass:[IMGDishComment class]]){
        IMGDishComment *dishComment=(IMGDishComment*)comment;
        commentStr=dishComment.content;
    }else if([comment isKindOfClass:[IMGReviewComment class]]){
        IMGReviewComment *reviewComment=(IMGReviewComment*)comment;
        commentStr=reviewComment.content;
    }else if([comment isKindOfClass:[IMGRestaurantEventComment class]]){
        IMGRestaurantEventComment *eventComment=(IMGRestaurantEventComment*)comment;
        commentStr=eventComment.content;
    }
    
    
    if(commentStr.length>140){
        commentStr=[NSString stringWithFormat:@"%@...",[commentStr substringToIndex:140]];
    }
    CGFloat height=[PopReviewTableViewCell calculatedCommentTextHeight:commentStr];
    return height+10+16+12+15;
    
}


@end
