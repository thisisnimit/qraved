//
//  HomeEngageTableViewCell.h
//  Qraved
//
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeTableViewCell.h"
#import "IMGEngageCard.h"

@protocol HomeEngageTableViewCellDelegate <NSObject>
-(void)engageBtnClick:(HomeEngageTableViewCellType)type engageCard:(IMGEngageCard *)engageCard;
@end

@interface HomeEngageTableViewCell : HomeTableViewCell

@property (nonatomic,weak)id homeEngageTableViewCellDelegate;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier;
-(void)setEngageCard:(IMGEngageCard*)engageCard;
+(CGFloat)calculatedHeight:(IMGEngageCard *)engageCard;
@end
