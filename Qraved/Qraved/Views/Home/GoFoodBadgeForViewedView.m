//
//  GoFoodBadgeForViewedView.m
//  Qraved
//
//  Created by harry on 2018/3/5.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "GoFoodBadgeForViewedView.h"

@implementation GoFoodBadgeForViewedView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:72/255.0 green:175/255.0 blue:74/255.0 alpha:1.0];
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 2;
        [self createUI];
    }
    return self;
}

- (void)createUI{
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.font = [UIFont systemFontOfSize:10];
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.text = @"ORDER NOW ON";
    
    UIImageView *imageView = [UIImageView new];
    imageView.image = [UIImage imageNamed:@"ic_badgeGofood"];
    
    [self sd_addSubviews:@[lblTitle, imageView]];
    
    lblTitle.sd_layout
    .leftSpaceToView(self, 5)
    .topSpaceToView(self, 0)
    .bottomSpaceToView(self, 0)
    .widthIs(86);
    
    imageView.sd_layout
    .leftSpaceToView(lblTitle, 0)
    .topSpaceToView(self, 4)
    .widthIs(55)
    .heightIs(10);
}

@end
