//
//  GoFoodBadgeView.m
//  Qraved
//
//  Created by harry on 2018/3/5.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "GoFoodBadgeView.h"

@implementation GoFoodBadgeView

- (instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:72/255.0 green:175/255.0 blue:74/255.0 alpha:1.0];
        [self createUI];
    }
    return self;
}

- (void)createUI{
    UIImageView *imageView = [UIImageView new];
    imageView.image = [UIImage imageNamed:@"ic_badgeGofood"];
    [self addSubview:imageView];
    
    imageView.sd_layout
    .centerXEqualToView(self)
    .centerYEqualToView(self)
    .widthIs(55)
    .heightIs(10);
}

@end
