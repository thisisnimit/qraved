//
//  HomeMenuTableViewCell.m
//  Qraved
//
//  Created by lucky on 16/4/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "HomeMenuTableViewCell.h"
#import "UIConstants.h"
#import "DLStarRatingControl.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"
#import "IMGRestaurant.h"
#import "UILabel+Helper.h"
#import "IMGDish.h"
#import "CXAHyperlinkLabel.h"
#import "NSString+CXAHyperlinkParser.h"
#import "IMGRestaurantEvent.h"
#import "LoadingView.h"
#import "UICommentInputView.h"
#import "RestaurantHandler.h"
#import "AppDelegate.h"
#import "IMGUser.h"
#import "Date.h"
#import "HBVLinkedTextView.h"
#import "UIShareButton.h"

#define LEFT_X 15

#define TITLE_LABEL_Y 13
#define TITLE_LABEL_HEIGHT 20
#define TITLE_LABEL_FONT 15

#define GAP_BETWEEN_TITLE_AND_CUISINE 1

#define CUISINE_DISTRICT_LABEL_HEIGHT 20

#define GAP_BETWEEN_CUISINE_AND_REVIEW 3

#define CELL_RATING_STAR_WIDTH 10

#define RESTAURANT_IMAGE_SIZE 70

@interface HomeMenuTableViewCell(){
    UILabel *headerLabel;
    UILabel *dateLabel;
//    CXAHyperlinkLabel *titleLabel;
    TYAttributedLabel *titleLabel;
    UILabel *reviewCountLabel;
    UIImageView *restaurantImageView;
    UILabel *cuisineAndDistrictLabel;
    DLStarRatingControl *ratingScoreControl;
    UIImageView *reviewCountImageView;
    UILabel *scoreLabel;
    UILabel *timeLabel;
    IMGRestaurantEvent *restaurantEvent;
    CardPhotosView *cardPhotosView;
    
    UIView *separateView;
    UIButton *saveBtn;
    UILikeCommentShareView *likeCommentShareView;
    NSMutableArray *restaurantMenuArrM;
    UIImageView *line1;
    //UILabel *listLabel;
    UIView *shareView;
    IMGMenu *menu;
}

@end

@implementation HomeMenuTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
    if(self=[super initWithStyle:style reuseIdentifier:identifier]){
        headerLabel=[[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET,5,DeviceWidth,30)];
        dateLabel=[[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET,headerLabel.endPointY+3,DeviceWidth,25)];
        
        line1 = [[UIImageView alloc] init];
        [line1 setImage:[UIImage imageNamed:@"search_line"] ];
        [self addSubview:line1];
        
        
        titleLabel = [[TYAttributedLabel alloc] initWithFrame:CGRectMake(LEFT_X, TITLE_LABEL_Y, DeviceWidth-4*LEFTLEFTSET, TITLE_LABEL_HEIGHT)];
        titleLabel.textColor=[UIColor color333333];
        titleLabel.backgroundColor=[UIColor clearColor];
        titleLabel.numberOfLines = 0;
        [titleLabel setLineBreakMode:kCTLineBreakByWordWrapping];
        
        
        saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        saveBtn.frame = CGRectMake(DeviceWidth - LEFTLEFTSET - 25, titleLabel.frame.origin.y + 5, 40, 40);
        [saveBtn setImage:[UIImage imageNamed:@"save_list__"] forState:UIControlStateNormal];
        [self addSubview:saveBtn];
        [saveBtn addTarget:self action:@selector(saveBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        
        reviewCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_X+cuisineAndDistrictLabel.frame.size.width+11, TITLE_LABEL_Y+TITLE_LABEL_HEIGHT+GAP_BETWEEN_TITLE_AND_CUISINE+CUISINE_DISTRICT_LABEL_HEIGHT+GAP_BETWEEN_CUISINE_AND_REVIEW, 18,CELL_RATING_STAR_WIDTH)];
        [reviewCountLabel setTextAlignment:NSTextAlignmentRight];
        [reviewCountLabel setFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:10]];
        [reviewCountLabel setTextColor:[UIColor colorFFC000]];
        [reviewCountLabel setBackgroundColor:[UIColor clearColor]];
        
        scoreLabel = [[UILabel alloc] init];
        scoreLabel.font = [UIFont systemFontOfSize:12.5];
        scoreLabel.textColor = [UIColor colorFFC000];
        scoreLabel.frame = CGRectMake(LEFTLEFTSET, titleLabel.endPointY, scoreLabel.expectedWidth, scoreLabel.font.lineHeight);
        [self addSubview:scoreLabel];
        
        
        ratingScoreControl=[[DLStarRatingControl alloc] initWithFrame:CGRectMake(LEFT_X, TITLE_LABEL_Y+TITLE_LABEL_HEIGHT+GAP_BETWEEN_TITLE_AND_CUISINE+CUISINE_DISTRICT_LABEL_HEIGHT+GAP_BETWEEN_CUISINE_AND_REVIEW, 74, 10) andStars:5 andStarWidth:CELL_RATING_STAR_WIDTH isFractional:YES spaceWidth:5];
        ratingScoreControl.userInteractionEnabled=NO;
        
        
        timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET, scoreLabel.endPointY + 13, 100, 15)];
        timeLabel.textColor = [UIColor grayColor];
        timeLabel.alpha = 0.7f;
        timeLabel.font = [UIFont systemFontOfSize:12];
        [self addSubview:timeLabel];
        
        restaurantImageView = [[UIImageView alloc] initWithFrame:CGRectMake(LEFT_X, timeLabel.endPointY+13, DeviceWidth-LEFT_X*2, 160)];
        [restaurantImageView setAlpha:0.0];
        
        cardPhotosView = [[CardPhotosView alloc] init];
        cardPhotosView.restaurant = self.restaurant;
        cardPhotosView.homeTableViewCellDelegate = self.homeTableViewCellDelegate;
        cardPhotosView.cardPhotosViewDelegate = self;
        
        [self addSubview:cardPhotosView];

        //listLabel=[[UILabel alloc] init];
        //listLabel.font=[UIFont systemFontOfSize:12];
        //listLabel.textColor=[UIColor colorC2060A];

        
        separateView = [[UIView alloc] init];
        separateView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
        
        [self addSubview:separateView];
        [self addSubview:headerLabel];
        [self addSubview:dateLabel];
        [self addSubview:titleLabel];
        [self addSubview:cuisineAndDistrictLabel];
        [self addSubview:reviewCountLabel];
        [self addSubview:ratingScoreControl];
        //[self addSubview:listLabel];
    }
    return self;
}

-(void)setMuneCard:(IMGMenuCard *)menuCard{
    NSMutableArray *menuArr = menuCard.menuArr;
    IMGRestaurant *restaurant = menuCard.restaurant;
    if (shareView)
    {
        [shareView removeFromSuperview];
    }
    
    //listLabel.hidden = YES;
    
    if (cardPhotosView)
    {
        [cardPhotosView removeFromSuperview];
    }
    
    
    if (titleLabel)
    {
        [titleLabel removeFromSuperview];
    }
    titleLabel = [[TYAttributedLabel alloc] initWithFrame:CGRectMake(LEFT_X, TITLE_LABEL_Y, DeviceWidth-4*LEFTLEFTSET, TITLE_LABEL_HEIGHT)];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.numberOfLines = 0;
    titleLabel.delegate = self;
    titleLabel.characterSpacing = 0.1;
    titleLabel.font = [UIFont systemFontOfSize:TITLE_LABEL_FONT];
    [titleLabel setLineBreakMode:kCTLineBreakByWordWrapping];
    titleLabel.textColor = [UIColor grayColor];
    [self addSubview:titleLabel];
    
    
    restaurantMenuArrM = [NSMutableArray arrayWithArray:menuArr];
    
    _restaurant=restaurant;
    NSString *title = [NSString stringWithFormat:@"Menu from %@",restaurant.title];
    titleLabel.text = title;
    [titleLabel setFrameWithOrign:CGPointMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y) Width:DeviceWidth-4*LEFTLEFTSET];
    TYLinkTextStorage *linkTextStorageRestaurant = [[TYLinkTextStorage alloc]init];
    linkTextStorageRestaurant.range = [titleLabel.text rangeOfString:_restaurant.title];
    linkTextStorageRestaurant.textColor = [UIColor color333333];
    linkTextStorageRestaurant.linkData = @"restaurant";
    linkTextStorageRestaurant.underLineStyle = kCTUnderlineStyleNone;
    [titleLabel addTextStorage:linkTextStorageRestaurant];
    
    
    
    
    if ([_restaurant.saved boolValue])
    {
        [saveBtn setImage:[UIImage imageNamed:@"save_list__"] forState:UIControlStateNormal];
    }
    else
        [saveBtn setImage:[UIImage imageNamed:@"unsave_list"] forState:UIControlStateNormal];
    
    
    float score = [restaurant.ratingScore floatValue];
    [scoreLabel setText:[NSString stringWithFormat:@"%.1f",score/2]];
    scoreLabel.frame = CGRectMake(LEFTLEFTSET, titleLabel.endPointY, scoreLabel.expectedWidth, scoreLabel.font.lineHeight);
    
    
    ratingScoreControl.frame = CGRectMake(scoreLabel.endPointX + 8, titleLabel.endPointY +3, 74, 10);
    
    [ratingScoreControl updateRating:[NSNumber numberWithFloat:[restaurant.ratingScore floatValue]]];
    
    [line1 setFrame:CGRectMake(0, ratingScoreControl.endPointY + 5, DeviceWidth, 1)];
    
    [timeLabel setText:[Date getTimeInteval_v4:[restaurant.timeline longLongValue]/1000]];
    timeLabel.frame = CGRectMake(LEFTLEFTSET, scoreLabel.endPointY + 13, 240, 15);
    
    cardPhotosView = [[CardPhotosView alloc] init];
    cardPhotosView.restaurant = self.restaurant;
    cardPhotosView.homeTableViewCellDelegate = self.homeTableViewCellDelegate;
    cardPhotosView.cardPhotosViewDelegate = self;
    
    [self addSubview:cardPhotosView];
    cardPhotosView.homeTableViewCellDelegate = self.homeTableViewCellDelegate;
    cardPhotosView.cardPhotosViewDelegate = self;
    menu = [menuArr firstObject];
    
    [cardPhotosView setPhotos:restaurantMenuArrM];
    if ([menu.imageHeight intValue])
    {
        cardPhotosView.frame = CGRectMake(LEFTLEFTSET, timeLabel.endPointY+13, DeviceWidth-LEFTLEFTSET*2, [menu.imageHeight intValue]);
    }
    else
        cardPhotosView.frame = CGRectMake(LEFTLEFTSET, timeLabel.endPointY+13, DeviceWidth-LEFTLEFTSET*2, cardPhotosView.cardPhotoHeight);
    
    float currentPointY = cardPhotosView.endPointY;
    
    if([_restaurant.savedCount intValue]>0){
        //listLabel.frame=CGRectMake(LEFTLEFTSET,currentPointY + 3,DeviceWidth-LEFTLEFTSET*2,20);
        //listLabel.text=[NSString stringWithFormat:@"%@ saved",_restaurant.savedCount];
        // currentPointY+=22;
        
        //listLabel.hidden = YES;
    }
    if (menuArr.count > 1)
    {
        separateView.frame = CGRectMake(0, currentPointY + 15, DeviceWidth, cellMargin);
    }
    else if (menuArr.count == 1)
    {
        shareView = [[UIView alloc] init];
        UILabel *textLabel = [[UILabel alloc] init];
        [textLabel setText:L(@"Share")];
        [textLabel setTextAlignment:NSTextAlignmentCenter];
        textLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:11];
        [textLabel sizeToFit];
        UIImageView *iconView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, COMMUNITYBUTTONICONSIZE, COMMUNITYBUTTONICONSIZE)];
        [shareView addSubview:iconView];
        textLabel.frame=CGRectMake(15, 0, textLabel.frame.size.width, textLabel.frame.size.height);
        [shareView addSubview:textLabel];
        
        [textLabel setTextColor:COMMUNITYBUTTONUNTAPPEDTITLECOLOR];
        iconView.image=[UIImage imageNamed:@"share.png"];
        shareView.frame = CGRectMake(LEFTLEFTSET, currentPointY + 7.5, (DeviceWidth-LEFTLEFTSET*2)/3, 17);
        [self addSubview:shareView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shareBtnClick:)];
        [shareView addGestureRecognizer:tap];
        
        separateView.frame = CGRectMake(0, shareView.endPointY+10, DeviceWidth, cellMargin);
        
    }

}
-(void)setRestaurant:(IMGRestaurant *)restaurant andDishArr:(NSArray *)menuArr;
{
    if (shareView)
    {
        [shareView removeFromSuperview];
    }

    //listLabel.hidden = YES;

    if (cardPhotosView)
    {
        [cardPhotosView removeFromSuperview];
    }
    

    if (titleLabel)
    {
        [titleLabel removeFromSuperview];
    }
    titleLabel = [[TYAttributedLabel alloc] initWithFrame:CGRectMake(LEFT_X, TITLE_LABEL_Y, DeviceWidth-4*LEFTLEFTSET, TITLE_LABEL_HEIGHT)];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.numberOfLines = 0;
    titleLabel.delegate = self;
    titleLabel.characterSpacing = 0.1;
    titleLabel.font = [UIFont systemFontOfSize:TITLE_LABEL_FONT];
    [titleLabel setLineBreakMode:kCTLineBreakByWordWrapping];
    titleLabel.textColor = [UIColor grayColor];
    [self addSubview:titleLabel];
    
    
    restaurantMenuArrM = [NSMutableArray arrayWithArray:menuArr];
    
    _restaurant=restaurant;
    NSString *title = [NSString stringWithFormat:@"Menu from %@",restaurant.title];
    titleLabel.text = title;
    [titleLabel setFrameWithOrign:CGPointMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y) Width:DeviceWidth-4*LEFTLEFTSET];
    TYLinkTextStorage *linkTextStorageRestaurant = [[TYLinkTextStorage alloc]init];
    linkTextStorageRestaurant.range = [titleLabel.text rangeOfString:_restaurant.title];
    linkTextStorageRestaurant.textColor = [UIColor color333333];
    linkTextStorageRestaurant.linkData = @"restaurant";
    linkTextStorageRestaurant.underLineStyle = kCTUnderlineStyleNone;
    [titleLabel addTextStorage:linkTextStorageRestaurant];
    
    
    
    
    if ([_restaurant.saved boolValue])
    {
        [saveBtn setImage:[UIImage imageNamed:@"save_list__"] forState:UIControlStateNormal];
    }
    else
        [saveBtn setImage:[UIImage imageNamed:@"unsave_list"] forState:UIControlStateNormal];
    
    
    float score = [restaurant.ratingScore floatValue];
    [scoreLabel setText:[NSString stringWithFormat:@"%.1f",score/2]];
    scoreLabel.frame = CGRectMake(LEFTLEFTSET, titleLabel.endPointY, scoreLabel.expectedWidth, scoreLabel.font.lineHeight);
    
    
    ratingScoreControl.frame = CGRectMake(scoreLabel.endPointX + 8, titleLabel.endPointY +3, 74, 10);
    
    [ratingScoreControl updateRating:[NSNumber numberWithFloat:[restaurant.ratingScore floatValue]]];
    
    [line1 setFrame:CGRectMake(0, ratingScoreControl.endPointY + 5, DeviceWidth, 1)];
    
    [timeLabel setText:[Date getTimeInteval_v4:[restaurant.timeline longLongValue]/1000]];
    timeLabel.frame = CGRectMake(LEFTLEFTSET, scoreLabel.endPointY + 13, 240, 15);
    
    cardPhotosView = [[CardPhotosView alloc] init];
    cardPhotosView.restaurant = self.restaurant;
    cardPhotosView.homeTableViewCellDelegate = self.homeTableViewCellDelegate;
    cardPhotosView.cardPhotosViewDelegate = self;
    
    [self addSubview:cardPhotosView];
    cardPhotosView.homeTableViewCellDelegate = self.homeTableViewCellDelegate;
    cardPhotosView.cardPhotosViewDelegate = self;
    menu = [menuArr firstObject];

    [cardPhotosView setPhotos:restaurantMenuArrM];
    if ([menu.imageHeight intValue])
    {
        cardPhotosView.frame = CGRectMake(LEFTLEFTSET, timeLabel.endPointY+13, DeviceWidth-LEFTLEFTSET*2, [menu.imageHeight intValue]);
    }
    else
        cardPhotosView.frame = CGRectMake(LEFTLEFTSET, timeLabel.endPointY+13, DeviceWidth-LEFTLEFTSET*2, cardPhotosView.cardPhotoHeight);
    
    float currentPointY = cardPhotosView.endPointY;
    
    if([_restaurant.savedCount intValue]>0){
       //listLabel.frame=CGRectMake(LEFTLEFTSET,currentPointY + 3,DeviceWidth-LEFTLEFTSET*2,20);
        //listLabel.text=[NSString stringWithFormat:@"%@ saved",_restaurant.savedCount];
       // currentPointY+=22;

        //listLabel.hidden = YES;
    }
    if (menuArr.count > 1)
    {
        separateView.frame = CGRectMake(0, currentPointY + 15, DeviceWidth, cellMargin);
    }
    else if (menuArr.count == 1)
    {
        shareView = [[UIView alloc] init];
        UILabel *textLabel = [[UILabel alloc] init];
        [textLabel setText:L(@"Share")];
        [textLabel setTextAlignment:NSTextAlignmentCenter];
        textLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:11];
        [textLabel sizeToFit];
        UIImageView *iconView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0, COMMUNITYBUTTONICONSIZE, COMMUNITYBUTTONICONSIZE)];
        [shareView addSubview:iconView];
        textLabel.frame=CGRectMake(15, 0, textLabel.frame.size.width, textLabel.frame.size.height);
        [shareView addSubview:textLabel];
        
        [textLabel setTextColor:COMMUNITYBUTTONUNTAPPEDTITLECOLOR];
        iconView.image=[UIImage imageNamed:@"share.png"];
        shareView.frame = CGRectMake(LEFTLEFTSET, currentPointY + 7.5, (DeviceWidth-LEFTLEFTSET*2)/3, 17);
        [self addSubview:shareView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shareBtnClick:)];
        [shareView addGestureRecognizer:tap];
        
        separateView.frame = CGRectMake(0, shareView.endPointY+10, DeviceWidth, cellMargin);

    }

    
}


- (void)gotoCardDetailPage
{
    if (self.homeTableViewCellDelegate)
    {
        NSDictionary *dataDic = [[NSDictionary alloc] initWithObjectsAndKeys:_restaurant,@"restaurant",restaurantMenuArrM,@"dataArr", nil];
        [self.homeTableViewCellDelegate goToCardDetailPageWithType:10 andData:dataDic andCellIndexPath:self.fromCellIndexPath];
        
        //Event Tranking
        NSString *photoSource;
        if ([@"User ID" isEqualToString:restaurantEvent.photoCreditTypeDic]) {
            photoSource = @"User";
        }else if([@"Qraved Team" isEqualToString:restaurantEvent.photoCreditTypeDic]){
            photoSource = @"Qraved Admin";
        }
        
        [IMGAmplitudeUtil trackPhotoWithName:@"RC - View Restaurant Event detail" andRestaurantId:restaurantEvent.restaurantId andRestaurantTitle:restaurantEvent.restaurantTitle andOrigin:nil andUploaderUser_ID:restaurantEvent.userIdDic andPhoto_ID:nil andRestaurantMenu_ID:nil andRestaurantEvent_ID:restaurantEvent.eventId andPhotoSource:photoSource andLocation:@"Home Page"];
        
    }
}
#pragma mark - privates
- (NSAttributedString *)attributedString:(NSArray *__autoreleasing *)outURLs
                               URLRanges:(NSArray *__autoreleasing *)outURLRanges andText:(NSString *)text
{
    NSString *HTMLText = text;
    NSArray *URLs;
    NSArray *URLRanges;
    UIColor *color = [UIColor grayColor];
    UIFont *font = [UIFont systemFontOfSize:TITLE_LABEL_FONT];
    NSMutableParagraphStyle *mps = [[NSMutableParagraphStyle alloc] init];
    //    mps.lineSpacing = ceilf(font.pointSize * .5);
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor whiteColor];
    shadow.shadowOffset = CGSizeMake(0, 1);
    NSString *str = [NSString stringWithHTMLText:HTMLText baseURL:nil URLs:&URLs URLRanges:&URLRanges];
    NSMutableAttributedString *mas = [[NSMutableAttributedString alloc] initWithString:str attributes:@
                                      {
                                          NSForegroundColorAttributeName : color,
                                          NSFontAttributeName            : font,
                                          NSParagraphStyleAttributeName  : mps,
                                          NSShadowAttributeName          : shadow,
                                      }];
    [URLRanges enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
        [mas addAttributes:@
         {
             NSForegroundColorAttributeName : [UIColor color222222],
             //       NSUnderlineStyleAttributeName  : @(NSUnderlineStyleSingle)
             NSForegroundColorAttributeName : [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:TITLE_LABEL_FONT]
             
         } range:[obj rangeValue]];
    }];
    
    *outURLs = URLs;
    *outURLRanges = URLRanges;
    
    return [mas copy];
}
-(void)cardPhotosViewImageClick:(int)imageIndex{
    [self.menuDelegate homeMenuTableViewCellImageClick:_restaurant andDishArr:restaurantMenuArrM andIndex:imageIndex];
    
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:@"Homepage" forKey:@"Location"];
    [eventProperties setValue:_restaurant.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:menu.menuId forKey:@"RestaurantMenu_ID"];
    [[Amplitude instance] logEvent:@"CL - Restaurant Menu Card" withEventProperties:eventProperties];
}
-(void)cardPhotosChangeHeight:(CGFloat)height
{
    menu.imageHeight = [NSNumber numberWithFloat:height];
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewReloadCell:self];
    }

}
- (void)saveBtnClick:(UIButton *)btn
{
    [self.menuDelegate homeMenuSaveBtnClick:btn.selected andRestaurant:_restaurant];
}
- (void)shareBtnClick:(UIGestureRecognizer*)geture
{   UIView* view=geture.view;
    [self.menuDelegate homeMenuShareBtnClick:[restaurantMenuArrM firstObject] withbtn:view];
}
+(CGFloat)calculatedHeight:(IMGMenuCard*)menuCard{
    NSMutableArray *menuArr = [NSMutableArray arrayWithArray:menuCard.menuArr];
    IMGRestaurant *restaurant = menuCard.restaurant;
    IMGMenu *menu = [menuArr firstObject];
    NSString *title = [NSString stringWithFormat:@"Menu from %@",restaurant.title];
    CGSize expectedLabelSize = [title sizeWithFont:[UIFont systemFontOfSize:TITLE_LABEL_FONT]
                                 constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, 99999)
                                     lineBreakMode:NSLineBreakByWordWrapping];
    float hiddenHeight = 0;
    float cardPhotosViewHeight = 0;
    if ([menuArr count] == 1)
    {
        hiddenHeight = hiddenHeight + 20;
        if ([menu.imageHeight floatValue])
        {
            cardPhotosViewHeight = [menu.imageHeight floatValue];
        }
        else
        {
            cardPhotosViewHeight = 160;
        }
    }
    else
    {
        cardPhotosViewHeight = [CardPhotosView cardPhotoHeightWithPhotoCout:[menuArr count]];
    }
    //    if ([restaurant.savedCount intValue])
    //    {
    //        hiddenHeight = hiddenHeight + 25;
    //    }
    return expectedLabelSize.height + 90 +cardPhotosViewHeight + hiddenHeight;
}

+(CGFloat)calculatedHeight:(IMGRestaurant *)restaurant andMenuArr:(NSArray *)menuArr andRestaurantEvent:(IMGRestaurantEvent *)restaurantEvent
{
    
    IMGMenu *menu = [menuArr firstObject];
    NSString *title = [NSString stringWithFormat:@"Menu from   %@",restaurant.title];
//    CGSize expectedLabelSize = [title sizeWithFont:[UIFont systemFontOfSize:TITLE_LABEL_FONT]
//                                 constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, 99999)
//                                     lineBreakMode:NSLineBreakByWordWrapping];
    TYAttributedLabel*  temTitleLabel = [[TYAttributedLabel alloc] init];
    temTitleLabel.frame = CGRectMake(0, 0, DeviceWidth - LEFTLEFTSET*3-40, 20);
    temTitleLabel.numberOfLines = 0;
    temTitleLabel.characterSpacing = 0.1;
    
    [temTitleLabel setLineBreakMode:kCTLineBreakByWordWrapping];
    
    
    [temTitleLabel setText:title];
    temTitleLabel.font = [UIFont systemFontOfSize:15];
    [temTitleLabel setFrameWithOrign:CGPointMake(temTitleLabel.frame.origin.x, temTitleLabel.frame.origin.y) Width:DeviceWidth - LEFTLEFTSET*4];
    CGSize expectedLabelSize  = temTitleLabel.frame.size;

    float hiddenHeight = 0;
    float cardPhotosViewHeight = 0;
    if ([menuArr count] == 1)
    {
        hiddenHeight = hiddenHeight + 20;
        if ([menu.imageHeight floatValue])
        {
            cardPhotosViewHeight = [menu.imageHeight floatValue];
        }
        else
        {
            cardPhotosViewHeight = 160;
        }
    }
    else
    {
        cardPhotosViewHeight = [CardPhotosView cardPhotoHeightWithPhotoCout:[menuArr count]];
    }
//    if ([restaurant.savedCount intValue])
//    {
//        hiddenHeight = hiddenHeight + 25;
//    }
    return expectedLabelSize.height + 90 +cardPhotosViewHeight + hiddenHeight;
}
- (void)attributedLabel:(TYAttributedLabel *)attributedLabel textStorageClicked:(id<TYTextStorageProtocol>)TextRun atPoint:(CGPoint)point{
    
    
    if ([TextRun isKindOfClass:[TYLinkTextStorage class]]) {
        
   
            [self.homeTableViewCellDelegate gotoRestaurantWithRestaurantId:_restaurant.restaurantId];
        
    }
    
}

@end
