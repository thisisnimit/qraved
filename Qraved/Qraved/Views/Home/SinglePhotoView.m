//
//  SinglePhotoView.m
//  Qraved
//
//  Created by System Administrator on 3/9/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "SinglePhotoView.h"
#import "UIImageView+WebCache.h"
#import "NSString+Helper.h"
#import "UILabel+Helper.h"
#import "IMGDish.h"
#import "UIConstants.h"

#define BOTTOM_BUTTON_HEIGHT 60
#define LIKE_COMMENT_HEIGHT 60

@interface SinglePhotoView(){
	UIImageView *imageView;
	UILabel *descLabel;
	UILabel *likeLabel;
	UILabel *commentLabel;
	UIView *btnView;
	UIImageView *likeImageView;
	UIImageView *commentImageView;
}

@end

@implementation SinglePhotoView

-(instancetype)initWithFrame:(CGRect)frame dish:(IMGDish*)dish{
	if(self=[self initWithFrame:frame]){
		self.dish=dish;
        imageView=[[UIImageView alloc] initWithFrame:frame];
        imageView.contentMode=UIViewContentModeScaleToFill;

        descLabel=[[UILabel alloc] init];
        descLabel.font=[UIFont systemFontOfSize:16];
        descLabel.numberOfLines=0;
        descLabel.lineBreakMode=NSLineBreakByWordWrapping;
        descLabel.textColor=[UIColor whiteColor];

        likeLabel=[[UILabel alloc] init];
        likeLabel.font=[UIFont systemFontOfSize:14];
        likeLabel.textColor=[UIColor whiteColor];

        commentLabel=[[UILabel alloc] init];
        commentLabel.font=[UIFont systemFontOfSize:14];
        commentLabel.textColor=[UIColor whiteColor];

        [self addSubview:imageView];
        [self addSubview:descLabel];
        [self addSubview:likeLabel];
        [self addSubview:commentLabel];
		[self setImageView];
	}
	return self;
}


-(void)setImageView{
	NSString *urlString = [self.dish.imageUrl returnFullImageUrlWithWidth:self.frame.size.width/2];
    __weak typeof(imageView) weakImageView = imageView;
//    [weakImageView setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//    	// image=[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    	// [weakImageView setImage:image];
//        [UIView animateWithDuration:1.0 animations:^{
//            [weakImageView setAlpha:1.0];
//        }];
//    }];
    [weakImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        // image=[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            	// [weakImageView setImage:image];
                [UIView animateWithDuration:1.0 animations:^{
                    [weakImageView setAlpha:1.0];
                }];

    }];
    if([self.dish.likeCount intValue]>0 || [self.dish.commentCount intValue]>0){
    	CGSize descSize=[self.dish.descriptionStr sizeWithFont:descLabel.font constrainedToSize:CGSizeMake(DeviceWidth-LEFTLEFTSET*2,1000) lineBreakMode:NSLineBreakByWordWrapping];
    	descLabel.frame=CGRectMake(LEFTLEFTSET,DeviceHeight-BOTTOM_BUTTON_HEIGHT-LIKE_COMMENT_HEIGHT-descSize.height,DeviceWidth-LEFTLEFTSET*2,descSize.height);
    	descLabel.text=self.dish.descriptionStr;
    	CGFloat start=LEFTLEFTSET;
    	if([self.dish.likeCount intValue]>0){
    		likeLabel.text=[NSString stringWithFormat:@"%@ %@",self.dish.likeCount,[self.dish.likeCount intValue]>0?L(@"Likes"):L(@"Like")];
    		likeLabel.frame=CGRectMake(start,DeviceHeight-BOTTOM_BUTTON_HEIGHT-LIKE_COMMENT_HEIGHT-5,likeLabel.expectedWidth,LIKE_COMMENT_HEIGHT);
    		start+=likeLabel.expectedWidth+10;
    	}
    	if([self.dish.commentCount intValue]>0){
    		commentLabel.text=[NSString stringWithFormat:@"%@ %@",self.dish.likeCount,[self.dish.commentCount intValue]>0?L(@"Comments"):L(@"Comment")];
    		commentLabel.frame=CGRectMake(start,DeviceHeight-BOTTOM_BUTTON_HEIGHT-LIKE_COMMENT_HEIGHT-5,commentLabel.expectedWidth,LIKE_COMMENT_HEIGHT);
    	}
    	btnView=[[UIView alloc] initWithFrame:CGRectMake(0,DeviceHeight-BOTTOM_BUTTON_HEIGHT,DeviceWidth,BOTTOM_BUTTON_HEIGHT)];
    	[self addSubview:btnView];
    }
}


@end
