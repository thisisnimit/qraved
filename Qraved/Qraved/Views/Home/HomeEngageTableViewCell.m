//
//  HomeEngageTableViewCell.m
//  Qraved
//
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "HomeEngageTableViewCell.h"
#import "UIConstants.h"
#import "UIImageView+WebCache.h"
#import "UIView+Helper.h"
#import "UIImage+Resize.h"

#define OFFSET 12.0

@interface HomeEngageTableViewCell(){
    UIImageView *imageView;
    UILabel *titleLabel;
    UIButton *btn;
    UIView *separateView;
    
    UIImageView *lineButtom;
    UIImageView *lineLeft;
    UIImageView *lineRight;
    
    IMGEngageCard *engageCard;
    
}
@end

@implementation HomeEngageTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
    if(self=[super initWithStyle:style reuseIdentifier:identifier]){
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        imageView = [[UIImageView alloc] init];
        [self.contentView addSubview:imageView];
        
        titleLabel = [[UILabel alloc] init];
        titleLabel.numberOfLines = 0;
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.textColor = [UIColor color333333];
        titleLabel.font = [UIFont boldSystemFontOfSize:14];
        [self.contentView addSubview:titleLabel];
        
        btn = [[UIButton alloc] init];
        [btn setBackgroundImage:[UIImage imageNamed:@"btn_bg_idle"] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage imageNamed:@"btn_bg_hover"] forState:UIControlStateHighlighted];
        btn.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:12];
        [btn setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(engageBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:btn];
        
        lineButtom = [[UIImageView alloc] init];
        [lineButtom setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"search_line"]]];
        [self.contentView addSubview:lineButtom];

        lineLeft = [[UIImageView alloc] init];
        [lineLeft setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"search_line"]]];
        [self.contentView addSubview:lineLeft];

        lineRight = [[UIImageView alloc] init];
        [lineRight setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"search_line"]]];
        [self.contentView addSubview:lineRight];
        
        separateView = [[UIView alloc] init];
        separateView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
        [self addSubview:separateView];
        
    }
    return self;
}

-(void)setEngageCard:(IMGEngageCard*)engageCardLocal{
    engageCard = engageCardLocal;
    imageView.frame = CGRectMake(OFFSET, OFFSET, DeviceWidth - 2*OFFSET, 177);
//    [imageView setImageWithURL:[NSURL URLWithString:[engageCard.restaurantImage returnFullImageUrlWithWidth:imageView.frame.size.width]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] sizeFitWidth:YES];
    __weak typeof(imageView) weakImageView = imageView;
    UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 177)];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[engageCard.restaurantImage returnFullImageUrlWithWidth:imageView.frame.size.width]] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        image = [image imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 177)];
        
        [weakImageView setImage:image];

    }];
    
    titleLabel.frame = CGRectMake(imageView.frame.origin.x+OFFSET, imageView.endPointY, (imageView.frame.size.width - 103 - OFFSET *3), 55);
    
    btn.frame = CGRectMake(titleLabel.endPointX+OFFSET, titleLabel.frame.origin.y+5, 115, 41);
    if (self.type == HomeEngageTableViewCellUploadPhoto) {
        titleLabel.text = [NSString stringWithFormat:@"Took any food photo at %@? Share with us.",engageCard.restaurantTitle];
        [btn setTitle:@"Upload a Photo" forState:UIControlStateNormal];
    }else if (self.type == HomeEngageTableViewCellWriteReview){
        titleLabel.text = [NSString stringWithFormat:@"Eat at %@ lately? Tell us about it.",engageCard.restaurantTitle];
        [btn setTitle:@"Write a Review" forState:UIControlStateNormal];
    }
    
    separateView.frame = CGRectMake(0, titleLabel.endPointY+OFFSET, DeviceWidth, cellMargin);
    
    lineButtom.frame = CGRectMake(imageView.frame.origin.x,titleLabel.endPointY,imageView.frame.size.width,1);
    lineLeft.frame = CGRectMake(imageView.frame.origin.x,imageView.endPointY,1,titleLabel.frame.size.height);
    lineRight.frame = CGRectMake(imageView.endPointX-1,imageView.endPointY,1,titleLabel.frame.size.height);
}

+(CGFloat)calculatedHeight:(IMGEngageCard *)engageCard{
    return 79+177+cellMargin;
}

-(void)engageBtnClick{
    if( (self.homeEngageTableViewCellDelegate != nil ) &&
       ([self.homeEngageTableViewCellDelegate respondsToSelector:@selector(engageBtnClick:engageCard:)]) ) {
        [self.homeEngageTableViewCellDelegate engageBtnClick:self.type engageCard:engageCard];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
