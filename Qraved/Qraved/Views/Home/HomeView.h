//
//  HomeView.h
//  Qraved
//
//  Created by Shine Wang on 10/21/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"

@protocol homeViewDelegate <NSObject>

-(void)gotoRestaurantDetailPage:(IMGRestaurant *)restaurant;

@end

@interface HomeView : UIView

@property(nonatomic,retain)IMGRestaurant *restaurant;
@property (nonatomic, assign) BOOL isQraved;
@property (nonatomic, assign) BOOL fromHome;
@property (nonatomic,assign)BOOL isEvent;
@property(nonatomic,weak)id<homeViewDelegate>delegate;

- (id)initWithFrame:(CGRect)frame andInfoArray:(IMGRestaurant *)restaurant;

- (id)initWithFrame:(CGRect)frame andInfoArray:(IMGRestaurant *)paramRestaurant isFromHome:(BOOL)isfromHome isEvent:(BOOL)isEvent;


@end
