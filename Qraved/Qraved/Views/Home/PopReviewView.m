//
//  PopReviewView.m
//  Qraved
//
//  Created by DeliveLee on 16/3/12.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "PopReviewView.h"
#import "UIView+Helper.h"
#import "UIConstants.h"
#import "PopReviewTableViewCell.h"
#import "UIViewPreviousCommentsButton.h"
#import "UILineImageView.h"
#import "EntityKind.h"
#import "UICommentCell.h"

@interface PopReviewView ()<PopReviewTableViewCellDelegate,UITableViewDataSource, UITableViewDelegate, UITextViewDelegate,UIScrollViewDelegate,UICommentCellDelegate>

@end

@implementation PopReviewView{
    UIView *blackScreen;
    UIView *reviewView;
    UIView *reviewHeaderView;
    UIView *reviewFooterView;
    
    UIView *hackView;
    UITextView *textViewInput;
    UILabel *placeHold;
    UIButton *btnSendReview;
    
    BOOL noKeyboardChangeFired;
    BOOL keyboardShown;
    float keyboardHeight;
    
    UITableView *reviewTableView;
    
    BOOL moreComments;
    int commentCurrentNumbers;
    int commentCount;
    NSMutableArray *commentList;
    
    IMGType imgType;
    
    
    UIViewPreviousCommentsButton *viewPreviousCommentsButton;
    
}


-(id)initInSuperView:(UIView*)parentView commentCount:(int)commentCount_ commentArray:(NSArray *)commentList_{
    self = [super initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    if (self) {
        
        commentList = [[NSMutableArray alloc]initWithArray:commentList_];
        commentCurrentNumbers=(int)commentList.count;
        commentCount=commentCount_;
        moreComments=commentCount>commentCurrentNumbers;
        
        [parentView addSubview:self];
        
        [self createUI];
    }
    return self;
}

-(void)createUI{
    
    blackScreen = [[UIView alloc]init];
    blackScreen.backgroundColor = RGBACOLOR(0, 0, 0, 0.7);
    [self addSubview:blackScreen];
    
    reviewView = [[UIView alloc]init];
    reviewView.backgroundColor = [UIColor whiteColor];
    reviewView.layer.cornerRadius = 5;
    reviewView.layer.borderWidth = 1;
    reviewView.layer.borderColor = RGBACOLOR(218, 218, 218, 1).CGColor;
    reviewView.clipsToBounds = YES;
    [blackScreen addSubview:reviewView];
    
    reviewView.sd_layout
    .topSpaceToView(blackScreen,0)
    .leftSpaceToView(blackScreen,0)
    .rightSpaceToView(blackScreen,0)
    .heightIs(DeviceHeight-40-keyboardHeight-20);
    
    //init header view
    reviewHeaderView = [[UIView alloc] init];
    [reviewView addSubview:reviewHeaderView];
    
    reviewHeaderView.sd_layout
    .topSpaceToView(reviewView,0)
    .leftSpaceToView(reviewView,0)
    .rightSpaceToView(reviewView,0)
    .heightIs(40);
    
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor whiteColor];
    [reviewHeaderView addSubview:headerView];
    
    headerView.sd_layout
    .topSpaceToView(reviewHeaderView,0)
    .leftSpaceToView(reviewHeaderView,0)
    .rightSpaceToView(reviewHeaderView,0)
    .heightIs(40);
    
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, headerView.endPointY-1, DeviceWidth, 1);
    bottomBorder.backgroundColor = RGBACOLOR(223, 224, 225, 1).CGColor;
    [headerView.layer addSublayer:bottomBorder];
    
    UILabel *lblTitleText = [[UILabel alloc]init];
    lblTitleText.font = [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:15];
    lblTitleText.text = @"Comment";
    lblTitleText.textAlignment = NSTextAlignmentCenter;
    lblTitleText.textColor = [UIColor color333333];
    [headerView addSubview:lblTitleText];
    
    lblTitleText.sd_layout
    .topSpaceToView(headerView,0)
    .leftSpaceToView(headerView,0)
    .rightSpaceToView(headerView,0)
    .heightIs(40);
    
    UIButton *btnDone = [[UIButton alloc]init];
    [btnDone setTitle:@"Done" forState:UIControlStateNormal];
    [btnDone setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    btnDone.titleLabel.font = [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:13];
    btnDone.titleLabel.textAlignment = NSTextAlignmentCenter;
    [btnDone addTarget:self action:@selector(btnDoneTapped) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:btnDone];
    
    btnDone.sd_layout
    .topSpaceToView(headerView,0)
    .leftSpaceToView(headerView,DeviceWidth - 60)
    .rightSpaceToView(headerView,0)
    .heightIs(40);
    
    //init footer View
    reviewFooterView = [[UIView alloc]init];
    [reviewView addSubview:reviewFooterView];
    
    reviewFooterView.sd_layout
    .leftSpaceToView(reviewView,0)
    .bottomSpaceToView(reviewView,0)
    .heightIs(40)
    .widthIs(DeviceWidth);
    
    CALayer *topBorder = [CALayer layer];
    topBorder.frame = CGRectMake(0, 0, DeviceWidth, 1);
    topBorder.backgroundColor = RGBACOLOR(223, 224, 225, 1).CGColor;
    [reviewFooterView.layer addSublayer:topBorder];
    
    hackView = [[UIView alloc]init];
    hackView.backgroundColor = [UIColor clearColor];
    hackView.userInteractionEnabled = NO;
    [reviewFooterView addSubview:hackView];
    
    hackView.sd_layout
    .leftSpaceToView(reviewFooterView,5)
    .topSpaceToView(reviewFooterView,4)
    .bottomSpaceToView(reviewFooterView,4)
    .widthIs(DeviceWidth - 65);
    
    textViewInput = [[UITextView alloc]init];
    textViewInput.contentOffset = CGPointMake(0, 0);
    textViewInput.layer.cornerRadius = 4;
    textViewInput.layer.masksToBounds = YES;
    textViewInput.delegate = self;
    textViewInput.layer.borderWidth = 1;
    textViewInput.font = [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:13];
    textViewInput.layer.borderColor = [[[UIColor lightGrayColor] colorWithAlphaComponent:0.4] CGColor];
    [reviewFooterView addSubview:textViewInput];
    
    textViewInput.sd_layout
    .leftSpaceToView(reviewFooterView,5)
    .topSpaceToView(reviewFooterView,4)
    .heightRatioToView(hackView,1)
    .widthRatioToView(hackView,1);
    
    placeHold = [[UILabel alloc]init];
    placeHold.text = @"Write your comment";
    placeHold.font = [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:13];
    placeHold.textColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.8];
    [textViewInput addSubview:placeHold];
    
    placeHold.sd_layout
    .topSpaceToView(textViewInput,0)
    .leftSpaceToView(textViewInput,4)
    .rightSpaceToView(textViewInput,0)
    .heightIs(32);
    [placeHold sizeToFit];
    
    btnSendReview = [[UIButton alloc]init];
    [btnSendReview setTitle:@"Post" forState:UIControlStateNormal];
    [btnSendReview setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    btnSendReview.titleLabel.font = [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:13];
    btnSendReview.titleLabel.textAlignment = NSTextAlignmentCenter;
    [btnSendReview addTarget:self action:@selector(btnPostTapped) forControlEvents:UIControlEventTouchUpInside];
    if(textViewInput.text.length==0){
        btnSendReview.alpha = 0.5;
        btnSendReview.enabled = NO;
    }else{
        btnSendReview.alpha = 1;
        btnSendReview.enabled = YES;
    }
    [reviewFooterView addSubview:btnSendReview];
    
    btnSendReview.sd_layout
    .rightSpaceToView(reviewFooterView,5)
    .leftSpaceToView(hackView,5)
    .topSpaceToView(reviewFooterView,4)
    .bottomSpaceToView(reviewFooterView,4);
    
    //init viewPreviousComments Button
    viewPreviousCommentsButton = [[UIViewPreviousCommentsButton alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 26) hidden:!moreComments];
    [viewPreviousCommentsButton addTarget:self action:@selector(viewPreviousComments:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:viewPreviousCommentsButton];
//    [UILineImageView initInParent:self withFrame:CGRectMake(0, viewPreviousCommentsButton.startPointY-1, DeviceWidth, 1)];
    viewPreviousCommentsButton.sd_layout
    .leftSpaceToView(self,0)
    .rightSpaceToView(self,0)
    .topSpaceToView(reviewHeaderView,0)
    .heightIs(26);
    
    //init reviewTable view
    reviewTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight-64) style:UITableViewStylePlain];
    reviewTableView.delegate = self;
    reviewTableView.dataSource = self;
    reviewTableView.sectionFooterHeight = 0;
    reviewTableView.tableFooterView = [[UIView alloc]init];
    reviewTableView.bounces = NO;
    [reviewView addSubview:reviewTableView];
    reviewTableView.sd_layout
    .leftSpaceToView(reviewView,0)
    .rightSpaceToView(reviewView,0)
    .topSpaceToView(reviewHeaderView,moreComments?26:0)
    .bottomSpaceToView(reviewFooterView,0);
    [self layoutIfNeeded];
    [textViewInput becomeFirstResponder];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return commentCurrentNumbers;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UICommentCell *popReviewTableViewCell = [tableView dequeueReusableCellWithIdentifier:@"CellID"];
    if (popReviewTableViewCell == nil) {
        popReviewTableViewCell = [[UICommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellID"];
        
    }
    
    [popReviewTableViewCell updateUI:[commentList objectAtIndex:(commentList.count-1-indexPath.row)]];
    popReviewTableViewCell.commentCellDelegate = self;
    //    [UILineImageView initInParent:commentCell withFrame:CGRectMake(0, popReviewTableViewCell.startPointY, DeviceWidth, 1)];
    return popReviewTableViewCell;
}


- (void)textViewDidBeginEditing:(UITextView *)textView
{
    placeHold.hidden = textViewInput.text.length > 0;
    
}

- (void)textViewDidChange:(UITextView *)textView
{
    placeHold.hidden = textView.text.length>0;
    
    UITextView *mTrasView = textView;
    
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:13]};
    CGSize singleSize = [@"test" boundingRectWithSize:CGSizeMake(mTrasView.frame.size.width-10, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    CGSize textSize = [mTrasView.text boundingRectWithSize:CGSizeMake(mTrasView.frame.size.width-10, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    
    
    NSInteger lines = textSize.height / singleSize.height;
    NSLog(@"lines = %ld",(long)lines);
    
    if(textView.text.length == 0){
        [self disableSendButton];
    }else{
        [self enableSendButton];
    }
    
    [self changeReviewFooterViewHeight:lines];
    
}

-(void)changeReviewFooterViewHeight:(NSInteger)lines{
    if(lines<1){
        lines = 1;
    }else if(lines>5){
        lines = 5;
    }
    
    float viewHeight = 13*(lines-1)+4*(lines-1)+40;
    
    reviewFooterView.sd_layout
    .leftSpaceToView(reviewView,0)
    .bottomSpaceToView(reviewView,0)
    .heightIs(viewHeight)
    .widthIs(DeviceWidth);
    
    [UIView animateWithDuration:0.3 animations:^{
        [self layoutIfNeeded];
    }];
}

-(void)updateTextView:(UITextView *)textView{
    [self textViewDidChange:textView];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    placeHold.hidden = textViewInput.text.length > 0;
}

- (void)enableSendButton
{
    btnSendReview.alpha = 1.0;
    btnSendReview.enabled = YES;
}

- (void)disableSendButton
{
    btnSendReview.alpha = 0.5;
    btnSendReview.enabled = NO;
}


- (void)didAddSubview:(UIView *)subview
{
    
    //add notification
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardChange:) name:UIKeyboardWillHideNotification object:nil];
    //    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(tableViewScrollToBottom) name:UIKeyboardDidShowNotification object:nil];
    
    
}

- (void)willRemoveSubview:(UIView *)subview
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    //    [[NSNotificationCenter defaultCenter]removeObserver:UIKeyboardDidShowNotification];
    
    
}

-(void)addNewComment:(id)comment{
    commentCount++;
    commentCurrentNumbers++;
    [commentList insertObject:comment atIndex:0];
    
    [self updateUI];
    [self cleanInput];
    [self scrollToBottom];
    
}
-(void)addPreviousComments:(NSArray *)commentList_{
    for(NSInteger i=0;i<=commentList_.count-1;i++){
        [commentList addObject:[commentList_ objectAtIndex:i]];
        commentCurrentNumbers++;
    }
    [self updateUI];
}

-(void)updateUI{
    moreComments=commentCount>commentCurrentNumbers;
    NSLog(@"is more = %@",moreComments?@"YES":@"NO");
    
    reviewTableView.sd_layout
    .leftSpaceToView(reviewView,0)
    .rightSpaceToView(reviewView,0)
    .topSpaceToView(reviewHeaderView,moreComments?26:0)
    .bottomSpaceToView(reviewFooterView,0);
    viewPreviousCommentsButton.hidden = !moreComments;
    if (!moreComments) {
        reviewTableView.y =reviewHeaderView.bottom;
    }

    [reviewTableView reloadData];
    
}
-(void)cleanInput{
    textViewInput.text = @"";
}

-(void)scrollToBottom{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:commentList.count-1 inSection:0];
    [reviewTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

-(void)keyboardChange:(NSNotification *)notification
{
    
    if(noKeyboardChangeFired){
        noKeyboardChangeFired=NO;
        return;
    }else{
        noKeyboardChangeFired=NO;
        
    }
    
    NSDictionary *userInfo = [notification userInfo];
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    keyboardHeight = keyboardEndFrame.size.height;
    
    if (notification.name == UIKeyboardWillShowNotification) {
        keyboardShown=YES;
        reviewView.sd_layout
        .heightIs(DeviceHeight-40-keyboardHeight-20);
    }else{
        keyboardShown=NO;
        reviewView.sd_layout
        .heightIs(DeviceHeight-40-20);
    }
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self layoutIfNeeded];
    }];
    
}
-(void)showPopReviewView:(BOOL)hasPreviousComments{
    [self viewDidLayoutSubviews];
    if(hasPreviousComments){
        viewPreviousCommentsButton.hidden = YES;
    }else{
        viewPreviousCommentsButton.hidden = !moreComments;
    }
    blackScreen.alpha = 0;
    blackScreen.sd_layout
    .heightIs(DeviceHeight)
    .widthIs(DeviceWidth)
    .topSpaceToView(self,0)
    .leftSpaceToView(self,0);
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         blackScreen.alpha = 1;
                         [self layoutIfNeeded];
                     } completion:^(BOOL fin) {
                         
                     }];
}

-(void)btnDoneTapped{
    [textViewInput resignFirstResponder];
    blackScreen.sd_layout
    .heightIs(0)
    .widthIs(0)
    .topSpaceToView(self,DeviceHeight/2)
    .leftSpaceToView(self,DeviceWidth/2);
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         blackScreen.alpha = 0;
                         [self layoutIfNeeded];
                     } completion:^(BOOL fin) {
                         [self removeFromSuperview];
                     }];
    
}

-(void)btnPostTapped{
    [self endEditing:YES];
    [self.delegate postComment:textViewInput];
}

-(void)viewDidLayoutSubviews
{
    if ([reviewTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [reviewTableView setSeparatorInset:UIEdgeInsetsMake(0,0,0,0)];
    }
    
    if ([reviewTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [reviewTableView setLayoutMargins:UIEdgeInsetsMake(0,0,0,0)];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewPreviousComments:(UIButton *)button{
    if(self.delegate){
        if(commentList.count>0){
            id comment = [commentList objectAtIndex:commentList.count-1];
            if(comment){
                [self endEditing:YES];
                [self.delegate popReviewView:self viewPreviousCommentsTapped:viewPreviousCommentsButton lastComment:comment];
            }
        }
        
    }
}


- (void)popReviewTableViewCellUserNameOrImageTapped:(id)sender
{
    if (self.delegate)
    {
        [self.delegate popReviewViewUserNameOrImageTapped:sender];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self endEditing:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [UICommentCell calculatedHeight:[commentList objectAtIndex:commentList.count-1-indexPath.row]];
}
- (void)UICommentCell:(UITableViewCell*)tableViewCell readMoreTapped:(id)comment{
    CGFloat height1 = tableViewCell.frame.size.height;
    NSIndexPath *indexPath = [reviewTableView indexPathForCell:tableViewCell];
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            if ([[reviewTableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                return;
            }
            [reviewTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        
        @catch ( NSException *e ) {
            return;
        }
    });
    UITableViewCell *cell2 = [reviewTableView cellForRowAtIndexPath:indexPath];
    CGFloat height2 = cell2.frame.size.height;
    CGFloat offset = height2-height1;
    self.frame=CGRectMake(self.startPointX, self.startPointY, self.frame.size.width, self.frame.size.height+offset);
    reviewTableView.frame=CGRectMake(reviewTableView.startPointX, reviewTableView.startPointY, reviewTableView.frame.size.width, reviewTableView.frame.size.height+offset);
    
}

-(void)UICommentCellUserNameOrImageTapped:(id)sender{
    if (self.delegate) {
        [self.delegate popReviewViewUserNameOrImageTapped:sender];
    }
    
}

@end
