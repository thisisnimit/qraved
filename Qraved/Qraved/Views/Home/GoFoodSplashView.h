//
//  GoFoodSplashView.h
//  Qraved
//
//  Created by harry on 2018/2/28.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoFoodSplashView : UIView

@property (nonatomic, copy) void(^dismissSplash)();
@property (nonatomic, copy) void(^reminderTapped)();
@property (nonatomic, copy) void(^turntoDelivery)();

@end
