//
//  HomeEngageOfferTableViewCell.m
//  Qraved
//
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "HomeEngageOfferTableViewCell.h"
#import "UIView+Helper.h"
#import "DLStarRatingControl.h"
#import "NSString+Helper.h"
#import "UIConstants.h"
#import "UIImageView+WebCache.h"
#import "UIColor+Helper.h"
#import "Amplitude.h"
#import "AppDelegate.h"
#import "IMGOfferCard.h"
#import "RestaurantHandler.h"
#import "Date.h"
#import "LikeView.h"
#import "HomeUtil.h"
#import "LoadingView.h"
#import "UIImage+Resize.h"
#import "V2_LikeCommentShareView.h"
#define OFFSET 12.0

@interface HomeEngageOfferTableViewCell ()<LikeListViewDelegate,V2_LikeCommentShareViewDelegete>{
    UILabel *topTitleLabel;
    UIImageView *titleImageViewLine;
    UILabel *creatTimelable;
    UILabel *restaurantTitleLabel;
    UIButton *saveBtn;
    
    DLStarRatingControl *ratingScoreControl;
    UILabel *ratingScoreLabel;
    
    
    UIImageView *imageView;
    UILabel *titleLabel;
    UIButton *btn;
    
    UIImageView *lineButtom;
    UIImageView *lineLeft;
    UIImageView *lineRight;

    UILabel *likeLabel;
    UILabel *commentLabel;
    
    V2_LikeCommentShareView *likeCommentShareView;
    
    UIView *separateView;
    IMGOfferCard *_offermodel;
    CGFloat curretY;
    LikeView *likepopView;
    NSMutableArray *likelistArr;
    
}

@end

@implementation HomeEngageOfferTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
    if(self=[super initWithStyle:style reuseIdentifier:identifier]){
        self.contentView.backgroundColor = [UIColor whiteColor];

        
        [self createUI];
    }
    return self;
}

- (void)createUI{
    topTitleLabel = [UILabel new];
    topTitleLabel.userInteractionEnabled=YES;
    UITapGestureRecognizer *topTitleTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gotoCardDetailView:)];
    [topTitleLabel addGestureRecognizer:topTitleTap];

    titleImageViewLine = [UIImageView new];
    [titleImageViewLine setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"search_line"]]];
    
    creatTimelable=[UILabel new];
    creatTimelable.textColor = [UIColor color999999];
    creatTimelable.font = [UIFont systemFontOfSize:12];
    
    
    restaurantTitleLabel = [UILabel new];
    
    restaurantTitleLabel.textColor=[UIColor color222222];
    restaurantTitleLabel.font=[UIFont boldSystemFontOfSize:17];
    restaurantTitleLabel.userInteractionEnabled=YES;
    UITapGestureRecognizer *restaurantTitleTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(restaurantTitleTapAct:)];
    [restaurantTitleLabel addGestureRecognizer:restaurantTitleTap];
 
    saveBtn = [UIButton new];
    [saveBtn setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
    [saveBtn addTarget:self action:@selector(saveBtnClick:) forControlEvents:UIControlEventTouchUpInside];

    ratingScoreControl=[[DLStarRatingControl alloc] initWithFrame:CGRectMake(0, 0, 74, 10) andStars:5 andStarWidth:10 isFractional:YES spaceWidth:5];
    ratingScoreControl.userInteractionEnabled=NO;
 
    ratingScoreLabel = [UILabel new];
    ratingScoreLabel.textColor = [UIColor colorWithRed:238.0/255.0 green:187.0/255.0 blue:56.0/255.0 alpha:1];
    ratingScoreLabel.font = [UIFont systemFontOfSize:12];
    
    
    imageView = [[UIImageView alloc] init];
    imageView.userInteractionEnabled=YES;
    UITapGestureRecognizer *imageviewTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gotoCardDetailView:)];
    [imageView addGestureRecognizer:imageviewTap];

    titleLabel = [UILabel new];
    titleLabel.textColor = [UIColor color333333];
    titleLabel.font = [UIFont boldSystemFontOfSize:14];
    titleLabel.userInteractionEnabled=YES;
    UITapGestureRecognizer *titleLabelTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gotoCardDetailView:)];
    [titleLabel addGestureRecognizer:titleLabelTap];
  
    
    lineButtom = [[UIImageView alloc] init];
    [lineButtom setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"search_line"]]];
  
    lineLeft = [[UIImageView alloc] init];
    [lineLeft setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"search_line"]]];

    lineRight = [[UIImageView alloc] init];
    [lineRight setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"search_line"]]];

    likeCommentShareView = [V2_LikeCommentShareView new];
    likeCommentShareView.delegate = self;
    
    UIButton *btnList = [UIButton new];
    [likeCommentShareView addSubview:btnList];
    
    separateView = [UIView new];
    separateView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    
    [self.contentView sd_addSubviews:@[topTitleLabel,titleImageViewLine,creatTimelable,restaurantTitleLabel,saveBtn,ratingScoreLabel,ratingScoreControl,imageView,titleLabel,lineButtom,lineLeft,lineRight,likeCommentShareView,separateView]];
    
    topTitleLabel.sd_layout
    .topSpaceToView(self.contentView, 10)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(18);
    
    titleImageViewLine.sd_layout
    .topSpaceToView(topTitleLabel, 10)
    .leftEqualToView(topTitleLabel)
    .rightEqualToView(topTitleLabel)
    .heightIs(1);
    
    creatTimelable.sd_layout
    .topSpaceToView(titleImageViewLine, 5)
    .leftEqualToView(titleImageViewLine)
    .rightEqualToView(titleImageViewLine)
    .heightIs(14);
    
    restaurantTitleLabel.sd_layout
    .topSpaceToView(creatTimelable, 10)
    .leftEqualToView(creatTimelable)
    .autoHeightRatio(0);
    
    [restaurantTitleLabel setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 60];
    [restaurantTitleLabel setMaxNumberOfLinesToShow:2];
    
    ratingScoreLabel.sd_layout
    .topSpaceToView(restaurantTitleLabel, 5)
    .leftEqualToView(restaurantTitleLabel)
    .heightIs(14);
    
    [ratingScoreLabel setSingleLineAutoResizeWithMaxWidth:DeviceWidth-60];
    
    ratingScoreControl.sd_layout
    .topSpaceToView(restaurantTitleLabel, 7)
    .leftSpaceToView(ratingScoreLabel, 5)
    .widthIs(74)
    .heightIs(10);
    
    saveBtn.sd_layout
    .topEqualToView(restaurantTitleLabel)
    .rightSpaceToView(self.contentView, 15)
    .widthIs(40)
    .heightIs(34);
    
    imageView.sd_layout
    .topSpaceToView(ratingScoreLabel, 10)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15);
    
    titleLabel.sd_layout
    .topSpaceToView(imageView, 15)
    .leftSpaceToView(self.contentView, 25)
    .rightSpaceToView(self.contentView, 25)
    .heightIs(16);
    
    lineButtom.sd_layout
    .topSpaceToView(titleLabel, 15)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(1);
    
    lineLeft.sd_layout
    .topSpaceToView(imageView, 0)
    .leftSpaceToView(self.contentView, 15)
    .widthIs(1)
    .heightIs(45);
    
    lineRight.sd_layout
    .topSpaceToView(imageView, 0)
    .rightSpaceToView(self.contentView, 15)
    .widthIs(1)
    .heightIs(45);
    
    likeCommentShareView.sd_layout
    .topSpaceToView(lineButtom, 0)
    .leftSpaceToView(self.contentView, 0)
    .rightSpaceToView(self.contentView, 0)
    .autoHeightRatio(0);
    
    btnList.sd_layout
    .topSpaceToView(likeCommentShareView,0)
    .leftSpaceToView(likeCommentShareView,DeviceWidth/3 - 60)
    .widthIs(60)
    .heightIs(50);
    [btnList addTarget:self action:@selector(likeCountClick:) forControlEvents:UIControlEventTouchUpInside];
    
    separateView.sd_layout
    .topSpaceToView(likeCommentShareView,0)
    .leftSpaceToView(self.contentView,0)
    .rightSpaceToView(self.contentView,0)
    .heightIs(20);
    
}


- (void)setOfferModel:(IMGOfferCard *)offerModel{
    //
    _offermodel=offerModel;
   
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"A new Promo posted"];
    [attributedString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:DEFAULT_FONT_NAME size:16],NSForegroundColorAttributeName:[UIColor colorWithRed:97/255.0 green:97/255.0 blue:97/255.0 alpha:1]} range:NSMakeRange(0,6)];
    [attributedString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:16],NSForegroundColorAttributeName:[UIColor colorWithRed:3/255.0 green:3/255.0 blue:3/255.0 alpha:1]} range:NSMakeRange(6,5)];
    [attributedString setAttributes:@{NSFontAttributeName:[UIFont fontWithName:DEFAULT_FONT_NAME size:16],NSForegroundColorAttributeName:[UIColor colorWithRed:97/255.0 green:97/255.0 blue:97/255.0 alpha:1]} range:NSMakeRange(12,6)];
    topTitleLabel.attributedText = attributedString;

    [creatTimelable setText:[Date getTimeInteval_v4:[_offermodel.timeline longLongValue]/1000]];

    
    NSString *resturantsTitle=[NSString stringWithFormat:@"%@ - %@",[_offermodel.restaurantTitle removeHTML],[_offermodel.landMark removeHTML]];
    if (_offermodel.landMark.length==0) {
        resturantsTitle=[NSString stringWithFormat:@"%@",[_offermodel.restaurantTitle removeHTML]];
    }

    restaurantTitleLabel.text = resturantsTitle;

    if (_offermodel.isSaved)
    {
        [saveBtn setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
    }
    else{
        [saveBtn setImage:[UIImage imageNamed:@"ic_heart_home"] forState:UIControlStateNormal];
    }

    float score = [_offermodel.rating floatValue];
    [ratingScoreLabel setText:[NSString stringWithFormat:@"%.1f",score/2]];

    [ratingScoreControl updateRating:[NSNumber numberWithFloat:[_offermodel.rating floatValue]]];

    
    __weak typeof(imageView) weakImageView= imageView;
    
    [imageView sd_setImageWithURL:[NSURL URLWithString:[_offermodel.offerImageUrl returnFullImageUrlWithWidth:DeviceWidth-30]]  placeholderImage:[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-30, [_offermodel.offerImageHeight floatValue])]completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image) {
            weakImageView.image = [image imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-30, [_offermodel.offerImageHeight floatValue])];
        }
    }];
    
    imageView.sd_layout
    .heightIs([_offermodel.offerImageHeight floatValue]);
    [imageView updateLayout];

    titleLabel.text = [_offermodel.offerTitle removeHTML];

    [likeCommentShareView setCommentCount:[offerModel.commentCount intValue] likeCount:[offerModel.likeCount intValue] liked:offerModel.isLike];
    
    [self setupAutoHeightWithBottomView:separateView bottomMargin:0];
}
-(void)restaurantTitleTapAct:(UITapGestureRecognizer*)tap{

    if ((self.homeTableViewCellDelegate!=nil)&&[self.homeTableViewCellDelegate respondsToSelector:@selector(gotoRestaurantWithRestaurantId:)]) {
        
        [self.homeTableViewCellDelegate gotoRestaurantWithRestaurantId:_offermodel.restaurantId];
        
        
    }
}
-(void)gotoCardDetailPage:(UITapGestureRecognizer*)tap{
    if ((self.homeOfferCellDelegate!=nil)&[self.homeOfferCellDelegate respondsToSelector:@selector(gotoDetailpageWithResturants:)]) {
        
        [self.homeOfferCellDelegate gotoOfferDetailpageWithResturants:_offermodel withIndexPath:self.fromCellIndexPath];
        
        
    }


}
-(void)likeCountClick:(UIButton *)btn
{
    [[Amplitude instance] logEvent:@" RC - View People Who Likes"];
    if (likepopView) {
        [likepopView removeFromSuperview];
    }
    
    [[LoadingView sharedLoadingView]startLoading];
        [HomeUtil getLikeListFromServiceWithOfferCardId:_offermodel.offerId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
            likelistArr=[NSMutableArray arrayWithArray:likeListArray];
            likepopView.likecount=likeviewcount;
            likepopView.likelistArray=likelistArr;
            [[LoadingView sharedLoadingView]stopLoading];
        }];
        
    
    likepopView=[[LikeView alloc]initWithFrame:CGRectMake(0, -20, DeviceWidth, DeviceHeight)];
    likepopView.delegate=self;
    UIResponder *responder = [self nextResponder];
    while (responder){
        if ([responder isKindOfClass:[UINavigationController class]]){
            UINavigationController* vc=(UINavigationController*)responder;
            
            [[vc.viewControllers lastObject].view addSubview:likepopView];
            likepopView.layer.zPosition=1000;
            return;
        }
        
        responder = [responder nextResponder];
        
    }
    
}
-(void)LikeviewLoadMoreData:(NSInteger)offset
{
    
   
        [HomeUtil getLikeListFromServiceWithOfferCardId:_offermodel.offerId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
            likepopView.likecount=likeviewcount;
            [likelistArr addObjectsFromArray:likeListArray];
            [likepopView.likeUserTableView reloadData];
        }];
    
}
-(void)returnLikeCount:(NSInteger)count
{
    if (count!=0) {
        
        
        float start = LEFTLEFTSET;
        float top = lineButtom.endPointY + 10;
        NSString *likeString=[NSString stringWithFormat:@"%ld %@",(long)count,count>1?L(@"Likes"):L(@"Like")];
        [likeLabel setText:likeString];
        [likeLabel setFrame:CGRectMake(start, top, likeLabel.expectedWidth,20)];
    }
}

- (void)saveBtnClick:(UIButton *)button
{
    IMGRestaurant *resturant=[[IMGRestaurant alloc]init];
    resturant.restaurantId=_offermodel.restaurantId;
    resturant.saved=[NSNumber numberWithBool:_offermodel.isSaved];
    if (self.homeTableViewCellDelegate) {
        [self.homeTableViewCellDelegate homeCardTableViewReloadCell:self.fromCellIndexPath entity:resturant];
    }
}

- (UITableView*)myTableView{
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder* nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UITableView class]]) {
            return (UITableView*)nextResponder;
        }
    }
    return nil;
}
-(void)gotoCardDetailView:(UITapGestureRecognizer*)tap{
    if ((self.homeOfferCellDelegate!=nil)&[self.homeOfferCellDelegate respondsToSelector:@selector(gotoDetailpageWithResturants:)]) {
        IMGRestaurant *restaurant=[[IMGRestaurant alloc]init];
        restaurant.restaurantId=_offermodel.restaurantId;
        [self.homeOfferCellDelegate gotoDetailpageWithResturants:restaurant];
        
    }

}

-(void)likeCommentShareView:(UIView *)likeCommentShareView viewPreviousCommentsTapped:(UIButton *)button lastComment:(id)comment
{
    
    if ((self.homeOfferCellDelegate!=nil)&[self.homeOfferCellDelegate respondsToSelector:@selector(gotoDetailpageWithResturants:)]) {
        
        [self.homeOfferCellDelegate gotoOfferDetailpageWithResturants:_offermodel withIndexPath:self.fromCellIndexPath];
        
        
    }

}
-(void)likeCommentShareView:(UIView *)likeCommentShareView_ likeButtonTapped:(UIButton *)button{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
    [RestaurantHandler likeRestaurantOfferCard:_offermodel.isLike OfferCardId:_offermodel.offerId andBlock:^{
        
        
    } failure:^(NSString *exceptionMsg) {
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }

    }];
        _offermodel.isLike=!_offermodel.isLike;
        if(_offermodel.isLike){
            _offermodel.likeCount=[NSNumber numberWithInt:[_offermodel.likeCount intValue]+1];
            
//            [[Amplitude instance] logEvent:@"UC - Like Restaurant Event Initiate" withEventProperties:eventProperties];
        }else{
            _offermodel.likeCount=[NSNumber numberWithInt:[_offermodel.likeCount intValue]-1];
//            [[Amplitude instance] logEvent:@"UC - UnLike Restaurant Event Initiate" withEventProperties:eventProperties];
        }
        
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self likeButtonTapped:button entity:_offermodel];
    }
    
    
}
-(void)likeCommentShareView:(UIView *)likeCommentShareView shareButtonTapped:(UIButton *)button{
      if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self shareButtonTapped:button entity:_offermodel];
    }
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView commentButtonTapped:(UIButton *)button{
    //home card cell都留空
    if (self.homeTableViewCellDelegate)
    {
        NSDictionary *dataDic = [[NSDictionary alloc] initWithObjectsAndKeys:_offerModel,@"offerModel", nil];
        [self.homeTableViewCellDelegate goToCardDetailPageWithType:8 andData:dataDic andCellIndexPath:self.fromCellIndexPath];
    }
}


- (void)likeCommentShareView:(UIView*)likeCommentShareView_ commentInputView:(UIView *)commentInputView inputViewExpand:(UITextView *)inputTextView commentInputViewYOffset:(CGFloat)commentInputViewYOffset{
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self likeCommentShareView:likeCommentShareView_ commentInputView:commentInputView inputViewExpand:inputTextView commentInputViewYOffset:commentInputViewYOffset];
    }
}

- (void)likeCommentShareView:(UIView*)likeCommentShareView_ commentInputView:(UIView *)commentInputView postButtonTapped:(UIButton *)postButton commentInputText:(NSString *)text{
    
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    postButton.enabled = NO;
   
        if(self.homeTableViewCellDelegate){
            
//            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
//            [eventProperties setValue:restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
//            [eventProperties setValue:restaurantEvent.restaurantId forKey:@"Restaurant_ID"];
//            [eventProperties setValue:@"Restaurant event card on homepage" forKey:@"Location"];
            
            [RestaurantHandler postComment:text restaurantOfferCardId:_offermodel.offerId andBlock:^(IMGDishComment *eventComment) {
                postButton.enabled = YES;
//                [eventProperties setValue:eventComment.commentId forKey:@"Comment_ID"];
//                [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Succeed" withEventProperties:eventProperties];
//                [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Restaurant Event Succeed" withValues:eventProperties];
                _offermodel.commentCount=[NSNumber numberWithInt:[_offermodel.commentCount intValue]+1];
                if (_offermodel.commentCardList){
                    if([_offermodel.commentCardList isKindOfClass:[NSArray class]]){
                        _offermodel.commentCardList = [NSMutableArray arrayWithArray:_offermodel.commentCardList];
                    }
                    [_offermodel.commentCardList insertObject:eventComment atIndex:0];
                }else{
                    _offermodel.commentCardList = [[NSMutableArray alloc] initWithObjects:eventComment, nil];
                }
                [self.homeTableViewCellDelegate homeCardTableViewCell:self newCommentPosted:eventComment];
                
            } failure:^(NSString *errorReason){
//                [eventProperties setValue:errorReason forKey:@"Reason"];
//                [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Failed" withEventProperties:eventProperties];
            }];
            
//            [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Initiate" withEventProperties:eventProperties];
//
    }
    
}

- (void)likeCommentShareView:(UIView*)likeCommentShareView_ commentInputView:(UIView *)view inputViewDidBeginEditing:(UITextView *)textView{
    if(self.homeTableViewCellDelegate){
//        [self restaurantEventPhotoCardAmplitude];
        [self.homeTableViewCellDelegate homeCardTableViewCell:self likeCommentShareView:likeCommentShareView_ commentInputView:view inputViewDidBeginEditing:textView];
    }
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView commentCell:(UITableViewCell *)cell readMoreTapped:(id)comment offset:(CGFloat)offset{
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self commentCell:cell readMoreTapped:comment offset:offset];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
