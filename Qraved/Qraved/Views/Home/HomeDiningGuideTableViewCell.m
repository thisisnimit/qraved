//
//  HomeDiningGuideTableViewCell.m
//  Qraved
//
//  Created by System Administrator on 2/24/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "HomeDiningGuideTableViewCell.h"
#import "IMGDiningGuide.h"
#import "Label.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "UIImage+Helper.h"
#import "UIConstants.h"
#import "UIImageView+WebCache.h"
#import "Amplitude.h"
#import "UIImage+Resize.h"
#define RESTAURANT_IMAGE_HEIGHT 160

@implementation HomeDiningGuideTableViewCell{
	UILabel *headerLabel;
	UILabel *dateLabel;
	UILabel *titleLabel;
	UIImageView *restaurantImageView;
	UILabel *numLabel;
	UIButton *seeBtn;
    UIView *separateView;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
	if(self=[super initWithStyle:style reuseIdentifier:identifier]){

        
        [self createUI];
	}
	return self;
}
- (void)createUI{
    headerLabel = [UILabel new];
    headerLabel.font = [UIFont systemFontOfSize:14];
    
    dateLabel = [UILabel new];
    dateLabel.font = [UIFont systemFontOfSize:12];
    dateLabel.textColor = [UIColor color999999];
    dateLabel.textAlignment = NSTextAlignmentRight;
    
    UIImageView *sepImageView = [UIImageView new];
    sepImageView.image = [UIImage imageNamed:@"search_line"];
    
    restaurantImageView = [UIImageView new];
    restaurantImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(restaurantImageViewTapped)];
    [restaurantImageView addGestureRecognizer:tap];
    
    titleLabel = [UILabel new];
    titleLabel.font=[UIFont systemFontOfSize:14];
    titleLabel.textColor=[UIColor whiteColor];
    titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
    
    
    numLabel = [UILabel new];
    numLabel.font = [UIFont systemFontOfSize:14];
    numLabel.textColor = [UIColor color999999];
    
    
    UIButton *btnSeeAll = [UIButton new];
    [btnSeeAll setTitle:@"See All" forState:UIControlStateNormal];
    btnSeeAll.titleLabel.font = [UIFont systemFontOfSize:14];
    [btnSeeAll setTitleColor:[UIColor colorWithHexString:@"09BFD3"] forState:UIControlStateNormal];
    [btnSeeAll addTarget:self action:@selector(goToDiningGuideDetail:) forControlEvents:UIControlEventTouchUpInside];
    
    
    separateView = [UIView new];
    separateView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    
    [self.contentView sd_addSubviews:@[headerLabel,dateLabel,sepImageView,restaurantImageView,titleLabel,numLabel,btnSeeAll,separateView]];
    
    headerLabel.sd_layout
    .leftSpaceToView(self.contentView,15)
    .topSpaceToView(self.contentView,15)
    .widthIs(200)
    .heightIs(16);
    
    dateLabel.sd_layout
    .rightSpaceToView(self.contentView,15)
    .topSpaceToView(self.contentView,15)
    .leftSpaceToView(headerLabel,10)
    .heightIs(16);
    
    sepImageView.sd_layout
    .leftSpaceToView(self.contentView,15)
    .rightSpaceToView(self.contentView,15)
    .topSpaceToView(headerLabel,15)
    .heightIs(1);
    
    restaurantImageView.sd_layout
    .topSpaceToView(sepImageView,10)
    .leftEqualToView(sepImageView)
    .rightEqualToView(sepImageView)
    .heightIs((DeviceWidth-LEFTLEFTSET*2)/375*245);
    
    [self insertTransparentGradient];
    
    numLabel.sd_layout
    .topSpaceToView(restaurantImageView,10)
    .leftSpaceToView(self.contentView,15)
    .widthIs(200)
    .heightIs(16);
    
    titleLabel.sd_layout
    .leftSpaceToView(self.contentView,25)
    .rightSpaceToView(self.contentView,20)
    .bottomSpaceToView(numLabel,21)
    .autoHeightRatio(0);
    
    [titleLabel setMaxNumberOfLinesToShow:2];
    
    btnSeeAll.sd_layout
    .topSpaceToView(restaurantImageView,0)
    .rightSpaceToView(self.contentView,15)
    .widthIs(45)
    .heightIs(40);
    
    separateView.sd_layout
    .topSpaceToView(btnSeeAll,10)
    .leftSpaceToView(self.contentView,0)
    .rightSpaceToView(self.contentView,0)
    .heightIs(20);
}

-(void)setDiningguide:(IMGDiningGuide *)diningguide {
    
    _diningguide=diningguide;

    NSString *title=[_diningguide.pageName filterHtml];
    NSString *num=[NSString stringWithFormat:@"%@ %@",_diningguide.restaurantCount,[_diningguide.restaurantCount intValue]>1?@"New Restaurants added":@"New Restaurant added"];

    [titleLabel setText:title];
    [numLabel setText:num];

    NSString *urlString = [diningguide.imageUrl returnFullImageUrlWithWidth:DeviceWidth-2*LEFTLEFTSET andHeight:(DeviceWidth-LEFTLEFTSET*2)/375*245];
    UIImage *placeHoderImage =[UIImage imageNamed:DEFAULT_IMAGE_STRING];

    [restaurantImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:placeHoderImage completed:nil];
    
    [self setupAutoHeightWithBottomView:separateView bottomMargin:0];
    
}

- (void)restaurantImageViewTapped
{
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:self.diningguide.diningGuideId forKey:@"DiningGuide_ID"];
    [eventProperties setValue:@"Homepage" forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL - Dining Guide Update Card" withEventProperties:eventProperties];

    
    [self.delegate goToDiningGuide:_diningguide];
}

-(void)goToDiningGuideDetail:(UIButton*)btn{
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:self.diningguide.diningGuideId forKey:@"DiningGuide_ID"];
    [eventProperties setValue:@"Homepage" forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL - Dining Guide Update Card" withEventProperties:eventProperties];
    
    [self.delegate goToDiningGuideList];
}

-(void)setTitle:(NSString*)title{
	_title=[title copy];
	headerLabel.text=_title;
}

-(void)setDate:(NSString*)update{
	dateLabel.text=[update copy];
}

- (void) insertTransparentGradient {
    UIColor *colorOne = [UIColor colorWithRed:(0/255.0)  green:(0/255.0)  blue:(0/255.0)  alpha:0];
    UIColor *colorTwo = [UIColor colorWithRed:(0/255.0)  green:(0/255.0)  blue:(0/255.0)  alpha:1.0];
    NSArray *colors = [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, nil];
    NSNumber *stopOne = [NSNumber numberWithFloat:0];
    NSNumber *stopTwo = [NSNumber numberWithFloat:1.0];
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    
    //crate gradient layer
    CAGradientLayer *headerLayer = [CAGradientLayer layer];
    
    headerLayer.colors = colors;
    headerLayer.locations = locations;
    
    headerLayer.frame = CGRectMake(0, 0, DeviceWidth-2*LEFTLEFTSET, (DeviceWidth-LEFTLEFTSET*2)/69*43);
    
    [restaurantImageView.layer insertSublayer:headerLayer atIndex:0];
}

@end
