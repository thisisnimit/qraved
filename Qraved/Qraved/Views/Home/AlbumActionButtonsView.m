//
//  CardButtonsView.m
//  Qraved
//
//  Created by lucky on 16/3/8.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "AlbumActionButtonsView.h"
#import "UIConstants.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"

#define COMMUNITYBUTTONBACKGROUNDCOLOR [UIColor colorWithRed:251/255.0f green:252/255.0f blue:255/255.0f alpha:1]
#define COMMUNITYBUTTONICONSIZE 13.5
#define COMMUNITYBUTTONGAP 7
#define LIKEBUTTONTAPPEDICON   @"like_red1.png"
#define LIKEBUTTONUNTAPPEDICON   @"like1.png"
#define LIKEBUTTONTAPPEDTITLECOLOR       [UIColor colorWithRed:234/255.0f green:98/255.0f blue:98/255.0f alpha:1]


@implementation AlbumActionButtonsView{
    BOOL liked;
}


-(id)initInSuperView:(UIView*)parentView atStartX:(CGFloat)X startY:(CGFloat)Y commentCount:(int)commentCount_ commentArray:(NSArray *)commentList_ liked:(BOOL)liked_ withFooter:(BOOL)withFooter_{
    self = [super initWithFrame:CGRectMake(X, Y, DeviceWidth, DeviceHeight-200)];
    if (self) {
        [parentView addSubview:self];
        liked = liked_;
        [self addBtns];
        [self updateLikedStatus:YES];
    }
    
    return self;
}

- (void)addBtns
{
    self.backgroundColor = [UIColor clearColor];
    
    NSArray *btns=@[@"Like",@"Comment",@"Share"];
    NSArray *btnImages=@[@"like",@"card_comment",@"card_share"];
    
    float width=(DeviceWidth)/[btns count];

    for(int i=0;i<[btns count];i++){
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.titleLabel.font = [UIFont systemFontOfSize:12];
        NSString *str=[btns objectAtIndex:i];
        CGSize strSize=[str sizeWithFont:btn.titleLabel.font constrainedToSize:CGSizeMake(width,20) lineBreakMode:NSLineBreakByTruncatingTail];
        UIImage *image=[UIImage imageNamed:[btnImages objectAtIndex:i]];
        UIImageView *btnImageView=[[UIImageView alloc] initWithImage:image];
        btnImageView.tag=100+i;
        float imgWidth=image.size.width;
        btnImageView.frame=CGRectMake((width-imgWidth-strSize.width)/2+width*i,10,imgWidth - 5,15);
        btnImageView.userInteractionEnabled=YES;
        btn.frame=CGRectMake((width-imgWidth-strSize.width)/2+imgWidth+width*i+3,7,strSize.width,20);
        btn.tag=200+i;
        [btn setTitle:str forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnClick_icon:)];
        [btnImageView addGestureRecognizer:tap];
        [btn addTarget:self action:@selector(btnClick_btn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        [self addSubview:btnImageView];
        
        if(i>0){
            CALayer *leftBorder = [CALayer layer];
            leftBorder.frame = CGRectMake(width*i, 7, 1, 20);
            leftBorder.backgroundColor = [UIColor colorWithRed:142/255.0 green:142/255.0 blue:142/255.0 alpha:1].CGColor;
            [self.layer addSublayer:leftBorder];

        }

    }
}

-(void)updateLikedStatus:(BOOL)isFirstLoad{
    if(!isFirstLoad){
        liked=!liked;
    }
    if(liked==0){
        [(UIButton *)[self viewWithTag:200] setTitleColor:COMMUNITYBUTTONUNTAPPEDTITLECOLOR forState:UIControlStateNormal];
        [(UIImageView *)[self viewWithTag:100] setImage:[UIImage imageNamed:LIKEBUTTONUNTAPPEDICON]];
    }else{
        [(UIButton *)[self viewWithTag:200] setTitleColor:LIKEBUTTONTAPPEDTITLECOLOR forState:UIControlStateNormal];
        [(UIImageView *)[self viewWithTag:100] setImage:[UIImage imageNamed:LIKEBUTTONTAPPEDICON]];
    }
}

-(void)btnClick_btn:(UIButton*)button{
    [self btnClick:button.tag withBtn:button];
}
-(void)btnClick_icon:(UITapGestureRecognizer*)tap{
    UIButton* btn=(UIButton*)tap.view;
    [self btnClick:tap.view.tag+100 withBtn:btn];

}
-(void)btnClick:(NSInteger)index withBtn:(UIButton*)btn {
    switch(index - 200){
        case 0:{
            NSLog(@"click like");
            [self.delegate albumActionButtonsClick:TypeForLike withBtn:btn];
//            [self updateLikedStatus];
        }
            break;
        case 1:{
            [self.delegate albumActionButtonsClick:TypeForComment withBtn:btn];
            NSLog(@"click comment");
        }
            break;
        case 2:{
            [self.delegate albumActionButtonsClick:TypeForShare withBtn:btn];
            NSLog(@"click share");
        }
            break;
            
    }
}


@end
