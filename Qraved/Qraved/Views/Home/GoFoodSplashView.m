//
//  GoFoodSplashView.m
//  Qraved
//
//  Created by harry on 2018/2/28.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "GoFoodSplashView.h"
#import <SDWebImage/UIImage+GIF.h>
@implementation GoFoodSplashView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.9];
        [self createUI];
    }
    return self;
}

- (void)createUI{
    UIView *bgView = [UIView new];
    bgView.backgroundColor = [UIColor colorWithRed:222/255.0 green:32/255.0 blue:41/255.0 alpha:1.0];
    bgView.layer.masksToBounds = YES;
    bgView.layer.cornerRadius = 5;
    bgView.userInteractionEnabled = YES;
    [bgView bk_whenTapped:^{
        if (self.turntoDelivery) {
            self.turntoDelivery();
        }
    }];
    [self addSubview:bgView];
    
    UIImageView *titleImageView = [UIImageView new];
    titleImageView.image = [UIImage imageNamed:@"ic_goFoodTitle"];
    
    UIImageView *gifImageView = [UIImageView new];
    gifImageView.backgroundColor = [UIColor clearColor];
    gifImageView.image = [UIImage sd_animatedGIFNamed:@"goFood_splash"];
    
    UIView *logoView = [UIView new];
    logoView.backgroundColor = [UIColor clearColor];
    
    UIImageView *logoImageView = [UIImageView new];
    logoImageView.image = [UIImage imageNamed:@"ic_splash_logo"];
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.font = [UIFont systemFontOfSize:18];
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.text = @"langsung disini!";
    [logoView sd_addSubviews:@[logoImageView, lblTitle]];
    
    FunctionButton *goFoodButton = [FunctionButton buttonWithType:UIButtonTypeCustom];
    [goFoodButton addTarget:self action:@selector(goFoodTapped) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnShow = [UIButton new];
    btnShow.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.15];
    [btnShow setTitle:@"Don’t show it again" forState:UIControlStateNormal];
    btnShow.titleLabel.font = [UIFont systemFontOfSize:12];
    [btnShow setTitleColor:[UIColor colorWithHexString:@"#EAEAEA"] forState:UIControlStateNormal];
    [btnShow addTarget:self action:@selector(reminderButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *btnClose = [UIButton new];
    btnClose.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.15];
    [btnClose setTitle:@"Close" forState:UIControlStateNormal];
    btnClose.titleLabel.font = [UIFont systemFontOfSize:12];
    [btnClose setTitleColor:[UIColor colorWithHexString:@"#EAEAEA"] forState:UIControlStateNormal];
    [btnClose addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *spaceView = [UIView new];
    spaceView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    
    [bgView sd_addSubviews:@[titleImageView, gifImageView, logoView, goFoodButton, btnShow, spaceView, btnClose]];

    bgView.sd_layout
    .centerYEqualToView(self)
    .leftSpaceToView(self, 27)
    .rightSpaceToView(self, 27)
    .autoHeightRatio(0);
    
    titleImageView.sd_layout
    .topSpaceToView(bgView, 49)
    .widthIs(193)
    .heightIs(105)
    .centerXEqualToView(bgView);
    
    gifImageView.sd_layout
    .topSpaceToView(titleImageView, 16)
    .leftSpaceToView(bgView, 0)
    .widthIs(DeviceWidth - 54)
    .heightIs((DeviceWidth - 54) * 0.53);
    
    logoImageView.sd_layout
    .leftSpaceToView(logoView, 0)
    .topSpaceToView(logoView, 3)
    .widthIs(83)
    .heightIs(16);
    
    lblTitle.sd_layout
    .topSpaceToView(logoView, 0)
    .widthIs(130)
    .heightIs(22)
    .leftSpaceToView(logoImageView, 6);
    
    logoView.sd_layout
    .topSpaceToView(gifImageView, 33)
    .heightIs(22)
    .centerXEqualToView(bgView)
    .autoWidthRatio(0);
    
    [logoView setupAutoWidthWithRightView:lblTitle rightMargin:0];
    
    goFoodButton.sd_layout
    .topSpaceToView(logoView, 20)
    .widthIs(167)
    .heightIs(38)
    .centerXEqualToView(bgView);
    
    spaceView.sd_layout
    .bottomSpaceToView(bgView, 8)
    .widthIs(2)
    .heightIs(22)
    .centerXEqualToView(bgView);
    
    btnShow.sd_layout
    .topSpaceToView(goFoodButton, 30)
    .leftSpaceToView(bgView, 0)
    .heightIs(38)
    .rightSpaceToView(spaceView, 0);
    
    btnClose.sd_layout
    .topEqualToView(btnShow)
    .rightSpaceToView(bgView, 0)
    .leftSpaceToView(btnShow, 0)
    .heightIs(38);
    
    [bgView setupAutoHeightWithBottomView:btnShow bottomMargin:0];
}

- (void)close{
    if (self.dismissSplash) {
        self.dismissSplash();
    }
}

- (void)reminderButtonTapped{
    if (self.reminderTapped) {
        self.reminderTapped();
    }
}

- (void)goFoodTapped{
    if (self.turntoDelivery) {
        self.turntoDelivery();
    }
}

@end
