//
//  SinglePhotoView.h
//  Qraved
//
//  Created by System Administrator on 3/9/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IMGDish;

@interface SinglePhotoView : UIView

@property(nonatomic,strong) IMGDish *dish;
@property(nonatomic,assign) BOOL liked;

-(instancetype)initWithFrame:(CGRect)frame dish:(IMGDish*)dish;

@end
