//
//  HomeDiningGuideTableViewCell.h
//  Qraved
//
//  Created by System Administrator on 2/29/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "HomeTableViewCell.h"

@class IMGDiningGuide;

@interface HomeDiningGuideTableViewCell : HomeTableViewCell

@property(nonatomic,strong) IMGDiningGuide *diningguide;
@property(nonatomic,copy) NSString *title;
@property(nonatomic,copy) NSString *date;
@property(nonatomic,weak) id<HomeTableViewCellDelegate> delegate;

@end
