//
//  UserIncentiveViewController.m
//  Qraved
//
//  Created by DeliveLee on 16/3/23.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "UserIncentiveViewController.h"
#import "UIDevice+Util.h"

@interface UserIncentiveViewController ()

@end

@implementation UserIncentiveViewController{
    int count;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self createUI];
    
}

-(id)initWithCount:(int)count_{
    self = [super init];
    if(self){
        count = count_;
    }
    return self;
}

-(void)createUI{
    [self.navigationController.navigationBar setHidden:YES];
    
    
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.frame = self.view.bounds;
    imageView.contentMode= UIViewContentModeScaleAspectFill;

    if ([UIDevice isLaterThanIphone5])
    {
        imageView.image = [UIImage imageNamed:@"reviewSplash_big.jpg"];
    }
    else
    {
        imageView.image = [UIImage imageNamed:@"reviewSplash_small.jpg"];
    }
    
    [self.view addSubview:imageView];

    UIButton *closeButton = [[UIButton alloc] initWithFrame:CGRectMake(DeviceWidth-45, 20, 45, 45)];
    [closeButton setImage:[UIImage imageNamed:@"ic_review_close"] forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeButton];
    [self.view bringSubviewToFront:closeButton];

}
- (void)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

 
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
