//
//  HomeRestUpdateTableViewCell.h
//  Qraved
//
//  Created by System Administrator on 2/26/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeTableViewCell.h"
#import "UILikeCommentShareView.h"
#import "IMGRestaurantEvent.h"
#import "CardPhotosView.h"
#import "LikeView.h"
#import "TYAttributedLabel.h"
#import "IMGUpdateCard.h"
#import "V2_LikeCommentShareView.h"
@protocol HomeRestUpdateTableViewCellDelegate <NSObject>
@optional
- (void)saveBtnClick:(BOOL)isSaved andRestaurant:(IMGRestaurant *)restaurant;
- (void)homeRestUpdateTableViewCellImageClick:(IMGRestaurant *)restaurant andDishArr:(NSArray *)dishArr andIndex:(int)index;
- (void)homeRestUpdateTableViewCellImageClick:(IMGRestaurant *)restaurant andDishArr:(NSArray *)dishArr andIndex:(int)index andFromCellIndexPath:(NSIndexPath*)fromCellIndexPath;

@end

@class IMGRestaurant;

@interface HomeRestUpdateTableViewCell : HomeTableViewCell<LikeCommentShareDelegate,CardPhotosViewDelegate,LikeListViewDelegate,TYAttributedLabelDelegate,V2_LikeCommentShareViewDelegete>

@property(nonatomic,strong) IMGRestaurant *restaurant;
@property(nonatomic,strong) IMGUpdateCard *updateCard;
@property (nonatomic,weak) id<HomeRestUpdateTableViewCellDelegate>restUpdateDelegate;



-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier;

@end
