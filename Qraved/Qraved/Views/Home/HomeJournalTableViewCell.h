//
//  HomeJournalTableViewCell.h
//  Qraved
//
//  Created by System Administrator on 2/26/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "HomeTableViewCell.h"
#import "UILikeCommentShareView.h"
#import "HomeJournalTableViewCell.h"
#import "LikeView.h"
@class IMGMediaComment;

@protocol HomeJournalTableViewCellDelegate <NSObject>

- (void)gotoJournalDetailPage:(IMGMediaComment *)journal cellIndexPath:(NSIndexPath*)cellIndexPath;

@end



@interface HomeJournalTableViewCell : HomeTableViewCell<LikeCommentShareDelegate,LikeListViewDelegate>

@property(nonatomic,strong) IMGMediaComment *journal;
@property(nonatomic,copy) NSString *title;
@property(nonatomic,copy) NSString *date;
@property(nonatomic,assign) BOOL liked;

@property (nonatomic,weak) id<HomeJournalTableViewCellDelegate>homeJournalTableViewCellDelegate;

-(void)setJournal:(IMGMediaComment *)journal;
//-(CGFloat)returnCellHeight;

@end
