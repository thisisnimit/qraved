
//
//  HomeView.m
//  Qraved
//
//  Created by Shine Wang on 10/21/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "HomeView.h"
#import "AppDelegate.h"
#import "UIImageView+Cache.h"
#import "UIImage+Resize.h"
#import "Label.h"
#import "Tools.h"
#import "DLStarRatingControl.h"
#import "Label.h"
#import "UIView+Helper.h"
#import "UIButton+WebCache.h"
#import "NSString+Helper.h"
#import "NSNumber+Helper.h"
#import "UIImageView+WebCache.h"
#import "UIColor+Helper.h"
#import <CoreText/CoreText.h>
#import "DTAttributedLabel.h"
#import "UIImage+Helper.h"
@interface HomeView()
@property (nonatomic ,weak) UIButton *touchBtn;
@property(nonatomic,retain) UIImageView *defaultImageView;
@property (nonatomic ,strong) UIImageView *cuisineImage;
@property (nonatomic ,strong) UIImageView *selecteView;
@end

@implementation HomeView
@synthesize isQraved;
@synthesize fromHome;



- (id)initWithFrame:(CGRect)frame andInfoArray:(IMGRestaurant *)restaurant;
{
    return [self initWithFrame:frame andInfoArray:restaurant isFromHome:NO isEvent:NO];
}

- (id)initWithFrame:(CGRect)frame andInfoArray:(IMGRestaurant *)paramRestaurant isFromHome:(BOOL)isfromHome isEvent:(BOOL)isEvent
{
    self = [super initWithFrame:frame];
    if (self) {
        self.fromHome = isfromHome;
        self.restaurant=paramRestaurant;
        self.isEvent = isEvent;
        [self setCuisineView];
    }
    return self;
}

-(void)setCuisineView {
    CGFloat imageWidth=self.frame.size.width;
    CGFloat imageHeight=self.frame.size.height;
    
    
    self.touchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.touchBtn.frame = CGRectMake(0, 0, imageWidth, imageHeight);
    self.touchBtn.backgroundColor = [UIColor clearColor];
//    UIImage *placeHolderImage = [[UIImage imageNamed:DEFAULT_IMAGE_STRING ] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth, 162)];
    UIImage *placeHolderImage = [UIImage imageWithColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]  size:CGSizeMake(DeviceWidth, 162)];;
    [self.touchBtn setImageWithURL:nil forState:UIControlStateNormal placeholderImage:placeHolderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        
    }];
    [self.touchBtn addTarget:self action:@selector(btnTouched:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.touchBtn];
    
    
    self.cuisineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, imageWidth, imageWidth)];
    [self addSubview:self.cuisineImage];
    
    NSString *urlString = [self.restaurant.imageUrl returnFullImageUrlWithWidth:imageWidth];

    __weak typeof(self) weakSelf = self;
    [self.cuisineImage setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        [UIView animateWithDuration:1.0 animations:^{
            [weakSelf.cuisineImage setAlpha:1];
        }];
    }];

    Label *restaurantLabel = [[Label alloc] init];
    
    
    ////////////////////////////////////////////
    [Label setTitle:restaurantLabel text:self.restaurant.title];
    if (self.restaurant.title==nil) {
        self.restaurant.title =@"Kaihomaru";
        [Label setTitle:restaurantLabel text:@"Kaihomaru"];
    }
    
    CGSize expectedLabelSize=[self.restaurant.title sizeWithFont:restaurantLabel.font constrainedToSize:CGSizeMake(DeviceWidth-30, 2*restaurantLabel.font.lineHeight)];
    
    [restaurantLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [restaurantLabel setNumberOfLines:2];
    [restaurantLabel setTextAlignment:NSTextAlignmentLeft];
    restaurantLabel.font =[UIFont  boldSystemFontOfSize:20];
    restaurantLabel.textColor = [UIColor whiteColor];
    [self addSubview:restaurantLabel];
    
    
    
    NSMutableString *cuisineString=[[NSMutableString alloc] init];
    
    //////////
    [cuisineString appendFormat:@"Japanese"];
    ///////////
    
    
    //append districtName
//    if(self.restaurant.location.district.name!=nil)
//    {
//        [cuisineString appendFormat:@" / %@",self.restaurant.location.district.name];
//    }
//    else
//    {
//    }
    [cuisineString appendString:@" / Jakarta"];

    ////////////////////////////////////////////
    //append PriceLevel
    
    [cuisineString appendFormat:@" / %@",@"$$$$"];
    ////////////////////////////////////////////
    
    
    
    CGSize expectedCuisineLabelSize=[cuisineString sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] constrainedToSize:CGSizeMake(imageWidth-30, 16) lineBreakMode:NSLineBreakByWordWrapping];
    
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc]initWithString:cuisineString];
    
    NSRange range = NSMakeRange(cuisineString.length-4+3, 4-3);
    
    NSRange range1 = [cuisineString rangeOfString:cuisineString];
    
    CTFontRef font = CTFontCreateWithName((CFStringRef)[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13].fontName, 13, NULL);
    [attStr addAttribute:(id)kCTFontAttributeName value:(id)CFBridgingRelease(font) range:range1];
    
    [attStr addAttribute:(id)kCTForegroundColorAttributeName value:(id)[UIColor whiteColor].CGColor range:range1];
    [attStr addAttribute:(id)kCTForegroundColorAttributeName value:(id)[UIColor colorWithRed:111/255.0 green:111/255.0 blue:111/255.0 alpha:1].CGColor range:range];
    
    DTAttributedLabel *cuisineLabel = [[DTAttributedLabel alloc]init];
    cuisineLabel.backgroundColor = [UIColor clearColor];
    [cuisineLabel setLineBreakMode:NSLineBreakByWordWrapping];
    cuisineLabel.numberOfLines = 0;
    [cuisineLabel setAttributedString:attStr];
    [self addSubview:cuisineLabel];
    
    

    
    
    UIView *ratingScoreBgView=[[UIView alloc]init];

    ratingScoreBgView.layer.masksToBounds=YES;

    ratingScoreBgView.backgroundColor = [UIColor clearColor];
    [self addSubview:ratingScoreBgView];
    DLStarRatingControl *ratingScore=[[DLStarRatingControl alloc]initWithFrame:CGRectMake(15, 5, 70, 10) andStars:5 andStarWidth:10 isFractional:YES spaceWidth:5];
    ratingScore.rating= 3.5;
    ratingScore.userInteractionEnabled=NO;
    [ratingScoreBgView addSubview:ratingScore];
    
    
    ratingScoreBgView.frame = CGRectMake(0, self.frame.size.height-30, RATING_STAR_WIDTH*7,30);
    
    Label *reviewContLabel = [[Label alloc]initWithFrame:CGRectMake(ratingScoreBgView.endPointX, self.frame.size.height-28, 20, 16) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor colorFFC000] andTextLines:1];
    reviewContLabel.textAlignment = NSTextAlignmentRight;
    reviewContLabel.text = @"3";
    [self addSubview:reviewContLabel];
    
    UIImageView *reviewImage = [[UIImageView alloc]initWithFrame:CGRectMake(reviewContLabel.endPointX+2, self.frame.size.height-24, 10, 10)];
    reviewImage.image = [UIImage imageNamed:@"comment"];
    [self addSubview:reviewImage];
    
    
    ///////////////////////
    
    if (expectedLabelSize.height>26) {
        cuisineLabel.frame = CGRectMake(15, self.frame.size.height-30-expectedCuisineLabelSize.height, imageWidth-30, expectedCuisineLabelSize.height+10);
        restaurantLabel.frame = CGRectMake(15, self.frame.size.height-30-expectedCuisineLabelSize.height-expectedLabelSize.height, imageWidth-80, expectedLabelSize.height+10);
    }
    else
    {
        cuisineLabel.frame = CGRectMake(15, self.frame.size.height-25-expectedCuisineLabelSize.height, imageWidth-30, expectedCuisineLabelSize.height+10);
        restaurantLabel.frame = CGRectMake(15, self.frame.size.height-30-expectedCuisineLabelSize.height-expectedLabelSize.height-10, imageWidth-80, expectedLabelSize.height+15);
    }
    

    
    
    
    
    UIImageView *distanceImageView = [[UIImageView alloc]initWithFrame:CGRectMake(imageWidth-50, imageHeight-15, 50, 5)];
//    distanceImageView.backgroundColor = [UIColor redColor];
    distanceImageView.image = [UIImage imageNamed:@"PlottingScale.png"];
    UILabel *distanceLabel = [[UILabel alloc]initWithFrame:CGRectMake(imageWidth-45, imageHeight-30, 45, 20)];
    distanceLabel.backgroundColor = [UIColor clearColor];
    distanceLabel.textColor = [UIColor whiteColor];
    distanceLabel.text = @"300m";
    distanceLabel.font =[ UIFont systemFontOfSize:12];
    [self addSubview:distanceLabel];
    [self addSubview:distanceImageView];
    
    
    if (self.isEvent) {
        CGSize expectSize = [@"up to 50%" sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13]];
        UIImageView *markImage = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth-expectSize.width-10, 7, expectSize.width+10, expectSize.height+9)];
        //            markImage.backgroundColor = [UIColor redColor];
        markImage.image = [UIImage imageNamed:@"LableRed"];
        [self addSubview:markImage];
        
        Label *markLabel = [[Label alloc]initWithFrame:CGRectMake(5, 3, expectSize.width, expectSize.height) andTextFont:[UIFont fontWithName:@"OpenSans" size:13] andTextColor:[UIColor whiteColor] andTextLines:1];
        markLabel.text = @"up to 50%";
        [markImage addSubview:markLabel];
    }
    
    
}

-(void)btnTouched:(UIButton *)sender {
    [self.delegate gotoRestaurantDetailPage:self.restaurant];
    
}

@end
