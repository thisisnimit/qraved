//
//  CardJournalView.h
//  Qraved
//
//  Created by System Administrator on 3/4/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "HomeTableViewCell.h"

@class IMGMediaComment;

@interface CardJournalView : UIView<UITableViewDelegate,UITableViewDataSource>{
	
}

@property(nonatomic,copy) NSString *title;
@property(nonatomic,copy) NSString *date;
@property(nonatomic,weak) id<HomeTableViewCellDelegate> delegate;


-(instancetype)initWithFrame:(CGRect)frame journal:(IMGMediaComment*)journal;

@end
