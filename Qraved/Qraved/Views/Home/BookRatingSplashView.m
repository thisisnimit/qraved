//
//  BookRatingSplashView.m
//  Qraved
//
//  Created by apple on 16/11/3.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "BookRatingSplashView.h"
#import "AppDelegate.h"
#import "UIView+Helper.h"
#import "DLStarRatingControl.h"
#import "UIColor+Helper.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Helper.h"

@interface BookRatingSplashView ()<DLStarRatingDelegate>

@end
@implementation BookRatingSplashView{
    UIImageView *restaurantImageView;
    UILabel *titleLabel;
    UILabel *districtLabel;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        UIView *bgView = [[UIView alloc] initWithFrame:[AppDelegate ShareApp].window.bounds];
        bgView.backgroundColor =[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7];
        [self addSubview:bgView];
        restaurantImageView = [[UIImageView alloc] initWithFrame:CGRectMake(40, [UIScreen mainScreen].bounds.size.height/7, DeviceWidth-80,  [UIScreen mainScreen].bounds.size.height/7*5/2)];
        [bgView addSubview:restaurantImageView];
        UIView *ratingView = [[UIView alloc] initWithFrame:CGRectMake(40, restaurantImageView.endPointY, DeviceWidth-80,  [UIScreen mainScreen].bounds.size.height/7*5/2)];
        ratingView.backgroundColor = [UIColor whiteColor];
        [bgView addSubview:ratingView];
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, ratingView.frame.size.width-20, 20)];
        titleLabel.text = @"title";
        titleLabel.textColor = [UIColor color333333];
        titleLabel.font = [UIFont boldSystemFontOfSize:17];
        districtLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, titleLabel.endPointY, titleLabel.frame.size.width, 20)];
        districtLabel.text = @"district";
        districtLabel.textColor = [UIColor color333333];
        districtLabel.font =[UIFont systemFontOfSize:13];
        [ratingView addSubview:titleLabel];
        [ratingView addSubview:districtLabel];
        UILabel *thankLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, districtLabel.endPointY, ratingView.frame.size.width, 30)];
        thankLabel.textAlignment = NSTextAlignmentCenter;
        thankLabel.text = @"Thank you";
        thankLabel.font = [UIFont boldSystemFontOfSize:17];
        UILabel *otherInfomation = [[UILabel  alloc] initWithFrame:CGRectMake(0, thankLabel.endPointY, ratingView.frame.size.width, 20)];
        otherInfomation.textAlignment = NSTextAlignmentCenter;
        otherInfomation.text = @"for dining with us!";
        otherInfomation.font = [UIFont systemFontOfSize:13];
        UILabel *questionLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, otherInfomation.endPointY, ratingView.frame.size.width, 20)];
        questionLabel.text = @"How was your dining experience?";
        questionLabel.textAlignment = NSTextAlignmentCenter;
        questionLabel.font = [UIFont systemFontOfSize:13];
        CGFloat starWitdth;
        CGFloat donotGoLabelY;
        if ([UIDevice isIphone5]) {
            starWitdth = 40;
            donotGoLabelY = 10;
        }else if ([UIDevice isIphone6]){
            starWitdth = 50;
            donotGoLabelY = 40;
        }else if ([UIDevice isIphone4Now]){
            starWitdth = 40;
            donotGoLabelY = 0;
        }else {
            starWitdth = 60;
            donotGoLabelY =60;
        }

        DLStarRatingControl *starControl = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(ratingView.frame.size.width/2-(ratingView.frame.size.width-24)/2, questionLabel.endPointY, ratingView.frame.size.width-24, 50) andStars:5 andStarWidth:starWitdth isFractional:NO spaceWidth:4];
        starControl.delegate = self;
        UILabel *donotGoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, starControl.endPointY+donotGoLabelY, ratingView.frame.size.width, 20)];
        NSMutableAttributedString *content = [[NSMutableAttributedString alloc] initWithString:@"I did not go"];
        NSRange contentRange = {0, [content length]};
        [content addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:contentRange];
        [content addAttribute:NSForegroundColorAttributeName value:[UIColor colorC2060A] range:contentRange];
        donotGoLabel.textAlignment = NSTextAlignmentCenter;
        donotGoLabel.font = [UIFont boldSystemFontOfSize:11];
        donotGoLabel.attributedText = content;
        donotGoLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDonotGoLabel)];
        if ([UIDevice isIphone4Now]) {
            ratingView.frame =CGRectMake(40, restaurantImageView.endPointY, DeviceWidth-80,[UIScreen mainScreen].bounds.size.height/7*5/2+30);
        }
        [donotGoLabel addGestureRecognizer:tap];
        [ratingView addSubview:thankLabel];
        [ratingView addSubview:otherInfomation];
        [ratingView addSubview:questionLabel];
        [ratingView addSubview:starControl];
        [ratingView addSubview:donotGoLabel];
    }
    return self;
}
-(void)tapDonotGoLabel{
    [self removeFromSuperview];
    if ([self.bookRatingSplashViewDelegate respondsToSelector:@selector(donotGoLabelTap:)]) {
        [self.bookRatingSplashViewDelegate donotGoLabelTap:self.splashRestaurant];
    }
}
-(void)setSplashRestaurant:(IMGReservation *)splashRestaurant{
    titleLabel.text = splashRestaurant.restaurantName;
    districtLabel.text = splashRestaurant.districtName;
    __weak typeof (restaurantImageView) weakImageView =restaurantImageView;
//    [restaurantImageView setImageWithURL:[splashRestaurant.imageUrl  returnFullImageUrlWithWidth:restaurantImageView.frame.size.width] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//        [UIView animateWithDuration:1.0 animations:^{
//            [weakImageView setAlpha:1.0];
//        }];
//    }];
    [restaurantImageView sd_setImageWithURL:[NSURL URLWithString:[splashRestaurant.imageUrl  returnFullImageUrlWithWidth:restaurantImageView.frame.size.width]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        image = [image imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-80,[UIScreen mainScreen].bounds.size.height/7*5/2)];
        [weakImageView setImage:image];
        }];

}
-(void)newRating:(DLStarRatingControl *)control :(float)rating{
    [self removeFromSuperview];
    if ([self.bookRatingSplashViewDelegate respondsToSelector:@selector(ratingTap:andRestaurant:)]) {
        [self.bookRatingSplashViewDelegate ratingTap:rating andRestaurant:self.splashRestaurant];
    }

}
@end
