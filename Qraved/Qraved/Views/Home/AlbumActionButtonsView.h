//
//  CardButtonsView.h
//  Qraved
//
//  Created by lucky on 16/3/8.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, AlbumActionButtonsType) {
    TypeForLike            = 0,
    TypeForComment         = 1,
    TypeForShare           = 2
};

@protocol AlbumActionButtonsDelegate

-(void)albumActionButtonsClick:(AlbumActionButtonsType)btnType withBtn:(UIButton*)btn;

@end


@interface AlbumActionButtonsView : UIView

-(id)initInSuperView:(UIView*)parentView atStartX:(CGFloat)X startY:(CGFloat)Y commentCount:(int)commentCount_ commentArray:(NSArray *)commentList_ liked:(BOOL)liked_ withFooter:(BOOL)withFooter_;
-(void)updateLikedStatus:(BOOL)isFirstLoad;

@property (nonatomic,weak) id<AlbumActionButtonsDelegate>delegate;

@end
