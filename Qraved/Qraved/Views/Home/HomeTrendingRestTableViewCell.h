//
//  HomeTrendingRestTableViewCell.h
//  Qraved
//
//  Created by System Administrator on 2/29/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "HomeTableViewCell.h"

@class IMGRestaurant;

@protocol HomeTrendingRestTableViewCellDelegate <NSObject>
- (void)homeTrendingSaveBtnClick:(BOOL)isSaved andRestaurant:(IMGRestaurant *)restaurant;

@end

@interface HomeTrendingRestTableViewCell : HomeTableViewCell<UIAlertViewDelegate>

@property(nonatomic,strong) IMGRestaurant *restaurant;
@property (nonatomic, copy) void (^isSaved)();
@property (nonatomic, copy) void (^unSaved)();
@property(nonatomic,weak) id<HomeTableViewCellDelegate> delegate;
@property (nonatomic,assign) id<HomeTrendingRestTableViewCellDelegate>homeTrendingRestTableViewCellDelegate;

@end
