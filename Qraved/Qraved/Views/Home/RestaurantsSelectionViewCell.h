//
//  MyListAddRestaurantCell.h
//  Qraved
//
//  Created by root on 9/26/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RestaurantsSelectionViewCell : UITableViewCell

@property(nonatomic,strong) IMGRestaurant *restaurant;

@end
