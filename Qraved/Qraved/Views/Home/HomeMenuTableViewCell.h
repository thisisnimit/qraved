//
//  HomeMenuTableViewCell.h
//  Qraved
//
//  Created by lucky on 16/4/11.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "HomeTableViewCell.h"
#import "HomeTableViewCell.h"
#import "UILikeCommentShareView.h"
#import "IMGRestaurantEvent.h"
#import "CardPhotosView.h"
#import "IMGMenu.h"
#import "TYAttributedLabel.h"
#import "IMGMenuCard.h"
@protocol HomeMenuTableViewCellDelegate <NSObject>

- (void)homeMenuSaveBtnClick:(BOOL)isSaved andRestaurant:(IMGRestaurant *)restaurant;
- (void)homeMenuTableViewCellImageClick:(IMGRestaurant *)restaurant andDishArr:(NSArray *)dishArr andIndex:(int)index;
- (void)homeMenuShareBtnClick:(IMGMenu *)menu withbtn:(UIView*)btn;

@end

@class IMGRestaurant;

@interface HomeMenuTableViewCell : HomeTableViewCell<CardPhotosViewDelegate,TYAttributedLabelDelegate>

@property(nonatomic,strong) IMGRestaurant *restaurant;
@property (nonatomic,weak) id<HomeMenuTableViewCellDelegate>menuDelegate;


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier;
-(void)setMuneCard:(IMGMenuCard *)menuCard;
-(void)setRestaurant:(IMGRestaurant *)restaurant andDishArr:(NSArray *)menuArr;
+(CGFloat)calculatedHeight:(IMGMenuCard*)menuCard;
+(CGFloat)calculatedHeight:(IMGRestaurant *)restaurant andMenuArr:(NSArray *)menuArr andRestaurantEvent:(IMGRestaurantEvent *)restaurantEvent;

@end
