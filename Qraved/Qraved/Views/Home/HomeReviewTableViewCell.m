//
//  HomeReviewTableViewCell.m
//  Qraved
//
//  Created by lucky on 16/3/1.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "HomeReviewTableViewCell.h"
#import "UIConstants.h"
#import "DLStarRatingControl.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"
#import "IMGRestaurant.h"
#import "UILabel+Helper.h"
#import "IMGDish.h"
#import "UIButton+WebCache.h"
#import "CXAHyperlinkLabel.h"
#import "NSString+CXAHyperlinkParser.h"
#import "CardPhotosView.h"
#import "UILikeCommentShareView.h"
#import "LoadingView.h"
#import "UICommentInputView.h"
#import "AppDelegate.h"
#import "ReviewHandler.h"
#import "Date.h"
#import "SHK.h"
#import "LikeView.h"
#import "HomeUtil.h"
#import "IMGLikeList.h"
#import "V2_LikeCommentShareView.h"

#define LEFTMARGIN 15
#define TEXTLABELTOPMARGIN 35
#define RIGHTMARGIN 30
#define TEXTLABELLEFTMARGIN 11+PROFILEIMAGEVIEWSIZE+8
#define PROFILEIMAGEVIEWSIZE 20
#define CONTENTLABELWIDTH DeviceWidth-LEFTMARGIN-PROFILEIMAGEVIEWSIZE-RIGHTMARGIN

#define SUMMARIZETEXTLENGTH 300

#define LINK_TEXT @"<a style=\"text-decoration:none;color:#3A8CE2;font-family:'OpenSans'\" href=\"http://www.qraved.com/\">...read more</a>"

@interface HomeReviewTableViewCell ()<UIWebViewDelegate,LikeListViewDelegate>
{
    UIButton *avatarLinkButton;
    TYAttributedLabel *titleLabel;
    UILabel *reviewAndPhotoCountLabel;
    UILabel *timeLabel;
    UIImageView *restaurantImageView;
    UILabel *lblName;
    
    UILabel *lblRestaurantName;
    UILabel *lblDistrictName;
    
    UILabel *reviewTitleLabel;
    UILabel *scoreLabel;
    DLStarRatingControl *ratingScoreControl;
    IMGReview *review;
    
    UIButton *uploadPhotoBtn;
    UILabel *likeLabel;
    UILabel *commentLabel;
    CardPhotosView *cardPhotosView;
    V2_LikeCommentShareView *likeCommentShareView;
    UIView *separateView;
    NSArray *dishArrM;
    
    UIView *reviewView;
    
    UIWebView *reviewSummarizeLabel;
    
    BOOL readMore;
    NSAttributedString *titleAttributedString;
    LikeView* likepopView;
    NSMutableArray* likelistArr;
    UIControl *control;
    UIButton *heartBtn;
}

@end

@implementation HomeReviewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
    if(self=[super initWithStyle:style reuseIdentifier:identifier]){
      
        [self createUI];
    }
    return self;
}

- (void)createUI{
    heartBtn = [UIButton new];
    [heartBtn setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
    [heartBtn addTarget:self action:@selector(heartClick:) forControlEvents:UIControlEventTouchUpInside];
    
    lblRestaurantName = [UILabel new];
    lblRestaurantName.font = [UIFont systemFontOfSize:14];
    lblRestaurantName.textColor = [UIColor color333333];
    
    lblDistrictName = [UILabel new];
    lblDistrictName.font = [UIFont systemFontOfSize:14];
    lblDistrictName.textColor = [UIColor color999999];
    
    UIImageView *topLine = [UIImageView new];
    topLine.image = [UIImage imageNamed:@"search_line"];
    
    avatarLinkButton = [UIButton new];
    [avatarLinkButton addTarget:self action:@selector(gotoOtherProfileWithUser) forControlEvents:UIControlEventTouchUpInside];
    
    lblName = [UILabel new];
    lblName.font = [UIFont systemFontOfSize:14];
    lblName.textColor = [UIColor color333333];
    
    
    reviewAndPhotoCountLabel = [UILabel new];
    reviewAndPhotoCountLabel.font = [UIFont systemFontOfSize:12];
    reviewAndPhotoCountLabel.textColor = [UIColor color999999];
    
    timeLabel = [UILabel new];
    timeLabel.font = [UIFont systemFontOfSize:12];
    timeLabel.textColor = [UIColor color999999];
    timeLabel.textAlignment = NSTextAlignmentRight;
    
//    reviewView = [[UIView alloc] init];
//    reviewView.userInteractionEnabled = YES;
//    UITapGestureRecognizer *reviewViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoCardDetailPage)];
//    [reviewView addGestureRecognizer:reviewViewTap];
    
    
    ratingScoreControl=[[DLStarRatingControl alloc] initWithFrame:CGRectMake(LEFTLEFTSET + 25,titleLabel.endPointY, 73, 14) andStars:5 andStarWidth:13 isFractional:NO spaceWidth:2];
    ratingScoreControl.userInteractionEnabled=NO;
    //[reviewView addSubview:ratingScoreControl];

    
    reviewSummarizeLabel = [[UIWebView alloc] init];
    reviewSummarizeLabel.tag=711;
    reviewSummarizeLabel.delegate = self;
    reviewSummarizeLabel.backgroundColor=[UIColor whiteColor];
    reviewSummarizeLabel.scrollView.showsHorizontalScrollIndicator = NO;
//    [reviewView addSubview:reviewSummarizeLabel];
   
    
    cardPhotosView = [[CardPhotosView alloc] init];
    
    
    uploadPhotoBtn = [UIButton new];
    [uploadPhotoBtn setTitle:@"Upload Photo" forState:UIControlStateNormal];
    [uploadPhotoBtn setTitleColor:[UIColor colorWithHexString:@"F10F33"] forState:UIControlStateNormal];
    [uploadPhotoBtn setImage:[UIImage imageNamed:@"ic_upload_photo"] forState:UIControlStateNormal];
    uploadPhotoBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    uploadPhotoBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [uploadPhotoBtn addTarget:self action:@selector(uploadPhotoBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *bottomLine = [UIImageView new];
    bottomLine.image = [UIImage imageNamed:@"search_line"];
    
    likeCommentShareView = [V2_LikeCommentShareView new];
    likeCommentShareView.delegate = self;
    
    UIButton *btn = [UIButton new];
    [likeCommentShareView addSubview:btn];
    
    separateView = [UIView new];
    separateView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    
    [self.contentView sd_addSubviews:@[heartBtn,lblRestaurantName,lblDistrictName,topLine,avatarLinkButton,lblName,reviewAndPhotoCountLabel,timeLabel,ratingScoreControl,reviewSummarizeLabel,cardPhotosView,uploadPhotoBtn,bottomLine,likeCommentShareView,separateView]];
    
    heartBtn.sd_layout
    .widthIs(40)
    .heightIs(34)
    .rightSpaceToView(self.contentView,15)
    .topSpaceToView(self.contentView,5);
    heartBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);

    
    lblRestaurantName.sd_layout
    .topSpaceToView(self.contentView, 0)
    .leftSpaceToView(self.contentView,15)
    .heightIs(44);
    
    [lblRestaurantName setSingleLineAutoResizeWithMaxWidth:DeviceWidth-55-80];
    
    
    lblDistrictName.sd_layout
    .topEqualToView(lblRestaurantName)
    .rightSpaceToView(heartBtn,5)
    .heightIs(44)
    .leftSpaceToView(lblRestaurantName,3);

    
    topLine.sd_layout
    .topSpaceToView(lblRestaurantName, 0)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(1);
    
    avatarLinkButton.sd_layout
    .topSpaceToView(topLine, 10)
    .leftEqualToView(topLine)
    .widthIs(40)
    .heightIs(40);
    
    lblName.sd_layout
    .topEqualToView(avatarLinkButton)
    .leftSpaceToView(avatarLinkButton, 10)
    .heightIs(16);
    
    [lblName setSingleLineAutoResizeWithMaxWidth:DeviceWidth-80];
    
    reviewAndPhotoCountLabel.sd_layout
    .topSpaceToView(lblName, 5)
    .leftSpaceToView(avatarLinkButton, 10)
    .heightIs(14);
    
    [reviewAndPhotoCountLabel setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
    timeLabel.sd_layout
    .topEqualToView(reviewAndPhotoCountLabel)
    .rightSpaceToView(self.contentView, 15)
    .leftSpaceToView(reviewAndPhotoCountLabel, 10)
    .heightIs(14);
    
    
    ratingScoreControl.sd_layout
    .topSpaceToView(avatarLinkButton,10)
    .leftSpaceToView(self.contentView, 15)
    .widthIs(75)
    .heightIs(14);
    
    reviewSummarizeLabel.sd_layout
    .topSpaceToView(ratingScoreControl, 5)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(0);
    
    cardPhotosView.sd_layout
    .topSpaceToView(reviewSummarizeLabel, 10)
    .leftSpaceToView(self.contentView, 0)
    .rightSpaceToView(self.contentView, 0)
    .autoHeightRatio(0);
    
    uploadPhotoBtn.sd_layout
    .topSpaceToView(cardPhotosView, 10)
    .centerXEqualToView(cardPhotosView)
    .widthIs(150)
    .heightIs(18);
    
    bottomLine.sd_layout
    .topSpaceToView(uploadPhotoBtn, 15)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(1);
    
    likeCommentShareView.sd_layout
    .topSpaceToView(bottomLine, 0)
    .leftSpaceToView(self.contentView, 0)
    .rightSpaceToView(self.contentView, 0)
    .autoHeightRatio(0);
    
    btn.sd_layout
    .topSpaceToView(likeCommentShareView,0)
    .leftSpaceToView(likeCommentShareView,DeviceWidth/3 - 60)
    .widthIs(60)
    .heightIs(50);
    [btn addTarget:self action:@selector(likeCountClick:) forControlEvents:UIControlEventTouchUpInside];
    
    separateView.sd_layout
    .topSpaceToView(likeCommentShareView,0)
    .leftSpaceToView(self.contentView,0)
    .rightSpaceToView(self.contentView,0)
    .heightIs(20);

}

-(void)setReviewCard:(IMGReviewCard *)reviewCard{
    _reviewCard = reviewCard;
    IMGRestaurant *restaurant = reviewCard.reviewCardRestaurant;
    IMGReview *reviewTemp = reviewCard.reviewCardReview;
    NSMutableArray *dishArr = reviewCard.dishArrM;
//    BOOL isReadMore = (bool)[reviewCard.reveiwCardIsReadMore objectForKey:reviewCard.reviewCardReview.targetId];
    BOOL isReadMore = reviewCard.isReadMore;
    readMore = isReadMore;
    _restaurant = restaurant;
    dishArrM  = dishArr;
    NSURL *urlString;
    review = reviewTemp;
    

    if ([restaurant.saved isEqual:@1]) {
        [heartBtn setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
    }else{
        [heartBtn setImage:[UIImage imageNamed:@"ic_heart_home"] forState:UIControlStateNormal];
    }
    review.avatar = [review.avatar stringByReplacingPercentEscapesUsingEncoding:
                     NSASCIIStringEncoding];
    if ([review.avatar hasPrefix:@"http"])
    {
        urlString = [NSURL URLWithString:[review.avatar filterHtml]];
    }
    else
    {
        urlString = [NSURL URLWithString:[review.avatar returnFullImageUrl]];
    }
   
    NSString *path_sandox = NSHomeDirectory();
    //设置一个图片的存储路径
    NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/avastar.png"];
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    if (image&&[review.userId intValue] == [[IMGUser currentUser].userId intValue]) {
        image = [image cutCircleImage];
        [avatarLinkButton setImage:image forState:UIControlStateNormal];
    }else{
        __weak UIButton *weakAvatarLinkButton = avatarLinkButton;
        [avatarLinkButton sd_setImageWithURL:urlString forState:UIControlStateNormal placeholderImage:[[UIImage imageNamed:@"headDefault.jpg"] cutCircleImage] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image.size.height>0&&image.size.width>0) {
                                image = [image cutCircleImage];
                                [weakAvatarLinkButton setImage:image forState:UIControlStateNormal];
                            }
        }];
    }

    
    lblRestaurantName.text = restaurant.title;
    lblDistrictName.text = restaurant.districtName;
    
    lblName.text = [NSString stringWithFormat:@"%@ wrote a review",review.fullName];
    
    
    if ([review.userReviewCount intValue]!=0&&[review.userDishCount intValue]!=0) {
        
        [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@ • %@ %@",review.userDishCount,[review.userDishCount intValue] > 1 ? @"photos":@"photo",review.userReviewCount,[review.userReviewCount intValue] > 1 ? @"reviews":@"review"]];

    }else if ([review.userDishCount intValue]==0||[review.userReviewCount intValue]==0){
        if ([review.userReviewCount intValue]==0&&[review.userDishCount intValue]!=0) {
            
            [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@",review.userDishCount,[review.userDishCount intValue] > 1 ? @"photos":@"photo"]];
            
        }
        if ([review.userDishCount intValue]==0&&[review.userReviewCount intValue]!=0) {
            
            [reviewAndPhotoCountLabel setText:[NSString stringWithFormat:@"%@ %@",review.userReviewCount,[review.userReviewCount intValue] > 1 ? @"reviews":@"review"]];

        }
        
    }
    
    [timeLabel setText:[Date getTimeInteval_v4:[review.timeline longLongValue]/1000]];
    
   // [reviewTitleLabel setText:review.title];

    float score = [review.score floatValue];

    [ratingScoreControl updateRating:[NSNumber numberWithFloat:score]];
    
    NSString *summarize=[NSString stringWithFormat:@"%@. %@",review.title,review.summarize];
    NSDictionary *strBackDic = [HomeReviewTableViewCell getIsNeedReadMoreForOverLines:summarize];

    if([strBackDic[@"isNeedLoadMore"] boolValue]&&!readMore){
        summarize= strBackDic[@"finalStr"];
    }else{
        summarize=[summarize stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
        if(!readMore && [self getStringLengthWithString:summarize]>300){
            
            if ([HomeReviewTableViewCell stringContainsEmoji:[summarize substringWithRange:NSMakeRange(299,2)]]){
                 summarize=[NSString stringWithFormat:@"%@%@",[summarize substringToIndex:299] ,LINK_TEXT];
            }else{
                summarize=[NSString stringWithFormat:@"%@%@",[summarize substringToIndex:300],LINK_TEXT];
            }

        }
    }
    CGFloat height = [self calculateCommentTextHeight:summarize]+5;

    reviewSummarizeLabel.sd_layout
    .heightIs(height);
    [reviewSummarizeLabel updateLayout];
    NSString *summarizeStr = [NSString stringWithFormat:@"<html> <head> <meta charset=\"UTF-8\">"
                              "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">"
                              "<style type=\"text/css\">"
                              "html {"
                              "    -webkit-text-size-adjust: none; /* Never autoresize text */"
                              "}"
                              "body{font-family:\"%@\";font-size: 12px; color:%@; padding:0; margin:0; width:%fpx; line-height: 15px;}"
                              "</style>"
                              "</head>"
                              "<body><div id=\"content\">%@</div></body>"
                              "</html>",DEFAULT_FONT_NAME,@"#929da7",DeviceWidth - LEFTLEFTSET*2,summarize];
    
    summarizeStr = [summarizeStr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
    [reviewSummarizeLabel loadHTMLString:summarizeStr baseURL:nil];
    
    [reviewSummarizeLabel layoutSubviews];
    
    
    
    if (![review.imageHeight floatValue]) {
        review.imageHeight = [NSNumber numberWithFloat:ONE_PHOTO_HEIGHT];
    }
    if (dishArrM.count) {
        if (dishArrM.count == 1)
        {
            cardPhotosView.cardPhotoHeight = [review.imageHeight floatValue];
        }
        
        uploadPhotoBtn.hidden = YES;
        uploadPhotoBtn.sd_layout
        .heightIs(0);
    }else{
        IMGUser *user = [IMGUser currentUser];
        if ([user.userId intValue] == [review.userId intValue])
        {
            uploadPhotoBtn.hidden = NO;
            uploadPhotoBtn.sd_layout
            .heightIs(18);
        }else{
            uploadPhotoBtn.hidden = YES;
            uploadPhotoBtn.sd_layout
            .heightIs(0);
        }
    }
    [cardPhotosView setPhotos:dishArrM];
    cardPhotosView.homeTableViewCellDelegate=self.homeTableViewCellDelegate;
    cardPhotosView.cardPhotosViewDelegate = self;
    [uploadPhotoBtn updateLayout];
    
    [likeCommentShareView setCommentCount:[review.commentCount intValue] likeCount:[review.likeCount intValue] liked:review.isLike];
    
    
    [self setupAutoHeightWithBottomView:separateView bottomMargin:0];

}
#pragma mark - webview delegate

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    CGFloat htmlHeight = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] floatValue];
//    webView.sd_layout
//    .heightIs(htmlHeight);
 
    UIScrollView *tempView = (UIScrollView *)[reviewSummarizeLabel.subviews objectAtIndex:0];
    tempView.scrollEnabled = NO;
    
    
    
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if (navigationType==UIWebViewNavigationTypeLinkClicked) {
        readMore = !readMore;
        [[Amplitude instance] logEvent:@"CL - Read More on Review" withEventProperties:@{@"Location":@"Homepage"}];
        
        if (self.homeReviewDelegate) {
            [self.homeReviewDelegate HomeReviewTableCell:self readMoreTapped:_reviewCard indexPath:self.fromCellIndexPath];
        }
        
        return NO;
    }
    
    return YES;
    
    
}

-(void)likeCountClick:(UIButton*)btn

{
    [[Amplitude instance] logEvent:@" RC - View People Who Likes"];
    [self userReviewCardAmplitude];
    if (likepopView) {
        [likepopView removeFromSuperview];
    }
    [[LoadingView sharedLoadingView]startLoading];
   
    [HomeUtil getLikeListFromServiceWithReviewId:review.reviewId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
        
            likelistArr=[NSMutableArray arrayWithArray:likeListArray];
       likepopView.likecount=likeviewcount;
        [[LoadingView sharedLoadingView]stopLoading];

        likepopView.likelistArray=likelistArr;

    }];
    likepopView=[[LikeView alloc]initWithFrame:CGRectMake(0, -20, DeviceWidth, DeviceHeight)];
    likepopView.delegate=self;
    UIResponder *responder = [self nextResponder];
    while (responder){
        
        if ([responder isKindOfClass:[UINavigationController class]]){
            UINavigationController* vc=(UINavigationController*)responder;
        [[vc.viewControllers lastObject].view addSubview:likepopView];
            likepopView.layer.zPosition=1000;
            return;
        }
        
        responder = [responder nextResponder];
    }
  
}

-(void)LikeviewLoadMoreData:(NSInteger)offset{
    
    [HomeUtil getLikeListFromServiceWithReviewId:review.reviewId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset]andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
        likepopView.likecount=likeviewcount;
        [likelistArr addObjectsFromArray:likeListArray];
        [likepopView.likeUserTableView reloadData];
    }];


}
-(void)returnLikeCount:(NSInteger)count
{
    
    if (count!=0) {
        
   
       float currentPoint=reviewView.endPointY;
        if (dishArrM.count)
    {
        
        
        currentPoint = cardPhotosView.endPointY + 10;
    }
    else
    {
        IMGUser *user = [IMGUser currentUser];
        if ([user.userId intValue] == [review.userId intValue])
        {
           // uploadPhotoBtn.hidden = NO;
//            uploadPhotoBtn.frame = CGRectMake(DeviceWidth/2 - 90, currentPoint + 20, 180, 35);
//            currentPoint = uploadPhotoBtn.endPointY + 10;
        }
    }
//    float start = LEFTLEFTSET;
//    float top = currentPoint;
//    
//
//    
//    NSString *likeString=[NSString stringWithFormat:@"%ld %@",(long)count,count>1?L(@"Likes"):L(@"Like")];
//    [likeLabel setText:likeString];
// likeLabel.frame=CGRectMake(start, top, likeLabel.expectedWidth,20);
    }


}
- (void)gotoOtherProfileWithUser
{
    [self userReviewCardAmplitude];
    IMGUser *user = [[IMGUser alloc] init];
    user.userId = [NSNumber numberWithInt:[review.userId intValue] ];
    user.userName = review.fullName;
    user.token = @"";
    user.avatar = review.avatar;
    [self.homeTableViewCellDelegate gotoOtherProfileWithUser:user];

}

+(NSDictionary *)getIsNeedReadMoreForOverLines:(NSString*)commentStr_{
    
    UILabel *lblTempStr = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, CONTENTLABELWIDTH, MAXFLOAT)];
    lblTempStr.text = commentStr_;
    lblTempStr.font = [UIFont systemFontOfSize:12];
    
    
    NSString *text = [lblTempStr text];
    UIFont   *font = [lblTempStr font];
    CGRect    rect = [lblTempStr frame];
    
    
    CTFontRef myFont = CTFontCreateWithName((CFStringRef)font.fontName,
                                            font.pointSize,
                                            NULL);
    
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:text];
    [attStr addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)myFont range:NSMakeRange(0, attStr.length)];
    
    CTFramesetterRef frameSetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)attStr);
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, CGRectMake(0,0,rect.size.width,100000));
    
    CTFrameRef frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, 0), path, NULL);
    
    NSArray *lines = (__bridge NSArray *)CTFrameGetLines(frame);
    
    NSMutableArray *linesArray = [[NSMutableArray alloc]init];
    
    NSMutableDictionary *strBackDic = [[NSMutableDictionary alloc]init];
    
    if(lines.count<10+1){
        [strBackDic setObject:@NO forKey:@"isNeedLoadMore"];
    }else{
        [strBackDic setObject:@YES forKey:@"isNeedLoadMore"];
        for (id line in lines)
        {
            CTLineRef lineRef = (__bridge CTLineRef)line;
            CFRange lineRange = CTLineGetStringRange(lineRef);
            NSRange range = NSMakeRange(lineRange.location, lineRange.length);
            
            NSString *lineString = [text substringWithRange:range];
            
            //            CFRelease(lineRef);
            [linesArray addObject:lineString];
            
        }
        
        //start range work for 4th line
        NSMutableAttributedString *attStr4Th = [[NSMutableAttributedString alloc] initWithString:linesArray[10-1]];
        [attStr4Th addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)myFont range:NSMakeRange(0, attStr4Th.length)];
        CTFramesetterRef frameSetter4Th = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)attStr4Th);
        
        CGMutablePathRef path4Th = CGPathCreateMutable();
        CGPathAddRect(path4Th, NULL, CGRectMake(0,0,CONTENTLABELWIDTH-70,MAXFLOAT));
        
        CTFrameRef frame4Th = CTFramesetterCreateFrame(frameSetter4Th, CFRangeMake(0, 0), path4Th, NULL);
        
        NSArray *lines4Th = (__bridge NSArray *)CTFrameGetLines(frame4Th);
        CTLineRef lineRef4Th = (__bridge CTLineRef )[lines4Th firstObject];
        CFRange lineRange4Th = CTLineGetStringRange(lineRef4Th);
        NSRange range4Th = NSMakeRange(lineRange4Th.location, lineRange4Th.length);
        NSString *lineString4Th = [linesArray[10-1] substringWithRange:range4Th];
        
        NSString *finalStr = @"";
        for(int i=0; i<10-1; i++){
            finalStr = [finalStr stringByAppendingString:linesArray[i]];
        }
        lineString4Th=[lineString4Th stringByReplacingOccurrencesOfString:@"\n" withString:@""];

        finalStr = [finalStr stringByAppendingString:lineString4Th];
        //        finalStr = [finalStr stringByAppendingString:@"<a href=\"http://www.qraved.com/\">...read more</a>"];
        
        finalStr = [finalStr stringByAppendingString:LINK_TEXT];
        
        
        finalStr=[finalStr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
        [strBackDic setObject:finalStr forKey:@"finalStr"];
        
        CFRelease(frame4Th);
        CFRelease(frameSetter4Th);
        CFRelease(path4Th);
//        CFRelease(lineRef4Th);
        
    }
    
    CFRelease(frame);
    CFRelease(myFont);
    CFRelease(frameSetter);
    CFRelease(path);
    return strBackDic;
}

-(CGFloat)calculateCommentTextHeight:(NSString*)commentStr_{
    
    commentStr_=[commentStr_ stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];
    commentStr_=[commentStr_ stringByReplacingOccurrencesOfString:LINK_TEXT withString:@"...read more"];
    
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12]};
    
    CGSize textSize = [commentStr_ boundingRectWithSize:CGSizeMake(DeviceWidth - LEFTLEFTSET*2, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    
    float height = ceil(textSize.height + 1.5f);
    
    return height;
}

-(void)btnClick_btn:(UIButton*)button{
    [self btnClick:button.tag];
}

-(void)btnClick_tap:(UIGestureRecognizer*)tap{
    [self btnClick:tap.view.tag];
}

-(void)btnClick:(NSInteger)index{
    switch(index){
        case 0:{
            NSLog(@"0");
        }
            break;
        case 1:{
            NSLog(@"1");
        }
            break;
        case 2:{
            NSLog(@"2");
        }
            break;
            
    }
}

- (void)uploadPhotoBtnClick
{
    if (self.homeReviewDelegate)
    {
        [self.homeReviewDelegate uploadPhotoButtonClickWithRestaurant:_restaurant andReview:review];
    }
}

- (void)gotoCardDetailPage
{
    [self userReviewCardAmplitude];
    if (self.homeTableViewCellDelegate)
    {
        NSDictionary *dataDic = [[NSDictionary alloc] initWithObjectsAndKeys:_restaurant,@"restaurant",dishArrM,@"dataArr",review,@"review", nil];
        
        [self.homeTableViewCellDelegate goToCardDetailPageWithType:14 andData:dataDic andCellIndexPath:self.fromCellIndexPath];
        
        
        
    }
}



+(CGFloat)calculatedCommentTextHeight:(NSString*)comment{
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12]};
    CGSize singleSize = [@"test" boundingRectWithSize:CGSizeMake(DeviceWidth-LEFTMARGIN-PROFILEIMAGEVIEWSIZE-RIGHTMARGIN, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    CGSize textSize = [comment boundingRectWithSize:CGSizeMake(DeviceWidth-LEFTMARGIN-PROFILEIMAGEVIEWSIZE-RIGHTMARGIN, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    NSInteger lines = textSize.height/singleSize.height;
    CGFloat height=0.0f;
    if(lines>10){
        height=10*singleSize.height;
    }else{
        height=textSize.height;
    }
    return height;
}

- (void)likeCommentShareView:(UIView*)likeCommentShareView_ viewPreviousCommentsTapped:(UIButton*)button lastComment:(id)comment{
    //goto CDP screen
    [[Amplitude instance] logEvent:@"CL - View Previous Comments CTA" withEventProperties:@{@"Location":@"Review card on homepage"}];
    [self gotoCardDetailPage];
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView_ likeButtonTapped:(UIButton *)button{
    [self userReviewCardAmplitude];
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
//    [[LoadingView sharedLoadingView] startLoading];
    //like service
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    //Review_ID, ReviewerUser_ID, Restaurant_ID, Location
    [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
    [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
    [eventProperties setValue:review.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:@"Review card on homepage" forKey:@"Location"];
    [ReviewHandler likeReview:review.isLike reviewId:review.reviewId andBlock:^{
        if (!review.isLike) {
            
            if (dishArrM.count>1) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                //Review_ID, ReviewerUser_ID, Restaurant_ID, Location
                [eventProperties setValue:review.reviewId forKey:@"MultiPhoto_ID"];
                [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
                [eventProperties setValue:review.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:@"Review card on homepage" forKey:@"Location"];

                [[Amplitude instance] logEvent:@"UC - Unlike Multi Photo Succeed" withEventProperties:eventProperties];
            }else{
                [[Amplitude instance] logEvent:@"UC - UnLike Review Succeed" withEventProperties:eventProperties];
            }
            

        }else{
            if (dishArrM.count>1) {
                
                [[Amplitude instance] logEvent:@"UC - Like multi photo Succeed" withEventProperties:eventProperties];

            }else{
            
                [[Amplitude instance] logEvent:@"UC - Like Review Succeed" withEventProperties:eventProperties];

            }
        }
    } failure:^(NSString *exceptionMsg){
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
    }];
    
    review.isLike=!review.isLike;
    if(review.isLike){
        review.likeCount=[NSNumber numberWithInt:[review.likeCount intValue]+1];
        
        [[Amplitude instance] logEvent:@"UC – Like Review Initiate" withEventProperties:eventProperties];
        
        if (dishArrM.count>1) {
            [[Amplitude instance] logEvent:@"UC - Like multi photo Initiate" withEventProperties:eventProperties];
        }else{
        
            [[Amplitude instance] logEvent:@"UC – Like Review Initiate" withEventProperties:eventProperties];
        }

        
    }else{
        review.likeCount=[NSNumber numberWithInt:[review.likeCount intValue]-1];

        if (dishArrM.count>1) {
            [[Amplitude instance] logEvent:@"UC - Unlike Multi Photo Initiate" withEventProperties:eventProperties];
        }else{
        
            [[Amplitude instance] logEvent:@"UC - UnLike Review Initiate" withEventProperties:eventProperties];
        }

        [[Amplitude instance] logEvent:@"UC - UnLike Review Initiate" withEventProperties:eventProperties];
    }
    
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self likeButtonTapped:button entity:review];
    }

    
}
-(void)likeCommentShareView:(UIView *)likeCommentShareView shareButtonTapped:(UIButton *)button{
    [self userReviewCardAmplitude];
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self shareButtonTapped:button entity:review];
    }
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView commentButtonTapped:(UIButton *)button{
    //home card cell都留空
    
    NSDictionary *dataDic = [[NSDictionary alloc] initWithObjectsAndKeys:review,@"review", nil];
    
    [self.homeTableViewCellDelegate goToCardDetailPageWithType:14 andData:dataDic andCellIndexPath:self.fromCellIndexPath];
}


- (void)likeCommentShareView:(UIView*)likeCommentShareView_ commentInputView:(UIView *)commentInputView inputViewExpand:(UITextView *)inputTextView commentInputViewYOffset:(CGFloat)commentInputViewYOffset{
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self likeCommentShareView:likeCommentShareView_ commentInputView:commentInputView inputViewExpand:inputTextView commentInputViewYOffset:commentInputViewYOffset];
    }
}

- (void)likeCommentShareView:(UIView*)likeCommentShareView_ commentInputView:(UIView *)commentInputView postButtonTapped:(UIButton *)postButton commentInputText:(NSString *)text{
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
    [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
    [eventProperties setValue:review.fullName forKey:@"ReviewFullName"];
    [eventProperties setValue:_restaurant.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:@"Review card on homepage" forKey:@"Location"];

    [[Amplitude instance] logEvent:@"UC - Comment Review Initiate" withEventProperties:eventProperties];

    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    postButton.enabled = NO;
    if(self.homeTableViewCellDelegate){
        [ReviewHandler postComment:text reviewId:review.reviewId restaurantId:_restaurant.restaurantId andBlock:^(IMGReviewComment *reviewComment) {
            postButton.enabled = YES;
            review.commentCount=[NSNumber numberWithInt:([review.commentCount intValue]+1)];
            if (review.commentList) {
                if([review.commentList isKindOfClass:[NSArray class]]){
                    review.commentList=[NSMutableArray arrayWithArray:review.commentList];
                }
                [review.commentList insertObject:reviewComment atIndex:0];
            }else{
                review.commentList = [[NSMutableArray alloc]initWithObjects:reviewComment, nil];
            }
            [self.homeTableViewCellDelegate homeCardTableViewCell:self newCommentPosted:reviewComment];
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
            [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
            [eventProperties setValue:review.fullName forKey:@"ReviewFullName"];
            [eventProperties setValue:reviewComment.commentId forKey:@"Comment_ID"];
            [eventProperties setValue:_restaurant.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:@"Review card on homepage" forKey:@"Location"];
            [[Amplitude instance] logEvent:@"UC - Comment Review Succeed" withEventProperties:eventProperties];
            [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Review Succeed" withValues:eventProperties];

        } failure:^(NSString *errorReason){
            if([errorReason isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
                [[AppDelegate ShareApp] goToLoginController];
            }
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
            [eventProperties setValue:review.userId forKey:@"ReviewerUser_ID"];
            [eventProperties setValue:_restaurant.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:[NSString stringWithFormat:@"%@",errorReason] forKey:@"Reason"];
            [eventProperties setValue:@"Review card on homepage" forKey:@"Location"];
            [[Amplitude instance] logEvent:@"UC - Comment Review Failed" withEventProperties:eventProperties];
        }];


    }
}

- (void)likeCommentShareView:(UIView*)likeCommentShareView_ commentInputView:(UIView *)view inputViewDidBeginEditing:(UITextView *)textView{
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self likeCommentShareView:likeCommentShareView_ commentInputView:view inputViewDidBeginEditing:textView];
    }
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView commentCell:(UITableViewCell *)cell readMoreTapped:(id)comment offset:(CGFloat)offset{
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self commentCell:cell readMoreTapped:comment offset:offset];
    }
}
#pragma mark - privates
- (NSAttributedString *)attributedString:(NSArray *__autoreleasing *)outURLs
                               URLRanges:(NSArray *__autoreleasing *)outURLRanges andText:(NSString *)text
{
    NSString *HTMLText = text;
    NSArray *URLs = [[NSArray alloc] init];
    NSArray *URLRanges = [[NSArray alloc] init];
    UIColor *color = [UIColor grayColor];
    //    UIFont *font = [UIFont fontWithName:@"Baskerville" size:17.];
    UIFont *font = [UIFont systemFontOfSize:15];
    NSMutableParagraphStyle *mps = [[NSMutableParagraphStyle alloc] init];
//        mps.lineSpacing = ceilf(font.pointSize * .5);
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor whiteColor];
    shadow.shadowOffset = CGSizeMake(0, 1);
    NSString *str = [NSString stringWithHTMLText:HTMLText baseURL:nil URLs:&URLs URLRanges:&URLRanges];
    NSMutableAttributedString *mas = [[NSMutableAttributedString alloc] initWithString:str attributes:@
                                      {
                                          NSForegroundColorAttributeName : color,
                                          NSFontAttributeName            : font,
                                          NSParagraphStyleAttributeName  : mps,
                                          NSShadowAttributeName          : shadow,
                                      }];
    [URLRanges enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
        [mas addAttributes:@
         {
             NSForegroundColorAttributeName : [UIColor color222222],
             //       NSUnderlineStyleAttributeName  : @(NSUnderlineStyleSingle)
             NSForegroundColorAttributeName : [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:15]
             
         } range:[obj rangeValue]];
    }];
    
    *outURLs = URLs;
    *outURLRanges = URLRanges;
    
    return [mas copy];
}
-(void)cardPhotosViewImageClick:(int)imageIndex{
    [self.homeReviewDelegate HomeReviewTableViewCellImageClick:_restaurant andReview:review andDishArr:dishArrM andIndex:imageIndex andFromCellIndexPath:self.fromCellIndexPath];
    
}
- (void)cardPhotosChangeHeight:(CGFloat)height
{
    if ([review.imageHeight floatValue] != ONE_PHOTO_HEIGHT)
    {
        return;
    }
    if (height <= ONE_PHOTO_HEIGHT)
    {
        return;
    }

    review.imageHeight = [NSNumber numberWithFloat:height];
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewReloadCell:self];
    }

}
- (void)attributedLabel:(TYAttributedLabel *)attributedLabel textStorageClicked:(id<TYTextStorageProtocol>)TextRun atPoint:(CGPoint)point{
    
    
    if ([TextRun isKindOfClass:[TYLinkTextStorage class]]) {
        
        NSString *linkStr = ((TYLinkTextStorage*)TextRun).linkData;
        
        if ([linkStr hasPrefix:@"user"]) {

            IMGUser *user = [[IMGUser alloc] init];
            user.userId = [NSNumber numberWithInt:[review.userId intValue] ];
            user.userName = review.fullName;
            user.token = @"";
            user.avatar = review.avatar;
            [self.homeTableViewCellDelegate gotoOtherProfileWithUser:user];
            
        }else {
            [self.homeTableViewCellDelegate gotoRestaurantWithRestaurantId:_restaurant.restaurantId];
        }
        [self userReviewCardAmplitude];
    }
    
}

-(void)userReviewCardAmplitude{
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:review.reviewId forKey:@"review_ID"];
    [eventProperties setValue:review.userId forKey:@"reviewUser_ID"];
    [eventProperties setValue:@"Homepage" forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL - User Review Card" withEventProperties:eventProperties];
}
+ (BOOL)stringContainsEmoji:(NSString *)string
{
    __block BOOL returnValue = NO;
    
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length])
                               options:NSStringEnumerationByComposedCharacterSequences
                            usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                                const unichar hs = [substring characterAtIndex:0];
                                if (0xd800 <= hs && hs <= 0xdbff) {
                                    if (substring.length > 1) {
                                        const unichar ls = [substring characterAtIndex:1];
                                        const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                                        if (0x1d000 <= uc && uc <= 0x1f77f) {
                                            returnValue = YES;
                                        }
                                    }
                                } else if (substring.length > 1) {
                                    const unichar ls = [substring characterAtIndex:1];
                                    if (ls == 0x20e3) {
                                        returnValue = YES;
                                    }
                                } else {
                                    if (0x2100 <= hs && hs <= 0x27ff) {
                                        returnValue = YES;
                                    } else if (0x2B05 <= hs && hs <= 0x2b07) {
                                        returnValue = YES;
                                    } else if (0x2934 <= hs && hs <= 0x2935) {
                                        returnValue = YES;
                                    } else if (0x3297 <= hs && hs <= 0x3299) {
                                        returnValue = YES;
                                    } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50) {
                                        returnValue = YES;
                                    }
                                }
                            }];
    
    return returnValue;
}
- (NSInteger)getStringLengthWithString:(NSString *)string
{
    __block NSInteger stringLength = 0;
    
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length])
                               options:NSStringEnumerationByComposedCharacterSequences
                            usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop)
     {
         const unichar hs = [substring characterAtIndex:0];
         if (0xd800 <= hs && hs <= 0xdbff)
         {
             if (substring.length > 1)
             {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f)
                 {
                     stringLength += 1;
                 }
                 else
                 {
                     stringLength += 1;
                 }
             }
             else
             {
                 stringLength += 1;
             }
         } else if (substring.length > 1)
         {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3)
             {
                 stringLength += 1;
             }
             else
             {
                 stringLength += 1;
             }
         } else {
             if (0x2100 <= hs && hs <= 0x27ff)
             {
                 stringLength += 1;
             }
             else if (0x2B05 <= hs && hs <= 0x2b07)
             {
                 stringLength += 1;
             }
             else if (0x2934 <= hs && hs <= 0x2935)
             {
                 stringLength += 1;
             }
             else if (0x3297 <= hs && hs <= 0x3299)
             {
                 stringLength += 1;
             }
             else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50)
             {
                 stringLength += 1;
             }
             else
             {
                 stringLength += 1;
             }
         }
     }];
    
    return stringLength;
}

- (void)heartClick:(UIButton *)btn{
    if (self.homeTableViewCellDelegate) {
        [self.homeTableViewCellDelegate homeCardTableViewReloadCell:self.fromCellIndexPath entity:_restaurant];
    }
}

@end
