//
//  HomeRestUpdateTableViewCell.m
//  Qraved
//
//  Created by System Administrator on 2/26/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "HomeRestUpdateTableViewCell.h"
#import "UIConstants.h"
#import "DLStarRatingControl.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"
#import "IMGRestaurant.h"
#import "UILabel+Helper.h"
#import "IMGDish.h"
#import "CXAHyperlinkLabel.h"
#import "NSString+CXAHyperlinkParser.h"
#import "IMGRestaurantEvent.h"
#import "LoadingView.h"
#import "UICommentInputView.h"
#import "RestaurantHandler.h"
#import "AppDelegate.h"
#import "IMGUser.h"
#import "Date.h"
#import "HBVLinkedTextView.h"
#import "LikeView.h"
#import "HomeUtil.h"


#define LEFT_X 15

#define TITLE_LABEL_Y 13
#define TITLE_LABEL_HEIGHT 20
#define TITLE_LABEL_FONT 15

#define GAP_BETWEEN_TITLE_AND_CUISINE 1

#define CUISINE_DISTRICT_LABEL_HEIGHT 20

#define GAP_BETWEEN_CUISINE_AND_REVIEW 3

#define CELL_RATING_STAR_WIDTH 10

#define RESTAURANT_IMAGE_SIZE 70

@interface HomeRestUpdateTableViewCell(){
    UILabel *headerLabel;
    UILabel *dateLabel;
//    CXAHyperlinkLabel *titleLabel;
    UILabel *titleLabel;
    UILabel *reviewCountLabel;
    UIImageView *restaurantImageView;
    UILabel *cuisineAndDistrictLabel;
    DLStarRatingControl *ratingScoreControl;
    UIImageView *reviewCountImageView;
    UILabel *scoreLabel;
    UILabel *timeLabel;
    IMGRestaurantEvent *restaurantEvent;
//    UILabel *likeLabel;
//    UILabel *commentLabel;
//    UILabel *listLabel;
    CardPhotosView *cardPhotosView;
    
    UIView *separateView;
    UIButton *saveBtn;
    V2_LikeCommentShareView *likeCommentShareView;
    NSMutableArray *restaurantEventArrM;
    UIImageView *line1;
    LikeView* likepopView;
    NSMutableArray* likelistArr;
}

@end

@implementation HomeRestUpdateTableViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
	if(self=[super initWithStyle:style reuseIdentifier:identifier]){
        [self createUI];
	}
	return self;
}
- (void)createUI{
    titleLabel = [UILabel new];
    titleLabel.font = [UIFont systemFontOfSize:14];
    titleLabel.textColor = [UIColor color333333];
    
    
    saveBtn = [UIButton new];
    [saveBtn setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
    [saveBtn addTarget:self action:@selector(saveBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    scoreLabel = [UILabel new];
    scoreLabel.font = [UIFont systemFontOfSize:12.5];
    scoreLabel.textColor = [UIColor colorFFC000];
    
    ratingScoreControl=[[DLStarRatingControl alloc] initWithFrame:CGRectMake(LEFT_X, TITLE_LABEL_Y+TITLE_LABEL_HEIGHT+GAP_BETWEEN_TITLE_AND_CUISINE+CUISINE_DISTRICT_LABEL_HEIGHT+GAP_BETWEEN_CUISINE_AND_REVIEW, 74, 10) andStars:5 andStarWidth:CELL_RATING_STAR_WIDTH isFractional:YES spaceWidth:5];
    ratingScoreControl.userInteractionEnabled=NO;
    
    line1 = [UIImageView new];
    [line1 setImage:[UIImage imageNamed:@"search_line"]];
    
    timeLabel = [UILabel new];
    timeLabel.textColor = [UIColor grayColor];
    timeLabel.alpha = 0.7f;
    timeLabel.font = [UIFont systemFontOfSize:12];
    
    
    cardPhotosView = [[CardPhotosView alloc] init];
    cardPhotosView.homeTableViewCellDelegate = self.homeTableViewCellDelegate;
    cardPhotosView.cardPhotosViewDelegate = self;
    
    UIImageView *bottomLine = [UIImageView new];
    bottomLine.image = [UIImage imageNamed:@"search_line"];
    
    likeCommentShareView = [V2_LikeCommentShareView new];
    likeCommentShareView.delegate = self;
    
    UIButton *btn = [UIButton new];
    [likeCommentShareView addSubview:btn];
    
    separateView = [[UIView alloc] init];
    separateView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    
    [self.contentView sd_addSubviews:@[titleLabel,saveBtn,scoreLabel,ratingScoreControl,line1,timeLabel,cardPhotosView,bottomLine,likeCommentShareView,separateView]];
    
    titleLabel.sd_layout
    .topSpaceToView(self.contentView, TITLE_LABEL_Y)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 3*LEFTLEFTSET)
    .autoHeightRatio(0);
    
    [titleLabel setMaxNumberOfLinesToShow:2];
    
    saveBtn.sd_layout
    .topSpaceToView(self.contentView, 13)
    .rightSpaceToView(self.contentView, 15)
    .widthIs(40)
    .heightIs(34);
    
    scoreLabel.sd_layout
    .leftSpaceToView(self.contentView, 15)
    .topSpaceToView(titleLabel, 5)
    .heightIs(14);
    
    [scoreLabel setSingleLineAutoResizeWithMaxWidth:DeviceWidth];
    
    ratingScoreControl.sd_layout
    .topSpaceToView(titleLabel, 7)
    .leftSpaceToView(scoreLabel, 5)
    .widthIs(74)
    .heightIs(10);
    
    line1.sd_layout
    .leftSpaceToView(self.contentView, 15)
    .topSpaceToView(scoreLabel, 5)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(1);
    
    timeLabel.sd_layout
    .leftSpaceToView(self.contentView, 15)
    .topSpaceToView(line1, 13)
    .heightIs(14);

    [timeLabel setSingleLineAutoResizeWithMaxWidth:DeviceWidth-30];
    
    cardPhotosView.sd_layout
    .topSpaceToView(timeLabel, 13)
    .leftSpaceToView(self.contentView, 0)
    .rightSpaceToView(self.contentView, 0)
    .autoHeightRatio(0);
    
    bottomLine.sd_layout
    .topSpaceToView(cardPhotosView, 15)
    .leftSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(1);
    
    likeCommentShareView.sd_layout
    .topSpaceToView(bottomLine, 0)
    .leftSpaceToView(self.contentView, 0)
    .rightSpaceToView(self.contentView, 0)
    .autoHeightRatio(0);
    
    btn.sd_layout
    .topSpaceToView(likeCommentShareView,0)
    .leftSpaceToView(likeCommentShareView,DeviceWidth/3 - 60)
    .widthIs(60)
    .heightIs(50);
    [btn addTarget:self action:@selector(likeCountClick:) forControlEvents:UIControlEventTouchUpInside];
    
    separateView.sd_layout
    .topSpaceToView(likeCommentShareView,0)
    .leftSpaceToView(self.contentView,0)
    .rightSpaceToView(self.contentView,0)
    .heightIs(20);
 
}


-(void)setUpdateCard:(IMGUpdateCard*)updateCard{
    IMGRestaurant *restaurant = updateCard.restaurant;
    NSMutableArray *restaurantEventList =[NSMutableArray arrayWithArray:updateCard.eventArr];
    
    restaurantEventArrM = [NSMutableArray arrayWithArray:restaurantEventList];
    
    _restaurant=restaurant;
    
    
    if (restaurantEventArrM.count>1) {
        titleLabel.text = [NSString stringWithFormat:@"%@ uploaded %ld photos",restaurant.title,restaurantEventArrM.count];
    }else{
        titleLabel.text = [NSString stringWithFormat:@"%@ uploaded a photo",restaurant.title];
    }

    [titleLabel updateLayout];
    
    if ([_restaurant.saved boolValue])
    {
        [saveBtn setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
    }
    else
        [saveBtn setImage:[UIImage imageNamed:@"ic_heart_home"] forState:UIControlStateNormal];
    
    
    float score = [restaurant.ratingScore floatValue];
    [scoreLabel setText:[NSString stringWithFormat:@"%.1f",score/2]];
    
    [ratingScoreControl updateRating:[NSNumber numberWithFloat:[restaurant.ratingScore floatValue]]];

    
    [timeLabel setText:[Date getTimeInteval_v4:[restaurant.timeline longLongValue]/1000]];

    restaurantEvent = [restaurantEventArrM firstObject];
    
    
    cardPhotosView.isFromPromo = YES;

    if (![restaurantEvent.imageHeight floatValue])
    {
        restaurantEvent.imageHeight = [NSNumber numberWithFloat:ONE_PHOTO_HEIGHT];
    }
    if (restaurantEventArrM.count == 1)
    {

        cardPhotosView.cardPhotoHeight = [restaurantEvent.imageHeight floatValue];
    }

    [cardPhotosView setPhotos:restaurantEventArrM];
    cardPhotosView.homeTableViewCellDelegate = self.homeTableViewCellDelegate;
    cardPhotosView.cardPhotosViewDelegate = self;
    
    if (restaurantEventArrM.count>1) {
        [likeCommentShareView setCommentCount:[restaurant.commentCardCount intValue] likeCount:[restaurant.likeCardCount intValue] liked:restaurant.isLikeCard];
    }else{
        [likeCommentShareView setCommentCount:[restaurantEvent.commentCount intValue] likeCount:[restaurantEvent.likeCount intValue] liked:restaurantEvent.isLike];
    }
    
    [self setupAutoHeightWithBottomView:separateView bottomMargin:0];
    


}

-(void)likeCountClick:(UIButton*)btn
{
    [[Amplitude instance] logEvent:@" RC - View People Who Likes"];
    [self restaurantEventPhotoCardAmplitude];
    if (likepopView) {
        [likepopView removeFromSuperview];
    }
    
    [[LoadingView sharedLoadingView]startLoading];
    if (restaurantEventArrM.count>1) {
        [HomeUtil getLikeListMuchPhotoFromServiceWithHomeTimelineRestaurantUpdateId:_restaurant.homeTimeLineId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray, NSNumber *likeviewcount) {
            
            likelistArr=[NSMutableArray arrayWithArray:likeListArray];
            
            likepopView.likecount=likeviewcount;
            likepopView.likelistArray=likelistArr;
            [[LoadingView sharedLoadingView]stopLoading];
            
        }];
    }else{
        [HomeUtil getLikeListFromServiceWithResturantId:restaurantEvent.eventId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
            likelistArr=[NSMutableArray arrayWithArray:likeListArray];
            likepopView.likecount=likeviewcount;
            likepopView.likelistArray=likelistArr;
            [[LoadingView sharedLoadingView]stopLoading];
        }];
        
    }
    likepopView=[[LikeView alloc]initWithFrame:CGRectMake(0, -20, DeviceWidth, DeviceHeight)];
    likepopView.delegate=self;
    UIResponder *responder = [self nextResponder];
    while (responder){
        if ([responder isKindOfClass:[UINavigationController class]]){
            UINavigationController* vc=(UINavigationController*)responder;
            
            [[vc.viewControllers lastObject].view addSubview:likepopView];
            likepopView.layer.zPosition=1000;
            return;
        }
        
        responder = [responder nextResponder];
        
    }
    
}
-(void)LikeviewLoadMoreData:(NSInteger)offset
{
    
    if (restaurantEventArrM.count>1) {
        
        [HomeUtil getLikeListMuchPhotoFromServiceWithHomeTimelineRestaurantUpdateId:_restaurant.homeTimeLineId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset] andBlock:^(NSArray *likeListArray, NSNumber *likeviewcount) {
            likepopView.likecount=likeviewcount;
            [likelistArr addObjectsFromArray:likeListArray];
            [likepopView.likeUserTableView reloadData];
            
        }];
    }else{
        [HomeUtil getLikeListFromServiceWithResturantId:restaurantEvent.eventId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
            likepopView.likecount=likeviewcount;
            
            [likelistArr addObjectsFromArray:likeListArray];
            [likepopView.likeUserTableView reloadData];
        }];
    }
}
-(void)returnLikeCount:(NSInteger)count
{
    //    if (count!=0) {
    //
    //
    //    float start = LEFTLEFTSET;
    //    float top = cardPhotosView.endPointY + 10;
    //    NSString *likeString=[NSString stringWithFormat:@"%ld %@",(long)count,count>1?L(@"Likes"):L(@"Like")];
    ////    [likeLabel setText:likeString];
    ////    [likeLabel setFrame:CGRectMake(start, top, likeLabel.expectedWidth,20)];
    //    }
}



- (void)gotoCardDetailPage
{
    [self restaurantEventPhotoCardAmplitude];
    NSMutableDictionary *eventDic=[[NSMutableDictionary alloc]init];
    [eventDic setValue:@"Homepage" forKey:@"Origin"];
    [eventDic setValue:restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
    [eventDic setValue:restaurantEvent.restaurantId forKey:@"Restaurant_ID"];
    NSString *photoSource;
    if ([@"User ID" isEqualToString:restaurantEvent.photoCreditTypeDic]) {
        photoSource = @"User";
    }else if([@"Qraved Team" isEqualToString:restaurantEvent.photoCreditTypeDic]){
        photoSource = @"Qraved Admin";
    }
    [eventDic setValue:photoSource forKey:@"Photo Source"];
    [eventDic setValue:restaurantEvent.userIdDic forKey:@"UploaderUser_ID"];
    
    [[Amplitude instance] logEvent:@"RC - View Event Card detail" withEventProperties:eventDic];
    
    if (restaurantEventArrM.count>1) {
        [self.restUpdateDelegate homeRestUpdateTableViewCellImageClick:_restaurant andDishArr:restaurantEventArrM andIndex:0 andFromCellIndexPath:self.fromCellIndexPath];
        return;
    }
    if (self.homeTableViewCellDelegate)
    {
        NSDictionary *dataDic = [[NSDictionary alloc] initWithObjectsAndKeys:_restaurant,@"restaurant",restaurantEventArrM,@"dataArr", nil];
        [self.homeTableViewCellDelegate goToCardDetailPageWithType:10 andData:dataDic andCellIndexPath:self.fromCellIndexPath];
        
    }
}


- (void)saveBtnClick:(UIButton *)btn
{
    [self restaurantEventPhotoCardAmplitude];
    if (self.homeTableViewCellDelegate) {
        [self.homeTableViewCellDelegate homeCardTableViewReloadCell:self.fromCellIndexPath entity:_restaurant];
    }

}


- (void)likeCommentShareView:(UIView*)likeCommentShareView_ viewPreviousCommentsTapped:(UIButton*)button lastComment:(id)comment{
    
    
    [[Amplitude instance] logEvent:@"CL - View Previous Comments CTA" withEventProperties:@{@"Location":@"Review card on homepage"}];
    if (restaurantEventArrM.count>1) {
        [self.restUpdateDelegate homeRestUpdateTableViewCellImageClick:_restaurant andDishArr:restaurantEventArrM andIndex:0 andFromCellIndexPath:self.fromCellIndexPath];
        
        NSMutableDictionary *eventDic=[[NSMutableDictionary alloc]init];
        [eventDic setValue:@"Homepage" forKey:@"Origin"];
        [eventDic setValue:restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
        [eventDic setValue:restaurantEvent.restaurantId forKey:@"Restaurant_ID"];
        NSString *photoSource;
        if ([@"User ID" isEqualToString:restaurantEvent.photoCreditTypeDic]) {
            photoSource = @"User";
        }else if([@"Qraved Team" isEqualToString:restaurantEvent.photoCreditTypeDic]){
            photoSource = @"Qraved Admin";
        }
        [eventDic setValue:photoSource forKey:@"Photo Source"];
        [eventDic setValue:restaurantEvent.userIdDic forKey:@"UploaderUser_ID"];
        
        [[Amplitude instance] logEvent:@"RC - View Event Card detail" withEventProperties:eventDic];

        return;
    }
    //goto CDP screen
    [self gotoCardDetailPage];

}

-(void)likeCommentShareView:(UIView *)likeCommentShareView_ likeButtonTapped:(UIButton *)button{
    [self restaurantEventPhotoCardAmplitude];
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }

//    [[LoadingView sharedLoadingView] startLoading];
    
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    //RestaurantEvent_ID, Restaurant_ID, Location
    [eventProperties setValue:restaurantEvent.homeTimeLineId forKey:@"MultiPhoto_ID"];
    [eventProperties setValue:restaurantEvent.restaurantId forKey:@"UploaderUser_ID"];
    [eventProperties setValue:restaurantEvent.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:@"Restaurant event card on homepage" forKey:@"Location"];
    if (restaurantEventArrM.count>1) {

        
        [RestaurantHandler likeRestaurantEventCard:_restaurant.isLikeCard homeTimelineRestaurantUpdateId:_restaurant.homeTimeLineId andBlock:^{
            if (!_restaurant.isLikeCard)
            {
                
                
                [[Amplitude instance] logEvent:@"UC - Unlike Multi Photo Succeed" withEventProperties:eventProperties];
            }else
            {
                [[Amplitude instance] logEvent:@"UC - Like multi photo Succeed" withEventProperties:eventProperties];
            }
            
        }failure:^(NSString *exceptionMsg){
            if([exceptionMsg isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
                [[AppDelegate ShareApp] goToLoginController];
            }
        }];
        _restaurant.isLikeCard=!_restaurant.isLikeCard;
        if(_restaurant.isLikeCard){
            _restaurant.likeCardCount=[NSNumber numberWithInt:[_restaurant.likeCardCount intValue]+1];
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:restaurantEvent.eventId forKey:@"EventID"];
            [eventProperties setValue:restaurantEvent.restaurantTitle forKey:@"RestaurantTite"];
            [eventProperties setValue:@"Restaurant event card on homepage" forKey:@"Location"];
            [[Amplitude instance] logEvent:@"UC - Like multi photo Initiate" withEventProperties:eventProperties];

        }else{
            _restaurant.likeCardCount=[NSNumber numberWithInt:[_restaurant.likeCardCount intValue]-1];
//            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
//            [eventProperties setValue:restaurantEvent.eventId forKey:@"EventID"];
//            [eventProperties setValue:restaurantEvent.restaurantTitle forKey:@"RestaurantTite"];
//            [eventProperties setValue:@"Restaurant event card on homepage,Homepage" forKey:@"Location"];
            [[Amplitude instance] logEvent:@"UC - Unlike Multi Photo Initiate" withEventProperties:eventProperties];

        }
        

    }else{
        [RestaurantHandler likeRestaurantEvent:restaurantEvent.isLike restaurantEventId:restaurantEvent.eventId andBlock:^{
            if(self.restUpdateDelegate){
                
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
                [eventProperties setValue:restaurantEvent.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:@"Restaurant event card on homepage" forKey:@"Location"];
                
                if (!restaurantEvent.isLike) {
                 [[Amplitude instance] logEvent:@"UC - UnLike Restaurant Event Succeed" withEventProperties:eventProperties];
                }else{
                [[Amplitude instance] logEvent:@"UC - Like Restaurant Event Succeed" withEventProperties:eventProperties];
                }
            }
        }failure:^(NSString *exceptionMsg){
            if([exceptionMsg isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
                [[AppDelegate ShareApp] goToLoginController];
            }
        }];
        restaurantEvent.isLike=!restaurantEvent.isLike;
        if(restaurantEvent.isLike){
            restaurantEvent.likeCount=[NSNumber numberWithInt:[restaurantEvent.likeCount intValue]+1];
            
            [[Amplitude instance] logEvent:@"UC - Like Restaurant Event Initiate" withEventProperties:eventProperties];
        }else{
            restaurantEvent.likeCount=[NSNumber numberWithInt:[restaurantEvent.likeCount intValue]-1];
            [[Amplitude instance] logEvent:@"UC - UnLike Restaurant Event Initiate" withEventProperties:eventProperties];
        }

    }
        if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self likeButtonTapped:button entity:restaurantEvent];
    }
    

}
-(void)likeCommentShareView:(UIView *)likeCommentShareView shareButtonTapped:(UIButton *)button{
    [self restaurantEventPhotoCardAmplitude];
    if (restaurantEventArrM.count>1) {
        [self.homeTableViewCellDelegate homeCardTableViewCell:self shareMutiPhotosCardButtonTapped:button entity:_restaurant];
    }
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self shareButtonTapped:button entity:restaurantEvent];
    }
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView commentButtonTapped:(UIButton *)button{
    //home card cell都留空
    NSDictionary *dataDic = [[NSDictionary alloc] initWithObjectsAndKeys:_restaurant,@"restaurant",restaurantEventArrM,@"dataArr", nil];
    [self.homeTableViewCellDelegate goToCardDetailPageWithType:10 andData:dataDic andCellIndexPath:self.fromCellIndexPath];
    
}


- (void)likeCommentShareView:(UIView*)likeCommentShareView_ commentInputView:(UIView *)commentInputView inputViewExpand:(UITextView *)inputTextView commentInputViewYOffset:(CGFloat)commentInputViewYOffset{
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self likeCommentShareView:likeCommentShareView_ commentInputView:commentInputView inputViewExpand:inputTextView commentInputViewYOffset:commentInputViewYOffset];
    }
}

- (void)likeCommentShareView:(UIView*)likeCommentShareView_ commentInputView:(UIView *)commentInputView postButtonTapped:(UIButton *)postButton commentInputText:(NSString *)text{
    
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    postButton.enabled = NO;
    if (restaurantEventArrM.count>1) {
        if (self.homeTableViewCellDelegate) {
            
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
            [eventProperties setValue:restaurantEvent.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:@"Restaurant event card on homepage" forKey:@"Location"];
            
            [RestaurantHandler postCardComment:text restaurantId:_restaurant.restaurantId homeTimeLineId:_restaurant.homeTimeLineId andBlock:^(IMGRestaurantEventComment *eventComment) {
                postButton.enabled = YES;
                [eventProperties setValue:eventComment.commentId forKey:@"Comment_ID"];
                [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Succeed" withEventProperties:eventProperties];
                [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Restaurant Event Succeed" withValues:eventProperties];
                _restaurant.commentCardCount=[NSNumber numberWithInt:[ _restaurant.commentCardCount intValue]+1];
                if (_restaurant.commentCardList){
                    if([_restaurant.commentCardList isKindOfClass:[NSArray class]]){
                        _restaurant.commentCardList = [NSMutableArray arrayWithArray:_restaurant.commentCardList];
                    }
                    [_restaurant.commentCardList insertObject:eventComment atIndex:0];
                }else{
                    _restaurant.commentCardList = [[NSMutableArray alloc] initWithObjects:eventComment, nil];
                }
                [self.homeTableViewCellDelegate homeCardTableViewCell:self newCommentPosted:eventComment];

                
            } failure:^(NSString *errorReason){
                if([errorReason isLogout]){
                    [[LoadingView sharedLoadingView] stopLoading];
                    [[IMGUser currentUser]logOut];
                    [[AppDelegate ShareApp] goToLoginController];
                }
                [eventProperties setValue:errorReason forKey:@"Reason"];
                [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Failed" withEventProperties:eventProperties];
            }];
            [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Initiate" withEventProperties:eventProperties];
            

        }
    }
    else{
        if(self.homeTableViewCellDelegate){
        
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
        [eventProperties setValue:restaurantEvent.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:@"Restaurant event card on homepage" forKey:@"Location"];
            
        [RestaurantHandler postComment:text restaurantEventId:restaurantEvent.eventId andBlock:^(IMGRestaurantEventComment *eventComment) {
            postButton.enabled = YES;
            [eventProperties setValue:eventComment.commentId forKey:@"Comment_ID"];
            [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Succeed" withEventProperties:eventProperties];
            [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Restaurant Event Succeed" withValues:eventProperties];
            restaurantEvent.commentCount=[NSNumber numberWithInt:[restaurantEvent.commentCount intValue]+1];
            if (restaurantEvent.commentList){
                if([restaurantEvent.commentList isKindOfClass:[NSArray class]]){
                    restaurantEvent.commentList = [NSMutableArray arrayWithArray:restaurantEvent.commentList];
                }
                [restaurantEvent.commentList insertObject:eventComment atIndex:0];
            }else{
                restaurantEvent.commentList = [[NSMutableArray alloc] initWithObjects:eventComment, nil];
            }
                        [self.homeTableViewCellDelegate homeCardTableViewCell:self newCommentPosted:eventComment];
            
        } failure:^(NSString *errorReason){
            [eventProperties setValue:errorReason forKey:@"Reason"];
            [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Failed" withEventProperties:eventProperties];
        }];

            [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Initiate" withEventProperties:eventProperties];


        }
    }
    
}

- (void)likeCommentShareView:(UIView*)likeCommentShareView_ commentInputView:(UIView *)view inputViewDidBeginEditing:(UITextView *)textView{
    if(self.homeTableViewCellDelegate){
        [self restaurantEventPhotoCardAmplitude];
        [self.homeTableViewCellDelegate homeCardTableViewCell:self likeCommentShareView:likeCommentShareView_ commentInputView:view inputViewDidBeginEditing:textView];
    }
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView commentCell:(UITableViewCell *)cell readMoreTapped:(id)comment offset:(CGFloat)offset{
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self commentCell:cell readMoreTapped:comment offset:offset];
    }
}

#pragma mark - privates
- (NSAttributedString *)attributedString:(NSArray *__autoreleasing *)outURLs
                               URLRanges:(NSArray *__autoreleasing *)outURLRanges andText:(NSString *)text
{
    NSString *HTMLText = text;
    NSArray *URLs;
    NSArray *URLRanges;
    UIColor *color = [UIColor grayColor];
    UIFont *font = [UIFont systemFontOfSize:TITLE_LABEL_FONT];
    NSMutableParagraphStyle *mps = [[NSMutableParagraphStyle alloc] init];
//    mps.lineSpacing = ceilf(font.pointSize * .5);
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor whiteColor];
    shadow.shadowOffset = CGSizeMake(0, 1);
    NSString *str = [NSString stringWithHTMLText:HTMLText baseURL:nil URLs:&URLs URLRanges:&URLRanges];
    NSMutableAttributedString *mas = [[NSMutableAttributedString alloc] initWithString:str attributes:@
                                      {
                                          NSForegroundColorAttributeName : color,
                                          NSFontAttributeName            : font,
                                          NSParagraphStyleAttributeName  : mps,
                                          NSShadowAttributeName          : shadow,
                                      }];
    [URLRanges enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop){
        [mas addAttributes:@
         {
             NSForegroundColorAttributeName : [UIColor color222222],
             //       NSUnderlineStyleAttributeName  : @(NSUnderlineStyleSingle)
             NSForegroundColorAttributeName : [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:TITLE_LABEL_FONT]
             
         } range:[obj rangeValue]];
    }];
    
    *outURLs = URLs;
    *outURLRanges = URLRanges;
    
    return [mas copy];
}

-(void)cardPhotosViewImageClick:(int)imageIndex{
    [self.restUpdateDelegate homeRestUpdateTableViewCellImageClick:_restaurant andDishArr:restaurantEventArrM andIndex:imageIndex andFromCellIndexPath:self.fromCellIndexPath];
    
    NSMutableDictionary *eventDic=[[NSMutableDictionary alloc]init];
    [eventDic setValue:@"Homepage" forKey:@"Origin"];
    [eventDic setValue:restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
    [eventDic setValue:restaurantEvent.restaurantId forKey:@"Restaurant_ID"];
    NSString *photoSource;
    if ([@"User ID" isEqualToString:restaurantEvent.photoCreditTypeDic]) {
        photoSource = @"User";
    }else if([@"Qraved Team" isEqualToString:restaurantEvent.photoCreditTypeDic]){
        photoSource = @"Qraved Admin";
    }
    [eventDic setValue:photoSource forKey:@"Photo Source"];
    [eventDic setValue:restaurantEvent.userIdDic forKey:@"UploaderUser_ID"];
    
    [[Amplitude instance] logEvent:@"RC - View Event Card detail" withEventProperties:eventDic];

}
- (void)cardPhotosChangeHeight:(CGFloat)height
{
    if ([restaurantEvent.imageHeight floatValue] != ONE_PHOTO_HEIGHT)
    {
        return;
    }
    if (height <= ONE_PHOTO_HEIGHT)
    {
        return;
    }

    restaurantEvent.imageHeight = [NSNumber numberWithFloat:height];
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewReloadCell:self];
    }
}
- (void)attributedLabel:(TYAttributedLabel *)attributedLabel textStorageClicked:(id<TYTextStorageProtocol>)TextRun atPoint:(CGPoint)point{
    
    
    if ([TextRun isKindOfClass:[TYLinkTextStorage class]]) {
        [self restaurantEventPhotoCardAmplitude];
        
        [self.homeTableViewCellDelegate gotoRestaurantWithRestaurantId:_restaurant.restaurantId];
        
    }
    
}
-(void)restaurantEventPhotoCardAmplitude{
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
    [eventProperties setValue:_restaurant.restaurantId forKey:@"Restaurant_ID"];
    [eventProperties setValue:@"Homepage" forKey:@"Location"];
    [[Amplitude instance] logEvent:@"CL - Restaurant Event Photo Card" withEventProperties:eventProperties];
}

@end

