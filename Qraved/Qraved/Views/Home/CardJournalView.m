//
//  CardJournalView.m
//  Qraved
//
//  Created by System Administrator on 3/4/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "CardJournalView.h"
#import "HomeJournalTableViewCell.h"

@class IMGMediaComment;

@interface CardJournalView(){

}

@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) IMGMediaComment *journal;

@end

@implementation CardJournalView{

}

@synthesize tableView=_tableView,journal=_journal,title,date;

-(instancetype)initWithFrame:(CGRect)frame journal:(IMGMediaComment*)journal{
	if(self=[super initWithFrame:frame]){
		_tableView=[[UITableView alloc] initWithFrame:frame];
		_tableView.delegate=self;
		_tableView.dataSource=self;
		_journal=journal;
		[self addSubview:_tableView];
	}
	return self;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
	return 1;
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section{
	return 1;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexpath{
	if(indexpath.section==0){
		HomeJournalTableViewCell *cell=[[HomeJournalTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"trending_info"];
		cell.homeTableViewCellDelegate=self.delegate;
        cell.title=self.title;
        cell.date=self.date;
        [cell setJournal:_journal];
        return cell;
	}
    return nil;
}


@end
