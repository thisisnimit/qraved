//
//  HomeUploadPhotoTableViewCell.h
//  Qraved
//
//  Created by lucky on 16/3/7.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeTableViewCell.h"
#import "IMGUser.h"
#import "UILikeCommentShareView.h"
#import "IMGDish.h"
#import "CardPhotosView.h"
#import "LikeView.h"
#import "TYAttributedLabel.h"
#import "IMGUploadPhotoCard.h"
#import "V2_LikeCommentShareView.h"
@class CardPhotosView;

@protocol HomeUploadPhotoTableViewCellDelegate <NSObject>
@optional
- (void)writeReviewWithRestaurant:(IMGRestaurant *)restaurant cellIndexPath:(NSIndexPath*)cellIndexPath;
- (void)homeUploadPhotoTableViewCellImageClick:(IMGRestaurant *)restaurant andUser:(IMGUser *)user andDishArr:(NSArray *)dishArr andIndex:(int)index;
-(void)homeUploadPhotoTableViewCellImageClick:(IMGRestaurant *)restaurant andUser:(IMGUser *)user andDishArr:(NSArray *)dishArr andIndex:(int)index andFromCellIndexPath:(NSIndexPath*)fromCellIndexPath;

@end


@interface HomeUploadPhotoTableViewCell : HomeTableViewCell<CardPhotosViewDelegate,LikeListViewDelegate,TYAttributedLabelDelegate,V2_LikeCommentShareViewDelegete>

@property(nonatomic,strong) IMGRestaurant *restaurant;
@property(nonatomic,weak) id<HomeUploadPhotoTableViewCellDelegate> homeUploadPhotoTableViewCellDelegate;
@property(nonatomic,weak) id<HomeTableViewCellDelegate> delegate;
@property(nonatomic,strong) IMGUploadPhotoCard *uploadCard;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier;


@end
