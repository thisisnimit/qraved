//
//  homeNewFeatuerView.m
//  Qraved
//
//  Created by Adam.zhang on 2017/9/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "homeNewFeatuerView.h"
#import <SDWebImage/UIImage+GIF.h>
@implementation homeNewFeatuerView

- (instancetype)initWithFrame:(CGRect)frame{
    if ([super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}
-(void)initUI{
    
    self.backgroundColor = [UIColor clearColor];
    
    UIImageView *gifImageview = [UIImageView new];
    gifImageview.backgroundColor = [UIColor clearColor];
    gifImageview.image = [UIImage sd_animatedGIFNamed:@"home_loader"];
    [self addSubview:gifImageview];
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.text = @"Hold on, while we find places you'll love";
    lblTitle.font = [UIFont systemFontOfSize:14];
    lblTitle.textColor = [UIColor whiteColor];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    [self addSubview:lblTitle];
    
    gifImageview.sd_layout
    .bottomSpaceToView(self, 0)
    .topSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .rightSpaceToView(self, 0);
    
    lblTitle.sd_layout
    .centerYIs(DeviceHEIGHT/2 +60)
    .centerXEqualToView(self)
    .widthIs(DeviceWidth)
    .autoHeightRatio(0);

}
@end
