
//
//  HomeJournalTableViewCell.m
//  Qraved
//
//  Created by System Administrator on 2/24/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "HomeJournalTableViewCell.h"
#import "IMGMediaComment.h"
#import "IMGUser.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "UIImage+Helper.h"
#import "UIConstants.h"
#import "AppDelegate.h"
#import "OfferLabel.h"
#import "JournalHandler.h"
#import "UIImageView+WebCache.h"
#import "JournalActivityItemProvider.h"
#import "UILikeCommentShareView.h"
#import "LoadingView.h"
#import "UICommentInputView.h"
#import "LikeView.h"
#import "HomeUtil.h"
#import "UIImage+Resize.h"
#import "Date.h"
#import "V2_LikeCommentShareView.h"

#define RESTAURANT_IMAGE_HEIGHT 160

@interface HomeJournalTableViewCell ()<V2_LikeCommentShareViewDelegete>

@end

@implementation HomeJournalTableViewCell{
	UILabel *headerLabel;
	UILabel *dateLabel;
	UILabel *titleLabel;
	UILabel *categoryLabel;
	UIImageView *restaurantImageView;
    UIImageView *lineImageView;
    UILabel *likeLabel;
    UILabel *commentLabel;
    UILabel *listLabel;
    UIView *separateView;
    IMGUser *user;
    V2_LikeCommentShareView *likeCommentShareView;
    
    UIView *photoAndTitleView;
    NSMutableArray* likeListArr;
    LikeView* likepopView;
    UILabel *lblDate;
    UILabel *lblViews;
    UIImageView *viewsImageView;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
	if(self=[super initWithStyle:style reuseIdentifier:identifier]){
        
        [self createUI];

	}
	return self;
}

- (void)createUI{
    
    headerLabel = [UILabel new];
    headerLabel.font = [UIFont systemFontOfSize:14];
    headerLabel.textColor = [UIColor color333333];
    
    dateLabel = [UILabel new];
    dateLabel.font = [UIFont systemFontOfSize:12];
    dateLabel.textColor = [UIColor color999999];
    dateLabel.textAlignment = NSTextAlignmentRight;
    
    UIImageView *sepImageView = [UIImageView new];
    sepImageView.image = [UIImage imageNamed:@"search_line"];
    
    photoAndTitleView = [UIView new];
    photoAndTitleView.layer.masksToBounds = YES;
    photoAndTitleView.layer.cornerRadius = 3;
    photoAndTitleView.layer.borderWidth = 1;
    photoAndTitleView.layer.borderColor = [UIColor colorWithHexString:@"E6E6E6"].CGColor;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photoAndTitleViewClick)];
    [photoAndTitleView addGestureRecognizer:tap];
    photoAndTitleView.userInteractionEnabled = YES;
    
    
    UIImageView *bottomLine = [UIImageView new];
    bottomLine.image = [UIImage imageNamed:@"search_line"];
    
    likeCommentShareView = [V2_LikeCommentShareView new];
    likeCommentShareView.delegate = self;
    
    UIButton *btn = [UIButton new];
    [likeCommentShareView addSubview:btn];
    
    separateView = [UIView new];
    separateView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    
    [self.contentView sd_addSubviews:@[headerLabel,dateLabel,sepImageView,photoAndTitleView,bottomLine,likeCommentShareView,separateView]];
    
    restaurantImageView = [UIImageView new];
    
    titleLabel = [UILabel new];
    titleLabel.font = [UIFont systemFontOfSize:14];
    titleLabel.textColor = [UIColor color333333];
    
    lblDate = [UILabel new];
    lblDate.font = [UIFont systemFontOfSize:12];
    lblDate.textColor = [UIColor color999999];
    
    viewsImageView = [UIImageView new];
    viewsImageView.image = [UIImage imageNamed:@"ic_eye_small"];
    viewsImageView.hidden = YES;
    
    lblViews = [UILabel new];
    lblViews.font = [UIFont systemFontOfSize:12];
    lblViews.textColor = [UIColor color999999];

    
    [photoAndTitleView sd_addSubviews:@[restaurantImageView,titleLabel,lblDate,viewsImageView,lblViews]];
    
    headerLabel.sd_layout
    .leftSpaceToView(self.contentView,15)
    .topSpaceToView(self.contentView,15)
    .widthIs(200)
    .heightIs(16);
    
    dateLabel.sd_layout
    .rightSpaceToView(self.contentView,15)
    .topSpaceToView(self.contentView,15)
    .leftSpaceToView(headerLabel,10)
    .heightIs(16);
    
    sepImageView.sd_layout
    .leftSpaceToView(self.contentView,15)
    .rightSpaceToView(self.contentView,15)
    .topSpaceToView(headerLabel,15)
    .heightIs(1);
    
    restaurantImageView.sd_layout
    .topSpaceToView(photoAndTitleView,0)
    .leftSpaceToView(photoAndTitleView,0)
    .rightSpaceToView(photoAndTitleView,0)
    .heightIs((DeviceWidth-24)*.5);
    
    titleLabel.sd_layout
    .topSpaceToView(restaurantImageView,10)
    .leftSpaceToView(photoAndTitleView,15)
    .rightSpaceToView(photoAndTitleView,15)
    .autoHeightRatio(0);
    
    [titleLabel setMaxNumberOfLinesToShow:2];
    
    lblDate.sd_layout
    .topSpaceToView(titleLabel,5)
    .leftEqualToView(titleLabel)
    .heightIs(14);
    
    [lblDate setSingleLineAutoResizeWithMaxWidth:DeviceWidth-30];
    
    viewsImageView.sd_layout
    .topSpaceToView(titleLabel,8)
    .leftSpaceToView(lblDate,5)
    .widthIs(12)
    .heightIs(8);
    
    lblViews.sd_layout
    .topEqualToView(lblDate)
    .leftSpaceToView(viewsImageView,5)
    .rightSpaceToView(photoAndTitleView,15)
    .heightIs(14);
    
    photoAndTitleView.sd_layout
    .topSpaceToView(sepImageView,10)
    .leftSpaceToView(self.contentView,12)
    .rightSpaceToView(self.contentView,12)
    .autoHeightRatio(0);
    
    [photoAndTitleView setupAutoHeightWithBottomView:lblViews bottomMargin:20];
    
    bottomLine.sd_layout
    .topSpaceToView(photoAndTitleView,20)
    .leftSpaceToView(self.contentView,15)
    .rightSpaceToView(self.contentView,15)
    .heightIs(1);
    
    likeCommentShareView.sd_layout
    .topSpaceToView(bottomLine,0)
    .leftSpaceToView(self.contentView,0)
    .rightSpaceToView(self.contentView,0)
    .autoHeightRatio(0);
    
    btn.sd_layout
    .topSpaceToView(likeCommentShareView,0)
    .leftSpaceToView(likeCommentShareView,DeviceWidth/3 - 60)
    .widthIs(60)
    .heightIs(50);
    [btn addTarget:self action:@selector(likeCountClick:) forControlEvents:UIControlEventTouchUpInside];
    
    separateView.sd_layout
    .topSpaceToView(likeCommentShareView,0)
    .leftSpaceToView(self.contentView,0)
    .rightSpaceToView(self.contentView,0)
    .heightIs(20);
}
- (void)likeCountClick:(UIButton *)btn{
    [[Amplitude instance] logEvent:@" RC - View People Who Likes"];
    if (likepopView) {
        [likepopView removeFromSuperview];
    }
    
    [[LoadingView sharedLoadingView]startLoading];
    
    [HomeUtil getLikeListFromServiceWithJournalArticleId:_journal andMax:[NSNumber numberWithInt:10] andoffset:[NSNumber numberWithInt:0] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
        [[LoadingView sharedLoadingView]stopLoading];
        
        likeListArr=[[NSMutableArray alloc]initWithArray:likeListArray];
        
        likepopView.likecount=likeviewcount;
        
        likepopView.likelistArray=likeListArr;
        
    }];
    likepopView=[[LikeView alloc]initWithFrame:CGRectMake(0, -20, DeviceWidth, DeviceHeight)];
    likepopView.delegate=self;
    UIResponder *responder = [self nextResponder];
    while (responder){
        
        if ([responder isKindOfClass:[UINavigationController class]]){
            UINavigationController* vc=(UINavigationController*)responder;
            [[vc.viewControllers lastObject].view addSubview:likepopView];
            likepopView.layer.zPosition=1000;
            return;
        }
        responder = [responder nextResponder];
    }

}

-(void)LikeviewLoadMoreData:(NSInteger)offset
{
    

    [HomeUtil getLikeListFromServiceWithJournalArticleId:_journal andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
         likepopView.likecount=likeviewcount;
        [likeListArr addObjectsFromArray:likeListArray];
        [likepopView.likeUserTableView reloadData];
  
    }];
 
}
-(void)returnLikeCount:(NSInteger)count
{

}


-(void)setJournal:(IMGMediaComment *)journal {
    _journal=journal;

    if (journal.typeName == nil) {
        headerLabel.text = @"Qraved Journal";
    }else{
        headerLabel.text = journal.typeName;
    }
    
    if (journal.createdDate == nil) {
        dateLabel.text = @"";
    }else{
        dateLabel.text = [Date getTimeInteval_v4:[journal.createdDate longLongValue]/1000];
    }
    
    [likeCommentShareView setCommentCount:[journal.commentCount intValue] likeCount:[journal.likeCount intValue] liked:journal.isLike];
    if (journal.viewCount == nil) {
        viewsImageView.hidden = YES;
        lblViews.text = @"";
    }else{
        viewsImageView.hidden = NO;
        lblViews.text = [NSString stringWithFormat:@"%@ Views",journal.viewCount];
    }
    
    
    lblDate.text = [Date getTimeInteval_v4:[journal.postTime longLongValue]/1000];;
    
    
    
    NSString *urlString = [_journal.journalImageUrl returnFullImageUrlWithWidth:DeviceWidth-LEFTLEFTSET*2];
    
    __weak typeof(restaurantImageView) weakRestaurantImageView = restaurantImageView;
    UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 160)];
    
    UIImage* cacheImg = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:urlString];
    if (cacheImg==nil) {
        [restaurantImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:placeHoderImage options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [[SDImageCache sharedImageCache] clearMemory];
             [[SDImageCache sharedImageCache] setValue:nil forKey:@"memCache"];
            image = [image imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 160)];
            [weakRestaurantImageView setImage:image];
        }];
    }else{
        restaurantImageView.image = [cacheImg imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 160)];
    }

    

    titleLabel.text=[_journal.journalTitle filterHtml];


    categoryLabel.text=_journal.categoryName;
    [self setupAutoHeightWithBottomView:separateView bottomMargin:0];
    
}
- (void)gotoCardDetailPage
{
    if (self.homeTableViewCellDelegate)
    {
        NSDictionary *dataDic = [[NSDictionary alloc] initWithObjectsAndKeys:_journal,@"journal", nil];
        [self.homeTableViewCellDelegate goToCardDetailPageWithType:2 andData:dataDic andCellIndexPath:self.fromCellIndexPath];
    }

}
- (void)photoAndTitleViewClick
{
    if (self.homeJournalTableViewCellDelegate && [self.homeJournalTableViewCellDelegate respondsToSelector:@selector(gotoJournalDetailPage:cellIndexPath:)])
    {
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:self.journal.restaurantId forKey:@"RestaurantID"];
        [eventProperties setValue:self.journal.restaurantTitle forKey:@"RestaurantTitle"];
        [eventProperties setValue:user.userId forKey:@"UserID"];
        [eventProperties setValue:user.userName forKey:@"UserName"];
        [[Amplitude instance] logEvent:@"Click -  Article card" withEventProperties:eventProperties];
        [self.homeJournalTableViewCellDelegate gotoJournalDetailPage:_journal cellIndexPath:self.fromCellIndexPath];
    }
}

-(void)setLiked:(BOOL)liked{
	_liked=liked;
	UIButton *btn=(UIButton*)[self viewWithTag:200];
	UIImageView *btnImageView=(UIImageView*)[self viewWithTag:100];
	if(liked){
		[btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
		[btnImageView setImage:[UIImage imageNamed:@"like-select"]];
	}else{
		[btn setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
		[btnImageView setImage:[UIImage imageNamed:@"like"]];
	}
}

- (void)likeCommentShareView:(UIView*)likeCommentShareView_ viewPreviousCommentsTapped:(UIButton*)button lastComment:(id)comment{
    //goto CDP screen
    
    [[Amplitude instance] logEvent:@"CL - View Previous Comments CTA" withEventProperties:@{@"Location":@"Journal card on homepage"}];
    [self gotoCardDetailPage];
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView_ likeButtonTapped:(UIButton *)button{
    
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:_journal.mediaCommentId forKey:@"Journal_ID"];
    [eventProperties setValue:@"Journal card on homepage" forKey:@"Location"];
    
    [JournalHandler likeJournal:_journal.isLike articleId:_journal.mediaCommentId andBlock:^{
        if (!_journal.isLike) {
            [[Amplitude instance] logEvent:@"UC - UnLike Journal Succeed" withEventProperties:eventProperties];
        }else{
            [[Amplitude instance] logEvent:@"UC - Like Journal Succeed" withEventProperties:eventProperties];
            [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Like Journal Succeed" withValues:eventProperties];
        }
    }failure:^(NSString *exceptionMsg){
        if([exceptionMsg isLogout]){
            [[LoadingView sharedLoadingView] stopLoading];
            [[IMGUser currentUser]logOut];
            [[AppDelegate ShareApp] goToLoginController];
        }
    }];
    _journal.isLike=!_journal.isLike;
    if(_journal.isLike){
        _journal.likeCount=[NSNumber numberWithInt:[_journal.likeCount intValue]+1];
        
        [[Amplitude instance] logEvent:@"UC - Like Journal Initiate" withEventProperties:eventProperties];
    }else{
        _journal.likeCount=[NSNumber numberWithInt:[_journal.likeCount intValue]-1];
        [[Amplitude instance] logEvent:@"UC - UnLike Journal Initiate" withEventProperties:eventProperties];
    }
    
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self likeButtonTapped:button entity:_journal];
    }

}

-(void)likeCommentShareView:(UIView *)likeCommentShareView shareButtonTapped:(UIButton *)button{
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self shareButtonTapped:button entity:_journal];
    }
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView commentButtonTapped:(UIButton *)button{
    //home card cell都留空
    
    if (self.homeTableViewCellDelegate)
    {
        NSDictionary *dataDic = [[NSDictionary alloc] initWithObjectsAndKeys:_journal,@"journal", nil];
        [self.homeTableViewCellDelegate goToCardDetailPageWithType:2 andData:dataDic andCellIndexPath:self.fromCellIndexPath];
    }
}


- (void)likeCommentShareView:(UIView*)likeCommentShareView_ commentInputView:(UIView *)commentInputView inputViewExpand:(UITextView *)inputTextView commentInputViewYOffset:(CGFloat)commentInputViewYOffset{
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self likeCommentShareView:likeCommentShareView_ commentInputView:commentInputView inputViewExpand:inputTextView commentInputViewYOffset:commentInputViewYOffset];
    }
}

- (void)likeCommentShareView:(UIView*)likeCommentShareView_ commentInputView:(UIView *)commentInputView postButtonTapped:(UIButton *)postButton commentInputText:(NSString *)text{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    if(self.homeTableViewCellDelegate){
        postButton.enabled = NO;
        
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:_journal.mediaCommentId forKey:@"Journal_ID"];
        [eventProperties setValue:@"Journal card on homepage" forKey:@"Location"];
        
        [JournalHandler postComment:text articleId:_journal.mediaCommentId andBlock:^(IMGJournalComment *journalComment) {
            postButton.enabled = YES;
            _journal.commentCount=[NSNumber numberWithInt:([_journal.commentCount intValue]+1)];
            if(_journal.commentList){
                if([_journal.commentList isKindOfClass:[NSArray class]]){
                    _journal.commentList=[NSMutableArray arrayWithArray:_journal.commentList];
                }
                [_journal.commentList insertObject:journalComment atIndex:0];
            }else{
                _journal.commentList = [[NSMutableArray alloc]initWithObjects:journalComment, nil];
            }

            [eventProperties setValue:journalComment.commentId forKey:@"Comment_ID"];
            [[Amplitude instance] logEvent:@"UC - Comment Journal Succeed" withEventProperties:eventProperties];
            [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Journal Succeed" withValues:eventProperties];
            
            [self.homeTableViewCellDelegate homeCardTableViewCell:self newCommentPosted:journalComment];
        } failure:^(NSString *errorReason){
            if([errorReason isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
                [[AppDelegate ShareApp] goToLoginController];
            }
            [eventProperties setValue:errorReason forKey:@"Reason"];
            [[Amplitude instance] logEvent:@"UC - Comment Journal Failed" withEventProperties:eventProperties];
        }];
        [[Amplitude instance] logEvent:@"UC - Comment Journal Initiate" withEventProperties:eventProperties];
      
    }
}

- (void)likeCommentShareView:(UIView*)likeCommentShareView_ commentInputView:(UIView *)view inputViewDidBeginEditing:(UITextView *)textView{
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self likeCommentShareView:likeCommentShareView_ commentInputView:view inputViewDidBeginEditing:textView];
    }
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView commentCell:(UITableViewCell *)cell readMoreTapped:(id)comment offset:(CGFloat)offset{
    if(self.homeTableViewCellDelegate){
        [self.homeTableViewCellDelegate homeCardTableViewCell:self commentCell:cell readMoreTapped:comment offset:offset];
    }
}
@end
