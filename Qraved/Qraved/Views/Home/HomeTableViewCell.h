//
//  HomeTableViewCell.h
//  Qraved
//
//  Created by lucky on 16/3/2.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"
#import "IMGReview.h"
#import "UILikeCommentShareView.h"

#define cellMargin 20
#define ONE_PHOTO_HEIGHT 161

@class IMGDiningGuide,IMGDish,IMGReview;

@protocol HomeTableViewCellDelegate<NSObject>
@optional
-(void)shareBtnClick:(UIActivityViewController*)activityCtl;
-(void)goToDiningGuide:(IMGDiningGuide*)dingingGudie;
-(void)goToDiningGuideList;
-(void)commentBtnClick:(NSDictionary*)context;
-(void)mapBtnClick:(IMGRestaurant*)restaurant;
-(void)viewFullImage:(IMGEntity *)dish;
-(void)viewFullImage:(IMGEntity *)dish andRestaurant:(IMGRestaurant*)restaurant andReview:(IMGReview*)review;
- (void)gotoRestaurantWithRestaurantId:(NSNumber *)restaurantId;
- (void)gotoOtherProfileWithUser:(IMGUser *)user;

- (void)goToCardDetailPageWithType:(int)type andData:(NSDictionary *)dataDic andCellIndexPath:(NSIndexPath*)cellIndexPath;

- (void)homeCardTableViewCell:(UITableViewCell*)cell shareButtonTapped:(UIButton*)button entity:(id)entity;
- (void)homeCardTableViewCell:(UITableViewCell*)cell shareCardButtonTapped:(UIButton*)button entity:(id)entity;
- (void)homeCardTableViewCell:(UITableViewCell*)cell shareMutiPhotosCardButtonTapped:(UIButton*)button entity:(id)entity;
- (void)homeCardTableViewCell:(UITableViewCell*)cell likeButtonTapped:(UIButton*)button entity:(id)entity;
- (void)homeCardTableViewCell:(UITableViewCell*)cell commentCell:(UITableViewCell *)commentCell readMoreTapped:(id)comment offset:(CGFloat)offset;
- (void)homeCardTableViewCell:(UITableViewCell*)cell newCommentPosted:(id)comment;
- (void)homeCardTableViewCell:(UITableViewCell*)cell likeCommentShareView:(UIView*)likeCommentShareView commentInputView:(UIView *)commentInputView inputViewExpand:(UITextView *)inputTextView commentInputViewYOffset:(CGFloat)commentInputViewYOffset;
- (void)homeCardTableViewCell:(UITableViewCell*)cell likeCommentShareView:(UIView*)likeCommentShareView commentInputView:(UIView *)view inputViewDidBeginEditing:(UITextView *)textView;
- (void)homeCardTableViewUserNameOrImageTapped:(id)sender;
- (void)homeCardTableViewReloadCell:(UITableViewCell *)cell;
- (void)homeCardTableViewReloadCell:(NSIndexPath *)indexPath entity:(id)entity;
@end

typedef NS_ENUM(NSUInteger,HomeEngageTableViewCellType){
    HomeEngageTableViewCellNone = 0,
    HomeEngageTableViewCellDish = 1001,
    HomeEngageTableViewCellJournal = 1002,
    HomeEngageTableViewCellOffer = 1003,
    HomeEngageTableViewCellRestaurant = 1004,
    HomeEngageTableViewCellWriteReview = 1005,
    HomeEngageTableViewCellUploadPhoto = 1006
};

@interface HomeTableViewCell : UITableViewCell<LikeCommentShareDelegate>

@property (nonatomic,weak) id<HomeTableViewCellDelegate>homeTableViewCellDelegate;
@property(nonatomic,strong) NSIndexPath *fromCellIndexPath;

@property(nonatomic,assign)HomeEngageTableViewCellType type;
//- (void)setRestaurant:(IMGRestaurant *)restaurant;
//- (float)getCellHeight;
-(void)setHomeEngageTableViewCellType:(HomeEngageTableViewCellType)typeLocal;
@end
