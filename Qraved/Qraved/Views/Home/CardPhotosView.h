//
//  CardPhotosView.h
//  Qraved
//
//  Created by lucky on 16/3/4.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGDish.h"
#import "HomeTableViewCell.h"
#import "IMGReview.h"

@protocol CardPhotosViewDelegate <NSObject>
@optional
-(void)cardPhotosViewImageClick:(int)imageIndex;
-(void)viewFullImage:(IMGEntity *)dish andRestaurant:(IMGRestaurant*)restaurant andReview:(IMGReview*)review;
- (void)cardPhotosChangeHeight:(CGFloat)height;
@end

@interface CardPhotosView : UIView

- (void)setPhotos:(NSArray *)photosArr;
+(float)cardPhotoHeightWithPhotoCout:(NSInteger)count;

@property(nonatomic,assign) id<HomeTableViewCellDelegate> homeTableViewCellDelegate;
@property (nonatomic, assign)id<CardPhotosViewDelegate>cardPhotosViewDelegate;
@property (nonatomic ,strong) IMGRestaurant *restaurant;
@property (nonatomic ,strong) IMGReview *review;
@property (nonatomic, assign) float cardPhotoHeight;
@property (nonatomic,assign) BOOL isFromPromo;

@end
