//
//  BookRatingSplashView.h
//  Qraved
//
//  Created by apple on 16/11/3.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGReservation.h"


@protocol BookRatingSplashViewDelegate<NSObject>

-(void)donotGoLabelTap:(IMGReservation*)restaurant;
-(void)ratingTap:(float)rating andRestaurant:(IMGReservation*)restaurant;

@end
@interface BookRatingSplashView : UIView

@property (nonatomic,assign) id<BookRatingSplashViewDelegate>bookRatingSplashViewDelegate;
@property(nonatomic,strong)IMGReservation *splashRestaurant;
@end
