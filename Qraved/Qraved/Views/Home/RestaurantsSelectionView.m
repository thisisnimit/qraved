//
//  MyListAddRestaurantView.m
//  Qraved
//
//  Created by System Administrator on 9/24/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "RestaurantsSelectionView.h"
#import "RestaurantsSelectionViewCell.h"
#import "ReviewHeaderView.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "UIDevice+Util.h"

#define DEFAULT_SEARCH_TEXT L(@"Search more restaurants by name");


@implementation RestaurantsSelectionView
{
    BOOL loading;
}
@synthesize searchBar;


-(instancetype)initWithFrame:(CGRect)frame title:(NSString*)headerTitle{
	if(self=[super initWithFrame:frame]){

	    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 10, DeviceWidth, 45)];
	    searchBar.delegate = self;
	    searchBar.enablesReturnKeyAutomatically=NO;
	    searchBar.placeholder = DEFAULT_SEARCH_TEXT;

	    if ([UIDevice isLaterThanIos6]) {
	        searchBar.searchBarStyle = UISearchBarStyleMinimal;
	    }else{
	        for(UIView *subview in searchBar.subviews){
	            if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
	                [subview removeFromSuperview];
	            }
	        }
	    }
	    searchBar.layer.borderColor = [UIColor whiteColor].CGColor;
	    searchBar.layer.borderWidth = 0.3;
        [searchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"search_input-box" ]  forState:UIControlStateNormal];
        [searchBar setImage:[UIImage imageNamed:@"ic_search_medium"]
                forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];

	    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0 ,searchBar.endPointY,frame.size.width,frame.size.height-searchBar.endPointY)];
	    _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
	    _tableView.delegate=self;
	    _tableView.dataSource=self;

        _tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            loading = YES;
            if([self.delegate respondsToSelector:@selector(loadRestaurantData:block:)]){
                [self.delegate loadRestaurantData:searchBar.text block:^(BOOL _hasData){
                    [_tableView.mj_footer endRefreshing];
                    if(!_hasData){
                        _tableView.mj_footer.hidden=YES;
                    }else{
                        _tableView.mj_footer.hidden=NO;
                    }
                    loading = NO;
                    
                }];
            }
        }];
        _tableView.mj_footer.automaticallyChangeAlpha = YES;
       // _tableView.mj_footer.hidden=YES;

	    [self addObserver:self forKeyPath:@"restaurants" options:NSKeyValueObservingOptionNew context:nil];

	    //[self addSubview:headeView];
	    [self addSubview:searchBar];
	    [self addSubview:_tableView];
	}
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame title:(NSString*)headerTitle isNeedWriteReview:(BOOL)isNeedWriteReview{
    if(self=[super initWithFrame:frame]){
        
        
        self.backgroundColor = [UIColor whiteColor];
        
        
        searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 10, DeviceWidth, 45)];
        searchBar.delegate = self;
        searchBar.enablesReturnKeyAutomatically=NO;
        searchBar.placeholder = DEFAULT_SEARCH_TEXT;
        [searchBar becomeFirstResponder];
        
        if ([UIDevice isLaterThanIos6]) {
            searchBar.searchBarStyle = UISearchBarStyleMinimal;
        }else{
            for(UIView *subview in searchBar.subviews){
                if ([subview isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                    [subview removeFromSuperview];
                }
            }
        }
        searchBar.layer.borderColor = [UIColor whiteColor].CGColor;
        searchBar.layer.borderWidth = 0.3;
        //searchBar.backgroundColor = [UIColor whiteColor];
       
        [searchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"search_input-box" ]  forState:UIControlStateNormal];
        [searchBar setImage:[UIImage imageNamed:@"ic_search_medium"]
                forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, searchBar.endPointY+15, DeviceWidth-30, 16)];
        lblTitle.font = [UIFont systemFontOfSize:14];
        lblTitle.textColor = [UIColor color333333];
        lblTitle.text = @"RESTAURANTS";
        
        
        _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0 ,searchBar.endPointY+46,frame.size.width,frame.size.height-searchBar.endPointY-46-64)];
        _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
        _tableView.delegate=self;
        _tableView.dataSource=self;
       // if (@available(iOS 11.0, *)) {
//            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
//        }
        _tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
            loading = YES;
            if([self.delegate respondsToSelector:@selector(loadRestaurantData:block:)]){
                [self.delegate loadRestaurantData:searchBar.text block:^(BOOL _hasData){
                    [_tableView.mj_footer endRefreshing];
                    if(!_hasData){
                        _tableView.mj_footer.hidden=YES;
                    }else{
                        _tableView.mj_footer.hidden=NO;
                    }
                    loading = NO;

                }];
            }
        }];
        _tableView.mj_footer.automaticallyChangeAlpha = YES;
        //_tableView.mj_footer.hidden=YES;
        [self addObserver:self forKeyPath:@"restaurants" options:NSKeyValueObservingOptionNew context:nil];
        
       // [self addSubview:self.headeView];
        [self addSubview:searchBar];
        [self addSubview:lblTitle];
        [self addSubview:_tableView];
    }
    return self;
}

-(void)closeButtonClick{
	if(self.delegate && [self.delegate respondsToSelector:@selector(closeButtonClick)]){
		[self.delegate closeButtonClick];
	}
}

//-(void)addRefreshView{
//    self.refreshView=[[EGORefreshTableFooterView alloc] initWithFrame:CGRectMake(0,self.tableView.contentSize.height+10,self.tableView.frame.size.width,66)];
//    self.refreshView.delegate=self;
//    self.refreshView.backgroundColor=[UIColor whiteColor];
//    [self.tableView addSubview:self.refreshView];
//}

//-(void)resetRefreshViewFrame{
//    if(self.refreshView==nil){
//        [self addRefreshView];
//    }
//    if(self.frame.size.height-searchBar.endPointY>self.tableView.contentSize.height){
//        self.refreshView.frame=CGRectMake(0,self.frame.size.height+10,self.tableView.frame.size.width,66);
//    }else{
//        self.refreshView.frame=CGRectMake(0,self.tableView.contentSize.height+10,self.tableView.frame.size.width,66);
//    }
//
//}

//-(void)hideRefreshView{
//    self.refreshView.hidden=YES;
//}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexpath{
	return 64;
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section{
	return self.restaurants.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
	return 1;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexpath{
	RestaurantsSelectionViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"viewedcell"];
	if(cell==nil){
		cell=[[RestaurantsSelectionViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"viewedcell"];
	}
	IMGRestaurant *restaurant=[self.restaurants objectAtIndex:indexpath.row];

    cell.restaurant = restaurant;
	return cell;
}

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexpath{
	IMGRestaurant *restaurant=[self.restaurants objectAtIndex:indexpath.row];

    if(self.delegate && [self.delegate respondsToSelector:@selector(pickPhotosWithRestaurant:block:)]){
        [self.delegate pickPhotosWithRestaurant:restaurant block:^(BOOL succeed){
        	if(succeed){
			}
        }];
	}
}

-(void)scrollViewDidScroll:(UITableView*)tableView{
	//[self.refreshView egoRefreshScrollViewDidScroll:tableView];
	[searchBar resignFirstResponder];
}

//- (void)scrollViewDidEndDragging:(UITableView*)tableView willDecelerate:(BOOL)decelerate{
//
//    if(tableView.contentOffset.y>tableView.contentSize.height-tableView.frame.size.height+60){
//        tableView.contentOffset=CGPointMake(0,tableView.contentSize.height-tableView.frame.size.height+66);
//        [self resetRefreshViewFrame];
//        [self.refreshView egoRefreshScrollViewDidEndDragging:tableView];
//    }
//}

//-(void)egoRefreshTableFooterDidTriggerRefresh:(EGORefreshTableFooterView*)refreshView{
//    loading = YES;
//    if([self.delegate respondsToSelector:@selector(loadRestaurantData:block:)]){
//        //__block __weak EGORefreshTableFooterView *weakRefreshView=refreshView;
//        [self.delegate loadRestaurantData:searchBar.text block:^(BOOL _hasData){
//            if(!_hasData){
//               _tableView.mj_footer.hidden=YES;
//            }
//            loading = NO;
////            [weakRefreshView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
////            self.refreshView.frame=CGRectMake(0,self.tableView.contentSize.height,self.tableView.frame.size.width,66);
//        }];
//    }
//}
//- (BOOL)egoRefreshTableFooterDataSourceIsLoading:(EGORefreshTableFooterView*)view{
//    return loading;
//}


-(void)observeValueForKeyPath:(NSString*)keyPath ofObject:object change:(NSDictionary *)change context:(void*)context{
	if([keyPath isEqualToString:@"restaurants"]){
		[self.tableView reloadData];
	}
}

-(BOOL)textFieldShouldReturn:(UITextField*)field{
	[field resignFirstResponder];
	return NO;
}

-(void)searchBar:(UISearchBar*)searchbar textDidChange:(NSString*)text{
    loading = NO;
    if(!text.length){
		searchbar.placeholder=DEFAULT_SEARCH_TEXT;
	}
    _tableView.mj_footer.hidden=YES;
    if([self.delegate respondsToSelector:@selector(searchRestaurants:block:)]){
		//__block __weak EGORefreshTableFooterView *weakRefreshView=self.refreshView;
		[self.delegate searchRestaurants:searchBar.text block:^(BOOL _hasData){
            [_tableView.mj_footer endRefreshing];
            if(!_hasData){
                _tableView.mj_footer.hidden=YES;
            }else{
                _tableView.mj_footer.hidden=NO;
            }
//            [weakRefreshView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
//            weakRefreshView.frame=CGRectMake(0,self.tableView.contentSize.height,self.tableView.frame.size.width,66);
		}];
	}
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)_searchBar {
    _searchBar.showsCancelButton = NO;
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)_searchBar {
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)_searchBar {
    _searchBar.showsCancelButton = NO;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)_searchBar {
    [_searchBar resignFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)_searchBar {
    [_searchBar resignFirstResponder];
}

-(void)dealloc{
	[self removeObserver:self forKeyPath:@"restaurants"];
}

@end
