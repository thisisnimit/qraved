//
//  MyListAddRestaurantCell.m
//  Qraved
//
//  Created by root on 9/26/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "RestaurantsSelectionViewCell.h"
#import "UIConstants.h"
#import "UIColor+Helper.h"

@implementation RestaurantsSelectionViewCell{
    UILabel *titleLabel;
    UILabel *cuisineLabel;
    UIImageView *imgRestaurant;
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
    if(self=[super initWithStyle:style reuseIdentifier:identifier]){
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        imgRestaurant = [[UIImageView alloc] initWithFrame:CGRectMake(15, 8, 48, 48)];
        
        titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(imgRestaurant.endPointX+10,9,DeviceWidth-90,22)];
        titleLabel.font=[UIFont systemFontOfSize:14];
        titleLabel.numberOfLines=1;
        cuisineLabel=[[UILabel alloc] initWithFrame:CGRectMake(imgRestaurant.endPointX+10,titleLabel.endPointY,DeviceWidth-90,20)];
        cuisineLabel.font=[UIFont systemFontOfSize:14];
        cuisineLabel.textColor=[UIColor color999999];
        cuisineLabel.numberOfLines=1;
        [self addSubview:imgRestaurant];
        [self addSubview:titleLabel];
        [self addSubview:cuisineLabel];
    }
    return self;
}

- (void)setRestaurant:(IMGRestaurant *)restaurant{
    _restaurant = restaurant;
    
    titleLabel.text = restaurant.title;
    
    NSString *showStr ;
    
    if ([restaurant.landMarkList firstObject]) {
        showStr = [NSString stringWithFormat:@"%@",[[restaurant.landMarkList firstObject] objectForKey:@"name"]];
    }else if(restaurant.districtName.length>1){
        showStr = [NSString stringWithFormat:@"%@",restaurant.districtName];
    }else{
        showStr = @"";
    }
    
    if (restaurant.landMarkList.count>0) {
        showStr = [NSString stringWithFormat:@"%@ • %@ • %@",restaurant.cuisineName, restaurant.districtName,[[restaurant.landMarkList objectAtIndex:0] objectForKey:@"name"]];
    }else{
        showStr = [NSString stringWithFormat:@"%@ • %@",restaurant.cuisineName, restaurant.districtName]; 
    }
    
    NSString *distance = [restaurant distance];
    if ([distance isEqualToString:@""] || [showStr isEqualToString:@""]) {
        cuisineLabel.text = [NSString stringWithFormat:@"%@%@",showStr,distance];
    }else{
        cuisineLabel.text = [NSString stringWithFormat:@"%@ • %@",showStr,distance];
    }
    
    NSString *imageUrl = restaurant.imageUrl;
    if (![imageUrl isKindOfClass:[NSNull class]]) {
        NSArray *images;
        if (imageUrl.length>0) {
            images=[imageUrl componentsSeparatedByString:@","];
            
        }
        if(imageUrl&& imageUrl.length && images.count>0){
            __weak typeof (imgRestaurant) weakRestaurantImageView = imgRestaurant;
            [imgRestaurant sd_setImageWithURL:[NSURL URLWithString:[images[0] returnFullImageUrlWithWidth:48]] placeholderImage:[[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake(48, 48)]completed:^(UIImage *image_, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
                if (!image_ && error) {
                    weakRestaurantImageView.image = [[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake(48, 48)];
                }else{
                    image_ = [image_ imageByScalingAndCroppingForSize:CGSizeMake(48, 48)];
                    weakRestaurantImageView.image = image_;
                }
                
            }];
        }else{
            imgRestaurant.image = [[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake(48, 48)];
        }
        
    }else{
        imgRestaurant.image = [[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake(48, 48)];
    }
}

@end
