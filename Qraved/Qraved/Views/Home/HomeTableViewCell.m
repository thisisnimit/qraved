//
//  HomeTableViewCell.m
//  Qraved
//
//  Created by lucky on 16/3/2.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "HomeTableViewCell.h"
#import "IMGDiningGuide.h"

@implementation HomeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setHomeEngageTableViewCellType:(HomeEngageTableViewCellType)typeLocal{
    self.type = typeLocal;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
	if(self=[super initWithStyle:style reuseIdentifier:identifier]){
		self.selectionStyle=UITableViewCellSelectionStyleNone;
        [self setHomeEngageTableViewCellType:HomeEngageTableViewCellNone];
	}
	return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)likeCommentShareViewUserNameOrImageTapped:(id)sender
{
    if (self.homeTableViewCellDelegate)
    {
        [self.homeTableViewCellDelegate homeCardTableViewUserNameOrImageTapped:sender];
    }
}

@end
