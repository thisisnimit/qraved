//
//  PopReviewTableViewCell.h
//  Qraved
//
//  Created by DeliveLee on 16/3/13.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGJournalComment.h"
#import "IMGDishComment.h"
#import "IMGReviewComment.h"
#import "IMGRestaurantEventComment.h"

@protocol PopReviewTableViewCellDelegate <NSObject>

- (void)popReviewTableViewCellUserNameOrImageTapped:(id)sender;

@end

@interface PopReviewTableViewCell : UITableViewCell

@property (nonatomic, assign) id<PopReviewTableViewCellDelegate>delegate;


+(CGFloat)calculatedHeight:(id)journalComment;
-(void)updateUI:(id)journalComment;

@end
