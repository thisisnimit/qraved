//
//  ImageListTableViewCell.h
//  Qraved
//
//  Created by DeliveLee on 16/3/9.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UILikeCommentShareView.h"
#import "LikeView.h"

@class IMGRestaurantEvent,IMGReview,IMGRestaurant;

@protocol ImageListTableViewCellDelegate <NSObject>
-(void)viewFullImage:(int)index;

- (void)imageListTableViewCell:(UITableViewCell*)cell shareButtonTapped:(UIButton*)button entity:(id)entity;
- (void)imageListTableViewCell:(UITableViewCell*)cell commentButtonTapped:(UIButton*)button entity:(id)entity;
- (void)imageListTableViewCell:(UITableViewCell*)cell likeButtonTapped:(UIButton*)button entity:(id)entity;
- (void)imageListTableViewCell:(UITableViewCell*)cell newCommentPosted:(id)comment;
- (void)imageListTableViewCell:(UITableViewCell*)cell likeCommentShareView:(UIView*)likeCommentShareView commentInputView:(UIView *)commentInputView inputViewExpand:(UITextView *)inputTextView commentInputViewYOffset:(CGFloat)commentInputViewYOffset;
- (void)imageListTableViewCell:(UITableViewCell*)cell likeCommentShareView:(UIView*)likeCommentShareView commentInputView:(UIView *)view inputViewDidBeginEditing:(UITextView *)textView;
//-(void)viewFullImage:(IMGEntity *)dish andRestaurant:(IMGRestaurant*)restaurant andReview:(IMGReview*)review;
@end


@interface ImageListTableViewCell : UITableViewCell<LikeCommentShareDelegate,LikeListViewDelegate>

@property(nonatomic,assign) NSInteger cellHeight;
@property (nonatomic, weak)id<ImageListTableViewCellDelegate>delegate;


-(void)setCellData:(IMGRestaurantEvent *)_dish;
+(CGFloat)calculateHeight:(IMGEntity *)_dish;
@end
