//
//  MyListAddRestaurantView.h
//  Qraved
//
//  Created by System Administrator on 9/24/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "EGORefreshTableFooterView.h"
#import "RestaurantsSelectionViewController.h"
@class ReviewHeaderView;
@interface RestaurantsSelectionView : UIView<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UISearchBarDelegate>

@property(nonatomic,strong) NSNumber *listId;

@property(nonatomic,strong) NSMutableArray *restaurants;

@property(nonatomic,strong) UISearchBar *searchBar;
@property(nonatomic,strong) UITableView *tableView;
//@property(nonatomic,strong) EGORefreshTableFooterView *refreshView;

@property(nonatomic,weak) NSObject<RestaurantsSelectionDelegate> *delegate;

-(instancetype)initWithFrame:(CGRect)frame title:(NSString*)headerTitle;
-(instancetype)initWithFrame:(CGRect)frame title:(NSString*)headerTitle isNeedWriteReview:(BOOL)isNeedWriteReview;
//-(void)addRefreshView;
//-(void)resetRefreshViewFrame;

@end


