//
//  ImageListTableViewCell.m
//  Qraved
//
//  Created by DeliveLee on 16/3/9.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "ImageListTableViewCell.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import "IMGDish.h"
#import "UIColor+Helper.h"
#import "IMGRestaurant.h"
#import "UIImageView+WebCache.h"
#import "UILabel+helper.h"
#import "UIImageView+Helper.h"
#import "NSString+Helper.h"
#import "IMGRestaurantEvent.h"
#import "LoadingView.h"
#import "UICommentInputView.h"
#import "RestaurantHandler.h"
#import "AppDelegate.h"
#import "IMGUser.h"
#import "EntityKind.h"
#import "UploadPhotoHandler.h"
#import "HomeUtil.h"
#import "LikeView.h"



@implementation ImageListTableViewCell{
    IMGRestaurantEvent *restaurantEvent;
    IMGDish *dish;
    UILabel *likeLabel;
    UILabel *commentLabel;
    UIImageView *photosView;
    UIView *cellGapView;
    IMGType imgType;
    UILikeCommentShareView *likeCommentShareView;
    NSMutableArray* likelistArr;
    LikeView* likepopView;

}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
    if(self=[super initWithStyle:style reuseIdentifier:identifier]){
        
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        
        photosView = [[UIImageView alloc] init];
        [self addSubview:photosView];
        
        
        likeLabel=[[UILabel alloc] init];
        likeLabel.userInteractionEnabled=YES;
        UITapGestureRecognizer* likeLabelGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(likeLabelTap:)];
        likeLabelGesture.numberOfTapsRequired=1;
        [likeLabel addGestureRecognizer:likeLabelGesture];
        likeLabel.font=[UIFont systemFontOfSize:9];
        likeLabel.textColor = [UIColor colorC2060A];
        
        commentLabel=[[UILabel alloc] init];
        
        commentLabel.font=[UIFont systemFontOfSize:9];
        commentLabel.textColor = [UIColor colorC2060A];
        
        
        if(likeCommentShareView){
            [likeCommentShareView removeFromSuperview];
        }

        cellGapView = [[UIView alloc]init];
        cellGapView.backgroundColor = [UIColor colorD9D9D9];
        
        UIImageView *shadowImage = [[UIImageView alloc] initShadowImageViewWithShadowOriginY_v2:0 andHeight:5];

        [self addSubview:cellGapView];
        
        
        [self addSubview:likeLabel];
        [self addSubview:commentLabel];
        
        [cellGapView addSubview:shadowImage];
        
    }
    return self;
}

-(void)likeLabelTap:(UITapGestureRecognizer*)tap
{
    [[Amplitude instance] logEvent:@" RC - View People Who Likes"];

    if (likepopView) {
        [likepopView removeFromSuperview];
    }

    [[LoadingView sharedLoadingView]startLoading];
    
    if (imgType == TypeForIMGDish) {

        [HomeUtil getLikeListFromServiceWithDishId:dish.dishId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray, NSNumber *likeviewcount) {
            
            
            likelistArr=[NSMutableArray arrayWithArray:likeListArray];
            
            likepopView.likelistArray=likelistArr;
            likepopView.likecount=likeviewcount;
            [[LoadingView sharedLoadingView]stopLoading];
            
    }];
    }else if (TypeForIMGRestaurantEvent){
    
        [HomeUtil getLikeListFromServiceWithResturantId:restaurantEvent.eventId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:0] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
            likelistArr=[NSMutableArray arrayWithArray:likeListArray];
            likepopView.likecount=likeviewcount;
            [[LoadingView sharedLoadingView]stopLoading];
            
            likepopView.likelistArray=likelistArr;
    }];
    
    }
    likepopView=[[LikeView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight)];
    likepopView.delegate=self;
    
    UIResponder *responder = [self nextResponder];
    while (responder){
        if ([responder isKindOfClass:[UINavigationController class]]){
            UINavigationController* vc=(UINavigationController*)responder;
            [[vc.viewControllers lastObject].view addSubview:likepopView];
            return;
        }
        
        responder = [responder nextResponder];
        
    }
 
}
-(void)LikeviewLoadMoreData:(NSInteger)offset
{
        if (imgType == TypeForIMGDish) {
        
        [HomeUtil getLikeListFromServiceWithDishId:dish.dishId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset] andBlock:^(NSArray *likeListArray, NSNumber *likeviewcount) {
            likepopView.likecount=likeviewcount;
            [likelistArr addObjectsFromArray:likeListArray];
            [likepopView.likeUserTableView reloadData];
        }];
    }else if (TypeForIMGRestaurantEvent){
        
        [HomeUtil getLikeListFromServiceWithResturantId:restaurantEvent.eventId andMax:[NSNumber numberWithInteger:10] andoffset:[NSNumber numberWithInteger:offset] andBlock:^(NSArray *likeListArray,NSNumber* likeviewcount) {
             likepopView.likecount=likeviewcount;
            [likelistArr addObjectsFromArray:likeListArray];
            [likepopView.likeUserTableView reloadData];
        }];
        
        
        
        
        
    }



}
-(void)returnLikeCount:(NSInteger)count
{
    if (count!=0) {
        
   
    float start = LEFTLEFTSET;
    float top = photosView.endPointY + 5;
    NSString *likeString=[NSString stringWithFormat:@"%ld %@",(long)count,count>1?L(@"Likes"):L(@"Like")];
    [likeLabel setText:likeString];
    likeLabel.frame=CGRectMake(start, top, likeLabel.expectedWidth,20);

    }
}
+(CGFloat)calculateHeight:(IMGEntity *)_dish
{
    float returnHeight = 0.0;
    IMGType type = [EntityKind typeKindFromEntity:_dish];
    if (type == TypeForIMGRestaurantEvent) {
        IMGRestaurantEvent *restaurantEvent = (IMGRestaurantEvent *)_dish;
        
        float imgWidth = DeviceWidth-LEFTLEFTSET*2;
        if (restaurantEvent.imageHeight==nil||[restaurantEvent.imageHeight isEqualToNumber:[NSNumber numberWithInt:0]]) {
            CGSize imgSize = [restaurantEvent getDishImageSize];
            returnHeight = (imgSize.width*imgWidth)/imgWidth;
        }else{
            returnHeight = [restaurantEvent.imageHeight floatValue];
        }
        if( ([restaurantEvent.likeCount intValue]>0) || ([restaurantEvent.commentCount intValue]>0) ){
            returnHeight = returnHeight + 20 +25;
        }else{
            returnHeight = returnHeight + 30;
        }
        
        returnHeight =returnHeight+COMMUNITYBUTTONVIEWHEIGHT;
        
    }else if (type == TypeForIMGDish){
        IMGDish *dish = (IMGDish *)_dish;
        
        float imgWidth = DeviceWidth-LEFTLEFTSET*2;
        if (dish.imageHeight==nil||[dish.imageHeight isEqualToNumber:[NSNumber numberWithInt:0]]) {
            CGSize imgSize = [dish getDishImageSize];
            returnHeight = (imgSize.width*imgWidth)/imgWidth;
        }else{
            returnHeight = [dish.imageHeight floatValue];
        }
        
        if( ([dish.likeCount intValue]>0) || ([dish.commentCount intValue]>0) ){
            returnHeight = returnHeight + 20;
        }
        
        returnHeight = returnHeight + 30+COMMUNITYBUTTONVIEWHEIGHT;
        
    }
    return returnHeight;
}
-(void)setCellData:(IMGEntity *)_dish
{
    
    [likeLabel setHidden:YES];
    [commentLabel setHidden:YES];
    [cellGapView setHidden:YES];
  
    imgType = [EntityKind typeKindFromEntity:_dish];
    if (imgType == TypeForIMGRestaurantEvent) {
        restaurantEvent = (IMGRestaurantEvent *)_dish;
    
        float imgWidth = DeviceWidth-LEFTLEFTSET*2;
        float imgHeight;
        if (restaurantEvent.imageHeight==nil||[restaurantEvent.imageHeight isEqualToNumber:[NSNumber numberWithInt:0]]) {
            CGSize imgSize = [restaurantEvent getDishImageSize];
            imgHeight = (imgSize.width*imgWidth)/imgWidth;
        }else{
            imgHeight = [restaurantEvent.imageHeight floatValue];
        }
        
        photosView.frame = CGRectMake(LEFTLEFTSET, 5, imgWidth, imgHeight);
        
        __weak typeof(photosView) weakImageView = photosView;
        UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(imgWidth, imgHeight)];

        [photosView setImageWithURL:[NSURL URLWithString:[restaurantEvent.imageUrl returnFullImageUrlWithWidth:imgWidth]] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
            weakImageView.image = [image imageByScalingAndCroppingForSize:CGSizeMake(imgWidth, imgHeight)];
        }];
        UITapGestureRecognizer *imageViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClicked:)];
        photosView.userInteractionEnabled = YES;
        [photosView addGestureRecognizer:imageViewTapGesture];
        
        
        
        float start = LEFTLEFTSET;
        float top = photosView.endPointY + 5;
        float currentPoint=photosView.endPointY + 10;
        
        if([restaurantEvent.likeCount intValue]>0){
            NSString *likeString=[NSString stringWithFormat:@"%@ %@",restaurantEvent.likeCount,[restaurantEvent.likeCount intValue]>1?L(@"Likes"):L(@"Like")];
            [likeLabel setText:likeString];
            likeLabel.frame=CGRectMake(start, top, likeLabel.expectedWidth,20);
            start+=likeLabel.expectedWidth+15;
            currentPoint = likeLabel.endPointY;
            [likeLabel setHidden:NO];
        }
        if([restaurantEvent.commentCount intValue]>0){
            NSString *commentString=[NSString stringWithFormat:@"%@ %@",restaurantEvent.commentCount,[restaurantEvent.commentCount intValue]>1?L(@"Comments"):L(@"Comment")];
            [commentLabel setText:commentString];
            commentLabel.frame=CGRectMake(start, top, commentLabel.expectedWidth,20);
            start+=commentLabel.expectedWidth+15;
            currentPoint = commentLabel.endPointY;
            [commentLabel setHidden:NO];
        }

        
        if(likeCommentShareView){
            [likeCommentShareView removeFromSuperview];
        }
        likeCommentShareView = [[UILikeCommentShareView alloc]initInSuperView:self atStartX:0 startY:currentPoint commentCount:[restaurantEvent.commentCount intValue] commentArray:restaurantEvent.commentList liked:restaurantEvent.isLike withFooter:YES needCommentListView:NO];
        likeCommentShareView.likeCommentShareDelegate=self;
        self.cellHeight=likeCommentShareView.endPointY;
        
        
        
        cellGapView.frame = CGRectMake(0, likeCommentShareView.endPointY, DeviceWidth, 15);
        [cellGapView setHidden:NO];
        self.cellHeight=cellGapView.endPointY;

    }else if (imgType == TypeForIMGDish){
        dish = (IMGDish *)_dish;
        
        float imgWidth = DeviceWidth-LEFTLEFTSET*2;
        float imgHeight;
        if (dish.imageHeight==nil||[dish.imageHeight isEqualToNumber:[NSNumber numberWithInt:0]]) {
            CGSize imgSize = [dish getDishImageSize];
            imgHeight = (imgSize.width*imgWidth)/imgWidth;
        }else{
            imgHeight = [dish.imageHeight floatValue];
        }
        
        photosView.frame = CGRectMake(LEFTLEFTSET, 5, imgWidth, imgHeight);
        
        __weak typeof(photosView) weakImageView = photosView;
        UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(imgWidth, imgHeight)];
        [photosView setImageWithURL:[NSURL URLWithString:[dish.imageUrl returnFullImageUrlWithWidth:imgWidth]] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
            weakImageView.image = [image imageByScalingAndCroppingForSize:CGSizeMake(imgWidth, imgHeight)];

            
            
        }];
        UITapGestureRecognizer *imageViewTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewClicked:)];
        photosView.userInteractionEnabled = YES;
        [photosView addGestureRecognizer:imageViewTapGesture];
        
        float currentPoint=photosView.endPointY + 10;
        
        float start = LEFTLEFTSET;
        float top = currentPoint;
        if([dish.likeCount intValue]>0){
            NSString *likeString=[NSString stringWithFormat:@"%@ %@",dish.likeCount,[dish.likeCount intValue]>1?L(@"Likes"):L(@"Like")];
            [likeLabel setText:likeString];
            likeLabel.frame=CGRectMake(start, top, likeLabel.expectedWidth,20);
            start+=likeLabel.expectedWidth+15;
            currentPoint = likeLabel.endPointY;
            [likeLabel setHidden:NO];
        }
        if([dish.commentCount intValue]>0){
            NSString *commentString=[NSString stringWithFormat:@"%@ %@",dish.commentCount,[dish.commentCount intValue]>1?L(@"Comments"):L(@"Comment")];
            [commentLabel setText:commentString];
            commentLabel.frame=CGRectMake(start, top, commentLabel.expectedWidth,20);
            start+=commentLabel.expectedWidth+15;
            currentPoint = commentLabel.endPointY;
            [commentLabel setHidden:NO];
        }

        
        if(likeCommentShareView){
            [likeCommentShareView removeFromSuperview];
        }
        likeCommentShareView = [[UILikeCommentShareView alloc]initInSuperView:self atStartX:0 startY:currentPoint commentCount:[dish.commentCount intValue] commentArray:dish.commentList liked:dish.isLike withFooter:YES needCommentListView:NO];
        likeCommentShareView.likeCommentShareDelegate=self;
        self.cellHeight=likeCommentShareView.endPointY;
        
        
        
        cellGapView.frame = CGRectMake(0, likeCommentShareView.endPointY, DeviceWidth, 15);
        [cellGapView setHidden:NO];
        self.cellHeight=cellGapView.endPointY;

    }
    
    
    
    return;
}




-(void)imageViewClicked:(UIGestureRecognizer*)tap{
    if (imgType == TypeForIMGRestaurantEvent){
        [self.delegate viewFullImage:[restaurantEvent.eventId intValue]];
        //Restaurant_ID, RestaurantEvent_ID, Photo Source, UploaderUser_ID Origin : Homepage, Restaurant event card detail page, Promo page, Restaurant detail page - event list, Page_ID

        NSString *photoSource;
        if ([@"User ID" isEqualToString:restaurantEvent.photoCreditTypeDic]) {
            photoSource = @"User";
        }else if([@"Qraved Team" isEqualToString:restaurantEvent.photoCreditTypeDic]){
            photoSource = @"Qraved Admin";
        }
        
        [IMGAmplitudeUtil trackPhotoWithName:@"RC - View Restaurant Event detail" andRestaurantId:restaurantEvent.restaurantId andRestaurantTitle:restaurantEvent.restaurantTitle andOrigin:nil andUploaderUser_ID:restaurantEvent.userIdDic andPhoto_ID:nil andRestaurantMenu_ID:nil andRestaurantEvent_ID:restaurantEvent.eventId andPhotoSource:photoSource andLocation:nil];
        
        
    }else if(imgType == TypeForIMGDish){
        [self.delegate viewFullImage:[dish.dishId intValue]];
    }
}


//+(CGFloat)calculatedHeight:(IMGRestaurant *)restaurant andDishArr:(NSArray *)dishArr
//{
//    NSString *title = [NSString stringWithFormat:@"Update from %@",restaurant.title];
//    CGSize expectedLabelSize = [title sizeWithFont:[UIFont systemFontOfSize:NSLineBreakByWordWrapping]
//                                 constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, 99999)
//                                     lineBreakMode:NSLineBreakByWordWrapping];
//    
//    if (dishArr.count > 1)
//    {
//        return expectedLabelSize.height + 310;
//    }
//    else
//        return expectedLabelSize.height + 240;
//    
//}

- (void)likeCommentShareView:(UIView*)likeCommentShareView_ viewPreviousCommentsTapped:(UIButton*)button lastComment:(id)comment{
    //goto CDP screen
    
}
-(void)likeCommentShareView:(UIView *)likeCommentShareView_ likeButtonTapped:(UIButton *)button{
    
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil)
    {
        [[AppDelegate ShareApp]goToLoginController];
        return;
    }
    
//    [[LoadingView sharedLoadingView] startLoading];
    //like service
    if ([dish.dishId intValue]) {
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        //Photo_ID, UploaderUser_ID, Restaurant_ID, Location
        [eventProperties setValue:dish.dishId forKey:@"Photo_ID"];
        [eventProperties setValue:dish.userId forKey:@"UploaderUser_ID"];
        [eventProperties setValue:restaurantEvent.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:@"Restaurant detail page - photo list" forKey:@"Location"];
        [UploadPhotoHandler likeDish:dish.isLike dishId:dish.dishId andBlock:^{
            if(self.delegate){
                if (!dish.isLike) {
                    [[Amplitude instance] logEvent:@"UC - UnLike Photo Succeed" withEventProperties:eventProperties];
                }else{
                    [[Amplitude instance] logEvent:@"UC - Like Photo Succeed" withEventProperties:eventProperties];
                }
                
            }
            dish.isLike = !dish.isLike;
            if(dish.isLike){
                dish.likeCount=[NSNumber numberWithInt:[dish.likeCount intValue]+1];
                
                [[Amplitude instance] logEvent:@"UC - Like Photo Initiate" withEventProperties:eventProperties];
                
            }else{
                dish.likeCount=[NSNumber numberWithInt:[dish.likeCount intValue]-1];
                
                [[Amplitude instance] logEvent:@"UC - UnLike Photo Initiate" withEventProperties:eventProperties];
            }
            if(self.delegate){
                [self.delegate imageListTableViewCell:self likeButtonTapped:button entity:dish];
            }
            
        }failure:^(NSString *exceptionMsg){
            if([exceptionMsg isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
                [[AppDelegate ShareApp] goToLoginController];
            }
            
        }];

    }
    else{
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        //RestaurantEvent_ID, Restaurant_ID, Location
        [eventProperties setValue:restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
        [eventProperties setValue:restaurantEvent.restaurantId forKey:@"Restaurant_ID"];
        [eventProperties setValue:@"Restaurant detail page - event photo list" forKey:@"Location"];
        [RestaurantHandler likeRestaurantEvent:restaurantEvent.isLike restaurantEventId:restaurantEvent.eventId andBlock:^{
           
            if (!restaurantEvent.isLike) {
                [[Amplitude instance] logEvent:@"UC - UnLike Restaurant Event Succeed" withEventProperties:eventProperties];
            }else{
                [[Amplitude instance] logEvent:@"UC - Like Restaurant Event Succeed" withEventProperties:eventProperties];
            }
            if(self.delegate){
                
                
            }
        }failure:^(NSString *exceptionMsg){
            if([exceptionMsg isLogout]){
                [[LoadingView sharedLoadingView] stopLoading];
                [[IMGUser currentUser]logOut];
                [[AppDelegate ShareApp] goToLoginController];
            }
        }];
        restaurantEvent.isLike=!restaurantEvent.isLike;
        if(restaurantEvent.isLike){
            restaurantEvent.likeCount=[NSNumber numberWithInt:[restaurantEvent.likeCount intValue]+1];
            
            [[Amplitude instance] logEvent:@"UC - Like Restaurant Event Initiate" withEventProperties:eventProperties];
        }else{
            restaurantEvent.likeCount=[NSNumber numberWithInt:[restaurantEvent.likeCount intValue]-1];
            
            [[Amplitude instance] logEvent:@"UC - UnLike Restaurant Event Initiate" withEventProperties:eventProperties];
        }
        if(self.delegate){
            [self.delegate imageListTableViewCell:self likeButtonTapped:button entity:restaurantEvent];
        }

    }

    
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView shareButtonTapped:(UIButton *)button{
    if(self.delegate){
        if (imgType == TypeForIMGRestaurantEvent){
            [self.delegate imageListTableViewCell:self shareButtonTapped:button entity:restaurantEvent];
        }else if(imgType == TypeForIMGDish){
            [self.delegate imageListTableViewCell:self shareButtonTapped:button entity:dish];
        }

    }
}

-(void)likeCommentShareView:(UIView *)likeCommentShareView commentButtonTapped:(UIButton *)button{
    //home card cell都留空
    //commentButtonTapped
    if (imgType == TypeForIMGRestaurantEvent){
        [self.delegate imageListTableViewCell:self commentButtonTapped:button entity:restaurantEvent];
    }else if(imgType == TypeForIMGDish){
        [self.delegate imageListTableViewCell:self commentButtonTapped:button entity:dish];
    }
}


- (void)likeCommentShareView:(UIView*)likeCommentShareView_ commentInputView:(UIView *)commentInputView inputViewExpand:(UITextView *)inputTextView commentInputViewYOffset:(CGFloat)commentInputViewYOffset{
    if(self.delegate){
        [self.delegate imageListTableViewCell:self likeCommentShareView:likeCommentShareView_ commentInputView:commentInputView inputViewExpand:inputTextView commentInputViewYOffset:commentInputViewYOffset];
    }
}

- (void)likeCommentShareView:(UIView*)likeCommentShareView_ commentInputView:(UIView *)commentInputView postButtonTapped:(UIButton *)postButton commentInputText:(NSString *)text{
    if(self.delegate){
        //write comment
        IMGUser *currentUser = [IMGUser currentUser];
        if (currentUser.userId == nil || currentUser.token == nil)
        {
            [[AppDelegate ShareApp]goToLoginController];
            return;
        }
        
        if(self.delegate){
            //write comment
            
            NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
            [eventProperties setValue:restaurantEvent.eventId forKey:@"RestaurantEvent_ID"];
            [eventProperties setValue:restaurantEvent.restaurantId forKey:@"Restaurant_ID"];
            [eventProperties setValue:@"Restaurant event card detail page" forKey:@"Location"];
            postButton.enabled = NO;
            [RestaurantHandler postComment:text restaurantEventId:restaurantEvent.eventId andBlock:^(IMGRestaurantEventComment *eventComment) {
                postButton.enabled = YES;
               [eventProperties setValue:eventComment.commentId forKey:@"Comment_ID"];
                [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Succeed" withEventProperties:eventProperties];
                [[AppsFlyerTracker sharedTracker] trackEvent:@"UC - Comment Restaurant Event Succeed" withValues:eventProperties];

                if (restaurantEvent.commentList) {
                    if([restaurantEvent.commentList isKindOfClass:[NSArray class]]){
                        restaurantEvent.commentList=[NSMutableArray arrayWithArray:restaurantEvent.commentList];
                    }
                    if (restaurantEvent.commentList.count >= 3) {
                        [restaurantEvent.commentList removeObjectAtIndex:restaurantEvent.commentList.count-1];
                    }
                    [restaurantEvent.commentList insertObject:eventComment atIndex:0];
                }else{
                    restaurantEvent.commentList = [[NSMutableArray alloc] initWithObjects:eventComment, nil];
                }

                [self.delegate imageListTableViewCell:self newCommentPosted:eventComment];
            } failure:^(NSString *errorReason){
                if([errorReason isLogout]){
                    [[LoadingView sharedLoadingView] stopLoading];
                    [[IMGUser currentUser]logOut];
                    [[AppDelegate ShareApp] goToLoginController];
                }
                [eventProperties setValue:errorReason forKey:@"Reason"];
                [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Failed" withEventProperties:eventProperties];
            }];
            [[Amplitude instance] logEvent:@"UC - Comment Restaurant Event Initiate" withEventProperties:eventProperties];
            
        }

    }
}

- (void)likeCommentShareView:(UIView*)likeCommentShareView_ commentInputView:(UIView *)view inputViewDidBeginEditing:(UITextView *)textView{
    if(self.delegate){
        [self.delegate imageListTableViewCell:self likeCommentShareView:likeCommentShareView_ commentInputView:view inputViewDidBeginEditing:textView];
    }
}



@end
