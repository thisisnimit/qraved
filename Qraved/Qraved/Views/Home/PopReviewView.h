//
//  PopReviewView.h
//  Qraved
//
//  Created by DeliveLee on 16/3/12.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PopReviewViewDelegate

-(void)postComment:(UITextView *)textInput;
-(void)popReviewView:(UIView *)popReviewView viewPreviousCommentsTapped:(UIButton*)button lastComment:(id)comment;
- (void)popReviewViewUserNameOrImageTapped:(id)sender;

@end

@interface PopReviewView : UIView


-(id)initInSuperView:(UIView*)parentView commentCount:(int)commentCount_ commentArray:(NSArray *)commentList_;
-(void)showPopReviewView:(BOOL)hasPreviousComments;
-(void)addNewComment:(id)comment;
-(void)addPreviousComments:(NSArray *)commentList_;
-(void)updateUI;
@property id<PopReviewViewDelegate>delegate;

@end
