//
//  SearchPageResturantCell.h
//  Qraved
//
//  Created by imaginato on 16/5/17.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"
#import "DLStarRatingControl.h"
#import "OfferLabel.h"
#import "IMGDiscover.h"
#import "IMGRestaurant.h"
@protocol SavedCellDelegate <NSObject>

- (void)addTolist:(IMGRestaurant*)restaurant;
- (void)removeFromList:(NSNumber *)restaurantId;
@end


@interface SavedTableViewCell : UITableViewCell
@property (nonatomic,retain) IMGRestaurant * restaurant;



@property (nonatomic,assign) CGFloat startPointY;
@property(nonatomic,assign)NSInteger index;
@property(nonatomic,assign)id<SavedCellDelegate> delegate;

-(void)setRestaurant:(IMGRestaurant *)restaurant andIsSavedPage:(BOOL)isSavedPage;

@end
