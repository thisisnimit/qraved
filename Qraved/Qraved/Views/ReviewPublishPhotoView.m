//
//  ReviewPublishPhotoView.m
//  Qraved
//
//  Created by harry on 2017/7/24.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "ReviewPublishPhotoView.h"
#import "IMGUploadPhoto.h"

@interface ReviewPublishPhotoView ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    UICollectionView *photoCollectionView;
    
}

@end

@implementation ReviewPublishPhotoView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(40, 50);
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 10;
    layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 0);
    
    photoCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 55) collectionViewLayout:layout];
    photoCollectionView.backgroundColor = [UIColor whiteColor];
    photoCollectionView.delegate = self;
    photoCollectionView.dataSource = self;
    
    [self addSubview:photoCollectionView];
    
    [photoCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"photoCell"];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.photoArray.count+1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    
    for (UIView *view in cell.contentView.subviews) {
        [view removeFromSuperview];
    }
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;

    if (indexPath.row == self.photoArray.count) {
        imageView.image = [UIImage imageNamed:@"ic_review_upload@2x"];
    }else{
        
        
        IMGUploadPhoto *photo = [self.photoArray objectAtIndex:indexPath.row];
        photo.tag = indexPath.row;
        if (photo.isOldPhoto)
        {
            NSString *urlString = [photo.imageUrl returnFullImageUrlWithWidth:40];
            __weak typeof(imageView) weakRestaurantImageView = imageView;
            [imageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [UIView animateWithDuration:1.0 animations:^{
                    [weakRestaurantImageView setAlpha:1.0];
                }];
            }];
        }else{
            [imageView setImage:photo.image];
        }
        
        imageView.userInteractionEnabled = YES;
        UIButton *btnCancle = [UIButton buttonWithType:UIButtonTypeCustom];
        btnCancle.frame = CGRectMake(20, 20, 20, 20);
        [btnCancle setImage:[UIImage imageNamed:@"ic_cancel"] forState:UIControlStateNormal];
        btnCancle.tag = indexPath.row;
        [btnCancle setImageEdgeInsets:UIEdgeInsetsMake(8, 8, 0, 0)];
        [btnCancle addTarget:self action:@selector(cancleButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        [imageView addSubview:btnCancle];
        

    }
    
    [cell.contentView addSubview:imageView];
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == self.photoArray.count) {
        if (self.uploadPhoto) {
            self.uploadPhoto();
        }
    }
}

- (void)setPhotoArray:(NSArray *)photoArray{
    _photoArray = photoArray;
    
    [photoCollectionView reloadData];
}

- (void)cancleButtonTapped:(UIButton *)button{
 
    int index = (int)button.tag;
    if (self.removePhoto) {
        self.removePhoto(index);
    }
}


@end
