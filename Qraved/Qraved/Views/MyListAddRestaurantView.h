//
//  MyListAddRestaurantView.h
//  Qraved
//
//  Created by System Administrator on 9/24/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "EGORefreshTableFooterView.h"
#import "MyListDelegate.h"

@interface MyListAddRestaurantView : UIView<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,EGORefreshTableFooterDelegate,UISearchBarDelegate>

@property(nonatomic,strong) NSNumber *listId;

@property(nonatomic,strong) NSMutableArray *restaurants;

@property(nonatomic,strong) UISearchBar *searchBar;
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) UILabel *lblViewed;
@property(nonatomic,strong) EGORefreshTableFooterView *refreshView;
@property(nonatomic,retain)NSString* listName;
@property(nonatomic,weak) NSObject<MyListDelegate> *delegate;

@property (nonatomic, weak) UIImageView *imgV;

-(instancetype)initWithFrame:(CGRect)frame andList:(NSNumber*)listid;

-(void)addRefreshView;
-(void)resetRefreshViewFrame;
- (void)updateSeachBar;
@end


