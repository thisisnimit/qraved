//
//  SavedNoDataView.h
//  Qraved
//
//  Created by harry on 2017/5/23.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SavedNoDataView : UIView

- (instancetype)initWithFrame:(CGRect)frame;
- (void)setNoDataViewWithImage:(UIImage *)image andName:(NSString *)name andTitle:(NSString *)title andButtonTitle:(NSString *)buttonTitle;
@end
