//
//  ReviewPublishButtonView.h
//  Qraved
//
//  Created by apple on 17/4/1.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ReviewPublishButtonViewDelegate <NSObject>

-(void)addPhotoClick;

@end

@interface ReviewPublishButtonView : UIView

@property(nonatomic,strong)UILabel *buttonCharNoticeLabel;
@property(nonatomic,weak)id<ReviewPublishButtonViewDelegate>delegate;
@end
