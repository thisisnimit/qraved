//
//  ScaleImageView.m
//  Qraved
//
//  Created by Admin on 8/28/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "ScaleImageView.h"
 
#import "UIImage+Resize.h"

@interface ScaleImageView () {
    UIScrollView *scrView;
    UIImageView *imageView;
    UIImage *loadImage;
    
    BOOL isDoubleTaped;
}
@end

@implementation ScaleImageView


-(id)initWithFrame:(CGRect)frame AndUrlStr:(NSString *)urlStr {
    self = [self initWithFrame:frame];
    if (self) {
//        self.backgroundColor = [UIColor yellowColor];
        scrView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        scrView.delegate = self;
        scrView.showsHorizontalScrollIndicator = NO;
        scrView.showsVerticalScrollIndicator = NO;
        scrView.maximumZoomScale = 2.0;
        scrView.zoomScale = 1.0;
        [self addSubview:scrView];
        
        UIImage *defaultImage = [UIImage imageNamed:@"placeholder"];
        
        imageView = [[UIImageView alloc]init];
        CGFloat targetHeight=(320*defaultImage.size.height)/defaultImage.size.width;
        imageView.frame = CGRectMake(0, 0, 320, targetHeight);
//        [imageView setImage:[[UIImage imageNamed:@"q-135"] imageByScalingAndCroppingForSize:CGSizeMake(320, targetHeight)]];
        CGRect screenRect = [UIScreen mainScreen].bounds;
        [scrView addSubview:imageView];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
           
//            [imageView setImageUrl:urlStr andWidth:imageView.frame.size.width];
        });
//        loadImage = [[UIImage alloc]init];
//        NSURL *url = [NSURL URLWithString:urlStr];
//        SDWebImageManager *manager = [SDWebImageManager sharedManager];
//        UIImage *cachedImage = [manager imageWithURL:url];
//        if (cachedImage) {
//            imageView.image = cachedImage;
//            imageView.frame = CGRectMake(0, 0, 320, (320*cachedImage.size.height)/cachedImage.size.width);
//        } else {
//            [manager downloadWithURL:url delegate:self];
//        }
        imageView.center = CGPointMake(screenRect.size.width/2, (screenRect.size.height - 20)/2);

//        UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(doubleTap)];
//        doubleTapGesture.numberOfTapsRequired = 2;
//        [self addGestureRecognizer:doubleTapGesture];
//        isDoubleTaped = NO;
    }
    return self;
}

#pragma mark -------UIScrollViewDelegate-------

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return imageView;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    imageView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                                      scrollView.contentSize.height * 0.5 + offsetY);
}

#pragma mark --------SDWebImageManagerDelegate-----------

- (void)webImageManager:(SDWebImageManager *)imageManager didFinishWithImage:(UIImage *)image {
    imageView.frame = CGRectMake(0, 0, 320, (320*image.size.height)/image.size.width);
    CGRect screenRect = [UIScreen mainScreen].bounds;
    imageView.center = CGPointMake(screenRect.size.width/2, (screenRect.size.height - 20)/2);
    imageView.image = image;
}


//-(void)doubleTap {
////    NSLog(@"jifjsdidjfids");
//    float frameWidth = self.frame.size.width;
//    float frameHeight = self.frame.size.height;
//    float scrWidth = frameWidth/2;
//    float scrHeight = frameHeight/2;
//    if (isDoubleTaped) {
//        [scrView zoomToRect:CGRectMake(0, 0, frameWidth, frameHeight) animated:YES];
//        isDoubleTaped = NO;
//    } else {
//        [scrView zoomToRect:CGRectMake((frameWidth-scrWidth)/2, (frameHeight-scrHeight)/2, scrWidth, scrHeight) animated:YES];
//        isDoubleTaped = YES;
//    }
//}

-(void)setNoneScale {
//    float frameWidth = self.frame.size.width;
//    float frameHeight = self.frame.size.height;
    [scrView setZoomScale:1.0];
//    isDoubleTaped = NO;

//    [scrView zoomToRect:CGRectMake(0, 0, frameWidth, frameHeight) animated:NO];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
