//
//  ButtonGridView.h
//  Qraved
//
//  Created by Jeff on 8/21/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGTag.h"
#import "IMGDiscover.h"

@interface TagButtonGridView : UIView
{
    UIView *view;
    NSArray *tagArray;
    CGSize sizeFit;
    UIColor *lblBackgroundColor;
    BOOL clickable;
}

@property (nonatomic, strong) UIView *view;
@property (nonatomic, strong) NSArray *tagArray;

- (id)initWithFrame:(CGRect)frame andDiscover:(IMGDiscover *)discover;
- (void)setLabelBackgroundColor:(UIColor *)color;
- (void)setButtonGrids:(NSArray *)array;
- (void)setButtonGrids:(NSArray *)array clickable:(BOOL)pClickable;
- (void)display;
- (CGSize)fittedSize;

@end
