//
//  UILineImageView.h
//  Qraved
//
//  Created by Jeff on 3/11/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILineImageView : UIImageView

+(UIImageView*)initInParent:(UIView*)parent withFrame:(CGRect)frame;

+(UIImageView*)initVerticalLineInParent:(UIView*)parent withFrame:(CGRect)frame;

@end
