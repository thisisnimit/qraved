//
//  UILineImageView.m
//  Qraved
//
//  Created by Jeff on 3/11/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "UILineImageView.h"

@implementation UILineImageView

+(UIImageView*)initInParent:(UIView*)parent withFrame:(CGRect)frame{
    UILineImageView *lineImageView1 = [[UILineImageView alloc] initWithFrame:frame];
    [lineImageView1 setImage:[UIImage imageNamed:@"search_line"]];
    [parent addSubview:lineImageView1];
    return lineImageView1;
}

+(UIImageView*)initVerticalLineInParent:(UIView*)parent withFrame:(CGRect)frame{
    UILineImageView *lineImageView1 = [[UILineImageView alloc] initWithFrame:frame];
    [lineImageView1 setImage:[UIImage imageNamed:@"search_line"]];
    [parent addSubview:lineImageView1];
    return lineImageView1;
}
@end
