//
//  LikeCommentShareView.m
//  Qraved
//
//  Created by Jeff on 3/3/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "UILikeCommentShareView.h"
#import "UILikeButton.h"
#import "UICommentButton.h"
#import "UIShareButton.h"
#import "UIViewPreviousCommentsButton.h"
#import "UIView+Helper.h"
#import "UICommentInputView.h"
#import "UICommentCell.h"
#import "IMGJournalComment.h"
#import "UILineImageView.h"
#import "AppDelegate.h"

#define VPCBUTTONHEIGHT 46

@implementation UILikeCommentShareView{
    BOOL moreComments;
    BOOL liked;
    BOOL withFooter;
    UIView *likeCommentShareButtonView;
    UIViewPreviousCommentsButton *viewPreviousCommentsButton;
    UITableView *commentsTableView;
    int commentCurrentNumbers;
    int commentCount;
    NSMutableArray *commentList;
    
    UILikeButton *likeButton;
    UICommentButton *commentButton;
    UIShareButton *shareButton;
    UICommentInputView *commentInputView;
    UIImageView *commentsTableImageView;
    
}
@synthesize likeCommentShare;
-(id)initInSuperView:(UIView*)parentView atStartX:(CGFloat)X startY:(CGFloat)Y commentCount:(int)commentCount_ commentArray:(NSArray *)commentList_ liked:(BOOL)liked_ withFooter:(BOOL)withFooter_ needCommentListView:(BOOL)needCommentList{
    self = [super initWithFrame:CGRectMake(X, Y, DeviceWidth, DeviceHeight-200)];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [parentView addSubview:self];
        
        liked=liked_;
        withFooter=withFooter_;
        
        commentList = [[NSMutableArray alloc]initWithArray:commentList_];
        commentCurrentNumbers=(int)commentList.count;
        commentCount=commentCount_;
        moreComments=commentCount>commentCurrentNumbers;
        
        CGFloat tableViewHeight;
        if(needCommentList){
            tableViewHeight = [UILikeCommentShareView calculatedTableViewHeight:commentList withFooter:withFooter];
        }else{
            tableViewHeight = 0;
        }

        self.frame=CGRectMake(X, Y, DeviceWidth, tableViewHeight+COMMUNITYBUTTONVIEWHEIGHT+((moreComments&&needCommentList)?VPCBUTTONHEIGHT:0));
        
        
        likeCommentShareButtonView = [[UIView alloc]initWithFrame:CGRectMake(0, 5, DeviceWidth, COMMUNITYBUTTONVIEWHEIGHT)];
        likeCommentShareButtonView.backgroundColor = [UIColor whiteColor];
        [self addSubview:likeCommentShareButtonView];
        [UILineImageView initInParent:self withFrame:CGRectMake(0, 0, DeviceWidth, 1)];
        
        likeButton = [[UILikeButton alloc] initInSuperView:likeCommentShareButtonView liked:liked];
        likeButton.communityDelegate = self;
        
        
        commentButton = [[UICommentButton alloc] initInSuperView:likeCommentShareButtonView];
        commentButton.communityDelegate = self;

        
        shareButton = [[UIShareButton alloc] initInSuperView:likeCommentShareButtonView];
        shareButton.communityDelegate = self;
       

        if(needCommentList){
        
            viewPreviousCommentsButton = [[UIViewPreviousCommentsButton alloc]initWithFrame:CGRectMake(0, COMMUNITYBUTTONVIEWHEIGHT, DeviceWidth, VPCBUTTONHEIGHT) hidden:!moreComments];
            [viewPreviousCommentsButton addTarget:self action:@selector(viewPreviousComments:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:viewPreviousCommentsButton];
//            [UILineImageView initInParent:self withFrame:CGRectMake(0, viewPreviousCommentsButton.startPointY, DeviceWidth, 1)];
        
        
            commentsTableView = [[UITableView alloc]initWithFrame:CGRectMake(0,moreComments?viewPreviousCommentsButton.endPointY:likeCommentShareButtonView.endPointY,DeviceWidth, tableViewHeight) style:UITableViewStylePlain];
            commentsTableView.dataSource = self;
            commentsTableView.delegate = self;
            commentsTableView.scrollEnabled=NO;
            commentsTableView.scrollsToTop = NO;
            [commentsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
            [self addSubview:commentsTableView];
//            commentsTableImageView=[UILineImageView initInParent:self withFrame:CGRectMake(0, commentsTableView.startPointY, DeviceWidth, 1)];
            
            commentInputView=[[UICommentInputView alloc]initView:YES];
            commentInputView.commentInputViewDelegate=self;
        }
    }
    return self;
}


-(id)initInSuperView:(UIView*)parentView atStartX:(CGFloat)X startY:(CGFloat)Y commentCount:(int)commentCount_ commentArray:(NSArray *)commentList_ liked:(BOOL)liked_ withFooter:(BOOL)withFooter_ needCommentListView:(BOOL)needCommentList likeCount:(int)likeCount{
    self = [super initWithFrame:CGRectMake(X, Y, DeviceWidth, DeviceHeight-200)];
    if (self) {
        [self setBackgroundColor:[UIColor whiteColor]];
        [parentView addSubview:self];
        
        liked=liked_;
        withFooter=withFooter_;
        
        commentList = [[NSMutableArray alloc]initWithArray:commentList_];
        commentCurrentNumbers=(int)commentList.count;
        commentCount=commentCount_;
        moreComments=commentCount>commentCurrentNumbers;
        
        CGFloat tableViewHeight;
        if(needCommentList){
            tableViewHeight = [UILikeCommentShareView calculatedTableViewHeight:commentList withFooter:withFooter];
        }else{
            tableViewHeight = 0;
        }
        
        self.frame=CGRectMake(X, Y, DeviceWidth, tableViewHeight+COMMUNITYBUTTONVIEWHEIGHT+((moreComments&&needCommentList)?VPCBUTTONHEIGHT:0));
        
        
        likeCommentShareButtonView = [[UIView alloc]initWithFrame:CGRectMake(0, 5, DeviceWidth, COMMUNITYBUTTONVIEWHEIGHT)];
        likeCommentShareButtonView.backgroundColor = [UIColor whiteColor];
        [self addSubview:likeCommentShareButtonView];
      
        likeCommentShare = [[V2_LikeCommentShareView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, COMMUNITYBUTTONVIEWHEIGHT)];
        likeCommentShare.delegate = self;
        [likeCommentShareButtonView addSubview:likeCommentShare];
        [UILineImageView initInParent:self withFrame:CGRectMake(0, 0, DeviceWidth, 1)];
        
        [likeCommentShare setCommentCount:commentCount_ likeCount:likeCount liked:liked_];
        
        UIButton *btnLikeList = [UIButton buttonWithType:UIButtonTypeCustom];
        btnLikeList.frame = CGRectMake(DeviceWidth/3 - 60, 0, 60, 56);
        [btnLikeList addTarget:self action:@selector(likeLickClick) forControlEvents:UIControlEventTouchUpInside];
        [likeCommentShare addSubview:btnLikeList];
        
        if(needCommentList){
            
            viewPreviousCommentsButton = [[UIViewPreviousCommentsButton alloc]initWithFrame:CGRectMake(0, COMMUNITYBUTTONVIEWHEIGHT, DeviceWidth, VPCBUTTONHEIGHT) hidden:!moreComments];
            [viewPreviousCommentsButton addTarget:self action:@selector(viewPreviousComments:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:viewPreviousCommentsButton];
            //            [UILineImageView initInParent:self withFrame:CGRectMake(0, viewPreviousCommentsButton.startPointY, DeviceWidth, 1)];
            
            
            commentsTableView = [[UITableView alloc]initWithFrame:CGRectMake(0,moreComments?viewPreviousCommentsButton.endPointY:likeCommentShareButtonView.endPointY,DeviceWidth, tableViewHeight) style:UITableViewStylePlain];
            commentsTableView.dataSource = self;
            commentsTableView.delegate = self;
            commentsTableView.scrollEnabled=NO;
            commentsTableView.scrollsToTop = NO;
            [commentsTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
            [self addSubview:commentsTableView];
            //            commentsTableImageView=[UILineImageView initInParent:self withFrame:CGRectMake(0, commentsTableView.startPointY, DeviceWidth, 1)];
            
            commentInputView=[[UICommentInputView alloc]initView:YES];
            commentInputView.commentInputViewDelegate=self;
        }
    }
    return self;

}
- (void)likeLickClick{
    [self.likeCommentShareDelegate likeListButtonTapped];
}
-(void)addPreviousComments:(NSArray *)commentList_{
//    int previousCommentIndex = commentCurrentNumbers;
    for(NSInteger i=0;i<=commentList_.count-1;i++){
        [commentList addObject:[commentList_ objectAtIndex:i]];
        commentCurrentNumbers++;
    }
    [self updateUI];
    [commentsTableView reloadData];
}

-(void)addNewComment:(id)comment{
    commentCount++;
    commentCurrentNumbers++;
    [commentList insertObject:comment atIndex:0];
    [self updateUI];
    [commentsTableView reloadData];
}
-(void)addNewCommentAndRemoveOldOne:(id)comment{
    commentCount++;
    [commentList insertObject:comment atIndex:0];
    [commentList removeObjectAtIndex:(commentList.count-1)];
    [self updateUI];
    [commentsTableView reloadData];
}
-(void)updateUI{
    moreComments=commentCount>commentCurrentNumbers;
    CGFloat tableViewHeight = [UILikeCommentShareView calculatedTableViewHeight:commentList withFooter:withFooter];
    self.frame=CGRectMake(self.startPointX, self.startPointY, DeviceWidth, tableViewHeight+COMMUNITYBUTTONVIEWHEIGHT+5+(moreComments?VPCBUTTONHEIGHT:0));
    commentsTableView.frame=CGRectMake(0,moreComments?viewPreviousCommentsButton.endPointY:likeCommentShareButtonView.endPointY,DeviceWidth, tableViewHeight);
    viewPreviousCommentsButton.hidden=!moreComments;
    if(!moreComments){
        [commentsTableImageView removeFromSuperview];
    }
}

+(CGFloat)calculatedTableViewHeight:(NSArray*)commentList_ withFooter:(BOOL)withFooter_{
    CGFloat height=0.0f;
    for(NSInteger i=commentList_.count-1;i>=0;i--){
        height += [UICommentCell calculatedHeight:[commentList_ objectAtIndex:i]];
    }
    CGFloat footHeight = withFooter_?UICOMMENTINPUTVIEWHEIGHT:40;
    height+=footHeight;
    return height;
}

+(CGFloat)calculatedSelfViewHeight:(NSArray*)commentList_ withFooter:(BOOL)withFooter_ withMoreComments:(BOOL)moreComments_{
    CGFloat height=[UILikeCommentShareView calculatedTableViewHeight:commentList_ withFooter:withFooter_];
    height+=VPCBUTTONHEIGHT*moreComments_+COMMUNITYBUTTONVIEWHEIGHT+5;
    return height;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return commentCurrentNumbers;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UICommentCell *commentCell = [tableView dequeueReusableCellWithIdentifier:@"CellID"];
    if (commentCell == nil) {
        commentCell = [[UICommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CellID"];
        commentCell.commentCellDelegate=self;
        
    }

    id comment = [commentList objectAtIndex:(commentList.count-1-indexPath.row)];
    [commentCell updateUI:comment];

    return commentCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [UICommentCell calculatedHeight:[commentList objectAtIndex:commentList.count-1-indexPath.row]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return withFooter?UICOMMENTINPUTVIEWHEIGHT:40;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
    view.backgroundColor = [UIColor whiteColor];
    return withFooter?commentInputView:view;
}

-(void)viewPreviousComments:(UIButton *)button{
    if(self.likeCommentShareDelegate){
        if(commentList.count>0){
            id comment = [commentList objectAtIndex:commentList.count-1];
            if(comment){
                [self.likeCommentShareDelegate likeCommentShareView:self viewPreviousCommentsTapped:viewPreviousCommentsButton  lastComment:comment];
            }
        }
        
    }
}

- (void)likeCommentShareView:(UIView *)likeCommentShareView likeButtonTapped:(UIButton *)button{
    IMGUser *currentUser = [IMGUser currentUser];
    if (currentUser.userId == nil || currentUser.token == nil){
        [[AppDelegate ShareApp] goToLoginControllerByDelegate:self dictionary:@{@"likeCommentShareButton":button}];
        return;
    }
    [self.likeCommentShareDelegate likeCommentShareView:self likeButtonTapped:button];
}

- (void)likeCommentShareView:(UIView *)likeCommentShareView commentButtonTapped:(UIButton *)button{
    if(withFooter){
        [commentInputView becomeFirstResponderToInputTextView];
    }
    [self.likeCommentShareDelegate likeCommentShareView:self commentButtonTapped:button];
}

- (void)likeCommentShareView:(UIView *)likeCommentShareView shareButtonTapped:(UIButton *)button{
    [self.likeCommentShareDelegate likeCommentShareView:self shareButtonTapped:button];
}
- (void)communityButtonTapped:(UIButton *)button{
    if(button.tag==0){
        IMGUser *currentUser = [IMGUser currentUser];
        if (currentUser.userId == nil || currentUser.token == nil){
            [[AppDelegate ShareApp] goToLoginControllerByDelegate:self dictionary:@{@"likeCommentShareButton":button}];
            return;
        }
        [self.likeCommentShareDelegate likeCommentShareView:self likeButtonTapped:button];
    }else if(button.tag==1){
        if(withFooter){
            [commentInputView becomeFirstResponderToInputTextView];
        }
        [self.likeCommentShareDelegate likeCommentShareView:self commentButtonTapped:button];
    }else if(button.tag==2){
        [self.likeCommentShareDelegate likeCommentShareView:self shareButtonTapped:button];
    }
}

-(void)updateLikedStatus{
    liked=!liked;
    [likeButton updateLikedStatus];
}

- (void)updateLikeButtonStatus:(BOOL)liked_ likeCount:(int)likeCount{
    [likeCommentShare setCommentCount:commentCount likeCount:likeCount liked:liked_];
}

-(void)commentInputView:(UIView *)view inputViewExpand:(UITextView *)inputTextView commentInputViewYOffset:(CGFloat)commentInputViewYOffset{
    if(self.likeCommentShareDelegate){
        [self.likeCommentShareDelegate likeCommentShareView:self commentInputView:view inputViewExpand:inputTextView commentInputViewYOffset:commentInputViewYOffset];
    }
}

-(void)commentInputView:(UIView *)view postButtonTapped:(UIButton *)postButton commentInputText:(NSString *)text{
    text = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    text= [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];

    if(self.likeCommentShareDelegate&&![self isEmpty:text]){
        [self.likeCommentShareDelegate likeCommentShareView:self commentInputView:view postButtonTapped:postButton commentInputText:text];
    }
}

-(void)commentInputView:(UIView *)view inputViewDidBeginEditing:(UITextView *)textView{

    if(self.likeCommentShareDelegate){
        [self.likeCommentShareDelegate likeCommentShareView:self commentInputView:view inputViewDidBeginEditing:textView];
    }
}

- (void)UICommentCell:(UITableViewCell*)tableViewCell readMoreTapped:(id)comment{
    CGFloat height1 = tableViewCell.frame.size.height;
    NSIndexPath *indexPath = [commentsTableView indexPathForCell:tableViewCell];
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            if ([[commentsTableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                return;
            }
            [commentsTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        
        @catch ( NSException *e ) {
            return;
        }
    });
    UITableViewCell *cell2 = [commentsTableView cellForRowAtIndexPath:indexPath];
    CGFloat height2 = cell2.frame.size.height;
    CGFloat offset = height2-height1;
    self.frame=CGRectMake(self.startPointX, self.startPointY, self.frame.size.width, self.frame.size.height+offset);
    commentsTableView.frame=CGRectMake(commentsTableView.startPointX, commentsTableView.startPointY, commentsTableView.frame.size.width, commentsTableView.frame.size.height+offset);
    if(self.likeCommentShareDelegate){
        [self.likeCommentShareDelegate likeCommentShareView:self commentCell:tableViewCell readMoreTapped:comment offset:offset];
    }
}
- (void)UICommentCellUserNameOrImageTapped:(id)sender
{
    if (self.likeCommentShareDelegate)
    {
        [self.likeCommentShareDelegate likeCommentShareViewUserNameOrImageTapped:sender];
    }
}
- (BOOL) isEmpty:(NSString *) str {
    
    if(!str) {
        
        return true;
        
    }else {
        
        
        //A character set containing only the whitespace characters space (U+0020) and tab (U+0009) and the newline and next line characters (U+000A–U+000D,U+0085).
        
        
        NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        
        
        //Returns a new string made by removing from both ends of the receiver characters contained in a given character set.
        
        
        NSString *trimedString = [str stringByTrimmingCharactersInSet:set];
        
        
        if([trimedString length] == 0) {
            
            return true;
            
            
        }else {
            
            
            return false;
            
            
        }
        
    }
    
    
}

@end
