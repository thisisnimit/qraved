//
//  CheckableTableViewCell.m
//  Qraved
//
//  Created by Jeff on 8/22/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "CheckableTableViewCell.h"
#import "UIColor+Helper.h"

@interface CheckableTableViewCell()
@end

@implementation CheckableTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style selectionStyle:(UITableViewCellSelectionStyle)selectionStyle reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setSelectionStyle:selectionStyle];
        
        self.checkView = [[UIView alloc] init];
        [self.checkView setFrame:CGRectMake(DeviceWidth-40, 0, 20, 20)];
        [self addSubview:self.checkView];
        
        UIImageView *celllineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.frame.origin.y+self.frame.size.height-1, DeviceWidth, 1)];
        [celllineImageView setImage:[UIImage imageNamed:@"search_line"] ];
        [self addSubview:celllineImageView];
        
        self.checkImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 20, 20)];
        self.checked = FALSE;
    }
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style selectionStyle:(UITableViewCellSelectionStyle)selectionStyle reuseIdentifier:(NSString *)reuseIdentifier withFrame:(CGRect)tmpFrame
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setSelectionStyle:selectionStyle];
        [self setFrame:tmpFrame];
        
        self.checkView = [[UIView alloc] init];
        [self.checkView setFrame:CGRectMake(DeviceHeight-40, 0, 20, 20)];
        [self addSubview:self.checkView];
        
        UIImageView *celllineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.frame.origin.y+self.frame.size.height-1, DeviceWidth, 1)];
        [celllineImageView setImage:[UIImage imageNamed:@"search_line"] ];
        [self addSubview:celllineImageView];
        
        self.checkImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 20, 20)];
        self.checked = FALSE;
    }
    return self;
}

- (void)setText:(NSString *)title {
    if(self.titleLabel==nil){
        self.titleLabel = [[SearchCellLabel alloc] initWithBlackColorAndFrame:CGRectMake(40, 0, 280, self.frame.size.height)];
        [self addSubview:self.titleLabel];
    }
    [self.titleLabel setText:title];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)uncheck
{
    if(_checked){
        _checked = FALSE;
        for (UIView *view in self.checkView.subviews) {
            if ([view isKindOfClass:[UILabel class]]==NO){
                [view removeFromSuperview];
            }
        }
    }
    [self.titleLabel setTextColor:[UIColor color222222]];
    [UIView commitAnimations];
}

- (void)check
{
    _checked = TRUE;
    UIImage *checkImage = [UIImage imageNamed:@"checked.png"];
    [self.checkImageView setImage:checkImage];
    [self.checkView addSubview:self.checkImageView];
    [self.titleLabel setTextColor:[UIColor color3BAF24]];
    [UIView commitAnimations];
}
-(void)setChecked:(BOOL)checked{
    checked?[self check]:[self uncheck];
}
- (BOOL)isChecked{
    return self.checked;
}

- (void)click
{
    self.checked=!self.checked;
    [UIView commitAnimations];
}


@end
