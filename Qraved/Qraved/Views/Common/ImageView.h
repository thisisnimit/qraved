//
//  ImageView.h
//  Qraved
//
//  Created by Shine Wang on 8/22/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "UIConstants.h"

@interface ImageView : UIImageView

@property(nonatomic,assign)CGFloat width;
@property(nonatomic,assign)CGFloat height;

-(ImageView *)initBigAvatar;
-(ImageView *)initMiddleAvatar;
-(ImageView *)initBigCommentAvatar;
-(ImageView *)initSmallCommentAvatar;
-(ImageView *)initSmallAvatar;

-(ImageView *)initMenuImage;
-(ImageView *)initScrollImage;

-(ImageView *)initPalaceholder;

@end
