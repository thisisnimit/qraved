//
//  CommunityButton.m
//  Qraved
//
//  Created by Jeff on 3/3/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "UICommunityButton.h"
#import "UIConstants.h"

@implementation UICommunityButton

-(id)initInSuperView:(UIView*)parentView{
    return [self initInSuperView:parentView tag:0];
}

-(id)initInSuperView:(UIView*)parentView tag:(int)tag{
    self = [super initWithFrame:CGRectMake(tag*DeviceWidth/3, 0, DeviceWidth/3, parentView.frame.size.height)];
    if (self) {
        self.tag = tag;
        [self setBackgroundColor:[UIColor whiteColor]];
        [parentView addSubview:self];
        
        self.textLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        self.textLabel.font = [UIFont systemFontOfSize:12];
        [self.textLabel setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:self.textLabel];
    }
    [self addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    return self;
}

-(void)buttonTapped:(UIButton*)button{
    if(self.communityDelegate){
        [self.communityDelegate communityButtonTapped:button];
    }
}

@end
