//
//  UIImage+ImageBlur.h
//  Qraved
//
//  Created by Libo Liu on 9/29/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ImageBlur)
- (UIImage *)imageWithGaussianBlur9;
@end
