//
//  Button.m
//  Qraved
//
//  Created by Shine Wang on 8/6/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "Button.h"
#import "UIConstants.h"

@implementation Button
@synthesize isChecked;
-(Button *)initWithPoint:(CGPoint )point andImage:(UIImage *)image andTitle:(NSString *)title andFont:(UIFont *)font andTitleColor:(UIColor *)color
{

    self=[super init];
    if(self)
    {
        self.isChecked=YES;
        self.frame=CGRectMake(point.x, point.y, image.size.width, image.size.height);
        [self setBackgroundImage:image forState:UIControlStateNormal];
        self.titleLabel.font=font;
        [self setTitleColor:color forState:UIControlStateNormal];
        [self setTitle:title forState:UIControlStateNormal];
    }
    
    return self;
}
-(UIButton *)initDefaultButton
{
    self=[super init];
    if(self)
    {
        
    }
    return self;
}


@end
