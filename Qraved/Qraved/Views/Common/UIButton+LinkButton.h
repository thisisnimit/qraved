//
//  UIButton+LinkButton.h
//  Qraved
//
//  Created by Shine Wang on 9/6/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDImageCache.h"

@interface UIButton (LinkButton)

-(void)setBackgroundImageUrl:(NSString *)urlString;

-(void)setBackgroundImageUrl:(NSString *)urlString andWidth:(CGFloat)width;
@end
