//
//  NodataView.h
//  Qraved
//
//  Created by Shine Wang on 9/13/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NodataView : UIView
@property(nonatomic,assign)CGFloat width;
@property(nonatomic,assign)CGFloat height;
- (id)initWithFrame:(CGRect)frame andMessage:(NSString *)message;

-(id)initWIthMessage:(NSString *)message;
@end
