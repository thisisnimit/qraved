//
//  MapButton.m
//  Qraved
//
//  Created by harry on 2017/12/6.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "MapButton.h"

@implementation MapButton

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
        self.titleLabel.font = [UIFont boldSystemFontOfSize:15];
        
        [self setImageEdgeInsets:UIEdgeInsetsMake(0, 60, 0, 0)];
        [self setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 40)];
        [self setImage:[UIImage imageNamed:@"ic_map"] forState:UIControlStateNormal];
        [self setImage:[UIImage imageNamed:@"ic_list_view"] forState:UIControlStateSelected];
        [self setTitle:@"LIST" forState:UIControlStateSelected];
        [self setTitle:@"MAP" forState:UIControlStateNormal];
        self.layer.cornerRadius = 20;
        self.layer.masksToBounds = YES;
        self.layer.borderWidth = 0.5;
        self.layer.borderColor = [UIColor color999999].CGColor;
    }
    return self;
}

@end
