//
//  BottomButton.h
//  Qraved
//
//  Created by Jeff on 8/28/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BottomButton : UIButton

- (id)initWithFrame:(CGRect)frame title:(NSString *)title;

@end
