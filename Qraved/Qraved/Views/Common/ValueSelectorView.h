//
//  ValueSelectorView.h
//  Qraved
//
//  Created by Jeff on 8/7/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//


#import <UIKit/UIKit.h>
@class ValueSelectorView;

@protocol ValueSelectorViewDelegate <NSObject>

- (void)selector:(ValueSelectorView *)valueSelector didSelectRowAtIndex:(NSInteger)index;

@end

@protocol ValueSelectorViewDataSource <NSObject>
- (NSInteger)numberOfRowsInSelector:(ValueSelectorView *)valueSelector;
- (UIView *)selector:(ValueSelectorView *)valueSelector viewForRowAtIndex:(NSInteger)index;
- (CGRect)rectForSelectionInSelector:(ValueSelectorView *)valueSelector;
- (CGFloat)rowHeightInSelector:(ValueSelectorView *)valueSelector;
- (CGFloat)rowWidthInSelector:(ValueSelectorView *)valueSelector;
@optional
- (UIView *)selector:(ValueSelectorView *)valueSelector viewForRowAtIndex:(NSInteger)index selected:(BOOL)selected;
@end



@interface ValueSelectorView : UIView <UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, assign) IBOutlet NSObject<ValueSelectorViewDelegate> *delegate;
@property (nonatomic, assign) IBOutlet NSObject<ValueSelectorViewDataSource> *dataSource;
@property (nonatomic, assign) BOOL shouldBeTransparent;
@property (nonatomic, assign) BOOL horizontalScrolling;
@property (nonatomic, strong) NSString *selectedImageName;
@property (nonatomic, assign) BOOL debugEnabled;
@property (nonatomic, assign) BOOL decelerates;
@property (nonatomic, assign) BOOL isCanScroll;

- (void)selectRowAtIndex:(NSUInteger)index;
- (void)selectRowAtIndex:(NSUInteger)index animated:(BOOL)animated;

- (void)reloadData;

@end
