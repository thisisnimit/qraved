//
//  CommunityButton.h
//  Qraved
//
//  Created by Jeff on 3/3/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

#define COMMUNITYBUTTONBACKGROUNDCOLOR [UIColor colorWithRed:251/255.0f green:252/255.0f blue:255/255.0f alpha:1]
#define COMMUNITYBUTTONUNTAPPEDTITLECOLOR       [UIColor color999999]
#define COMMUNITYBUTTONICONSIZE 13.5
#define COMMUNITYBUTTONGAP 7

@protocol CommunityButtonTappedDelegate <NSObject>

- (void)communityButtonTapped:(UIButton *)button;

@end

@interface UICommunityButton : UIButton

-(id)initInSuperView:(UIView*)parentView;
-(id)initInSuperView:(UIView*)parentView tag:(int)tag;
@property (nonatomic, assign) id<CommunityButtonTappedDelegate>communityDelegate;
@property (nonatomic, retain)UILabel *textLabel;
@property (nonatomic, retain)UIImageView *iconView;

-(void)buttonTapped:(UIButton*)button;

@end
