//
//  UIImageButton.h
//  Qraved
//
//  Created by Jeff on 8/5/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageButton : UIButton

@property(nonatomic,assign)int row;//第几行
@property(nonatomic,assign)int column;//第几列
@end
