//
//  ButtonGridView.m
//  Qraved
//
//  Created by Jeff on 8/21/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "TagButtonGridView.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+Helper.h"
#import "GreatforButton.h"
#import "UIConstants.h"
#import "NotificationConsts.h"

#define CORNER_RADIUS 13.0f
#define BUTTON_MARGIN 5.0f
#define BOTTOM_MARGIN 7.0f
#define FONT_SIZE 12.0f
#define HORIZONTAL_PADDING 7.0f
#define VERTICAL_PADDING 5.0f
#define BACKGROUND_COLOR [UIColor colorWithRed:0.93 green:0.93 blue:0.93 alpha:1.00]
#define TEXT_COLOR [UIColor color333333]
#define TEXT_SHADOW_COLOR [UIColor whiteColor]
#define TEXT_SHADOW_OFFSET CGSizeMake(0.0f, 1.0f)
#define BORDER_COLOR [UIColor colorEEEEEE].CGColor
#define BORDER_WIDTH 1.0f

@implementation TagButtonGridView
{
    IMGDiscover *_discover;
}
@synthesize view, tagArray;

- (id)initWithFrame:(CGRect)frame andDiscover:(IMGDiscover *)discover
{
    self = [super initWithFrame:frame];
    if (self) {
        _discover = discover;
        if(_discover==nil){
            _discover = [[IMGDiscover alloc]init];
        }
        [self addSubview:view];
    }
    return self;
}

- (void)setButtonGrids:(NSArray *)array
{
    [self setButtonGrids:array clickable:TRUE];
}

- (void)setButtonGrids:(NSArray *)array clickable:(BOOL)pClickable
{
    tagArray = [[NSArray alloc] initWithArray:array];
    sizeFit = CGSizeZero;
    clickable = pClickable;
    [self display];
}

- (void)setLabelBackgroundColor:(UIColor *)color
{
    lblBackgroundColor = color;
    [self display];
}

- (void)display
{
    UIFont *font=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:FONT_SIZE];
    for (UIButton *subview in [self subviews]) {
        [subview removeFromSuperview];
    }
    float totalHeight = 0;
    CGRect previousFrame = CGRectZero;
    BOOL gotPreviousFrame = NO;
    for (IMGTag *tag in tagArray) {
        CGSize textSize = [tag.name sizeWithFont:font constrainedToSize:CGSizeMake(self.frame.size.width, 1500) lineBreakMode:NSLineBreakByWordWrapping];
        textSize.width += HORIZONTAL_PADDING*2;
        textSize.height += VERTICAL_PADDING*2;
        GreatforButton *button = nil;
        if (!gotPreviousFrame) {
            button = [[GreatforButton alloc] initWithFrame:CGRectMake(0, 0, textSize.width, 24)];
            totalHeight = 24;
        } else {
            CGRect newRect = CGRectZero;
            if (previousFrame.origin.x + previousFrame.size.width + textSize.width + BUTTON_MARGIN > self.frame.size.width) {
                newRect.origin = CGPointMake(0, previousFrame.origin.y + textSize.height + BOTTOM_MARGIN);
                totalHeight += 24 + BOTTOM_MARGIN;
            } else {
                newRect.origin = CGPointMake(previousFrame.origin.x + previousFrame.size.width + BUTTON_MARGIN, previousFrame.origin.y);
            }
            newRect.size = textSize;
            button = [[GreatforButton alloc] initWithFrame:newRect];
        }
        previousFrame = button.frame;
        gotPreviousFrame = YES;
        //        [label setFont:[UIFont systemFontOfSize:FONT_SIZE]];
        if (!lblBackgroundColor) {
            [button setBackgroundColor:[UIColor whiteColor]];
        } else {
            [button setBackgroundColor:[UIColor colorC2060A]];
        }
        //        [label setTextColor:TEXT_COLOR];
        //        [label setText:text];
        //        [label setTextAlignment:UITextAlignmentCenter];
        //        [label setShadowColor:TEXT_SHADOW_COLOR];
        //        [label setShadowOffset:TEXT_SHADOW_OFFSET];
        [button setTitle:tag.name forState:UIControlStateNormal];
        button.tag = [tag.tagId intValue];
        button.occasion = tag;
        [button.titleLabel setFont:font];
        [button setTitleColor:TEXT_COLOR forState:UIControlStateNormal];
        [button.layer setMasksToBounds:YES];
        [button.layer setCornerRadius:CORNER_RADIUS];
        [button.layer setBorderColor:BORDER_COLOR];
        [button.layer setBorderWidth: BORDER_WIDTH];
        if([_discover hasOccasion:tag]){
            [button check];
        }
        [button addTarget:self action:@selector(check:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:button];
    }
    sizeFit = CGSizeMake(self.frame.size.width, totalHeight + 1.0f);
}

- (void)check:(id)sender{
    if(clickable){
        GreatforButton *button = (GreatforButton *)sender;
        [button check];
        if(button.checked){
            [_discover addOccasion:button.occasion];
        }else{
            [_discover removeOccasion:button.occasion];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:SEARCH_FILTER object:nil userInfo:@{@"discover":_discover}];
    }
}

- (CGSize)fittedSize
{
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, sizeFit.height)];
    return sizeFit;
}

@end
