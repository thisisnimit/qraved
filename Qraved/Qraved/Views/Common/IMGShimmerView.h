//
//  IMGShimmerView.h
//  Qraved
//
//  Created by harry on 2018/3/28.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IMGShimmerView : UIView

- (void)createJournalListShimmerView;
- (void)createJDPShimmerView;
- (void)createBrandAndMallShimmerView;
- (void)createRestaurantShimmerView;
- (void)createNotificationShimmerView;
- (void)createPhotoShimmerView;
- (void)createGuideListShimmerView;
@end
