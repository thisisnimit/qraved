//
//  BottomButton.m
//  Qraved
//
//  Created by Jeff on 8/28/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "BottomButton.h"
#import "UIConstants.h"
#import "UIColor+Helper.h"

@implementation BottomButton

- (id)initWithFrame:(CGRect)frame title:(NSString *)title
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setTitle:title forState:UIControlStateNormal];
        self.titleLabel.font = [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:13];
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self setBackgroundImage:[UIImage imageNamed:@"SendBtn"] forState:UIControlStateNormal];
        
    }
    return self;
}


@end
