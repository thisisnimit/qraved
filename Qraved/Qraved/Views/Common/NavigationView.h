//
//  NavigationView.h
//  Qraved
//
//  Created by Laura on 14-9-17.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationView : UIView

- (id)initBackButton;

@end
