//
//  Button.h
//  Qraved
//
//  Created by Shine Wang on 8/6/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Button : UIButton
-(Button *)initWithPoint:(CGPoint )point andImage:(UIImage *)image andTitle:(NSString *)title andFont:(UIFont *)font andTitleColor:(UIColor *)color;

@property BOOL isChecked;
@end
