//
//  UIButton+LinkButton.m
//  Qraved
//
//  Created by Shine Wang on 9/6/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "UIButton+LinkButton.h"
#import "UIImage+Resize.h"
#import "Tools.h"

@implementation UIButton (LinkButton)
-(void)setBackgroundImageUrl:(NSString *)urlString;
{
//    UIImage *cachedImage=[[SDImageCache sharedImageCache] imageFromKey:urlString fromDisk:YES];
    UIImage *cachedImage=[[SDImageCache sharedImageCache] imageFromDiskCacheForKey:urlString];
    if(cachedImage)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            CGSize targetSize = CGSizeMake(self.frame.size.width, self.frame.size.height);
            UIImage *newImage = [cachedImage imageByScalingAndCroppingForSize:(CGSize)targetSize];
            
            [self setBackgroundImage:newImage forState:UIControlStateNormal];
        });
    }
    else
    {
        NSURL *url=[NSURL URLWithString:urlString];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData *imageData=[NSData dataWithContentsOfURL:url];
        if(imageData!=nil)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIImage *image=[UIImage imageWithData:imageData];
                CGSize targetSize = CGSizeMake(self.frame.size.width, self.frame.size.height);
                UIImage *newImage = [image imageByScalingAndCroppingForSize:(CGSize)targetSize];

                [self setBackgroundImage:newImage forState:UIControlStateNormal];
//                [[SDImageCache sharedImageCache] storeImage:image imageData:imageData forKey:urlString toDisk:YES];
                [[SDImageCache sharedImageCache] storeImage:image forKey:urlString toDisk:YES];
            });
        }
        });
    }
}


-(void)setBackgroundImageUrl:(NSString *)urlString andWidth:(CGFloat)width
{
    CGFloat realWidth=width;

    if([Tools isRetinaScreen])
    {
        realWidth=width*2;
    }
    
    NSString* encodedUrl = [urlString stringByAddingPercentEscapesUsingEncoding:
                            NSASCIIStringEncoding];
    if(encodedUrl.length) {
        NSString *fullURLString =[[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:encodedUrl] stringByAppendingFormat:@"&width=%f",realWidth];
        
//        UIImage *cachedImage=[[SDImageCache sharedImageCache] imageFromKey:fullURLString fromDisk:YES];
        UIImage *cachedImage=[[SDImageCache sharedImageCache] imageFromDiskCacheForKey:fullURLString];
        if(cachedImage)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                CGSize targetSize = CGSizeMake(self.frame.size.width, self.frame.size.height);
                UIImage *newImage = [cachedImage imageByScalingAndCroppingForSize:(CGSize)targetSize];
                
                [self setBackgroundImage:newImage forState:UIControlStateNormal];
            });
        }
        
        else
        {
            NSString *testUrlString=[[QRAVED_WEB_IMAGE_SERVER stringByAppendingString:encodedUrl] stringByAppendingFormat:@"&width=1"];
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSData *testData=[NSData dataWithContentsOfURL:[NSURL URLWithString:testUrlString]];
                
                if(testData!=nil)
                {
                    NSURL *url=[NSURL URLWithString:fullURLString];
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        NSData *realData=[NSData dataWithContentsOfURL:url];
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            UIImage *image=[UIImage imageWithData:realData];
                            CGSize targetSize = CGSizeMake(self.frame.size.width, self.frame.size.height);
                            UIImage *newImage = [image imageByScalingAndCroppingForSize:(CGSize)targetSize];
                            
                            [self setBackgroundImage:newImage forState:UIControlStateNormal];
                            
//                            [[SDImageCache sharedImageCache] storeImage:newImage imageData:realData forKey:fullURLString toDisk:YES];
                            [[SDImageCache sharedImageCache] storeImage:newImage forKey:fullURLString toDisk:YES];
                        });
                    });
                }
                
            });
        }
    }
   
}

@end
