//
//  RadioButton.h
//  Qraved
//
//  Created by Jeff on 8/8/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//


#import <UIKit/UIKit.h>

@protocol RadioButtonDelegate;

@interface RadioButton : UIButton {
    BOOL                            _checked;
}

@property(nonatomic, assign)id<RadioButtonDelegate>   delegate;
@property(nonatomic, copy, readonly)NSString            *groupId;
@property(nonatomic, assign)BOOL checked;

- (id)initWithDelegate:(id)delegate groupId:(NSString*)groupId;

@end

@protocol RadioButtonDelegate <NSObject>

@optional

- (void)didSelectedRadioButton:(RadioButton *)radio groupId:(NSString *)groupId;

@end
