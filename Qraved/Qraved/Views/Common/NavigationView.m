//
//  NavigationView.m
//  Qraved
//
//  Created by Laura on 14-9-17.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "NavigationView.h"
#import "UIDevice+Util.h"
#import "UIViewController+Helper.h"
#import "UIColor+Helper.h"
@implementation NavigationView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}
- (id)initBackButton
{
    self = [[NavigationView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 44+[UIDevice heightDifference])];
    self.backgroundColor = [UIColor colorC2060A];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setBackgroundImage:[UIImage imageNamed:NavigationBackImage] forState:UIControlStateNormal];
    backButton.frame = CGRectMake(15, 44+[UIDevice heightDifference]-30, 12, 18);
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:backButton];

    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
