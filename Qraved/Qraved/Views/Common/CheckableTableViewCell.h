//
//  CheckableTableViewCell.h
//  Qraved
//
//  Created by Jeff on 8/22/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchCellLabel.h"

@interface CheckableTableViewCell : UITableViewCell

@property(nonatomic,strong) UIImageView *checkImageView;
@property(nonatomic,strong) SearchCellLabel *titleLabel;
@property(nonatomic,assign) BOOL  checked;
@property(nonatomic,strong) UIView *checkView;

- (void)setText:(NSString *)title;
- (void)uncheck;
- (void)check;
- (void)click;
- (BOOL)isChecked;

- (id)initWithStyle:(UITableViewCellStyle)style selectionStyle:(UITableViewCellSelectionStyle)selectionStyle reuseIdentifier:(NSString *)reuseIdentifier;
- (id)initWithStyle:(UITableViewCellStyle)style selectionStyle:(UITableViewCellSelectionStyle)selectionStyle reuseIdentifier:(NSString *)reuseIdentifier withFrame:(CGRect)tmpFrame;

@end
