//
// NavigationBarButtonItem.h
//  Qraved
//
//  Created by jefftang on 8/13/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface NavigationBarButtonItem : UIBarButtonItem {
    UIImage *customImage;
    SEL customAction;
}


//+ (NavigationBarButtonItem *)barItemWithImage:(UIImage*)image andTitle:(NSString *)title target:(id)target action:(SEL)action width:(float)width;

//+ (NavigationBarButtonItem *)barItemWithImage:(UIImage*)image andTitle:(NSString *)title target:(id)target action:(SEL)action width:(float)width offset:(float)offset;
- (id)initWithImage:(UIImage *)image andTitle:(NSString *)title
             target:(id)target action:(SEL)action width:(float)width offset:(float)offset;
- (void)setCustomImage:(UIImage *)image;

- (void)setCustomAction:(SEL)action;

@end
