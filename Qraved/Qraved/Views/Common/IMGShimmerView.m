//
//  IMGShimmerView.m
//  Qraved
//
//  Created by harry on 2018/3/28.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "IMGShimmerView.h"
#import <FBShimmeringView.h>
@implementation IMGShimmerView
{
    UIView *journalView;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        FBShimmeringView *shimmeringView = [[FBShimmeringView alloc] init];
        [self addSubview:shimmeringView];
        
        shimmeringView.shimmering = YES;
        shimmeringView.shimmeringOpacity = 1;
        shimmeringView.shimmeringBeginFadeDuration = 0.2;
        shimmeringView.shimmeringEndFadeDuration = 0;
        shimmeringView.shimmeringPauseDuration = 1.8;
        
        journalView = [[UIView alloc] initWithFrame:self.bounds];
        journalView.backgroundColor = [UIColor whiteColor];
        shimmeringView.contentView = journalView;
    }
    return self;
}

- (void)createJournalListShimmerView;{
    UIView *shimmerView = [[[NSBundle mainBundle] loadNibNamed:@"JournalShimmerView" owner:self options:nil] lastObject];
    shimmerView.frame = self.bounds;
    [journalView addSubview:shimmerView];
}

- (void)createJDPShimmerView{
    UIView *shimmerView = [[[NSBundle mainBundle] loadNibNamed:@"JDPShimmerView" owner:self options:nil] lastObject];
    shimmerView.frame = self.bounds;
    [journalView addSubview:shimmerView];
}

- (void)createBrandAndMallShimmerView{
    UIView *shimmerView = [[[NSBundle mainBundle] loadNibNamed:@"BrandShimmerView" owner:self options:nil] lastObject];
    shimmerView.frame = self.bounds;
    [journalView addSubview:shimmerView];
}

- (void)createRestaurantShimmerView{
    UIView *shimmerView = [[[NSBundle mainBundle] loadNibNamed:@"RetaurantCardShimmerView" owner:self options:nil] lastObject];
    shimmerView.frame = self.bounds;
    [journalView addSubview:shimmerView];
}

- (void)createNotificationShimmerView{
    UIView *shimmerView = [[[NSBundle mainBundle] loadNibNamed:@"NotificationShimmerView" owner:self options:nil] lastObject];
    shimmerView.frame = self.bounds;
    [journalView addSubview:shimmerView];
}

- (void)createPhotoShimmerView{
    UIView *shimmerView = [[UIView alloc] initWithFrame:self.bounds];
    shimmerView.backgroundColor = [UIColor colorWithHexString:@"#E6E6E6"];
    CGFloat viewHeight = (DeviceWidth - 2) / 3;
    for (int i = 1; i < 3; i ++) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(viewHeight * i, 0, 1, (viewHeight + 1) * 5)];
        view.backgroundColor = [UIColor whiteColor];
        [shimmerView addSubview:view];
    }
    
    for (int i = 1; i < 5; i ++) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, viewHeight * i, DeviceWidth, 1)];
        view.backgroundColor = [UIColor whiteColor];
        [shimmerView addSubview:view];
    }
    
    [journalView addSubview:shimmerView];
}

- (void)createGuideListShimmerView{
    UIView *shimmerView = [[[NSBundle mainBundle] loadNibNamed:@"GuideListShimmerView" owner:self options:nil] lastObject];
    shimmerView.frame = self.bounds;
    [journalView addSubview:shimmerView];
}

@end
