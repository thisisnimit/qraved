//
// NavigationBarButtonItem.m
//  Qraved
//
//  Created by jefftang on 8/13/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//


#import "NavigationBarButtonItem.h"
#import "UIColor+Helper.h"
#import "UIConstants.h"
@interface NavigationBarButtonItem() {
    id customTarget;
    UIButton *customButton;
}
@end

@implementation NavigationBarButtonItem
-(id)initWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style target:(id)target action:(SEL)action{
    self = [super initWithTitle:title style:style target:target action:action];
    if (self) {
        //self.tintColor = [UIColor blackColor];
    }
    return self;
}
- (id)initWithImage:(UIImage *)image andTitle:(NSString *)title
             target:(id)target action:(SEL)action width:(float)width offset:(float)offset{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0.0f, 0.0f, width, width)];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [btn setImage:image forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.imageEdgeInsets = UIEdgeInsetsMake(0, offset, 0, 0);
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, offset, 0, 0);
    btn.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:16];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self = [[NavigationBarButtonItem alloc] initWithCustomView:btn];
    
    if (self) {
        customButton = btn;
        customImage = image;
        customTarget = target;
        customAction = action;
    }
    
    return self;
}

- (id)initWithImage:(UIImage *)image andTitle:(NSString *)title
             target:(id)target action:(SEL)action width:(float)width{
    return [self initWithImage:image andTitle:title target:target action:action width:width offset:0];
}

//+ (NavigationBarButtonItem *)barItemWithImage:(UIImage*)image andTitle:(NSString *)title target:(id)target action:(SEL)action
//width:(float)width
//{
//    return [[NavigationBarButtonItem alloc] initWithImage:image andTitle:title target:target action:action width:width];
//}
//+ (NavigationBarButtonItem *)barItemWithImage:(UIImage*)image andTitle:(NSString *)title target:(id)target action:(SEL)action
//                                        width:(float)width offset:(float)offset
//{
//    return [[NavigationBarButtonItem alloc] initWithImage:image andTitle:title target:target action:action width:width offset:offset];
//}


- (void)setCustomImage:(UIImage *)image {
    customImage = image;
    [customButton setImage:image forState:UIControlStateNormal];
}

- (void)setCustomAction:(SEL)action {
    customAction = action;
    [customButton removeTarget:nil action:NULL forControlEvents:UIControlEventAllEvents];
    [customButton addTarget:customTarget action:action forControlEvents:UIControlEventTouchUpInside];
}

@end
