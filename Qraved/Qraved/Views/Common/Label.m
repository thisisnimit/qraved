//
//  Label.m
//  Qraved
//
//  Created by Shine Wang on 8/7/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "Label.h"
#import "UIConstants.h"
#import "NSString+Helper.h"

@implementation Label

@synthesize verticalAlignment1 = verticalAlignment_;

int const TITLE_FONT_SIZE = 13.0;
int const CAPTION_FONT_SIZE = 13.0;
int const STAT_FONT_SIZE = 12.0;

CGFloat const COUNT_LABEL_WIDTH=70;
CGFloat const COUNT_LABEL_HEIGHT=10;
CGFloat const NAME_LABEL_WIDTH=145;
CGFloat const NAME_LABEL_HEIGHT=20;
CGFloat const TIME_LABEL_HEIGHT=16;
CGFloat const TIME_LABEL_WIDTH=70;


-(Label *)initWithFrame:(CGRect )frame andTextFont:(UIFont *)font andTextColor:(UIColor *)color andTextLines:(NSInteger)number
{
    self=[self initDefaultLabel];
    if(self)
    {
        self.frame=frame;
        [self setTextColor:color];
        [self setFont:font];
        [self setNumberOfLines:number];
        [self setLineBreakMode:NSLineBreakByWordWrapping];
        self.verticalAlignment1 = VerticalAlignmentMiddle1;
    }
    
    return self;
}

- (void)setVerticalAlignment:(VerticalAlignment1)verticalAlignment {
    verticalAlignment_ = verticalAlignment;
    [self setNeedsDisplay];
}

- (CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines {
    CGRect textRect = [super textRectForBounds:bounds limitedToNumberOfLines:numberOfLines];
    switch (self.verticalAlignment1) {
        case VerticalAlignmentTop1:
            textRect.origin.y = bounds.origin.y;
            break;
        case VerticalAlignmentBottom1:
            textRect.origin.y = bounds.origin.y + bounds.size.height - textRect.size.height;
            break;
        case VerticalAlignmentMiddle1:
            // Fall through.
        default:
            textRect.origin.y = bounds.origin.y + (bounds.size.height - textRect.size.height) / 2.0;
    }
    return textRect;
}

-(void)drawTextInRect:(CGRect)requestedRect {
    CGRect actualRect = [self textRectForBounds:requestedRect limitedToNumberOfLines:self.numberOfLines];
    [super drawTextInRect:actualRect];
}




-(void)setString:(NSString *)string
{
    CGSize maximumlabelsize=CGSizeMake(296, FLT_MAX);
    CGSize expectedLabelSize=[string sizeWithFont:self.font constrainedToSize:maximumlabelsize lineBreakMode:self.lineBreakMode];
    
    //adjust the label the the new height.
    CGRect newFrame=self.frame;
    newFrame.size.height=expectedLabelSize.height;
    self.frame=newFrame;
    
    [self setText:string];
}

- (Label *)initNotificationLabel:(NSString *)text {
    self=[super init];
    if(self)
    {
        self.backgroundColor = [UIColor clearColor];
        self.textColor = [UIColor colorWithHue:(DEFAULT_FONT_BOLD_HUE/360.0)
                                     saturation:DEFAULT_FONT_BOLD_SAT
                                     brightness:DEFAULT_FONT_BOLD_BRI
                                          alpha:1.0];
        self.textAlignment = NSTextAlignmentLeft;
        self.font = [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:POST_TITLE_FONT_SIZE];
        self.lineBreakMode = NSLineBreakByWordWrapping;
        
        self.text = text;
        self.numberOfLines = 0;
        [self sizeToFit];
    }
    return self;
    
}

+ (UILabel *)setRating:(UILabel *)label text:(NSString *)text {
    return label;
    
}

+(UILabel *)setCaption:(UILabel *)label text:(NSString *)text {
    return label;
    
}
+ (UILabel *)setTitle:(UILabel *)label text:(NSString *)text {
    
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithHue:(DEFAULT_FONT_BOLD_HUE/360.0)
                                 saturation:DEFAULT_FONT_BOLD_SAT
                                 brightness:DEFAULT_FONT_BOLD_BRI
                                      alpha:1.0];
    label.textAlignment = NSTextAlignmentLeft;
    label.font = [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:POST_TITLE_FONT_SIZE];
    label.text = text;
    
    return label;
    
}
+ (UILabel *)setTitleCaption:(UILabel *)label text:(NSString *)text {
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor grayColor];
    label.textAlignment = NSTextAlignmentLeft;
    label.lineBreakMode=NSLineBreakByWordWrapping;
    label.numberOfLines=0;
    label.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:POST_TITLE_CAPTION_FONT_SIZE];
    label.text = text;
    
    return label;
    
}

+ (UILabel *)setStat:(UILabel *)label text:(NSString *)text {
    
    label.backgroundColor = [UIColor clearColor];
    
    label.textColor = [UIColor grayColor];
    
    
    //label.textColor = [UIColor colorWithHue:(0.0/360.0)  //white
    //                                 saturation:0.0
    //                                 brightness:100.0
    //                                      alpha:1.0];
    //label.shadowColor = [UIColor whiteColor];
    //label.shadowOffset = CGSizeMake(0.5, 0.5);
    label.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:STAT_FONT_SIZE];
    label.textAlignment = NSTextAlignmentLeft;
    label.text = text;
    return label;
}

+ (UILabel *)setDrawerTitle:(UILabel *)label text:(NSString *)text{
    label.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:16];
    label.textAlignment = NSTextAlignmentLeft;
    label.text = text;
    return label;
}
+ (UILabel *)setDrawerTitleCaption:(UILabel *)label text:(NSString *)text{
    label.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:STAT_FONT_SIZE];
    label.textAlignment = NSTextAlignmentLeft;
    label.text = text;
    return label;
}
+ (UILabel *)setDrawerNotificationsUserName:(UILabel *)label text:(NSString *)text{
    label.text = text;
    return label;
}
+ (UILabel *)setDrawerNotificationsMessage:(UILabel *)label text:(NSString *)text{
    label.text = text;
    return label;
}
+ (UILabel *)setDrawerNotificationsTimestamp:(UILabel *)label text:(NSString *)text{
    label.text = text;
    return label;
}


-(Label *)initDefaultLabel
{
    self=[super init];
    if(self)
    {
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

-(UIFont *)defaultFont:(CGFloat)fontSize
{
    return [UIFont systemFontOfSize:fontSize];
}

-(Label *)initScoreLabel
{
    self=[self initDefaultLabel];
    if(self)
    {
        [self setTextColor:[UIColor whiteColor]];
        [self setFont:[self defaultFont:11]];
        [self setTextAlignment:NSTextAlignmentCenter];
        [self setFrame:CGRectMake(0, 6, 25, 14)];
    }
    return self;
}

-(Label *)initBodyLabel
{
    self=[self initDefaultLabel];
    if(self)
    {
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_NORMAL_HUE/360) saturation:DEFAULT_FONT_NORMAL_SAT brightness:DEFAULT_FONT_NORMAL_BRI alpha:1.0]];
        [self setFont:[UIFont fontWithName:DEFAULT_FONT_NAME size:13.0]];
        [self setLineBreakMode:NSLineBreakByWordWrapping];
    }
    return self;
}

-(Label *)initReviewsDetalLabel
{
    self=[self initDefaultLabel];
    if(self)
    {
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_NORMAL_HUE/360) saturation:DEFAULT_FONT_NORMAL_SAT brightness:DEFAULT_FONT_NORMAL_BRI alpha:1.0]];
        [self setFont:[UIFont fontWithName:DEFAULT_FONT_NAME size:13.0]];
        [self setLineBreakMode:NSLineBreakByWordWrapping];
    }
    return self;
}

-(Label *)initProfileTitleLabel
{
    self=[self initDefaultLabel];
    if(self)
    {
        
        
        
        [self setTextColor:[UIColor whiteColor]];
        self.shadowColor = [UIColor blackColor];
        self.shadowOffset = CGSizeMake(0.5,0.5);
        
        [self setFont:[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:PAGE_TITLE_FONT_SIZE]];
        [self setNumberOfLines:1];
    }
    return self;
}

-(Label *)initProfileSubTitleLabel
{
    self=[self initDefaultLabel];
    if(self)
    {

        [self setTextColor:[UIColor whiteColor]];
        self.shadowColor = [UIColor blackColor];
        self.shadowOffset = CGSizeMake(0.5,0.5);
        
        [self setFont:[UIFont fontWithName:DEFAULT_FONT_NAME size:PAGE_TITLE_CAPTION_FONT_SIZE]];
        [self setNumberOfLines:1];
    }
    return self;
}


-(Label *)initPageTitleLabel
{
    self=[self initDefaultLabel];
    if(self)
    {
        
        
        
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_BOLD_HUE/360.0)
                                      saturation:DEFAULT_FONT_BOLD_SAT
                                      brightness:DEFAULT_FONT_BOLD_BRI
                                           alpha:1.0]];
        
        [self setFont:[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:PAGE_TITLE_FONT_SIZE]];
        [self setNumberOfLines:1];
    }
    return self;
}

-(Label *)initPageTitleCaptionLabel
{
    self=[self initDefaultLabel];
    if(self)
    {
        
        
        
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_NORMAL_HUE/360.0)
                                      saturation:DEFAULT_FONT_NORMAL_SAT
                                      brightness:DEFAULT_FONT_NORMAL_BRI
                                           alpha:1.0]];
        
        [self setFont:[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:PAGE_TITLE_CAPTION_FONT_SIZE]];
//        [self setNumberOfLines:1];
    }
    return self;
}

-(Label *)initTitleLabel
{
    self=[self initDefaultLabel];
    if(self)
    {
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_BOLD_HUE/360.0)
                                      saturation:DEFAULT_FONT_BOLD_SAT
                                      brightness:DEFAULT_FONT_BOLD_BRI
                                           alpha:1.0]];
        [self setFont:[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:POST_TITLE_FONT_SIZE]];
        //[self setFont:[self defaultFont:15]];
        [self setNumberOfLines:1];
    }
    return self;
}

-(Label *)initTitleCaptionLabel
{
    self=[self initDefaultLabel];
    if(self)
    {
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_NORMAL_HUE/360.0)
                                      saturation:DEFAULT_FONT_NORMAL_SAT
                                      brightness:DEFAULT_FONT_NORMAL_BRI
                                           alpha:1.0]];
        [self setFont:[self defaultFont:POST_TITLE_CAPTION_FONT_SIZE]];
        [self setLineBreakMode:NSLineBreakByWordWrapping];
        [self setNumberOfLines:0];
    }
    return self;
}

-(Label *)initTitleCaptionBoldLabel
{
    self=[self initDefaultLabel];
    if(self)
    {
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_NORMAL_HUE/360.0)
                                      saturation:DEFAULT_FONT_NORMAL_SAT
                                      brightness:DEFAULT_FONT_NORMAL_BRI
                                           alpha:1.0]];
        [self setFont:[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:POST_TITLE_CAPTION_FONT_SIZE]];
        [self setLineBreakMode:NSLineBreakByWordWrapping];
        [self setNumberOfLines:0];
    }
    return self;
}

-(Label *)initCountLabel
{
    self=[self initDefaultLabel];
    if(self)
    {
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_LIGHT_HUE/360.0)
                                      saturation:DEFAULT_FONT_LIGHT_SAT
                                      brightness:DEFAULT_FONT_LIGHT_BRI
                                           alpha:1.0]];
        [self setTextAlignment:NSTextAlignmentLeft];
        [self setFont:[self defaultFont:POST_COUNT_FONT_SIZE]];
        [self setNumberOfLines:1];
    }
    return self;
}

-(Label *)initCreateTimeLabel
{
    self=[self initDefaultLabel];
    if(self)
    {
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_LIGHT_HUE/360.0)
                                      saturation:DEFAULT_FONT_LIGHT_SAT
                                      brightness:DEFAULT_FONT_LIGHT_BRI
                                           alpha:1.0]];
        [self setFont:[self defaultFont:POST_COUNT_FONT_SIZE]];
        [self setNumberOfLines:1];
    }
    return self;
}

-(Label *)initQraveCountLabel
{
    self=[self initDefaultLabel];
    if(self)
    {
        
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_LIGHT_HUE/360.0)
                                      saturation:DEFAULT_FONT_LIGHT_SAT
                                      brightness:DEFAULT_FONT_LIGHT_BRI
                                           alpha:1.0]];
        [self setBackgroundColor:[UIColor clearColor]];
        [self setFont:[self defaultFont:14]];
        [self setNumberOfLines:1];
        
    }
    return self;
}

-(Label *)initCommentLabel
{
    self=[self initDefaultLabel];
    if(self)
    {
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_NORMAL_HUE/360.0)
                                      saturation:DEFAULT_FONT_NORMAL_SAT
                                      brightness:DEFAULT_FONT_NORMAL_BRI
                                           alpha:1.0]];
        [self setFont:[self defaultFont:POST_COMMENT_FONT_SIZE]];
         [self setLineBreakMode:NSLineBreakByWordWrapping];
        [self setNumberOfLines:0];
    }
    return self;
}
-(Label *)initCommentNameLabel
{
    self=[self initDefaultLabel];
    if(self)
    {
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_BOLD_HUE/360.0)
                                      saturation:DEFAULT_FONT_BOLD_SAT
                                      brightness:DEFAULT_FONT_BOLD_BRI
                                           alpha:1.0]];
        [self setFont:[self defaultFont:POST_COMMENT_NAME_FONT_SIZE]];
        [self setNumberOfLines:1];
    }
    return self;
}
-(Label *)initCommentTimestampLabel
{
    self=[self initDefaultLabel];
    if(self)
    {
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_LIGHT_HUE/360.0)
                                      saturation:DEFAULT_FONT_LIGHT_SAT
                                      brightness:DEFAULT_FONT_LIGHT_BRI
                                           alpha:1.0]];
        [self setFont:[self defaultFont:POST_COMMENT_TIMESTAMP_FONT_SIZE]];
        [self setNumberOfLines:1];
    }
    return self;
}


-(Label *)initInfoHeaderLabel
{
    self=[self initDefaultLabel];
    if(self)
    {
        
        [self setTextColor:[UIColor grayColor]];
        [self setFont:[self defaultFont:DEFAULT_INFO_HEADER_FONT_SIZE]];
        [self setNumberOfLines:1];
    }
    return self;
}
-(Label *)initInfoDataLabel
{
    self=[self initDefaultLabel];
    if(self)
    {
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_NORMAL_HUE/360.0)
                                      saturation:DEFAULT_FONT_NORMAL_SAT
                                      brightness:DEFAULT_FONT_NORMAL_BRI
                                           alpha:1.0]];
        [self setFont:[self defaultFont:DEFAULT_INFO_DATA_FONT_SIZE]];
        [self setNumberOfLines:1];
    }
    return self;
}
-(Label *)initTableSectionHeaderLabel
{
    self=[self initDefaultLabel];
    if(self)
    {
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_INVERSE_BOLD_HUE/360.0)
                                      saturation:DEFAULT_FONT_INVERSE_BOLD_SAT                                      brightness:DEFAULT_FONT_INVERSE_BOLD_BRI
                                           alpha:1.0]];
        [self setFont:[UIFont fontWithName:TABLE_SECTION_HEADER_FONT_NAME size:TABLE_SECTION_HEADER_FONT_SIZE]];
        [self setNumberOfLines:1];
        
    }
    return self;
}

-(Label *)initErrorMessage
{
    self=[self initDefaultLabel];
    if(self)
    {
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_LIGHT_HUE/360) saturation:DEFAULT_FONT_LIGHT_SAT brightness:DEFAULT_FONT_LIGHT_BRI alpha:1.0]];
//        [self setFont:[UIFont fontWithName:DEFAULT_FONT_NAME size:12.0]];
        
        [self setLineBreakMode:NSLineBreakByWordWrapping];
    }
    return self;
}

-(Label *)initCuisineTitleLabel
{
    self=[self initDefaultLabel];
    if(self)
    {
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_INVERSE_BOLD_HUE/360) saturation:DEFAULT_FONT_INVERSE_BOLD_SAT brightness:DEFAULT_FONT_INVERSE_BOLD_BRI alpha:1.0]];
        [self setFont:[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:CUISINE_TITLE_FONT_SIZE]];
        
        [self setLineBreakMode:NSLineBreakByWordWrapping];
    }
    return self;
}

-(Label *)initDetailTitleLabel {
    self=[self initDefaultLabel];
    if(self)
    {
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_BOLD_HUE/360) saturation:DEFAULT_FONT_BOLD_SAT brightness:DEFAULT_FONT_BOLD_BRI alpha:1.0]];
        [self setFont:[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:POST_DETAIL_TITLE_FONT_SIZE]];
        
        [self setLineBreakMode:NSLineBreakByWordWrapping];
    }
    return self;
}
-(Label *)initDetailTitleCaptionLabel{
    self=[self initDefaultLabel];
    if(self)
    {
//        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_NORMAL_HUE/360) saturation:DEFAULT_FONT_NORMAL_SAT brightness:DEFAULT_FONT_NORMAL_BRI alpha:1.0]];
        [self setTextColor:[UIColor grayColor]];
        
        [self setFont:[UIFont fontWithName:DEFAULT_FONT_NAME size:POST_DETAIL_TITLE_CAPTION_FONT_SIZE]];
        
        [self setLineBreakMode:NSLineBreakByWordWrapping];
        [self setNumberOfLines:0];
    }
    return self;
}
-(Label *)initDetailNicknameLabel{
    self=[self initDefaultLabel];
    if(self)
    {
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_NORMAL_HUE/360) saturation:DEFAULT_FONT_NORMAL_SAT brightness:DEFAULT_FONT_NORMAL_BRI alpha:1.0]];
        [self setFont:[UIFont fontWithName:DEFAULT_FONT_NAME size:POST_DETAIL_NICKNAME_FONT_SIZE]];
        
        [self setLineBreakMode:NSLineBreakByWordWrapping];
    }
    return self;
}
-(Label *)initDetailTimestampLabel{
    self=[self initDefaultLabel];
    if(self)
    {
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_NORMAL_HUE/360) saturation:DEFAULT_FONT_NORMAL_SAT brightness:DEFAULT_FONT_NORMAL_BRI alpha:1.0]];
        [self setFont:[UIFont fontWithName:DEFAULT_FONT_NAME size:POST_DETAIL_TIMESTAMP_FONT_SIZE]];
        
        [self setLineBreakMode:NSLineBreakByWordWrapping];
    }
    return self;
}
-(Label *)initDetailDescriptionLabel{
    self=[self initDefaultLabel];
    if(self)
    {
        [self setTextColor:[UIColor colorWithHue:(DEFAULT_FONT_INVERSE_BOLD_HUE/360) saturation:DEFAULT_FONT_INVERSE_BOLD_SAT brightness:DEFAULT_FONT_INVERSE_BOLD_BRI alpha:1.0]];
        [self setFont:[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:CUISINE_TITLE_FONT_SIZE]];
        
        [self setLineBreakMode:NSLineBreakByWordWrapping];
    }
    return self;
}


#pragma mark copyToPasteboard
-(BOOL)canBecomeFirstResponder
{
    return YES;
}
-(BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    return (action == @selector(copy:));
}
-(void)copy:(id)sender
{
    UIPasteboard *pboard = [UIPasteboard generalPasteboard];
    pboard.string = self.text;
}
-(void)awakeFromNib
{
    [super awakeFromNib];
    [self attachTapHandler];
}

-(void)attachTapHandler
{
    self.userInteractionEnabled = YES;
    //UITapGestureRecognizer *touch = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    //touch.numberOfTapsRequired = 2;
    UILongPressGestureRecognizer *longPressGR = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    longPressGR.minimumPressDuration = 1;
    [self addGestureRecognizer:longPressGR];
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self attachTapHandler];
    }
    return self;
}
-(void)handleTap:(UIGestureRecognizer*) recognizer
{
    [self becomeFirstResponder];
    //UIMenuItem *copyLink = [[UIMenuItem alloc] initWithTitle:@"Copy" action:@selector(copy:)];
    //[[UIMenuController sharedMenuController] setMenuItems:[NSArray arrayWithObjects:copyLink, nil]];
    CGRect rect = CGRectMake(self.frame.origin.x+10, self.frame.origin.y+self.frame.size.height/2, 10, 10);
    [[UIMenuController sharedMenuController] setTargetRect:rect inView:self.superview];
    [[UIMenuController sharedMenuController] setMenuVisible:YES animated: YES];
}

- (void)setFont:(UIFont *)font range:(NSRange)range
{
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:self.text];
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString:str];
    [text addAttribute:NSFontAttributeName value:font range:range];
    [self setAttributedText:text];
}

@end
