//
//  Label.h
//  Qraved
//
//  Created by Shine Wang on 8/7/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIConstants.h"


typedef enum
{
    VerticalAlignmentTop1 = 0, // default
    VerticalAlignmentMiddle1,
    VerticalAlignmentBottom1,
} VerticalAlignment1;

@interface Label : UILabel
{
@private  VerticalAlignment1 _verticalAlignment;
}
@property (nonatomic) VerticalAlignment1 verticalAlignment1;

-(Label *)initWithFrame:(CGRect )frame andTextFont:(UIFont *)font andTextColor:(UIColor *)color andTextLines:(NSInteger)number;

-(void)setString:(NSString *)string;


/* General labels */

//global
//+ (UILabel *)setDefault:(UILabel *)label text:(NSString *)text;
+ (UILabel *)setTitle:(UILabel *)label text:(NSString *)text;
+ (UILabel *)setTitleCaption:(UILabel *)label text:(NSString *)text;
//+ (UILabel *)setDescription:(UILabel *)label text:(NSString *)text;
//+ (UILabel *)setTimestamp:(UILabel *)label text:(NSString *)text;
+ (UILabel *)setRating:(UILabel *)label text:(NSString *)text;
+ (UILabel *)setCaption:(UILabel *)label text:(NSString *)text;
//+ (UILabel *)setUserName:(UILabel *)label text:(NSString *)text;
//+ (UILabel *)setPrice:(UILabel *)label text:(NSString *)text;
//+ (UILabel *)setDiscount:(UILabel *)label text:(NSString *)text;
+ (UILabel *)setStat:label text:(NSString *)text;

//left drawer
+ (UILabel *)setDrawerTitle:(UILabel *)label text:(NSString *)text;
+ (UILabel *)setDrawerTitleCaption:(UILabel *)label text:(NSString *)text;
+ (UILabel *)setDrawerNotificationsUserName:(UILabel *)label text:(NSString *)text;
+ (UILabel *)setDrawerNotificationsMessage:(UILabel *)label text:(NSString *)text;
+ (UILabel *)setDrawerNotificationsTimestamp:(UILabel *)label text:(NSString *)text;

//+ (UILabel *)setButtonMainUp:(UILabel *)label text:(NSString *)text;
//+ (UILabel *)setButtonMainDown:(UILabel *)label text:(NSString *)text;
//+ (UILabel *)setButtonSecondUp:(UILabel *)label text:(NSString *)text;
//+ (UILabel *)setButtonSecondDown:(UILabel *)label text:(NSString *)text;



-(UILabel *)initDefaultLabel;


-(Label *)initScoreLabel;
-(Label *)initBodyLabel;
-(Label *)initPageTitleLabel;
-(Label *)initPageTitleCaptionLabel;
-(Label *)initTitleLabel;
-(Label *)initTitleCaptionLabel;
-(Label *)initTitleCaptionBoldLabel;
-(Label *)initReviewsDetalLabel;
-(Label *)initCountLabel;
-(Label *)initCommentLabel;
-(Label *)initCommentNameLabel;
-(Label *)initCommentTimestampLabel;
-(Label *)initCreateTimeLabel;
-(Label *)initQraveCountLabel;
-(Label *)initInfoHeaderLabel;
-(Label *)initInfoDataLabel;
-(Label *)initTableSectionHeaderLabel;
-(Label *)initProfileTitleLabel;
-(Label *)initProfileSubTitleLabel;
-(Label *)initErrorMessage;
-(Label *)initCuisineTitleLabel;
-(Label *)initDetailTitleLabel;
-(Label *)initDetailTitleCaptionLabel;
-(Label *)initDetailNicknameLabel;
-(Label *)initDetailTimestampLabel;
-(Label *)initDetailDescriptionLabel;

- (Label *)initNotificationLabel:(NSString *)text;
- (void)setFont:(UIFont *)font range:(NSRange)range;

extern CGFloat const COUNT_LABEL_WIDTH;
extern CGFloat const COUNT_LABEL_HEIGHT;
extern CGFloat const NAME_LABEL_WIDTH;
extern CGFloat const NAME_LABEL_HEIGHT;
extern CGFloat const TIME_LABEL_WIDTH;
extern CGFloat const TIME_LABEL_HEIGHT;


@end
