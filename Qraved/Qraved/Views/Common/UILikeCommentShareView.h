//
//  LikeCommentShareView.h
//  Qraved
//
//  Created by Jeff on 3/3/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICommunityButton.h"
#import "IMGJournalComment.h"
#import "IMGDishComment.h"
#import "IMGRestaurantEventComment.h"
#import "IMGReviewComment.h"
#import "UICommentInputView.h"
#import "UICommentCell.h"
#import "LoginViewController.h"
#import "V2_LikeCommentShareView.h"
#define COMMUNITYBUTTONVIEWHEIGHT 56

@protocol LikeCommentShareDelegate <NSObject>
@optional
- (void)likeCommentShareView:(UIView*)likeCommentShareView viewPreviousCommentsTapped:(UIButton*)button lastComment:(id)comment;
- (void)likeCommentShareView:(UIView*)likeCommentShareView commentButtonTapped:(UIButton*)button;
- (void)likeCommentShareView:(UIView*)likeCommentShareView shareButtonTapped:(UIButton*)button;
- (void)likeCommentShareView:(UIView*)likeCommentShareView likeButtonTapped:(UIButton*)button;
- (void)likeCommentShareView:(UIView*)likeCommentShareView commentInputView:(UIView *)view inputViewExpand:(UITextView *)inputTextView commentInputViewYOffset:(CGFloat)commentInputViewYOffset;
- (void)likeCommentShareView:(UIView*)likeCommentShareView commentInputView:(UIView *)view postButtonTapped:(UIButton *)postButton commentInputText:(NSString *)text;
- (void)likeCommentShareView:(UIView*)likeCommentShareView commentInputView:(UIView *)view inputViewDidBeginEditing:(UITextView *)textView;
- (void)likeCommentShareView:(UIView*)likeCommentShareView commentCell:(UITableViewCell*)cell readMoreTapped:(id)comment offset:(CGFloat)offset;
- (void)likeCommentShareViewUserNameOrImageTapped:(id)sender;
- (void)likeListButtonTapped;

@end

@interface UILikeCommentShareView : UIView<CommunityButtonTappedDelegate,UITableViewDataSource,UITableViewDelegate,CommentInputViewDelegate,UICommentCellDelegate,AutoPostDelegate,V2_LikeCommentShareViewDelegete>

@property (nonatomic, assign) id<LikeCommentShareDelegate>likeCommentShareDelegate;

@property (nonatomic, strong)V2_LikeCommentShareView *likeCommentShare;;

-(id)initInSuperView:(UIView*)parentView atStartX:(CGFloat)X startY:(CGFloat)Y commentCount:(int)commentCount_ commentArray:(NSArray *)commentList liked:(BOOL)liked_ withFooter:(BOOL)withFooter needCommentListView:(BOOL)needCommentList;

-(id)initInSuperView:(UIView*)parentView atStartX:(CGFloat)X startY:(CGFloat)Y commentCount:(int)commentCount_ commentArray:(NSArray *)commentList liked:(BOOL)liked_ withFooter:(BOOL)withFooter needCommentListView:(BOOL)needCommentList likeCount:(int)likeCount;

-(void)addPreviousComments:(NSArray *)commentList_;
-(void)addNewComment:(id)journalComment;
-(void)addNewCommentAndRemoveOldOne:(id)comment;
-(void)updateLikedStatus;
- (void)updateLikeButtonStatus:(BOOL)liked likeCount:(int)likeCount;
+(CGFloat)calculatedSelfViewHeight:(NSArray*)commentList_ withFooter:(BOOL)withFooter_ withMoreComments:(BOOL)moreComments_;
+(CGFloat)calculatedTableViewHeight:(NSArray*)commentList_ withFooter:(BOOL)withFooter_;

@end
