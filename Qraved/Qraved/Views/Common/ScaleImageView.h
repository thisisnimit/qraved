//
//  ScaleImageView.h
//  Qraved
//
//  Created by Admin on 8/28/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDWebImageManager.h"

@interface ScaleImageView : UIView <UIScrollViewDelegate, SDWebImageManagerDelegate>

-(id)initWithFrame:(CGRect)frame AndUrlStr:(NSString *)urlStr;

-(void)setNoneScale;
//@property (nonatomic, assign) id delegate;

@end
