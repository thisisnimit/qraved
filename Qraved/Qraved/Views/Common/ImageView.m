//
//  ImageView.m
//  Qraved
//
//  Created by Shine Wang on 8/22/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "ImageView.h"

@implementation ImageView
@synthesize width,height;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(ImageView *)initDefault
{
    self=[super init];
    if(self)
    {
        
    }
    
    return self;
}

-(ImageView *)initBigAvatar
{
    self=[self initDefault];
    if(self)
    {
        self.width=BIG_AVATAR_IMAGE_WIDTH;
        self.height=BIG_AVATAR_IMAGE_HEIGHT;
    }
    return self;
}
-(ImageView *)initMiddleAvatar
{
    self=[self initDefault];
    if(self)
    {
        self.width=MIDDLE_AVATAR_IMAGE_WIDTH;
        self.height=MIDDLE_AVATAR_IMAGE_HEIGHT;
    }
    return self;
}
-(ImageView *)initBigCommentAvatar
{
    self=[self initDefault];
    if(self)
    {
        self.width=COMMENT_BIG_AVATAR_IMAGE_WIDTH;
        self.height=COMMENT_BIG_AVATAR_IMAGE_HEIGHT;
    }
    return self;
}
-(ImageView *)initSmallCommentAvatar
{
    self=[self initDefault];
    if(self)
    {
        self.width=COMMENT_SMALL_AVATAR_IMAGE_WIDTH;
        self.height=COMMENT_SMALL_AVATAR_IMAGE_HEIGHT;
    }
    return self;
}
-(ImageView *)initSmallAvatar
{
    self=[self initDefault];
    if(self)
    {
        self.width=SMALL_AVATAR_IMAGE_WIDTH;
        self.height=SMALL_AVATAR_IMAGE_HEIGHT;
    }
    return self;
}

-(ImageView *)initScrollImage
{
    self=[self initDefault];
    if(self)
    {
        self.width=SCROLL_IMAGE_WIDTH;
        self.height=SCROLL_IMAGE_HEIGHT;
    }
    return self;
}

-(ImageView *)initMenuImage
{
    self=[self initDefault];
    if(self)
    {
        self.width=MENU_IMAGE_WIDTH;
        self.height=MENU_IMAGE_HEIGHT;
    }
    return self;
}

-(ImageView *)initPalaceholder
{
    self=[self initDefault];
    if(self)
    {
        UIImage *iconImage=[UIImage imageNamed:@"q.png"];
//        NSLog(@"iconimage size %f,%f",iconImage.size.height,iconImage.size.width);
        [self setImage:iconImage];
        self.width=iconImage.size.width;
        self.height=iconImage.size.height;
    }
    return self;
}

@end
