//
//  V2_BrandPreferenceNewCollectionViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/8/9.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_BrandPreferenceNewCollectionViewCell.h"

@implementation V2_BrandPreferenceNewCollectionViewCell
{
    UIImageView  *imageV;
    myLabel *lblLabel;
}
- (instancetype)initWithFrame:(CGRect)frame{

    if ([super initWithFrame:frame]) {
        [self initUI];
    }
    return self;
}
- (void)initUI{
    
    self.selectView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 63, 63)];
    self.selectView.backgroundColor = [UIColor whiteColor];
    self.selectView.layer.borderColor = [UIColor colorWithHexString:@"#EAEAEA"].CGColor;
    self.selectView.layer.borderWidth = 2;
    self.selectView.layer.cornerRadius = 63/2;
    self.selectView.layer.masksToBounds = YES;
    [self.contentView addSubview:self.selectView];

    imageV = [[UIImageView alloc] initWithFrame:CGRectMake(4, 4, 55, 55)];
    imageV.layer.cornerRadius = 55/2;
    imageV.layer.masksToBounds = YES;
    imageV.layer.borderWidth = 0.5;
    imageV.layer.borderColor = [UIColor colorWithHexString:@"#D8D8D8"].CGColor;
    [self.contentView addSubview:imageV];
    
    self.selectImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 63, 63)];
    self.selectImageView.image = [UIImage imageNamed:@"ic_marks"];
    self.selectImageView.hidden = YES;
    self.selectImageView.layer.cornerRadius = 63/2;
    self.selectImageView.layer.masksToBounds = YES;
    [self.contentView addSubview:self.selectImageView];
    
    
    lblLabel = [[myLabel alloc] initWithFrame:CGRectMake(0, imageV.bottom +8, 63, 40)];
    lblLabel.textColor = [UIColor color333333];
    lblLabel.numberOfLines = 2;
    lblLabel.verticalAlignment = VerticalAlignmentTop;
    lblLabel.font = [UIFont systemFontOfSize:12];
    lblLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:lblLabel];
}


- (void)setModel:(V2_BrandModel *)model{

    _model = model;
    
    lblLabel.text = model.name;
    [imageV sd_setImageWithURL:[NSURL URLWithString:[model.imageUrl returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]]];
    
    if ([model.state isEqualToString:@"1"]) {
        self.selectImageView.hidden = NO;
        self.selectView.layer.borderColor = [UIColor colorWithHexString:@"#D20000"].CGColor;
    }else{
        self.selectImageView.hidden = YES;
        self.selectView.layer.borderColor = [UIColor colorWithHexString:@"#EAEAEA"].CGColor;
    }
}







@end
