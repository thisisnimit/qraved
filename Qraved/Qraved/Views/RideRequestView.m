////
////  RideRequestView.m
////  Qraved
////
////  Copyright © 2016年 Imaginato. All rights reserved.
////
//
//#import "RideRequestView.h"
//
//@implementation RideRequestView{
//    IMGRestaurant *restaurant;
//    UberButtonType uberButtonType;
//}
//
//-(instancetype)initWithRestaurant:(IMGRestaurant *)restaurantLocal withButtonType:(UberButtonType)uberButtonTypeLocal{
//    if(self=[super init]){
//        restaurant = restaurantLocal;
//        uberButtonType = uberButtonTypeLocal;
//        
//        UBSDKDeeplinkRequestingBehavior *requestBehavior = [[UBSDKDeeplinkRequestingBehavior alloc] init];
//        self.rideParameters = [[UBSDKRideParametersBuilder alloc] init];
//        
//        CLLocation *pickupLocation = [[CLLocation alloc] initWithLatitude:[AppDelegate ShareApp].locationManager.location.coordinate.latitude longitude:[AppDelegate ShareApp].locationManager.location.coordinate.longitude];
//        CLLocation *dropoffLocation = [[CLLocation alloc] initWithLatitude:[restaurant.latitude doubleValue] longitude:[restaurant.longitude doubleValue]];
//        [self.rideParameters setPickupLocation: pickupLocation];
//        self.rideParameters.dropoffLocation = dropoffLocation;
//        self.rideParameters.pickupNickname = restaurant.title;
////        [self.rideParameters setDropoffLocation:dropoffLocation nickname:restaurant.title];
//        
//        self.btn= [[UBSDKRideRequestButton alloc] initWithRequestingBehavior:requestBehavior];
//        [self.btn addTarget:self action:@selector(rideRequestButtonClick:) forControlEvents:UIControlEventTouchUpInside];
//        [self addSubview:self.btn];
//        
//        UIImage *image;
//        for (UIView *item in self.btn.subviews) {
//            if ([item isKindOfClass:[UIImageView class]]) {
//                image = ((UIImageView *)item).image;
//            }
//            [item removeFromSuperview];
//        }
//        
//        self.imageView = [[UIImageView alloc] initWithImage:image];
//        [self.btn addSubview:self.imageView];
//        
//        self.timeLabel = [[UILabel alloc] init];
//        self.timeLabel.numberOfLines = 0;
//        self.timeLabel.textAlignment = NSTextAlignmentLeft;
//        self.timeLabel.textColor = [UIColor whiteColor];
//        self.timeLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:10];
//        [self.btn addSubview:self.timeLabel];
//        
//        self.costLabel = [[UILabel alloc] init];
//        self.costLabel.numberOfLines = 0;
//        self.costLabel.textAlignment = NSTextAlignmentRight;
//        self.costLabel.textColor = [UIColor whiteColor];
//        self.costLabel.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:9];
//        [self.btn addSubview:self.costLabel];
//        
//    }
//    return self;
//}
//    
//-(void)setUBSDKPriceEstimate:(UBSDKPriceEstimate*)priceEstimate displayStyle:(DisplayStyle)displayStyle timeEstimate:(UBSDKTimeEstimate *)timeEstimate{
//    self.rideParameters.productID = priceEstimate.productID;
////    self.rideParameters = [self.rideParameters setProductID:priceEstimate.productID];
//    NSString *uberStr = @"uber";
//    if (uberButtonType == UberButtonUberMotor) {
//        uberStr = @"uberMOTOR";
//    }else if(uberButtonType == UberButtonUberX) {
//        uberStr = @"uberX";
//    }
//    if (displayStyle == DisplayStyleDouble) {
//        self.timeLabel.text = [NSString stringWithFormat:@"%@\nin %ld %@",uberStr,(timeEstimate.estimate/60),(timeEstimate.estimate/60>1)?@"mins":@"min"];
//        self.costLabel.text = [NSString stringWithFormat:@"%ldK - %ldK",priceEstimate.lowEstimate/1000,priceEstimate.highEstimate/1000];
//    }else if(displayStyle == DisplayStyleSimple){
//        self.timeLabel.text = [NSString stringWithFormat:@"%@ in %ld %@",uberStr,(timeEstimate.estimate/60),(timeEstimate.estimate/60>1)?@"mins":@"min"];
//        self.costLabel.text = [NSString stringWithFormat:@"Rp. %.3lfK - %.3lfK",priceEstimate.lowEstimate/1000.0,priceEstimate.highEstimate/1000.0];
//    }
//    self.btn.rideParameters = [self.rideParameters build];
//    [self.btn loadRideInformation];
//}
//    
//-(void)rideRequestButtonClick:(id)sender{
//    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
//    [eventProperties setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
//    if (uberButtonType == UberButtonUberMotor) {
//        [eventProperties setValue:@"UberMotor" forKey:@"Ride_Type"];
//    }else if(uberButtonType == UberButtonUberX) {
//        [eventProperties setValue:@"UberX" forKey:@"Ride_Type"];
//    }
//    [[Amplitude instance] logEvent:@"CL - Uber ride request" withEventProperties:eventProperties];
//}
//-(void)setDisplayStyle:(DisplayStyle)displayStyle{
//    self.btn.frame = self.bounds;
//    if (displayStyle == DisplayStyleDouble) {
//        self.imageView.frame = CGRectMake(10, 10, 15, 15);
//        self.timeLabel.frame = CGRectMake(self.imageView.endPointX+10, 0, 60,self.btn.frame.size.height);
//        self.costLabel.frame = CGRectMake(self.timeLabel.endPointX-5, 0, self.btn.frame.size.width - self.timeLabel.endPointX,self.btn.frame.size.height);
//    }else if(displayStyle == DisplayStyleSimple) {
//        self.imageView.frame = CGRectMake(10, 13.5, 15, 15);
//        self.timeLabel.frame = CGRectMake(self.imageView.endPointX+10, 0, 130,self.btn.frame.size.height);
//        self.costLabel.frame = CGRectMake(self.timeLabel.endPointX, 0, self.btn.frame.size.width - self.timeLabel.endPointX-5,self.btn.frame.size.height);
//    }
//}
//
//@end

