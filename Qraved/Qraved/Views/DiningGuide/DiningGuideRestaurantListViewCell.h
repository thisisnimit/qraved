//
//  DiningGuideRestaurantListViewCell.h
//  Qraved
//
//  Created by System Administrator on 11/25/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiningGuideRestaurantListViewCell : UITableViewCell{

}

@property(nonatomic,copy) NSString *title;
@property(nonatomic,copy) NSString *image;
@property(nonatomic,copy) NSString *cuisineDistrict;
@property(nonatomic,assign) NSInteger rateCount;
@property(nonatomic,assign) NSInteger reviewCount;
@property(nonatomic,copy) NSString *distance;

@end
