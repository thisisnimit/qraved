
//
//  DiningGuideRestaurantsView.m
//  Qraved
//
//  Created by System Administrator on 9/24/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "DiningGuideRestaurantsView.h"
#import "DiningGuideRestaurantsViewCell.h"
#import "IMGDiningGuide.h"
#import "IMGRestaurant.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "UIDevice+Util.h"
#import "NSString+Helper.h"
#import "UIImage+Resize.h"
#import "UIImageView+Helper.h"
#import "UIImageView+WebCache.h"
#import "NSString+Helper.h"

@implementation DiningGuideRestaurantsView{
	CGFloat navigationHeight;
	float titleHeight;
	NSString *diningGuideName;
	UILabel *navTitleLabel;
	UILabel *titleLabel;
    UIImageView *introView;
    UIButton *backButton;
    UIButton *shareButton;
}


-(instancetype)initWithFrame:(CGRect)frame diningGuide:(IMGDiningGuide*)diningGuide delegate:(NSObject<DiningGuideRestaurantsDelegate>*)delegate{
	if(self=[super initWithFrame:frame]){

		self.backgroundColor=[UIColor whiteColor];
		self.diningGuide=diningGuide;
		self.delegate=delegate;
		diningGuideName=[self.diningGuide.pageName filterHtml];
		self.hasData=YES;
		navigationHeight=44+[UIDevice heightDifference];
//		bannerHeight=298;
//		headerHeight=bannerHeight+8;
		titleHeight = [UIDevice heightDifference];

	    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0 ,0,frame.size.width,frame.size.height)];
	    _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
	    _tableView.delegate=self;
	    _tableView.dataSource=self;
	    [self addSubview:_tableView];

		[self setHeaderView];
		[self setNavigationView];
	    [self addObserver:self forKeyPath:@"restaurants" options:NSKeyValueObservingOptionNew context:nil];
	}
    return self;
}

-(void)setHeaderView{

	UIView *headerView=[UIView new];

	NSURL *urlString = [NSURL URLWithString:[self.diningGuide.headerImage ImgURLStringWide:DeviceWidth High:DeviceWidth*.6]];

    introView = [UIImageView new];

    UIImage *placeHolderImage = [[UIImage imageNamed:DEFAULT_IMAGE_STRING2 ] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth, DeviceWidth*.6)];
    __weak typeof(introView) weakIntroView = introView;

    [introView sd_setImageWithURL:urlString placeholderImage:placeHolderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        image = [image imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, DeviceWidth*.6)];
                [weakIntroView setImage:image];

    }];
    
    [self insertTransparentGradient];
    
    titleLabel=[UILabel new];
    titleLabel.font=[UIFont systemFontOfSize:24];
    titleLabel.text=diningGuideName;
    titleLabel.textColor=[UIColor color333333];


    NSString *content=[[self.diningGuide.pageContent removeHTML] stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    UILabel *contentLabel=[UILabel new];
    contentLabel.font = [UIFont systemFontOfSize:14];
    contentLabel.textColor=[UIColor color999999];

    [headerView sd_addSubviews:@[introView,titleLabel,contentLabel]];
    
    introView.sd_layout
    .topSpaceToView(headerView,0)
    .leftSpaceToView(headerView,0)
    .rightSpaceToView(headerView,0)
    .heightIs(DeviceWidth*.6);
    
    titleLabel.sd_layout
    .topSpaceToView(introView,5)
    .leftSpaceToView(headerView,15)
    .rightSpaceToView(headerView,15)
    .heightIs(28);
    
    contentLabel.sd_layout
    .topSpaceToView(titleLabel,5)
    .leftEqualToView(titleLabel)
    .rightEqualToView(titleLabel)
    .autoHeightRatio(0);
    
    [contentLabel setMaxNumberOfLinesToShow:5];
    
    contentLabel.text=content;
    
    [headerView setupAutoHeightWithBottomView:contentLabel bottomMargin:20];
    
    _tableView.tableHeaderView=headerView;
}

- (void) insertTransparentGradient {
    UIColor *colorOne = [UIColor colorWithRed:(0/255.0)  green:(0/255.0)  blue:(0/255.0)  alpha:0.8];
    UIColor *colorTwo = [UIColor colorWithRed:(0/255.0)  green:(0/255.0)  blue:(0/255.0)  alpha:0.0];
    NSArray *colors = [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, nil];
    NSNumber *stopOne = [NSNumber numberWithFloat:0.0];
    NSNumber *stopTwo = [NSNumber numberWithFloat:1.0];
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    
    //crate gradient layer
    CAGradientLayer *headerLayer = [CAGradientLayer layer];
    
    headerLayer.colors = colors;
    headerLayer.locations = locations;
    
    headerLayer.frame = CGRectMake(0, 0, DeviceWidth, DeviceWidth*.6);
    [introView.layer insertSublayer:headerLayer atIndex:0];
}

-(void)setNavigationView{
    
	_navigationView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, navigationHeight)];
	_navigationView.backgroundColor=[UIColor clearColor];
	_navigationView.userInteractionEnabled=YES;
	_navigationView.layer.zPosition=999999;
    
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"ic_back_guide"] forState:UIControlStateNormal];
    backButton.frame = CGRectMake(LEFTLEFTSET, 20, 60,44);
    [backButton addTarget:self.delegate action:@selector(returnBack) forControlEvents:UIControlEventTouchUpInside];
    backButton.imageEdgeInsets = UIEdgeInsetsMake(12, 0, 12, 48);
    
    shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareButton setImage:[UIImage imageNamed:@"ic_share_guide"] forState:UIControlStateNormal];
    shareButton.frame = CGRectMake(DeviceWidth-LEFTLEFTSET-44, 20, 60, 44);
    [shareButton addTarget:self.delegate action:@selector(shareButtonClick_v2:) forControlEvents:UIControlEventTouchUpInside];

    navTitleLabel=[[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET+60, 20, DeviceWidth-(LEFTLEFTSET+60)*2, 44)];
    [navTitleLabel setFont:[UIFont systemFontOfSize:17]];
    navTitleLabel.text=diningGuideName;
    navTitleLabel.textColor=[UIColor color333333];
    navTitleLabel.textAlignment=NSTextAlignmentCenter;
    navTitleLabel.hidden=YES;

    [_navigationView addSubview:backButton];
    [_navigationView addSubview:shareButton];
    [_navigationView addSubview:navTitleLabel];
    [self addSubview:_navigationView];
}
-(void)mapButtonTapped{
    if ( (self.delegate !=nil) && ([self.delegate respondsToSelector:@selector(mapButtonClick)]) ) {
        [self.delegate mapButtonClick];
    }
}
//-(void)addRefreshView{
//	self.refreshView=[[EGORefreshTableFooterView alloc] initWithFrame:CGRectMake(0,self.tableView.contentSize.height+10,self.tableView.frame.size.width,66)];
//	self.refreshView.delegate=self;
//	self.refreshView.backgroundColor=[UIColor whiteColor];
//	[self.tableView addSubview:self.refreshView];
//}
//
//-(void)resetRefreshViewFrame{
//	if(self.refreshView==nil){
//		[self addRefreshView];
//	}
////	if(self.frame.size.height-headerHeight>self.tableView.contentSize.height){
////		self.refreshView.frame=CGRectMake(0,self.frame.size.height+10,self.tableView.frame.size.width,66);	
////	}else{
////		self.refreshView.frame=CGRectMake(0,self.tableView.contentSize.height+10,self.tableView.frame.size.width,66);
////	}
//	
//}
//
//-(void)hideRefreshView{
//	self.refreshView.hidden=YES;
//}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexpath{
    IMGRestaurant *restaurant=[self.restaurants objectAtIndex:indexpath.row];
    return [tableView cellHeightForIndexPath:indexpath model:restaurant keyPath:@"restaurant" cellClass:[DiningGuideRestaurantsViewCell class] contentViewWidth:DeviceWidth];
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section{
	return self.restaurants.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
	return 1;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexpath{
	DiningGuideRestaurantsViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"viewedcell"];
	if(cell==nil){
		cell=[[DiningGuideRestaurantsViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"viewedcell"];
	}
	IMGRestaurant *restaurant=[self.restaurants objectAtIndex:indexpath.row];
	cell.delegate=self.delegate;
	cell.restaurant=restaurant;
	return cell;
}

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexpath{
	IMGRestaurant *restaurant=[self.restaurants objectAtIndex:indexpath.row];
    if(self.delegate && [self.delegate respondsToSelector:@selector(restaurantSelected:)]){
        [self.delegate restaurantSelected:restaurant];
	}
}



//- (void)scrollViewDidEndDragging:(UITableView*)tableView willDecelerate:(BOOL)decelerate{
//	if(tableView.contentOffset.y>tableView.contentSize.height-tableView.frame.size.height+60 && self.hasData && !self.isLoading){
//		[self.refreshView egoRefreshScrollViewDidEndDragging:tableView];
//		tableView.contentOffset=CGPointMake(0,tableView.contentSize.height-tableView.frame.size.height+66);
//		[self resetRefreshViewFrame];
//	}
//}

-(void)egoRefreshTableFooterDidTriggerRefresh:(EGORefreshTableFooterView*)refreshView{
	if([self.delegate respondsToSelector:@selector(loadRestaurantData:)]){
		__block __weak EGORefreshTableFooterView *weakRefreshView=refreshView;
		[self.delegate loadRestaurantData:^(BOOL hasData_){
            if(!hasData_){
                self.refreshView.hidden=YES;
            }
            self.hasData=hasData_;
			[weakRefreshView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
			self.refreshView.frame=CGRectMake(0,self.tableView.contentSize.height,self.tableView.frame.size.width,66);
		}];
	}
}

-(void)observeValueForKeyPath:(NSString*)keyPath ofObject:object change:(NSDictionary *)change context:(void*)context{
	if([keyPath isEqualToString:@"restaurants"]){
		[self.tableView reloadData];
	}
}

-(void)dealloc{
	[self removeObserver:self forKeyPath:@"restaurants"];
}

@end
