//
//  DiningGuideView.m
//  Qraved
//
//  Created by mac on 14-11-18.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "DiningGuideView.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Helper.h"
#import "UIImage+Resize.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "Label.h"
@implementation DiningGuideView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
UIActivityIndicatorView *_activityView;
-(id)initWithFrame:(CGRect)frame andAnimating:(BOOL)animating{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
        if (animating) {
            _activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            _activityView.frame = self.bounds;
            [self addSubview:_activityView];
            [_activityView startAnimating];

        }
    }
    return self;

}
-(id)initWithImageUrl:(NSURL*)url andTitle:(NSString*)title andNum:(NSString*)num
{
    if (self = [super init]) {
        UIImage *placeHolderImage = [UIImage imageWithColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]  size:CGSizeMake(DeviceWidth, 159)];
        
        UIImageView*_diningImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 159)];
        _diningImageView.clipsToBounds = YES;
//        [_diningImageView setImageWithURL:url placeholderImage:placeHolderImage];
        [_diningImageView sd_setImageWithURL:url placeholderImage:placeHolderImage];
        _diningImageView.contentMode = UIViewContentModeCenter;
        [self addSubview:_diningImageView];
        
        UIImageView *placeHolderImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 161)];
        [placeHolderImageView setImage:placeHolderImage];
        [self addSubview:placeHolderImageView];
        
        CGSize titleSize = [title sizeWithFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:20] constrainedToSize:CGSizeMake(DeviceWidth-40, 160)];
        CGSize numSize = [num sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] constrainedToSize:CGSizeMake(DeviceWidth-40, 160)];
        
        Label*titleLabel = [[Label alloc]initWithFrame:CGRectMake(20, 159/2-(titleSize.height+numSize.height)/2, DeviceWidth-40, titleSize.height) andTextFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:20] andTextColor:[UIColor whiteColor] andTextLines:0];
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        titleLabel.text = title;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:titleLabel];
        
        Label*numLabel = [[Label alloc]initWithFrame:CGRectMake(20, titleLabel.endPointY, DeviceWidth-40, numSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13] andTextColor:[UIColor whiteColor] andTextLines:1];
        numLabel.text = num;
        numLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:numLabel];
    }
    return self;
}

@end
