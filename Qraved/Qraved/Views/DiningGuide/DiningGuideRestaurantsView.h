//
//  DiningGuideRestaurantsView.h
//  Qraved
//
//  Created by System Administrator on 11/25/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import "EGORefreshTableFooterView.h"
@class IMGDiningGuide;
@class IMGRestaurant;

@protocol DiningGuideRestaurantsDelegate

-(void)restaurantSelected:(IMGRestaurant*)restaurant;
-(void)loadRestaurantData:(void(^)(BOOL))block;
-(void)shareButtonClick_v2:(UIButton*)btn;
-(void)returnBack;
-(void)bookNow:(IMGRestaurant*)restaurant;
-(void)mapButtonClick;
@end

@interface DiningGuideRestaurantsView : UIView<UITableViewDelegate,UITableViewDataSource,EGORefreshTableFooterDelegate>

@property(nonatomic,strong) IMGDiningGuide *diningGuide;
@property(nonatomic,strong) EGORefreshTableFooterView *refreshView;
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) UIView *navigationView;
@property(nonatomic,strong) NSMutableArray *restaurants;
@property(nonatomic,weak) NSObject<DiningGuideRestaurantsDelegate> *delegate;
@property(nonatomic,assign) BOOL hasData;;
@property(nonatomic,assign) BOOL isLoading;

-(instancetype)initWithFrame:(CGRect)frame diningGuide:(IMGDiningGuide*)diningGuide delegate:(NSObject<DiningGuideRestaurantsDelegate>*)delegate;
-(void)resetRefreshViewFrame;

@end
