//
//  HomeDiningGuideView.m
//  Qraved
//
//  Created by Lucky on 15/9/8.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import "HomeDiningGuideView.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Helper.h"
#import "UIImage+Resize.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "Label.h"
#import "UIDevice+Util.h"

#define ViewWidth    DeviceWidth/2


@implementation HomeDiningGuideView
UIActivityIndicatorView *_activityView;
-(id)initWithFrame:(CGRect)frame andAnimating:(BOOL)animating{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.2];
        if (animating) {
            _activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            _activityView.frame = self.bounds;
            [self addSubview:_activityView];
            [_activityView startAnimating];
            
        }
    }
    return self;
    
}
-(id)initWithImageUrl:(NSURL*)url andTitle:(NSString*)title andNum:(NSString*)num
{
    if (self = [super init]) {
        UIImage *placeHolderImage = [UIImage imageWithColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]  size:CGSizeMake(ViewWidth, ViewWidth)];
        NSLog(@"tilte = %@",title);
        UIImageView*_diningImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ViewWidth, ViewWidth)];
        _diningImageView.clipsToBounds = YES;
        [_diningImageView sd_setImageWithURL:url placeholderImage:placeHolderImage];
        _diningImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_diningImageView];
        
        UIImageView *placeHolderImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ViewWidth, ViewWidth+3)];
        [placeHolderImageView setImage:placeHolderImage];
        [self addSubview:placeHolderImageView];
        
        CGSize titleSize = [title sizeWithFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:14] constrainedToSize:CGSizeMake(ViewWidth-LEFTLEFTSET*2, ViewWidth) lineBreakMode:NSLineBreakByWordWrapping];
        CGSize numSize = [num sizeWithFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12] constrainedToSize:CGSizeMake(DeviceWidth-40, 160)];
        float i=1;
        if ([UIDevice isIphone6Plus]) {
            i=1.2;
        }
        if ([UIDevice isIphone6]) {
            i=1.2;
        }

        
        
        Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(20, (159/2-(titleSize.height+numSize.height)/2)*i, ViewWidth-LEFTLEFTSET*2, titleSize.height) andTextFont:[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:14] andTextColor:[UIColor whiteColor] andTextLines:0];
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        title = [title filterHtml];
        titleLabel.text = title;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:titleLabel];
        
        Label*numLabel = [[Label alloc]initWithFrame:CGRectMake(20, titleLabel.endPointY+7, titleLabel.frame.size.width, numSize.height) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12] andTextColor:[UIColor whiteColor] andTextLines:1];
        numLabel.text = num;
        numLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:numLabel];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
