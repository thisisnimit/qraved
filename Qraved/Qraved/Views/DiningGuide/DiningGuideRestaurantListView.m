
//
//  DiningGuideRestaurantListView.m
//  Qraved
//
//  Created by System Administrator on 9/24/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "DiningGuideRestaurantListView.h"
#import "DiningGuideRestaurantListViewCell.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "UIDevice+Util.h"


@implementation DiningGuideRestaurantListView{
	CGFloat bannerHeight;
}


-(instancetype)initWithFrame:(CGRect)frame diningGuide:(IMGDiningGuide*)diningGuide{
	if(self=[super initWithFrame:frame]){

		self.view.backgroundColor=[UIColor whiteColor];

		self.diningGuide=diningGuide;

		bannerHeight=298;

		[self setHeaderView];

	    _tableView=[[UITableView alloc] initWithFrame:CGRectMake(0 ,298+8,frame.size.width,frame.size.height-298-8)];
	    _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
	    _tableView.delegate=self;
	    _tableView.dataSource=self;

	    [self addObserver:self forKeyPath:@"restaurants" options:NSKeyValueObservingOptionNew context:nil];

	    [self addSubview:_tableView];
	    [self setNavigation];
	}
    return self;
}

-(void)setHeaderView{

	NSURL *urlString = [NSURL URLWithString:[self.diningGuide.headerImage returnFullImageUrlWithWidth:DeviceWidth]];

    UIImageView *introView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, bannerHeight)];

    UIImage *placeHolderImage = [[UIImage imageNamed:DEFAULT_IMAGE_STRING2 ] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth, 159)];
    [introView setImageWithURL:urlString placeholderImage:placeHolderImage];
    
    // UIImageView *bgImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, bannerHeight)];
    // [bgImage setImageWithURL:nil placeholderImage:[UIImage imageNamed:@"BgPhoto.png"]];
    // [introView addSubview:bgImage];
    // 
    UIFont *titleFont=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:24];
    CGSize titleSize=[self.diningGuide.pageName sizeWithFont:titleFont constrainedToSize:CGSizeMake(DeviceWidth,titleFont.lineHeight*2) lineBreakMode:NSLineBreakByWordWrapping];
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(30,45,DeviceWidth-60,titleSize.height)];
    titleLabel.backgroundColor=[UIColor clearColor];
    titleLabel.font=titleFont;
    titleLabel.textAlignment=NSTextAlignmentCenter;
    titleLabel.text=self.diningGuide.pageName;
    titleLabel.lineBreakMode=NSLineBreakByWordWrapping;
    titleLabel.numberOfLines=2;
    titleLabel.textColor=[UIColor whiteColor];


    NSString *content=[[self.diningGuide.pageContent removeHTML] stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    UIFont *contentFont=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    CGSize contentSize=[content sizeWithFont:contentFont constrainedToSize:CGSizeMake(DeviceWidth-30,110) lineBreakMode:NSLineBreakByWordWrapping];
    UILabel *contentLabel=[[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET,titleLabel.endPointY,DeviceWidth-LEFTLEFTSET*2,contentSize.height)];
    contentLabel.text=content;
    contentLabel.textAlignment=NSTextAlignmentCenter;
    contentLabel.backgroundColor=[UIColor clearColor];
    contentLabel.textColor=[UIColor whiteColor];
    contentLabel.numberOfLines=0;
    contentLabel.lineBreakMode=NSLineBreakByWordWrapping;

    
    UIImageView *lineImage = [[UIImageView alloc] initWithFrame:CGRectMake(LEFTLEFTSET, bannerHeight-51, DeviceWidth-2*LEFTLEFTSET, 1)];
    lineImage.backgroundColor = [UIColor color6B6B6B];
    
    UILabel *restaurantCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, lineImage.endPointY, DeviceWidth-40, 50)];
    restaurantCountLabel.backgroundColor = [UIColor clearColor];
    restaurantCountLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
    restaurantCountLabel.textAlignment = NSTextAlignmentCenter;
    restaurantCountLabel.text = [NSString stringWithFormat:L(@"%@ restaurants"),self.diningGuide.restaurantCount];
    restaurantCountLabel.textColor = [UIColor whiteColor];
    
    UIImageView *shadowImage = [[UIImageView alloc] initShadowImageViewWithShadowOriginY:bannerHeight andHeight:8];
    
    [introView addSubview:titleLabel];
    [introView addSubview:contentLabel];
    [introView addSubview:lineImage];
    [introView addSubview:restaurantCountLabel];
    [self addSubview:introView];
    [self addSubview:shadowImage];
}

-(void)setNavigation{
	self.navigationBar.backgroundColor=[UIColor clearColor];
	UIBarButtonItem leftBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:NavigationBackImage] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
	UIBarButtonItem rightBtn=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:NavigationShareImage] style:UIBarButtonItemStylePlain target:self action:nil];
	self.navigationBar.leftBarButtonItem=leftBtn;
	self.navigationBar.rightBarButtonItem=rightBtn;
}

-(void)addRefreshView{
	self.refreshView=[[EGORefreshTableFooterView alloc] initWithFrame:CGRectMake(0,self.tableView.contentSize.height+10,self.tableView.frame.size.width,66)];
	self.refreshView.delegate=self;
	self.refreshView.backgroundColor=[UIColor whiteColor];
	[self.tableView addSubview:self.refreshView];
}

-(void)resetRefreshViewFrame{
	if(self.refreshView==nil){
		[self addRefreshView];
	}
	if(self.frame.size.height-searchBar.endPointY>self.tableView.contentSize.height){
		self.refreshView.frame=CGRectMake(0,self.frame.size.height+10,self.tableView.frame.size.width,66);	
	}else{
		self.refreshView.frame=CGRectMake(0,self.tableView.contentSize.height+10,self.tableView.frame.size.width,66);
	}
	
}

-(void)hideRefreshView{
	self.refreshView.hidden=YES;
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexpath{
	return 50;
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section{
	return self.restaurants.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
	return 1;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexpath{
	RestaurantsSelectionViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"viewedcell"];
	if(cell==nil){
		cell=[[RestaurantsSelectionViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"viewedcell"];
	}
	cell.title=[[self.restaurants objectAtIndex:indexpath.row] objectForKey:@"title"];
	cell.cuisine=[[self.restaurants objectAtIndex:indexpath.row] objectForKey:@"cuisine"];
	return cell;
}

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexpath{
	NSMutableDictionary *selected=[[self.restaurants objectAtIndex:indexpath.row] mutableCopy];
	IMGRestaurant *restaurant=[[IMGRestaurant alloc] init];
	restaurant.restaurantId=[selected objectForKey:@"restaurantId"];
	restaurant.title=[selected objectForKey:@"title"];
	RestaurantsSelectionViewCell *cell=[tableView cellForRowAtIndexPath:indexpath];
    if(self.delegate && [self.delegate respondsToSelector:@selector(pickPhotosWithRestaurant:block:)]){
        [self.delegate pickPhotosWithRestaurant:restaurant block:^(BOOL succeed){
        	if(succeed){
			}
        }];
	}
}

-(void)scrollViewDidScroll:(UITableView*)tableView{
	[self.refreshView egoRefreshScrollViewDidScroll:tableView];
	[searchBar resignFirstResponder];
}

- (void)scrollViewDidEndDragging:(UITableView*)tableView willDecelerate:(BOOL)decelerate{
	if(tableView.contentOffset.y>tableView.contentSize.height-tableView.frame.size.height+60){
		[self.refreshView egoRefreshScrollViewDidEndDragging:tableView];
		tableView.contentOffset=CGPointMake(0,tableView.contentSize.height-tableView.frame.size.height+66);
		[self resetRefreshViewFrame];
	}
}

-(void)egoRefreshTableFooterDidTriggerRefresh:(EGORefreshTableFooterView*)refreshView{
	if([self.delegate respondsToSelector:@selector(loadRestaurantData:block:)]){
		__block __weak EGORefreshTableFooterView *weakRefreshView=refreshView;
		[self.delegate loadRestaurantData:searchBar.text block:^(BOOL _hasData){
            if(!_hasData){
                self.refreshView.hidden=YES;
            }
			[weakRefreshView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
			self.refreshView.frame=CGRectMake(0,self.tableView.contentSize.height,self.tableView.frame.size.width,66);
		}];
	}
}

-(void)observeValueForKeyPath:(NSString*)keyPath ofObject:object change:(NSDictionary *)change context:(void*)context{
	if([keyPath isEqualToString:@"restaurants"]){
		[self.tableView reloadData];
	}
}

-(void)dealloc{
	[self removeObserver:self forKeyPath:@"restaurants"];
}

@end
