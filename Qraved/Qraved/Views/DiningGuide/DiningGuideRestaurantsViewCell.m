//
//  DiningGuideRestaurantListViewCell.m
//  Qraved
//
//  Created by System Administrator on 11/25/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import "DiningGuideRestaurantsViewCell.h"
#import "DLStarRatingControl.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
#import "UIColor+Helper.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+Helper.h"
#import "UIConstants.h"
#import "IMGRestaurant.h"
#import "GoFoodBadgeForViewedView.h"
#define CELL_CONTENT_OFFSET 15

@implementation DiningGuideRestaurantsViewCell{
	UIImageView *restaurantImageView;
	DLStarRatingControl *rate;
	UILabel *titleLabel;
	UILabel *cuisineDistrictLabel;
	UIImageView *reviewImageView;
	UILabel *reviewLable;
	UILabel *distanceLabel;
	UIImageView *lineImageView;
	UILabel *discountLabel;
	UIView *bookView;
    UILabel *lblWellKnow;
    UIButton *btnSaved;
    GoFoodBadgeForViewedView *goFoodBadgeView;
}


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
	if(self=[super initWithStyle:style reuseIdentifier:identifier]){
		self.selectionStyle=UITableViewCellSelectionStyleNone;

        [self createUI];
	}
	return self;
}
- (void)createUI{
    restaurantImageView = [UIImageView new];
    
    btnSaved = [UIButton new];
    [btnSaved addTarget:self action:@selector(btnSavedClick:) forControlEvents:UIControlEventTouchUpInside];
    
    titleLabel = [UILabel new];
    titleLabel.font = [UIFont systemFontOfSize:14];
    titleLabel.textColor = [UIColor color333333];
    
    rate=[[DLStarRatingControl alloc] initWithFrame:CGRectMake(CELL_CONTENT_OFFSET,0, 73, 14) andStars:5 andStarWidth:13 isFractional:YES spaceWidth:2];
    rate.userInteractionEnabled=NO;
    
    reviewLable = [UILabel new];
    reviewLable.textColor = [UIColor color999999];
    reviewLable.font = [UIFont systemFontOfSize:12];
    
    cuisineDistrictLabel = [UILabel new];
    cuisineDistrictLabel.font = [UIFont systemFontOfSize:12];
    cuisineDistrictLabel.textColor = [UIColor color999999];
    
    lblWellKnow = [UILabel new];
    lblWellKnow.font = [UIFont systemFontOfSize:12];
    lblWellKnow.textColor = [UIColor color999999];
    
    [self.contentView sd_addSubviews:@[restaurantImageView,btnSaved,titleLabel,rate,reviewLable,cuisineDistrictLabel,lblWellKnow]];
    
    goFoodBadgeView = [[GoFoodBadgeForViewedView alloc] init];
    goFoodBadgeView.hidden = YES;
    [restaurantImageView addSubview:goFoodBadgeView];
    
    restaurantImageView.sd_layout
    .topSpaceToView(self.contentView,0)
    .leftSpaceToView(self.contentView,15)
    .rightSpaceToView(self.contentView,15)
    .heightIs((DeviceWidth-30)*188/345);
    
    goFoodBadgeView.sd_layout
    .bottomSpaceToView(restaurantImageView, 10)
    .leftSpaceToView(restaurantImageView, 10)
    .widthIs(155)
    .heightIs(18);
    
    btnSaved.sd_layout
    .rightSpaceToView(self.contentView,15)
    .topSpaceToView(self.contentView,0)
    .widthIs(40)
    .heightIs(34);
    
    titleLabel.sd_layout
    .topSpaceToView(restaurantImageView,5)
    .leftEqualToView(restaurantImageView)
    .rightEqualToView(restaurantImageView)
    .heightIs(16);
    
    rate.sd_layout
    .topSpaceToView(titleLabel,5)
    .leftEqualToView(titleLabel)
    .widthIs(73)
    .heightIs(14);
    
    reviewLable.sd_layout
    .topEqualToView(rate)
    .leftSpaceToView(rate,5)
    .rightEqualToView(titleLabel)
    .heightIs(14);
    
    cuisineDistrictLabel.sd_layout
    .topSpaceToView(rate,5)
    .leftEqualToView(titleLabel)
    .rightEqualToView(titleLabel)
    .heightIs(14);
    
    lblWellKnow.sd_layout
    .topSpaceToView(cuisineDistrictLabel, 5)
    .leftEqualToView(cuisineDistrictLabel)
    .rightEqualToView(cuisineDistrictLabel)
    .heightIs(14);
    
}
- (void)setRestaurant:(IMGRestaurant *)restaurant{
    _restaurant = restaurant;
    
    titleLabel.text = restaurant.title;
    
    UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-30, (DeviceWidth-30)*188/345)];
    [restaurantImageView sd_setImageWithURL:[NSURL URLWithString:[restaurant.imageUrl returnFullImageUrlWithWidth:DeviceWidth-30 andHeight:(DeviceWidth-30)*188/345]] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        //restaurantImageView.image = image;
    }];
    
    if ([Tools isBlankString:restaurant.goFoodLink]) {
        goFoodBadgeView.hidden = YES;
    }else{
        goFoodBadgeView.hidden = NO;
    }
    
    float score = [restaurant.ratingScore floatValue];
    int scoreInt =(int)roundf(score);
    [rate updateRating:[NSNumber numberWithFloat:scoreInt]];

    
    NSString *distance = [restaurant distance];
    NSString *ratingStr = @"";
    if (restaurant.ratingCount != nil && ![restaurant.ratingCount isEqual:@0]) {
        ratingStr = [NSString stringWithFormat:@"(%@) •",restaurant.ratingCount];
    }
    
    if (![distance isEqualToString:@""]) {
        reviewLable.text = [NSString stringWithFormat:@"%@ %@",ratingStr, distance];
    }else{
        reviewLable.text = ratingStr;
    }
    
    if ([restaurant.cuisineName isEqualToString:@""]) {
        cuisineDistrictLabel.text = [NSString stringWithFormat:@"%@ • %@",restaurant.districtName,restaurant.priceName];
    }else{
        cuisineDistrictLabel.text = [NSString stringWithFormat:@"%@ • %@ • %@",restaurant.cuisineName,restaurant.districtName,restaurant.priceName];
    }
    
    if ([restaurant.wellKnownFor isEqualToString:@""]) {
        lblWellKnow.text = @"";
        [self setupAutoHeightWithBottomView:cuisineDistrictLabel bottomMargin:30];
    }else{
        lblWellKnow.text  = [restaurant.wellKnownFor filterHtml];
        [self setupAutoHeightWithBottomView:lblWellKnow bottomMargin:30];
    }
    
    if ([restaurant.saved isEqual:@1]) {
        [btnSaved setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
    }else{
        [btnSaved setImage:[UIImage imageNamed:@"ic_home_heart"] forState:UIControlStateNormal];
    }
    
}

- (void)btnSavedClick:(UIButton *)button{
    if (self.savedClick) {
        self.savedClick(self.restaurant,self.indexPath);
    }
}

-(void)bookNow{
	if(self.delegate && [self.delegate respondsToSelector:@selector(bookNow:)]){
		[self.delegate bookNow:self.restaurant];
	}
}

@end
