//
//  HomeDiningGuideView.h
//  Qraved
//
//  Created by Lucky on 15/9/8.
//  Copyright (c) 2015年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeDiningGuideView : UIView
-(id)initWithImageUrl:(NSURL*)url andTitle:(NSString*)title andNum:(NSString*)num;
-(id)initWithFrame:(CGRect)frame andAnimating:(BOOL)animating;

@end
