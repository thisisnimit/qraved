//
//  DiningGuideRestaurantListView.h
//  Qraved
//
//  Created by System Administrator on 11/25/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import "EGORefreshTableFooterView.h"

@interface DiningGuideRestaurantListView : UIView<UITableViewDelegate,UITableViewDataSource,EGORefreshTableFooterDelegate>

@property(nonatomic,strong) IMGDiningGuide *diningGuide;
@property(nonatomic,strong) EGORefreshTableFooterView *refreshView;
@property(nonatomic,strong) UITableView *tableView;

@end
