//
//  DiningGuideRestaurantsViewCell.h
//  Qraved
//
//  Created by System Administrator on 11/25/15.
//  Copyright © 2015 Imaginato. All rights reserved.
//

#import "DiningGuideRestaurantsView.h"

@class IMGRestaurant;

@interface DiningGuideRestaurantsViewCell : UITableViewCell{

}
@property(nonatomic,strong) IMGRestaurant *restaurant;

@property(nonatomic,assign) BOOL canBook;

@property(nonatomic, copy) void (^ savedClick)(IMGRestaurant *restaurant,NSIndexPath *indexPath);

@property(nonatomic, strong) NSIndexPath *indexPath;

@property(nonatomic,weak) NSObject<DiningGuideRestaurantsDelegate> *delegate;

-(void)resetFrame;
-(void)setRatingCountWithRating:(NSNumber*)ratingCount_;

@end
