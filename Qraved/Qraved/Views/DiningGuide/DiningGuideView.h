//
//  DiningGuideView.h
//  Qraved
//
//  Created by mac on 14-11-18.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface DiningGuideView : UIView
-(id)initWithImageUrl:(NSURL*)url andTitle:(NSString*)title andNum:(NSString*)num;
-(id)initWithFrame:(CGRect)frame andAnimating:(BOOL)animating;
@end
