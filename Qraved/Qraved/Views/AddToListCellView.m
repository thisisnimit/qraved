//
//  AddToListCellView.m
//  Qraved
//
//  Created by System Administrator on 9/25/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "AddToListCellView.h"
#import "UIConstants.h"
#import "UIView+Helper.h"

@implementation AddToListCellView{
	UILabel *titleLabel;
	UIButton *saveBtn;
	CGFloat image_width;
    UIImageView *restaurantImageView;
}

@synthesize title;
@synthesize image;
@synthesize inList;



-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
	if(self=[super initWithStyle:style reuseIdentifier:identifier]){
		CGFloat padding_top=20;
		image_width=32;
		self.selectionStyle=UITableViewCellSelectionStyleNone;
        restaurantImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 2, self.size.height-4, self.size.height-4)];
        [self addSubview:restaurantImageView];
		titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(restaurantImageView.endPointX+5,4,DeviceWidth-LEFTLEFTSET*2-image_width,self.size.height-4-8)];
		titleLabel.font=[UIFont systemFontOfSize:20];
		titleLabel.numberOfLines=1;
		saveBtn=[[UIButton alloc] init];
        saveBtn.backgroundColor=[UIColor clearColor];
        saveBtn.userInteractionEnabled=NO;
        UIImageView *line = [[UIImageView alloc] initWithFrame:CGRectMake(0,titleLabel.endPointY+10,DeviceWidth-LEFTLEFTSET*2, 1)];
	    line.image = [UIImage imageNamed:@"CutOffRule"];
		[self addSubview:titleLabel];
		[self addSubview:saveBtn];
		[self addSubview:line];
		[self addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
		[self addObserver:self forKeyPath:@"image" options:NSKeyValueObservingOptionNew context:nil];
		[self addObserver:self forKeyPath:@"inList" options:NSKeyValueObservingOptionNew context:nil];
        
	}
	return self;
}

-(void)observeValueForKeyPath:(NSString*)keypath ofObject:object change:(NSDictionary*)change context:(void*)context{
	if([keypath isEqualToString:@"title"]){
		titleLabel.text=[change objectForKey:@"new"];
	}else if([keypath isEqualToString:@"image"]){
		UIImage *image_=[UIImage imageNamed:[change objectForKey:@"new"]];
		[saveBtn setImage:[UIImage imageNamed:[change objectForKey:@"new"]] forState:UIControlStateNormal];
		saveBtn.frame=CGRectMake(DeviceWidth-LEFTLEFTSET*2-image_.size.width,5,image_.size.width,35);
	}else if([keypath isEqualToString:@"inList"]){
		UIImage *image_=nil;
		if([[change objectForKey:@"new"] boolValue]){
			image_=[UIImage imageNamed:@"save_list_"];
		}else{
			image_=[UIImage imageNamed:@"unsave_list_"];
		}
		[saveBtn setImage:image_ forState:UIControlStateNormal];
		saveBtn.frame=CGRectMake(DeviceWidth-LEFTLEFTSET*2-image_width,(self.frame.size.height-image_.size.height)/2,image_width,image_.size.height);
	}
}
-(void)setImageUrl:(NSString*)imageUrl{
    if (restaurantImageView) {
        [restaurantImageView removeFromSuperview];
    }
    if (inList) {
        saveBtn.hidden = YES;
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth-145, 4, 130, self.size.height-8)];
        label.text = @"Already added";
        label.textColor = [UIColor grayColor];
        label.font = [UIFont systemFontOfSize:17];
        [self addSubview:label];
    }

    restaurantImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 2, self.size.height-4, self.size.height-4)];
    [self addSubview:restaurantImageView];
    NSArray *images=[imageUrl componentsSeparatedByString:@","];
    if(imageUrl&& imageUrl.length && images.count>0){
        [restaurantImageView sd_setImageWithURL:[NSURL URLWithString:[images[0] returnFullImageUrlWithWidth:self.size.height-4]] placeholderImage:[[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake(self.size.height-4, self.size.height-4)]];
    }else{
        restaurantImageView.image = [[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake(self.size.height-4, self.size.height-4)];
    }
}
-(void)dealloc{
	[self removeObserver:self forKeyPath:@"title"];
	[self removeObserver:self forKeyPath:@"image"];
	[self removeObserver:self forKeyPath:@"inList"];
}

@end
