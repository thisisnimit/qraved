//
//  JournalGoFoodListView.h
//  Qraved
//
//  Created by harry on 2018/4/8.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JournalGoFoodListView : UIView
@property (nonatomic, strong) id controller;
@property (nonatomic, strong) NSNumber *journalId;
@property (nonatomic, strong) NSArray *restaurantArray;
@property (nonatomic, copy) void (^ freshRestaurantList)(IMGRestaurant *restaurant);

@end
