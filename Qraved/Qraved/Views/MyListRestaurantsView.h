//
//  MyListRestaurantsView.h
//  Qraved
//
//  Created by System Administrator on 10/14/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "MyListDelegate.h"
#import "EGORefreshTableFooterView.h"
#import "SavedTableViewCell.h"

@class IMGMyList;

@interface MyListRestaurantsView : UIView<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,EGORefreshTableFooterDelegate,SavedCellDelegate>

@property(nonatomic,strong) IMGMyList *mylist;
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,assign) BOOL hasData;
@property(nonatomic,strong) NSMutableArray *restaurants;
@property(nonatomic,weak) NSObject<MyListDelegate> *delegate;
@property(nonatomic,strong) EGORefreshTableFooterView *refreshView;
@property(nonatomic,assign) BOOL isOtherUser;

@property (copy, nonatomic) void (^refreshList)(NSNumber *restaurantId);

-(instancetype)initWithFrame:(CGRect)frame list:(IMGMyList*)mylist;
-(void)setRefreshViewFrame;

@end
