//
//  ReviewPublishPhotoView.h
//  Qraved
//
//  Created by harry on 2017/7/24.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewPublishPhotoView : UIView

@property (nonatomic, strong) NSArray *photoArray;
@property (nonatomic, copy) void (^uploadPhoto)();
@property (nonatomic, copy) void (^removePhoto)(int index);

@end
