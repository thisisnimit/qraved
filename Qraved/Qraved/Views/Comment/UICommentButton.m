//
//  CommentButton.m
//  Qraved
//
//  Created by Jeff on 3/3/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "UICommentButton.h"
#import "UIView+Helper.h"

#define COMMENTBUTTONTAPPEDICON   @"ic_comment.png"

@implementation UICommentButton

-(id)initInSuperView:(UIView*)parentView{
    self = [super initInSuperView:parentView tag:1];
    if (self) {
        [self.textLabel setText:L(@"Comment")];
        [self.textLabel sizeToFit];
        self.iconView = [[UIImageView alloc]initWithFrame:CGRectMake((self.frame.size.width-COMMUNITYBUTTONICONSIZE-COMMUNITYBUTTONGAP-self.textLabel.frame.size.width)/2, (self.frame.size.height-COMMUNITYBUTTONICONSIZE)/2, COMMUNITYBUTTONICONSIZE, COMMUNITYBUTTONICONSIZE)];
        [self addSubview:self.iconView];        
        self.textLabel.frame=CGRectMake(self.iconView.endPointX+COMMUNITYBUTTONGAP, (self.frame.size.height-self.textLabel.frame.size.height)/2, self.textLabel.frame.size.width, self.textLabel.frame.size.height);
        
        [self.textLabel setTextColor:COMMUNITYBUTTONUNTAPPEDTITLECOLOR];
        self.iconView.image=[UIImage imageNamed:COMMENTBUTTONTAPPEDICON];

    }
    return self;
}
-(id)initInSuperView:(UIView*)parentView commentCount:(int)commentCount isFromPhotoViewer:(BOOL)isFromPhotoViewer{
    self = [super initInSuperView:parentView tag:1];
    if (self) {
        if (commentCount>1) {
            [self.textLabel setText:[NSString stringWithFormat:@"%d Comments",commentCount]];
        }else if (commentCount==1){
            [self.textLabel setText:[NSString stringWithFormat:@"%d Comment",commentCount]];
        }else{
            [self.textLabel setText:L(@"Comment")];
        }
        
        [self.textLabel sizeToFit];
        self.iconView = [[UIImageView alloc]initWithFrame:CGRectMake((self.frame.size.width-COMMUNITYBUTTONICONSIZE-COMMUNITYBUTTONGAP-self.textLabel.frame.size.width)/2, (self.frame.size.height-COMMUNITYBUTTONICONSIZE)/2, COMMUNITYBUTTONICONSIZE, COMMUNITYBUTTONICONSIZE)];
        [self addSubview:self.iconView];
        self.textLabel.frame=CGRectMake(self.iconView.endPointX+COMMUNITYBUTTONGAP, (self.frame.size.height-self.textLabel.frame.size.height)/2, self.textLabel.frame.size.width, self.textLabel.frame.size.height);
        
        [self.textLabel setTextColor:isFromPhotoViewer?[UIColor whiteColor]:COMMUNITYBUTTONUNTAPPEDTITLECOLOR];
        self.iconView.image=[UIImage imageNamed:isFromPhotoViewer?@"ic_comment_white":COMMENTBUTTONTAPPEDICON];
        
    }
    return self;
}

@end
