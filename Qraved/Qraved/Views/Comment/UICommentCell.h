//
//  UICommentCell.h
//  Qraved
//
//  Created by Jeff on 3/6/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGJournalComment.h"
#import "IMGDishComment.h"
#import "IMGReviewComment.h"
#import "IMGRestaurantEventComment.h"
@protocol UICommentCellDelegate <NSObject>

- (void)UICommentCell:(UITableViewCell*)tableViewCell readMoreTapped:(id)comment;
- (void)UICommentCellUserNameOrImageTapped:(id)sender;

@end

@interface UICommentCell : UITableViewCell<UIWebViewDelegate>

@property (nonatomic, assign) id<UICommentCellDelegate>commentCellDelegate;
+(CGFloat)calculatedHeight:(id)journalComment;
-(void)updateUI:(id)journalComment;

@end
