//
//  ViewPreviousCommentsButton.h
//  Qraved
//
//  Created by Jeff on 3/5/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewPreviousCommentsButton : UIButton

-(id)initWithFrame:(CGRect)frame hidden:(BOOL)hidden;

@end
