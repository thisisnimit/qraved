//
//  CommentButton.h
//  Qraved
//
//  Created by Jeff on 3/3/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICommunityButton.h"

@interface UICommentButton : UICommunityButton

-(id)initInSuperView:(UIView*)parentView commentCount:(int)commentCount isFromPhotoViewer:(BOOL)isFromPhotoViewer;
@end
