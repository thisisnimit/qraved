//
//  UICommentInputView.h
//  Qraved
//
//  Created by Jeff on 3/6/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"

#define UICOMMENTINPUTVIEWHEIGHT 45

@protocol CommentInputViewDelegate <NSObject>
@optional
- (void)commentInputView:(UIView*)view inputViewExpand:(UITextView*)inputTextView  commentInputViewYOffset:(CGFloat)commentInputViewYOffset;
- (void)commentInputView:(UIView*)view postButtonTapped:(UIButton*)postButton commentInputText:(NSString *)text;
- (void)commentInputView:(UIView*)view inputViewDidBeginEditing:(UITextView *)textView;
- (void)commentInputView:(UIView*)view inputViewShouldBeginEditing:(UITextView *)textView;
@end

@interface UICommentInputView : UIView<UITextViewDelegate,AutoPostDelegate>

@property (nonatomic, retain) UITextView *inputTextView;
@property (nonatomic, retain) UIButton *postButton;
@property (nonatomic, assign) id<CommentInputViewDelegate>commentInputViewDelegate;

-(id)initView:(BOOL)inFooter_;
-(void)clearInputTextView;
-(void)becomeFirstResponderToInputTextView;

@end
