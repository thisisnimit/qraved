//
//  UICommentCell.m
//  Qraved
//
//  Created by Jeff on 3/6/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "UICommentCell.h"
#import "UIView+Helper.h"
#import "UIConstants.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"
#import "Date.h"
#import "UILineImageView.h"
#import "UILabel+Helper.h"
#import "IMGUser.h"
#import "UIColor+Helper.h"

#define LEFTMARGIN 15
#define RIGHTMARGIN 30
#define TEXTLABELLEFTMARGIN 11+PROFILEIMAGEVIEWSIZE+8
#define TEXTLABELTOPMARGIN 35
#define PROFILEIMAGEVIEWSIZE 32
#define COMMENTTEXTLENGTH 140
#define CONTENTLABELWIDTH DeviceWidth-LEFTMARGIN-PROFILEIMAGEVIEWSIZE-RIGHTMARGIN-7
#define LINK_TEXT @"<a style=\"text-decoration:none;color:#3A8CE2;font-family:'OpenSans'\" href=\"http://www.qraved.com/\">...read more</a>"

@implementation UICommentCell{
    UIImageView *profileImageView;
    UILabel *userNameLabel;
    UILabel *timeLabel;

    BOOL viewMore;
    NSString *commentStr;
    id commentObject;
    
    UIControl *userNameLabelControl;
    UIWebView *contentLabel;
    UIView *lineView;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.userInteractionEnabled = YES;
        //[UILineImageView initInParent:self withFrame:CGRectMake(0, 0, DeviceWidth, 1)];
        
        profileImageView = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTMARGIN, 15, PROFILEIMAGEVIEWSIZE, PROFILEIMAGEVIEWSIZE)];
        profileImageView.layer.masksToBounds = YES;
        profileImageView.layer.cornerRadius=PROFILEIMAGEVIEWSIZE/2;
        profileImageView.layer.borderColor=[UIColor clearColor].CGColor;
        profileImageView.layer.borderWidth=0.0f;
        [self addSubview:profileImageView];
        
        UIControl *profileImageViewControl = [[UIControl alloc] init];
        [profileImageViewControl addTarget:self action:@selector(controlClick) forControlEvents:UIControlEventTouchUpInside];
        profileImageViewControl.frame = profileImageView.frame;
        [self addSubview:profileImageViewControl];
        
        userNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(profileImageView.endPointX+7, 23, 100, 16)];
        userNameLabel.font = [UIFont systemFontOfSize:14];
        userNameLabel.textColor=[UIColor color333333];
        [self addSubview:userNameLabel];
        
        userNameLabelControl = [[UIControl alloc] init];
        [userNameLabelControl addTarget:self action:@selector(controlClick) forControlEvents:UIControlEventTouchUpInside];
        userNameLabelControl.frame = userNameLabel.frame;
        [self addSubview:userNameLabelControl];
        
        timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(userNameLabel.endPointX+10, 23, DeviceWidth-180, 14)];
        timeLabel.font=[UIFont systemFontOfSize:12];
        timeLabel.textAlignment = NSTextAlignmentRight;
        timeLabel.textColor=[UIColor color999999];
        [self addSubview:timeLabel];
        
        lineView = [[UIView alloc] initWithFrame:CGRectMake(profileImageView.endPointX+7, timeLabel.endPointY+20, DeviceWidth-54, 1)];
        lineView.backgroundColor = [UIColor colorWithHexString:@"C1C1C1"];
        [self addSubview:lineView];
        
    }
    return self;
}
- (void)controlClick
{
    if (self.commentCellDelegate)
    {
        IMGComment *comment = (IMGComment *)commentObject;
        if (!comment.isVendor && [comment.userType intValue] == 1)
        {
            IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
            restaurant.title = comment.restaurantName;
            restaurant.restaurantId = comment.restaurantId;
            [self.commentCellDelegate UICommentCellUserNameOrImageTapped:restaurant];
        }
        else
        {
            IMGUser *user = [[IMGUser alloc] init];
            user.userName = [NSString stringWithString:comment.userName];
            user.userId = comment.userId;
            user.token = @"";
            user.avatar = comment.userImageUrl;
            [self.commentCellDelegate UICommentCellUserNameOrImageTapped:user];
        }
    }
}
-(void)updateUI:(id)comment{
    commentObject = comment;
    NSString *userName=@"";
    NSNumber *createTime=[NSNumber numberWithLongLong:[[NSDate date]timeIntervalSince1970]];
    commentStr=@"";
    NSString *imageUrl=@"";
    BOOL readMore=NO;
    userNameLabel.textColor=[UIColor color333333];

    if([comment isKindOfClass:[IMGJournalComment class]]){
        IMGJournalComment* journalComment=(IMGJournalComment*)comment;
        commentStr=journalComment.content;
        createTime=journalComment.createTime;
        userName=journalComment.userName;
        imageUrl=journalComment.userImageUrl;
        readMore=journalComment.readMore;
    }else if([comment isKindOfClass:[IMGDishComment class]]){
        IMGDishComment* dishComment=(IMGDishComment*)comment;
        commentStr=dishComment.content;
        createTime=dishComment.createTime;
        if (!dishComment.isVendor && [dishComment.userType intValue] == 1)
        {
            userName = dishComment.restaurantName;
            imageUrl = dishComment.restaurantImageUrl;
            userNameLabel.textColor=[UIColor colorRed];
        }
        else
        {
            userName=dishComment.userName;
            imageUrl=dishComment.userImageUrl;
        }
        readMore=dishComment.readMore;
    }else if([comment isKindOfClass:[IMGReviewComment class]]){
        IMGReviewComment* reviewComment=(IMGReviewComment*)comment;
        commentStr=reviewComment.content;
        createTime=reviewComment.createTime;
        if (!reviewComment.isVendor && [reviewComment.userType intValue] == 1)
        {
            userName = reviewComment.restaurantName;
            imageUrl = reviewComment.restaurantImageUrl;
            userNameLabel.textColor=[UIColor colorRed];
        }
        else
        {
            userName=reviewComment.userName;
            imageUrl=reviewComment.userImageUrl;
        }
        readMore=reviewComment.readMore;
    }else if([comment isKindOfClass:[IMGRestaurantEventComment class]]){
        IMGRestaurantEventComment* restaurantEventComment=(IMGRestaurantEventComment*)comment;
        commentStr=restaurantEventComment.content;
        createTime=restaurantEventComment.createTime;
        if (!restaurantEventComment.isVendor && [restaurantEventComment.userType intValue] == 1)
        {
            userName = restaurantEventComment.restaurantName;
            imageUrl = restaurantEventComment.restaurantImageUrl;
            userNameLabel.textColor=[UIColor colorRed];
        }
        else
        {
            userName=restaurantEventComment.userName;
            imageUrl=restaurantEventComment.userImageUrl;
        }
        readMore=restaurantEventComment.readMore;
    }
    
    
    NSURL *avatar;
    if ([imageUrl isEqualToString:DEFAULTPROFILEIMAGEURL]) {
        avatar = [NSURL URLWithString:@""];
    }else if ([imageUrl hasPrefix:@"http"]){
        avatar = [NSURL URLWithString:imageUrl];
    }else{
        avatar = [NSURL URLWithString:[imageUrl returnFullImageUrl]];
    }
    IMGComment *tempComment = (IMGComment*)comment;
    NSString *path_sandox = NSHomeDirectory();
    //设置一个图片的存储路径
    NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/avastar.png"];
    UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
    if (image&&[tempComment.userId intValue] == [[IMGUser currentUser].userId intValue]) {
        profileImageView.image = image;
    }else{
        
        [profileImageView sd_setImageWithURL:avatar placeholderImage:[UIImage imageNamed:@"headDefault.jpg"]];
    }
    profileImageView.layer.masksToBounds = YES;

    profileImageView.layer.cornerRadius=PROFILEIMAGEVIEWSIZE/2;
    profileImageView.layer.borderColor=[UIColor clearColor].CGColor;
    profileImageView.layer.borderWidth=0.0f;
    
    userNameLabel.text=[userName filterHtml];
    userNameLabel.frame = CGRectMake(userNameLabel.frame.origin.x, userNameLabel.frame.origin.y, userNameLabel.expectedWidth, userNameLabel.frame.size.height);
    userNameLabelControl.frame = userNameLabel.frame;
    timeLabel.text = [Date getTimeInteval_v4:[createTime longLongValue]/1000];
    
    NSDictionary *strBackDic = [UICommentCell getIsNeedLoadMoreForOverLines:commentStr];
    float contentLabelHeight;
    commentStr=[commentStr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
    NSString *tempStr = @"1\n2\n3\n4";
    CGFloat tempHeight = [UICommentCell calculateCommentTextHeight:tempStr];

    if([strBackDic[@"isNeedLoadMore"] boolValue]&&!readMore){
        commentStr= strBackDic[@"finalStr"];
        contentLabelHeight = [UICommentCell calculateCommentTextHeight:commentStr];
        if (contentLabelHeight<tempHeight) {
            contentLabelHeight = tempHeight;
        }
    }else{
        if(!readMore && commentStr.length>COMMENTTEXTLENGTH){
//            commentStr=[NSString stringWithFormat:@"%@<a href=\"http://www.qraved.com/\">...read more</a>",[commentStr substringToIndex:COMMENTTEXTLENGTH]];
            
            commentStr=[NSString stringWithFormat:@"%@%@",[commentStr substringToIndex:COMMENTTEXTLENGTH],LINK_TEXT];
            contentLabelHeight = [UICommentCell calculateCommentTextHeight:commentStr];

        }else{
        contentLabelHeight = [UICommentCell calculateCommentTextHeight:commentStr];
        }
    }
    contentLabel = (UIWebView *)[self viewWithTag:711];
    if (!contentLabel) {
        contentLabel = [[UIWebView alloc] initWithFrame:CGRectMake(profileImageView.endPointX+7, userNameLabel.endPointY+7, CONTENTLABELWIDTH, 10)];
        contentLabel.tag = 711;
        contentLabel.delegate = self;
        contentLabel.scrollView.bounces = NO;
        contentLabel.backgroundColor=[UIColor whiteColor];
        contentLabel.scrollView.showsHorizontalScrollIndicator = NO;
        contentLabel.scrollView.scrollEnabled = YES;
        contentLabel.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        //        contentLabel.scalesPageToFit = YES;
        //        [contentLabel sizeToFit]
        [self addSubview:contentLabel];
    }
    
    contentLabel.frame = CGRectMake(profileImageView.endPointX+7, userNameLabel.endPointY+7, CONTENTLABELWIDTH, contentLabelHeight);
    contentLabel.backgroundColor=[UIColor clearColor];
    NSString *summarizeStr = [NSString stringWithFormat:@"<html> <head> <meta charset=\"UTF-8\">"
                              "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">"
                              "<style type=\"text/css\">"
                              "html {"
                              "    -webkit-text-size-adjust: none; /* Never autoresize text */"
                              "}"
                              "body{font-family:\"%@\";font-size: 14px; color:%@; padding:0; margin:0; width:%fpx; line-height: 16px;word-wrap:break-word;}"
                              "</style>"
                              "</head>"
                              "<body><div id=\"content\">%@</div></body>"
                              "</html>",DEFAULT_FONT_NAME,[UIColor color333333],CONTENTLABELWIDTH,commentStr];
    
    summarizeStr = [summarizeStr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
    [contentLabel loadHTMLString:summarizeStr baseURL:nil];
    [contentLabel layoutSubviews];
    lineView.frame = CGRectMake(profileImageView.endPointX+7, contentLabel.endPointY+15, DeviceWidth-54, 1);
    
}

#pragma mark - webview delegate

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    CGFloat htmlHeight = [[webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"] floatValue];
    webView.frame = CGRectMake(profileImageView.endPointX+7, userNameLabel.endPointY+7, CONTENTLABELWIDTH, htmlHeight);
    
    
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if (navigationType==UIWebViewNavigationTypeLinkClicked) {
        
        if([commentObject isKindOfClass:[IMGJournalComment class]]){
            IMGJournalComment *journalComment=(IMGJournalComment*)commentObject;
            journalComment.readMore=!journalComment.readMore;
        }else if([commentObject isKindOfClass:[IMGDishComment class]]){
            IMGDishComment *dishComment=(IMGDishComment*)commentObject;
            dishComment.readMore=!dishComment.readMore;
        }else if([commentObject isKindOfClass:[IMGReviewComment class]]){
            IMGReviewComment *reviewComment=(IMGReviewComment*)commentObject;
            reviewComment.readMore=!reviewComment.readMore;
        }else if([commentObject isKindOfClass:[IMGRestaurantEventComment class]]){
            IMGRestaurantEventComment *eventComment=(IMGRestaurantEventComment*)commentObject;
            eventComment.readMore=!eventComment.readMore;
        }
        if(self.commentCellDelegate){
            [self.commentCellDelegate UICommentCell:self readMoreTapped:commentObject];
        }
        
        return NO;
    }
    
    return YES;
    
    
}


-(BOOL)calculatedCommentTextHeight:(NSString*)comment{
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:17]};
    CGSize singleSize = [@"test" boundingRectWithSize:CGSizeMake(DeviceWidth-LEFTMARGIN-PROFILEIMAGEVIEWSIZE-RIGHTMARGIN, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    CGSize textSize = [comment boundingRectWithSize:CGSizeMake(DeviceWidth-LEFTMARGIN-PROFILEIMAGEVIEWSIZE-RIGHTMARGIN, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    NSInteger lines = textSize.height/singleSize.height;
    if(lines>10){
        return YES;
    }else{
        return NO;
    }
}

+(CGFloat)calculatedHeight:(id)comment{
    
    NSString *commentStr_=@"";
    BOOL readMore=NO;
    if([comment isKindOfClass:[IMGJournalComment class]]){
        IMGJournalComment *journalComment=(IMGJournalComment*)comment;
        commentStr_=journalComment.content;
        readMore=journalComment.readMore;
    }else if([comment isKindOfClass:[IMGDishComment class]]){
        IMGDishComment *dishComment=(IMGDishComment*)comment;
        commentStr_=dishComment.content;
        readMore=dishComment.readMore;
    }else if([comment isKindOfClass:[IMGReviewComment class]]){
        IMGReviewComment *reviewComment=(IMGReviewComment*)comment;
        commentStr_=reviewComment.content;
        readMore=reviewComment.readMore;
    }else if([comment isKindOfClass:[IMGRestaurantEventComment class]]){
        IMGRestaurantEventComment *eventComment=(IMGRestaurantEventComment*)comment;
        commentStr_=eventComment.content;
        readMore=eventComment.readMore;
    }
    
    commentStr_ = [commentStr_ stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
    NSString *tempStr = @"1\n2\n3\n4";
    CGFloat tempHeight = [UICommentCell calculateCommentTextHeight:tempStr];
    float contentLabelHeight = [UICommentCell calculateCommentTextHeight:commentStr_];
    NSDictionary *strBackDic = [UICommentCell getIsNeedLoadMoreForOverLines:commentStr_];
    
    if([strBackDic[@"isNeedLoadMore"] boolValue]&&!readMore){
        commentStr_= strBackDic[@"finalStr"];
        contentLabelHeight = [UICommentCell calculateCommentTextHeight:commentStr_];
  //      contentLabelHeight = tempHeight;
        if (contentLabelHeight<tempHeight) {
            contentLabelHeight=tempHeight;
        }
    }else{
        contentLabelHeight = [UICommentCell calculateCommentTextHeight:commentStr_];

    }

    if (!readMore && contentLabelHeight > tempHeight ) {
        contentLabelHeight = tempHeight;
    }else{
        
        if(!readMore && commentStr_.length>COMMENTTEXTLENGTH&&![strBackDic[@"isNeedLoadMore"] boolValue]){
            commentStr_=[NSString stringWithFormat:@"%@%@",[commentStr_ substringToIndex:COMMENTTEXTLENGTH],LINK_TEXT];
            contentLabelHeight = [UICommentCell calculateCommentTextHeight:commentStr_];

        }
        
   
    }
    
    
    return contentLabelHeight+47+15;
}

+(CGFloat)calculateCommentTextHeight:(NSString*)commentStr_{
    commentStr_=[commentStr_ stringByReplacingOccurrencesOfString:@"<br/>" withString:@"\n"];

    commentStr_=[commentStr_ stringByReplacingOccurrencesOfString:LINK_TEXT withString:@"...read more"];

//    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12.1]};
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:14]};

    CGSize textSize = [commentStr_ boundingRectWithSize:CGSizeMake(CONTENTLABELWIDTH, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    

    float height = ceil(textSize.height + 1.5f);
    return height;
}


+(NSDictionary *)getIsNeedLoadMoreForOverLines:(NSString*)commentStr_{

    UILabel *lblTempStr = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, CONTENTLABELWIDTH, MAXFLOAT)];
    lblTempStr.text = commentStr_;
    lblTempStr.font = [UIFont systemFontOfSize:14];
    
    
    NSString *text = [lblTempStr text];
    UIFont   *font = [lblTempStr font];
    CGRect    rect = [lblTempStr frame];
    
    
    CTFontRef myFont = CTFontCreateWithName((CFStringRef)font.fontName,
                                            font.pointSize,
                                            NULL);
    
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:text];
    [attStr addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)myFont range:NSMakeRange(0, attStr.length)];
    
    CTFramesetterRef frameSetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)attStr);
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, CGRectMake(0,0,rect.size.width,100000));
    
    CTFrameRef frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, 0), path, NULL);
    
    NSArray *lines = (__bridge NSArray *)CTFrameGetLines(frame);
    
    NSMutableArray *linesArray = [[NSMutableArray alloc]init];
    
    NSMutableDictionary *strBackDic = [[NSMutableDictionary alloc]init];
    
    if(lines.count<4+1){
        [strBackDic setObject:@NO forKey:@"isNeedLoadMore"];
    }else{
        [strBackDic setObject:@YES forKey:@"isNeedLoadMore"];
        for (id line in lines)
        {
            CTLineRef lineRef = (__bridge CTLineRef)line;
            CFRange lineRange = CTLineGetStringRange(lineRef);
            NSRange range = NSMakeRange(lineRange.location, lineRange.length);
            
            NSString *lineString = [text substringWithRange:range];
            
//            CFRelease(lineRef);
            [linesArray addObject:lineString];
            
        }
        
        //start range work for 4th line
        NSMutableAttributedString *attStr4Th = [[NSMutableAttributedString alloc] initWithString:linesArray[4-1]];
        [attStr4Th addAttribute:(NSString *)kCTFontAttributeName value:(__bridge id)myFont range:NSMakeRange(0, attStr4Th.length)];
        CTFramesetterRef frameSetter4Th = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef)attStr4Th);
        
        CGMutablePathRef path4Th = CGPathCreateMutable();
        CGPathAddRect(path4Th, NULL, CGRectMake(0,0,CONTENTLABELWIDTH-70,MAXFLOAT));
        
        CTFrameRef frame4Th = CTFramesetterCreateFrame(frameSetter4Th, CFRangeMake(0, 0), path4Th, NULL);
        
        NSArray *lines4Th = (__bridge NSArray *)CTFrameGetLines(frame4Th);
        CTLineRef lineRef4Th = (__bridge CTLineRef )[lines4Th firstObject];
        CFRange lineRange4Th = CTLineGetStringRange(lineRef4Th);
        NSRange range4Th = NSMakeRange(lineRange4Th.location, lineRange4Th.length);
        NSString *lineString4Th = [linesArray[4-1] substringWithRange:range4Th];
        
        NSString *finalStr = @"";
        for(int i=0; i<4-1; i++){
            finalStr = [finalStr stringByAppendingString:linesArray[i]];
        }
        finalStr = [finalStr stringByAppendingString:[lineString4Th stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]]];
        
//        finalStr = [finalStr stringByAppendingString:@"<a href=\"http://www.qraved.com/\">...read more</a>"];
        
         finalStr = [finalStr stringByAppendingString:LINK_TEXT];
        
        
        finalStr=[finalStr stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
        [strBackDic setObject:finalStr forKey:@"finalStr"];
        
        CFRelease(frame4Th);
        CFRelease(frameSetter4Th);
        CFRelease(path4Th);
//        CFRelease(lineRef4Th);

    }
    
    CFRelease(frame);
    CFRelease(myFont);
    CFRelease(frameSetter);
    CFRelease(path);
    return strBackDic;
}

@end
