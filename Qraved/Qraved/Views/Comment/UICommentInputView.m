//
//  UICommentInputView.m
//  Qraved
//
//  Created by Jeff on 3/6/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "UICommentInputView.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import "AppDelegate.h"
#define UIPOSTBUTTONWIDTH 70

@implementation UICommentInputView
{
    UILabel *placeHolderLabel;
    NSInteger linesBeforeChanged;
    UIView *backgroundView;
    BOOL inFooter;
}

-(id)initView:(BOOL)inFooter_{
    self = [super initWithFrame:CGRectMake(0, 0, DeviceWidth, UICOMMENTINPUTVIEWHEIGHT)];
    if(self){
        inFooter=inFooter_;
        self.backgroundColor = [UIColor whiteColor];
        backgroundView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, UICOMMENTINPUTVIEWHEIGHT)];
        backgroundView.backgroundColor = [UIColor whiteColor];
        [self addSubview:backgroundView];
        
        UIView *topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 1)];
        topLine.backgroundColor = [UIColor colorEBEBEB];
        [backgroundView addSubview:topLine];

        self.postButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.postButton.frame = CGRectMake(DeviceWidth-UIPOSTBUTTONWIDTH, 1, UIPOSTBUTTONWIDTH, UICOMMENTINPUTVIEWHEIGHT-2);
//        self.postButton.layer.borderWidth=0;
        self.postButton.backgroundColor = [UIColor clearColor];
        [self.postButton setTitle:L(@"Post") forState:UIControlStateNormal];
        [self.postButton setTitleColor:[UIColor colorWithHexString:@"09BFD3"] forState:UIControlStateNormal];
        [self.postButton.titleLabel setFont:[UIFont systemFontOfSize:17]];
        [self.postButton addTarget:self action:@selector(postComment:) forControlEvents:UIControlEventTouchUpInside];
        [backgroundView addSubview:self.postButton];
        
        self.inputTextView = [[UITextView alloc]initWithFrame:CGRectMake(5, 7, DeviceWidth-UIPOSTBUTTONWIDTH-5, 31)];
        self.inputTextView.scrollsToTop = NO;
        self.inputTextView.scrollEnabled=NO;
        self.inputTextView.contentOffset = CGPointMake(0, 0);
        self.inputTextView.layer.cornerRadius = 16;
        self.inputTextView.layer.masksToBounds = YES;
        self.inputTextView.delegate = self;
        self.inputTextView.layer.borderWidth = 1;
        self.inputTextView.font = [UIFont systemFontOfSize:12];
        self.inputTextView.layer.borderColor = [[UIColor colorEBEBEB] CGColor];
        
        [self updatePostButtonStatus:self.inputTextView];
        [backgroundView addSubview:self.inputTextView];
        
        //input placeholder
        placeHolderLabel = [[UILabel alloc]initWithFrame:CGRectMake(27, 0, self.inputTextView.frame.size.width-27, self.inputTextView.frame.size.height)];
        placeHolderLabel.text = L(@"Write a comment...");
//        if(IS_IPHONE_4S||IS_IPHONE_5){
//            placeHolderLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:10.0f];
//        }else{
//            placeHolderLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:12.0f];
//        }
        placeHolderLabel.font = [UIFont systemFontOfSize:17];
        placeHolderLabel.textColor = [UIColor color999999];
        [self.inputTextView addSubview:placeHolderLabel];
        
        //cut line
//        self.layer.borderWidth = 1;
//        self.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.3].CGColor;
        
        //add Notification
//        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textViewDidEndEditing:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}

-(void)updatePostButtonStatus:(UITextView*)textView{
    if(textView.text.length==0){
        //self.postButton.alpha = 0.5;
        self.postButton.enabled = NO;
    }else{
        //self.postButton.alpha = 1;
        self.postButton.enabled = YES;
    }
}

-(void)postComment:(UIButton*)button{
    if(self.commentInputViewDelegate){
        [self endEditing:YES];
        IMGUser *currentUser = [IMGUser currentUser];
        if (currentUser.userId == nil || currentUser.token == nil){
            [[AppDelegate ShareApp] goToLoginControllerByDelegate:self dictionary:@{@"postComment":button}];
            return;
        }
        [self.commentInputViewDelegate commentInputView:self postButtonTapped:button commentInputText:self.inputTextView.text];
    }
}


-(void)calculateLinesBeforeChanged:(UITextView*)textView{
    UITextView *mTrasView = textView;
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12]};
    CGSize singleSize = [@"test" boundingRectWithSize:CGSizeMake(mTrasView.frame.size.width-10, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    CGSize textSize = [mTrasView.text boundingRectWithSize:CGSizeMake(mTrasView.frame.size.width-10, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    
    linesBeforeChanged = textSize.height / singleSize.height;

    if(linesBeforeChanged>5){
        linesBeforeChanged=5;
    }else if(linesBeforeChanged<1){
        linesBeforeChanged=1;
    }
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    self.inputTextView.scrollEnabled=YES;
    placeHolderLabel.hidden = YES;
    if(self.commentInputViewDelegate){
        [self.commentInputViewDelegate commentInputView:self inputViewDidBeginEditing:textView];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    self.inputTextView.scrollEnabled=NO;
    placeHolderLabel.hidden = self.inputTextView.text.length > 0;
    [self updatePostButtonStatus:self.inputTextView];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    [self calculateLinesBeforeChanged:textView];
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if(self.commentInputViewDelegate && ([self.commentInputViewDelegate respondsToSelector:@selector(commentInputView:inputViewShouldBeginEditing:)])){
        [self.commentInputViewDelegate commentInputView:self inputViewShouldBeginEditing:textView];
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    placeHolderLabel.hidden = textView.text.length>0;
    [self updatePostButtonStatus:textView];
    
    UITextView *mTrasView = textView;
    
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12]};
    CGSize singleSize = [@"test" boundingRectWithSize:CGSizeMake(mTrasView.frame.size.width-10, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    CGSize textSize = [mTrasView.text boundingRectWithSize:CGSizeMake(mTrasView.frame.size.width-10, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    
    NSInteger lines = textSize.height / singleSize.height;

    if(lines>5){
        lines=5;
    }else if(lines<1){
        lines=1;
    }
    
    if(linesBeforeChanged>5){
        linesBeforeChanged=5;
    }else if(linesBeforeChanged<1){
        linesBeforeChanged=1;
    }
    
    if(!inFooter){
        if(self.commentInputViewDelegate&&lines!=linesBeforeChanged){
            NSInteger linesOffset=lines - linesBeforeChanged;
            CGFloat commentInputViewYOffset = singleSize.height*linesOffset;
            [self updateFrameAfterLineChanges:commentInputViewYOffset];
            [self.commentInputViewDelegate commentInputView:self inputViewExpand:textView commentInputViewYOffset:commentInputViewYOffset];
        }else{
            if(lines==1 && lines==linesBeforeChanged && self.inputTextView.frame.size.height>31){
                CGFloat commentInputViewYOffset = 31-self.inputTextView.frame.size.height;
                [self updateFrameAfterLineChanges:commentInputViewYOffset];
                [self.commentInputViewDelegate commentInputView:self inputViewExpand:textView commentInputViewYOffset:commentInputViewYOffset];
            }
        }
    }
    linesBeforeChanged=lines;
}

-(void)updateFrameAfterLineChanges:(CGFloat)commentInputViewYOffset{
    self.frame=CGRectMake(0, self.startPointY-commentInputViewYOffset, self.frame.size.width, self.frame.size.height+commentInputViewYOffset);
    backgroundView.frame=CGRectMake(0, 0, backgroundView.frame.size.width, backgroundView.frame.size.height+commentInputViewYOffset);
    backgroundView.backgroundColor = [UIColor whiteColor];
    self.inputTextView.frame=CGRectMake(5, 7, self.inputTextView.frame.size.width, self.inputTextView.frame.size.height+commentInputViewYOffset);
    self.postButton.frame=CGRectMake(self.postButton.startPointX, self.postButton.startPointY, self.postButton.frame.size.width, self.postButton.frame.size.height+commentInputViewYOffset);
}

-(void)clearInputTextView{
    [self calculateLinesBeforeChanged:self.inputTextView];
    self.inputTextView.text=@"";
    [self textViewDidChange:self.inputTextView];
}

-(void)becomeFirstResponderToInputTextView{
    [self.inputTextView becomeFirstResponder];
}
@end
