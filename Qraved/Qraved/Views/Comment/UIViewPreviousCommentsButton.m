//
//  ViewPreviousCommentsButton.m
//  Qraved
//
//  Created by Jeff on 3/5/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "UIViewPreviousCommentsButton.h"
#import "UIConstants.h"

@implementation UIViewPreviousCommentsButton

-(id)initWithFrame:(CGRect)frame hidden:(BOOL)hidden{
    self=[super initWithFrame:frame];
    if(self){
        [self setHidden:hidden];
        UIFont *font = [UIFont systemFontOfSize:14];
        NSString *title = L(@"View previous comment");
        NSRange range = NSMakeRange(0,title.length);
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init] ;
        [paragraphStyle setAlignment:NSTextAlignmentLeft];
        
        NSMutableAttributedString *attributeTitle = [[NSMutableAttributedString alloc]initWithString:title];
//        [attributeTitle addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle ] range:range];
        [attributeTitle addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"09BFD3"] range:range];
        [attributeTitle addAttribute:NSFontAttributeName value:font range:range];
        [attributeTitle addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:range];
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, self.frame.size.width, self.frame.size.height)];
        label.attributedText=attributeTitle;
        [self addSubview:label];
        
    }
    return self;
}

@end
