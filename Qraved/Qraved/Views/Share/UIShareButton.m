//
//  ShareButton.m
//  Qraved
//
//  Created by Jeff on 3/3/16.
//  Copyright © 2016 Imaginato. All rights reserved.
//

#import "UIShareButton.h"
#import "UIView+Helper.h"

#define SHAREBUTTONTAPPEDICON @"ic_home_share.png"

@implementation UIShareButton

-(id)initInSuperView:(UIView*)parentView{
    self = [super initInSuperView:parentView tag:2];
    if (self) {
        [self.textLabel setText:L(@"Share")];
        [self.textLabel sizeToFit];
        self.iconView = [[UIImageView alloc]initWithFrame:CGRectMake((self.frame.size.width-COMMUNITYBUTTONICONSIZE-COMMUNITYBUTTONGAP-self.textLabel.frame.size.width)/2, (self.frame.size.height-COMMUNITYBUTTONICONSIZE)/2, COMMUNITYBUTTONICONSIZE, COMMUNITYBUTTONICONSIZE)];
        [self addSubview:self.iconView];
        self.textLabel.frame=CGRectMake(self.iconView.endPointX+COMMUNITYBUTTONGAP, (self.frame.size.height-self.textLabel.frame.size.height)/2, self.textLabel.frame.size.width, self.textLabel.frame.size.height);
        
        [self.textLabel setTextColor:COMMUNITYBUTTONUNTAPPEDTITLECOLOR];
        self.iconView.image=[UIImage imageNamed:SHAREBUTTONTAPPEDICON];
        
    }
    return self;
}

-(id)initInSuperView:(UIView*)parentView isFromPhotoViewer:(BOOL)isFromPhotoViewer{
    self = [super initInSuperView:parentView tag:2];
    if (self) {
        [self.textLabel setText:L(@"Share")];
        [self.textLabel sizeToFit];
        self.iconView = [[UIImageView alloc]initWithFrame:CGRectMake((self.frame.size.width-COMMUNITYBUTTONICONSIZE-COMMUNITYBUTTONGAP-self.textLabel.frame.size.width)/2, (self.frame.size.height-COMMUNITYBUTTONICONSIZE)/2, COMMUNITYBUTTONICONSIZE, COMMUNITYBUTTONICONSIZE)];
        [self addSubview:self.iconView];
        self.textLabel.frame=CGRectMake(self.iconView.endPointX+COMMUNITYBUTTONGAP, (self.frame.size.height-self.textLabel.frame.size.height)/2, self.textLabel.frame.size.width, self.textLabel.frame.size.height);
        
        [self.textLabel setTextColor:isFromPhotoViewer?[UIColor whiteColor]:COMMUNITYBUTTONUNTAPPEDTITLECOLOR];
        self.iconView.image=[UIImage imageNamed:isFromPhotoViewer?@"ic_share_white":SHAREBUTTONTAPPEDICON];
        
    }
    return self;
}

@end
