//
//  ShareToPathView.h
//  Qraved
//
//  Created by Jeff on 12/24/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"
#import "BZFoursquare.h"

@interface ShareToPathView : UIView <BZFoursquareRequestDelegate,BZFoursquareSessionDelegate>
-(id)initWithFrame:(CGRect)frame title:(NSString *)aTitle url:(NSString *)aUrl latitude:(NSNumber *)alatitude longitude:(NSNumber *)alongitude restaurantTitle:(NSString *)arestaurantTitle phone:(NSString *)phone address:(NSString *)address;
-(id)initWithFrame:(CGRect)frame title:(NSString *)aTitle url:(NSString *)aUrl;
@end
