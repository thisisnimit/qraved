//
//  ShareToPathView.m
//  Qraved
//
//  Created by Jeff on 12/24/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "ShareToPathView.h"
#import "NXOAuth2.h"
#import "IMGUser.h"
#import "LoadingView.h"
#import "NSString+Helper.h"
#import "UIConstants.h"
#import "UIImageView+WebCache.h"
//#import "AFHTTPRequestOperationManager.h"

#define FOURSQUARE_CLIENT_ID @"G4OITXH0T4BTHFNXRBFDETX0VM4AUFD3WPRIQVYEZUX4XMNB"
#define FOURSQUARE_CLIENT_SECRET @"UK3VNZAXHX3E1NQP3BDPDUJLFKY31IIJDS3ITNHDXRVS10IT"
#define FOURSQUARE_REDIRECT_URI @"qraved731842943://www.qraved.com/foursquare/redirect"
#define FOURSQUARE_ACCESS_TOKEN @"XQHPJQG4FYNLXAPYC5UZXPMXLJVKDT4YOBPIPWG430OULL01"

@implementation ShareToPathView{
    UITextView *textView;
    NSString *title;
    NSString *restaurantTitle;
    NSString *url;
    NSString *phone;
    NSString *address;
    NSNumber *latitude;
    NSNumber *longitude;
    BZFoursquare *foursquare;
}

-(id)initWithFrame:(CGRect)frame title:(NSString *)aTitle url:(NSString *)aUrl latitude:(NSNumber *)alatitude longitude:(NSNumber *)alongitude restaurantTitle:(NSString *)arestaurantTitle phone:(NSString *)aphone address:(NSString *)aaddress{
    self = [super initWithFrame:frame];
    title = aTitle;
    url = aUrl;
    phone=aphone;
    address=aaddress;
    latitude=alatitude;
    longitude=alongitude;
    restaurantTitle = arestaurantTitle;
    [self setBackgroundColor:[UIColor clearColor]];
    
    UIView *backgroundView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    [backgroundView setBackgroundColor:[UIColor lightGrayColor]];
    [backgroundView setAlpha:0.5];
    [self addSubview:backgroundView];
    
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(20, 25, frame.size.width-40, 240)];
    [contentView setBackgroundColor:[UIColor whiteColor]];
    contentView.layer.cornerRadius = 13;
    contentView.layer.borderWidth = 1;
    contentView.layer.borderColor = [UIColor grayColor].CGColor ;

    UIButton *cancelButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, 45)];
    [cancelButton setBackgroundColor:[UIColor clearColor]];
    [cancelButton setTitle:L(@"Cancel") forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:cancelButton];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(80, 0, contentView.frame.size.width-160, 45)];
    [titleLabel setText:L(@"Path")];
    [titleLabel setTextColor:[UIColor blackColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setBackgroundColor:[UIColor whiteColor]];
    [contentView addSubview:titleLabel];
    
    UIButton *postButton = [[UIButton alloc]initWithFrame:CGRectMake(contentView.frame.size.width-82, 0, 82, 45)];
    [postButton setBackgroundColor:[UIColor clearColor]];
    [postButton setTitle:L(@"Check In") forState:UIControlStateNormal];
    [postButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [postButton addTarget:self action:@selector(checkIn:) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:postButton];
    
    UIImageView *celllineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 44, DeviceWidth, 1)];
    [celllineImageView setImage:[UIImage imageNamed:@"search_line"] ];
    [contentView addSubview:celllineImageView];

    if(aUrl!=nil){
        UIImageView *sourceImageView = [[UIImageView alloc]initWithFrame:CGRectMake(contentView.frame.size.width-86, 48, 80, 80)];
        [sourceImageView sd_setImageWithURL:[NSURL URLWithString:[aUrl returnFullImageUrlWithWidth:80]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2]];
        textView = [[UITextView alloc]initWithFrame:CGRectMake(0, 45, contentView.frame.size.width-86, 195)];
        [contentView addSubview:sourceImageView];
    }else{
        textView = [[UITextView alloc]initWithFrame:CGRectMake(0, 45, contentView.frame.size.width, 195)];
    }
    
    [textView setText:title];
    [textView becomeFirstResponder];
    [textView setBackgroundColor:[UIColor clearColor]];
    [textView setFont:[UIFont systemFontOfSize:15]];
    [contentView addSubview:textView];

    [self addSubview:contentView];
    [self bringSubviewToFront:contentView];
    foursquare = [[BZFoursquare alloc] initWithClientID:FOURSQUARE_CLIENT_ID callbackURL:FOURSQUARE_REDIRECT_URI];
    foursquare.version = @"20120609";
    foursquare.locale = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
    foursquare.accessToken=FOURSQUARE_ACCESS_TOKEN;
    foursquare.sessionDelegate=self;
    return self;
}

-(id)initWithFrame:(CGRect)frame title:(NSString *)aTitle url:(NSString *)aUrl{
    self = [super initWithFrame:frame];
    title = aTitle;
    url = aUrl;
    [self setBackgroundColor:[UIColor clearColor]];
    
    UIView *backgroundView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    [backgroundView setBackgroundColor:[UIColor lightGrayColor]];
    [backgroundView setAlpha:0.5];
    [self addSubview:backgroundView];
    
    UIView *contentView = [[UIView alloc]initWithFrame:CGRectMake(20, 25, frame.size.width-40, 240)];
    [contentView setBackgroundColor:[UIColor whiteColor]];
    contentView.layer.cornerRadius = 13;
    contentView.layer.borderWidth = 1;
    contentView.layer.borderColor = [UIColor grayColor].CGColor ;
    
    UIButton *cancelButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 80, 45)];
    [cancelButton setBackgroundColor:[UIColor clearColor]];
    [cancelButton setTitle:L(@"Cancel") forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [cancelButton addTarget:self action:@selector(cancelButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:cancelButton];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(80, 0, contentView.frame.size.width-160, 45)];
    [titleLabel setText:L(@"Path")];
    [titleLabel setTextColor:[UIColor blackColor]];
    [titleLabel setTextAlignment:NSTextAlignmentCenter];
    [titleLabel setBackgroundColor:[UIColor whiteColor]];
    [contentView addSubview:titleLabel];
    
    UIButton *postButton = [[UIButton alloc]initWithFrame:CGRectMake(contentView.frame.size.width-82, 0, 82, 45)];
    [postButton setBackgroundColor:[UIColor clearColor]];
    [postButton setTitle:L(@"Post") forState:UIControlStateNormal];
    [postButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [postButton addTarget:self action:@selector(post:) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:postButton];
    
    UIImageView *celllineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 44, DeviceWidth, 1)];
    [celllineImageView setImage:[UIImage imageNamed:@"search_line"] ];
    [contentView addSubview:celllineImageView];
    
    if(aUrl!=nil){
        UIImageView *sourceImageView = [[UIImageView alloc]initWithFrame:CGRectMake(contentView.frame.size.width-86, 48, 80, 80)];
        [sourceImageView sd_setImageWithURL:[NSURL URLWithString:[aUrl returnFullImageUrlWithWidth:80]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2]];
        textView = [[UITextView alloc]initWithFrame:CGRectMake(0, 45, contentView.frame.size.width-86, 195)];
        [contentView addSubview:sourceImageView];
    }else{
        textView = [[UITextView alloc]initWithFrame:CGRectMake(0, 45, contentView.frame.size.width, 195)];
    }
    
    [textView setText:title];
    [textView becomeFirstResponder];
    [textView setBackgroundColor:[UIColor clearColor]];
    [textView setFont:[UIFont systemFontOfSize:15]];
    [contentView addSubview:textView];
    
    [self addSubview:contentView];
    [self bringSubviewToFront:contentView];
    return self;
}


-(void)cancelButtonTapped{
    [self removeFromSuperview];
}

-(void)checkIn:(UIButton *)sender{
    [sender setEnabled:FALSE];
    NSString *pathToken = [[NSUserDefaults standardUserDefaults] objectForKey:QRAVED_PATH_TOKEN_KEY];
    if(pathToken==nil){
        [[NXOAuth2AccountStore sharedStore] requestAccessToAccountWithType:@"Path"];
        return;
    }else{
        NSLog(@"get path token directly from NSUserDefaults");
    }
    [[LoadingView sharedLoadingView]startLoading];
//    NSDictionary *parameters = @{@"name": restaurantTitle, @"ll": [NSString stringWithFormat:@"%@,%@",latitude,longitude]};
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc]init];
    [parameters setObject:restaurantTitle forKey:@"name"];
    [parameters setObject:[NSString stringWithFormat:@"%@,%@",latitude,longitude] forKey:@"ll"];
    if(phone!=nil){
        [parameters setObject:phone forKey:@"phone"];
    }
    [parameters setObject:address forKey:@"address"];
    
    BZFoursquareRequest *request = [foursquare requestWithPath:@"venues/add" HTTPMethod:@"POST" parameters:parameters delegate:self];
    
    [request start];
    
}//booking, rating, pv,#reviews,#votes

-(void)post:(UIButton *)sender{
    [sender setEnabled:FALSE];
    NSString *pathToken = [[NSUserDefaults standardUserDefaults] objectForKey:QRAVED_PATH_TOKEN_KEY];
    if(pathToken==nil){
        [[NXOAuth2AccountStore sharedStore] requestAccessToAccountWithType:@"Path"];
        return;
    }else{
        NSLog(@"get path token directly from NSUserDefaults");
    }
    [[LoadingView sharedLoadingView]startLoading];
    [self postToPath];
    
}

#pragma mark BZFousquareSessionDelegate

- (void)foursquareDidAuthorize:(BZFoursquare *)foursquare {

}

- (void)foursquareDidNotAuthorize:(BZFoursquare *)foursquare error:(NSDictionary *)errorInfo {
    NSLog(@"%s: %@", __PRETTY_FUNCTION__, errorInfo);
}

#pragma mark BZFoursquareRequestDelegate

- (void)requestDidFinishLoading:(BZFoursquareRequest *)request {
    NSString *venuId;
    NSLog(@"requestDidFinishLoading.response,%@",request.response);
    venuId=[[request.response objectForKey:@"venue"] objectForKey:@"id"];
    NSLog(@"response.venueId,%@",venuId);
    [self checkInPath:venuId];
}

- (void)request:(BZFoursquareRequest *)request didFailWithError:(NSError *)error {
    NSString *venuId;
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:[error userInfo][@"errorDetail"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"") otherButtonTitles:nil];
//    [alertView show];

    NSLog(@"didFailWithError.response,%@",request.response);
    NSArray *candidateDuplicateVenues = [request.response objectForKey:@"candidateDuplicateVenues"];
    if(candidateDuplicateVenues!=nil && [candidateDuplicateVenues count]>0){
        venuId=[[candidateDuplicateVenues objectAtIndex:0] objectForKey:@"id"];
        NSLog(@"response.venueId,%@",venuId);
    }
    [self checkInPath:venuId];
}

-(void)checkInPath:(NSString *)venuId {
//    NSString *pathToken = [[NSUserDefaults standardUserDefaults] objectForKey:QRAVED_PATH_TOKEN_KEY];
    
    NSMutableDictionary *parameters1 = [[NSMutableDictionary alloc]init];
    NSString *caption =title;
    [parameters1 setObject:caption forKey:@"caption"];
    if(venuId!=nil){
        //share photo
//        NSMutableDictionary *place = [[NSMutableDictionary alloc]init];
//        [place setObject:@"foursquare" forKey:@"source"];
//        [place setObject:venuId forKey:@"id"];
//        [place setObject:@"place" forKey:@"type"];
//        NSArray *tagArray = [NSArray arrayWithObjects:place, nil];
//        [parameters1 setObject:tagArray forKey:@"tags"];
        
        //check in
        [parameters1 setObject:@"foursquare" forKey:@"source"];
        [parameters1 setObject:venuId forKey:@"source_id"];
    }
    
    if(url!=nil){
        NSString* encodedUrl = [QRAVED_WEB_IMAGE_SERVER stringByAppendingString:[url stringByAddingPercentEscapesUsingEncoding:
                                                                                 NSASCIIStringEncoding]];
        [parameters1 setObject:encodedUrl forKey:@"source_url"];
    }

//    AFHTTPRequestOperationManager *httpRequestOperationManager = [AFHTTPRequestOperationManager pathmanager];
//    httpRequestOperationManager.requestSerializer =[AFJSONRequestSerializer serializer];
//    [httpRequestOperationManager POST:[NSString stringWithFormat:@"%@",@"1/moment/place"] parameters:parameters1 accessToken:pathToken success:^(AFHTTPRequestOperation *operation1, id responseObject1) {
//        NSLog(@"**********moment.responseObject1%@",responseObject1);
//        [[LoadingView sharedLoadingView]stopLoading];
//        [self removeFromSuperview];
//    } failure:^(AFHTTPRequestOperation  *operation, NSError *error1) {
//        [[LoadingView sharedLoadingView]stopLoading];
//        NSLog(@"**********moment.error1%@",error1.localizedDescription);
//    }];
}

-(void)postToPath {
    NSString *pathToken = [[NSUserDefaults standardUserDefaults] objectForKey:QRAVED_PATH_TOKEN_KEY];
    
    NSMutableDictionary *parameters1 = [[NSMutableDictionary alloc]init];
    NSString *caption =title;
    [parameters1 setObject:caption forKey:@"caption"];
    
    if(url!=nil){
        NSString* encodedUrl = [QRAVED_WEB_IMAGE_SERVER stringByAppendingString:[url stringByAddingPercentEscapesUsingEncoding:
                                                                                 NSASCIIStringEncoding]];
        [parameters1 setObject:encodedUrl forKey:@"source_url"];
    }
    AFHTTPSessionManager *httpRequestOperationManager = [AFHTTPSessionManager manager];
    httpRequestOperationManager.requestSerializer =[AFJSONRequestSerializer serializer];
    [httpRequestOperationManager.requestSerializer setAuthorizationHeaderFieldWithUsername:pathToken password:@""];

    [httpRequestOperationManager POST:[NSString stringWithFormat:@"%@",@"1/moment/photo"] parameters:parameters1 progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"**********moment.responseObject1%@",responseObject);
        [[LoadingView sharedLoadingView]stopLoading];
        [self removeFromSuperview];

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [[LoadingView sharedLoadingView]stopLoading];

    }];
    //    AFHTTPRequestOperationManager *httpRequestOperationManager = [AFHTTPRequestOperationManager pathmanager];
//    httpRequestOperationManager.requestSerializer =[AFJSONRequestSerializer serializer];
//    [httpRequestOperationManager POST:[NSString stringWithFormat:@"%@",@"1/moment/photo"] parameters:parameters1 accessToken:pathToken success:^(AFHTTPRequestOperation *operation1, id responseObject1) {
//        NSLog(@"**********moment.responseObject1%@",responseObject1);
//        [[LoadingView sharedLoadingView]stopLoading];
//        [self removeFromSuperview];
//    } failure:^(AFHTTPRequestOperation  *operation, NSError *error1) {
//        [[LoadingView sharedLoadingView]stopLoading];
//        NSLog(@"**********moment.error1%@",error1.localizedDescription);
//    }];
}

@end
