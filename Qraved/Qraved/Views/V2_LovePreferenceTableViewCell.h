//
//  V2_LovePreferenceTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/8/8.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_EatModel.h"

@protocol V2_LovePreferenceTableViewCellDelegate <NSObject>

- (void)delegateSelecteArray:(NSMutableArray *)array;

@end

@interface V2_LovePreferenceTableViewCell : UITableViewCell

@property (nonatomic, strong)  UIButton *btnEat;
@property (nonatomic, strong)  UIImageView  *imageV;
@property (nonatomic, strong)  NSArray  *eatingArray;
//@property (nonatomic, strong)  NSArray  *normalArray;
@property (nonatomic, strong)  NSMutableArray  *selecteArr;

@property (nonatomic,assign) CGFloat cellHeight;

- (void)createUI:(NSArray *)array;

@property (nonatomic, weak)  id<V2_LovePreferenceTableViewCellDelegate> Delegate;

@end
