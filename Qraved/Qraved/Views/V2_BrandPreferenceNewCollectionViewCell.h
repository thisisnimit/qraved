//
//  V2_BrandPreferenceNewCollectionViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/8/9.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_BrandModel.h"
#import "myLabel.h"
@interface V2_BrandPreferenceNewCollectionViewCell : UICollectionViewCell


@property (nonatomic, strong) UIView  *selectView;
@property (nonatomic, strong) UIImageView  *selectImageView;
@property (nonatomic, strong) V2_BrandModel *model;
@end
