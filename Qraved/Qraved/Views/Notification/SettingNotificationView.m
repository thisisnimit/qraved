//
//  SettingNotificationView.m
//  Qraved
//
//  Created by harry on 2018/3/16.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "SettingNotificationView.h"

@implementation SettingNotificationView

- (instancetype)init{
    self = [super init];
    if (self) {
        [self initUI];
    }
    return self;
}

- (void)initUI{
    UIButton *btnClose = [UIButton new];
    [btnClose setImage:[UIImage imageNamed:@"ic_notification_close"] forState:UIControlStateNormal];
    [btnClose addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [btnClose setImageEdgeInsets:UIEdgeInsetsMake(0, 27, 27, 0)];
    
    UILabel *lblTitle = [UILabel new];
    lblTitle.font = [UIFont systemFontOfSize:14];
    lblTitle.textColor = [UIColor color333333];
    lblTitle.text = @"NEVER MISS AN UPDATE";
    lblTitle.textAlignment = NSTextAlignmentCenter;
    
    UILabel *lblDes = [UILabel new];
    lblDes.font = [UIFont systemFontOfSize:12];
    lblDes.textColor = [UIColor color999999];
    lblDes.text = @"Turn on push notifications to receive updates";
    lblDes.textAlignment = NSTextAlignmentCenter;
    
    FunctionButton *btnSet = [FunctionButton buttonWithType:UIButtonTypeCustom andTitle:@"Turn On Notifications" andBGColor:[UIColor whiteColor] andTitleColor:[UIColor colorWithHexString:@"#09BFD3"]];
    btnSet.titleLabel.font = [UIFont systemFontOfSize:14];
    btnSet.layer.borderWidth = 1;
    [btnSet addTarget:self action:@selector(settingButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    btnSet.layer.borderColor = [UIColor colorWithHexString:@"#09BFD3"].CGColor;
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    
    [self sd_addSubviews:@[btnClose, lblTitle, lblDes, btnSet, lineView]];
    
    btnClose.sd_layout
    .topSpaceToView(self, 5)
    .rightSpaceToView(self, 5)
    .widthIs(45)
    .heightIs(45);
    
    lblTitle.sd_layout
    .topSpaceToView(self, 21)
    .rightSpaceToView(self, 15)
    .leftSpaceToView(self, 15)
    .heightIs(18);
    
    lblDes.sd_layout
    .topSpaceToView(lblTitle, 5)
    .leftSpaceToView(self, 15)
    .rightSpaceToView(self, 15)
    .heightIs(16);
    
    btnSet.sd_layout
    .topSpaceToView(lblDes, 10)
    .widthIs(185)
    .centerXEqualToView(self)
    .heightIs(38);
    
    btnSet.layer.masksToBounds = YES;
    btnSet.layer.cornerRadius = 19;
    
    lineView.sd_layout
    .topSpaceToView(btnSet, 21)
    .leftSpaceToView(self, 0)
    .rightSpaceToView(self, 0)
    .heightIs(1);
}

- (void)close{
    if (self.closeView) {
        self.closeView();
    }
}

- (void)settingButtonTapped{
    if (self.settingTapped) {
        self.settingTapped();
    }
}

@end
