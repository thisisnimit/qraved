//
//  NotificationTableViewCell.h
//  Qraved
//
//  Created by Laura on 14-9-16.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGNotification.h"
#import "IMGNotifications.h"

@interface NotificationCell : UITableViewCell

@property (nonatomic,retain)UIImageView *iconImageView;
@property (nonatomic,retain)IMGNotification *notification;
@property (nonatomic,retain)UILabel *notificationLabel;
@property (nonatomic,retain)UILabel *creatTimeLabel;
@property (nonatomic,retain)UILabel *contentLabel;
//@property (nonatomic,retain)UIImageView *smallIconImageView;

@property (nonatomic,retain)UIImageView *pointImageView;

-(void)setNotifications:(IMGNotifications *)notifications;


@end
