//
//  NotificationTableViewCell.m
//  Qraved
//
//  Created by Laura on 14-8-22.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "NotificationTableViewCell.h"

@implementation NotificationTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.phoneImageView = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth-80, 11, 18, 24)];
        [self.contentView addSubview:self.phoneImageView];
        self.phoneImageView.userInteractionEnabled = YES;
        self.emailImageView = [[UIImageView alloc]initWithFrame:CGRectMake(DeviceWidth-40, 14, 24, 18)];
        [self.contentView addSubview:self.emailImageView];
        self.emailImageView.userInteractionEnabled = YES;
        
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
