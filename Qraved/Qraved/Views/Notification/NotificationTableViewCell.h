//
//  NotificationTableViewCell.h
//  Qraved
//
//  Created by Laura on 14-8-22.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTableViewCell : UITableViewCell

@property(nonatomic,retain)UIImageView *phoneImageView;
@property(nonatomic,retain)UIImageView *emailImageView;
@end
