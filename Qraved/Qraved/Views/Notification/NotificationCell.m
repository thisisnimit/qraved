//
//  NotificationTableViewCell.m
//  Qraved
//
//  Created by Laura on 14-9-16.
//  Copyright (c) 2014年 Imaginato. All rights reserved.
//

#import "NotificationCell.h"

#import "UIConstants.h"

#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "UIDevice+Util.h"
#import "UILabel+Helper.h"
#import "NSString+Helper.h"
#import "Date.h"
#import "UIImageView+WebCache.h"
#import "IMGUser.h"
#import "UICommentCell.h"
#import "IMGDish.h"

#define LABEL_FONT 14


@implementation NotificationCell
{
    UIImageView *lineImage;
    UILabel *commentLabel;
    UIImageView *dishImageView;
    UIImageView* ImageView;
    UIView* dishView;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.iconImageView = [[UIImageView alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 20, 25, 25)];
        [self addSubview:self.iconImageView];
        [self.iconImageView.layer setMasksToBounds:YES];
        [self.iconImageView.layer setCornerRadius:12.5];

        self.notificationLabel = [[UILabel alloc]init];
        self.notificationLabel.frame = CGRectMake(self.iconImageView.endPointX + LEFTLEFTSET, 18, DeviceWidth - (40 + LEFTLEFTSET) - 30, 30);
        self.notificationLabel.backgroundColor = [UIColor clearColor];
        self.notificationLabel.numberOfLines = 0;
        self.notificationLabel.font = [UIFont systemFontOfSize:LABEL_FONT];
        self.notificationLabel.textColor = [UIColor color999999];
        [self.contentView addSubview:self.notificationLabel];
        
        self.contentLabel = [[UILabel alloc] init];
        self.contentLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.endPointY, self.notificationLabel.frame.size.width, 30);
        self.contentLabel.numberOfLines = 2;
        self.contentLabel.backgroundColor = [UIColor clearColor];
        self.contentLabel.textColor = [UIColor color999999];
        self.contentLabel.font = [UIFont italicSystemFontOfSize:LABEL_FONT - 2];
        self.contentLabel.alpha = 0.9f;
        [self.contentView addSubview:self.contentLabel];
        self.contentLabel.hidden = YES;
        
        commentLabel = [[UILabel alloc] init];
        commentLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.endPointY, self.notificationLabel.frame.size.width, 0);
        commentLabel.numberOfLines = 0;
        commentLabel.backgroundColor = [UIColor clearColor];
        commentLabel.textColor = [UIColor color999999];
        commentLabel.font = [UIFont italicSystemFontOfSize:LABEL_FONT - 2];
        commentLabel.alpha = 0.9f;
        [self.contentView addSubview:commentLabel];
        self.contentLabel.hidden = YES;
        
        dishImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:dishImageView];
        dishImageView.hidden = YES;
        
        self.creatTimeLabel = [[UILabel alloc] init];
        self.creatTimeLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.endPointY + 10, self.notificationLabel.frame.size.width, 20);
        self.creatTimeLabel.font = [UIFont italicSystemFontOfSize:LABEL_FONT - 2];
        self.creatTimeLabel.textColor = [UIColor color999999];
        self.creatTimeLabel.alpha = 0.6f;
        [self.contentView addSubview:self.creatTimeLabel];
        
        self.pointImageView = [[UIImageView alloc] init];
        self.pointImageView.frame = CGRectMake(DeviceWidth - 20, self.iconImageView.frame.origin.y + 8, 7, 7);
        self.pointImageView.image = [UIImage imageNamed:@"redPoint"];
        [self.contentView addSubview:self.pointImageView];
        self.pointImageView.hidden = YES;
        
        lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 79, DeviceWidth, 1)];
        lineImage.backgroundColor = [UIColor colorEEEEEE];
        [self.contentView addSubview:lineImage];

    }
    return self;
}

-(void)setNotifications:(IMGNotifications *)notifications
{
    [commentLabel setHidden:YES];
    [dishImageView setHidden:YES];
    [dishView setHidden:YES];
    IMGUser *user = [IMGUser currentUser];
    int type = [notifications.type intValue];
    switch (type)
    {
        case 0:
        {
            NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:[notifications.message removeHTML]];
            self.notificationLabel.attributedText = attributeStr;
            
            self.notificationLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.frame.origin.y, self.notificationLabel.frame.size.width, self.notificationLabel.expectedHeight);
            
        }
            break;
            
        case 1:
        {
            [self.iconImageView setImage:[UIImage imageNamed:@"notificationIcon"]];

            if (notifications.content.length)
            {
                self.contentLabel.hidden = NO;
                self.contentLabel.text = [notifications.content removeHTML];
            }
            self.notificationLabel.text=[notifications.message removeHTML];
            self.notificationLabel.textColor=[UIColor color333333];
            self.notificationLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.frame.origin.y, self.notificationLabel.frame.size.width, self.notificationLabel.expectedHeight);
            
        }
            break;
            
        case 2:
        {
            [self.iconImageView setImage:[UIImage imageNamed:@"splashIcon"]];
            NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:[notifications.contentTitle removeHTML]];
            self.notificationLabel.attributedText = attributeStr;
            self.notificationLabel.textColor = [UIColor color333333];
            
            self.notificationLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.frame.origin.y, self.notificationLabel.frame.size.width, self.notificationLabel.expectedHeight);
            
            
        }
            break;
            
        case 3:
        {
            [self.iconImageView setImage:[UIImage imageNamed:@"bookIcon"]];

            if ([notifications.subtype intValue] == 31)
            {
                NSString *bookDateTime=[NSString stringWithFormat:@"%@ %@",notifications.bookDate,notifications.bookTime];
                NSDateFormatter *df=[[NSDateFormatter alloc] init];
                df.timeZone=[NSTimeZone timeZoneWithName:@"Asia/Jakarta"];
                df.dateFormat=@"yyyy-MM-dd HH:mm";
                NSDate *utcDate=[df dateFromString:bookDateTime];
                df.dateFormat=@"EEEE, dd MMM yyyy HH:mm";
                NSString *localDate=[df stringFromDate:utcDate];
                
                NSString *messageString = [[NSString alloc] initWithFormat:@"Hi %@, kamu ditunggu di %@ pada pukul %@ ! See ya!",[notifications.userName removeHTML],notifications.restaurantName,localDate];
                self.notificationLabel.textColor = [UIColor grayColor];
                NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:messageString];
                NSRange userNameRange = [messageString rangeOfString:[notifications.userName removeHTML]];
                [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor color333333] range:userNameRange];
                
                NSRange restaurantNameRange = [messageString rangeOfString:notifications.restaurantName];
                [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorRed] range:restaurantNameRange];
                
                NSRange timeRange = [messageString rangeOfString:localDate];
                [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorRed] range:timeRange];
                
                self.notificationLabel.attributedText = attributeStr;
                self.notificationLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.frame.origin.y, self.notificationLabel.frame.size.width, self.notificationLabel.expectedHeight);
            }
            else if ([notifications.subtype intValue] == 32)
            {
                NSString *messageString = [[NSString alloc] initWithFormat:@"Hi %@, reservasi kamu di %@ telah dikonfirmasi oleh restoran! Bon Appetit!",[notifications.userName removeHTML],notifications.restaurantName];
                
                self.notificationLabel.textColor = [UIColor grayColor];
                NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:messageString];
                NSRange userNameRange = [messageString rangeOfString:[notifications.userName removeHTML]];
                [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor color333333] range:userNameRange];
                
                NSRange restaurantNameRange = [messageString rangeOfString:notifications.restaurantName];
                [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorRed] range:restaurantNameRange];
                
                self.notificationLabel.attributedText = attributeStr;
                self.notificationLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.frame.origin.y, self.notificationLabel.frame.size.width, self.notificationLabel.expectedHeight);
            }
            
            
        }
            break;
            
        case 4:
        {
            
            NSString *urlString = [notifications.restaurantImageUrl returnFullImageUrlWithWidth:25];
            __weak typeof(self.iconImageView) weakRestaurantImageView = self.iconImageView;
            [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [UIView animateWithDuration:1.0 animations:^{
                                        [weakRestaurantImageView setAlpha:1.0];
                                    }];
            }];
            self.notificationLabel.textColor = [UIColor grayColor];
            NSString *messageString = [[NSString alloc] initWithFormat:@"Pst! Check %@ out! They just updated their page!",notifications.restaurantName];
            
            NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:messageString];
            
            NSRange userNameRange = [messageString rangeOfString:[notifications.userName removeHTML]];
            [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor color333333] range:userNameRange];
            
            NSRange restaurantNameRange = [messageString rangeOfString:notifications.restaurantName];
            [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorRed] range:restaurantNameRange];

            self.notificationLabel.attributedText = attributeStr;
            self.notificationLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.frame.origin.y, self.notificationLabel.frame.size.width, self.notificationLabel.expectedHeight);
            
        }
            break;
        case 5:
        {
            NSString *urlString = [notifications.userAvatar returnFullImageUrlWithWidth:25];
            __weak typeof(self.iconImageView) weakRestaurantImageView = self.iconImageView;
//            [self.iconImageView setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//                [UIView animateWithDuration:1.0 animations:^{
//                    [weakRestaurantImageView setAlpha:1.0];
//                }];
//            }];
            [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [UIView animateWithDuration:1.0 animations:^{
                                        [weakRestaurantImageView setAlpha:1.0];
                                    }];
            }];
            if ([notifications.subtype intValue] == 51)
            {
                if ([user.userId intValue] == [notifications.reviewUserId intValue])
                {
                    NSString *messageString;
                    NSRange nameRange;
                    if (!notifications.isVendor && [notifications.userType intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ liked your review",[notifications.restaurantName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.restaurantName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ liked your review",[notifications.guestUserName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.guestUserName removeHTML]];
                    }
                    
                    self.notificationLabel.textColor = [UIColor grayColor];
                    
                    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:messageString];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor color333333] range:nameRange];
                    
                    NSRange restaurantNameRange = [messageString rangeOfString:@"liked"];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorRed] range:restaurantNameRange];
                    
                    self.notificationLabel.attributedText = attributeStr;
                    self.notificationLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.frame.origin.y, self.notificationLabel.frame.size.width, self.notificationLabel.expectedHeight);

                }
                else
                {
                    NSString *messageString;
                    NSRange nameRange;
                    if (!notifications.isVendor && [notifications.userType intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also liked %@'s review",[notifications.restaurantName removeHTML],[notifications.reviewUserName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.restaurantName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also liked %@'s review",[notifications.guestUserName removeHTML],[notifications.reviewUserName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.guestUserName removeHTML]];
                    }

                    self.notificationLabel.textColor = [UIColor grayColor];
                    
                    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:messageString];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor color333333] range:nameRange];
                    
                    NSRange restaurantNameRange = [messageString rangeOfString:@"also liked"];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorRed] range:restaurantNameRange];
                    
                    self.notificationLabel.attributedText = attributeStr;
                    self.notificationLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.frame.origin.y, self.notificationLabel.frame.size.width, self.notificationLabel.expectedHeight);
                }
            }
            else if ([notifications.subtype intValue] == 52)
            {
                if ([user.userId intValue] == [notifications.reviewUserId intValue])
                {
                    NSString *messageString;
                    NSRange nameRange;
                    if (!notifications.isVendor && [notifications.userType intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ commented on your review",[notifications.restaurantName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.restaurantName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ commented on your review",[notifications.guestUserName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.guestUserName removeHTML]];
                    }
                    self.notificationLabel.textColor = [UIColor grayColor];
                    
                    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:messageString];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor color333333] range:nameRange];
                    
                    NSRange restaurantNameRange = [messageString rangeOfString:@"commented on"];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorGreen] range:restaurantNameRange];
                    
                    self.notificationLabel.attributedText = attributeStr;
                    self.notificationLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.frame.origin.y, self.notificationLabel.frame.size.width, self.notificationLabel.expectedHeight);
                }
                else
                {
                    NSString *messageString;
                    NSRange nameRange;
                    if (!notifications.isVendor && [notifications.userType intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also commented on %@'s review",[notifications.restaurantName removeHTML],[notifications.reviewUserName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.restaurantName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also commented on %@'s review",[notifications.guestUserName removeHTML],[notifications.reviewUserName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.guestUserName removeHTML]];
                    }
                    self.notificationLabel.textColor = [UIColor grayColor];
                    
                    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:messageString];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor color333333] range:nameRange];
                    
                    NSRange restaurantNameRange = [messageString rangeOfString:@"also commented on"];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorGreen] range:restaurantNameRange];
                    
                    self.notificationLabel.attributedText = attributeStr;
                    self.notificationLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.frame.origin.y, self.notificationLabel.frame.size.width, self.notificationLabel.expectedHeight);
                    
                }
            }

        }
            break;
        case 6:
        {
            NSString *urlString = [notifications.userAvatar returnFullImageUrlWithWidth:25];
            __weak typeof(self.iconImageView) weakRestaurantImageView = self.iconImageView;
//            [self.iconImageView setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//                [UIView animateWithDuration:1.0 animations:^{
//                    [weakRestaurantImageView setAlpha:1.0];
//                }];
//            }];
            [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [UIView animateWithDuration:1.0 animations:^{
                                        [weakRestaurantImageView setAlpha:1.0];
                                    }];
            }];
            if ([notifications.subtype intValue] == 51)
            {
                if ([user.userId intValue] == [notifications.dishUserId intValue])
                {
                    NSString *messageString;
                    NSRange nameRange;
                    if (!notifications.isVendor && [notifications.userType intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ liked your photo",[notifications.restaurantName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.restaurantName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ liked your photo",[notifications.guestUserName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.guestUserName removeHTML]];
                    }
                    self.notificationLabel.textColor = [UIColor grayColor];
                    
                    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:messageString];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor color333333] range:nameRange];
                    
                    NSRange restaurantNameRange = [messageString rangeOfString:@"liked"];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorRed] range:restaurantNameRange];
                    
                    self.notificationLabel.attributedText = attributeStr;
                    self.notificationLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.frame.origin.y, self.notificationLabel.frame.size.width, self.notificationLabel.expectedHeight);
                }
                else
                {
                    NSString *messageString;
                    NSRange nameRange;
                    if (!notifications.isVendor && [notifications.userType intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also liked %@'s photo",[notifications.restaurantName removeHTML],[notifications.dishUserName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.restaurantName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also liked %@'s photo",[notifications.guestUserName removeHTML],[notifications.dishUserName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.guestUserName removeHTML]];
                    }
                    self.notificationLabel.textColor = [UIColor grayColor];
                    
                    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:messageString];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor color333333] range:nameRange];
                    
                    NSRange restaurantNameRange = [messageString rangeOfString:@"also liked"];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorRed] range:restaurantNameRange];
                    
                    self.notificationLabel.attributedText = attributeStr;
                    self.notificationLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.frame.origin.y, self.notificationLabel.frame.size.width, self.notificationLabel.expectedHeight);

                }
            }
            else if ([notifications.subtype intValue] == 52)
            {
                if ([user.userId intValue] == [notifications.dishUserId intValue])
                {
                    NSString *messageString;
                    NSRange nameRange;
                    if (!notifications.isVendor && [notifications.userType intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ commented on your photo",[notifications.restaurantName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.restaurantName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ commented on your photo",[notifications.guestUserName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.guestUserName removeHTML]];
                    }
                    self.notificationLabel.textColor = [UIColor grayColor];
                    
                    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:messageString];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor color333333] range:nameRange];
                    
                    NSRange restaurantNameRange = [messageString rangeOfString:@"commented on"];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorGreen] range:restaurantNameRange];
                    
                    self.notificationLabel.attributedText = attributeStr;
                    self.notificationLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.frame.origin.y, self.notificationLabel.frame.size.width, self.notificationLabel.expectedHeight);

                }
                else
                {
                    NSString *messageString;
                    NSRange nameRange;
                    if (!notifications.isVendor && [notifications.userType intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also commented on %@'s photo",[notifications.restaurantName removeHTML],[notifications.dishUserName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.restaurantName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also commented on %@'s photo",[notifications.guestUserName removeHTML],[notifications.dishUserName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.guestUserName removeHTML]];
                    }
                    self.notificationLabel.textColor = [UIColor grayColor];
                    
                    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:messageString];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor color333333] range:nameRange];
                    
                    NSRange restaurantNameRange = [messageString rangeOfString:@"also commented on"];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorGreen] range:restaurantNameRange];
                    
                    self.notificationLabel.attributedText = attributeStr;
                    self.notificationLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.frame.origin.y, self.notificationLabel.frame.size.width, self.notificationLabel.expectedHeight);
                }
            }

        }
            break;
            case 7:
        {
            NSString *urlString = [notifications.guestUserAvatar returnFullImageUrlWithWidth:25];
            __weak typeof(self.iconImageView) weakRestaurantImageView = self.iconImageView;
//            [self.iconImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",urlString]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//                [UIView animateWithDuration:1.0 animations:^{
//                    [weakRestaurantImageView setAlpha:1.0];
//                }];
//            }];
            [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",urlString]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [UIView animateWithDuration:1.0 animations:^{
                                        [weakRestaurantImageView setAlpha:1.0];
                                    }];
            }];
            if ([notifications.subtype intValue] == 71){//多图like
            
                if ([user.userId intValue] == [notifications.hostUserId intValue])
                {
                    NSString *messageString;
                    NSRange nameRange;
                    if (!notifications.hostIsVendor && [notifications.guestIsVendor intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ liked your photos",[notifications.restaurantName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.restaurantName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ liked your photos",[notifications.guestUserName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.guestUserName removeHTML]];
                    }
                    self.notificationLabel.textColor = [UIColor grayColor];
                    
                    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:messageString];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor color333333] range:nameRange];
                    
                    NSRange restaurantNameRange = [messageString rangeOfString:@"liked"];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorRed] range:restaurantNameRange];
                    
                    self.notificationLabel.attributedText = attributeStr;
                    self.notificationLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.frame.origin.y, self.notificationLabel.frame.size.width, self.notificationLabel.expectedHeight);
                }
                else
                {
                    NSString *messageString;
                    NSRange nameRange;
                    if (!notifications.hostIsVendor && [notifications.guestIsVendor intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also liked %@'s photos",[notifications.restaurantName removeHTML],[notifications.dishUserName removeHTML]];
                    
                            nameRange = [messageString rangeOfString:[notifications.guestUserName removeHTML]];
                        
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also liked %@'s photos",[notifications.guestUserName removeHTML],[notifications.dishUserName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.guestUserName removeHTML]];
                        
                    }
                    self.notificationLabel.textColor = [UIColor grayColor];
                    
                    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:messageString];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor color333333] range:nameRange];
                    
                    NSRange restaurantNameRange = [messageString rangeOfString:@"also liked"];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorRed] range:restaurantNameRange];
                    
                    self.notificationLabel.attributedText = attributeStr;
                    self.notificationLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.frame.origin.y, self.notificationLabel.frame.size.width, self.notificationLabel.expectedHeight);
                    
                }
   
            
            }else if ([notifications.subtype intValue] == 72){//多图comment
            
                if ([user.userId intValue] == [notifications.hostUserId intValue])
                {
                    NSString *messageString;
                    NSRange nameRange;
                    if (!notifications.hostIsVendor && [notifications.guestIsVendor intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ commented your photos",[notifications.restaurantName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.restaurantName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ commented your photos",[notifications.guestUserName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.guestUserName removeHTML]];
                    }
                    self.notificationLabel.textColor = [UIColor grayColor];
                    
                    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:messageString];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor color333333] range:nameRange];
                    
                    NSRange restaurantNameRange = [messageString rangeOfString:@"commented"];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorGreen] range:restaurantNameRange];
                    
                    self.notificationLabel.attributedText = attributeStr;
                    self.notificationLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.frame.origin.y, self.notificationLabel.frame.size.width, self.notificationLabel.expectedHeight);
                    
                }
                else
                {
                    NSString *messageString;
                    NSRange nameRange;
                    if (!notifications.hostIsVendor && [notifications.guestIsVendor intValue] == 1)
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also commented on %@'s photos",[notifications.restaurantName removeHTML],[notifications.dishUserName removeHTML]];
                        nameRange = [messageString rangeOfString:[notifications.restaurantName removeHTML]];
                    }
                    else
                    {
                        messageString = [[NSString alloc] initWithFormat:@"%@ also commented on %@'s photos",[notifications.guestUserName removeHTML],[notifications.hostUserName removeHTML]];
                        
                        nameRange = [messageString rangeOfString:[notifications.guestUserName removeHTML]];
                        
                        
                    }
                    self.notificationLabel.textColor = [UIColor grayColor];
                    
                    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:messageString];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor color333333] range:nameRange];
                    
                    NSRange restaurantNameRange = [messageString rangeOfString:@"also commented on"];
                    [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorGreen] range:restaurantNameRange];
                    
                    self.notificationLabel.attributedText = attributeStr;
                    self.notificationLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.frame.origin.y, self.notificationLabel.frame.size.width, self.notificationLabel.expectedHeight);
                }
            }
        }
            break;
        case 12:{
            NSString *urlString = [notifications.couponSaveInfo objectForKey:@"logoImageUrl"];
            if ([urlString hasPrefix:@"http"]) {
                [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
            }else{
                [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:[urlString returnFullImageUrl]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
            }
            NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc]initWithString:notifications.message];
            
            [attributeStr addAttribute:NSForegroundColorAttributeName value:[UIColor color333333] range:NSMakeRange(0, attributeStr.length)];
            self.notificationLabel.attributedText = attributeStr;
            self.notificationLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.frame.origin.y, self.notificationLabel.frame.size.width, self.notificationLabel.expectedHeight);

            
            break;
        }
        default:
            break;
    }
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [formatter setDateFormat:@"MM/dd/YYYY HH:mm:ss"];
//    NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[notifications.createTime longLongValue]/1000];
//    NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
    
    self.creatTimeLabel.text= [Date getTimeInteval_v4:[notifications.createTime longLongValue]/1000];
    if (notifications.content.length)
    {
        self.contentLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.endPointY +5, self.notificationLabel.frame.size.width, 30);
        self.creatTimeLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.contentLabel.endPointY, self.notificationLabel.frame.size.width, 20);
        [self.contentLabel setHidden:NO];
    }
    else if ([notifications.subtype intValue] == 52)
    {
        NSString *commentStr = [notifications.commentContext removeHTML];
        if(commentStr.length>140){
            commentStr=[NSString stringWithFormat:@"\"%@...\"",[commentStr substringToIndex:140]];
        }
        else
            commentStr = [NSString stringWithFormat:@"\"%@\"",commentStr];
        [commentLabel setText:commentStr];
        float height = [self calculatedCommentTextHeight:commentStr];
        [commentLabel setFrame:CGRectMake(commentLabel.pointX, self.notificationLabel.endPointY+5, commentLabel.frame.size.width, height)];
        [commentLabel setHidden:NO];        
        [self.creatTimeLabel setFrame:CGRectMake(self.notificationLabel.frame.origin.x, commentLabel.endPointY + 10, self.notificationLabel.frame.size.width, 20)];
        [self.contentLabel setHidden:YES];
    }
    else if ([notifications.subtype intValue] == 72)
    {
        NSString *commentStr = [notifications.comment removeHTML];
        if(commentStr.length>140){
            commentStr=[NSString stringWithFormat:@"\"%@...\"",[commentStr substringToIndex:140]];
        }
        else
            commentStr = [NSString stringWithFormat:@"\"%@\"",commentStr];
        [commentLabel setText:commentStr];
        float height = [self calculatedCommentTextHeight:commentStr];
        [commentLabel setFrame:CGRectMake(commentLabel.pointX, self.notificationLabel.endPointY+5, commentLabel.frame.size.width, height)];
        [commentLabel setHidden:NO];
        [self.creatTimeLabel setFrame:CGRectMake(self.notificationLabel.frame.origin.x, commentLabel.endPointY + 10, self.notificationLabel.frame.size.width, 20)];
        [self.contentLabel setHidden:YES];
    }

    else if ([notifications.type intValue] == 6 && [notifications.subtype intValue] == 51)
    {
        [dishImageView setHidden:NO];
        dishImageView.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.endPointY + 10, 45, 45);
        IMGDish *dish = [notifications.dishList firstObject];
        NSString *urlString = [dish.imageUrl returnFullImageUrlWithWidth:50];
        __weak typeof(dishImageView) weakDishImageView = dishImageView;
//        [dishImageView setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//            [UIView animateWithDuration:1.0 animations:^{
//                [weakDishImageView setAlpha:1.0];
//            }];
//        }];
        [dishImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [UIView animateWithDuration:1.0 animations:^{
                                [weakDishImageView setAlpha:1.0];
                            }];
        }];

        self.creatTimeLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, dishImageView.endPointY + 10, self.notificationLabel.frame.size.width, 20);
        [self.contentLabel setHidden:YES];

    }
    else if ([notifications.type intValue] == 7 && [notifications.subtype intValue] == 71)
    {
        
        dishView=[[UIView alloc]initWithFrame:CGRectMake(0, self.notificationLabel.endPointY + 10, DeviceWidth, 45)];
        dishView.hidden=NO;
        [self.contentView addSubview:dishView];
        for (int i=0; i<notifications.dishList.count; i++) {
            
            if (i == 3&& notifications.dishList.count!=4)
            {
                UIImageView* fourImageView=[[UIImageView alloc]initWithFrame:CGRectMake(self.notificationLabel.frame.origin.x+55*i, 0, 45, 45)];
                fourImageView.backgroundColor=[UIColor colorWithRed:238/255.0 green:238/255.0 blue:238/255.0 alpha:1];
                [dishView addSubview:fourImageView];
                UILabel *numLabel = [[UILabel alloc] init];
                numLabel.frame = CGRectMake(fourImageView.frame.size.width/2-8,fourImageView.frame.size.height/2-18,35,35);
                numLabel.font = [UIFont systemFontOfSize:15];
                numLabel.textColor = [UIColor color333333];
                [numLabel setText:[NSString stringWithFormat:@"+%lu",notifications.dishList.count -3]];
                [fourImageView addSubview:numLabel];
                    break;
            }
            
             ImageView=[[UIImageView alloc]initWithFrame:CGRectMake(self.notificationLabel.frame.origin.x+55*i, 0, 45, 45)];
            [dishView addSubview:ImageView];
            IMGDish *dish = [notifications.dishList objectAtIndex:i];
            NSString *urlString = [dish.imageUrl returnFullImageUrlWithWidth:55];
//            [ImageView setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//            }];
            [ImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
            }];
        }
        
        self.creatTimeLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, dishView.endPointY + 10, self.notificationLabel.frame.size.width, 20);
        [self.contentLabel setHidden:YES];
        
    }

    else
    {
        self.creatTimeLabel.frame = CGRectMake(self.notificationLabel.frame.origin.x, self.notificationLabel.endPointY + 5, self.notificationLabel.frame.size.width, 20);
        [self.contentLabel setHidden:YES];
    }
    
    lineImage.frame = CGRectMake(0, self.creatTimeLabel.endPointY + 10, DeviceWidth, 1);

    if (notifications.icon.length)
    {
        NSString *urlString;
        if ([notifications.icon hasPrefix:@"http://"]||[notifications.icon hasPrefix:@"https://"])
        {
            urlString = notifications.icon;
        }
        else
            urlString = [notifications.icon returnFullImageUrlWithWidth:25];
        __weak typeof(self.iconImageView) weakRestaurantImageView = self.iconImageView;
//        [self.iconImageView setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//            [UIView animateWithDuration:1.0 animations:^{
//                [weakRestaurantImageView setAlpha:1.0];
//            }];
//        }];
        [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [UIView animateWithDuration:1.0 animations:^{
                                [weakRestaurantImageView setAlpha:1.0];
                            }];
        }];
    }
    
    
    
    if (!notifications.isRead)
    {
        [self.pointImageView setHidden:NO];
    }
    else
        [self.pointImageView setHidden:YES];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(CGFloat)calculatedCommentTextHeight:(NSString*)comment{
    NSDictionary *attribute = @{NSFontAttributeName: [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:10]};
    CGSize singleSize = [@"test" boundingRectWithSize:CGSizeMake(DeviceWidth - (40 + LEFTLEFTSET) - 30, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    CGSize textSize = [comment boundingRectWithSize:CGSizeMake(DeviceWidth - (40 + LEFTLEFTSET) - 30, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:attribute context:nil].size;
    NSInteger lines = textSize.height/singleSize.height;
    CGFloat height=0.0f;
    if(lines>3){
        height=3*singleSize.height;
    }else{
        height=textSize.height;
    }
    return height;
}


@end
