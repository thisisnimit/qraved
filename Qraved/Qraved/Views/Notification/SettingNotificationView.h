//
//  SettingNotificationView.h
//  Qraved
//
//  Created by harry on 2018/3/16.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingNotificationView : UIView

@property (nonatomic, copy) void(^settingTapped)();
@property (nonatomic, copy) void(^closeView)();

@end
