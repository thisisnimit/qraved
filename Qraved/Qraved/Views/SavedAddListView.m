//
//  SavedAddListView.m
//  Qraved
//
//  Created by apple on 2017/5/18.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "SavedAddListView.h"
#import "CityUtils.h"
#import "IMGCity.h"
#import "ListHandler.h"
#import "IMGMyList.h"
#define LEFT_X 15
#define LEFT_Y 13
#define LEFT_IMAGE 15
@interface SavedAddListView ()<UITextFieldDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate>


@property (nonatomic, retain) NSMutableArray *listArr;

@end

@implementation SavedAddListView{

    UIButton *btnNewlist;
    UICollectionView *listCollectionView;
    int max;
    int myListOffset;
    int myListCount;
    BOOL hasListData;
    int page;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        [self loadList];
       
        
    }
    return self;
}
- (void)initUI{

    UIView *bgView = [[UIView alloc] initWithFrame:[AppDelegate ShareApp].window.bounds];
    bgView.backgroundColor =[UIColor colorWithRed:0 green:0 blue:0 alpha:0.6];
    [self addSubview:bgView];
    
    UIView *tapView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, bgView.size.height-210)];
    [self addSubview:tapView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeSavedlistView)];
    [tapView addGestureRecognizer:tap];
    
    
    UIView *listView = [[UIView alloc] initWithFrame:CGRectMake(0, bgView.size.height - 210, DeviceWidth, 210)];
    listView.backgroundColor = [UIColor whiteColor];
    [bgView addSubview:listView];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_X, LEFT_Y, 120, 20)];
    lblTitle.text = @"Save in your list";
    lblTitle.font = [UIFont systemFontOfSize:14];
    [listView addSubview:lblTitle];
    
    btnNewlist = [[UIButton alloc] initWithFrame:CGRectMake(DeviceWidth - LEFT_X - 140, LEFT_Y, 140, 20)];
    [btnNewlist setTitle:@" Create New List" forState:UIControlStateNormal];
    [btnNewlist setTitleColor:[UIColor barButtonTitleColor] forState:UIControlStateNormal];
    [btnNewlist setImage:[UIImage imageNamed:@"ic_list_plus"] forState:UIControlStateNormal];
    btnNewlist.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    btnNewlist.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [btnNewlist addTarget:self action:@selector(createNewList:) forControlEvents:UIControlEventTouchUpInside];
    [listView addSubview:btnNewlist];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    
    layout.itemSize = CGSizeMake(140, 140);
    
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    
    layout.sectionInset = UIEdgeInsetsMake(0,0, 0, 10);
    
    layout.minimumInteritemSpacing = 10;
    
    layout.minimumLineSpacing = 10;
    
    listCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(LEFT_X, lblTitle.endPointY + LEFT_Y, DeviceWidth-LEFT_X, 140) collectionViewLayout:layout];
    listCollectionView.backgroundColor = [UIColor whiteColor];
    [listView addSubview:listCollectionView];
    
    listCollectionView.delegate = self;
    listCollectionView.dataSource = self;
    
    [listCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"listCell"];
    
    max = 10;
    myListOffset = 0;
    page = 1;
    self.listArr = [NSMutableArray array];
}
#pragma mark - UICollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.listArr.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"listCell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    IMGMyList *list = [self.listArr objectAtIndex:indexPath.row];;
    
    for (UIView *subView in cell.contentView.subviews) {
        [subView removeFromSuperview];
    }
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:cell.contentView.bounds];
    [cell.contentView addSubview:imgView];
    
    NSArray *images=[list.listImageUrls componentsSeparatedByString:@","];
    UIImage *placehoderImage = [[UIImage imageNamed:DEFAULT_IMAGE_STRING]  imageByScalingAndCroppingForSize:CGSizeMake(140,140)];
    NSString* imgStr = [images[0] returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution];
    __weak typeof (imgView) weakListImageView = imgView;
    if(list.listImageUrls && list.listImageUrls.length && images.count>0){
        [imgView sd_setImageWithURL:[NSURL URLWithString:imgStr] placeholderImage:placehoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image) {
                image = [image imageByScalingAndCroppingForSize:CGSizeMake(140,140)];
                weakListImageView.image = image;
            }
            
        }];
    }else{
        imgView.image = placehoderImage;
        
    }
    
    [self insertTransparentGradient:imgView];

    UILabel *lblListName = [[UILabel alloc] initWithFrame:CGRectMake(10, cell.contentView.bounds.size.height-25, cell.contentView.size.width-20, 20)];
    lblListName.text = [list.listName uppercaseString];
    lblListName.font = [UIFont boldSystemFontOfSize:14];
    lblListName.textColor = [UIColor whiteColor];
    [cell.contentView addSubview:lblListName];

    return cell;
}

- (void) insertTransparentGradient:(UIImageView *)imageView {
    UIColor *colorOne = [UIColor colorWithWhite:0 alpha:0];
    UIColor *colorTwo = [UIColor colorWithWhite:0 alpha:1];
    NSArray *colors = [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, nil];
    NSNumber *stopOne = [NSNumber numberWithFloat:0.5];
    NSNumber *stopTwo = [NSNumber numberWithFloat:1.0];
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    
    //crate gradient layer
    CAGradientLayer *headerLayer = [CAGradientLayer layer];
    
    headerLayer.colors = colors;
    headerLayer.locations = locations;
    
    headerLayer.frame = CGRectMake(0, 0, imageView.size.width, imageView.size.height);
    [imageView.layer insertSublayer:headerLayer atIndex:0];
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    IMGMyList *myList = [self.listArr objectAtIndex:indexPath.row];
    [self removeFromSuperview];
    if (self.delegate && [self.delegate respondsToSelector:@selector(addProductToList:)]) {
        [self.delegate addProductToList:myList.listId];
    }
    
}
- (void)removeSavedlistView{
    [self removeFromSuperview];
}
- (void)loadList{
    [[LoadingView sharedLoadingView] startLoading];
    [ListHandler getListWithUser:[IMGUser currentUser] andOffset:myListOffset andMax:max andBlock:^(NSArray *dataArray, int listCount, BOOL hasData) {
        [[LoadingView sharedLoadingView] stopLoading];
        [self initUI];
        if (myListOffset==0) {
            [self.listArr removeAllObjects];
        }

        if (listCount>myListOffset) {
            myListOffset = myListOffset +10;
        }
        
        
        hasListData = hasData;
        myListCount = listCount;
        [self.listArr addObjectsFromArray:dataArray];
        if (self.listArr.count%10>0) {
            page = (int)self.listArr.count/10 +1;
        }else{
            page = (int)self.listArr.count/10;
        }
        [listCollectionView reloadData];
        
    } andFailedBlock:^{
         [[LoadingView sharedLoadingView] stopLoading];
        
    }];

}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView==listCollectionView) {
        if (!hasListData) {
            
            return;
        }
        if (myListCount<(page-1)*10) {
            return;
        }
        if (scrollView.contentOffset.x +scrollView.width - scrollView.contentSize.width > 30) {
            page ++;
            if ((page-1)*10<=myListOffset) {
                [self loadList];
            }
          
        }
    }
}
- (void)createNewList:(UIButton *)sender{
    [self removeFromSuperview];
    if (self.delegate && [self.delegate respondsToSelector:@selector(addNewList)]) {
        [self.delegate addNewList];
    }
}

- (void)tapListImage:(UITapGestureRecognizer *)tap{
    [self removeFromSuperview];
    UIView *tapView = tap.view;
    if (self.delegate && [self.delegate respondsToSelector:@selector(addProductToList:)]) {
        [self.delegate addProductToList:[NSNumber numberWithInteger:tapView.tag]];
    }
}

@end
