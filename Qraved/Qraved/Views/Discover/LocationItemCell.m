//
//  LocationItemCell.m
//  Qraved
//
//  Created by Jeff on 8/6/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocationItemCell.h"
#import "IMGLocation.h"

@implementation LocationItemCell

- (id)initWithStyle:(UITableViewCellStyle)style selectionStyle:(UITableViewCellSelectionStyle)selectionStyle reuseIdentifier:(NSString *)reuseIdentifier
{
    return [[LocationItemCell alloc] initWithStyle:style selectionStyle:selectionStyle reuseIdentifier:reuseIdentifier withFrame:CGRectMake(0, 0, DeviceWidth, 80)];
}

@end
