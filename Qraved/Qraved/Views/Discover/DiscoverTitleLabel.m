//
//  UIDiscoverTitleLabel.m
//  Qraved
//
//  Created by Jeff on 8/26/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "DiscoverTitleLabel.h"
#import "UIConstants.h"
#import "UIColor+Helper.h"

@implementation DiscoverTitleLabel

- (id)initWithFrame:(CGRect)frame title:(NSString *)title
{
    self = [super initWithFrame:frame];
    if (self) {
        UIFont *titleFont=[UIFont fontWithName:FONT_GOTHAM_BOOK size:24];
        UIColor *textColor=[UIColor whiteColor];
        
        [self setBackgroundColor:[UIColor clearColor]];
        [self setTextColor:textColor];
        [self setFont:titleFont];
        [self setLineBreakMode:NSLineBreakByWordWrapping];
        [self setNumberOfLines:0];
        [self setText:title];
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc]init];
        paragraphStyle.lineSpacing = 9;
        
        NSDictionary *attributes = @{ NSFontAttributeName:titleFont, NSParagraphStyleAttributeName:paragraphStyle};
        self.attributedText = [[NSAttributedString alloc]initWithString:self.text attributes:attributes];
        [self setTextAlignment:NSTextAlignmentCenter];
    }
    return self;
}

@end
