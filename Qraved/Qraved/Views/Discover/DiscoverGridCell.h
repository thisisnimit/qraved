//
//  UIGridViewCell.h
//  Qraved
//
//  Created by Jeff on 8/5/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiscoverGridCell : UITableViewCell

@property(nonatomic,strong)NSArray *buttons;

@end