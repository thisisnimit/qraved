//
//  DiscoverGridCellView.m
//  Qraved
//
//  Created by Jeff on 8/5/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "DiscoverGridCellView.h"
#import "UIImageButton.h"

#import "Consts.h"
#import "UIConstants.h"
#import "UIButton+WebCache.h"
#import "NSString+Helper.h"
#import "UIImage+Helper.h"

@interface DiscoverGridCellView ()
@property(nonatomic,strong)UIImage *image;
@end

@implementation DiscoverGridCellView

#define COLUMN_WIDTH  152
#define CELL_HEIGHT  100


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
/*
- (id)initWithDiscoverCategory:(IMGTag *)tag andRow:(NSInteger)row andColumn:(NSInteger)column
{
    CGRect frame = CGRectMake(0, 0, COLUMN_WIDTH, CELL_HEIGHT);
    self = [super initWithFrame:frame];
    if (self) {
        self.image = [UIImage imageNamed:@"pic-bg_discover"];//[self cutCenterImage:[UIImage imageNamed:@"macbook_pro.jpg"]  size:CGSizeMake(100, 100)];
        UIImageButton *button = [UIImageButton buttonWithType:UIButtonTypeCustom];
        button.frame = frame;
        button.tag=[tag.tagId intValue];
        [button setValue:[NSNumber numberWithLong:column] forKey:@"column"];
        [button setValue:[NSNumber numberWithLong:row] forKey:@"row"];
        [button addTarget:self action:@selector(imageItemClick:) forControlEvents:UIControlEventTouchUpInside];
        [button setBackgroundImage:self.image forState:UIControlStateNormal];
       
        [self addSubview:button];
        
        
        UIFont *font=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16];
        UIColor *textColor=[UIColor whiteColor];
        UILabel *categoryTitleLabel=[[UILabel alloc] initWithFrame:CGRectMake(16, 0, COLUMN_WIDTH-32, self.frame.size.height)];
        [categoryTitleLabel setTextAlignment:NSTextAlignmentCenter];
        [categoryTitleLabel setText:tag.name];
        [categoryTitleLabel setTextColor:textColor];
        [categoryTitleLabel setFont:font];
        [categoryTitleLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [categoryTitleLabel setNumberOfLines:2];
        [categoryTitleLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:categoryTitleLabel];
        
        self.bounds = CGRectMake(0, 0, COLUMN_WIDTH, CELL_HEIGHT);
        self.center = CGPointMake((1 + column) * 5 + COLUMN_WIDTH *( 0.5 + column) , 5 + CELL_HEIGHT * 0.5);
    }
    return self;
}
 */

- (id)initWithDiscover:(IMGDiscover *)discover andRow:(NSInteger)row andColumn:(NSInteger)column
{
    CGRect frame = CGRectMake(0, 0, COLUMN_WIDTH, CELL_HEIGHT);
    self = [super initWithFrame:frame];
    if (self) {
        self.image = [UIImage imageNamed:@"pic-bg_discover"];//[self cutCenterImage:[UIImage imageNamed:@"macbook_pro.jpg"]  size:CGSizeMake(100, 100)];
        UIImageButton *button = [UIImageButton buttonWithType:UIButtonTypeCustom];
        button.frame = frame;
        UIImageView *coverImageView = [[UIImageView alloc]initWithFrame:button.bounds];
        UIImage *placeHolderImage = [UIImage imageWithColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5] size:CGSizeMake(DeviceWidth, 159)];
        [coverImageView setImage:placeHolderImage];
        [button addSubview:coverImageView];
        button.tag=[discover.discoverId intValue];
        [button setValue:[NSNumber numberWithLong:column] forKey:@"column"];
        [button setValue:[NSNumber numberWithLong:row] forKey:@"row"];
        [button addTarget:self action:@selector(imageItemClick:) forControlEvents:UIControlEventTouchUpInside];
//        [button setBackgroundImage:self.image forState:UIControlStateNormal];
        [button sd_setImageWithURL:[NSURL URLWithString:[discover.imageUrl returnFullImageUrl]] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING]];
        [self addSubview:button];
        
        
        UIFont *font=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16];
        UIColor *textColor=[UIColor whiteColor];
        UILabel *categoryTitleLabel=[[UILabel alloc] initWithFrame:CGRectMake(16, 0, COLUMN_WIDTH-32, self.frame.size.height)];
        [categoryTitleLabel setTextAlignment:NSTextAlignmentCenter];
        [categoryTitleLabel setText:discover.name];
        [categoryTitleLabel setTextColor:textColor];
        [categoryTitleLabel setFont:font];
        [categoryTitleLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [categoryTitleLabel setNumberOfLines:2];
        [categoryTitleLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:categoryTitleLabel];
        
        self.bounds = CGRectMake(0, 0, COLUMN_WIDTH, CELL_HEIGHT);
        self.center = CGPointMake((1 + column) * 5 + COLUMN_WIDTH *( 0.5 + column) , 5 + CELL_HEIGHT * 0.5);
    }
    return self;
}


-(void)imageItemClick:(UIImageButton *)button{
    
//    NSString *msg = [NSString stringWithFormat:@"row %i column%i",button.row + 1, button.column + 1];
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Tip"
//                                                    message:msg
//                                                   delegate:nil
//                                          cancelButtonTitle:@"ok"
//                                          otherButtonTitles:nil, nil];
//    [alert show];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_POPUP_LOCATIONLISTVIEWCONTROLLER object:nil  userInfo:@{@"discoverId":[NSNumber numberWithLong:button.tag]}];
}



-(UIImage *)cutCenterImage:(UIImage *)image size:(CGSize)size{
    CGSize imageSize = image.size;
    CGRect rect;

    if (imageSize.width > imageSize.height) {
        float leftMargin = (imageSize.width - imageSize.height) * 0.5;
        rect = CGRectMake(leftMargin, 0, imageSize.height, imageSize.height);
    }else{
        float topMargin = (imageSize.height - imageSize.width) * 0.5;
        rect = CGRectMake(0, topMargin, imageSize.width, imageSize.width);
    }
    
    CGImageRef imageRef = image.CGImage;

    CGImageRef imageRefRect = CGImageCreateWithImageInRect(imageRef, rect);
    
    UIImage *tmp = [[UIImage alloc] initWithCGImage:imageRefRect];
    CGImageRelease(imageRefRect);
    
    UIGraphicsBeginImageContext(size);
    CGRect rectDraw = CGRectMake(0, 0, size.width, size.height);
    [tmp drawInRect:rectDraw];

    tmp = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return tmp;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
