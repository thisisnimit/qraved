//
//  DiscoverGridCellView.h
//  Qraved
//
//  Created by Jeff on 8/5/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGTag.h"
#import "IMGDiscover.h"

@interface DiscoverGridCellView : UIView

- (id)initWithFrame:(CGRect)frame;


//- (id)initWithDiscoverCategory:(IMGTag *)discoverCategory andRow:(NSInteger)row andColumn:(NSInteger)column;

- (id)initWithDiscover:(IMGDiscover *)discover andRow:(NSInteger)row andColumn:(NSInteger)column;

@end
