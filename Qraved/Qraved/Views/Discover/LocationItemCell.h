//
//  LocationItemCell.h
//  Qraved
//
//  Created by Jeff on 8/6/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGLocation.h"
#import "CheckableTableViewCell.h"

@interface LocationItemCell : CheckableTableViewCell
@property(nonatomic,strong) IMGLocation *object;


@end
