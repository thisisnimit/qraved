//
//  SearchEmptyResultView.m
//  Qraved
//
//  Copyright (c) 2016年 Imaginato. All rights reserved.
//

#import "SearchEmptyResultView.h"
#import "UIView+Helper.h"
#import "AppDelegate.h"
#import "RestaurantSuggestViewController.h"

@implementation SearchEmptyResultView

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        UIImageView *icon = [[UIImageView alloc] initWithFrame:CGRectMake((DeviceWidth-102)/2, 42.5, 102, 90)];
        icon.image = [UIImage imageNamed:@"searchEmptyResult@2x.png"];
        [self addSubview:icon];
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, icon.endPointY+28.5, DeviceWidth, 100)];
        label1.text = L(@"Looks like there are no matching results");
        label1.numberOfLines = 0;
        label1.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:12.45];
        label1.textAlignment = NSTextAlignmentCenter;
        label1.textColor = [UIColor colorWithRed:61.0/255.0 green:66.0/255.0 blue:69.0/255.0 alpha:1];
        [label1 sizeToFit];
        label1.frame = CGRectMake((DeviceWidth-label1.frame.size.width)/2, icon.endPointY+28.5, label1.frame.size.width, label1.frame.size.height);
        [self addSubview:label1];
        
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(0, label1.endPointY+10, DeviceWidth, 100)];
        label2.text = L(@"Check for spelling mistake or typos\nTry to search by cuisine, location or landmark");
        label2.numberOfLines = 0;
        label2.font = [UIFont fontWithName:@"HelveticaNeue" size:11.26];
        label2.textAlignment = NSTextAlignmentCenter;
        label2.textColor = [UIColor colorWithRed:147.0/255.0 green:147.0/255.0 blue:147.0/255.0 alpha:1];
        [label2 sizeToFit];
        label2.frame = CGRectMake((DeviceWidth-label2.frame.size.width)/2, label1.endPointY+10, label2.frame.size.width, label2.frame.size.height);
        [self addSubview:label2];
        
        UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0, label2.endPointY+15, 57, 1)];
        line1.backgroundColor = [UIColor colorWithRed:213.0/255.0 green:216.0/255.0 blue:221.0/255.0 alpha:1];
        [self addSubview:line1];
        
        UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(0, label2.endPointY+15, DeviceWidth, 100)];
        label3.text = L(@"Or");
        label3.numberOfLines = 0;
        label3.font = [UIFont fontWithName:@"HelveticaNeue-Italic" size:11.26];
        label3.textAlignment = NSTextAlignmentCenter;
        label3.textColor = [UIColor colorWithRed:147.0/255.0 green:147.0/255.0 blue:147.0/255.0 alpha:1];
        [label3 sizeToFit];
        label3.frame = CGRectMake((DeviceWidth-label3.frame.size.width)/2, label2.endPointY+10, label3.frame.size.width, label3.frame.size.height);
        [self addSubview:label3];
        
        UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(label3.endPointX+23, label2.endPointY+15, 57, 1)];
        line2.backgroundColor = [UIColor colorWithRed:213.0/255.0 green:216.0/255.0 blue:221.0/255.0 alpha:1];
        [self addSubview:line2];
        
        line1.frame = CGRectMake((DeviceWidth - 114 - 46 - label3.frame.size.width)/2, label3.center.y, 57, 1);
        line2.frame = CGRectMake(label3.endPointX+23, label3.center.y, 57, 1);
        
        UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(0, label3.endPointY+15, DeviceWidth, 100)];
        label4.text = L(@"Did we miss something?");
        label4.numberOfLines = 0;
        label4.font = [UIFont fontWithName:@"HelveticaNeue" size:11.26];
        label4.textAlignment = NSTextAlignmentCenter;
        label4.textColor = [UIColor colorWithRed:147.0/255.0 green:147.0/255.0 blue:147.0/255.0 alpha:1];
        [label4 sizeToFit];
        label4.frame = CGRectMake((DeviceWidth-label4.frame.size.width)/2, label3.endPointY+15, label4.frame.size.width, label4.frame.size.height);
        [self addSubview:label4];
        
        UIButton *suggestANewPlaceBtn = [[UIButton alloc] initWithFrame:CGRectMake((DeviceWidth - 127.5)/2, label4.endPointY + 15, 127.5, 22)];
        [suggestANewPlaceBtn setTitle:L(@"Suggest a New Place") forState:UIControlStateNormal];
        [suggestANewPlaceBtn setTitleColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1] forState:UIControlStateNormal];
        suggestANewPlaceBtn.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:11.26];
        suggestANewPlaceBtn.backgroundColor = [UIColor colorWithRed:223.0/255.0 green:66.0/255.0 blue:84.0/255.0 alpha:1];
        suggestANewPlaceBtn.clipsToBounds = YES;
        suggestANewPlaceBtn.layer.cornerRadius = 5;
        [suggestANewPlaceBtn addTarget:self action:@selector(suggestANewPlaceBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:suggestANewPlaceBtn];
        
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, suggestANewPlaceBtn.endPointY+10);
        
    }
    return self;
}

-(void)suggestANewPlaceBtnClick{
    if ((self.delegate !=nil) && ([self.delegate respondsToSelector:@selector(gotoSuggest)])) {
        [self.delegate gotoSuggest];
    }
}

@end
