//
//  UIDiscoverTitleLabel.h
//  Qraved
//
//  Created by Jeff on 8/26/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiscoverTitleLabel : UILabel

- (id)initWithFrame:(CGRect)frame title:(NSString *)title;

@end
