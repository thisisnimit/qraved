//
//  SearchEmptyResultView.h
//  Qraved
//
//  Copyright (c) 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SearchEmptyResultViewDelegate <NSObject>

- (void)gotoSuggest;

@end

@interface SearchEmptyResultView : UIView

@property (nonatomic, weak) id<SearchEmptyResultViewDelegate> delegate;

@end
