//
//  ReviewPublishMoreRestaurantView.h
//  Qraved
//
//  Created by harry on 2017/8/24.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewPublishMoreRestaurantView : UIView<TTGTextTagCollectionViewDelegate>

@property (nonatomic, strong) NSArray *restaurantArr;

@property (nonatomic, copy) void(^selectRestaurant)(IMGRestaurant *restaurant);

@end
