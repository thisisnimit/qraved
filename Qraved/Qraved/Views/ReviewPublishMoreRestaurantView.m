//
//  ReviewPublishMoreRestaurantView.m
//  Qraved
//
//  Created by harry on 2017/8/24.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "ReviewPublishMoreRestaurantView.h"
#import "ContributionListModel.h"

@implementation ReviewPublishMoreRestaurantView
{
    TTGTextTagCollectionView *tagCollectionView;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    
    self.backgroundColor = [UIColor whiteColor];
    
    tagCollectionView = [[TTGTextTagCollectionView alloc] initWithFrame:CGRectMake(0, 2, DeviceWidth, 44)];
    tagCollectionView.defaultConfig.tagExtraSpace = CGSizeMake(14, 16);
    
    tagCollectionView.delegate = self;
    tagCollectionView.defaultConfig.tagTextFont = [UIFont systemFontOfSize:12];
    tagCollectionView.scrollDirection = TTGTagCollectionScrollDirectionHorizontal;
    tagCollectionView.alignment = TTGTagCollectionAlignmentLeft;
    tagCollectionView.numberOfLines = 1;
    tagCollectionView.defaultConfig.tagBackgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    tagCollectionView.defaultConfig.tagSelectedBackgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    tagCollectionView.defaultConfig.tagTextColor = [UIColor color333333];
    tagCollectionView.showsHorizontalScrollIndicator = NO;
    tagCollectionView.defaultConfig.tagSelectedTextColor = [UIColor color333333];
    tagCollectionView.defaultConfig.tagBorderWidth = 0;
    tagCollectionView.defaultConfig.tagBorderColor = [UIColor clearColor];
    tagCollectionView.contentInset = UIEdgeInsetsMake(5, 15, 5, 5);
    tagCollectionView.defaultConfig.tagShadowOffset = CGSizeMake(0, 0);
    tagCollectionView.defaultConfig.tagShadowRadius = 0;
    tagCollectionView.defaultConfig.tagShadowOpacity = 0;
    
    [self addSubview:tagCollectionView];
}

- (void)textTagCollectionView:(TTGTextTagCollectionView *)textTagCollectionView didTapTag:(NSString *)tagText atIndex:(NSUInteger)index selected:(BOOL)selected{
    ContributionListModel *model = [self.restaurantArr objectAtIndex:index];
    
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    restaurant.restaurantId = model.restaurant_id;
    restaurant.title = model.restaurant_name;
    restaurant.cuisineName = model.cuisine;
    
    if (self.selectRestaurant) {
        self.selectRestaurant(restaurant);
    }
    
}

- (void)setRestaurantArr:(NSArray *)restaurantArr{
    _restaurantArr = restaurantArr;
    
    NSMutableArray *arr = [NSMutableArray array];
    for (ContributionListModel *model in restaurantArr) {
        NSString *str = [NSString stringWithFormat:@"%@ %@",model.restaurant_name,model.cuisine];
        [arr addObject:str];
    }
    
    [tagCollectionView addTags:arr];
    
}

@end
