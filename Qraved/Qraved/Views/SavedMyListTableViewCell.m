//
//  SavedMyListTableViewCell.m
//  Qraved
//
//  Created by apple on 2017/5/12.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "SavedMyListTableViewCell.h"
#import "UIImageView+Helper.h"
#import "UIImage+Resize.h"
#import "NSString+Helper.h"
#import "SavedAddListView.h"
#define IMAGEWIDTH (DeviceWidth-45)/2

@interface SavedMyListTableViewCell ()<SavedAddListViewDelegate>

@end
@implementation SavedMyListTableViewCell{
    UIImageView *firstImageView;
    IMGMyList *firstList;
    IMGMyList *secList;
    UILabel* firstLabel;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        firstImageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, IMAGEWIDTH, IMAGEWIDTH)];
        firstImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:firstImageView];
        
        firstLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, IMAGEWIDTH-34+15, IMAGEWIDTH-5, 40)];
        firstLabel.textColor = [UIColor whiteColor];
        firstLabel.font = [UIFont boldSystemFontOfSize:14];
        //firstLabel.text = myList.listName;
        [self addSubview:firstLabel];
        
        firstImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapfirstListImage:)];
        [firstImageView addGestureRecognizer:tap];
       
        _secondImageView = [[UIImageView alloc] initWithFrame:CGRectMake(firstImageView.endPointX+15, 15, IMAGEWIDTH, IMAGEWIDTH)];
        _secondImageView.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_secondImageView];

        
        self.lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth-10-IMAGEWIDTH, IMAGEWIDTH-34+15, IMAGEWIDTH-5, 40)];
        self.lblTitle.textColor = [UIColor whiteColor];
        self.lblTitle.font = [UIFont boldSystemFontOfSize:14];
        //secondLabel.text = myList.listName;
        [self addSubview:self.lblTitle];
        
        _secondImageView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapS = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapSecListImage:)];
        [_secondImageView addGestureRecognizer:tapS];
        
        
         [self insertTransparentGradient];
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setFirstMyList:(IMGMyList *)myList withIndex:(NSInteger)index{
    firstList = myList;
//    if (firstImageView) {
//        [firstImageView removeFromSuperview];
//    }
    
    firstLabel.text = myList.listName;
    NSArray *images=[myList.listImageUrls componentsSeparatedByString:@","];
    UIImage *placehoderImage = [[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(IMAGEWIDTH,IMAGEWIDTH)];

    __weak typeof (firstImageView) weakFirstImageView = firstImageView;
    if(myList.listImageUrls && myList.listImageUrls.length && images.count>0){
        [firstImageView sd_setImageWithURL:[NSURL URLWithString:[images[0] returnFullImageUrlWithImageSizeType:ImageSizeTypeThumbnail]] placeholderImage:placehoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image) {
                image = [image imageByScalingAndCroppingForSize:CGSizeMake(IMAGEWIDTH,IMAGEWIDTH)];
                weakFirstImageView.image = image;
            }
            
        }];
    }else{
        firstImageView.image = placehoderImage;
        
    }
    // }
}
-(void)setSecondMyList:(IMGMyList *)myList{
    secList = myList;
    NSArray *images=[myList.listImageUrls componentsSeparatedByString:@","];
    UIImage *placehoderImage = [[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(IMAGEWIDTH,IMAGEWIDTH)];
//    if (_secondImageView) {
//        [_secondImageView removeFromSuperview];
//    }
//    
    self.lblTitle.text = myList.listName ;
    __weak typeof (_secondImageView) weakSecondImageView = _secondImageView;
    if(myList.listImageUrls && myList.listImageUrls.length && images.count>0){
        [_secondImageView sd_setImageWithURL:[NSURL URLWithString:[images[0] returnFullImageUrlWithImageSizeType:ImageSizeTypeThumbnail]] placeholderImage:placehoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (image) {
                image = [image imageByScalingAndCroppingForSize:CGSizeMake(IMAGEWIDTH,IMAGEWIDTH)];
                weakSecondImageView.image = image;
            }
            
        }];
    }else{
        
        _secondImageView.image = placehoderImage;
    }
    
}
//-(void)addIMageVIewClicked{
//    SavedAddListView *savedAddListView = [[SavedAddListView alloc] init];
//    savedAddListView.delegate = self;
//    savedAddListView.frame = [AppDelegate ShareApp].window.bounds;
//    [[AppDelegate ShareApp].window addSubview:savedAddListView];
//}
-(void)loadNewList{
    if ([self.delegate respondsToSelector:@selector(reloadForNewList)]) {
        [self.delegate reloadForNewList];
    }
}
-(void)handleTextFieldTextDidChangeNotification:(NSNotification *)notification{
    //    [notification resignFirstResponder];
    UITextField *temTextFiled = (UITextField*)notification.object;
    [temTextFiled resignFirstResponder];
    UIView *cityView = [[UIView alloc] initWithFrame:CGRectMake(temTextFiled.startPointX, temTextFiled.endPointX, temTextFiled.size.width, 44)];
    cityView.backgroundColor = [UIColor redColor];
    [temTextFiled.superview addSubview:cityView];
}
- (void)tapSecListImage:(UITapGestureRecognizer *)tap{
    if ([self.delegate respondsToSelector:@selector(gotoListPage:)]) {
        [self.delegate gotoListPage:secList];
    }
}
- (void)tapfirstListImage:(UITapGestureRecognizer *)tap{
    if ([self.delegate respondsToSelector:@selector(gotoListPage:)]) {
        [self.delegate gotoListPage:firstList];
    }
}
- (void) insertTransparentGradient {
    if (firstImageView) {
        CAGradientLayer *headerLayer;
        UIColor *colorOne = [UIColor colorWithWhite:0 alpha:0];
        UIColor *colorTwo = [UIColor colorWithWhite:0 alpha:1];
        NSArray *colors = [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, nil];
        NSNumber *stopOne = [NSNumber numberWithFloat:0.7];
        NSNumber *stopTwo = [NSNumber numberWithFloat:1.0];
        NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
        
        //crate gradient layer
        if (!headerLayer) {
            headerLayer = [CAGradientLayer layer];
            
            headerLayer.colors = colors;
            headerLayer.locations = locations;
            
            headerLayer.frame = CGRectMake(0, 0, IMAGEWIDTH, IMAGEWIDTH);
            [firstImageView.layer insertSublayer:headerLayer atIndex:0];
        }
    }
    if (_secondImageView) {
        CAGradientLayer *headerLayer;
        UIColor *colorOne = [UIColor colorWithWhite:0 alpha:0];
        UIColor *colorTwo = [UIColor colorWithWhite:0 alpha:1];
        NSArray *colors = [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, nil];
        NSNumber *stopOne = [NSNumber numberWithFloat:0.7];
        NSNumber *stopTwo = [NSNumber numberWithFloat:1.0];
        NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
        
        //crate gradient layer
        if (!headerLayer) {
            headerLayer = [CAGradientLayer layer];
            
            headerLayer.colors = colors;
            headerLayer.locations = locations;
            
            headerLayer.frame = CGRectMake(0, 0, IMAGEWIDTH, IMAGEWIDTH);
            [_secondImageView.layer insertSublayer:headerLayer atIndex:0];
        }

    }
    

    
    
}
@end
