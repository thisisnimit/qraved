//
//  ReviewPublishAddToListView.m
//  Qraved
//
//  Created by harry on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "ReviewPublishAddToListView.h"

@implementation ReviewPublishAddToListView
{
    UIButton *btnAddToList;
    UILabel *lblListName;
}
- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    
    self.backgroundColor = [UIColor whiteColor];
    
    UIView *toplineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 0.5)];
    toplineView.backgroundColor = [UIColor colorWithHexString:@"#C1C1C1"];
    [self addSubview:toplineView];
    
    btnAddToList = [UIButton buttonWithType:UIButtonTypeCustom];
    btnAddToList.frame = CGRectMake(0, toplineView.endPointY, DeviceWidth, 45);
    [btnAddToList setTitle:@"Add to List" forState:UIControlStateNormal];
    [btnAddToList setTitleColor:[UIColor colorWithHexString:@"09BFD3"] forState:UIControlStateNormal];
    btnAddToList.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    btnAddToList.titleLabel.font = [UIFont systemFontOfSize:17];
    btnAddToList.titleEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
    [btnAddToList addTarget:self action:@selector(addtoListTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnAddToList];
    
    lblListName = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth-190, 0, 150, 45)];
    lblListName.font = [UIFont systemFontOfSize:17];
    lblListName.textColor = [UIColor color999999];
    lblListName.textAlignment = NSTextAlignmentRight;
    [self addSubview:lblListName];
    
    UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-33, 16, 8, 14)];
    arrowImageView.image = [UIImage imageNamed:@"ic_review_arrow" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    
    [btnAddToList addSubview:arrowImageView];
}

- (void)setCurrentRestaurant:(IMGRestaurant *)currentRestaurant{
    _currentRestaurant = currentRestaurant;
    
    if ([currentRestaurant.restaurantId intValue]==0||currentRestaurant.title.length==0){
        btnAddToList.enabled = NO;
    }else{
        if ([currentRestaurant.saved isEqual:@1]) {
            btnAddToList.enabled = NO;
        }else{
            btnAddToList.enabled = YES;
        }
        
        lblListName.text = currentRestaurant.stateName;
    }
}

- (void)addtoListTapped:(UIButton *)button{
    if (self.addToListClick) {
        self.addToListClick(self.currentRestaurant);
    }
}

@end
