//
//  PriceLevelButton.m
//  Qraved
//
//  Created by Jeff on 8/22/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "PriceLevelButton.h"
#import "UIColor+Helper.h"
#import "UIConstants.h"
#import "NotificationConsts.h"

@implementation PriceLevelButton
{
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.borderWidth = 1.5;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame priceLevel:(IMGPriceLevel *)tmpPriceLevel
{
    self = [super initWithFrame:frame];
    if (self) {
        self.priceLevel = tmpPriceLevel;
        self.layer.borderWidth = 1.5;
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        self.layer.cornerRadius = 5;
        UIFont *labelFont=[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:10];
        
        self.dollarsLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 8, frame.size.width, frame.size.height/2-10)];
        [self.dollarsLabel setText:tmpPriceLevel.dollors];
        [self.dollarsLabel setFont:labelFont];
        [self.dollarsLabel setTextAlignment:NSTextAlignmentCenter];
        [self.dollarsLabel setTextColor:[UIColor color999999]];
        [self addSubview:self.dollarsLabel];
        
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, frame.size.height/2+2, frame.size.width, frame.size.height/2-10)];
        [self.nameLabel setFont:labelFont];
        [self.nameLabel setTextAlignment:NSTextAlignmentCenter];
        [self.nameLabel setTextColor:[UIColor color999999]];
        [self.nameLabel setText:tmpPriceLevel.title];
        [self addSubview:self.nameLabel];
    }
    return self;
}

- (void)uncheck{
    
    self.selected = YES;
    self.layer.borderColor = [UIColor whiteColor].CGColor;
    [self.dollarsLabel setTextColor:[UIColor color999999]];
    [self.nameLabel setTextColor:[UIColor color999999]];
    
    [UIView commitAnimations];
}

- (void)check{
    
            self.selected = YES;
            self.layer.borderColor = [UIColor color3BAF24].CGColor;
            [self.dollarsLabel setTextColor:[UIColor color3BAF24]];
            [self.nameLabel setTextColor:[UIColor color222222]];
    
    [UIView commitAnimations];
}



@end
