//
//  OfferLabel.h
//  Qraved
//
//  Created by Jeff on 8/28/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"
#import "IMGDiscover.h"
#import "IMGRestaurantOffer.h"

@interface OfferLabel : UILabel
@property (nonatomic,retain) IMGRestaurant * restaurant;
- (id)initWithFrame:(CGRect)frame restaurant:(IMGRestaurant *)restaurant;
- (id)initWithFrame:(CGRect)frame restaurant:(IMGRestaurant *)restaurant off:(NSString *)off;
- (id)initWithFrame:(CGRect)frame;
-(void)setRestaurant:(IMGRestaurant *)restaurant andBookDate:(NSDate *)bookDate andBookTime:(NSString *)bookTime andPax:(NSNumber *)pax  andRestaurantOffer:(IMGRestaurantOffer *)restaurantOffer;
@end
