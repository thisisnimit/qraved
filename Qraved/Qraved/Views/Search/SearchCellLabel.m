//
//  SearchCellLabel.m
//  Qraved
//
//  Created by Jeff on 8/27/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "SearchCellLabel.h"
#import "UIConstants.h"
#import "UIColor+Helper.h"

@implementation SearchCellLabel

- (id)initWith13BoldGreenColorAndFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIFont *titleFont=[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13];
        UIColor *textColor=[UIColor color3BAF24];
        
        [self setTextAlignment:NSTextAlignmentLeft];
        [self setTextColor:textColor];
        [self setFont:titleFont];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}


- (id)initWith13BoldBlackColorAndFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIFont *titleFont=[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:13];
        UIColor *textColor=[UIColor color222222];
        
        [self setTextAlignment:NSTextAlignmentLeft];
        [self setTextColor:textColor];
        [self setFont:titleFont];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (id)initWith16BoldBlackColorAndFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIFont *titleFont=[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:16];
        UIColor *textColor=[UIColor color222222];
        
        [self setTextAlignment:NSTextAlignmentLeft];
        [self setTextColor:textColor];
        [self setFont:titleFont];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}


- (id)initWithBlackColorAndFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIFont *titleFont=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16];
        UIColor *textColor=[UIColor color222222];
        
        [self setTextAlignment:NSTextAlignmentLeft];
        [self setTextColor:textColor];
        [self setFont:titleFont];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (id)initWithRedColorAndFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIFont *titleFont=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16];
        UIColor *textColor=[UIColor colorC2060A];
        
        [self setTextAlignment:NSTextAlignmentLeft];
        [self setTextColor:textColor];
        [self setFont:titleFont];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (id)initWithGreenColorAndFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIFont *titleFont=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16];
        UIColor *textColor=[UIColor color3BAF24];
        
        [self setTextAlignment:NSTextAlignmentLeft];
        [self setTextColor:textColor];
        [self setFont:titleFont];
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

@end
