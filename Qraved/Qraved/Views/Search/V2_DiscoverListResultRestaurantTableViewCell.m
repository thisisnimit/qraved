//
//  V2_DiscoverListResultRestaurantTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverListResultRestaurantTableViewCell.h"
#import "GoFoodBadgeView.h"
@implementation V2_DiscoverListResultRestaurantTableViewCell
{
    UIImageView  *iconImageView;
    UIImageView  *picCountImageView;
    UILabel  *lblPicCount;
    UILabel  *lblTitle;
    DLStarRatingControl * starR;
    UILabel  *lblKilometer;
    UIButton  *btnLike;
    UILabel  *lblPrice;
    UILabel  *lblPlace;
    UILabel  *lblStatus;
    UIImageView  *logoImageView;
    UILabel  *lblLogoDetail;
    
    UIView  *bottomLineView;
    GoFoodBadgeView *goFoodBadgeView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}

- (void)initUI{

    iconImageView = [[UIImageView alloc] init];
    iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    iconImageView.clipsToBounds = YES;
    
    lblTitle = [[UILabel alloc] init];
    lblTitle.textColor = [UIColor color333333];
    lblTitle.font = [UIFont systemFontOfSize:14];
    
    starR = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(15, 0, 73, 18) andStars:5 andStarWidth:13 isFractional:YES spaceWidth:2];
    starR.enabled = NO;
    [starR setHighlightedStar:[UIImage imageNamed:@"starImage"]];
    
    lblKilometer = [[UILabel alloc] init];
    lblKilometer.textColor = [UIColor color999999];
    lblKilometer.font = [UIFont systemFontOfSize:12];
    
    
    btnLike = [UIButton buttonWithType:UIButtonTypeCustom];
//    _btnLike.backgroundColor = [UIColor blueColor];
    [btnLike setImage:[UIImage imageNamed:@"ic_Heart1"] forState:UIControlStateNormal];//ic_Heart_on@2x
    [btnLike addTarget:self action:@selector(likeClick:) forControlEvents:UIControlEventTouchUpInside];
    
    lblPrice = [[UILabel alloc] init];
    lblPrice.textColor = [UIColor color999999];
    lblPrice.font = [UIFont systemFontOfSize:12];
    
    lblPlace = [[UILabel alloc] init];
    lblPlace.textColor = [UIColor color999999];
    lblPlace.font = [UIFont systemFontOfSize:12];
    
    lblStatus = [[UILabel alloc] init];
    lblStatus.textColor = [UIColor colorWithRed:70/255.0f green:163/255.0f blue:131/255.0f alpha:1.0f];
    lblStatus.font = [UIFont systemFontOfSize:12];
    
    logoImageView = [[UIImageView alloc] init];
//    _logoImageView.backgroundColor = [UIColor yellowColor];
    
    lblLogoDetail = [[UILabel alloc] init];
    lblLogoDetail.textColor = [UIColor color999999];
    lblLogoDetail.font = [UIFont systemFontOfSize:12];
    
    picCountImageView = [[UIImageView alloc] init];
    picCountImageView.image = [UIImage imageNamed:@"ic_image"];
    
    lblPicCount = [[UILabel alloc] init];
    lblPicCount.textColor = [UIColor whiteColor];
    lblPicCount.font = [UIFont systemFontOfSize:10];
    
    bottomLineView = [UIView new];
    bottomLineView.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:248/255.0 alpha:1];
    
    goFoodBadgeView = [[GoFoodBadgeView alloc] init];
    goFoodBadgeView.hidden = YES;
    
    [iconImageView sd_addSubviews:@[picCountImageView, lblPicCount, goFoodBadgeView]];
    [self.contentView sd_addSubviews:@[iconImageView, lblTitle, starR, lblKilometer, btnLike, lblPrice,lblPlace, lblStatus , logoImageView, lblLogoDetail, bottomLineView]];
    
    iconImageView.sd_layout
    .topSpaceToView(self.contentView, 15)
    .leftSpaceToView(self.contentView, 15)
    .heightIs(90)
    .widthIs(90);
    
    lblPicCount.sd_layout
    .rightSpaceToView(iconImageView, 10)
    .bottomSpaceToView(iconImageView, 10)
    .heightIs(10);
    [lblPicCount setSingleLineAutoResizeWithMaxWidth:90];
    
    picCountImageView.sd_layout
    .rightSpaceToView(lblPicCount,5)
    .bottomSpaceToView(iconImageView, 10)
    .heightIs(10)
    .widthIs(10);
    
    goFoodBadgeView.sd_layout
    .leftSpaceToView(iconImageView, 0)
    .rightSpaceToView(iconImageView, 0)
    .bottomSpaceToView(iconImageView, 0)
    .heightIs(18);
    
    lblTitle.sd_layout
    .topSpaceToView(self.contentView, 12)
    .leftSpaceToView(iconImageView, 10)
    .heightIs(16);
    [lblTitle setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 160];
    
    starR.sd_layout
    .topSpaceToView(lblTitle, 5)
    .leftEqualToView(lblTitle)
    .heightIs(14)
    .widthIs(73);
    
    lblKilometer.sd_layout
    .topSpaceToView(lblTitle, 4)
    .leftSpaceToView(starR, 5)
    .heightIs(14);
    [lblKilometer setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 160 - 108];
    
    btnLike.sd_layout
    .topSpaceToView(self.contentView, 15)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(20)
    .widthIs(20);
    
    lblPrice.sd_layout
    .topSpaceToView(starR, 3)
    .leftEqualToView(lblTitle)
    .heightIs(14);
    [lblPrice setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 130];
    
    lblPlace.sd_layout
    .topSpaceToView(lblPrice, 3)
    .leftEqualToView(lblTitle)
    .heightIs(14);
    [lblPlace setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 130];
    
    lblStatus.sd_layout
    .topSpaceToView(lblPlace, 3)
    .leftEqualToView(lblTitle)
    .heightIs(14);
    [lblStatus setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 130];
    
    logoImageView.sd_layout
    .topSpaceToView(iconImageView, 13)
    .leftSpaceToView(self.contentView, 15)
    .heightIs(10)
    .widthIs(10);
    
    lblLogoDetail.sd_layout
    .centerYEqualToView(logoImageView)
    .leftSpaceToView(logoImageView, 10)
    .heightIs(18);
    [lblLogoDetail setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 50];
    
    bottomLineView.sd_layout
    .bottomSpaceToView(self.contentView, 1)
    .leftSpaceToView(self.contentView, 0)
    .widthIs(DeviceWidth)
    .heightIs(1);
}

- (void)setModel:(IMGRestaurant *)model{

    _model = model;
    
    [iconImageView sd_setImageWithURL:[NSURL URLWithString:[model.imageUrl returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    lblTitle.text = model.title ? model.title : @"";
    [starR updateRating:[NSNumber numberWithFloat:[model.ratingScore floatValue]]];
    
    NSString *distance = [model distance];
    NSString *ratingStr = @"";
    if (model.ratingCount != nil && ![model.ratingCount isEqual:@0]) {
        ratingStr = [NSString stringWithFormat:@"(%@) •",model.ratingCount];
    }
    
    if (![distance isEqualToString:@""]) {
        lblKilometer.text = [NSString stringWithFormat:@"%@ %@",ratingStr, distance];
    }else{
        lblKilometer.text = ratingStr;
    }
    
    if ([Tools isBlankString:model.goFoodLink]) {
        goFoodBadgeView.hidden = YES;
        picCountImageView.hidden = NO;
        lblPicCount.hidden = NO;
    }else{
        goFoodBadgeView.hidden = NO;
        picCountImageView.hidden = YES;
        lblPicCount.hidden = YES;
    }
    
    NSString *dollar;
    if (model.priceName ==nil) {
        switch(model.priceLevel.intValue){
            case 1:dollar=@"Below 100K";break;
            case 2:dollar=@"100K - 200K";break;
            case 3:dollar=@"200K - 300K";break;
            case 4:dollar=@"Start from 300K";break;
        }
    }else{
        dollar = model.priceName;
    }
    
    if (model.landMarkList.count>0) {
        lblPrice.text = [NSString stringWithFormat:@"%@ • %@ • %@ • %@",model.cuisineName, model.districtName,[[model.landMarkList objectAtIndex:0] objectForKey:@"name"], dollar];
    }else{
        lblPrice.text = [NSString stringWithFormat:@"%@ • %@ • %@",model.cuisineName, model.districtName, dollar];
    }
    
    
    lblPlace.text = model.wellKnownFor ? model.wellKnownFor : @"";
    
    
    NSString *status;
    NSString *endStatus;
    NSInteger weekNum=[self returnTodayIsWeekDay];
    NSString* weekDay=[self returnWeekDayWithNumber:weekNum];
    
    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"HH:mm"];
    NSString *  locationString=[dateformatter stringFromDate:senddate];
    NSString* hours=[locationString substringToIndex:2];
    NSRange rang={3,2};
    NSString* min=[locationString substringWithRange:rang];
    int curenttime=[hours intValue]*3600+[min intValue]*60;
    
    if ([model.state intValue]==6||[model.state intValue]==1) {
        
        NSString *closedTime = model.nextclosedTime;
        if ([weekDay isEqualToString:model.nextopenDay]) {
            if ([model.nextopenTime isEqualToString:@"00:00"]&&[model.nextclosedTime isEqualToString:@"00:00"]) {
                closedTime = @"24:00";
            }
            NSString *openHour = [model.nextopenTime substringToIndex:2];
            NSString *openMin = [model.nextopenTime substringWithRange:rang];
            NSString *closeHour = [closedTime substringToIndex:2];
            NSString *closeMin = [closedTime substringWithRange:rang];
            
            int openTime = [openHour intValue]*3600+[openMin intValue]*60;
            int closeTime = [closeHour intValue]*3600+[closeMin intValue]*60;
            if (openTime < closeTime) {
                if (openTime <= curenttime && curenttime <= closeTime) {
                    status = @"open";
                    endStatus = @"now";
                }else if (curenttime < openTime){
                    status = @"closed";
                    endStatus = [NSString stringWithFormat:@"• Open at %@",model.nextopenTime];
                }else if (curenttime > closeTime){
                    status = @"closed";
                    endStatus = [NSString stringWithFormat:@"• Open on %@",[model.secondopenDay uppercaseString]];
                }
            }else if (openTime >closeTime){
                if (openTime<=curenttime) {
                    status = @"open";
                    endStatus = @"now";
                }else{
                    status = @"closed";
                    endStatus = [NSString stringWithFormat:@"• Open at %@",model.nextopenTime];
                }
            }else{
                status = @"open";
                endStatus = @"now";
            }
            
        }else{
            status = @"closed";
            endStatus = [NSString stringWithFormat:@"• Open on %@",[model.nextopenDay uppercaseString]];
        }
 
        NSString* lastWeekDay=[self returnWeekDayWithNumber:weekNum-1];
        
        long lastCloseTime;
        if ([model.yesteropenDay isEqualToString:@""] && [model.yesteropenDay isEqualToString:lastWeekDay]) {
            lastCloseTime = [model.yesterclosedTime longLongValue];
            
            if (curenttime<=lastCloseTime) {
                status = @"open";
                endStatus = @"now";
            }
        }
 
        if ([model.nextopenTime isEqualToString:@""] || [model.nextclosedTime isEqualToString:@""]) {
            status = @"Closed";
            endStatus = @"";
        }
        
    }else{
        status = @"Closed";
        endStatus = @"";
    }
    
    if ([status isEqualToString:@"open"]) {
        lblStatus.textColor = [UIColor colorWithRed:70/255.0f green:163/255.0f blue:131/255.0f alpha:1.0f];
    }else{
        lblStatus.textColor = [UIColor color999999];
    }

    lblStatus.text = [NSString stringWithFormat:@"%@ %@",[status capitalizedString],[endStatus stringByReplacingOccurrencesOfString:@"00:00" withString:@"24:00"]];
    
    
    lblPicCount.text = model.photoCount ? [NSString stringWithFormat:@"%@",model.photoCount] : @"";
    
    if ([model.saved isEqual:@1]) {
        [btnLike setImage:[UIImage imageNamed:@"ic_heart_full"] forState:UIControlStateNormal];
    }else{
        [btnLike setImage:[UIImage imageNamed:@"ic_Heart1"] forState:UIControlStateNormal];
    }
    
    
    NSString *statusStr;
    if ([model.trendingInstagram isEqual:@1]) {
        logoImageView.hidden = NO;
        logoImageView.image = [UIImage imageNamed:@"ic_trendingInstagram"];
        statusStr = @"Trending on instagram";
    }else if ([model.inJournal  isEqual:@1]){
        logoImageView.hidden = NO;
        logoImageView.image = [UIImage imageNamed:@"ic_inJournal"];
        statusStr = [model.journaArticlelTitle stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    }else{
        logoImageView.hidden = YES;
        statusStr = @"";
    }
    lblLogoDetail.text = statusStr;
    [lblLogoDetail updateLayout];
    if (statusStr.length==0) {
        [self setupAutoHeightWithBottomView:iconImageView bottomMargin:13];
    }else{
        [self setupAutoHeightWithBottomView:lblLogoDetail bottomMargin:13];
    }
}

- (void)likeClick:(UIButton *)like{

    if(self.Delegate){
        [self.Delegate delegateLikeBtn:self andRestaurant:self.model];
    }
}

- (void)refreshSavedButton:(BOOL)isSaved{
    if (isSaved) {
        [btnLike setImage:[UIImage imageNamed:@"ic_heart_full"] forState:UIControlStateNormal];
        self.model.saved = @1;
    }else{
        [btnLike setImage:[UIImage imageNamed:@"ic_Heart1"] forState:UIControlStateNormal];
        self.model.saved = @0;
    }
}
-(NSString*)returnWeekDayWithNumber:(NSInteger)number
{
    
    switch (number) {
        case 1:
            return [NSString stringWithFormat:@"Sun"];
            break;
        case 2:
            return [NSString stringWithFormat:@"Mon"];
            break;
        case 3:
            return [NSString stringWithFormat:@"Tue"];
            break;
        case 4:
            return [NSString stringWithFormat:@"Wed"];
            break;
        case 5:
            return [NSString stringWithFormat:@"Thu"];
            break;
        case 6:
            return [NSString stringWithFormat:@"Fri"];
            break;
        case 7:
            return [NSString stringWithFormat:@"Sat"];
            break;
        default:
            break;
    }
    
    
    return nil;
    
    
}

-(NSInteger)returnTodayIsWeekDay{
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *now = [NSDate date];;
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    comps = [calendar components:unitFlags fromDate:now];
    NSInteger weekDay=[comps weekday];
    NSLog(@"today is %ld day ",(long)weekDay);
    return weekDay;
    
}

@end
