//
//  V2_DiscoverListResultFiltersCuisinesTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverListResultFiltersCuisinesTableViewCell.h"
#import "IMGCuisine.h"
@implementation V2_DiscoverListResultFiltersCuisinesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initUI:(NSMutableArray *)tagssArr;{
    self.cuisineIdArray = [NSMutableArray array];
    self.indexArray = [NSMutableArray array];
    self.tagTextArray = [NSMutableArray array];
    self.tagsArr = [NSMutableArray array];
    for (IMGCuisine *searchCuisine in tagssArr) {
        [self.tagsArr addObject:searchCuisine.name];
        [self.cuisineIdArray addObject:searchCuisine.cuisineId];
    }
    
    TTGTextTagCollectionView *TagCollectionView = [[TTGTextTagCollectionView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 115)];
    TagCollectionView.delegate = self;
    TagCollectionView.defaultConfig.tagTextFont = [UIFont systemFontOfSize:12];
    TagCollectionView.scrollDirection = TTGTagCollectionScrollDirectionHorizontal;
    TagCollectionView.alignment = TTGTagCollectionAlignmentLeft;
    TagCollectionView.numberOfLines = 3;
    TagCollectionView.defaultConfig.tagBackgroundColor = [UIColor color234Gray];
    TagCollectionView.defaultConfig.tagSelectedBackgroundColor = [UIColor color222Red];
    TagCollectionView.defaultConfig.tagTextColor = [UIColor color333333];
    TagCollectionView.defaultConfig.tagBorderWidth = 0;
    TagCollectionView.defaultConfig.tagBorderColor = [UIColor clearColor];
    TagCollectionView.contentInset = UIEdgeInsetsMake(5, 15, 5, 5);
    TagCollectionView.defaultConfig.tagShadowOffset = CGSizeMake(0, 0);
    TagCollectionView.defaultConfig.tagShadowRadius = 0;
    TagCollectionView.defaultConfig.tagShadowOpacity = 0;
    [TagCollectionView addTags:self.tagsArr];
    [self.contentView addSubview:TagCollectionView];


}

- (void)textTagCollectionView:(TTGTextTagCollectionView *)textTagCollectionView didTapTag:(NSString *)tagText atIndex:(NSUInteger)index selected:(BOOL)selected{

//    NSLog(@"%@===%ld===%d", tagText, index, selected ? YES : NO);
   
    
    if (selected == 0) {
        [self.indexArray removeObject:[NSString stringWithFormat:@"%@",self.cuisineIdArray[index]]];
        [self.tagTextArray removeObject:tagText];
    }else{
     [self.indexArray addObject:[NSString stringWithFormat:@"%@",self.cuisineIdArray[index]]];
        [self.tagTextArray addObject:tagText];
    }
//     NSLog(@"%@",self.tagTextArray);
    
    NSString *string = [self.indexArray componentsJoinedByString:@","];
//    NSLog(@"%@",string);
    if (self.Delegate) {
        [self.Delegate delegateCuisines:string CuisinesArray:self.tagTextArray indexArray:self.indexArray];
    }
}









@end
