//
//  V2_DiscoverListResultFiltersSortsByTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol V2_DiscoverListResultFiltersSortsByTableViewCellDelegate <NSObject>

- (void)delegateSortBy:(NSInteger)buttonTag;

@end


@interface V2_DiscoverListResultFiltersSortsByTableViewCell : UITableViewCell

@property (nonatomic, strong)  UIView  *backGroundView;
@property (nonatomic, strong)  UIButton  *btnPopularity;
@property (nonatomic, strong)  UIButton  *btnRating;
@property (nonatomic, strong)  UIButton  *btnPrice;
@property (nonatomic, strong)  UIButton  *btnDistance;


@property (nonatomic, weak)  id<V2_DiscoverListResultFiltersSortsByTableViewCellDelegate >Delegate;
@end
