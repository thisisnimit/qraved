//
//  CallOutAnnotationVifew.m
//  IYLM
//
//  Created by Jian-Ye on 12-11-8.
//  Copyright (c) 2012年 Jian-Ye. All rights reserved.
//

#import "CallOutAnnotationView.h"
#import <QuartzCore/QuartzCore.h>
#import "UIConstants.h"
#import "DLStarRatingControl.h"
#import "UILabel+Helper.h"
#import "UIColor+Helper.h"
#import "CalloutMapAnnotation.h"
#import "OfferLabel.h"
#import "DetailViewController.h"
#import "NotificationConsts.h"
#import "NSString+Helper.h"

#define  Arror_height 0

#define LEFT_X 10
#define CELL_HEIGHT 25
#define RATING_STAR_WIDTH 15

@interface CallOutAnnotationView ()

-(void)drawInContext:(CGContextRef)context;
- (void)getDrawPath:(CGContextRef)context;
@end

@implementation CallOutAnnotationView
{
    BOOL _postNotification;
    NSString *cuisineAndDistrictText;
}
@synthesize contentView;


- (id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier andPostNotification:(BOOL)postNotification isFromSearchPage:(BOOL)fromSearchPage
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        _postNotification=postNotification;
        self.backgroundColor = [UIColor clearColor];
        self.canShowCallout = NO;
        self.centerOffset = CGPointMake(0, -52);
        self.frame = CGRectMake(0, 0, DeviceWidth-30, 50);
        
        UIView *_contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - Arror_height)];
        _contentView.backgroundColor   = [UIColor clearColor];
        
        CalloutMapAnnotation *customMapAnnotation = (CalloutMapAnnotation *)annotation;
        IMGRestaurant *restaurant = customMapAnnotation.restaurant;
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_X, 1, self.frame.size.width-100, CELL_HEIGHT)];
        UIFont *titleLabelFont=[UIFont boldSystemFontOfSize:14];
        [titleLabel setText:restaurant.title];
        [titleLabel setTextAlignment:NSTextAlignmentLeft];
        [titleLabel setFont:titleLabelFont];
        [titleLabel setTextColor:[UIColor color222222]];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        [_contentView addSubview:titleLabel];
        
        DLStarRatingControl *ratingScoreControl=[[DLStarRatingControl alloc]initWithFrame:CGRectMake(self.frame.size.width-2-RATING_STAR_WIDTH*5-20, 5, RATING_STAR_WIDTH*5+20, RATING_STAR_WIDTH) andStars:5 andStarWidth:RATING_STAR_WIDTH isFractional:YES spaceWidth:5];
        [ratingScoreControl updateRating:[NSNumber numberWithFloat:[restaurant.ratingScore floatValue]]];
        ratingScoreControl.userInteractionEnabled=NO;
        [_contentView addSubview:ratingScoreControl];

        NSString *dollar=nil;
        if(restaurant.priceName.length>0){
            dollar=restaurant.priceName;
        }else{
            switch(restaurant.priceLevel.intValue){
                case 1:dollar=@"Below 100K";break;
                case 2:dollar=@"100K - 200K";break;
                case 3:dollar=@"200K - 300K";break;
                case 4:dollar=@"Start from 300K";break;
            }
        }
        
        UIFont *font=[UIFont systemFontOfSize:11];
        
        if (fromSearchPage) {
            if (restaurant.priceLevel != nil) {
                cuisineAndDistrictText=[NSString stringWithFormat:@"%@ / %@ / %@",restaurant.cuisineName,restaurant.districtName,restaurant.priceLevel];
            }else{
                cuisineAndDistrictText=[NSString stringWithFormat:@"%@ / %@",restaurant.cuisineName,restaurant.districtName];
            }
            
        }else{
        
            if (dollar != nil) {
                cuisineAndDistrictText=[NSString stringWithFormat:@"%@ / %@ / %@",restaurant.cuisineName,restaurant.districtName,dollar];
            }else{
                cuisineAndDistrictText=[NSString stringWithFormat:@"%@ / %@",restaurant.cuisineName,restaurant.districtName];
            }
           
        
        }
        
       
        CGSize size=[cuisineAndDistrictText sizeWithFont:font constrainedToSize:CGSizeMake(self.frame.size.width-LEFT_X*2,1000) lineBreakMode:NSLineBreakByTruncatingTail];
        
//        CGFloat margin=size.height-font.lineHeight;

        UIView *bottomBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, CELL_HEIGHT, self.frame.size.width, size.height+12)];
        [bottomBackgroundView setBackgroundColor:[UIColor colorEDEDED]];
        [_contentView addSubview:bottomBackgroundView];
        
        UILabel *cuisineDistrictLabel=[[UILabel alloc] initWithFrame:CGRectMake(LEFT_X, CELL_HEIGHT+6, self.frame.size.width-LEFT_X*2,size.height)];
        cuisineDistrictLabel.text=cuisineAndDistrictText;
        cuisineDistrictLabel.numberOfLines=2;
        cuisineDistrictLabel.font=font;
        cuisineDistrictLabel.lineBreakMode=NSLineBreakByTruncatingTail;
        [_contentView addSubview:cuisineDistrictLabel];

//        if (!restaurant.cuisineName.length || !restaurant.districtName)
//        {
//            cuisineDistrictLabel.hidden = YES;
//        }

        // self.frame=CGRectMake(0, 0, 270, 50+margin);
        // _contentView.frame=CGRectMake(0, 0, self.frame.size.width, self.frame.size.height-Arror_height);
//        if (!restaurant.cuisineName.length || !restaurant.districtName||!restaurant.priceLevel)
//        {
//            cuisineDistrictLabel.hidden = YES;
//        }

        //if(restaurant.disCount){
//            OfferLabel *offerLabel = [[OfferLabel alloc] initWithFrame:CGRectMake(0,CELL_HEIGHT+2,200,CELL_HEIGHT-3) restaurant:restaurant];
//            [offerLabel setFrame:CGRectMake(self.frame.size.width-3-offerLabel.frame.size.width, offerLabel.frame.origin.y, offerLabel.frame.size.width+2, offerLabel.frame.size.height)];
//            [_contentView addSubview:offerLabel];
       // }
        [self addSubview:_contentView];
        self.contentView = _contentView;
        self.tag=[restaurant.restaurantId intValue];

            UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(gotoDetail:)];

            [self addGestureRecognizer:tapGesture];

        

    }
    return self;
}

-(void)gotoDetail:(UITapGestureRecognizer*)gesture{
    if(_postNotification){
        [[NSNotificationCenter defaultCenter] postNotificationName:GO_DETAIL_FROM_SEARCH object:nil userInfo:@{@"restaurantId":[NSNumber numberWithLong:gesture.view.tag]}];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:GO_DETAIL object:nil userInfo:@{@"restaurantId":[NSNumber numberWithLong:gesture.view.tag]}];
    }
}

-(void)drawInContext:(CGContextRef)context
{
	
   CGContextSetLineWidth(context, 1.0);
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
   
    [self getDrawPath:context];
    CGContextFillPath(context);
    

    
}
- (void)getDrawPath:(CGContextRef)context
{
    CGRect rrect = self.bounds;
	CGFloat radius = 2.0;
    
	CGFloat minx = CGRectGetMinX(rrect),
    midx = CGRectGetMidX(rrect), 
    maxx = CGRectGetMaxX(rrect);
	CGFloat miny = CGRectGetMinY(rrect), 
    // midy = CGRectGetMidY(rrect), 
    maxy = CGRectGetMaxY(rrect)-Arror_height;
    CGContextMoveToPoint(context, midx+Arror_height, maxy);
    CGContextAddLineToPoint(context,midx, maxy+Arror_height);
    CGContextAddLineToPoint(context,midx-Arror_height, maxy);
    
    CGContextAddArcToPoint(context, minx, maxy, minx, miny, radius);
    CGContextAddArcToPoint(context, minx, minx, maxx, miny, radius);
    CGContextAddArcToPoint(context, maxx, miny, maxx, maxx, radius);
    CGContextAddArcToPoint(context, maxx, maxy, midx, maxy, radius);
    CGContextClosePath(context);
}

- (void)drawRect:(CGRect)rect
{
	[self drawInContext:UIGraphicsGetCurrentContext()];
    
    self.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.layer.shadowOpacity = 0.2;

    self.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
}
@end
