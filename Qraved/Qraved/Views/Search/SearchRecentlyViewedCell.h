//
//  SearchRecentlyViewedCell.h
//  Qraved
//
//  Created by harry on 2017/11/28.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchRecentlyViewedCell : UITableViewCell

@property (nonatomic, copy) NSArray *restaurantArray;
@property (nonatomic, copy) void(^gotoRestaurantDetail)(IMGRestaurant *restaurant);
@property (nonatomic, copy) void(^savedTapped)(IMGRestaurant *restaurant);

- (void)freshSavedButton:(BOOL)liked;

@end
