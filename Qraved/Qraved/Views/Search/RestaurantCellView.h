//
//  RestaurantCellView.h
//  Qraved
//
//  Created by Jeff on 8/14/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"

#define LEFT_X 18

#define TITLE_LABEL_Y 13
#define TITLE_LABEL_HEIGHT 20

#define GAP_BETWEEN_TITLE_AND_CUISINE 1

#define CUISINE_DISTRICT_LABEL_HEIGHT 20

#define GAP_BETWEEN_CUISINE_AND_REVIEW 3

#define CELLVIEW_RATING_STAR_WIDTH 10

#define RESTAURANT_IMAGE_SIZE 70

@interface RestaurantCellView : UIView

@property (nonatomic,retain,readonly) UIImageView *photoImageView;


- (id)initWithFrame:(CGRect)frame andRestaurant:(IMGRestaurant *)restaurant;
-(void)setRestaurant:(IMGRestaurant *)restaurant;

@end
