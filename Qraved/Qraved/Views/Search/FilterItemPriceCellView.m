//
//  FilterItemPriceCellView.m
//  Qraved
//
//  Created by Jeff on 8/21/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "FilterItemPriceCellView.h"
#import "PriceLevelButton.h"
#import "NotificationConsts.h"
#import "UIColor+Helper.h"

@implementation FilterItemPriceCellView
{
    IMGDiscover *_discover;
}

- (id)initWithFrame:(CGRect)frame objectArray:(NSArray *)priceLevelArray andDiscover:(IMGDiscover *)discover
{
    self = [super initWithFrame:frame];
    if (self) {
        _discover = discover;
        NSMutableArray *buttonsArray = [[NSMutableArray alloc]init];
        if(priceLevelArray!=nil){
            float width = frame.size.width/[priceLevelArray count];
            if (priceLevelArray.count>1) {
                for(int ii=0;ii<[priceLevelArray count];ii++){
                    IMGPriceLevel *priceLevel = (IMGPriceLevel *)priceLevelArray[ii];
                    PriceLevelButton *priceLevelButton = [[PriceLevelButton alloc] initWithFrame:CGRectMake(ii*width+1, 6, width-3, frame.size.height-12) priceLevel:priceLevel];
                    [priceLevelButton addTarget:self action:@selector(check:) forControlEvents:UIControlEventTouchUpInside];
                    [self addSubview:priceLevelButton];
                    [buttonsArray addObject:priceLevelButton];
                }
            }
            NSArray *array = discover.priceLevelArray;
            for (int i=0; i<buttonsArray.count; i++) {
                PriceLevelButton *priceButton = [buttonsArray objectAtIndex:i];
                int j;
                for (j=0; j<array.count; j++) {
                    IMGPriceLevel *priceLevel = [array objectAtIndex:j];
                    if (i+1==[priceLevel.priceLevelId intValue]) {
                        [priceButton check];
                        break;
                    }
                }
                if (j==array.count) {
                    [priceButton uncheck];
                }
            }
        }
    }
    
    return self;
}

- (void)check:(UIButton*)sender
{
//    NSMutableArray *priceLevelArray = [[NSMutableArray alloc]initWithArray:_discover.priceLevelArray];
//    NSArray *array = _discover.priceLevelArray;
//    PriceLevelButton *priceLevelButton = (PriceLevelButton *)sender;
//    int j;
//    
//    for (j=0; j<array.count; j++) {
//        IMGPriceLevel *priceLevel = [array objectAtIndex:j];
//        if ([priceLevelButton.priceLevel.priceLevelId integerValue] ==  [priceLevel.priceLevelId integerValue]) {
//            [priceLevelButton uncheck];
//            [priceLevelArray removeObject:priceLevel];
//            break;
//        }
//    }
//    if (j==array.count) {
//        [priceLevelButton check];
//        [priceLevelArray addObject:priceLevelButton.priceLevel];
//    }
//
//
    
    IMGPriceLevel *priceLevel1 = [[IMGPriceLevel alloc] init];
    priceLevel1.priceLevelId = [NSNumber numberWithInt:1];
    priceLevel1.title=@"100k-199k";
    priceLevel1.dollors=@"$";
    IMGPriceLevel *priceLevel2 = [[IMGPriceLevel alloc] init];
    priceLevel2.priceLevelId = [NSNumber numberWithInt:2];
    priceLevel2.title=@"200k-299k";
    priceLevel2.dollors=@"$$";
    IMGPriceLevel *priceLevel3 = [[IMGPriceLevel alloc] init];
    priceLevel3.priceLevelId = [NSNumber numberWithInt:3];
    priceLevel3.title=@"300k-399k";
    priceLevel3.dollors=@"$$$";
    IMGPriceLevel *priceLevel4 = [[IMGPriceLevel alloc] init];
    priceLevel4.priceLevelId = [NSNumber numberWithInt:4];
    priceLevel4.title=@">400k";
    priceLevel4.dollors=@"$$$$";
    
    NSMutableArray *priceLevelArray = [[NSMutableArray alloc]init];
    NSArray *array = _discover.priceLevelArray;
    
    BOOL firstSelect = false;
    BOOL secondSelect = false;
    BOOL thirthSelect = false;
    BOOL fourthSelect = false;
    for (int i=0; i<array.count; i++) {
        IMGPriceLevel *priceLevel = [array objectAtIndex:i];
        if ([priceLevel.priceLevelId integerValue] == 1) {
            firstSelect = true;
        }
        if ([priceLevel.priceLevelId integerValue] == 2) {
            secondSelect = true;
        }
        if ([priceLevel.priceLevelId integerValue] == 3) {
            thirthSelect = true;
        }
        if ([priceLevel.priceLevelId integerValue] == 4) {
            fourthSelect = true;
        }
    }
    PriceLevelButton *priceLevelButton = (PriceLevelButton *)sender;
    switch ([priceLevelButton.priceLevel.priceLevelId intValue]) {
        case 1:{
            if (firstSelect) {//第一个选中
                [priceLevelArray addObjectsFromArray:array];
                for (IMGPriceLevel *priceLevel in priceLevelArray) {
                    if ([priceLevel.priceLevelId integerValue] == 1) {
                        [priceLevelArray removeObject:priceLevel];
                        break;
                    }
                }
            }else if (secondSelect){//第二个选中
                [priceLevelArray addObject:priceLevel1];
                [priceLevelArray addObject:priceLevel2];
                if (thirthSelect) {
                    [priceLevelArray addObject:priceLevel3];
                    if (fourthSelect) {
                        [priceLevelArray addObject:priceLevel4];
                    }
                }
                
            }else if (thirthSelect){//第三个选中
                [priceLevelArray addObject:priceLevel1];
                [priceLevelArray addObject:priceLevel2];
                [priceLevelArray addObject:priceLevel3];
                if (fourthSelect) {
                    [priceLevelArray addObject:priceLevel4];
                }
            }else if (fourthSelect){//第四个选中
                [priceLevelArray addObject:priceLevel1];
                [priceLevelArray addObject:priceLevel2];
                [priceLevelArray addObject:priceLevel3];
                [priceLevelArray addObject:priceLevel4];
            }else{
                [priceLevelArray addObject:priceLevel1];
            }
        }
            break;
        case 2:{
            if (secondSelect) {//第二个选中
                [priceLevelArray addObjectsFromArray:array];
                if (thirthSelect) {
                    for (IMGPriceLevel *priceLevel in priceLevelArray) {
                        if ([priceLevel.priceLevelId integerValue] == 1) {
                            [priceLevelArray removeObject:priceLevel];
                            break;
                        }
                    }
                }
                if (!firstSelect || !thirthSelect) {
                    for (IMGPriceLevel *priceLevel in priceLevelArray) {
                        if ([priceLevel.priceLevelId integerValue] == 2) {
                            [priceLevelArray removeObject:priceLevel];
                            break;
                        }
                    }
                }
            }else if (firstSelect){//第一个选中
                [priceLevelArray addObject:priceLevel1];
                [priceLevelArray addObject:priceLevel2];
            }else if (thirthSelect){//第三个选中
                [priceLevelArray addObject:priceLevel2];
                [priceLevelArray addObject:priceLevel3];
                if (fourthSelect){//第四个选中
                    [priceLevelArray addObject:priceLevel4];
                }
            }else if (fourthSelect){//第四个选中
                [priceLevelArray addObject:priceLevel2];
                [priceLevelArray addObject:priceLevel3];
                [priceLevelArray addObject:priceLevel4];

            }else{
                [priceLevelArray addObject:priceLevel2];
            }
        }
            
            break;
        case 3:{
            if (thirthSelect) {//第三个选中
                [priceLevelArray addObjectsFromArray:array];
                if (!fourthSelect || !secondSelect) {
                    for (IMGPriceLevel *priceLevel in priceLevelArray) {
                        if ([priceLevel.priceLevelId integerValue] == 3) {
                            [priceLevelArray removeObject:priceLevel];
                            break;
                        }
                    }
                }else{
                    if (firstSelect) {
                        for (IMGPriceLevel *priceLevel in priceLevelArray) {
                            if ([priceLevel.priceLevelId integerValue] == 1) {
                                [priceLevelArray removeObject:priceLevel];
                                break;
                            }
                        }
                        for (IMGPriceLevel *priceLevel in priceLevelArray) {
                            if ([priceLevel.priceLevelId integerValue] == 2) {
                                [priceLevelArray removeObject:priceLevel];
                                break;
                            }
                        }
                    }else if (secondSelect){
                        for (IMGPriceLevel *priceLevel in priceLevelArray) {
                            if ([priceLevel.priceLevelId integerValue] == 2) {
                                [priceLevelArray removeObject:priceLevel];
                                break;
                            }
                        }
                    }
                }
                
            }else if (firstSelect){//第一个选中
                [priceLevelArray addObject:priceLevel1];
                [priceLevelArray addObject:priceLevel2];
                [priceLevelArray addObject:priceLevel3];
            }else if (secondSelect){//第二个选中
                [priceLevelArray addObject:priceLevel2];
                [priceLevelArray addObject:priceLevel3];
            }else if (fourthSelect){//第四个选中
                [priceLevelArray addObject:priceLevel3];
                [priceLevelArray addObject:priceLevel4];
            }else{
                [priceLevelArray addObject:priceLevel3];
            }
        }
            
            break;
        case 4:{
            if (fourthSelect){//第四个选中
                [priceLevelArray addObjectsFromArray:array];
                for (IMGPriceLevel *priceLevel in priceLevelArray) {
                    if ([priceLevel.priceLevelId integerValue] == 4) {
                        [priceLevelArray removeObject:priceLevel];
                        break;
                    }
                }
            }else if (firstSelect) {//第一个选中
                [priceLevelArray addObject:priceLevel1];
                [priceLevelArray addObject:priceLevel2];
                [priceLevelArray addObject:priceLevel3];
                [priceLevelArray addObject:priceLevel4];
            }else if (secondSelect){//第二个选中
                [priceLevelArray addObject:priceLevel2];
                [priceLevelArray addObject:priceLevel3];
                [priceLevelArray addObject:priceLevel4];
            }else if (thirthSelect){//第三个选中
                [priceLevelArray addObject:priceLevel3];
                [priceLevelArray addObject:priceLevel4];
            }else{
                [priceLevelArray addObject:priceLevel4];
            }
        }
            
            break;
        default:
            break;
    }
    
    
    _discover.priceLevelArray = priceLevelArray;
    [UIView commitAnimations];
    [[NSNotificationCenter defaultCenter] postNotificationName:SEARCH_FILTER object:nil userInfo:@{@"discover":_discover}];

    
}

@end
