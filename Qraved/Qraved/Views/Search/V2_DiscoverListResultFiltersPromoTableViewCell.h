//
//  V2_DiscoverListResultFiltersPromoTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/28.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface V2_DiscoverListResultFiltersPromoTableViewCell : UITableViewCell

@property (nonatomic, strong)  UILabel  *lblTitle;

@property (nonatomic, strong)  UILabel  *lblDetail;

@property (nonatomic, strong)  UIImageView  *arrowImageView;

@end
