//
//  V2_DiscoverListResultFiltersSortsByTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverListResultFiltersSortsByTableViewCell.h"

@implementation V2_DiscoverListResultFiltersSortsByTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}

- (void)initUI{

    _backGroundView = [[UIView alloc] initWithFrame:CGRectMake(15, 5, DeviceWidth - 30, 30)];
    _backGroundView.backgroundColor = [UIColor whiteColor];
    _backGroundView.layer.cornerRadius = 5;
    _backGroundView.layer.masksToBounds = YES;
    _backGroundView.layer.borderColor = [UIColor colorWithRed:222/255.0f green:32/255.0f blue:41/255.0f alpha:1.0f].CGColor;
    _backGroundView.layer.borderWidth = 0.5;
    [self.contentView addSubview:_backGroundView];
    
    _btnPopularity = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnPopularity.frame = CGRectMake(0, 0, (DeviceWidth - 30)/4, 30);
    [_btnPopularity setTitle:@"Popularity" forState:UIControlStateNormal];
    [_btnPopularity setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    _btnPopularity.titleLabel.font = [UIFont systemFontOfSize:12];
    _btnPopularity.tag = 1;
    [_btnPopularity addTarget:self action:@selector(btnCkick:) forControlEvents:UIControlEventTouchUpInside];
    [_backGroundView addSubview:_btnPopularity];
    
    _btnRating = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnRating.frame = CGRectMake(_btnPopularity.right, 0, (DeviceWidth - 30)/4, 30);
    [_btnRating setTitle:@"Rating" forState:UIControlStateNormal];
    [_btnRating setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    _btnRating.titleLabel.font = [UIFont systemFontOfSize:12];
    _btnRating.tag = 2;
    [_btnRating addTarget:self action:@selector(btnCkick:) forControlEvents:UIControlEventTouchUpInside];
    [_backGroundView addSubview:_btnRating];
    
    _btnPrice = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnPrice.frame = CGRectMake(_btnRating.right, 0, (DeviceWidth - 30)/4, 30);
    [_btnPrice setTitle:@"Price" forState:UIControlStateNormal];
    [_btnPrice setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    _btnPrice.titleLabel.font = [UIFont systemFontOfSize:12];
    _btnPrice.tag = 3;
    [_btnPrice addTarget:self action:@selector(btnCkick:) forControlEvents:UIControlEventTouchUpInside];
    [_backGroundView addSubview:_btnPrice];
    
    _btnDistance = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnDistance.frame = CGRectMake(_btnPrice.right, 0, (DeviceWidth - 30)/4, 30);
    [_btnDistance setTitle:@"Distance" forState:UIControlStateNormal];
    [_btnDistance setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    _btnDistance.titleLabel.font = [UIFont systemFontOfSize:12];
    _btnDistance.tag = 4;
    [_btnDistance addTarget:self action:@selector(btnCkick:) forControlEvents:UIControlEventTouchUpInside];
    [_backGroundView addSubview:_btnDistance];
   
    
}
- (void)btnCkick:(UIButton *)btnCkick{

    NSLog(@"==-=-=--==%ld",(long)btnCkick.tag);
    if (btnCkick.tag == 1) {
        
        [_btnPopularity setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnPopularity.backgroundColor = [UIColor color222Red];
        [_btnRating setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _btnRating.backgroundColor = [UIColor whiteColor];
        [_btnPrice setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _btnPrice.backgroundColor = [UIColor whiteColor];
        [_btnDistance setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _btnDistance.backgroundColor = [UIColor whiteColor];
        
    }else if (btnCkick.tag == 2){
        
        [_btnPopularity setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _btnPopularity.backgroundColor = [UIColor whiteColor];
        [_btnRating setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnRating.backgroundColor = [UIColor color222Red];
        [_btnPrice setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _btnPrice.backgroundColor = [UIColor whiteColor];
        [_btnDistance setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _btnDistance.backgroundColor = [UIColor whiteColor];
    }else if (btnCkick.tag == 3){
        
        [_btnPopularity setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _btnPopularity.backgroundColor = [UIColor whiteColor];
        [_btnRating setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _btnRating.backgroundColor = [UIColor whiteColor];
        [_btnPrice setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnPrice.backgroundColor = [UIColor color222Red];
        [_btnDistance setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _btnDistance.backgroundColor = [UIColor whiteColor];
    }else{
        
        [_btnPopularity setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _btnPopularity.backgroundColor = [UIColor whiteColor];
        [_btnRating setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _btnRating.backgroundColor = [UIColor whiteColor];
        [_btnPrice setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _btnPrice.backgroundColor = [UIColor whiteColor];
        [_btnDistance setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnDistance.backgroundColor = [UIColor color222Red];
        
    }
    
    if (self.Delegate) {
        [self.Delegate delegateSortBy:btnCkick.tag];
    }

}

@end
