
#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"

@interface RestaurantSelectionCell : UITableViewCell

-(void)setRestaurant:(IMGRestaurant *)restaurant recently:(BOOL)recently;

@end
