//
//  GreatforButton.m
//  Qraved
//
//  Created by Jeff on 8/22/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "GreatforButton.h"
#import "UIColor+Helper.h"

@implementation GreatforButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)check{
    if(self.checked){
        self.checked = FALSE;
        [self setTitleColor:[UIColor color222222] forState:UIControlStateNormal];
        [self setBackgroundColor:[UIColor whiteColor]];
    }else{
        self.checked = TRUE;
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self setBackgroundColor:[UIColor color3BAF24]];
    }
    [UIView commitAnimations];
}

@end
