//
//  FilterItemOffersCellView.m
//  Qraved
//
//  Created by Jeff on 8/21/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "FilterItemOfferCellView.h"
#import "UIConstants.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "IMGCuisine.h"

@implementation FilterItemOfferCellView


- (id)initWithFrame:(CGRect)frame objectArray:(NSArray *)tmpObjectArray{
    self = [super initWithFrame:frame];
    if(self){
        
    }
    return self;
}

-(NSString *)titleFromDiscover:(IMGDiscover *)tmpDiscover{
    if(tmpDiscover==nil||((tmpDiscover.offArray==nil || tmpDiscover.offArray.count==0)&&(tmpDiscover.offerTypeArray==nil || tmpDiscover.offerTypeArray.count==0))){
        return @"";
    }
    NSMutableArray *titleArray = [[NSMutableArray alloc]initWithCapacity:0];
    for(IMGOfferType *tmpOfferType in tmpDiscover.offArray){
        [titleArray addObject:[NSString stringWithFormat:@"%@%@ Qraved Discount",tmpOfferType.off,@"%"]];
    }
    for(IMGOfferType *tmpOfferType in tmpDiscover.offerTypeArray){
//        if([tmpOfferType.typeId intValue]==1){
//            [titleArray addObject:@"Special Set Menu"];
//        }else if([tmpOfferType.typeId intValue]==2){
//            [titleArray addObject:@"In-restaurant Promotion"];
//        }else if([tmpOfferType.typeId intValue]==3){
//            [titleArray addObject:@"Featured Promo"];
//        }else{
//            
//        }
        
        
        [titleArray addObject:tmpOfferType.name];

    }
    for(IMGOfferType *tmpOfferType in tmpDiscover.eventArray){
        [titleArray addObject:tmpOfferType.name];
    }
    if(titleArray.count==0){
        return @"";
    }
    return [[NSString alloc] groupConcat:titleArray];
}

@end
