
#import "RestaurantSelectionCell.h"
#import "IMGRestaurant.h"
#import "UIConstants.h"
#import "DLStarRatingControl.h"
#import "UILabel+Helper.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
 
#import "UIImageView+WebCache.h"
#import "IMGTag.h"


@implementation RestaurantSelectionCell{
    UILabel *titleLable;
    UILabel *locationLabel;
    UILabel *recentlyLabel;
}

-(void)setRestaurant:(IMGRestaurant *)restaurant recently:(BOOL)recently{
    [titleLable setText:restaurant.title];
    NSString *locationName=restaurant.districtName;
    
    FMResultSet *resultSet = [[DBManager manager] executeQuery:[NSString stringWithFormat:@"select tagId from IMGRestaurantTag where restaurantId=%@",restaurant.restaurantId]];
    while([resultSet next]){
        IMGTag *tag=[IMGTag findById:[resultSet objectForColumnName:@"tagId"]];
        if(tag!=nil&&[tag.type isEqual:TAG_TYPE_LANDMARK]){
            locationName=tag.name;
            break;
        }
    }
    [resultSet close];
    
    [locationLabel setText:locationName];
    if(recently){
        [recentlyLabel setText:L(@"Recently Viewed")];
    }else{
        [recentlyLabel setText:@""];
    }
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        titleLable = [[UILabel alloc]initWithFrame:CGRectMake(8, 7, DeviceWidth-5, 16)];
        titleLable.font=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:13];
        titleLable.textColor=[UIColor color222222];
        titleLable.backgroundColor=[UIColor clearColor];
        [self addSubview:titleLable];


        locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 27, DeviceWidth-5, 14)];
        [locationLabel setFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:11]];
        [locationLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:locationLabel];
        
        recentlyLabel= [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth-85, 25, 85, 14)];
        [recentlyLabel setFont:[UIFont fontWithName:FONT_OPEN_SANS_LIGHT size:10]];
        [recentlyLabel setTextColor:[UIColor lightGrayColor]];
        [recentlyLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:recentlyLabel];

        UIImageView *celllineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 48, DeviceWidth, 1)];
        [celllineImageView setImage:[UIImage imageNamed:@"search_line"] ];
        [self addSubview:celllineImageView];

    }
    return self;
}


@end
