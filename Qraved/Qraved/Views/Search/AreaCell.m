//
//  AreaCell.m
//  Qraved
//
//  Created by lucky on 16/9/21.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "AreaCell.h"

@implementation AreaCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setText:(NSString *)title {
    if(self.titleLabel==nil){
        self.titleLabel = [[SearchCellLabel alloc] initWithBlackColorAndFrame:CGRectMake(15, 0, DeviceWidth-40, self.frame.size.height)];
        [self addSubview:self.titleLabel];
    }
    [self.titleLabel setText:title];
    
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
