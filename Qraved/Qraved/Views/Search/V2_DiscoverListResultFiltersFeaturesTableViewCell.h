//
//  V2_DiscoverListResultFiltersFeaturesTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol V2_DiscoverListResultFiltersFeaturesTableViewCellDelegate <NSObject>

- (void)delegateFeatures:(NSString *)Features FeaturesArray:(NSMutableArray *)FeaturesArray indexArray:(NSMutableArray *)indexArray;

@end


@interface V2_DiscoverListResultFiltersFeaturesTableViewCell : UITableViewCell<TTGTextTagCollectionViewDelegate>

@property (nonatomic, strong)  NSMutableArray  *tagIdArray;
@property (nonatomic,strong)NSMutableArray *tagsArr;
@property (nonatomic, strong)  NSMutableArray  *tagTextArray;
@property (nonatomic, strong)  NSMutableArray  *indexArray;
@property (nonatomic, weak)  id<V2_DiscoverListResultFiltersFeaturesTableViewCellDelegate>  Delegate;

- (void)initUI:(NSMutableArray *)tagssArr;

@end
