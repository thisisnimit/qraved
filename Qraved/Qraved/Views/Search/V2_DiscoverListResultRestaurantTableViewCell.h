//
//  V2_DiscoverListResultRestaurantTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLStarRatingControl.h"
#import "V2_DiscoverListResultRestaurantModel.h"
#import "IMGRestaurant.h"

@protocol V2_DiscoverListResultRestaurantTableViewCellDelegate <NSObject>

- (void)delegateLikeBtn:(UITableViewCell *)cell andRestaurant:(IMGRestaurant *)restaurant;

@end


@interface V2_DiscoverListResultRestaurantTableViewCell : UITableViewCell

//@property (nonatomic, strong)  V2_DiscoverListResultRestaurantModel  *model;
@property (nonatomic, weak)  id<V2_DiscoverListResultRestaurantTableViewCellDelegate> Delegate;

@property (nonatomic, strong)  IMGRestaurant  *model;

- (void)refreshSavedButton:(BOOL)isSaved;

@end
