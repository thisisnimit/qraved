//
//  V2_DiscoverListJournalSeeAllTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/25.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_DiscoverListResultRestaurantTableViewCellSubModel.h"


@interface V2_DiscoverListJournalSeeAllTableViewCell : UITableViewCell


@property (nonatomic, strong)  V2_DiscoverListResultRestaurantTableViewCellSubModel  *model;

@property (nonatomic, strong)  UIImageView  *iconImageView;
@property (nonatomic, strong)  UILabel  *lblTitle;

@end
