//
//  V2_DiscoverListResultFiltersAvailablityTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol V2_DiscoverListResultFiltersAvailablityTableViewCellDelegate <NSObject>

- (void)delegateAvailablity:(NSInteger)AvailablityTag;

@end

@interface V2_DiscoverListResultFiltersAvailablityTableViewCell : UITableViewCell

@property (nonatomic, strong)  UIButton  *btnPromo;
@property (nonatomic, strong)  UIButton  *btnOpenNow;
@property (nonatomic, strong)  UIButton  *btnSaved;

@property (nonatomic, weak)  id<V2_DiscoverListResultFiltersAvailablityTableViewCellDelegate> Delegate;

@end
