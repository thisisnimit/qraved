//
//  SearchScrolView.m
//  Qraved
//
//  Created by josn.liu on 2017/1/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "SearchScrolView.h"
#import "UIView+Helper.h"
#import "ImageAndTitleView.h"
#import "IMGCuisine.h"
#import "IMGLandMark.h"
#import "IMGDiningGuide.h"
#import "IMGDistrict.h"
#import "UIDevice+Util.h"
#define TopSplace 10
@implementation SearchScrolView{

    UILabel *title;
    UIScrollView *scrolView;
    NSMutableArray *restaurantArr;
}

-(instancetype)initWithFrame:(CGRect)frame andtype:(SearchType)type withDataArr:(NSArray*)dataArr withImageHeight:(CGFloat)height{

    if ([super initWithFrame:frame]) {
        self.type=type;
        [self addTitleView];
        [self addMainView:dataArr withImageHeight:height];
    }

    return self;


}
-(void)addTitleView{

    title=[[UILabel alloc]initWithFrame:CGRectMake(15, TopSplace, DeviceWidth-100, 20)];
    title.font=[UIFont boldSystemFontOfSize:18];
    title.textColor = [UIColor color333333];
    if ([UIDevice isIphone4Now]||[UIDevice isIphone5]) {
        title.font=[UIFont boldSystemFontOfSize:16];
    }

    [self addSubview:title];
    UILabel *showAll=[[UILabel alloc]initWithFrame:CGRectMake(DeviceWidth-85, TopSplace, 100, 20)];
    showAll.font=[UIFont boldSystemFontOfSize:14];
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(showAllTap:)];
    showAll.userInteractionEnabled=YES;
    [showAll addGestureRecognizer:tap];
    showAll.textColor=[UIColor redColor];
    [self addSubview:showAll];
    switch (self.type) {
        case CuisineType:
        {
            title.text=[NSString stringWithFormat:@"Browse %@ by Cuisine",[[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT] capitalizedString]];
            showAll.text=@"Show All >";
        }
            break;
        case DiningGuideType:
        {
            title.text=@"Dining Guides";
            showAll.text=@"";
        }
            break;
        case LandMarkType:
        {
            title.text=@"Malls";
            showAll.text=@"Show All >";
        }
            break;
        case DistrictType:
        {
            title.text=@"District";
            showAll.text=@"Show All >";
        }
            break;
        case SavedType:
        {
            title.text=@"Recently Viewed";
            title.font=[UIFont boldSystemFontOfSize:FONT_SIZE_14];
        }
            break;

            
        default:
            break;
    }

    UIImageView *moreImage=[[UIImageView alloc]initWithFrame:CGRectMake(showAll.endPointX, TopSplace, 7, 10)];
    moreImage.backgroundColor=[UIColor redColor];
    [self addSubview:moreImage];
    
}
-(void)showAllTap:(UITapGestureRecognizer*)tap{

    if ([self.deleage respondsToSelector:@selector(gotoShowAll:)]) {
        [self.deleage gotoShowAll:self.type];
    }
}
-(void)addMainView:(NSArray*)dataArr withImageHeight:(CGFloat)height{
    scrolView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, TopSplace+30, DeviceWidth, self.frame.size.height-TopSplace-20-8)];
    scrolView.showsHorizontalScrollIndicator=NO;
    scrolView.showsVerticalScrollIndicator=NO;
    [self addSubview:scrolView];
    switch (self.type) {
        case CuisineType:
        {
            NSMutableArray *arr=[[NSMutableArray alloc] init];
            for (int i=0; i<dataArr.count; i++) {
                 IMGCuisine *cusine=[dataArr objectAtIndex:i];
                ImageAndTitleView *imageview=[[ImageAndTitleView alloc]initWithFrame:CGRectMake(15+(height+10)*i, 5,height, 100) withImageUrl:cusine.imageUrl withTitle:cusine.name andValue:cusine.restaurantCount];
                imageview.tag=i;
                UITapGestureRecognizer *cuisineTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
                [imageview addGestureRecognizer:cuisineTap];
                imageview.userInteractionEnabled=YES;
                [scrolView addSubview:imageview];
                [arr addObject:[NSNumber numberWithFloat:imageview.viewHeight]];
                CGFloat maxValue = [[arr valueForKeyPath:@"@max.floatValue"] floatValue];
                scrolView.contentSize=CGSizeMake((height+15)*(i+1), imageview.viewHeight);
                scrolView.frame=CGRectMake(0, TopSplace+30, DeviceWidth, maxValue+5);
                self.scrolViewHeight=scrolView.endPointY;
                    
            }
        
        }
            break;
        case DiningGuideType:
        {
            NSMutableArray *arr=[[NSMutableArray alloc] init];
            for (int i=0; i<dataArr.count; i++) {
                IMGDiningGuide *diningGuide=[dataArr objectAtIndex:i];
                ImageAndTitleView *imageview=[[ImageAndTitleView alloc]initWithFrame:CGRectMake(15+(height+10)*i, 5,height, 100) withImageUrl:diningGuide.headerImage withTitle:diningGuide.name andValue:diningGuide.restaurantCount];
                [scrolView addSubview:imageview];
                imageview.tag=i;
                UITapGestureRecognizer *cuisineTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
                [imageview addGestureRecognizer:cuisineTap];
                imageview.userInteractionEnabled=YES;
                [arr addObject:[NSNumber numberWithFloat:imageview.viewHeight]];
                CGFloat maxValue = [[arr valueForKeyPath:@"@max.floatValue"] floatValue];
                scrolView.contentSize=CGSizeMake((height+15)*(i+1), imageview.viewHeight);
                scrolView.frame=CGRectMake(0, TopSplace+30, DeviceWidth, maxValue+5);
                self.scrolViewHeight=scrolView.endPointY;
                
            }


            
        }
            break;
        case LandMarkType:
        {
            NSMutableArray *arr=[[NSMutableArray alloc] init];
            for (int i=0; i<dataArr.count; i++) {
                IMGLandMark *landMark=[dataArr objectAtIndex:i];
                ImageAndTitleView *imageview=[[ImageAndTitleView alloc]initWithFrame:CGRectMake(15+(height+10)*i, 5,height, 100) withImageUrl:landMark.imageUrl withTitle:landMark.name andValue:landMark.restaurantCount];
                imageview.tag=i;
                [scrolView addSubview:imageview];
                UITapGestureRecognizer *cuisineTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
                [imageview addGestureRecognizer:cuisineTap];
                imageview.userInteractionEnabled=YES;
                [arr addObject:[NSNumber numberWithFloat:imageview.viewHeight]];
                CGFloat maxValue = [[arr valueForKeyPath:@"@max.floatValue"] floatValue];
                scrolView.contentSize=CGSizeMake((height+15)*(i+1), imageview.viewHeight);
                scrolView.frame=CGRectMake(0, TopSplace+30, DeviceWidth, maxValue+5);
                self.scrolViewHeight=scrolView.endPointY;
                
            }
   
            
        }
            break;
        case DistrictType:
        {
             NSMutableArray *arr=[[NSMutableArray alloc] init];
            for (int i=0; i<dataArr.count; i++) {
                IMGDistrict *district=[dataArr objectAtIndex:i];
                ImageAndTitleView *imageview=[[ImageAndTitleView alloc]initWithFrame:CGRectMake(15+(height+10)*i, 5,height, 100) withImageUrl:district.imageUrl withTitle:district.name andValue:district.restaurantCount];
                imageview.tag=i;
                [scrolView addSubview:imageview];
                 imageview.frame=CGRectMake(imageview.startPointX, imageview.startPointY, height, imageview.viewHeight);
                UITapGestureRecognizer *cuisineTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
                [imageview addGestureRecognizer:cuisineTap];
                imageview.userInteractionEnabled=YES;
                [arr addObject:[NSNumber numberWithFloat:imageview.viewHeight]];
                CGFloat maxValue = [[arr valueForKeyPath:@"@max.floatValue"] floatValue];
                scrolView.contentSize=CGSizeMake((height+15)*(i+1), imageview.viewHeight);
                scrolView.frame=CGRectMake(0, TopSplace+30, DeviceWidth, maxValue+5);
                self.scrolViewHeight=scrolView.endPointY;
                
            }
 
            
        }
            break;
        case SavedType:
        {
            restaurantArr = [NSMutableArray arrayWithArray:dataArr];
            NSMutableArray *arr=[[NSMutableArray alloc] init];
            for (int i=0; i<dataArr.count; i++) {
                IMGRestaurant *restaurant=[dataArr objectAtIndex:i];
                ImageAndTitleView *imageview=[[ImageAndTitleView alloc]initWithFrame:CGRectMake(15+(height+10)*i, 0,height, 62+height*.56+15) withRestaurant:restaurant];
                UIButton *unSavedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                unSavedBtn.frame =CGRectMake(imageview.size.width-40, 0, 40, 34);
                [unSavedBtn setImage:[UIImage imageNamed:@"ic_unheart_full"] forState:UIControlStateNormal];
                unSavedBtn.tag = i;
                [unSavedBtn addTarget:self action:@selector(unSavedBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
                [imageview addSubview:unSavedBtn];
                imageview.tag=i;
                UITapGestureRecognizer *cuisineTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGesture:)];
                [imageview addGestureRecognizer:cuisineTap];
                imageview.userInteractionEnabled=YES;
                imageview.layer.masksToBounds = YES;
                imageview.layer.borderWidth = 0.5;
                imageview.layer.borderColor = [UIColor colorCCCCCC].CGColor;
                [scrolView addSubview:imageview];
                
                [arr addObject:[NSNumber numberWithFloat:imageview.viewHeight]];
                CGFloat maxValue = [[arr valueForKeyPath:@"@max.floatValue"] floatValue];
                scrolView.frame=CGRectMake(0, TopSplace+30, DeviceWidth, maxValue+20);
                scrolView.contentSize=CGSizeMake(15+(height+10)*(i+1)-10, 0);
                
                self.scrolViewHeight=scrolView.endPointY;
                
            }
        }
            break;
        default:
            break;
    }
}
-(void)tapGesture:(UITapGestureRecognizer*)tap{

    UIView *view=tap.view;
    IMGRestaurant *restaurant = [restaurantArr objectAtIndex:view.tag];
    
    [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Page" andRestaurantId:restaurant.restaurantId andRestaurantTitle:restaurant.title andLocation:nil andOrigin:@"Saved - Recent" andType:nil];
    
    if ([self.deleage respondsToSelector:@selector(gotoPage:andIndex:)]) {
        [self.deleage gotoPage:self.type andIndex:[restaurant.restaurantId integerValue]];
    }

}
-(void)unSavedBtnClicked:(UIButton*)sender{
    IMGRestaurant *restaurant = [restaurantArr objectAtIndex:sender.tag];
    if ([self.deleage respondsToSelector:@selector(addRestaurantTolistWithRestrantId:)]) {
        [self.deleage addRestaurantTolistWithRestrantId:[restaurant.restaurantId integerValue]];
    }
}
@end
