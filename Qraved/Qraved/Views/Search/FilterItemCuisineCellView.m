//
//  FilterItemCuisineCellView.m
//  Qraved
//
//  Created by Jeff on 8/21/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "FilterItemCuisineCellView.h"
#import "NSString+Helper.h"

@implementation FilterItemCuisineCellView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(NSString *)titleFromDiscover:(IMGDiscover *)tmpDiscover{
    if(tmpDiscover==nil||(tmpDiscover.cuisineArray==nil || tmpDiscover.cuisineArray.count==0)){
        return @"";
    }
    NSMutableArray *titleArray = [[NSMutableArray alloc]initWithCapacity:0];
    for(IMGCuisine *tmpCuisine in tmpDiscover.cuisineArray){
        [titleArray addObject:tmpCuisine.name];
    }
    if(titleArray.count==0){
        return @"";
    }
    return [[NSString alloc] groupConcat:titleArray];
}

@end
