//
//  RestaurantCellView.m
//  Qraved
//
//  Created by Jeff on 8/14/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "RestaurantCellView.h"
#import "IMGRestaurant.h"
#import "UIConstants.h"
#import "DLStarRatingControl.h"
#import "UIView+Helper.h"
#import "UILabel+Helper.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
 
#import "UIImageView+WebCache.h"
#import "OfferLabel.h"

#define CELL_RATING_STAR_WIDTH 10


@implementation RestaurantCellView
{
    UILabel *titleLabel;
    UILabel *cuisineAndDistrictLabel;

    UILabel *priceLevelLabel;
    OfferLabel *offerLabel;
    
    UILabel *reviewCountLabel;
    UIImage *reviewCountImage;
    UIImageView *reviewCountImageView;

    UIImageView *restaurantImageView;
    
    UILabel * titleLable;
    DLStarRatingControl *ratingScoreControl;
    UIImageView *logoImageView;
    UILabel *distanceLabel;
    IMGRestaurant *_restaurant;
    
}

- (id)initWithFrame:(CGRect)frame andRestaurant:(IMGRestaurant *)restaurant
{
    self = [super initWithFrame:frame];
    if (self) {
        titleLable = [[UILabel alloc]initWithFrame:CGRectMake(LEFT_X, TITLE_LABEL_Y, DeviceWidth-2*LEFTLEFTSET-RESTAURANT_IMAGE_SIZE-5, TITLE_LABEL_HEIGHT)];
        titleLable.font=[UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:18];
        titleLable.textColor=[UIColor color222222];
        titleLable.backgroundColor=[UIColor clearColor];
        [self addSubview:titleLable];

        cuisineAndDistrictLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_X, TITLE_LABEL_Y+TITLE_LABEL_HEIGHT+GAP_BETWEEN_TITLE_AND_CUISINE, 260, CUISINE_DISTRICT_LABEL_HEIGHT)];
        cuisineAndDistrictLabel.numberOfLines=2;
        cuisineAndDistrictLabel.lineBreakMode=NSLineBreakByTruncatingTail;
        cuisineAndDistrictLabel.font=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:13];
        [self addSubview:cuisineAndDistrictLabel];
        
        ratingScoreControl=[[DLStarRatingControl alloc]initWithFrame:CGRectMake(LEFT_X, TITLE_LABEL_Y+TITLE_LABEL_HEIGHT+GAP_BETWEEN_TITLE_AND_CUISINE+CUISINE_DISTRICT_LABEL_HEIGHT+GAP_BETWEEN_CUISINE_AND_REVIEW, 74, 10) andStars:5 andStarWidth:CELL_RATING_STAR_WIDTH isFractional:YES spaceWidth:5];
        ratingScoreControl.userInteractionEnabled=NO;
        [self addSubview:ratingScoreControl];
        
        offerLabel = [[OfferLabel alloc] initWithFrame:CGRectMake(-4,80,100,28)];
        [self addSubview:offerLabel];
        
        reviewCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_X+ratingScoreControl.frame.size.width+11, TITLE_LABEL_Y+TITLE_LABEL_HEIGHT+GAP_BETWEEN_TITLE_AND_CUISINE+CUISINE_DISTRICT_LABEL_HEIGHT+GAP_BETWEEN_CUISINE_AND_REVIEW, 18,CELL_RATING_STAR_WIDTH)];
        [reviewCountLabel setTextAlignment:NSTextAlignmentRight];
        [reviewCountLabel setFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:10]];
        [reviewCountLabel setTextColor:[UIColor colorFFC000]];
        [reviewCountLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:reviewCountLabel];
        
        reviewCountImageView = [[UIImageView alloc] initWithFrame:CGRectMake(LEFT_X+ratingScoreControl.frame.size.width+15+reviewCountLabel.frame.size.width, TITLE_LABEL_Y+TITLE_LABEL_HEIGHT+GAP_BETWEEN_TITLE_AND_CUISINE+CUISINE_DISTRICT_LABEL_HEIGHT+GAP_BETWEEN_CUISINE_AND_REVIEW+2, 8,8)];
        [reviewCountImageView setImage:[UIImage imageNamed:@"reviewCount"]];
        [self addSubview:reviewCountImageView];
        
        restaurantImageView = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-RESTAURANT_IMAGE_SIZE-15, 15, RESTAURANT_IMAGE_SIZE, RESTAURANT_IMAGE_SIZE)];
        [restaurantImageView.layer setMasksToBounds:YES];
        restaurantImageView.layer.borderColor = [UIColor whiteColor].CGColor;
        restaurantImageView.layer.cornerRadius = 5;
        restaurantImageView.layer.borderWidth = 0.3;
        [restaurantImageView setAlpha:0.0];
        [self addSubview:restaurantImageView];
        
        
//        _photoImageView = [[UIImageView alloc] init];
//        _photoImageView.frame = CGRectMake(LEFTLEFTSET, 10, DeviceWidth-LEFTLEFTSET*2, 160);
//        
//        [self addSubview:_photoImageView];
        
        logoImageView = [[UIImageView alloc] init];
        logoImageView.frame = CGRectMake(restaurantImageView.frame.size.width-30, 0, 30, 30);
        logoImageView.backgroundColor = [UIColor clearColor];
        [restaurantImageView addSubview:logoImageView];
        
        distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth-17-RESTAURANT_IMAGE_SIZE/2+4, restaurantImageView.frame.origin.y+ restaurantImageView.frame.size.height+5, 45, 20)];
        [distanceLabel setFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:10]];
        [distanceLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:distanceLabel];
        
        UIImageView *lineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-15-RESTAURANT_IMAGE_SIZE/2, restaurantImageView.frame.origin.y+restaurantImageView.frame.size.height+26, 48, 2)];
        [lineImageView setImage:[UIImage imageNamed:@"PlottingScaleGray"] ];
        [self addSubview:lineImageView];
        
        UIImageView *celllineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, frame.size.height-1, DeviceWidth, 1)];
        [celllineImageView setImage:[UIImage imageNamed:@"search_line"] ];
        [self addSubview:celllineImageView];
        
    }
    return self;
}
-(void)setRestaurant:(IMGRestaurant *)restaurant {
    
    _restaurant=restaurant;
    
    titleLable.text=_restaurant.title;

    NSString *dollar=nil;
    switch(_restaurant.priceLevel.intValue){
        case 1:dollar=@"Below 100K";break;
        case 2:dollar=@"100K - 200K";break;
        case 3:dollar=@"200K - 300K";break;
        case 4:dollar=@"Start from 300K";break;
    }

    NSString *cuisineAndDistrictText=[NSString stringWithFormat:@"%@ / %@ / %@",_restaurant.cuisineName,_restaurant.districtName,dollar];

    CGSize size=[cuisineAndDistrictText sizeWithFont:cuisineAndDistrictLabel.font constrainedToSize:CGSizeMake(DeviceWidth-LEFT_X*2-RESTAURANT_IMAGE_SIZE-5,cuisineAndDistrictLabel.font.lineHeight*2) lineBreakMode:NSLineBreakByTruncatingTail];
    cuisineAndDistrictLabel.frame=CGRectMake(LEFT_X, TITLE_LABEL_Y+TITLE_LABEL_HEIGHT+GAP_BETWEEN_TITLE_AND_CUISINE, size.width,size.height);
    cuisineAndDistrictLabel.text=cuisineAndDistrictText;

    ratingScoreControl.frame=CGRectMake(LEFT_X, TITLE_LABEL_Y+TITLE_LABEL_HEIGHT+GAP_BETWEEN_TITLE_AND_CUISINE+size.height+GAP_BETWEEN_CUISINE_AND_REVIEW, 74, 10);
    
    [ratingScoreControl updateRating:[NSNumber numberWithFloat:[restaurant.ratingScore floatValue]]];

    reviewCountLabel.frame=CGRectMake(LEFT_X+ratingScoreControl.frame.size.width+11, TITLE_LABEL_Y+TITLE_LABEL_HEIGHT+GAP_BETWEEN_TITLE_AND_CUISINE+size.height+GAP_BETWEEN_CUISINE_AND_REVIEW, 18,CELL_RATING_STAR_WIDTH);
    
    [reviewCountLabel setText:[NSString stringWithFormat:@"%@",_restaurant.ratingCount]];

    reviewCountImageView.frame=CGRectMake(LEFT_X+ratingScoreControl.frame.size.width+15+reviewCountLabel.frame.size.width, TITLE_LABEL_Y+TITLE_LABEL_HEIGHT+GAP_BETWEEN_TITLE_AND_CUISINE+size.height+GAP_BETWEEN_CUISINE_AND_REVIEW+2, 8,8);

    offerLabel.frame =CGRectMake(-4,ratingScoreControl.endPointY+10,100,28);
    
    [offerLabel setRestaurant:_restaurant  andBookDate:nil andBookTime:nil andPax:nil andRestaurantOffer:nil];

    NSString *urlString = [_restaurant.imageUrl returnFullImageUrlWithWidth:RESTAURANT_IMAGE_SIZE];

    __weak typeof(restaurantImageView) weakRestaurantImageView = restaurantImageView;
//    [restaurantImageView setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//        [UIView animateWithDuration:1.0 animations:^{
//            [weakRestaurantImageView setAlpha:1.0];
//        }];
//    }];
    [restaurantImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [UIView animateWithDuration:1.0 animations:^{
                        [weakRestaurantImageView setAlpha:1.0];
                    }];
    }];
    
    if (_restaurant.logoImage.length>1)
    {
        NSString *logoUrlString;
        
        NSString* encodedUrl = [_restaurant.logoImage stringByAddingPercentEscapesUsingEncoding:
                                NSASCIIStringEncoding];
        if(encodedUrl!=NULL)
        {
            logoUrlString = [QRAVED_WEB_IMAGE_SERVER stringByAppendingString:encodedUrl];
        }
        
        __weak typeof (logoImageView) weakLogoImageView = logoImageView;
//        NSLog(@"logoUrlString = %@",logoUrlString);
//        [logoImageView setImageWithURL:[NSURL URLWithString:logoUrlString] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//            [UIView animateWithDuration:1.0 animations:^{
//                [weakLogoImageView setAlpha:1.0];
//            }];
//        }];
        [logoImageView sd_setImageWithURL:[NSURL URLWithString:logoUrlString] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [UIView animateWithDuration:1.0 animations:^{
                                [weakLogoImageView setAlpha:1.0];
                            }];
        }];
    }else
    {
        [logoImageView setImage:nil];
    }
    [distanceLabel setText:[_restaurant distance]];
}

@end
