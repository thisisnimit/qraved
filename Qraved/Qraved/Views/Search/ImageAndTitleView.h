//
//  ImageAndTitleView.h
//  Qraved
//
//  Created by josn.liu on 2017/1/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageAndTitleView : UIView
@property(nonatomic,assign)CGFloat viewHeight;
-(instancetype)initWithFrame:(CGRect)frame withImageUrl:(NSString*)url withTitle:(NSString*)title andValue:(NSNumber*)value;
-(instancetype)initWithFrame:(CGRect)frame withRestaurant:(IMGRestaurant *)restaurant;

@end
