//
//  SearchRecentlyViewedCell.m
//  Qraved
//
//  Created by harry on 2017/11/28.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "SearchRecentlyViewedCell.h"
#import "V2_RestaurantCell.h"
@interface SearchRecentlyViewedCell ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,V2_RestaurantCellDelegate>{
    UICollectionView *restaurantCollectionView;
    UIView *spaceView;
    UILabel *lblTitle;
    IMGRestaurant *currentRestaurant;
    NSIndexPath *currntIndexPath;
}
@end

@implementation SearchRecentlyViewedCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(216, 195);
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 10;
    layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 0);
    
    restaurantCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,0, DeviceWidth, 195) collectionViewLayout:layout];
    restaurantCollectionView.backgroundColor = [UIColor whiteColor];
    restaurantCollectionView.delegate = self;
    restaurantCollectionView.dataSource = self;
    restaurantCollectionView.showsHorizontalScrollIndicator = NO;
    [self.contentView addSubview:restaurantCollectionView];
    
    spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, restaurantCollectionView.endPointY+20, DeviceWidth, 5)];
    spaceView.backgroundColor = [UIColor colorWithHexString:@"#EAEAEA"];
    [self.contentView addSubview:spaceView];
    
    restaurantCollectionView.hidden = YES;
    spaceView.hidden = YES;
    
    [restaurantCollectionView registerNib:[UINib nibWithNibName:@"V2_RestaurantCell" bundle:nil] forCellWithReuseIdentifier:@"restaurantCell"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.restaurantArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    V2_RestaurantCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"restaurantCell" forIndexPath:indexPath];
    IMGRestaurant *model = [self.restaurantArray objectAtIndex:indexPath.row];
    cell.currentIndex = indexPath;
    cell.delegate = self;
    cell.model = model;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    IMGRestaurant *model = [self.restaurantArray objectAtIndex:indexPath.row];
    if (self.gotoRestaurantDetail) {
        self.gotoRestaurantDetail(model);
    }
}

- (void)setRestaurantArray:(NSArray *)restaurantArray{
    _restaurantArray = restaurantArray;
    restaurantCollectionView.hidden = NO;
    spaceView.hidden = NO;
    [restaurantCollectionView reloadData];
    
}

- (void)savedToList:(IMGRestaurant *)restaurant IndexPath:(NSIndexPath *)indexPath{
    currentRestaurant = restaurant;
    currntIndexPath = indexPath;
    if (self.savedTapped) {
        self.savedTapped(restaurant);
    }
}

- (void)freshSavedButton:(BOOL)liked{
    if (liked) {
        currentRestaurant.saved = @1;
    }else{
        currentRestaurant.saved = @0;
    }
    
    V2_RestaurantCell *cell = (V2_RestaurantCell *)[restaurantCollectionView cellForItemAtIndexPath:currntIndexPath];
    [cell freshSavedButton:liked];
    
    //[restaurantCollectionView reloadData];
    
    
}

@end
