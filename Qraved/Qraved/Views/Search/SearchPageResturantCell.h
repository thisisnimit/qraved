//
//  SearchPageResturantCell.h
//  Qraved
//
//  Created by imaginato on 16/5/17.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"
#import "DLStarRatingControl.h"
#import "OfferLabel.h"
#import "IMGDiscover.h"
#import "IMGRestaurant.h"
@protocol searchPageCellDelegate <NSObject>

-(void)reviewTap:(IMGRestaurant*)restaurant;
-(void)restaurantImageTap:(IMGRestaurant*)restaurant;
- (void)bookNowTap:(IMGRestaurant *)restaurant;
- (void)menuBtnTap:(IMGRestaurant *)restaurant;
- (void)searchDistrictTap:(IMGRestaurant *)restaurant;
@end


@interface SearchPageResturantCell : UITableViewCell
@property (nonatomic,retain) IMGRestaurant * restaurant;

@property (nonatomic,retain,readonly) UILabel * titleLable;
@property (nonatomic,retain,readonly) UILabel * cuisineAndDistrictLabel;

@property (nonatomic,retain,readonly) OfferLabel * offerLabel;

@property (nonatomic,retain,readonly) UILabel * reviewCountLabel;

@property (nonatomic,retain,readonly) UIImageView * restaurantImageView;


@property (nonatomic,retain,readonly) DLStarRatingControl *ratingScoreControl;
//@property (nonatomic,retain,readonly) StarsView *ratingScoreControl;

@property (nonatomic,retain,readonly) UILabel *distanceLabel;

@property (nonatomic,retain,readonly) UIImageView *logoImageView;
@property(nonatomic,assign)NSInteger index;
@property(nonatomic,assign)id<searchPageCellDelegate> searchPageCellDelegate;

-(void)setRestaurant:(IMGRestaurant *)restaurant  andBookDate:(NSDate *)bookDate andBookTime:(NSString *)bookTime andPax:(NSNumber *)pax andRestaurantOffer:(IMGRestaurantOffer *)restaurantOffer andRestaurantNumber:(NSInteger)restaurantNumber;
+(CGFloat)calculateHeightWithRestaurant:(IMGRestaurant *)restaurant  andBookDate:(NSDate *)bookDate andBookTime:(NSString *)bookTime andPax:(NSNumber *)pax andRestaurantOffer:(IMGRestaurantOffer *)restaurantOffer andRestaurantNumber:(NSInteger)restaurantNumber;
@end
