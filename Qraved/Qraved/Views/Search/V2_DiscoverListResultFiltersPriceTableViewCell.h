//
//  V2_DiscoverListResultFiltersPriceTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDHelper.h"

@protocol V2_DiscoverListResultFiltersPriceTableViewCellDelegate <NSObject>

-(void)delegatePrice:(NSInteger)PriceTag;

@end

@interface V2_DiscoverListResultFiltersPriceTableViewCell : UITableViewCell<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic,strong)NSArray *tagsArr;
@property (nonatomic, strong)  UICollectionView  *centerCollectionView;


@property (nonatomic, strong)  UILabel  *lblBelow;
@property (nonatomic, strong)  UILabel  *lblfifty;
@property (nonatomic, strong)  UILabel  *lbltwoHundred;
@property (nonatomic, strong)  UILabel  *lblAbove;

@property (nonatomic, weak)  id<V2_DiscoverListResultFiltersPriceTableViewCellDelegate> Delegate;
@end
