//
//  OfferTypeCell.h
//  Qraved
//
//  Created by Jeff on 8/22/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "CheckableTableViewCell.h"
#import "IMGOfferType.h"

@interface OfferTypeCell : CheckableTableViewCell

@property(nonatomic,strong) IMGOfferType *object;

@end
