//
//  V2_DiscoverListResultFiltersCuisinesTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol V2_DiscoverListResultFiltersCuisinesTableViewCellDelegate <NSObject>

- (void)delegateCuisines:(NSString *)Cuisines CuisinesArray:(NSMutableArray *)CuisinesArray indexArray:(NSMutableArray *)indexArray;

@end

@interface V2_DiscoverListResultFiltersCuisinesTableViewCell : UITableViewCell<TTGTextTagCollectionViewDelegate>

- (void)initUI:(NSMutableArray *)tagssArr;
@property (nonatomic,strong)NSMutableArray *tagsArr;
@property (nonatomic, strong)  NSMutableArray  *cuisineIdArray;
@property (nonatomic, strong)  NSMutableArray  *tagTextArray;
@property (nonatomic, strong)  NSMutableArray  *indexArray;

@property (nonatomic, weak)  id<V2_DiscoverListResultFiltersCuisinesTableViewCellDelegate>  Delegate;
@end
