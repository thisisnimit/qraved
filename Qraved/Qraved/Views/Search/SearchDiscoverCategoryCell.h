//
//  SearchDiscoverCategoryCell.h
//  Qraved
//
//  Created by Jeff on 8/11/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGDiscover.h"

@interface SearchDiscoverCategoryCell : UITableViewCell
@property(nonatomic,retain)UIImageView *discoverImageView;
@property(nonatomic,retain)UILabel *titleLabel;

-(void)updateDiscoverName:(IMGDiscover *)discover;

@end
