//
//  SearchScrolView.h
//  Qraved
//
//  Created by josn.liu on 2017/1/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef  NS_ENUM(NSInteger,SearchType){
    CuisineType,
    DiningGuideType,
    LandMarkType,
    SavedType,
    DistrictType

};
@protocol SearchScrolViewDelegate <NSObject>

- (void)gotoPage:(SearchType)type andIndex:(NSInteger)index;
@optional
-(void)gotoShowAll:(SearchType)type;
-(void)addRestaurantTolistWithRestrantId:(NSInteger)restaurantId;
@end

@interface SearchScrolView : UIView
@property(nonatomic,assign)CGFloat scrolViewHeight;
@property(nonatomic,assign)CGFloat imageViewHeight;
@property(nonatomic,assign)SearchType type;
@property(nonatomic,assign)id<SearchScrolViewDelegate>deleage;
-(instancetype)initWithFrame:(CGRect)frame andtype:(SearchType)type withDataArr:(NSArray*)dataArr withImageHeight:(CGFloat)height;
@end
