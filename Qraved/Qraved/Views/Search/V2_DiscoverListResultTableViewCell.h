//
//  V2_DiscoverListResultTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_DiscoverListResultRestaurantTableViewCellModel.h"
#import "V2_DiscoverListResultRestaurantTableViewCellSubModel.h"


@protocol V2_DiscoverListResultTableViewCellDelegate <NSObject>

- (void)delegateSellegateIndexpath:(NSNumber *)MyID;

@end

@interface V2_DiscoverListResultTableViewCell : UITableViewCell<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

- (void)createUIWithModel:(V2_DiscoverListResultRestaurantTableViewCellModel *)model;

@property (nonatomic, strong)  V2_DiscoverListResultRestaurantTableViewCellModel  *V2_DiscoverListResultRestaurantModel;
@property (nonatomic, strong)  V2_DiscoverListResultRestaurantTableViewCellSubModel  *V2_DiscoverListResultRestaurantSubModel;

@property (nonatomic, weak)  id<V2_DiscoverListResultTableViewCellDelegate> Delegate;
@end
