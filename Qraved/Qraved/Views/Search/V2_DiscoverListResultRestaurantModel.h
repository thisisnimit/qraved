//
//  V2_DiscoverListResultRestaurantModel.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/24.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface V2_DiscoverListResultRestaurantModel : NSObject

@property (nonatomic, strong)  NSString  *address1;
@property (nonatomic, strong)  NSString  *altText;
@property (nonatomic, strong)  NSString  *bestOfferName;
@property (nonatomic, strong)  NSNumber  *bookStatus;
@property (nonatomic, strong)  NSString  *myClass;
@property (nonatomic, strong)  NSString  *cuisineName;
@property (nonatomic, strong)  NSNumber  *districtId;
@property (nonatomic, strong)  NSString  *districtName;
@property (nonatomic, strong)  NSString  *eventLogo;
@property (nonatomic, strong)  NSString  *imageUrl;
@property (nonatomic, strong)  NSString  *latitude;
@property (nonatomic, strong)  NSString  *longitude;
@property (nonatomic, strong)  NSNumber  *menuPhotoCount;
@property (nonatomic, strong)  NSDictionary  *nextOpenDay;/*closedTime intermission openDay openTime*/
@property (nonatomic, strong)  NSString  *phoneNumber;
@property (nonatomic, strong)  NSString  *priceLevel;
@property (nonatomic, strong)  NSNumber  *ratingScore;
@property (nonatomic, strong)  NSNumber  *restaurantId;
@property (nonatomic, strong)  NSNumber  *reviewCount;
@property (nonatomic, strong)  NSNumber  *saved;
@property (nonatomic, strong)  NSDictionary  *secondOpenDay;/*closedTime intermission openDay openTime*/
@property (nonatomic, strong)  NSNumber  *state;
@property (nonatomic, strong)  NSString  *stateName;
@property (nonatomic, strong)  NSString  *title;
@property (nonatomic, strong)  NSDictionary  *yesterDayOpenTime;/*closedTime intermission openDay openTime*/

@property (nonatomic, strong)  NSString  *wellKnownFor;
@property (nonatomic, strong)  NSNumber  *trendingInstagram;

@property (nonatomic, strong)  NSNumber  *inJournal;
@property (nonatomic, strong)  NSString  *journaArticlelTitle;
@property (nonatomic, strong)  NSNumber  *journalArticleId;



@end
