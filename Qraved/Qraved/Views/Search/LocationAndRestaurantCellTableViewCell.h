//
//  LocationAndRestaurantCellTableViewCell.h
//  Qraved
//
//  Created by Jeff on 10/16/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"
#import "IMGDistrictAndLandmark.h"
#import "IMGCuisineAndFoodType.h"

@interface LocationAndRestaurantCellTableViewCell : UITableViewCell
@property (nonatomic,retain) IMGRestaurant *restaurant;
@property (nonatomic,retain) IMGDistrictAndLandmark *district;
@property (nonatomic,retain) IMGCuisineAndFoodType *food;
@end
