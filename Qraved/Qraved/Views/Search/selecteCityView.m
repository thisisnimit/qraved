//
//  selecteCityView.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "selecteCityView.h"
#import "selecteCityViewCollectionViewCell.h"

@implementation selecteCityView

//+(instancetype)sdTagsViewWithTagsArr:(NSArray*)tagsArr{
//    selecteCityView *sdTagsView =[[selecteCityView alloc]init];
//    sdTagsView.tagsArr =[[NSArray alloc]initWithArray:tagsArr];
//    return sdTagsView;
//}


- (instancetype)initWithFrame:(CGRect)frame{

    if ([super initWithFrame:frame]) {
        [self initUI];
//        self.tagsArr = @[@"sdsadds",@"qqq",@"wweewewewe",@"231e21e12e12",@"sdsaddsadssdsadadadaasddsa",@"qqq",@"wweewewewe",@"231e21e12e12",@"sdsaddsadssdsadadadaasddsa",@"qqq"];
        self.tagsCenterArr = @[@"sdsadds",@"qqq",@"wweewewewe",@"231e21e12e12",@"sdsaddsadssdsadadadaasddsa",@"qqq",@"wweewewewe",@"231e21e12e12",@"sdsaddsadssdsadadadaasddsa",@"qqq",@"wweewewewe",@"231e21e12e12",@"sdsaddsadssdsadadadaasddsa",@"qqq",@"wweewewewe",@"231e21e12e12",@"sdsaddsadssdsadadadaasddsa",@"qqq",@"wweewewewe",@"231e21e12e12"];
    }
    return self;
}

- (void) initUI{
    
    self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4f];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.minimumLineSpacing = 5;
    flowLayout.minimumInteritemSpacing = 5;
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 15);
    
    _topCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, DeviceHeight +20 - 408, DeviceWidth, 40) collectionViewLayout:flowLayout];
    _topCollectionView.delegate = self;
    _topCollectionView.dataSource = self;
    _topCollectionView.backgroundColor = [UIColor whiteColor];
    
    [self addSubview:_topCollectionView];

    [self initCenterUI];
}

- (void)initCenterUI{

    V2_LovePreferenceLayout *flowLayout = [[V2_LovePreferenceLayout alloc] init];
    
    _centerCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, _topCollectionView.bottom, DeviceWidth, 308) collectionViewLayout:flowLayout];
    _centerCollectionView.delegate = self;
    _centerCollectionView.dataSource = self;
    _centerCollectionView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_centerCollectionView];
    
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    if (collectionView == _topCollectionView) {
        return self.tagsArr.count;
    }else{
        return 20;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == _topCollectionView) {
        
        selecteCityViewModel *model =self.tagsArr[indexPath.row];//[NSString stringWithFormat:@"%@",model.title]
        
//        NSString *string = self.tagsArr[indexPath.row];
        CGFloat width = [SDHelper widthForLabel:model.name fontSize:14];
        return CGSizeMake(width+10,40);
    }else{
    
//        selecteCityCenterViewModel *model =self.tagsCenterArr[indexPath.row];//[NSString stringWithFormat:@"%@",model.title]
        NSString *string = self.tagsCenterArr[indexPath.row];
        CGFloat width = [SDHelper widthForLabel:string fontSize:12];
        return CGSizeMake(width+10,30);
    }
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    if (collectionView == _topCollectionView) {
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        [collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([selecteCityViewCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:CellIdentifier];
        
        selecteCityViewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
        selecteCityViewModel *model =self.tagsArr[indexPath.row];
        
        cell.lblTitle.text = model.name;
         [collectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionNone];
        return cell;
        
    }else{
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:CellIdentifier];
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
//        selecteCityCenterViewModel *model =self.tagsCenterArr[indexPath.row];
         NSString *string = self.tagsCenterArr[indexPath.row];
        UILabel *label = [[UILabel alloc] init];
        label.text = string;//[NSString stringWithFormat:@"%@",model.title];
        label.frame = CGRectMake(0, 0, ([SDHelper widthForLabel:label.text fontSize:12] + 10), 30);
        label.font = [UIFont systemFontOfSize:12];
        label.layer.cornerRadius = 2.0;
        label.layer.masksToBounds = YES;
        label.backgroundColor = [UIColor color234Gray];
        label.textAlignment = NSTextAlignmentCenter;
        [cell.contentView addSubview:label];
        
        return cell;
    
    
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    if (collectionView == _topCollectionView) {
        selecteCityViewModel *model =self.tagsArr[indexPath.row];
        
        NSLog(@"=====index:%@",model.myID);
    }else{
//    selecteCityCenterViewModel *model =self.tagsCenterArr[indexPath.row];
    NSLog(@"------index:%ld",indexPath.item);
    }
}

- (void)setTagsArr:(NSArray *)tagsArr{

    _tagsArr = tagsArr;
    
}







@end
