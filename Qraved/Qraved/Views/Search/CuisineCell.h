//
//  CuisineCell.h
//  Qraved
//
//  Created by Jeff on 8/22/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "CheckableTableViewCell.h"
#import "IMGCuisine.h"

@interface CuisineCell : CheckableTableViewCell

//@property(nonatomic,strong) IMGCuisine *object;

@end
