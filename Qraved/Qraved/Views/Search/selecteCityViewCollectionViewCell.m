//
//  selecteCityViewCollectionViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/27.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "selecteCityViewCollectionViewCell.h"

@implementation selecteCityViewCollectionViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.backgroundColor = [UIColor whiteColor];
    
    self.selecteView.backgroundColor = [UIColor colorWithRed:222/255.0f green:32/255.0f blue:41/255.0f alpha:1.0f];
    self.selecteView.hidden = YES;
    
}

- (void)setSelected:(BOOL)selected  {
    [super setSelected:selected];
    
    self.selecteView.hidden = !selected;
    self.lblTitle.textColor  = selected ? self.selecteView.backgroundColor : [UIColor blackColor];
    
    
}

@end
