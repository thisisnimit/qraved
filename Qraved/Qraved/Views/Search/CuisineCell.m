//
//  CuisineCell.m
//  Qraved
//
//  Created by Jeff on 8/22/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "CuisineCell.h"

@implementation CuisineCell
- (void)setText:(NSString *)title {
    if(self.titleLabel==nil){
        self.titleLabel = [[SearchCellLabel alloc] initWithBlackColorAndFrame:CGRectMake(15, 0, DeviceWidth-40, self.frame.size.height)];
        [self addSubview:self.titleLabel];
    }
    [self.titleLabel setText:title];
    
}
@end
