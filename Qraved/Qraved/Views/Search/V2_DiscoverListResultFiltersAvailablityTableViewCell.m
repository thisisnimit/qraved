//
//  V2_DiscoverListResultFiltersAvailablityTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverListResultFiltersAvailablityTableViewCell.h"

@implementation V2_DiscoverListResultFiltersAvailablityTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}

- (void)initUI{

    
    _btnPromo = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnPromo.frame = CGRectMake(15, 5, 80, 30);
    [_btnPromo setTitle:@"Promo" forState:UIControlStateNormal];
    [_btnPromo setImage:[UIImage imageNamed:@"ic_local_offer"] forState:UIControlStateNormal];
    [_btnPromo setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    _btnPromo.titleLabel.font = [UIFont systemFontOfSize:12];
    [_btnPromo setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    _btnPromo.layer.cornerRadius = 5;
    _btnPromo.layer.masksToBounds = YES;
    _btnPromo.tag = 10;
    [_btnPromo addTarget:self action:@selector(btnCkick:) forControlEvents:UIControlEventTouchUpInside];
    _btnPromo.backgroundColor = [UIColor color234Gray];
    [self.contentView addSubview:_btnPromo];
    
    _btnOpenNow = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnOpenNow.frame = CGRectMake(_btnPromo.right + 10, 5, 100, 30);
    [_btnOpenNow setTitle:@"Open Now" forState:UIControlStateNormal];
    [_btnOpenNow setImage:[UIImage imageNamed:@"ic_schedule"] forState:UIControlStateNormal];
    [_btnOpenNow setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    _btnOpenNow.titleLabel.font = [UIFont systemFontOfSize:12];
    _btnOpenNow.tag = 11;
    _btnOpenNow.layer.cornerRadius = 5;
    _btnOpenNow.layer.masksToBounds = YES;
    [_btnOpenNow setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    [_btnOpenNow addTarget:self action:@selector(btnCkick:) forControlEvents:UIControlEventTouchUpInside];
    _btnOpenNow.backgroundColor = [UIColor color234Gray];
    [self.contentView addSubview:_btnOpenNow];
    
    _btnSaved = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnSaved.frame = CGRectMake(_btnOpenNow.right + 10, 5, 80, 30);
    [_btnSaved setTitle:@"Saved" forState:UIControlStateNormal];
    [_btnSaved setImage:[UIImage imageNamed:@"ic_Heart_on"] forState:UIControlStateNormal];
    [_btnSaved setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
    _btnSaved.titleLabel.font = [UIFont systemFontOfSize:12];
    _btnSaved.tag = 12;
    _btnSaved.layer.cornerRadius = 5;
    _btnSaved.layer.masksToBounds = YES;
    [_btnSaved setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    [_btnSaved addTarget:self action:@selector(btnCkick:) forControlEvents:UIControlEventTouchUpInside];
    _btnSaved.backgroundColor = [UIColor color234Gray];
    [self.contentView addSubview:_btnSaved];

}

- (void)btnCkick:(UIButton *)btnCkick{

    NSLog(@"-_+_+_+_+_+_+%ld",btnCkick.tag);
    if (btnCkick.tag == 10) {
        
        [_btnPromo setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnPromo.backgroundColor = [UIColor color222Red];
         [_btnOpenNow setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
        _btnOpenNow.backgroundColor = [UIColor color234Gray];
         [_btnSaved setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
        _btnSaved.backgroundColor = [UIColor color234Gray];
         [_btnPromo setImage:[UIImage imageNamed:@"ic_local_offer_white"] forState:UIControlStateNormal];
        [_btnOpenNow setImage:[UIImage imageNamed:@"ic_schedule"] forState:UIControlStateNormal];
         [_btnSaved setImage:[UIImage imageNamed:@"ic_Heart_on"] forState:UIControlStateNormal];
    }else if (btnCkick.tag == 11){
        
        [_btnPromo setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
        _btnPromo.backgroundColor = [UIColor color234Gray];
        [_btnOpenNow setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnOpenNow.backgroundColor = [UIColor color222Red];
        [_btnSaved setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
        _btnSaved.backgroundColor = [UIColor color234Gray];
        
        [_btnPromo setImage:[UIImage imageNamed:@"ic_local_offer"] forState:UIControlStateNormal];
        [_btnOpenNow setImage:[UIImage imageNamed:@"ic_schedule_white"] forState:UIControlStateNormal];
        [_btnSaved setImage:[UIImage imageNamed:@"ic_Heart_on"] forState:UIControlStateNormal];
    }else{
        
        [_btnPromo setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
        _btnPromo.backgroundColor = [UIColor color234Gray];
        [_btnOpenNow setTitleColor:[UIColor color333333] forState:UIControlStateNormal];
        _btnOpenNow.backgroundColor = [UIColor color234Gray];
        [_btnSaved setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _btnSaved.backgroundColor = [UIColor color222Red];
        
        [_btnPromo setImage:[UIImage imageNamed:@"ic_local_offer"] forState:UIControlStateNormal];
        [_btnOpenNow setImage:[UIImage imageNamed:@"ic_schedule"] forState:UIControlStateNormal];
        [_btnSaved setImage:[UIImage imageNamed:@"ic_heart_white"] forState:UIControlStateNormal];
        
    }
    
    if (self.Delegate) {
        [self.Delegate delegateAvailablity:btnCkick.tag];
    }
}

@end
