//
//  OfferLabel.m
//  Qraved
//
//  Created by Jeff on 8/28/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "OfferLabel.h"
#import "UIConstants.h"
#import "UILabel+Helper.h"
#import "UIColor+Helper.h"
#import "BookUtil.h"
#import "DBManager.h"
#import "IMGOperatingState.h"
#import "IMGRestaurantOffer.h"
#import "ColorUtil.h"

@implementation OfferLabel{
    NSString *offString;
}

- (id)initWithFrame:(CGRect)frame restaurant:(IMGRestaurant *)restaurant
{
    self = [self initWithFrame:frame];
    if(self){
        self.restaurant=restaurant;
    }
    return self;
}
- (id)initWithFrame:(CGRect)frame restaurant:(IMGRestaurant *)restaurant off:(NSString *)off
{
    self = [self initWithFrame:frame];
    if(self){
        offString = off;
        self.restaurant=restaurant;
    }
    return self;
}
-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.textColor=[UIColor whiteColor];
        self.backgroundColor=[UIColor clearColor];
        self.font=[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:12];
        self.numberOfLines=1;
        self.lineBreakMode=NSLineBreakByTruncatingTail;
        self.layer.borderWidth = 0;
        self.layer.cornerRadius = 5;
    }
    return self;
}


-(void)setRestaurant:(IMGRestaurant *)restaurant andBookDate:(NSDate *)bookDate andBookTime:(NSString *)bookTime andPax:(NSNumber *)pax andRestaurantOffer:(IMGRestaurantOffer *)restaurantOffer{
    _restaurant=restaurant;
    
    
    
    if([restaurant.state intValue]==2 || [restaurant.state intValue]==3||[restaurant.state intValue]==4||[restaurant.state intValue]==5){
        IMGOperatingState *operatingState = [IMGOperatingState findByStateId:restaurant.state];
        self.layer.backgroundColor = [ColorUtil getColor:operatingState.color].CGColor;
        [self setText:[[NSString alloc] initWithFormat:@"   %@",operatingState.stateName]];
        [self setTextColor:[UIColor color333333]];
        [self resizeToStretch:8];
        return;
    }
    
    [self setTextColor:[UIColor whiteColor]];
    if(offString!=nil){
        self.layer.backgroundColor = [UIColor colorC2060A].CGColor;
        [self setText:[[NSString alloc] initWithFormat:L(@" Up to %@%@"),offString,@"%"]];
        return;
    }
    
    if(restaurantOffer){
        if (!restaurantOffer.offerId)
        {
            self.layer.backgroundColor = [UIColor clearColor].CGColor;
            [self setText:@""];
            return;
        }
        
        self.layer.backgroundColor = [UIColor colorC2060A].CGColor;
        NSString *offerTitle=restaurantOffer.offerTitle;
        [self setText:[[NSString alloc] initWithFormat:@"   %@",offerTitle]];
    }else{
        if(_restaurant.bestOfferName!=nil&&![_restaurant.bestOfferName isKindOfClass:[NSNull class]]&&![_restaurant.bestOfferName isEqualToString:@""]){
            self.layer.backgroundColor = [UIColor colorC2060A].CGColor;
            NSString *offerTitle=_restaurant.bestOfferName;
            if(offerTitle==nil || offerTitle.length==0){
                self.layer.backgroundColor = [UIColor clearColor].CGColor;
                return ;
            }
            [self setText:[[NSString alloc] initWithFormat:@"   %@",offerTitle]];
        }else{
            if([restaurant.state intValue]==1){
                IMGOperatingState *operatingState = [IMGOperatingState findByStateId:restaurant.state];
                self.layer.backgroundColor = [ColorUtil getColor:operatingState.color].CGColor;
                [self setText:[[NSString alloc] initWithFormat:@"   %@",operatingState.stateName]];
                [self setTextColor:[UIColor color333333]];
                [self resizeToStretch:8];
                return;
            }else{
                self.layer.backgroundColor = [UIColor clearColor].CGColor;
                [self setText:@""];
                return ;
            }
        }
    }
    
    
    [self resizeToStretch:8];
    
}
@end
