//
//  V2_DiscoverListJournalSeeAllTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/25.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverListJournalSeeAllTableViewCell.h"

@implementation V2_DiscoverListJournalSeeAllTableViewCell
{
    UIImageView *videoImageView;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}
- (void)initUI{

    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.iconImageView.clipsToBounds = YES;
    
    videoImageView = [UIImageView new];
    videoImageView.image = [UIImage imageNamed:@"ic_journal_video.png"];
    [self.iconImageView addSubview:videoImageView];
    videoImageView.hidden = YES;
    
    self.lblTitle = [[UILabel alloc] init];
    self.lblTitle.textColor = [UIColor color333333];
    self.lblTitle.font = [UIFont systemFontOfSize:14];
    
    [self.contentView sd_addSubviews:@[self.iconImageView, self.lblTitle]];
    
    self.iconImageView.sd_layout
    .topSpaceToView(self.contentView, 15)
    .leftSpaceToView(self.contentView, 15)
    .widthIs(DeviceWidth - 30)
    .heightIs((DeviceWidth - 30) * 0.55);
    
    self.lblTitle.sd_layout
    .topSpaceToView(self.iconImageView, 15)
    .leftEqualToView(self.iconImageView)
    .widthIs(DeviceWidth - 30)
    .autoHeightRatio(0);
    
    [self.iconImageView addSubview:videoImageView];
    videoImageView.sd_layout
    .topSpaceToView(self.iconImageView, 15)
    .rightSpaceToView(self.iconImageView, 15)
    .widthIs(27)
    .heightIs(19);
}

- (void)setModel:(V2_DiscoverListResultRestaurantTableViewCellSubModel *)model{

    _model = model;
    
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:[model.img returnFullImageUrl]] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    self.lblTitle.text = model.title ? model.title : @"";
    
    if ([model.hasVideo isEqual:@1]) {
        videoImageView.hidden = NO;
    }else{
        videoImageView.hidden = YES;
    }
    
    [self setupAutoHeightWithBottomView:self.lblTitle bottomMargin:15];

}


@end
