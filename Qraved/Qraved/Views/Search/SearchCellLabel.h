//
//  SearchCellLabel.h
//  Qraved
//
//  Created by Jeff on 8/27/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchCellLabel : UILabel

- (id)initWith13BoldGreenColorAndFrame:(CGRect)frame;
- (id)initWith13BoldBlackColorAndFrame:(CGRect)frame;
- (id)initWith16BoldBlackColorAndFrame:(CGRect)frame;
- (id)initWithBlackColorAndFrame:(CGRect)frame;
- (id)initWithRedColorAndFrame:(CGRect)frame;
- (id)initWithGreenColorAndFrame:(CGRect)frame;

@end
