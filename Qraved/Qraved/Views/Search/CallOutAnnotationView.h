//
//  CallOutAnnotationVifew.h
//  IYLM
//
//  Created by Jian-Ye on 12-11-8.
//  Copyright (c) 2012年 Jian-Ye. All rights reserved.
//
#import <MapKit/MapKit.h>
#import "IMGRestaurant.h"

@interface CallOutAnnotationView : MKAnnotationView 
@property (nonatomic,retain)UIView *contentView;
- (id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier andPostNotification:(BOOL)postNotification isFromSearchPage:(BOOL)fromSearchPage;

@end
