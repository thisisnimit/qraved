//
//  SearchDiscoverCategoryCell.m
//  Qraved
//
//  Created by Jeff on 8/11/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "SearchDiscoverCategoryCell.h"


@implementation SearchDiscoverCategoryCell



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.discoverImageView = [[UIImageView alloc ]initWithFrame:CGRectMake(15, 10, 25, 25)];
        
        [self.contentView addSubview:self.discoverImageView];
        
        self.titleLabel= [[UILabel alloc] initWithFrame:CGRectMake(15, 0, DeviceWidth-50, 45)];
        
        [self.contentView addSubview:self.titleLabel];
        
        UIImageView *celllineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.frame.origin.y+self.frame.size.height-1, DeviceWidth, 1)];
        [celllineImageView setImage:[UIImage imageNamed:@"search_line"] ];
        [self.contentView addSubview:celllineImageView];

        
    }
    return self;
}

-(void)updateDiscoverName:(IMGDiscover *)discover{
    [self.titleLabel setText:discover.name];
}

@end
