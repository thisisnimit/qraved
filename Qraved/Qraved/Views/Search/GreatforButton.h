//
//  GreatforButton.h
//  Qraved
//
//  Created by Jeff on 8/22/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGGreatfor.h"
#import "IMGTag.h"

@interface GreatforButton : UIButton

@property BOOL  checked;
@property(nonatomic,strong) IMGGreatfor *greatforItem;
@property(nonatomic,retain) IMGTag *occasion;

- (void)check;

@end
