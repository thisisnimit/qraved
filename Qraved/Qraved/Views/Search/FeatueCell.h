//
//  FeatueCell.h
//  Qraved
//
//  Created by Jeff on 8/22/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "CheckableTableViewCell.h"
#import "IMGFeature.h"

@interface FeatueCell : CheckableTableViewCell

@property(nonatomic,strong) IMGFeature *object;

@end
