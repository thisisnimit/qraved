
#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"
#import "DLStarRatingControl.h"
#import "OfferLabel.h"
#import "IMGDiscover.h"


@interface RestaurantCell : UITableViewCell

@property (nonatomic,retain) IMGRestaurant * restaurant;

@property (nonatomic,retain,readonly) UILabel * titleLable;
@property (nonatomic,retain,readonly) UILabel * cuisineAndDistrictLabel;

@property (nonatomic,retain,readonly) OfferLabel * offerLabel;

@property (nonatomic,retain,readonly) UILabel * reviewCountLabel;

@property (nonatomic,retain,readonly) UIImageView * restaurantImageView;


@property (nonatomic,retain,readonly) DLStarRatingControl *ratingScoreControl;

@property (nonatomic,retain,readonly) UILabel *distanceLabel;

@property (nonatomic,retain,readonly) UIImageView *logoImageView;


-(void)setRestaurant:(IMGRestaurant *)restaurant  andBookDate:(NSDate *)bookDate andBookTime:(NSString *)bookTime andPax:(NSNumber *)pax andRestaurantOffer:(IMGRestaurantOffer *)restaurantOffer;

@end
