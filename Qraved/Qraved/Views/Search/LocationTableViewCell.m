//
//  LocationTableViewCell.m
//  Qraved
//
//  Created by lucky on 16/5/25.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "LocationTableViewCell.h"

@implementation LocationTableViewCell

- (void)setText:(NSString *)title {
    if(self.titleLabel==nil){
        self.titleLabel = [[SearchCellLabel alloc] initWithBlackColorAndFrame:CGRectMake(15, 0, DeviceWidth-40, self.frame.size.height)];
        [self addSubview:self.titleLabel];
    }
    [self.titleLabel setText:title];
    
}

@end
