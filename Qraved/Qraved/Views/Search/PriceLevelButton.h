//
//  PriceLevelButton.h
//  Qraved
//
//  Created by Jeff on 8/22/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGPriceLevel.h"

@interface PriceLevelButton : UIButton

@property(nonatomic,strong) IMGPriceLevel *priceLevel;
@property(nonatomic,strong) UILabel *dollarsLabel;
@property(nonatomic,strong) UILabel *nameLabel;

- (id)initWithFrame:(CGRect)frame priceLevel:(IMGPriceLevel *)tmpPriceLevel;
- (void)uncheck;
- (void)check;

@end
