//
//  FilterItemDistrictCellView.m
//  Qraved
//
//  Created by lucky on 16/5/24.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "FilterItemDistrictCellView.h"
#import "IMGDistrict.h"
@implementation FilterItemDistrictCellView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(NSString *)titleFromDiscover:(IMGDiscover *)tmpDiscover{
    if(tmpDiscover==nil||(tmpDiscover.searchDistrictArray==nil || tmpDiscover.searchDistrictArray.count==0)){
        return @"";
    }
    NSMutableArray *titleArray = [[NSMutableArray alloc]initWithCapacity:0];
    for(IMGDistrict *district in tmpDiscover.searchDistrictArray){
        [titleArray addObject:district.name];
    }
    if(titleArray.count==0){
        return @"";
    }
    return [[NSString alloc] groupConcat:titleArray];

}

@end
