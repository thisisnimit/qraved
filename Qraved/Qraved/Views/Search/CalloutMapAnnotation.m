#import "CalloutMapAnnotation.h"
#import "IMGRestaurant.h"

@interface CalloutMapAnnotation()


@end

@implementation CalloutMapAnnotation

@synthesize latitude = _latitude;
@synthesize longitude = _longitude;

- (id)initWithLatitude:(CLLocationDegrees)latitude
		  andLongitude:(CLLocationDegrees)longitude andRestaurant:(IMGRestaurant *)paramRestaurant; {
    self=[self initWithLatitude:latitude andLongitude:longitude];
    self.restaurant=paramRestaurant;
    return self;
}

-(id)initWithLatitude:(CLLocationDegrees)latitude
         andLongitude:(CLLocationDegrees)longitude
{
    self=[super init];
    if(self)
    {
        self.latitude=latitude;
        self.longitude=longitude;
    }
    return self;
}


- (CLLocationCoordinate2D)coordinate {
	CLLocationCoordinate2D coordinate;
	coordinate.latitude = self.latitude;
	coordinate.longitude = self.longitude;
	return coordinate;
}

@end
