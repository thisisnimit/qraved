//
//  V2_DiscoverListResultFiltersRatingTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverListResultFiltersRatingTableViewCell.h"

@implementation V2_DiscoverListResultFiltersRatingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
//        self.tagsArr = @[@"All", @"Above 3 Stars", @"Above 4 Stars", @"Above 5 Stars"];
        [self initUI];
    }
    return self;
}
//- (void)initUI{
//    
//    TTGTextTagCollectionView *TagCollectionView = [[TTGTextTagCollectionView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 35)];
//    TagCollectionView.delegate = self;
//    TagCollectionView.defaultConfig.tagTextFont = [UIFont systemFontOfSize:12];
//    TagCollectionView.scrollDirection = TTGTagCollectionScrollDirectionHorizontal;
//    TagCollectionView.alignment = TTGTagCollectionAlignmentCenter;
//    TagCollectionView.numberOfLines = 1;
//    TagCollectionView.defaultConfig.tagBackgroundColor = [UIColor color234Gray];
//    TagCollectionView.defaultConfig.tagSelectedBackgroundColor = [UIColor color222Red];
//    TagCollectionView.defaultConfig.tagTextColor = [UIColor color333333];
//    TagCollectionView.defaultConfig.tagBorderWidth = 0;
//    TagCollectionView.defaultConfig.tagBorderColor = [UIColor clearColor];
//    TagCollectionView.contentInset = UIEdgeInsetsMake(5, 15, 5, 5);
//    TagCollectionView.defaultConfig.tagShadowOffset = CGSizeMake(0, 0);
//    TagCollectionView.defaultConfig.tagShadowRadius = 0;
//    TagCollectionView.defaultConfig.tagShadowOpacity = 0;
//    [TagCollectionView addTags:self.tagsArr];
//    [self.contentView addSubview:TagCollectionView];
//    
//}
//- (void)textTagCollectionView:(TTGTextTagCollectionView *)textTagCollectionView didTapTag:(NSString *)tagText atIndex:(NSUInteger)index selected:(BOOL)selected{
//    
//    NSLog(@"%@===%ld", tagText, index);
//}

- (void)initUI{

    _lblAll = [UILabel new];
    _lblAll.text = @"All";
    _lblAll.textColor = [UIColor color333333];
    _lblAll.font = [UIFont systemFontOfSize:12];
    _lblAll.backgroundColor = [UIColor color234Gray];
    _lblAll.layer.cornerRadius = 5;
    _lblAll.layer.masksToBounds = YES;
    _lblAll.tag = 20;
    _lblAll.userInteractionEnabled = YES;
    _lblAll.textAlignment = NSTextAlignmentCenter;
    
    _lblAbove3Stars = [UILabel new];
    _lblAbove3Stars.text = @"Above 3 Stars";
    _lblAbove3Stars.textColor = [UIColor color333333];
    _lblAbove3Stars.font = [UIFont systemFontOfSize:12];
    _lblAbove3Stars.backgroundColor = [UIColor color234Gray];
    _lblAbove3Stars.layer.cornerRadius = 5;
    _lblAbove3Stars.layer.masksToBounds = YES;
    _lblAbove3Stars.tag = 21;
    _lblAbove3Stars.userInteractionEnabled = YES;
    _lblAbove3Stars.textAlignment = NSTextAlignmentCenter;
    
    _lblAbove4Stars = [UILabel new];
    _lblAbove4Stars.text = @"Above 4 Stars";
    _lblAbove4Stars.textColor = [UIColor color333333];
    _lblAbove4Stars.font = [UIFont systemFontOfSize:12];
    _lblAbove4Stars.backgroundColor = [UIColor color234Gray];
    _lblAbove4Stars.layer.cornerRadius = 5;
    _lblAbove4Stars.layer.masksToBounds = YES;
    _lblAbove4Stars.tag = 22;
    _lblAbove4Stars.userInteractionEnabled = YES;
    _lblAbove4Stars.textAlignment = NSTextAlignmentCenter;
    
    _lblAbove5Stars = [UILabel new];
    _lblAbove5Stars.text = @"Above 5 Stars";
    _lblAbove5Stars.textColor = [UIColor color333333];
    _lblAbove5Stars.font = [UIFont systemFontOfSize:12];
    _lblAbove5Stars.backgroundColor = [UIColor color234Gray];
    _lblAbove5Stars.layer.cornerRadius = 5;
    _lblAbove5Stars.layer.masksToBounds = YES;
    _lblAbove5Stars.tag = 23;
    _lblAbove5Stars.userInteractionEnabled = YES;
    _lblAbove5Stars.textAlignment = NSTextAlignmentCenter;
    
    [self.contentView sd_addSubviews:@[_lblAll, _lblAbove3Stars, _lblAbove4Stars, _lblAbove5Stars]];
    
    UITapGestureRecognizer *tapOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
    [_lblAll addGestureRecognizer:tapOne];
    
    UITapGestureRecognizer *tapTwo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
    [_lblAbove3Stars addGestureRecognizer:tapTwo];
    
    UITapGestureRecognizer *tapThree = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
    [_lblAbove4Stars addGestureRecognizer:tapThree];
    
    UITapGestureRecognizer *tapFour = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
    [_lblAbove5Stars addGestureRecognizer:tapFour];
    
    _lblAll.sd_layout
    .topSpaceToView(self.contentView, 5)
    .leftSpaceToView(self.contentView, 15)
    .heightIs(30)
    .widthIs(30);
    
    _lblAbove3Stars.sd_layout
    .topEqualToView(_lblAll)
    .leftSpaceToView(_lblAll, 5)
    .heightIs(30)
    .widthIs(90);
    
    _lblAbove4Stars.sd_layout
    .topEqualToView(_lblAll)
    .leftSpaceToView(_lblAbove3Stars, 5)
    .heightIs(30)
    .widthIs(90);
    
    _lblAbove5Stars.sd_layout
    .topEqualToView(_lblAll)
    .leftSpaceToView(_lblAbove4Stars, 5)
    .heightIs(30)
    .widthIs(90);
    
}

- (void)tapClick:(UITapGestureRecognizer *)tapClick{

    NSLog(@"ratings=======%ld",tapClick.view.tag);

    if (tapClick.view.tag == 20) {
        _lblAll.textColor = [UIColor whiteColor];
        _lblAll.backgroundColor = [UIColor color222Red];
        _lblAbove3Stars.textColor = [UIColor color333333];
        _lblAbove3Stars.backgroundColor = [UIColor color234Gray];
        _lblAbove4Stars.textColor = [UIColor color333333];
        _lblAbove4Stars.backgroundColor = [UIColor color234Gray];
        _lblAbove5Stars.textColor = [UIColor color333333];
        _lblAbove5Stars.backgroundColor = [UIColor color234Gray];
    }else if (tapClick.view.tag == 21){
    
        _lblAll.textColor = [UIColor color333333];
        _lblAll.backgroundColor = [UIColor color234Gray];
        _lblAbove3Stars.textColor = [UIColor whiteColor];
        _lblAbove3Stars.backgroundColor = [UIColor color222Red];
        _lblAbove4Stars.textColor = [UIColor color333333];
        _lblAbove4Stars.backgroundColor = [UIColor color234Gray];
        _lblAbove5Stars.textColor = [UIColor color333333];
        _lblAbove5Stars.backgroundColor = [UIColor color234Gray];
    }else if (tapClick.view.tag == 22){
    
        _lblAll.textColor = [UIColor color333333];
        _lblAll.backgroundColor = [UIColor color234Gray];
        _lblAbove3Stars.textColor = [UIColor color333333];
        _lblAbove3Stars.backgroundColor = [UIColor color234Gray];
        _lblAbove4Stars.textColor = [UIColor whiteColor];
        _lblAbove4Stars.backgroundColor = [UIColor color222Red];
        _lblAbove5Stars.textColor = [UIColor color333333];
        _lblAbove5Stars.backgroundColor = [UIColor color234Gray];
    }else{
    
        _lblAll.textColor = [UIColor color333333];
        _lblAll.backgroundColor = [UIColor color234Gray];
        _lblAbove3Stars.textColor = [UIColor color333333];
        _lblAbove3Stars.backgroundColor = [UIColor color234Gray];
        _lblAbove4Stars.textColor = [UIColor color333333];
        _lblAbove4Stars.backgroundColor = [UIColor color234Gray];
        _lblAbove5Stars.textColor = [UIColor whiteColor];
        _lblAbove5Stars.backgroundColor = [UIColor color222Red];
    
    }
    if (self.Delegate) {
        [self.Delegate delegateRating:tapClick.view.tag];
    }
    
}


@end
