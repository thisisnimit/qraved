//
//  V2_DiscoverListResultFiltersOhtersTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGTagUpdate.h"

@interface V2_DiscoverListResultFiltersOhtersTableViewCell : UITableViewCell

@property (nonatomic, strong)  UILabel  *lblTitle;

@property (nonatomic, strong)  UILabel  *lblDetail;

@property (nonatomic, strong)  UIImageView  *arrowImageView;

@property (nonatomic, strong)  IMGTagUpdate *model;
@end
