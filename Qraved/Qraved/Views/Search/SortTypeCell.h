//
//  SortTypeCell.h
//  Qraved
//
//  Created by Jeff on 8/18/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGSortType.h"
#import "CheckableTableViewCell.h"

@interface SortTypeCell : CheckableTableViewCell


@property(nonatomic,strong) IMGSortType *object;


@end
