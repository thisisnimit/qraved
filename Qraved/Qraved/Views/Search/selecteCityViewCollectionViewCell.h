//
//  selecteCityViewCollectionViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/27.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface selecteCityViewCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UIView *selecteView;


@end
