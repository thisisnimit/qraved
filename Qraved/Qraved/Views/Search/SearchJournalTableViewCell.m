//
//  SearchJournalTableViewCell.m
//  Qraved
//
//  Created by Lucky on 15/10/13.
//  Copyright © 2015年 Imaginato. All rights reserved.
//

#import "SearchJournalTableViewCell.h"

@implementation SearchJournalTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIImageView *celllineImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.frame.origin.y+self.frame.size.height-1, DeviceWidth, 1)];
        [celllineImageView setImage:[UIImage imageNamed:@"search_line"] ];
        [self.contentView addSubview:celllineImageView];
        
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
