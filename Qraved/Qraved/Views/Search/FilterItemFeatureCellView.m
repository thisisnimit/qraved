//
//  FilterItemFeaturesCellView.m
//  Qraved
//
//  Created by Jeff on 8/21/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "FilterItemFeatureCellView.h"
#import "NSString+Helper.h"

@implementation FilterItemFeatureCellView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(NSString *)titleFromDiscover:(IMGDiscover *)tmpDiscover{
    if(tmpDiscover==nil||(tmpDiscover.tagsArr==nil || tmpDiscover.tagsArr.count==0)){
        return @"";
    }
    NSMutableArray *titleArray = [[NSMutableArray alloc]initWithCapacity:0];
    for(IMGTag *tmpTag in tmpDiscover.tagsArr){
        if([tmpTag.type isEqualToString:TAG_TYPE_FEATURES]){
            [titleArray addObject:tmpTag.name];
        }
    }
    if(titleArray.count==0){
        return @"";
    }
    return [[NSString alloc] groupConcat:titleArray];
}

@end
