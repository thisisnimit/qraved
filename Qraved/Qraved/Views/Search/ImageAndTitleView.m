//
//  ImageAndTitleView.m
//  Qraved
//
//  Created by josn.liu on 2017/1/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "ImageAndTitleView.h"
#import "UIView+Helper.h"
#import "UIConstants.h"
#import "UIImageView+WebCache.h"
#import "UIImage+Resize.h"
#import "NSString+Helper.h"
#import "DLStarRatingControl.h"
@implementation ImageAndTitleView
-(instancetype)initWithFrame:(CGRect)frame withImageUrl:(NSString*)url withTitle:(NSString*)title andValue:(NSNumber*)value{

    if ([super initWithFrame:frame]) {
       
        UIImageView *imageview=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.width*0.68)];
        imageview.layer.cornerRadius=6;
        imageview.clipsToBounds=YES;
        UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(imageview.frame.size.width,imageview.frame.size.height)];
        __weak typeof(imageview) weakImageView = imageview;
        [imageview sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",QRAVED_WEB_JAKARTASERVER,url]] placeholderImage:placeHoderImage options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (!image||error) {
                [imageview sd_setImageWithURL:[NSURL URLWithString:[url returnFullImageUrlWithWidth:imageview.frame.size.width andHeight:imageview.frame.size.height]] placeholderImage:placeHoderImage options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    
                        [UIView animateWithDuration:1.0 animations:^{
                            [weakImageView setAlpha:1.0];
                        }];
                        image = [image imageByScalingAndCroppingForSize:CGSizeMake(imageview.frame.size.width,imageview.frame.size.height)];
                        [weakImageView setImage:image];
                }];

                
            }else{
            
                [UIView animateWithDuration:1.0 animations:^{
                    [weakImageView setAlpha:1.0];
                }];
                image = [image imageByScalingAndCroppingForSize:CGSizeMake(imageview.frame.size.width,imageview.frame.size.height)];
                [weakImageView setImage:image];

            
            }
            
        }];
        [self addSubview:imageview];

        UILabel *name=[[UILabel alloc]initWithFrame:CGRectMake(imageview.startPointX, imageview.endPointY+5, imageview.frame.size.width, 18)];
        name.textAlignment=NSTextAlignmentLeft;
        name.numberOfLines=2;
        name.font=[UIFont fontWithName:DEFAULT_FONT_NAME size:16];
        name.lineBreakMode=NSLineBreakByWordWrapping;
        name.text=[title removeHTML];
        UIFont *font=[UIFont fontWithName:DEFAULT_FONT_NAME size:16];
        CGSize size=[title sizeWithFont:font constrainedToSize:CGSizeMake(imageview.frame.size.width,font.lineHeight*2) lineBreakMode:NSLineBreakByTruncatingTail];
//        [name sizeToFit];
        name.frame=CGRectMake(imageview.startPointX, imageview.endPointY+3, imageview.frame.size.width, size.height);
        [self addSubview:name];
        UILabel *valueLable=[[UILabel alloc]initWithFrame:CGRectMake(imageview.startPointX, name.endPointY+5,  imageview.frame.size.width, 20)];
        valueLable.textAlignment=NSTextAlignmentLeft;
        if (value==nil||[value isKindOfClass:[NSNull class]]) {
            value=@0;
        }
        valueLable.text=[NSString stringWithFormat:@"(%@)",value];
        valueLable.textColor=[UIColor grayColor];
        [self addSubview:valueLable];
        self.viewHeight=valueLable.endPointY;
    }

    return self;
}
-(instancetype)initWithFrame:(CGRect)frame withRestaurant:(IMGRestaurant *)restaurant{
    if ([super initWithFrame:frame]) {
        
        UIImageView *imageview=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.width*0.56)];
        imageview.contentMode = UIViewContentModeScaleAspectFill;
        imageview.clipsToBounds = YES;
        UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(imageview.frame.size.width,imageview.frame.size.height)];
        NSString *urlSting = [restaurant.imageUrl ImgURLStringWide:imageview.frame.size.width High:imageview.frame.size.height];
        __weak typeof(imageview) weakImageView = imageview;
        [imageview sd_setImageWithURL:[NSURL URLWithString:urlSting] placeholderImage:placeHoderImage options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if (!image||error) {
                [UIView animateWithDuration:1.0 animations:^{
                    [weakImageView setAlpha:1.0];
                }];
                
                [weakImageView setImage:placeHoderImage];  
            }else{
                
                [UIView animateWithDuration:1.0 animations:^{
                    [weakImageView setAlpha:1.0];
                }];
                image = [image imageByScalingAndCroppingForSize:CGSizeMake(imageview.frame.size.width,imageview.frame.size.height)];
                [weakImageView setImage:image];
                
                
            }
            
        }];
        [self addSubview:imageview];
        
        UILabel *name=[[UILabel alloc]initWithFrame:CGRectMake(imageview.startPointX+8, imageview.endPointY+8, imageview.frame.size.width-16, 16)];
        name.textAlignment=NSTextAlignmentLeft;
        //name.numberOfLines=2;
        name.font=[UIFont systemFontOfSize:FONT_SIZE_14];
        name.lineBreakMode=NSLineBreakByTruncatingTail;
        name.text=[restaurant.title removeHTML];
        name.textColor = [UIColor color333333];
        //UIFont *font=[UIFont systemFontOfSize:FONT_SIZE_14];
//        CGSize size=[restaurant.title sizeWithFont:font constrainedToSize:CGSizeMake(imageview.frame.size.width,font.lineHeight*2) lineBreakMode:NSLineBreakByTruncatingTail];
        //        [name sizeToFit];
//        name.frame=CGRectMake(imageview.startPointX+8, imageview.endPointY+8, imageview.frame.size.width, 16);
        [self addSubview:name];
        
        DLStarRatingControl *star = [[DLStarRatingControl alloc] initWithFrame:CGRectMake(imageview.startPointX+8, name.endPointY+5, 73, 14) andStars:5 andStarWidth:13 isFractional:YES spaceWidth:2];
        star.userInteractionEnabled = NO;
        float score = [restaurant.ratingScore floatValue];
        
        int scoreInt =(int)roundf(score);
        [star updateRating:[NSNumber numberWithFloat:scoreInt]];
        [self addSubview:star];
        
        UILabel *valueLable=[[UILabel alloc]initWithFrame:CGRectMake(star.endPointX, name.endPointY+5,  imageview.frame.size.width, 14)];
        valueLable.textAlignment=NSTextAlignmentLeft;
        
        if (restaurant.reviewCount==nil||[restaurant.reviewCount isKindOfClass:[NSNull class]]) {
            restaurant.reviewCount=@0;
        }
        valueLable.font = [UIFont systemFontOfSize:FONT_SIZE_12];
        valueLable.text=[NSString stringWithFormat:@" (%@)",restaurant.reviewCount];
        valueLable.textColor=[UIColor color999999];
        [self addSubview:valueLable];
        
        UILabel *cuisineLabel = [[UILabel alloc] initWithFrame:CGRectMake(imageview.startPointX+8, valueLable.endPointY+5,  self.frame.size.width-10, 14)];
        if ([restaurant.cuisineName isEqualToString:@""]) {
            cuisineLabel.text = [NSString stringWithFormat:@"%@",restaurant.districtName];
        }else{
            cuisineLabel.text = [NSString stringWithFormat:@"%@ • %@",restaurant.cuisineName,restaurant.districtName];
        }
        
        cuisineLabel.font = [UIFont systemFontOfSize:FONT_SIZE_12];
        cuisineLabel.textColor = [UIColor color999999];
        [self addSubview:cuisineLabel];
        
        self.viewHeight=cuisineLabel.endPointY+15;
        
    }
    return self;
}
@end
