//
//  SearchPageResturantCell.m
//  Qraved
//
//  Created by imaginato on 16/5/17.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "SearchPageResturantCell.h"
#import "IMGRestaurant.h"
#import "UIConstants.h"
#import "DLStarRatingControl.h"
#import "UILabel+Helper.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"
 
#import "UIImageView+WebCache.h"
#import "IMGUser.h"
#import "TrackHandler.h"
#import "SendCall.h"
#import "Amplitude.h"
#import "ReviewViewController.h"
#import "AppDelegate.h"
#import "UIColor+Hex.h"
#define LEFT_X 18

#define TITLE_LABEL_Y 13
#define TITLE_LABEL_HEIGHT 20

#define GAP_BETWEEN_TITLE_AND_CUISINE 1

#define CUISINE_DISTRICT_LABEL_HEIGHT 20

#define GAP_BETWEEN_CUISINE_AND_REVIEW 3

#define CELL_RATING_STAR_WIDTH 13

#define RESTAURANT_IMAGE_SIZE 86
@interface SearchPageResturantCell ()<UIAlertViewDelegate>
@property (nonatomic,retain) UILabel * priceLevelLabel;
@property (nonatomic,retain) UILabel * reviewLabel;
@property (nonatomic,retain) UIImageView *celllineImageView;
@end

@implementation SearchPageResturantCell{

    UILabel* districtLabel;
    UILabel* distrctDescription;
    UIButton* callBtn;
    UIButton* menuBtn;
    UIButton* bookBtn;
    CGFloat offerHeight;
    UILabel* openTime;
    UIView* line;
    int nextOpenCurenttime;
    int nextCloseCurenttime;
    int secondOpenCurenttime;
    int secondCloseCurenttime;
    UIImageView *locationImageView;
    UILabel *adLableview;
    UILabel *ratingLabel;
}
@synthesize titleLable;
@synthesize cuisineAndDistrictLabel;
@synthesize priceLevelLabel;
@synthesize offerLabel;
@synthesize celllineImageView;

@synthesize reviewLabel;
@synthesize reviewCountLabel;
@synthesize restaurantImageView;
@synthesize ratingScoreControl;
@synthesize distanceLabel;

@synthesize logoImageView;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        titleLable = [[UILabel alloc]initWithFrame:CGRectMake(LEFT_X, TITLE_LABEL_Y, DeviceWidth-2*LEFT_X-RESTAURANT_IMAGE_SIZE, TITLE_LABEL_HEIGHT)];
        titleLable.font=[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:18];
        titleLable.textColor=[UIColor colorWithRed:196/255.0 green:0 blue:0 alpha:1];
        titleLable.numberOfLines=2;
        titleLable.tag=1000;
        titleLable.backgroundColor=[UIColor clearColor];
        UITapGestureRecognizer *resNameTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(restaurantImageTap:)];
        titleLable.userInteractionEnabled=YES;
        [titleLable addGestureRecognizer:resNameTap];
        [self addSubview:titleLable];
        adLableview=[[UILabel alloc]initWithFrame:CGRectMake(0,2, 33,20)];
        adLableview.layer.cornerRadius=6;
        adLableview.textAlignment=NSTextAlignmentCenter;
        adLableview.clipsToBounds=YES;
        adLableview.text=@"Ad";
        adLableview.font=[UIFont boldSystemFontOfSize:16];
        adLableview.textColor=[UIColor whiteColor];
        adLableview.backgroundColor=[UIColor colorWithRed:196/255.0 green:0 blue:0 alpha:1];;
        adLableview.hidden=YES;
        [titleLable addSubview:adLableview];
        cuisineAndDistrictLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_X, titleLable.endPointY, DeviceWidth-LEFT_X*2-RESTAURANT_IMAGE_SIZE, CUISINE_DISTRICT_LABEL_HEIGHT)];
        cuisineAndDistrictLabel.numberOfLines=2;
        cuisineAndDistrictLabel.font=[UIFont fontWithName:DEFAULT_FONT_NAME size:15];
        cuisineAndDistrictLabel.lineBreakMode=NSLineBreakByTruncatingTail;
        UITapGestureRecognizer *cuisineTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(restaurantImageTap:)];
        cuisineAndDistrictLabel.tag=1001;
        cuisineAndDistrictLabel.userInteractionEnabled=YES;
        [cuisineAndDistrictLabel addGestureRecognizer:cuisineTap];

        [self addSubview:cuisineAndDistrictLabel];
        
        ratingScoreControl=[[DLStarRatingControl alloc]initWithFrame:CGRectMake(LEFT_X, cuisineAndDistrictLabel.endPointY+5, 84, 18) andStars:5 andStarWidth:CELL_RATING_STAR_WIDTH isFractional:YES spaceWidth:5];
        ratingScoreControl.userInteractionEnabled=NO;
        [self addSubview:ratingScoreControl];
        ratingLabel = [[UILabel alloc] init];
        [ratingLabel setTextAlignment:NSTextAlignmentLeft];
        ratingLabel.font=[UIFont boldSystemFontOfSize:10];
        ratingLabel.textColor= [UIColor colorWithRed:253/255.0 green:181/255.0 blue:10/255.0 alpha:1];
        [self addSubview:ratingLabel];
//        ratingScoreControl = [[StarsView alloc] initWithStarSize:CGSizeMake(CELL_RATING_STAR_WIDTH, CELL_RATING_STAR_WIDTH) space:5 numberOfStar:5];
//        ratingScoreControl.frame = CGRectMake(LEFT_X, cuisineAndDistrictLabel.endPointY+5, 84, 18);
//        ratingScoreControl.selectable = NO;
//         [self addSubview:ratingScoreControl];
        
        reviewCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_X+ratingScoreControl.frame.size.width+2, cuisineAndDistrictLabel.endPointY+2, 25,20)];
        reviewCountLabel.font=[UIFont boldSystemFontOfSize:10];
        [reviewCountLabel setTextColor:[UIColor colorWithRed:71/255.0 green:124/255.0 blue:230/255.0 alpha:1]];
        [reviewCountLabel setBackgroundColor:[UIColor clearColor]];
//        [self addSubview:reviewCountLabel];
        
        reviewLabel = [[UILabel alloc] initWithFrame:CGRectMake(LEFT_X+ratingScoreControl.frame.size.width+2, cuisineAndDistrictLabel.endPointY+3, 90,20)];
        //reviewLabel.text=@"Review";
        reviewLabel.font=[UIFont fontWithName:DEFAULT_FONT_NAME size:10];
        reviewLabel.textColor=[UIColor colorWithRed:71/255.0 green:124/255.0 blue:230/255.0 alpha:1];
        reviewLabel.userInteractionEnabled=YES;
        UITapGestureRecognizer* tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(reviewTapGesture:)];
        tap.numberOfTapsRequired=1;
        [reviewLabel addGestureRecognizer:tap];
        [self addSubview:reviewLabel];
        
        line=[[UIView alloc]initWithFrame:CGRectMake(reviewCountLabel.startPointX+11, reviewLabel.endPointY+2, 63, 1)];
        line.backgroundColor=[UIColor colorWithRed:71/255.0 green:124/255.0 blue:230/255.0 alpha:1];
//        [self addSubview:line];
        offerLabel=[[OfferLabel alloc]initWithFrame:CGRectMake(LEFT_X, ratingScoreControl.endPointY, 180, 25)];
         offerLabel.textColor=[UIColor colorWithRed:196/255.0 green:0 blue:0 alpha:1];
        offerLabel.font=[UIFont fontWithName:DEFAULT_FONT_NAME size:14];
        [self addSubview:offerLabel];
        
        openTime=[[UILabel alloc]initWithFrame:CGRectMake(DeviceWidth-94, 12, 95, 22)];
        openTime.textAlignment=NSTextAlignmentRight;
        openTime.font=[UIFont boldSystemFontOfSize:11];
        openTime.textColor=[UIColor grayColor];
        openTime.numberOfLines = 0;
        [self addSubview:openTime];
        restaurantImageView = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-RESTAURANT_IMAGE_SIZE-15, openTime.endPointY+10, RESTAURANT_IMAGE_SIZE, RESTAURANT_IMAGE_SIZE)];
        restaurantImageView.userInteractionEnabled=YES;
        restaurantImageView.tag=1002;
        [restaurantImageView setContentMode:UIViewContentModeScaleAspectFill];
        UITapGestureRecognizer* reviewTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(restaurantImageTap:)];
        [restaurantImageView addGestureRecognizer:reviewTap];
        [restaurantImageView.layer setMasksToBounds:YES];
        restaurantImageView.layer.borderColor = [UIColor whiteColor].CGColor;
        restaurantImageView.layer.cornerRadius = 5;
        restaurantImageView.layer.borderWidth = 0.3;
        [restaurantImageView setAlpha:0.0];
        [self addSubview:restaurantImageView];
        
        districtLabel=[[UILabel alloc]initWithFrame:CGRectMake(LEFT_X, ratingScoreControl.endPointY+offerLabel.frame.size.height,180, 20)];
        districtLabel.font=[UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:15];
        //districtLabel有点击事件Filter过滤的，临时关了
//        districtLabel.userInteractionEnabled=YES;
//        UITapGestureRecognizer* districtTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(distrctTapAct:)];
//        [districtLabel addGestureRecognizer:districtTap];
        
        UITapGestureRecognizer *distrctTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(restaurantImageTap:)];
        districtLabel.tag=1004;
        districtLabel.userInteractionEnabled=YES;
        [districtLabel addGestureRecognizer:distrctTap];

        [self addSubview:districtLabel];
        distrctDescription=[[UILabel alloc]initWithFrame:CGRectMake(LEFT_X, districtLabel.endPointY, DeviceWidth-LEFT_X, 20)];
        distrctDescription.lineBreakMode=NSLineBreakByTruncatingTail;
        distrctDescription.numberOfLines=2;
        distrctDescription.font=[UIFont fontWithName:DEFAULT_FONT_NAME size:15];
        UITapGestureRecognizer *distrctDescriptionTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(restaurantImageTap:)];
        distrctDescription.tag=1003;
        distrctDescription.userInteractionEnabled=YES;
        [cuisineAndDistrictLabel addGestureRecognizer:distrctDescriptionTap];

        [self addSubview:distrctDescription];
        
        
        logoImageView = [[UIImageView alloc] init];
        logoImageView.frame = CGRectMake(restaurantImageView.frame.size.width-30, 0, 30, 30);
        logoImageView.backgroundColor = [UIColor clearColor];
        [restaurantImageView addSubview:logoImageView];
        
        distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth-17-RESTAURANT_IMAGE_SIZE/2+4, restaurantImageView.endPointY+8, 48, 20)];
        [distanceLabel setFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12]];
//        distanceLabel.font=[UIFont boldSystemFontOfSize:10];
        [distanceLabel setBackgroundColor:[UIColor clearColor]];
        [self addSubview:distanceLabel];
        locationImageView = [[UIImageView alloc] initWithFrame:CGRectMake(distanceLabel.startPointX-15, distanceLabel.startPointY+5, 10, 10)];
        locationImageView.contentMode = UIViewContentModeScaleAspectFit;
        [locationImageView setImage:[UIImage imageNamed:@"searchLocation_map.png"]];
        [self addSubview:locationImageView];
        
        celllineImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        celllineImageView.backgroundColor=[UIColor colorWithRed:242/255.0 green:242/255.0 blue:248/255.0 alpha:1];
        [self addSubview:celllineImageView];
        
    }
    return self;
}
-(void)btnAct:(UIButton*)btn
{
    switch (btn.tag) {
        case 100:
            NSLog(@"call btn  act  ");
        {
            IMGUser *user = [IMGUser currentUser];
            NSNumber *userId = [[NSNumber alloc] init];
            if (user.userId != nil)
            {
                userId = user.userId;
            }
            else
                userId = [NSNumber numberWithInt:0];
            
            [TrackHandler trackWithUserId:userId andRestaurantId:_restaurant.restaurantId andContent:[NSDictionary dictionaryWithObject:@"0" forKey:@"type"]];
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"" message:_restaurant.phoneNumber delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
            alertView.tag=1;
            [alertView show];
        }
            
            
            break;
        case 101:
        {
            NSMutableDictionary *eventProperties=[[NSMutableDictionary alloc]init];
            [eventProperties setValue:[NSNumber numberWithInteger:self.index+1] forKey:@"position"];
            [eventProperties setValue:[NSNumber numberWithBool:_restaurant.isAd]forKey:@"sponsored"];
            [eventProperties setValue:[NSNumber numberWithInteger:_restaurant.adAllNums]forKey:@"numberOfAds"];
            [eventProperties setValue:@"Menu" forKey:@"areaClicked"];
            [[Amplitude instance] logEvent:@"CL - SRP CTR tracking" withEventProperties:eventProperties];

            [self.searchPageCellDelegate menuBtnTap:_restaurant];
        }
            break;
        case 102:
        {
            NSMutableDictionary *eventProperties=[[NSMutableDictionary alloc]init];
            [eventProperties setValue:[NSNumber numberWithInteger:self.index+1] forKey:@"position"];
            [eventProperties setValue:[NSNumber numberWithBool:_restaurant.isAd]forKey:@"sponsored"];
            [eventProperties setValue:[NSNumber numberWithInteger:_restaurant.adAllNums]forKey:@"numberOfAds"];
            [eventProperties setValue:@"Book Now" forKey:@"areaClicked"];
            [[Amplitude instance] logEvent:@"CL - SRP CTR tracking" withEventProperties:eventProperties];

            [self.searchPageCellDelegate bookNowTap:_restaurant];
        }
            break;
        default:
            break;
    }





}
-(void)distrctTapAct:(UITapGestureRecognizer*)tap
{
    NSLog(@" distrct tap gesture");
    [self.searchPageCellDelegate searchDistrictTap:_restaurant];
}
-(void)reviewTapGesture:(UITapGestureRecognizer*)tap

{

    NSMutableDictionary *eventProperties=[[NSMutableDictionary alloc]init];
    [eventProperties setValue:[NSNumber numberWithInteger:self.index+1] forKey:@"position"];
    [eventProperties setValue:[NSNumber numberWithBool:_restaurant.isAd]forKey:@"sponsored"];
    [eventProperties setValue:[NSNumber numberWithInteger:_restaurant.adAllNums]forKey:@"numberOfAds"];
    [eventProperties setValue:@"numberReviews" forKey:@"areaClicked"];
    [[Amplitude instance] logEvent:@"CL - SRP CTR tracking" withEventProperties:eventProperties];

    if (self.searchPageCellDelegate) {
        [self.searchPageCellDelegate reviewTap:_restaurant];
    }

}
-(void)restaurantImageTap:(UITapGestureRecognizer*)tap
{
    
    UIView *view=tap.view;
    NSString *str =@"";
    switch (view.tag) {
        case 1000:
        {
        str=@"Resto Name";
        }
            break;
        case 1001:
        {
            str=@"Cuisine";
 
        }
            break;
        case 1002:
        {
            str=@"Picture";

        }
            break;

        case 1003:
        {
            str=@"District";
            
        }
            break;
        case 1004:
        {
            str=@"District";
            
        }

     
        default:
            break;
    }
    NSMutableDictionary *eventProperties=[[NSMutableDictionary alloc]init];
    [eventProperties setValue:[NSNumber numberWithInteger:self.index+1] forKey:@"position"];
    [eventProperties setValue:[NSNumber numberWithBool:_restaurant.isAd]forKey:@"sponsored"];
    [eventProperties setValue:[NSNumber numberWithInteger:_restaurant.adAllNums]forKey:@"numberOfAds"];
    

    [eventProperties setValue:str forKey:@"areaClicked"];
    [[Amplitude instance] logEvent:@"CL - SRP CTR tracking" withEventProperties:eventProperties];
    
     if (self.searchPageCellDelegate) {
        [self.searchPageCellDelegate restaurantImageTap:_restaurant];
    }
    NSLog(@" restaurant tap gesture ====%ld",(long)self.index);

}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{

    if(buttonIndex==0){
        [alertView dismissWithClickedButtonIndex:0 animated:NO];
    }else if (buttonIndex==1){
        NSMutableDictionary* eventDic=[[NSMutableDictionary alloc]init];
        [eventDic setValue:_restaurant.restaurantId forKey:@"Restaurant_ID"];
        [eventDic setValue:@"Restaurant search result page" forKey:@"Location"];
        [[Amplitude instance] logEvent:@"CL - Phone Number" withEventProperties:eventDic];
        [[Amplitude instance] logEvent:@"CL - Call Restaurant CTA" withEventProperties:eventDic];
        [[AppsFlyerTracker sharedTracker] trackEvent:@"CL - Call Restaurant CTA" withValues:eventDic];
        
        [SendCall sendCall:_restaurant.phoneNumber];
    }

  
}
-(void)setRestaurant:(IMGRestaurant *)restaurant andBookDate:(NSDate *)bookDate andBookTime:(NSString *)bookTime andPax:(NSNumber *)pax andRestaurantOffer:(IMGRestaurantOffer *)restaurantOffer andRestaurantNumber:(NSInteger)restaurantNumber{
    _restaurant=restaurant;
    if (reviewLabel.hidden) {
        reviewLabel.hidden=NO;
    }
     adLableview.hidden=YES;
    NSString *tittle=[NSString stringWithFormat:@"%ld. %@",(long)restaurantNumber,_restaurant.title];
    NSString *blank=@"        ";
    if (_restaurant.isAd) {
        adLableview.hidden=NO;
        NSMutableString *str=[[NSMutableString alloc]initWithString:tittle];
        [str insertString:blank atIndex:0];
        titleLable.text=str;

    }else{
       titleLable.text=tittle;
    }
    UIFont* tiltleFont= [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:18];
    CGSize titleSize=[titleLable.text sizeWithFont:tiltleFont constrainedToSize:CGSizeMake(DeviceWidth-2*LEFT_X-RESTAURANT_IMAGE_SIZE,tiltleFont.lineHeight*2) lineBreakMode:NSLineBreakByTruncatingTail];
    titleLable.frame=CGRectMake(LEFT_X, TITLE_LABEL_Y,DeviceWidth-2*LEFT_X-RESTAURANT_IMAGE_SIZE , titleSize.height);
    UIFont *font=[UIFont fontWithName:DEFAULT_FONT_NAME size:15];
    NSString *cuisineAndDistrictText=[NSString stringWithFormat:@"%@ / %@",_restaurant.cuisineName,_restaurant.priceLevel];
    CGSize size=[cuisineAndDistrictText sizeWithFont:font constrainedToSize:CGSizeMake(DeviceWidth-LEFT_X*2-RESTAURANT_IMAGE_SIZE,font.lineHeight*2) lineBreakMode:NSLineBreakByTruncatingTail];
    cuisineAndDistrictLabel.text=cuisineAndDistrictText;
    cuisineAndDistrictLabel.frame=CGRectMake(LEFT_X, titleLable.endPointY,size.width,size.height);
    
//    ratingScoreControl.score = [[NSNumber numberWithFloat:[restaurant.ratingScore floatValue]] getRatingNumber];
    
    ratingScoreControl.frame=CGRectMake(LEFT_X, cuisineAndDistrictLabel.endPointY+5, 84, 18);
    float score = [restaurant.ratingScore floatValue];
    int scoreInt =(int)roundf(score);
    if (scoreInt/2.0-scoreInt/2>0) {
        ratingLabel.text = [NSString stringWithFormat:@"(%.1f/5)",scoreInt/2.0];
        [ratingScoreControl updateRating:[NSNumber numberWithFloat:scoreInt]];

    }else{
        ratingLabel.text = [NSString stringWithFormat:@"(%d/5)",scoreInt/2];
        [ratingScoreControl updateRating:[NSNumber numberWithFloat:scoreInt]];

        
    }
    ratingLabel.frame = CGRectMake(ratingScoreControl.endPointX, ratingScoreControl.frame.origin.y-3, 35, 20);
    if ([restaurant.ratingScore intValue]==0) {
        ratingLabel.frame = CGRectMake(ratingScoreControl.endPointX, ratingScoreControl.frame.origin.y-3, 0, 0);
    }


    if ([restaurant.reviewCount intValue]!=0) {
//        [reviewCountLabel setText:[NSString stringWithFormat:@"%@%@",_restaurant.reviewCount,[_restaurant.reviewCount intValue]>1?@"Reviews":@"Review"]];
        NSString *reviewStr=[NSString stringWithFormat:@"%@ %@",_restaurant.reviewCount,[_restaurant.reviewCount intValue]>1?@"Reviews":@"Review"];
        CGSize reviewLabelSize =[reviewStr sizeWithFont:reviewLabel.font constrainedToSize:CGSizeMake(MAXFLOAT, 15)];
//        reviewCountLabel.frame=CGRectMake(ratingLabel.endPointX, cuisineAndDistrictLabel.endPointY+4, reviewLabelSize.width,15);
        reviewLabel.frame=CGRectMake(ratingLabel.endPointX+3, cuisineAndDistrictLabel.endPointY+4, reviewLabelSize.width,15);
        NSString *str=[NSString stringWithFormat:@"%@ %@",_restaurant.reviewCount,[_restaurant.reviewCount intValue]>1?@"Reviews":@"Review"];
        // 下划线
        NSDictionary *attribtDic = @{NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle]};
        NSMutableAttributedString *attribtStr = [[NSMutableAttributedString alloc]initWithString:str attributes:attribtDic];
        reviewLabel.attributedText=attribtStr;

        
//        
//        [reviewLabel setText:[NSString stringWithFormat:@"%@",[_restaurant.reviewCount intValue]>1?@"reviews":@"review"]];
//        line.frame=CGRectMake(reviewCountLabel.startPointX, reviewCountLabel.endPointY-2, reviewCountLabel.width, 1);
 
    }else{
    
        reviewLabel.hidden=YES;
//        reviewCountLabel.hidden=YES;
//        line.hidden=YES;
    
    }
        //修改高度控制有无
    if ((_restaurant.bestOfferName.length!=0) && ([restaurant.state intValue]!=3) ) {
       
        offerLabel.frame=CGRectMake(LEFT_X, ratingScoreControl.endPointY, 180, 25);
        offerLabel.text=[_restaurant.bestOfferName filterHtml];
       
    }else{
        offerLabel.frame=CGRectMake(LEFT_X, ratingScoreControl.endPointY, 180, 0);
    }
  
    districtLabel.frame=CGRectMake(LEFT_X, ratingScoreControl.endPointY+offerLabel.frame.size.height,180, 20);
    districtLabel.text=[_restaurant.districtName filterHtml];
    
    
    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"HH:mm"];
    NSString *  locationString=[dateformatter stringFromDate:senddate];
    NSString* hours=[locationString substringToIndex:2];
    NSRange rang={3,2};
    NSString* min=[locationString substringWithRange:rang];
    int curenttime=[hours intValue]*3600+[min intValue]*60;
    NSLog(@" current   time  %d",curenttime);
   //nextDay
    NSLog(@"________%@",restaurant.nextopenTime);
    if (![restaurant.nextopenTime isEqualToString:@""]&&![restaurant.nextclosedTime isEqualToString:@""]) {
        
       NSString* nextHours=[restaurant.nextopenTime substringToIndex:2];
        NSString* nextMin=[restaurant.nextopenTime substringWithRange:rang];
        nextOpenCurenttime=[nextHours intValue]*3600+[nextMin intValue]*60;
     
    if ([restaurant.nextclosedTime isEqualToString:@"00:00"]&&![restaurant.nextopenTime isEqualToString:@"00:00"]) {
            restaurant.nextclosedTime=@"24:00";
        }
        NSString* nextCloseHours=[restaurant.nextclosedTime substringToIndex:2];
        NSString* nextCloseMin=[restaurant.nextclosedTime substringWithRange:rang];
        nextCloseCurenttime=[nextCloseHours intValue]*3600+[nextCloseMin intValue]*60;
        if (nextOpenCurenttime>nextCloseCurenttime) {
            
            restaurant.nextclosedTime=@"24:00";
            NSString* nextCloseHours1=[restaurant.nextclosedTime substringToIndex:2];
            NSString* nextCloseMin1=[restaurant.nextclosedTime substringWithRange:rang];
            nextCloseCurenttime=[nextCloseHours1 intValue]*3600+[nextCloseMin1 intValue]*60;
        }
     
    }
    //secondDay
    if (![restaurant.secondopenTime isEqualToString:@""]&&![restaurant.secondclosedTime isEqualToString:@""]) {
   
    NSString* secondHours=[restaurant.secondopenTime substringToIndex:2];
    NSString* secondMin=[restaurant.secondopenTime substringWithRange:rang];
    secondOpenCurenttime=[secondHours intValue]*3600+[secondMin intValue]*60;
    NSLog(@"secondOpenCurenttime  =%d",secondOpenCurenttime);
    NSString* secondCloseHours=[restaurant.secondclosedTime substringToIndex:2];
    NSString* secondCloseMin=[restaurant.secondclosedTime substringWithRange:rang];
    secondCloseCurenttime=[secondCloseHours intValue]*3600+[secondCloseMin intValue]*60;
    NSLog(@" secondCloseCurenttime =%d",secondCloseCurenttime);
    }
    //获取数据以后截取
    NSString* str=[_restaurant.address1 filterHtml];
    if (str.length>50) {
        NSString* strAddress=[str substringToIndex:50];
        NSString* strnow=[NSString stringWithFormat:@"%@...",strAddress];
         distrctDescription.text=strnow;
    }else{
    
        distrctDescription.text=[_restaurant.address1 filterHtml];
    
    }
   
    UIFont* districtDesFont=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14];
    CGSize districtDesSize=[distrctDescription.text sizeWithFont:districtDesFont constrainedToSize:CGSizeMake(DeviceWidth-LEFT_X*2-RESTAURANT_IMAGE_SIZE, districtDesFont.lineHeight*2) lineBreakMode:NSLineBreakByTruncatingTail];
    distrctDescription.frame=CGRectMake(LEFT_X, districtLabel.endPointY, DeviceWidth-LEFT_X*2-RESTAURANT_IMAGE_SIZE, districtDesSize.height);
    int i=0;
    if (![_restaurant.phoneNumber isEqualToString:@""]&&_restaurant.phoneNumber.length>3 ) {
        if (callBtn) {
            [callBtn removeFromSuperview];
        }
        callBtn=[[UIButton alloc]initWithFrame:CGRectMake(LEFT_X, distrctDescription.endPointY+13, 55, 25)];
        callBtn.tag=100;
        [callBtn addTarget:self action:@selector(btnAct:) forControlEvents:UIControlEventTouchUpInside];
        callBtn.backgroundColor=[UIColor colorWithRed:32/255.0 green:162/255.0 blue:105/255.0 alpha:1];
        [callBtn setTitle:@"Call" forState:UIControlStateNormal];
        callBtn.titleLabel.font=[UIFont boldSystemFontOfSize:13];
        callBtn.layer.cornerRadius=3;
        [self addSubview:callBtn];

        i++;
        
    }else{
    
        if (callBtn) {
            [callBtn removeFromSuperview];
        }
    }
    if (![_restaurant.menuCount isEqual:@0]) {
        if (menuBtn) {
            [menuBtn removeFromSuperview];
        }
        menuBtn=[[UIButton alloc]initWithFrame:CGRectMake(LEFT_X+(55+3)*i, distrctDescription.endPointY+13, 55, 25)];
        menuBtn.tag=101;
        [menuBtn addTarget:self action:@selector(btnAct:) forControlEvents:UIControlEventTouchUpInside];
        menuBtn.backgroundColor=[UIColor whiteColor];
        [menuBtn setTitle:@"Menu" forState:UIControlStateNormal];
        [menuBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        menuBtn.titleLabel.font=[UIFont boldSystemFontOfSize:12];
        menuBtn.layer.borderWidth=1;
        menuBtn.layer.borderColor=[UIColor grayColor].CGColor;
        menuBtn.layer.cornerRadius=3;
        [self addSubview:menuBtn];

        
        i++;
        
    }else{
        if (menuBtn) {
            [menuBtn removeFromSuperview];
        }
    }if ([_restaurant.bookStatus intValue]==1)
    {
        if ([_restaurant.state intValue]==1 || [_restaurant.state intValue]==6) {
            if (bookBtn)
            {
                [bookBtn removeFromSuperview];
            }
            bookBtn=[[UIButton alloc]initWithFrame:CGRectMake(LEFT_X+(55+3)*i, distrctDescription.endPointY+13, 55, 25)];
            bookBtn.tag=102;
            [bookBtn addTarget:self action:@selector(btnAct:) forControlEvents:UIControlEventTouchUpInside];
            bookBtn.backgroundColor=[UIColor whiteColor];
            [bookBtn setTitle:@"Book" forState:UIControlStateNormal];
            [bookBtn setTitleColor:[UIColor grayColor]forState:UIControlStateNormal];
            bookBtn.titleLabel.font=[UIFont boldSystemFontOfSize:12];;
            bookBtn.layer.borderWidth=1;
            bookBtn.layer.borderColor=[UIColor grayColor].CGColor;
            bookBtn.layer.cornerRadius=3;
            [self addSubview:bookBtn];
            
        }
        else
        {
            if (bookBtn) {
                [bookBtn removeFromSuperview];
                
            }
            
            
        }
        
    }else{
        if (bookBtn) {
            [bookBtn removeFromSuperview];
            
            
        }
        
    }
    
    if ([restaurant.menuCount intValue]!=0||([restaurant.bookStatus intValue]==1||[restaurant.bookStatus intValue]==6)||restaurant.phoneNumber.length>4) {
         celllineImageView.frame=CGRectMake(0, distrctDescription.endPointY+13+25+10, DeviceWidth, 1);
    }else
    {
    
     celllineImageView.frame=CGRectMake(0, distrctDescription.endPointY+38+10, DeviceWidth, 1);
    
    }
   
    NSString *urlString = [_restaurant.imageUrl returnFullImageUrl];
    __weak typeof(restaurantImageView) weakRestaurantImageView = restaurantImageView;
    [restaurantImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [UIView animateWithDuration:1.0 animations:^{
            [weakRestaurantImageView setAlpha:1.0];
            
        }];
    
    }];
    
    
    if (_restaurant.logoImage.length>1)
    {
        NSString *logoUrlString;
        
        NSString* encodedUrl = [_restaurant.logoImage stringByAddingPercentEscapesUsingEncoding:
                                NSASCIIStringEncoding];
        if(encodedUrl!=NULL)
        {
            logoUrlString = [QRAVED_WEB_IMAGE_SERVER stringByAppendingString:encodedUrl];
        }
        
        __weak typeof (logoImageView) weakLogoImageView = logoImageView;
        //        NSLog(@"logoUrlString = %@",logoUrlString);
//        [logoImageView setImageWithURL:[NSURL URLWithString:logoUrlString] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//            [UIView animateWithDuration:1.0 animations:^{
//                [weakLogoImageView setAlpha:1.0];
//            }];
//        }];
        [logoImageView sd_setImageWithURL:[NSURL URLWithString:logoUrlString] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [UIView animateWithDuration:1.0 animations:^{
                                [weakLogoImageView setAlpha:1.0];
                            }];
        }];
    }else
    {
        [logoImageView setImage:nil];
    }
    NSString *distanceStr = [_restaurant distance];
    [distanceLabel setText:distanceStr];
//    lineImageView.hidden = [@"" isEqualToString:distanceStr];
    
    NSInteger weekNum=[self returnTodayIsWeekDay];
    NSString* weekDay=[self returnWeekDayWithNumber:weekNum];
    titleLable.textColor=[UIColor colorWithRed:196/255.0 green:0 blue:0 alpha:1];
    openTime.frame= CGRectMake(DeviceWidth-95-15, 12, 95, 22);
    openTime.textAlignment=NSTextAlignmentRight;
    openTime.font=[UIFont boldSystemFontOfSize:11];
    
    cuisineAndDistrictLabel.textColor = [UIColor color333333];
    districtLabel.textColor = [UIColor color333333];
    distrctDescription.textColor = [UIColor color333333];
    distanceLabel.textColor = [UIColor color333333];
    
    if([restaurant.state intValue]==3){
        cuisineAndDistrictLabel.textColor = [UIColor grayColor];
        districtLabel.textColor = [UIColor grayColor];
        distrctDescription.textColor = [UIColor grayColor];
        distanceLabel.textColor = [UIColor grayColor];
        
        titleLable.textColor=[UIColor colorWithHexString:@"#7f8691"];
        
        openTime.textAlignment=NSTextAlignmentRight;
        openTime.font=[UIFont boldSystemFontOfSize:12];
        openTime.frame= CGRectMake(restaurantImageView.frame.origin.x, 12, restaurantImageView.frame.size.width, 30);
        openTime.text = @"Permanently\nClosed";
        callBtn.hidden = YES;
        menuBtn.hidden = YES;
        bookBtn.hidden = YES;

        openTime.textColor=[UIColor colorWithHexString:@"#7f8691"];
        
        reviewLabel.hidden=YES;
//        reviewCountLabel.hidden=YES;
//        line.hidden=YES;
        
        [menuBtn removeFromSuperview];
        [bookBtn removeFromSuperview];
        
    }else if ([restaurant.state intValue]==6||[restaurant.state intValue]==1) {
        
        
        if ([restaurant.nextopenTime isEqualToString:@"00:00"]&&[restaurant.nextclosedTime isEqualToString:@"00:00"]) {
            openTime.text=@"OPEN";
            openTime.textColor=[UIColor colorWithRed:32/255.0 green:162/255.0 blue:105/255.0 alpha:1];
            return;
        }
        
        if ([restaurant.nextopenDay isEqualToString:@""]||[restaurant.nextopenTime isEqualToString:@""]||[restaurant.nextclosedTime isEqualToString:@""]) {
            openTime.text=@"Closed";
            callBtn.hidden = YES;
            menuBtn.hidden = YES;
            bookBtn.hidden = YES;

            openTime.textColor=[UIColor grayColor];

            return;
        }
        if ([weekDay isEqualToString:restaurant.nextopenDay]) {
            
            if (curenttime>nextOpenCurenttime&&curenttime<nextCloseCurenttime) {
                openTime.text=@"OPEN";

                openTime.textColor=[UIColor colorWithRed:32/255.0 green:162/255.0 blue:105/255.0 alpha:1];
            }else if (curenttime>nextCloseCurenttime){
                
                openTime.frame= CGRectMake(DeviceWidth-10-99, 12, 100, 22);
                openTime.font=[UIFont boldSystemFontOfSize:10];
                openTime.text=[NSString stringWithFormat:@"OPEN ON %@",[restaurant.secondopenDay uppercaseString]];
                openTime.textColor=[UIColor grayColor];
                

                
            }else
            {
                
                openTime.frame= CGRectMake(DeviceWidth-15-90, 12, 90, 22);
                openTime.text=[NSString stringWithFormat:@"OPEN AT %@",restaurant.nextopenTime ];
                openTime.textColor=[UIColor grayColor];

            }
            
        }else{
            openTime.frame= CGRectMake(DeviceWidth-16-99, 12, 100, 22);
            openTime.font=[UIFont boldSystemFontOfSize:9];
            openTime.text=[NSString stringWithFormat:@"OPEN ON %@",[restaurant.nextopenDay uppercaseString]];

            openTime.textColor=[UIColor grayColor];
        }
        
            if (_restaurant.yesteropenDay&&![_restaurant.yesteropenDay isEqualToString:@""]) {
                NSString* secondHours=[restaurant.yesterclosedTime substringToIndex:2];
                NSString* secondMin=[restaurant.yesterclosedTime substringWithRange:rang];
                int yesterCloseCurenttime=[secondHours intValue]*3600+[secondMin intValue]*60;
                if (curenttime<yesterCloseCurenttime) {
                openTime.text=@"OPEN";

                openTime.textColor=[UIColor colorWithRed:32/255.0 green:162/255.0 blue:105/255.0 alpha:1];
                }
      
            }
        
    }else{
        openTime.font=[UIFont boldSystemFontOfSize:10];
        openTime.textAlignment=NSTextAlignmentRight;
        openTime.text=restaurant.stateName;
        openTime.textColor=[UIColor grayColor];

        
    }

}
-(NSString*)returnWeekDayWithNumber:(NSInteger)number
{

    switch (number) {
        case 1:
            return [NSString stringWithFormat:@"Sun"];
            break;
        case 2:
            return [NSString stringWithFormat:@"Mon"];
            break;
        case 3:
            return [NSString stringWithFormat:@"Tue"];
            break;
        case 4:
            return [NSString stringWithFormat:@"Wed"];
            break;
        case 5:
            return [NSString stringWithFormat:@"Thu"];
            break;
        case 6:
            return [NSString stringWithFormat:@"Fri"];
            break;
        case 7:
            return [NSString stringWithFormat:@"Sat"];
            break;
        default:
            break;
    }


    return nil;


}
-(NSInteger)returnTodayIsWeekDay{

    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *now = [NSDate date];;
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    comps = [calendar components:unitFlags fromDate:now];
    NSInteger weekDay=[comps weekday];
    NSLog(@"today is %ld day ",(long)weekDay);
    return weekDay;



}
+(CGFloat)calculateHeightWithRestaurant:(IMGRestaurant *)restaurant andBookDate:(NSDate *)bookDate andBookTime:(NSString *)bookTime andPax:(NSNumber *)pax andRestaurantOffer:(IMGRestaurantOffer *)restaurantOffer andRestaurantNumber:(NSInteger)restaurantNumber
{
    CGFloat height=0;
    NSString *tittle=[NSString stringWithFormat:@"%ld. %@",(long)restaurantNumber,restaurant.title];
    NSString *blank=@"        ";
    if (restaurant.isAd) {
        NSMutableString *str=[[NSMutableString alloc]initWithString:tittle];
        [str insertString:blank atIndex:0];
        tittle=str;
        
    }
   
    UIFont* tiltleFont= [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:19];
    CGSize titleSize=[tittle sizeWithFont:tiltleFont constrainedToSize:CGSizeMake(DeviceWidth-2*LEFT_X-RESTAURANT_IMAGE_SIZE,tiltleFont.lineHeight*2) lineBreakMode:NSLineBreakByTruncatingTail];
    height=TITLE_LABEL_Y+titleSize.height;
    UIFont *font=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14];
    NSString *cuisineAndDistrictText=[NSString stringWithFormat:@"%@ / %@",restaurant.cuisineName,restaurant.priceLevel];
    CGSize size=[cuisineAndDistrictText sizeWithFont:font constrainedToSize:CGSizeMake(DeviceWidth-LEFT_X*2-RESTAURANT_IMAGE_SIZE,font.lineHeight*2) lineBreakMode:NSLineBreakByTruncatingTail];
    height=height+size.height+23;
  
    //修改高度控制有无
    if (restaurant.bestOfferName.length!=0) {

        height=height+25;
    }
    height=height+20;
    NSString* str=[restaurant.address1 filterHtml];
    if (str.length>50) {
        str=[str substringToIndex:50];
       
    }
    UIFont* districtDesFont=[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14];
    CGSize districtDesSize=[str sizeWithFont:districtDesFont constrainedToSize:CGSizeMake(DeviceWidth-LEFT_X*2-RESTAURANT_IMAGE_SIZE, districtDesFont.lineHeight*2) lineBreakMode:NSLineBreakByTruncatingTail];
    height=height+districtDesSize.height;
      if (![restaurant.phoneNumber isEqualToString:@""]||![restaurant.menuCount isEqual:@0]||[restaurant.bookStatus intValue]==1||[restaurant.bookStatus intValue]==6) {
        height=height+38;
      }else{
      
          height=height+38;
      }
    
 
    return height+17.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
