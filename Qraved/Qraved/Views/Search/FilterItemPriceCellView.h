//
//  FilterItemPriceCellView.h
//  Qraved
//
//  Created by Jeff on 8/21/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGPriceLevel.h"
#import "IMGDiscover.h"

@interface FilterItemPriceCellView : UIView


- (id)initWithFrame:(CGRect)frame objectArray:(NSArray *)priceLevelArray andDiscover:(IMGDiscover*)discover;

@end
