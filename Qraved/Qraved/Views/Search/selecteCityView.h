//
//  selecteCityView.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDHelper.h"
#import "selecteCityViewModel.h"
#import "selecteCityCenterViewModel.h"
#import "V2_LovePreferenceLayout.h"
@interface selecteCityView : UIView<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong)  UICollectionView  *topCollectionView;
@property (nonatomic, strong)  UICollectionView  *centerCollectionView;


@property (nonatomic,strong)NSArray *tagsCenterArr;
@property (nonatomic,strong)NSArray *tagsArr;
//+(instancetype)sdTagsViewWithTagsArr:(NSArray*)tagsArr;
@property (nonatomic, assign)  NSInteger  currentIndex;
@end
