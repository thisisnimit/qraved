//
//  V2_DiscoverListResultFiltersPriceTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverListResultFiltersPriceTableViewCell.h"

@implementation V2_DiscoverListResultFiltersPriceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{

    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.tagsArr = @[@"Below 50rb",@"50rb - 100rb",@"100rb - 200rb",@"Above 200rb"];
        [self initUI];
    }
    return self;
}

- (void)initUI{

    UIScrollView *scrollview = [[UIScrollView alloc] init];
    scrollview.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:scrollview];
    
    scrollview.sd_layout
    .topSpaceToView(self.contentView, 0)
    .leftSpaceToView(self.contentView, 0)
    .widthIs(DeviceWidth)
    .heightIs(40);
    
    _lblBelow = [[UILabel alloc] init];
    _lblBelow.text = self.tagsArr[0];
    _lblBelow.textColor = [UIColor color333333];
    _lblBelow.font = [UIFont systemFontOfSize:12];
    _lblBelow.textAlignment = NSTextAlignmentCenter;
    _lblBelow.backgroundColor = [UIColor color234Gray];
    _lblBelow.layer.cornerRadius = 5;
    _lblBelow.layer.masksToBounds = YES;
    _lblBelow.tag = 30;
    _lblBelow.userInteractionEnabled = YES;
    
    _lblfifty = [[UILabel alloc] init];
    _lblfifty.text = self.tagsArr[1];
    _lblfifty.textColor = [UIColor color333333];
    _lblfifty.font = [UIFont systemFontOfSize:12];
    _lblfifty.textAlignment = NSTextAlignmentCenter;
    _lblfifty.backgroundColor = [UIColor color234Gray];
    _lblfifty.layer.cornerRadius = 5;
    _lblfifty.layer.masksToBounds = YES;
    _lblfifty.tag = 31;
    _lblfifty.userInteractionEnabled = YES;
    
    _lbltwoHundred = [[UILabel alloc] init];
    _lbltwoHundred.text = self.tagsArr[2];
    _lbltwoHundred.textColor = [UIColor color333333];
    _lbltwoHundred.font = [UIFont systemFontOfSize:12];
    _lbltwoHundred.textAlignment = NSTextAlignmentCenter;
    _lbltwoHundred.backgroundColor = [UIColor color234Gray];
    _lbltwoHundred.layer.cornerRadius = 5;
    _lbltwoHundred.layer.masksToBounds = YES;
    _lbltwoHundred.tag = 32;
    _lbltwoHundred.userInteractionEnabled = YES;
    
    _lblAbove = [[UILabel alloc] init];
    _lblAbove.text = self.tagsArr[3];
    _lblAbove.textColor = [UIColor color333333];
    _lblAbove.font = [UIFont systemFontOfSize:12];
    _lblAbove.textAlignment = NSTextAlignmentCenter;
    _lblAbove.backgroundColor = [UIColor color234Gray];
    _lblAbove.layer.cornerRadius = 5;
    _lblAbove.layer.masksToBounds = YES;
    _lblAbove.tag = 33;
    _lblAbove.userInteractionEnabled = YES;
    
    [scrollview sd_addSubviews:@[_lblBelow, _lblfifty, _lbltwoHundred, _lblAbove]];
    
    UITapGestureRecognizer *tapOne = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
    [_lblBelow addGestureRecognizer:tapOne];
    
    UITapGestureRecognizer *tapTwo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
    [_lblfifty addGestureRecognizer:tapTwo];
    
    UITapGestureRecognizer *tapThree = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
    [_lbltwoHundred addGestureRecognizer:tapThree];
    
    UITapGestureRecognizer *tapFour = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
    [_lblAbove addGestureRecognizer:tapFour];
    
    _lblBelow.sd_layout
    .topSpaceToView(scrollview, 5)
    .leftSpaceToView(scrollview, 15)
    .heightIs(30)
    .widthIs(85);
    
    _lblfifty.sd_layout
    .topEqualToView(_lblBelow)
    .leftSpaceToView(_lblBelow, 5)
    .heightIs(30)
    .widthIs(90);
    
    _lbltwoHundred.sd_layout
    .topEqualToView(_lblBelow)
    .leftSpaceToView(_lblfifty, 5)
    .heightIs(30)
    .widthIs(100);
    
    _lblAbove.sd_layout
    .topEqualToView(_lblBelow)
    .leftSpaceToView(_lbltwoHundred, 5)
    .heightIs(30)
    .widthIs(90);
    
    [scrollview setupAutoContentSizeWithRightView:_lblAbove rightMargin:15];

}

- (void)tapClick:(UITapGestureRecognizer *)tapClick{
    
    NSLog(@"ratings=======%ld",tapClick.view.tag);
    
    if (tapClick.view.tag == 30) {
        _lblBelow.textColor = [UIColor whiteColor];
        _lblBelow.backgroundColor = [UIColor color222Red];
        _lblfifty.textColor = [UIColor color333333];
        _lblfifty.backgroundColor = [UIColor color234Gray];
        _lbltwoHundred.textColor = [UIColor color333333];
        _lbltwoHundred.backgroundColor = [UIColor color234Gray];
        _lblAbove.textColor = [UIColor color333333];
        _lblAbove.backgroundColor = [UIColor color234Gray];
    }else if (tapClick.view.tag == 31){
        
        _lblBelow.textColor = [UIColor color333333];
        _lblBelow.backgroundColor = [UIColor color234Gray];
        _lblfifty.textColor = [UIColor whiteColor];
        _lblfifty.backgroundColor = [UIColor color222Red];
        _lbltwoHundred.textColor = [UIColor color333333];
        _lbltwoHundred.backgroundColor = [UIColor color234Gray];
        _lblAbove.textColor = [UIColor color333333];
        _lblAbove.backgroundColor = [UIColor color234Gray];
    }else if (tapClick.view.tag == 32){
        
        _lblBelow.textColor = [UIColor color333333];
        _lblBelow.backgroundColor = [UIColor color234Gray];
        _lblfifty.textColor = [UIColor color333333];
        _lblfifty.backgroundColor = [UIColor color234Gray];
        _lbltwoHundred.textColor = [UIColor whiteColor];
        _lbltwoHundred.backgroundColor = [UIColor color222Red];
        _lblAbove.textColor = [UIColor color333333];
        _lblAbove.backgroundColor = [UIColor color234Gray];
    }else{
        
        _lblBelow.textColor = [UIColor color333333];
        _lblBelow.backgroundColor = [UIColor color234Gray];
        _lblfifty.textColor = [UIColor color333333];
        _lblfifty.backgroundColor = [UIColor color234Gray];
        _lbltwoHundred.textColor = [UIColor color333333];
        _lbltwoHundred.backgroundColor = [UIColor color234Gray];
        _lblAbove.textColor = [UIColor whiteColor];
        _lblAbove.backgroundColor = [UIColor color222Red];
        
    }
    if (self.Delegate) {
        [self.Delegate delegatePrice:tapClick.view.tag];
    }
    
}

//- (void)initUI{
//
//    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
//    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
//    flowLayout.minimumLineSpacing =5;
//    flowLayout.minimumInteritemSpacing = 5;
//    flowLayout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 15);
//    
//    _centerCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40) collectionViewLayout:flowLayout];
//    _centerCollectionView.delegate = self;
//    _centerCollectionView.dataSource = self;
//    _centerCollectionView.backgroundColor = [UIColor whiteColor];
//    [self addSubview:_centerCollectionView];
//
//}
//
//
//- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
//    
//    return 4;
//}
//
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
//    
//    NSString *string = self.tagsArr[indexPath.row];
//    CGFloat width = [SDHelper widthForLabel:string fontSize:14];
//    return CGSizeMake(width+10,40);
//}
//
//- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    
//    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
//    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:CellIdentifier];
//    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
//    //        selecteCityCenterViewModel *model =self.tagsCenterArr[indexPath.row];
//    NSString *string = self.tagsArr[indexPath.row];
//    UILabel *label = [[UILabel alloc] init];
//    label.text = string;//[NSString stringWithFormat:@"%@",model.title];
//    label.frame = CGRectMake(0, 5, ([SDHelper widthForLabel:label.text fontSize:12] + 10), 30);
//    label.font = [UIFont systemFontOfSize:12];
//    label.layer.cornerRadius = 2.0;
//    label.layer.masksToBounds = YES;
//    label.backgroundColor = [UIColor color234Gray];
//    label.textAlignment = NSTextAlignmentCenter;
//    [cell.contentView addSubview:label];
//    
//    return cell;
//    
//}
//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    
//    
//    
//}



@end
