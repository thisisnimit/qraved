//
//  FilterItemCellView.h
//  Qraved
//
//  Created by Jeff on 8/21/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGDiscover.h"
#import "IMGTagUpdate.h"
@interface FilterItemCellView : UIView

@property(nonatomic,retain) NSArray *objectArray;

- (id)initWithFrame:(CGRect)frame discover:(IMGDiscover *)tmpDiscover title:(NSString *)title andTagUpdate:(IMGTagUpdate *)tagUpdate;
- (NSString *)titleFromDiscover:(IMGDiscover *)tmpDiscover;

@end
