//
//  FilterItemCellView.m
//  Qraved
//
//  Created by Jeff on 8/21/14.
//  Copyright (c) 2014 Imaginato. All rights reserved.
//

#import "FilterItemCellView.h"
#import "UIConstants.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "IMGCuisine.h"
#import "IMGFeature.h"
#import "SearchCellLabel.h"
#import "UILabel+Helper.h"
#import "UIView+Helper.h"
#import "IMGArea.h"

@implementation FilterItemCellView

{
    SearchCellLabel *titleLabel;
    SearchCellLabel *contentLabel;
    UIImageView *arrowImageView;
    IMGDiscover *discover;
    IMGTagUpdate *tagUpdate;
}


- (id)initWithFrame:(CGRect)frame discover:(IMGDiscover *)tmpDiscover title:(NSString *)title andTagUpdate:(IMGTagUpdate *)tagUpdateTemp
{
    self = [super initWithFrame:frame];
    if (self) {

        discover = tmpDiscover;
        tagUpdate = tagUpdateTemp;
        
        
        NSLog(@"%d",[tagUpdate.typeId intValue]);
        if ([tagUpdate.typeId intValue] == 3 || [tagUpdate.typeId intValue] == 5 ) {
            
        }else{
            titleLabel = [[SearchCellLabel alloc] init];
            [titleLabel setText:title];
            titleLabel.frame = CGRectMake(15, 0, titleLabel.expectedWidth, frame.size.height);
            titleLabel.textAlignment=NSTextAlignmentLeft;
            [self addSubview:titleLabel];
            
            contentLabel = [[SearchCellLabel alloc] initWith13BoldGreenColorAndFrame:CGRectMake(titleLabel.endPointX + 10, 0, DeviceWidth-titleLabel.frame.size.width-50, frame.size.height)];
            
            [contentLabel setText:[self titleFromDiscover:discover]];
            [contentLabel setTextColor:[UIColor grayColor]];
            [contentLabel setTextAlignment:NSTextAlignmentRight];
            [self addSubview:contentLabel];
            
            arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-25, (frame.size.height-20)/2, 10, 20)];
            
            UIImage *arrowImage = [UIImage imageNamed:@"ArrowRight"];
            [arrowImageView setImage:arrowImage];
            [self addSubview:arrowImageView];

        }
        
        
        
    }
    return self;
}

-(NSString *)titleFromDiscover:(IMGDiscover *)tmpDiscover{
    
    if (!tagUpdate)
    {
        return @"";
    }
    if ([tagUpdate.typeId intValue] == 1)
    {
        if(tmpDiscover==nil||(tmpDiscover.areaArray==nil || tmpDiscover.areaArray.count==0)){
            return @"";
        }
        NSMutableArray *titleArray = [[NSMutableArray alloc]initWithCapacity:0];
        for(IMGArea *area in tmpDiscover.areaArray){
            if ([area isKindOfClass:[IMGArea class]]) {
                [titleArray addObject:area.name];
            }
        }
        if(titleArray.count==0){
            return @"";
        }
        return [[NSString alloc] groupConcat:titleArray];
    }
    else//if ([tagUpdate.typeId intValue] == 2)
    {
        if(tmpDiscover==nil||(tmpDiscover.searchDistrictArray==nil || tmpDiscover.searchDistrictArray.count==0)){
            return @"";
        }
        NSMutableArray *titleArray = [[NSMutableArray alloc]initWithCapacity:0];
        for(IMGDistrict *district in tmpDiscover.searchDistrictArray){
            [titleArray addObject:district.name];
        }
        if(titleArray.count==0){
            return @"";
        }
        return [[NSString alloc] groupConcat:titleArray];
    }
//    else if ([tagUpdate.typeId intValue] == 3)
//    {
//        if(tmpDiscover==nil||(tmpDiscover.cuisineArray==nil || tmpDiscover.cuisineArray.count==0)){
//            return @"";
//        }
//        NSMutableArray *titleArray = [[NSMutableArray alloc]initWithCapacity:0];
//        for(IMGCuisine *tmpCuisine in tmpDiscover.cuisineArray){
//            [titleArray addObject:tmpCuisine.name];
//        }
//        if(titleArray.count==0){
//            return @"";
//        }
//        return [[NSString alloc] groupConcat:titleArray];
//    }
//    else
//    {
//        if(tmpDiscover==nil||(tmpDiscover.tagsArr==nil || tmpDiscover.tagsArr.count==0)){
//            return @"";
//        }
//        
//        NSMutableArray *titleArray = [[NSMutableArray alloc]initWithCapacity:0];
//        for(IMGTag *tag in tmpDiscover.tagsArr){
//            if ([tagUpdate.shows isEqualToString:tag.typeShows])
//            {
//                [titleArray addObject:tag.name];
//            }
//        }
//        if(titleArray.count==0){
//            return @"";
//        }
//        return [[NSString alloc] groupConcat:titleArray];
//        
//    }
    
    
}


@end
