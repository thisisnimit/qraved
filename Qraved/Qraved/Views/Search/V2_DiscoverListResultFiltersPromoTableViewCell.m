//
//  V2_DiscoverListResultFiltersPromoTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/28.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverListResultFiltersPromoTableViewCell.h"

@implementation V2_DiscoverListResultFiltersPromoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if ([super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}

- (void)initUI{
    
    _lblTitle = [UILabel new];
    _lblTitle.text = @"Promo";
    _lblTitle.textColor = [UIColor color333333];
    _lblTitle.font = [UIFont systemFontOfSize:12];
    
    _lblDetail = [UILabel new];
    _lblDetail.text = @"Promo";
    _lblDetail.textColor = [UIColor color333333];
    _lblDetail.font = [UIFont systemFontOfSize:12];
    _lblDetail.textAlignment = NSTextAlignmentRight;
    
    _arrowImageView = [UIImageView new];
    _arrowImageView.image = [UIImage imageNamed:@"ic_home_arrow"];
    
    [self.contentView sd_addSubviews:@[_lblTitle, _lblDetail, _arrowImageView]];
    
    _lblTitle.sd_layout
    .topSpaceToView(self.contentView, 5)
    .leftSpaceToView(self.contentView, 15)
    .heightIs(30);
    [_lblTitle setSingleLineAutoResizeWithMaxWidth:DeviceWidth - 30];
    
    _arrowImageView.sd_layout
    .centerYEqualToView(_lblTitle)
    .rightSpaceToView(self.contentView, 15)
    .heightIs(10)
    .widthIs(10);
    
    _lblDetail.sd_layout
    .centerYEqualToView(_lblTitle)
    .leftSpaceToView(_lblTitle, 5)
    .rightSpaceToView(_arrowImageView, 15)
    .heightIs(30);
    
    
}

@end
