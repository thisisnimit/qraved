#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "IMGRestaurant.h"

@interface CalloutMapAnnotation : NSObject <MKAnnotation> {
	CLLocationDegrees _latitude;
	CLLocationDegrees _longitude;
}

@property (nonatomic) CLLocationDegrees latitude;
@property (nonatomic) CLLocationDegrees longitude;
@property(nonatomic)IMGRestaurant *restaurant;

- (id)initWithLatitude:(CLLocationDegrees)latitude
		  andLongitude:(CLLocationDegrees)longitude andRestaurant:(IMGRestaurant *)paramRestaurant;

-(id)initWithLatitude:(CLLocationDegrees)latitude
         andLongitude:(CLLocationDegrees)longitude;

@end
