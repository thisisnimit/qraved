//
//  V2_DiscoverListResultFiltersRatingTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDHelper.h"

@protocol V2_DiscoverListResultFiltersRatingTableViewCellDelegate <NSObject>

- (void)delegateRating:(NSInteger)RatingTag;

@end

@interface V2_DiscoverListResultFiltersRatingTableViewCell : UITableViewCell


@property (nonatomic,strong)NSArray *tagsArr;
@property (nonatomic, strong)  UILabel  *lblAll;
@property (nonatomic, strong)  UILabel  *lblAbove3Stars;
@property (nonatomic, strong)  UILabel  *lblAbove4Stars;
@property (nonatomic, strong)  UILabel  *lblAbove5Stars;

@property (nonatomic, weak)  id<V2_DiscoverListResultFiltersRatingTableViewCellDelegate> Delegate;

@end
