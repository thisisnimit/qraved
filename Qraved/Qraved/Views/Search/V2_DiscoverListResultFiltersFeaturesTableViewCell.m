//
//  V2_DiscoverListResultFiltersFeaturesTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverListResultFiltersFeaturesTableViewCell.h"
#import "IMGTag.h"
@implementation V2_DiscoverListResultFiltersFeaturesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void) initUI:(NSMutableArray *)tagssArr{
    self.tagIdArray = [NSMutableArray array];
    self.indexArray = [NSMutableArray array];
    self.tagTextArray = [NSMutableArray array];
    self.tagsArr = [NSMutableArray array];
    for (IMGTag *tag in tagssArr) {
        [self.tagsArr addObject:tag.name];
        [self.tagIdArray addObject:tag.tagId];
    }

    TTGTextTagCollectionView *TagCollectionView = [[TTGTextTagCollectionView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 75)];
    TagCollectionView.delegate = self;
    TagCollectionView.defaultConfig.tagTextFont = [UIFont systemFontOfSize:12];
    TagCollectionView.scrollDirection = TTGTagCollectionScrollDirectionHorizontal;
    TagCollectionView.alignment = TTGTagCollectionAlignmentLeft;
    TagCollectionView.numberOfLines = 2;
    TagCollectionView.defaultConfig.tagBackgroundColor = [UIColor color234Gray];
    TagCollectionView.defaultConfig.tagSelectedBackgroundColor = [UIColor color222Red];
    TagCollectionView.defaultConfig.tagTextColor = [UIColor color333333];
    TagCollectionView.defaultConfig.tagBorderWidth = 0;
    TagCollectionView.defaultConfig.tagBorderColor = [UIColor clearColor];
    TagCollectionView.contentInset = UIEdgeInsetsMake(5, 15, 5, 5);
    TagCollectionView.defaultConfig.tagShadowOffset = CGSizeMake(0, 0);
    TagCollectionView.defaultConfig.tagShadowRadius = 0;
    TagCollectionView.defaultConfig.tagShadowOpacity = 0;
    [TagCollectionView addTags:self.tagsArr];
    [self.contentView addSubview:TagCollectionView];


}

- (void)textTagCollectionView:(TTGTextTagCollectionView *)textTagCollectionView didTapTag:(NSString *)tagText atIndex:(NSUInteger)index selected:(BOOL)selected{
    
    NSLog(@"%@===%ld===%d", tagText, index, selected ? YES : NO);
    
    
    if (selected == 0) {
        [self.indexArray removeObject:[NSString stringWithFormat:@"%@",self.tagIdArray[index]]];
        [self.tagTextArray removeObject:tagText];
    }else{
        [self.indexArray addObject:[NSString stringWithFormat:@"%@",self.tagIdArray[index]]];
        [self.tagTextArray addObject:tagText];
    }
    //     NSLog(@"%@",self.tagTextArray);
    
    NSString *string = [self.indexArray componentsJoinedByString:@","];
    //    NSLog(@"%@",string);
    if (self.Delegate) {
        [self.Delegate delegateFeatures:string FeaturesArray:self.tagTextArray indexArray:self.indexArray];
    }
}

@end
