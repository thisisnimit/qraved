//
//  V2_DiscoverListResultTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/20.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DiscoverListResultTableViewCell.h"
#import "myLabel.h"
@implementation V2_DiscoverListResultTableViewCell
{
    myLabel *lblTitle;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUIWithModel:(V2_DiscoverListResultRestaurantTableViewCellModel *)model{

    _V2_DiscoverListResultRestaurantModel = model;
    
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(125, 165);
    layout.minimumLineSpacing = 15;
    layout.minimumInteritemSpacing = 1;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    UICollectionView* photoCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(15,15, DeviceWidth-15, 165) collectionViewLayout:layout];
    photoCollectionView.backgroundColor = [UIColor whiteColor];
    photoCollectionView.delegate = self;
    photoCollectionView.dataSource = self;
    photoCollectionView.showsHorizontalScrollIndicator = NO;
    [self.contentView addSubview:photoCollectionView];
    
    [photoCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"photoCell"];

    
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    if (_V2_DiscoverListResultRestaurantModel.data.count >=10) {
        
        return 10;
        
    }else{
    
        return _V2_DiscoverListResultRestaurantModel.data.count;
    }
    
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    if (_V2_DiscoverListResultRestaurantModel.data.count >0) {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
        V2_DiscoverListResultRestaurantTableViewCellSubModel *model = [_V2_DiscoverListResultRestaurantModel.data objectAtIndex:indexPath.row];
        
        for (UIView *view in cell.contentView.subviews) {
            [view removeFromSuperview];
        }
        
        UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 125, 85)];
        imageView.backgroundColor = [UIColor whiteColor];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        [imageView sd_setImageWithURL:[NSURL URLWithString:[model.img returnFullImageUrlWithWidth:125 andHeight:85]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
        [cell.contentView addSubview:imageView];
        
        UIImageView *videoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(imageView.bounds.size.width-39, 12, 27, 19)];
        videoImageView.image = [UIImage imageNamed:@"ic_journal_video.png"];
        [imageView addSubview:videoImageView];
        if ([model.hasVideo isEqual:@1]) {
            videoImageView.hidden = NO;
        }else{
            videoImageView.hidden = YES;
        }

        
        if ([model.isTrending isEqualToNumber:@1]) {
            UILabel *trendingLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, imageView.height - 20, 80, 20)];
            trendingLabel.backgroundColor = [UIColor redColor];
            trendingLabel.text = @"TRENDING";
            trendingLabel.textColor = [UIColor whiteColor];
            trendingLabel.font = [UIFont systemFontOfSize:14];
            [imageView addSubview:trendingLabel];
        }
        
        
        lblTitle = [[myLabel alloc] initWithFrame:CGRectMake(0, imageView.bottom, 125, 80)];
        lblTitle.backgroundColor = [UIColor whiteColor];
        lblTitle.text = model.title ? model.title : @"";
        lblTitle.textColor = [UIColor color333333];
        lblTitle.font = [UIFont systemFontOfSize:14];
        lblTitle.textAlignment = NSTextAlignmentLeft;
        lblTitle.verticalAlignment = VerticalAlignmentTop;
        lblTitle.numberOfLines = 4;
        [cell.contentView addSubview:lblTitle];
        return cell;

    }else{
    
        return nil;
    }

}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
//    NSLog(@"%ld", (long)indexPath.row);
    V2_DiscoverListResultRestaurantTableViewCellSubModel *model = [_V2_DiscoverListResultRestaurantModel.data objectAtIndex:indexPath.row];
//    NSLog(@"%@",model.myID);
    
    if(self.Delegate){
        [self.Delegate delegateSellegateIndexpath:model.myID];
    }
    
}


@end
