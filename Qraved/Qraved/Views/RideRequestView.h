////
////  RideRequestView.h
////  Qraved
////
////  Copyright © 2016年 Imaginato. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
////#import <UberRides/UberRides-Swift.h>
//#import "AppDelegate.h"
//#import "IMGRestaurant.h"
//
//typedef NS_ENUM(NSUInteger,UberButtonType){
//    UberButtonUberMotor,
//    UberButtonUberX
//};
//
//typedef NS_ENUM(NSUInteger,DisplayStyle){
//    DisplayStyleSimple,
//    DisplayStyleDouble
//};
//
//@interface RideRequestView : UIView
//
//@property(nonatomic,strong)UBSDKRideRequestButton *btn;
//@property(nonatomic,strong)UIImageView *imageView;
//@property(nonatomic,strong)UILabel *timeLabel;
//@property(nonatomic,strong)UILabel *costLabel;
//@property(nonatomic,strong)UBSDKRideParametersBuilder *rideParameters;
//    
//-(instancetype)initWithRestaurant:(IMGRestaurant *)restaurantLocal withButtonType:(UberButtonType)uberButtonTypeLocal;
//-(void)setUBSDKPriceEstimate:(UBSDKPriceEstimate*)UBSDKPriceEstimate displayStyle:(DisplayStyle)displayStyle timeEstimate:(UBSDKTimeEstimate *)timeEstimate;
//-(void)setDisplayStyle:(DisplayStyle)displayStyle;
//    
//@end

