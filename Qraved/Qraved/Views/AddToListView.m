//
//  AddToListView.m
//  Qraved
//
//  Created by System Administrator on 9/25/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "AddToListView.h"
#import "IMGRestaurant.h"
#import "AddToListCellView.h"
#import "IMGMyList.h"
#import "EGORefreshTableFooterView.h"
#import "UIView+Helper.h"
#import "UIColor+Helper.h"
#import "NSString+Helper.h"
#import "Amplitude.h"

@implementation AddToListView{
//	UITapGestureRecognizer *addTap;
	UITapGestureRecognizer *saveTap;
	NSInteger offset;
	CGFloat newHeight;
}

@synthesize hasData;

-(instancetype)initWithFrame:(CGRect)frame andRestaurant:(IMGRestaurant*)restaurant list:(NSArray*)mylists{
	if(self=[super initWithFrame:frame]){
		[self addObserver:self forKeyPath:@"mylists" options:NSKeyValueObservingOptionNew context:nil];
		self.restaurant=restaurant;
		[self addCreateView];
		newHeight=60;
		hasData=YES;
		self.tableView=[[UITableView alloc] initWithFrame:CGRectMake(0,newHeight,frame.size.width,frame.size.height-newHeight)];
		self.tableView.delegate=self;
		self.tableView.dataSource=self;
		self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
		self.mylists=[NSMutableArray arrayWithArray:mylists];
		[self addSubview:self.tableView];
	}
	return self;
}

-(void)addCreateView{
	CGFloat width=self.bounds.size.width;
	_textField=[[UITextField alloc] initWithFrame:CGRectMake(0,16,width-60,32)];
	NSMutableAttributedString *placeholder=[[NSMutableAttributedString alloc] initWithString:@"Create New List"];
	UIColor *nameColor=[UIColor color333333];
	[placeholder addAttribute:NSForegroundColorAttributeName value:nameColor range:NSMakeRange(0,placeholder.length)];
	_textField.attributedPlaceholder=placeholder;
	_textField.textColor=[UIColor color333333];
	_textField.font=[UIFont systemFontOfSize:16];
	_textField.delegate=self;
	_textField.returnKeyType=UIReturnKeyDone;
	_saveBtn=[[UIButton alloc] initWithFrame:CGRectMake(width-25,22,20,20)];
	[_saveBtn setImage:[UIImage imageNamed:@"ic_plus"] forState:UIControlStateNormal];
	_saveBtn.tintColor=[UIColor color333333];
//    [_saveBtn addTarget:self action:@selector(saveBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    UIControl *control = [[UIControl alloc] initWithFrame:CGRectMake(width-30, 10, 50, 50)];
    [control addTarget:self action:@selector(saveBtnClick:) forControlEvents:UIControlEventTouchUpInside];
//	addTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(saveBtnClick:)];
//	addTap.numberOfTapsRequired=1;
//	addTap.numberOfTouchesRequired=1;
//	[_saveBtn addGestureRecognizer:addTap];
	UIImageView *line = [[UIImageView alloc] initWithFrame:CGRectMake(0, _textField.endPointY+17,width, 1)];
    line.image = [UIImage imageNamed:@"CutOffRule"];
	[self addSubview:_textField];
	[self addSubview:_saveBtn];
	[self addSubview:line];
    [self addSubview:control];
}

-(void)addRefreshView{
	self.refreshView=[[EGORefreshTableFooterView alloc] initWithFrame:CGRectMake(0,self.tableView.contentSize.height+10,self.tableView.frame.size.width,66)];
	self.refreshView.backgroundColor=[UIColor clearColor];
	self.refreshView.delegate=self;
	[self.tableView addSubview:self.refreshView];
}

-(void)resetRrefreshViewFrame{
	self.refreshView.frame=CGRectMake(0,self.tableView.contentSize.height+10,self.tableView.frame.size.width,66);
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexpath{
	return 52;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
	return 1;
}

-(NSInteger)tableView:(UITableView*)tableView numberOfRowsInSection:(NSInteger)section{
	return self.mylists.count;
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexpath{
	AddToListCellView *cell=[tableView dequeueReusableCellWithIdentifier:@"listcell"];
	if(cell==nil){
		cell=[[AddToListCellView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"listcell"];
	}
	IMGMyList *mylist=(IMGMyList*)self.mylists[indexpath.row];
	cell.title=mylist.listName;
	cell.inList=[mylist.inList boolValue];
    [cell setImageUrl:mylist.listImageUrls];
	return cell;
}

-(void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath*)indexpath{
	IMGMyList *mylist=(IMGMyList*)self.mylists[indexpath.row];
	AddToListCellView *cell=[tableView cellForRowAtIndexPath:indexpath];
	if(cell.inList){
//        NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
//        [param setObject:self.amplitudeType forKey:@"Location"];
//        if ([mylist.listName isEqualToString:@"Favorites"]) {
//            [param setObject:@"Favorite" forKey:@"Type"];
//        }else{
//            [param setObject:@"Custom list" forKey:@"Type"];
//        }
//        [param setObject:self.restaurant.restaurantId forKey:@"Restaurant_ID"];
//        [param setObject:mylist.listId forKey:@"List_ID"];
//        
//        [[Amplitude instance] logEvent:@"BM - Unsave Restaurant Initiate" withEventProperties:param];

        
        if(self.delegate && [self.delegate respondsToSelector:@selector(myList:removeRestaurant:block:failure:)]){
//            [self.delegate myList:mylist.listId removeRestaurant:self.restaurant.restaurantId block:^(BOOL succeed){
//            	if(succeed){
//                    
//                    [[Amplitude instance] logEvent:@"BM - Unsave Restaurant Succeed" withEventProperties:param];
//            cell.inList=NO;
//            [restaurant setObject:[NSNumber numberWithBool:NO] forKey:@"inList"];
//            [self.restaurants replaceObjectAtIndex:indexpath.row withObject:restaurant];

					cell.inList=NO;
					mylist.inList=[NSNumber numberWithBool:NO];
					[self.mylists replaceObjectAtIndex:indexpath.row withObject:mylist];
//                    self.restaurant.savedCount = [NSNumber numberWithInt:(self.restaurant.savedCount.intValue-1)];
//                    self.restaurant.saved = [NSNumber numberWithInt:0];
//                    for (IMGMyList*item in self.mylists) {
//                        if ([item.inList isEqualToNumber:[NSNumber numberWithInt:1]]) {
//                            self.restaurant.saved = [NSNumber numberWithInt:0];
//                        }
//                    }
//				}
//            }failure:^(NSString *exceptionMsg){
//                [param setObject:exceptionMsg forKey:@"Reason"];
//                [[Amplitude instance] logEvent:@"BM - Unsave Restaurant Failed" withEventProperties:param];
//            }];
		}
	}else{
        
//        NSDictionary *param = [[NSMutableDictionary alloc]init];
//        [param setValue:self.amplitudeType forKey:@"Location"];
//        [param setValue:mylist.listId forKey:@"List_ID"];
//        [param setValue:_restaurant.restaurantId forKey:@"Restaurant_ID"];
//        if ([mylist.listName isEqualToString:@"Favorites"]) {
//            [param setValue:@"Favorite" forKey:@"Type"];
//        }else{
//            [param setValue:@"Custom list" forKey:@"Type"];
//        }
//        [[Amplitude instance] logEvent:@"BM - Save Restaurant Initiate" withEventProperties:param];
//        
//        if(self.delegate && [self.delegate respondsToSelector:@selector(myList:addRestaurant:block:)]){
//            [self.delegate myList:mylist.listId addRestaurant:self.restaurant.restaurantId block:^(BOOL succeed,id status){
//            	if(succeed){
//                    [[Amplitude instance] logEvent:@"BM - Save Restaurant Succeed" withEventProperties:param];
//                    [[AppsFlyerTracker sharedTracker]trackEvent:@"BM - Save Restaurant Succeed" withValues:param];
//
					cell.inList=YES;
					mylist.inList=[NSNumber numberWithInt:3];
					[self.mylists replaceObjectAtIndex:indexpath.row withObject:mylist];
//                    self.restaurant.savedCount = [NSNumber numberWithInt:(self.restaurant.savedCount.intValue+1)];
//                    self.restaurant.saved = [NSNumber numberWithInt:0];
//                    for (IMGMyList*item in self.mylists) {
//                        if ([item.inList isEqualToNumber:[NSNumber numberWithInt:3]]) {
//                            self.restaurant.saved = [NSNumber numberWithInt:3];
//                        }
//                    }
//                }else{
//                    [param setValue:status forKey:@"Reason"];
//                    [[Amplitude instance] logEvent:@"BM - Save Restaurant Failed" withEventProperties:param];
//                }
//            }];
//		}
	}
}

-(void)egoRefreshTableFooterDidTriggerRefresh:(EGORefreshTableFooterView*)refreshView{
	if([self.delegate respondsToSelector:@selector(loadListData:)] && hasData){
		__block __weak EGORefreshTableFooterView *weakRefreshView=refreshView;
		[self.delegate loadListData:^(BOOL _hasData){
			hasData=_hasData;
            if(!hasData){
                self.refreshView.hidden=YES;
            }
			[weakRefreshView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
			[self resetRrefreshViewFrame];
		}];
	}
}

-(void)scrollViewDidScroll:(UITableView*)tableView{
	[self.refreshView egoRefreshScrollViewDidScroll:tableView];
}

- (void)scrollViewDidEndDragging:(UITableView*)tableView willDecelerate:(BOOL)decelerate{
	if(self.tableView.contentOffset.y>self.tableView.contentSize.height-self.tableView.frame.size.height+60&&self.startHasData&&hasData){
		self.tableView.contentOffset=CGPointMake(0,self.tableView.contentSize.height-self.tableView.frame.size.height+66);
		[self resetRrefreshViewFrame];
		[self.refreshView egoRefreshScrollViewDidEndDragging:tableView];
	}
}

-(void)saveBtnClick:(UITapGestureRecognizer*)tap{
	if(self.delegate && [self.delegate respondsToSelector:@selector(createNewList:)]){
		if(_textField.text.length && ![_textField.text isBlankString]){
            [self.delegate createNewList:_textField.text];
		}
	}
}

-(void)textFieldDidEndEditing:(UITextField*)textField{
	_textField.textColor=[UIColor colorRed];
	_textField.font=[UIFont boldSystemFontOfSize:16];
	if(!_textField.text.length){
		NSMutableAttributedString *placeholder=[[NSMutableAttributedString alloc] initWithString:@"Create New List"]; 
		UIColor *nameColor=[UIColor colorRed];
		[placeholder addAttribute:NSForegroundColorAttributeName value:nameColor range:NSMakeRange(0,placeholder.length)];
		_textField.attributedPlaceholder=placeholder;
	}
}

-(void)observeValueForKeyPath:(NSString*)keypath ofObject:(id)object change:(NSDictionary*)change context:(void*)context{
	if([keypath isEqualToString:@"mylists"]){
		for(IMGMyList *mylist in self.mylists){
			if([mylist.listType intValue]==1){
				[self.mylists removeObject:mylist];
				break;
			}
		}
		[self.tableView reloadData];
	}
}

-(BOOL)textFieldShouldReturn:(UITextField*)field{
	[field resignFirstResponder];
	if(field.text){
		[self saveBtnClick:nil];
	}
	return NO;
}

-(void)dealloc{
	[self removeObserver:self forKeyPath:@"mylists" context:nil];
}

@end
