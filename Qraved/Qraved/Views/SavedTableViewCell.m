//
//  SearchPageResturantCell.m
//  Qraved
//
//  Created by imaginato on 16/5/17.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "SavedTableViewCell.h"
#import "IMGRestaurant.h"
#import "UIConstants.h"
#import "DLStarRatingControl.h"
#import "UILabel+Helper.h"
#import "UIColor+Helper.h"
#import "UIView+Helper.h"
#import "NSString+Helper.h"

#import "UIImageView+WebCache.h"
#import "IMGUser.h"
#import "TrackHandler.h"
#import "SendCall.h"
#import "Amplitude.h"
#import "ReviewViewController.h"
#import "AppDelegate.h"
#import "UIColor+Hex.h"
#import "GoFoodBadgeView.h"
#define LEFT_X 15

#define TITLE_LABEL_Y 13
#define TITLE_LABEL_HEIGHT 20

#define GAP_BETWEEN_TITLE_AND_CUISINE 1

#define CUISINE_DISTRICT_LABEL_HEIGHT 20

#define GAP_BETWEEN_CUISINE_AND_REVIEW 3

#define CELL_RATING_STAR_WIDTH 13
#define RESTAURANT_LABEL_HEIGHT 14

#define RESTAURANT_IMAGE_SIZE 87
@interface SavedTableViewCell ()<UIAlertViewDelegate>
@property (nonatomic,retain) UILabel * priceLevelLabel;

@end

@implementation SavedTableViewCell{
    
    UILabel * titleLable;
    UILabel * cuisineAndDistrictLabel;
    UILabel* lblDate;
    UILabel* distrctDescription;
    UILabel *dishCountlabel;
    CGFloat offerHeight;
    //UILabel* openTime;
    UIView* line;
    int nextOpenCurenttime;
    int nextCloseCurenttime;
    int secondOpenCurenttime;
    int secondCloseCurenttime;
    UILabel *reviewCountLabel;
    UIButton *btnSaved;
    UILabel *lblWellKnown;
    UILabel *lblStatus;
    UIImageView * restaurantImageView;
    DLStarRatingControl *ratingScoreControl;
    UIImageView *celllineImageView;
    UIImageView *badgeImgaeView;
    GoFoodBadgeView *goFoodBadgeView;
    UIImageView *dishImageView;

}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
  
        titleLable = [[UILabel alloc]init];
        titleLable.font=[UIFont systemFontOfSize:FONT_SIZE_14];
//        titleLable.numberOfLines=1;
        titleLable.textColor = [UIColor color333333];
        titleLable.tag=1000;
        [self.contentView addSubview:titleLable];
        
        
        
        //star level
        ratingScoreControl=[[DLStarRatingControl alloc]initWithFrame:CGRectMake(LEFT_X, 36, 73, 14) andStars:5 andStarWidth:CELL_RATING_STAR_WIDTH isFractional:YES spaceWidth:2];
        ratingScoreControl.userInteractionEnabled=NO;
        [self.contentView addSubview:ratingScoreControl];
   

        //star count
        reviewCountLabel = [[UILabel alloc] init];
        reviewCountLabel.font = [UIFont systemFontOfSize:FONT_SIZE_12];
        reviewCountLabel.textColor = [UIColor color999999];
        [self.contentView addSubview:reviewCountLabel];
        
        //cuisine district and price
        cuisineAndDistrictLabel = [[UILabel alloc] init];
        //cuisineAndDistrictLabel.numberOfLines=1;
        cuisineAndDistrictLabel.textColor = [UIColor color999999];
        cuisineAndDistrictLabel.font=[UIFont systemFontOfSize:FONT_SIZE_12];
        cuisineAndDistrictLabel.lineBreakMode=NSLineBreakByTruncatingTail;
        [self.contentView addSubview:cuisineAndDistrictLabel];
        
        restaurantImageView = [[UIImageView alloc] init];
        restaurantImageView.tag=1002;
        restaurantImageView.userInteractionEnabled = YES;
        [restaurantImageView setContentMode:UIViewContentModeScaleAspectFill];
        restaurantImageView.clipsToBounds = YES;

        [self.contentView addSubview:restaurantImageView];
        
        btnSaved = [UIButton buttonWithType:UIButtonTypeCustom];
        //btnSaved.frame = CGRectMake(RESTAURANT_IMAGE_SIZE-36, 0, 40, 34);
        [btnSaved setImage:[UIImage imageNamed:@"ic_heart_full"] forState:UIControlStateNormal];
       
        [btnSaved addTarget:self action:@selector(fullHeartClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:btnSaved];
        
        lblWellKnown = [[UILabel alloc] init];
        lblWellKnown.font = [UIFont systemFontOfSize:FONT_SIZE_12];
        lblWellKnown.textColor = [UIColor color999999];
        [self.contentView addSubview:lblWellKnown];
        
        lblDate=[[UILabel alloc]init];
        lblDate.font=[UIFont systemFontOfSize:FONT_SIZE_12];
        lblDate.textColor = [UIColor colorWithRed:70/255.0f green:163/255.0f blue:131/255.0f alpha:1.0f];
        [self.contentView addSubview:lblDate];
        
        badgeImgaeView = [[UIImageView alloc] init];
        badgeImgaeView.hidden = YES;
        [self.contentView addSubview:badgeImgaeView];
        
        lblStatus = [[UILabel alloc] init];
        //lblStatus.hidden = YES;
        lblStatus.font = [UIFont systemFontOfSize:FONT_SIZE_12];
        lblStatus.textColor = [UIColor color999999];
        [self.contentView addSubview:lblStatus];
     
        celllineImageView = [[UIImageView alloc] initWithFrame:CGRectZero];
        celllineImageView.backgroundColor=[UIColor colorWithRed:242/255.0 green:242/255.0 blue:248/255.0 alpha:1];
        [self.contentView addSubview:celllineImageView];

        dishImageView = [[UIImageView alloc] init];
        dishImageView.image = [UIImage imageNamed:@"ic_image.png"];
        dishImageView.contentMode = UIViewContentModeScaleAspectFit;
        [restaurantImageView addSubview:dishImageView];
        
         dishCountlabel = [[UILabel alloc] init];
        dishCountlabel.font = [UIFont systemFontOfSize:FONT_SIZE_12];
        dishCountlabel.textColor = [UIColor whiteColor];
        [restaurantImageView addSubview:dishCountlabel];
        
        goFoodBadgeView = [[GoFoodBadgeView alloc] init];
        goFoodBadgeView.hidden = YES;
        [restaurantImageView addSubview:goFoodBadgeView];
        
        restaurantImageView.sd_layout
        .leftSpaceToView(self.contentView,15)
        .topSpaceToView(self.contentView,13)
        .widthIs(RESTAURANT_IMAGE_SIZE)
        .heightIs(RESTAURANT_IMAGE_SIZE);
        
        dishCountlabel.sd_layout
        .rightSpaceToView(restaurantImageView,5)
        .bottomSpaceToView(restaurantImageView,5)
        .heightIs(14);
        
        [dishCountlabel setSingleLineAutoResizeWithMaxWidth:100];
        
        dishImageView.sd_layout
        .rightSpaceToView(dishCountlabel,2)
        .bottomSpaceToView(restaurantImageView,6)
        .widthIs(13)
        .heightIs(13);
        
        goFoodBadgeView.sd_layout
        .leftSpaceToView(restaurantImageView, 0)
        .rightSpaceToView(restaurantImageView, 0)
        .bottomSpaceToView(restaurantImageView, 0)
        .heightIs(18);
        
        btnSaved.sd_layout
        .rightSpaceToView(self.contentView,15)
        .widthIs(40)
        .heightIs(34)
        .topSpaceToView(self.contentView,12);
        btnSaved.imageEdgeInsets = UIEdgeInsetsMake(0, 20, 9, 0);
        
        titleLable.sd_layout
        .topSpaceToView(self.contentView,12)
        .leftSpaceToView(restaurantImageView,7)
        .rightSpaceToView(self.contentView,60)
        .heightIs(16);
    
        
        ratingScoreControl.sd_layout
        .topSpaceToView(titleLable,5)
        .widthIs(73)
        .heightIs(14)
        .leftSpaceToView(restaurantImageView,7);
        
        reviewCountLabel.sd_layout
        .leftSpaceToView(ratingScoreControl,3)
        .rightSpaceToView(self.contentView,60)
        .bottomEqualToView(ratingScoreControl)
        .heightIs(14);
        
        cuisineAndDistrictLabel.sd_layout
        .topSpaceToView(titleLable,24)
        .leftEqualToView(titleLable)
        .rightSpaceToView(self.contentView,15)
        .heightIs(14);
        
        lblWellKnown.sd_layout
        .topSpaceToView(cuisineAndDistrictLabel,3)
        .leftEqualToView(cuisineAndDistrictLabel)
        .rightEqualToView(cuisineAndDistrictLabel)
        .heightIs(14);
        
        lblDate.sd_layout
        .topSpaceToView(lblWellKnown,3)
        .rightEqualToView(lblWellKnown)
        .leftEqualToView(lblWellKnown)
        .heightIs(14);
        
        
        badgeImgaeView.sd_layout
        .topSpaceToView(restaurantImageView,7)
        .leftSpaceToView(self.contentView,15)
        .widthIs(14)
        .heightIs(14);
        
        lblStatus.sd_layout
        .topSpaceToView(restaurantImageView,8)
        .rightEqualToView(lblDate)
        .leftSpaceToView(badgeImgaeView,3)
        .heightIs(14);
  
    }
    return self;
}

- (void)fullHeartClick:(UITapGestureRecognizer *)tap{
    if (self.delegate && [self.delegate respondsToSelector:@selector(removeFromList:)]) {
        [self.delegate removeFromList:self.restaurant.restaurantId];
    }
    
}
- (void)setRestaurant:(IMGRestaurant *)restaurant{
    _restaurant=restaurant;
    NSString *tittle=[NSString stringWithFormat:@"%@",_restaurant.title];
    titleLable.text=tittle;
    
    [titleLable updateLayout];
    
    NSString *dollar;
    switch(restaurant.priceLevel.intValue){
        case 1:dollar=@"Below 100K";break;
        case 2:dollar=@"100K - 200K";break;
        case 3:dollar=@"200K - 300K";break;
        case 4:dollar=@"Start from 300K";break;
    }
    NSString *status;
    NSString *cuisineAndDistrictText;
    if ([restaurant.cuisineName isEqualToString:@""]) {
        cuisineAndDistrictText = [NSString stringWithFormat:@"%@ • %@",restaurant.districtName,dollar];
    }else{
        cuisineAndDistrictText = [NSString stringWithFormat:@"%@ • %@ • %@",_restaurant.cuisineName,restaurant.districtName,dollar];
    }
    
    cuisineAndDistrictLabel.text=cuisineAndDistrictText;
    
    if ([Tools isBlankString:restaurant.goFoodLink]) {
        goFoodBadgeView.hidden = YES;
        dishImageView.hidden = NO;
        dishCountlabel.hidden = NO;
    }else{
        goFoodBadgeView.hidden = NO;
        dishImageView.hidden = YES;
        dishCountlabel.hidden = YES;
    }
    
    float score = [restaurant.ratingScore floatValue];
    
    int scoreInt =(int)roundf(score);
    if (scoreInt/2.0-scoreInt/2>0) {
        [ratingScoreControl updateRating:[NSNumber numberWithFloat:scoreInt]];
        
    }else{
        [ratingScoreControl updateRating:[NSNumber numberWithFloat:scoreInt]];
    }
    
    NSString *distance = [_restaurant distance];
    
    NSString *ratingStr = @"";
    if (restaurant.ratingCount != nil && ![restaurant.ratingCount isEqual:@0]) {
        ratingStr = [NSString stringWithFormat:@"(%@) •",restaurant.ratingCount];
    }
    
    if (![distance isEqualToString:@""]) {
        reviewCountLabel.text = [NSString stringWithFormat:@"%@ %@",ratingStr, distance];
    }else{
        reviewCountLabel.text = ratingStr;
    }
    
    UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(RESTAURANT_IMAGE_SIZE,RESTAURANT_IMAGE_SIZE)];
    NSString *urlString = [_restaurant.imageUrl returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution];
    __weak typeof(restaurantImageView) weakRestaurantImageView = restaurantImageView;
    [restaurantImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (!image && error) {
            weakRestaurantImageView.image = placeHoderImage;
        }else{
            image = [image imageByScalingAndCroppingForSize:CGSizeMake(RESTAURANT_IMAGE_SIZE, RESTAURANT_IMAGE_SIZE)];
            weakRestaurantImageView.image = image;
        }
        
    }];
    
    dishCountlabel.text = [NSString stringWithFormat:@"%@",_restaurant.dishCount];
    
    if ([restaurant.wellKnownFor isEqualToString:@""]) {
        lblWellKnown.sd_layout
        .topSpaceToView(cuisineAndDistrictLabel,0)
        .leftEqualToView(cuisineAndDistrictLabel)
        .rightEqualToView(cuisineAndDistrictLabel)
        .heightIs(0);
        lblWellKnown.text = @"";
        
    }else{
        lblWellKnown.sd_layout
        .topSpaceToView(cuisineAndDistrictLabel,5)
        .leftEqualToView(cuisineAndDistrictLabel)
        .rightEqualToView(cuisineAndDistrictLabel)
        .heightIs(14);
        lblWellKnown.text = [restaurant.wellKnownFor stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    }
    
    NSInteger weekNum=[self returnTodayIsWeekDay];
    NSString* weekDay=[self returnWeekDayWithNumber:weekNum];
    
    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"HH:mm"];
    NSString *  locationString=[dateformatter stringFromDate:senddate];
    NSString* hours=[locationString substringToIndex:2];
    NSRange rang={3,2};
    NSString* min=[locationString substringWithRange:rang];
    int curenttime=[hours intValue]*3600+[min intValue]*60;
    NSString *endStatus ;
    if ([restaurant.state intValue]==6||[restaurant.state intValue]==1) {
        
        NSString *closedTime = restaurant.nextclosedTime;
        if ([weekDay isEqualToString:restaurant.nextopenDay]) {
            if ([restaurant.nextopenTime isEqualToString:@"00:00"]&&[restaurant.nextclosedTime isEqualToString:@"00:00"]) {
                closedTime = @"24:00";
                
            }
            NSString *openHour = [restaurant.nextopenTime substringToIndex:2];
            NSString *openMin = [restaurant.nextopenTime substringWithRange:rang];
            NSString *closeHour = [closedTime substringToIndex:2];
            NSString *closeMin = [closedTime substringWithRange:rang];
            
            int openTime = [openHour intValue]*3600+[openMin intValue]*60;
            int closeTime = [closeHour intValue]*3600+[closeMin intValue]*60;
            if (openTime < closeTime) {
                if (openTime <= curenttime && curenttime <= closeTime) {
                    status = @"open";
//                    endStatus = [NSString stringWithFormat:@"until %@",restaurant.nextclosedTime];
                    endStatus = @"now";
                    lblDate.textColor = [UIColor colorWithRed:70/255.0f green:163/255.0f blue:131/255.0f alpha:1.0f];
                }else if (curenttime < openTime){
                    status = @"closed";
                    endStatus = [NSString stringWithFormat:@"• Open at %@",restaurant.nextopenTime];
                    lblDate.textColor = [UIColor color999999];
                }else if (curenttime > closeTime){
                    status = @"closed";
                    endStatus = [NSString stringWithFormat:@"• Open on %@",[restaurant.secondopenDay uppercaseString]];
                    lblDate.textColor = [UIColor color999999];
                    //open day
                }
            }else if (openTime >closeTime){
                if (openTime<=curenttime) {
                    status = @"open";
//                    endStatus = [NSString stringWithFormat:@"until %@",restaurant.nextclosedTime];
                    endStatus = @"now";
                    lblDate.textColor = [UIColor colorWithRed:70/255.0f green:163/255.0f blue:131/255.0f alpha:1.0f];
                }else{
                    //
                    status = @"closed";
                    endStatus = [NSString stringWithFormat:@"• Open at %@",restaurant.nextopenTime];
                    lblDate.textColor = [UIColor color999999];
                }
            }else{
                status = @"open";
//                endStatus = [NSString stringWithFormat:@"until %@",restaurant.nextclosedTime];
                endStatus = @"now";
                lblDate.textColor = [UIColor colorWithRed:70/255.0f green:163/255.0f blue:131/255.0f alpha:1.0f];
            }
            
        }else{
            status = @"closed";
            endStatus = [NSString stringWithFormat:@"• Open on %@",[restaurant.nextopenDay uppercaseString]];
            lblDate.textColor = [UIColor color999999];
        }
        
        
        
        NSString* lastWeekDay=[self returnWeekDayWithNumber:weekNum-1];
        
        long lastCloseTime;
        if (restaurant.yesteropenDay != nil && [restaurant.yesteropenDay isEqualToString:lastWeekDay]) {
            lastCloseTime = [restaurant.yesterclosedTime longLongValue];
            
            if (curenttime<=lastCloseTime) {
                status = @"open";
//                endStatus = [NSString stringWithFormat:@"until %@",restaurant.yesterclosedTime];
                endStatus = @"now";
                lblDate.textColor = [UIColor colorWithRed:70/255.0f green:163/255.0f blue:131/255.0f alpha:1.0f];
            }
        }
        
        
        if ([restaurant.nextopenTime isEqualToString:@""] || [restaurant.nextclosedTime isEqualToString:@""]) {
            status = @"Closed";
            endStatus = @"";
            lblDate.textColor = [UIColor color999999];
        }
        
    } else{
        status = @"Closed";
        endStatus = @"";
        lblDate.textColor = [UIColor color999999];
    }
    
    lblDate.text=[NSString stringWithFormat:@"%@ %@",[status capitalizedString],[endStatus stringByReplacingOccurrencesOfString:@"00:00" withString:@"24:00"]];

    NSString *statusStr;
    if ([restaurant.trendingInstagram isEqual:@1]) {
        badgeImgaeView.hidden = NO;
        badgeImgaeView.image = [UIImage imageNamed:@"ic_trendingInstagram"];
        statusStr = @"Trending on instagram";
    }else if ([restaurant.inJournal  isEqual:@1]){
        badgeImgaeView.hidden = NO;
        badgeImgaeView.image = [UIImage imageNamed:@"ic_inJournal"];
        statusStr = [restaurant.journaArticlelTitle stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    }else{
        badgeImgaeView.hidden = YES;
        statusStr = @"";
    }
    
    lblStatus.text = statusStr;
    [lblStatus updateLayout];
    celllineImageView.sd_layout
    .leftSpaceToView(self.contentView,0)
    .rightSpaceToView(self.contentView,0)
    .bottomSpaceToView(self.contentView,1)
    .heightIs(1);
    if (lblWellKnown.text.length==0 && lblStatus.text.length==0) {
        [self setupAutoHeightWithBottomView:restaurantImageView bottomMargin:13];
    }else{
        [self setupAutoHeightWithBottomView:lblStatus bottomMargin:13];
    }
    
 
}

-(NSString*)returnWeekDayWithNumber:(NSInteger)number
{
    
    switch (number) {
        case 1:
            return [NSString stringWithFormat:@"Sun"];
            break;
        case 2:
            return [NSString stringWithFormat:@"Mon"];
            break;
        case 3:
            return [NSString stringWithFormat:@"Tue"];
            break;
        case 4:
            return [NSString stringWithFormat:@"Wed"];
            break;
        case 5:
            return [NSString stringWithFormat:@"Thu"];
            break;
        case 6:
            return [NSString stringWithFormat:@"Fri"];
            break;
        case 7:
            return [NSString stringWithFormat:@"Sat"];
            break;
        default:
            break;
    }
    
    
    return nil;
    
    
}

-(NSInteger)returnTodayIsWeekDay{
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *now = [NSDate date];;
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    comps = [calendar components:unitFlags fromDate:now];
    NSInteger weekDay=[comps weekday];
    NSLog(@"today is %ld day ",(long)weekDay);
    return weekDay;
 
}

-(void)addtoListTap{
    if ([self.delegate respondsToSelector:@selector(addTolist:)]) {
        [self.delegate addTolist:_restaurant];
    }
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
}

@end
