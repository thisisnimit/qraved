//
//  ReviewPublishRatingView.m
//  Qraved
//
//  Created by apple on 17/4/1.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "ReviewPublishRatingView.h"

@implementation ReviewPublishRatingView

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        
        self.starComponent = [[QRStarComponent alloc] initWithFrame:CGRectMake(80, 12, 120, 20)];
        self.starComponent.delegate = self;
        [self addSubview:self.starComponent];
        
        self.lblRating = [[UILabel alloc] initWithFrame:CGRectMake(self.starComponent.endPointX+10, 12, 80, 20)];
        self.lblRating.font = [UIFont systemFontOfSize:14];
        self.lblRating.textColor = [UIColor color999999];
        self.lblRating.text = @"Tap to rate";
        [self addSubview:self.lblRating];
        
        UIView *bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, 43.5, DeviceWidth, 0.5)];
        bottomLine.backgroundColor = [UIColor colorWithHexString:@"C1C1C1"];
        [self addSubview:bottomLine];
//        UILabel *noticeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 60)];
//        noticeLabel.text =@"Tell people what you think";
//        noticeLabel.textAlignment = NSTextAlignmentCenter;
//        noticeLabel.font = [UIFont boldSystemFontOfSize:20];
//        [self addSubview:noticeLabel];
//        UIImageView *lineImageView = [[UIImageView alloc]initWithFrame:CGRectMake(50, noticeLabel.endPointY, DeviceWidth-100, 1)];
//        lineImageView.image = [UIImage imageNamed:@"CutOffRule"];
//        [self addSubview:lineImageView];
//        self.startRating = [[DLStarRatingControl alloc]initWithFrame:CGRectMake(DeviceWidth/2-112.5,lineImageView.endPointY , 225, 45) andStars:5 andStarWidth:30 isFractional:NO spaceWidth:15];
//        _startRating.tag = 101;
//        [self addSubview:self.startRating];
//        UILabel *seclctRatingLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.startRating.endPointY, DeviceWidth, 20)];
//        seclctRatingLabel.text = @"select rating";
//        seclctRatingLabel.textAlignment = NSTextAlignmentCenter;
//        seclctRatingLabel.font = [UIFont systemFontOfSize:11];
//        seclctRatingLabel.textColor = [UIColor grayColor];
//        [self addSubview:seclctRatingLabel];
//        
//        UIView *fivestarView = [[UIView alloc] initWithFrame:CGRectMake(DeviceWidth/2-65, seclctRatingLabel.endPointY+25, DeviceWidth/2+20, 20)];
//        UILabel *five = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
//        UILabel *starFive = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 20, 20)];
//        UILabel *fiveStarLabel = [[UILabel alloc] initWithFrame:CGRectMake(30,0, DeviceWidth/2+20, 20)];
//        
//        five.text = @"5";
//        starFive.text = @"★";
//        fiveStarLabel.text= @"I loved it";
//        [fivestarView addSubview:starFive];
//        [fivestarView addSubview:five];
//        [fivestarView addSubview:fiveStarLabel];
//        [self addSubview:fivestarView];
//        UIView *fourstarView = [[UIView alloc] initWithFrame:CGRectMake(DeviceWidth/2-65, fivestarView.endPointY+5, DeviceWidth/2+20, 20)];
//        UILabel *four = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
//        UILabel *fourStarLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, DeviceWidth/2+20, 20)];
//        UILabel *starFour = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 20, 20)];
//        starFour.text = @"★";
//        four.text=@"4";
//        fourStarLabel.text = @"I like it";
//        [fourstarView addSubview:starFour];
//        [fourstarView addSubview:four];
//        [fourstarView addSubview:fourStarLabel];
//        [self addSubview:fourstarView];
//        UIView *threestarView = [[UIView alloc] initWithFrame:CGRectMake(DeviceWidth/2-65, fourstarView.endPointY+5, DeviceWidth/2+20, 20)];
//        
//        UILabel *threeStarLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, DeviceWidth/2+20, 20)];
//        UILabel *starThree = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 20, 20)];
//        UILabel *three = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
//        three.text =@"3";
//        starThree.text = @"★";
//        threeStarLabel.text = @"It was okay";
//        [threestarView addSubview:starThree];
//        [threestarView addSubview:threeStarLabel];
//        [threestarView addSubview:three];
//        [self addSubview:threestarView];
//        
//        UIView *twostarView = [[UIView alloc] initWithFrame:CGRectMake(DeviceWidth/2-65, threestarView.endPointY+5, DeviceWidth/2+20, 20)];
//        
//        UILabel *twoStarLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, DeviceWidth/2+20, 20)];
//        UILabel *starTwo = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 20, 20)];
//        UILabel *two = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
//        two.text =@"2";
//        starTwo.text = @"★";
//        twoStarLabel.text = @"I didn't like it";
//        [twostarView addSubview:two];
//        [twostarView addSubview:starTwo];
//        [twostarView addSubview:twoStarLabel];
//        [self addSubview:twostarView];
//        
//        UIView *oneView = [[UIView alloc] initWithFrame:CGRectMake(DeviceWidth/2-65, twostarView.endPointY+5, DeviceWidth/2+20, 20)];
//        
//        UILabel *oneStarLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 0, DeviceWidth/2+20, 20)];
//        UILabel *starOne = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 20, 20)];
//        UILabel *one = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
//        starOne.text = @"★";
//        one.text=@"1";
//        oneStarLabel.text = @"I won't come back";
//        [oneView addSubview:oneStarLabel];
//        [oneView addSubview:one];
//        [oneView addSubview:starOne];
//        [self addSubview:oneView];

    }
    return self;
}

- (void)updateRatingText:(int)rating {
    NSArray *ratingText = @[
                            @"Terrible",
                            @"Bad",
                            @"Average",
                            @"Good",
                            @"Excellent"
                            ];
    
    if (rating<0) {
        self.lblRating.text = @"Tap to Rate";
    }else{
        self.lblRating.text = ratingText[(NSUInteger) rating];
    }
}

- (void)starComponent:(QRStarComponent *)control onRatingChangeEnd:(int)rating {
   
    [self updateRatingText:rating];
    if (self.ratingRestaurant) {
        self.ratingRestaurant(rating);
    }
}

- (void)updateRating:(int)rating{
    
    int rat = rating/2 - 1;
    [self.starComponent setValue:rat];
    [self updateRatingText:rat];
}

@end
