//
//  MyListAddRestaurantCell.m
//  Qraved
//
//  Created by root on 9/26/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "MyListAddRestaurantCell.h"
#import "UIConstants.h"
#import "UIColor+Helper.h"

@implementation MyListAddRestaurantCell{
	UILabel *titleLabel;
	UILabel *cuisineLabel;
	CGFloat image_width;
    UIImageView *restaurantImageView;
    UILabel *label;
    UIView *savedView;
}

@synthesize title;
@synthesize cuisine;
@synthesize image;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
	if(self=[super initWithStyle:style reuseIdentifier:identifier]){
		image_width=32;
		self.selectionStyle=UITableViewCellSelectionStyleNone;
		self.selectionStyle=UITableViewCellSelectionStyleNone;
        restaurantImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 8, 48, 48)];
        [self addSubview:restaurantImageView];

		titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(restaurantImageView.endPointX+10,10,DeviceWidth-120,22)];
		titleLabel.font=[UIFont systemFontOfSize:FONT_SIZE_14];
		titleLabel.numberOfLines=1;
		cuisineLabel=[[UILabel alloc] initWithFrame:CGRectMake(titleLabel.startPointX,titleLabel.endPointY,DeviceWidth-120,20)];
		cuisineLabel.font=[UIFont systemFontOfSize:FONT_SIZE_14];
		cuisineLabel.textColor=[UIColor color999999];
		cuisineLabel.numberOfLines=1;
		self.savedImageView=[[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-43, 26, 13, 12)];
        self.savedImageView.image = [UIImage imageNamed:@"ic_Checkmark1"];
		self.savedImageView.hidden=YES;
        savedView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth-LEFTLEFTSET, 64)];
        savedView.userInteractionEnabled = NO;
        savedView.backgroundColor = [UIColor whiteColor];
        savedView.alpha = 0.5;
        savedView.hidden = YES;
		UIImageView *line = [[UIImageView alloc] initWithFrame:CGRectMake(0,63,DeviceWidth-LEFTLEFTSET*2, 1)];
	    line.image = [UIImage imageNamed:@"CutOffRule"];
		[self addSubview:titleLabel];
		[self addSubview:cuisineLabel];
		[self addSubview:self.savedImageView];
		[self addSubview:line];
        [self addSubview:savedView];
//		[self addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
//		[self addObserver:self forKeyPath:@"cuisine" options:NSKeyValueObservingOptionNew context:nil];
		[self addObserver:self forKeyPath:@"inList" options:NSKeyValueObservingOptionNew context:nil];
	}
	return self;
}

-(void)observeValueForKeyPath:(NSString*)keypath ofObject:object change:(NSDictionary*)change context:(void*)context{
      if([keypath isEqualToString:@"inList"]){
		
		if([[change objectForKey:@"new"] boolValue]){
            self.savedImageView.hidden = NO;
		}else{
			self.savedImageView.hidden = YES;
		}
//		[saveBtn setImage:image_ forState:UIControlStateNormal];
//		saveBtn.frame=CGRectMake(DeviceWidth-LEFTLEFTSET*2-image_width,(self.frame.size.height-image_.size.height)/2,image_width,image_.size.height);
	}
}
//-(void)setImageUrl:(NSString*)imageUrl{
////    if (restaurantImageView) {
////        [restaurantImageView removeFromSuperview];
////    }
//    if (self.inList) {
//        savedView.hidden = NO;
//        cuisineLabel.text = @"Already added to list";
//    }
//    
////    restaurantImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, self.size.height-8, self.size.height-8)];
//    restaurantImageView.contentMode = UIViewContentModeScaleAspectFit;
//    //[self addSubview:restaurantImageView];
//    NSArray *images;
//    if (imageUrl.length>0) {
//        images=[imageUrl componentsSeparatedByString:@","];
//
//    }
//    if(imageUrl&& imageUrl.length && images.count>0){
//        __weak typeof (restaurantImageView) weakRestaurantImageView = restaurantImageView;
//        [restaurantImageView sd_setImageWithURL:[NSURL URLWithString:[images[0] returnFullImageUrlWithWidth:40]] placeholderImage:[[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake(40, 40)]completed:^(UIImage *image_, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//            image_ = [image_ imageByScalingAndCroppingForSize:CGSizeMake(40, 40)];
//            weakRestaurantImageView.image = image_;
//        }];
//    }else{
//        restaurantImageView.image = [[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake(40, 40)];
//    }
//}
- (void)setRestaurantWithDic:(NSDictionary *)restaurant {
    titleLabel.text = [restaurant objectForKey:@"title"];
    //self.inList = [[restaurant objectForKey:@"inList"] boolValue];
    if (![[restaurant objectForKey:@"inList"] boolValue]) {
       NSString *distanceStr = [NSString returnDistanceWithLatitude:[restaurant objectForKey:@"latitude"] andLongitude:[restaurant objectForKey:@"longitude"]];
        if ([distanceStr isEqualToString:@""]) {
            cuisineLabel.text = [NSString stringWithFormat:@"%@",[restaurant objectForKey:@"cuisine"]];
        }else{
            cuisineLabel.text = [NSString stringWithFormat:@"%@ • %@",[restaurant objectForKey:@"cuisine"], distanceStr];
        }
        
        savedView.hidden = YES;
    }else{
        NSString *listName = [restaurant objectForKey:@"listName"];
        cuisineLabel.text = [NSString stringWithFormat:@"Already added to %@",listName];
        savedView.hidden = NO;
        _savedImageView.hidden = YES;
    }
    NSString *imageUrl = [restaurant objectForKey:@"imageUrl"];
    if (![imageUrl isKindOfClass:[NSNull class]]) {
        NSArray *images;
        if (imageUrl.length>0) {
            images=[imageUrl componentsSeparatedByString:@","];
            
        }
        if(imageUrl&& imageUrl.length && images.count>0){
            __weak typeof (restaurantImageView) weakRestaurantImageView = restaurantImageView;
            [restaurantImageView sd_setImageWithURL:[NSURL URLWithString:[images[0] returnFullImageUrlWithWidth:48]] placeholderImage:[[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake(48, 48)]completed:^(UIImage *image_, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                
                if (!image_ && error) {
                    weakRestaurantImageView.image = [[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake(48, 48)];
                }else{
                    image_ = [image_ imageByScalingAndCroppingForSize:CGSizeMake(48, 48)];
                    weakRestaurantImageView.image = image_;
                }
               
            }];
        }else{
            restaurantImageView.image = [[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake(48, 48)];
        }

    }else{
        restaurantImageView.image = [[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake(48, 48)];
    }
}
//- (void)setInList:(BOOL)inList{
//    if (inList) {
//        _savedImageView.hidden = NO;
//    }else{
//        _savedImageView.hidden = YES;
//    }
//}
-(void)dealloc{
	[self removeObserver:self forKeyPath:@"inList"];
}

@end
