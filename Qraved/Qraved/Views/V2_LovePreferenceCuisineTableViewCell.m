//
//  V2_LovePreferenceCuisineTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/8/9.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_LovePreferenceCuisineTableViewCell.h"
#import "V2_LovePreferenceModel.h"
@implementation V2_LovePreferenceCuisineTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI:(NSArray *)array{
//    self.cuisineArra = array1;
//    _selearray =array;
    _selearray = [NSMutableArray array];
    
    self.selectedObjectIdArr = [NSMutableArray array];
    for ( V2_CuisineModel *cuisineModel in array) {
        [_selearray addObject:cuisineModel.name];
        
    }
   
    
    TTGTextTagCollectionView *TagCollectionView = [[TTGTextTagCollectionView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 44)];
    TagCollectionView.tag = self.section;
    TagCollectionView.delegate = self;
    TagCollectionView.defaultConfig.tagExtraSpace = CGSizeMake(25, 18);
    TagCollectionView.defaultConfig.tagTextFont = [UIFont systemFontOfSize:14];
    TagCollectionView.alignment = TTGTagCollectionAlignmentLeft;
    TagCollectionView.defaultConfig.tagBackgroundColor = [UIColor colorWithHexString:@"EAEAEA"];
    TagCollectionView.defaultConfig.tagSelectedBackgroundColor = [UIColor colorWithHexString:@"#D20000"];
    TagCollectionView.defaultConfig.tagTextColor = [UIColor color333333];
    TagCollectionView.defaultConfig.tagBorderWidth = 0;
    TagCollectionView.defaultConfig.tagBorderColor = [UIColor clearColor];
    TagCollectionView.contentInset = UIEdgeInsetsMake(5, 15, 5, 5);
    TagCollectionView.defaultConfig.tagShadowOffset = CGSizeMake(0, 0);
    TagCollectionView.defaultConfig.tagShadowRadius = 0;
    TagCollectionView.defaultConfig.tagShadowOpacity = 0;
    TagCollectionView.showsHorizontalScrollIndicator = NO;
    TagCollectionView.manualCalculateHeight = YES;
    TagCollectionView.defaultConfig.tagCornerRadius = 16;
    TagCollectionView.defaultConfig.tagSelectedCornerRadius = 16;
    [TagCollectionView addTags:_selearray];
    [self.contentView addSubview:TagCollectionView];
    
    
    for (int i = 0; i < array.count; i ++) {
        V2_CuisineModel *model = [array objectAtIndex:i];
        if ([model.state isEqualToString:@"1"]) {
            [TagCollectionView setTagAtIndex:i selected:YES];
        }
    }
    TagCollectionView.frame = CGRectMake(0, 0, DeviceWidth, TagCollectionView.contentSize.height);
    self.pointY = TagCollectionView.contentSize.height;
    
}
- (void)textTagCollectionView:(TTGTextTagCollectionView *)textTagCollectionView didTapTag:(NSString *)tagText atIndex:(NSUInteger)index selected:(BOOL)selected{
    
    if (self.Delegate) {
        [self.Delegate textTagCollectionView:textTagCollectionView.tag didTapTag:tagText atIndex:index selected:selected];
    }
    
 
}

@end
