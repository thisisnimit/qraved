//
//  V2_LovePreferenceTableViewCell.m
//  Qraved
//
//  Created by Adam.zhang on 2017/8/8.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_LovePreferenceTableViewCell.h"

@implementation V2_LovePreferenceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI:(NSArray *)array{
    _eatingArray = array;
    self.selecteArr = [NSMutableArray array];
    
//    _normalArray = @[@"ic_eating_everything1",@"ic_eating_veggles1",
//                    @"ic_eating_no_pork1",@"ic_eating_no_alcohol1"];
    CGFloat space = 10;
    CGFloat btnWidth = (DeviceWidth - 5 * 10)/4;
    NSMutableArray *temp = [NSMutableArray array];
    for (int i = 0; i < array.count; i ++) {
        CGFloat X = space + i * btnWidth + i * space;
        UIButton *btnEat = [UIButton new];
        V2_EatModel *eatModel = [array objectAtIndex:i];
        btnEat.tag = [eatModel.objectId intValue];
        NSString *normalImage ;
        if ([eatModel.objectId isEqual:@1]) {
            normalImage = @"ic_eating_everything1";
        }else if ([eatModel.objectId isEqual:@2]){
            normalImage = @"ic_eating_veggles1";
        }else if ([eatModel.objectId isEqual:@3]){
            normalImage = @"ic_eating_no_alcohol1";
        }else if ([eatModel.objectId isEqual:@4]){
            normalImage = @"ic_eating_no_pork1";
        }
        
        btnEat.layer.cornerRadius = 5;
        btnEat.layer.masksToBounds = YES;
        
        btnEat.frame = CGRectMake(X, 0, btnWidth , 108);
        
        [btnEat addTarget:self action:@selector(chooseEat:) forControlEvents:UIControlEventTouchUpInside];
        
        [temp addObject:btnEat];
        
     
        self.imageV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, btnWidth, btnWidth)];
        self.imageV.image = [UIImage imageNamed:normalImage];
        self.imageV.layer.cornerRadius = btnWidth/2;
        self.imageV.layer.masksToBounds = YES;
        [btnEat addSubview:self.imageV];
        
        UIImageView *marksView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, btnWidth, btnWidth)];
        marksView.tag = [eatModel.objectId intValue] + 20;
        marksView.image = [UIImage imageNamed:@"ic_marks"];
        marksView.layer.cornerRadius = btnWidth/2;
        marksView.layer.masksToBounds = YES;
        marksView.hidden = YES;
        [btnEat addSubview:marksView];
        
        V2_EatModel *model = [_eatingArray objectAtIndex:i];
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, self.imageV.bottom +8, btnWidth, 35)];
        lblTitle.text = model.name;
        lblTitle.textColor = [UIColor color333333];
        lblTitle.font = [UIFont systemFontOfSize:12];
        lblTitle.numberOfLines = 2;
        lblTitle.textAlignment = NSTextAlignmentCenter;
        [btnEat addSubview:lblTitle];
        
        CGSize lblTitleH = [lblTitle sizeThatFits:CGSizeMake(btnWidth, CGFLOAT_MAX)];
        lblTitle.frame = CGRectMake(0, self.imageV.bottom +8, btnWidth, lblTitleH.height);

        btnEat.frame = CGRectMake(X, 0, btnWidth , lblTitle.endPointY);
       
        [self.contentView addSubview:btnEat];
        

         _cellHeight = btnEat.endPointY+20;
    }
    [self setSelectButton];
}


- (void)setSelectButton{
    UIButton * button1 = [self.contentView viewWithTag:1];
    UIButton * button2 = [self.contentView viewWithTag:2];
    UIButton * button3 = [self.contentView viewWithTag:3];
    UIButton * button4 = [self.contentView viewWithTag:4];
    
    UIImageView *image1 =[button1 viewWithTag:21];
    UIImageView *image2 =[button2 viewWithTag:22];
    UIImageView *image3 =[button3 viewWithTag:23];
    UIImageView *image4 =[button4 viewWithTag:24];

    V2_EatModel *model1 = [_eatingArray objectAtIndex:0];
    V2_EatModel *model2 = [_eatingArray objectAtIndex:1];
    V2_EatModel *model3 = [_eatingArray objectAtIndex:2];
    V2_EatModel *model4 = [_eatingArray objectAtIndex:3];
    
    if ([model1.state isEqualToString:@"1"]) {
        button1.enabled = YES;
        image1.image = [UIImage imageNamed:@"ic_marks"];
        image1.hidden = NO;
        
        button2.enabled = NO;
        image2.image = [UIImage imageNamed:@"markswhite"];
        image2.hidden = NO;
        
        button3.enabled = NO;
        image3.image = [UIImage imageNamed:@"markswhite"];
        image3.hidden = NO;
        
        button4.enabled = NO;
        image4.image = [UIImage imageNamed:@"markswhite"];
        image4.hidden = NO;
    }else{
        image1.hidden = YES;
        button1.enabled = YES;
        button2.enabled = YES;
        button3.enabled = YES;
        button4.enabled = YES;
    }
    if ([model2.state isEqualToString:@"1"]){
        button1.enabled = NO;
        image1.image = [UIImage imageNamed:@"markswhite"];
        image1.hidden = NO;
        
        button2.enabled = YES;
        image2.image = [UIImage imageNamed:@"ic_marks"];
        image2.hidden = NO;
        
        button4.enabled = NO;
        image4.image = [UIImage imageNamed:@"ic_marks"];
        image4.hidden = NO;
        button4.selected = YES;
    }else{
        if ([model1.state isEqualToString:@"0"]) {
            image2.hidden = YES;
            button4.enabled = YES;
        }
    }
    if ([model3.state isEqualToString:@"1"]){
        button1.enabled = NO;
        image1.image = [UIImage imageNamed:@"markswhite"];
        image1.hidden = NO;
        
        button3.enabled = YES;
        image3.image = [UIImage imageNamed:@"ic_marks"];
        image3.hidden = NO;
    }else{
        if ([model1.state isEqualToString:@"0"]) {
            image3.hidden = YES;
        }
    }
    if ([model4.state isEqualToString:@"1"]){
        if ([model2.state isEqualToString:@"1"]) {
            button4.enabled = NO;
        }else{
            button4.enabled = YES;
        }
        button1.enabled = NO;
        image1.image = [UIImage imageNamed:@"markswhite"];
        image1.hidden = NO;

        image4.image = [UIImage imageNamed:@"ic_marks"];
        image4.hidden = NO;
    }else{
        if ([model1.state isEqualToString:@"0"]) {
            image4.hidden = YES;
        }
    }
    
    if ([model2.state isEqualToString:@"0"] && [model3.state isEqualToString:@"0"] && [model4.state isEqualToString:@"0"]) {
        button1.enabled = YES;
    }
}

#pragma amrk - 按钮点击事件
- (void)chooseEat:(UIButton *)cellBtn{
    
    V2_EatModel *model1 = [_eatingArray objectAtIndex:0];
    V2_EatModel *model2 = [_eatingArray objectAtIndex:1];
    V2_EatModel *model3 = [_eatingArray objectAtIndex:2];
    V2_EatModel *model4 = [_eatingArray objectAtIndex:3];
    
    if (cellBtn.tag==1) {
        if ([model1.state isEqualToString:@"0"]) {
            model1.state = @"1";
        }else{
            model1.state = @"0";
        }
    }else if (cellBtn.tag == 2){
        if ([model2.state isEqualToString:@"0"]) {
            model2.state = @"1";
            
            model4.state = @"1";
        }else{
            model2.state = @"0";
        }
    }else if (cellBtn.tag == 3){
        if ([model3.state isEqualToString:@"0"]) {
            model3.state = @"1";
        }else{
            model3.state = @"0";
        }
    }else if (cellBtn.tag == 4){
        if ([model4.state isEqualToString:@"0"]) {
            model4.state = @"1";
        }else{
            model4.state = @"0";
        }
    }
    
    [self setSelectButton];
    
    NSLog(@"%@0----",_eatingArray);
    [self.selecteArr removeAllObjects];
    for (V2_EatModel *model in _eatingArray) {
        if ([model.state isEqualToString:@"1"]) {
            [self.selecteArr addObject:model.name];
        }
    }
    
    
    NSLog(@"%@",self.selecteArr);
    NSLog(@"%lu",(unsigned long)self.selecteArr.count);
    if (self.Delegate) {
        [self.Delegate delegateSelecteArray:_eatingArray];
    }
}

@end
