//
//  AddToListView.h
//  Qraved
//
//  Created by System Administrator on 9/25/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyListDelegate.h"
#import "EGORefreshTableFooterView.h"

@class IMGRestaurant;

@interface AddToListView : UIView <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,EGORefreshTableFooterDelegate>

@property(nonatomic,strong) IMGRestaurant *restaurant;
@property(nonatomic,strong) UITextField *textField;
@property(nonatomic,strong) UIButton *saveBtn;
@property(nonatomic,strong) UITableView *tableView;
@property(nonatomic,strong) NSMutableArray *mylists;
@property(nonatomic,strong) EGORefreshTableFooterView *refreshView;
@property(nonatomic,weak) NSObject<MyListDelegate> *delegate;
@property(nonatomic,assign) BOOL hasData;
@property(nonatomic,assign) BOOL startHasData;

@property(nonatomic,retain)NSString* amplitudeType;

-(instancetype)initWithFrame:(CGRect)frame andRestaurant:(IMGRestaurant*)restaurant list:(NSArray*)mylists;

-(void)addRefreshView;
-(void)resetRrefreshViewFrame;
@end
