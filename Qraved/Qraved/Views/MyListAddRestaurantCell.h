//
//  MyListAddRestaurantCell.h
//  Qraved
//
//  Created by root on 9/26/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyListAddRestaurantCell : UITableViewCell

@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *cuisine;
@property(nonatomic,strong) NSString *image;
@property(nonatomic,assign) BOOL inList;
@property(nonatomic,strong) UIImageView *savedImageView;

//-(void)setImageUrl:(NSString*)imageUrl;
- (void)setRestaurantWithDic:(NSDictionary *)restaurant;
@end
