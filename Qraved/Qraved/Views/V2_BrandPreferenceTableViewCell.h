//
//  V2_BrandPreferenceTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/8/8.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "V2_BrandModel.h"
#import "V2_BrandModelSuper.h"
@interface V2_BrandPreferenceTableViewCell : UITableViewCell<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>


@property (nonatomic, strong)  UIButton *btnEat;
@property (nonatomic, strong)  UIImageView  *imageV;
@property (nonatomic, strong)  NSArray  *eatingArray;
@property (nonatomic, strong)  NSMutableArray  *normalArray;
@property (nonatomic, strong)  UICollectionView* photoCollectionView;
@property (nonatomic, strong)  UIImageView *iamgeView;
@property (nonatomic, strong)  NSString  *section;
@property (nonatomic, strong)  NSArray  *brandTitleArray;


@property (nonatomic,assign) CGFloat cellHeight;

- (void)createUI:(NSArray *)iconArray brandTitleArray:(NSArray *)brandTitleArray;
@end
