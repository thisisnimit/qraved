//
//  V2_BrandModelSuper.h
//  Qraved
//
//  Created by Adam.zhang on 2017/8/8.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "V2_BrandModel.h"
@interface V2_BrandModelSuper : NSObject

@property (nonatomic, strong)  NSString  *brandType;
@property (nonatomic, strong)  NSArray<V2_BrandModel *>  *brandList;

@end
