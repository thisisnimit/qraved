//
//  AddToListCellView.h
//  Qraved
//
//  Created by System Administrator on 9/25/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddToListCellView : UITableViewCell

@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *image;
@property(nonatomic,assign) BOOL inList;
-(void)setImageUrl:(NSString*)imageUrl;
@end
