//
//  ReviewPublishRatingView.h
//  Qraved
//
//  Created by apple on 17/4/1.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLStarRatingControl.h"
#import "QRStarComponent.h"


@interface ReviewPublishRatingView : UIView<QRStarRatingDelegate>
@property (nonatomic,strong)DLStarRatingControl *startRating;
@property (nonatomic, strong) QRStarComponent *starComponent;
@property (nonatomic, strong) UILabel *lblRating;
@property(nonatomic, copy) void (^ratingRestaurant)(int rating);

- (void)updateRating:(int)rating;
@end
