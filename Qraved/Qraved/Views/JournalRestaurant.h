//
//  JournalRestaurant.h
//  Qraved
//
//  Created by System Administrator on 9/2/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CuisineAndDistrict.h"
#import "YTPlayerView.h"
#import "TYAttributedLabel.h"

@class IMGRestaurant;
@class IMGDiscover;

@protocol JournalRestaurantDelegate <NSObject>

@optional

-(void)seeMoreRestaurantPhotos:(IMGRestaurant*)rest;
-(void)JournalRestaurantBtnClick:(NSNumber*)resturantId withOfferId:(NSNumber*)offerId;
-(void)JournalRestaurantSeeMenu:(IMGRestaurant*)restaurant;
-(void)JournalRestaurantSeeMap:(IMGRestaurant*)restaurant;
-(void)JournalRestaurantBookNow:(IMGRestaurant*)restaurant;
-(void)JournalRestaurantViewDetail:(IMGRestaurant*)restaurant;
-(void)JournalRestaurantViewOffer:(IMGRestaurant*)restaurant;
-(void)journalCreditClick:(NSString*)creditUrl;
-(void)journalCuisineClick:(IMGDiscover*)discover;
-(void)journalDistrictClick:(IMGDiscover*)discover;
-(void)JournalRestaurantLike:(NSNumber*)restaurantId liked:(BOOL)liked andBlock:(void(^)(BOOL))block;
-(void)JournalRestaurantSaved:(IMGRestaurant*)restaurant andView:(UIView *)journalView;
-(void)JournalRestaurantImageClick:(UITapGestureRecognizer*)tap withTargatURL:(NSString*)url andRestaurantId:(NSNumber *)restaurantId;
-(void)JournalRestaurantContenClik:(NSString*)url;
- (void)journalRestaurantGojekTapped:(IMGRestaurant *)restaurant;


@end

@interface JournalRestaurant : UIView<CuisineAndDistrictDelegate,UIWebViewDelegate,TYAttributedLabelDelegate>

@property(nonatomic,strong) NSNumber *order;
@property(nonatomic,strong) NSNumber *restaurantId;
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *photo;
@property(nonatomic,strong) NSString *credit;
@property(nonatomic,strong) NSString *creditType;
@property(nonatomic,strong) NSString *url;
@property(nonatomic,strong) NSString *content;
@property(nonatomic,strong) NSString *button;
@property(nonatomic,strong) NSDictionary *restaurantDic;
@property(nonatomic,assign)CGFloat viewHeight;
@property(nonatomic,strong) NSNumber *titleBackgroundCopyViewY;
@property(nonatomic,strong) UIView *titleBackgroundCopyView;
@property(nonatomic,retain)NSString* videoURL;
@property(nonatomic,retain)NSString* photoTarget;
@property(nonatomic,assign)BOOL isFirstGIF;
@property(nonatomic,retain)NSString* GifCache;
@property(nonatomic,assign)float Imageheight;
@property(nonatomic,assign)float Imageweight;
@property(nonatomic,retain)YTPlayerView* webview;
@property(nonatomic,strong) NSNumber *selectedOfferId;
@property(nonatomic,strong) NSString *landPage;

@property(nonatomic,weak) id<JournalRestaurantDelegate> delegate;

-(instancetype)initWithDictionary:(NSDictionary*)data delegate:(id<JournalRestaurantDelegate>)delegate menuPhotoCount:(NSNumber *)menuPhotoCount;
- (void)refreshSavedButton:(BOOL)isSaved;

@end
