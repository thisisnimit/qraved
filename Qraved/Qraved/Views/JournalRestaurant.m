//
//  JournalRestaurant.m
//  Qraved
//
//  Created by System Administrator on 9/2/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "JournalRestaurant.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import "UILabel+Helper.h"
#import "UIColor+Hex.h"
#import "NSString+Helper.h"
#import "UIImageView+WebCache.h"
#import "UIColor+Helper.h"
#import "IMGRestaurant.h"
#import "JournalImageBtn.h"
#import "AppDelegate.h"
#import "RestaurantHandler.h"
#import "DBManager.h"
#import "SendCall.h"
#import "SpecialOffersViewController.h"
#import "IMGDiscover.h"
#import "YTPlayerView.h"
#import "UIImage+GIF.h"
#import "UIImage+animatedGIF.h"
#import "UIImage+Resize.h"
#import "DLStarRatingControl.h"
@implementation JournalRestaurant{
    IMGRestaurant *restaurant;
    NSInteger index;
    NSInteger count;
    NSString *cuisine;
    NSNumber *cuisineId;
    NSString *district;
    NSNumber *districtId;
    NSNumber *score;
    NSNumber *ratingCount;
    NSNumber *priceLevel;
    NSNumber *_menuPhotoCount;
    NSInteger likeCount;
    BOOL liked;
    NSInteger saveCount;
    BOOL saved;
    NSInteger reviewCount;
    NSInteger menuCount;
    NSDictionary *restaurantDic;

    NSString *offer;
    NSString *tagline;
    BOOL hasPhone;
    BOOL canBook;
    BOOL hasMenu;

    NSMutableArray *cRects;
    NSMutableArray *dRects;

    NSString *phoneNumber;

    JournalImageBtn *likeBtn;
    JournalImageBtn *saveBtn;
    JournalImageBtn *favBtn;
    JournalImageBtn *savedBtn;
    JournalImageBtn *reviewBtn;
    CGFloat currentY;
  
//    YTPlayerView* Player;
    UIImageView *imgView;
    UIImageView* defauView;
    NSArray* imagesArr;
    NSArray* arr;
    UIButton *btnSaved;
    UIButton *btnSavedCopy;
    IMGRestaurant *currentRestaurant;
}

-(instancetype)initWithDictionary:(NSDictionary*)data delegate:(id<JournalRestaurantDelegate>)delegate menuPhotoCount:(NSNumber *)menuPhotoCount{
    if(self=[super init]){
        
        _menuPhotoCount = menuPhotoCount;
        self.delegate=delegate;
        [data enumerateKeysAndObjectsUsingBlock:^(id key,id objc,BOOL *stop){
            if([key isEqualToString:@"restaurant"]){
                [self initWithRestaurant:objc];
            }else if([key isEqualToString:@"title"]){
                [self initWithTitle:objc];
            }else if([key isEqualToString:@"content"]){
                [self initWithContent:objc];
            }else if([key isEqualToString:@"photo"]){
                [self initWithPhoto:objc];
            }else if([key isEqualToString:@"button"]){
                [self initWithButton:objc];
            }else if([key isEqualToString:@"end"]){
                //[self initWithAction:objc];
            }else if ([key isEqualToString:@"video"]){
            
                [self initwithVideo:objc];
            
            
            }
        }];
    }
    return self;
}

-(void)initwithVideo:(NSDictionary*)video{
    self.videoURL=[video objectForKey:@"videoUrl"];
    CGFloat sizeWidth=DeviceWidth-LEFTLEFTSET*2;

   YTPlayerView* Player=[[YTPlayerView alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 0, sizeWidth, 200)];
    Player.backgroundColor=[UIColor redColor];
    [self addSubview:Player];
    UIView *coverTitleView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, sizeWidth, 50)];
    coverTitleView.backgroundColor=[UIColor clearColor];
   // coverTitleView.backgroundColor=[UIColor redColor];
    NSString* URL=[[NSString alloc]init];;
    if ([self.videoURL rangeOfString:@"embed/"].location != NSNotFound) {
        NSRange range=  [self.videoURL rangeOfString:@"embed/"];
        URL=[self.videoURL substringFromIndex:range.location+range.length];
    }else if ([self.videoURL rangeOfString:@"https://youtu.be/"].location != NSNotFound){
        NSRange range = [self.videoURL rangeOfString:@"https://youtu.be/"];
        URL = [self.videoURL substringFromIndex:range.location+range.length];
    }else if ([self.videoURL rangeOfString:@"https://www.youtube.com/watch?v="].location != NSNotFound){
        NSRange range = [self.videoURL rangeOfString:@"https://www.youtube.com/watch?v="];
        URL = [self.videoURL substringFromIndex:range.location+range.length];
    }
    
    if ([URL rangeOfString:@"?"].location!=NSNotFound) {
        URL=[URL substringToIndex:[URL rangeOfString:@"?"].location];
    }
    
    
    NSLog(@"==========%@",self.videoURL);
    NSLog(@"----------%@",URL);
    
    BOOL a= [Player loadWithVideoId:URL];
    NSLog(@"%d",a);
    [Player playVideo];

    [Player addSubview:coverTitleView];
    self.webview=Player;
    
    
    
    
  
    currentY=Player.endPointY;
    
    self.viewHeight=currentY;
    
    
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    if([request.URL.scheme isEqualToString:@"loadsuccess"])
    {
        [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.getElementById('url').src='%@';",self.videoURL]];
        
    }
       return true;
}

-(void)initWithRestaurant:(NSDictionary*)restaurant_{
    restaurantDic = restaurant_;
    self.restaurantId=[restaurant_ objectForKey:@"restaurantId"];
    self.title=[restaurant_ objectForKey:@"title"];
    index=((NSNumber*)[restaurant_ objectForKey:@"index"]).integerValue;
    count=((NSNumber*)[restaurant_ objectForKey:@"count"]).integerValue;
    score=[restaurant_ objectForKey:@"score"];
    ratingCount = [restaurant_ objectForKey:@"ratingCount"];
    district=[[restaurant_ objectForKey:@"district"] filterHtml];
    districtId=[restaurant_ objectForKey:@"districtId"];
    cuisine=[[restaurant_ objectForKey:@"cuisine"] filterHtml];
    cuisineId=[restaurant_ objectForKey:@"cuisineId"];
    priceLevel=[restaurant_ objectForKey:@"priceLevel"];
    tagline=[restaurant_ objectForKey:@"tagline"];
    offer=[restaurant_ objectForKey:@"offer"];
    phoneNumber=[restaurant_ objectForKey:@"phoneNumber"];
    likeCount=((NSNumber*)[restaurant_ objectForKey:@"likeCount"]).integerValue;
    saveCount=((NSNumber*)[restaurant_ objectForKey:@"saveCount"]).integerValue;
    reviewCount=((NSNumber*)[restaurant_ objectForKey:@"reviewCount"]).integerValue;
    menuCount=((NSNumber*)[restaurant_ objectForKey:@"menuCount"]).integerValue;
    canBook=((NSNumber*)[restaurant_ objectForKey:@"bookStatus"]).boolValue;
    liked=((NSNumber*)[restaurant_ objectForKey:@"liked"]).boolValue;
    saved=((NSNumber*)[restaurant_ objectForKey:@"saved"]).boolValue;
    _menuPhotoCount =((NSNumber*)[restaurant_ objectForKey:@"menuPhotoCount"]);
    self.viewHeight=0.0f;
    [self loadJournalRest];
    self.tag = [self.restaurantId intValue];
    //[self loadJournalRestAction_1];
}

-(void)initWithTitle:(NSDictionary*)title{
    self.title=[title objectForKey:@"title"];
    self.restaurantId=[title objectForKey:@"restaurantId"];
    [self loadJournalTitle];
}

-(void)initWithContent:(NSDictionary*)content{
    self.content=[content objectForKey:@"content"];
    self.restaurantId=[content objectForKey:@"restaurantId"];
    [self loadJournalContent];
}

-(void)initWithPhoto:(NSDictionary*)photo{
    self.photo=[photo objectForKey:@"photo"];
    self.order=[photo objectForKey:@"order"];
    self.credit=[photo objectForKey:@"credit"];
    self.creditType=[photo objectForKey:@"creditType"];
    self.url=[photo objectForKey:@"url"];
    self.photoTarget=[photo objectForKey:@"photoTarget"];
    self.Imageheight=[[photo objectForKey:@"height"] floatValue];
    self.Imageweight=[[photo objectForKey:@"width"] floatValue];
//    self.photoTarget=@"";
//    self.photo=[NSString stringWithFormat:@"http://s1.dwstatic.com/group1/M00/8B/84/0215c2a3363883c62e0f1678dd575358.gif"];

    imagesArr=[[NSArray alloc]init];
    
    self.restaurantId=[photo objectForKey:@"restaurantId"];
    [self loadJournalPhoto];
}

-(void)initWithButton:(NSDictionary*)button{
    self.button=[button objectForKey:@"button"];
    self.restaurantId=[button objectForKey:@"restaurantId"];
    self.landPage=[button objectForKey:@"landingPage"];
    self.selectedOfferId=[button objectForKey:@"SelectPromoId"];
    self.restaurantDic = [button objectForKey:@"restaurant"];
    
    [self loadJournalButton];
}

-(void)initWithAction:(NSDictionary*)end_{
    NSDictionary *restaurant_=[end_ objectForKey:@"restaurant"];
    self.restaurantId=[restaurant_ objectForKey:@"restaurantId"];
    self.title=[restaurant_ objectForKey:@"title"];
    phoneNumber=[restaurant_ objectForKey:@"phoneNumber"];
    tagline=(NSString*)[restaurant_ objectForKey:@"tagline"];
    offer=[restaurant_ objectForKey:@"offer"];
    likeCount=((NSNumber*)[restaurant_ objectForKey:@"likeCount"]).integerValue;
    saveCount=((NSNumber*)[restaurant_ objectForKey:@"saveCount"]).integerValue;
    reviewCount=((NSNumber*)[restaurant_ objectForKey:@"reviewCount"]).integerValue;
    menuCount=((NSNumber*)[restaurant_ objectForKey:@"menuCount"]).integerValue;
    canBook=((NSNumber*)[restaurant_ objectForKey:@"bookStatus"]).boolValue;
    liked=((NSNumber*)[restaurant_ objectForKey:@"liked"]).boolValue;
    saved=((NSNumber*)[restaurant_ objectForKey:@"saved"]).boolValue;
    _menuPhotoCount =((NSNumber*)[restaurant_ objectForKey:@"menuPhotoCount"]);
//    [self loadJournalRestOffer];
    [self loadJournalRestAction_2];
}

-(void)loadJournalRest{
    CGFloat currentX=0.0f;
    currentY=0.0f;

    UIView *spaceView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 7)];
    spaceView.backgroundColor = [UIColor colorEBEBEB];
    [self addSubview:spaceView];
    currentY=spaceView.endPointY+10;
//   CGFloat titleWidth=24.0f;
    
    UIView *titleBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, currentY, DeviceWidth, 100)];
    titleBackgroundView.backgroundColor = [UIColor whiteColor];
    [self addSubview:titleBackgroundView];
    
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET,0,DeviceWidth-LEFTLEFTSET*2-44,200)];
    titleLabel.text=self.title;
    titleLabel.numberOfLines = 0;
    titleLabel.frame=CGRectMake(LEFTLEFTSET,0,DeviceWidth-LEFTLEFTSET*2-44,titleLabel.expectedHeight);
    titleLabel.font=[UIFont systemFontOfSize:14];
    titleLabel.textColor=[UIColor color333333];
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewDetail:)];
    tap.numberOfTapsRequired=1;
    tap.numberOfTouchesRequired=1;
    [titleBackgroundView addGestureRecognizer:tap];
    titleBackgroundView.userInteractionEnabled=YES;
    CGSize size = [titleLabel.text sizeWithFont:titleLabel.font constrainedToSize:CGSizeMake(titleLabel.frame.size.width, 10000) lineBreakMode:NSLineBreakByWordWrapping];
    titleLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y, size.width, size.height);
    
    [titleBackgroundView addSubview:titleLabel];
    DLStarRatingControl *ratingScoreControl=[[DLStarRatingControl alloc]initWithFrame:CGRectMake(LEFTLEFTSET, titleLabel.endPointY+5, 73, 13) andStars:5 andStarWidth:13 isFractional:YES spaceWidth:2];
    ratingScoreControl.userInteractionEnabled=NO;
    [ratingScoreControl updateRating:score];
    [titleBackgroundView addSubview:ratingScoreControl];
    
    UILabel *lblRating = [[UILabel alloc] initWithFrame:CGRectMake(93, titleLabel.endPointY+5, DeviceWidth-110, 14)];
    lblRating.font = [UIFont systemFontOfSize:12];
    lblRating.textColor = [UIColor color999999];
    lblRating.text = [NSString stringWithFormat:@"(%@)",ratingCount];
    [titleBackgroundView addSubview:lblRating];
    
    NSString *dollar=nil;
    switch(priceLevel.intValue){
        case 1:dollar=@"Below 100K";break;
        case 2:dollar=@"100K - 200K";break;
        case 3:dollar=@"200K - 300K";break;
        case 4:dollar=@"Start from 300K";break;
    }

    NSString *cuisineAndDistrictText=@"";

    if ([cuisine isEqualToString:@""]) {
        cuisineAndDistrictText = [NSString stringWithFormat:@"%@ • %@",district,dollar];
    }else{
        cuisineAndDistrictText = [NSString stringWithFormat:@"%@ • %@ • %@",cuisine,district,dollar];
    }


    UIFont *cuisineFont = [UIFont systemFontOfSize:12];
    CGSize cdSize=[cuisineAndDistrictText sizeWithFont:cuisineFont constrainedToSize:CGSizeMake(DeviceWidth-30,cuisineFont.lineHeight*2) lineBreakMode:NSLineBreakByTruncatingTail];
    UILabel *cuisineAndDistrictLabel=[[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET,ratingScoreControl.endPointY+5,DeviceWidth-30,cdSize.height)];
    cuisineAndDistrictLabel.font=[UIFont systemFontOfSize:12];
    cuisineAndDistrictLabel.textColor = [UIColor color999999];
    cuisineAndDistrictLabel.lineBreakMode=NSLineBreakByTruncatingTail;
    cuisineAndDistrictLabel.numberOfLines=2;
//    cuisineAndDistrictLabel.tag=111;
//    cuisineAndDistrictLabel.userInteractionEnabled=YES;
    cuisineAndDistrictLabel.text=cuisineAndDistrictText;
    [titleBackgroundView addSubview:cuisineAndDistrictLabel];
    
    
    btnSaved = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSaved.frame = CGRectMake(DeviceWidth-61, 10, 46, 40);
    if (saved) {
        [btnSaved setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
    }else{
        [btnSaved setImage:[UIImage imageNamed:@"ic_heart_home"] forState:UIControlStateNormal];
    }
    btnSaved.imageEdgeInsets = UIEdgeInsetsMake(0, 11, 0, 0);
    [btnSaved addTarget:self action:@selector(saveClick:) forControlEvents:UIControlEventTouchUpInside];
    [titleBackgroundView addSubview:btnSaved];
    
    
    CGRect titleBackgroundViewFrame = titleBackgroundView.frame;
    titleBackgroundViewFrame.size.height = cuisineAndDistrictLabel.endPointY+10;
    titleBackgroundView.frame = titleBackgroundViewFrame;
    
    self.titleBackgroundCopyViewY = [NSNumber numberWithFloat:titleBackgroundView.frame.origin.y];
    [self createTitleBackgroundCopyView:titleBackgroundView :titleLabel :cuisineAndDistrictLabel savedBtn:btnSaved];
    
    currentY=titleBackgroundView.endPointY;
    self.viewHeight=currentY;
}

-(void)createTitleBackgroundCopyView:(UIView*)view :(UILabel*)label :(UILabel *)cdLabel savedBtn:(UIButton *)button{
    UIView *titleBackgroundView = [[UIView alloc] initWithFrame:view.frame];
    titleBackgroundView.backgroundColor = [UIColor whiteColor];
    titleBackgroundView.hidden = YES;
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:label.frame];
    titleLabel.text=self.title;
    titleLabel.numberOfLines = 0;
    titleLabel.font=[UIFont systemFontOfSize:14];
    titleLabel.textColor=[UIColor color333333];
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewDetail:)];
    tap.numberOfTapsRequired=1;
    tap.numberOfTouchesRequired=1;
    [titleBackgroundView addGestureRecognizer:tap];
    titleBackgroundView.userInteractionEnabled=YES;
    [titleBackgroundView addSubview:titleLabel];
    
    DLStarRatingControl *ratingScoreControl=[[DLStarRatingControl alloc]initWithFrame:CGRectMake(LEFTLEFTSET, titleLabel.endPointY+5, 73, 13) andStars:5 andStarWidth:13 isFractional:YES spaceWidth:2];
    ratingScoreControl.userInteractionEnabled=NO;
    [ratingScoreControl updateRating:score];
    [titleBackgroundView addSubview:ratingScoreControl];
    
    UILabel *lblRating = [[UILabel alloc] initWithFrame:CGRectMake(93, titleLabel.endPointY+5, DeviceWidth-110, 14)];
    lblRating.font = [UIFont systemFontOfSize:12];
    lblRating.textColor = [UIColor color999999];
    lblRating.text = [NSString stringWithFormat:@"(%@)",ratingCount];
    [titleBackgroundView addSubview:lblRating];
    
    
    
    UILabel *cuisineAndDistrictLabel=[[UILabel alloc] initWithFrame:cdLabel.frame];
    cuisineAndDistrictLabel.font=[UIFont systemFontOfSize:12];
    cuisineAndDistrictLabel.textColor = [UIColor color999999];
    cuisineAndDistrictLabel.lineBreakMode=NSLineBreakByTruncatingTail;
    cuisineAndDistrictLabel.numberOfLines=2;
    //    cuisineAndDistrictLabel.tag=111;
    //    cuisineAndDistrictLabel.userInteractionEnabled=YES;
    cuisineAndDistrictLabel.text=cdLabel.text;
    [titleBackgroundView addSubview:cuisineAndDistrictLabel];
    
    
    btnSavedCopy = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSavedCopy.frame = button.frame;
    if (saved) {
        [btnSavedCopy setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
    }else{
        [btnSavedCopy setImage:[UIImage imageNamed:@"ic_heart_home"] forState:UIControlStateNormal];
    }
    btnSavedCopy.imageEdgeInsets = UIEdgeInsetsMake(0, 11, 0, 0);
    [btnSavedCopy addTarget:self action:@selector(saveClick:) forControlEvents:UIControlEventTouchUpInside];
    [titleBackgroundView addSubview:btnSavedCopy];
    
    self.titleBackgroundCopyView = titleBackgroundView;
}

-(void)loadJournalTitle{
    
    NSString *contentLalelString = [self flattenHTML:self.title];
    contentLalelString = [contentLalelString removeHTML];
    contentLalelString =[contentLalelString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(LEFTLEFTSET,0,DeviceWidth-LEFTLEFTSET*2,DeviceHeight)];
    titleLabel.numberOfLines=0;
    titleLabel.text=contentLalelString;
    titleLabel.font=[UIFont boldSystemFontOfSize:16];
    titleLabel.frame=CGRectMake(LEFTLEFTSET,0,DeviceWidth-LEFTLEFTSET*2,titleLabel.expectedHeight);
    [self addSubview:titleLabel];
    self.viewHeight=titleLabel.endPointY;
}

-(void)loadJournalRestAction_1{
    
    CGFloat actionHeight=16;

    CGFloat maxWidth=DeviceWidth-LEFTLEFTSET*2;
    int i=0;
    CGFloat height=0;
   // if (likeCount!=0) {
        
    likeBtn=[[JournalImageBtn alloc] initWithFrame:CGRectMake(0,currentY,20,actionHeight) image:@"favorite" size:CGSizeMake(12,12) text:[NSString stringWithFormat:@"%ld",(long)likeCount] margin:5 target:self action:nil];
    likeBtn.backgroundColor=[UIColor clearColor];
    likeBtn.label.textColor=[UIColor color999999];
    likeBtn.label.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
    likeBtn.padding_right=5;
    likeBtn.hidden=YES;
    [self addSubview:likeBtn];
    
    if (likeCount!=0) {
        likeBtn.hidden=NO;
        height=likeBtn.frame.size.height;
        i++;
    }

   // }
    //if (saveCount!=0) {
        
    saveBtn=[[JournalImageBtn alloc] initWithFrame:CGRectMake((20+14)*i,currentY,maxWidth,actionHeight) image:@"journalsave" size:CGSizeMake(12,12) text:[NSString stringWithFormat:@"%ld",(long)saveCount] margin:5 target:self action:nil];
    saveBtn.backgroundColor=[UIColor clearColor];
    saveBtn.hidden=YES;
    saveBtn.label.textColor=[UIColor color999999];
    saveBtn.label.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
    saveBtn.padding_right=5;
    [self addSubview:saveBtn];
    if (saveCount!=0) {
        saveBtn.hidden=NO;
        height=saveBtn.frame.size.height;
        i++;
    }

   // }
    if (reviewCount!=0) {
        
   
    reviewBtn=[[JournalImageBtn alloc] initWithFrame:CGRectMake((20+14)*i,currentY,maxWidth,actionHeight) image:@"review" size:CGSizeMake(12,12) text:[NSString stringWithFormat:@"%ld",(long)reviewCount] margin:5 target:self action:nil];
    reviewBtn.backgroundColor=[UIColor clearColor];
    reviewBtn.label.textColor=[UIColor color999999];
    reviewBtn.label.baselineAdjustment=UIBaselineAdjustmentAlignCenters;
    [self addSubview:reviewBtn];
        height=reviewBtn.frame.size.height;
    }
    [self freshLikeSaveReviewBtn];
    actionHeight=25;
    
    float favBtnY = 26.0;
    if(liked){
        favBtn=[[JournalImageBtn alloc] initWithFrame:CGRectMake(0,currentY+favBtnY,maxWidth,actionHeight) image:@"favorite_4" text:@"" margin:2 target:self action:@selector(likeClick:)];
        favBtn.backgroundColor=[UIColor colorC2060A];
    }else{
        favBtn=[[JournalImageBtn alloc] initWithFrame:CGRectMake(0,currentY+favBtnY,maxWidth,actionHeight) image:@"favorite_3" text:@"" margin:2 target:self action:@selector(likeClick:)];
        favBtn.backgroundColor=[UIColor colorEEEEEE];
    }
    favBtn.padding_left=10;
    favBtn.padding_right=10;
    [self addSubview:favBtn];

    if(saved){
        savedBtn=[[JournalImageBtn alloc] initWithFrame:CGRectMake(favBtn.endPointX+5,favBtnY+currentY,maxWidth,actionHeight) image:@"journalsave_1" text:L(@"Saved") margin:2 target:self action:@selector(saveClick:)];
        savedBtn.label.textColor=[UIColor whiteColor];
        savedBtn.backgroundColor=[UIColor colorFFC000];
    }else{
        savedBtn=[[JournalImageBtn alloc] initWithFrame:CGRectMake(favBtn.endPointX+5,favBtnY+currentY,maxWidth,actionHeight) image:@"journalsave_2" text:L(@"Save to list") margin:2 target:self action:@selector(saveClick:)];
        savedBtn.label.textColor=[UIColor color999999];
        savedBtn.backgroundColor=[UIColor colorEEEEEE];
    }
    savedBtn.padding_left=10;
    savedBtn.padding_right=10;
    // savedBtn.font=[UIFont fontWithName:@"OpenSans" size:12];
    
    [self addSubview:savedBtn];

    currentY=favBtn.endPointY;

    self.viewHeight=currentY;
}

-(void)loadJournalPhoto{
    CGFloat sizeWidth=DeviceWidth;
    self.Imageheight = sizeWidth*self.Imageheight/self.Imageweight;

    
    if (self.Imageheight>0) {
        imgView=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,sizeWidth,self.Imageheight)];

    }else{
        imgView=[[UIImageView alloc] initWithFrame:CGRectMake(0,0,sizeWidth,DeviceWidth*0.67)];

    }
    imgView.contentMode = UIViewContentModeScaleAspectFill;
    imgView.clipsToBounds = YES;
    if ([self.photo rangeOfString:@".gif"].location==NSNotFound) {
        UIImage *placeHoderImage =[UIImage imageNamed:DEFAULT_IMAGE_STRING2];

        [imgView sd_setImageWithURL:[NSURL URLWithString:[self.photo returnFullImageUrlWithWidth:sizeWidth]] placeholderImage:placeHoderImage];

//           [imgView setImageWithURL:[NSURL URLWithString:[self.photo returnFullImageUrlWithWidth:sizeWidth]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] sizeFitWidth:sizeWidth];
    }else{
        int height = 0;
        if ( (self.Imageheight == 0.0) || (self.Imageweight == 0.0) ) {
            height = 150;
        }else{
            height=imgView.frame.size.width*self.Imageheight/self.Imageweight;
        }
        imgView.frame=CGRectMake(0,0,sizeWidth,height);
        UIImage *placeHoderImage =[UIImage imageNamed:DEFAULT_IMAGE_STRING2];
        [imgView sd_setImageWithURL:[NSURL URLWithString:[self.photo returnFullImageUrlWithWidth:sizeWidth]] placeholderImage:placeHoderImage];

//            [imgView setImageWithURL:[NSURL URLWithString:[self.photo returnFullImageUrlWithWidth:sizeWidth andHeight:height]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] sizeFitWidth:sizeWidth];
            UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,0,sizeWidth,150)];
            [webView setScalesPageToFit:YES];
            webView.scrollView.bounces = NO;
            webView.userInteractionEnabled = YES;
          webView.scrollView.showsHorizontalScrollIndicator = NO;
          webView.scrollView.showsVerticalScrollIndicator = NO;
        
            webView.frame=imgView.frame;
            [self addSubview:webView];
        UIView * coverView=[[UIView alloc]initWithFrame:webView.bounds];
        coverView.tag=[self.order integerValue];

        coverView.backgroundColor=[UIColor clearColor];
        UITapGestureRecognizer * coverTap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageClick:)];
        coverTap.numberOfTapsRequired=1;
        coverTap.numberOfTouchesRequired=1;

        [coverView addGestureRecognizer:coverTap];
        
        [webView addSubview:coverView];

            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_async(queue, ^{
                NSString *filePath =[NSString stringWithFormat:@"%@/%@.gif",qravedGIFCachePath,[self.photo MD5String]];
                
                NSFileManager *fileManager = [NSFileManager defaultManager];
                NSData* gifData;
                if ([fileManager fileExistsAtPath:filePath]) {
                    gifData = [NSData dataWithContentsOfFile:filePath];
                }else{
                    gifData =[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",QRAVED_WEB_IMAGE_SERVER,self.photo]]];
                    [gifData writeToFile:filePath  atomically:NO];
                }
                dispatch_sync(dispatch_get_main_queue(), ^{
                    [webView loadData:gifData MIMEType:@"image/gif" textEncodingName:@"" baseURL:[NSURL URLWithString:@""]];
                    [imgView removeFromSuperview];
                    

                });
            });
        }
    imgView.tag=[self.order integerValue];
    UITapGestureRecognizer *imgTap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageClick:)];
    imgTap.numberOfTapsRequired=1;
    imgTap.numberOfTouchesRequired=1;
    imgView.userInteractionEnabled=YES;
    [imgView addGestureRecognizer:imgTap];
    [self addSubview:imgView];
    currentY=imgView.endPointY;

    self.viewHeight=currentY;

    CGFloat seemoreWidth=0;
    if([self.restaurantId intValue]!=0){
        seemoreWidth=160;
        UIButton *btnSeemore = [UIButton buttonWithType:UIButtonTypeCustom];
        btnSeemore.frame = CGRectMake((DeviceWidth-seemoreWidth)/2,self.viewHeight-7-30,seemoreWidth,30);
        [btnSeemore setBackgroundImage:[UIImage imageNamed:@"ic_gradient_button"] forState:UIControlStateNormal];
        [btnSeemore setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnSeemore setTitle:L(@"VIEW ALL PHOTOS") forState:UIControlStateNormal];
        btnSeemore.titleLabel.font = [UIFont boldSystemFontOfSize:12];
        btnSeemore.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [btnSeemore addTarget:self action:@selector(seeMorePhotos:) forControlEvents:UIControlEventTouchUpInside];
        [imgView addSubview:btnSeemore];
        btnSeemore.layer.masksToBounds = YES;
        btnSeemore.layer.cornerRadius = 15;

    }

    if(self.credit && ![self.credit isEqual:[NSNull null]]&&![self.credit isEqualToString:@""]){
        UILabel *credit=[[UILabel alloc] initWithFrame:CGRectMake(0,currentY+5,DeviceWidth-LEFTLEFTSET*2,DeviceHeight)];
        credit.textAlignment = NSTextAlignmentCenter;
        credit.text=[NSString stringWithFormat:L(@"Photo source : %@"),self.credit];
        credit.numberOfLines=0;
        
        credit.font=[UIFont systemFontOfSize:10];
        CGSize size = [credit.text sizeWithFont:credit.font constrainedToSize:CGSizeMake(DeviceWidth-LEFTLEFTSET*2, 10000) lineBreakMode:NSLineBreakByWordWrapping];
        credit.frame = CGRectMake(credit.frame.origin.x, credit.frame.origin.y, DeviceWidth-LEFTLEFTSET*2, size.height);
        credit.textColor=[UIColor color999999];
        credit.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(journalCreditClick:)];
        tapGesture.numberOfTouchesRequired = 1;
        tapGesture.numberOfTapsRequired = 1;
        [credit addGestureRecognizer:tapGesture];
        [self addSubview:credit];
        self.viewHeight=credit.endPointY;
    }
}

-(void)loadJournalContent{
    if(!self.content) return;
//    CGFloat sizeWidth=DeviceWidth-LEFTLEFTSET*2;
    NSString *contentLalelString = [self flattenHTML:self.content];
    contentLalelString = [contentLalelString removeHTML];
    contentLalelString =[contentLalelString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    UILabel *contentLabel = [[UILabel alloc] init];
    contentLabel.text = contentLalelString;
    contentLabel.numberOfLines = 0;
    contentLabel.font = [UIFont systemFontOfSize:14];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentLalelString attributes:@{NSKernAttributeName:@(0.1)}];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];

    [paragraphStyle setLineSpacing:10];//调整行间距

    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, contentLalelString.length)];
    contentLabel.attributedText = attributedString;
    
    contentLabel.frame = CGRectMake(LEFTLEFTSET, 0, DeviceWidth - LEFTLEFTSET*2, 10000);
    
    CGSize size = CGSizeMake(contentLabel.frame.size.width, 500000);
    
    CGSize labelSize = [contentLabel sizeThatFits:size];
//    CGRect frame = [contentLalelString boundingRectWithSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET,1000) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}context:nil];
    contentLabel.frame = CGRectMake(contentLabel.frame.origin.x, contentLabel.frame.origin.y, labelSize.width,labelSize.height);

    
    currentY = contentLabel.endPointY;
    self.viewHeight = currentY;

            //helvetica neue LT,

    TYAttributedLabel* titleLabel = [[TYAttributedLabel alloc] init];
    //        titleLabel.font = [UIFont systemFontOfSize:18];
    titleLabel.numberOfLines = 0;
    titleLabel.delegate = self;
    titleLabel.characterSpacing = 0.1;
    [titleLabel setLineBreakMode:kCTLineBreakByWordWrapping];
    titleLabel.font = [UIFont systemFontOfSize:14];
    titleLabel.textColor = [UIColor color333333];
    titleLabel.text = contentLalelString;
    titleLabel.linesSpacing = 10.f;
    [self addSubview:titleLabel];
    titleLabel.frame = CGRectMake(LEFTLEFTSET, 0,DeviceWidth - LEFTLEFTSET*2, labelSize.height);
    NSArray *array=[self.content componentsSeparatedByString:@">"];
    if (array.count>1)
    {
        for (int i = 0 ; i<(array.count-1)/2; i++)
        {
            NSString *separateString=[array objectAtIndex:1+2*i];
            NSString *string = [array objectAtIndex:2*i];
            NSArray *tempArry = [string componentsSeparatedByString:@"<"];
            NSArray *dataArray = [string componentsSeparatedByString:@"\""];
            if (dataArray.count<2) {
                break;
            }
            NSString *dataString = [dataArray objectAtIndex:1];
            string = [tempArry objectAtIndex:0];
            if (separateString.length>3) {
                separateString = [separateString substringToIndex:(separateString.length-3)];
            }
//            separateString = [separateString substringToIndex:(separateString.length-3)];
            TYLinkTextStorage *linkTextStorage = [[TYLinkTextStorage alloc]init];
            linkTextStorage.range = [contentLalelString rangeOfString:separateString];
            linkTextStorage.textColor = [UIColor colorWithHexString:@"#2a80b9"];
            linkTextStorage.linkData =dataString;
            linkTextStorage.underLineStyle = kCTUnderlineStyleNone;
            [titleLabel addTextStorage:linkTextStorage];
        }
    }

}

-(void)loadJournalButton{
    FunctionButton *restaurantButton = [FunctionButton buttonWithType:UIButtonTypeCustom andTitle:[self.button uppercaseString] andBGColor:[UIColor redButtonColor] andTitleColor:[UIColor whiteColor]];
    restaurantButton.frame = CGRectMake(LEFTLEFTSET, 0, DeviceWidth - LEFTLEFTSET *2, FUNCTION_BUTTON_HEIGHT);
    [restaurantButton addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:restaurantButton];
    
    currentRestaurant = [[IMGRestaurant alloc] init];
    [currentRestaurant setValuesForKeysWithDictionary:self.restaurantDic];
    NSDictionary* yesterOpenDic=[self.restaurantDic objectForKey:@"yesterDayOpenTime"];
    currentRestaurant.yesteropenDay=[yesterOpenDic objectForKey:@"openDay"];
    currentRestaurant.yesteropenTime=[yesterOpenDic objectForKey:@"openTime"];
    currentRestaurant.yesterclosedTime=[yesterOpenDic objectForKey:@"closedTime"];
    
    
    NSDictionary* nextOpenDay=[self.restaurantDic objectForKey:@"nextOpenDay"];
    currentRestaurant.nextopenTime=[nextOpenDay objectForKey:@"openTime"];
    currentRestaurant.nextclosedTime=[nextOpenDay objectForKey:@"closedTime"];
    currentRestaurant.nextopenDay=[nextOpenDay objectForKey:@"openDay"];
    
    NSDictionary* secondOpenDic=[self.restaurantDic objectForKey:@"secondOpenDay"];
    currentRestaurant.secondclosedTime=[secondOpenDic objectForKey:@"closedTime"];
    currentRestaurant.secondopenTime=[secondOpenDic objectForKey:@"openTime"];
    currentRestaurant.secondopenDay=[secondOpenDic objectForKey:@"openDay"];
    
    if ([Tools isBlankString:currentRestaurant.goFoodLink]) {
        currentY = restaurantButton.endPointY;
    }else{
        FunctionButton *goFoodButton = [FunctionButton buttonWithType:UIButtonTypeCustom];
        goFoodButton.frame = CGRectMake(15, restaurantButton.endPointY + 10, DeviceWidth - LEFTLEFTSET *2, FUNCTION_BUTTON_HEIGHT);
        [goFoodButton addTarget:self action:@selector(goFoodTapped) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:goFoodButton];
        
        currentY = goFoodButton.endPointY;
    }
     self.viewHeight=currentY;
}

- (void)goFoodTapped{
    if (self.delegate && [self.delegate respondsToSelector:@selector(journalRestaurantGojekTapped:)]) {
        [self.delegate journalRestaurantGojekTapped:currentRestaurant];
    }
}

-(void)loadJournalRestOffer{
    if((tagline && tagline.length && ![tagline isEqual:[NSNull null]]) || (offer && offer.length && ![offer isEqual:[NSNull null]])){
        currentY=5;
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoOffer:)];
        tap.numberOfTapsRequired=1;
        tap.numberOfTouchesRequired=1;
        CGFloat height=0;
        UIView *box=[[UIView alloc] initWithFrame:CGRectMake(0,0,DeviceWidth-LEFTLEFTSET*2,1000)];
        box.layer.borderWidth=1.0f;
        box.layer.borderColor=[UIColor colorC2060A].CGColor;
        box.layer.cornerRadius=5.0f;
        box.userInteractionEnabled=YES;
        [box addGestureRecognizer:tap];
        CGFloat margin=10.0f;
        if(tagline && tagline.length && ![tagline isEqual:[NSNull null]]){
            UILabel *taglineLabel=[[UILabel alloc] initWithFrame:CGRectMake(margin,margin,DeviceWidth-LEFTLEFTSET*2-margin*2,40)];
            taglineLabel.text=[NSString stringWithFormat:@"GET : %@",tagline];
            taglineLabel.textColor=[UIColor colorC2060A];
            taglineLabel.font=[UIFont boldSystemFontOfSize:16];
            taglineLabel.textAlignment=NSTextAlignmentCenter;
            taglineLabel.numberOfLines=0;
            CGSize size = [taglineLabel.text sizeWithFont:taglineLabel.font constrainedToSize:CGSizeMake(taglineLabel.frame.size.width, 10000) lineBreakMode:NSLineBreakByWordWrapping];
            taglineLabel.frame = CGRectMake((DeviceWidth-LEFTLEFTSET*2-size.width)/2, taglineLabel.frame.origin.y, size.width, size.height);
            [box addSubview:taglineLabel];
            currentY=taglineLabel.endPointY;
            height=taglineLabel.frame.size.height;
        }else if(offer && offer.length && ![offer isEqual:[NSNull null]]){
            UILabel *offerLabel=[[UILabel alloc] initWithFrame:CGRectMake(margin,margin,DeviceWidth-LEFTLEFTSET*2-margin*2,40)];
            offerLabel.text=[NSString stringWithFormat:@"GET : %@",offer];
            offerLabel.textColor=[UIColor colorC2060A];
            offerLabel.font=[UIFont boldSystemFontOfSize:16];
            offerLabel.textAlignment=NSTextAlignmentCenter;
            offerLabel.numberOfLines=0;
            [self addSubview:offerLabel];
            CGSize size = [offerLabel.text sizeWithFont:offerLabel.font constrainedToSize:CGSizeMake(offerLabel.frame.size.width, 10000) lineBreakMode:NSLineBreakByWordWrapping];
            offerLabel.frame = CGRectMake((DeviceWidth-LEFTLEFTSET*2-size.width)/2, offerLabel.frame.origin.y, size.width, size.height);
            currentY=offerLabel.endPointY;
            height=offerLabel.frame.size.height;
        }
        box.frame=CGRectMake(0,0,DeviceWidth-LEFTLEFTSET*2,height+margin*2);
        [self addSubview:box];
        currentY=box.endPointY;
        self.viewHeight=box.endPointY;
    }
}    

-(void)loadJournalRestAction_2{
    CGFloat actionPointY=currentY+10;
    CGFloat actionHeight=25;
    CGFloat currentX=0.0f;
    if(phoneNumber && ![phoneNumber isEqual:[NSNull null]]){
        JournalImageBtn *phoneBtn=[[JournalImageBtn alloc] initWithFrame:CGRectMake(LEFTLEFTSET,actionPointY,100,actionHeight) image:@"call" text:@"" margin:5 target:self action:@selector(callPhone:)];
        phoneBtn.padding_left=8;
        phoneBtn.padding_right=8;
        [self addSubview:phoneBtn];
        currentY=phoneBtn.endPointY+10;
        currentX=phoneBtn.endPointX+5;
    }
    if(canBook){
        JournalImageBtn *bookBtn=[[JournalImageBtn alloc] initWithFrame:CGRectMake(currentX,actionPointY,200,actionHeight) image:@"book" text:L(@"Book Now") margin:5 target:self action:@selector(bookNow:)];
        bookBtn.padding_left=8;
        bookBtn.padding_right=8;
        [self addSubview:bookBtn];
        currentY=bookBtn.endPointY+10;
        currentX=bookBtn.endPointX+5;
    }else{
        JournalImageBtn *bookBtn=[[JournalImageBtn alloc] initWithFrame:CGRectMake(currentX,actionPointY,200,actionHeight) image:@"book" text:L(@"View Detail") margin:5 target:self action:@selector(viewDetail:)];
        bookBtn.padding_left=8;
        bookBtn.padding_right=8;
        [self addSubview:bookBtn];
        currentY=bookBtn.endPointY+10;
        currentX=bookBtn.endPointX+5;
    }

//    if(menuCount>0){
//        JournalImageBtn *menuBtn=[[JournalImageBtn alloc] initWithFrame:CGRectMake(currentX,actionPointY,200,actionHeight) image:@"menu" text:L(@"See Menu") margin:5 target:self action:@selector(seeMenu:)];
//        menuBtn.padding_left=8;
//        menuBtn.padding_right=8;
//        [self addSubview:menuBtn];
//        currentY=menuBtn.endPointY+10;
//        currentX=menuBtn.endPointX+5;
//    }
    
    if ([_menuPhotoCount intValue]> 0) {
        JournalImageBtn *menuBtn=[[JournalImageBtn alloc] initWithFrame:CGRectMake(currentX,actionPointY,200,actionHeight) image:@"menu" text:L(@"See Menu") margin:5 target:self action:@selector(seeMenu:)];
        menuBtn.padding_left=8;
        menuBtn.padding_right=8;
        [self addSubview:menuBtn];
        currentY=menuBtn.endPointY+10;
        currentX=menuBtn.endPointX+5;
    }

    JournalImageBtn *mapBtn=[[JournalImageBtn alloc] initWithFrame:CGRectMake(currentX,actionPointY,200,actionHeight) image:@"map" text:L(@"See Map") margin:5 target:self action:@selector(seeMap:)];
    mapBtn.padding_left=8;
    mapBtn.padding_right=8;
    [self addSubview:mapBtn];
    currentY=mapBtn.endPointY;
    currentX=mapBtn.endPointX;
    self.viewHeight=currentY;
}

-(void)likeClick:(UITapGestureRecognizer*)tap{
    if([_delegate respondsToSelector:@selector(JournalRestaurantLike:liked:andBlock:)]){
        [_delegate JournalRestaurantLike:self.restaurantId liked:liked andBlock:^(BOOL status){
            int a=0;
            if(status){
                
                if(liked){
                    likeBtn.text=[NSString stringWithFormat:@"%ld",[likeBtn.label.text integerValue]-1];
                    if ([likeBtn.text isEqualToString:@"0"]) {
                        likeBtn.hidden=YES;
                        likeCount=likeCount-1;
                        a--;
                        saveBtn.frame=CGRectMake((20+14)*(a+1),likeBtn.frame.origin.y,saveBtn.frame.size.width,saveBtn.frame.size.height);
                        reviewBtn.frame=CGRectMake((20+14)*(a+2),likeBtn.frame.origin.y,reviewBtn.frame.size.width,reviewBtn.frame.size.height);
                    }
                    favBtn.imageName=@"favorite_3";
                    favBtn.backgroundColor=[UIColor colorEEEEEE];
                }else{
                    likeBtn.text=[NSString stringWithFormat:@"%ld",[likeBtn.label.text integerValue]+1];
                    favBtn.imageName=@"favorite_4";
                    if (likeCount==0) {
                       
                        likeBtn.hidden=NO;
                        likeCount=likeCount+1;
                        a++;
                        likeBtn.text=[NSString stringWithFormat:@"%ld",likeCount];
                        saveBtn.frame=CGRectMake((20+14)*a,likeBtn.frame.origin.y,saveBtn.frame.size.width,saveBtn.frame.size.height);
                        reviewBtn.frame=CGRectMake((20+14)*(a+1),likeBtn.frame.origin.y,reviewBtn.frame.size.width,reviewBtn.frame.size.height);
                    }
                    favBtn.backgroundColor=[UIColor colorC2060A];
                }
                liked=!liked;
                [self freshLikeSaveReviewBtn];
            }
        }];
    }
}

-(void)saveClick:(UITapGestureRecognizer*)tap{
//    if (saved) {
//        [btnSaved setImage:[UIImage imageNamed:@"ic_heart_home"] forState:UIControlStateNormal];
//        [btnSavedCopy setImage:[UIImage imageNamed:@"ic_heart_home"] forState:UIControlStateNormal];
//        saved=NO;
//    }else{
//        [btnSaved setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
//        [btnSavedCopy setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
//        saved=YES;
//    }
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    [restaurant setValuesForKeysWithDictionary:restaurantDic];
    if([_delegate respondsToSelector:@selector(JournalRestaurantSaved:andView:)]){
        [_delegate JournalRestaurantSaved:restaurant andView:self];
    }
}

- (void)refreshSavedButton:(BOOL)isSaved{
    
    if (isSaved) {
        [btnSaved setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
        [btnSavedCopy setImage:[UIImage imageNamed:@"ic_heart_home_full"] forState:UIControlStateNormal];
        saved=YES;
        
    }else{
        [btnSaved setImage:[UIImage imageNamed:@"ic_heart_home"] forState:UIControlStateNormal];
        [btnSavedCopy setImage:[UIImage imageNamed:@"ic_heart_home"] forState:UIControlStateNormal];
        saved=NO;
    }
    [restaurantDic setValue:[NSNumber numberWithBool:isSaved] forKey:@"saved"];
}

-(void)changeSaveStatus:(BOOL)saved_{
    NSInteger saveCount_=[saveBtn.label.text integerValue];
    if(saved_){
        if(saveCount_==0){
            savedBtn.imageName=@"journalsave_1";
            savedBtn.text=L(@"Saved");
            savedBtn.label.textColor=[UIColor whiteColor];
            savedBtn.backgroundColor=[UIColor colorFFC000];
            saved_=YES;
        }
        saveBtn.text=[NSString stringWithFormat:@"%ld",[saveBtn.label.text integerValue]+1];
    }else{
        saveBtn.text=[NSString stringWithFormat:@"%ld",[saveBtn.label.text integerValue]-1];
        if(saveCount_==1){
            savedBtn.imageName=@"journalsave_2";
            savedBtn.text=L(@"Save to list");
            savedBtn.label.textColor=[UIColor color999999];
            savedBtn.backgroundColor=[UIColor colorEEEEEE];
            saved_=NO;
        }
    }
}

-(void)callPhone:(UITapGestureRecognizer*)tap{
    
    NSMutableDictionary* eventdic=[[NSMutableDictionary alloc]init];
    [eventdic setValue:@"Journal detail page" forKey:@"Location"];
    [eventdic setValue:restaurant.restaurantId forKey:@"Restaurant_ID"];
    [[Amplitude instance] logEvent:@"CL - Phone Number" withEventProperties:eventdic];
    [[Amplitude instance] logEvent:@"CL - Call Restaurant CTA" withEventProperties:eventdic];
     [[AppsFlyerTracker sharedTracker] trackEvent:@"CL - Call Restaurant CTA" withValues:eventdic];
    [SendCall sendCall:phoneNumber];
}

-(void)bookNow:(UITapGestureRecognizer*)tap{
    if([_delegate respondsToSelector:@selector(JournalRestaurantBookNow:)]){
        if(restaurant==nil){
            UIView* view=tap.view;
            view.userInteractionEnabled=NO;
            [self getRestaurant:^(IMGRestaurant* rest){
                [_delegate JournalRestaurantBookNow:rest];
                view.userInteractionEnabled=YES;
            }];
        }else{
            [[self class] cancelPreviousPerformRequestsWithTarget:self selector:@selector(todoSomething) object:tap.view];
            [self performSelector:@selector(todoSomething) withObject:tap.view afterDelay:0.5f];
        }
    }
}
-(void)todoSomething{

    [_delegate JournalRestaurantBookNow:restaurant];


}
-(void)viewDetail:(UITapGestureRecognizer*)tap{
    if([_delegate respondsToSelector:@selector(JournalRestaurantViewDetail:)]){
        if(restaurant==nil){
            UIView* view=tap.view;
            view.userInteractionEnabled=NO;

            [self getRestaurant:^(IMGRestaurant* rest){
                [_delegate JournalRestaurantViewDetail:rest];
                view.userInteractionEnabled=YES;
            }];
        }else{
            [_delegate JournalRestaurantViewDetail:restaurant];
        }
    }
}

-(void)gotoOffer:(UITapGestureRecognizer*)tap{
    if([_delegate respondsToSelector:@selector(JournalRestaurantViewOffer:)]){
        if(restaurant==nil){
            UIView* view=tap.view;
            view.userInteractionEnabled=NO;
            [self getRestaurant:^(IMGRestaurant* rest){
                [_delegate JournalRestaurantViewOffer:rest];
                view.userInteractionEnabled=YES;
            }];
        }else{
            [_delegate JournalRestaurantViewOffer:restaurant];
        }
    }
}

-(void)seeMenu:(UITapGestureRecognizer*)tap{
    if([_delegate respondsToSelector:@selector(JournalRestaurantSeeMenu:)]){
        if(restaurant==nil){
            UIView* view=tap.view;
            view.userInteractionEnabled=NO;
            [self getRestaurant:^(IMGRestaurant* rest){
                [_delegate JournalRestaurantSeeMenu:rest];
                view.userInteractionEnabled=YES;
            }];
        }else{
            [_delegate JournalRestaurantSeeMenu:restaurant];
        }
    }
}

-(void)seeMap:(UITapGestureRecognizer*)tap{
    if([_delegate respondsToSelector:@selector(JournalRestaurantSeeMap:)]){
        if(restaurant==nil){
            UIView* view=tap.view;
            view.userInteractionEnabled=NO;
            [self getRestaurant:^(IMGRestaurant* rest){
                [_delegate JournalRestaurantSeeMap:rest];
                view.userInteractionEnabled=YES;
            }];
        }else{
            [_delegate JournalRestaurantSeeMap:restaurant];
        }
    }
}

-(void)imageClick:(UITapGestureRecognizer*)tap{
    if([_delegate respondsToSelector:@selector(JournalRestaurantImageClick:withTargatURL:andRestaurantId:)]){
        [_delegate JournalRestaurantImageClick:tap withTargatURL:self.photoTarget andRestaurantId:self.restaurantId];
    }
}

-(void)journalCreditClick:(UITapGestureRecognizer*)tap{
    if([self.creditType isEqualToString:@"Photo Source"] || [self.creditType isEqualToString:@"User ID"]){
        return;
    }else if(([self.creditType isEqualToString:@"Restaurant"] || [self.creditType isEqualToString:@"Qraved Team"])){
        [self viewDetail:nil];
    }else if([self.creditType isEqualToString:@"URL"]){
        if([_delegate respondsToSelector:@selector(journalCreditClick:)]){
            [_delegate journalCreditClick:self.url];
        }
    }
}
- (void)btnClick:(UIButton *)btn{
    if([_delegate respondsToSelector:@selector(JournalRestaurantBtnClick:withOfferId:)]){
//        BOOL isoffer;
//        NSNumber *numberId;
//        if ([self.landPage isEqualToString:@"Promo"]) {
//            isoffer=YES;
//            numberId=self.selectedOfferId;
//        }else {
//            isoffer=NO;
//            numberId=self.restaurantId;
//        }
        [_delegate JournalRestaurantBtnClick:self.restaurantId withOfferId:self.selectedOfferId];
    }
}

-(void)seeMorePhotos:(UIButton*)button{
    IMGRestaurant *restaurant_=[[IMGRestaurant alloc] init];
    restaurant_.title=self.title;
    restaurant_.restaurantId=self.restaurantId;
    if([_delegate respondsToSelector:@selector(seeMoreRestaurantPhotos:)]){
        [_delegate seeMoreRestaurantPhotos:restaurant_];
    }
}

-(void)cuisineClick:(UITapGestureRecognizer*)tap{
    if([_delegate respondsToSelector:@selector(journalCuisineClick:)]){
        NSDictionary *dic=@{@"cuisineList":@[@{@"cuisineId":cuisineId}]};
        IMGDiscover *discover=[[IMGDiscover alloc] init];
        [discover initFromDictionary:dic];
        [_delegate journalCuisineClick:discover];
    }
}

-(void)districtClick:(UITapGestureRecognizer*)tap{
    if([_delegate respondsToSelector:@selector(journalDistrictClick:)]){
        NSDictionary *dic=@{@"districtList":@[@{@"districtId":districtId}]};
        IMGDiscover *discover=[[IMGDiscover alloc] init];
        [discover initFromDictionary:dic];
        [_delegate journalDistrictClick:discover];
    }
}


-(void)getRestaurant:(void(^)(IMGRestaurant*))block{
    [RestaurantHandler getRestaurantDetailFromServer:self.restaurantId andBlock:^(IMGRestaurant* rest){
        restaurant=rest;
        restaurant.menuCount = _menuPhotoCount;
        block(rest);
    }];
}

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event{
    for(UITouch *touch in touches){
        if(touch.view.tag==111){
            CGPoint position=[touch locationInView:[touch.view superview]];
            for(NSValue *value in cRects){
                if(CGRectContainsPoint([value CGRectValue],position)){
                    [self cuisineClick:nil];
                    // NSLog(@"cuisine clicked");
                    return;
                }
            }
            for(NSValue *value in dRects){
                if(CGRectContainsPoint([value CGRectValue],position)){
                    [self districtClick:nil];
                    // NSLog(@"district clicked");
                    return;
                }
            }
        }
    }
}


- (NSString *)flattenHTML:(NSString *)html {
    
    NSScanner *theScanner;
    NSString *text = nil;
    
    theScanner = [NSScanner scannerWithString:html];
    
    while ([theScanner isAtEnd] == NO) {
        // find start of tag
        [theScanner scanUpToString:@"<" intoString:NULL] ;
        // find end of tag
        [theScanner scanUpToString:@">" intoString:&text] ;
        // replace the found tag with a space
        //(you can filter multi-spaces out later if you wish)
        html = [html stringByReplacingOccurrencesOfString:
                [NSString stringWithFormat:@"%@>", text]
                                               withString:@""];
    } // while //
    
    return html;
}
- (void)attributedLabel:(TYAttributedLabel *)attributedLabel textStorageClicked:(id<TYTextStorageProtocol>)TextRun atPoint:(CGPoint)point{
    
    NSString *linkStr = ((TYLinkTextStorage*)TextRun).linkData;
    if ([linkStr rangeOfString:@"@"].location!=NSNotFound) {
        [[ UIApplication sharedApplication] openURL:[NSURL URLWithString:linkStr]];
        return;
    }
    [self.delegate JournalRestaurantContenClik:linkStr];
    
    
    
}
-(void)freshLikeSaveReviewBtn{
    if (!likeBtn.hidden) {
        CGRect frame = likeBtn.frame;
        frame.origin.x = 0;
        likeBtn.frame = frame;
    }
    if (!saveBtn.hidden) {
        if (!likeBtn.hidden) {
            CGRect frame = saveBtn.frame;
            frame.origin.x = likeBtn.endPointX;
            saveBtn.frame = frame;
        }else{
            CGRect frame = saveBtn.frame;
            frame.origin.x = 0;
            saveBtn.frame = frame;
        }
    }
    if (!reviewBtn.hidden) {
        if (!saveBtn.hidden) {
            CGRect frame = reviewBtn.frame;
            frame.origin.x = saveBtn.endPointX;
            reviewBtn.frame = frame;
        }else{
            if (!likeBtn.hidden) {
                CGRect frame = reviewBtn.frame;
                frame.origin.x = likeBtn.endPointX;
                reviewBtn.frame = frame;
            }else{
                CGRect frame = reviewBtn.frame;
                frame.origin.x = 0;
                reviewBtn.frame = frame;
            }
        }
    }
}
@end
