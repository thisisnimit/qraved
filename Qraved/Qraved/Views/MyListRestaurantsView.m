//
//  MyListRestaurantsView.m
//  Qraved
//
//  Created by System Administrator on 10/14/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "MyListRestaurantsView.h"
#import "MyListRestaurantsViewCell.h"
#import "IMGMyList.h"
#import "IMGRestaurant.h"
#import "UIConstants.h"
#import "UIDevice+Util.h"
#import "ListHandler.h"

#define HEADER_HEIGHT 44

@implementation MyListRestaurantsView

@synthesize hasData;

-(instancetype)initWithFrame:(CGRect)frame list:(IMGMyList*)mylist{
	if(self=[super initWithFrame:frame]){
		self.mylist=mylist;
		hasData=YES;
		self.restaurants=[[NSMutableArray alloc] init];
		self.tableView=[[UITableView alloc] initWithFrame:CGRectMake(0,0,frame.size.width,frame.size.height-HEADER_HEIGHT)];
		self.tableView.delegate=self;
		self.tableView.dataSource=self;
        self.tableView.scrollsToTop = YES;
		self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
		[self addSubview:self.tableView];
		[self addObserver:self forKeyPath:@"restaurants" options:NSKeyValueObservingOptionNew context:nil];
	}
	return self;
}

-(void)setRefreshViewFrame{
	if(hasData){
		if(self.refreshView==nil){
			self.refreshView=[[EGORefreshTableFooterView alloc] initWithFrame:CGRectMake(0,DeviceHeight,DeviceWidth,66)];
			self.refreshView.backgroundColor=[UIColor clearColor];
			self.refreshView.delegate=self;
			[self.tableView addSubview:self.refreshView];
		}
		if(self.tableView.contentSize.height>self.tableView.frame.size.height){
			self.refreshView.frame=CGRectMake(0,self.tableView.contentSize.height+10,DeviceWidth,66);
		}else{
			self.refreshView.frame=CGRectMake(0,self.tableView.frame.size.height+10,DeviceWidth,66);
		}
	}else if(self.refreshView){
		[self.refreshView removeFromSuperview];
		self.refreshView=nil;
	}
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    if (_restaurants.count>0) {
        IMGRestaurant *restaurant = [_restaurants objectAtIndex:indexPath.row];
        return [tableView cellHeightForIndexPath:indexPath model:restaurant keyPath:@"restaurant" cellClass:[SavedTableViewCell class] contentViewWidth:DeviceWidth];
    }
    return 0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView*)tableView{
	return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	return self.restaurants.count;
}


-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    static NSString *cellId = @"SavedCell";
    SavedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[SavedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    cell.delegate = self;
    IMGRestaurant *restaurant = [_restaurants objectAtIndex:indexPath.row];
    cell.restaurant = restaurant;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexpath{
    [tableView deselectRowAtIndexPath:indexpath animated:NO];
    if(self.delegate && [self.delegate respondsToSelector:@selector(goToRestaurant:)]){
		IMGRestaurant *restaurant=[self.restaurants objectAtIndex:indexpath.row];
		if (restaurant!=nil){
            
			[self.delegate goToRestaurant:restaurant];
            
                      
		}	
	}
}
//- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return @"Remove";
//}
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:@"Are you sure you want to remove this restaurant?" preferredStyle:UIAlertControllerStyleAlert];
//        [alert addAction:[UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleCancel handler:nil]];
//        [alert addAction:[UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//            IMGRestaurant *restaurant = _restaurants[indexPath.row];
//            [[LoadingView sharedLoadingView] startLoading];
//            [ListHandler listRemoveWithUser:[IMGUser currentUser] andListId:_mylist.listId andRestaurantId:restaurant.restaurantId andBlock:^{
//                [[LoadingView sharedLoadingView] stopLoading];
//
//                [_restaurants removeObjectAtIndex:indexPath.row];
//                [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
//                
//                
//                
//            } failure:^(NSString *exceptionMsg) {
//                
//            }];
//            
//        }]];
//        [[AppDelegate ShareApp].window.rootViewController presentViewController:alert animated:YES completion:nil];
//        
//    
//}
//
//- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return UITableViewCellEditingStyleDelete;
//}
//- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
//    return YES;
//}

#pragma mark - SavedTableViewCellDelegate
- (void)removeFromList:(NSNumber *)restaurantId{
    [[LoadingView sharedLoadingView] startLoading];
    [ListHandler removeRestaurantFromListWithRestaurantId:restaurantId andListId:_mylist.listId andBlock:^{
        [[LoadingView sharedLoadingView] stopLoading];
//        for (IMGRestaurant *restaurant in self.restaurants) {
//            if ([restaurantId isEqual:restaurant.restaurantId]) {
//                [self.restaurants removeObject:restaurant];
//                break;
//            }
//        }
//        
//        [self.tableView reloadData];
        if (self.refreshList != nil) {
            self.refreshList(restaurantId);
        }
        
    } failure:^(NSString *exceptionMsg) {
        
    }];
}
-(void)goToRestaurant:(UITapGestureRecognizer*)tap{
	
}

- (void)egoRefreshTableFooterDidTriggerRefresh:(EGORefreshTableFooterView*)view{
	if(self.delegate && [self.delegate respondsToSelector:@selector(loadMyListRestaurants:)]){
		[self.delegate loadMyListRestaurants:^(BOOL _hasData){
			hasData=_hasData;
			[self setRefreshViewFrame];
        }];
	}
}

-(void)scrollViewDidScroll:(UITableView*)tableView{
	[self.refreshView egoRefreshScrollViewDidScroll:tableView];
}

-(void)scrollViewDidEndDragging:(UITableView*)tableView willDecelerate:(BOOL)decelerate{
	if(self.tableView.contentOffset.y>self.tableView.contentSize.height-self.tableView.frame.size.height+66){
		self.refreshView.frame=CGRectMake(0,self.tableView.contentSize.height,DeviceWidth,66);
		[self.refreshView egoRefreshScrollViewDidEndDragging:tableView];
	}
}

-(void)observeValueForKeyPath:(NSString*)keypath ofObject:(id)objc change:(NSDictionary*)change context:(void*)context{
	if([keypath isEqualToString:@"restaurants"]){
		[self.tableView reloadData];
	}
}

-(void)dealloc{
	[self removeObserver:self forKeyPath:@"restaurants" context:nil];
}


@end
