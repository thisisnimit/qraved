//
//  MyListRestaurantsViewCell.h
//  Qraved
//
//  Created by System Administrator on 10/14/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyListDelegate.h"

@class IMGRestaurant;

@interface MyListRestaurantsViewCell : UITableViewCell<UITextViewDelegate>

@property(nonatomic,strong) IMGRestaurant *restaurant;
@property(nonatomic,copy) NSString *restaurantTitle;
@property(nonatomic,strong) UILabel *cuisizeDistrictLabel;
@property(nonatomic,copy) NSString *rate;
@property(nonatomic,copy) NSNumber *reviewCount;
@property(nonatomic,copy) NSNumber *ratingCount;
@property(nonatomic,copy) NSString *cuisizeDistrict;
@property(nonatomic,copy) NSString *saveDate;
@property(nonatomic,copy) NSString *distance;
@property(nonatomic,copy) NSString *image;
@property(nonatomic,copy) NSString *note;
@property(nonatomic,assign) BOOL isOtherUser;

@property(nonatomic,weak) NSObject<MyListDelegate> *delegate;

@end
