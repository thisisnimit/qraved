 //
//  JNReservationTableViewCell.m
//  Qraved
//
//  Created by lucky on 16/9/9.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "JNReservationTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "Date.h"
#import "TrackHandler.h"
#import "RestaurantHandler.h"
#import "SendCall.h"
#import "MapViewController.h"
#import <CoreText/CoreText.h>
#import "AppDelegate.h"
#import "NXOAuth2AccountStore.h"
#import "RestaurantHandler.h"
#import "Label.h"
#import "CustomIOS7AlertView.h"

@implementation JNReservationTableViewCell
{
    UIImageView* dateImageView;
    UILabel* dateLabel;
    UIImageView* paxImageView;
    UILabel* paxLabel;
    UIImageView* offerImageView;
    UILabel* offerLabel;
    UIButton* callBtn;
    IMGJNReservation *_reservation;
    IMGUser* user;
    IMGRestaurant* currentRestaurant;
    UIButton* mapBtn;
    UIButton* calenderBtn;
    UIButton* cancelBtn;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
    if(self=[super initWithStyle:style reuseIdentifier:identifier]){
        
        [self addTopView];
        
        self.iconImageView.image = [UIImage imageNamed:@"journey_filter_book"];
        user=[IMGUser currentUser];
        
        dateImageView=[UIImageView new];
        dateImageView.image=[UIImage imageNamed:@"myprofile_datetime"];
        dateLabel=[UILabel new];
        dateLabel.font = [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:11];
        paxImageView=[UIImageView new];
        paxImageView.image=[UIImage imageNamed:@"myprofile_pax"];
        paxLabel=[UILabel new];
        paxLabel.font = [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:11];
        offerImageView=[UIImageView new];
        offerImageView.hidden=NO;
        offerImageView.image=[UIImage imageNamed:@"myprofile_offer"];
        offerLabel=[UILabel new];
        offerLabel.font = [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:11];
        offerLabel.textColor=[UIColor colorWithRed:164.0/255.0 green:6.0/255.0 blue:10.0/255.0 alpha:1];
        [self.contentView sd_addSubviews:@[dateImageView,dateLabel,paxImageView,paxLabel,offerImageView,offerLabel]];
        
        dateImageView.sd_layout
        .topSpaceToView(self.cuisineAndDistrictLabel,12)
        .leftEqualToView(self.cuisineAndDistrictLabel)
        .widthIs(16)
        .heightIs(16);
        
        dateLabel.sd_layout
        .topEqualToView(dateImageView)
        .leftSpaceToView(dateImageView,8)
        .rightEqualToView(self.contentView)
        .autoHeightRatio(0);
        
        paxImageView.sd_layout
        .topSpaceToView(dateLabel,8)
        .leftEqualToView(dateImageView)
        .widthIs(16)
        .heightIs(16);
        
        paxLabel.sd_layout
        .topSpaceToView(dateLabel,8)
        .leftSpaceToView(paxImageView,8)
        .rightEqualToView(self.contentView)
        .autoHeightRatio(0);
        
        offerImageView.sd_layout
        .topSpaceToView(paxLabel,8)
        .leftEqualToView(paxImageView)
        .widthIs(16)
        .heightIs(16);
        
        offerLabel.sd_layout
        .topSpaceToView(paxLabel,8)
        .leftSpaceToView(offerImageView,8)
        .rightEqualToView(self.contentView)
        .autoHeightRatio(0);

    }
    return self;
}

-(void)setReservation:(IMGJNReservation *)reservation{
    _reservation = reservation;
    NSString* creatDateStr=[Date getTimeInteval_v4:[reservation.dateCreated longLongValue]/1000];
    self.dateLabel.text =[NSString stringWithFormat:@"Booked %@",creatDateStr];
    if (callBtn) {
        [callBtn removeFromSuperview];
        [mapBtn removeFromSuperview];
        [calenderBtn removeFromSuperview];
        [cancelBtn removeFromSuperview];
    }
      offerImageView.hidden=NO;
    if ((!_reservation.isOutOfDate&&([_reservation.status isEqualToNumber:@1]||[_reservation.status isEqualToNumber:@11]||[_reservation.status isEqualToNumber:@2]))&&!self.isOtherUser) {
        
            callBtn=[UIButton new];
            callBtn.tag=100;
            [callBtn addTarget:self action:@selector(btnAct:) forControlEvents: UIControlEventTouchUpInside];
            UIImageView* callImage=[[UIImageView alloc]initWithFrame:CGRectMake(17, 7, 18, 15)];
            callImage.image=[UIImage imageNamed:@"MenuPhone"];
            [callBtn addSubview:callImage];
            callBtn.backgroundColor=[UIColor colorWithRed:97.0/255.0 green:177.0/255.0 blue:46.0/255.0 alpha:1];
            callBtn.layer.cornerRadius=14;
            mapBtn=[UIButton new];
            mapBtn.userInteractionEnabled=NO;
            mapBtn.layer.cornerRadius=14;
            mapBtn.tag=101;
            [mapBtn addTarget:self action:@selector(btnAct:) forControlEvents:UIControlEventTouchUpInside];
            UIImageView* mapImage=[[UIImageView alloc]initWithFrame:CGRectMake(17, 7, 18, 15)];
            mapImage.image=[UIImage imageNamed:@"locationBlack"];
            [mapBtn addSubview:mapImage];
    
            mapBtn.backgroundColor=[UIColor colorWithRed:234.0/255.0 green:234.0/255.0 blue:234.0/255.0 alpha:1];
            calenderBtn=[UIButton new];
            calenderBtn.layer.cornerRadius=14;
            calenderBtn.tag=102;
            [calenderBtn addTarget:self action:@selector(btnAct:) forControlEvents:UIControlEventTouchUpInside];
            UIImageView* calenderImage=[[UIImageView alloc]initWithFrame:CGRectMake(17, 7, 18, 15)];
            calenderImage.image=[UIImage imageNamed:@"calendar"];
            [calenderBtn addSubview:calenderImage];
    
            calenderBtn.backgroundColor=[UIColor colorWithRed:234.0/255.0 green:234.0/255.0 blue:234.0/255.0 alpha:1];
            cancelBtn=[UIButton new];
            cancelBtn.layer.cornerRadius=14;
            cancelBtn.tag=103;
            [cancelBtn addTarget:self action:@selector(btnAct:) forControlEvents:UIControlEventTouchUpInside];
            [cancelBtn setTitle:@"cancel" forState:UIControlStateNormal];
            [cancelBtn setTitleColor:[UIColor colorWithRed:171/255.0 green:0 blue:9/255.0 alpha:1] forState:UIControlStateNormal];
            cancelBtn.titleLabel.font=[UIFont systemFontOfSize:13];
            cancelBtn.backgroundColor=[UIColor colorWithRed:234.0/255.0 green:234.0/255.0 blue:234.0/255.0 alpha:1];
    
            [self.contentView sd_addSubviews:@[callBtn,mapBtn,calenderBtn,cancelBtn]];
            callBtn.sd_layout.topSpaceToView(offerLabel,12)
            .leftEqualToView(offerImageView)
            .widthIs(50)
            .heightIs(30);
    
            mapBtn.sd_layout.topEqualToView(callBtn)
            .leftSpaceToView(callBtn,10)
            .widthIs(50)
            .heightIs(30);
    
            calenderBtn.sd_layout.topEqualToView(callBtn)
            .leftSpaceToView(mapBtn,10)
            .widthIs(50)
            .heightIs(30);
    
            cancelBtn.sd_layout.topEqualToView(callBtn)
            .leftSpaceToView(calenderBtn,10)
            .widthIs(75)
            .heightIs(30);
    }else{
        
        [self.contentView sd_addSubviews:@[dateImageView,dateLabel,paxImageView,paxLabel,offerImageView,offerLabel]];
        
        dateImageView.sd_layout
        .topSpaceToView(self.cuisineAndDistrictLabel,12)
        .leftEqualToView(self.cuisineAndDistrictLabel)
        .widthIs(16)
        .heightIs(16);
        
        dateLabel.sd_layout
        .topEqualToView(dateImageView)
        .leftSpaceToView(dateImageView,8)
        .rightEqualToView(self.contentView)
        .autoHeightRatio(0);
        
        paxImageView.sd_layout
        .topSpaceToView(dateLabel,8)
        .leftEqualToView(dateImageView)
        .widthIs(16)
        .heightIs(16);
        
        paxLabel.sd_layout
        .topSpaceToView(dateLabel,8)
        .leftSpaceToView(paxImageView,8)
        .rightEqualToView(self.contentView)
        .autoHeightRatio(0);
        
        offerImageView.sd_layout
        .topSpaceToView(paxLabel,8)
        .leftEqualToView(paxImageView)
        .widthIs(16)
        .heightIs(16);
        
        offerLabel.sd_layout
        .topSpaceToView(paxLabel,8)
        .leftSpaceToView(offerImageView,8)
        .rightEqualToView(self.contentView)
        .autoHeightRatio(0);
        [self setupAutoHeightWithBottomView:paxImageView bottomMargin:10];

    }
    currentRestaurant=[[IMGRestaurant alloc]init];
    [RestaurantHandler getRestaurantDetailFromServer:_reservation.restaurantId andBlock:^(IMGRestaurant *restaurant) {
        currentRestaurant=restaurant;
        // [self upcommingUpdated];
        [self layoutIfNeeded];
        mapBtn.userInteractionEnabled=YES;
        
    }];

    self.restTitleLabel.text = reservation.restaurantTitle;
    NSString *dollar=nil;
    switch(reservation.restaurantPriceLevel.intValue){
        case 1:dollar=@"Below 100K";break;
        case 2:dollar=@"100K - 200K";break;
        case 3:dollar=@"200K - 300K";break;
        case 4:dollar=@"Start from 300K";break;
    }

    NSString *cuisineAndDistrictText=[NSString stringWithFormat:@"%@ / %@ / %@",[reservation.restaurantCuisine removeHTML],[reservation.restaurantDistrict removeHTML],dollar];
    self.cuisineAndDistrictLabel.text = cuisineAndDistrictText;
    if ([reservation.offerName isEqualToString:@""]) {
     offerImageView.hidden=YES;
       }
    NSDateFormatter* dateFormatter=[[NSDateFormatter alloc]init];
    dateFormatter.dateFormat=@"yyyy-MM-dd";
    NSDate *bookDate=[dateFormatter dateFromString:reservation.bookDate];
    dateFormatter.dateStyle=NSDateFormatterMediumStyle;
    dateLabel.text=[NSString stringWithFormat:@"%@ %@",reservation.bookTime,[dateFormatter stringFromDate:bookDate]];
    paxLabel.text=[NSString stringWithFormat:@"%@ people",reservation.bookPax];
    offerLabel.text=[reservation.offerName removeHTML];
    if ((!_reservation.isOutOfDate&&([_reservation.status isEqualToNumber:@1]||[_reservation.status isEqualToNumber:@11]||[_reservation.status isEqualToNumber:@2]))&&!_reservation.isother) {
        
            [self setupAutoHeightWithBottomView:callBtn bottomMargin:10];
    }else{
        
        if ([offerLabel.text isEqualToString:@""]) {
            [self setupAutoHeightWithBottomView:paxImageView bottomMargin:10];

        }else{
        
            [self setupAutoHeightWithBottomView:offerImageView bottomMargin:10];

        }
        
        
    }
}
-(void)btnAct:(UIButton*)btn{
    
    switch (btn.tag) {
        case 100:
        {
            [self telephoneButtonClick];
            NSLog(@" click call btn");
        }
            break;
        case 101:
        {
            
             [self.delegate JourneyReserrationMapClick:currentRestaurant  withReservation:_reservation ];
            NSLog(@" click map btn");
        }
            
            break;
            
        case 102:
        {
            [self addToCalendar];
            
            NSLog(@" click calender btn");
        }
            
            break;
        case 103:
        {
            [self.delegate journeyReserationCancelClick:_reservation ];
            
            NSLog(@" click cancel btn");
        }
            
            break;
            
        default:
            break;
    }
}

- (void)restTitleLabelClick
{
    [self.JNDelegate didClickRestTitleLableWithRestId:_reservation.restaurantId];
}
-(void)addToCalendar{
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    if ([eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)]){
        // the selector is available, so we must be on iOS 6 or newer
        [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (error){
                    // display error message here
                }else if (!granted){
                    // display access denied error message here
                }else{
                    // access granted
                    // ***** do the important stuff here *****
                    EKEvent *ekEvent  = [EKEvent eventWithEventStore:eventStore];
                    ekEvent.title     =_reservation.restaurantTitle;
                    ekEvent.location = [_reservation address];
                    
                    
                    NSDateFormatter *tempFormatter = [[NSDateFormatter alloc]init];
                    [tempFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
                    ekEvent.startDate=[tempFormatter dateFromString:[NSString stringWithFormat:@"%@ %@",_reservation.bookDate,_reservation.bookTime]];
                    ekEvent.endDate=[tempFormatter dateFromString:[NSString stringWithFormat:@"%@ %@",_reservation.bookDate,_reservation.bookTime]];
                    
                    [tempFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
                    ekEvent.allDay = NO;
                    
                    //添加提醒
                    [ekEvent addAlarm:[EKAlarm alarmWithRelativeOffset:60.0f * -60.0f * 24]];
                    [ekEvent addAlarm:[EKAlarm alarmWithRelativeOffset:60.0f * -15.0f]];
                    
                    [ekEvent setCalendar:[eventStore defaultCalendarForNewEvents]];
                    NSError *err;
                    [eventStore saveEvent:ekEvent span:EKSpanThisEvent error:&err];
                    
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Calendar" message:@"Reservation is added to calendar." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [alert show];
                    
                }
            });
        }];
    }
}

-(void)telephoneButtonClick{
    
    
    if(_reservation!=nil && _reservation.reservationId!=nil &&_reservation.restaurantPhone!=nil){
        NSNumber *userId = [[NSNumber alloc] init];
        if (user.userId != nil)
        {
            userId = user.userId;
        }
        else
            userId = [NSNumber numberWithInt:0];
        
        [TrackHandler trackWithUserId:userId andRestaurantId:_reservation.restaurantId andContent:[NSDictionary dictionaryWithObject:@"0" forKey:@"type"]];
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"" message:[NSString stringWithFormat:@"%@",_reservation.restaurantPhone ]delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
        alertView.tag=1;
        [alertView show];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag==1){
        if(buttonIndex==0){
            [alertView dismissWithClickedButtonIndex:0 animated:NO];
        }else if (buttonIndex==1){
            
            [SendCall sendCall:alertView.message];
        }
    }
}

@end
