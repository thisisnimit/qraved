//
//  JNReviewTableViewCell.h
//  Qraved
//
//  Created by lucky on 16/9/8.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "JNBaseTableViewCell.h"
#import "IMGJNReview.h"

@interface JNReviewTableViewCell : JNBaseTableViewCell

@property (nonatomic,strong)    IMGJNReview *review;
@property(nonatomic,assign)BOOL isOther;
@end
