//
//  JNPhotoContainerView.m
//  Qraved
//
//  Created by lucky on 16/9/12.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "JNPhotoContainerView.h"
#import "UIView+SDAutoLayout.h"
#import "UIImageView+WebCache.h"
#import "UIConstants.h"
#import "IMGDish.h"
#import "UIImage+Resize.h"

@implementation JNPhotoContainerView
{
    UILabel *numLabel;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setup];
    }
    return self;
}

- (void)setup
{
    NSMutableArray *temp = [NSMutableArray new];
    
    for (int i = 0; i < 6; i++) {
        UIImageView *imageView = [UIImageView new];
        [self addSubview:imageView];
        imageView.userInteractionEnabled = YES;
        imageView.tag = i;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImageView:)];
        [imageView addGestureRecognizer:tap];
        [temp addObject:imageView];
    }
    
    
    numLabel = [[UILabel alloc] init];
    numLabel.tag = 5;
    numLabel.backgroundColor = [UIColor colorWithRed:237/255.0 green:237/255.0 blue:237/255.0 alpha:1];
    numLabel.font = [UIFont systemFontOfSize:17];
    numLabel.textColor = [UIColor color333333];
    [self addSubview:numLabel];
    UITapGestureRecognizer *numLabelTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImageView:)];
    numLabel.userInteractionEnabled = YES;
    [numLabel addGestureRecognizer:numLabelTapGesture];
    numLabel.textAlignment = NSTextAlignmentCenter;

    
    self.imageViewsArray = [temp copy];
}


- (void)setDishArr:(NSArray *)dishArr
{
    _dishArr = dishArr;
    numLabel.hidden = YES;

    for (long i = _dishArr.count; i < self.imageViewsArray.count; i++) {
        UIImageView *imageView = [self.imageViewsArray objectAtIndex:i];
        imageView.hidden = YES;
    }
    
    if (_dishArr.count == 0) {
        self.height = 0;
        self.fixedHeight = @(0);
        return;
    }
    
    CGFloat itemW = 0;
    CGFloat itemH = 0;
    
    if (_dishArr.count == 1)
    {
        itemW = DeviceWidth;
        UIImageView *imageView = [_imageViewsArray objectAtIndex:0];
        imageView.contentMode =UIViewContentModeScaleAspectFill;
        imageView.frame = CGRectMake(0, 0, itemW, 360);
        imageView.hidden = NO;
        __weak typeof(imageView) weakImageView = imageView;
        IMGDish *dish = [_dishArr objectAtIndex:0];
        [imageView sd_setImageWithURL:[NSURL URLWithString:[dish.imageUrl returnFullImageUrlWithWidth:imageView.frame.size.width]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
//            [UIView animateWithDuration:1.0 animations:^{
//                [weakImageView setAlpha:1.0];
//            }];
            image = [image imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth, 360)];
            
            [weakImageView setImage:image];

        }];
    }
    else if (_dishArr.count == 2)
    {
        for (int i= 0;i<_dishArr.count;i++)
        {
            IMGDish *dish = [_dishArr objectAtIndex:i];
            UIImageView *imageView = [_imageViewsArray objectAtIndex:i];
            itemW = (DeviceWidth-1)/2;
            imageView.frame = CGRectMake(i*(itemW+1), 0, itemW, itemW);
            imageView.hidden = NO;
            __weak typeof(imageView) weakImageView = imageView;
//            [imageView setImageWithURL:[NSURL URLWithString:[dish.imageUrl returnFullImageUrlWithWidth:imageView.frame.size.width]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//                [UIView animateWithDuration:1.0 animations:^{
//                    [weakImageView setAlpha:1.0];
//                }];
//            }];
            [imageView sd_setImageWithURL:[NSURL URLWithString:[dish.imageUrl returnFullImageUrlWithWidth:imageView.frame.size.width]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                weakImageView.image = [image imageByScalingAndCroppingForSize:CGSizeMake(itemW, itemW)];
            }];
        }
    }
    else if (_dishArr.count == 3)
    {
        for (int i= 0;i<_dishArr.count;i++)
        {
            IMGDish *dish = [_dishArr objectAtIndex:i];
            UIImageView *imageView = [_imageViewsArray objectAtIndex:i];
            itemW = (DeviceWidth-2)/3;
            imageView.frame = CGRectMake(i*(itemW+1), 0, itemW, itemW);
            imageView.hidden = NO;
            __weak typeof(imageView) weakImageView = imageView;
//            [imageView setImageWithURL:[NSURL URLWithString:[dish.imageUrl returnFullImageUrlWithWidth:imageView.frame.size.width]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//                [UIView animateWithDuration:1.0 animations:^{
//                    [weakImageView setAlpha:1.0];
//                }];
//            }];
            [imageView sd_setImageWithURL:[NSURL URLWithString:[dish.imageUrl returnFullImageUrlWithWidth:imageView.frame.size.width]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                weakImageView.image = [image imageByScalingAndCroppingForSize:CGSizeMake(itemW, itemW)];
            }];
        }
    }
    else if (_dishArr.count == 4)
    {
        int j = 0;
        int k = 0;
        for (int i= 0;i<_dishArr.count;i++)
        {
            IMGDish *dish = [_dishArr objectAtIndex:i];
            UIImageView *imageView = [_imageViewsArray objectAtIndex:i];
            itemW = (DeviceWidth-1)/2;
            imageView.frame = CGRectMake((itemW+1)*k, j*(itemW+1), itemW, itemW);
            imageView.hidden = NO;
            __weak typeof(imageView) weakImageView = imageView;
//            [imageView setImageWithURL:[NSURL URLWithString:[dish.imageUrl returnFullImageUrlWithWidth:imageView.frame.size.width]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//                [UIView animateWithDuration:1.0 animations:^{
//                    [weakImageView setAlpha:1.0];
//                }];
//            }];
            [imageView sd_setImageWithURL:[NSURL URLWithString:[dish.imageUrl returnFullImageUrlWithWidth:imageView.frame.size.width]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                weakImageView.image = [image imageByScalingAndCroppingForSize:CGSizeMake(itemW, itemW)];

            }];
            k++;
            if (i == 1)
            {
                j++;
                k = 0;
            }
        }
    }
    else if (dishArr.count == 5)
    {
        int j = 0;
        int k = 0;
        itemW = (DeviceWidth-2)/3;
        itemH = (DeviceWidth-2)/3;;
        for (int i= 0;i<_dishArr.count;i++)
        {
            IMGDish *dish = [_dishArr objectAtIndex:i];
            UIImageView *imageView = [_imageViewsArray objectAtIndex:i];
            imageView.frame = CGRectMake((itemW+1)*k, j*(itemH+1), itemW, itemH);
            imageView.hidden = NO;
            __weak typeof(imageView) weakImageView = imageView;
//            [imageView setImageWithURL:[NSURL URLWithString:[dish.imageUrl returnFullImageUrlWithWidth:imageView.frame.size.width]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//                [UIView animateWithDuration:1.0 animations:^{
//                    [weakImageView setAlpha:1.0];
//                }];
//            }];
            [imageView sd_setImageWithURL:[NSURL URLWithString:[dish.imageUrl returnFullImageUrlWithWidth:imageView.frame.size.width]]  placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                weakImageView.image = [image imageByScalingAndCroppingForSize:CGSizeMake(itemW, itemW)];

            }];
            k++;
            if (i == 2)
            {
                j++;
                k = 0;
                itemW = (DeviceWidth-1)/2;
            }
        }
    }
    else if (dishArr.count == 6)
    {
        int j = 0;
        int k = 0;
        itemW = (DeviceWidth-2)/3;
        itemH = (DeviceWidth-2)/3;;
        for (int i= 0;i<_dishArr.count;i++)
        {
            IMGDish *dish = [_dishArr objectAtIndex:i];
            UIImageView *imageView = [_imageViewsArray objectAtIndex:i];
            imageView.frame = CGRectMake((itemW+1)*k, j*(itemH+1), itemW, itemH);
            imageView.hidden = NO;
            __weak typeof(imageView) weakImageView = imageView;
//            [imageView setImageWithURL:[NSURL URLWithString:[dish.imageUrl returnFullImageUrlWithWidth:imageView.frame.size.width]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//                [UIView animateWithDuration:1.0 animations:^{
//                    [weakImageView setAlpha:1.0];
//                }];
//            }];
            [imageView sd_setImageWithURL:[NSURL URLWithString:[dish.imageUrl returnFullImageUrlWithWidth:imageView.frame.size.width]]  placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                weakImageView.image = [image imageByScalingAndCroppingForSize:CGSizeMake(itemW, itemW)];
            }];
            k++;
            if (i == 2)
            {
                j++;
                k = 0;
            }
        }
    }
    else
    {
        int j = 0;
        int k = 0;
        itemW = (DeviceWidth-2)/3;
        itemH = (DeviceWidth-2)/3;;
        for (int i= 0;i<_dishArr.count;i++)
        {
            if (i == 5)
            {
                numLabel.frame = CGRectMake((itemW+1)*k, j*(itemH+1), itemW, itemH);
                [numLabel setText:[NSString stringWithFormat:@"+%lu",_dishArr.count -5]];
                numLabel.hidden = NO;
                break;
            }

            IMGDish *dish = [_dishArr objectAtIndex:i];
            UIImageView *imageView = [_imageViewsArray objectAtIndex:i];
            imageView.frame = CGRectMake((itemW+1)*k, j*(itemH+1), itemW, itemH);
            imageView.hidden = NO;
            __weak typeof(imageView) weakImageView = imageView;
//            [imageView setImageWithURL:[NSURL URLWithString:[dish.imageUrl returnFullImageUrlWithWidth:imageView.frame.size.width]] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
//                [UIView animateWithDuration:1.0 animations:^{
//                    [weakImageView setAlpha:1.0];
//                }];
//            }];
            [imageView sd_setImageWithURL:[NSURL URLWithString:[dish.imageUrl returnFullImageUrlWithWidth:imageView.frame.size.width]]  placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                weakImageView.image = [image imageByScalingAndCroppingForSize:CGSizeMake(itemW, itemW)];

            }];
            k++;
            if (i == 2)
            {
                j++;
                k = 0;
            }
        }
    }
    
    self.width = DeviceWidth;
    self.height = [self viewHeightForPicPathArray:_dishArr];;
    
    self.fixedWidth = @(DeviceWidth);
    self.fixedHeight = @([self viewHeightForPicPathArray:_dishArr]);
}

#pragma mark - private actions

- (void)tapImageView:(UITapGestureRecognizer *)tap
{
    NSInteger tag = tap.view.tag;
    [self.delegate JNPhotoContainerViewImagePressed:tag];
}

- (CGFloat)viewHeightForPicPathArray:(NSArray *)array
{
    if (array.count == 1)
    {
        return 360;
    }
    else if (array.count == 2)
    {
        return (DeviceWidth-1)/2;
    }
    else if (array.count == 3)
    {
        return (DeviceWidth-2)/3;
    }
    else if (array.count == 4)
    {
        return ((DeviceWidth-1)/2)*2+1;
    }
    else if (array.count == 5)
    {
        return ((DeviceWidth-2)/3)*2+1;
    }
    else
    {
        return ((DeviceWidth-2)/3)*2+1;
    }
}


//- (CGFloat)itemWidthForPicPathArray:(NSArray *)array
//{
//    return DeviceWidth - LEFTLEFTSET*3 - 45;
//    //    if (array.count == 1) {
//    //        return 120;
//    //    } else {
//    //        CGFloat w = [UIScreen mainScreen].bounds.size.width > 320 ? 80 : 70;
//    //        return w;
//    //    }
//}
//
//- (NSInteger)perRowItemCountForPicPathArray:(NSArray *)array
//{
//    if (array.count < 3) {
//        return array.count;
//    } else if (array.count <= 4) {
//        return 2;
//    } else {
//        return 3;
//    }
//}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
