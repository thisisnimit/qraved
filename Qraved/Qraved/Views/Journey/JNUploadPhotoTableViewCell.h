//
//  JNUploadPhotoTableViewCell.h
//  Qraved
//
//  Created by lucky on 16/9/9.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "JNBaseTableViewCell.h"
#import "IMGJNUploadPhoto.h"
#import "JNPhotoContainerView.h"
@protocol JourneyUploadDelegate <NSObject>
-(void)journeyUploadPhotoCancel:(UITableViewCell*)uploadPhotoCell;
@end

@interface JNUploadPhotoTableViewCell : JNBaseTableViewCell<JNPhotoContainerViewDelegate>

@property (nonatomic,strong)    IMGJNUploadPhoto *uploadPhoto;
@property(nonatomic,assign)id<JourneyUploadDelegate>delegate;
@property(nonatomic,assign)BOOL isOtherUser;
@end
