//
//  JNBaseTableViewCell.h
//  Qraved
//
//  Created by lucky on 16/9/8.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+SDAutoLayout.h"
#import "UIConstants.h"
#import "IMGRestaurant.h"
#import "JNPhotoContainerView.h"
#import "IMGJNUploadPhoto.h"

@protocol JNBaseTableViewCellDelegate <NSObject>

- (void)didClickRestTitleLableWithRestId:(NSNumber *)restaurantId;
- (void)didClickEditBtn:(IMGRestaurant *)restaurant andReviewId:(NSNumber*)reviewId indexPath:(NSIndexPath *)indexPath;
- (void)didClickUploadPhotoEditBtn:(IMGJNUploadPhoto *)_uploadPhoto indexPath:(NSIndexPath *)indexPath;
- (void)didClickPhotoWithTag:(NSInteger)photoTag andDishArr:(NSArray *)dishArr;
@end

@interface JNBaseTableViewCell : UITableViewCell<JNPhotoContainerViewDelegate>

@property (nonatomic,retain)    UIImageView *iconImageView;
@property (nonatomic,strong)    UILabel *dateLabel;
@property (nonatomic,strong)    UILabel *restTitleLabel;
@property (nonatomic,strong)    UILabel *cuisineAndDistrictLabel;
@property (nonatomic,strong)    UIView *bottomLineView;

@property (nonatomic,assign)    id <JNBaseTableViewCellDelegate> JNDelegate;
@property (nonatomic,strong)    NSIndexPath *indexPath;
- (void)addTopView;
- (void)restTitleLabelClick;
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier;
@end
