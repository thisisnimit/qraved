//
//  JNBaseTableViewCell.m
//  Qraved
//
//  Created by lucky on 16/9/8.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "JNBaseTableViewCell.h"

@implementation JNBaseTableViewCell
{

}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
    if(self=[super initWithStyle:style reuseIdentifier:identifier]){
        
    }
    return self;
}

- (void)addTopView
{
    self.iconImageView = [UIImageView new];
    self.iconImageView.backgroundColor = [UIColor cyanColor];
    [self.iconImageView.layer setMasksToBounds:YES];
    [self.iconImageView.layer setCornerRadius:25/2];
    
    self.dateLabel = [UILabel new];
    self.dateLabel.font = [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:12];
    self.dateLabel.textColor = [UIColor grayColor];
    
    self.restTitleLabel = [UILabel new];
    self.restTitleLabel.font = [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:17];
    self.restTitleLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(restTitleLabelClick)];
    [self.restTitleLabel addGestureRecognizer:tap];
    
    self.cuisineAndDistrictLabel = [UILabel new];
    self.cuisineAndDistrictLabel.font = [UIFont fontWithName:FONT_MONTSERRAT_REGULAR size:11];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor grayColor];
    lineView.alpha = 0.4f;
    
    self.bottomLineView = [UIView new];
    self.bottomLineView.backgroundColor = [UIColor grayColor];
    self.bottomLineView.alpha = 0.3f;
    
    [self.contentView sd_addSubviews:@[lineView,self.iconImageView,self.dateLabel,self.restTitleLabel,self.cuisineAndDistrictLabel,self.bottomLineView]];
    
    lineView.sd_layout
    .leftSpaceToView(self.contentView,LEFTLEFTSET+12.5)
    .topEqualToView(self.contentView)
    .widthIs(1)
    .bottomEqualToView(self.contentView);
    
    _iconImageView.sd_layout
    .leftSpaceToView(self.contentView,LEFTLEFTSET)
    .topSpaceToView(self.contentView,LEFTLEFTSET)
    .widthIs(25)
    .heightIs(25);
    
    self.dateLabel.sd_layout
    .leftSpaceToView(_iconImageView,LEFTLEFTSET)
    .topSpaceToView(self.contentView,20)
    .rightSpaceToView(self.contentView,LEFTLEFTSET)
    .autoHeightRatio(0);
    
    self.restTitleLabel.sd_layout
    .leftEqualToView(self.dateLabel)
    .topSpaceToView(self.dateLabel,5)
    .rightSpaceToView(self.contentView,LEFTLEFTSET)
    .autoHeightRatio(0);
    
    self.cuisineAndDistrictLabel.sd_layout
    .leftEqualToView(self.dateLabel)
    .topSpaceToView(self.restTitleLabel,5)
    .rightSpaceToView(self.contentView,LEFTLEFTSET)
    .autoHeightRatio(0);
    
    self.bottomLineView.sd_layout
    .leftSpaceToView(_iconImageView,8)
    .rightEqualToView(self.contentView)
    .bottomEqualToView(self.contentView)
    .heightIs(1);
    
    self.backgroundColor = [UIColor whiteColor];
}

- (void)restTitleLabelClick
{
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
