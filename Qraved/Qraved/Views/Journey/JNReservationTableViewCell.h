//
//  JNReservationTableViewCell.h
//  Qraved
//
//  Created by lucky on 16/9/9.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "JNBaseTableViewCell.h"
#import "IMGJNReservation.h"
#import "IMGRestaurant.h"
#import "IMGJNReservation.h"
@protocol JourneyReserrationMapClickDelegate <NSObject>
- (void)JourneyReserrationMapClick:(IMGRestaurant *)restaurant withReservation:(IMGJNReservation*)reservation;
-(void)journeyReserationCancelClick:(IMGJNReservation*)reservation;

@end

@interface JNReservationTableViewCell : JNBaseTableViewCell
@property(nonatomic,retain)IMGJNReservation* reservation;
@property(nonatomic,assign)id<JourneyReserrationMapClickDelegate>delegate;
@property(nonatomic,assign)BOOL isOtherUser;

@end
