//
//  JNUploadPhotoTableViewCell.m
//  Qraved
//
//  Created by lucky on 16/9/9.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "JNUploadPhotoTableViewCell.h"
#import "Date.h"
#import "UIColor+Helper.h"
#import "JourneyHandler.h"
#import "IMGUser.h"
#import "CustomIOS7AlertView.h"
#import "Label.h"
#import "UIView+Helper.h"
@implementation JNUploadPhotoTableViewCell
{
    JNPhotoContainerView *_picContainerView;
    IMGJNUploadPhoto *_uploadPhoto;
    UIButton *_editBtn;
    UIButton *_deleteBtn;

}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
    if(self=[super initWithStyle:style reuseIdentifier:identifier]){
        [self addTopView];
        self.iconImageView.image = [UIImage imageNamed:@"journey_filter_photo"];
        _picContainerView = [JNPhotoContainerView new];
        _picContainerView.delegate = self;
        [self.contentView addSubview:_picContainerView];
        
        _editBtn = [[UIButton alloc] init];
        [_editBtn setImage:[UIImage imageNamed:@"journey_edit"] forState:UIControlStateNormal];
        _editBtn.backgroundColor=[UIColor colorEEEEEE];
        [_editBtn.layer setMasksToBounds:YES];
        [_editBtn.layer setCornerRadius:22/2];
        [_editBtn addTarget:self action:@selector(editBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_editBtn];
        
        _deleteBtn = [[UIButton alloc] init];
        [_deleteBtn setImage:[UIImage imageNamed:@"journey_delete"] forState:UIControlStateNormal];
        _deleteBtn.backgroundColor=[UIColor colorEEEEEE];
        [_deleteBtn.layer setMasksToBounds:YES];
        [_deleteBtn.layer setCornerRadius:22/2];
        [_deleteBtn addTarget:self action:@selector(deleteBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_deleteBtn];
    }
    return self;
}

- (void)setUploadPhoto:(IMGJNUploadPhoto *)uploadPhoto
{
    _uploadPhoto = uploadPhoto;
    if (self.isOtherUser) {
        _editBtn.hidden=YES;
        _deleteBtn.hidden=YES;
    }
    self.dateLabel.text = [Date getTimeInteval_v4:[uploadPhoto.uploadedPhotoTimeCreated longLongValue]/1000];
    self.restTitleLabel.text = uploadPhoto.restaurantTitle;
    NSString *dollar=nil;
    switch(uploadPhoto.restaurantPriceLevel.intValue){
        case 1:dollar=@"Below 100K";break;
        case 2:dollar=@"100K - 200K";break;
        case 3:dollar=@"200K - 300K";break;
        case 4:dollar=@"Start from 300K";break;
    }

    NSString *cuisineAndDistrictText=[NSString stringWithFormat:@"%@ / %@ / %@",[uploadPhoto.restaurantCuisine removeHTML],[uploadPhoto.restaurantDistrict removeHTML],dollar];
    self.cuisineAndDistrictLabel.text = cuisineAndDistrictText;

    _picContainerView.dishArr = uploadPhoto.dishArr;
    
    _picContainerView.sd_layout
    .leftEqualToView(self.contentView)
    .topSpaceToView(self.cuisineAndDistrictLabel,LEFTLEFTSET);
    
    _editBtn.sd_layout
    .leftEqualToView(self.dateLabel)
    .topSpaceToView(_picContainerView,10)
    .widthIs(44)
    .heightIs(22);
    
    _deleteBtn.sd_layout
    .leftSpaceToView(_editBtn,10)
    .topSpaceToView(_picContainerView,10)
    .widthIs(44)
    .heightIs(22);
    if (uploadPhoto.isother) {
        
        [self setupAutoHeightWithBottomView:_picContainerView bottomMargin:10];
    }else{
        [self setupAutoHeightWithBottomView:_editBtn bottomMargin:10];

    }
}
- (void)JNPhotoContainerViewImagePressed:(NSInteger)tag
{
    [self.JNDelegate didClickPhotoWithTag:tag andDishArr:_uploadPhoto.dishArr];
}
- (void)restTitleLabelClick
{
    [self.JNDelegate didClickRestTitleLableWithRestId:_uploadPhoto.restaurantId];
}
-(void)editBtnClick{
    if (self.JNDelegate && [self.JNDelegate respondsToSelector:@selector(didClickUploadPhotoEditBtn:indexPath:)]) {
        [self.JNDelegate didClickUploadPhotoEditBtn:_uploadPhoto indexPath:self.indexPath];
    }
}

-(void)deleteBtnClick{
    IMGUser* user=[IMGUser currentUser];
    UIView *popUpView = [[UIView alloc]init];
    
    NSString *offerTitleStr = @"Are you sure you want to delete this uploaded photo?";
    
    CGSize maxSize = [offerTitleStr sizeWithFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
    Label *seeMenuTitle = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 64/2-(maxSize.height+4)/2+8, maxSize.width, maxSize.height+4) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] andTextColor:[UIColor color333333] andTextLines:0];
    seeMenuTitle.text = offerTitleStr;
    seeMenuTitle.textAlignment = NSTextAlignmentCenter;
    [popUpView addSubview:seeMenuTitle];
    popUpView.frame = CGRectMake(0, 0, DeviceWidth-2*LEFTLEFTSET, maxSize.height+40);
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc]init];
    alertView.buttonTitles = @[@"No, keep it",@"Yes"];
    [alertView setContainerView:popUpView];
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, NSInteger buttonIndex) {
        if(buttonIndex==1){
            [[LoadingView sharedLoadingView] startLoading];
            //            IMGUser * user = [IMGUser currentUser];
            if(user.userId==nil){
                //lead to login
                return;
            }
            if(user.token==nil){
                //lead to login
                return;
            }
            NSDictionary* parmer=@{@"uploadedPhotoId":_uploadPhoto.uploadedPhotoId,@"userId":user.userId,@"t":user.token};
            [JourneyHandler deletUploadPhoto:parmer andBlock:^(NSDictionary *returnInfo) {
                if ([[returnInfo objectForKey:@"status"] isEqualToString:@"succeed"]) {
                    [self.delegate journeyUploadPhotoCancel:self];
                    
                }
                [[LoadingView sharedLoadingView] stopLoading];
            }];
          
        }
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    [alertView show];

    
    
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
