//
//  JNPhotoContainerView.h
//  Qraved
//
//  Created by lucky on 16/9/12.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol JNPhotoContainerViewDelegate <NSObject>

-(void)JNPhotoContainerViewImagePressed:(NSInteger)tag;

@end

@interface JNPhotoContainerView : UIView

@property (nonatomic, strong) NSArray *dishArr;
@property (nonatomic, strong) NSArray *imageViewsArray;

@property (nonatomic,assign)    id <JNPhotoContainerViewDelegate> delegate;

@end
