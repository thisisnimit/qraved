//
//  JNReviewTableViewCell.m
//  Qraved
//
//  Created by lucky on 16/9/8.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "JNReviewTableViewCell.h"
#import "DLStarRatingControl.h"
#import "UIColor+Helper.h"
#import "UIImageView+WebCache.h"
#import "Date.h"
#import "NSNumber+Helper.h"
#import "UIColor+Helper.h"
#import "ReviewPublishViewController.h"
#import "JNPhotoContainerView.h"

@implementation JNReviewTableViewCell
{
    UILabel *_titleLabel;
    UILabel *_scoreLabel;
    DLStarRatingControl *_ratingScoreControl;
    UILabel *_summarizeLabel;
    IMGJNReview *_review;
    UIButton *_editBtn;
    JNPhotoContainerView *_picContainerView;
}
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier{
    if(self=[super initWithStyle:style reuseIdentifier:identifier]){
     
        [self addTopView];
        self.iconImageView.image = [UIImage imageNamed:@"journey_filter_review"];

        _titleLabel = [UILabel new];
        _titleLabel.textColor = [UIColor colorWithRed:171/255.0 green:0 blue:9/255.0 alpha:1];
        _titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:12];
        
        _ratingScoreControl=[[DLStarRatingControl alloc] initWithFrame:CGRectMake(0, 0, 0, 0) andStars:5 andStarWidth:10 isFractional:NO spaceWidth:5];
        _ratingScoreControl.userInteractionEnabled=NO;

        _scoreLabel = [UILabel new];
        _scoreLabel.font = [UIFont systemFontOfSize:12.5];
        _scoreLabel.textColor = [UIColor colorFFC000];

        _summarizeLabel = [UILabel new];
        _summarizeLabel.textColor = [UIColor colorWithRed:51/255.0 green:51/255.0 blue:51/255.0 alpha:1];
        _summarizeLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:12];
        
        _picContainerView = [JNPhotoContainerView new];
        _picContainerView.delegate = self;
        
        _editBtn = [[UIButton alloc] init];
        [_editBtn setImage:[UIImage imageNamed:@"journey_edit"] forState:UIControlStateNormal];
        _editBtn.backgroundColor=[UIColor colorEEEEEE];
        [_editBtn.layer setMasksToBounds:YES];
        [_editBtn.layer setCornerRadius:22/2];
        [_editBtn addTarget:self action:@selector(editBtnClick) forControlEvents:UIControlEventTouchUpInside];
        
        [self.contentView sd_addSubviews:@[_titleLabel,_scoreLabel,_ratingScoreControl,_summarizeLabel,_picContainerView,_editBtn]];
        
        _titleLabel.sd_layout
        .leftEqualToView(self.dateLabel)
        .topSpaceToView(self.cuisineAndDistrictLabel,8)
        .rightEqualToView(self.dateLabel)
        .autoHeightRatio(0);
        
        _scoreLabel.sd_layout
        .leftEqualToView(self.dateLabel)
        .topSpaceToView(_titleLabel,5)
        .autoHeightRatio(0);
        [_scoreLabel setSingleLineAutoResizeWithMaxWidth:50];
        
        _ratingScoreControl.sd_layout
        .leftSpaceToView(_scoreLabel,1)
        .topEqualToView(_scoreLabel)
        .widthIs(74)
        .heightIs(15);
        
        _summarizeLabel.sd_layout
        .leftEqualToView(self.dateLabel)
        .topSpaceToView(_ratingScoreControl,8)
        .rightEqualToView(self.dateLabel)
        .autoHeightRatio(0);
        
        _picContainerView.sd_layout
        .leftEqualToView(self.contentView)
        .topSpaceToView(_summarizeLabel,LEFTLEFTSET);
        
        _editBtn.sd_layout
        .leftEqualToView(self.dateLabel)
        .topSpaceToView(_picContainerView,10)
        .widthIs(44)
        .heightIs(22);
        
    }
    return self;
}
- (void)setReview:(IMGJNReview *)review
{
    
    if (self.isOther) {
        _editBtn.hidden=YES;
    }

    _review = review;
    self.dateLabel.text = [Date getTimeInteval_v4:[review.reviewTimeCreated longLongValue]/1000];
    self.restTitleLabel.text = review.restaurantTitle;
    NSString *dollar=nil;
    switch(review.restaurantPriceLevel.intValue){
        case 1:dollar=@"Below 100K";break;
        case 2:dollar=@"100K - 200K";break;
        case 3:dollar=@"200K - 300K";break;
        case 4:dollar=@"Start from 300K";break;
    }

    NSString *cuisineAndDistrictText=[NSString stringWithFormat:@"%@ / %@ / %@",[review.restaurantCuisine removeHTML],[review.restaurantDistrict removeHTML],dollar];
    self.cuisineAndDistrictLabel.text = cuisineAndDistrictText;
    
    if (!review.reviewTitle.length && !review.reviewSummarize.length)
    {
        self.iconImageView.image = [UIImage imageNamed:@"journey_filter_rate"];
    }
    else
    {
        self.iconImageView.image = [UIImage imageNamed:@"journey_filter_review"];
    }
    _titleLabel.text = review.reviewTitle;
    _scoreLabel.text = [NSString stringWithFormat:@"%.1f",[review.reviewScore floatValue]/2];
    _ratingScoreControl.rating= [[NSNumber numberWithDouble:[review.reviewScore doubleValue]] getRatingNumber];
    _summarizeLabel.text = review.reviewSummarize;
    _picContainerView.dishArr = _review.dishArr;
    
    if (review.isother) {
        if (_picContainerView.dishArr.count==0) {
            [self setupAutoHeightWithBottomView:_summarizeLabel bottomMargin:10];
        }else{
        
            [self setupAutoHeightWithBottomView:_picContainerView bottomMargin:10];
        }


    }else{
    
        [self setupAutoHeightWithBottomView:_editBtn bottomMargin:10];

    }
}

- (void)restTitleLabelClick
{
    [self.JNDelegate didClickRestTitleLableWithRestId:_review.restaurantId];
}
-(void)editBtnClick{
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    restaurant.restaurantId = _review.restaurantId;
    restaurant.priceLevel = _review.restaurantPriceLevel;
    restaurant.cuisineName = _review.restaurantCuisine;
    restaurant.districtName = _review.restaurantDistrict;
    restaurant.cityName = _review.restaurantCityName;
    restaurant.title = _review.restaurantTitle;
    restaurant.seoKeyword = _review.restaurantSeoKeywords;
    restaurant.imageUrl = _review.restaurantImage;

    if (self.JNDelegate && [self.JNDelegate respondsToSelector:@selector(didClickEditBtn:andReviewId:indexPath:)]) {
        [self.JNDelegate didClickEditBtn:restaurant andReviewId:_review.reviewId indexPath:self.indexPath];
    }
}

- (void)JNPhotoContainerViewImagePressed:(NSInteger)tag
{
    [self.JNDelegate didClickPhotoWithTag:tag andDishArr:_review.dishArr];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
