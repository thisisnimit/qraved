//
//  SavedMapListView.h
//  Qraved
//
//  Created by harry on 2017/5/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGRestaurant.h"
@interface SavedMapListView : UIView

@property (nonatomic, strong) IMGRestaurant *restaurant;

@property (nonatomic, copy) void (^buttonSavedClick)(IMGRestaurant *restaurant);

- (void)refreshSavedButtonStyle:(BOOL)isSaved;

@end
