//
//  CuisineAndDistrict.m
//  Qraved
//
//  Created by carl on 16/5/26.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "CuisineAndDistrict.h"

@implementation CuisineAndDistrict

-(void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event{
    for(UITouch *touch in touches){
        if(touch.view.tag==111){
            CGPoint position=[touch locationInView:self.parentView];
            for(NSValue *value in self.cRects){
                if(CGRectContainsPoint([value CGRectValue],position)){
                    if([self.delegate respondsToSelector:@selector(cuisineClick:)]){
                        [self.delegate cuisineClick:nil];
                    }
                    return;
                }
            }
            for(NSValue *value in self.dRects){
                if(CGRectContainsPoint([value CGRectValue],position)){
                    if([self.delegate respondsToSelector:@selector(districtClick:)]){
                        [self.delegate districtClick:nil];
                    }
                    return;
                }
            }
        }
    }
}

@end
