//
//  NewListView.m
//  Qraved
//
//  Created by System Administrator on 9/24/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "NewListView.h"
#import "UIConstants.h"
#import "UIColor+Helper.h"

@implementation NewListView


-(instancetype)initWithFrame:(CGRect)frame withDelegate:(id<UITextFieldDelegate>)delegate{
	if(self=[super initWithFrame:frame]){
		self.backgroundColor=[UIColor whiteColor];
		_field=[[UITextField alloc] initWithFrame:CGRectMake(LEFTLEFTSET,20,frame.size.width-LEFTLEFTSET*2,30)];
		_field.placeholder=L(@"Name your list");
		_field.textColor=[UIColor colorCCCCCC];
		_field.font=[UIFont systemFontOfSize:18];
		_field.delegate=delegate;
		_field.returnKeyType=UIReturnKeyDone;
		UIImageView *lineImage3 = [[UIImageView alloc] initWithFrame:CGRectMake(LEFTLEFTSET, 65, frame.size.width-LEFTLEFTSET*2, 1)];
	    lineImage3.image = [UIImage imageNamed:@"CutOffRule"];
		[self addSubview:_field];
	    [self addSubview:lineImage3];
        
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 66, DeviceWidth, 44)];
        bottomView.backgroundColor = [UIColor clearColor];
        bottomView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changesCity)];
        [bottomView addGestureRecognizer:tap];
        [self addSubview:bottomView];
        UILabel *changeLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 100, 44)];
        changeLabel.text = @"Change City";
        changeLabel.font = [UIFont systemFontOfSize:15];
        [bottomView addSubview:changeLabel];
        _cityLabel = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth-130, 0, 110, 44)];
        _cityLabel.textAlignment = NSTextAlignmentRight;
      
        self.cityLabel.text = [[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT] capitalizedString];
            
        [[NSUserDefaults standardUserDefaults] setValue:[[NSUserDefaults standardUserDefaults] objectForKey:CITY_SELECT_ID] forKey:@"listCityId"];
        _cityLabel.font = changeLabel.font;
        [bottomView addSubview:_cityLabel];
	}
	return self;
}
-(void)changesCity{
    if ([self.delegate respondsToSelector:@selector(changeCity)]) {
        [self.delegate changeCity];
    }
}
@end
