//
//  SavedAddListView.h
//  Qraved
//
//  Created by apple on 2017/5/18.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol SavedAddListViewDelegate <NSObject>

- (void)addNewList;
- (void)addProductToList:(NSNumber *)listId;

@end

@interface SavedAddListView : UIView
@property (weak, nonatomic)id<SavedAddListViewDelegate>delegate;

@end
