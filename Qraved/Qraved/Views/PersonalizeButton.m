//
//  PersonalizeButton.m
//  Qraved
//
//  Created by bill on 16/11/8.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "PersonalizeButton.h"
#import "UIColor+Helper.h"
#import "UIImageView+WebCache.h"
#import "UIConstants.h"
#import "UIView+Helper.h"
#import "UIImage+RTTint.h"
#import "UIImage+Resize.h"
@implementation PersonalizeButton{
    UIImageView *selectImageView;
    UIImageView *backImgViewTag;
    UILabel *nameTag;
    UIImage *Oimage;
}

-(instancetype)initWithType:(PersonalizeButtonType)type andFrame:(CGRect)frame andParam:(NSDictionary*)paramLocal{
    self = [super initWithFrame:frame];
    if (self) {
        self.ID = [paramLocal objectForKey:@"id"];
        self.iconUrl = [paramLocal objectForKey:@"iconUrl"];
        self.name = [paramLocal objectForKey:@"name"];
        self.type = type;
        
        if(type == PersonalizeButtonDistrict) {
            self.layer.borderColor = [[UIColor colorC2060A] CGColor];
            self.layer.borderWidth = 2.0f;
            self.layer.cornerRadius = 8.0f;
            self.layer.masksToBounds = YES;
            
            self.titleLabel.numberOfLines = 0;
            [self setTitle:self.name forState:UIControlStateNormal];
            [self setTitleColor:[UIColor colorC2060A] forState:UIControlStateNormal];
            self.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:14];
            
            [self addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
        }else if (type == PersonalizeButtonCuisine) {
            self.layer.cornerRadius = 4.0f;
            self.layer.masksToBounds = YES;
            
            UIImageView *backImgView = [[UIImageView alloc] initWithFrame:self.bounds];
            UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake(self.bounds.size.width, self.bounds.size.height)];
            __weak typeof(backImgView) weakBackImgView = backImgView;

            [backImgView sd_setImageWithURL:[NSURL URLWithString:self.iconUrl] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
              //  image = [image imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 160)];
                
                [weakBackImgView setImage:image];
            }];
//            [backImgView setImageWithURL:[NSURL URLWithString:self.iconUrl] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] sizeFitWidth:NO];
            backImgView.userInteractionEnabled = YES;
            [self addSubview:backImgView];
            
            UILabel *name = [[UILabel alloc] initWithFrame:self.bounds];
            name.textAlignment = NSTextAlignmentCenter;
            name.userInteractionEnabled = YES;
            name.text = self.name;
            name.textColor = [UIColor whiteColor];
            name.numberOfLines = 0;
            name.font = [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:14];
            [self addSubview:name];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnClick)];
            tap.numberOfTapsRequired = 1;
            tap.numberOfTouchesRequired = 1;
            [backImgView addGestureRecognizer:tap];
            
            UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnClick)];
            tap1.numberOfTapsRequired = 1;
            tap1.numberOfTouchesRequired = 1;
            [backImgView addGestureRecognizer:tap1];
            [name addGestureRecognizer:tap1];
            
            
        }else if(type == PersonalizeButtonPriceLevel) {
            
            self.layer.borderColor = [[UIColor colorC2060A] CGColor];
            self.layer.borderWidth = 2.0f;
            self.layer.cornerRadius = 16.0f;
            self.layer.masksToBounds = YES;
            
            self.titleLabel.numberOfLines = 0;
            [self setTitle:self.name forState:UIControlStateNormal];
            [self setTitleColor:[UIColor colorC2060A] forState:UIControlStateNormal];
            self.titleLabel.font = [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:14];
            
            [self addTarget:self action:@selector(btnClick) forControlEvents:UIControlEventTouchUpInside];
            
            
        }else if(type == PersonalizeButtonTag) {
            self.layer.borderColor = [[UIColor colorC2060A] CGColor];
            self.layer.borderWidth = 2.0f;
            self.layer.cornerRadius = 50.0f;
            self.layer.masksToBounds = YES;
            
            backImgViewTag = [[UIImageView alloc] initWithFrame:CGRectMake(30, 10, 40, 40)];
            UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING2] imageByScalingAndCroppingForSize:CGSizeMake(40,40)];
            __weak typeof(backImgViewTag) weakBackImgViewTag = backImgViewTag;

            [backImgViewTag sd_setImageWithURL:[NSURL URLWithString:self.iconUrl] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
               // image = [image imageByScalingAndCroppingForSize:CGSizeMake(DeviceWidth-2*LEFTLEFTSET, 160)];
                
                [weakBackImgViewTag setImage:image];

            }];

//            [backImgViewTag setImageWithURL:[NSURL URLWithString:self.iconUrl] placeholderImage:[UIImage imageNamed:DEFAULT_IMAGE_STRING2] sizeFitWidth:NO];
            backImgViewTag.userInteractionEnabled = YES;
            [self addSubview:backImgViewTag];
            
            nameTag = [[UILabel alloc] initWithFrame:CGRectMake(0, backImgViewTag.endPointY, 100, 30)];
            nameTag.userInteractionEnabled = YES;
            nameTag.textAlignment = NSTextAlignmentCenter;
            nameTag.text = self.name;
            nameTag.textColor = [UIColor colorC2060A];
            nameTag.numberOfLines = 0;
            nameTag.font = [UIFont fontWithName:DEFAULT_FONT_BOLD_NAME size:14];
            [nameTag sizeToFit];
            nameTag.frame = CGRectMake((self.frame.size.width - nameTag.frame.size.width)/2.0, backImgViewTag.endPointY, nameTag.frame.size.width, nameTag.frame.size.height);
            [self addSubview:nameTag];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnClick)];
            tap.numberOfTapsRequired = 1;
            tap.numberOfTouchesRequired = 1;
            [backImgViewTag addGestureRecognizer:tap];
            
            UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(btnClick)];
            tap1.numberOfTapsRequired = 1;
            tap1.numberOfTouchesRequired = 1;
            [nameTag addGestureRecognizer:tap1];
        }
    }
    return self;
}

-(void)btnClick{
    if (self.isBtnSelected) {
        self.isBtnSelected = NO;
        if(self.type == PersonalizeButtonDistrict){
            self.backgroundColor = [UIColor whiteColor];
            [self setTitleColor:[UIColor colorC2060A] forState:UIControlStateNormal];
        }else if (self.type == PersonalizeButtonCuisine) {
            [selectImageView removeFromSuperview];
            selectImageView = nil;
        }else if(self.type == PersonalizeButtonPriceLevel){
            self.backgroundColor = [UIColor whiteColor];
            [self setTitleColor:[UIColor colorC2060A] forState:UIControlStateNormal];
        }else if(self.type == PersonalizeButtonTag){
//            [selectImageView removeFromSuperview];
//            selectImageView = nil;
            self.backgroundColor=[UIColor whiteColor];
            nameTag.textColor=[UIColor colorC2060A];
            backImgViewTag.image=Oimage;
        }
        if (self.delegate && ([self.delegate respondsToSelector:@selector(personalizeButtonMinusWithType:andID:)])) {
            [self.delegate personalizeButtonMinusWithType:PersonalizeButtonDistrict andID:self.ID];
        }
    }else{
        self.isBtnSelected = YES;
        if(self.type == PersonalizeButtonDistrict){
            self.backgroundColor = [UIColor colorC2060A];
            [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }else if (self.type == PersonalizeButtonCuisine) {
            UIImage *img = [UIImage imageNamed:@"checked"];
            selectImageView = [[UIImageView alloc] initWithImage:img];
            selectImageView.frame = CGRectMake(self.frame.size.width/2.0-20, self.frame.size.height/2.0-25, 40, 40);
            [self addSubview:selectImageView];
        }else if(self.type == PersonalizeButtonPriceLevel){
            self.backgroundColor = [UIColor colorC2060A];
            [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }else if(self.type == PersonalizeButtonTag){
//            UIImage *img = [UIImage imageNamed:@"checked"];
//            selectImageView = [[UIImageView alloc] initWithImage:img];
//            selectImageView.frame = CGRectMake((self.frame.size.width - img.size.width)/2.0, (self.frame.size.height - img.size.height)/2.0, img.size.width, img.size.height);
//            [self addSubview:selectImageView];
            Oimage=backImgViewTag.image;
            self.backgroundColor=[UIColor colorC2060A];
            backImgViewTag.image=[backImgViewTag.image rt_tintedImageWithColor:[UIColor whiteColor]];
            nameTag.textColor=[UIColor whiteColor];
        }
        if (self.delegate && ([self.delegate respondsToSelector:@selector(personalizeButtonAddWithType:andID:)])) {
            [self.delegate personalizeButtonAddWithType:self.type andID:self.ID];
        }
    }
}
@end
