//
//  SavedMyListTableViewCell.h
//  Qraved
//
//  Created by apple on 2017/5/12.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IMGMyList.h"

@protocol SavedMyListTableViewCellDelgate <NSObject>

-(void)gotoListPage:(IMGMyList*)mylist;
-(void)reloadForNewList;
@end

@interface SavedMyListTableViewCell : UITableViewCell

@property (nonatomic,strong) UIImageView *secondImageView;
@property (nonatomic,strong) UIView *maskView;
@property (nonatomic,strong) UILabel *lblTitle;
@property (nonatomic,weak) id<SavedMyListTableViewCellDelgate>delegate;
-(void)setFirstMyList:(IMGMyList *)myList withIndex:(NSInteger)index;
-(void)setSecondMyList:(IMGMyList *)myList;

@end
