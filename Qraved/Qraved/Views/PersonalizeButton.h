//
//  PersonalizeButton.h
//  Qraved
//
//  Created by bill on 16/11/8.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,PersonalizeButtonType){
    PersonalizeButtonDistrict,
    PersonalizeButtonCuisine,
    PersonalizeButtonPriceLevel,
    PersonalizeButtonTag
};

@protocol PersonalizeButtonDelegate <NSObject>
-(void)personalizeButtonAddWithType:(PersonalizeButtonType)type andID:(NSNumber*)ID;
-(void)personalizeButtonMinusWithType:(PersonalizeButtonType)type andID:(NSNumber*)ID;
@end

@interface PersonalizeButton : UIButton

@property (nonatomic,retain)NSNumber *ID;
@property (nonatomic,copy)NSString *iconUrl;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,assign)BOOL isBtnSelected;
@property (nonatomic,assign)PersonalizeButtonType type;
@property(nonatomic,weak)id<PersonalizeButtonDelegate> delegate;

-(instancetype)initWithType:(PersonalizeButtonType)type andFrame:(CGRect)frame andParam:(NSDictionary*)param;
@end
