//
//  SavedNoDataView.m
//  Qraved
//
//  Created by harry on 2017/5/23.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "SavedNoDataView.h"

@interface SavedNoDataView ()
{
    UIImageView *imageView;
    UILabel *lblName;
    UILabel *lblTitle;
    UIButton *btnBottom;
}

@end

@implementation SavedNoDataView


- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
  
    imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 94, 110)];
    imageView.centerX = self.centerX;
    [self addSubview:imageView];
    
    lblName = [[UILabel alloc] initWithFrame:CGRectMake(60, imageView.endPointY + 10, DeviceWidth - 120, 20)];
    lblName.textAlignment = NSTextAlignmentCenter;
    lblName.font = [UIFont fontWithName:DEFAULT_FONT_NAME size:16.0];
    [self addSubview:lblName];
    
    lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(60, lblName.endPointY , DeviceWidth - 120, 20)];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.textColor = [UIColor colorWithRed:135/255.0 green:135/255.0 blue:135/255.0 alpha:1];;
    lblTitle.font = [UIFont systemFontOfSize:14.0];
    [self addSubview:lblTitle];
    
    btnBottom = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBottom.frame = CGRectMake(100, lblTitle.endPointY + 10, DeviceWidth - 200, 40);
    btnBottom.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [btnBottom setTitleColor:[UIColor barButtonTitleColor] forState:UIControlStateNormal];
    btnBottom.layer.cornerRadius = 20;
    btnBottom.layer.masksToBounds = YES;
    btnBottom.layer.borderWidth = 1;
    btnBottom.layer.borderColor = [UIColor barButtonTitleColor].CGColor;
    [self addSubview:btnBottom];
}
- (void)setNoDataViewWithImage:(UIImage *)image andName:(NSString *)name andTitle:(NSString *)title andButtonTitle:(NSString *)buttonTitle{
    imageView.image = image;
    lblName.text = name;
    lblTitle.text = title;
    [btnBottom setTitle:buttonTitle forState:UIControlStateNormal];
}

@end
