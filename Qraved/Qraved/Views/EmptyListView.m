//
//  EmptyListView.m
//  Qraved
//
//  Created by System Administrator on 9/24/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import "EmptyListView.h"
#import "UIColor+Helper.h"
#import "UIColor+Hex.h"
#import "UIView+Helper.h"
#import "UIDevice+Util.h"
#import "UIConstants.h"

@interface EmptyListView ()
{
    UIImageView *imageView;
    UILabel *lblName;
    UILabel *lblTitle;
    UIButton *btnBottom;
}

@end

@implementation EmptyListView

- (instancetype)initWithFrame:(CGRect)frame withType:(ListType)listType{
    if ([super initWithFrame:frame]) {
        [self createUI:listType];
    }
    return self;
}

- (void)createUI:(ListType)listType{
    
    NSString *name;
    NSString *title;
    NSString *buttonTitle;
    NSString *imageName;
    CGFloat titleSpace;
    CGFloat buttonSpace;
    CGRect imageViewRect;
    
    switch (listType) {
        case ListDetailType:
            name = @"NO RESTAURANT ADDED YET";
            title = @"Let's start add restaurant to your list";
            buttonTitle = @"Add Restaurant";
            imageName = @"ic_no_restaurant";
            //titleSpace = 5;
            buttonSpace = 30;
            imageViewRect = CGRectMake(0, 10, 132, 120);
            break;
        case MyListType:
            name = @"NO LIST CREATED YET";
            title = @"Let's start creating your first list";
            buttonTitle = @"Create New List";
            imageName = @"ic_no_list";
            //titleSpace = 5;
            buttonSpace = 5;
            imageViewRect = CGRectMake(0, 10, 94, 110);
            break;
        case WanttoGoType:
            name = @"NO SAVED RESTAURANT YET";
            title = @"Let's saved your first favorite restaurants";
            buttonTitle = @"Browse Restaurants";
            imageName = @"ic_empty_wantToGo";
            //titleSpace = 5;
            buttonSpace = 5;
            imageViewRect = CGRectMake(0, 10, 94, 110);
            break;
            
        default:
            break;
    }
    imageView = [[UIImageView alloc] initWithFrame:imageViewRect];
    imageView.centerX = self.centerX;
    imageView.image = [UIImage imageNamed:imageName];
    [self addSubview:imageView];
    
    lblName = [[UILabel alloc] initWithFrame:CGRectMake(20, imageView.endPointY + 10, DeviceWidth - 40, 16)];
    lblName.text = name;
    lblName.textAlignment = NSTextAlignmentCenter;
    lblName.font = [UIFont systemFontOfSize:FONT_SIZE_14];
    [self addSubview:lblName];
    
    lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(20, lblName.endPointY+5 , DeviceWidth - 40, 14)];
    lblTitle.text = title;
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.textColor = [UIColor colorWithRed:135/255.0 green:135/255.0 blue:135/255.0 alpha:1];;
    lblTitle.font = [UIFont systemFontOfSize:FONT_SIZE_12];
    [self addSubview:lblTitle];
    
    btnBottom = [UIButton buttonWithType:UIButtonTypeCustom];
    btnBottom.frame = CGRectMake((DeviceWidth-185)/2, lblTitle.endPointY + 10 + buttonSpace, 185, 38);
    [btnBottom setTitle:buttonTitle forState:UIControlStateNormal];
    btnBottom.titleLabel.font = [UIFont systemFontOfSize:FONT_SIZE_14];
    [btnBottom setTitleColor:[UIColor barButtonTitleColor] forState:UIControlStateNormal];
    [btnBottom addTarget:self action:@selector(goToAddRestaurant:) forControlEvents:UIControlEventTouchUpInside];
    btnBottom.layer.cornerRadius = 20;
    btnBottom.layer.masksToBounds = YES;
    btnBottom.layer.borderWidth = 1;
    btnBottom.layer.borderColor = [UIColor barButtonTitleColor].CGColor;
    [self addSubview:btnBottom];
}

- (void)setIsOtherUser:(BOOL)isOtherUser{
    _isOtherUser = isOtherUser;
    if (isOtherUser) {
        btnBottom.hidden = YES;
    }else{
        btnBottom.hidden = NO;
    }
}

-(void)goToAddRestaurant:(UIButton *)button{
    
    [IMGAmplitudeUtil trackSavedWithName:@"SC - Open Search Idle Page" andRestaurantId:nil andRestaurantTitle:nil andOrigin:@"Saved" andListId:nil andType:nil andLocation:nil];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(goToAddRestaurant:)]){
        [self.delegate goToAddRestaurant:button.titleLabel.text];
	}
}

@end
