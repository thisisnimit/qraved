//
//  GoFoodRestaurantTableViewCell.h
//  Qraved
//
//  Created by harry on 2018/4/8.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoFoodRestaurantTableViewCell : UITableViewCell
@property (nonatomic, strong) IMGRestaurant *restaurant;
@property(nonatomic, copy) void (^ saveTapped)(IMGRestaurant *restaurant,NSIndexPath *indexPath);
@property(nonatomic, strong) NSIndexPath *indexPath;
- (void)freshSavedButton:(BOOL)saved;
@end
