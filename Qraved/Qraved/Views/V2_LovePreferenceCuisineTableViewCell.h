//
//  V2_LovePreferenceCuisineTableViewCell.h
//  Qraved
//
//  Created by Adam.zhang on 2017/8/9.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol V2_LovePreferenceCuisineTableViewCellDelegate <NSObject>

- (void)textTagCollectionView:(NSInteger )textTagCollectionViewTag didTapTag:(NSString *)tagText atIndex:(NSUInteger)index selected:(BOOL)selected;

@end

@interface V2_LovePreferenceCuisineTableViewCell : UITableViewCell<TTGTextTagCollectionViewDelegate>

@property (nonatomic, strong)  NSMutableArray  *selearray;
@property (nonatomic, strong)  NSMutableArray  *selectedObjectIdArr;
@property (nonatomic, strong)  NSMutableArray  *selectedOneObjectIdArr;
@property (nonatomic, strong)  NSMutableArray  *selectedTwoObjectIdArr;
@property (nonatomic, strong)  NSMutableArray  *selectedAllArray;

@property (nonatomic, assign) CGFloat pointY;
- (void)createUI:(NSArray *)array;
//@property (nonatomic, strong)  NSArray  *cuisineArra;

@property (nonatomic, assign)  NSInteger  section;

@property (nonatomic, weak)  id<V2_LovePreferenceCuisineTableViewCellDelegate> Delegate;
@end
