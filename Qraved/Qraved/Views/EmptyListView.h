//
//  EmptyListView.h
//  Qraved
//
//  Created by System Administrator on 9/24/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyListDelegate.h"
typedef  NS_ENUM(NSInteger,ListType){
    WanttoGoType,
    MyListType,
    ListDetailType
};
@interface EmptyListView : UIView

@property(nonatomic,weak) NSObject<MyListDelegate> *delegate;
@property(nonatomic,assign)ListType type;
@property(nonatomic,assign) BOOL isOtherUser;

- (instancetype)initWithFrame:(CGRect)frame withType:(ListType)listType;

@end
