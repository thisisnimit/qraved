//
//  MainMenuTableViewCell.m
//  Qraved
//
//  Created by Admin on 9/22/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import "MainMenuTableViewCell.h"
#import "UIColor+Helper.h"
#import "UIConstants.h"
#import "AppDelegate.h"

@implementation MainMenuTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setNotificationNumber:(NSInteger)number{
    if (number==0 || ![AppDelegate ShareApp].userLogined) {
        [_numLabel removeFromSuperview];
        _numLabel =nil;
        return ;
    }

    if(_numLabel==nil){
        _numLabel =[[UILabel alloc]initWithFrame:CGRectMake(9.5, -6.5, 12, 12)];
        _numLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:8];
        _numLabel.textColor = [UIColor whiteColor];
        _numLabel.numberOfLines = 1;
        _numLabel.textAlignment = NSTextAlignmentCenter;
        _numLabel.backgroundColor = [UIColor colorC2060A];
        _numLabel.layer.cornerRadius = 6;
        _numLabel.layer.masksToBounds = YES;
    }
    _numLabel.text = [NSString stringWithFormat:@"%ld",(long)number];
    [_iconImage addSubview:_numLabel];
    [_iconImage layoutSubviews];
    [self layoutSubviews];
}

@end
