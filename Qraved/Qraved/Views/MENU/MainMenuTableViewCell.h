//
//  MainMenuTableViewCell.h
//  Qraved
//
//  Created by Admin on 9/22/13.
//  Copyright (c) 2013 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *TitleLabel;

@property (weak, nonatomic) IBOutlet UIImageView *iconImage;

@property (retain, nonatomic) UILabel * numLabel;

-(void)setNotificationNumber:(NSInteger)number;

@end
