//
//  HeadView.h


#import <UIKit/UIKit.h>



@protocol HeadViewDelegate; 

@interface MainMenuHeadView : UIView{
    NSInteger section;
    UIButton* backBtn;
}
@property(nonatomic, assign) id<HeadViewDelegate> delegate;
@property(nonatomic, assign) NSInteger section;
@property(nonatomic, retain) UIButton* backBtn;
@property(nonatomic, retain) IBOutlet UIImageView* iconView;
@property(nonatomic, retain) IBOutlet UILabel* detailTextLabel;
@property(nonatomic, retain) IBOutlet UILabel* textLabel;
@property(nonatomic, retain) IBOutlet UIImageView* lineImageView;
@property(nonatomic, retain) IBOutlet UIImageView* bgImageView;
@property(nonatomic, retain) IBOutlet UIImageView* arrowImageView;


- (void)configSelf;
-(BOOL)isOpenView;
-(void)openView;
-(void)closeView;


@end




@protocol HeadViewDelegate <NSObject>
-(void)selectedWith:(MainMenuHeadView *)view;
@end