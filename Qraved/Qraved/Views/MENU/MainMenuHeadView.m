//
//  HeadView.m


#import "MainMenuHeadView.h"

@interface MainMenuHeadView ()
@property(nonatomic, assign) BOOL open;
@end


@implementation MainMenuHeadView
@synthesize delegate;
@synthesize section,backBtn;
-(BOOL)isOpenView{
    return self.open;
}
-(void)openView{
    self.open = YES;
    [UIView animateWithDuration:0.2 animations:^{
        self.arrowImageView.transform = CGAffineTransformMakeRotation((90.0f * M_PI) / 180.0f);
    }];
    [self showBgClicked];
}
-(void)closeView{
    self.open = NO;
    /*
    [UIView animateWithDuration:0.2 animations:^{
        self.arrowImageView.transform = CGAffineTransformIdentity;
    }];*/
    [self hideBgClicked];
}
- (void)hideBgClicked
{
    self.lineImageView.hidden = NO;
    self.bgImageView.hidden = YES;
}
- (void)showBgClicked
{
    self.lineImageView.hidden = YES;
    self.bgImageView.hidden = NO;
}
- (void)configSelf
{
    self.open = NO;
    UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 320, 44);
    [btn addTarget:self action:@selector(doSelected) forControlEvents:UIControlEventTouchUpInside];
    // [headview.backBtn setTitle:tempData.title forState:UIControlStateNormal];
    //[btn setBackgroundImage:[UIImage imageNamed:@"btn_momal"] forState:UIControlStateNormal];
    //[btn setBackgroundImage:[UIImage imageNamed:@"btn_on"] forState:UIControlStateHighlighted];
    [self addSubview:btn];
    self.backBtn = btn;
    [self hideBgClicked];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}


-(void)doSelected{
    //    [self setImage];
    //NSLog(@"doSelected");
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectedWith:)]){
     	[self.delegate selectedWith:self];
    }
}
@end
