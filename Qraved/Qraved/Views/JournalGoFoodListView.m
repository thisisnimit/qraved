//
//  JournalGoFoodListView.m
//  Qraved
//
//  Created by harry on 2018/4/8.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import "JournalGoFoodListView.h"
#import "GoFoodRestaurantTableViewCell.h"

@interface JournalGoFoodListView ()<UITableViewDelegate, UITableViewDataSource>{
    UIView *bgView;
    UITableView *restaurantTableView;
}

@end

@implementation JournalGoFoodListView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)drawBackViewWithView:(UIView *)view BackColor:(UIColor *) color{
    CGSize finalSize = CGSizeMake(CGRectGetWidth(view.bounds), CGRectGetHeight(view.bounds));
    CGFloat layerHeight = finalSize.height;
    CAShapeLayer *layer = [CAShapeLayer layer];
    UIBezierPath *bezier = [UIBezierPath bezierPath];
    [bezier moveToPoint:CGPointMake(6, finalSize.height - layerHeight)];
    [bezier addLineToPoint:CGPointMake(0, finalSize.height)];
    [bezier addLineToPoint:CGPointMake(finalSize.width, finalSize.height)];
    [bezier addLineToPoint:CGPointMake(finalSize.width-6, finalSize.height - layerHeight)];
    [bezier addLineToPoint:CGPointMake(6,0)];
    layer.path = bezier.CGPath;
    layer.fillColor = color.CGColor;
    [view.layer addSublayer:layer];

    UIButton *btnDown = [UIButton buttonWithType:UIButtonTypeCustom];
    btnDown.frame = CGRectMake(0, 5, 50, 15);
    btnDown.backgroundColor = [UIColor whiteColor];
    [btnDown setImage:[UIImage imageNamed:@"ic_arrowDown"] forState:UIControlStateNormal];
    btnDown.centerX = view.centerX;
    [btnDown addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btnDown];
}

- (void)createUI{
    self.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.6];
    
    bgView = [[UIView alloc] initWithFrame:CGRectMake(0, DeviceHEIGHT - 100, DeviceWidth, 100)];
    bgView.backgroundColor = [UIColor clearColor];
    [self addSubview:bgView];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 65, 20)];
    [self drawBackViewWithView:view BackColor:[UIColor whiteColor]];
    view.centerX = bgView.centerX;
    [bgView addSubview:view];
    
    restaurantTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 20, DeviceWidth, 85) style:UITableViewStylePlain];
    restaurantTableView.backgroundColor = [UIColor whiteColor];
    restaurantTableView.delegate = self;
    restaurantTableView.dataSource = self;
    [bgView addSubview:restaurantTableView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.restaurantArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *idenStr = @"goFoodCell";
    GoFoodRestaurantTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
    if (!cell) {
        cell = [[GoFoodRestaurantTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    IMGRestaurant *restaurant = [self.restaurantArray objectAtIndex:indexPath.row];
    cell.restaurant = restaurant;
    cell.indexPath = indexPath;
    __weak typeof(cell) weakCell = cell;
    cell.saveTapped = ^(IMGRestaurant *restaurant, NSIndexPath *indexPath) {
        [CommonMethod doSaveWithRestaurant:restaurant andViewController:self.controller andCallBackBlock:^(BOOL isSaved) {
            if (isSaved) {
                [IMGAmplitudeUtil trackRestaurantWithName:@"BM - Save Restaurant Initiate" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:@"JDP" andOrigin:nil andType:@"Custom List"];
            }else{
                [IMGAmplitudeUtil trackRestaurantWithName:@"BM - Unsave Restaurant Initiate" andRestaurantId:restaurant.restaurantId andRestaurantTitle:nil andLocation:@"JDP" andOrigin:nil andType:@"Custom List"];
            }
            [weakCell freshSavedButton:isSaved];
            restaurant.saved = [NSNumber numberWithBool:isSaved];
            if (self.freshRestaurantList) {
                self.freshRestaurantList(restaurant);
            }
        }];
    };
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 85;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    IMGRestaurant *restaurant = [self.restaurantArray objectAtIndex:indexPath.row];
    
    [IMGAmplitudeUtil trackGoFoodWithName:@"CL - Go-Food CTA" andJournalId:self.journalId andRestaurantId:restaurant.restaurantId andPhotoId:nil andLocation:@"JDP" andOrigin:nil andOrder:nil];
    
    [CommonMethod goFoodButtonTapped:restaurant andViewController:self.controller andRestaurantState:[restaurant isOpenRestaurant] andCallBackBlock:^(BOOL isSaved) {
        restaurant.saved = [NSNumber numberWithBool:isSaved];
        GoFoodRestaurantTableViewCell *cell = (GoFoodRestaurantTableViewCell *)[restaurantTableView cellForRowAtIndexPath:indexPath];
        [cell freshSavedButton:isSaved];
        if (self.freshRestaurantList) {
            self.freshRestaurantList(restaurant);
        }
    }];
    [self removeFromSuperview];
}

- (void)setRestaurantArray:(NSArray *)restaurantArray{
    _restaurantArray = restaurantArray;
    [restaurantTableView reloadData];
    
    if (restaurantArray.count <= 4 && restaurantArray.count > 0) {
        bgView.frame = CGRectMake(0, DeviceHEIGHT, DeviceWidth, restaurantArray.count * 85 + 20);
        restaurantTableView.frame = CGRectMake(0, 20, DeviceWidth, restaurantArray.count * 85);
        [self showWithAnimation:restaurantArray.count * 85 + 20];
    }else if (restaurantArray.count > 4){
        bgView.frame = CGRectMake(0, DeviceHEIGHT, DeviceWidth, 85*4 + 50);
        restaurantTableView.frame = CGRectMake(0, 20, DeviceWidth, 85*4 + 30);
        [self showWithAnimation:85*4 + 50];
    }
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self closeWithAnimation];
}

- (void)showWithAnimation:(CGFloat)height{
    [UIView animateWithDuration:0.2 animations:^{
        bgView.frame = CGRectMake(0, DeviceHEIGHT - height, DeviceWidth, height);
    } completion:^(BOOL finished) {
        
    }];
}

- (void)closeWithAnimation{
    [IMGAmplitudeUtil trackJDPWithName:@"CL - Close Order Now" andJournalId:self.journalId andOrigin:nil andChannel:nil andLocation:nil];
    [UIView animateWithDuration:0.2 animations:^{
        bgView.frame = CGRectMake(0, DeviceHEIGHT, DeviceWidth, 0);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)close{
    [self closeWithAnimation];
}

@end
