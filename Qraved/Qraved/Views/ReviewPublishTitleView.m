//
//  ReviewPublishTitleView.m
//  Qraved
//
//  Created by apple on 17/4/1.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "ReviewPublishTitleView.h"

@implementation ReviewPublishTitleView
{
    UILabel *lblName;
    UILabel *lblCuisine;
}

-(instancetype)initWith:(IMGRestaurant*)restaurant{
    if (self = [super init]) {
        self.frame =  CGRectMake(0, 0, DeviceWidth, 45);
        self.backgroundColor = [UIColor whiteColor];
   
        UIImageView *restaurantImageView = [[UIImageView alloc] initWithFrame:CGRectMake(22, 13, 14, 19)];
        restaurantImageView.image = [UIImage imageNamed:@"ic_review_location" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        [self addSubview:restaurantImageView];
        
        lblName = [[UILabel alloc] initWithFrame:CGRectMake(64, 10, 200, 24)];
        lblName.font = [UIFont systemFontOfSize:14];
        lblName.textColor = [UIColor color333333];
        [self addSubview:lblName];
        
        if ([restaurant.restaurantId intValue]==0||restaurant.title.length==0) {
            lblName.text = @"Select Restaurant";
        }else{
            lblName.text = restaurant.title;
        }
        
        CGSize size=[lblName.text sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]}];
        CGSize cuisineSize = [restaurant.districtName sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]}];
        if (size.width > DeviceWidth-cuisineSize.width-100) {
            lblName.frame = CGRectMake(64, 10, DeviceWidth-cuisineSize.width-100, 24);
        }else{
            lblName.frame = CGRectMake(64, 10, size.width, 24);
        }
        
        lblCuisine = [[UILabel alloc] initWithFrame:CGRectMake(66+lblName.size.width, 10, cuisineSize.width, 24)];
        lblCuisine.font = [UIFont systemFontOfSize:14];
        lblCuisine.textColor = [UIColor color333333];
        [self addSubview:lblCuisine];
        
        if ([restaurant.restaurantId intValue]==0||restaurant.title.length==0) {
            lblCuisine.text = @"";
        }else{
            lblCuisine.text = restaurant.districtName;
        }
        
        UIImageView *arrowImageView = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth-33, 15, 8, 14)];
        //ic_chevron
        arrowImageView.image = [UIImage imageNamed:@"ic_review_arrow" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
        
        [self addSubview:arrowImageView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gotoSearch:)];
        [self addGestureRecognizer:tap];
        
        
        UIView *bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, 44.5, DeviceWidth, 0.5)];
        bottomLine.backgroundColor = [UIColor colorWithHexString:@"C1C1C1"];
        [self addSubview:bottomLine];
    }
        return self;
}

- (void)setRestaurant:(IMGRestaurant *)restaurant{
    _restaurant = restaurant;
    
    lblName.text = restaurant.title;
    
    CGSize size=[lblName.text sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]}];
    CGSize cuisineSize = [restaurant.cuisineName sizeWithAttributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]}];
    if (size.width > DeviceWidth-cuisineSize.width-100) {
        lblName.frame = CGRectMake(64, 10, DeviceWidth-cuisineSize.width-100, 24);
    }else{
        lblName.frame = CGRectMake(64, 10, size.width, 24);
    }
    
    lblCuisine.frame = CGRectMake(lblName.endPointX+2, 10, cuisineSize.width, 24);
    lblCuisine.text = restaurant.cuisineName;
}

- (void)gotoSearch:(UITapGestureRecognizer *)tap{
    if (self.gotoSearchRestaurant) {
        self.gotoSearchRestaurant();
    }
}

@end
