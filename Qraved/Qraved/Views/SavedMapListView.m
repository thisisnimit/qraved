//
//  SavedMapListView.m
//  Qraved
//
//  Created by harry on 2017/5/26.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "SavedMapListView.h"
#import "DLStarRatingControl.h"

@interface SavedMapListView(){
    UILabel * titleLable;
    UILabel * cuisineAndDistrictLabel;
    UILabel* lblDate;
    int nextOpenCurenttime;
    int nextCloseCurenttime;
    int secondOpenCurenttime;
    int secondCloseCurenttime;
    UILabel *reviewCountLabel;
    UILabel *lblWellKnown;
    UILabel *lblStatus;
    UIImageView * restaurantImageView;
    DLStarRatingControl *ratingScoreControl;
    UIImageView *badgeImgaeView;
    UIButton *btnSaved;
}

@end

@implementation SavedMapListView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    
    CGFloat width = self.frame.size.width;
    restaurantImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 95, 147)];
    restaurantImageView.userInteractionEnabled = YES;
    //[restaurantImageView setContentMode:UIViewContentModeScaleAspectFill];
    [self addSubview:restaurantImageView];
    
    btnSaved = [UIButton buttonWithType:UIButtonTypeCustom];
    btnSaved.frame = CGRectMake(width-52, 10, 40, 34);
    btnSaved.imageEdgeInsets = UIEdgeInsetsMake(0, 20, 9, 0);
    [btnSaved addTarget:self action:@selector(fullHeartClick:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btnSaved];
    
    titleLable = [[UILabel alloc]initWithFrame:CGRectMake(restaurantImageView.endPointX+7, 12, width-95-7-60, 16)];
    titleLable.font=[UIFont systemFontOfSize:FONT_SIZE_14];
    titleLable.textColor = [UIColor color333333];
    [self addSubview:titleLable];
    
    ratingScoreControl=[[DLStarRatingControl alloc]initWithFrame:CGRectMake(restaurantImageView.endPointX+7, titleLable.endPointY+5, 73, 14) andStars:5 andStarWidth:13 isFractional:YES spaceWidth:2];
    ratingScoreControl.userInteractionEnabled=NO;
    [self addSubview:ratingScoreControl];
    
    reviewCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(ratingScoreControl.endPointX+3, ratingScoreControl.startPointY-1, 100, 14)];
    reviewCountLabel.font = [UIFont systemFontOfSize:FONT_SIZE_12];
    reviewCountLabel.textColor = [UIColor color999999];
    [self addSubview:reviewCountLabel];
    
    cuisineAndDistrictLabel = [[UILabel alloc] initWithFrame:CGRectMake(restaurantImageView.endPointX+7, ratingScoreControl.endPointY+5, width-15-95-10, 14)];
    cuisineAndDistrictLabel.textColor = [UIColor color999999];
    cuisineAndDistrictLabel.font=[UIFont systemFontOfSize:FONT_SIZE_12];
    cuisineAndDistrictLabel.lineBreakMode=NSLineBreakByTruncatingTail;
    [self addSubview:cuisineAndDistrictLabel];
    
    lblWellKnown = lblWellKnown = [[UILabel alloc] initWithFrame:CGRectMake(restaurantImageView.endPointX+7, cuisineAndDistrictLabel.endPointY+3, width-15-95-10, 14)];
    lblWellKnown.font = [UIFont systemFontOfSize:FONT_SIZE_12];
    lblWellKnown.textColor = [UIColor color999999];
    [self addSubview:lblWellKnown];
    
    lblDate = lblDate=[[UILabel alloc]initWithFrame:CGRectMake(restaurantImageView.endPointX+7, lblWellKnown.endPointY+3,width-15-95-10, 14)];
    lblDate.font=[UIFont systemFontOfSize:FONT_SIZE_12];
    lblDate.textColor = [UIColor colorWithRed:70/255.0f green:163/255.0f blue:131/255.0f alpha:1.0f];
    [self addSubview:lblDate];
    
    badgeImgaeView = [[UIImageView alloc] initWithFrame:CGRectMake(restaurantImageView.endPointX+7, lblDate.endPointY+6, 14, 14)];
    [self addSubview:badgeImgaeView];
    
    lblStatus = [[UILabel alloc] initWithFrame:CGRectMake(restaurantImageView.endPointX+7+15, lblDate.endPointY+5,width-15-95-10-15, 40)];
    lblStatus.font = [UIFont systemFontOfSize:FONT_SIZE_12];
    lblStatus.textColor = [UIColor color999999];
    lblStatus.numberOfLines = 2;
    
    [self addSubview:lblStatus];
    
}

- (void)setRestaurant:(IMGRestaurant *)restaurant{
    _restaurant=restaurant;
    titleLable.text = restaurant.title;
    NSString *dollar;
    if (restaurant.priceName ==nil) {
        switch(restaurant.priceLevel.intValue){
            case 1:dollar=@"Below 100K";break;
            case 2:dollar=@"100K - 200K";break;
            case 3:dollar=@"200K - 300K";break;
            case 4:dollar=@"Start from 300K";break;
        }
    }else{
        dollar = restaurant.priceName;
    }
    
    NSString *status;
    NSString *cuisineAndDistrictText;

    if ([restaurant.cuisineName isEqualToString:@""]) {
        cuisineAndDistrictText = [NSString stringWithFormat:@"%@ • %@",restaurant.districtName,dollar];
    }else{
        cuisineAndDistrictText = [NSString stringWithFormat:@"%@ • %@ • %@",_restaurant.cuisineName,restaurant.districtName,dollar];
    }
    
    cuisineAndDistrictLabel.text=cuisineAndDistrictText;
    
    if ([restaurant.saved isEqual:@1]) {
        [btnSaved setImage:[UIImage imageNamed:@"ic_heart_full"] forState:UIControlStateNormal];
    }else{
        [btnSaved setImage:[UIImage imageNamed:@"ic_heart_map"] forState:UIControlStateNormal];
    }
    
    float score = [restaurant.ratingScore floatValue];
    
    int scoreInt =(int)roundf(score);
    if (scoreInt/2.0-scoreInt/2>0) {
        [ratingScoreControl updateRating:[NSNumber numberWithFloat:scoreInt]];
        
    }else{
        [ratingScoreControl updateRating:[NSNumber numberWithFloat:scoreInt]];
    }
    
    NSString *distance = [_restaurant distance];
    //if ([restaurant.reviewCount intValue]!=0) {
    if ([distance isEqualToString:@""]){
        reviewCountLabel.text = [NSString stringWithFormat:@"(%@)",_restaurant.reviewCount];
    }else{
        reviewCountLabel.text = [NSString stringWithFormat:@"(%@) • %@",_restaurant.reviewCount,distance];
    }
    
//    }else{
//        
//        reviewCountLabel.hidden=YES;
//    }
//    
    UIImage *placeHoderImage =[[UIImage imageNamed:DEFAULT_IMAGE_STRING] imageByScalingAndCroppingForSize:CGSizeMake(95,147)];
    NSString *urlString = [_restaurant.imageUrl ImgURLStringWide:95 High:147];
    __weak typeof(restaurantImageView) weakRestaurantImageView = restaurantImageView;
    [restaurantImageView sd_setImageWithURL:[NSURL URLWithString:urlString] placeholderImage:placeHoderImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (!image && error) {
            weakRestaurantImageView.image = placeHoderImage;
        }else{
            image = [image imageByScalingAndCroppingForSize:CGSizeMake(95, 147)];
            weakRestaurantImageView.image = image;
        }
        
    }];
    
    if ([restaurant.wellKnownFor isEqualToString:@""]) {
        lblWellKnown.frame = CGRectMake(restaurantImageView.endPointX+7, cuisineAndDistrictLabel.endPointY, self.frame.size.width-15-95-10, 0);
        
    }else{
        lblWellKnown.frame = CGRectMake(restaurantImageView.endPointX+7, cuisineAndDistrictLabel.endPointY+3, self.frame.size.width-15-95-10, 14);

        lblWellKnown.text = [restaurant.wellKnownFor stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    }
    lblDate.frame = CGRectMake(restaurantImageView.endPointX+7, lblWellKnown.endPointY+3,self.frame.size.width-15-95-10, 14);
    lblStatus.frame = CGRectMake(restaurantImageView.endPointX+7+17, lblDate.endPointY+6,self.frame.size.width-15-95-10-15, 40);
    
    badgeImgaeView.frame = CGRectMake(restaurantImageView.endPointX+7, lblDate.endPointY+5, 14, 14);

    if ([restaurant.trendingInstagram boolValue]) {
        badgeImgaeView.image = [UIImage imageNamed:@"ic_trendingInstagram"];
        lblStatus.text = @"Trending on instagram";
    }else if ([restaurant.inJournal  boolValue]){
        badgeImgaeView.image = [UIImage imageNamed:@"badges_qraved_small"];
        lblStatus.text = [restaurant.journaArticlelTitle stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    }
    //lblStatus.numberOfLines = 2;
    [lblStatus sizeToFit];
    
    NSInteger weekNum=[self returnTodayIsWeekDay];
    NSString* weekDay=[self returnWeekDayWithNumber:weekNum];
    
    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"HH:mm"];
    NSString *  locationString=[dateformatter stringFromDate:senddate];
    NSString* hours=[locationString substringToIndex:2];
    NSRange rang={3,2};
    NSString* min=[locationString substringWithRange:rang];
    int curenttime=[hours intValue]*3600+[min intValue]*60;
    NSString *endStatus ;
    if ([restaurant.state intValue]==6||[restaurant.state intValue]==1) {
        
        if (restaurant.nextopenTime == nil) {
            return;
        }
        
        if ([restaurant.nextopenTime isEqualToString:@""] || [restaurant.nextclosedTime isEqualToString:@""]) {
            status = @"Closed";
        }
        NSString *closedTime = restaurant.nextclosedTime;
        if ([weekDay isEqualToString:restaurant.nextopenDay]) {
            if ([restaurant.nextopenTime isEqualToString:@"00:00"]&&[restaurant.nextclosedTime isEqualToString:@"00:00"]) {
                closedTime = @"24:00";
                
            }
            NSString *openHour = [restaurant.nextopenTime substringToIndex:2];
            NSString *openMin = [restaurant.nextopenTime substringWithRange:rang];
            NSString *closeHour = [closedTime substringToIndex:2];
            NSString *closeMin = [closedTime substringWithRange:rang];
            
            int openTime = [openHour intValue]*3600+[openMin intValue]*60;
            int closeTime = [closeHour intValue]*3600+[closeMin intValue]*60;
            if (openTime < closeTime) {
                if (openTime <= curenttime && curenttime <= closeTime) {
                    status = @"open";
//                    endStatus = [NSString stringWithFormat:@"until %@",restaurant.nextclosedTime];
                     endStatus = @"now";
                }else if (curenttime < openTime){
                    status = @"closed";
                    endStatus = [NSString stringWithFormat:@"• Open at %@",restaurant.nextopenTime];
                    lblDate.textColor = [UIColor color999999];
                }else if (curenttime > closeTime){
                    status = @"closed";
                    endStatus = [NSString stringWithFormat:@"• Open on %@",[restaurant.secondopenDay uppercaseString]];
                    lblDate.textColor = [UIColor color999999];
                    //open day
                }
            }else if (openTime >closeTime){
                if (openTime<=curenttime) {
                    status = @"open";
//                    endStatus = [NSString stringWithFormat:@"until %@",restaurant.nextclosedTime];
                     endStatus = @"now";
                    lblDate.textColor = [UIColor colorWithRed:70/255.0f green:163/255.0f blue:131/255.0f alpha:1.0f];
                }else{
                    //
                    status = @"closed";
                    endStatus = [NSString stringWithFormat:@"• Open at %@",restaurant.nextopenTime];
                    lblDate.textColor = [UIColor color999999];
                }
            }else{
                status = @"open";
//                endStatus = [NSString stringWithFormat:@"until %@",restaurant.nextclosedTime];
                 endStatus = @"now";
                lblDate.textColor = [UIColor colorWithRed:70/255.0f green:163/255.0f blue:131/255.0f alpha:1.0f];
            }
            
        }else{
            status = @"closed";
            endStatus = [NSString stringWithFormat:@"• Open on %@",[restaurant.nextopenDay uppercaseString]];//大写
            lblDate.textColor = [UIColor color999999];
        }
        
        
        
        NSString* lastWeekDay=[self returnWeekDayWithNumber:weekNum-1];
        
        long lastCloseTime;
        if (restaurant.yesteropenDay != nil && [restaurant.yesteropenDay isEqualToString:lastWeekDay]) {
            lastCloseTime = [restaurant.yesterclosedTime longLongValue];
            
            if (curenttime<=lastCloseTime) {
                status = @"open";
//                endStatus = [NSString stringWithFormat:@"until %@",restaurant.yesterclosedTime];
                 endStatus = @"now";
                lblDate.textColor = [UIColor colorWithRed:70/255.0f green:163/255.0f blue:131/255.0f alpha:1.0f];
            }
        }
        
        
        //next time  和 close time进行比较  转成同一类型
    }else{
        status = @"Closed";
        lblDate.textColor = [UIColor color999999];
        lblDate.text = status;
        return;
    }
    
    lblDate.text=[NSString stringWithFormat:@"%@ %@",[status capitalizedString],[endStatus stringByReplacingOccurrencesOfString:@"00:00" withString:@"24：00"]];
}

- (void)fullHeartClick:(UIButton *)button{
    if (self.buttonSavedClick) {
        self.buttonSavedClick(self.restaurant);
    }
}

- (void)refreshSavedButtonStyle:(BOOL)isSaved{
    if (isSaved) {
        [btnSaved setImage:[UIImage imageNamed:@"ic_heart_full"] forState:UIControlStateNormal];
    }else{
        [btnSaved setImage:[UIImage imageNamed:@"ic_heart_map"] forState:UIControlStateNormal];
    }
}

-(NSString*)returnWeekDayWithNumber:(NSInteger)number
{
    
    switch (number) {
        case 1:
            return [NSString stringWithFormat:@"Sun"];
            break;
        case 2:
            return [NSString stringWithFormat:@"Mon"];
            break;
        case 3:
            return [NSString stringWithFormat:@"Tue"];
            break;
        case 4:
            return [NSString stringWithFormat:@"Wed"];
            break;
        case 5:
            return [NSString stringWithFormat:@"Thu"];
            break;
        case 6:
            return [NSString stringWithFormat:@"Fri"];
            break;
        case 7:
            return [NSString stringWithFormat:@"Sat"];
            break;
        default:
            break;
    }
    
    
    return nil;
    
    
}

-(NSInteger)returnTodayIsWeekDay{
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *now = [NSDate date];;
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    NSInteger unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday |
    NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    comps = [calendar components:unitFlags fromDate:now];
    NSInteger weekDay=[comps weekday];
    NSLog(@"today is %ld day ",(long)weekDay);
    return weekDay;
    
}


@end
