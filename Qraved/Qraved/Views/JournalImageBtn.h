//
//  JournalImageBtn.h
//  Qraved
//
//  Created by System Administrator on 9/7/15.
//  Copyright (c) 2015 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JournalImageBtn : UIView

@property(nonatomic,strong) UIImage *image;
@property(nonatomic,strong) UILabel *label;
@property(nonatomic,strong) UIButton *button;

@property(nonatomic,assign) CGSize imageSize;
@property(nonatomic,strong) NSString *imageName;
@property(nonatomic,strong) NSString *text;
@property(nonatomic,strong) UIFont *font;

@property(nonatomic,assign) NSInteger margin;
@property(nonatomic,assign) NSInteger padding_left;
@property(nonatomic,assign) NSInteger padding_right;

-(instancetype)initWithFrame:(CGRect)frame image:(NSString*)image text:(NSString*)text margin:(NSInteger)margin target:(id)target action:(SEL)action;
-(instancetype)initWithFrame:(CGRect)frame image:(NSString*)image size:(CGSize)size text:(NSString*)text margin:(NSInteger)margin target:(id)target action:(SEL)selector;
@end
