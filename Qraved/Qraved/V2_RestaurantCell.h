//
//  V2_RestaurantCell.h
//  Qraved
//
//  Created by harry on 2017/6/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DLStarRatingControl.h"
#import "V2_RestaurantModel.h"

@protocol V2_RestaurantCellDelegate <NSObject>

- (void)savedToList:(IMGRestaurant *)restaurant IndexPath:(NSIndexPath *)indexPath;

@end

@interface V2_RestaurantCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *restaurantImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (nonatomic, strong)DLStarRatingControl *starRatingControl;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UILabel *lblPriceLevel;
@property (weak, nonatomic) IBOutlet UILabel *lblWellknown;
@property (weak, nonatomic) IBOutlet UIButton *btnSaved;
@property (weak, nonatomic) IBOutlet UILabel *lblState;
@property (weak, nonatomic) IBOutlet UIImageView *stateImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblWellkonwnHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lblWellknownSpace;
- (IBAction)savedClick:(id)sender;

@property (copy, nonatomic) IMGRestaurant *model;
@property (assign, nonatomic) NSIndexPath *currentIndex;
@property (assign, nonatomic) id<V2_RestaurantCellDelegate>delegate;

- (void)freshSavedButton:(BOOL)saved;

@end
