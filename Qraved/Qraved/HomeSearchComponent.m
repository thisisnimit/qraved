//
//  HomeSearchComponent.m
//  Qraved
//
//  Created by Tek Yin on 5/9/17.
//  Copyright © 2017 Imaginato. All rights reserved.
//

#import "HomeSearchComponent.h"

@implementation HomeSearchComponent

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
//        self = [HomeSearchComponent construct];
    }

    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
//        self = [HomeSearchComponent construct];
    }

    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.isExpanded = YES;
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 5;
}


+ (HomeSearchComponent *)create {
    NSBundle            *bundle = [NSBundle bundleForClass:[self class]];
    HomeSearchComponent *view   = [bundle loadNibNamed:@"HomeSearchComponent" owner:self options:nil][0];
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    return view;
}

//- (void)shrink {
//    self.isExpanded = NO;
//    // decreases the corner radius
//    CABasicAnimation *cornerRadiusAnim = [CABasicAnimation new];
//    cornerRadiusAnim.duration            = 0.5;
//    cornerRadiusAnim.fromValue           = @(0);
//    cornerRadiusAnim.toValue             = @(20);
//    cornerRadiusAnim.timingFunction      = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    cornerRadiusAnim.fillMode            = kCAFillModeForwards;
//    cornerRadiusAnim.removedOnCompletion = NO;
//    [self.layer addAnimation:cornerRadiusAnim forKey:@"cornerRadius"];
//
//    [UIView animateWithDuration:0.5
//                     animations:^{
//                         self.width             = 40;
//                         self.uSearchText.alpha = 0;
//                     }];
//}
//
//- (void)expand {
//    self.isExpanded = YES;
//    // decreases the corner radius
//    CABasicAnimation *cornerRadiusAnim = [CABasicAnimation new];
//    cornerRadiusAnim.duration            = 0.5;
//    cornerRadiusAnim.fromValue           = @(20);
//    cornerRadiusAnim.toValue             = @(0);
//    cornerRadiusAnim.timingFunction      = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    cornerRadiusAnim.fillMode            = kCAFillModeForwards;
//    cornerRadiusAnim.removedOnCompletion = NO;
//    [self.layer addAnimation:cornerRadiusAnim forKey:@"cornerRadius"];
//
//    [UIView animateWithDuration:0.5
//                     animations:^{
//                         self.width             = self.originalFrame.size.width;
//                         self.uSearchText.alpha = 1;
//                     }];
//
//}

//- (void)initialFrame:(CGRect)rect {
//    self.originalFrame = rect;
//    self.frame         = rect;
//}
@end
