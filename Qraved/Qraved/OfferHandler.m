//
//  OfferHandler.m
//  Qraved
//
//  Created by josn.liu on 2016/12/19.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "OfferHandler.h"
#import "IMGOfferType.h"
#import "DBManager.h"

@implementation OfferHandler
+(void)getOffersFromServer:(void(^)(NSArray* offerArr))successBlock andError:(void(^)(NSError *error))errorBlock
{
    
    [[IMGNetWork sharedManager] GET:@"app/search/filter/offertype/list" parameters:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         NSMutableArray *offersList=[[NSMutableArray alloc]init];
         NSArray *offerListArr=[responseObject objectForKey:@"offerTypeList"];
         for (NSDictionary *dic in offerListArr) {
             IMGOfferType *offerType=[[IMGOfferType alloc] init];
             offerType.eventType=[NSNumber numberWithInt:0];
             //             offerType.offerId =[dic objectForKey:@""];
             offerType.typeId =[dic objectForKey:@"objectId"];
             offerType.off =[dic objectForKey:@"off"];
             offerType.name = [dic objectForKey:@"name"];
             [offersList addObject:offerType];
             
         }
         successBlock(offersList);
         
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error) {
         NSLog(@"AFHTTPRequestOperation erors when get offertype/list datas:%@",error.localizedDescription);
         errorBlock(error);
     }];
    
}
+(void)getOffersFromServer{
    
    [[IMGNetWork sharedManager] GET:@"app/search/filter/offertype/list" parameters:nil success:^(NSURLSessionDataTask *operation, id responseObject)
     {
         
         
         FMDatabaseQueue *queue = [FMDatabaseQueue databaseQueue];
         [queue inDatabase:^(FMDatabase *db) {
             [db executeUpdate:@"delete from IMGOfferType"];
//             NSArray *array = [responseObject objectForKey:@"offerTypeList"];
             
             for (NSDictionary *dic in responseObject) {
                 IMGOfferType *offerType=[[IMGOfferType alloc] init];
                 offerType.eventType=[NSNumber numberWithInt:0];
                 //             offerType.offerId =[dic objectForKey:@""];
                 offerType.typeId =[dic objectForKey:@"id"];
                 offerType.off =[dic objectForKey:@"off"];
                 offerType.name = [dic objectForKey:@"name"];
                 [[DBManager manager]insertModel:offerType selectField:@"name" andSelectID:offerType.name andFMDatabase:db];
             }
             
         }];
         
     }failure:^(NSURLSessionDataTask *operation, NSError *error) {
         NSLog(@"AFHTTPRequestOperation erors when get offertype/list datas:%@",error.localizedDescription);
     }];
    
}


@end
