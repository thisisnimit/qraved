//
//  SavedDataHandler.h
//  Qraved
//
//  Created by apple on 2017/5/15.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SavedDataHandler : NSObject

+(void)getSavedPreviousRestaurantsWithPara:(NSDictionary *)parameter andBlock:(void(^)(NSMutableArray *restaurantsArr))block;
+(void)getWantToGoListWithPara:(NSDictionary *)parameter andBlock:(void(^)(NSArray *restaurantsArr,NSNumber *restaurantListItemCount,NSNumber *listId))block andFailedBlock:(void(^)())FailedBlock;
@end
