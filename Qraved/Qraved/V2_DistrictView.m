//
//  V2_DistrictView.m
//  Qraved
//
//  Created by harry on 2017/6/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_DistrictView.h"
#import "HMSegmentedControl.h"
#import "V2_DistrictModel.h"
#import "DistrictCell.h"
#import <BlocksKit/BlocksKit+UIKit.h>
#import "V2_LovePreferenceLayout.h"
#import "V2_InstagramDistrictCell.h"
@interface V2_DistrictView ()<UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>{
    UIScrollView *districtScrollView;
    UICollectionView *districtCollectionView;
    HMSegmentedControl *tab;
    //NSMutableArray *selectedArr;
    NSMutableArray *collectionViewArr;
    NSInteger currentIndex;
}

@end

@implementation V2_DistrictView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    self.backgroundColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.4];
    
    currentIndex = 0;
    //selectedArr  = [NSMutableArray array];
    collectionViewArr = [NSMutableArray array];
    
}

- (void)createCollectionView{
    for (int i = 0; i < self.districtArr.count; i ++) {
        
        V2_LovePreferenceLayout *layout = [[V2_LovePreferenceLayout alloc] init];
        
        districtCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(DeviceWidth * i, 0, DeviceWidth, districtScrollView.bounds.size.height) collectionViewLayout:layout];
        districtCollectionView.delegate = self;
        districtCollectionView.dataSource = self;
        districtCollectionView.tag  = i;
        districtCollectionView.backgroundColor = [UIColor whiteColor];
        districtCollectionView.allowsMultipleSelection = YES;
        
        [districtScrollView addSubview:districtCollectionView];
        [collectionViewArr addObject:districtCollectionView];
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSInteger index = collectionView.tag;
    V2_DistrictModel *model = [self.districtArr objectAtIndex:index];
    
    return model.districts.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
     [collectionView registerClass:[V2_InstagramDistrictCell class] forCellWithReuseIdentifier:CellIdentifier];
    V2_InstagramDistrictCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
  
    NSInteger index = collectionView.tag;
    V2_DistrictModel *districtModel = [self.districtArr objectAtIndex:index];
    V2_AreaModel *areaModel = [districtModel.districts objectAtIndex:indexPath.row];
    
    __weak typeof(self) weakSelf = self;
    cell.chooseDistrict = ^(V2_AreaModel *model){
        [weakSelf removeOtherDistrict:model];
        if ([model.name isEqualToString:@"All"]) {
            if (self.selectedDistrict) {
                self.selectedDistrict(districtModel);
            }
        }else if ([model.name isEqualToString:@"My Location"]){
            if (self.nearbyCheck) {
                self.nearbyCheck();
            }
        }else{
            if (self.selectedDistrict) {
                self.selectedDistrict(model);
            }
        }
        
        [weakSelf removeFromSuperview];
    };
    
    cell.model = areaModel;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger index = collectionView.tag;
    V2_DistrictModel *model = [self.districtArr objectAtIndex:index];
    V2_AreaModel *areaModel = [model.districts objectAtIndex:indexPath.row];
    NSString *title = areaModel.name;
    CGFloat tagLabelW = [title boundingRectWithSize:CGSizeMake(DeviceWidth, MAXFLOAT)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,NSFontAttributeName:[UIFont systemFontOfSize:12]}
                                            context:nil].size.width;
    
    if ([title isEqualToString:@"My Location"]) {
        return CGSizeMake(tagLabelW + 55, 32);
    }else if ([title isEqualToString:@"All"]){
        return CGSizeMake(tagLabelW + 40, 32);
    }else{
        if ([areaModel.landMarkTypeId isEqual:@1] || [areaModel.landMarkTypeId isEqual:@2]) {
            return CGSizeMake(tagLabelW + 45, 32);
        }else{
            return CGSizeMake(tagLabelW + 30, 32);
        }
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self removeFromSuperview];
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == districtScrollView) {
        int index = scrollView.contentOffset.x / DeviceWidth;
        currentIndex = index;
        [tab setSelectedSegmentIndex:index];
    }
   
}
- (void)changeLocationSkip{
    currentIndex = 1;
    districtScrollView.contentOffset = CGPointMake(DeviceWidth , 0);
}
- (void)setDistrictArr:(NSArray *)districtArr{
    _districtArr = districtArr;
    
    NSMutableArray *nameArr = [NSMutableArray array];
    
    for (V2_DistrictModel *model in districtArr) {
        [nameArr addObject:model.name];
    }
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height/3, DeviceWidth, DeviceHeight +20 - self.bounds.size.height/3)];
    bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:bgView];
    
    
    tab=[[HMSegmentedControl alloc] initWithSectionTitles:nameArr];
    tab.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    tab.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    tab.selectionIndicatorColor = [UIColor colorWithHexString:@"DE2029"];
    tab.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor colorWithHexString:@"DE2029"],NSFontAttributeName:[UIFont systemFontOfSize:14]};
    tab.titleTextAttributes = @{NSFontAttributeName:[UIFont systemFontOfSize:14]};
    tab.backgroundColor = [UIColor colorWithHexString:@"FAFAFA"];
    tab.selectionIndicatorHeight = 2.0;
    [tab setFrame:CGRectMake(0,0,DeviceWidth,45)];
    districtScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 45, DeviceWidth, DeviceHeight +20 - self.bounds.size.height/3 - 45)];
    districtScrollView.delegate  = self;
    districtScrollView.pagingEnabled = YES;
    districtScrollView.showsHorizontalScrollIndicator = NO;
    [bgView addSubview:districtScrollView];
    [self createCollectionView];
    
    [districtScrollView setContentSize:CGSizeMake(nameArr.count * DeviceWidth, 0)];

    __weak typeof (districtScrollView) weakDistrictScrollView = districtScrollView;
    [tab setIndexChangeBlock:^(NSInteger index){
        NSLog(@"-------");
        weakDistrictScrollView.contentOffset = CGPointMake(DeviceWidth *index, 0);
        
    }];
    
    [bgView addSubview:tab];
}

- (void)removeOtherDistrict:(V2_AreaModel *)areaModel{
    for (int i = 0; i < self.districtArr.count; i ++) {
        V2_DistrictModel *districtModel = [self.districtArr objectAtIndex:i];
        for (V2_AreaModel *model in districtModel.districts) {
            if ([model.name isEqualToString:@"All"] &&![model.districtId isEqual:areaModel.districtId]) {
                model.selected = NO;
            }else if (![model.name isEqualToString:areaModel.name]&&![model.districtId isEqual:areaModel.districtId]){
                model.selected = NO;
            }
        }
    }
}



@end
