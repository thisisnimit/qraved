//
//  V2_BrandModel.h
//  Qraved
//
//  Created by harry on 2017/6/16.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface V2_BrandModel : NSObject

@property (nonatomic, copy) NSString *imageUrl;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSNumber *objectId;
@property (nonatomic, copy) NSNumber *objectType;
@property (nonatomic, strong)  NSString  *brandType;
@property (nonatomic, copy) NSString *state;


@end
