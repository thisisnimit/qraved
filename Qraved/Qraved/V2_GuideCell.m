//
//  V2_GuideCell.m
//  Qraved
//
//  Created by harry on 2017/6/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_GuideCell.h"
#import "WebServiceV2.h"
#import <SDWebImage/UIImage+WebP.h>
@implementation V2_GuideCell
{
    CAGradientLayer *headerLayer;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.guideIden.layer.masksToBounds = YES;
    self.guideIden.layer.cornerRadius = 8;
    self.guideImageView.backgroundColor = [UIColor colorWithHexString:@"#E6E6E6"];
    self.guideIden.hidden = YES;
}

- (void)setDiningGuideModel:(IMGDiningGuide *)diningGuideModel{
    _diningGuideModel = diningGuideModel;
    
    self.guideIden.hidden = NO;
    self.guideImageView.backgroundColor = [UIColor whiteColor];
    [self.guideImageView sd_setImageWithURL:[NSURL URLWithString:[diningGuideModel.headerImageThumbnail returnFullImageUrl]] placeholderImage:[UIImage imageNamed:@"placeholder"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    self.guideName.text = [diningGuideModel.pageName uppercaseString];
    [self insertTransparentGradient];

}
- (void)setModel:(IMGDiningGuide *)model{
    _model = model;
    NSString *imageUrl;
    if ([Tools isBlankString:model.headerImageThumbnail]) {
        imageUrl = model.headerImage;
    }else{
        imageUrl = model.headerImageThumbnail;
    }
    [self.guideImageView sd_setImageWithURL:[NSURL URLWithString:[imageUrl returnFullImageUrl]] placeholderImage:[UIImage imageNamed:@"placeholder"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
 
    self.guideName.text = [model.pageName uppercaseString];
    [self insertTransparentGradient];
}

- (void)setGuideModel:(IMGGuideModel *)guideModel
{

    _guideModel = guideModel;
    [self.guideImageView sd_setImageWithURL:[NSURL URLWithString:[guideModel.coverImage returnFullImageUrl]] placeholderImage:[UIImage imageNamed:@"placeholder"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
    
    
    self.guideName.text = [guideModel.name uppercaseString];
}
- (void) insertTransparentGradient {
    UIColor *colorOne = [UIColor colorWithWhite:0 alpha:0];
    UIColor *colorTwo = [UIColor colorWithWhite:0 alpha:1];
    NSArray *colors = [NSArray arrayWithObjects:(id)colorOne.CGColor, colorTwo.CGColor, nil];
    NSNumber *stopOne = [NSNumber numberWithFloat:0.5];
    NSNumber *stopTwo = [NSNumber numberWithFloat:1.0];
    NSArray *locations = [NSArray arrayWithObjects:stopOne, stopTwo, nil];
    
    //crate gradient layer
    if (!headerLayer) {
        headerLayer = [CAGradientLayer layer];
        
        headerLayer.colors = colors;
        headerLayer.locations = locations;
        
        headerLayer.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
        [self.guideImageView.layer insertSublayer:headerLayer atIndex:0];
    }

}
@end
