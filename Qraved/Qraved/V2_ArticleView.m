//
//  V2_ArticleView.m
//  Qraved
//
//  Created by harry on 2017/6/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "V2_ArticleView.h"
#import "UIColor+Hex.h"
#import "V2_ArticleCell.h"
@interface V2_ArticleView ()<UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource>{
    UICollectionView *articleCollectionView;
    UILabel *lblTitle;
}

@end

@implementation V2_ArticleView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createUI];
    }
    return self;
}

- (void)createUI{
    self.backgroundColor = [UIColor whiteColor];
    
    lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 17, DeviceWidth-90, 16)];
    lblTitle.textColor = [UIColor color333333];
    lblTitle.text = @"Journals";
    lblTitle.font = [UIFont boldSystemFontOfSize:14];
    [self addSubview:lblTitle];
    
//    UIButton *btnChange = [UIButton buttonWithType:UIButtonTypeCustom];
//    btnChange.frame = CGRectMake(15, 0, DeviceWidth-30, 50);
//    [btnChange addTarget:self action:@selector(seeAllClick) forControlEvents:UIControlEventTouchUpInside];
//    [self addSubview:btnChange];
//    [btnChange setImage:[UIImage imageNamed:@"ic_home_arrow"] forState:UIControlStateNormal];
//    btnChange.imageEdgeInsets = UIEdgeInsetsMake(0, DeviceWidth-30-10, 0, 0);
//    
//    UILabel *lblSeeAll = [[UILabel alloc] initWithFrame:CGRectMake(DeviceWidth-95, 6, 50, 38)];
//    lblSeeAll.text = @"See All";
//    lblSeeAll.textAlignment = NSTextAlignmentRight;
//    lblSeeAll.textColor = [UIColor color999999];
//    lblSeeAll.font =[UIFont systemFontOfSize:12];
//    [btnChange addSubview:lblSeeAll];
    
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(125, 158);
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 10;
    layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 0);
    
    articleCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,50, DeviceWidth, 158) collectionViewLayout:layout];
    articleCollectionView.backgroundColor = [UIColor whiteColor];
    articleCollectionView.delegate = self;
    articleCollectionView.dataSource = self;
    articleCollectionView.showsHorizontalScrollIndicator = NO;
    [self addSubview:articleCollectionView];
    
    [articleCollectionView registerNib:[UINib nibWithNibName:@"V2_ArticleCell" bundle:nil] forCellWithReuseIdentifier:@"articleCell"];
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.articleArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    V2_ArticleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"articleCell" forIndexPath:indexPath];

    IMGMediaComment *model = [self.articleArray objectAtIndex:indexPath.row];
    cell.model = model;
    cell.isBrand = YES;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    IMGMediaComment *journal = [self.articleArray objectAtIndex:indexPath.row];
    if (self.gotoJournalDetail) {
        self.gotoJournalDetail(journal);
    }
}

- (void)setArticleArray:(NSArray *)articleArray{
    _articleArray = articleArray;
    [self setupAutoHeightWithBottomView:articleCollectionView bottomMargin:0];
    [articleCollectionView reloadData];
    
}
//- (void)setName:(NSString *)name{
//    _name = name;
//    lblTitle.text = name;
//}

- (void)seeAllClick{
    if (self.seeAllJournal) {
        self.seeAllJournal();
    }
}

@end
