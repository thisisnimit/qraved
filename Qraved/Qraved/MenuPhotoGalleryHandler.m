//
//  MenuPhotoGalleryHandler.m
//  Qraved
//
//  Created by imaginato on 16/4/7.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "MenuPhotoGalleryHandler.h"
#import "IMGMenu.h"

@implementation MenuPhotoGalleryHandler

+(void)getMenuGalleryDataFromService:(NSNumber *)restaurantId offset:(NSNumber*)offset andBlock:(void (^)(NSArray *menuList,NSNumber *menuPhotoCount,NSNumber *menuPhotoCountTypeRestant,NSNumber *menuPhotoCountTypeBar,NSNumber *menuPhotoCountTypeDelivery,IMGMenu *menu_))block{
   
    NSDictionary *parameters=@{@"restaurantId":restaurantId,@"offset":offset,@"max":@50};
    
    [[IMGNetWork sharedManager]POST:@"app/restaurant/menuphoto/list" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSArray *menuPhotoList = [[NSArray alloc] init];
        //        offset = offset + max;
        menuPhotoList = [responseObject objectForKey:@"menuPhotoList"];
        NSNumber *menePhotoCount = [responseObject objectForKey:@"menuPhotoCount"];
        NSNumber *menuPhotoCountTypeRestant = [responseObject objectForKey:@"menuPhotoCountType1"];
        NSNumber *menuPhotoCountTypeBar = [responseObject objectForKey:@"menuPhotoCountType2"];
        NSNumber *menuPhotoCountTypeDelivery = [responseObject objectForKey:@"menuPhotoCountType3"];
        IMGMenu *menu = [[IMGMenu alloc] init];
        menu.menuPhotoCount = menePhotoCount;
        menu.menuPhotoCountTypeRestant = menuPhotoCountTypeRestant;
        menu.menuPhotoCountTypeBar = menuPhotoCountTypeBar;
        menu.menuPhotoCountTypeDelivery = menuPhotoCountTypeDelivery;
        
        block(menuPhotoList,menePhotoCount,menuPhotoCountTypeRestant,menuPhotoCountTypeBar,menuPhotoCountTypeDelivery,menu);
     
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
         NSLog(@"getPhotoGalleryDataFromService error: %@",error.localizedDescription);
    }];


}
+(void)getSelfMenuGalleryDataFromService:(NSNumber *)restaurantId userId:(NSNumber*)userId offset:(int )offset andBlock:(void (^)(NSArray *menuList,NSNumber *menuPhotoCount,NSNumber *menuPhotoCountTypeRestant,NSNumber *menuPhotoCountTypeBar,NSNumber *menuPhotoCountTypeDelivery,IMGMenu *menu_))block{
    
    NSDictionary *parameters=@{@"restaurantId":restaurantId,@"offset":[NSString stringWithFormat:@"%d",offset],@"userId":userId,@"max":@50};
    //app/restaurant/menuphoto/userupload/list?
    [[IMGNetWork sharedManager]POST:@"app/restaurant/menuphoto/userupload/list" parameters:parameters progress:nil success:^(NSURLSessionDataTask *operation, id responseObject) {
        NSArray *menuPhotoList = [[NSArray alloc] init];
        //        offset = offset + max;
        menuPhotoList = [responseObject objectForKey:@"menuPhotoList"];
        NSNumber *menePhotoCount = [responseObject objectForKey:@"menuPhotoCount"];
        NSNumber *menuPhotoCountTypeRestant = [responseObject objectForKey:@"menuPhotoCountType1"];
        NSNumber *menuPhotoCountTypeBar = [responseObject objectForKey:@"menuPhotoCountType2"];
        NSNumber *menuPhotoCountTypeDelivery = [responseObject objectForKey:@"menuPhotoCountType3"];
        IMGMenu *menu = [[IMGMenu alloc] init];
        menu.menuPhotoCount = menePhotoCount;
        menu.menuPhotoCountTypeRestant = menuPhotoCountTypeRestant;
        menu.menuPhotoCountTypeBar = menuPhotoCountTypeBar;
        menu.menuPhotoCountTypeDelivery = menuPhotoCountTypeDelivery;
        
        block(menuPhotoList,menePhotoCount,menuPhotoCountTypeRestant,menuPhotoCountTypeBar,menuPhotoCountTypeDelivery,menu);
        
        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        NSLog(@"getPhotoGalleryDataFromService error: %@",error.localizedDescription);
    }];
    
    
}

@end
