//
//  V2_RestaurantModel.h
//  Qraved
//
//  Created by harry on 2017/6/19.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface V2_RestaurantModel : NSObject

@property (nonatomic,copy) NSDictionary *city;
@property (nonatomic,copy) NSDictionary *district;
@property (nonatomic,copy) NSString *cuisine;
@property (nonatomic,copy) NSString *featured_article;
@property (nonatomic,copy) NSString *latitude;
@property (nonatomic,copy) NSString *longitude;
@property (nonatomic,copy) NSString *photo;
@property (nonatomic,copy) NSString *link;
@property (nonatomic,copy) NSString *price_level;
@property (nonatomic,copy) NSNumber *rating;
@property (nonatomic,copy) NSNumber *rating_count;
@property (nonatomic,copy) NSNumber *restaurant_id;
@property (nonatomic,copy) NSNumber *trending_instagram;
@property (nonatomic,copy) NSString *restaurant_landmark;
@property (nonatomic,copy) NSString *restaurant_name;
@property (nonatomic,copy) NSString *seo_keyword;
@property (nonatomic,copy) NSString *short_desc;

-(NSString *)distance;

@end
