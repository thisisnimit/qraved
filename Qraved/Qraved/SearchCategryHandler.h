//
//  SearchCategryHandler.h
//  Qraved
//
//  Created by josn.liu on 2017/1/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchCategryHandler : NSObject
+(void)getSearchAllCategryDataFromeService:(NSDictionary*)dic andSuccessBlock:(void(^)(NSArray* cusinearr,NSArray* landmark,NSArray* district,NSArray* dininguide))successBlock anderrorBlock:(void(^)( NSString *str))errorBlock;
@end
