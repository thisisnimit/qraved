//
//  CouponHandler.h
//  Qraved
//
//  Created by harry on 2018/1/22.
//  Copyright © 2018年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CouponModel.h"
#import "BrandModel.h"
@interface CouponHandler : NSObject


+(void)getCouponDetail:(NSDictionary*)dic andSuccessBlock:(void(^)(CouponModel *model))successBlock anderrorBlock:(void(^)())errorBlock;

+(void)getBrand:(NSDictionary*)dic andSuccessBlock:(void(^)(BrandModel *model))successBlock anderrorBlock:(void(^)())errorBlock;

+(void)getMall:(NSDictionary*)dic andSuccessBlock:(void(^)(BrandModel *model))successBlock anderrorBlock:(void(^)())errorBlock;

+(void)couponSave:(NSDictionary*)dic andSuccessBlock:(void(^)())successBlock anderrorBlock:(void(^)())errorBlock;

+(void)couponUnsave:(NSDictionary*)dic andSuccessBlock:(void(^)())successBlock anderrorBlock:(void(^)())errorBlock;

+(void)couponRedeem:(NSDictionary*)dic andSuccessBlock:(void(^)())successBlock anderrorBlock:(void(^)())errorBlock;

+(void)couponList:(NSDictionary*)dic andSuccessBlock:(void(^)(NSArray *couponList))successBlock anderrorBlock:(void(^)())errorBlock;

@end
