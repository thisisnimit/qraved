//
//  OfferHandler.h
//  Qraved
//
//  Created by josn.liu on 2016/12/19.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "DataHandler.h"

@interface OfferHandler : DataHandler
+(void)getOffersFromServer:(void(^)(NSArray* offerArr))successBlock andError:(void(^)(NSError *error))errorBlock;
+(void)getOffersFromServer;

@end
