//
//  V2_HomeModel.h
//  Qraved
//
//  Created by harry on 2017/7/12.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface V2_HomeModel : NSObject

@property (nonatomic,copy) NSString *asset;
@property (nonatomic,copy) NSArray *docs;
@property (nonatomic,copy) NSString *title;
@property (nonatomic,copy) NSNumber *componentId;
@property (nonatomic,copy) NSNumber *categoryId;

@end
