//
//  PersonalizationHandler.h
//  Qraved
//
//  Created by harry on 2017/6/16.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "V2_BrandModel.h"
#import "V2_LovePreferenceModel.h"
#import "V2_EatModel.h"
@interface PersonalizationHandler : NSObject

+ (void)getUserPersonalizationWithPrama:(NSDictionary*)param andSuccessBlock:(void(^)(id arayList, id brandList ,id cuisineList, id eatingList))successBlock anderrorBlock:(void(^)())errorBlock;
+ (void)chooseCuisineWithPrama:(NSDictionary*)param andSuccessBlock:(void(^)())successBlock anderrorBlock:(void(^)())errorBlock;

+ (void)getProfileWithPrama:(NSDictionary*)param andSuccessBlock:(void(^)(id aarayList))successBlock anderrorBlock:(void(^)())errorBlock;
@end
