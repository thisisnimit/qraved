//
//  FoodTagHeaderView.h
//  Qraved
//
//  Created by harry on 2017/11/22.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiscoverCategories.h"
@interface FoodTagHeaderView : UIView

@property (nonatomic, strong)NSArray *foodTagArr;
@property (nonatomic, copy) void(^quicklySearchFoodTag)(DiscoverCategoriesModel *model);

@end
