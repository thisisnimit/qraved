//
//  FUProgressView.h
//  fundsUP
//
//  Created by DeliveLee on 2017/5/12.
//  Copyright © 2017年 mediaBunker. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FUProgressView : UIView

@property(nonatomic,assign) CGFloat percent;
@property(nonatomic,strong) UIColor * bgColor;
@property (nonatomic, strong)   UIView *contentView;
@end
