//
//  PersonalRightViewController.h
//  PersonalHomePageDemo
//
//  Created by Kegem Huang on 2017/3/15.
//  Copyright © 2017年 huangkejin. All rights reserved.
//

#import <UIKit/UIKit.h>

//@class PersonalRightViewController;
//@protocol PersonalRightViewControllerDelegate <NSObject>
//
//-(void)dl_viewControllerDidFinishRefreshing:(PersonalRightViewController *)viewController;
//@end

@interface PersonalRightViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic,assign) BOOL isSelf;
@property(nonatomic,retain) IMGUser *otherUser;
@property (nonatomic,strong)UICollectionView * photoCollection;
@property (assign, nonatomic) BOOL canScroll;
@property(nonatomic,assign)BOOL isRefreshing;
//@property(nonatomic,weak)id<PersonalRightViewControllerDelegate> delegate;
//
//-(void)dl_refresh;
@end
