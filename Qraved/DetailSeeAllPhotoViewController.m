//
//  DetailSeeAllPhotoViewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/10.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "DetailSeeAllPhotoViewController.h"
#import "DetailSeeAllPhotoAllViewController.h"
#import "DetailSeeAllPhotoQravedViewController.h"
#import "DetailSeeAllPhotoInstagramViewController.h"
#import "IMGRestaurant.h"

CGFloat const ZLJTitlesViewH = 40;

CGFloat const ZLJTitlesViewY = 64;

@interface DetailSeeAllPhotoViewController ()<UIScrollViewDelegate>
{
    IMGRestaurant *restaurant;
     NSMutableArray *restaurantPhotoArray;
    NSNumber    *dishCount;
    NSNumber *instagramPhotoCount;
    NSNumber     *qravedCount;
    NSNumber *restaurantImagesCount;
    NSMutableArray *btnArr;
}
/**标签栏底部的红色指示器 */
@property  (nonatomic, weak)  UIView       *indicatorView;
/**当前选中的按钮 */
@property  (nonatomic, weak)  UIButton     *selectedButton;
/**顶部所有标签 */
@property  (nonatomic,  weak) UIView       *titlesView;
/**底部所有内容*/
@property  (nonatomic, weak)  UIScrollView *contentView;



@end

@implementation DetailSeeAllPhotoViewController

-(instancetype)initWithRestaurant:(IMGRestaurant*)paramRestaurant restaurantPhotoArray:(NSArray*)photoArr{
    self = [super init];
    if (self != nil) {
        restaurant = paramRestaurant;
        //        if([[photoArr firstObject] isKindOfClass:[IMGRestaurantEvent class]]){
        //            photoType = 2;
        //            eventPhotoArray = [photoArr mutableCopy];
        //        }else{
        //            photoType = 1;
        restaurantPhotoArray=[photoArr mutableCopy];
        
        //        }
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Photos";
    btnArr = [NSMutableArray array];
    [self addBackBtn];
    
    [self setupChildVces];
    
    [self setupTitleView];
    // scrollview
    [self setupContentview];
    [self initData];
    [self setNavi];

    self.view.backgroundColor = [UIColor whiteColor];
//    UIBarButtonItem *searchBtn=[[UIBarButtonItem alloc] initWithTitle:L(@"Save") style:UIBarButtonItemStylePlain target:self action:@selector(SaveButtonClick:)];
//    searchBtn.tintColor=[UIColor whiteColor];
//    self.navigationItem.rightBarButtonItem=searchBtn;


}
- (void)initData{

    [[IMGNetWork sharedManager] GET:@"app/detail/photos/v2" parameters:@{@"restaurantId":[NSString stringWithFormat:@"%@",restaurant.restaurantId]} progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        [self setupRestaurant:responseObject];
        dishCount = responseObject[@"dishCount"];
        qravedCount = responseObject[@"qravedCount"];
        instagramPhotoCount = responseObject[@"instagramPhotoCount"];
         NSArray *restaurantPhotoArray_ = [responseObject objectForKey:@"restaurantImages"];
        NSLog(@"%lu",(unsigned long)restaurantPhotoArray_.count);
        restaurantImagesCount = [NSNumber numberWithInteger:restaurantPhotoArray_.count];
       
        for (int i = 0; i < btnArr.count; i ++) {
            UIButton *button = [btnArr objectAtIndex:i];
            if (i == 0) {
                [button setTitle:[NSString stringWithFormat:@"All(%@)",[NSNumber numberWithFloat:[qravedCount floatValue] + [instagramPhotoCount floatValue] + [restaurantImagesCount floatValue]]] forState:UIControlStateNormal];
            }else if (i == 1){
                [button setTitle:[NSString stringWithFormat:@"Qraved(%@)",qravedCount] forState:UIControlStateNormal];
            }else{
                [button setTitle:[NSString stringWithFormat:@"Instagram(%@)",instagramPhotoCount] forState:UIControlStateNormal];
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
    }];
}

- (void)setupRestaurant:(id)responseObj{
    if (responseObj) {
        restaurant.goFoodLink = [responseObj objectForKey:@"goFoodLink"];
        restaurant.state = [responseObj objectForKey:@"state"];
        
        NSDictionary* yesterOpenDic=[responseObj objectForKey:@"yesterDayOpenTime"];
        restaurant.yesteropenDay=[yesterOpenDic objectForKey:@"openDay"];
        restaurant.yesteropenTime=[yesterOpenDic objectForKey:@"openTime"];
        restaurant.yesterclosedTime=[yesterOpenDic objectForKey:@"closedTime"];
        
        NSDictionary* nextOpenDay=[responseObj objectForKey:@"nextOpenDay"];
        restaurant.nextopenTime=[nextOpenDay objectForKey:@"openTime"];
        restaurant.nextclosedTime=[nextOpenDay objectForKey:@"closedTime"];
        restaurant.nextopenDay=[nextOpenDay objectForKey:@"openDay"];
        
        NSDictionary* secondOpenDic=[responseObj objectForKey:@"secondOpenDay"];
        restaurant.secondclosedTime=[secondOpenDic objectForKey:@"closedTime"];
        restaurant.secondopenTime=[secondOpenDic objectForKey:@"openTime"];
        restaurant.secondopenDay=[secondOpenDic objectForKey:@"openDay"];
    }
}

- (void)setupChildVces{
    
    DetailSeeAllPhotoAllViewController *DetailSeeAllPhotoAllV = [[DetailSeeAllPhotoAllViewController alloc] initWithRestaurant:restaurant restaurantPhotoArray:restaurantPhotoArray];
    DetailSeeAllPhotoAllV.isUseWebP = self.isUseWebP;
    [self addChildViewController:DetailSeeAllPhotoAllV];
    DetailSeeAllPhotoAllV.title = @"All";
    
    DetailSeeAllPhotoQravedViewController *DetailSeeAllPhotoQravedV = [[DetailSeeAllPhotoQravedViewController alloc] initWithRestaurant:restaurant];
    [self addChildViewController:DetailSeeAllPhotoQravedV];
    DetailSeeAllPhotoQravedV.title = @"Qraved";
    
    DetailSeeAllPhotoInstagramViewController *DetailSeeAllPhotoInstagramV = [[DetailSeeAllPhotoInstagramViewController alloc] initWithRestaurant:restaurant];
    [self addChildViewController:DetailSeeAllPhotoInstagramV];
    DetailSeeAllPhotoInstagramV.title = @"Instagram";
    
    
}


- (void)setupTitleView{
    
    UIView *titlesView = [[UIView alloc] init];
    titlesView.backgroundColor = [UIColor whiteColor];//[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5];
    titlesView.width = self.view.width;
    titlesView.height = ZLJTitlesViewH;
    titlesView.y = 0;
    [self.view addSubview:titlesView];
    self.titlesView = titlesView;
    
    UIView *indicatorView = [[UIView alloc] init];
    indicatorView.backgroundColor = [UIColor redColor];
    indicatorView.height = 2;
    indicatorView.tag = -1;
    indicatorView.y = titlesView.height - indicatorView.height;
    
    self.indicatorView = indicatorView;
    
    
    CGFloat width = titlesView.width / self.childViewControllers.count;
    CGFloat height = titlesView.height;
    for (NSInteger i = 0; i < self.childViewControllers.count; i ++) {
        UIButton *button = [[UIButton alloc] init];
        button.tag = i;
        button.height = height;
        button.width = width ;
        button.x = i * width;
        if (i == 0) {
            [button setTitle:@"All" forState:UIControlStateNormal];
        }else if (i == 1){
            [button setTitle:@"Qraved" forState:UIControlStateNormal];
        }else{
            [button setTitle:@"Instagram" forState:UIControlStateNormal];
        }


        //        [button layoutIfNeeded];
        [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor redColor] forState:UIControlStateDisabled];
        button.titleLabel.font = [UIFont systemFontOfSize:14];
        [button addTarget:self action:@selector(titleClick:) forControlEvents:UIControlEventTouchUpInside];
        [titlesView addSubview:button];
        
     
        if (i == 0) {
            button.enabled = NO;
            self.selectedButton = button;
            [button.titleLabel sizeToFit];
            self.indicatorView.width = button.titleLabel.width;
            self.indicatorView.centerX = button.centerX;
        }
        [btnArr addObject:button];
    }
    
    [titlesView addSubview:indicatorView];
    
    UIView *lineView = [UIView new];
    lineView.backgroundColor = [UIColor colorCCCCCC];
    [titlesView addSubview:lineView];
    lineView.sd_layout
    .bottomSpaceToView(titlesView, 0)
    .leftSpaceToView(titlesView, 0)
    .widthIs(DeviceWidth)
    .heightIs(0.8);
    
}
#pragma mark - <UIScrollViewDelegate>

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    
 
    NSInteger index =  scrollView.contentOffset.x / scrollView.width;
    
    UITableViewController *vc = self.childViewControllers[index];
    vc.view.x = scrollView.contentOffset.x;
    vc.view.y = 0;
   
    vc.view.height =scrollView.height;
    
    
    [scrollView addSubview:vc.view];
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [self scrollViewDidEndScrollingAnimation:scrollView];
  
    NSInteger index = scrollView.contentOffset.x / scrollView.width;
    [self titleClick:self.titlesView.subviews[index]];
}

#pragma mark -底部setupscrollview
- (void)setupContentview{
    

    self.automaticallyAdjustsScrollViewInsets = NO;
    UIScrollView *contentView = [[UIScrollView alloc] init];
    contentView.frame = self.view.bounds;
    contentView.delegate = self;
    contentView.pagingEnabled = YES;
    //    CGFloat bottom = self.tabBarController.tabBar.height;
    //    CGFloat top = CGRectGetMaxY(self.titleView.frame);
    //    contentView.contentInset = UIEdgeInsetsMake(top, 0, bottom, 0);
    [self.view insertSubview:contentView atIndex:0];
    contentView.contentSize = CGSizeMake(contentView.width * self.childViewControllers.count,0 );
    self.contentView = contentView;
    
    [self scrollViewDidEndScrollingAnimation:contentView];
    
    if (self.currentIndex) {
        [self titleClick:self.titlesView.subviews[self.currentIndex]];
    }
}
- (void)titleClick:(UIButton *)button{
    
    
    self.selectedButton.enabled = YES;
    button.enabled = NO;
    self.selectedButton = button;
    [UIView animateWithDuration:0.25 animations:^{

        CGSize size = [button.titleLabel.text sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(DeviceWidth/3, ZLJTitlesViewH)];
        self.indicatorView.width = size.width;
        self.indicatorView.centerX = button.centerX;
    }];

    CGPoint offset = self.contentView.contentOffset;
    offset.x =  button.tag * self.contentView.width;
    [self.contentView setContentOffset:offset animated:YES];
    
}

-(void)setNavi{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor color333333],
                                                                    NSFontAttributeName:[UIFont boldSystemFontOfSize:17]};
//    [self setLeftItemWithIcon:[UIImage imageNamed:@"back_black.png"] title:nil selector:@selector(backAction:)];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
