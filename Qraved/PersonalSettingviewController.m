//
//  PersonalSettingviewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/6/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "PersonalSettingviewController.h"
#import "UIColor+Helper.h"
#import "Label.h"
#import "UIView+Helper.h"
#import "UIViewController+Helper.h"
#import "UIDevice+Util.h"
#import "UIImageView+Helper.h"
#import "NSString+Helper.h"
#import "DBManager.h"
#import "CustomIOS7AlertView.h"
#import "IMGUser.h"
#import "LoadingView.h"
#import "AppDelegate.h"
#import "PushHelper.h"
#import "PointRewardsViewController.h"
#import "AboutUsViewController.h"
#import "SetNotificationViewController.h"
#import "EditProfileViewController.h"
#import "NotificationSettingViewController.h"
#import "SendFeedbackViewController.h"
#import "JournalSuggestViewController.h"
#import "RestaurantSuggestViewController.h"
#import "V2_PreferenceViewController.h"

@interface PersonalSettingviewController ()<UITableViewDelegate, UITableViewDataSource,registerDelegate>
{
    NSArray *socialAccountsArr;
    NSArray *imagesArr;
    
    IMGThirdPartyUser *facebookUser;
    IMGThirdPartyUser *twitterUser;
    BOOL connectInProgress;
}

@property (nonatomic ,strong) UITableView *tableView;

@end

@implementation PersonalSettingviewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(loginWithTwitterInQraved:) name:TwitterConnectPartDone object:nil];
    [[self rdv_tabBarController] setTabBarHidden:YES animated:NO];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TwitterConnectPartDone object:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"Settings";
    [self addBackBtn];
    [self setBackBarButtonOffset30];
    [self initData];
    [self initUI];
    
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(LogoutSuccess) name:@"logoutNotification" object:nil];
}
- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logoutNotification" object:nil];
}
-(void)LogoutSuccess{
    //    [loginButton setTitle:@"Login" forState:UIControlStateNormal];
    
    [AppDelegate ShareApp].userLogined=NO;
    [IMGUser currentUser].userId = nil;
    [IMGUser currentUser].email = nil;
    [IMGUser currentUser].firstName = nil;
    [IMGUser currentUser].lastName = nil;
    [IMGUser currentUser].fullName = nil;
    [IMGUser currentUser].userName = nil;
    [IMGUser currentUser].avatar = nil;
    [IMGUser currentUser].token = nil;
    [IMGUser currentUser].birthday = nil;
    [IMGUser currentUser].phone = nil;
    [IMGUser currentUser].website = nil;
    [IMGUser currentUser].gender = nil;
    [IMGUser currentUser].cityName = nil;
    [IMGUser currentUser].districtName = nil;
    [IMGUser currentUser].facebookAcount = nil;
    [IMGUser currentUser].twitterAcount = nil;
    [IMGUser currentUser].lastLoginDate = nil;
    [IMGUser currentUser].bookingCount = nil;
    [IMGUser currentUser].redeemablePoints = nil;
    [IMGUser currentUser].reviewCount = nil;
    [IMGUser currentUser].islogin = nil;
    
    [AppDelegate ShareApp].myProfileViewController=nil;
    [AppDelegate ShareApp].pointRewardViewController=nil;
}

-(void)initData{
    IMGUser *user=[IMGUser currentUser];
    NSString *sqlString=[NSString stringWithFormat:@"select * from IMGThirdPartyUser where userId=%@;",user.userId];
    FMResultSet * resultSet = [[DBManager manager] executeQuery:sqlString];
    while([resultSet next]){
        IMGThirdPartyUser *thirdPartyUser=[[IMGThirdPartyUser alloc]init];
        [thirdPartyUser setValueWithResultSet:resultSet];
        if([thirdPartyUser.type intValue]==1){
            facebookUser=thirdPartyUser;
        }else if([thirdPartyUser.type intValue]==2){
            twitterUser=thirdPartyUser;
        }
    }
    [resultSet close];
}
-(void)setSaveBarButton{
    UIBarButtonItem *searchBtn=[[UIBarButtonItem alloc] initWithTitle:L(@"Save") style:UIBarButtonItemStylePlain target:self action:@selector(SaveButtonClick:)];
    searchBtn.tintColor=[UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=searchBtn;
}
-(void)SaveButtonClick{
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)initUI{

    socialAccountsArr = @[L(@"Facebook"),L(@"Twitter")];
    imagesArr = @[@"facebook",@"twitter"];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, DeviceHeight +20 -64) style:UITableViewStyleGrouped];
    self.tableView.delegate =self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }else{
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 6;
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    if (section == 0) {
        return 4;
    }else if (section == 1){
//        return 3;
         return socialAccountsArr.count;
    }else if (section == 2){
        return 0;
        
    } else if (section == 3){
        return 1;
    }else if (section == 4){
        return 3;
    }else{
        return 1;
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 40;
    }else if (section == 1){
        return 40;
       
    }else if (section == 2){
        return 0.001;
        
    } else if (section == 3){
        return 40;
    }else if (section == 4){
        return 40;
    }else{
        return 40;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.001;
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    return [[UIView alloc] init];
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        UIView *sectionOneV =[[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
        sectionOneV.backgroundColor = [UIColor defaultColor];
        UILabel *lblOne = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, DeviceWidth - 20, 40)];
        lblOne.text = @"ACCOUNT";
        lblOne.textColor = [UIColor color999999];
        lblOne.font = [UIFont systemFontOfSize:14];
        lblOne.textAlignment = NSTextAlignmentLeft;
        [sectionOneV addSubview:lblOne];
        
        return sectionOneV;
    }else if (section == 1){
        UIView *sectionTwoV =[[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
        sectionTwoV.backgroundColor = [UIColor defaultColor];
        UILabel *lblTwo = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, DeviceWidth - 20, 40)];
        lblTwo.text = @"SOCIAL";
        lblTwo.textColor = [UIColor color999999];
        lblTwo.font = [UIFont systemFontOfSize:14];
        lblTwo.textAlignment = NSTextAlignmentLeft;
        [sectionTwoV addSubview:lblTwo];
        
        return sectionTwoV;
    }else if (section == 2){
        return nil;
    }else if (section == 3){
        UIView *sectionForV =[[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
        sectionForV.backgroundColor = [UIColor defaultColor];
        UILabel *lblFor = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, DeviceWidth - 20, 40)];
        lblFor.text = @"   ";
        lblFor.textColor = [UIColor color999999];
        lblFor.font = [UIFont systemFontOfSize:14];
        lblFor.textAlignment = NSTextAlignmentLeft;
        [sectionForV addSubview:lblFor];
        
        return sectionForV;
    }else if (section == 4){
        UIView *sectionFiveV =[[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
        sectionFiveV.backgroundColor = [UIColor defaultColor];
        UILabel *lblFive = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, DeviceWidth - 20, 40)];
        lblFive.text = @"ABOUT";
        lblFive.textColor = [UIColor color999999];
        lblFive.font = [UIFont systemFontOfSize:14];
        lblFive.textAlignment = NSTextAlignmentLeft;
        [sectionFiveV addSubview:lblFive];
        
        return sectionFiveV;
    }else{
        UIView *sectionFiveV =[[UIView alloc] initWithFrame:CGRectMake(0, 0, DeviceWidth, 40)];
        sectionFiveV.backgroundColor = [UIColor defaultColor];
        UILabel *lblFive = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, DeviceWidth - 20, 40)];
        lblFive.text = @"   ";
        lblFive.textColor = [UIColor color999999];
        lblFive.font = [UIFont systemFontOfSize:14];
        lblFive.textAlignment = NSTextAlignmentLeft;
        [sectionFiveV addSubview:lblFive];
        
        return sectionFiveV;
    }
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 44;
    }else if (indexPath.section == 1){
        return 63.0f;
        
    }else if (indexPath.section == 2){
        return 0;
        
    } else if (indexPath.section == 3){
        return 44;
    }else if (indexPath.section == 4){
        return 44;
    }else{
        return 44;
    }

}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section == 0) {
        NSArray *array = @[@"Edit Profile",@"Preferences",@"Rewards",@"Notifications"];
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.text = array[indexPath.row];
            cell.textLabel.textColor = [UIColor color333333];
            UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth - 20, 15, 8, 14)];
            imageV.image = [UIImage imageNamed:@"Chevron@2x"];
            [cell addSubview:imageV];
        }
        return cell;

    }else if (indexPath.section == 1){
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        UITableViewCell *    cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        
        UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 61, DeviceWidth, 1)];
        lineImage.backgroundColor = [UIColor colorEDEDED];
        [cell.contentView addSubview:lineImage];
        
        //  }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.backgroundColor=[UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.textColor = [UIColor color333333];
        cell.textLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:16];
        if(indexPath.row==0){
            UIImageView *lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth, 1)];
            lineImage.backgroundColor = [UIColor colorEDEDED];
            [cell.contentView addSubview:lineImage];
        }
        cell.imageView.image = [UIImage imageNamed:[imagesArr objectAtIndex:indexPath.row]];
        if ((indexPath.row==0&&facebookUser!=nil)||(indexPath.row==1&&twitterUser!=nil)){
            Label *titleLabel = [[Label alloc]initWithFrame:CGRectMake(60, 8, 140, 25) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:14] andTextColor:[UIColor color222222] andTextLines:1];
            titleLabel.text = [socialAccountsArr objectAtIndex:indexPath.row];
            [cell.contentView addSubview:titleLabel];
            
//            UIButton *disconectButton = [UIButton buttonWithType:UIButtonTypeCustom];
//            disconectButton.frame = CGRectMake(DeviceWidth-100, 20, 90, 23);
//            [disconectButton setTitle:L(@"Disconnect") forState:UIControlStateNormal];
//            [disconectButton setTitleColor:[UIColor color222222] forState:UIControlStateNormal];
//            disconectButton.tag = indexPath.row;
//            disconectButton.titleLabel.font = [UIFont fontWithName:FONT_OPEN_SANS_SEMIBOLD size:14];
//            [disconectButton addTarget:self action:@selector(disconectButtonClick:) forControlEvents:UIControlEventTouchUpInside];
//            [cell.contentView addSubview:disconectButton];
           UISwitch *disconectButton = [UISwitch new];
            disconectButton.frame = CGRectMake(DeviceWidth-70, 20, 60, 23);
            [disconectButton addTarget:self action:@selector(disconectButtonClick:) forControlEvents:UIControlEventValueChanged];
            [disconectButton setOnTintColor:[UIColor colorWithRed:9/255.0f green:191/255.0f blue:211/255.0f alpha:1.0f]];
            disconectButton.tag = indexPath.row;
            [disconectButton setOn:YES animated:YES];
            [cell.contentView addSubview:disconectButton];
            
            Label *nameLabel = [[Label alloc]initWithFrame:CGRectMake(60, titleLabel.endPointY, 140, 25) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:14] andTextColor:[UIColor color222222] andTextLines:1];
            if(indexPath.row==0){
                nameLabel.text = [NSString stringWithFormat:@"%@ %@",facebookUser.firstName,facebookUser.lastName];
            }else if(indexPath.row==1){
                nameLabel.text = [NSString stringWithFormat:@"%@ %@",twitterUser.firstName,twitterUser.lastName];
            }
            [cell.contentView addSubview:nameLabel];
        }else{
            cell.textLabel.text = [NSString stringWithFormat:L(@"Connect %@ account"),[socialAccountsArr objectAtIndex:indexPath.row]];
        }
        return cell;

            

    }else if (indexPath.section == 2){
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.textLabel.text = @"Invite friends using qraved";
            cell.textLabel.textColor = [UIColor colorWithRed:9/255.0f green:191/255.0f blue:211/255.0f alpha:1.0f];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth - 20, 15, 8, 14)];
            imageV.image = [UIImage imageNamed:@"Chevron@2x"];
            [cell addSubview:imageV];
            
        }
        return cell;

    }else if (indexPath.section == 3){
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.textLabel.text = @"Add new places";
            cell.textLabel.textColor = [UIColor colorWithRed:9/255.0f green:191/255.0f blue:211/255.0f alpha:1.0f];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth - 20, 15, 8, 14)];
            imageV.image = [UIImage imageNamed:@"Chevron@2x"];
            [cell addSubview:imageV];
        }
        return cell;

    }else if (indexPath.section == 4){
        NSArray *array = @[@"About Qraved",@"Feedback",@"Rate us on App Store"];
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.textLabel.text = array[indexPath.row];
            cell.textLabel.textColor = [UIColor color333333];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(DeviceWidth - 20, 15, 8, 14)];
            imageV.image = [UIImage imageNamed:@"Chevron@2x"];
            [cell addSubview:imageV];
        }
        return cell;
        
    }else{
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld%ld", (long)[indexPath section], (long)[indexPath row]];
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.textLabel.text = @"Log out";
            cell.textLabel.textColor = [UIColor color333333];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cell;

    }}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            
            //Edit profile
            if ([AppDelegate ShareApp].userLogined) {
                [[Amplitude instance] logEvent:@"ST - Profile Update Initiate" withEventProperties:nil];
                EditProfileViewController *editProfileVC = [[EditProfileViewController alloc]init];
                editProfileVC.isFromProfile = NO;
                [self.navigationController pushViewController:editProfileVC animated:YES];
            }
            else
            {
                [[AppDelegate ShareApp] goToLoginController];
            }
            
        }else if (indexPath.row == 1){
            //preferences
            
            
            if ([AppDelegate ShareApp].userLogined) {
                [[Amplitude instance] logEvent:@"ST - Change User Preference Initiate" withEventProperties:nil];
                V2_PreferenceViewController *preference = [[V2_PreferenceViewController alloc] init];
                preference.isFromProfile = YES;
                [self.navigationController pushViewController:preference animated:YES];
            }
            else
            {
                [[AppDelegate ShareApp] goToLoginController];
            }

        
        }else if (indexPath.row == 2){
            //rewards
            [[Amplitude instance] logEvent:@"RC - View User Rewards Page" withEventProperties:nil];
             PointRewardsViewController *reward = [[PointRewardsViewController alloc]init];
            [self.navigationController pushViewController:reward animated:YES];
        }else{
            
            //Notification setting
            if ([AppDelegate ShareApp].userLogined) {
                
                SetNotificationViewController *socialAccountsVC = [[SetNotificationViewController alloc]init];
                [self.navigationController pushViewController:socialAccountsVC animated:YES];
            }
            else
            {
                [[AppDelegate ShareApp] goToLoginController];
            }

        }
        
    }else if (indexPath.section == 1){
        if(indexPath.row==0&&facebookUser==nil){
            [self facebookConnect];
            
        }else if(indexPath.row==1&&twitterUser==nil){
            [self twitterConnect];
        }
    }else if (indexPath.section == 2){
        NSLog(@"Invite friends using qraved");
        [[Amplitude instance] logEvent:@"SH - Invite Friends Initiate" withEventProperties:nil];
    }else if (indexPath.section == 3){
        NSLog(@"Add new places");
        
        NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
        [eventProperties setValue:@"User Setting Page" forKey:@"Origin"];
        [[Amplitude instance] logEvent:@"UC - Suggest New Restaurant Initiate" withEventProperties:eventProperties];
        
        RestaurantSuggestViewController *jsvc = [[RestaurantSuggestViewController alloc] init];
        [self.navigationController pushViewController:jsvc animated:YES];
        
    }else if (indexPath.section == 4){
        if (indexPath.row == 0) {
            //ABout US
            [[Amplitude instance] logEvent:@"RC - View About Qraved Page" withEventProperties:nil];
            AboutUsViewController *aboutUsVC = [[AboutUsViewController alloc]init];
            [self.navigationController pushViewController:aboutUsVC animated:YES];
            
        }else if (indexPath.row == 1){
            //sendfeedback
            [[Amplitude instance] logEvent:@"UC - Give Feedback Initiate" withEventProperties:nil];
            SendFeedbackViewController *sendFeenbackVC = [[SendFeedbackViewController alloc]init];
            [self.navigationController pushViewController:sendFeenbackVC animated:YES];
        }else{
            //Rate us on AppStore
            [[Amplitude instance] logEvent:@"UC - Rate on App Store" withEventProperties:nil];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://itunes.apple.com/cn/app/qraved/id731842943?l=en&mt=8"]];
        }
    }else{
    
        //Signout
        
        UIView *signOutView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, DeviceWidth-2*LEFTLEFTSET, 100)];
        
        Label *signOutTitle = [[Label alloc]initWithFrame:CGRectMake(0, 20, DeviceWidth-2*LEFTLEFTSET, 30) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] andTextColor:[UIColor color222222] andTextLines:0];
        signOutTitle.text = L(@"Log out");
        signOutTitle.textAlignment = NSTextAlignmentCenter;
        [signOutView addSubview:signOutTitle];
        NSString *loginType = [[NSUserDefaults standardUserDefaults] objectForKey:@"loginType"];
        if (loginType.length>0) {
            [[Amplitude instance] logEvent:@"SO - Sign Out Initiate" withEventProperties:@{@"Type":loginType}];
            
        }
        Label *signOutAlertLabel = [[Label alloc]initWithFrame:CGRectMake(0, 50, DeviceWidth-2*LEFTLEFTSET, 40) andTextFont:[UIFont fontWithName:FONT_OPEN_SANS_DEFAULT size:15] andTextColor:[UIColor color333333] andTextLines:1];
        signOutAlertLabel.textAlignment = NSTextAlignmentCenter;
        signOutAlertLabel.text = L(@"Are you sure you want to log out?");
        signOutAlertLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [signOutView addSubview:signOutAlertLabel];
        
        CustomIOS7AlertView *signOutAlertView = [[CustomIOS7AlertView alloc]init];
        
        signOutAlertView.buttonTitles = @[L(@"No"),L(@"Yes")];
        
        // Add some custom content to the alert view
        [signOutAlertView setContainerView:signOutView];
        
        //                signOutAlertView.delegate = self;
        
        // You may use a Block, rather than a delegate.
        [signOutAlertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex==0) {
                if (loginType.length>0) {
                    [[Amplitude instance] logEvent:@"SO - Sign Out Cancel" withEventProperties:@{@"Type":loginType}];
                    
                }
            }
            else if (buttonIndex == 1)
            {
                [AppDelegate ShareApp].isShowPersonalization = NO;
                [[AppDelegate ShareApp].ProfileNavigationController popToRootViewControllerAnimated:NO];
//                [[AppDelegate ShareApp].myProfileViewController setCurrentTabIndex:0];
                
                if (loginType.length>0) {
                    [[Amplitude instance] logEvent:@"SO - Sign Out Succeed" withEventProperties:@{@"Type":loginType}];
                    
                }
                [UserDataHandler logout];
                [[Amplitude instance] setUserId:nil];
                [[Amplitude instance] clearUserProperties];
                [AppDelegate ShareApp].isAlreadySetAmplitudeUserProperties = NO;
                [[GIDSignIn sharedInstance] signOut];
                
                [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"userLogined"];
                [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"loginUserId"];
                
                NSString *path_sandox = NSHomeDirectory();
                //设置一个图片的存储路径
                NSString *imagePath = [path_sandox stringByAppendingString:@"/Documents/avastar.png"];
                NSFileManager * fileManager = [[NSFileManager alloc]init];
                [fileManager removeItemAtPath:imagePath error:nil];
                
                [AppDelegate ShareApp].userLogined=NO;
                
                [SHK logoutOfAll];
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isPushSelCity"];
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"emailPushSelCity"];
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"registPushSelCity"];
                [[NSUserDefaults standardUserDefaults]setBool:YES forKey: @"isFirstOpen"];
               
                
                NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.qraved"];
                [shared removeObjectForKey:@"userId"];
                [shared removeObjectForKey:@"userToken"];
                [shared removeObjectForKey:@"person"];
                [shared removeObjectForKey:@"imgUrl"];
                [shared synchronize];
//                IMGUser * user = [IMGUser currentUser];
//                user.userId = nil;
//                user.token = nil;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"logoutNotification" object:nil userInfo:nil];
                [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_REFRESH_HOME object:nil];
                [[AppDelegate ShareApp] requestToShowNotificationCount];
                [[AppDelegate ShareApp] goToPage:0];
                
            }
            
            [alertView close];
        }];
        
        [signOutAlertView setUseMotionEffects:true];
        
        // And launch the dialog
        [signOutAlertView show];
    }
    
}

-(void)twitterConnect{
    if(connectInProgress==TRUE){
        return;
    }
    connectInProgress=TRUE;
    [[NSUserDefaults standardUserDefaults]setObject:@"connect" forKey:TWITTERNOTIFICATIONTYPE];
    [SHK setRootViewController:self];
    SHKTwitter *twitter = [[SHKTwitter alloc] initWithNextDelegate:self];
    [twitter authorize];
    [self performSelector:@selector(progressAvailable) withObject:nil afterDelay:0.5];
}

-(void)progressAvailable{
    connectInProgress=FALSE;
}

-(void)loginWithTwitterInQraved:(NSNotification *)notification{
    NSDictionary *twitterResult = [notification.userInfo objectForKey:@"twitterResult"];
    NSError *error = (NSError *)[notification.userInfo objectForKey:@"error"];
    UserDataHandler *userDataController = [[UserDataHandler alloc] init];
    [[LoadingView sharedLoadingView]startLoading];
    [userDataController twitterConnect:twitterResult withError:error withDelegateBlock: ^(){
        [self loginSuccessful];
        [[LoadingView sharedLoadingView] stopLoading];
    }failedBlock:^{
        [[LoadingView sharedLoadingView] stopLoading];
        
    }];
}

-(void)facebookConnect{
    if(connectInProgress==TRUE){
        return;
    }
    connectInProgress=TRUE;
    NSArray *permissions = @[@"public_profile", @"email"];
    UserDataHandler *userDataHandler = [[UserDataHandler alloc] init];
    [userDataHandler faceBookConnect:permissions delegateWithBlock:^{
        [self loginSuccessful];
    } failedBlock:^{
        
    }];
    
    [self performSelector:@selector(progressAvailable) withObject:nil afterDelay:0.5];
}

-(void)loginSuccessful{
    [self initData];
    [self.tableView reloadData];
}
-(void)registerSuccess
{
    [self initData];
    [self.tableView reloadData];
}

-(void)disconectButtonClick:(UISwitch*)button{
    UIView *popUpView = [[UIView alloc]init];
    IMGUser *user=[IMGUser currentUser];
    NSString *offerTitleStr = L(@"Are you sure want to disconnect this account?");
    CGSize maxSize = [offerTitleStr sizeWithFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] constrainedToSize:CGSizeMake(DeviceWidth-4*LEFTLEFTSET, DeviceHeight) lineBreakMode:NSLineBreakByWordWrapping];
    Label *seeMenuTitle = [[Label alloc]initWithFrame:CGRectMake(LEFTLEFTSET, 64/2-(maxSize.height+4)/2, maxSize.width, maxSize.height+4) andTextFont:[UIFont fontWithName:FONT_GOTHAM_MEDIUM size:18] andTextColor:[UIColor color333333] andTextLines:0];
    seeMenuTitle.text = offerTitleStr;
    seeMenuTitle.textAlignment = NSTextAlignmentCenter;
    [popUpView addSubview:seeMenuTitle];
    
    popUpView.frame = CGRectMake(0, 0, DeviceWidth-2*LEFTLEFTSET, seeMenuTitle.endPointY);
    NSString *thirdId;
    NSNumber *type;
    if(button.tag==0&&facebookUser!=nil){
        thirdId=facebookUser.thirdId;
        type=[NSNumber numberWithInt:1];
    }else if(button.tag==1&&twitterUser!=nil){
        thirdId=twitterUser.thirdId;
        type=[NSNumber numberWithInt:2];
    }else{
        return;
    }
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc]init];
    alertView.buttonTitles = @[@"No, keep it",@"Yes"];
    [alertView setContainerView:popUpView];
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, NSInteger buttonIndex){
        if(buttonIndex==1){
            NSDictionary * parameters = @{@"userId":user.userId,@"t":user.token,@"thirdId":thirdId,@"type":type};
            [[LoadingView sharedLoadingView]startLoading];
            [[IMGNetWork sharedManager]POST:@"user/third/disconnect" parameters:parameters progress:nil  success:^(NSURLSessionDataTask *operation, id responseObject){
                NSString * exceptionMsg=[responseObject objectForKey:@"exceptionmsg"];
                if(exceptionMsg!=nil && [exceptionMsg isLogout]){
                    [[LoadingView sharedLoadingView]stopLoading];
                    [[IMGUser currentUser]logOut];
                    [[AppDelegate ShareApp]goToLoginController];
                }else{
                    NSString *returnStatusString = [responseObject objectForKey:@"status"];
                    if([returnStatusString isEqualToString:@"succeed"]){
                        NSString *sqlString=[NSString stringWithFormat:@"delete from IMGThirdPartyUser where userId='%@' and thirdId='%@' and type='%@';",user.userId,thirdId,type];
                        [[DBManager manager] deleteWithSql:sqlString];
                        if(button.tag == 0){
                            facebookUser=nil;
                        }else if(button.tag==1){
                            twitterUser=nil;
                        }
                        [[LoadingView sharedLoadingView]stopLoading];
                        [self.tableView reloadData];
                    }
                }
            }failure:^(NSURLSessionDataTask *operation, NSError *error){
                NSLog(@"requestSaveUser error: %@",error.localizedDescription);
                [[LoadingView sharedLoadingView]stopLoading];
            }];
        }
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    [alertView show];
    
}














@end
