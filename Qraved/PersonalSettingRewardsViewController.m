//
//  PersonalSettingRewardsViewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/6/21.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "PersonalSettingRewardsViewController.h"
#import "AccountView.h"
@interface PersonalSettingRewardsViewController ()<ProfileDelegate>

@property(nonatomic, strong) AccountView *accountView;

@end

@implementation PersonalSettingRewardsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"Rewards";
    [self addBackBtn];
    self.view.backgroundColor = [UIColor whiteColor];
    [self initUI];
}

- (void)initUI{
    self.accountView=[[AccountView alloc] initWithFrame:CGRectMake(0,0,DeviceWidth,DeviceHeight) withOtherUser:NO];
    self.accountView.delegate=self;
    [self.view addSubview:self.accountView];
}


@end
