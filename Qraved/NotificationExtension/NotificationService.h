//
//  NotificationService.h
//  NotificationExtension
//
//  Created by josn.liu on 2016/11/4.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
