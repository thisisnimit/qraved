//
//  NotificationService.m
//  NotificationExtension
//
//  Created by josn.liu on 2016/11/4.
//  Copyright © 2016年 Imaginato. All rights reserved.
//

#import "NotificationService.h"
#import "Consts.h"
@interface NotificationService ()

@property (nonatomic, strong) void (^contentHandler)(UNNotificationContent *contentToDeliver);
@property (nonatomic, strong) UNMutableNotificationContent *bestAttemptContent;

@end

@implementation NotificationService

- (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler {
#pragma mark --AmpltitudeAPIKey
      NSString *ampApikey=[NSString stringWithFormat:@"%@",QRAVED_AMPLITUDE_API_KEY];
    NSString *app_version=[NSString stringWithFormat:@"%@",QRAVED_WEB_SERVICE_VERSION];
    NSString *platform=@"iOS";
    
    
    self.contentHandler = contentHandler;
    self.bestAttemptContent = [request.content mutableCopy];
    NSDictionary *userinfo=request.content.userInfo;
    NSDictionary *aps=[userinfo objectForKey:@"aps"];
    NSDictionary *alert=[aps objectForKey:@"alert"];
    NSString *body=[alert objectForKey:@"body"];
    NSNumber *type=[userinfo objectForKey:@"type"];
    NSDictionary *userDic =[userinfo objectForKey:@"userInfo"];
    NSString *email=[userDic objectForKey:@"email"];
    NSString *firstName=[userDic objectForKey:@"firstName"];
    NSString *gender=[userDic objectForKey:@"gender"];
    NSString *lastname=[userDic objectForKey:@"lastname"];
    NSString * userid=[userDic objectForKey:@"userId"];
    NSString *userToken=[userDic objectForKey:@"deviceToken"];
    body=[self subWord:body withNumber:10];
    NSString* typename=@"";
    switch ([type intValue]) {
        case 1:case 0:
            typename= @"Landing Page Message";
        case 2:
            typename= @"Splash Notification";
        case 3:
            typename= @"Reservation";
        case 4:
            typename= @"Restaurant info update";
        case 5:
            typename= @"Review";
        case 6:
            typename= @"Photo";
            
        case 7:
            typename=@"Photo List";
        default:
            typename= @"unKnow type";
    }
    
    NSString *landpage=@"";
    switch ([type intValue]) {
        case 1:
            landpage=@"Landing Page";
        case 2:
            landpage=@"Splash Page";
        case 3:
            landpage=@"Reservation Detial Page";
        case 4:
            landpage=@"Restaurant Detail Page";
        case 5:
            landpage=@"Review Card Detial Page";
        case 6:
            landpage=@"Photo Card Detial Page";
        case 7:
            landpage=@"Photo Card Detail Page - Photo List";
        case 0:
            landpage=@"Notification List Page";
        default:
            landpage=@"unKnow Page";
    }
    NSString *notiId=[userinfo objectForKey:@"nid"];
    
    // Modify the notification content here...
//    self.bestAttemptContent.title = [NSString stringWithFormat:@"%@djgjdhjhdjflghjdfhgjhldkfied]", self.bestAttemptContent.title];
    self.contentHandler(self.bestAttemptContent);
    NSString *str=[NSString stringWithFormat:@"[{\"user_id\":\"%@\", \"event_type\":\"RC - Notifications on Device Notification page\", \"user_properties\":{\"email\":\"%@\",\"firstName\":\"%@\",\"gender\":\"%@\",\"lastname\":\"%@\"},\"time\":%f,\"app_version\":\"%@\",\"platform\":\"%@\",\"event_properties\":{\"LandingPage\":\"%@\",\"Type\":\"%@\",\"value\":\"%@\",\"Notification Id\":\"%@\",\"Device Token\":\"%@\"}}]",userid, email,firstName,gender,lastname,[[NSDate date]timeIntervalSince1970],app_version,platform,landpage,typename,body,notiId,userToken];
    NSString *s = [[self class] encodeString:str];
    NSString *GETURL =[NSString stringWithFormat:@"https://api.amplitude.com/httpapi?api_key=%@&event=%@",ampApikey,s];
    
    // 1、创建URL
    NSURL *url = [NSURL URLWithString:GETURL];
    // 2、创建请求对象
    NSMutableURLRequest *requestget = [[NSMutableURLRequest alloc] initWithURL:url];
    
    // 3、 发送异步请求
    [NSURLConnection sendAsynchronousRequest:requestget queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
        
        NSString*  str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"..........%@",str);
        
    }];
    
    
}
+(NSString*)encodeString:(NSString*)unencodedString{
    
    // CharactersToBeEscaped = @":/?&=;+!@#$()~',*";
    // CharactersToLeaveUnescaped = @"[].";
    
    NSString *encodedString = (NSString *)
    CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
                                                              (CFStringRef)unencodedString,
                                                              NULL,
                                                              (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                              kCFStringEncodingUTF8));
    
    return encodedString;
}
-(NSString*)subWord:(NSString*)str withNumber:(int)num{
    
    NSUInteger wordsNumber = 0;
    NSScanner *scanner = [NSScanner scannerWithString:str]; //s是需要截取最前面8个单词的英文句子。
    NSCharacterSet *whiteSpace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    while ([scanner scanUpToCharactersFromSet:whiteSpace intoString:nil]) {
        wordsNumber ++;
    }
    
    NSString *sPartForTitle = nil;
    NSMutableString *stringMutable = [[NSMutableString alloc] initWithString:str] ;
    if (wordsNumber > num) {
        
        if ([stringMutable hasPrefix:@"“"]) { //s这个英文句子开始处可能存在着引号
            
            
            [stringMutable deleteCharactersInRange:NSMakeRange(0, 1)];
        }
        if ([stringMutable hasSuffix:@"”"]) {//s这个英文句子结尾处可能存在着引号
            
            [stringMutable deleteCharactersInRange:NSMakeRange([stringMutable length]-1, 1)];
        }
        
        NSArray *sWords = [stringMutable componentsSeparatedByString:@" "];
        sPartForTitle = [[[[[[[[[[[[[[[[sWords objectAtIndex:0] stringByAppendingString:@" "] stringByAppendingString:[sWords objectAtIndex:1]  ] stringByAppendingString:@" "] stringByAppendingString:[sWords objectAtIndex:2]] stringByAppendingString:@" "] stringByAppendingString:[sWords objectAtIndex:3]] stringByAppendingString:@" "] stringByAppendingString:[sWords objectAtIndex:4]] stringByAppendingString:@" "] stringByAppendingString:[sWords objectAtIndex:5]] stringByAppendingString:@" "] stringByAppendingString:[sWords objectAtIndex:6]] stringByAppendingString:@" "] stringByAppendingString:[sWords objectAtIndex:7]] stringByAppendingString:@"..."];
    } else {
        sPartForTitle = stringMutable;
    }
    return sPartForTitle;
    
}


- (void)serviceExtensionTimeWillExpire {
    // Called just before the extension will be terminated by the system.
    // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
    self.contentHandler(self.bestAttemptContent);
}

@end
