//
//  FUProgressView.m
//  fundsUP
//
//  Created by DeliveLee on 2017/5/12.
//  Copyright © 2017年 mediaBunker. All rights reserved.
//

#import "FUProgressView.h"

@interface FUProgressView ()
@end

@implementation FUProgressView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}

-(void)initUI{

    self.backgroundColor = [UIColor colorWithHexString:@"#E5E6E8"];
    _contentView = [UIView new];
    _contentView.backgroundColor = [UIColor redColor];
    [self addSubview:_contentView];
    _contentView.sd_layout
    .leftEqualToView(self)
    .topEqualToView(self)
    .bottomEqualToView(self)
    .widthIs(0);
}

-(void)setPercent:(CGFloat)percent{
    
    _contentView.sd_layout.widthIs(160 * (percent / 100));
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 160 * (percent / 100), 12) byRoundingCorners:UIRectCornerTopRight | UIRectCornerBottomRight cornerRadii:CGSizeMake(6, 6)];
    CAShapeLayer *layer = [[CAShapeLayer alloc]init];
    layer.frame = CGRectMake(0, 0, 160 * (percent / 100), 12);
    layer.path = maskPath.CGPath;
    _contentView.layer.mask = layer;
    [_contentView updateLayout];

    _percent = percent;
    
}

- (void)setBgColor:(UIColor *)bgColor
{

    _contentView.backgroundColor = bgColor;
    
}

@end
