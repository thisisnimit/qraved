//
//  DetailSeeAllPhotoInstagramViewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/10.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "DetailSeeAllPhotoInstagramViewController.h"
#import "IMGRestaurant.h"
#import "DetailDataHandler.h"
#import "DetailSeeAllPhotoInstagramModel.h"
#import "IMGRestaurantImage.h"
#import "AlbumViewController.h"
#import "IMGDish.h"
#import "HomeService.h"
#import "LoadingView.h"
@interface DetailSeeAllPhotoInstagramViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    
    UICollectionView *InstagramCollectionView;
    DetailSeeAllPhotoInstagramModel *InstagramModel;
    IMGRestaurant *restaurant;
    int offset;
    BOOL noMorePhotos;
    IMGShimmerView *shimmerView;
}
@property (nonatomic, strong)  NSMutableArray  *dataArray;

@end

@implementation DetailSeeAllPhotoInstagramViewController

-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant{
    self = [super init];
    if (self != nil) {
        restaurant = paramRestaurant;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor cyanColor];
    [self initUI];
    offset = 0;
    noMorePhotos = NO;
    _dataArray = [NSMutableArray array];
    InstagramCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    
    [self requestData];
}
- (void)loadMoreData{
    
    [self initData];
}
- (void)requestData{
    [self initData];
    
};
- (void)initData{
    
    NSDictionary *dict=@{@"restaurantId":[NSString stringWithFormat:@"%@",restaurant.restaurantId],@"max":@"15",@"offset":[NSString stringWithFormat:@"%d",offset]};
    
//    [[LoadingView sharedLoadingView] startLoading];
    [DetailDataHandler SellAllPhotoInstagram:dict andSuccessBlock:^(NSMutableArray * array,NSString *goFoodlink) {
        [InstagramCollectionView.mj_footer endRefreshing];
        if (offset == 0) {
            [shimmerView removeFromSuperview];
        }
        restaurant.goFoodLink = goFoodlink;
        [self loadImageUrl:array];
        [_dataArray addObjectsFromArray:array];
        
        if ([array count] >0) {
            offset += 15;
        }else{
            noMorePhotos = YES;
        }
        [InstagramCollectionView reloadData];
    } anderrorBlock:^{
        [InstagramCollectionView.mj_footer endRefreshing];
//         [[LoadingView sharedLoadingView] stopLoading];
    }];
    
}
- (void)initUI{
    
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    CGFloat width = (DeviceWidth-2)/3;
    layout.itemSize = CGSizeMake(width, width);
    layout.minimumLineSpacing = 1;
    layout.minimumInteritemSpacing = 1;
    //    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    
    InstagramCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,40, DeviceWidth, DeviceHeight - 40 - 44) collectionViewLayout:layout];
    InstagramCollectionView.backgroundColor = [UIColor whiteColor];
    InstagramCollectionView.delegate = self;
    InstagramCollectionView.dataSource = self;
    InstagramCollectionView.showsHorizontalScrollIndicator = NO;
    InstagramCollectionView.showsVerticalScrollIndicator = NO;
    InstagramCollectionView.bounces = NO;
    [self.view addSubview:InstagramCollectionView];
    
    
    [InstagramCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"photoCell"];
    shimmerView = [[IMGShimmerView alloc] initWithFrame:CGRectMake(0, 40, DeviceWidth, self.view.bounds.size.height - 40)];
    [shimmerView createPhotoShimmerView];
    [self.view addSubview:shimmerView];
}

#pragma mark -- collection  delegate
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    
    for (UIView *subView in cell.contentView.subviews) {
        [subView removeFromSuperview];
    }
    InstagramModel = [_dataArray objectAtIndex:indexPath.row];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.contentView.bounds];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    [imageView sd_setImageWithURL:[NSURL URLWithString:InstagramModel.lowResolutionImage] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        //image animation
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        if ([manager diskImageExistsForURL:[NSURL URLWithString:InstagramModel.lowResolutionImage]]) {
//            NSLog(@"dont loading animation");
        }else {
            if (image) {
                imageView.alpha = 0.0;
                [UIView transitionWithView:imageView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                    imageView.image = image;
                    imageView.alpha = 1.0;
                } completion:NULL];
            }
        }
        
        NSLog(@"%@====%ld", error, (long)error.code);
        [HomeService deleteErrorImageUrlerrorCode:error.code imageUrl:InstagramModel.lowResolutionImage mediaId:@"" photoId:[NSString stringWithFormat:@"%@",InstagramModel.myid] celebrityPhotoId:@"" andBlock:^(id contributionArr) {
            
            
            
        } andError:^(NSError *error) {
            
            
        }];
    }];
    [cell.contentView addSubview:imageView];
    
    if( indexPath.row==_dataArray.count-15) {
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            if (!noMorePhotos) {
                [self initData];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
            });
            
        });
    }

    
    return cell;
    
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return _dataArray.count;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger tag = indexPath.row;
    NSMutableArray *images=[[NSMutableArray alloc] init];
    for(DetailSeeAllPhotoInstagramModel *instagramModel in _dataArray){
        IMGDish *dish = [[IMGDish alloc] init];
        dish.imageUrl=instagramModel.standardResolutionImage;
        dish.restaurantId=instagramModel.restaurantId;
        dish.type = instagramModel.type;
        dish.userName = instagramModel.instagramUserFullName;
        dish.dishId = instagramModel.myid;
        
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        NSDate *createTime=[[NSDate alloc] init];
        [formatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
        NSDate *confromTimesp = [NSDate dateWithTimeIntervalSince1970:[instagramModel.approvedDate longLongValue]/1000];
        NSString *confromTimespStr = [formatter stringFromDate:confromTimesp];
        
        createTime=[formatter dateFromString:confromTimespStr];
        dish.createTime=[NSNumber numberWithDouble:createTime.timeIntervalSince1970];
       
        if (instagramModel.instagramUserId != nil) {
            dish.photoCreditTypeDic = @"User ID";
        }
        
        dish.userAvatarDic = instagramModel.instagramUserProfilePicture;
        dish.userIdDic = [NSNumber numberWithInt:[instagramModel.instagramUserId intValue]];
//        dish.userPhotoCountDic = instagramModel.photoCredit[@"userPhotoCount"];
//        dish.photoCreditTypeDic  = instagramModel.photoCredit[@"photoCreditType"];
//        dish.userIdDic  = instagramModel.photoCredit[@"userId"];
//        dish.photoCreditDic  = instagramModel.photoCredit[@"photoCredit"];
//        dish.userReviewCountDic  = instagramModel.photoCredit[@"userReviewCount"];
//        dish.userAvatarDic  = instagramModel.photoCredit[@"userAvatar"];
//        dish.photoCreditUrlDic  = instagramModel.photoCredit[@"photoCreditUrl"];
//        dish.restaurantIdDic  = instagramModel.photoCredit[@"restaurantId"];
        
        dish.isRestaurantPhoto = YES;
        [images addObject:dish];
    }
    //    [images addObjectsFromArray:userPhotoArray];
    
    AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:images andPhotoTag:tag andRestaurant:restaurant andFromDetail:NO];
    
    albumViewController.title = restaurant.title;
    albumViewController.showPage=NO;
    albumViewController.amplitudeType = @"Restaurant detail page - ";
    //    albumViewController.isUseWebP = self.isUseWebP;
    [self.navigationController pushViewController:albumViewController animated:YES];
}

- (void)loadImageUrl:(NSMutableArray *)imageArr{
    imageArr = (NSMutableArray *)[[imageArr reverseObjectEnumerator] allObjects];
    for (DetailSeeAllPhotoInstagramModel *model in imageArr) {
        
        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:model.lowResolutionImage] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            
        }];
    }
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"%f",InstagramCollectionView.contentOffset.y);
    if (InstagramCollectionView.contentOffset.y <= 0) {
        InstagramCollectionView.bounces = NO;
        
        NSLog(@"jinzhixiala");
    }
    else
        if (InstagramCollectionView.contentOffset.y >= 0){
            InstagramCollectionView.bounces = YES;
            NSLog(@"allow shangla");
            
        }
}


@end
