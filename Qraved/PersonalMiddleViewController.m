//
//  PersonalMiddleViewController.m
//  PersonalHomePageDemo
//
//  Created by Kegem Huang on 2017/3/15.
//  Copyright © 2017年 huangkejin. All rights reserved.
//

#import "PersonalMiddleViewController.h"
#import "ProfileHandler.h"
#import "PersonalMiddleOnePictureTableViewCell.h"

#import "PersonalMiddleAddPictureTableViewCell.h"
#import "PersonalMiddleHasBookedTableViewCell.h"
#import "PersonalMiddleUpcomingBookingTableViewCell.h"
#import "PersonalMiddleSaveTableViewCell.h"
#import "PersonalMiddleRatedTableViewCell.h"
#import "PersonalPhotoWithoutReviewTableViewCell.h"
#import "PersonalMiddleCancelBookingTableViewCell.h"

#import "IMGJourneyReview.h"
#import "IMGJourneyBook.h"
#import "IMGJourneyPhoto.h"
#import "IMGJourneySave.h"

#import "IMGMediaComment.h"
#import "JournalActivityItemProvider.h"
#import "TwitterActivityItemProvider.h"
#import "RestaurantActivityItemProvider.h"
#import "IMGOfferCard.h"
#import "RestaurantOfferActivityItemProvider.h"
#import "IMGRestaurantEvent.h"
#import "RestaurantEventActivityItemProvider.h"
#import "ReviewActivityItemProvider.h"
#import "IMGDish.h"
#import "MapViewController.h"
#import "DetailViewController.h"
#import "ReviewPublishViewController.h"
#import "V2_CommentListViewController.h"
#import "IMGProfileJourney.h"
#import "JourneyCouponTableViewCell.h"
#import "CouponDetailViewController.h"

typedef NS_ENUM(NSInteger, JourneyCellType)
{
    JourneyTypeNone     =  0,
    JourneyTypeReview =  1,
    JourneyTypeBook  =  2,
    JourneyTypeUploadPhoto  =  3,
    JourneyTypeSave    =  4,
    JourneyTypeCoupon    =  5,
};

@interface PersonalMiddleViewController ()<UITableViewDelegate,UITableViewDataSource, HomeReviewTableViewCellDelegate, PersonalMiddleUpcomingBookingTableViewCellDelegate, PersonalMiddleCancelBookingTableViewCellDelegate, PersonalMiddleRatedTableViewCellDelegate, PhotoWithHomeReviewTableViewCellDelegate, PersonalMiddleSaveTableViewCellDelegate, PersonalMiddleHasBookedTableViewCellDelegate>
{
    int offset;
    NSMutableArray *journeyArr;
}
@property (nonatomic, strong)  UITableView  *tableView;


@end

@implementation PersonalMiddleViewController
- (void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    [self downRefreshData];
    
//    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(downRefreshData)];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [IMGAmplitudeUtil trackProfileWithName:@"RC - View User Profile Journey Tab" andUserId:[IMGUser currentUser].userId andLocation:nil];
    
    [self initUI];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeAvatarImage:) name:@"changeAvatar" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeCoverImage:) name:@"changeCoverImage" object:nil];
}
- (void) initUI{

    journeyArr = [NSMutableArray array];
    
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.tableView.separatorStyle = UITableViewCellEditingStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        self.tableView.estimatedRowHeight = 0;
    }
    [self.view addSubview:self.tableView];
    self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(downRefreshData)];
}
- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"changeAvatar" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"changeCoverImage" object:nil];
}
- (void)downRefreshData{
    offset = 0;
    [self LoadData];
}
#pragma mark - loaddata
- (void)LoadData{
    
    IMGUser * user = [IMGUser currentUser];
    if (self.isSelf == NO) {
         [[LoadingView sharedLoadingView] startLoading];
        NSDictionary *param = @{@"userId":user.userId,@"t":user.token,@"max":@"10",@"offset":[NSString stringWithFormat:@"%d",offset]};
        
        [ProfileHandler getJourney:param andSuccessBlock:^(NSMutableArray *journeyArray) {
            
            [[LoadingView sharedLoadingView] stopLoading];
            [self.tableView.mj_footer endRefreshing];
            [self.tableView.mj_header endRefreshing];
            
            if (offset==0) {
                [journeyArr removeAllObjects];
            }
            
            if ([journeyArray count] > 0) {
                offset += 10;
                [journeyArr addObjectsFromArray:journeyArray];
            }
            [self.tableView reloadData];
            
        } anderrorBlock:^{
            [[LoadingView sharedLoadingView] stopLoading];
            [self.tableView.mj_footer endRefreshing];
            [self.tableView.mj_header endRefreshing];
        }];

    }else{
     [[LoadingView sharedLoadingView] startLoading];
        NSDictionary *param = @{@"userId":user.userId,@"targetUserId":_otherUser.userId,@"t":user.token,@"max":@"10",@"offset":[NSString stringWithFormat:@"%d",offset]};
        [ProfileHandler getOtherJourney:param andSuccessBlock:^(NSMutableArray *journeyArray) {
            [[LoadingView sharedLoadingView] stopLoading];
            [self.tableView.mj_footer endRefreshing];
            [self.tableView.mj_header endRefreshing];
            if (offset==0) {
                [journeyArr removeAllObjects];
            }
            
            if ([journeyArray count] > 0) {
                offset += 10;
                [journeyArr addObjectsFromArray:journeyArray];
            }
            
            [self.tableView reloadData];
        } anderrorBlock:^{
            [[LoadingView sharedLoadingView] stopLoading];
            [self.tableView.mj_footer endRefreshing];
            [self.tableView.mj_header endRefreshing];
            
        }];
    }
}
- (void)loadMoreData{
    
    [self LoadData];
}
- (void)requestData{
    
    [self LoadData];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return journeyArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IMGProfileJourney *journey = [journeyArr objectAtIndex:indexPath.row];
    JourneyCellType cellType = [journey.type intValue];
    
    switch (cellType) {
        case JourneyTypeReview:{
            IMGJourneyReview *review = journey.journeyReview;
            
            if (review.dishList.count == 0 && [review.reviewTitle isEqualToString:@""] && [review.reviewSummarize isEqualToString:@""]) {
                return 130.5;
                
            }else if (review.dishList.count >0 ||(![review.reviewTitle isEqualToString:@""] || ![review.reviewSummarize isEqualToString:@""] )){
                PersonalMiddleOnePictureTableViewCell * cell = [[PersonalMiddleOnePictureTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"OnePicture"];
                
                cell.model = review;
                return cell.cellHeight;
            }
           break;
        }
            case JourneyTypeBook:{
                IMGJourneyBook *book = journey.journeyBook;
                switch ([book.fettle intValue]) {
                    case 1:{
                        return 206;
                    }break;
                    case 2:{
                        return 155.5;
                    }break;
                    case 3:{
                        return 140;
                    }break;
                        
                    default:{
                    }break;
                }
            }break;
            
        case JourneyTypeUploadPhoto:{
            PersonalPhotoWithoutReviewTableViewCell * cell = [[PersonalPhotoWithoutReviewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PhotoWithoutReview"];
            
            cell.model = journey.journeyPhoto;
            
            return cell.cellHeight;
            
        }break;
            
        case JourneyTypeSave:{
            return 135.5;
            break;
        }
        case JourneyTypeCoupon:{
            return [tableView cellHeightForIndexPath:indexPath model:journey.journeyCoupon keyPath:@"model" cellClass:[JourneyCouponTableViewCell class] contentViewWidth:DeviceWidth];
            break;
        }
        default:
            return 0;
            break;
    }
    return 0;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IMGProfileJourney *journey = [journeyArr objectAtIndex:indexPath.row];
    
    JourneyCellType cellType = [journey.type intValue];
    
    switch (cellType) {
        case JourneyTypeReview:{
            IMGJourneyReview *review = journey.journeyReview;
            
            if (review.dishList.count == 0 && [review.reviewTitle isEqualToString:@""] && [review.reviewSummarize isEqualToString:@""]) {
                NSString *idenStr = @"reviewCell";
                PersonalMiddleRatedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
                if (!cell) {
                    cell = [[PersonalMiddleRatedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    
                }
                cell.Delegate = self;
                cell.fromCellIndexPath = indexPath;
                cell.model = review;
                return cell;
                
            }else if (review.dishList.count >0 ||(![review.reviewTitle isEqualToString:@""] || ![review.reviewSummarize isEqualToString:@""] )){
                
                NSString *idenStr = @"reviewPhotoCell";
                PersonalMiddleOnePictureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
                if (!cell) {
                    cell = [[PersonalMiddleOnePictureTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                }
                cell.model = review;
                cell.homeReviewDelegate = self;
                cell.fromCellIndexPath = indexPath;
                return cell;
            }
            break;
        }
            case JourneyTypeBook:{
                IMGJourneyBook *book = journey.journeyBook;
                switch ([book.fettle intValue]) {
                    case 1:{
                        NSString *idenStr = @"upcomingBookCell";
                        PersonalMiddleUpcomingBookingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
                        if (!cell) {
                            cell = [[PersonalMiddleUpcomingBookingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
                            cell.selectionStyle = UITableViewCellSelectionStyleNone;
                        }
                        cell.fromCellIndexPath = indexPath;
                        cell.Delegate =self;
                        cell.model = book;
                        return cell;
                        break;
                    }
      
                    case 2:{
                        NSString *idenStr = @"hasBookCell";
                        PersonalMiddleHasBookedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
                        if (!cell) {
                            cell = [[PersonalMiddleHasBookedTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
                            cell.selectionStyle = UITableViewCellSelectionStyleNone;
                        }
                        cell.model = book;
                        return cell;
                        break;
                    }
                        
                    case 3:{
                        NSString *idenStr = @"cancelBookCell";
                        PersonalMiddleCancelBookingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
                        if (!cell) {
                            cell = [[PersonalMiddleCancelBookingTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
                            cell.selectionStyle = UITableViewCellSelectionStyleNone;
                        }
                        cell.Delegate = self;
                        cell.fromCellIndexPath = indexPath;
                        cell.model = book;
                        
                        return cell;
                        break;
                    }
                        
                    default:
                        break;
                }
                break;
            }
        case JourneyTypeUploadPhoto:{
            NSString *idenStr = @"uploadPhotoCell";
            PersonalPhotoWithoutReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
            if (!cell) {
                cell = [[PersonalPhotoWithoutReviewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.model = journey.journeyPhoto;
            cell.homeReviewDelegate = self;
            cell.fromCellIndexPath = indexPath;
            return cell;
            break;
        }
        case JourneyTypeSave:{
            NSString *idenStr = @"saveCell";
            PersonalMiddleSaveTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
            if (!cell) {
                
                cell = [[PersonalMiddleSaveTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cell.Delegate = self;
            cell.fromCellIndexPath = indexPath;
            cell.model = journey.journeySave;
            return cell;
            break;
        }
        case JourneyTypeCoupon:{
            NSString *idenStr = @"couponCell";
            JourneyCouponTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:idenStr];
            if (!cell) {
                cell = [[JourneyCouponTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:idenStr];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            cell.model = journey.journeyCoupon;
            
            return cell;
        }
        default:{
            break;
        }
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    IMGProfileJourney *journey = [journeyArr objectAtIndex:indexPath.row];
    
    JourneyCellType cellType = [journey.type intValue];
    
    switch (cellType) {
        case JourneyTypeCoupon:{
            CouponModel *model = journey.journeyCoupon;
            [IMGAmplitudeUtil trackCampaignWithName:@"RC - View Coupon Detail" andCampaignId:nil andBrandId:nil andCouponId:model.coupon_id andLocation:nil andOrigin:@"Profile" andIsMall:NO];
    
            CouponDetailViewController *couponDetailViewController = [[CouponDetailViewController alloc] init];
            //couponDetailViewController.isFromProfile = YES;
            couponDetailViewController.couponId = model.coupon_id;
            MLNavigationController *nav = [[MLNavigationController alloc] initWithRootViewController:couponDetailViewController];
            [self.navigationController presentViewController:nav animated:YES completion:nil];
            break;
        }
        default:{
            break;
        }
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (void)HomeReviewTableCell:(UITableViewCell *)tableViewCell readMoreTapped:(id)review indexPath:(NSIndexPath *)path{
    
    IMGJourneyReview *thisReview = (IMGJourneyReview *)review;
    
    //[reveiwCardIsReadMore setObject:isReadMore?@1:@0 forKey:thisReview.targetId];
    thisReview.isReadMore = YES;
    
    //    NSIndexPath *indexPath = [_tableView indexPathForCell:tableViewCell];
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            if ([[_tableView indexPathsForVisibleRows] indexOfObject:path] == NSNotFound) {
                return;
            }
//            if(lastPageDataArr.count == 0) {
//                return;
//            }
            [_tableView reloadRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
        }
        
        @catch ( NSException *e ) {
            return;
        }
    });
    
    
}
//share btn
- (void)homeCardTableViewCell:(UITableViewCell*)cell shareButtonTapped:(UIButton*)button entity:(id)entity{

    NSLog(@"点击 share");
//    personalTypeOneModel
   if([entity isKindOfClass:[IMGJourneyReview class]]){
        IMGJourneyReview *review = (IMGJourneyReview*)entity;
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/review/%@",QRAVED_WEB_SERVER_OLD,review.reviewId]];
        
        NSString *shareContentString = [NSString stringWithFormat:@"CHECK IT OUT NOW! A Restaurant Review by %@ on Qraved",review.userFullName];
        ReviewActivityItemProvider *provider=[[ReviewActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
        provider.username = review.userFullName;
        provider.restaurantTitle = review.restaurantTitle;
        provider.url=url;
        provider.title=shareContentString;
        TwitterActivityItemProvider *twitterProvider=[[TwitterActivityItemProvider alloc] initWithPlaceholderItem:@""];
        twitterProvider.title=[NSString stringWithFormat:L(@"Check out %@ review at %@ on #Qraved!"),review.userFullName,review.restaurantTitle];
        
        
        UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterProvider] applicationActivities:nil];
        activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
        [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK IT OUT NOW!A Restaurant Review By %@ on Qraved!"),review.userFullName] forKey:@"subject"];
        
        activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
            if (completed) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:review.reviewId forKey:@"Review_ID"];
                [eventProperties setValue:review.userFullName forKey:@"ReviewerName"];
                [eventProperties setValue:@"Review card on homepage" forKey:@"Location"];
                [eventProperties setValue:review.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:provider.activityType forKey:@"Channel"];
                
                [[Amplitude instance] logEvent:@"SH - Share Review" withEventProperties:eventProperties];
            }else{
            }
        };
        if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
            // iOS8
            activityCtl.popoverPresentationController.sourceView =
            button;
        }
        
        
        [self presentViewController:activityCtl animated:YES completion:^{
            
        }];
//        personalTypeThreeModel
    }else if([entity isKindOfClass:[ IMGJourneyPhoto class]]){
         IMGJourneyPhoto *_dish = ( IMGJourneyPhoto*)entity;
        IMGRestaurant *_restaurant = [[IMGRestaurant alloc] init];
        _restaurant.restaurantId = _dish.restaurantId;
        _restaurant.title = _dish.restaurantTitle;
//        _restaurant.seoKeyword = _dish.restaurantSeo;
        NSURL *url=[NSURL URLWithString:[NSString stringWithFormat:@"%@post/photo/%@",QRAVED_WEB_SERVER_OLD,_dish.uploadedPhotoId]];
        NSString *shareContentString = [NSString stringWithFormat:@"Check out this photo at %@ on Qraved. %@",_restaurant.title,url];
        RestaurantActivityItemProvider *provider=[[RestaurantActivityItemProvider alloc] initWithPlaceholderItem:shareContentString];
        provider.url=url;
        provider.title=shareContentString;
        provider.restaurantTitle=_restaurant.title;
        provider.cuisineAreaPrice = _dish.userFullName;
        
        TwitterActivityItemProvider *twitterActivityItemProvider = [[TwitterActivityItemProvider alloc]initWithPlaceholderItem:@""] ;
        twitterActivityItemProvider.title = [NSString stringWithFormat:@"Check out this photo at %@ on Qraved",_restaurant.title];
        
        
        UIActivityViewController *activityCtl=[[UIActivityViewController alloc] initWithActivityItems:@[provider,twitterActivityItemProvider] applicationActivities:nil];
        activityCtl.excludedActivityTypes=@[UIActivityTypeAirDrop];
        [activityCtl setValue:[NSString stringWithFormat:L(@"CHECK IT OUT NOW! A Restaurant Photo by %@ on Qraved!"),_dish.userFullName] forKey:@"subject"];
        activityCtl.completionWithItemsHandler=^(NSString* activityType,BOOL completed,NSArray *returnedItems,NSError *activityError){
            if (completed) {
                NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
                [eventProperties setValue:_dish.uploadedPhotoId forKey:@"Photo_ID"];
                [eventProperties setValue:_dish.restaurantId forKey:@"Restaurant_ID"];
                [eventProperties setValue:_dish.restaurantCityName forKey:@"DishName"];
                [eventProperties setValue:@"Photo card on homepage" forKey:@"Location" ];
                [eventProperties setValue:provider.activityType forKey:@"Channel"];
                
                [[Amplitude instance] logEvent:@"SH - Share Photo" withEventProperties:eventProperties];
            }else{
            }
        };
        if ( [activityCtl respondsToSelector:@selector(popoverPresentationController)] ) {
            // iOS8
            activityCtl.popoverPresentationController.sourceView =
            button;
        }
        
        [self presentViewController:activityCtl animated:YES completion:^{
            
        }];
    }

}
//call (UpcomingBooking)
- (void)UpcomingBookingHomeReviewTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path book:(IMGJourneyBook *)model{
    
    [IMGAmplitudeUtil trackBookWithName:@"CL - Call Restaurant CTA" andRestaurantId:model.restaurantId andReservationId:nil andLocation:@"User Profile Page - Journey" andChannel:nil];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:model.restaurantPhone delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Call", nil];
    [alertView show];
    

}
//reminder (UpcomingBooking)
- (void)UpcomingBookingReminderTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path book:(IMGJourneyBook *)model{
    
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    if ([eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)]){
        // the selector is available, so we must be on iOS 6 or newer
        [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(error){
                    // display error message here
                }else if(!granted){
                    // display access denied error message here
                }else{
                    // access granted
                    // ***** do the important stuff here *****
                    EKEvent *ekEvent  = [EKEvent eventWithEventStore:eventStore];
                    ekEvent.title     = model.restaurantTitle;
                    ekEvent.location = [model address];
                    
                    [IMGAmplitudeUtil trackBookWithName:@"CL - Booking Reminder CTA" andRestaurantId:model.restaurantId andReservationId:nil andLocation:@"User Profile Page - Journey" andChannel:nil];
                    
                    NSDateFormatter *tempFormatter = [[NSDateFormatter alloc]init];
                    [tempFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
                    ekEvent.startDate=[tempFormatter dateFromString:[NSString stringWithFormat:@"%@ %@",model.bookDate,model.bookTime]];
                    ekEvent.endDate=[tempFormatter dateFromString:[NSString stringWithFormat:@"%@ %@",model.bookDate,model.bookTime]];
                    
                    [tempFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
                    ekEvent.allDay = NO;
                    
                    //添加提醒
                    [ekEvent addAlarm:[EKAlarm alarmWithRelativeOffset:60.0f * -60.0f * 24]];
                    [ekEvent addAlarm:[EKAlarm alarmWithRelativeOffset:60.0f * -15.0f]];
                    
                    [ekEvent setCalendar:[eventStore defaultCalendarForNewEvents]];
                    NSError *err;
                    [eventStore saveEvent:ekEvent span:EKSpanThisEvent error:&err];
                    
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:L(@"Calendar") message:L(@"Reservation is added to calendar.") delegate:nil cancelButtonTitle:L(@"OK") otherButtonTitles:nil];
                    [alert show];
                    
                }
            });
        }];
    }


}
//map (UpcomingBooking)
- (void)UpcomingBookingViewMapTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path book:(IMGJourneyBook *)model{

    [IMGAmplitudeUtil trackRestaurantWithName:@"RC - View Restaurant Map" andRestaurantId:model.restaurantId andRestaurantTitle:nil andLocation:@"User Journey Page" andOrigin:nil andType:nil];
    
    IMGRestaurant *restaurant = [[IMGRestaurant alloc] init];
    restaurant.title = model.restaurantTitle;
    restaurant.latitude = model.restaurantLatitude;
    restaurant.longitude = model.restaurantLongitude;
    
    MapViewController *mapViewController_=[[MapViewController alloc] initWithRestaurant:restaurant];
    mapViewController_.amplitudeType = @"Homepage";
    mapViewController_.fromHomeCard = YES;
    [self.navigationController pushViewController:mapViewController_ animated:YES];


}
//touxiang (UpcomingBooking)
- (void)UpcomingBookingIconImageViewTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path book:(IMGJourneyBook *)model{

    DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:model.restaurantId];
    dvc.amplitudeType = @"Homepage";
    
    [self.navigationController pushViewController:dvc animated:YES];
}
- (void)delegateRefresh{

    [self downRefreshData];
}

//touxiang (cancelBooking)
- (void)CancelBookingViewMapTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path book:(IMGJourneyBook *)model{
    DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:model.restaurantId];
    dvc.amplitudeType = @"Homepage";
    [self.navigationController pushViewController:dvc animated:YES];
    
}
//touxiang (rated)
- (void)RatedIconImageViewTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path review:(IMGJourneyReview *)model{

    DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:model.restaurantId];
    dvc.amplitudeType = @"Homepage";
    [self.navigationController pushViewController:dvc animated:YES];
}
// write review  (rate)
- (void)didClickEditBtn:(IMGRestaurant *)restaurant andReviewId:(NSNumber*)reviewId indexPath:(NSIndexPath *)indexPath{

    ReviewPublishViewController *rpvc = [[ReviewPublishViewController alloc] initWithRestaurant:restaurant andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
    rpvc.isFromDetail = YES;
    rpvc.isUpdateReview = YES;
    rpvc.isEditReview = YES;
    rpvc.isEdit = YES;
    rpvc.jnCellIndexPath = indexPath;
    rpvc.reviewId = reviewId;
    rpvc.amplitudeType = @"User Journey Page";
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:rpvc] animated:YES completion:nil];

}

// touxiang (PhotoWithReview)
- (void)PhotoWithReviewIconImageViewTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path uploadPhoto:(IMGJourneyPhoto *)model{
    DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:model.restaurantId];
    dvc.amplitudeType = @"Homepage";
    [self.navigationController pushViewController:dvc animated:YES];
    
}
//touxiang ( onepictuer)
- (void)OnePictureIconImageViewTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path review:(IMGJourneyReview *)model{
    
    DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:model.restaurantId];
    dvc.amplitudeType = @"Homepage";
    [self.navigationController pushViewController:dvc animated:YES];
    

}
//touxiang (save)
- (void)saveIconImageViewTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path save:(IMGJourneySave *)model{

    DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:model.restaurantId];
    dvc.amplitudeType = @"Homepage";
    [self.navigationController pushViewController:dvc animated:YES];
}
//save  review
- (void)didwriteClickEditBtn:(IMGRestaurant *)restaurant andReviewId:(NSNumber*)reviewId indexPath:(NSIndexPath *)indexPath{

    ReviewPublishViewController *rpvc = [[ReviewPublishViewController alloc] initWithRestaurant:restaurant andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
    rpvc.isFromDetail = YES;
    rpvc.amplitudeType = @"User Journey Page";
//    rpvc.isUpdateReview = YES;
//    rpvc.isEditReview = YES;
    rpvc.isEdit = YES;
    rpvc.jnCellIndexPath = indexPath;
    rpvc.reviewId = reviewId;
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:rpvc] animated:YES completion:nil];
}
//touxiang (HasBooked)
- (void)HasBookedIconImageViewTableCell:(UITableViewCell *)tableViewCell indexPath:(NSIndexPath*)path book:(IMGJourneyBook *)model{

    DetailViewController *dvc = [[DetailViewController alloc] initWithRestaurantId:model.restaurantId];
    dvc.amplitudeType = @"Homepage";
    [self.navigationController pushViewController:dvc animated:YES];
}
//write review (PhotoWithoutReview)
- (void)PhotoWithoutReviewEditBtn:(IMGRestaurant *)restaurant andReviewId:(NSNumber*)reviewId indexPath:(NSIndexPath *)indexPath array:(NSMutableArray *)array{
    
    NSLog(@"edit");
    ReviewPublishViewController *publishReviewVC = [[ReviewPublishViewController alloc]initWithRestaurant:restaurant andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
    publishReviewVC.amplitudeType = @"User Journey Page";
    publishReviewVC.isUploadPhoto = YES;
    publishReviewVC.photosArrM = array;
    publishReviewVC.ifSuccessLoginClickPost = YES;
    publishReviewVC.isEdit = YES;
    publishReviewVC.isFromJourney = YES;
    publishReviewVC.uploadedPhotoId = reviewId;
    publishReviewVC.jnCellIndexPath = indexPath;
    [self.navigationController presentViewController:[[UINavigationController alloc] initWithRootViewController:publishReviewVC] animated:YES completion:nil];
    

}
//write review (OnePicture)
- (void)OnePictureEditBtn:(IMGRestaurant *)restaurant andReviewId:(NSNumber*)reviewId indexPath:(NSIndexPath *)indexPath{

    ReviewPublishViewController *rpvc = [[ReviewPublishViewController alloc] initWithRestaurant:restaurant andOverallRating:0 andCuisineRating:0 andAmbientRating:0 andServiceRating:0];
    rpvc.isFromDetail = YES;
    rpvc.isFromProfile = YES;
    rpvc.isUpdateReview = YES;
    rpvc.isEditReview = YES;
    rpvc.isEdit = YES;
    rpvc.jnCellIndexPath = indexPath;
    rpvc.reviewId = reviewId;
    rpvc.amplitudeType = @"User Journey Page";
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:rpvc] animated:YES completion:nil];
  
}

- (void)homeCardTableViewCell:(UITableViewCell *)cell likeButtonTapped:(UIButton *)button entity:(id)entity{
    
    NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            if ([[_tableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                return;
            }
            [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        
        @catch ( NSException *e ) {
            return;
        }
    });

}

//comment
- (void)goToCardDetailPageWithType:(int)type andData:(NSDictionary *)dataDic andCellIndexPath:(NSIndexPath*)cellIndexPath
{

    switch (type) {
        case 16: {
        
            IMGJourneyReview *review = [dataDic objectForKey:@"review"];
            V2_CommentListViewController *commentListVC = [[V2_CommentListViewController alloc] initWithTypeID:type andModel:review];
            
            [commentListVC setRefreshComment:^{
                int commentCount = [review.commentCount intValue];
                commentCount ++;
                review.commentCount = [NSNumber numberWithInt:commentCount];
                
                [self refreshCell:cellIndexPath];
            }];

            
            [self.navigationController pushViewController:commentListVC animated:YES];

            
        } break;
            
        case 17: {
            
            NSArray *dishArr = [dataDic objectForKey:@"dataArr"];
            IMGJourneyPhoto *user = [dataDic objectForKey:@"user"];
            if (dishArr.count>1) {
                V2_CommentListViewController *commentListVC = [[V2_CommentListViewController alloc] initWithTypeID:type andModel:user];
                
                [self.navigationController pushViewController:commentListVC animated:YES];
            }else{
                IMGDish *dish = [dishArr firstObject];
                V2_CommentListViewController *commentListVC = [[V2_CommentListViewController alloc] initWithTypeID:type andModel:dish];
                
                [self.navigationController pushViewController:commentListVC animated:YES];
            }

            
        } break;
        default:
            break;
    }

}
- (void)refreshCell:(NSIndexPath *)indexPath{
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            if ([[_tableView indexPathsForVisibleRows] indexOfObject:indexPath] == NSNotFound) {
                return;
            }
            if(journeyArr.count == 0) {
                return;
            }
            [_tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            NSLog(@"%f,getCommentData 3.0",[[NSDate date]timeIntervalSince1970]);
            
        }
        
        @catch ( NSException *e ) {
            return;
        }
    });
    
}

- (void)changeAvatarImage:(NSNotification *)noti{

    NSLog(@"=========");
//     self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(downRefreshData)];
}
- (void)changeCoverImage:(NSNotification *)noti{
    NSLog(@"~~~~~~~~~~");
//     self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(downRefreshData)];
}



@end
