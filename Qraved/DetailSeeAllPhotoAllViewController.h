//
//  DetailSeeAllPhotoAllViewController.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/10.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"
#import "IMGRestaurant.h"
@interface DetailSeeAllPhotoAllViewController : BaseViewController


@property (nonatomic,assign) BOOL isUseWebP;


-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant;
-(instancetype)initWithRestaurant:(IMGRestaurant*)paramRestaurant restaurantPhotoArray:(NSArray*)photoArr;
- (void)getDishesFromServer:(int)restaurantId max:(int)max;
@end
