//
//  PersonalLeftViewController.h
//  PersonalHomePageDemo
//
//  Created by Kegem Huang on 2017/3/15.
//  Copyright © 2017年 huangkejin. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "BaseTableViewController.h"
#import "IMGProfileModel.h"

@interface PersonalLeftViewController : UIViewController

@property (nonatomic,assign) BOOL isSelf;
@property(nonatomic,retain) IMGUser *otherUser;
@property (nonatomic, strong) IMGProfileModel * profileModel;

@end
