//
//  DetailSeeAllPhotoQravedViewController.m
//  Qraved
//
//  Created by Adam.zhang on 2017/7/10.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "DetailSeeAllPhotoQravedViewController.h"
#import "DetailSeeAllPhotoQravedModel.h"
#import "DetailDataHandler.h"
#import "IMGRestaurant.h"
#import "AlbumViewController.h"
#import "IMGDish.h"
#import "LoadingView.h"
@interface DetailSeeAllPhotoQravedViewController ()<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{

    UICollectionView *QravedCollectionView;
    DetailSeeAllPhotoQravedModel *QravedModel;
    IMGRestaurant *restaurant;
    int offset;
    BOOL noMorePhotos;
    IMGShimmerView *shimmerView;
}
@property (nonatomic, strong)  NSMutableArray  *dataArray;

@end

@implementation DetailSeeAllPhotoQravedViewController

-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant{
    self = [super init];
    if (self != nil) {
        restaurant = paramRestaurant;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor cyanColor];
    [self initUI];
    offset = 0;
    noMorePhotos = NO;
    _dataArray = [NSMutableArray array];
    QravedCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(loadMoreData)];

    [self requestData];
}
- (void)loadMoreData{
    
    [self initData];
}
- (void)requestData{
     [self initData];

};
- (void)initData{
    
    NSDictionary *dict=@{@"restaurantId":[NSString stringWithFormat:@"%@",restaurant.restaurantId],@"max":@"15",@"offset":[NSString stringWithFormat:@"%d",offset]};
//     [[LoadingView sharedLoadingView] startLoading];
    [DetailDataHandler SellAllPhotoQraved:dict andSuccessBlock:^(NSMutableArray * array,NSMutableArray *imgArray,NSString *goFoodlink) {
        if (offset == 0) {
            [shimmerView removeFromSuperview];
        }
        restaurant.goFoodLink = goFoodlink;
        [self loadURL:imgArray];
         [QravedCollectionView.mj_footer endRefreshing];
//         [[LoadingView sharedLoadingView] stopLoading];
        if ([array count] >0) {
            offset += 15;
        }else{
            noMorePhotos = YES;
        }
        [_dataArray addObjectsFromArray:array];
        [QravedCollectionView reloadData];
    } anderrorBlock:^{
         [QravedCollectionView.mj_footer endRefreshing];
//         [[LoadingView sharedLoadingView] stopLoading];
    }];

}

- (void)loadURL:(NSArray *)arr{
    for (NSString *imgString in arr) {
        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[imgString returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]] options:0 progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            
        }];
    }
    
}

- (void)initUI{
    
    UICollectionViewFlowLayout *layout  = [[UICollectionViewFlowLayout alloc] init];
    CGFloat width = (DeviceWidth-2)/3;
    layout.itemSize = CGSizeMake(width, width);
    layout.minimumLineSpacing = 1;
    layout.minimumInteritemSpacing = 1;
    //    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    
    QravedCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0,40, DeviceWidth, DeviceHeight - 40 - 44) collectionViewLayout:layout];
    QravedCollectionView.backgroundColor = [UIColor whiteColor];
    QravedCollectionView.delegate = self;
    QravedCollectionView.dataSource = self;
    QravedCollectionView.showsHorizontalScrollIndicator = NO;
    QravedCollectionView.showsVerticalScrollIndicator = NO;
    QravedCollectionView.bounces = NO;
    [self.view addSubview:QravedCollectionView];
    
    
    [QravedCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"photoCell"];
    
    shimmerView = [[IMGShimmerView alloc] initWithFrame:CGRectMake(0, 40, DeviceWidth, self.view.bounds.size.height - 40)];
    [shimmerView createPhotoShimmerView];
    [self.view addSubview:shimmerView];
}

#pragma mark -- collection  delegate
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"photoCell" forIndexPath:indexPath];
    
    for (UIView *subView in cell.contentView.subviews) {
        [subView removeFromSuperview];
    }
    QravedModel = [_dataArray objectAtIndex:indexPath.row];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.contentView.bounds];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    NSDate *datenow = [NSDate date];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[QravedModel.imageUrl returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]] placeholderImage:[UIImage imageNamed:@"placeholder"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
//        //image animation
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        if ([manager diskImageExistsForURL:[NSURL URLWithString:[QravedModel.imageUrl returnFullImageUrlWithImageSizeType:ImageSizeTypeLowResolution]]]) {
//            NSLog(@"dont loading animation");
        }else {
            if (image) {
                imageView.alpha = 0.0;
                [UIView transitionWithView:imageView duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                    imageView.image = image;
                    imageView.alpha = 1.0;
                } completion:NULL];
            }
        }
        
    }];
    [cell.contentView addSubview:imageView];
    
    if( indexPath.row==_dataArray.count-15) {
        
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            if (!noMorePhotos) {
                [self initData];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                
            });
            
        });
    }

    
    return cell;
    
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return _dataArray.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger tag = indexPath.row;
    NSMutableArray *images=[[NSMutableArray alloc] init];
    for(DetailSeeAllPhotoQravedModel *qravedModel in _dataArray){
        IMGDish *dish = [[IMGDish alloc] init];
        dish.imageUrl=qravedModel.imageUrl;
        dish.restaurantId=qravedModel.restaurantId;
        dish.dishId = qravedModel.myid;
        dish.userPhotoCountDic = qravedModel.photoCredit[@"userPhotoCount"];
        dish.photoCreditTypeDic  = qravedModel.photoCredit[@"photoCreditType"];
        dish.userIdDic  = qravedModel.photoCredit[@"userId"];
        dish.photoCreditDic  = qravedModel.photoCredit[@"photoCredit"];
        dish.userReviewCountDic  = qravedModel.photoCredit[@"userReviewCount"];
        dish.userAvatarDic  = qravedModel.photoCredit[@"userAvatar"];
        dish.photoCreditUrlDic  = qravedModel.photoCredit[@"photoCreditUrl"];
        dish.restaurantIdDic  = qravedModel.photoCredit[@"restaurantId"];
        dish.userName = qravedModel.creator;
        dish.descriptionStr = qravedModel.mydescription;
        dish.commentCount = qravedModel.commentCount;
        dish.likeCount = qravedModel.likeCount;
        dish.isLike = [qravedModel.isLike boolValue];
        NSDateFormatter *formatter=[[NSDateFormatter alloc] init];
        NSDate *createTime=[[NSDate alloc] init];
        [formatter setDateFormat:@"yyyy-MM-ddHH:mm:ss"];
        createTime=[formatter dateFromString:qravedModel.createTime];
        dish.createTime=[NSNumber numberWithDouble:createTime.timeIntervalSince1970];
        //dish.isRestaurantPhoto = YES;
        [images addObject:dish];
    }
//    [images addObjectsFromArray:userPhotoArray];
    
    AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:images andPhotoTag:tag andRestaurant:restaurant andFromDetail:NO];
    
    albumViewController.title = restaurant.title;
    albumViewController.showPage=NO;
    albumViewController.amplitudeType = @"Restaurant detail page - ";
    //albumViewController.isUseWebP = self.isUseWebP;
    [self.navigationController pushViewController:albumViewController animated:YES];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"%f",QravedCollectionView.contentOffset.y);
    if (QravedCollectionView.contentOffset.y <= 0) {
        QravedCollectionView.bounces = NO;
        
        NSLog(@"jinzhixiala");
    }
    else
        if (QravedCollectionView.contentOffset.y >= 0){
            QravedCollectionView.bounces = YES;
            NSLog(@"allow shangla");
            
        }
}

@end
