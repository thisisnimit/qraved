//
//  DetailSeeAllPhotoViewController.h
//  Qraved
//
//  Created by Adam.zhang on 2017/7/10.
//  Copyright © 2017年 Imaginato. All rights reserved.
//

#import "BaseViewController.h"

@interface DetailSeeAllPhotoViewController : BaseViewController


@property (nonatomic,assign) BOOL isFromSearch;
@property (nonatomic,assign) BOOL isUseWebP;
@property (nonatomic,assign) NSInteger currentIndex;


-(id)initWithRestaurant:(IMGRestaurant *)paramRestaurant;
-(instancetype)initWithRestaurant:(IMGRestaurant*)paramRestaurant restaurantPhotoArray:(NSArray*)photoArr;
- (void)getDishesFromServer:(int)restaurantId max:(int)max;

@end
