//
//  PersonalLeftViewController.m
//  PersonalHomePageDemo
//
//  Created by Kegem Huang on 2017/3/15.
//  Copyright © 2017年 huangkejin. All rights reserved.
//

#import "PersonalLeftViewController.h"
#import "UpcomingBookingTableViewCell.h"
#import "RatingsTableViewCell.h"
#import "MostTableViewCell.h"
#import "ProfileHandler.h"
#import "TopPhotoTableViewCell.h"
#import "GuideListTableViewCell.h"
#import "PersonalSummaryTopReviewTableViewCell.h"
#import "PersonalSummaryTopReviewModel.h"
#import "ReviewPublishViewController.h"
#import "DetailViewController.h"
#import "PersonalRightViewController.h"
#import "ReviewActivityItemProvider.h"
#import "TwitterActivityItemProvider.h"
#import "V2_CommentListViewController.h"
#import "AlbumViewController.h"
#import "IMGDish.h"
#import "MyListRestaurantsViewController.h"
#import "IMGMyList.h"
#import "DetailViewController.h"
#import "BookingConfirmViewController.h"
#import "MapViewController.h"
#import "CouponTableViewCell.h"
#import "CouponDetailViewController.h"

@interface PersonalLeftViewController ()<UITableViewDelegate, UITableViewDataSource, UpcomingBookingTableViewCellDelegate>
{
    IMGSummary * summary;
    NSArray * bookingArr;
}

@property (nonatomic, strong)  UITableView  *tableView;

@end

@implementation PersonalLeftViewController

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self RequestData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initUI];
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(downRefreshData)];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logOutClcik) name:@"logoutNotification" object:nil];
    
    
    IMGUser *user = [IMGUser currentUser];
    NSMutableDictionary *eventProperties = [NSMutableDictionary dictionary];
    [eventProperties setValue:user.userId forKey:@"userID"];
    [[Amplitude instance] logEvent:@"RC – View User Profile Summary Tab" withEventProperties:eventProperties];
    
}
- (void)logOutClcik{
    summary = nil;
    [self.tableView reloadData];
}
-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"logoutNotification" object:nil];
}
- (void)downRefreshData{
    [self RequestData];
}

- (void)initUI{
    
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    
    [self.tableView registerClass:[UpcomingBookingTableViewCell class] forCellReuseIdentifier:@"cellid"];
    [self.tableView registerClass:[RatingsTableViewCell class] forCellReuseIdentifier:@"cellRatings"];
    [self.tableView registerClass:[MostTableViewCell class] forCellReuseIdentifier:@"cellMost"];
    [self.tableView registerClass:[TopPhotoTableViewCell class] forCellReuseIdentifier:@"cellTopPhoto"];
    [self.tableView registerClass:[GuideListTableViewCell class] forCellReuseIdentifier:@"cellGuideList"];
    [self.tableView registerClass:[PersonalSummaryTopReviewTableViewCell class] forCellReuseIdentifier:@"CellSummaryTopReview"];
    [self.tableView registerClass:[CouponTableViewCell class] forCellReuseIdentifier:@"couponCell"];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)RequestData{
    [self loadData];
}

- (void)loadData{
    
    NSLog(@"%d",self.isSelf ? YES: NO);
    IMGUser *user = [IMGUser currentUser];
    if (user.userId!=nil) {
        [[LoadingView sharedLoadingView] startLoading];
        [ProfileHandler getsummary:@{@"targetUserId":self.isSelf?_otherUser.userId:user.userId,@"userId":user.userId} andSuccessBlock:^(IMGSummary *summ) {
            summary = summ;
            [self.tableView reloadData];
            
            [[LoadingView sharedLoadingView] stopLoading];
            [self.tableView.mj_header endRefreshing];
        } anderrorBlock:^{
            [[LoadingView sharedLoadingView] stopLoading];
            [self.tableView.mj_header endRefreshing];
        }];
        if (self.isSelf == NO) {
            [ProfileHandler getUpCommingBooking:@{@"userID":user.userId,@"t":user.token} andSuccessBlock:^(NSMutableArray * reservationArr) {
                if (reservationArr.count>0) {
                    bookingArr = reservationArr;
                    [self.tableView reloadData];
                }
            } anderrorBlock:^{
            }];
            
        }else{
            
            [ProfileHandler getOtherList:@{@"targetUserId":_otherUser.userId} andSuccessBlock:^(IMGListModel *model) {
                if (model.guideList.count >0) {
                    bookingArr = model.guideList;
                    [self.tableView reloadData];
                }
                
            } anderrorBlock:^{
            }];
        }
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
     return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
     return 7;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if (indexPath.row == 0) {
        if (summary.couponList.count > 0) {
            return 220;
        }else{
            return 0;
        }
    }else if (indexPath.row == 1) {
        if (bookingArr.count>0) {
            if (self.isSelf == NO) {
                return 199+30;
            }else{
                return 202+30;
            }
        }else{
            return 0;
        }
    }else if (indexPath.row ==2){
        return 128;
    }else if (indexPath.row==3){
        if (summary.cuisineReviewList.count>0) {
            return 75+17*summary.cuisineReviewList.count;
        }else{
            return 0;
        }
    }else if (indexPath.row==4){
        if (summary.districtReviewList.count>0) {
            return 75+17*summary.districtReviewList.count;
        }else{
            return 0;
        }
    }else if (indexPath.row==5){
        if (summary.topPhotoList.count != 0) {
            return 197;
        }else{
            return 0;
        }
        
    }else if (indexPath.row == 6){
        return [tableView cellHeightForIndexPath:indexPath model:summary keyPath:@"summary" cellClass:[PersonalSummaryTopReviewTableViewCell class] contentViewWidth:DeviceWidth];
//        if (summary.topReviews.count>0) {
//
//        }else{
//            return 0;
//        }
    }
   
    return 0.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row ==0) {
        CouponTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"couponCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        for (UIView *subView in cell.contentView.subviews)
        {
            [subView removeFromSuperview];
        }
        if (summary.couponList.count >0) {
            __weak typeof(self) weakSelf = self;
            cell.couponViewTapped = ^(CouponModel *model) {
                [weakSelf couponTapped:model];
            };
            [cell createUIWithModel:summary.couponList];
        }
        return cell;
    }else if (indexPath.row==1) {
        if (self.isSelf == NO) {
            UpcomingBookingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellid" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            for (UIView *subView in cell.contentView.subviews)
            {
                [subView removeFromSuperview];
            }
            if (bookingArr.count>0) {
                cell.delegate = self;
                [cell createUIWithModel:bookingArr];
            }
            
            return cell;
        }else{
            
            GuideListTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cellGuideList" forIndexPath:indexPath];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            for (UIView *subView in cell.contentView.subviews)
            {
                [subView removeFromSuperview];
            }
            
            if (bookingArr.count>0) {
                [cell createUIWithArray:bookingArr andName:self.otherUser.userName];
                
                cell.listDetail = ^(IMGGuideModel *guideModel) {
                    [self gotoListDetail:guideModel];
                };
            }
            
            return cell;
            
        }
    }else if (indexPath.row ==2){
        
        RatingsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cellRatings" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        for (UIView *subView in cell.contentView.subviews)
        {
            [subView removeFromSuperview];
        }
        [cell createUIWithModel:summary];
        
        return cell;
    }else if (indexPath.row==3){
        
        MostTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cellMost" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        for (UIView *subView in cell.contentView.subviews)
        {
            [subView removeFromSuperview];
        }
        if (summary.cuisineReviewList.count>0) {
            [cell createUIWithModel:summary andIndex:indexPath.row];
        }
        
        return cell;
    }else if (indexPath.row==4){
        
        MostTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cellMost" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        for (UIView *subView in cell.contentView.subviews)
        {
            [subView removeFromSuperview];
        }
        if (summary.districtReviewList.count>0) {
            [cell createUIWithModel:summary andIndex:indexPath.row];
        }
        return cell;
    }else if (indexPath.row == 5){
        TopPhotoTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cellTopPhoto" forIndexPath:indexPath];
        for (UIView *subView in cell.contentView.subviews)
        {
            [subView removeFromSuperview];
        }
        cell.goToPhotos = ^(){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"goToPhotos" object:nil];
        };
        __weak typeof(self) weakSelf = self;
        cell.photoClick = ^(NSArray *photoArray,NSInteger index){
            [weakSelf photoTapped:photoArray andIndex:index];
        };
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if (summary.topPhotoList.count>0) {
            [cell createUIWithModel:summary];
        }
        return cell;
        
    }else{
        PersonalSummaryTopReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellSummaryTopReview"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        for (UIView *subView in cell.contentView.subviews)
        {
            [subView removeFromSuperview];
        }
        
        if (summary.topReviews.count > 0) {
            cell.summary = summary;
        }
        
        

        
//        if (SummaryTopReviewModel==nil) {
//            cell.hidden = YES;
//        }else{
//            //                [cell createUIWithModel:SummaryTopReviewModel];
//            //                    [cell createUIWithModel:SummaryTopReviewModel mini1RestaurantM:self.mini1RestaurantModel locationM:self.locationModel];
//            cell.homeReviewDelegate = self;
//            cell.fromCellIndexPath = indexPath;
//        }
        
        return cell;
    }
}

- (void)photoTapped:(NSArray *)photoArray andIndex:(NSInteger)index{
    NSMutableArray *photoArr = [NSMutableArray array];
    for (IMGPhotoModel *model in photoArray) {
        IMGDish *dish = [[IMGDish alloc] init];
        dish.dishId = model.myID;
        dish.userId = model.userId;
        dish.restaurantId = model.restaurantId;
        dish.commentCount = model.commentCount;
        dish.likeCount = model.likeCount;
        dish.descriptionStr = model.des;
        //dish.createTime = model.createTime;
        //dish.userName = model.creator;
        dish.type = model.type;
        dish.restaurantTitle = model.restaurantTitle;
        dish.imageUrl = model.imageUrl;
        dish.isRestaurantPhoto = YES;
        [photoArr addObject:dish];
    }
    
    AlbumViewController *albumViewController = [[AlbumViewController alloc]initWithPhotosArray:photoArr andPhotoTag:index andRestaurant:nil andFromDetail:NO];
    albumViewController.amplitudeType = @"User Top Photos";
    albumViewController.showPage=NO;
    [self.navigationController pushViewController:albumViewController animated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"点击left-%d",(int)indexPath.row);
}

- (void)couponTapped:(CouponModel *)model{
    
    [IMGAmplitudeUtil trackCampaignWithName:@"RC - View Coupon Detail" andCampaignId:nil andBrandId:nil andCouponId:model.coupon_id andLocation:nil andOrigin:@"Profile" andIsMall:NO];
    
    CouponDetailViewController *couponDetailViewController = [[CouponDetailViewController alloc] init];
    couponDetailViewController.couponId = model.coupon_id;
    //couponDetailViewController.isFromProfile = YES;
    MLNavigationController *nav = [[MLNavigationController alloc] initWithRootViewController:couponDetailViewController];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

- (void)gotoListDetail:(IMGGuideModel *)model{
    IMGMyList *myList = [[IMGMyList alloc] init];
    myList.listId = model.myID;
    myList.listName = model.name;
    myList.listType = model.listType;
    
    MyListRestaurantsViewController *listRestaurantVC = [[MyListRestaurantsViewController alloc] init];
    listRestaurantVC.isOtherUser = YES;
    listRestaurantVC.otherUser = self.otherUser;
    listRestaurantVC.mylist = myList;
    [self.navigationController pushViewController:listRestaurantVC animated:YES];
}

- (void)gotoRDP:(NSNumber *)restaurantId{
    DetailViewController *detail = [[DetailViewController alloc] initWithRestaurantId:restaurantId];
    [self.navigationController pushViewController:detail animated:YES];
}

- (void)gotoBookDetail:(IMGReservation *)book andRestaurant:(IMGRestaurant *)restaurant{
    BookingConfirmViewController *confirm = [[BookingConfirmViewController alloc] initWithReservation:book];
    confirm.isFromProfile = YES;
//    confirm.shopStr = book.restaurantName;
//    confirm.iconImage = book.restaurantImage;
//    confirm.detailStr = book.restaurantCuisine;
    [self.navigationController pushViewController:confirm animated:YES];
}

- (void)map:(IMGRestaurant *)restaurant{
    IMGUser *user = [IMGUser currentUser];
    NSNumber *userId = [[NSNumber alloc] init];
    if (user.userId != nil)
    {
        userId = user.userId;
    }
    else
        userId = [NSNumber numberWithInt:0];
    
    //    [TrackHandler trackWithUserId:userId andRestaurantId:restaurant.restaurantId andContent:[NSDictionary dictionaryWithObject:@"1" forKey:@"type"]];
    [IMGAmplitudeUtil trackBookWithName:@"RC - View Restaurant Map" andRestaurantId:restaurant.restaurantId andReservationId:nil andLocation:@"User Profile Page - Summary" andChannel:nil];
    
    MapViewController *mapViewController=[[MapViewController alloc]initWithRestaurant:restaurant];
    mapViewController.fromDetail = YES;
    [self.navigationController pushViewController:mapViewController animated:YES];
}

- (void)reminder:(IMGRestaurant *)restaurant andBook:(IMGReservation *)book{
    //    IMGReservation *currentReservation=reservation;
    //    IMGRestaurant *currentRestaurant=restaurant;
    EKEventStore *eventStore = [[EKEventStore alloc] init];
    if ([eventStore respondsToSelector:@selector(requestAccessToEntityType:completion:)]){
        // the selector is available, so we must be on iOS 6 or newer
        [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if(error){
                    // display error message here
                }else if(!granted){
                    // display access denied error message here
                }else{
                    // access granted
                    // ***** do the important stuff here *****
                    EKEvent *ekEvent  = [EKEvent eventWithEventStore:eventStore];
                    ekEvent.title     = restaurant.title;
                    ekEvent.location = [restaurant address];
                    
                    [IMGAmplitudeUtil trackBookWithName:@"CL - Booking Reminder CTA" andRestaurantId:restaurant.restaurantId andReservationId:nil andLocation:@"User Profile Page - Summary" andChannel:nil];
                    
                    NSDateFormatter *tempFormatter = [[NSDateFormatter alloc]init];
                    [tempFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
                    ekEvent.startDate=[tempFormatter dateFromString:[NSString stringWithFormat:@"%@ %@",book.bookDate,book.bookTime]];
                    ekEvent.endDate=[tempFormatter dateFromString:[NSString stringWithFormat:@"%@ %@",book.bookDate,book.bookTime]];
                    
                    [tempFormatter setDateFormat:@"dd.MM.yyyy HH:mm"];
                    ekEvent.allDay = NO;
                    
                    //添加提醒
                    [ekEvent addAlarm:[EKAlarm alarmWithRelativeOffset:60.0f * -60.0f * 24]];
                    [ekEvent addAlarm:[EKAlarm alarmWithRelativeOffset:60.0f * -15.0f]];
                    
                    [ekEvent setCalendar:[eventStore defaultCalendarForNewEvents]];
                    NSError *err;
                    [eventStore saveEvent:ekEvent span:EKSpanThisEvent error:&err];
                    
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:L(@"Calendar") message:L(@"Reservation is added to calendar.") delegate:nil cancelButtonTitle:L(@"OK") otherButtonTitles:nil];
                    [alert show];
                    
                }
            });
        }];
    }
    
}

@end
